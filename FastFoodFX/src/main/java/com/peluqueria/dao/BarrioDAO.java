package com.peluqueria.dao;

import com.peluqueria.core.domain.Barrio;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface BarrioDAO extends CRUDGenericDAO<Barrio> {

    Long rowCount();

    List<Barrio> listarNoFetch(long limRow, long offSet);

    List<Barrio> listarFETCH(long limRow, long offSet);

    List<Barrio> getByIdCiudad(long idCi);

    public List<Barrio> listarPorCiudad(long idCiudad);
}
