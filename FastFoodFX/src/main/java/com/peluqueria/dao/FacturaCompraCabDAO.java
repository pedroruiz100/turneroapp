package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaCompraCab;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface FacturaCompraCabDAO extends CRUDGenericDAO<FacturaCompraCab> {

    FacturaCompraCab insertarObtenerObj(FacturaCompraCab recep);

    public FacturaCompraCab getByNroOrden(String text);

    public List<FacturaCompraCab> listarPorFecha(Date desde, Date hasta);

    public Map getByNroOrdenEntrada(String text, String text0);

    public List<FacturaCompraCab> getByNroOrdenEntradaFecha(String text, String text0, LocalDate value);

    public FacturaCompraCab getByIdFecha(Long idPedidoCab, LocalDate value);

    public FacturaCompraCab getByNroOrdenPedido(String text);

    public FacturaCompraCab getByOC(String oc);

    public List<FacturaCompraCab> getByOCFiltro(String text);

    public FacturaCompraCab getByIdCompra(long parseLong);

    public List<FacturaCompraCab> listarCabeceras(String toString, String toString0);

    public String recuperarOcByFecha(String fecha, String idProv);

}
