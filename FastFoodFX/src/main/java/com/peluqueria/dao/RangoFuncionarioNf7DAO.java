package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf7;

public interface RangoFuncionarioNf7DAO extends CRUDGenericDAO<RangoFuncionarioNf7> {

    boolean insertarObtenerEstado(RangoFuncionarioNf7 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
