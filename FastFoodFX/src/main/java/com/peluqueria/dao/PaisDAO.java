package com.peluqueria.dao;

import com.peluqueria.core.domain.Pais;

/**
 *
 * @author ExcelsisWalker
 */
public interface PaisDAO extends CRUDGenericDAO<Pais> {

}
