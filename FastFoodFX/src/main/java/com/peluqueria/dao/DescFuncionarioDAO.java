package com.peluqueria.dao;

import com.peluqueria.core.domain.DescFuncionario;

public interface DescFuncionarioDAO extends CRUDGenericDAO<DescFuncionario> {

}
