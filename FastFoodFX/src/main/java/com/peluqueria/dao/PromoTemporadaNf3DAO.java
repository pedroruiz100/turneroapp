package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf3;

public interface PromoTemporadaNf3DAO extends CRUDGenericDAO<PromoTemporadaNf3> {

    void insercionMasiva(PromoTemporadaNf3 promoTemporadaNf3, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf3 promoTemporadaNf3);
}
