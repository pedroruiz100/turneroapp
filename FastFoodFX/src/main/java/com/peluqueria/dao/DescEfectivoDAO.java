package com.peluqueria.dao;

import com.peluqueria.core.domain.DescEfectivo;

public interface DescEfectivoDAO extends CRUDGenericDAO<DescEfectivo> {

}
