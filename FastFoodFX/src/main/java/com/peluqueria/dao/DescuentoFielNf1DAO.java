package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf1;

public interface DescuentoFielNf1DAO extends CRUDGenericDAO<DescuentoFielNf1> {

    void insercionMasiva(DescuentoFielNf1 descuentoFielNf1, long i);

    boolean insercionMasivaEstado(DescuentoFielNf1 descuentoFielNf1);

    boolean insertarObtenerEstado(DescuentoFielNf1 descuentoFielNf1);

}
