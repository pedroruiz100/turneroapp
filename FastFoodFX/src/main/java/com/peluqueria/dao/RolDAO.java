package com.peluqueria.dao;

import com.peluqueria.core.domain.Rol;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface RolDAO extends CRUDGenericDAO<Rol> {

    Rol insertarObtenerObjeto(Rol rol);

    List<Rol> listarFETCH(long limRow, long offSet);

    List<Rol> listarFETCHFiltro(long limRow, long offSet, String nombre, boolean activo, boolean inactivo);

    long rowCount();

    long rowCountFiltro(String nombre, boolean activo, boolean inactivo);

//    Rol insertarObtenerObjeto(Rol rol);

    List<Rol> buscarRolUsuarioAsignado(long idUsuario);

    List<Rol> buscarRolUsuarioNOAsignado(long idUsuario);

}
