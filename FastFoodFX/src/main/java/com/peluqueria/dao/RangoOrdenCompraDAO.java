package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoOrdenCompra;

public interface RangoOrdenCompraDAO extends CRUDGenericDAO<RangoOrdenCompra> {

    boolean insertarObtenerEstado(RangoOrdenCompra rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

    public String recuperarActual();

}
