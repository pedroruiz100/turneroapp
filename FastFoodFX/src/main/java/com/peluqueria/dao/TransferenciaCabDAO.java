package com.peluqueria.dao;

import com.peluqueria.core.domain.TransferenciaCab;

public interface TransferenciaCabDAO extends CRUDGenericDAO<TransferenciaCab> {

//    void cancelarFactura(long id, String usuMod);
//
//    void actualizarMontoVenta(int monto, long id);

    TransferenciaCab insertarObtenerObjeto(TransferenciaCab fac);

    TransferenciaCab actualizarObtenerObjeto(
            TransferenciaCab transCab);

}
