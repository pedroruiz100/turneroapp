package com.peluqueria.dao;

import com.peluqueria.core.domain.SeccionFunc;
import java.sql.Timestamp;
import java.util.List;

public interface SeccionFuncDAO extends CRUDGenericDAO<SeccionFunc> {

    List<SeccionFunc> listarFETCH(long limRow, long offSet);

    List<SeccionFunc> listarFETCHSeccion(long limRow, long offSet, String nombre);

    Long rowCount();

    long rowCountFiler(String nombre);

    void bajas();

    public boolean actualizarObtenerEstado(SeccionFunc fromSeccionFuncAsociadoDTO);

    public boolean bajasLocal(String u, Timestamp ts);

    public SeccionFunc insertarObtenerObjeto(SeccionFunc fromSeccionFuncAsociadoDTO);
}
