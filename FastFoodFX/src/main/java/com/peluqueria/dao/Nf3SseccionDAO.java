package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf3Sseccion;
import java.util.List;

public interface Nf3SseccionDAO extends CRUDGenericDAO<Nf3Sseccion> {

    List<Nf3Sseccion> listarNombreId();

    List<Nf3Sseccion> listarFuncionarioNombreId();

    List<Nf3Sseccion> listarPromoTemp(String inicio, String fin);

    public List<Nf3Sseccion> listarPorNf(long id);

}
