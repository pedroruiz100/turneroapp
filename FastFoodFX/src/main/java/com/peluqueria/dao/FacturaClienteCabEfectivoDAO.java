package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabEfectivo;
import java.util.List;

public interface FacturaClienteCabEfectivoDAO extends CRUDGenericDAO<FacturaClienteCabEfectivo> {

    FacturaClienteCabEfectivo insertarObtenerObjeto(
            FacturaClienteCabEfectivo facturaClienteCabEfectivo);

    public List<FacturaClienteCabEfectivo> listarPorFactura(long parseLong);

    public List<FacturaClienteCabEfectivo> listarPorFechaHoy();

}
