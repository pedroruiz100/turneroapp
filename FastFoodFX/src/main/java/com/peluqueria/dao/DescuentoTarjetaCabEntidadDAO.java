package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabEntidad;
import java.util.List;

public interface DescuentoTarjetaCabEntidadDAO extends
        CRUDGenericDAO<DescuentoTarjetaCabEntidad> {

    List<DescuentoTarjetaCabEntidad> listarLimitado(long limite, long inicio,
            String nombre);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad);
}
