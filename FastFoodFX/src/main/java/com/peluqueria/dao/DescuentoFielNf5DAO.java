package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf5;

public interface DescuentoFielNf5DAO extends CRUDGenericDAO<DescuentoFielNf5> {

    void insercionMasiva(DescuentoFielNf5 descuentoFielNf5, long i);

    boolean insercionMasivaEstado(DescuentoFielNf5 descuentoFielNf5);

    boolean insertarObtenerEstado(DescuentoFielNf5 descuentoFielNf5);
}
