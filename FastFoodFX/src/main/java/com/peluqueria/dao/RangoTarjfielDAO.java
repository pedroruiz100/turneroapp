package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFactura;
import com.peluqueria.core.domain.RangoTarjfiel;

public interface RangoTarjfielDAO extends CRUDGenericDAO<RangoTarjfiel> {

    boolean insertarObtenerEstado(RangoFactura rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
