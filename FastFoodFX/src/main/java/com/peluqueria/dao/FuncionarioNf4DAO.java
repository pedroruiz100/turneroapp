package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf4;

public interface FuncionarioNf4DAO extends CRUDGenericDAO<FuncionarioNf4> {

    List<FuncionarioNf4> listarFetchNf4(long limRow, long offSet);

    List<FuncionarioNf4> listarFetchFilterNf4(long limRow, long offSet, String nombre);

    Long rowCountNf4();

    long rowCountFilterNf4(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf4 insertarObtenerObjeto(FuncionarioNf4 funcionarioNf4);

    public boolean actualizarObtenerEstado(FuncionarioNf4 funcionarioNf4);
    
    boolean validacionInsercion(long idNf);

}
