package com.peluqueria.dao;

import com.peluqueria.core.domain.ManejoLocal;

public interface ManejoLocalDAO extends CRUDGenericDAO<ManejoLocal> {

    public boolean actualizarObtenerEstado(ManejoLocal manejo);

    public boolean insertarObtenerEstado(ManejoLocal manejo);

    public boolean eliminarObtenerEstado(long id);

    public long verificarExistencia();

    public long recuperarId();

}
