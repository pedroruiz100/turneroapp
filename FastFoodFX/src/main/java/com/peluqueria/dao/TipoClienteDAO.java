package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoCliente;

public interface TipoClienteDAO extends CRUDGenericDAO<TipoCliente> {

}
