package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteDet;
import java.util.List;

public interface FacturaClienteDetDAO extends CRUDGenericDAO<FacturaClienteDet> {

    void insercionMasiva(FacturaClienteDet facDet, long i);

    public List<FacturaClienteDet> filtroFechaDescuentoFact(String numFactura, String fechaInicio, String fechaFin, String selectedItem);

    public List<FacturaClienteDet> filtroFechaDescuento(String numFactura, String fechaInicio, String fechaFin, String selectedItem);

    public List<FacturaClienteDet> listarPorFactura(long parseLong);

    public List<FacturaClienteDet> filtroFecha(String fechaInicio, String fechaFin);

}
