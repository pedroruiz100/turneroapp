package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf3;

public interface RangoFuncionarioNf3DAO extends CRUDGenericDAO<RangoFuncionarioNf3> {

    boolean insertarObtenerEstado(RangoFuncionarioNf3 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
