package com.peluqueria.dao;

import com.peluqueria.core.domain.Caja;
import java.security.NoSuchAlgorithmException;

public interface CajaDAO extends CRUDGenericDAO<Caja> {

    Caja buscarCaja(Integer nroCaja, String clave) throws NoSuchAlgorithmException;
}
