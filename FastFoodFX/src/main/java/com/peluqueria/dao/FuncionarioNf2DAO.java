package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf2;

public interface FuncionarioNf2DAO extends CRUDGenericDAO<FuncionarioNf2> {

    List<FuncionarioNf2> listarFetchNf2(long limRow, long offSet);

    List<FuncionarioNf2> listarFetchFilterNf2(long limRow, long offSet, String nombre);

    Long rowCountNf2();

    long rowCountFilterNf2(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf2 insertarObtenerObjeto(FuncionarioNf2 funcionarioNf2);

    public boolean actualizarObtenerEstado(FuncionarioNf2 funcionarioNf2);
    
    boolean validacionInsercion(long idNf);

}
