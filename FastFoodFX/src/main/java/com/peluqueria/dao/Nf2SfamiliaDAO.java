package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf2Sfamilia;
import java.util.List;

public interface Nf2SfamiliaDAO extends CRUDGenericDAO<Nf2Sfamilia> {

    List<Nf2Sfamilia> listarNombreId();

    List<Nf2Sfamilia> listarFuncionarioNombreId();

    List<Nf2Sfamilia> listarPromoTemp(String inicio, String fin);

    public List<Nf2Sfamilia> listarPorNf(long idNf1);

}
