package com.peluqueria.dao;

import com.peluqueria.core.domain.ArtPromoTemporada;

public interface ArtPromoTemporadaDAO extends
        CRUDGenericDAO<ArtPromoTemporada> {

    void insercionMasiva(ArtPromoTemporada artDTO, long i);

    boolean insercionMasivaEstado(ArtPromoTemporada artDTO);

}
