/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.Cuponera;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface CuponeraDAO extends CRUDGenericDAO<Cuponera> {

    Cuponera insertarObtenerObjeto(Cuponera cuponera);

    List<Cuponera> listarFETCH(long limRow, long offSet);

    Long rowCount();

    List<Cuponera> listarFETCHCuponera(long limRow, long offSet,
            String nombre, String fechaInicio, String fechaFin);

    Long rowCountFiler(String nombre, String fechaInicio, String fechaFin);

    List<Cuponera> listarPorFechaActual(Timestamp tiempoActual);

    void actualizarPorFecha(Cuponera desc, int i);

    void bajas();

    public boolean bajasLocal(String u, Timestamp ts);

    public boolean actualizarObtenerEstado(Cuponera fromCuponeraDTO);

    boolean verificandoInsercion(String descripcion, Timestamp fecha);
    
    boolean verificandoExistencia();
}
