package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoMonedaExtranjera;

public interface RangoMonedaExtranjeraDAO extends CRUDGenericDAO<RangoMonedaExtranjera> {

    long actualizarRecuperarRangoActual(long idRango);
}
