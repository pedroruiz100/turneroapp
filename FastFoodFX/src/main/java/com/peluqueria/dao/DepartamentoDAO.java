package com.peluqueria.dao;

import com.peluqueria.core.domain.Departamento;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface DepartamentoDAO extends CRUDGenericDAO<Departamento> {

    public List<Departamento> listarPorPais(long idPais);

}
