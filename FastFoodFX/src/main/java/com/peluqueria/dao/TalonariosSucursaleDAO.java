package com.peluqueria.dao;

import com.peluqueria.core.domain.TalonariosSucursales;

public interface TalonariosSucursaleDAO extends CRUDGenericDAO<TalonariosSucursales> {

    boolean actualizarNroActual(TalonariosSucursales talo);

}
