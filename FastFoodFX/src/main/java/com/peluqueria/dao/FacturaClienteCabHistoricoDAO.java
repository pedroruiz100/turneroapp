package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import java.util.List;

public interface FacturaClienteCabHistoricoDAO extends CRUDGenericDAO<FacturaClienteCabHistorico> {

    public List<FacturaClienteCabHistorico> listarPorFactura(Long idFacturaClienteCab);

    public FacturaClienteCabHistorico consultar(String nroFactFiltro, String tipoFactFiltro, String fechaFactFiltro, String nroCajaFiltro, String timbradoFiltro);

    public List<Integer> listarCajaDistinct();

    public List<String> listarTimbDistinct();

}
