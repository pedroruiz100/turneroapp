package com.peluqueria.dao;

import com.peluqueria.core.domain.FamiliaProv;

public interface FamiliaProvDAO extends CRUDGenericDAO<FamiliaProv> {

}
