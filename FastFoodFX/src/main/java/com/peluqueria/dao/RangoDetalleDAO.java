package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoDetalle;

public interface RangoDetalleDAO extends CRUDGenericDAO<RangoDetalle> {

    long actualizarObtenerRango(long idRango);

}
