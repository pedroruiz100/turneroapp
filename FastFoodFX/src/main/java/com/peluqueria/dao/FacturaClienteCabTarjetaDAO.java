package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabTarjeta;
import java.util.List;

public interface FacturaClienteCabTarjetaDAO extends CRUDGenericDAO<FacturaClienteCabTarjeta> {

    FacturaClienteCabTarjeta insercionMasiva(FacturaClienteCabTarjeta fac, int id);

    FacturaClienteCabTarjeta insertarObtenerObj(
            FacturaClienteCabTarjeta facturaClienteCabTarjeta);

    public List<FacturaClienteCabTarjeta> listarPorFactura(long parseLong);

    public List<FacturaClienteCabTarjeta> filtroFechaTarjeta(String fechaInicio, String fechaFin, String selectedItem);

    public List<FacturaClienteCabTarjeta> listarPorFechaHoy();
}
