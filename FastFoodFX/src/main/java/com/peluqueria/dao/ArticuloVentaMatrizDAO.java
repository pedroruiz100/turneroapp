package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloCompraMatriz;
import com.peluqueria.core.domain.ArticuloVentaMatriz;
import java.time.LocalDate;

/**
 *
 * @author ExcelsisWalker
 */
public interface ArticuloVentaMatrizDAO extends CRUDGenericDAO<ArticuloVentaMatriz> {

//    public List<ArticuloCompraMatriz> listarPorCiudad(long idCiudad);
    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);
    
    public ArticuloVentaMatriz getByCodFecha(String toString, LocalDate nowLast, String sucursal);
}
