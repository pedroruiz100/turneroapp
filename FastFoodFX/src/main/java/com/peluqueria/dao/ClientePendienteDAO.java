package com.peluqueria.dao;

import com.peluqueria.core.domain.ClientePendiente;
import java.sql.Timestamp;
import java.util.List;

public interface ClientePendienteDAO extends CRUDGenericDAO<ClientePendiente> {

    ClientePendiente insertarObtenerObj(ClientePendiente fromClientePendiente);

    List<ClientePendiente> listarPendiente();

    public boolean actualizarEstado(long idCliPen, Timestamp timestamp, String nombreCaj);

}
