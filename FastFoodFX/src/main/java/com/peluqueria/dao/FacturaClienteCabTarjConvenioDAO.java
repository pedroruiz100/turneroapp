package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabTarjConvenio;

public interface FacturaClienteCabTarjConvenioDAO extends CRUDGenericDAO<FacturaClienteCabTarjConvenio> {

    FacturaClienteCabTarjConvenio insertarObtenerObjeto(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio);

}
