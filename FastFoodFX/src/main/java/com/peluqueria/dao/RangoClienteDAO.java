package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoCliente;

public interface RangoClienteDAO extends CRUDGenericDAO<RangoCliente> {

    long actualizarObtenerRangoActual(long idRango);

}
