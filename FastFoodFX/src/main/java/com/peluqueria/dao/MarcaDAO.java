package com.peluqueria.dao;

import com.peluqueria.core.domain.Marca;

public interface MarcaDAO extends CRUDGenericDAO<Marca> {

}
