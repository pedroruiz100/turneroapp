package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaDet;
import java.util.List;

public interface DescuentoTarjetaDetDAO extends
        CRUDGenericDAO<DescuentoTarjetaDet> {

    List<DescuentoTarjetaDet> listarLimitado(long limite, long inicio,
            String nombre);

    public boolean insertarObtenerEstado(DescuentoTarjetaDet fromDescuentoTarjetaDetDTO);

}
