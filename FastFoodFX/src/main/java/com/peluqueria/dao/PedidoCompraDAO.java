package com.peluqueria.dao;

import com.peluqueria.core.domain.PedidoCompra;
import java.time.LocalDate;
import java.util.List;

public interface PedidoCompraDAO extends CRUDGenericDAO<PedidoCompra> {

    void insercionMasiva(PedidoCompra facDet, long i);

    public List<PedidoCompra> listarPorFechaRecepcionActual(LocalDate value, LocalDate value0, String idProveedor);

    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public List<PedidoCompra> listarPorCabecera(long idPedCab);

    public boolean eliminarPorcabecera(Long idPedidoCab);

    public PedidoCompra getByIdPedidoCabAndCodArt(long parseLong, Long codArticulo);

}
