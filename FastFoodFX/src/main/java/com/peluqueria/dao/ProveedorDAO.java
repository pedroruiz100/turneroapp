package com.peluqueria.dao;

import com.peluqueria.core.domain.Proveedor;
import java.util.List;

public interface ProveedorDAO extends CRUDGenericDAO<Proveedor> {

    public Proveedor listarPorRuc(String rucCliente);

    public List<Proveedor> listarPorCIRevancha(String rucCliente);

    public List<Proveedor> listarPorNomRuc(String nom, String ruc);

    public boolean insertarEstado(Proveedor pro);

    public boolean actualizarEstado(Proveedor pro);

}
