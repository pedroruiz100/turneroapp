package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf6Secnom6;

public interface ArticuloNf6Secnom6DAO extends CRUDGenericDAO<ArticuloNf6Secnom6> {

    public ArticuloNf6Secnom6 getByIdArticulo(long id);

    public ArticuloNf6Secnom6 listarArticulo(long idArt);

}
