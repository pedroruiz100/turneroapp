package com.peluqueria.dao;

import com.peluqueria.core.domain.IpBoca;

public interface IpBocaDAO extends CRUDGenericDAO<IpBoca> {

}
