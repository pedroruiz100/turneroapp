package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf7;

public interface DescuentoTarjetaCabNf7DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf7> {

    void insercionMasiva(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7);
}
