package com.peluqueria.dao;

import com.peluqueria.core.domain.Funcion;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface FuncionDAO extends CRUDGenericDAO<Funcion> {

    List<Funcion> buscarFuncion(String cadenaIN);

    public Funcion getByIdFuncion(long parseLong);

    public List<String> buscarRolFuncionAsignado(long idModulo, long idRol);

    public List<String> buscarRolFuncionNoAsignado(long idModulo, long idRol);

}
