package com.peluqueria.dao;

import com.peluqueria.core.domain.DescTarjetaFiel;

public interface DescTarjetaFielDAO extends CRUDGenericDAO<DescTarjetaFiel> {

}
