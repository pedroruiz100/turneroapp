package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf1Tipo;
import java.util.List;

public interface Nf1TipoDAO extends CRUDGenericDAO<Nf1Tipo> {

    List<Nf1Tipo> listarNombreId();

    List<Nf1Tipo> listarFuncionarioNombreId();

    List<Nf1Tipo> listarPromoTemp(String inicio, String fin);

}
