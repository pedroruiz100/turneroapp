package com.peluqueria.dao;

import com.peluqueria.core.domain.DescCombo;

public interface DescComboDAO extends CRUDGenericDAO<DescCombo> {

}
