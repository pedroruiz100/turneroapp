package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf5Seccion2;
import java.util.List;

public interface Nf5Seccion2DAO extends CRUDGenericDAO<Nf5Seccion2> {

    List<Nf5Seccion2> listarNombreId();

    List<Nf5Seccion2> listarFuncionarioNombreId();

    List<Nf5Seccion2> listarPromoTemp(String inicio, String fin);
    
    public List<Nf5Seccion2> listarPorNf(long id);

}
