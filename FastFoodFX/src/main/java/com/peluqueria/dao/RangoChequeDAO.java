package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoCheque;

public interface RangoChequeDAO extends CRUDGenericDAO<RangoCheque> {

    long actualizarObtenerRango(long idRango);

}
