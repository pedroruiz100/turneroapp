package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf5;

public interface FuncionarioNf5DAO extends CRUDGenericDAO<FuncionarioNf5> {

    List<FuncionarioNf5> listarFetchNf5(long limRow, long offSet);

    List<FuncionarioNf5> listarFetchFilterNf5(long limRow, long offSet, String nombre);

    Long rowCountNf5();

    long rowCountFilterNf5(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf5 insertarObtenerObjeto(FuncionarioNf5 funcionarioNf5);

    public boolean actualizarObtenerEstado(FuncionarioNf5 funcionarioNf5);
    
    boolean validacionInsercion(long idNf);

}
