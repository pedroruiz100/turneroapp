package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFunc;

public interface RangoFuncDAO extends CRUDGenericDAO<RangoFunc> {

    long actualizarObtenerRangoActual(long idRango);

}
