package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabArticulo;
import java.util.List;

public interface DescuentoTarjetaCabArticuloDAO extends
        CRUDGenericDAO<DescuentoTarjetaCabArticulo> {

    List<DescuentoTarjetaCabArticulo> listarLimitado(long limite, long inicio,
            String nombre);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo);

    public List<DescuentoTarjetaCabArticulo> listarPorId(String id);
}
