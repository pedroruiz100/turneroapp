package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaConvenioDet;
import java.util.List;

public interface DescuentoTarjetaConvenioDetDAO extends CRUDGenericDAO<DescuentoTarjetaConvenioDet> {

    List<DescuentoTarjetaConvenioDet> listarLimitado(long limite,
            long inicio, String nombre);

    boolean insertarObtenerEstado(DescuentoTarjetaConvenioDet fromDescuentoTarjetaConvenioDetDTO);
}
