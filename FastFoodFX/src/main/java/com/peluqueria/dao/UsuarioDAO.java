package com.peluqueria.dao;

import com.peluqueria.core.domain.Usuario;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface UsuarioDAO extends CRUDGenericDAO<Usuario> {

    Usuario buscarUsuario(String usuarioAcceso, String contrasenha)
            throws NoSuchAlgorithmException;

    boolean actualizarContrasenha(String contrasenha, long id, String usuMod, Timestamp fechaMod);

    Usuario busUsuCI(String CI);

    List<Usuario> listarFETCH(long limRow, long offSet);

    List<Usuario> listarFETCHFiltro(long limRow, long offSet, String nombre, String fCI, boolean activo,
            boolean inactivo);

    long rowCount();

    long rowCountFiltro(String nombre, String fCI, boolean activo, boolean inactivo);

    public Usuario restaurandoPass(long idUsuario, String pass, String uMod, Timestamp fMod);

    Usuario actualizarObtenerObjeto(Usuario usuario);

    public Usuario insertarObtenerObjeto(Usuario fromUsuarioFuncDTO);
}
