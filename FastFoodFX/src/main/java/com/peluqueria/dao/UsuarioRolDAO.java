package com.peluqueria.dao;

import com.peluqueria.core.domain.UsuarioRol;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface UsuarioRolDAO extends CRUDGenericDAO<UsuarioRol> {

    List<UsuarioRol> buscarUsuarioRol(String usuario);

    public void delete(Long idUsuario);

    public UsuarioRol insertarObtenerObjeto(UsuarioRol fromUsuarioRolSeguridadDTO);
}
