package com.peluqueria.dao;

import com.peluqueria.core.domain.RetiroDinero;

/**
 *
 * @author ADMIN
 */
public interface RetiroDineroDAO extends CRUDGenericDAO<RetiroDinero> {

    RetiroDinero insertarObtenerObj(RetiroDinero retiroDinero);
}
