package com.peluqueria.dao;

import com.peluqueria.core.domain.ArtPromoTemporadaObsequio;

public interface ArtPromoTemporadaObsequioDAO extends
        CRUDGenericDAO<ArtPromoTemporadaObsequio> {

    void insercionMasiva(ArtPromoTemporadaObsequio artDTO, long i);

    boolean insercionMasivaEstado(ArtPromoTemporadaObsequio artDTO);

}
