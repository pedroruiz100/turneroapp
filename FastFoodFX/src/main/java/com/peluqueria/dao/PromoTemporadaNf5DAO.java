package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf5;

public interface PromoTemporadaNf5DAO extends CRUDGenericDAO<PromoTemporadaNf5> {

    void insercionMasiva(PromoTemporadaNf5 promoTemporadaNf5, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf5 promoTemporadaNf5);
}
