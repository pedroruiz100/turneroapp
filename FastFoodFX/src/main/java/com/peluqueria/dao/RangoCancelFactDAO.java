package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoCancelFact;

public interface RangoCancelFactDAO extends CRUDGenericDAO<RangoCancelFact> {

    boolean insertarObtenerEstado(RangoCancelFact rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
