package com.peluqueria.dao;

import com.peluqueria.core.domain.Supervisor;

/**
 *
 * @author ExcelsisWalker
 */
public interface SupervisorDAO extends CRUDGenericDAO<Supervisor> {

    Supervisor buscarNumeroCodigoSup(int numSup, String codSup);

    Supervisor buscarCodSup(String codSup);
    
    Supervisor buscarSupervisor(String usuario, String clave);

    public int obteniendoMaxNroSupervisor();

    public Supervisor insertarObtenerObjeto(Supervisor fromSupervisorAsociadoDTO);

    public Supervisor actualizarObtenerObjeto(Supervisor fromSupervisorAsociadoDTO);

    public Supervisor buscarIdUsuario(Long idUsuario);
}
