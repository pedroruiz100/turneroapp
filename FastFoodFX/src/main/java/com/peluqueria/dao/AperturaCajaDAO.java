/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.AperturaCaja;

public interface AperturaCajaDAO extends CRUDGenericDAO<AperturaCaja> {

    AperturaCaja insertarObtenerObj(AperturaCaja aperturaCaja);

    String recuperarPorCajaFecha(long idCaja, String fecha);

    public AperturaCaja recuperarPorCaja(Long idCaja, int x);

    public boolean insertarRecuperarEstado(AperturaCaja ac);

    public boolean verificarAperturaDia();

    public long getCantidadAperturaDia();

}
