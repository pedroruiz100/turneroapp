/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.Gastos;
import java.time.LocalDate;
import java.util.List;

public interface GastosDAO extends CRUDGenericDAO<Gastos> {

    Gastos insertarObtenerObj(Gastos aperturaCaja);

//    String recuperarPorCajaFecha2(long idCaja, String fecha);
//    AperturaCaja recuperarPorCajaFecha(long id, String fecha);
    public boolean insertarEstado(Gastos pro);

    public boolean actualizarEstado(Gastos pro);

    public List<Gastos> listarPorFechas(LocalDate firstDay, LocalDate lastDay);

    public List<Gastos> filtroFecha(String fechaInicio, String fechaFin);

    public Gastos recuperarUltimo();

    public List<String> listarProveedorDistinct();

    public Iterable<Gastos> filtroFechaTipo(String fechaInicio, String fechaFin);

}
