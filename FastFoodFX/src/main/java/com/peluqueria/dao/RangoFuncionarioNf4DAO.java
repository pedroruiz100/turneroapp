package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf4;

public interface RangoFuncionarioNf4DAO extends CRUDGenericDAO<RangoFuncionarioNf4> {

    boolean insertarObtenerEstado(RangoFuncionarioNf4 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
