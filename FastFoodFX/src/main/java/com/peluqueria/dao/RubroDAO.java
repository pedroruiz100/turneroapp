package com.peluqueria.dao;

import com.peluqueria.core.domain.Rubro;

public interface RubroDAO extends CRUDGenericDAO<Rubro> {

}
