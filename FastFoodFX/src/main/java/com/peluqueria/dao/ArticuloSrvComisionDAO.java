package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloSrvComision;
import com.peluqueria.dao.CRUDGenericDAO;
import com.peluqueria.dao.CRUDGenericDAO;

public interface ArticuloSrvComisionDAO extends CRUDGenericDAO<ArticuloSrvComision> {

    ArticuloSrvComision insertarObtenerObj(
            ArticuloSrvComision fromArticuloSrvComisionAsociado);

}
