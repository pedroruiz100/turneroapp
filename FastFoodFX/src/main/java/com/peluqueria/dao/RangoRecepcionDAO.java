package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoRecepcion;

public interface RangoRecepcionDAO extends CRUDGenericDAO<RangoRecepcion> {

    boolean insertarObtenerEstado(RangoRecepcion rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

    public String recuperarActual();

}
