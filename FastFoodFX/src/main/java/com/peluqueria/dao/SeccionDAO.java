package com.peluqueria.dao;

import com.peluqueria.core.domain.Seccion;
import java.util.List;

public interface SeccionDAO extends CRUDGenericDAO<Seccion> {

    List<Seccion> listarNombreId();

    List<Seccion> listarFuncionarioNombreId();

    List<Seccion> listarPromoTemp(String inicio, String fin);

    public Seccion listarPorNombre(String toUpperCase);

    public List<Seccion> listarPorNombreLista(String toUpperCase);

    public Seccion listarPorNombreAll(String toUpperCase);

    public List<Seccion> listarTodosTRUE();
}
