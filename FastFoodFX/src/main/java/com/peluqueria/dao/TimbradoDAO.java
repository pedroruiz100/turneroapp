package com.peluqueria.dao;

import com.peluqueria.core.domain.Timbrado;

public interface TimbradoDAO extends CRUDGenericDAO<Timbrado> {

}
