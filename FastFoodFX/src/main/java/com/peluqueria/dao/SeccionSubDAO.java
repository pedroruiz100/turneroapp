package com.peluqueria.dao;

import com.peluqueria.core.domain.SeccionSub;

public interface SeccionSubDAO extends CRUDGenericDAO<SeccionSub> {

}
