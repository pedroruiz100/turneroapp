package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf2Sfamilia;

public interface ArticuloNf2SfamiliaDAO extends CRUDGenericDAO<ArticuloNf2Sfamilia> {

    public ArticuloNf2Sfamilia getByIdArticulo(long id);

    public ArticuloNf2Sfamilia listarArticulo(long idArt);

}
