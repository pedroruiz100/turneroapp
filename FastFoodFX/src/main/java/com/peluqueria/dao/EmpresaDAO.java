package com.peluqueria.dao;

import com.peluqueria.core.domain.Empresa;

public interface EmpresaDAO extends CRUDGenericDAO<Empresa> {

}
