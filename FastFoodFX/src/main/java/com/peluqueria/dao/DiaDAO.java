package com.peluqueria.dao;

import com.peluqueria.core.domain.Dias;

public interface DiaDAO extends CRUDGenericDAO<Dias> {

}
