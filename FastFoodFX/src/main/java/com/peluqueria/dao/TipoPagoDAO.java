package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoPago;

public interface TipoPagoDAO extends CRUDGenericDAO<TipoPago> {

}
