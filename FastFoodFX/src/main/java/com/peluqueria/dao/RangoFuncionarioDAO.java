package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionario;

public interface RangoFuncionarioDAO extends CRUDGenericDAO<RangoFuncionario> {

    boolean insertarObtenerEstado(RangoFuncionario rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
