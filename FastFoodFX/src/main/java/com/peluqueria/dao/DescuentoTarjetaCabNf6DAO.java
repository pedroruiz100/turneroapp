package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf6;

public interface DescuentoTarjetaCabNf6DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf6> {

    void insercionMasiva(DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6);
}
