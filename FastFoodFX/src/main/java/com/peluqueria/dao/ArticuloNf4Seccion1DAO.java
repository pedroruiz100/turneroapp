package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf4Seccion1;

public interface ArticuloNf4Seccion1DAO extends CRUDGenericDAO<ArticuloNf4Seccion1> {

    public ArticuloNf4Seccion1 getByIdArticulo(long id);

    public ArticuloNf4Seccion1 listarArticulo(long idArt);

}
