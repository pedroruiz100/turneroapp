package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf4;

public interface DescuentoFielNf4DAO extends CRUDGenericDAO<DescuentoFielNf4> {

    void insercionMasiva(DescuentoFielNf4 descuentoFielNf4, long i);

    boolean insercionMasivaEstado(DescuentoFielNf4 descuentoFielNf4);

    boolean insertarObtenerEstado(DescuentoFielNf4 descuentoFielNf4);
}
