package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf2;

public interface PromoTemporadaNf2DAO extends CRUDGenericDAO<PromoTemporadaNf2> {

    void insercionMasiva(PromoTemporadaNf2 promoTemporadaNf2, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf2 promoTemporadaNf2);
}
