package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf3;

public interface FuncionarioNf3DAO extends CRUDGenericDAO<FuncionarioNf3> {

    List<FuncionarioNf3> listarFetchNf3(long limRow, long offSet);

    List<FuncionarioNf3> listarFetchFilterNf3(long limRow, long offSet, String nombre);

    Long rowCountNf3();

    long rowCountFilterNf3(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf3 insertarObtenerObjeto(FuncionarioNf3 funcionarioNf3);

    public boolean actualizarObtenerEstado(FuncionarioNf3 funcionarioNf3);
    
    boolean validacionInsercion(long idNf);

}
