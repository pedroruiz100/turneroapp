package com.peluqueria.dao;

import com.peluqueria.core.domain.TarjetaConvenio;
import java.util.List;

public interface TarjetaConvenioDAO extends CRUDGenericDAO<TarjetaConvenio> {

    List<TarjetaConvenio> listarNombreId();

}
