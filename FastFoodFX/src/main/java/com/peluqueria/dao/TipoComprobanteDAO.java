package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoComprobante;

public interface TipoComprobanteDAO extends CRUDGenericDAO<TipoComprobante> {

}
