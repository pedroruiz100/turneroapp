package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabNotaCredito;
import java.util.List;

public interface FacturaClienteCabNotaCreditoDAO extends CRUDGenericDAO<FacturaClienteCabNotaCredito> {

    FacturaClienteCabNotaCredito insertarObtenerObjeto(FacturaClienteCabNotaCredito fac);

    public List<FacturaClienteCabNotaCredito> listarPorFactura(long parseLong);

    public Iterable<FacturaClienteCabNotaCredito> listarPorFechaHoy();

}
