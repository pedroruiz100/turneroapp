package com.peluqueria.dao;

import com.peluqueria.core.domain.RolFuncion;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface RolFuncionDAO extends CRUDGenericDAO<RolFuncion> {

    List<RolFuncion> buscarRolFuncion(String cadenaIdRolFuncion, boolean b);

    public boolean crear(List<RolFuncion> fromRolFuncionAsociadoDTO);

    public boolean crearDeUno(RolFuncion rf);

    public List<RolFuncion> buscarPorRol(long parseLong);

    public void eliminarByObj(RolFuncion rolF);

    public List<RolFuncion> buscarRolFuncion(long idR, String cIN);
}
