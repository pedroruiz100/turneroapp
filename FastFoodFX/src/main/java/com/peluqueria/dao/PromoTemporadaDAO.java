package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporada;
import java.sql.Timestamp;
import java.util.List;

public interface PromoTemporadaDAO extends CRUDGenericDAO<PromoTemporada> {

    long rowCount();

    long rowCountFilter(String nombre, String fechaInicio, String fechaFin);

    List<PromoTemporada> listarFETCH(long limite, long inicio);

    List<PromoTemporada> listarFETCHFilter(long limite, long inicio,
            String nombre, String fechaInicio, String fechaFin);

    PromoTemporada insertarObtenerObjeto(PromoTemporada promoTemporada);

    List<PromoTemporada> listarPorFechaActual(Timestamp tiempoActual);

    void actualizarPorFecha(PromoTemporada pro, int numeroActual);

    void baja();

    boolean actualizarObtenerEstado(PromoTemporada fromPromoTemporadaDTO);

    boolean bajasLocal(String u, Timestamp ts);
    
    boolean validacionInsercion(int nf, long idNf);
}
