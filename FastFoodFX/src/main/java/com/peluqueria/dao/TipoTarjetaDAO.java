package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoTarjeta;

public interface TipoTarjetaDAO extends CRUDGenericDAO<TipoTarjeta> {

}
