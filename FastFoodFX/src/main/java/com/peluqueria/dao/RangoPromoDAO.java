package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoPromo;

public interface RangoPromoDAO extends CRUDGenericDAO<RangoPromo> {

    long actualizarObtenerRangoActual(long idRango);

}
