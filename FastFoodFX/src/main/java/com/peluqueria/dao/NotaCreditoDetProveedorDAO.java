package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaCreditoDetProveedor;

public interface NotaCreditoDetProveedorDAO extends CRUDGenericDAO<NotaCreditoDetProveedor> {

}
