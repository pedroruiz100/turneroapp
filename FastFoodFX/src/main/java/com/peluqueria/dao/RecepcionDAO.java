package com.peluqueria.dao;

import com.peluqueria.core.domain.Recepcion;
import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

public interface RecepcionDAO extends CRUDGenericDAO<Recepcion> {

    Recepcion insertarObtenerObj(Recepcion recep);

    public Recepcion getByNroOrden(String text);

    public List<Recepcion> listarPorFecha(Date desde, Date hasta);

    public Map getByNroOrdenEntrada(String text, String text0);

    public List<Recepcion> getByNroOrdenEntradaFecha(String text, String text0, LocalDate value);

    public Recepcion getByIdFecha(Long idPedidoCab, LocalDate value);

    public Recepcion getByNroOrdenPedido(String text);

}
