package com.peluqueria.dao;

import com.peluqueria.core.domain.ProveedorRubro;

public interface ProveedorRubroDAO extends CRUDGenericDAO<ProveedorRubro> {

}
