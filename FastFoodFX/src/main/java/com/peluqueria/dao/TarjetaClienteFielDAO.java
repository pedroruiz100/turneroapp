package com.peluqueria.dao;

import com.peluqueria.core.domain.TarjetaClienteFiel;
import java.util.List;

public interface TarjetaClienteFielDAO extends CRUDGenericDAO<TarjetaClienteFiel> {

    TarjetaClienteFiel listarPorCodCliente(String codigo);

    List<TarjetaClienteFiel> filtrarPorNomApeCi(String nombre,
            String apellido, String cipas);

    void actualizarPorCliente(TarjetaClienteFiel tar);

    long obtenerIdTarjetaFiel(long id);

    void eliminarPorCliente(long id);

    boolean insetarObtenerObj(TarjetaClienteFiel tar);

}
