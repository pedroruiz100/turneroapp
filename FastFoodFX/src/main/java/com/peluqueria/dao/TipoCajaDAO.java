package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoCaja;

/**
 *
 * @author ADMIN
 */
public interface TipoCajaDAO extends CRUDGenericDAO<TipoCaja> {

}
