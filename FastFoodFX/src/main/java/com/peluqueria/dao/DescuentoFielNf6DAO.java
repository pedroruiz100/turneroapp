package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf6;

public interface DescuentoFielNf6DAO extends CRUDGenericDAO<DescuentoFielNf6> {

    void insercionMasiva(DescuentoFielNf6 descuentoFielNf6, long i);

    boolean insercionMasivaEstado(DescuentoFielNf6 descuentoFielNf6);

    boolean insertarObtenerEstado(DescuentoFielNf6 descuentoFielNf6);
}
