package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf7Secnom7;
import java.util.List;

public interface Nf7Secnom7DAO extends CRUDGenericDAO<Nf7Secnom7> {

    List<Nf7Secnom7> listarNombreId();

    List<Nf7Secnom7> listarFuncionarioNombreId();

    List<Nf7Secnom7> listarPromoTemp(String inicio, String fin);

    public List<Nf7Secnom7> listarPorNf(long id);

}
