package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf7;

public interface DescuentoFielNf7DAO extends CRUDGenericDAO<DescuentoFielNf7> {

    void insercionMasiva(DescuentoFielNf7 descuentoFielNf7, long i);

    boolean insercionMasivaEstado(DescuentoFielNf7 descuentoFielNf7);

    boolean insertarObtenerEstado(DescuentoFielNf7 descuentoFielNf7);
}
