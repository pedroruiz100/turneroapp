package com.peluqueria.dao;

import com.peluqueria.core.domain.MotivoCancelacionFactura;

/**
 *
 * @author ADMIN
 */
public interface MotivoCancelacionFacturaDAO extends CRUDGenericDAO<MotivoCancelacionFactura> {

}
