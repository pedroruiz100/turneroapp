package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabVale;

public interface FacturaClienteCabValeDAO extends CRUDGenericDAO<FacturaClienteCabVale> {

}
