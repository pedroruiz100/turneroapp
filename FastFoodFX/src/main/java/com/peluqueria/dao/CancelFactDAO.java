package com.peluqueria.dao;

import com.peluqueria.core.domain.CancelFact;

public interface CancelFactDAO extends CRUDGenericDAO<CancelFact> {

}
