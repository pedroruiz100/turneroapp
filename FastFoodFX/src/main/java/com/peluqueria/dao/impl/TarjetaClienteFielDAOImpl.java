package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TarjetaClienteFiel;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TarjetaClienteFielDAOImpl implements TarjetaClienteFielDAO {

    @Override
    public List<TarjetaClienteFiel> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c ORDER BY c.nombre DESC")
                    .setMaxResults(10).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TarjetaClienteFiel getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            TarjetaClienteFiel tar = (TarjetaClienteFiel) em.find(TarjetaClienteFiel.class, id);
            return tar;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TarjetaClienteFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TarjetaClienteFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.remove(id);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TarjetaClienteFiel listarPorCodCliente(String codigo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<TarjetaClienteFiel> filtrarPorNomApeCi(String nombre,
            String apellido, String cipas) {
        List<TarjetaClienteFiel> tarFiel = new ArrayList<>();
        EntityManager em = null;
        StringTokenizer st = new StringTokenizer(cipas, "-");
        String ci = st.nextElement().toString(); //1
        try {
            em = getEmf().createEntityManager();
            if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")
                    && !cipas.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "and upper(tar.cipas) like :ci ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setParameter("ci", ci.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")
                    && !cipas.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(tar.cipas) like :ci "
                                + "ORDER BY c.nombre")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("ci", ci.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!cipas.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(tar.cipas) like :ci "
                                + "and upper(c.apellido) like :apellido "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("ci", ci.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!apellido.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(c.apellido) like :apellido ORDER BY c.apellido")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")) {
                String sql = "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE upper(c.nombre) like :nombre ORDER BY c.nombre";
                tarFiel = em
                        .createQuery(sql)
                        .setMaxResults(10)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getResultList();
            } else if (!cipas.equalsIgnoreCase("null")) {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c WHERE tar.cipas =:ci")
                        .setParameter("ci", ci).getResultList();
            } else {
                tarFiel = em
                        .createQuery(
                                "FROM TarjetaClienteFiel tar JOIN FETCH tar.cliente c ORDER BY c.nombre")
                        .setMaxResults(10).getResultList();
            }
            return tarFiel;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarPorCliente(TarjetaClienteFiel tar) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            long idTarj = obtenerIdTarjetaFiel(tar.getCliente()
                    .getIdCliente());
            tar.setIdTarjetaClienteFiel(idTarj);
            em.merge(tar);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long obtenerIdTarjetaFiel(long idCli) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (long) em
                    .createQuery(
                            "SELECT tcf.idTarjetaClienteFiel FROM TarjetaClienteFiel tcf WHERE tcf.cliente.idCliente=:idCliente")
                    .setParameter("idCliente", idCli).setMaxResults(1)
                    .getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminarPorCliente(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            long idTarj = obtenerIdTarjetaFiel(id);
            TarjetaClienteFiel tarj = em.find(TarjetaClienteFiel.class, idTarj);
            em.remove(tarj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insetarObtenerObj(TarjetaClienteFiel tar) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(tar);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
