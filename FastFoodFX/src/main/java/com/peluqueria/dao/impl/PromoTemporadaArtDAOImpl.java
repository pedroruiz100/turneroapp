package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.PromoTemporadaArt;
import com.peluqueria.dao.PromoTemporadaArtDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class PromoTemporadaArtDAOImpl implements PromoTemporadaArtDAO {

    @Override
    public List<PromoTemporadaArt> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporadaArt> list = em
                    .createQuery(
                            "FROM PromoTemporadaArt pt WHERE pt.estadoPromo=true ORDER BY pt.descripcionTemporadaArt")
                    .getResultList();
            return list;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PromoTemporadaArt getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            PromoTemporadaArt p = em.find(PromoTemporadaArt.class, id);
            return p;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PromoTemporadaArt obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PromoTemporadaArt obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.promo_temporada_art pro WHERE estado_promo=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilter(String nombre, String fechaInicio,
            String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            long row = 0L;
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                row = 0L;
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND UPPER(descripcionTemporadaArt) "
                        + "like :nombre AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND UPPER(descripcionTemporadaArt) "
                        + "like :nombre AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND UPPER(descripcionTemporadaArt) "
                        + "like :nombre AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND UPPER(descripcionTemporadaArt) "
                        + "like :nombre";
                row = (long) em.createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getSingleResult();

            } else if (!fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true AND "
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getSingleResult();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true "
                        + "AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else {
                String cadena = "SELECT count(*) FROM PromoTemporadaArt pro WHERE estadoPromo=true";
                row = (long) em.createQuery(cadena).getSingleResult();
            }
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PromoTemporadaArt> listarFETCH(long limite, long inicio) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PromoTemporadaArt pro WHERE pro.estadoPromo=true ORDER BY pro.descripcionTemporadaArt")
                    .setFirstResult((int) inicio).setMaxResults((int) limite).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<PromoTemporadaArt> listarFETCHFilter(long limite, long inicio,
            String nombre, String fechaInicio, String fechaFin) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        if (fechaInicio.equalsIgnoreCase("null")) {
            fechaInicio = "";
        }
        if (fechaFin.equalsIgnoreCase("null")) {
            fechaFin = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporadaArt> pro = new ArrayList<>();
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                pro.add(null);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporadaArt) like :nombre AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporadaArt) like :nombre AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporadaArt) like :nombre "
                        + " AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporadaArt) like :nombre"
                        + " ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                pro = em.createQuery(
                        "FROM PromoTemporadaArt pro  WHERE pro.estadoPromo=true ORDER BY pro.descripcionTemporadaArt")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite).getResultList();

            }
            return pro;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PromoTemporadaArt insertarObtenerObjeto(PromoTemporadaArt promoTemporada) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(promoTemporada);
            em.getTransaction().commit();
            em.refresh(promoTemporada);
            return promoTemporada;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<PromoTemporadaArt> listarPorFechaActual(Timestamp tiempoActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PromoTemporadaArt pt WHERE pt.estadoPromo=true AND (to_date(to_char(pt.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <:fechaActual)")
                    .setParameter("fechaActual",
                            Utilidades.stringToUtilDate(tiempoActual.toString()))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarPorFecha(PromoTemporadaArt pro, int numeroActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(pro);
            em.getTransaction().commit();
            em.clear();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void baja() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporadaArt> promo = em.createQuery(
                    "FROM PromoTemporadaArt pt WHERE pt.estadoPromo = true")
                    .getResultList();
            em.getTransaction().begin();
            for (PromoTemporadaArt pt : promo) {
                pt.setEstadoPromo(false);
                em.merge(pt);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(PromoTemporadaArt obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE PromoTemporadaArt d SET d.estadoPromo=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoPromo=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
//    @Override
//    public boolean bajasLocal(String u, Timestamp ts) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            List<PromoTemporadaArt> promo = em.createQuery(
//                    "FROM PromoTemporadaArt pt WHERE pt.estadoPromo = true")
//                    .getResultList();
//            for (PromoTemporadaArt desc : promo) {
//                desc.setEstadoPromo(false);
//                desc.setFechaMod(ts);
//                desc.setUsuMod(u);
//                try {
//                    em.getTransaction().begin();
//                    em.merge(desc);
//                    em.getTransaction().commit();
//                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    return false;
//                }
//            }
//            return true;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }

}
