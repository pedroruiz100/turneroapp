package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoPromo;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoPromoDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoPromoDAOImpl implements RangoPromoDAO {

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoPromo rango = em.find(RangoPromo.class, idRango);
            long r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return 0;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public List<RangoPromo> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoPromo getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoPromo obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoPromo obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
