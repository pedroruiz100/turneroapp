package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoCliente;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoClienteDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoClienteDAOImpl implements RangoClienteDAO {

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoCliente rango = em.find(RangoCliente.class, idRango);
            r = rango.getRangoActual();
            try {
                rango.setRangoActual(r + 1);
                em.getTransaction().begin();
                em.merge(rango);
                em.getTransaction().commit();
                return r;
            } catch (Exception e) {
                System.out.println("--->> " + e.getLocalizedMessage());
                return r;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<RangoCliente> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoCliente getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoCliente obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoCliente obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
