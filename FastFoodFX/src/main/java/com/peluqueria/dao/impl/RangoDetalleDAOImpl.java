package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoDetalle;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoDetalleDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoDetalleDAOImpl implements RangoDetalleDAO {

    @Override
    public List<RangoDetalle> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoDetalle getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoDetalle obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoDetalle obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarObtenerRango(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoDetalle rango = em.find(RangoDetalle.class, idRango);
            r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.getTransaction().begin();
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return r;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
