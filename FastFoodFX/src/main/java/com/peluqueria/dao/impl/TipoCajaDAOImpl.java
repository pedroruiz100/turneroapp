package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TipoCaja;
import com.peluqueria.dao.TipoCajaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class TipoCajaDAOImpl implements TipoCajaDAO {

    @Override
    public List<TipoCaja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM TipoCaja tp").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TipoCaja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(TipoCaja.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TipoCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TipoCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
