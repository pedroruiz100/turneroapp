package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaCompraDet;
import com.peluqueria.dao.FacturaCompraDetDAO;
import static com.javafx.util.EMF.getEmf;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaCompraDetDAOImpl implements FacturaCompraDetDAO {

    @Override
    public List<FacturaCompraDet> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaCompraDet fcd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraDet getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FacturaCompraDet.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(FacturaCompraDet facDet, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facDet);
            em.getTransaction().commit();
//            if (i % 30 == 0) {
//                em.flush();
//                em.clear();
//            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaCompraDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaCompraDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraDet> listarPorFechaRecepcionActual(LocalDate desde, LocalDate hasta, String idProv) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        try {
            if (idProv.equals("0")) {
                return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep JOIN FETCH recep.proveedor pro WHERE DATE(fcd.facturaCompraCab.fechaDoc) >=:fecDesde AND "
                        + "DATE(fcd.facturaCompraCab.fechaDoc) <=:fecHasta ORDER BY fcd.sec2 ASC")
                        .setParameter("fecDesde", Date.from(desde.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                        .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                        .getResultList();
            } else {
                return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep JOIN FETCH recep.proveedor pro WHERE DATE(fcd.facturaCompraCab.fechaDoc) >=:fecDesde AND "
                        + "DATE(fcd.facturaCompraCab.fechaDoc) <=:fecHasta and fcd.facturaCompraCab.proveedor.idProveedor=" + idProv + " ORDER BY fcd.sec2 ASC")
                        .setParameter("fecDesde", Date.from(desde.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                        .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                        .getResultList();
            }
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
//            String sql = "SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
//                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
//                    + "  recep.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
//                    + "   AND DATE(recep.fecha_doc)>='" + Date.from(desde.atStartOfDay(ZoneId.systemDefault()).toInstant())
//                    + "' AND DATE(recep.fecha_doc)<='" + Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()) + "' AND sucursal='CASA CENTRAL'";
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_compra_cab recep ON "
                    + "  recep.id_factura_compra_cab=fcd.id_factura_compra_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desd
                    + "' AND DATE(recep.fecha_doc)<='" + hast + "' AND sucursal='CASA CENTRAL'";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_compra_cab recep ON "
                    + "  recep.id_factura_compra_cab=fcd.id_factura_compra_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desd
                    + "' AND DATE(recep.fecha_doc)<='" + hast + "' AND sucursal='SAN LORENZO'";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_compra_cab recep ON "
                    + "  recep.id_factura_compra_cab=fcd.id_factura_compra_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desd
                    + "' AND DATE(recep.fecha_doc)<='" + hast + "' AND sucursal='CACIQUE'";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraDet> listarPorRecepcion(long idRecepcio) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaCompraDet fcd WHERE fcd.facturaCompraCab.idFacturaCompraCab=:idRecep")
                    .setParameter("idRecep", idRecepcio).getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public String recuperarUltimaFechaVenta(String idProv) {
        EntityManager em = null;
//        String ultimaVenta = "";
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT MAX(fcc.fecha_doc) FROM factura_cliente.factura_compra_det fcd LEFT JOIN factura_cliente.factura_compra_cab fcc ON fcd.id_factura_compra_cab=fcc.id_factura_compra_cab "
                    + "LEFT JOIN cuenta.proveedor pro ON fcc.id_proveedor=pro.id_proveedor WHERE fcc.id_proveedor=" + idProv;
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return x;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraDet> listarPorUltimaFecha(String fecData, String idProv) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        LocalDate local = LocalDate.parse(fecData);
//        DateTimeFormatter FORMATTER = DateTimeFormat.forPattern("yyyy-MM-dd");
//        DateTime dateTime = FORMATTER.parseDateTime(fecData);
//        LocalDate localDate = dateTime.toLocalDate();
        try {
            return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep JOIN FETCH recep.proveedor pro WHERE DATE(fcd.facturaCompraCab.fechaDoc) =:fecDesde AND "
                    + "fcd.facturaCompraCab.proveedor.idProveedor=" + idProv + " ORDER BY fcd.sec2 ASC")
                    .setParameter("fecDesde", /*fecData*/ Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    //                    .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraDet getByLastCodigo(Long codArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraDet) em.createQuery("FROM FacturaCompraDet fcd WHERE fcd.codArticulo=:codigo ORDER BY fcd.idFacturaCompraDet DESC")
                    .setParameter("codigo", codArticulo)
                    //                    .getFirstResult()
                    .setMaxResults(1).setFirstResult(0)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraDet> buscarPorSucursalOC(String oc, String sucursal) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        boolean central = false;
        boolean cacique = false;
        boolean sanlo = false;
        String suc = sucursal.substring(0, (sucursal.length() - 2));
        StringTokenizer sts = new StringTokenizer(suc, ".");
        String suc1 = "";
        try {
            suc1 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        String suc2 = "";
        try {
            suc2 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        String suc3 = "";
        try {
            suc3 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        suc1 = suc1.trim();
        suc2 = suc2.trim();
        suc3 = suc3.trim();

        if (suc1.equalsIgnoreCase("CENTRAL")) {
            central = true;
        } else if (suc1.equalsIgnoreCase("SAN LORENZO")) {
            sanlo = true;
        } else if (suc1.equalsIgnoreCase("CACIQUE")) {
            cacique = true;
        }

        if (suc3.equalsIgnoreCase("CENTRAL")) {
            if (!central) {
                central = true;
            }
        } else if (suc3.equalsIgnoreCase("SAN LORENZO")) {
            if (!sanlo) {
                sanlo = true;
            }
        } else if (suc3.equalsIgnoreCase("CACIQUE")) {
            if (!cacique) {
                cacique = true;
            }
        }

        if (suc2.equalsIgnoreCase("CENTRAL")) {
            if (!central) {
                central = true;
            }
        } else if (suc2.equalsIgnoreCase("SAN LORENZO")) {
            if (!sanlo) {
                sanlo = true;
            }
        } else if (suc2.equalsIgnoreCase("CACIQUE")) {
            if (!cacique) {
                cacique = true;
            }
        }
        try {
            return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep WHERE recep.oc=:ordenCompra AND "
                    + "(recep.cc=:ecc OR recep.sc=:esc) ORDER BY recep.nroDoc")
                    //            return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep WHERE recep.oc=:ordenCompra AND "
                    //                    + "(recep.cc=:ecc OR recep.sc=:esc OR recep.sl=:esl)")
                    .setParameter("ordenCompra", oc)
                    .setParameter("ecc", central)
                    .setParameter("esc", cacique)
                    //                    .setParameter("esl", sanlo)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraDet> listarPorOC(String oc) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        try {
            return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep "
                    + "WHERE recep.oc='" + oc + "' ORDER BY fcd.sec2 ASC")
                    //                    .setParameter("ocompra", "'" + oc + "'")
                    //                    .setParameter("fecDesde", /*fecData*/ Date.from(local.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    //                    .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long listarPorOCAndCodigoCC(String oc, Long codArticulo) {
        EntityManager em = null;
//        String ultimaVenta = "";
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT sum(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN factura_cliente.factura_compra_cab fcc "
                    + "ON fcd.id_factura_compra_cab=fcc.id_factura_compra_cab WHERE fcc.oc='" + oc + "' AND fcd.cod_articulo=" + codArticulo + " AND fcc.cc=TRUE";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0L;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long listarPorOCAndCodigoSC(String oc, Long codArticulo) {
        EntityManager em = null;
//        String ultimaVenta = "";
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT sum(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN factura_cliente.factura_compra_cab fcc "
                    + "ON fcd.id_factura_compra_cab=fcc.id_factura_compra_cab WHERE fcc.oc='" + oc + "' AND fcd.cod_articulo=" + codArticulo + " AND fcc.sc=TRUE";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0L;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long listarPorOCAndCodigoSL(String oc, Long codArticulo) {
        EntityManager em = null;
//        String ultimaVenta = "";
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT sum(fcd.cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN factura_cliente.factura_compra_cab fcc "
                    + "ON fcd.id_factura_compra_cab=fcc.id_factura_compra_cab WHERE fcc.oc='" + oc + "' AND fcd.cod_articulo=" + codArticulo + " AND fcc.sl=TRUE";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0L;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
