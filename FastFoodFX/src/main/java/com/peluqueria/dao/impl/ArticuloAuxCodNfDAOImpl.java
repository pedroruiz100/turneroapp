package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloAuxCodNf;
import com.peluqueria.dao.ArticuloAuxCodNfDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloAuxCodNfDAOImpl implements ArticuloAuxCodNfDAO {

    @Override
    public List<ArticuloAuxCodNf> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloAuxCodNf a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloAuxCodNf getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloAuxCodNf) em
                    .createQuery("FROM ArticuloAuxCodNf a WHERE a.idArticuloAuxCodNf=:idArticuloAuxCodNf")
                    .setParameter("idArticuloAuxCodNf", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloAuxCodNf obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloAuxCodNf obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloAuxCodNf articuloAuxCodNf = em.find(ArticuloAuxCodNf.class, id);
            em.remove(articuloAuxCodNf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
