package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabTipoPago;
import com.peluqueria.dao.FacturaClienteCabTipoPagoDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabTipoPagoDAOImpl implements FacturaClienteCabTipoPagoDAO {

    @Override
    public List<FacturaClienteCabTipoPago> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabTipoPago fcp").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabTipoPago getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FacturaClienteCabTipoPago.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabTipoPago obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabTipoPago obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public FacturaClienteCabTipoPago insertarObtenerObj(
            FacturaClienteCabTipoPago facturaClienteCabTipoPago) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabTipoPago);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabTipoPago);
            return facturaClienteCabTipoPago;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
