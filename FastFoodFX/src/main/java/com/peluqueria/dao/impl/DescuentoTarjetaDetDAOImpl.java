package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoTarjetaDet;
import com.peluqueria.dao.DescuentoTarjetaDetDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaDetDAOImpl implements DescuentoTarjetaDetDAO {

    @Override
    public List<DescuentoTarjetaDet> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaDet dtcd ORDER BY dtcd.idDescuentoTarjetaDet DESC")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaDet getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DescuentoTarjetaDet det = (DescuentoTarjetaDet) em.createQuery(
                    "FROM DescuentoTarjetaDet dtcd WHERE dtcd.idDescuentoTarjetaDet=:idDet ORDER BY dtcd.idDescuentoTarjetaDet DESC")
                    .setParameter("idDet", id)
                    .getSingleResult();
            return det;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<DescuentoTarjetaDet> listarLimitado(long limite, long inicio,
            String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaDet dtcd WHERE upper(dtcd.descuentoTarjetaCab.descriTarjeta) like :descriTar "
                            + "ORDER BY dtcd.idDescuentoTarjetaDet")
                    .setParameter("descriTar", nombre.toUpperCase() + "%")
                    .setFirstResult((int) inicio).setMaxResults((int) limite)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaDet desc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(desc);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

}
