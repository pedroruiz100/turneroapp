package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoDevolucion;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoDevolucionDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoDevolucionDAOImpl implements RangoDevolucionDAO {

    @Override
    public List<RangoDevolucion> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoDevolucion getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoDevolucion obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoDevolucion obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarRecuperarRangoActual(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoDevolucion rango = em.find(RangoDevolucion.class, idRango);
            r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.getTransaction().begin();
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return r;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long recuperarRangoActual(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
            RangoDevolucion rango = em.find(RangoDevolucion.class, 1L);
            long r = rango.getRangoActual();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return -1l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
