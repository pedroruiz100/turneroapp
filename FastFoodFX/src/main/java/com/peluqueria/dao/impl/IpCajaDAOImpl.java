package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.IpCaja;
import com.peluqueria.dao.IpCajaDAO;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class IpCajaDAOImpl implements IpCajaDAO {

    @Override
    public List<IpCaja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM IpCaja ib").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public IpCaja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(IpCaja.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(IpCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(IpCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
