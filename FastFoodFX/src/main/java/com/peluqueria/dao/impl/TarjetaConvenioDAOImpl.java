package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TarjetaConvenio;
import com.peluqueria.dao.TarjetaConvenioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TarjetaConvenioDAOImpl implements TarjetaConvenioDAO {

    @Override
    public List<TarjetaConvenio> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<TarjetaConvenio> s = em.createQuery("FROM TarjetaConvenio tc ORDER BY tc.descripcion").getResultList();
            return s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TarjetaConvenio getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            TarjetaConvenio t = em.find(TarjetaConvenio.class, id);
            return t;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TarjetaConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TarjetaConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<TarjetaConvenio> listarNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM TarjetaConvenio tar WHERE NOT EXISTS(FROM DescuentoTarjetaConvenioCab descCab WHERE descCab.estadoDesc=true AND descCab.tarjetaConvenio.idTarjetaConvenio=tar.idTarjetaConvenio) ORDER BY tar.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
