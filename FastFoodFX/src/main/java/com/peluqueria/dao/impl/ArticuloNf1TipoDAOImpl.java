package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf1TipoDAOImpl implements ArticuloNf1TipoDAO {

    @Override
    public List<ArticuloNf1Tipo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf1Tipo a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf1Tipo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf1Tipo) em
                    .createQuery("FROM ArticuloNf1Tipo a WHERE a.idNf1TipoArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf1Tipo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf1Tipo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf1Tipo articuloNf1Tipo = em.find(ArticuloNf1Tipo.class, id);
            em.remove(articuloNf1Tipo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf1Tipo getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf1Tipo) em
                    .createQuery("FROM ArticuloNf1Tipo a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf1Tipo listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf1Tipo) em.createQuery("FROM ArticuloNf1Tipo a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", idArt).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
