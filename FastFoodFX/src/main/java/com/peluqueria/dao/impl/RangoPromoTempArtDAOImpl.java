package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoPromoTempArt;
import com.peluqueria.dao.RangoPromoTempArtDAO;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoPromoTempArtDAOImpl implements RangoPromoTempArtDAO {

    @Override
    public boolean insertarObtenerEstado(RangoPromoTempArt rango) {
        return false;
    }

    @Override
    public boolean actualizarObtenerEstado(long idRango) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoPromoTempArt rango = em.find(RangoPromoTempArt.class, idRango);
            long r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return -1l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<RangoPromoTempArt> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoPromoTempArt getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoPromoTempArt obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoPromoTempArt obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
