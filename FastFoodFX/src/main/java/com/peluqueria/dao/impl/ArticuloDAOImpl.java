package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.dao.ArticuloDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ArticuloDAOImpl implements ArticuloDAO {

    @Override
    public List<Articulo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Articulo> listarSoloDiez() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a WHERE a.servicio=TRUE ORDER BY a.descripcion").setMaxResults(30).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Articulo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Articulo) em
                    .createQuery("FROM Articulo a WHERE a.idArticulo=:idArt")
                    .setParameter("idArt", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Articulo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            System.out.println("-->> " + ex.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Articulo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            System.out.println("-->> " + ex.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Articulo ar = em.find(Articulo.class, id);
            em.remove(ar);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Articulo buscarCod(String codigo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Articulo) em
                    .createQuery("FROM Articulo art JOIN FETCH art.iva iva "
                            + "JOIN FETCH art.seccion sec WHERE art.codArticulo = :cod")
                    .setParameter("cod", codigo).getSingleResult();
        } catch (Exception e) {
            System.out.println("-->> " + e.getMessage());
            System.out.println("-->> " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Articulo buscarCodPorFecha(Long codigo, String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada art y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return (Articulo) em
                    .createQuery(
                            "FROM Articulo a WHERE NOT EXISTS(FROM ArtPromoTemporada artProTemp WHERE"
                            + " artProTemp.promoTemporadaArt.estadoPromo=true AND artProTemp.articulo.idArticulo=a.idArticulo AND"
                            + " ((to_date(to_char(artProTemp.promoTemporadaArt.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(artProTemp.promoTemporadaArt.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND a.codArticulo =:codigo")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .setParameter("codigo", codigo)
                    .getSingleResult();
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Articulo insertarRecuperarDato(Articulo arti) {
        EntityManager em = null;
//        Articulo ar = new Articulo();
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
//            ar = em.find(Articulo.class, arti.getIdArticulo());
            em.persist(arti);
//            em.refresh(arti);
            em.getTransaction().commit();
        } catch (Exception ex) {
            arti = null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return arti;
    }

    @Override
    public List<Articulo> listarPorDescripcion(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a WHERE UPPER(a.descripcion) LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
                    .setParameter("descri", "%" + text.toUpperCase() + "%")
                    .setMaxResults(30)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Articulo> listarPorCodigo(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a WHERE a.codArticulo=:descri AND a.servicio=TRUE ORDER BY a.descripcion")
                    .setParameter("descri", text)
                    .setMaxResults(30)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Articulo> listarPorCodigoAprox(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a WHERE a.codArticulo LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
                    .setParameter("descri", text + "%")
                    .setMaxResults(30)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Articulo> listarPorSeccion(String seccion) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Articulo a JOIN FETCH a.seccion sec WHERE UPPER(sec.descripcion) LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
                    .setParameter("descri", seccion.toUpperCase() + "%")
                    .setMaxResults(30)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Articulo> listarPorCombinaciones(String cod, String descri, String seccion) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();

            if (!cod.equals("")) {
                return em.createQuery("FROM Articulo a JOIN FETCH a.seccion sec WHERE a.codArticulo=:cod AND UPPER(a.descripcion) LIKE :descripc AND UPPER(sec.descripcion) LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
                        .setParameter("cod", cod)
                        .setParameter("descripc", "%" + descri.toUpperCase() + "%")
                        .setParameter("descri", seccion.toUpperCase() + "%")
                        .setMaxResults(30)
                        .getResultList();
            } else {
                return em.createQuery("FROM Articulo a JOIN FETCH a.seccion sec WHERE UPPER(a.descripcion) LIKE :descripc AND UPPER(sec.descripcion) LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
                        .setParameter("descripc", "%" + descri.toUpperCase() + "%")
                        .setParameter("descri", seccion.toUpperCase())
                        .setMaxResults(30)
                        .getResultList();
            }

        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
