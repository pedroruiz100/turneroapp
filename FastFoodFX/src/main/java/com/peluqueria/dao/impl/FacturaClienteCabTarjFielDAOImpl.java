package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabTarjFiel;
import com.peluqueria.dao.FacturaClienteCabTarjFielDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabTarjFielDAOImpl implements
        FacturaClienteCabTarjFielDAO {

    @Override
    public List<FacturaClienteCabTarjFiel> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabTarjFiel fac")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabTarjFiel getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabTarjFiel) em
                    .createQuery(
                            "FROM FacturaClienteCabTarjFiel fac WHERE fac.idFacturaClienteCabTarjFiel=:idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabTarjFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabTarjFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public FacturaClienteCabTarjFiel insertarObtenerObjeto(
            FacturaClienteCabTarjFiel facturaClienteCabTarjFiel) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabTarjFiel);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabTarjFiel);
            return facturaClienteCabTarjFiel;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
