package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RetiroDinero;
import com.peluqueria.dao.RetiroDineroDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class RetiroDineroDAOImpl implements RetiroDineroDAO {

    @Override
    public List<RetiroDinero> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<RetiroDinero> ls = em.createQuery("FROM RetiroDinero rd").getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RetiroDinero getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(RetiroDinero.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(RetiroDinero obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(RetiroDinero obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RetiroDinero rd = em.find(RetiroDinero.class, id);
            em.remove(rd);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RetiroDinero insertarObtenerObj(RetiroDinero retiroDinero) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(retiroDinero);
            em.getTransaction().commit();
            em.refresh(retiroDinero);
            return retiroDinero;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
