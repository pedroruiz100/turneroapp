package com.peluqueria.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.FuncionarioNf1;
import com.peluqueria.dao.FuncionarioNf1DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import javax.persistence.PersistenceException;

@Repository
public class FuncionarioNf1DAOImpl implements FuncionarioNf1DAO {

    @Override
    public List<FuncionarioNf1> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<FuncionarioNf1> funcionarioNf1s = em
                    .createQuery(
                            "FROM FuncionarioNf1 funcionarioNf1 WHERE funcionarioNf1.estadoDesc=true ORDER BY funcionarioNf1.descriSeccion")
                    .getResultList();
            return funcionarioNf1s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf1 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FuncionarioNf1.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FuncionarioNf1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FuncionarioNf1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FuncionarioNf1 sf = em.find(FuncionarioNf1.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf1> listarFetchNf1(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FuncionarioNf1 funcionarioNf1 WHERE funcionarioNf1.estadoDesc=true ORDER BY funcionarioNf1.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf1> listarFetchFilterNf1(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FuncionarioNf1 funcionarioNf1 WHERE funcionarioNf1.estadoDesc=true AND"
                            + " (LOWER(funcionarioNf1.descriSeccion) LIKE :descri) order by funcionarioNf1.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf1() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM descuento.funcionario_nf1 funcionarioNf1 WHERE funcionarioNf1.estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilterNf1(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM FuncionarioNf1 funcionarioNf1 WHERE funcionarioNf1.estadoDesc=true AND"
                    + " (LOWER(funcionarioNf1.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf1 funcionarioNf1 "
                    + "SET funcionarioNf1.estadoDesc=false "
                    + "WHERE funcionarioNf1.estadoDesc=true");
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(FuncionarioNf1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf1 funcionarioNf1 "
                    + "SET funcionarioNf1.estadoDesc=false, funcionarioNf1.usuMod=:uMod, funcionarioNf1.fechaMod=:fMod "
                    + "WHERE funcionarioNf1.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf1 insertarObtenerObjeto(FuncionarioNf1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean validacionInsercion(long idNf) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            if (em.createQuery(
                    "FROM FuncionarioNf1 as funcionarioNf1 "
                    + "WHERE funcionarioNf1.estadoDesc = true "
                    + "AND funcionarioNf1.nf1Tipo.idNf1Tipo = :idNf "
                    + "ORDER BY funcionarioNf1.descriSeccion")
                    .setParameter("idNf", idNf)
                    .setMaxResults(1)
                    .getSingleResult() != null) {
                existe = true;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
