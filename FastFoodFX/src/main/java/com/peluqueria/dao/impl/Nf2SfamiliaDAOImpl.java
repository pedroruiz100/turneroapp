package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.dao.Nf2SfamiliaDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf2SfamiliaDAOImpl implements Nf2SfamiliaDAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf2Sfamilia> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf2Sfamilia nf2Sfamilia order by nf2Sfamilia.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf2Sfamilia getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf2Sfamilia.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf2Sfamilia nf2Sfamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf2Sfamilia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf2Sfamilia nf2Sfamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf2Sfamilia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf2Sfamilia> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia)"
                            + " AND nf2Sfamilia.idNf2Sfamilia != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf2Sfamilia> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM FuncionarioNf2 funcionarioNf2 "
                            + "WHERE funcionarioNf2.estadoDesc=true AND funcionarioNf2.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia) "
                            + "AND nf2Sfamilia.idNf2Sfamilia != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf2Sfamilia> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM PromoTemporadaNf2 promoTemporadaNf2 WHERE"
                            + " promoTemporadaNf2.promoTemporada.estadoPromo=true AND promoTemporadaNf2.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia AND"
                            + " ((to_date(to_char(promoTemporadaNf2.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf2.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf2Sfamilia.idNf2Sfamilia != 0 ORDER BY nf2Sfamilia.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf2Sfamilia> listarPorNf(long idNf1) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf2Sfamilia nf2Sfamilia WHERE nf2Sfamilia.nf1Tipo.idNf1Tipo=" + idNf1
                    + " order by nf2Sfamilia.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
