package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.dao.Nf6Secnom6DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf6Secnom6DAOImpl implements Nf6Secnom6DAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf6Secnom6> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf6Secnom6 nf6Secnom6 order by nf6Secnom6.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf6Secnom6 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf6Secnom6.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf6Secnom6 nf6Secnom6) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf6Secnom6);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf6Secnom6 nf6Secnom6) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf6Secnom6);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf6Secnom6> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf6Secnom6 nf6Secnom6 WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf6Secnom6.idNf6Secnom6=nf6Secnom6.idNf6Secnom6)"
                            + " AND nf6Secnom6.idNf6Secnom6 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf6Secnom6> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf6Secnom6 nf6Secnom6 WHERE NOT EXISTS(FROM FuncionarioNf6 funcionarioNf6 "
                            + "WHERE funcionarioNf6.estadoDesc=true AND funcionarioNf6.nf6Secnom6.idNf6Secnom6=nf6Secnom6.idNf6Secnom6) "
                            + "AND nf6Secnom6.idNf6Secnom6 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf6Secnom6> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf6Secnom6 nf6Secnom6 WHERE NOT EXISTS(FROM PromoTemporadaNf6 promoTemporadaNf6 WHERE"
                            + " promoTemporadaNf6.promoTemporada.estadoPromo=true AND promoTemporadaNf6.nf6Secnom6.idNf6Secnom6=nf6Secnom6.idNf6Secnom6 AND"
                            + " ((to_date(to_char(promoTemporadaNf6.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf6.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf6Secnom6.idNf6Secnom6 != 0 ORDER BY nf6Secnom6.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf6Secnom6> listarPorNf(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf6Secnom6 nf6Secnom6 WHERE "
                    + "nf6Secnom6.nf5Seccion2.idNf5Seccion2=" + id + " order by nf6Secnom6.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
