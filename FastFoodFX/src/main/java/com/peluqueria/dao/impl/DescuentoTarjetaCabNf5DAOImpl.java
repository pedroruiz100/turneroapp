package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf5;
import com.peluqueria.dao.DescuentoTarjetaCabNf5DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class DescuentoTarjetaCabNf5DAOImpl implements DescuentoTarjetaCabNf5DAO {

    @Override
    public List<DescuentoTarjetaCabNf5> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5 WHERE descuentoTarjetaCabNf5.descuentoTarjetaCab.estadoDesc=true "
                    + "ORDER BY idDescuentoTarjetaCabNf5").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCabNf5 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoTarjetaCabNf5.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCabNf5 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCabNf5 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(DescuentoTarjetaCabNf5 sec, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(sec);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf5);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf5);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("ERROR -->> insertarObtenerEstado(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5) " + e.fillInStackTrace());
            return false;
        }
    }

}
