package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ClientePendiente;
import com.peluqueria.dao.ClientePendienteDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ClientePendienteDAOImpl implements ClientePendienteDAO {

    @Override
    public List<ClientePendiente> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM ClientePendiente cli JOIN FETCH cli.cliente cliente ORDER BY cli.idClientePendiente")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ClientePendiente getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            ClientePendiente cp = (ClientePendiente) em.createQuery(
                    "FROM ClientePendiente cli WHERE cli.idClientePendiente=:idCli")
                    .setParameter("idCli", id)
                    .getSingleResult();
            System.out.println("-->> " + cp.getFechaMod() + " -->> " + cp.getFechaAlta());
            return cp;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ClientePendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ClientePendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ClientePendiente cli = em.find(ClientePendiente.class, id);
            em.remove(cli);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ClientePendiente insertarObtenerObj(
            ClientePendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ClientePendiente> listarPendiente() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM ClientePendiente cli WHERE cli.procesado=false ORDER BY cli.idClientePendiente")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean actualizarEstado(long idCliPen, Timestamp timestamp, String nombreCaj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE ClientePendiente cp SET cp.procesado=true, cp.usuMod=:uMod, cp.fechaMod=:fMod WHERE cp.idClientePendiente=:idCliPen")
                    .setParameter("uMod", nombreCaj)
                    .setParameter("fMod", timestamp)
                    .setParameter("idCliPen", idCliPen)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("Exception", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
