package com.peluqueria.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.FuncionarioNf2;
import com.peluqueria.dao.FuncionarioNf2DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import javax.persistence.PersistenceException;

@Repository
public class FuncionarioNf2DAOImpl implements FuncionarioNf2DAO {

    @Override
    public List<FuncionarioNf2> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<FuncionarioNf2> funcionarioNf2s = em
                    .createQuery(
                            "FROM FuncionarioNf2 funcionarioNf2 WHERE funcionarioNf2.estadoDesc=true ORDER BY funcionarioNf2.descriSeccion")
                    .getResultList();
            return funcionarioNf2s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf2 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FuncionarioNf2.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FuncionarioNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FuncionarioNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FuncionarioNf2 sf = em.find(FuncionarioNf2.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf2> listarFetchNf2(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FuncionarioNf2 funcionarioNf2 WHERE funcionarioNf2.estadoDesc=true ORDER BY funcionarioNf2.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf2> listarFetchFilterNf2(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FuncionarioNf2 funcionarioNf2 WHERE funcionarioNf2.estadoDesc=true AND"
                            + " (LOWER(funcionarioNf2.descriSeccion) LIKE :descri) order by funcionarioNf2.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf2() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.seccion_func sf WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilterNf2(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM FuncionarioNf2 funcionarioNf2 WHERE funcionarioNf2.estadoDesc=true AND"
                    + " (LOWER(funcionarioNf2.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf2 funcionarioNf2 "
                    + "SET funcionarioNf2.estadoDesc=false "
                    + "WHERE funcionarioNf2.estadoDesc=true");
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(FuncionarioNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf2 funcionarioNf2 "
                    + "SET funcionarioNf2.estadoDesc=false, funcionarioNf2.usuMod=:uMod, funcionarioNf2.fechaMod=:fMod "
                    + "WHERE funcionarioNf2.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf2 insertarObtenerObjeto(FuncionarioNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean validacionInsercion(long idNf) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            if (em.createQuery(
                    "FROM FuncionarioNf2 as funcionarioNf2 "
                    + "WHERE funcionarioNf2.estadoDesc = true "
                    + "AND funcionarioNf2.nf2Sfamilia.idNf2Sfamilia = :idNf "
                    + "ORDER BY funcionarioNf2.descriSeccion")
                    .setParameter("idNf", idNf)
                    .setMaxResults(1)
                    .getSingleResult() != null) {
                existe = true;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
