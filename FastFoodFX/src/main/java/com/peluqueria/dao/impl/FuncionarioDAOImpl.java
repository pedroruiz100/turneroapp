package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.dao.FuncionarioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FuncionarioDAOImpl implements FuncionarioDAO {

    @Override
    public List<Funcionario> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Funcionario f ORDER BY f.nombre")
                    .setMaxResults(10).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Funcionario getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Funcionario) em
                    .createQuery("FROM Funcionario f WHERE f.idFuncionario =:idFun")
                    .setParameter("idFun", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Funcionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Funcionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id
    ) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<Funcionario> filtrarNomApeCi(String nombre, String apellido, String ci) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Funcionario> lista = new ArrayList<>();
            if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")
                    && !ci.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "and upper(c.ci) like :numci ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setParameter("numci", ci.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();

            } else if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")
                    && !ci.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.ci) like :numci "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("numci", ci.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!apellido.equalsIgnoreCase("null")
                    && !ci.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.apellido) like :apellido "
                                + "and upper(c.ci) like :numci "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setParameter("numci", ci.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!apellido.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.apellido) like :apellido ORDER BY c.apellido")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")) {
                lista = em
                        .createQuery(
                                "FROM Funcionario c WHERE upper(c.nombre) like :nombre ORDER BY c.nombre")
                        .setMaxResults(10)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getResultList();
            } else if (!ci.equalsIgnoreCase("null")) {
                lista = em.createQuery("FROM Funcionario c WHERE c.ci =:numci")
                        .setParameter("numci", ci).getResultList();

            } else {
                lista = em
                        .createQuery(
                                "FROM Funcionario c ORDER BY c.nombre, c.apellido")
                        .setMaxResults(10).getResultList();
            }
            return lista;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Funcionario listarFuncionarioPorCi(String ci) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Funcionario fun = (Funcionario) em.createQuery("FROM Funcionario c WHERE c.ci =:numci")
                    .setParameter("numci", ci).getSingleResult();
            em.getTransaction().commit();
            return fun;
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            return new Funcionario();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Funcionario> listarFuncionariosDisponibles(String filtro) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (filtro.contentEquals("null")) {
                filtro = "%%";
            } else {
                filtro = filtro.toUpperCase() + "%";
            }
            return em
                    .createQuery(
                            "FROM Funcionario f WHERE upper(f.ci) like :filtro AND NOT EXISTS(FROM Usuario u WHERE u.funcionario.idFuncionario = f.idFuncionario) ORDER BY f.apellido, f.nombre ASC")
                    .setParameter("filtro", filtro).getResultList();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
