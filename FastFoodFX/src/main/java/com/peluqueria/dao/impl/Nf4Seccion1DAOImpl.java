package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.dao.Nf4Seccion1DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf4Seccion1DAOImpl implements Nf4Seccion1DAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf4Seccion1> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf4Seccion1 nf4Seccion1 order by nf4Seccion1.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf4Seccion1 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf4Seccion1.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf4Seccion1 nf4Seccion1) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf4Seccion1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf4Seccion1 nf4Seccion1) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf4Seccion1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf4Seccion1> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf4Seccion1 nf4Seccion1 WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf4Seccion1.idNf4Seccion1=nf4Seccion1.idNf4Seccion1)"
                            + " AND nf4Seccion1.idNf4Seccion1 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf4Seccion1> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf4Seccion1 nf4Seccion1 WHERE NOT EXISTS(FROM FuncionarioNf4 funcionarioNf4 "
                            + "WHERE funcionarioNf4.estadoDesc=true AND funcionarioNf4.nf4Seccion1.idNf4Seccion1=nf4Seccion1.idNf4Seccion1) "
                            + "AND nf4Seccion1.idNf4Seccion1 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf4Seccion1> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf4Seccion1 nf4Seccion1 WHERE NOT EXISTS(FROM PromoTemporadaNf4 promoTemporadaNf4 WHERE"
                            + " promoTemporadaNf4.promoTemporada.estadoPromo=true AND promoTemporadaNf4.nf4Seccion1.idNf4Seccion1=nf4Seccion1.idNf4Seccion1 AND"
                            + " ((to_date(to_char(promoTemporadaNf4.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf4.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf4Seccion1.idNf4Seccion1 != 0 ORDER BY nf4Seccion1.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf4Seccion1> listarPorNf(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf4Seccion1 nf4Seccion1 WHERE nf4Seccion1.nf3Sseccion.idNf3Sseccion=" + id
                    + " order by nf4Seccion1.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
