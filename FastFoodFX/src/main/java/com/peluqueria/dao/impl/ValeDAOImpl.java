package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Vales;
import com.peluqueria.dao.ValeDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ValeDAOImpl implements ValeDAO {

    @Override
    public List<Vales> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Vales> ls = em.createQuery("FROM Vales v WHERE v.idVale NOT IN (1, 2, 3, 4, 5) ORDER BY v.descripcionVale")
                    .getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Vales getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Vales v = (Vales) em.createQuery("FROM Vales v WHERE v.idVale =:idVa")
                    .setParameter("idVa", id).getSingleResult();
            return v;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Vales obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Vales obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
    }

}
