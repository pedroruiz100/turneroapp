package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.UsuarioRol;
import com.peluqueria.dao.UsuarioRolDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class UsuarioRolDAOImpl implements UsuarioRolDAO {

    @Override
    public List<UsuarioRol> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM UsuarioRol ur").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public UsuarioRol getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(UsuarioRol.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(UsuarioRol obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(UsuarioRol obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List<UsuarioRol> buscarUsuarioRol(String usuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery("Select UR from UsuarioRol UR where (UPPER(UR.usuario.nomUsuario) = :usuario)" + "")
                    .setParameter("usuario", usuario.toUpperCase()).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void delete(Long idUsuario) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.createNativeQuery("DELETE FROM seguridad.usuario_rol WHERE id_usuario=" + idUsuario)
//                    .executeUpdate();
//        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.createNativeQuery("DELETE FROM seguridad.usuario_rol WHERE id_usuario=" + idUsuario)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public UsuarioRol insertarObtenerObjeto(UsuarioRol usuarioRol) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(usuarioRol);
            em.refresh(usuarioRol);
            em.getTransaction().commit();
            return usuarioRol;
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
