package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.MotivoCancelacionFactura;
import com.peluqueria.dao.MotivoCancelacionFacturaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class MotivoCancelacionFacturaDAOImpl implements MotivoCancelacionFacturaDAO {

    @Override
    public List<MotivoCancelacionFactura> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM MotivoCancelacionFactura mcf").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public MotivoCancelacionFactura getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(MotivoCancelacionFactura.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(MotivoCancelacionFactura obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(MotivoCancelacionFactura obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
