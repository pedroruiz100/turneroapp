/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Recepcion;
import com.peluqueria.dao.RecepcionDAO;
import static com.javafx.util.EMF.getEmf;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class RecepcionDAOImpl implements RecepcionDAO {

    @Override
    public Recepcion insertarObtenerObj(Recepcion aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Recepcion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Recepcion ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Recepcion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Recepcion) em.createQuery("FROM Recepcion ap WHERE ap.idRecepcion=:aid")
                    .setParameter("aid", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Recepcion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Recepcion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Recepcion ac = em.find(Recepcion.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public String recuperarPorCajaFecha(long idCaja, String fecha) {
//
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            String sql = "SELECT fecha_apertura FROM caja.apertura_caja WHERE id_caja=" + idCaja + " AND "
//                    + "to_char(fecha_apertura, 'YYYY-MM-DD')='" + fecha + "'";
//            System.out.println("SQL -> " + sql);
//            String fechaData = String.valueOf(em.createNativeQuery(sql)
//                    .setMaxResults(1)
//                    .getSingleResult());
//            return fechaData;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public Recepcion getByNroOrden(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Recepcion) em.createQuery("FROM Recepcion ap JOIN FETCH ap.pedidoCab pc WHERE ap.nroOrden=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Recepcion getByNroOrdenPedido(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Recepcion) em.createQuery("FROM Recepcion ap JOIN FETCH ap.pedidoCab pc WHERE pc.oc=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Recepcion> listarPorFecha(Date desde, Date hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Recepcion ap WHERE ap.fechaDoc>= :desde AND ap.fechaDoc <= :hasta")
                    .setParameter("desde", desde)
                    .setParameter("hasta", hasta)
                    .getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Map getByNroOrdenEntrada(String compra, String entrada) {
        EntityManager em = null;
        Map mapeo = new HashMap();
//        Recepcion recep = new Recepcion();
        if (!compra.equals("") && !entrada.equals("")) {
//            try {
//                long id = Long.parseLong(String.valueOf(em.createNativeQuery("SELECT id_pedido_cab "
//                        + "FROM factura_cliente.pedido_cab WHERE oc='" + compra + "'")
//                        .setMaxResults(1)
//                        .getSingleResult()));
//                mapeo.put("idPedidoCab", id);
//                mapeo.put("idRecepcion", "");
//            } catch (Exception ex) {
//                System.out.println("-->> " + ex.getLocalizedMessage());
//                System.out.println("-->> " + ex.getMessage());
//                System.out.println("-->> " + ex.fillInStackTrace());
            mapeo = null;
//            } finally {
//                if (em != null) {
//                    em.close();
//                }
//            }
        } else if (!compra.equals("")) {
//            try {
//                String sql = "SELECT id_pedido_cab FROM factura_cliente.pedido_cab WHERE oc='" + compra + "'";
//                System.out.println("sql ->> " + sql);
//                long id = Long.parseLong(String.valueOf(em.createNativeQuery(sql)
//                        .setMaxResults(1)
//                        .getSingleResult()));
//                mapeo.put("idPedidoCab", id);
//                mapeo.put("idRecepcion", "");
//            } catch (Exception ex) {
//                System.out.println("-->> " + ex.getLocalizedMessage());
//                System.out.println("-->> " + ex.getStackTrace());
//                System.out.println("-->> " + ex.fillInStackTrace());
//                ex.printStackTrace();
            mapeo = null;
//            } finally {
//                if (em != null) {
//                    em.close();
//                }
//            }
        } else if (!entrada.equals("")) {
            try {
                em = getEmf().createEntityManager();
                Recepcion recep = (Recepcion) em.createQuery("FROM Recepcion recep WHERE recep.nroOrden=:entrada")
                        .setParameter("entrada", entrada)
                        .getSingleResult();
                mapeo.put("idPedidoCab", "");
                mapeo.put("idRecepcion", recep.getIdRecepcion());
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                mapeo = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return mapeo;
    }

    @Override
    public List<Recepcion> getByNroOrdenEntradaFecha(String compra, String entrada, LocalDate fecha) {
        EntityManager em = null;
        Map mapeo = new HashMap();
        List<Recepcion> listRecepcion = new ArrayList<>();
//        Recepcion recep = new Recepcion();
        if (!compra.equals("") && !entrada.equals("")) {
            mapeo = null;
        } else if (!compra.equals("")) {
            mapeo = null;
        } else if (!entrada.equals("")) {
            try {
                System.out.println("FECHA: -> " + Date.valueOf(fecha));
                em = getEmf().createEntityManager();
                listRecepcion = (List<Recepcion>) em.createQuery("FROM Recepcion recep WHERE recep.nroOrden=:entrada AND "
                        + "recep.fechaDoc=:fecha")
                        //                Recepcion recep = (Recepcion) em.createQuery("FROM Recepcion recep WHERE recep.nroOrden=:entrada AND "
                        //                        + "recep.fechaDoc=:fecha")
                        .setParameter("entrada", entrada)
                        .setParameter("fecha", Date.valueOf(fecha))
                        .getResultList();
//                        .setMaxResults(1)
//                        .getSingleResult();
//                listRecepcion.add(recep);
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                listRecepcion = new ArrayList<>();
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return listRecepcion;
    }

    @Override
    public Recepcion getByIdFecha(Long idPedidoCab, LocalDate fecha) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Recepcion) em.createQuery("FROM Recepcion ap WHERE ap.idRecepcion=:aid AND recep.fechaDoc=:fecha")
                    .setParameter("aid", idPedidoCab)
                    .setParameter("fecha", Date.valueOf(fecha))
                    .getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
