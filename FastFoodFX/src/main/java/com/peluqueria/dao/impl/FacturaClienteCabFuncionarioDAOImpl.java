package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabFuncionario;
import com.peluqueria.dao.FacturaClienteCabFuncionarioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabFuncionarioDAOImpl implements
        FacturaClienteCabFuncionarioDAO {

    @Override
    public List<FacturaClienteCabFuncionario> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabFuncionario fac")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabFuncionario getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabFuncionario) em
                    .createQuery(
                            "FROM FacturaClienteCabFuncionario fac WHERE idFacturaClienteCabFuncionarioSrv=:idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabFuncionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabFuncionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public FacturaClienteCabFuncionario insertarObtenerObjeto(FacturaClienteCabFuncionario facturaClienteCabFuncionario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabFuncionario);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabFuncionario);
            return facturaClienteCabFuncionario;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
