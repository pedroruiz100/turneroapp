/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.dao.PedidoCabDAO;
import static com.javafx.util.EMF.getEmf;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class PedidoCabDAOImpl implements PedidoCabDAO {

    @Override
    public PedidoCab insertarObtenerObj(PedidoCab aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoCab ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab ap WHERE ap.idPedidoCab=:aid")
                    .setParameter("aid", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PedidoCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PedidoCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            PedidoCab ac = em.find(PedidoCab.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCab getByNroOrden(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab ap JOIN FETCH ap.proveedor pro WHERE ap.nroPedido=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCab> listarPorFecha(Date desde, Date hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoCab ap WHERE ap.fecha>= :desde AND ap.fecha <= :hasta")
                    .setParameter("desde", desde)
                    .setParameter("hasta", hasta)
                    .getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCab> filtrarPorProveedorSucursalOc(String proveedor, String sucursal, String oc) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        List<PedidoCab> lista = new ArrayList<>();
        if (!proveedor.equals("") && !sucursal.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.sucursal=:suc AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("suc", sucursal)
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("") && !sucursal.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.sucursal=:suc AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("suc", sucursal)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!sucursal.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.sucursal=:suc AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("suc", sucursal)
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!sucursal.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.sucursal=:suc AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("suc", sucursal)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.oc=:orden")
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.oc!=null")
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return lista;
    }

    @Override
    public PedidoCab listarPorOrden(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab pc WHERE pc.oc=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCab getByOC(String oc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab ap JOIN FETCH ap.proveedor pro WHERE ap.oc=:aid")
                    .setParameter("aid", oc).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCab listarPorOrdenSucursal(String compra, String sucursal) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab pc WHERE pc.oc=:aid AND UPPER(pc.sucursal) LIKE :sucu")
                    .setParameter("sucu", sucursal.toUpperCase() + "%")
                    .setParameter("aid", compra).getSingleResult();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCab> filtrarPorProveedorOc(String proveedor, String oc) {
        String sucursal = "";
        EntityManager em = null;
        em = getEmf().createEntityManager();
        List<PedidoCab> lista = new ArrayList<>();
        if (!proveedor.equals("") && !sucursal.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.sucursal=:suc AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("suc", sucursal)
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("") && !sucursal.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.sucursal=:suc AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("suc", sucursal)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!sucursal.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.sucursal=:suc AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("suc", sucursal)
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("") && !oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.oc=:orden ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!proveedor.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.proveedor.idProveedor=:idProv AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("idProv", Long.parseLong(proveedor))
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!sucursal.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.sucursal=:suc AND ap.oc!=null ORDER BY ap.idPedidoCab DESC")
                        .setParameter("suc", sucursal)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else if (!oc.equals("")) {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.oc=:orden")
                        .setParameter("orden", oc)
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        } else {
            try {
                lista = em.createQuery("FROM PedidoCab ap WHERE ap.oc!=null")
                        .getResultList();
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                lista = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return lista;
    }

    @Override
    public PedidoCab listarPorCompraCab(String id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCab) em.createQuery("FROM PedidoCab ap JOIN FETCH ap.proveedor pro WHERE ap.nroPedido=:aid")
                    .setParameter("aid", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
