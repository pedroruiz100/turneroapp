package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescFuncionario;
import com.peluqueria.dao.DescFuncionarioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescFuncionarioDAOImpl implements DescFuncionarioDAO {

    @Override
    public List<DescFuncionario> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescFuncionario df").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescFuncionario getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescFuncionario) em
                    .createQuery(
                            "FROM DescFuncionario df WHERE df.idDescFuncionario=:idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescFuncionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescFuncionario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescFuncionario desc = em.find(DescFuncionario.class, id);
            em.remove(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
