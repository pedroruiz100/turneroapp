package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Deposito;
import com.peluqueria.dao.DepositoDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DepositoDAOImpl implements DepositoDAO {

    @Override
    public List<Deposito> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Deposito m").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Deposito getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Deposito.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Deposito obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Deposito obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public long getByDescripcion(String selectedItem) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return Long.parseLong(em.createNativeQuery("SELECT id_deposito FROM stock.deposito WHERE UPPER(descripcion) LIKE '" + selectedItem.toUpperCase() + "%'").getSingleResult() + "");
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
