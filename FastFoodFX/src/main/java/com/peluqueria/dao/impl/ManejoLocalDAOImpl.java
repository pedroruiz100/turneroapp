package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ManejoLocal;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.util.EMF2;

@Repository
public class ManejoLocalDAOImpl implements ManejoLocalDAO {

    @Override
    public List<ManejoLocal> listar() {
        EntityManager em = null;
        try {
            em = EMF2.getEmf().createEntityManager();
            return em.createQuery("FROM ManejoLocal d ORDER BY d.idManejo").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ManejoLocal getById(long id) {
        EntityManager em = null;
        try {
            em = EMF2.getEmf().createEntityManager();
            return em.find(ManejoLocal.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insertar(ManejoLocal obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(ManejoLocal obj) {

    }

    @Override
    public boolean actualizarObtenerEstado(ManejoLocal manejo) {
        EntityManager em = null;
        try {
            em = EMF2.getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(manejo);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(ManejoLocal manejo) {
        EntityManager em = null;
        try {
            em = EMF2.getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(manejo);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean eliminarObtenerEstado(long id) {
        EntityManager em = null;
        try {
            em = EMF2.getEmf().createEntityManager();
            ManejoLocal manejo = em.find(ManejoLocal.class, id);
            em.getTransaction().begin();
            em.remove(manejo);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings({"JPQLValidation", "null"})
    public long verificarExistencia() {
        EntityManager em = null;
        long num = 0;
        try {
            em = EMF2.getEmf().createEntityManager();
            num = (long) (em
                    .createQuery(
                            "SELECT MAX(idManejo) FROM ManejoLocal")
                    .getSingleResult());
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return num;
    }

    @Override
    @SuppressWarnings({"JPQLValidation", "null"})
    public long recuperarId() {
        EntityManager em = null;
        long num = 0;
        try {
            em = EMF2.getEmf().createEntityManager();
            num = (long) (em
                    .createQuery(
                            "SELECT MAX(idManejo) FROM ManejoLocal")
                    .getSingleResult());
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return num;
    }

}
