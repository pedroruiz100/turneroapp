package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabTarjConvenio;
import com.peluqueria.dao.FacturaClienteCabTarjConvenioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabTarjConvenioDAOImpl implements
        FacturaClienteCabTarjConvenioDAO {

    @Override
    public List<FacturaClienteCabTarjConvenio> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabTarjConvenio fcc")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabTarjConvenio getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabTarjConvenio) em
                    .createQuery(
                            "FROM FacturaClienteCabTarjConvenio fcc WHERE fcc.idFacturaClienteCabTarjConvenio =:idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabTarjConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabTarjConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaClienteCabTarjConvenio fac = em.find(
                    FacturaClienteCabTarjConvenio.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabTarjConvenio insertarObtenerObjeto(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabTarjConvenio);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabTarjConvenio);
            return facturaClienteCabTarjConvenio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
