package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.dao.ClienteDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import org.springframework.stereotype.Repository;

@Repository
public class ClienteDAOImpl implements ClienteDAO {

    @Override
    public List<Cliente> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Cliente> ls = em.createQuery("FROM Cliente c ORDER BY c.fechaAlta")
                    .getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cliente getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Cliente c = em.find(Cliente.class, id);
            return c;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Cliente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
//            em.refresh(obj);
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Cliente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Cliente c = em.find(Cliente.class, id);
            em.remove(c);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Cliente> listarPorNomRuc(String nombre, String apellido,
            String ruc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Cliente> listaCliente = new ArrayList<>();
            if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")
                    && !ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "and upper(c.ruc) like :numRuc ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setParameter("numRuc", ruc.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();

            } else if (!nombre.equalsIgnoreCase("null")
                    && !apellido.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.apellido) like :apellido "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")
                    && !ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.nombre) like :nombre "
                                + "and upper(c.ruc) like :numRuc "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("numRuc", ruc.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!apellido.equalsIgnoreCase("null")
                    && !ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.apellido) like :apellido "
                                + "and upper(c.ruc) like :numRuc "
                                + "ORDER BY c.nombre, c.apellido")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setParameter("numRuc", ruc.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!apellido.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.apellido) like :apellido ORDER BY c.apellido")
                        .setParameter("apellido", apellido.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();
            } else if (!nombre.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "FROM Cliente c WHERE upper(c.nombre) like :nombre ORDER BY c.nombre")
                        .setMaxResults(10)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getResultList();
            } else if (!ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery("FROM Cliente c WHERE c.ruc =:numRuc")
                        .setParameter("numRuc", ruc).getResultList();

            } else {
                listaCliente = em
                        .createQuery("FROM Cliente c ORDER BY c.fechaAlta")
                        .setMaxResults(10).getResultList();
            }
            return listaCliente;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarNomApeRuc(String nombre, String apellido, String ruc, String telefono, String celular,
            long id, String usuMod, Timestamp fechaMod) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Cliente cli = em.find(Cliente.class, id);
            cli.setNombre(nombre);
            cli.setRuc(ruc);
            cli.setApellido(apellido);
            cli.setUsuMod(usuMod);
            cli.setFechaMod(fechaMod);
            cli.setTelefono(telefono);
            cli.setTelefono2(celular);
            Pais pais = new Pais();
            pais.setIdPais(0L);
            Departamento dpto = new Departamento();
            dpto.setIdDepartamento(0l);
            Ciudad ciu = new Ciudad();
            ciu.setIdCiudad(0l);
            Barrio barr = new Barrio();
            barr.setIdBarrio(0l);
            cli.setPais(pais);
            cli.setDepartamento(dpto);
            cli.setCiudad(ciu);
            cli.setBarrio(barr);
            em.merge(cli);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cliente listarPorCodigo(int codCliente) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Cliente) em.createQuery("FROM Cliente c WHERE c.codCliente=:cod")
                    .setParameter("cod", codCliente)
                    .getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarClienteEstetica(JSONObject cliente) {
        JSONParser parser = new JSONParser();
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Cliente cli = em.find(Cliente.class, Long.parseLong(cliente.get("idCliente").toString()));
            cli.setNombre(cliente.get("nombre").toString());
            cli.setApellido(cliente.get("apellido").toString());
            cli.setRuc(cliente.get("ruc").toString());
            cli.setCallePrincipal(cliente.get("callePrincipal").toString());
            cli.setTelefono(cliente.get("telefono").toString());
            cli.setTelefono2(cliente.get("telefono2").toString());
            cli.setEmail(cliente.get("email").toString());
            cli.setUsuMod(cliente.get("usuMod").toString());
            long fecMod = Long.parseLong(cliente.get("fechaMod").toString());
            cli.setFechaMod(new Timestamp(fecMod));
            if (cliente.get("fecNac") == null) {
                cli.setFecNac(null);
            } else {
                cli.setFecNac(Utilidades.stringToSqlDate(cliente.get("fecNac").toString()));
            }
            long idCiu = 0l;
            long idBar = 0l;
            JSONObject jsonCiu = (JSONObject) parser.parse(cliente.get("ciudad").toString());
            idCiu = Long.parseLong(jsonCiu.get("idCiudad").toString());
            JSONObject jsonBar = (JSONObject) parser.parse(cliente.get("barrio").toString());
            idBar = Long.parseLong(jsonBar.get("idBarrio").toString());
            Ciudad ciu = new Ciudad();
            ciu.setIdCiudad(idCiu);
            Barrio barr = new Barrio();
            barr.setIdBarrio(idBar);
            cli.setCiudad(ciu);
            cli.setBarrio(barr);
            em.getTransaction().begin();
            em.merge(cli);
            em.getTransaction().commit();
            return true;
        } catch (NumberFormatException | ParseException e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            Utilidades.log.error("MENSAJE -> ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cliente listarPorRuc(String ruc) {
        EntityManager em = null;
        try {
//            em = getEmf().createEntityManager();
//            return (Cliente) em.createQuery("FROM Cliente c WHERE c.ruc =:numRuc").setParameter("numRuc", ruc).getSingleResult();
            em = getEmf().createEntityManager();
            return (Cliente) em.createQuery("FROM Cliente c WHERE upper(c.ruc) like :numRuc")
                    .setParameter("numRuc", ruc.toUpperCase() + "%")
                    .setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
//            Cliente cli = new Cliente();
//            cli.setIdCliente(null);
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Cliente> listarPorCIRevancha(String rucCliente) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (List<Cliente>) em.createQuery("FROM Cliente c WHERE upper(c.ruc) like :numRuc")
                    .setParameter("numRuc", rucCliente.toUpperCase() + "-%").getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cliente getByCod(int codCli) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Cliente cli = (Cliente) em.createQuery("FROM Cliente c WHERE c.codCliente=:cod")
                    .setParameter("cod", codCli)
                    .getSingleResult();
            return cli;
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cliente listarPorRucExacto(String ruc) {
        EntityManager em = null;
        Cliente cli = new Cliente();
        cli.setIdCliente(null);
        try {
            em = getEmf().createEntityManager();
            List<Cliente> listCliente = em.createQuery("FROM Cliente c WHERE upper(c.ruc) like :numRuc")
                    .setParameter("numRuc", ruc.toUpperCase() + "%")
                    //                    .setMaxResults(1)
                    .getResultList();
            for (Cliente cliente : listCliente) {
                if (!isNumeric(ruc)) {
                    if (cliente.getRuc().toUpperCase().equalsIgnoreCase(ruc.toUpperCase())) {
                        cli = cliente;
                    }
                } else {
                    StringTokenizer st = new StringTokenizer(cliente.getRuc(), "-");
                    String rucCli = st.nextElement().toString();

                    if (rucCli.toUpperCase().equalsIgnoreCase(ruc.toUpperCase())) {
                        cli = cliente;
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            cli.setIdCliente(null);
            return cli;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return cli;
    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @Override
    public boolean insertarDatos(Cliente cli) {
        EntityManager em = null;
        boolean valor = true;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(cli);
            em.getTransaction().commit();
//            em.refresh(obj);
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public boolean actualizarNomApeRucReturn(String nombre, String apellido, String ruc, String telefono, String telefono2, Long idCliente, String usuMod, Timestamp fechaMod) {
        EntityManager em = null;
        boolean data = false;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Cliente cli = em.find(Cliente.class, idCliente);
            cli.setNombre(nombre);
            cli.setRuc(ruc);
            cli.setApellido(apellido);
            cli.setUsuMod(usuMod);
            cli.setFechaMod(fechaMod);
            cli.setTelefono(telefono);
            cli.setTelefono2(telefono2);
            Pais pais = new Pais();
            pais.setIdPais(0L);
            Departamento dpto = new Departamento();
            dpto.setIdDepartamento(0l);
            Ciudad ciu = new Ciudad();
            ciu.setIdCiudad(0l);
            Barrio barr = new Barrio();
            barr.setIdBarrio(0l);
            cli.setPais(pais);
            cli.setDepartamento(dpto);
            cli.setCiudad(ciu);
            cli.setBarrio(barr);
            em.merge(cli);
            em.getTransaction().commit();
            data = true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            data = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return data;
    }
}
