package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf7Secnom7DAOImpl implements ArticuloNf7Secnom7DAO {

    @Override
    public List<ArticuloNf7Secnom7> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf7Secnom7 a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf7Secnom7 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf7Secnom7) em
                    .createQuery("FROM ArticuloNf7Secnom7 a WHERE a.idNf7Secnom7Articulo=:idNf7Secnom7Articulo")
                    .setParameter("idNf7Secnom7Articulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf7Secnom7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf7Secnom7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf7Secnom7 articuloNf7Secnom7 = em.find(ArticuloNf7Secnom7.class, id);
            em.remove(articuloNf7Secnom7);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf7Secnom7 getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf7Secnom7) em
                    .createQuery("FROM ArticuloNf7Secnom7 a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void deleteArticulo(long idArt) {
//        ArticuloNf7Secnom7 artNF1 = articuloNf7Secnom7DAO.listarArticulo(idArticulo);
//        articuloNf7Secnom7DAO.eliminar(artNF1.getIdNf7Secnom7Articulo());
    }

    @Override
    public ArticuloNf7Secnom7 listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf7Secnom7) em.createQuery("FROM ArticuloNf7Secnom7 a WHERE a.articulo.idArticulo=:idArt")
                    .setParameter("idArt", idArt).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
