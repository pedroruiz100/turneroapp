package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.FacturaClienteCabCheque;
import com.peluqueria.dao.FacturaClienteCabChequeDAO;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabChequeDAOImpl implements
        FacturaClienteCabChequeDAO {

    @Override
    public List<FacturaClienteCabCheque> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabCheque f").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabCheque getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabCheque) em
                    .createQuery(
                            "FROM FacturaClienteCabCheque fccc WHERE fccc.idFacturaClienteCabCheque = :idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabCheque obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabCheque obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaClienteCabCheque fac = em
                    .find(FacturaClienteCabCheque.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insercionMasiva(FacturaClienteCabCheque fac, long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(fac);
            em.getTransaction().commit();
            if (id % 20 == 0) {
                em.flush();
                em.clear();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCabCheque> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabCheque fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
