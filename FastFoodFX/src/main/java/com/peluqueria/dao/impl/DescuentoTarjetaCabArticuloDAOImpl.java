package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoTarjetaCabArticulo;
import com.peluqueria.dao.DescuentoTarjetaCabArticuloDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaCabArticuloDAOImpl implements DescuentoTarjetaCabArticuloDAO {

    @Override
    public List<DescuentoTarjetaCabArticulo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCabArticulo dtcd ORDER BY dtcd.idDescuentoTarjetaCabArticulo DESC")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCabArticulo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DescuentoTarjetaCabArticulo det = (DescuentoTarjetaCabArticulo) em.createQuery(
                    "FROM DescuentoTarjetaCabArticulo dtcd WHERE dtcd.idDescuentoTarjetaCabArticulo=:idDet ORDER BY dtcd.idDescuentoTarjetaCabArticulo DESC")
                    .setParameter("idDet", id)
                    .getSingleResult();
            return det;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCabArticulo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCabArticulo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<DescuentoTarjetaCabArticulo> listarLimitado(long limite, long inicio,
            String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCabArticulo dtcd WHERE upper(dtcd.descuentoTarjetaCab.descriTarjeta) like :descriTar "
                            + "ORDER BY dtcd.idDescuentoTarjetaCabArticulo")
                    .setParameter("descriTar", nombre.toUpperCase() + "%")
                    .setFirstResult((int) inicio).setMaxResults((int) limite)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabArticulo);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.info("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public List<DescuentoTarjetaCabArticulo> listarPorId(String id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCabArticulo dtcd WHERE dtcd.descuentoTarjetaCab.idDescuentoTarjetaCab=:id ORDER BY dtcd.idDescuentoTarjetaCabArticulo DESC")
                    .setParameter("id", id).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
