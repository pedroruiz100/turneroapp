package com.peluqueria.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.FuncionarioNf4;
import com.peluqueria.dao.FuncionarioNf4DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import javax.persistence.PersistenceException;

@Repository
public class FuncionarioNf4DAOImpl implements FuncionarioNf4DAO {

    @Override
    public List<FuncionarioNf4> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<FuncionarioNf4> funcionarioNf4s = em
                    .createQuery(
                            "FROM FuncionarioNf4 funcionarioNf4 WHERE funcionarioNf4.estadoDesc=true ORDER BY funcionarioNf4.descriSeccion")
                    .getResultList();
            return funcionarioNf4s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf4 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FuncionarioNf4.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FuncionarioNf4 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FuncionarioNf4 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FuncionarioNf4 sf = em.find(FuncionarioNf4.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf4> listarFetchNf4(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FuncionarioNf4 funcionarioNf4 WHERE funcionarioNf4.estadoDesc=true ORDER BY funcionarioNf4.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf4> listarFetchFilterNf4(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FuncionarioNf4 funcionarioNf4 WHERE funcionarioNf4.estadoDesc=true AND"
                            + " (LOWER(funcionarioNf4.descriSeccion) LIKE :descri) order by funcionarioNf4.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf4() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.seccion_func sf WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilterNf4(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM FuncionarioNf4 funcionarioNf4 WHERE funcionarioNf4.estadoDesc=true AND"
                    + " (LOWER(funcionarioNf4.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf4 funcionarioNf4 "
                    + "SET funcionarioNf4.estadoDesc=false "
                    + "WHERE funcionarioNf4.estadoDesc=true");
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(FuncionarioNf4 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf4 funcionarioNf4 "
                    + "SET funcionarioNf4.estadoDesc=false, funcionarioNf4.usuMod=:uMod, funcionarioNf4.fechaMod=:fMod "
                    + "WHERE funcionarioNf4.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf4 insertarObtenerObjeto(FuncionarioNf4 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean validacionInsercion(long idNf) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            if (em.createQuery(
                    "FROM FuncionarioNf4 as funcionarioNf4 "
                    + "WHERE funcionarioNf4.estadoDesc = true "
                    + "AND funcionarioNf4.nf4Seccion1.idNf4Seccion1 = :idNf "
                    + "ORDER BY funcionarioNf4.descriSeccion")
                    .setParameter("idNf", idNf)
                    .setMaxResults(1)
                    .getSingleResult() != null) {
                existe = true;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
