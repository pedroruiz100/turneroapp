package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoNotaCred;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoNotaCredDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoNotaCredDAOImpl implements RangoNotaCredDAO {

    @Override
    public List<RangoNotaCred> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoNotaCred getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoNotaCred obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoNotaCred obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public long actualizarObtenerRango(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoNotaCred rango = em.find(RangoNotaCred.class, idRango);
            r = rango.getRangoActual();
            try {
                rango.setRangoActual(r + 1);
                em.getTransaction().begin();
                em.merge(rango);
                em.getTransaction().commit();
                return r;
            } catch (Exception e) {
                System.out.println("--->> " + e.getLocalizedMessage());
                return r;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
