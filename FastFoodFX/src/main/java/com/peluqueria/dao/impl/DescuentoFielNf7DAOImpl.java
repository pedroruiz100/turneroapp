package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.DescuentoFielNf7;
import com.peluqueria.dao.DescuentoFielNf7DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class DescuentoFielNf7DAOImpl implements DescuentoFielNf7DAO {

    @Override
    public List<DescuentoFielNf7> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoFielNf7 descuentoFielNf7 WHERE descuentoFielNf7.descuentoFielCab.estadoDesc=true "
                    + "ORDER BY idDescuentoFielNf7").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoFielNf7 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoFielNf7.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoFielNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoFielNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(DescuentoFielNf7 descuentoFielNf7, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoFielNf7);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(DescuentoFielNf7 descuentoFielNf7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoFielNf7);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoFielNf7 descuentoFielNf7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoFielNf7);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

}
