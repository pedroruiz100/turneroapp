/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.AperturaCaja;
import com.peluqueria.core.domain.Gastos;
import com.peluqueria.dao.GastosDAO;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class GastosDAOImpl implements GastosDAO {

    @Override
    public Gastos insertarObtenerObj(Gastos aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Gastos> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Gastos ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Gastos getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Gastos) em.createQuery("FROM Gastos ap WHERE ap.idGasto=:aid")
                    .setParameter("aid", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Gastos obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Gastos obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            AperturaCaja ac = em.find(AperturaCaja.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public String recuperarPorCajaFecha(long idCaja, String fecha) {
//
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            String sql = "SELECT fecha_apertura FROM caja.apertura_caja WHERE id_caja=" + idCaja + " AND "
//                    + "to_char(fecha_apertura, 'YYYY-MM-DD')='" + fecha + "'";
//            System.out.println("SQL -> " + sql);
//            String fechaData = String.valueOf(em.createNativeQuery(sql)
//                    .setMaxResults(1)
//                    .getSingleResult());
//            return fechaData;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public boolean insertarEstado(Gastos pro) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(pro);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public boolean actualizarEstado(Gastos pro) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(pro);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public List<Gastos> listarPorFechas(LocalDate firstDay, LocalDate lastDay) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Gastos ap WHERE DATE(ap.fecha)>=:desde AND DATE(ap.fecha)<=:hasta ORDER BY ap.tipo")
                    .setParameter("desde", Date.from(firstDay.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .setParameter("hasta", Date.from(lastDay.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Gastos> filtroFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Gastos fcc WHERE to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.fecha")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Gastos recuperarUltimo() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Gastos) em.createQuery("FROM Gastos ap ORDER BY ap.idGasto DESC")
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<String> listarProveedorDistinct() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<String> listTimb = em
                    .createQuery(
                            "SELECT distinct empresa FROM Gastos fac ORDER BY empresa")
                    .getResultList();
            return listTimb;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Iterable<Gastos> filtroFechaTipo(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Gastos fcc WHERE to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.tipo")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
