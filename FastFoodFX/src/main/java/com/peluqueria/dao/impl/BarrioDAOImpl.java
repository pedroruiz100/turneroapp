package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.dao.BarrioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class BarrioDAOImpl implements BarrioDAO {

    @Override
    public List<Barrio> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Barrio> listBarrio = em.createQuery("FROM Barrio b WHERE b.activo=true ORDER BY b.descripcion").getResultList();
            return listBarrio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Barrio getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Barrio) em
                    .createQuery("Select b from Barrio b WHERE b.idBarrio=:idBa")
                    .setParameter("idBa", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Barrio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Barrio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Barrio ba = em.find(Barrio.class, id);
            em.remove(ba);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM general.barrio b";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Barrio> listarNoFetch(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("SELECT B FROM Barrio B")
                    .setMaxResults((int) limRow)// LIMIT
                    // "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Barrio> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery("SELECT B FROM Barrio B JOIN FETCH B.ciudad as C")
                    .setMaxResults((int) limRow)// LIMIT
                    // "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Barrio> getByIdCiudad(long idCi) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Barrio> listBarrio = em.createQuery("FROM Barrio b WHERE b.ciudad.idCiudad=:idCi ORDER BY b.descripcion")
                    .setParameter("idCi", idCi).getResultList();
            return listBarrio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Barrio> listarPorCiudad(long idCiudad) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Barrio> listBarrio = em.createQuery("FROM Barrio b WHERE b.activo=true AND b.ciudad.idCiudad=:idCiu")
                    .setParameter("idCiu", idCiudad)
                    .getResultList();
            return listBarrio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
