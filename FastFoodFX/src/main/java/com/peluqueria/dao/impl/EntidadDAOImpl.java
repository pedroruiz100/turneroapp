/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Entidad;
import com.peluqueria.dao.EntidadDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class EntidadDAOImpl implements EntidadDAO {

    @Override
    public Entidad getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Entidad.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Entidad> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Entidad> entidads = em.createQuery("FROM Entidad e ORDER BY e.descripcion").getResultList();
            return entidads;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Entidad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Entidad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Entidad> listarNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Entidad e order by e.idEntidad asc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
