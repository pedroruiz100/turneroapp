package com.peluqueria.dao.impl;

import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.RangoRetiroCompra;
import com.peluqueria.dao.RangoRetiroCompraDAO;

@Repository
public class RangoRetiroCompraDAOImpl implements RangoRetiroCompraDAO {

    @Override
    public List<RangoRetiroCompra> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoRetiroCompra getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoRetiroCompra obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoRetiroCompra obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarObtenerRango(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoRetiroCompra rango = em.find(RangoRetiroCompra.class, idRango);
            r = rango.getRangoActual();
            try {
                rango.setRangoActual(r + 1);
                em.getTransaction().begin();
                em.merge(rango);
                em.getTransaction().commit();
                return r;
            } catch (Exception e) {
                System.out.println("--->> " + e.getLocalizedMessage());
                return r;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RangoRetiroCompra actualizarObtenerRangoObjectActual(long idRango) {
        EntityManager em = null;
//        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoRetiroCompra rango = em.find(RangoRetiroCompra.class, idRango);
            long r = rango.getRangoActual();
            try {
                rango.setRangoActual(r + 1);
                em.getTransaction().begin();
                em.merge(rango);
                em.getTransaction().commit();
                return rango;
            } catch (Exception e) {
                System.out.println("--->> " + e.getLocalizedMessage());
                return null;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
