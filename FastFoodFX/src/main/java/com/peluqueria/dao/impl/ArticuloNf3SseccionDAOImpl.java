package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf3SseccionDAOImpl implements ArticuloNf3SseccionDAO {

    @Override
    public List<ArticuloNf3Sseccion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf3Sseccion a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf3Sseccion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf3Sseccion) em
                    .createQuery("FROM ArticuloNf3Sseccion a WHERE a.idNf3SseccionArticulo=:idNf3SseccionArticulo")
                    .setParameter("idNf3SseccionArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf3Sseccion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf3Sseccion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf3Sseccion articuloNf3Sseccion = em.find(ArticuloNf3Sseccion.class, id);
            em.remove(articuloNf3Sseccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf3Sseccion getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf3Sseccion) em
                    .createQuery("FROM ArticuloNf3Sseccion a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf3Sseccion listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf3Sseccion) em
                    .createQuery("FROM ArticuloNf3Sseccion a WHERE a.articulo.idArticulo=:idNf3SseccionArticulo")
                    .setParameter("idNf3SseccionArticulo", idArt).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
