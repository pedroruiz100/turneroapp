package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.CancelacionFactura;
import com.peluqueria.dao.CancelacionFacturaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 * 
 *
 * @author ADMIN
 */
@Repository
public class CancelacionFacturaDAOImpl implements CancelacionFacturaDAO {

    @Override
    public List<CancelacionFactura> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM CancelacionFactura cf").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CancelacionFactura getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(CancelacionFactura.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(CancelacionFactura obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(CancelacionFactura obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            CancelacionFactura cf = em.find(CancelacionFactura.class, id);
            em.remove(cf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
