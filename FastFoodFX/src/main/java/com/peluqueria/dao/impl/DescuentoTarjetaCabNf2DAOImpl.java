package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf2;
import com.peluqueria.dao.DescuentoTarjetaCabNf2DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class DescuentoTarjetaCabNf2DAOImpl implements DescuentoTarjetaCabNf2DAO {

    @Override
    public List<DescuentoTarjetaCabNf2> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2 WHERE descuentoTarjetaCabNf2.descuentoTarjetaCab.estadoDesc=true "
                    + "ORDER BY idDescuentoTarjetaCabNf2").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCabNf2 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoTarjetaCabNf2.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCabNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCabNf2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(DescuentoTarjetaCabNf2 sec, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(sec);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf2);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf2);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("ERROR -->> insertarObtenerEstado(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2) " + e.fillInStackTrace());
            return false;
        }
    }

}
