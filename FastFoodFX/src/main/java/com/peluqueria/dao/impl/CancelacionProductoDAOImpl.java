package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.CancelacionProducto;
import com.peluqueria.dao.CancelacionProductoDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class CancelacionProductoDAOImpl implements CancelacionProductoDAO {

    @Override
    public List<CancelacionProducto> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM CancelacionProducto cp").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CancelacionProducto getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(CancelacionProducto.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(CancelacionProducto obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(CancelacionProducto obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            CancelacionProducto cp = em.find(CancelacionProducto.class, id);
            em.remove(cp);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
