package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.PreparacionDetalle;
import com.peluqueria.dao.PreparacionDetalleDAO;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class PreparacionDetalleDAOImpl implements
        PreparacionDetalleDAO {

    @Override
    public List<PreparacionDetalle> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PreparacionDetalle f").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PreparacionDetalle getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PreparacionDetalle) em
                    .createQuery(
                            "FROM PreparacionDetalle fccc WHERE fccc.idPreparacionDetalle = :idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PreparacionDetalle obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PreparacionDetalle obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            PreparacionDetalle fac = em
                    .find(PreparacionDetalle.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insercionMasiva(PreparacionDetalle fac, long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(fac);
            em.getTransaction().commit();
//            if (id % 20 == 0) {
//                em.flush();
//                em.clear();
//            }
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionDetalle> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PreparacionDetalle fact JOIN FETCH fact.gastos fcc WHERE fcc.idGasto=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionDetalle> listarTodosMayoresACero() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PreparacionDetalle fact JOIN FETCH fact.gastos fcc WHERE fact.cantidad>0 AND upper(fcc.tipo)=:tipo ORDER BY fcc.tipo")
                    .setParameter("tipo", "COMPRAS P/ PREPARACION")
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionDetalle> listarPorProveedorMayorAcero(String prov) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PreparacionDetalle fact JOIN FETCH fact.gastos fcc WHERE fact.cantidad>0 AND upper(fcc.tipo)=:tipo AND upper(fcc.empresa)=:empre ORDER BY fcc.tipo")
                    .setParameter("tipo", "COMPRAS P/ PREPARACION")
                    .setParameter("empre", prov.toUpperCase())
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionDetalle> listarPorFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PreparacionDetalle pd JOIN FETCH pd.preparacionCabecera pc JOIN FETCH pd.detalleGasto dg WHERE to_char(pc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(pc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY pc.fecha")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
