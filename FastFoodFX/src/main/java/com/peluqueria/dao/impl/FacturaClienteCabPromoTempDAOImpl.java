package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabPromoTemp;
import com.peluqueria.dao.FacturaClienteCabPromoTempDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabPromoTempDAOImpl implements
        FacturaClienteCabPromoTempDAO {

    @Override
    public List<FacturaClienteCabPromoTemp> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabPromoTemp f ORDER BY f.idFacturaClienteCabPromoTemp")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabPromoTemp getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabPromoTemp) em
                    .createQuery(
                            "FROM FacturaClienteCabPromoTemp f WHERE f.idFacturaClienteCabPromoTemp=:idPromo")
                    .setParameter("idPromo", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabPromoTemp obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabPromoTemp obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaClienteCabPromoTemp fac = em.find(FacturaClienteCabPromoTemp.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabPromoTemp insertarObtenerObjeto(
            FacturaClienteCabPromoTemp facturaClienteCabPromoTemporada) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabPromoTemporada);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabPromoTemporada);
            return facturaClienteCabPromoTemporada;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
