/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.AperturaCaja;
import com.peluqueria.dao.AperturaCajaDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class AperturaCajaDAOImpl implements AperturaCajaDAO {

    @Override
    public AperturaCaja insertarObtenerObj(AperturaCaja aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<AperturaCaja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM AperturaCaja ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public AperturaCaja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (AperturaCaja) em.createQuery("FROM AperturaCaja ap WHERE ap.idApertura=:aid")
                    .setParameter("aid", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(AperturaCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(AperturaCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            AperturaCaja ac = em.find(AperturaCaja.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public String recuperarPorCajaFecha(long idCaja, String fecha) {

        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT fecha_apertura FROM caja.apertura_caja WHERE id_caja=" + idCaja + " AND "
                    + "to_char(fecha_apertura, 'YYYY-MM-DD')='" + fecha + "'";
            System.out.println("SQL -> " + sql);
            String fechaData = String.valueOf(em.createNativeQuery(sql)
                    .setMaxResults(1)
                    .getSingleResult());
            return fechaData;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public AperturaCaja recuperarPorCaja(Long idCaja, int x) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (AperturaCaja) em.createQuery("FROM AperturaCaja ap JOIN FETCH ap.caja caja JOIN FETCH ap.usuarioCajero usuCaja WHERE caja.idCaja=:aid")
                    .setParameter("aid", idCaja)
                    .setFirstResult(x)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarRecuperarEstado(AperturaCaja ac) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(ac);
            em.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean verificarAperturaDia() {
        EntityManager em = null;
        boolean val = false;
        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            em = getEmf().createEntityManager();
            em.createQuery("FROM AperturaCaja ap where (to_date(to_char(ap.fechaApertura, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
                    .setParameter("fec", Utilidades.stringToUtilDate(format.format(date)))
                    .getResultList();
            val = true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }

    @Override
    public long getCantidadAperturaDia() {
        EntityManager em = null;
        long val = 0l;
        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            em = getEmf().createEntityManager();
            val = em.createQuery("FROM AperturaCaja ap where (to_date(to_char(ap.fechaApertura, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
                    .setParameter("fec", Utilidades.stringToUtilDate(format.format(date)))
                    .getResultList().size();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }
}
