package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoTarjetaCab;
import com.peluqueria.dao.DescuentoTarjetaCabDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaCabDAOImpl implements DescuentoTarjetaCabDAO {

    @Override
    public List<DescuentoTarjetaCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoTarjetaCab dfc WHERE dfc.estadoDesc=true ORDER BY dfc.descriTarjeta")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DescuentoTarjetaCab s = em.find(DescuentoTarjetaCab.class, id);
            return s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoTarjetaCab> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d"
                            + " WHERE dfc.estadoDesc=true ORDER BY dfc.descriTarjeta")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_tarjeta_cab dfc WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCab insertarObtenerObjeto(DescuentoTarjetaCab dfc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(dfc);
            em.getTransaction().commit();
            em.refresh(dfc);
            return dfc;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescuentoTarjetaCab> listarFETCHTarjeta(long limRow,
            long offSet, String nombre, String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            List<DescuentoTarjetaCab> dfCab = new ArrayList<DescuentoTarjetaCab>();
            // Si la fecha de inicio es mayor a la del fin no realizara nada el
            // sistema
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                dfCab.add(null);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (LOWER(dfc.descriTarjeta) LIKE :descri) AND (to_date(to_char(dfc.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(dfc.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) order by dfc.descriTarjeta")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (LOWER(dfc.descriTarjeta) LIKE :descri) AND (to_date(to_char(dfc.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) order by dfc.descriTarjeta")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (LOWER(dfc.descriTarjeta) LIKE :descri) AND (to_date(to_char(dfc.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) order by dfc.descriTarjeta")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " ((to_date(to_char(dfc.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) >= :inicio) AND ((to_date(to_char(dfc.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) <= :fin) "
                                + " ORDER BY dfc.descriTarjeta")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (LOWER(dfc.descriTarjeta) LIKE :descri) ORDER BY dfc.descriTarjeta")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (to_date(to_char(dfc.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) order by dfc.descriTarjeta")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                dfCab = em
                        .createQuery(
                                "FROM DescuentoTarjetaCab dfc JOIN FETCH dfc.tarjeta tarj JOIN FETCH tarj.tipoTarjeta tipoTarj JOIN FETCH dfc.descuentoTarjetaDets dets JOIN FETCH dets.dia d WHERE dfc.estadoDesc=true AND"
                                + " (to_date(to_char(dfc.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) order by dfc.descriTarjeta")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                dfCab = em
                        .createQuery(
                                "SELECT d FROM DescuentoTarjetaCab d JOIN FETCH d.tarjeta tar JOIN FETCH tar.tipoTarjeta tiTarj JOIN FETCH d.descuentoTarjetaDets det JOIN FETCH det.dia WHERE d.estadoDesc=true order by d.descriTarjeta")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet).getResultList();
            }
            return dfCab;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountFiler(String nombre, String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            List<Long> row = new ArrayList<Long>();

            if (!fechaFin.equals("") && !fechaInicio.equals("")
                    && Utilidades.stringToUtilDate(fechaInicio).after(
                            Utilidades.stringToUtilDate(fechaFin))) {
                row.add(0L);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (LOWER(d.descriTarjeta) LIKE :nomTarjeta) AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)")
                        .setParameter("nomTarjeta", nombre.toLowerCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (LOWER(d.descriTarjeta) LIKE :nomTarjeta) AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)")
                        .setParameter("nomTarjeta", nombre.toLowerCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (LOWER(d.descriTarjeta) LIKE :nomTarjeta) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)")
                        .setParameter("nomTarjeta", nombre.toLowerCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (LOWER(d.descriTarjeta) LIKE :nomTarjeta)")
                        .setParameter("nomTarjeta", nombre.toLowerCase() + "%")
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true AND"
                        + " (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                row = em.createQuery(
                        "SELECT count(*) FROM DescuentoTarjetaCab d WHERE d.estadoDesc=true")
                        .getResultList();
            }
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescuentoTarjetaCab> listarPorFechaActual(Timestamp tiempoActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCab dfc WHERE dfc.estadoDesc=true and (to_date(to_char(dfc.fechaFin , 'DD-MON-YY'), 'DD-MON-YY') <:fechaActual)")
                    .setParameter("fechaActual",
                            Utilidades.stringToUtilDate(tiempoActual.toString()))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarPorFecha(DescuentoTarjetaCab desc, int i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoTarjetaCab> desc = em.createQuery(
                    "FROM DescuentoTarjetaCab d WHERE d.estadoDesc = true")
                    .getResultList();
            em.getTransaction().begin();
            for (DescuentoTarjetaCab descuento : desc) {
                descuento.setEstadoDesc(false);
                em.merge(descuento);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;

        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE DescuentoTarjetaCab d SET d.estadoDesc=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
//    @Override
//    public boolean bajasLocal(String u, Timestamp ts) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            List<DescuentoTarjetaCab> lista = em.createQuery(
//                    "FROM DescuentoTarjetaCab d WHERE d.estadoDesc = true")
//                    .getResultList();
//            for (DescuentoTarjetaCab desc : lista) {
//                desc.setEstadoDesc(false);
//                desc.setFechaMod(ts);
//                desc.setUsuMod(u);
//                try {
//                    em.getTransaction().begin();
//                    em.merge(desc);
//                    em.getTransaction().commit();
//                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    return false;
//                }
//            }
//            return true;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }

    @Override
    public boolean actualizarObtenerEstado(DescuentoTarjetaCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public boolean verificandoInsercion(String descripcion, Timestamp fechaInicio) {
        EntityManager em = null;
        boolean existe = false;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoTarjetaCab> listDescuentoTarjetaCabs = em.createQuery(
                    "FROM DescuentoTarjetaCab dfc WHERE dfc.estadoDesc=true "
                            + "and (to_date(to_char(dfc.fechaInicio , 'DD-MON-YY'), 'DD-MON-YY') =:fechaInicio) "
                            + "and upper(dfc.descriTarjeta) LIKE :descripcion")
                    .setParameter("fechaInicio",
                            Utilidades.stringToUtilDate(fechaInicio.toString()))
                    .setParameter("descripcion",
                            descripcion.toUpperCase())
                    .getResultList();
            if (!listDescuentoTarjetaCabs.isEmpty()) {
                existe = true;
            }
            return existe;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
