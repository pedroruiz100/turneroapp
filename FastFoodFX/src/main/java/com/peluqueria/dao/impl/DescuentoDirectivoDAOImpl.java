package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoDirectivo;
import com.peluqueria.dao.DescuentoDirectivoDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoDirectivoDAOImpl implements DescuentoDirectivoDAO {

    @Override
    public List<DescuentoDirectivo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoDirectivo dd ORDER BY dd.idDescuentoDirectivo")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoDirectivo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescuentoDirectivo) em
                    .createQuery(
                            "FROM DescuentoDirectivo dd WHERE dd.idDescuentoDirectivo=:idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoDirectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoDirectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
