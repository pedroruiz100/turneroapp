package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoCheque;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoChequeDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoChequeDAOImpl implements RangoChequeDAO {

    @Override
    public List<RangoCheque> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoCheque getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoCheque obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoCheque obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarObtenerRango(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoCheque rango = em.find(RangoCheque.class, idRango);
            r = rango.getRangoActual();
            try {
                rango.setRangoActual(r + 1);
                em.getTransaction().begin();
                em.merge(rango);
                em.getTransaction().commit();
                return r;
            } catch (Exception e) {
                System.out.println("--->> " + e.getLocalizedMessage());
                return r;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
