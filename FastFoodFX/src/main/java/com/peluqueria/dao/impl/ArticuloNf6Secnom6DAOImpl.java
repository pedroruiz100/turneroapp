package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf6Secnom6DAOImpl implements ArticuloNf6Secnom6DAO {

    @Override
    public List<ArticuloNf6Secnom6> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf6Secnom6 a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf6Secnom6 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf6Secnom6) em
                    .createQuery("FROM ArticuloNf6Secnom6 a WHERE a.idNf6Secnom6Articulo=:idNf6Secnom6Articulo")
                    .setParameter("idNf6Secnom6Articulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf6Secnom6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf6Secnom6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf6Secnom6 articuloNf6Secnom6 = em.find(ArticuloNf6Secnom6.class, id);
            em.remove(articuloNf6Secnom6);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf6Secnom6 getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf6Secnom6) em
                    .createQuery("FROM ArticuloNf6Secnom6 a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf6Secnom6 listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf6Secnom6) em
                    .createQuery("FROM ArticuloNf6Secnom6 a WHERE a.articulo.idArticulo=:idArt")
                    .setParameter("idArt", idArt).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
