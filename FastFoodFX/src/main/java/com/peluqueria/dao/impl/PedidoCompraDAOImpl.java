package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.dao.PedidoCompraDAO;
import static com.javafx.util.EMF.getEmf;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class PedidoCompraDAOImpl implements PedidoCompraDAO {

    @Override
    public List<PedidoCompra> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoCompra fcd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCompra getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(PedidoCompra.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(PedidoCompra facDet, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facDet);
            em.getTransaction().commit();
            if (i % 30 == 0) {
                em.flush();
                em.clear();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PedidoCompra obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PedidoCompra obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCompra> listarPorFechaRecepcionActual(LocalDate desde, LocalDate hasta, String idProv) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoCompra fcd WHERE fcd.recepcion.fechaDoc >=:fecDesde AND "
                    + "fcd.recepcion.fechaDoc <=:fecHasta and fcd.recepcion.proveedor.idProveedor=" + idProv + " ORDER BY fcd.sec2")
                    .setParameter("fecDesde", Date.from(desde.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='CASA CENTRAL'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='SAN LORENZO'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='CACIQUE'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoCompra> listarPorCabecera(long idPedCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoCompra pd WHERE pd.pedidoCab.idPedidoCab=:idPed")
                    .setParameter("idPed", idPedCab).getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean eliminarPorcabecera(Long idPedidoCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.createNativeQuery("DELETE FROM factura_cliente.pedido_det WHERE id_pedido_cab=" + idPedidoCab)
                    //                    .setParameter("idPed", idPedCab)
                    .getResultList();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoCompra getByIdPedidoCabAndCodArt(long idPedido, Long idArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PedidoCompra) em.createQuery("FROM PedidoCompra fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
                    + "fcd.articulo.idArticulo=:idArt")
                    .setParameter("idPed", idPedido)
                    .setParameter("idArt", idArticulo).getSingleResult();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
