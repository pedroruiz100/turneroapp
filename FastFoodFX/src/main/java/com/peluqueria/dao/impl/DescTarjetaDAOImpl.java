package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescTarjeta;
import com.peluqueria.dao.DescTarjetaDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.math.BigInteger;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescTarjetaDAOImpl implements DescTarjetaDAO {

    @Override
    public List<DescTarjeta> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescTarjeta dt").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescTarjeta getById(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void insertar(DescTarjeta obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescTarjeta obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescTarjeta desc = em.find(DescTarjeta.class, id);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescTarjeta> filtroFechaDescTarjeta(String fechaInicio, String fechaFin, String nroCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescTarjeta dt JOIN FETCH dt.facturaClienteCabTarjeta fccf JOIN FETCH fccf.facturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.fechaEmision")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescTarjeta> filtroFechaDescTarjeta(String fechaInicio, String fechaFin, String nroCaja, String idTarjeta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescTarjeta dt JOIN FETCH dt.facturaClienteCabTarjeta fccf JOIN FETCH fccf.tarjeta t JOIN FETCH fccf.facturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin and t.idTarjeta=:idTarj ORDER BY fcc.fechaEmision")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .setParameter("idTarj", idTarjeta).getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<BigInteger> recuperarEntidadTarjeta(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createNativeQuery(
                            "SELECT DISTINCT dtc.id_descuento_tarjeta_cab "
                            + "FROM factura_cliente.desc_tarjeta dt LEFT JOIN cuenta.descuento_tarjeta_cab dtc "
                            + "ON dt.id_descuento_tarjeta_cab=dtc.id_descuento_tarjeta_cab "
                            + "WHERE CAST(dtc.fecha_inicio as DATE)<='"
                            + Utilidades.stringToSqlDate(fechaInicio) + "'"
                            + " AND CAST(dtc.fecha_fin as DATE)>='"
                            + Utilidades.stringToSqlDate(fechaFin) + "'")
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
