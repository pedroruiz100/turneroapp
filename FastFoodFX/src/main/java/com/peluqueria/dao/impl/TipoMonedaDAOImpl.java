package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TipoMoneda;
import com.peluqueria.dao.TipoMonedaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TipoMonedaDAOImpl implements TipoMonedaDAO {

    @Override
    public List<TipoMoneda> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<TipoMoneda> ls = em.createQuery("FROM TipoMoneda tm ORDER BY tm.descripcion").getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TipoMoneda getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(TipoMoneda.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TipoMoneda obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TipoMoneda obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
