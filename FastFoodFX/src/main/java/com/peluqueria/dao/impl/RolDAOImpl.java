package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Rol;
import com.peluqueria.dao.RolDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class RolDAOImpl implements RolDAO {

    @Override
    public List<Rol> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Rol r").getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Rol getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Rol.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Rol obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Rol obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Rol insertarObtenerObjeto(Rol rol) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(rol);
            em.getTransaction().commit();
            em.refresh(rol);
            return rol;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Rol> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
//            String cadena = "SELECT count(1) FROM seguridad.rol r";
            return em.createQuery("FROM Rol rol ORDER BY rol.descripcion").setMaxResults((int) limRow)
                    .setFirstResult((int) offSet).getResultList();
//            String fila = String.valueOf(em.createNativeQuery(cadena).getSingleResult());
//            return Long.valueOf(fila);
        } catch (Exception ex) {
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Rol> listarFETCHFiltro(long limRow, long offSet, String nombre, boolean activo, boolean inactivo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (!inactivo && !activo) {// check Todos
                return em.createQuery("FROM Rol rol where (LOWER(rol.descripcion) LIKE :nombre) ORDER BY rol.descripcion")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").setMaxResults((int) limRow)
                        .setFirstResult((int) offSet).getResultList();
            } else {
                return em
                        .createQuery("From Rol rol "
                                + "where (LOWER(rol.descripcion) LIKE :nombre) and (rol.activo = :activo) ORDER BY rol.descripcion")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").setParameter("activo", activo)
                        .setMaxResults((int) limRow).setFirstResult((int) offSet).getResultList();
            }
//            String fila = String.valueOf(em.createNativeQuery(cadena).getSingleResult());
//            return Long.valueOf(fila);
        } catch (Exception ex) {
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM seguridad.rol r";
            String fila = String.valueOf(em.createNativeQuery(cadena).getSingleResult());
            return Long.valueOf(fila);
        } catch (Exception ex) {
            return 0L;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFiltro(String nombre, boolean activo, boolean inactivo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            List<Long> row = new ArrayList<Long>();
            if (!inactivo && !activo) {// check Todos
                row = em.createQuery("Select count(*) from Rol rol where (LOWER(rol.descripcion) LIKE :nombre)")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").getResultList();
            } else {
                row = em.createQuery(
                        "Select count(*) from Rol rol where (LOWER(rol.descripcion) LIKE :nombre) and (rol.activo = :activo)")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").setParameter("activo", activo)
                        .getResultList();

            }
            return row.get(0);
        } catch (Exception ex) {
            return 0L;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Rol> buscarRolUsuarioAsignado(long idUsuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery("FROM Rol R JOIN FETCH R.usuarioRols UR "
                            + "WHERE UR.usuario.idUsuario = :idUsuario ORDER BY R.descripcion")
                    .setParameter("idUsuario", idUsuario).getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Rol> buscarRolUsuarioNOAsignado(long idUsuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT R.* FROM seguridad.rol R WHERE R.descripcion "
                    + "NOT IN (select SR.descripcion FROM seguridad.rol SR JOIN seguridad.usuario_rol SUR ON SR.id_rol = SUR.id_rol "
                    + "JOIN seguridad.usuario SU ON SUR.id_usuario = SU.id_usuario WHERE SU.id_usuario = " + idUsuario
                    + " )";
            return em.createNativeQuery(cadena, Rol.class).getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
