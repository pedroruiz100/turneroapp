package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Usuario;
import com.peluqueria.dao.UsuarioDAO;
import com.javafx.util.CryptoBack;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class UsuarioDAOImpl implements UsuarioDAO {

    private Usuario usuario;

    @Override
    public List<Usuario> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Usuario> u = em.createQuery("FROM Usuario u").getResultList();
            return u;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Usuario getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Usuario.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Usuario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Usuario obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public Usuario buscarUsuario(String usuarioAcceso, String contrasenha)
            throws NoSuchAlgorithmException {
        return busUsu(usuarioAcceso, contrasenha);
    }

    private Usuario busUsu(String usuarioAcceso, String contrasenha)
            throws NoSuchAlgorithmException {
        boolean autEmail = false;
        String user = "";
        CryptoBack cb = new CryptoBack();
        for (int i = 0; i < usuarioAcceso.length(); i++) {
            if (usuarioAcceso.substring(i, i + 1).contentEquals("@")) {
                autEmail = true;
            }
        }
        if (!autEmail) {
            user = usuarioAcceso.toUpperCase();
        } else {
            user = usuarioAcceso;
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Usuario) em
                    .createQuery(
                            "FROM Usuario u "
                            + " where (u.nomUsuario = :usuarioAcceso or u.email = :usuarioAcceso)"
                            + " and (u.contrasenha = :contrasenha) and u.activo=true")
                    .setParameter("usuarioAcceso", user)
                    .setParameter("contrasenha", cb.getHash(contrasenha))
                    .getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarContrasenha(String contrasenha, long id, String usuMod, Timestamp fechaMod) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Usuario usuario = em.find(Usuario.class, id);
            usuario.setUsuMod(usuMod);
            usuario.setFechaMod(fechaMod);
            usuario.setContrasenha(contrasenha);
            usuario.setGenerico(false);
            em.merge(usuario);
            em.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            Utilidades.log.error("ERROR actualizarContrasenha: ", ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Usuario busUsuCI(String CI) {
        return busUsuLocalCI(CI);
    }

    private Usuario busUsuLocalCI(String CI) {
        EntityManager em = getEmf().createEntityManager();
        try {
            return (Usuario) em.createQuery("FROM Usuario u WHERE (u.funcionario.ci = :CI) and u.activo=true").setParameter("CI", CI).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public List<Usuario> listarFETCH(long limRow, long offSet) {
        EntityManager em = getEmf().createEntityManager();
        try {
            return em.createQuery("FROM Usuario usuario ORDER BY usuario.nomUsuario").setMaxResults((int) limRow)
                    .setFirstResult((int) offSet).getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public List<Usuario> listarFETCHFiltro(long limRow, long offSet, String nombre, String fCI, boolean activo, boolean inactivo) {
        EntityManager em = getEmf().createEntityManager();
        try {
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "%%";
            } else {
                nombre = "%" + nombre.toLowerCase() + "%";
            }
            if (fCI.equalsIgnoreCase("null")) {
                fCI = "%%";
            } else {
                fCI = fCI.toLowerCase() + "%";
            }
            if (!inactivo && !activo) {// check Todos
                return em
                        .createQuery(
                                "FROM Usuario usuario where (LOWER(usuario.nomUsuario) LIKE :nombre) and (usuario.funcionario.ci LIKE :fCI) ORDER BY usuario.nomUsuario")
                        .setParameter("nombre", nombre).setParameter("fCI", fCI).setMaxResults((int) limRow)
                        .setFirstResult((int) offSet).getResultList();
            } else {
                return em
                        .createQuery("From Usuario usuario "
                                + "where (LOWER(usuario.nomUsuario) LIKE :nombre) and (usuario.activo = :activo) and (usuario.funcionario.ci LIKE :fCI) ORDER BY usuario.nomUsuario")
                        .setParameter("nombre", nombre).setParameter("fCI", fCI).setParameter("activo", activo)
                        .setMaxResults((int) limRow).setFirstResult((int) offSet).getResultList();
            }
        } catch (Exception ex) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public long rowCount() {
        EntityManager em = getEmf().createEntityManager();
        try {
            String cadena = "SELECT count(1) FROM seguridad.usuario u";
            String fila = String.valueOf(em.createNativeQuery(cadena).getSingleResult());
            return Long.valueOf(fila);
        } catch (Exception ex) {
            return 0L;
        } finally {
            em.close();
        }
    }

    @Override
    public long rowCountFiltro(String nombre, String fCI, boolean activo, boolean inactivo) {
        EntityManager em = getEmf().createEntityManager();
        try {
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fCI.equalsIgnoreCase("null")) {
                fCI = "";
            }
            List<Long> row = new ArrayList<Long>();
            if (!inactivo && !activo) {// check Todos
                row = em.createQuery("Select count(*) from Usuario usuario where (LOWER(usuario.nomUsuario) LIKE :nombre)")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").getResultList();
            } else {
                row = em.createQuery(
                        "Select count(*) from Usuario usuario where (LOWER(usuario.nomUsuario) LIKE :nombre) and (usuario.activo = :activo)")
                        .setParameter("nombre", "%" + nombre.toLowerCase() + "%").setParameter("activo", activo)
                        .getResultList();

            }
            return row.get(0);
        } catch (Exception ex) {
            return 0L;
        } finally {
            em.close();
        }
    }

    @Override
    public Usuario restaurandoPass(long idUsuario, String pass, String uMod, Timestamp fMod) {
        EntityManager em = getEmf().createEntityManager();
        try {
            Usuario usuario = em.find(Usuario.class, idUsuario);
            usuario.setContrasenha(pass);
            usuario.setGenerico(true);
            usuario.setUsuMod(uMod);
            usuario.setFechaMod(fMod);
            em.getTransaction().begin();
            em.merge(usuario);
            em.getTransaction().commit();
            return usuario;
        } catch (Exception ex) {
            return null;
        } finally {
            em.close();
        }
    }

    @Override
    public Usuario actualizarObtenerObjeto(Usuario usuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(usuario);
            em.getTransaction().commit();
            return usuario;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Usuario insertarObtenerObjeto(Usuario usu) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(usu);
            em.refresh(usu);
            em.getTransaction().commit();
            return usu;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
