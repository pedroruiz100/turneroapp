package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.PromoTemporada;
import com.peluqueria.dao.PromoTemporadaDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

@Repository
public class PromoTemporadaDAOImpl implements PromoTemporadaDAO {

    @Override
    public List<PromoTemporada> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporada> list = em
                    .createQuery(
                            "FROM PromoTemporada pt WHERE pt.estadoPromo=true ORDER BY pt.descripcionTemporada")
                    .getResultList();
            return list;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PromoTemporada getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            PromoTemporada p = em.find(PromoTemporada.class, id);
            return p;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PromoTemporada obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PromoTemporada obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.promo_temporada pro WHERE estado_promo=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilter(String nombre, String fechaInicio,
            String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            long row = 0L;
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                row = 0L;
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND UPPER(descripcionTemporada) "
                        + "like :nombre AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND UPPER(descripcionTemporada) "
                        + "like :nombre AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND UPPER(descripcionTemporada) "
                        + "like :nombre AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else if (!nombre.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND UPPER(descripcionTemporada) "
                        + "like :nombre";
                row = (long) em.createQuery(cadena)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getSingleResult();

            } else if (!fechaInicio.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true AND "
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getSingleResult();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true "
                        + "AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)";
                row = (long) em
                        .createQuery(cadena)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getSingleResult();
            } else {
                String cadena = "SELECT count(*) FROM PromoTemporada pro WHERE estadoPromo=true";
                row = (long) em.createQuery(cadena).getSingleResult();
            }
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PromoTemporada> listarFETCH(long limite, long inicio) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PromoTemporada pro WHERE pro.estadoPromo=true ORDER BY pro.descripcionTemporada")
                    .setFirstResult((int) inicio).setMaxResults((int) limite).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<PromoTemporada> listarFETCHFilter(long limite, long inicio,
            String nombre, String fechaInicio, String fechaFin) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        if (fechaInicio.equalsIgnoreCase("null")) {
            fechaInicio = "";
        }
        if (fechaFin.equalsIgnoreCase("null")) {
            fechaFin = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporada> pro = new ArrayList<>();
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                pro.add(null);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporada) like :nombre AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporada) like :nombre AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporada) like :nombre "
                        + " AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND UPPER(pro.descripcionTemporada) like :nombre"
                        + " ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("nombre", nombre.toUpperCase() + "%")
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND "
                        + "(to_date(to_char(pro.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true AND (to_date(to_char(pro.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                pro = em.createQuery(
                        "FROM PromoTemporada pro  WHERE pro.estadoPromo=true ORDER BY pro.descripcionTemporada")
                        .setFirstResult((int) inicio)
                        .setMaxResults((int) limite).getResultList();

            }
            return pro;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PromoTemporada insertarObtenerObjeto(PromoTemporada promoTemporada) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(promoTemporada);
            em.getTransaction().commit();
            em.refresh(promoTemporada);
            return promoTemporada;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<PromoTemporada> listarPorFechaActual(Timestamp tiempoActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PromoTemporada pt WHERE pt.estadoPromo=true AND (to_date(to_char(pt.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <:fechaActual)")
                    .setParameter("fechaActual",
                            Utilidades.stringToUtilDate(tiempoActual.toString()))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarPorFecha(PromoTemporada pro, int numeroActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(pro);
            em.getTransaction().commit();
            em.clear();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void baja() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<PromoTemporada> promo = em.createQuery(
                    "FROM PromoTemporada pt WHERE pt.estadoPromo = true")
                    .getResultList();
            em.getTransaction().begin();
            for (PromoTemporada pt : promo) {
                pt.setEstadoPromo(false);
                em.merge(pt);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(PromoTemporada obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE PromoTemporada d SET d.estadoPromo=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoPromo=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean validacionInsercion(int nf, long idNf) {
        //mira que se puede retornar en cada "case"... 
        //pero como es considerado una mala práctica tener varias salidas... 
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            switch (nf) {
                case 1:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf1.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf1 as promoTemporadaNf1 "
                            + "WHERE promoTemporadaNf1.nf1Tipo.idNf1Tipo = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 2:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf2.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf2 as promoTemporadaNf2 "
                            + "WHERE promoTemporadaNf2.nf2Sfamilia.idNf2Sfamilia = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 3:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf3.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf3 as promoTemporadaNf3 "
                            + "WHERE promoTemporadaNf3.nf3Sseccion.idNf3Sseccion = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 4:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf4.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf4 as promoTemporadaNf4 "
                            + "WHERE promoTemporadaNf4.nf4Seccion1.idNf4Seccion1 = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 5:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf5.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf5 as promoTemporadaNf5 "
                            + "WHERE promoTemporadaNf5.nf5Seccion2.idNf5Seccion2 = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 6:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf6.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf6 as promoTemporadaNf6 "
                            + "WHERE promoTemporadaNf6.nf6Secnom6.idNf6Secnom6 = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 7:
                    if (em.createQuery(
                            "FROM PromoTemporada as promoTemporada "
                            + "WHERE promoTemporada.estadoPromo=true "
                            + "AND (promoTemporada.idTemporada IN "
                            + "(SELECT promoTemporadaNf7.promoTemporada.idTemporada "
                            + "FROM PromoTemporadaNf7 as promoTemporadaNf7 "
                            + "WHERE promoTemporadaNf7.nf7Secnom7.idNf7Secnom7 = :idNf)) "
                            + "ORDER BY promoTemporada.idTemporada")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                default:
                    break;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
