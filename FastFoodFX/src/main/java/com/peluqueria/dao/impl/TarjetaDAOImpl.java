package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.dao.TarjetaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TarjetaDAOImpl implements TarjetaDAO {

    @Override
    public Tarjeta getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Tarjeta.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Tarjeta> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Tarjeta> tarj = em.createQuery("FROM Tarjeta t ORDER BY t.descripcion").getResultList();
            return tarj;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Tarjeta obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Tarjeta obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Tarjeta> listarNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Tarjeta tar order by tar.familiaTarj.descripcion asc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
