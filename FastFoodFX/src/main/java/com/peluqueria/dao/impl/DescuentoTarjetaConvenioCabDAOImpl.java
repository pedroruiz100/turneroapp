package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoFielCab;
import com.peluqueria.core.domain.DescuentoTarjetaConvenioCab;
import java.sql.Timestamp;
import com.peluqueria.dao.DescuentoTarjetaConvenioCabDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaConvenioCabDAOImpl implements
        DescuentoTarjetaConvenioCabDAO {

    @Override
    public List<DescuentoTarjetaConvenioCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoTarjetaConvenioCab> dtc = em
                    .createQuery(
                            "FROM DescuentoTarjetaConvenioCab dfc WHERE dfc.estadoDesc=true ORDER BY dfc.descriTarjetaConvenio DESC")
                    .getResultList();
            return dtc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaConvenioCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoTarjetaConvenioCab.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaConvenioCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaConvenioCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescuentoFielCab d = em.find(DescuentoFielCab.class, id);
            em.remove(d);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescuentoTarjetaConvenioCab> listarFETCH(long limRow,
            long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaConvenioCab dfc  WHERE dfc.estadoDesc=true ORDER BY dfc.descriTarjetaConvenio")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_tarjeta_convenio_cab dfc WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaConvenioCab insertarObtenerObjeto(
            DescuentoTarjetaConvenioCab dfc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(dfc);
            em.getTransaction().commit();
            em.refresh(dfc);
            return dfc;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescuentoTarjetaConvenioCab> listarFETCHTarjeta(long limRow,
            long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaConvenioCab dfc  WHERE dfc.estadoDesc=true AND"
                            + " (LOWER(dfc.descriTarjetaConvenio) LIKE :descri) order by dfc.descriTarjetaConvenio")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountFiler(String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM DescuentoTarjetaConvenioCab d WHERE d.estadoDesc=true AND"
                    + " (LOWER(d.descriTarjetaConvenio) LIKE :nomTarjeta)")
                    .setParameter("nomTarjeta", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoTarjetaConvenioCab> desc = em.createQuery(
                    "FROM DescuentoTarjetaConvenioCab d WHERE d.estadoDesc = true")
                    .getResultList();
            em.getTransaction().begin();
            for (DescuentoTarjetaConvenioCab descuento : desc) {
                descuento.setEstadoDesc(false);
                em.merge(descuento);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE DescuentoTarjetaConvenioCab d SET d.estadoDesc=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
//    @Override
//    public boolean bajasLocal(String u, Timestamp ts) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            List<DescuentoTarjetaConvenioCab> lista = em.createQuery(
//                    "SELECT d FROM DescuentoTarjetaConvenioCab d WHERE d.estadoDesc=true")
//                    .getResultList();
//            for (DescuentoTarjetaConvenioCab desc : lista) {
//                desc.setEstadoDesc(false);
//                desc.setFechaMod(ts);
//                desc.setUsuMod(u);
//                try {
//                    em.getTransaction().begin();
//                    em.merge(desc);
//                    em.getTransaction().commit();
//                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    return false;
//                }
//            }
//            return true;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }

    @Override
    public boolean actualizarObtenerEstado(DescuentoTarjetaConvenioCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
