package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoPedido;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoPedidoDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoPedidoDAOImpl implements RangoPedidoDAO {

    @Override
    public boolean insertarObtenerEstado(RangoPedido rango) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarObtenerEstado(long idRango) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoPedido rango = em.find(RangoPedido.class, idRango);
            long r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return -1l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<RangoPedido> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoPedido getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoPedido obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoPedido obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String recuperarActual() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoPedido rango = em.find(RangoPedido.class, 1L);
            long r = rango.getRangoActual();
            return r + "";
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return -1l + "";
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
