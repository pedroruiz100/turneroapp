package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoTarjetaCabEntidad;
import com.peluqueria.dao.DescuentoTarjetaCabEntidadDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaCabEntidadDAOImpl implements DescuentoTarjetaCabEntidadDAO {

    @Override
    public List<DescuentoTarjetaCabEntidad> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCabEntidad dtcd ORDER BY dtcd.idDescuentoTarjetaCabEntidad DESC")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCabEntidad getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DescuentoTarjetaCabEntidad det = (DescuentoTarjetaCabEntidad) em.createQuery(
                    "FROM DescuentoTarjetaCabEntidad dtcd WHERE dtcd.idDescuentoTarjetaCabEntidad=:idDet ORDER BY dtcd.idDescuentoTarjetaCabEntidad DESC")
                    .setParameter("idDet", id)
                    .getSingleResult();
            return det;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCabEntidad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCabEntidad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<DescuentoTarjetaCabEntidad> listarLimitado(long limite, long inicio,
            String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaCabEntidad dtcd WHERE upper(dtcd.descuentoTarjetaCab.descriTarjeta) like :descriTar "
                            + "ORDER BY dtcd.idDescuentoTarjetaCabEntidad")
                    .setParameter("descriTar", nombre.toUpperCase() + "%")
                    .setFirstResult((int) inicio).setMaxResults((int) limite)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabEntidad);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.info("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

}
