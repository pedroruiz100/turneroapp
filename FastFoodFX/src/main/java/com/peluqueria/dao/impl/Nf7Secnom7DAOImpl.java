package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.dao.Nf7Secnom7DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf7Secnom7DAOImpl implements Nf7Secnom7DAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf7Secnom7> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf7Secnom7 nf7Secnom7 order by nf7Secnom7.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf7Secnom7 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf7Secnom7.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf7Secnom7 nf7Secnom7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf7Secnom7);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf7Secnom7 nf7Secnom7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf7Secnom7);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf7Secnom7> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf7Secnom7 nf7Secnom7 WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf7Secnom7.idNf7Secnom7=nf7Secnom7.idNf7Secnom7)"
                            + " AND nf7Secnom7.idNf7Secnom7 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf7Secnom7> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf7Secnom7 nf7Secnom7 WHERE NOT EXISTS(FROM FuncionarioNf7 funcionarioNf7 "
                            + "WHERE funcionarioNf7.estadoDesc=true AND funcionarioNf7.nf7Secnom7.idNf7Secnom7=nf7Secnom7.idNf7Secnom7) "
                            + "AND nf7Secnom7.idNf7Secnom7 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf7Secnom7> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf7Secnom7 nf7Secnom7 WHERE NOT EXISTS(FROM PromoTemporadaNf7 promoTemporadaNf7 WHERE"
                            + " promoTemporadaNf7.promoTemporada.estadoPromo=true AND promoTemporadaNf7.nf7Secnom7.idNf7Secnom7=nf7Secnom7.idNf7Secnom7 AND"
                            + " ((to_date(to_char(promoTemporadaNf7.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf7.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf7Secnom7.idNf7Secnom7 != 0 ORDER BY nf7Secnom7.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf7Secnom7> listarPorNf(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf7Secnom7 nf7Secnom7 WHERE " + "nf7Secnom7.nf6Secnom6.idNf6Secnom6=" + id
                    + " order by nf7Secnom7.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
