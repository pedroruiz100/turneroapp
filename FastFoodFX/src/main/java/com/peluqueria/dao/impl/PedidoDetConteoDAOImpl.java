package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.PedidoDetConteo;
import com.peluqueria.dao.PedidoDetConteoDAO;
import static com.javafx.util.EMF.getEmf;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class PedidoDetConteoDAOImpl implements PedidoDetConteoDAO {

    @Override
    public List<PedidoDetConteo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoDetConteo fcd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoDetConteo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(PedidoDetConteo.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(PedidoDetConteo facDet, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facDet);
            em.getTransaction().commit();
//            if (i % 30 == 0) {
//                em.flush();
//                em.clear();
//            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PedidoDetConteo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PedidoDetConteo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoDetConteo> listarPorFechaRecepcionActual(LocalDate desde, LocalDate hasta, String idProv) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.recepcion.fechaDoc >=:fecDesde AND "
                    + "fcd.recepcion.fechaDoc <=:fecHasta and fcd.recepcion.proveedor.idProveedor=" + idProv + " ORDER BY fcd.sec2")
                    .setParameter("fecDesde", Date.from(desde.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .setParameter("fecHasta", Date.from(hasta.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='CASA CENTRAL'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='SAN LORENZO'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String x = em.createNativeQuery("SELECT SUM(cantidad) FROM factura_cliente.factura_compra_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN stock.recepcion recep ON "
                    + "  fcc.id_recepcion=fcd.id_recepcion WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(recep.fecha_doc)>='" + desde.getYear() + "-" + desde.getMonth() + "-" + desde.getDayOfMonth()
                    + "' AND DATE(recep.fecha_doc)<='" + hasta.getYear() + "-" + hasta.getMonth() + "-" + hasta.getDayOfMonth() + "' AND sucursal='CACIQUE'").getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PedidoDetConteo> listarPorCabecera(long idPedCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PedidoDetConteo pd WHERE pd.pedidoCab.idPedidoCab=:idPed")
                    .setParameter("idPed", idPedCab).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean eliminarPorcabecera(Long idPedidoCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.createNativeQuery("DELETE FROM factura_cliente.pedido_det WHERE id_pedido_cab=" + idPedidoCab)
                    //                    .setParameter("idPed", idPedCab)
                    .getResultList();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean eliminarPorcabeceraPedidoDetConteo(long idPedidoCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.createNativeQuery("DELETE FROM factura_cliente.pedido_det_conteo WHERE id_pedido_cab=" + idPedidoCab)
                    //                    .setParameter("idPed", idPedCab)
                    .getResultList();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long recuperarTotal(Long idPedidoCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return Long.parseLong(em.createNativeQuery("SELECT SUM(precio) FROM factura_cliente.pedido_det WHERE id_pedido_cab=" + idPedidoCab)
                    //                    .setParameter("idPed", idPedCab)
                    .getSingleResult().toString());
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoDetConteo getByIdPedidoCabAndCodArt(long idPedido, Long idArticulo, String sucursal) {
        EntityManager em = null;
        boolean central = false;
        boolean cacique = false;
        boolean sanlo = false;
        try {
            em = getEmf().createEntityManager();
//            return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                    + "fcd.articulo.idArticulo=:idArt AND "
//                    + "UPPER(fcd.sucursal) LIKE :sucu")
//                    .setParameter("idPed", idPedido)
//                    .setParameter("idArt", idArticulo)
//                    .setParameter("sucu", sucursal.toUpperCase() + "%").getSingleResult();
            String suc = sucursal.substring(0, (sucursal.length() - 2));
            StringTokenizer sts = new StringTokenizer(suc, ".");
            String suc1 = "";
            try {
                suc1 = sts.nextElement().toString(); //1   
            } catch (Exception e) {
            } finally {
            }
            String suc2 = "";
            try {
                suc2 = sts.nextElement().toString(); //1   
            } catch (Exception e) {
            } finally {
            }
            String suc3 = "";
            try {
                suc3 = sts.nextElement().toString(); //1   
            } catch (Exception e) {
            } finally {
            }
            suc1 = suc1.trim();
            suc2 = suc2.trim();
            suc3 = suc3.trim();

            if (suc1.equalsIgnoreCase("CENTRAL")) {
                central = true;
            } else if (suc1.equalsIgnoreCase("SAN LORENZO")) {
                sanlo = true;
            } else if (suc1.equalsIgnoreCase("CACIQUE")) {
                cacique = true;
            }

            if (suc3.equalsIgnoreCase("CENTRAL")) {
                if (!central) {
                    central = true;
                }
            } else if (suc3.equalsIgnoreCase("SAN LORENZO")) {
                if (!sanlo) {
                    sanlo = true;
                }
            } else if (suc3.equalsIgnoreCase("CACIQUE")) {
                if (!cacique) {
                    cacique = true;
                }
            }

            if (suc2.equalsIgnoreCase("CENTRAL")) {
                if (!central) {
                    central = true;
                }
            } else if (suc2.equalsIgnoreCase("SAN LORENZO")) {
                if (!sanlo) {
                    sanlo = true;
                }
            } else if (suc2.equalsIgnoreCase("CACIQUE")) {
                if (!cacique) {
                    cacique = true;
                }
            }

            return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
                    + "fcd.articulo.idArticulo=:idArt AND "
                    + "fcd.cc=" + central + " AND fcd.sc=" + cacique + " AND fcd.sl=" + sanlo
            ).setParameter("idPed", idPedido)
                    .setParameter("idArt", idArticulo)
                    .getSingleResult();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            try {
                PedidoDetConteo pdc = (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
                        + "fcd.articulo.idArticulo=:idArt AND "
                        + "(fcd.cc=" + central + " OR fcd.sc=" + cacique + " OR fcd.sl=" + sanlo + ")"
                ).setParameter("idPed", idPedido)
                        .setParameter("idArt", idArticulo)
                        .setMaxResults(1)
                        .getSingleResult();

                em = getEmf().createEntityManager();
                String sql = "SELECT SUM(CAST(pdc.cantidad as bigint)) FROM factura_cliente.pedido_det_conteo pdc WHERE pdc.id_pedido_cab=" + idPedido + " AND "
                        + "(cc=" + central + " OR sc=" + cacique + " OR sl=" + sanlo + ") and id_articulo=" + idArticulo;
                long cant = Long.parseLong(em.createNativeQuery(sql)
                        //                    .setParameter("idPed", idPedCab)
                        .getSingleResult().toString());

                pdc.setCantidad(cant + "");
                return pdc;
            } catch (Exception e) {
                System.out.println("EX -->> " + e.getLocalizedMessage());
                System.out.println("EX -->> " + e.getMessage());
                System.out.println("EX -->> " + e.fillInStackTrace());
                return null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
    }

    @Override
    public boolean eliminarPorcabeceraPedidoDetConteoSucursal(long idPedidoCab, String sucursal) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();a
//            String sql = "DELETE FROM factura_cliente.pedido_det_conteo WHERE id_pedido_cab=" + idPedidoCab + " AND "
//                    + "upper(sucursal)='" + sucursal.toUpperCase() + "'";
//            System.out.println("SQL -->> " + sql);
//            em.createNativeQuery(sql).getResultList();
//
//            return true;
//        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.getMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
//            return false;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            String sql = "DELETE FROM factura_cliente.pedido_det_conteo WHERE id_pedido_cab=" + idPedidoCab + " AND "
                    + "upper(sucursal)='" + sucursal.toUpperCase() + "'";
            System.out.println("SQL -->> " + sql);
            em.createNativeQuery(sql).getResultList();
            em.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PedidoDetConteo getByIdPedidoCabAndCodArt(long idPedido, Long idArticulo, String suc1, String suc2, String suc3) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            if (!suc1.equals("") && !suc2.equals("") && !suc3.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu1 OR UPPER(fcd.sucursal) LIKE :sucu2 OR UPPER(fcd.sucursal) LIKE :sucu3)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu1", suc1.toUpperCase() + "%")
//                        .setParameter("sucu2", suc2.toUpperCase() + "%")
//                        .setParameter("sucu3", suc3.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc1.equals("") && !suc2.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu1 OR UPPER(fcd.sucursal) LIKE :sucu2)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu1", suc1.toUpperCase() + "%")
//                        .setParameter("sucu2", suc2.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc1.equals("") && !suc3.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu1 OR UPPER(fcd.sucursal) LIKE :sucu3)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu1", suc1.toUpperCase() + "%")
//                        .setParameter("sucu3", suc3.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc2.equals("") && !suc3.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu2 OR UPPER(fcd.sucursal) LIKE :sucu3)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu2", suc2.toUpperCase() + "%")
//                        .setParameter("sucu3", suc3.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc1.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu1)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu1", suc1.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc2.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu2)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu2", suc2.toUpperCase() + "%")
//                        .getSingleResult();
//            } else if (!suc3.equals("")) {
//                return (PedidoDetConteo) em.createQuery("FROM PedidoDetConteo fcd WHERE fcd.pedidoCab.idPedidoCab=:idPed AND "
//                        + "fcd.articulo.idArticulo=:idArt AND "
//                        + "(UPPER(fcd.sucursal) LIKE :sucu3)")
//                        .setParameter("idPed", idPedido)
//                        .setParameter("idArt", idArticulo)
//                        .setParameter("sucu3", suc3.toUpperCase() + "%")
//                        .getSingleResult();
//            }
//        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.getMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
        return null;
    }

    @Override
    public void actualizacionMasiva(PedidoDetConteo pdc, int i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(pdc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
