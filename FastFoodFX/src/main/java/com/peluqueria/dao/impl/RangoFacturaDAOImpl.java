package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoFactura;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoFacturaDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoFacturaDAOImpl implements RangoFacturaDAO {

    @Override
    public List<RangoFactura> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<RangoFactura> ls = em.createQuery("FROM RangoFactura r").getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RangoFactura getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            RangoFactura rf = em.find(RangoFactura.class, id);
            return rf;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(RangoFactura obj) {
    }

    @Override
    public void actualizar(RangoFactura obj) {
    }

    @Override
    public void eliminar(long id) {
    }

    @Override
    public boolean insertarObtenerEstado(RangoFactura rango) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarObtenerEstado(long idRango) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoFactura rango = em.find(RangoFactura.class, idRango);
            rango.setRangoActual(rango.getRangoActual() + 1);
            em.persist(rango);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        long num = 0;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoFactura rango = em.find(RangoFactura.class, idRango);
            num = rango.getRangoActual();
            rango.setRangoActual(num + 1);
            em.persist(rango);
            em.getTransaction().commit();
            return num;
        } catch (Exception e) {
            return num;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
