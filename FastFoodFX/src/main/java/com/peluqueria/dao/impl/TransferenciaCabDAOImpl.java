package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TransferenciaCab;
import com.peluqueria.dao.TransferenciaCabDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TransferenciaCabDAOImpl implements TransferenciaCabDAO {

//    @Override
//    public List<TransferenciaCab> listar() {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em.createQuery("FROM TransferenciaCab fcc").getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//
//    @Override
//    public TransferenciaCab getById(long id) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em.find(TransferenciaCab.class, id);
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public void insertar(TransferenciaCab obj) {
//        
//    }
//    @Override
//    public void actualizar(TransferenciaCab obj) {
//        
//    }
    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            TransferenciaCab fac = em.find(TransferenciaCab.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public FacturaClienteCab insertarObtenerObjeto(
//            TransferenciaCab facturaClienteCab) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            em.persist(facturaClienteCab);
//            em.getTransaction().commit();
//            return facturaClienteCab;
//        } catch (Exception e) {
////            Utilidades.log.error("Exception", e.fillInStackTrace());
//            System.out.println("-->> " + e.getLocalizedMessage());
//            em = getEmf().createEntityManager();
//            TransferenciaCab fac = em.find(FacturaClienteCab.class, TransferenciaCab.getIdFacturaClienteCab());
//            if (fac.getIdFacturaClienteCab() != null) {
//                return fac;
//            } else {
//                Utilidades.log.error("ERROR: ", e.fillInStackTrace());
//                return null;
//            }
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public void cancelarFactura(long id, String usuario) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            java.util.Date date = new java.util.Date();
//            Timestamp tiempoActual = new Timestamp(date.getTime());
//            EstadoFactura ef = new EstadoFactura();
//            ef.setIdEstadoFactura(2L);
//            FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
//            fac.setFechaMod(tiempoActual);
//            fac.setEstadoFactura(ef);
//            fac.setUsuMod(usuario);
//            em.merge(fac);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public void actualizarMontoVenta(int monto, long id) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
//            fac.setMontoFactura(monto);
//            em.merge(fac);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public FacturaClienteCab actualizarObtenerObjeto(
//            FacturaClienteCab facturaClienteCab) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            em.merge(facturaClienteCab);
//            em.getTransaction().commit();
//            return facturaClienteCab;
//        } catch (Exception ex) {
//            return null;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public TransferenciaCab insertarObtenerObjeto(TransferenciaCab facturaClienteCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCab);
            em.getTransaction().commit();
            return facturaClienteCab;
        } catch (Exception e) {
//            Utilidades.log.error("Exception", e.fillInStackTrace());
            System.out.println("-->> " + e.getLocalizedMessage());
            em = getEmf().createEntityManager();
            TransferenciaCab fac = em.find(TransferenciaCab.class, facturaClienteCab.getIdTransferenciaCab());
            if (fac.getIdTransferenciaCab() != null) {
                return fac;
            } else {
                Utilidades.log.error("ERROR: ", e.fillInStackTrace());
                return null;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TransferenciaCab actualizarObtenerObjeto(TransferenciaCab transCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(transCab);
            em.getTransaction().commit();
            return transCab;
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TransferenciaCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TransferenciaCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<TransferenciaCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM TransferenciaCab fcc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TransferenciaCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(TransferenciaCab.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
