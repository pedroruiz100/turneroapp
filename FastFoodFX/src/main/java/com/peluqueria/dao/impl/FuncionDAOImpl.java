package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Funcion;
import com.peluqueria.dao.FuncionDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class FuncionDAOImpl implements FuncionDAO {

    @Override
    public List<Funcion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Funcion f").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Funcion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Funcion.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Funcion getByIdFuncion(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Funcion) em.createQuery("FROM Funcion f JOIN FETCH f.modulo m WHERE f.idFuncion=:idFun")
                    .setParameter("idFun", id).getSingleResult();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new Funcion();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Funcion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Funcion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
    }

    @Override
    public List<Funcion> buscarFuncion(String cadenaIN) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "select F.* from seguridad.funcion F where descripcion IN " + cadenaIN;
            return em.createNativeQuery(cadena, Funcion.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<String> buscarRolFuncionAsignado(long idModulo, long idRol) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Funcion> funcionList = em
                    .createQuery(
                            "Select F FROM Funcion F JOIN FETCH F.rolFuncions RF JOIN FETCH RF.rol R JOIN FETCH F.modulo M "
                            + "WHERE (M.idModulo = :idModulo and R.idRol = :idRol) ORDER BY F.descripcion")
                    .setParameter("idModulo", idModulo).setParameter("idRol", idRol).getResultList();
            List<String> listFuncionesAsignadas = new ArrayList<String>();
            if (!funcionList.isEmpty()) {
                for (int i = 0; i < funcionList.size(); i++) {
                    listFuncionesAsignadas.add(funcionList.get(i).getDescripcion());
                }
            }
            return listFuncionesAsignadas;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<String> buscarRolFuncionNoAsignado(long idModulo, long idRol) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<String> listFuncionesNoAsignadas = new ArrayList<String>();
            if (idModulo != -1) {
                String cadena = "SELECT F.* FROM seguridad.funcion F WHERE F.id_modulo = " + idModulo + " and F.id_funcion "
                        + "NOT IN (select SF.id_funcion FROM seguridad.funcion SF JOIN seguridad.rol_funcion SRF ON SF.id_funcion = SRF.id_funcion "
                        + "JOIN seguridad.rol SR ON SRF.id_rol = SR.id_rol WHERE SR.id_rol = " + idRol
                        + " ) ORDER BY F.descripcion";
                List<Funcion> funcionList = em.createNativeQuery(cadena, Funcion.class).getResultList();
                if (!funcionList.isEmpty()) {
                    for (int i = 0; i < funcionList.size(); i++) {
                        listFuncionesNoAsignadas.add(funcionList.get(i).getDescripcion());
                    }
                }
            }
            return listFuncionesNoAsignadas;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
