package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TransferenciaDet;
import com.peluqueria.dao.TransferenciaDetDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TransferenciaDetDAOImpl implements TransferenciaDetDAO {

    @Override
    public List<TransferenciaDet> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM TransferenciaDet fcd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TransferenciaDet getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(TransferenciaDet.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TransferenciaDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TransferenciaDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(TransferenciaDet facDet, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facDet);
            em.getTransaction().commit();
            if (i % 30 == 0) {
                em.flush();
                em.clear();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

}
