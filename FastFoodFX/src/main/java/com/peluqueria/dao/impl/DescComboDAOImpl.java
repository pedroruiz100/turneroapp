package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescCombo;
import com.peluqueria.dao.DescComboDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescComboDAOImpl implements DescComboDAO {

    @Override
    public List<DescCombo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescCombo dc ORDER BY dc.idDescCombo")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescCombo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescCombo) em
                    .createQuery("FROM DescCombo dc WHERE dc.idDescCombo=:idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescCombo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescCombo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescCombo desc = em.find(DescCombo.class, id);
            em.remove(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
