/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Cuponera;
import com.peluqueria.dao.CuponeraDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class CuponeraDAOImpl implements CuponeraDAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Cuponera> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM Cuponera cuponera WHERE cuponera.activo=true ORDER BY cuponera.nombre")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cuponera getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Cuponera cuponera = em.find(Cuponera.class, id);
            return cuponera;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Cuponera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Cuponera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Cuponera> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Cuponera cuponera "
                            + " WHERE cuponera.activo=true ORDER BY cuponera.nombre")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuponera.cuponera cuponera WHERE cuponera.activo=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Cuponera insertarObtenerObjeto(Cuponera cuponera) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(cuponera);
            em.getTransaction().commit();
            em.refresh(cuponera);
            return cuponera;
        } catch (Exception e) {
            Utilidades.log.error("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Cuponera> listarFETCHCuponera(long limRow,
            long offSet, String nombre, String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            List<Cuponera> cuponeras = new ArrayList<>();
            // Si la fecha de inicio es mayor a la del fin no realizara nada el
            // sistema
            if (!fechaFin.equals("") && !fechaInicio.equals("") && Utilidades.stringToUtilDate(fechaInicio).after(
                    Utilidades.stringToUtilDate(fechaFin))) {
                cuponeras.add(null);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (LOWER(cuponera.nombre) LIKE :descri) AND (to_date(to_char(cuponera.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(cuponera.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin) order by cuponera.nombre")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (LOWER(cuponera.nombre) LIKE :descri) AND (to_date(to_char(cuponera.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) order by cuponera.nombre")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (LOWER(cuponera.nombre) LIKE :descri) AND (to_date(to_char(cuponera.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) order by cuponera.nombre")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " ((to_date(to_char(cuponera.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) >= :inicio) AND ((to_date(to_char(cuponera.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) <= :fin) "
                                + " ORDER BY cuponera.nombre")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (LOWER(cuponera.nombre) LIKE :descri) ORDER BY cuponera.nombre")
                        .setParameter("descri", nombre.toLowerCase() + "%")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (to_date(to_char(cuponera.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio) order by cuponera.nombre")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        // OFFSET "+offSet
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                cuponeras = em
                        .createQuery(
                                "FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                                + " (to_date(to_char(cuponera.fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin) order by cuponera.nombre")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet)
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                cuponeras = em
                        .createQuery(
                                "SELECT cuponera FROM Cuponera cuponera WHERE cuponera.activo=true order by cuponera.nombre")
                        .setMaxResults((int) limRow)
                        // LIMIT "+limRow+"
                        .setFirstResult((int) offSet).getResultList();
            }
            return cuponeras;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFiler(String nombre, String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            if (fechaInicio.equalsIgnoreCase("null")) {
                fechaInicio = "";
            }
            if (fechaFin.equalsIgnoreCase("null")) {
                fechaFin = "";
            }
            List<Long> row = new ArrayList<>();

            if (!fechaFin.equals("") && !fechaInicio.equals("")
                    && Utilidades.stringToUtilDate(fechaInicio).after(
                            Utilidades.stringToUtilDate(fechaFin))) {
                row.add(0L);
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera d WHERE d.activo=true AND"
                        + " (LOWER(d.nombre) LIKE :nombre) AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)")
                        .setParameter("nombre", nombre.toLowerCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (LOWER(cuponera.nombre) LIKE :nombre) AND (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)")
                        .setParameter("nombre", nombre.toLowerCase() + "%")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")
                    && !fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (LOWER(cuponera.nombre) LIKE :nombre) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)")
                        .setParameter("nombre", nombre.toLowerCase() + "%")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")
                    && !fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') >=:inicio) AND (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') <=:fin)")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else if (!nombre.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (LOWER(cuponera.nombre) LIKE :nombre)")
                        .setParameter("nombre", nombre.toLowerCase() + "%")
                        .getResultList();
            } else if (!fechaInicio.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (to_date(to_char(fechaInicio, 'DD-MON-YY'), 'DD-MON-YY') =:inicio)")
                        .setParameter("inicio",
                                Utilidades.stringToUtilDate(fechaInicio))
                        .getResultList();
            } else if (!fechaFin.equalsIgnoreCase("")) {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true AND"
                        + " (to_date(to_char(fechaFin, 'DD-MON-YY'), 'DD-MON-YY') =:fin)")
                        .setParameter("fin",
                                Utilidades.stringToUtilDate(fechaFin))
                        .getResultList();
            } else {
                row = em.createQuery(
                        "SELECT count(*) FROM Cuponera cuponera WHERE cuponera.activo=true")
                        .getResultList();
            }
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Cuponera> listarPorFechaActual(Timestamp tiempoActual) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Cuponera cuponera WHERE cuponera.activo=true and (to_date(to_char(cuponera.fechaFin , 'DD-MON-YY'), 'DD-MON-YY') <:fechaActual)")
                    .setParameter("fechaActual",
                            Utilidades.stringToUtilDate(tiempoActual.toString()))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarPorFecha(Cuponera desc, int i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            @SuppressWarnings("JPQLValidation")
            List<Cuponera> cuponeras = em.createQuery(
                    "FROM Cuponera cuponera WHERE cuponera.activo = true")
                    .getResultList();
            em.getTransaction().begin();
            for (Cuponera cuponera : cuponeras) {
                cuponera.setActivo(false);
                em.merge(cuponera);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;

        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE Cuponera cuponera SET cuponera.activo=false, cuponera.usuMod=:uMod, cuponera.fechaMod=:fMod WHERE cuponera.activo=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(Cuponera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("-->> " + e.getLocalizedMessage());
            return false;
        }
    }

    @Override
    public boolean verificandoInsercion(String descripcion, Timestamp fechaInicio) {
        EntityManager em = null;
        boolean existe = false;
        try {
            em = getEmf().createEntityManager();
            @SuppressWarnings("JPQLValidation")
            List<Cuponera> listCuponeras = em.createQuery(
                    "FROM Cuponera cuponera WHERE cuponera.activo=true "
                    + "and (to_date(to_char(cuponera.fechaInicio , 'DD-MON-YY'), 'DD-MON-YY') =:fechaInicio) "
                    + "and upper(cuponera.nombre) LIKE :descripcion")
                    .setParameter("fechaInicio",
                            Utilidades.stringToUtilDate(fechaInicio.toString()))
                    .setParameter("descripcion",
                            descripcion.toUpperCase())
                    .getResultList();
            if (!listCuponeras.isEmpty()) {
                existe = true;
            }
            return existe;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean verificandoExistencia() {
        EntityManager em = null;
        boolean vacio = true;
        try {
            em = getEmf().createEntityManager();
            List<Cuponera> cuponeras = em.createQuery(
                    "FROM Cuponera cuponera WHERE cuponera.activo = true")
                    .getResultList();
            vacio = cuponeras.isEmpty();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return vacio;
    }

}
