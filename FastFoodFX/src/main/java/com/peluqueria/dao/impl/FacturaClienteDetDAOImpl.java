package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.FacturaClienteDet;
import com.peluqueria.dao.FacturaClienteDetDAO;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteDetDAOImpl implements FacturaClienteDetDAO {

    @Override
    public List<FacturaClienteDet> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteDet fcd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteDet getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FacturaClienteDet.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(FacturaClienteDet facDet, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facDet);
            em.getTransaction().commit();
            if (i % 30 == 0) {
                em.flush();
                em.clear();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public List<FacturaClienteDet> filtroFechaDescuentoFact(String nroFact, String fechaDesde, String fechaHasta, String nroCaja) {
        EntityManager em = null;
        try {
            if (nroFact.equalsIgnoreCase("null")) {
                nroFact = "%%";
            } else {
                nroFact = "%" + nroFact.toLowerCase() + "%";
            }
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin "
                            + "and fcc.nroFactura like :nroFact ORDER BY fcc.fechaEmision ")
                    .setParameter("fecIni", fechaDesde)
                    .setParameter("fecFin", fechaHasta)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .setParameter("nroFact", nroFact)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteDet> filtroFechaDescuento(String nroFact, String fechaDesde, String fechaHasta, String nroCaja) {
        EntityManager em = null;
        try {
            if (nroFact.equalsIgnoreCase("null")) {
                nroFact = "%%";
            } else {
                nroFact = "%" + nroFact.toLowerCase() + "%";
            }
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin "
                            + "and fcc.nroFactura like :nroFact ORDER BY fcc.fechaEmision ")
                    .setParameter("fecIni", fechaDesde)
                    .setParameter("fecFin", fechaHasta)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .setParameter("nroFact", nroFact)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteDet> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteDet> filtroFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin "
                            + " ORDER BY fcd.seccion")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
