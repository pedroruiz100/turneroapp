package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.EstadoFactura;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.dao.FacturaClienteCabDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabDAOImpl implements FacturaClienteCabDAO {

    @Override
    public List<FacturaClienteCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCab fcc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FacturaClienteCab.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCab insertarObtenerObjeto(
            FacturaClienteCab facturaClienteCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCab);
            em.getTransaction().commit();
            return facturaClienteCab;
        } catch (Exception e) {
//            Utilidades.log.error("Exception", e.fillInStackTrace());
            System.out.println("-->> " + e.getLocalizedMessage());
            em = getEmf().createEntityManager();
            FacturaClienteCab fac = em.find(FacturaClienteCab.class, facturaClienteCab.getIdFacturaClienteCab());
            if (fac.getIdFacturaClienteCab() != null) {
                return fac;
            } else {
                Utilidades.log.error("ERROR: ", e.fillInStackTrace());
                return null;
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void cancelarFactura(long id, String usuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            java.util.Date date = new java.util.Date();
            Timestamp tiempoActual = new Timestamp(date.getTime());
            EstadoFactura ef = new EstadoFactura();
            ef.setIdEstadoFactura(2L);
            FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
            fac.setFechaMod(tiempoActual);
            fac.setEstadoFactura(ef);
            fac.setUsuMod(usuario);
            em.merge(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizarMontoVenta(int monto, long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
            fac.setMontoFactura(monto);
            em.merge(fac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCab actualizarObtenerObjeto(
            FacturaClienteCab facturaClienteCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(facturaClienteCab);
            em.getTransaction().commit();
            return facturaClienteCab;
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public long getCantidadArticulo(String codArt, LocalDate desde, LocalDate hasta) {
//        
//    }
    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM factura_cliente.factura_cliente_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_cliente_cab fcc ON "
                    + "  fcc.id_factura_cliente_cab=fcd.id_factura_cliente_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(fcc.fecha_emision)>='" + desd
                    + "' AND DATE(fcc.fecha_emision)<='" + hast + "' AND id_sucursal=1";
            System.out.println("->> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM factura_cliente.factura_cliente_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_cliente_cab fcc ON "
                    + "  fcc.id_factura_cliente_cab=fcd.id_factura_cliente_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(fcc.fecha_emision)>='" + desd
                    + "' AND DATE(fcc.fecha_emision)<='" + hast + "' AND id_sucursal=3";
            System.out.println("-> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM factura_cliente.factura_cliente_det fcd LEFT JOIN stock.articulo a"
                    + "  ON a.id_articulo=fcd.id_articulo LEFT JOIN factura_cliente.factura_cliente_cab fcc ON "
                    + "  fcc.id_factura_cliente_cab=fcd.id_factura_cliente_cab WHERE a.cod_articulo='" + codArt + "'"
                    + "   AND DATE(fcc.fecha_emision)>='" + desd
                    + "' AND DATE(fcc.fecha_emision)<='" + hast + "' AND id_sucursal=2";
            System.out.println("-> " + sql);
            String x = (em.createNativeQuery(sql).getSingleResult() + "");
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCab> filtroFechaDescuento(String fechaInicio, String fechaFin, String nroCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.idFacturaClienteCab")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCab> filtroFecha(String fechaInicio, String fechaFin, String nroCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCab fcc JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCab> filtroNroFact(String nroFactura,
            String nroCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and fcc.nroFactura= :nroFact")
                    .setParameter("nroFact", nroFactura)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCab> filtroFechaDescuento(String nroFact, String fechaDesde, String fechaHasta, String nroCaja) {
        EntityManager em = null;
        if (nroFact.equalsIgnoreCase("null")) {
            nroFact = "%%";
        } else {
            nroFact = "%" + nroFact.toLowerCase() + "%";
        }
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery("FROM FacturaClienteCab fcc "
                            + "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and fcc.nroFactura like :numFact and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.idFacturaClienteCab")
                    .setParameter("fecIni", fechaDesde)
                    .setParameter("fecFin", fechaHasta)
                    .setParameter("numFact", nroFact)
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCab> filtroSolFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCab fcc JOIN FETCH fcc.caja c WHERE to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
    
    @Override
    public List<FacturaClienteCab> filtroFechaHoy() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            return em
                    .createQuery(
                            "FROM FacturaClienteCab fcc JOIN FETCH fcc.caja c WHERE to_char(fcc.fechaEmision,'YYYY-MM-DD') = :fecIni "
                            + "")
                    .setParameter("fecIni", (format.format(date)))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
