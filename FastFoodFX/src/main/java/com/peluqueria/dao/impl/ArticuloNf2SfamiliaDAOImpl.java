package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf2SfamiliaDAOImpl implements ArticuloNf2SfamiliaDAO {

    @Override
    public List<ArticuloNf2Sfamilia> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf2Sfamilia a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf2Sfamilia getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf2Sfamilia) em
                    .createQuery("FROM ArticuloNf2Sfamilia a WHERE a.idNf2SfamiliaArticulo=:idNf2SfamiliaArticulo")
                    .setParameter("idNf2SfamiliaArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf2Sfamilia obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf2Sfamilia obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf2Sfamilia articuloNf2Sfamilia = em.find(ArticuloNf2Sfamilia.class, id);
            em.remove(articuloNf2Sfamilia);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf2Sfamilia getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf2Sfamilia) em
                    .createQuery("FROM ArticuloNf2Sfamilia a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf2Sfamilia listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf2Sfamilia) em
                    .createQuery("FROM ArticuloNf2Sfamilia a WHERE a.articulo.idArticulo=:idNf2SfamiliaArticulo")
                    .setParameter("idNf2SfamiliaArticulo", idArt).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
