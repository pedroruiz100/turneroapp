package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.dao.CiudadDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class CiudadDAOImpl implements CiudadDAO {

    @Override
    public List<Ciudad> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Ciudad> listCiudad = em.createQuery("FROM Ciudad c WHERE c.activo=true ORDER BY c.descripcion").getResultList();
            return listCiudad;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Ciudad getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Ciudad.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Ciudad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Ciudad obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Ciudad cf = em.find(Ciudad.class, id);
            em.remove(cf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Ciudad> listarPorCiudad(long iddpto) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Ciudad> listCiudad = em.createQuery("FROM Ciudad c WHERE c.activo=true AND c.departamento.idDepartamento=:idDpto")
                    .setParameter("idDpto", iddpto)
                    .getResultList();
            return listCiudad;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
