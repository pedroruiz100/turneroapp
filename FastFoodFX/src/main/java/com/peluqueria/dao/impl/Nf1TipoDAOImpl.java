package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.dao.Nf1TipoDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf1TipoDAOImpl implements Nf1TipoDAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf1Tipo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf1Tipo nf1Tipo order by nf1Tipo.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf1Tipo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf1Tipo.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf1Tipo nf1Tipo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf1Tipo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf1Tipo nf1Tipo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf1Tipo);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    // lista solo nombre e id de la seccion
    // lista para el combo utilizado en el modulo de configuraciones para
    // descuento cliente fiel
    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf1Tipo> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf1Tipo nf1Tipo WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf1Tipo.idNf1Tipo=nf1Tipo.idNf1Tipo)"
                            + " AND nf1Tipo.idNf1Tipo != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf1Tipo> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf1Tipo nf1Tipo WHERE NOT EXISTS(FROM FuncionarioNf1 funcionarioNf1 "
                            + "WHERE funcionarioNf1.estadoDesc=true AND funcionarioNf1.nf1Tipo.idNf1Tipo=nf1Tipo.idNf1Tipo) "
                            + "AND nf1Tipo.idNf1Tipo != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf1Tipo> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf1Tipo nf1Tipo WHERE NOT EXISTS(FROM PromoTemporadaNf1 promoTemporadaNf1 WHERE"
                            + " promoTemporadaNf1.promoTemporada.estadoPromo=true AND promoTemporadaNf1.nf1Tipo.idNf1Tipo=nf1Tipo.idNf1Tipo AND"
                            + " ((to_date(to_char(promoTemporadaNf1.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf1.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf1Tipo.idNf1Tipo != 0 ORDER BY nf1Tipo.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
