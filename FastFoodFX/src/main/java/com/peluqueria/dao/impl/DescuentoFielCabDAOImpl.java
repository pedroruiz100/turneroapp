package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoFielCab;
import java.sql.Timestamp;
import com.peluqueria.dao.DescuentoFielCabDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoFielCabDAOImpl implements DescuentoFielCabDAO {

    @Override
    public List<DescuentoFielCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab dfc WHERE dfc.estadoDesc=true ORDER BY dfc.descriSeccion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoFielCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DescuentoFielCab desc = em.find(DescuentoFielCab.class, id);
            return desc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoFielCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoFielCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescuentoFielCab d = em.find(DescuentoFielCab.class, id);
            em.remove(d);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DescuentoFielCab> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoFielCab> list = em
                    .createQuery(
                            "FROM DescuentoFielCab dfc WHERE dfc.estadoDesc=true ORDER BY dfc.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
            return list;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab dfc WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoFielCab insertarObtenerObjeto(DescuentoFielCab dfc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(dfc);
            em.getTransaction().commit();
            return dfc;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFiler(String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            List<Long> row = new ArrayList<>();
            row = em.createQuery(
                    "SELECT count(*) FROM DescuentoFielCab d WHERE d.estadoDesc=true AND"
                    + " (LOWER(d.descriSeccion) LIKE :nomSeccion)")
                    .setParameter("nomSeccion", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFETCHSeccion(long limRow, long offSet,
            String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nombre.equalsIgnoreCase("null")) {
                nombre = "";
            }
            List<DescuentoFielCab> dfCab = new ArrayList<DescuentoFielCab>();
            dfCab = em
                    .createQuery(
                            "FROM DescuentoFielCab dfc WHERE dfc.estadoDesc=true AND"
                            + " (LOWER(dfc.descriSeccion) LIKE :descri) order by dfc.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
            return dfCab;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            List<DescuentoFielCab> lista = em.createQuery(
                    "SELECT d FROM DescuentoFielCab d WHERE d.estadoDesc=true")
                    .getResultList();
            for (DescuentoFielCab desc : lista) {
                desc.setEstadoDesc(false);
                em.merge(desc);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoFielCab dfc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(dfc);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE DescuentoFielCab d SET d.estadoDesc=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts).executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(DescuentoFielCab dfc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(dfc);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf1() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf1 descuentoFielNf1 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf1.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf2() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf2 descuentoFielNf2 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf2.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf3() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf3 descuentoFielNf3 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf3.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf4() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf4 descuentoFielNf4 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf4.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf5() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf5 descuentoFielNf5 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf5.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf6() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf6 descuentoFielNf6 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf6.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf7() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, descuento.descuento_fiel_nf7 descuentoFielNf7 "
                    + "WHERE descuentoFielCab.estado_desc=true and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf7.id_descuento_fiel_cab";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf1(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf1.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf1 as descuentoFielNf1)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf2(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf2.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf2 as descuentoFielNf2)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf3(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf3.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf3 as descuentoFielNf3)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf4(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf4.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf4 as descuentoFielNf4)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf5(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf5.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf5 as descuentoFielNf5)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf6(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf6.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf6 as descuentoFielNf6)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchNf7(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf7.descuentoFielCab.idDescuentoFielCab FROM DescuentoFielNf7 as descuentoFielNf7)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf1(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf1 descuentoFielNf1, "
                    + "stock.nf1_tipo nf1Tipo "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf1.id_descuento_fiel_cab "
                    + "and descuentoFielNf1.id_nf1_tipo = nf1Tipo.id_nf1_tipo "
                    + "and LOWER(nf1Tipo.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf2(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf2 descuentoFielNf2, "
                    + "stock.nf2_sfamilia nf2Sfamilia "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf2.id_descuento_fiel_cab "
                    + "and descuentoFielNf2.id_nf2_sfamilia = nf2Sfamilia.id_nf2_sfamilia "
                    + "and LOWER(nf2Sfamilia.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf3(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf3 descuentoFielNf3, "
                    + "stock.nf3_sseccion nf3Sseccion "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf3.id_descuento_fiel_cab "
                    + "and descuentoFielNf3.id_nf3_sseccion = nf3Sseccion.id_nf3_sseccion "
                    + "and LOWER(nf3Sseccion.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf4(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf4 descuentoFielNf4, "
                    + "stock.nf4_seccion1 nf4Seccion1 "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf4.id_descuento_fiel_cab "
                    + "and descuentoFielNf4.id_nf4_seccion1 = nf4Seccion1.id_nf4_seccion1 "
                    + "and LOWER(nf4Seccion1.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf5(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf5 descuentoFielNf5, "
                    + "stock.nf5_seccion2 nf5Seccion2 "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf5.id_descuento_fiel_cab "
                    + "and descuentoFielNf5.id_nf5_seccion2 = nf5Seccion2.id_nf5_seccion2 "
                    + "and LOWER(nf5Seccion2.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf6(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf6 descuentoFielNf6, "
                    + "stock.nf6_secnom6 nf6Secnom6 "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf6.id_descuento_fiel_cab "
                    + "and descuentoFielNf6.id_nf6_secnom6 = nf6Secnom6.id_nf6_secnom6 "
                    + "and LOWER(nf6Secnom6.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public Long rowCountFilterNf7(String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            String cadena = "SELECT count(1) FROM cuenta.descuento_fiel_cab descuentoFielCab, "
                    + "descuento.descuento_fiel_nf7 descuentoFielNf7, "
                    + "stock.nf7_secnom7 nf7Secnom7 "
                    + "WHERE descuentoFielCab.estado_desc=true "
                    + "and descuentoFielCab.id_descuento_fiel_cab = descuentoFielNf7.id_descuento_fiel_cab "
                    + "and descuentoFielNf7.id_nf7_secnom7 = nf7Secnom7.id_nf7_secnom7 "
                    + "and LOWER(nf7Secnom7.descripcion) LIKE '" + nivelFamilia.toLowerCase() + "%'";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf1(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf1.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf1 as descuentoFielNf1 "
                            + "WHERE LOWER(descuentoFielNf1.nf1Tipo.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf2(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf2.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf2 as descuentoFielNf2 "
                            + "WHERE LOWER(descuentoFielNf2.nf2Sfamilia.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf3(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf3.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf3 as descuentoFielNf3 "
                            + "WHERE LOWER(descuentoFielNf3.nf3Sseccion.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf4(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf4.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf4 as descuentoFielNf4 "
                            + "WHERE LOWER(descuentoFielNf4.nf4Seccion1.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf5(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf5.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf5 as descuentoFielNf5 "
                            + "WHERE LOWER(descuentoFielNf5.nf5Seccion2.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf6(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf6.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf6 as descuentoFielNf6 "
                            + "WHERE LOWER(descuentoFielNf6.nf6Secnom6.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<DescuentoFielCab> listarFetchFilterNf7(long limRow, long offSet, String nivelFamilia) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            if (nivelFamilia.equalsIgnoreCase("null")) {
                nivelFamilia = "";
            }
            return em
                    .createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf7.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf7 as descuentoFielNf7 "
                            + "WHERE LOWER(descuentoFielNf7.nf7Secnom7.descripcion) LIKE :nivelFamilia)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .setParameter("nivelFamilia", nivelFamilia.toLowerCase() + "%")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean validacionInsercion(int nf, long idNf) {
        //mira que se puede retornar en cada "case"... 
        //pero como es considerado una mala práctica tener varias salidas... 
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            switch (nf) {
                case 1:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf1.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf1 as descuentoFielNf1 "
                            + "WHERE descuentoFielNf1.nf1Tipo.idNf1Tipo = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 2:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf2.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf2 as descuentoFielNf2 "
                            + "WHERE descuentoFielNf2.nf2Sfamilia.idNf2Sfamilia = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 3:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf3.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf3 as descuentoFielNf3 "
                            + "WHERE descuentoFielNf3.nf3Sseccion.idNf3Sseccion = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 4:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf4.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf4 as descuentoFielNf4 "
                            + "WHERE descuentoFielNf4.nf4Seccion1.idNf4Seccion1 = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 5:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf5.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf5 as descuentoFielNf5 "
                            + "WHERE descuentoFielNf5.nf5Seccion2.idNf5Seccion2 = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 6:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf6.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf6 as descuentoFielNf6 "
                            + "WHERE descuentoFielNf6.nf6Secnom6.idNf6Secnom6 = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                case 7:
                    if (em.createQuery(
                            "FROM DescuentoFielCab as descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true "
                            + "AND (descuentoFielCab.idDescuentoFielCab IN "
                            + "(SELECT descuentoFielNf7.descuentoFielCab.idDescuentoFielCab "
                            + "FROM DescuentoFielNf7 as descuentoFielNf7 "
                            + "WHERE descuentoFielNf7.nf7Secnom7.idNf7Secnom7 = :idNf)) "
                            + "ORDER BY descuentoFielCab.idDescuentoFielCab")
                            .setParameter("idNf", idNf)
                            .getSingleResult() != null) {
                        existe = true;
                    }
                    break;
                default:
                    break;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
