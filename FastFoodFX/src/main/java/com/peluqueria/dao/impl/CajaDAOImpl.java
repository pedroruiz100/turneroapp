package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Caja;
import com.peluqueria.dao.CajaDAO;
import com.javafx.util.CryptoBack;
import static com.javafx.util.EMF.getEmf;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class CajaDAOImpl implements CajaDAO {

    @Override
    public List<Caja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Caja c ORDER BY c.nroCaja").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Caja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Caja) em.find(Caja.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Caja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Caja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            Caja ca = em.find(Caja.class, id);
            em.remove(ca);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Caja buscarCaja(Integer nroCaja, String clave)
            throws NoSuchAlgorithmException {
        CryptoBack cb = new CryptoBack();
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Caja) em
                    .createQuery(
                            "FROM Caja c "
                            + " where (c.nroCaja = :nro)"
                            + " and (c.claveCaja = :clave) and (c.activo = true)"
                            + "")
                    .setParameter("nro", nroCaja)
                    .setParameter("clave", cb.getHash(clave)).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
