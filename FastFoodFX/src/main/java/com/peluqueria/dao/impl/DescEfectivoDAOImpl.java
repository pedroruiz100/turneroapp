package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescEfectivo;
import com.peluqueria.dao.DescEfectivoDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescEfectivoDAOImpl implements DescEfectivoDAO {

    @Override
    public List<DescEfectivo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescEfectivo de").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescEfectivo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescEfectivo) em
                    .createQuery(
                            "FROM DescEfectivo de WHERE de.idDescEfectivo =:idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescEfectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescEfectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescEfectivo desc = em.find(DescEfectivo.class, id);
            em.remove(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
