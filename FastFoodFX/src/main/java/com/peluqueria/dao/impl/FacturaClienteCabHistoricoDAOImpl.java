package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import static com.javafx.util.EMF.getEmf;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabHistoricoDAOImpl implements
        FacturaClienteCabHistoricoDAO {

    @Override
    public List<FacturaClienteCabHistorico> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabHistorico f")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabHistorico getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabHistorico) em
                    .createQuery(
                            "FROM FacturaClienteCabHistorico f WHERE f.idFacturaClienteCabHistorico =:idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabHistorico obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabHistorico obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<FacturaClienteCabHistorico> listarPorFactura(Long idFacturaClienteCab) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
//            return em
//                    .createQuery(
//                            "FROM FacturaClienteCabHistorico f WHERE f.idFacturaClienteCabHistorico =:idFac")
//                    .setParameter("idFac", id)
//                    .getRe
            return em
                    .createQuery(
                            "FROM FacturaClienteCabHistorico f JOIN FETCH f.facturaClienteCab fac WHERE fac.idFacturaClienteCab =:idFac")
                    .setParameter("idFac", idFacturaClienteCab)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabHistorico consultar(String nroFactura, String tipoComprobante, String fechaEmision, String nroCaja, String timbrado) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            FacturaClienteCabHistorico fact = (FacturaClienteCabHistorico) em
                    .createQuery(
                            "FROM FacturaClienteCabHistorico factHistorico JOIN FETCH factHistorico.facturaClienteCab factCab JOIN FETCH factCab.tipoComprobante tc JOIN FETCH factCab.estadoFactura ef WHERE ef.idEstadoFactura=1 and factHistorico.nroCaja=:numCaja and "
                            + "factHistorico.nroTimbrado=:numTimbrado and to_char(factCab.fechaEmision,'yyyy-MM-dd')=:fechaEmi and factCab.nroFactura=:numFactu and tc.idTipoComprobante=:idTipoCom")
                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .setParameter("numTimbrado", timbrado)
                    .setParameter("fechaEmi",
                            fechaEmision)
                    .setParameter("numFactu", nroFactura)
                    .setParameter("idTipoCom", Long.parseLong(tipoComprobante))
                    .getSingleResult();
            return fact;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Integer> listarCajaDistinct() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Integer> listNroCaja = em
                    .createQuery(
                            "SELECT distinct nroCaja FROM FacturaClienteCabHistorico fac ORDER BY nroCaja")
                    .getResultList();
            return listNroCaja;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<String> listarTimbDistinct() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<String> listTimb = em
                    .createQuery(
                            "SELECT distinct nroTimbrado FROM FacturaClienteCabHistorico fac ORDER BY nroTimbrado")
                    .getResultList();
            return listTimb;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
