package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Seccion;
import com.peluqueria.dao.SeccionDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class SeccionDAOImpl implements SeccionDAO {

    @Override
    public List<Seccion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Seccion> s = em.createQuery("FROM Seccion s order by s.descripcion ASC")
                    .getResultList();
            return s;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Seccion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Seccion s = em.find(Seccion.class, id);
            return s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Seccion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Seccion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    // lista solo nombre e id de la seccion
    // lista para el combo utilizado en el modulo de configuraciones para
    // descuento cliente fiel
    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Seccion> listarNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Seccion s WHERE NOT EXISTS(FROM DescuentoFielCab descCab WHERE descCab.estadoDesc=true AND descCab.seccion.idSeccion=s.idSeccion)"
                            + " AND s.idSeccion != 0 AND estado=TRUE").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Seccion> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Seccion s WHERE NOT EXISTS(FROM SeccionFunc sf WHERE sf.estadoDesc=true AND sf.seccion.idSeccion=s.idSeccion)"
                            + " AND s.idSeccion != 0 AND estado=TRUE").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Seccion> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Seccion s WHERE NOT EXISTS(FROM SeccionPromoTemporada secProTemp WHERE"
                            + " secProTemp.promoTemporada.estadoPromo=true AND secProTemp.seccion.idSeccion=s.idSeccion AND"
                            + " ((to_date(to_char(secProTemp.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(secProTemp.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND s.idSeccion != 0 AND estado=TRUE ORDER BY s.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Seccion listarPorNombre(String descri) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Seccion s = (Seccion) em.createQuery("FROM Seccion s WHERE UPPER(s.descripcion) LIKE :descri AND estado=TRUE")
                    .setParameter("descri", descri.toUpperCase())
                    .getSingleResult();
            return s;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Seccion listarPorNombreAll(String descri) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Seccion s = (Seccion) em.createQuery("FROM Seccion s WHERE UPPER(s.descripcion) LIKE :descri")
                    .setParameter("descri", descri.toUpperCase())
                    .getSingleResult();
            return s;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Seccion> listarPorNombreLista(String descri) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Seccion> s = em.createQuery("FROM Seccion s WHERE UPPER(s.descripcion) LIKE :descri AND s.idSeccion!=0 ORDER BY s.idSeccion")
                    .setParameter("descri", "%" + descri.toUpperCase() + "%")
                    .getResultList();
            return s;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Seccion> listarTodosTRUE() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Seccion s WHERE estado=TRUE ORDER BY s.descripcion")
                    .getResultList();
//            return s;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
