package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.DetalleGasto;
import com.peluqueria.dao.DetalleGastoDAO;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DetalleGastoDAOImpl implements
        DetalleGastoDAO {

    @Override
    public List<DetalleGasto> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DetalleGasto f").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DetalleGasto getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DetalleGasto) em
                    .createQuery(
                            "FROM DetalleGasto fccc WHERE fccc.idDetalleGasto = :idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DetalleGasto obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DetalleGasto obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DetalleGasto fac = em
                    .find(DetalleGasto.class, id);
            em.remove(fac);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insercionMasiva(DetalleGasto fac, long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(fac);
            em.getTransaction().commit();
            if (id % 20 == 0) {
                em.flush();
                em.clear();
            }
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto fact JOIN FETCH fact.gastos fcc WHERE fcc.idGasto=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarTodosMayoresACero() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto fact JOIN FETCH fact.gastos fcc WHERE fact.cantidad>0 AND upper(fcc.tipo)=:tipo ORDER BY fcc.tipo")
                    .setParameter("tipo", "COMPRAS P/ PREPARACION")
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarPorProveedorMayorAcero(String prov) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto fact JOIN FETCH fact.gastos fcc WHERE fact.cantidad>0 AND upper(fcc.tipo)=:tipo AND upper(fcc.empresa)=:empre ORDER BY fcc.tipo")
                    .setParameter("tipo", "COMPRAS P/ PREPARACION")
                    .setParameter("empre", prov.toUpperCase())
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarPorFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto dg JOIN FETCH dg.gastos fcc WHERE to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.fecha")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarPorFechaParaVenta(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto dg JOIN FETCH dg.gastos fcc WHERE UPPER(fcc.tipo) LIKE 'COMPRAS P/ VENTA' AND to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.fecha")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<DetalleGasto> listarPorFechaParaPreparacion(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DetalleGasto dg JOIN FETCH dg.gastos fcc WHERE UPPER(fcc.tipo) LIKE 'COMPRAS P/ PREPARACION' AND to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.fecha")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean eliminarObtenerEstado(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DetalleGasto fac = em
                    .find(DetalleGasto.class, id);
            em.remove(fac);
            em.getTransaction().commit();
            return true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
