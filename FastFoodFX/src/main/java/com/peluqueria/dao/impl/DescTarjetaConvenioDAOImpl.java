package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescTarjetaConvenio;
import com.peluqueria.dao.DescTarjetaConvenioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescTarjetaConvenioDAOImpl implements DescTarjetaConvenioDAO {

    @Override
    public List<DescTarjetaConvenio> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescTarjetaConvenio dtc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescTarjetaConvenio getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescTarjetaConvenio) em
                    .createQuery(
                            "FROM DescTarjetaConvenio dtc WHERE dtc.idDescTarjetaConvenio = :idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescTarjetaConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescTarjetaConvenio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            DescTarjetaConvenio desc = em.find(DescTarjetaConvenio.class, id);
            em.remove(desc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
