package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoEfectivo;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoEfectivoDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class RangoEfectivoDAOImpl implements RangoEfectivoDAO {

    @Override
    public List<RangoEfectivo> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoEfectivo getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoEfectivo obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoEfectivo obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public long actualizarRecuperarRangoActual(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoEfectivo rango = em.find(RangoEfectivo.class, idRango);
            r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.getTransaction().begin();
            em.merge(rango);
            em.getTransaction().commit();
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return r;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
