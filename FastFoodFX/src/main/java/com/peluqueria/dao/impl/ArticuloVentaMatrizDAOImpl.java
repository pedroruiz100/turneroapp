package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ArticuloCompraMatriz;
import com.peluqueria.core.domain.ArticuloVentaMatriz;
import com.peluqueria.dao.ArticuloVentaMatrizDAO;
import static com.javafx.util.EMF.getEmf;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class ArticuloVentaMatrizDAOImpl implements ArticuloVentaMatrizDAO {

    @Override
    public List<ArticuloVentaMatriz> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<ArticuloVentaMatriz> listBarrio = em.createQuery("FROM ArticuloVentaMatriz b ORDER BY b.fecha").getResultList();
            return listBarrio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloVentaMatriz getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloVentaMatriz) em
                    .createQuery("Select b from ArticuloVentaMatriz b WHERE b.idArticuloVentaMatriz=:idBa")
                    .setParameter("idBa", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloVentaMatriz obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloVentaMatriz obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloVentaMatriz ba = em.find(ArticuloVentaMatriz.class, id);
            em.remove(ba);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM stock.articulo_venta_matriz fcd "
                    + "   WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND sucursal='CASA CENTRAL'";
            System.out.println("->> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM stock.articulo_venta_matriz fcd "
                    + "   WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND sucursal='CACIQUE'";
            System.out.println("->> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) FROM stock.articulo_venta_matriz fcd "
                    + "   WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND sucursal='SAN LORENZO'";
            System.out.println("->> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloVentaMatriz getByCodFecha(String cod, LocalDate nowLast, String sucursal) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloVentaMatriz) em
                    .createQuery("Select b from ArticuloVentaMatriz b WHERE b.codigo='" + cod + "' AND "
                            + "DATE(b.fecha)='" + nowLast + "' AND upper(b.sucursal)='" + sucursal + "'").getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
