package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.SeccionFunc;
import com.peluqueria.dao.SeccionFuncDAO;
import static com.javafx.util.EMF.getEmf;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class SeccionFuncDAOImpl implements SeccionFuncDAO {

    @Override
    public List<SeccionFunc> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<SeccionFunc> sf = em
                    .createQuery(
                            "FROM SeccionFunc sf WHERE sf.estadoDesc=true ORDER BY sf.descriSeccion")
                    .getResultList();
            return sf;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public SeccionFunc getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(SeccionFunc.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(SeccionFunc obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(SeccionFunc obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            SeccionFunc sf = em.find(SeccionFunc.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<SeccionFunc> listarFETCH(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM SeccionFunc sf  WHERE sf.estadoDesc=true ORDER BY sf.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<SeccionFunc> listarFETCHSeccion(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM SeccionFunc sf  WHERE sf.estadoDesc=true AND"
                            + " (LOWER(sf.descriSeccion) LIKE :descri) order by sf.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCount() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.seccion_func sf WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFiler(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM SeccionFunc sf WHERE sf.estadoDesc=true AND"
                    + " (LOWER(sf.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            List<SeccionFunc> sf = em.createQuery("FROM SeccionFunc sf WHERE sf.estadoDesc=true")
                    .getResultList();
            for (SeccionFunc s : sf) {
                s.setEstadoDesc(false);
                em.merge(s);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(SeccionFunc obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE SeccionFunc d SET d.estadoDesc=false, d.usuMod=:uMod, d.fechaMod=:fMod WHERE d.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public SeccionFunc insertarObtenerObjeto(SeccionFunc obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
