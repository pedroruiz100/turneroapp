package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.dao.Nf5Seccion2DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf5Seccion2DAOImpl implements Nf5Seccion2DAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf5Seccion2> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf5Seccion2 nf5Seccion2 order by nf5Seccion2.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf5Seccion2 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf5Seccion2.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf5Seccion2 nf5Seccion2) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf5Seccion2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf5Seccion2 nf5Seccion2) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf5Seccion2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf5Seccion2> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf5Seccion2 nf5Seccion2 WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf5Seccion2.idNf5Seccion2=nf5Seccion2.idNf5Seccion2)"
                            + " AND nf5Seccion2.idNf5Seccion2 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf5Seccion2> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf5Seccion2 nf5Seccion2 WHERE NOT EXISTS(FROM FuncionarioNf5 funcionarioNf5 "
                            + "WHERE funcionarioNf5.estadoDesc=true AND funcionarioNf5.nf5Seccion2.idNf5Seccion2=nf5Seccion2.idNf5Seccion2) "
                            + "AND nf5Seccion2.idNf5Seccion2 != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf5Seccion2> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf5Seccion2 nf5Seccion2 WHERE NOT EXISTS(FROM PromoTemporadaNf5 promoTemporadaNf5 WHERE"
                            + " promoTemporadaNf5.promoTemporada.estadoPromo=true AND promoTemporadaNf5.nf5Seccion2.idNf5Seccion2=nf5Seccion2.idNf5Seccion2 AND"
                            + " ((to_date(to_char(promoTemporadaNf5.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf5.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf5Seccion2.idNf5Seccion2 != 0 ORDER BY nf5Seccion2.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf5Seccion2> listarPorNf(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf5Seccion2 nf5Seccion2 WHERE " + "nf5Seccion2.nf4Seccion1.idNf4Seccion1=" + id
                    + " order by nf5Seccion2.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
