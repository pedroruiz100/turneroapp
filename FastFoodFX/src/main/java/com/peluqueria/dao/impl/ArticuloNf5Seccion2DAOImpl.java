package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf5Seccion2DAOImpl implements ArticuloNf5Seccion2DAO {

    @Override
    public List<ArticuloNf5Seccion2> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf5Seccion2 a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf5Seccion2 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf5Seccion2) em
                    .createQuery("FROM ArticuloNf5Seccion2 a WHERE a.idNf5Seccion2Articulo=:idNf5Seccion2Articulo")
                    .setParameter("idNf5Seccion2Articulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf5Seccion2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf5Seccion2 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf5Seccion2 articuloNf5Seccion2 = em.find(ArticuloNf5Seccion2.class, id);
            em.remove(articuloNf5Seccion2);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf5Seccion2 getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf5Seccion2) em
                    .createQuery("FROM ArticuloNf5Seccion2 a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf5Seccion2 listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf5Seccion2) em.createQuery("FROM ArticuloNf5Seccion2 a WHERE a.articulo.idArticulo=:idArt")
                    .setParameter("idArt", idArt).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
