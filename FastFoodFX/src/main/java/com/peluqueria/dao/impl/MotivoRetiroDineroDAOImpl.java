package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.MotivoRetiroDinero;
import com.peluqueria.dao.MotivoRetiroDineroDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class MotivoRetiroDineroDAOImpl implements MotivoRetiroDineroDAO {

    @Override
    public List<MotivoRetiroDinero> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM MotivoRetiroDinero mrd").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public MotivoRetiroDinero getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(MotivoRetiroDinero.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(MotivoRetiroDinero obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(MotivoRetiroDinero obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
