package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ServPendiente;
import com.peluqueria.dao.ServPendienteDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ServPendienteDAOImpl implements ServPendienteDAO {

    @Override
    public List<ServPendiente> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM ServPendiente sp ORDER BY sp.idServPendiente")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ServPendiente getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(ServPendiente.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ServPendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ServPendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ServPendiente sp = em.find(ServPendiente.class, id);
            em.remove(sp);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("->> " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR: ", e.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ServPendiente insertarObtenerObj(ServPendiente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ServPendiente> listarTodos() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM ServPendiente sp JOIN FETCH sp.clientePendiente cp ORDER BY cp.idClientePendiente")
                    .getResultList();
        } catch (Exception e) {
            System.out.println("->> " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR: ", e.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
