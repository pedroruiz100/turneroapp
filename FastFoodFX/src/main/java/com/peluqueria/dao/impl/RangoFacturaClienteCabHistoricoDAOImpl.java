/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoFacturaClienteCabHistorico;
import java.util.List;
import javax.persistence.EntityManager;
import com.peluqueria.dao.RangoFacturaClienteCabHistoricoDAO;
import static com.javafx.util.EMF.getEmf;

/**
 *
 * @author ExcelsisWalker
 */
public class RangoFacturaClienteCabHistoricoDAOImpl implements RangoFacturaClienteCabHistoricoDAO {

    @Override
    public List<RangoFacturaClienteCabHistorico> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<RangoFacturaClienteCabHistorico> ls = em.createQuery("FROM RangoFacturaClienteCabHistorico r").getResultList();
            return ls;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RangoFacturaClienteCabHistorico getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            RangoFacturaClienteCabHistorico rf = em.find(RangoFacturaClienteCabHistorico.class, id);
            return rf;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(RangoFacturaClienteCabHistorico obj) {

    }

    @Override
    public void actualizar(RangoFacturaClienteCabHistorico obj) {
    }

    @Override
    public void eliminar(long id) {
    }

    @Override
    public boolean insertarObtenerEstado(RangoFacturaClienteCabHistorico rango) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean actualizarObtenerEstado(long idRango) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoFacturaClienteCabHistorico rango = em.find(RangoFacturaClienteCabHistorico.class, idRango);
            rango.setRangoActual(rango.getRangoActual() + 1);
            em.persist(rango);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long actualizarObtenerRangoActual(long idRango) {
        EntityManager em = null;
        long num = 0;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            RangoFacturaClienteCabHistorico rango = em.find(RangoFacturaClienteCabHistorico.class, idRango);
            num = rango.getRangoActual();
            rango.setRangoActual(num + 1);
            em.persist(rango);
            em.getTransaction().commit();
            return num;
        } catch (Exception e) {
            return num;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
