package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf7;
import com.peluqueria.dao.DescuentoTarjetaCabNf7DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class DescuentoTarjetaCabNf7DAOImpl implements DescuentoTarjetaCabNf7DAO {

    @Override
    public List<DescuentoTarjetaCabNf7> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7 WHERE descuentoTarjetaCabNf7.descuentoTarjetaCab.estadoDesc=true "
                    + "ORDER BY idDescuentoTarjetaCabNf7").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaCabNf7 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoTarjetaCabNf7.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaCabNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaCabNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(DescuentoTarjetaCabNf7 sec, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(sec);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf7);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(descuentoTarjetaCabNf7);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            Utilidades.log.error("ERROR -->> insertarObtenerEstado(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7) " + e.fillInStackTrace());
            return false;
        }
    }

}
