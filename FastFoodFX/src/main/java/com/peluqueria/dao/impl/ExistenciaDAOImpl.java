package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Existencia;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.dao.ExistenciaDAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ExistenciaDAOImpl implements ExistenciaDAO {

//    @Override
//    @SuppressWarnings("JPQLValidation")
//    public List<Nf2Sfamilia> listar() {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em.createQuery("FROM Nf2Sfamilia nf2Sfamilia order by nf2Sfamilia.descripcion")
//                    .getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public Nf2Sfamilia getById(long id) {
//        
//    }
//    @Override
//    public void insertar(Nf2Sfamilia nf2Sfamilia) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            em.persist(nf2Sfamilia);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public void actualizar(Nf2Sfamilia nf2Sfamilia) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            em.merge(nf2Sfamilia);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

//    @Override
//    @SuppressWarnings("JPQLValidation")
//    public List<Nf2Sfamilia> listarNombreId() {
//        //FALTA MODIFICAR...
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em
//                    .createQuery(
//                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
//                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia)"
//                            + " AND nf2Sfamilia.idNf2Sfamilia != 0").getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//        //FALTA MODIFICAR...
//    }
//    @Override
//    @SuppressWarnings("JPQLValidation")
//    public List<Nf2Sfamilia> listarFuncionarioNombreId() {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em
//                    .createQuery(
//                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM FuncionarioNf2 funcionarioNf2 "
//                            + "WHERE funcionarioNf2.estadoDesc=true AND funcionarioNf2.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia) "
//                            + "AND nf2Sfamilia.idNf2Sfamilia != 0").getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    @SuppressWarnings("JPQLValidation")
//    public List<Nf2Sfamilia> listarPromoTemp(String inicio, String fin) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            // compara las fecha entre el fin de la promo temporada y la fecha
//            // actual
//            // la fecha actual es la que actualmente se obtiene del sistema
//            // operativo
//            return em
//                    .createQuery(
//                            "FROM Nf2Sfamilia nf2Sfamilia WHERE NOT EXISTS(FROM PromoTemporadaNf2 promoTemporadaNf2 WHERE"
//                            + " promoTemporadaNf2.promoTemporada.estadoPromo=true AND promoTemporadaNf2.nf2Sfamilia.idNf2Sfamilia=nf2Sfamilia.idNf2Sfamilia AND"
//                            + " ((to_date(to_char(promoTemporadaNf2.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
//                            + " OR (to_date(to_char(promoTemporadaNf2.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
//                            + " AND nf2Sfamilia.idNf2Sfamilia != 0 ORDER BY nf2Sfamilia.descripcion")
//                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
//                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
//                    .getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
//    @Override
//    public List<Nf2Sfamilia> listarPorNf(long idNf1) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            return em.createQuery("FROM Nf2Sfamilia nf2Sfamilia WHERE nf2Sfamilia.nf1Tipo.idNf1Tipo=" + idNf1
//                    + " order by nf2Sfamilia.descripcion").getResultList();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public void insertar(Existencia obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Existencia obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Existencia> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Existencia nf2Sfamilia order by nf2Sfamilia.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Existencia getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Existencia.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Existencia listarPorDepositoArticulo(long idDep, Long idArticulo) {
        EntityManager em = null;
//        Existencia ex = new Existencia();
        try {
            em = getEmf().createEntityManager();
            return (Existencia) em.createQuery("FROM Existencia existencia WHERE existencia.deposito.idDeposito=" + idDep + " AND "
                    + "existencia.articulo.idArticulo=" + idArticulo + " order by existencia.idExistencia")
                    .getSingleResult();
        } catch (Exception exce) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Existencia> listarPorArticulo(Long idArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Existencia existencia JOIN FETCH existencia.deposito dep WHERE "
                    + "existencia.articulo.idArticulo=" + idArticulo + " order by existencia.idExistencia")
                    .getResultList();
        } catch (Exception exce) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(Long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) "
                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=1 OR id_deposito=6)";
//            String sql = "SELECT SUM(cantidad) "
//                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=1 OR id_deposito=2)";
            System.out.println("SQL CC -> " + sql);
            return Long.parseLong(em.createNativeQuery(sql).getSingleResult() + "");
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(Long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) "
                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=2)";
//            String sql = "SELECT SUM(cantidad) "
//                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=3 OR id_deposito=4)";
            System.out.println("SQL SL -> " + sql);
            return Long.parseLong(em.createNativeQuery(sql).getSingleResult() + "");
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(Long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT SUM(cantidad) "
                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=3)";
//            String sql = "SELECT SUM(cantidad) "
//                    + "FROM stock.existencia WHERE id_articulo=" + idArt + " AND (id_deposito=5 OR id_deposito=6)";
            System.out.println("SQL SC -> " + sql);
            return Long.parseLong(em.createNativeQuery(sql).getSingleResult() + "");
        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Existencia filtrarPorArtDeposito(long idArt, long idDeposito) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Existencia) em.createQuery("FROM Existencia exis WHERE exis.articulo.idArticulo=:idArt AND "
                    + "exis.deposito.idDeposito=:idDepo")
                    .setParameter("idArt", idArt)
                    .setParameter("idDepo", idDeposito)
                    .getSingleResult();
        } catch (Exception ex) {
            System.out.println("->> " + ex.getLocalizedMessage());
            System.out.println("->> " + ex.getMessage());
            System.out.println("->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.createNativeQuery("SELECT * FROM stock.existencia WHERE id_articulo=" + idArt + " AND id_deposito=" + idDeposito).getSingleResult();
//            return true;
//        } catch (Exception ex) {
//            System.out.println("-->> " + ex.getLocalizedMessage());
//            System.out.println("-->> " + ex.fillInStackTrace());
//            return false;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

}
