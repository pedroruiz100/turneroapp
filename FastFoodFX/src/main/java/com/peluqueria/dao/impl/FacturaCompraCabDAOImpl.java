/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaCompraCab;
import com.peluqueria.dao.FacturaCompraCabDAO;
import static com.javafx.util.EMF.getEmf;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class FacturaCompraCabDAOImpl implements FacturaCompraCabDAO {

    @Override
    public FacturaCompraCab insertarObtenerObj(FacturaCompraCab aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraCab> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaCompraCab ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraCab getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.proveedor pro WHERE ap.idFacturaCompraCab=:aid")
                    .setParameter("aid", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaCompraCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaCompraCab obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FacturaCompraCab ac = em.find(FacturaCompraCab.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public String recuperarPorCajaFecha(long idCaja, String fecha) {
//
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            String sql = "SELECT fecha_apertura FROM caja.apertura_caja WHERE id_caja=" + idCaja + " AND "
//                    + "to_char(fecha_apertura, 'YYYY-MM-DD')='" + fecha + "'";
//            System.out.println("SQL -> " + sql);
//            String fechaData = String.valueOf(em.createNativeQuery(sql)
//                    .setMaxResults(1)
//                    .getSingleResult());
//            return fechaData;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public FacturaCompraCab getByNroOrden(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.pedidoCab pc WHERE ap.nroOrden=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraCab getByNroOrdenPedido(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.pedidoCab pc WHERE pc.oc=:aid")
                    .setParameter("aid", text).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraCab> listarPorFecha(Date desde, Date hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaCompraCab ap WHERE ap.fechaDoc>= :desde AND ap.fechaDoc <= :hasta")
                    .setParameter("desde", desde)
                    .setParameter("hasta", hasta)
                    .getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Map getByNroOrdenEntrada(String compra, String entrada) {
        EntityManager em = null;
        Map mapeo = new HashMap();
//        FacturaCompraCab recep = new FacturaCompraCab();
        if (!compra.equals("") && !entrada.equals("")) {
//            try {
//                long id = Long.parseLong(String.valueOf(em.createNativeQuery("SELECT id_pedido_cab "
//                        + "FROM factura_cliente.pedido_cab WHERE oc='" + compra + "'")
//                        .setMaxResults(1)
//                        .getSingleResult()));
//                mapeo.put("idPedidoCab", id);
//                mapeo.put("idFacturaCompraCab", "");
//            } catch (Exception ex) {
//                System.out.println("-->> " + ex.getLocalizedMessage());
//                System.out.println("-->> " + ex.getMessage());
//                System.out.println("-->> " + ex.fillInStackTrace());
            mapeo = null;
//            } finally {
//                if (em != null) {
//                    em.close();
//                }
//            }
        } else if (!compra.equals("")) {
//            try {
//                String sql = "SELECT id_pedido_cab FROM factura_cliente.pedido_cab WHERE oc='" + compra + "'";
//                System.out.println("sql ->> " + sql);
//                long id = Long.parseLong(String.valueOf(em.createNativeQuery(sql)
//                        .setMaxResults(1)
//                        .getSingleResult()));
//                mapeo.put("idPedidoCab", id);
//                mapeo.put("idFacturaCompraCab", "");
//            } catch (Exception ex) {
//                System.out.println("-->> " + ex.getLocalizedMessage());
//                System.out.println("-->> " + ex.getStackTrace());
//                System.out.println("-->> " + ex.fillInStackTrace());
//                ex.printStackTrace();
            mapeo = null;
//            } finally {
//                if (em != null) {
//                    em.close();
//                }
//            }
        } else if (!entrada.equals("")) {
            try {
                em = getEmf().createEntityManager();
                FacturaCompraCab recep = (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab recep WHERE recep.nroOrden=:entrada")
                        .setParameter("entrada", entrada)
                        .getSingleResult();
                mapeo.put("idPedidoCab", "");
                mapeo.put("idFacturaCompraCab", recep.getIdFacturaCompraCab());
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                mapeo = null;
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return mapeo;
    }

    @Override
    public List<FacturaCompraCab> getByNroOrdenEntradaFecha(String compra, String entrada, LocalDate fecha) {
        EntityManager em = null;
        Map mapeo = new HashMap();
        List<FacturaCompraCab> listFacturaCompraCab = new ArrayList<>();
//        FacturaCompraCab recep = new FacturaCompraCab();
        if (!compra.equals("") && !entrada.equals("")) {
            mapeo = null;
        } else if (!compra.equals("")) {
            mapeo = null;
        } else if (!entrada.equals("")) {
            try {
                System.out.println("FECHA: -> " + Date.valueOf(fecha));
                em = getEmf().createEntityManager();
                listFacturaCompraCab = (List<FacturaCompraCab>) em.createQuery("FROM FacturaCompraCab recep WHERE recep.nroOrden=:entrada AND "
                        + "recep.fechaDoc=:fecha")
                        //                FacturaCompraCab recep = (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab recep WHERE recep.nroOrden=:entrada AND "
                        //                        + "recep.fechaDoc=:fecha")
                        .setParameter("entrada", entrada)
                        .setParameter("fecha", Date.valueOf(fecha))
                        .getResultList();
//                        .setMaxResults(1)
//                        .getSingleResult();
//                listFacturaCompraCab.add(recep);
            } catch (Exception ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                listFacturaCompraCab = new ArrayList<>();
            } finally {
                if (em != null) {
                    em.close();
                }
            }
        }
        return listFacturaCompraCab;
    }

    @Override
    public FacturaCompraCab getByIdFecha(Long idPedidoCab, LocalDate fecha) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap WHERE ap.idFacturaCompraCab=:aid AND recep.fechaDoc=:fecha")
                    .setParameter("aid", idPedidoCab)
                    .setParameter("fecha", Date.valueOf(fecha))
                    .getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraCab getByOC(String oc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.proveedor pro WHERE ap.oc=:aid")
                    .setParameter("aid", oc).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraCab> getByOCFiltro(String text) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.proveedor pro WHERE ap.oc LIKE :aid ORDER BY ap.idFacturaCompraCab DESC")
                    .setParameter("aid", text + "%").getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaCompraCab getByIdCompra(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaCompraCab) em.createQuery("FROM FacturaCompraCab ap JOIN FETCH ap.pedidoCab pro WHERE ap.idFacturaCompraCab=:aid")
                    .setParameter("aid", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaCompraCab> listarCabeceras(String oc, String sucursal) {
        EntityManager em = null;
        em = getEmf().createEntityManager();
        boolean central = false;
        boolean cacique = false;
        boolean sanlo = false;
        String suc = sucursal.substring(0, (sucursal.length() - 2));
        StringTokenizer sts = new StringTokenizer(suc, ".");
        String suc1 = "";
        try {
            suc1 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        String suc2 = "";
        try {
            suc2 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        String suc3 = "";
        try {
            suc3 = sts.nextElement().toString(); //1   
        } catch (Exception e) {
        } finally {
        }
        suc1 = suc1.trim();
        suc2 = suc2.trim();
        suc3 = suc3.trim();

        if (suc1.equalsIgnoreCase("CENTRAL")) {
            central = true;
        } else if (suc1.equalsIgnoreCase("SAN LORENZO")) {
            sanlo = true;
        } else if (suc1.equalsIgnoreCase("CACIQUE")) {
            cacique = true;
        }

        if (suc3.equalsIgnoreCase("CENTRAL")) {
            if (!central) {
                central = true;
            }
        } else if (suc3.equalsIgnoreCase("SAN LORENZO")) {
            if (!sanlo) {
                sanlo = true;
            }
        } else if (suc3.equalsIgnoreCase("CACIQUE")) {
            if (!cacique) {
                cacique = true;
            }
        }

        if (suc2.equalsIgnoreCase("CENTRAL")) {
            if (!central) {
                central = true;
            }
        } else if (suc2.equalsIgnoreCase("SAN LORENZO")) {
            if (!sanlo) {
                sanlo = true;
            }
        } else if (suc2.equalsIgnoreCase("CACIQUE")) {
            if (!cacique) {
                cacique = true;
            }
        }
        try {
            return em.createQuery("FROM FacturaCompraCab recep WHERE recep.oc=:ordenCompra AND "
                    + "(recep.cc=:ecc OR recep.sc=:esc) ORDER BY recep.nroDoc")
                    //            return em.createQuery("FROM FacturaCompraDet fcd JOIN FETCH fcd.facturaCompraCab recep WHERE recep.oc=:ordenCompra AND "
                    //                    + "(recep.cc=:ecc OR recep.sc=:esc OR recep.sl=:esl)")
                    .setParameter("ordenCompra", oc)
                    .setParameter("ecc", central)
                    .setParameter("esc", cacique)
                    //                    .setParameter("esl", sanlo)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public String recuperarOcByFecha(String fecha, String idProv) {
        EntityManager em = null;
//        String ultimaVenta = "";
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT MAX(fcc.oc) FROM factura_cliente.factura_compra_cab fcc WHERE fcc.fecha_doc='" + fecha + "' AND "
                    + "fcc.id_proveedor=" + Long.parseLong(idProv);
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return x;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
