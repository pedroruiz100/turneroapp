package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TalonariosSucursaleDAOImpl implements TalonariosSucursaleDAO {

    @Override
    public boolean actualizarNroActual(TalonariosSucursales talo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(talo);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<TalonariosSucursales> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<TalonariosSucursales> list = em.createQuery("FROM TalonariosSucursale ts").getResultList();
            return list;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TalonariosSucursales getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            TalonariosSucursales tal = em.find(TalonariosSucursales.class, id);
            return tal;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TalonariosSucursales obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TalonariosSucursales obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
    }

}
