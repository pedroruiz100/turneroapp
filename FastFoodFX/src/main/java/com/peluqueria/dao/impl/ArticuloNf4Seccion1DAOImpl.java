package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class ArticuloNf4Seccion1DAOImpl implements ArticuloNf4Seccion1DAO {

    @Override
    public List<ArticuloNf4Seccion1> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloNf4Seccion1 a").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf4Seccion1 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf4Seccion1) em
                    .createQuery("FROM ArticuloNf4Seccion1 a WHERE a.idNf4Seccion1Articulo=:idNf4Seccion1Articulo")
                    .setParameter("idNf4Seccion1Articulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloNf4Seccion1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloNf4Seccion1 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloNf4Seccion1 articuloNf4Seccion1 = em.find(ArticuloNf4Seccion1.class, id);
            em.remove(articuloNf4Seccion1);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf4Seccion1 getByIdArticulo(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf4Seccion1) em
                    .createQuery("FROM ArticuloNf4Seccion1 a WHERE a.articulo.idArticulo=:idNf1TipoArticulo")
                    .setParameter("idNf1TipoArticulo", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloNf4Seccion1 listarArticulo(long idArt) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloNf4Seccion1) em.createQuery("FROM ArticuloNf4Seccion1 a WHERE a.articulo.idArticulo=:idArt")
                    .setParameter("idArt", idArt).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
