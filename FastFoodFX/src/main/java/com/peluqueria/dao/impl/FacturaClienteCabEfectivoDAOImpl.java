package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.FacturaClienteCabEfectivo;
import com.peluqueria.dao.FacturaClienteCabEfectivoDAO;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabEfectivoDAOImpl implements
        FacturaClienteCabEfectivoDAO {

    @Override
    public List<FacturaClienteCabEfectivo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FacturaClienteCabEfectivo f")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabEfectivo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (FacturaClienteCabEfectivo) em
                    .createQuery(
                            "FROM FacturaClienteCabEfectivo f WHERE f.idFacturaClienteCabEfectivo =:idFac")
                    .setParameter("idFac", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabEfectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabEfectivo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public FacturaClienteCabEfectivo insertarObtenerObjeto(FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(facturaClienteCabEfectivo);
            em.getTransaction().commit();
            em.refresh(facturaClienteCabEfectivo);
            return facturaClienteCabEfectivo;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCabEfectivo> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabEfectivo fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCabEfectivo> listarPorFechaHoy() {
        EntityManager em = null;
        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabEfectivo fact JOIN FETCH fact.facturaClienteCab fcc WHERE to_char(fcc.fechaEmision,'YYYY-MM-DD') = :fecIni")
                    .setParameter("fecIni", (format.format(date)))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
