package com.peluqueria.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.FuncionarioNf7;
import com.peluqueria.dao.FuncionarioNf7DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import javax.persistence.PersistenceException;

@Repository
public class FuncionarioNf7DAOImpl implements FuncionarioNf7DAO {

    @Override
    public List<FuncionarioNf7> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<FuncionarioNf7> funcionarioNf7s = em
                    .createQuery(
                            "FROM FuncionarioNf7 funcionarioNf7 WHERE funcionarioNf7.estadoDesc=true ORDER BY funcionarioNf7.descriSeccion")
                    .getResultList();
            return funcionarioNf7s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf7 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FuncionarioNf7.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FuncionarioNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FuncionarioNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FuncionarioNf7 sf = em.find(FuncionarioNf7.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf7> listarFetchNf7(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FuncionarioNf7 funcionarioNf7 WHERE funcionarioNf7.estadoDesc=true ORDER BY funcionarioNf7.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf7> listarFetchFilterNf7(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FuncionarioNf7 funcionarioNf7 WHERE funcionarioNf7.estadoDesc=true AND"
                            + " (LOWER(funcionarioNf7.descriSeccion) LIKE :descri) order by funcionarioNf7.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf7() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.seccion_func sf WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilterNf7(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM FuncionarioNf7 funcionarioNf7 WHERE funcionarioNf7.estadoDesc=true AND"
                    + " (LOWER(funcionarioNf7.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf7 funcionarioNf7 "
                    + "SET funcionarioNf7.estadoDesc=false "
                    + "WHERE funcionarioNf7.estadoDesc=true");
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(FuncionarioNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf7 funcionarioNf7 "
                    + "SET funcionarioNf7.estadoDesc=false, funcionarioNf7.usuMod=:uMod, funcionarioNf7.fechaMod=:fMod "
                    + "WHERE funcionarioNf7.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf7 insertarObtenerObjeto(FuncionarioNf7 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean validacionInsercion(long idNf) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            if (em.createQuery(
                    "FROM FuncionarioNf7 as funcionarioNf7 "
                    + "WHERE funcionarioNf7.estadoDesc = true "
                    + "AND funcionarioNf7.nf7Secnom7.idNf7Secnom7 = :idNf "
                    + "ORDER BY funcionarioNf7.descriSeccion")
                    .setParameter("idNf", idNf)
                    .setMaxResults(1)
                    .getSingleResult() != null) {
                existe = true;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
