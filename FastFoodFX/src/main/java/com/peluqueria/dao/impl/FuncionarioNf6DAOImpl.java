package com.peluqueria.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.FuncionarioNf6;
import com.peluqueria.dao.FuncionarioNf6DAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import javax.persistence.PersistenceException;

@Repository
public class FuncionarioNf6DAOImpl implements FuncionarioNf6DAO {

    @Override
    public List<FuncionarioNf6> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<FuncionarioNf6> funcionarioNf6s = em
                    .createQuery(
                            "FROM FuncionarioNf6 funcionarioNf6 WHERE funcionarioNf6.estadoDesc=true ORDER BY funcionarioNf6.descriSeccion")
                    .getResultList();
            return funcionarioNf6s;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf6 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FuncionarioNf6.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FuncionarioNf6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FuncionarioNf6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            FuncionarioNf6 sf = em.find(FuncionarioNf6.class, id);
            em.remove(sf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf6> listarFetchNf6(long limRow, long offSet) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM FuncionarioNf6 funcionarioNf6 WHERE funcionarioNf6.estadoDesc=true ORDER BY funcionarioNf6.descriSeccion")
                    .setMaxResults((int) limRow).setFirstResult((int) offSet)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FuncionarioNf6> listarFetchFilterNf6(long limRow, long offSet, String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FuncionarioNf6 funcionarioNf6 WHERE funcionarioNf6.estadoDesc=true AND"
                            + " (LOWER(funcionarioNf6.descriSeccion) LIKE :descri) order by funcionarioNf6.descriSeccion")
                    .setParameter("descri", nombre.toLowerCase() + "%")
                    .setMaxResults((int) limRow)// LIMIT "+limRow+"
                    .setFirstResult((int) offSet)// OFFSET "+offSet
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Long rowCountNf6() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "SELECT count(1) FROM cuenta.seccion_func sf WHERE estado_desc=true";
            String fila = String.valueOf(em.createNativeQuery(cadena)
                    .getSingleResult());
            Long row = Long.valueOf(fila);
            return row;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long rowCountFilterNf6(String nombre) {
        if (nombre.equalsIgnoreCase("null")) {
            nombre = "";
        }
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Long> row = new ArrayList<Long>();
            row = em.createQuery(
                    "SELECT count(*) FROM FuncionarioNf6 funcionarioNf6 WHERE funcionarioNf6.estadoDesc=true AND"
                    + " (LOWER(funcionarioNf6.descriSeccion) LIKE :nombre)")
                    .setParameter("nombre", nombre.toLowerCase() + "%")
                    .getResultList();
            long fila = row.get(0);
            return fila;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void bajas() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf6 funcionarioNf6 "
                    + "SET funcionarioNf6.estadoDesc=false "
                    + "WHERE funcionarioNf6.estadoDesc=true");
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean actualizarObtenerEstado(FuncionarioNf6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean bajasLocal(String u, Timestamp ts) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createQuery(
                    "UPDATE FuncionarioNf6 funcionarioNf6 "
                    + "SET funcionarioNf6.estadoDesc=false, funcionarioNf6.usuMod=:uMod, funcionarioNf6.fechaMod=:fMod "
                    + "WHERE funcionarioNf6.estadoDesc=true")
                    .setParameter("uMod", u).setParameter("fMod", ts)
                    .executeUpdate();
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FuncionarioNf6 insertarObtenerObjeto(FuncionarioNf6 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            em.refresh(obj);
            return obj;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean validacionInsercion(long idNf) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            boolean existe = false;
            if (em.createQuery(
                    "FROM FuncionarioNf6 as funcionarioNf6 "
                    + "WHERE funcionarioNf6.estadoDesc = true "
                    + "AND funcionarioNf6.nf6Secnom6.idNf6Secnom6 = :idNf "
                    + "ORDER BY funcionarioNf6.descriSeccion")
                    .setParameter("idNf", idNf)
                    .setMaxResults(1)
                    .getSingleResult() != null) {
                existe = true;
            }
            return existe;
        } catch (PersistenceException e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
