package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class SucursalDAOImpl implements SucursalDAO {

    @Override
    public List<Sucursal> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Sucursal s").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Sucursal getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Sucursal.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Sucursal obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Sucursal obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public Sucursal consultandoSuc(long id) {
        EntityManager em = getEmf().createEntityManager();
        try {
            return (Sucursal) em.createQuery("FROM Sucursal s where s.idSucursal = :id").setParameter("id", id).getSingleResult();
        } finally {
            em.close();
        }
    }

}
