/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RangoDescFielCab;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import com.peluqueria.dao.RangoDescFielCabDAO;
import static com.javafx.util.EMF.getEmf;

/**
 *
 * @author PC
 */
@Repository
public class RangoDescFielCabDAOImpl implements RangoDescFielCabDAO {

    @Override
    public long actualizarRecuperarRangoActual(long idRango) {
        EntityManager em = null;
        long r = -1l;
        try {
            em = getEmf().createEntityManager();
            RangoDescFielCab rango = em.find(RangoDescFielCab.class, idRango);
            r = rango.getRangoActual();
            rango.setRangoActual(r + 1);
            em.getTransaction().begin();
            em.merge(rango);
            em.getTransaction().commit();
            //Para que muestre el actual, sino solo el 1
            return r;
        } catch (Exception e) {
            System.out.println("--->> " + e.getLocalizedMessage());
            return r;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<RangoDescFielCab> listar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RangoDescFielCab getById(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insertar(RangoDescFielCab obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actualizar(RangoDescFielCab obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void eliminar(long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
