package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.FacturaClienteCabMonedaExtranjera;
import com.peluqueria.dao.FacturaClienteCabMonedaExtranjeraDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class FacturaClienteCabMonedaExtranjeraDAOImpl implements
        FacturaClienteCabMonedaExtranjeraDAO {

    @Override
    public List<FacturaClienteCabMonedaExtranjera> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabMonedaExtranjera fact ORDER BY fact.idFacturaClienteCabMonedaExtranjera")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public FacturaClienteCabMonedaExtranjera getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(FacturaClienteCabMonedaExtranjera.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(FacturaClienteCabMonedaExtranjera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(FacturaClienteCabMonedaExtranjera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(FacturaClienteCabMonedaExtranjera fac, int num) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(fac);
            em.getTransaction().commit();
            if (num % 20 == 0) {
                em.flush();
                em.clear();
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<FacturaClienteCabMonedaExtranjera> listarPorFactura(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM FacturaClienteCabMonedaExtranjera fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
                    .setParameter("idFac", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
