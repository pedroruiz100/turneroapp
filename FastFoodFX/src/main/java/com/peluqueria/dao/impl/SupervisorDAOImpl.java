package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.dao.SupervisorDAO;
import com.javafx.util.CryptoBack;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class SupervisorDAOImpl implements SupervisorDAO {

    @Override
    public List<Supervisor> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Supervisor s ORDER BY d.idSupervisor").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Supervisor getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Supervisor.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Supervisor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Supervisor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public Supervisor buscarCodSup(String codSup) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Supervisor) em
                    .createQuery(
                            "FROM Supervisor s "
                            + " where "
                            + "(s.codSupervisor = :codSup) and (s.activo = true)")
                    .setParameter("codSup", codSup).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Supervisor buscarNumeroCodigoSup(int numSup, String codSup) {
        EntityManager em = null;
        CryptoBack cb = new CryptoBack();
        try {
            em = getEmf().createEntityManager();
            return (Supervisor) em
                    .createQuery(
                            "FROM Supervisor s"
                            + " where (s.nroSupervisor = :numSup)"
                            //                            + " and (s.codSupervisor = :codSup) and (s.activo = true)")
                            + " and (s.usuario.contrasenha = :codSup) and (s.activo = true)")
                    .setParameter("numSup", numSup)
                    .setParameter("codSup", cb.getHash(codSup)).getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Supervisor buscarSupervisor(String usuario, String clave) {
        EntityManager em = null;
        CryptoBack cb = new CryptoBack();
        try {
            em = getEmf().createEntityManager();
            return (Supervisor) em
                    .createQuery(
                            "FROM Supervisor s"
                            + " where (s.usuario.nomUsuario = :numUsu)"
                            //                            + " and (s.codSupervisor = :codSup) and (s.activo = true)")
                            + " and (s.usuario.contrasenha = :clave) and (s.activo = true)")
                    .setParameter("numUsu", usuario.toUpperCase())
                    .setParameter("clave", cb.getHash(clave)).getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public int obteniendoMaxNroSupervisor() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (int) em.createQuery("SELECT s.nroSupervisor FROM Supervisor s ORDER BY s.nroSupervisor desc")
                    .setMaxResults(1).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public Supervisor insertarObtenerObjeto(Supervisor supervisor) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(supervisor);
            em.refresh(supervisor);
            em.getTransaction().commit();
            return supervisor;
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Supervisor actualizarObtenerObjeto(Supervisor supervisor) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(supervisor);
//            em.refresh(supervisor);
            em.getTransaction().commit();
            return supervisor;
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Supervisor buscarIdUsuario(Long idUsuario) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (Supervisor) em
                    .createQuery(
                            "FROM Supervisor s "
                            + " where "
                            + "(s.usuario.idUsuario = :idSup) and (s.activo = true)")
                    .setParameter("idSup", idUsuario).getSingleResult();
        } catch (Exception e) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
