package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescuentoTarjetaConvenioDet;
import com.peluqueria.dao.DescuentoTarjetaConvenioDetDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescuentoTarjetaConvenioDetDAOImpl implements
        DescuentoTarjetaConvenioDetDAO {

    @Override
    public List<DescuentoTarjetaConvenioDet> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<DescuentoTarjetaConvenioDet> list = em
                    .createQuery(
                            "FROM DescuentoTarjetaConvenioDet dtcd ORDER BY dtcd.idDescuentoTarjetaConvenioDet DESC")
                    .getResultList();
            return list;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescuentoTarjetaConvenioDet getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DescuentoTarjetaConvenioDet.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescuentoTarjetaConvenioDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescuentoTarjetaConvenioDet obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public List<DescuentoTarjetaConvenioDet> listarLimitado(long limite,
            long inicio, String nombre) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM DescuentoTarjetaConvenioDet dtcd WHERE upper(dtcd.descuentoTarjetaConvenioCab.descriTarjetaConvenio) like :descriTar "
                            + "ORDER BY dtcd.idDescuentoTarjetaConvenioDet")
                    .setParameter("descriTar", nombre.toUpperCase() + "%")
                    .setFirstResult((int) inicio).setMaxResults((int) limite)
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(DescuentoTarjetaConvenioDet desc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(desc);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
