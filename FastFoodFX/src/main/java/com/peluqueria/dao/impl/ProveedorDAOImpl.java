package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ProveedorDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ProveedorDAOImpl implements ProveedorDAO {

    @Override
    public List<Proveedor> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Proveedor p").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Proveedor getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Proveedor.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Proveedor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {

        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Proveedor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public Proveedor listarPorRuc(String ruc) {
        EntityManager em = null;
        try {
//            em = getEmf().createEntityManager();
//            return (Cliente) em.createQuery("FROM Cliente c WHERE c.ruc =:numRuc").setParameter("numRuc", ruc).getSingleResult();
            em = getEmf().createEntityManager();
            return (Proveedor) em.createQuery("FROM Proveedor c WHERE upper(c.ruc) like :numRuc")
                    .setParameter("numRuc", ruc.toUpperCase() + "%")
                    .setMaxResults(1).getSingleResult();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            Proveedor cli = new Proveedor();
            cli.setIdProveedor(null);
            return cli;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Proveedor> listarPorCIRevancha(String rucCliente) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (List<Proveedor>) em.createQuery("FROM Proveedor c WHERE upper(c.ruc) like :numRuc")
                    .setParameter("numRuc", rucCliente.toUpperCase() + "-%").getResultList();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Proveedor> listarPorNomRuc(String nom, String ruc) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Proveedor> listaCliente = new ArrayList<>();
            if (!nom.equalsIgnoreCase("null")
                    && !ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "SELECT c FROM Proveedor c WHERE upper(c.descripcion) like :nombre "
                                + "and upper(c.ruc) like :numRuc ORDER BY c.descripcion")
                        .setParameter("nombre", nom.toUpperCase() + "%")
                        .setParameter("numRuc", ruc.toUpperCase() + "%")
                        .setMaxResults(10).getResultList();

            } else if (!nom.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery(
                                "SELECT c FROM Proveedor c WHERE upper(c.descripcion) like :nombre ORDER BY c.descripcion")
                        .setMaxResults(10)
                        .setParameter("nombre", nom.toUpperCase() + "%")
                        .getResultList();
            } else if (!ruc.equalsIgnoreCase("null")) {
                listaCliente = em
                        .createQuery("SELECT c FROM Proveedor c WHERE c.ruc =:numRuc")
                        .setParameter("numRuc", ruc).getResultList();

            } else {
                listaCliente = em
                        .createQuery("SELECT c FROM Proveedor c ORDER BY c.fechaAlta")
                        .setMaxResults(10).getResultList();
            }
            return listaCliente;
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.getMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarEstado(Proveedor obj) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public boolean actualizarEstado(Proveedor obj) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }
}
