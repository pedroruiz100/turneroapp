package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.dao.Nf3SseccionDAO;
import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;

@Repository
public class Nf3SseccionDAOImpl implements Nf3SseccionDAO {

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf3Sseccion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf3Sseccion nf3Sseccion order by nf3Sseccion.descripcion")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Nf3Sseccion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(Nf3Sseccion.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Nf3Sseccion nf3Sseccion) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(nf3Sseccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Nf3Sseccion nf3Sseccion) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(nf3Sseccion);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf3Sseccion> listarNombreId() {
        //FALTA MODIFICAR...
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf3Sseccion nf3Sseccion WHERE NOT EXISTS(FROM DescuentoFielCab descuentoFielCab "
                            + "WHERE descuentoFielCab.estadoDesc=true AND descuentoFielCab.nf3Sseccion.idNf3Sseccion=nf3Sseccion.idNf3Sseccion)"
                            + " AND nf3Sseccion.idNf3Sseccion != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
        //FALTA MODIFICAR...
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf3Sseccion> listarFuncionarioNombreId() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM Nf3Sseccion nf3Sseccion WHERE NOT EXISTS(FROM FuncionarioNf3 funcionarioNf3 "
                            + "WHERE funcionarioNf3.estadoDesc=true AND funcionarioNf3.nf3Sseccion.idNf3Sseccion=nf3Sseccion.idNf3Sseccion) "
                            + "AND nf3Sseccion.idNf3Sseccion != 0").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    @SuppressWarnings("JPQLValidation")
    public List<Nf3Sseccion> listarPromoTemp(String inicio, String fin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            // compara las fecha entre el fin de la promo temporada y la fecha
            // actual
            // la fecha actual es la que actualmente se obtiene del sistema
            // operativo
            return em
                    .createQuery(
                            "FROM Nf3Sseccion nf3Sseccion WHERE NOT EXISTS(FROM PromoTemporadaNf3 promoTemporadaNf3 WHERE"
                            + " promoTemporadaNf3.promoTemporada.estadoPromo=true AND promoTemporadaNf3.nf3Sseccion.idNf3Sseccion=nf3Sseccion.idNf3Sseccion AND"
                            + " ((to_date(to_char(promoTemporadaNf3.promoTemporada.fechaInicio, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin"
                            + " OR (to_date(to_char(promoTemporadaNf3.promoTemporada.fechaFin, 'DD-MON-YY'), 'DD-MON-YY')) BETWEEN :inicio AND :fin))"
                            + " AND nf3Sseccion.idNf3Sseccion != 0 ORDER BY nf3Sseccion.descripcion")
                    .setParameter("inicio", Utilidades.stringToUtilDate(inicio))
                    .setParameter("fin", Utilidades.stringToUtilDate(fin))
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<Nf3Sseccion> listarPorNf(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM Nf3Sseccion nf3Sseccion WHERE nf3Sseccion.nf2Sfamilia.idNf2Sfamilia=" + id
                    + " order by nf3Sseccion.descripcion").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
