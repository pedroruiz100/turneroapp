package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ArtPromoTemporadaObsequio;
import com.peluqueria.dao.ArtPromoTemporadaObsequioDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ArtPromoTemporadaObsequioDAOImpl implements ArtPromoTemporadaObsequioDAO {

    @Override
    public List<ArtPromoTemporadaObsequio> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM ArtPromoTemporadaObsequio apt WHERE apt.promoTemporadaArt.estadoPromo=true "
                    + "ORDER BY idArtPromoTemporadaObsequio").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArtPromoTemporadaObsequio getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(ArtPromoTemporadaObsequio.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArtPromoTemporadaObsequio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArtPromoTemporadaObsequio obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(ArtPromoTemporadaObsequio art, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(art);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(ArtPromoTemporadaObsequio art) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(art);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
