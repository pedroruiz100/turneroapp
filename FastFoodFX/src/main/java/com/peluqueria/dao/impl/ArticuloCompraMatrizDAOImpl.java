package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.ArticuloCompraMatriz;
import com.peluqueria.dao.ArticuloCompraMatrizDAO;
import static com.javafx.util.EMF.getEmf;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class ArticuloCompraMatrizDAOImpl implements ArticuloCompraMatrizDAO {

    @Override
    public List<ArticuloCompraMatriz> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<ArticuloCompraMatriz> listBarrio = em.createQuery("FROM ArticuloCompraMatriz b ORDER BY b.fecha").getResultList();
            return listBarrio;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloCompraMatriz getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloCompraMatriz) em
                    .createQuery("Select b from ArticuloCompraMatriz b WHERE b.idArticuloCompraMatriz=:idBa")
                    .setParameter("idBa", id).getSingleResult();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            ex.fillInStackTrace();
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloCompraMatriz obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloCompraMatriz obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloCompraMatriz ba = em.find(ArticuloCompraMatriz.class, id);
            em.remove(ba);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM stock.articulo_compra_matriz fcd "
                    + "  WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND cc='true'";
            System.out.println("SQL -> " + sql);
            //            long x = Long.parseLong(String.valueOf(em.createNativeQuery(sql).getSingleResult()));
            //            return x;
            Query q = em.createNativeQuery(sql);
            String x = q.getSingleResult() + "";
            return Long.parseLong(x);
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM stock.articulo_compra_matriz fcd "
                    + "  WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND sc='true'";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.fillInStackTrace());
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            DateTimeFormatter formatters = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            String desd = desde.format(formatters);
            String hast = hasta.format(formatters);
            String sql = "SELECT SUM(fcd.cantidad) FROM stock.articulo_compra_matriz fcd "
                    + "  WHERE fcd.codigo='" + codArt + "'"
                    + "   AND DATE(fcd.fecha)>='" + desd
                    + "' AND DATE(fcd.fecha)<='" + hast + "' AND sl='true'";
            System.out.println("SQL -> " + sql);
            String x = em.createNativeQuery(sql).getSingleResult() + "";
            return Long.parseLong(x.replace(".000", ""));
        } catch (Exception ex) {
            System.out.println("-->> " + ex.fillInStackTrace());
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloCompraMatriz getByCodFecha(String cod, LocalDate nowLast, boolean cc, boolean sc, boolean sl) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloCompraMatriz) em
                    .createQuery("Select b from ArticuloCompraMatriz b WHERE b.codigo='" + cod + "' AND "
                            + "DATE(b.fecha)='" + nowLast + "' AND (cc='" + cc + "' OR sc='" + sc + "') AND sl='" + sl + "'").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
