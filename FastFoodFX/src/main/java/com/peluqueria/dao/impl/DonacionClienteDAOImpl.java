package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DonacionCliente;
import com.peluqueria.dao.DonacionClienteDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DonacionClienteDAOImpl implements DonacionClienteDAO {

    @Override
    public List<DonacionCliente> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DonacionCliente dc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DonacionCliente getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(DonacionCliente.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DonacionCliente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DonacionCliente obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
