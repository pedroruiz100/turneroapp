package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.CancelFact;
import com.peluqueria.dao.CancelFactDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 *
 * @author ADMIN
 */
@Repository
public class CancelFactDAOImpl implements CancelFactDAO {

    @Override
    public List<CancelFact> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM CancelFact cf").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CancelFact getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(CancelFact.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(CancelFact obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(CancelFact obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            CancelFact cf = em.find(CancelFact.class, id);
            em.remove(cf);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
