package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.Dias;
import com.peluqueria.dao.DiaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DiaDAOImpl implements DiaDAO {

    @Override
    public List<Dias> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<Dias> d = em.createQuery("FROM Dias d ORDER BY d.idDia").getResultList();
            return d;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public Dias getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            Dias d = em.find(Dias.class, id);
            return d;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(Dias obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(Dias obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
