package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.DescTarjetaFiel;
import com.peluqueria.dao.DescTarjetaFielDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class DescTarjetaFielDAOImpl implements DescTarjetaFielDAO {

    @Override
    public List<DescTarjetaFiel> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM DescTarjetaFiel dtf").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public DescTarjetaFiel getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (DescTarjetaFiel) em
                    .createQuery(
                            "FROM DescTarjetaFiel dtf WHERE dtf.idDescTarjetaFiel=:idDesc")
                    .setParameter("idDesc", id).getSingleResult();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(DescTarjetaFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(DescTarjetaFiel obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
