package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.CierreCaja;
import com.peluqueria.dao.CierreCajaDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class CierreCajaDAOImpl implements CierreCajaDAO {

    @Override
    public List<CierreCaja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM CierreCaja c ORDER BY c.idCierre").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public CierreCaja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(CierreCaja.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(CierreCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(CierreCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            CierreCaja cc = em.find(CierreCaja.class, id);
            em.remove(cc);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
