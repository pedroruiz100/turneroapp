/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.peluqueria.core.domain.AperturaCaja;
import com.peluqueria.core.domain.PreparacionCabecera;
import com.peluqueria.dao.PreparacionCabeceraDAO;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ADMIN
 */
@Repository
public class PreparacionCabeceraDAOImpl implements PreparacionCabeceraDAO {

    @Override
    public PreparacionCabecera insertarObtenerObj(PreparacionCabecera aperturaCaja) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(aperturaCaja);
            em.getTransaction().commit();
            em.refresh(aperturaCaja);
            return aperturaCaja;
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionCabecera> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PreparacionCabecera ap").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PreparacionCabecera getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PreparacionCabecera) em.createQuery("FROM PreparacionCabecera ap WHERE ap.idGasto=:aid")
                    .setParameter("aid", id).getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PreparacionCabecera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PreparacionCabecera obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            AperturaCaja ac = em.find(AperturaCaja.class, id);
            em.remove(ac);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

//    @Override
//    public String recuperarPorCajaFecha(long idCaja, String fecha) {
//
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            String sql = "SELECT fecha_apertura FROM caja.apertura_caja WHERE id_caja=" + idCaja + " AND "
//                    + "to_char(fecha_apertura, 'YYYY-MM-DD')='" + fecha + "'";
//            System.out.println("SQL -> " + sql);
//            String fechaData = String.valueOf(em.createNativeQuery(sql)
//                    .setMaxResults(1)
//                    .getSingleResult());
//            return fechaData;
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
//    }
    @Override
    public boolean insertarEstado(PreparacionCabecera pro) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(pro);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public boolean actualizarEstado(PreparacionCabecera pro) {
        boolean valor = false;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(pro);
            em.getTransaction().commit();
            valor = true;
        } catch (Exception ex) {
            valor = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return valor;
    }

    @Override
    public List<PreparacionCabecera> listarPorFechas(LocalDate firstDay, LocalDate lastDay) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM PreparacionCabecera ap WHERE DATE(ap.fecha)>=:desde AND DATE(ap.fecha)<=:hasta ORDER BY ap.tipo")
                    .setParameter("desde", Date.from(firstDay.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .setParameter("hasta", Date.from(lastDay.atStartOfDay(ZoneId.systemDefault()).toInstant()))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
            System.out.println("-> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<PreparacionCabecera> filtroFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM PreparacionCabecera fcc WHERE to_char(fcc.fecha,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(fcc.fecha,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.tipo")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PreparacionCabecera recuperarUltimo() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (PreparacionCabecera) em.createQuery("FROM PreparacionCabecera ap ORDER BY ap.idGasto DESC")
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<String> listarProveedorDistinct() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            List<String> listTimb = em
                    .createQuery(
                            "SELECT distinct empresa FROM PreparacionCabecera fac ORDER BY empresa")
                    .getResultList();
            return listTimb;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }
}
