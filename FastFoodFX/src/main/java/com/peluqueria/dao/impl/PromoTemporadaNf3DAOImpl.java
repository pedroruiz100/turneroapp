package com.peluqueria.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.PromoTemporadaNf3;
import com.peluqueria.dao.PromoTemporadaNf3DAO;
import static com.javafx.util.EMF.getEmf;

@Repository
public class PromoTemporadaNf3DAOImpl implements PromoTemporadaNf3DAO {

    @Override
    public List<PromoTemporadaNf3> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery(
                    "FROM PromoTemporadaNf3 promoTemporadaNf3 WHERE promoTemporadaNf3.promoTemporada.estadoPromo=true "
                    + "ORDER BY idPromoTemporadaNf3").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public PromoTemporadaNf3 getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(PromoTemporadaNf3.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(PromoTemporadaNf3 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(PromoTemporadaNf3 obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

    @Override
    public void insercionMasiva(PromoTemporadaNf3 sec, long i) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(sec);
            em.getTransaction().commit();
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insercionMasivaEstado(PromoTemporadaNf3 sec) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(sec);
            em.getTransaction().commit();
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
