package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.TipoCombo;
import com.peluqueria.dao.TipoComboDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class TipoComboDAOImpl implements TipoComboDAO {

    @Override
    public List<TipoCombo> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM TipoCombo tc").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public TipoCombo getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(TipoCombo.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(TipoCombo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(TipoCombo obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub
    }

}
