package com.peluqueria.dao.impl;

import com.peluqueria.core.domain.RolFuncion;
import com.peluqueria.dao.RolFuncionDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class RolFuncionDAOImpl implements RolFuncionDAO {

    @Override
    public List<RolFuncion> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM RolFuncion rf").getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public RolFuncion getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(RolFuncion.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(RolFuncion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(RolFuncion obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
//        EntityManager em = null;
//        try {
//            em = getEmf().createEntityManager();
//            em.getTransaction().begin();
//            em.persist(obj);
//            em.getTransaction().commit();
//        } finally {
//            if (em != null) {
//                em.close();
//            }
//        }
    }

    @Override
    public List<RolFuncion> buscarRolFuncion(String cadenaIdRolFuncion,
            boolean b) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "select RF.* from seguridad.funcion F JOIN seguridad.rol_funcion RF ON F.id_funcion = RF.id_funcion JOIN seguridad.rol R ON RF.id_rol = R.id_rol "
                    + "WHERE R.id_rol IN " + cadenaIdRolFuncion;
            return em.createNativeQuery(cadena, RolFuncion.class).getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean crear(List<RolFuncion> listRolFuncion) {
        EntityManager em = null;
        boolean val = false;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            for (int i = 0; i < listRolFuncion.size(); i++) {
                RolFuncion rf = new RolFuncion();
                rf.setFuncion(listRolFuncion.get(i).getFuncion());
                rf.setRol(listRolFuncion.get(i).getRol());
                //
//                rf.setFechaAlta(listRolFuncion.get(i).getRol().getFechaAlta());
                rf.setFechaAlta(null);
//                rf.setFechaMod(listRolFuncion.get(i).getRol().getFechaMod());
                rf.setFechaMod(null);
                rf.setUsuAlta(listRolFuncion.get(i).getRol().getUsuAlta());
                rf.setUsuMod(listRolFuncion.get(i).getRol().getUsuMod());
                //
                em.persist(rf);
                em.refresh(rf);
            }
            em.getTransaction().commit();
            val = true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            val = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }

    @Override
    public boolean crearDeUno(RolFuncion rf) {
        EntityManager em = null;
        boolean val = false;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(rf);
            em.getTransaction().commit();
            val = true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            val = false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }

    @Override
    public List<RolFuncion> buscarPorRol(long parseLong) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM RolFuncion rf JOIN FETCH rf.rol r WHERE r.idRol=:idR")
                    .setParameter("idR", parseLong)
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminarByObj(RolFuncion rolF) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.remove(rolF);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("1) -->> " + ex.getLocalizedMessage());
            System.out.println("2) -->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<RolFuncion> buscarRolFuncion(long idR, String cIN) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String cadena = "select RF.* from seguridad.funcion F JOIN seguridad.rol_funcion RF ON F.id_funcion = RF.id_funcion JOIN seguridad.rol R ON RF.id_rol = R.id_rol "
                    + "WHERE F.descripcion IN " + cIN + " AND R.id_rol = " + idR + "";
            return em.createNativeQuery(cadena, RolFuncion.class).getResultList();
        } catch (Exception ex) {
            System.out.println("1) -->> " + ex.getLocalizedMessage());
            System.out.println("2) -->> " + ex.fillInStackTrace());
            return new ArrayList<>();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
