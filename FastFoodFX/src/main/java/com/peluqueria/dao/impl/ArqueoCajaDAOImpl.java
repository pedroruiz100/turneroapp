package com.peluqueria.dao.impl;

import static com.javafx.util.EMF.getEmf;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.ArqueoCaja;
import com.peluqueria.dao.ArqueoCajaDAO;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

@Repository
public class ArqueoCajaDAOImpl implements ArqueoCajaDAO {

    @Override
    public List<ArqueoCaja> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArqueoCaja ac ORDER BY ac.idArqueoCaja")
                    .getResultList();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArqueoCaja getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.find(ArqueoCaja.class, id);
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArqueoCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public void actualizar(ArqueoCaja obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public ArqueoCaja insertarObtenerObj(ArqueoCaja ar) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(ar);
            em.getTransaction().commit();
            em.refresh(ar);
            return ar;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public int recuperarZeta(long idCaja, String nro) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (int) (em
                    .createQuery(
                            "SELECT MAX(zeta) FROM ArqueoCaja ac WHERE ac.caja.idCaja=:idCaj and ac.nroTimbrado=:nro")
                    .setParameter("idCaj", idCaja).setParameter("nro", nro)
                    .getSingleResult());
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return 0;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean insertarObtenerEstado(ArqueoCaja arqueo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(arqueo);
            em.getTransaction().commit();
            em.refresh(arqueo);
            return true;
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public long recuperarNumMaxZeta() {
        int numFinal = 0;
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            numFinal = (int) (em
                    .createQuery(
                            "SELECT MAX(zeta) FROM ArqueoCaja")
                    .getSingleResult());
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return Long.valueOf(numFinal + "");
    }

    @Override
    public String recuperarPorCajaFecha(long idCaja, String fecha) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            String sql = "SELECT fecha_emision FROM caja.arqueo_caja WHERE id_caja=" + idCaja + " AND "
                    + "to_char(fecha_emision, 'YYYY-MM-DD')='" + fecha + "'";
            System.out.println("SQL -> " + sql);
            String fechaData = String.valueOf(em.createNativeQuery(sql)
                    .setMaxResults(1)
                    .getSingleResult());
            return fechaData;
        } finally {
            if (em != null) {
                em.close();
            }
        }

    }

    @Override
    public ArqueoCaja recuperarPorCajaFechaObj(long id, String fecha) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            ArqueoCaja ap = (ArqueoCaja) em
                    .createQuery(
                            "FROM ArqueoCaja ac WHERE ac.caja.idCaja=:id AND "
                            + "to_char(ac.fechaEmision, 'YYYY-MM-DD')=:fecha")
                    .setParameter("id", id).setParameter("fecha", fecha)
                    .setMaxResults(1).getSingleResult();
//		System.out.println("-->> " + ap.getFechaApertura());
            return ap;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ArqueoCaja> filtroFecha(String fechaInicio, String fechaFin) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em
                    .createQuery(
                            "FROM ArqueoCaja pd JOIN FETCH pd.caja caja JOIN FETCH pd.supervisor sup WHERE to_char(pd.fechaEmision,'YYYY-MM-DD') >= :fecIni "
                            + "and to_char(pd.fechaEmision,'YYYY-MM-DD') <= :fecFin ORDER BY pd.fechaEmision")
                    .setParameter("fecIni", fechaInicio)
                    .setParameter("fecFin", fechaFin)
                    //                    .setParameter("numCaja", Integer.parseInt(nroCaja))
                    .getResultList();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean verificarArqueoDia() {
        EntityManager em = null;
        boolean val = false;
        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            em = getEmf().createEntityManager();
            em.createQuery("FROM ArqueoCaja ap where (to_date(to_char(ap.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
                    .setParameter("fec", Utilidades.stringToUtilDate(format.format(date)))
                    .getResultList();
            val = true;
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return false;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }

    @Override
    public long getCantidadArqueoDia() {
        EntityManager em = null;
        long val = 0l;
        try {
            Date date = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            em = getEmf().createEntityManager();
            val = em.createQuery("FROM ArqueoCaja ap where (to_date(to_char(ap.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
                    .setParameter("fec", Utilidades.stringToUtilDate(format.format(date)))
                    .getResultList().size();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return 0l;
        } finally {
            if (em != null) {
                em.close();
            }
        }
        return val;
    }

}
