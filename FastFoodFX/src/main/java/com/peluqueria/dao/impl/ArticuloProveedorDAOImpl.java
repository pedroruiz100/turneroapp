package com.peluqueria.dao.impl;

import com.peluqueria.dao.ArticuloProveedorDAO;
import static com.javafx.util.EMF.getEmf;
import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.stereotype.Repository;

import com.peluqueria.core.domain.ArticuloProveedor;

@Repository
public class ArticuloProveedorDAOImpl implements ArticuloProveedorDAO {

//    @PersistenceContext
//    private EntityManager em;
    @Override
    public List<ArticuloProveedor> listar() {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloProveedor ap").getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public ArticuloProveedor getById(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return (ArticuloProveedor) em.createQuery("FROM ArticuloProveedor ap WHERE ap.idArticuloProveedor=:idArt")
                    .setParameter("idArt", id)
                    .getSingleResult();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void insertar(ArticuloProveedor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.persist(obj);
            em.getTransaction().commit();
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.getMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void actualizar(ArticuloProveedor obj) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.merge(obj);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminar(long id) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            ArticuloProveedor art = em.find(ArticuloProveedor.class, id);
            em.remove(art);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public void eliminarTodos(Long idArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            em.getTransaction().begin();
            em.createNativeQuery("DELETE FROM stock.articulo_proveedor WHERE id_articulo=" + idArticulo).getResultList();
            em.getTransaction().commit();
        } catch (Exception ex) {
//            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ArticuloProveedor> listarPorArticulo(Long idArticulo) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloProveedor ap JOIN FETCH ap.proveedor pro WHERE ap.articulo.idArticulo=:idArt")
                    .setParameter("idArt", idArticulo)
                    .getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public List<ArticuloProveedor> findByProveedor(String idProv) {
        EntityManager em = null;
        try {
            em = getEmf().createEntityManager();
            return em.createQuery("FROM ArticuloProveedor ap JOIN FETCH ap.proveedor pro JOIN FETCH ap.articulo art WHERE pro.idProveedor=:idArt")
                    .setParameter("idArt", Long.parseLong(idProv))
                    .getResultList();
        } catch (Exception ex) {
            return null;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

}
