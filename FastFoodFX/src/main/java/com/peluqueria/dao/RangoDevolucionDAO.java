package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoDevolucion;

public interface RangoDevolucionDAO extends CRUDGenericDAO<RangoDevolucion> {

    long actualizarRecuperarRangoActual(long idRango);
    
    long recuperarRangoActual(long id);

}
