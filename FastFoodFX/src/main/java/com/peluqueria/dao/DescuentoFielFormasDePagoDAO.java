/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielFormasDePago;

/**
 *
 * @author ExcelsisWalker
 */
public interface DescuentoFielFormasDePagoDAO extends CRUDGenericDAO<DescuentoFielFormasDePago> {

}
