package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoEfectivo;

public interface RangoEfectivoDAO extends CRUDGenericDAO<RangoEfectivo> {

    long actualizarRecuperarRangoActual(long idRango);

}
