package com.peluqueria.dao;

import com.peluqueria.core.domain.Existencia;
import java.util.List;

public interface ExistenciaDAO extends CRUDGenericDAO<Existencia> {

    public Existencia listarPorDepositoArticulo(long i, Long idArticulo);

//    List<Nf2Sfamilia> listarNombreId();
//
//    List<Nf2Sfamilia> listarFuncionarioNombreId();
//
//    List<Nf2Sfamilia> listarPromoTemp(String inicio, String fin);
//
//    public List<Nf2Sfamilia> listarPorNf(long idNf1);
    public List<Existencia> listarPorArticulo(Long idArticulo);

    public long getCantidadArticuloCC(Long codArticulo);

    public long getCantidadArticuloSL(Long codArticulo);

    public long getCantidadArticuloSC(Long codArticulo);

    public Existencia filtrarPorArtDeposito(long idArt, long idDeposito);

}
