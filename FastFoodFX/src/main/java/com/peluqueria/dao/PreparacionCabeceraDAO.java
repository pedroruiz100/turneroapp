/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.PreparacionCabecera;
import com.peluqueria.core.domain.PreparacionCabecera;
import java.time.LocalDate;
import java.util.List;

public interface PreparacionCabeceraDAO extends CRUDGenericDAO<PreparacionCabecera> {

    PreparacionCabecera insertarObtenerObj(PreparacionCabecera aperturaCaja);

//    String recuperarPorCajaFecha2(long idCaja, String fecha);
//    AperturaCaja recuperarPorCajaFecha(long id, String fecha);
    public boolean insertarEstado(PreparacionCabecera pro);

    public boolean actualizarEstado(PreparacionCabecera pro);

    public List<PreparacionCabecera> listarPorFechas(LocalDate firstDay, LocalDate lastDay);

    public List<PreparacionCabecera> filtroFecha(String fechaInicio, String fechaFin);

    public PreparacionCabecera recuperarUltimo();

    public List<String> listarProveedorDistinct();

}
