/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoClientePendiente;

/**
 *
 * @author PC
 */
public interface RangoClientePendienteDAO extends CRUDGenericDAO<RangoClientePendiente> {

    long actualizarRecuperarRangoActual(long idRango);
}
