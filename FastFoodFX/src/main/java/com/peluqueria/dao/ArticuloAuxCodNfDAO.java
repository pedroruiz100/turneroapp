package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloAuxCodNf;

public interface ArticuloAuxCodNfDAO extends CRUDGenericDAO<ArticuloAuxCodNf> {

}
