package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf6;

public interface FuncionarioNf6DAO extends CRUDGenericDAO<FuncionarioNf6> {

    List<FuncionarioNf6> listarFetchNf6(long limRow, long offSet);

    List<FuncionarioNf6> listarFetchFilterNf6(long limRow, long offSet, String nombre);

    Long rowCountNf6();

    long rowCountFilterNf6(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf6 insertarObtenerObjeto(FuncionarioNf6 funcionarioNf6);

    public boolean actualizarObtenerEstado(FuncionarioNf6 funcionarioNf6);
    
    boolean validacionInsercion(long idNf);

}
