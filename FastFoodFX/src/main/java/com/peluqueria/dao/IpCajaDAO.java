package com.peluqueria.dao;

import com.peluqueria.core.domain.IpCaja;

public interface IpCajaDAO extends CRUDGenericDAO<IpCaja> {

}
