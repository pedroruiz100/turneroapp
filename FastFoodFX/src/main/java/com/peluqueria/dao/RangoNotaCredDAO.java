package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoNotaCred;

public interface RangoNotaCredDAO extends CRUDGenericDAO<RangoNotaCred> {

    long actualizarObtenerRango(long idRango);

}
