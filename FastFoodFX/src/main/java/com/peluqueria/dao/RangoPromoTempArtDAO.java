package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoPromoTempArt;

public interface RangoPromoTempArtDAO extends CRUDGenericDAO<RangoPromoTempArt> {

    boolean insertarObtenerEstado(RangoPromoTempArt rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
