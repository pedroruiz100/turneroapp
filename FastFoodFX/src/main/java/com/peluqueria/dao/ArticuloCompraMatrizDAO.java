package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloCompraMatriz;
import java.time.LocalDate;

/**
 *
 * @author ExcelsisWalker
 */
public interface ArticuloCompraMatrizDAO extends CRUDGenericDAO<ArticuloCompraMatriz> {

//    public List<ArticuloCompraMatriz> listarPorCiudad(long idCiudad);
    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public ArticuloCompraMatriz getByCodFecha(String toString, LocalDate nowLast, boolean cc, boolean sc, boolean sl);
}
