package com.peluqueria.dao;

import com.peluqueria.core.domain.CancelacionProducto;

/**
 *
 * @author ADMIN
 */
public interface CancelacionProductoDAO extends CRUDGenericDAO<CancelacionProducto> {

}
