package com.peluqueria.dao;

import com.peluqueria.core.domain.PedidoCab;
import java.sql.Date;
import java.util.List;

public interface PedidoCabDAO extends CRUDGenericDAO<PedidoCab> {

    PedidoCab insertarObtenerObj(PedidoCab recep);

    public PedidoCab getByNroOrden(String text);

    public List<PedidoCab> listarPorFecha(Date desde, Date hasta);

    public List<PedidoCab> filtrarPorProveedorSucursalOc(String proveedor, String sucursal, String oc);

    public PedidoCab listarPorOrden(String text);

    public PedidoCab getByOC(String oc);

    public PedidoCab listarPorOrdenSucursal(String text, String selectedItem);

    public List<PedidoCab> filtrarPorProveedorOc(String proveedor, String oc);

    public PedidoCab listarPorCompraCab(String toString);

}
