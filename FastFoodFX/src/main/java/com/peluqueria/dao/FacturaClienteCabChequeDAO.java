package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabCheque;
import java.util.List;

public interface FacturaClienteCabChequeDAO extends CRUDGenericDAO<FacturaClienteCabCheque> {

    void insercionMasiva(FacturaClienteCabCheque fac, long id);

    public List<FacturaClienteCabCheque> listarPorFactura(long parseLong);

}
