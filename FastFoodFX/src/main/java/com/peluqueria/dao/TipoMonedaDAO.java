package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoMoneda;

public interface TipoMonedaDAO extends CRUDGenericDAO<TipoMoneda> {

}
