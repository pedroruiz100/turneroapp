package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoRetiroCompra;

public interface RangoRetiroCompraDAO extends CRUDGenericDAO<RangoRetiroCompra> {

    long actualizarObtenerRango(long idRango);

    public RangoRetiroCompra actualizarObtenerRangoObjectActual(long idRango1);

}
