package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabTarjFiel;

public interface FacturaClienteCabTarjFielDAO extends CRUDGenericDAO<FacturaClienteCabTarjFiel> {

    FacturaClienteCabTarjFiel insertarObtenerObjeto(
            FacturaClienteCabTarjFiel facturaClienteCabTarjFiel);
}
