package com.peluqueria.dao;

import com.peluqueria.core.domain.MotivoRetiroDinero;

/**
 *
 * @author ADMIN
 */
public interface MotivoRetiroDineroDAO extends CRUDGenericDAO<MotivoRetiroDinero> {

}
