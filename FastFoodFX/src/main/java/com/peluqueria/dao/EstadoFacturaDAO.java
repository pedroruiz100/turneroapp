package com.peluqueria.dao;

import com.peluqueria.core.domain.EstadoFactura;

public interface EstadoFacturaDAO extends CRUDGenericDAO<EstadoFactura> {

}
