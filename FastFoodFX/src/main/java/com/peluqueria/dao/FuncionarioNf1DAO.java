package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf1;

public interface FuncionarioNf1DAO extends CRUDGenericDAO<FuncionarioNf1> {

    List<FuncionarioNf1> listarFetchNf1(long limRow, long offSet);

    List<FuncionarioNf1> listarFetchFilterNf1(long limRow, long offSet, String nombre);

    Long rowCountNf1();

    long rowCountFilterNf1(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf1 insertarObtenerObjeto(FuncionarioNf1 funcionarioNf1);

    public boolean actualizarObtenerEstado(FuncionarioNf1 funcionarioNf1);

    boolean validacionInsercion(long idNf);

}
