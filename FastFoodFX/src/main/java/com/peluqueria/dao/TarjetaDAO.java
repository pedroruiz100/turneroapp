package com.peluqueria.dao;

import com.peluqueria.core.domain.Tarjeta;
import java.util.List;

public interface TarjetaDAO extends CRUDGenericDAO<Tarjeta> {

    List<Tarjeta> listarNombreId();
}
