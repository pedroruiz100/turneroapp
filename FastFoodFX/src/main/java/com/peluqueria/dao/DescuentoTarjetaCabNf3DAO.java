package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf3;

public interface DescuentoTarjetaCabNf3DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf3> {

    void insercionMasiva(DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3);
}
