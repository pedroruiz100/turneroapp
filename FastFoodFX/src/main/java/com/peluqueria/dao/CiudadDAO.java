package com.peluqueria.dao;

import com.peluqueria.core.domain.Ciudad;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface CiudadDAO extends CRUDGenericDAO<Ciudad> {

    public List<Ciudad> listarPorCiudad(long idCiudad);

}
