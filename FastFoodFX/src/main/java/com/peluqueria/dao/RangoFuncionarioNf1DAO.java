package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf1;

public interface RangoFuncionarioNf1DAO extends CRUDGenericDAO<RangoFuncionarioNf1> {

    boolean insertarObtenerEstado(RangoFuncionarioNf1 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
