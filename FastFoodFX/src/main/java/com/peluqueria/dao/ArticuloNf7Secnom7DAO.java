package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf7Secnom7;

public interface ArticuloNf7Secnom7DAO extends CRUDGenericDAO<ArticuloNf7Secnom7> {

    public ArticuloNf7Secnom7 getByIdArticulo(long id);

    public void deleteArticulo(long idArt);

    public ArticuloNf7Secnom7 listarArticulo(long idArt);

}
