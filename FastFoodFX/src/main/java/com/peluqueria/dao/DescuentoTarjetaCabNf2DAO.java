package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf2;

public interface DescuentoTarjetaCabNf2DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf2> {

    void insercionMasiva(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2);
}
