package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabRetencion;

public interface FacturaClienteCabRetencionDAO extends CRUDGenericDAO<FacturaClienteCabRetencion> {

}
