package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFactura;
import com.peluqueria.core.domain.RangoTarjetaConvenio;

public interface RangoTarjetaConvenioDAO extends CRUDGenericDAO<RangoTarjetaConvenio> {

    boolean insertarObtenerEstado(RangoFactura rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
