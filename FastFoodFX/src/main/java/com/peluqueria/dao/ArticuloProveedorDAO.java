package com.peluqueria.dao;

import java.util.List;
import com.peluqueria.core.domain.ArticuloProveedor;

public interface ArticuloProveedorDAO extends CRUDGenericDAO<ArticuloProveedor> {

    public void eliminarTodos(Long idArticulo);

    public List<ArticuloProveedor> listarPorArticulo(Long idArticulo);

    public List<ArticuloProveedor> findByProveedor(String toString);

}
