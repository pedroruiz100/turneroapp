package com.peluqueria.dao;

import com.peluqueria.core.domain.DescMonedaExtranjera;

public interface DescMonedaExtranjeraDAO extends CRUDGenericDAO<DescMonedaExtranjera> {

}
