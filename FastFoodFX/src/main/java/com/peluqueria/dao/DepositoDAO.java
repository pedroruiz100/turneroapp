package com.peluqueria.dao;

import com.peluqueria.core.domain.Deposito;

public interface DepositoDAO extends CRUDGenericDAO<Deposito> {

    public long getByDescripcion(String selectedItem);

}
