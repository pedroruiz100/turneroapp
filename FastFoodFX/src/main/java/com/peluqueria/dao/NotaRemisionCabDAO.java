package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaRemisionCab;

public interface NotaRemisionCabDAO extends CRUDGenericDAO<NotaRemisionCab> {

}
