package com.peluqueria.dao;

import com.peluqueria.core.domain.Funcionario;
import java.util.List;

public interface FuncionarioDAO extends CRUDGenericDAO<Funcionario> {

    List<Funcionario> filtrarNomApeCi(String nombre, String apellido, String ci);

    Funcionario listarFuncionarioPorCi(String ci);

    public List<Funcionario> listarFuncionariosDisponibles(String filtro);

}
