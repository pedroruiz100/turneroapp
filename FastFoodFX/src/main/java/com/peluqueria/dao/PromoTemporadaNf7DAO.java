package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf7;

public interface PromoTemporadaNf7DAO extends CRUDGenericDAO<PromoTemporadaNf7> {

    void insercionMasiva(PromoTemporadaNf7 promoTemporadaNf7, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf7 promoTemporadaNf7);
}
