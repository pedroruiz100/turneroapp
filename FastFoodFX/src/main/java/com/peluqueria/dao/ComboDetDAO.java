package com.peluqueria.dao;

import com.peluqueria.core.domain.ComboDet;

public interface ComboDetDAO extends CRUDGenericDAO<ComboDet> {

}
