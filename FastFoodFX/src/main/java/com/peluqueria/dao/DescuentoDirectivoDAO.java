package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoDirectivo;

public interface DescuentoDirectivoDAO extends CRUDGenericDAO<DescuentoDirectivo> {

}
