package com.peluqueria.dao;

import com.peluqueria.core.domain.DescTarjeta;
import java.math.BigInteger;
import java.util.List;

public interface DescTarjetaDAO extends CRUDGenericDAO<DescTarjeta> {

    public List<DescTarjeta> filtroFechaDescTarjeta(String fechaInicio, String fechaFin, String nroCaja);

    public List<DescTarjeta> filtroFechaDescTarjeta(String fechaInicio, String fechaFin, String numCaja, String idTarjeta);

    public List<BigInteger> recuperarEntidadTarjeta(String toString, String toString0);

}
