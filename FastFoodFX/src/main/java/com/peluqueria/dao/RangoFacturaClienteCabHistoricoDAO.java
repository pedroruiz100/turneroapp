/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFacturaClienteCabHistorico;

/**
 *
 * @author ExcelsisWalker
 */
public interface RangoFacturaClienteCabHistoricoDAO extends CRUDGenericDAO<RangoFacturaClienteCabHistorico> {

    boolean insertarObtenerEstado(RangoFacturaClienteCabHistorico rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
