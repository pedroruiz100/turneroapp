package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import java.time.LocalDate;
import java.util.List;
import javax.persistence.metamodel.SingularAttribute;

public interface FacturaClienteCabDAO extends CRUDGenericDAO<FacturaClienteCab> {

    void cancelarFactura(long id, String usuMod);

    void actualizarMontoVenta(int monto, long id);

    FacturaClienteCab insertarObtenerObjeto(FacturaClienteCab fac);

    long getCantidadArticuloCC(String codArt, LocalDate desde, LocalDate hasta);

    long getCantidadArticuloSL(String codArt, LocalDate desde, LocalDate hasta);

    long getCantidadArticuloSC(String codArt, LocalDate desde, LocalDate hasta);

    FacturaClienteCab actualizarObtenerObjeto(
            FacturaClienteCab facturaClienteCab);

    public List<FacturaClienteCab> filtroFechaDescuento(String fechaInicio, String fechaFin, String nroCaja);

    public List<FacturaClienteCab> filtroFecha(String fechaInicio, String fechaFin, String selectedItem);

    public List<FacturaClienteCab> filtroNroFact(String text, String selectedItem);

    public List<FacturaClienteCab> filtroFechaDescuento(String numFactura, String fechaInicio, String fechaFin, String selectedItem);

    public List<FacturaClienteCab> filtroSolFecha(String toString, String toString0);

    public List<FacturaClienteCab> filtroFechaHoy();

}
