package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielDet;
import java.util.List;

public interface DescuentoFielDetDAO extends CRUDGenericDAO<DescuentoFielDet> {

    List<DescuentoFielDet> listarLimitado();

    boolean insertarObtenerEstado(DescuentoFielDet desc);

}
