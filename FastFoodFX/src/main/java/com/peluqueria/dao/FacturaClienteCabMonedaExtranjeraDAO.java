package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabMonedaExtranjera;
import java.util.List;

public interface FacturaClienteCabMonedaExtranjeraDAO extends CRUDGenericDAO<FacturaClienteCabMonedaExtranjera> {

    public abstract void insercionMasiva(FacturaClienteCabMonedaExtranjera fac, int num);

    public List<FacturaClienteCabMonedaExtranjera> listarPorFactura(long parseLong);

}
