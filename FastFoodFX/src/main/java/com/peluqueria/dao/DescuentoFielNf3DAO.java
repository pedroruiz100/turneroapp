package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf3;

public interface DescuentoFielNf3DAO extends CRUDGenericDAO<DescuentoFielNf3> {

    void insercionMasiva(DescuentoFielNf3 descuentoFielNf3, long i);

    boolean insercionMasivaEstado(DescuentoFielNf3 descuentoFielNf3);

    boolean insertarObtenerEstado(DescuentoFielNf3 descuentoFielNf3);
}
