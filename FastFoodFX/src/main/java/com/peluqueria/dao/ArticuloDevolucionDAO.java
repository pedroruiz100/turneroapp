package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloDevolucion;
import java.time.LocalDate;
import java.util.List;

public interface ArticuloDevolucionDAO extends CRUDGenericDAO<ArticuloDevolucion> {

    void insercionMasiva(ArticuloDevolucion facDet, long i);

    public List<ArticuloDevolucion> listarPorFechaRecepcionActual(LocalDate value, LocalDate value0, String idProveedor);

    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public List<ArticuloDevolucion> listarPorCabecera(long idPedCab);

    public boolean eliminarPorcabecera(Long idPedidoCab);

    public ArticuloDevolucion getByIdPedidoCabAndCodArt(long parseLong, Long codArticulo);

    public ArticuloDevolucion getByIdFacturaCab(Long idFacturaCompraCab);

    public List<ArticuloDevolucion> listarByIdFacturaCab(String idFacturaCompraCab);

    public List<ArticuloDevolucion> getByNroDevolucion(String text);

}
