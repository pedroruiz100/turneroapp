package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf4;

public interface PromoTemporadaNf4DAO extends CRUDGenericDAO<PromoTemporadaNf4> {

    void insercionMasiva(PromoTemporadaNf4 promoTemporadaNf4, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf4 promoTemporadaNf4);
}
