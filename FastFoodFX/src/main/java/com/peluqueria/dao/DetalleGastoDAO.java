package com.peluqueria.dao;

import com.peluqueria.core.domain.DetalleGasto;
import java.util.List;

public interface DetalleGastoDAO extends CRUDGenericDAO<DetalleGasto> {

    void insercionMasiva(DetalleGasto fac, long id);

    public List<DetalleGasto> listarPorFactura(long parseLong);

    public List<DetalleGasto> listarTodosMayoresACero();

    public List<DetalleGasto> listarPorProveedorMayorAcero(String prov);

    public List<DetalleGasto> listarPorFecha(String fechaInicio, String fechaFin);

    public List<DetalleGasto> listarPorFechaParaVenta(String fechaInicio, String fechaFin);

    public List<DetalleGasto> listarPorFechaParaPreparacion(String fechaInicio, String fechaFin);

    public boolean eliminarObtenerEstado(long parseLong);

}
