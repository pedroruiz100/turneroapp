package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaArt;
import java.sql.Timestamp;
import java.util.List;

public interface PromoTemporadaArtDAO extends CRUDGenericDAO<PromoTemporadaArt> {

    long rowCount();

    long rowCountFilter(String nombre, String fechaInicio, String fechaFin);

    List<PromoTemporadaArt> listarFETCH(long limite, long inicio);

    List<PromoTemporadaArt> listarFETCHFilter(long limite, long inicio,
            String nombre, String fechaInicio, String fechaFin);

    PromoTemporadaArt insertarObtenerObjeto(PromoTemporadaArt promoTemporadaArt);

    List<PromoTemporadaArt> listarPorFechaActual(Timestamp tiempoActual);

    void actualizarPorFecha(PromoTemporadaArt pro, int numeroActual);

    void baja();

    boolean actualizarObtenerEstado(PromoTemporadaArt fromPromoTemporadaArtDTO);

    boolean bajasLocal(String u, Timestamp ts);
}
