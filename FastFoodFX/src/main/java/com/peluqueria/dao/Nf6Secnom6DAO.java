package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf6Secnom6;
import java.util.List;

public interface Nf6Secnom6DAO extends CRUDGenericDAO<Nf6Secnom6> {

    List<Nf6Secnom6> listarNombreId();

    List<Nf6Secnom6> listarFuncionarioNombreId();

    List<Nf6Secnom6> listarPromoTemp(String inicio, String fin);
    
    public List<Nf6Secnom6> listarPorNf(long id);

}
