package com.peluqueria.dao;

import com.peluqueria.core.domain.PreparacionDetalle;
import java.util.List;

public interface PreparacionDetalleDAO extends CRUDGenericDAO<PreparacionDetalle> {

    void insercionMasiva(PreparacionDetalle fac, long id);

    public List<PreparacionDetalle> listarPorFactura(long parseLong);

    public List<PreparacionDetalle> listarTodosMayoresACero();

    public List<PreparacionDetalle> listarPorProveedorMayorAcero(String prov);

    public List<PreparacionDetalle> listarPorFecha(String fechaInicio, String fechaFin);

}
