package com.peluqueria.dao;

import com.peluqueria.core.domain.ServPendiente;
import java.util.List;

public interface ServPendienteDAO extends CRUDGenericDAO<ServPendiente> {

	ServPendiente insertarObtenerObj(ServPendiente fromServPendienteAsociado);

    public List<ServPendiente> listarTodos();

}
