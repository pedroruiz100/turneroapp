package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoTesoreria;

public interface RangoTesoreriaDAO extends CRUDGenericDAO<RangoTesoreria> {

    long actualizarObtenerRango(long idRango);

}
