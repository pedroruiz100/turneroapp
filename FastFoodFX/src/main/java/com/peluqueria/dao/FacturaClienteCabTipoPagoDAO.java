package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabTipoPago;

public interface FacturaClienteCabTipoPagoDAO extends CRUDGenericDAO<FacturaClienteCabTipoPago> {

    FacturaClienteCabTipoPago insertarObtenerObj(
            FacturaClienteCabTipoPago facturaClienteCabTipoPago);
}
