package com.peluqueria.dao;

import com.peluqueria.core.domain.FamiliaTarj;

public interface FamiliaTarjDAO extends CRUDGenericDAO<FamiliaTarj> {

}
