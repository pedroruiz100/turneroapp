package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaConvenioCab;
import java.sql.Timestamp;
import java.util.List;

public interface DescuentoTarjetaConvenioCabDAO extends
        CRUDGenericDAO<DescuentoTarjetaConvenioCab> {

    List<DescuentoTarjetaConvenioCab> listarFETCH(long limRow, long offSet);

    Long rowCount();

    DescuentoTarjetaConvenioCab insertarObtenerObjeto(
            DescuentoTarjetaConvenioCab dfc);

    List<DescuentoTarjetaConvenioCab> listarFETCHTarjeta(long limRow,
            long offSet, String nombre);

    Long rowCountFiler(String nombre);

    boolean bajasLocal(String u, Timestamp ts);

    void bajas();

    boolean actualizarObtenerEstado(DescuentoTarjetaConvenioCab fromDescuentoTarjetaConvenioCabDTO);

}
