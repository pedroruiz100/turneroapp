package com.peluqueria.dao;

import com.peluqueria.core.domain.MotivoTraslado;

public interface MotivoTrasladoDAO extends CRUDGenericDAO<MotivoTraslado> {

}
