package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf6;

public interface RangoFuncionarioNf6DAO extends CRUDGenericDAO<RangoFuncionarioNf6> {

    boolean insertarObtenerEstado(RangoFuncionarioNf6 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
