package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielNf2;

public interface DescuentoFielNf2DAO extends CRUDGenericDAO<DescuentoFielNf2> {

    void insercionMasiva(DescuentoFielNf2 descuentoFielNf2, long i);

    boolean insercionMasivaEstado(DescuentoFielNf2 descuentoFielNf2);

    boolean insertarObtenerEstado(DescuentoFielNf2 descuentoFielNf2);
}
