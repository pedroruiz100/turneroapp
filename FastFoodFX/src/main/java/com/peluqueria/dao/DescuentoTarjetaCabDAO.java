package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCab;
import java.sql.Timestamp;
import java.util.List;

public interface DescuentoTarjetaCabDAO extends
        CRUDGenericDAO<DescuentoTarjetaCab> {

    List<DescuentoTarjetaCab> listarFETCH(long limRow, long offSet);

    Long rowCount();

    DescuentoTarjetaCab insertarObtenerObjeto(DescuentoTarjetaCab dfc);

    List<DescuentoTarjetaCab> listarFETCHTarjeta(long limRow, long offSet,
            String nombre, String fechaInicio, String fechaFin);

    Long rowCountFiler(String nombre, String fechaInicio, String fechaFin);

    List<DescuentoTarjetaCab> listarPorFechaActual(Timestamp tiempoActual);

    void actualizarPorFecha(DescuentoTarjetaCab desc, int i);

    void bajas();

    public boolean bajasLocal(String u, Timestamp ts);

    public boolean actualizarObtenerEstado(DescuentoTarjetaCab fromDescuentoTarjetaCabDTO);

    boolean verificandoInsercion(String descripcion, Timestamp fecha);
}
