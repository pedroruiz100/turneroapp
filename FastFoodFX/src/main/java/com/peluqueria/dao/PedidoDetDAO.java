package com.peluqueria.dao;

import com.peluqueria.core.domain.PedidoDet;
import java.time.LocalDate;
import java.util.List;

public interface PedidoDetDAO extends CRUDGenericDAO<PedidoDet> {

    void insercionMasiva(PedidoDet facDet, long i);

    public List<PedidoDet> listarPorFechaRecepcionActual(LocalDate value, LocalDate value0, String idProveedor);

    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public List<PedidoDet> listarPorCabecera(long idPedCab);

    public boolean eliminarPorcabecera(Long idPedidoCab);

    public long recuperarTotal(Long idPedidoCab);

    public long recuperarTotalCC(Long idPedidoCab);

    public long recuperarTotalSC(Long idPedidoCab);

    public long recuperarTotalSL(Long idPedidoCab);

}
