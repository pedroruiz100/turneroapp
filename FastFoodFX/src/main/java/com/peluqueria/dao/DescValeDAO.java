package com.peluqueria.dao;

import com.peluqueria.core.domain.DescVale;

public interface DescValeDAO extends CRUDGenericDAO<DescVale> {

}
