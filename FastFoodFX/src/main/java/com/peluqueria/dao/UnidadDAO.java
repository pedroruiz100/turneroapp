package com.peluqueria.dao;

import com.peluqueria.core.domain.Unidad;

public interface UnidadDAO extends CRUDGenericDAO<Unidad> {

}
