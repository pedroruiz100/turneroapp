package com.peluqueria.dao;

import com.peluqueria.core.domain.TipoCombo;

public interface TipoComboDAO extends CRUDGenericDAO<TipoCombo> {

}
