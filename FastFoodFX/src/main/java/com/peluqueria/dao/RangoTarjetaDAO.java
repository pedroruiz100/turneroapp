package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFactura;
import com.peluqueria.core.domain.RangoTarjeta;

public interface RangoTarjetaDAO extends CRUDGenericDAO<RangoTarjeta> {

    boolean insertarObtenerEstado(RangoFactura rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

    long actualizarRecuperarRangoActual(long idRango);

}
