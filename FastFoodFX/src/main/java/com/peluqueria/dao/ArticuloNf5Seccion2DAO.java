package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf5Seccion2;

public interface ArticuloNf5Seccion2DAO extends CRUDGenericDAO<ArticuloNf5Seccion2> {

    public ArticuloNf5Seccion2 getByIdArticulo(long id);

    public ArticuloNf5Seccion2 listarArticulo(long idArt);

}
