package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoPromoTemp;

public interface RangoPromoTempDAO extends CRUDGenericDAO<RangoPromoTemp> {

    boolean insertarObtenerEstado(RangoPromoTemp rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
