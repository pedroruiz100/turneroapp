package com.peluqueria.dao;

import com.peluqueria.core.domain.Nf4Seccion1;
import java.util.List;

public interface Nf4Seccion1DAO extends CRUDGenericDAO<Nf4Seccion1> {

    List<Nf4Seccion1> listarNombreId();

    List<Nf4Seccion1> listarFuncionarioNombreId();

    List<Nf4Seccion1> listarPromoTemp(String inicio, String fin);

    public List<Nf4Seccion1> listarPorNf(long id);

}
