package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoDescTarjConvCab;

public interface RangoDescTarjConvCabDAO extends CRUDGenericDAO<RangoDescTarjConvCab> {

    long actualizarRecuperarRangoActual(long idRango);

}
