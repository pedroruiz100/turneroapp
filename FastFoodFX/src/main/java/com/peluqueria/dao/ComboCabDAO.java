package com.peluqueria.dao;

import com.peluqueria.core.domain.ComboCab;

public interface ComboCabDAO extends CRUDGenericDAO<ComboCab> {

}
