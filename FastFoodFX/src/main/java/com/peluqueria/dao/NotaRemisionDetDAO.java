package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaRemisionDet;

public interface NotaRemisionDetDAO extends CRUDGenericDAO<NotaRemisionDet> {

}
