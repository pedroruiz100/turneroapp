package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaCreditoCabProveedor;

public interface NotaCreditoCabProveedorDAO extends CRUDGenericDAO<NotaCreditoCabProveedor> {

}
