package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabPromoTemp;

public interface FacturaClienteCabPromoTempDAO extends CRUDGenericDAO<FacturaClienteCabPromoTemp> {

    FacturaClienteCabPromoTemp insertarObtenerObjeto(
            FacturaClienteCabPromoTemp facturaClienteCabPromoTemporada);

}
