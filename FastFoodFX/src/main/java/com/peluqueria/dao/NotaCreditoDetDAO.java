package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaCreditoDet;

public interface NotaCreditoDetDAO extends CRUDGenericDAO<NotaCreditoDet> {

}
