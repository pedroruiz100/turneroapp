package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabFuncionario;

public interface FacturaClienteCabFuncionarioDAO extends CRUDGenericDAO<FacturaClienteCabFuncionario> {

    FacturaClienteCabFuncionario insertarObtenerObjeto(
            FacturaClienteCabFuncionario facturaClienteCabFuncionario);

}
