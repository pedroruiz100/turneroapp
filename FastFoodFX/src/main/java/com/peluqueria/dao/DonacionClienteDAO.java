package com.peluqueria.dao;

import com.peluqueria.core.domain.DonacionCliente;

public interface DonacionClienteDAO extends CRUDGenericDAO<DonacionCliente> {

}
