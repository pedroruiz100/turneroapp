package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf1;

public interface DescuentoTarjetaCabNf1DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf1> {

    void insercionMasiva(DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1);
}
