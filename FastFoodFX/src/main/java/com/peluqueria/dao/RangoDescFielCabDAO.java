package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoDescFielCab;

public interface RangoDescFielCabDAO extends CRUDGenericDAO<RangoDescFielCab> {

    long actualizarRecuperarRangoActual(long idRango);

}
