package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf5;

public interface DescuentoTarjetaCabNf5DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf5> {

    void insercionMasiva(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5);
}
