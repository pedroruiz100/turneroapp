package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoTarjetaCabNf4;

public interface DescuentoTarjetaCabNf4DAO extends CRUDGenericDAO<DescuentoTarjetaCabNf4> {

    void insercionMasiva(DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4, long i);

    boolean insercionMasivaEstado(DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4);

    public boolean insertarObtenerEstado(DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4);
}
