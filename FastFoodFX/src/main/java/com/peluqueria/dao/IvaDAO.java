package com.peluqueria.dao;

import com.peluqueria.core.domain.Iva;

public interface IvaDAO extends CRUDGenericDAO<Iva> {

}
