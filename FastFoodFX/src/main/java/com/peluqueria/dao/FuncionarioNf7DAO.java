package com.peluqueria.dao;

import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.FuncionarioNf7;

public interface FuncionarioNf7DAO extends CRUDGenericDAO<FuncionarioNf7> {

    List<FuncionarioNf7> listarFetchNf7(long limRow, long offSet);

    List<FuncionarioNf7> listarFetchFilterNf7(long limRow, long offSet, String nombre);

    Long rowCountNf7();

    long rowCountFilterNf7(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    FuncionarioNf7 insertarObtenerObjeto(FuncionarioNf7 funcionarioNf7);

    public boolean actualizarObtenerEstado(FuncionarioNf7 funcionarioNf7);
    
    boolean validacionInsercion(long idNf);

}
