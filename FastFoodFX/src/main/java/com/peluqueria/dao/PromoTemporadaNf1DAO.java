package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf1;

public interface PromoTemporadaNf1DAO extends CRUDGenericDAO<PromoTemporadaNf1> {

    void insercionMasiva(PromoTemporadaNf1 promoTemporadaNf1, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf1 promoTemporadaNf1);
}
