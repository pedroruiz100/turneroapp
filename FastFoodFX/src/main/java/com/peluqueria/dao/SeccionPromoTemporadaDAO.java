package com.peluqueria.dao;

import com.peluqueria.core.domain.SeccionPromoTemporada;

public interface SeccionPromoTemporadaDAO extends
        CRUDGenericDAO<SeccionPromoTemporada> {

    void insercionMasiva(SeccionPromoTemporada secDTO, long i);

    boolean insercionMasivaEstado(SeccionPromoTemporada secDTO);

}
