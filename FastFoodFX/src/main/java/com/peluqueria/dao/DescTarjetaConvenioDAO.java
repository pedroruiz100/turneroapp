package com.peluqueria.dao;

import com.peluqueria.core.domain.DescTarjetaConvenio;

public interface DescTarjetaConvenioDAO extends CRUDGenericDAO<DescTarjetaConvenio> {

}
