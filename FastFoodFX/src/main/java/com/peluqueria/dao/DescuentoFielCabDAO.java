package com.peluqueria.dao;

import com.peluqueria.core.domain.DescuentoFielCab;
import java.sql.Timestamp;
import java.util.List;

public interface DescuentoFielCabDAO extends CRUDGenericDAO<DescuentoFielCab> {

    List<DescuentoFielCab> listarFETCH(long limRow, long offSet);

    Long rowCount();

    DescuentoFielCab insertarObtenerObjeto(DescuentoFielCab dfc);

    boolean insertarObtenerEstado(DescuentoFielCab dfc);

    List<DescuentoFielCab> listarFETCHSeccion(long limRow, long offSet,
            String nombre);

    Long rowCountFiler(String nombre);

    void bajas();

    boolean bajasLocal(String u, Timestamp ts);

    boolean actualizarObtenerEstado(DescuentoFielCab dfc);

    Long rowCountNf1();

    Long rowCountNf2();

    Long rowCountNf3();

    Long rowCountNf4();

    Long rowCountNf5();

    Long rowCountNf6();

    Long rowCountNf7();

    List<DescuentoFielCab> listarFetchNf1(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf2(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf3(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf4(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf5(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf6(long limRow, long offSet);

    List<DescuentoFielCab> listarFetchNf7(long limRow, long offSet);

    Long rowCountFilterNf1(String nombre);

    Long rowCountFilterNf2(String nivelFamilia);

    Long rowCountFilterNf3(String nivelFamilia);

    Long rowCountFilterNf4(String nivelFamilia);

    Long rowCountFilterNf5(String nivelFamilia);

    Long rowCountFilterNf6(String nivelFamilia);

    Long rowCountFilterNf7(String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf1(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf2(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf3(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf4(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf5(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf6(long limRow, long offSet,
            String nivelFamilia);

    List<DescuentoFielCab> listarFetchFilterNf7(long limRow, long offSet,
            String nivelFamilia);
    
    boolean validacionInsercion(int nf, long idNf);

}
