package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFactura;

public interface RangoFacturaDAO extends CRUDGenericDAO<RangoFactura> {

    boolean insertarObtenerEstado(RangoFactura rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
