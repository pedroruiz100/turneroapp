package com.peluqueria.dao;

import com.peluqueria.core.domain.MotivoCancelacionProducto;

/**
 *
 * @author ADMIN
 */
public interface MotivoCancelacionProductoDAO extends CRUDGenericDAO<MotivoCancelacionProducto> {

}
