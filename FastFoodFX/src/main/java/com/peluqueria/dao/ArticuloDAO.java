package com.peluqueria.dao;

import com.peluqueria.core.domain.Articulo;
import java.util.List;

public interface ArticuloDAO extends CRUDGenericDAO<Articulo> {

    Articulo buscarCod(String codigo);

    Articulo buscarCodPorFecha(Long codigo, String inicio, String fin);

    public Articulo insertarRecuperarDato(Articulo arti);

    public List<Articulo> listarSoloDiez();

    public List<Articulo> listarPorDescripcion(String text);

    public List<Articulo> listarPorCodigo(String text);

    public List<Articulo> listarPorSeccion(String toUpperCase);

    public List<Articulo> listarPorCombinaciones(String text, String text0, String toUpperCase);

    public List<Articulo> listarPorCodigoAprox(String text);

}
