package com.peluqueria.dao;

import com.peluqueria.core.domain.Modulo;

/**
 *
 * @author ExcelsisWalker
 */
public interface ModuloDAO extends CRUDGenericDAO<Modulo> {

}
