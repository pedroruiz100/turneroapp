package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaCabClientePendiente;

public interface FacturaCabClientePendienteDAO extends CRUDGenericDAO<FacturaCabClientePendiente> {

    FacturaCabClientePendiente insertarObtenerObj(
            FacturaCabClientePendiente fromFacturaCabClientePendienteAsociado);

}
