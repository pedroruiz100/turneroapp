package com.peluqueria.dao;

import com.peluqueria.core.domain.Sucursal;

public interface SucursalDAO extends CRUDGenericDAO<Sucursal> {

    public Sucursal consultandoSuc(long l);

}
