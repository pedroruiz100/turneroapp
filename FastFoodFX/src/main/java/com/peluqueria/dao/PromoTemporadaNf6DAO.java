package com.peluqueria.dao;

import com.peluqueria.core.domain.PromoTemporadaNf6;

public interface PromoTemporadaNf6DAO extends CRUDGenericDAO<PromoTemporadaNf6> {

    void insercionMasiva(PromoTemporadaNf6 promoTemporadaNf6, long i);

    boolean insercionMasivaEstado(PromoTemporadaNf6 promoTemporadaNf6);
}
