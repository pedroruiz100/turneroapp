package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaClienteCabPorcVale;

public interface FacturaClienteCabPorcValeDAO extends CRUDGenericDAO<FacturaClienteCabPorcVale> {

}
