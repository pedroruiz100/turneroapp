package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf5;

public interface RangoFuncionarioNf5DAO extends CRUDGenericDAO<RangoFuncionarioNf5> {

    boolean insertarObtenerEstado(RangoFuncionarioNf5 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
