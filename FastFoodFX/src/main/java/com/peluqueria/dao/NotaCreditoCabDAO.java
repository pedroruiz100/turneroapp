package com.peluqueria.dao;

import com.peluqueria.core.domain.NotaCreditoCab;

public interface NotaCreditoCabDAO extends CRUDGenericDAO<NotaCreditoCab> {

}
