package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoClientefiel;

public interface RangoClientefielDAO extends CRUDGenericDAO<RangoClientefiel> {

    long actualizarObtenerRango(long idRango);

}
