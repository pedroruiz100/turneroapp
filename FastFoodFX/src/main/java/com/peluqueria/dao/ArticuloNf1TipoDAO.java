package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf1Tipo;

public interface ArticuloNf1TipoDAO extends CRUDGenericDAO<ArticuloNf1Tipo> {

    public ArticuloNf1Tipo getByIdArticulo(long id);

    public ArticuloNf1Tipo listarArticulo(long idArt);

}
