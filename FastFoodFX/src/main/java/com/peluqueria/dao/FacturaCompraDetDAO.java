package com.peluqueria.dao;

import com.peluqueria.core.domain.FacturaCompraDet;
import java.time.LocalDate;
import java.util.List;

public interface FacturaCompraDetDAO extends CRUDGenericDAO<FacturaCompraDet> {

    void insercionMasiva(FacturaCompraDet facDet, long i);

    public List<FacturaCompraDet> listarPorFechaRecepcionActual(LocalDate value, LocalDate value0, String idProveedor);

    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

//    public void listarPorNroEntradaOC(String text, String text0);

    public List<FacturaCompraDet> listarPorRecepcion(long idPedCab);

    public String recuperarUltimaFechaVenta(String idProv);

    public List<FacturaCompraDet> listarPorUltimaFecha(String fecData, String toString);

    public FacturaCompraDet getByLastCodigo(Long codArticulo);

    public List<FacturaCompraDet> buscarPorSucursalOC(String toString, String toString0);

    public List<FacturaCompraDet> listarPorOC(String oc);

    public long listarPorOCAndCodigoCC(String text, Long codArticulo);

    public long listarPorOCAndCodigoSC(String text, Long codArticulo);

    public long listarPorOCAndCodigoSL(String text, Long codArticulo);

}
