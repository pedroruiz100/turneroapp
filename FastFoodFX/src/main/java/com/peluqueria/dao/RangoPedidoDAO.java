package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoPedido;

public interface RangoPedidoDAO extends CRUDGenericDAO<RangoPedido> {

    boolean insertarObtenerEstado(RangoPedido rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

    public String recuperarActual();

}
