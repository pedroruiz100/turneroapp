package com.peluqueria.dao;

import com.peluqueria.core.domain.PedidoDetConteo;
import java.time.LocalDate;
import java.util.List;

public interface PedidoDetConteoDAO extends CRUDGenericDAO<PedidoDetConteo> {

    void insercionMasiva(PedidoDetConteo facDet, long i);

    public List<PedidoDetConteo> listarPorFechaRecepcionActual(LocalDate value, LocalDate value0, String idProveedor);

    public long getCantidadArticuloCC(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSL(String string, LocalDate value, LocalDate value0);

    public long getCantidadArticuloSC(String string, LocalDate value, LocalDate value0);

    public List<PedidoDetConteo> listarPorCabecera(long idPedCab);

    public boolean eliminarPorcabecera(Long idPedidoCab);

    public long recuperarTotal(Long idPedidoCab);

    public boolean eliminarPorcabeceraPedidoDetConteo(long idPedidoCab);

//    public PedidoDetConteo getByIdPedidoCabAndCodArt(long parseLong, Long idArticulo);

    public boolean eliminarPorcabeceraPedidoDetConteoSucursal(long idPedidoCab, String selectedItem);

    public PedidoDetConteo getByIdPedidoCabAndCodArt(long parseLong, Long idArticulo, String sucursal);

    public PedidoDetConteo getByIdPedidoCabAndCodArt(long parseLong, Long idArticulo, String suc1, String suc2, String suc3);

    public void actualizacionMasiva(PedidoDetConteo pdc, int i);

}
