package com.peluqueria.dao;

import com.peluqueria.core.domain.TransferenciaDet;

public interface TransferenciaDetDAO extends CRUDGenericDAO<TransferenciaDet> {

    void insercionMasiva(TransferenciaDet facDet, long i);

}
