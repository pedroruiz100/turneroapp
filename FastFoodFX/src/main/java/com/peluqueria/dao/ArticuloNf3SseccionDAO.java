package com.peluqueria.dao;

import com.peluqueria.core.domain.ArticuloNf3Sseccion;

public interface ArticuloNf3SseccionDAO extends CRUDGenericDAO<ArticuloNf3Sseccion> {

    public ArticuloNf3Sseccion getByIdArticulo(long id);

    public ArticuloNf3Sseccion listarArticulo(long idArt);

}
