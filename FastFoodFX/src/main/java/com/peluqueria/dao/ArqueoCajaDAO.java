package com.peluqueria.dao;

import com.peluqueria.core.domain.ArqueoCaja;
import java.util.List;

public interface ArqueoCajaDAO extends CRUDGenericDAO<ArqueoCaja> {

    ArqueoCaja insertarObtenerObj(ArqueoCaja ar);

    int recuperarZeta(long idCaja, String nro);

    boolean insertarObtenerEstado(ArqueoCaja fromArqueoCajaDTO);

    long recuperarNumMaxZeta();

    public String recuperarPorCajaFecha(long idCaja, String toString);

    public ArqueoCaja recuperarPorCajaFechaObj(long id, String fecha);

    public List<ArqueoCaja> filtroFecha(String fechaInicio, String fechaFin);

    public boolean verificarArqueoDia();

    public long getCantidadArqueoDia();

}
