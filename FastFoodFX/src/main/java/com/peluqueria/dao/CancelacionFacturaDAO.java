package com.peluqueria.dao;

import com.peluqueria.core.domain.CancelacionFactura;

public interface CancelacionFacturaDAO extends CRUDGenericDAO<CancelacionFactura> {

}
