package com.peluqueria.dao;

import com.peluqueria.core.domain.RangoFuncionarioNf2;

public interface RangoFuncionarioNf2DAO extends CRUDGenericDAO<RangoFuncionarioNf2> {

    boolean insertarObtenerEstado(RangoFuncionarioNf2 rango);

    boolean actualizarObtenerEstado(long idRango);

    long actualizarObtenerRangoActual(long idRango);

}
