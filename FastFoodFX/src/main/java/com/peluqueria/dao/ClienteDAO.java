package com.peluqueria.dao;

import com.peluqueria.core.domain.Cliente;
import java.sql.Timestamp;
import java.util.List;
import org.json.simple.JSONObject;

public interface ClienteDAO extends CRUDGenericDAO<Cliente> {

    List<Cliente> listarPorNomRuc(String nombre, String apellido, String ruc);

    void actualizarNomApeRuc(String nombre, String apellido, String ruc, String telefono, String celular, long id, String usuMod, Timestamp fechaMod);

    Cliente listarPorRuc(String ruc);

    Cliente listarPorCodigo(int codCliente);

    boolean actualizarClienteEstetica(JSONObject cliente);

    List<Cliente> listarPorCIRevancha(String rucCliente);

    public Cliente getByCod(int codCli);

    public Cliente listarPorRucExacto(String ruc);

    public boolean insertarDatos(Cliente cli);

    public boolean actualizarNomApeRucReturn(String nombre, String apellido, String ruc, String telefono, String telefono2, Long idCliente, String usuMod, Timestamp fechaMod);

}
