package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class RolDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRol;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String tipo;
    private String usuAlta;
    private String usuMod;
    private List<RolFuncionDTO> rolFuncions;
    private List<UsuarioRolDTO> usuarioRols;

    public RolDTO() {
    }

    public Long getIdRol() {
        return this.idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<RolFuncionDTO> getRolFuncions() {
        return this.rolFuncions;
    }

    public void setRolFuncions(List<RolFuncionDTO> rolFuncions) {
        this.rolFuncions = rolFuncions;
    }

//	public RolFuncionDTO addRolFuncion(RolFuncionDTO rolFuncion) {
//		getRolFuncions().add(rolFuncion);
//		rolFuncion.setRol(this);
//
//		return rolFuncion;
//	}
//
//	public RolFuncionDTO removeRolFuncion(RolFuncionDTO rolFuncion) {
//		getRolFuncions().remove(rolFuncion);
//		rolFuncion.setRol(null);
//
//		return rolFuncion;
//	}
    public List<UsuarioRolDTO> getUsuarioRols() {
        return this.usuarioRols;
    }

    public void setUsuarioRols(List<UsuarioRolDTO> usuarioRols) {
        this.usuarioRols = usuarioRols;
    }

    //DEBE ESTAR EN EL CORE
//	public UsuarioRolDTO addUsuarioRol(UsuarioRolDTO usuarioRol) {
//		getUsuarioRols().add(usuarioRol);
//		usuarioRol.setRol(this);
//
//		return usuarioRol;
//	}
//
//	public UsuarioRolDTO removeUsuarioRol(UsuarioRolDTO usuarioRol) {
//		getUsuarioRols().remove(usuarioRol);
//		usuarioRol.setRol(null);
//
//		return usuarioRol;
//	}
}
