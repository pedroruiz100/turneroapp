package com.peluqueria.dto;

import java.io.Serializable;

public class ArtPromoTemporadaObsequioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArtPromoTemporada;
    private ArticuloDTO articulo;
    private PromoTemporadaArtDTO promoTemporadaArt;
    private Integer minReq;
    private Integer cantObsequio;
    private String descriArticulo;

    public ArtPromoTemporadaObsequioDTO() {
        super();
    }

    public Long getIdArtPromoTemporada() {
        return idArtPromoTemporada;
    }

    public void setIdArtPromoTemporada(Long idArtPromoTemporada) {
        this.idArtPromoTemporada = idArtPromoTemporada;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public PromoTemporadaArtDTO getPromoTemporadaArt() {
        return promoTemporadaArt;
    }

    public void setPromoTemporadaArt(PromoTemporadaArtDTO promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public String getDescriArticulo() {
        return descriArticulo;
    }

    public void setDescriArticulo(String descriArticulo) {
        this.descriArticulo = descriArticulo;
    }

    public Integer getMinReq() {
        return minReq;
    }

    public void setMinReq(Integer minReq) {
        this.minReq = minReq;
    }

    public Integer getCantObsequio() {
        return cantObsequio;
    }

    public void setCantObsequio(Integer cantObsequio) {
        this.cantObsequio = cantObsequio;
    }

}
