package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class PedidoDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPedidoDet;

    private PedidoCabDTO pedidoCab;

    private ArticuloDTO articulo;

    private String descripcion;

    private Long precio;

    private String descuento;

    private String cantCentral;

    private String cantCacique;

    private String cantSanlo;

    public PedidoDetDTO() {
    }

    public Long getIdPedidoDet() {
        return idPedidoDet;
    }

    public void setIdPedidoDet(Long idPedidoDet) {
        this.idPedidoDet = idPedidoDet;
    }

    public PedidoCabDTO getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCabDTO pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getCantCentral() {
        return cantCentral;
    }

    public void setCantCentral(String cantCentral) {
        this.cantCentral = cantCentral;
    }

    public String getCantCacique() {
        return cantCacique;
    }

    public void setCantCacique(String cantCacique) {
        this.cantCacique = cantCacique;
    }

    public String getCantSanlo() {
        return cantSanlo;
    }

    public void setCantSanlo(String cantSanlo) {
        this.cantSanlo = cantSanlo;
    }

}
