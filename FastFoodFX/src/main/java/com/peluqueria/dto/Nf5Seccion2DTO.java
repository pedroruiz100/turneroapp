package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf5Seccion2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf5Seccion2;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf4Seccion1DTO nf4Seccion1;
    private List<ArticuloNf5Seccion2DTO> articuloNf5Seccion2s;
    private List<Nf6Secnom6DTO> nf6Secnom6s;
    private List<FuncionarioNf5DTO> funcionarioNf5s;
    private List<DescuentoFielNf5DTO> descuentoFielNf5s;
    private List<PromoTemporadaNf5DTO> promoTemporadaNf5s;

    public Nf5Seccion2DTO() {
    }

    public Long getIdNf5Seccion2() {
        return this.idNf5Seccion2;
    }

    public void setIdNf5Seccion2(Long idNf5Seccion2) {
        this.idNf5Seccion2 = idNf5Seccion2;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf5Seccion2DTO> getArticuloNf5Seccion2s() {
        return this.articuloNf5Seccion2s;
    }

    public void setArticuloNf5Seccion2s(List<ArticuloNf5Seccion2DTO> articuloNf5Seccion2s) {
        this.articuloNf5Seccion2s = articuloNf5Seccion2s;
    }

    public ArticuloNf5Seccion2DTO addArticuloNf5Seccion2(ArticuloNf5Seccion2DTO articuloNf5Seccion2) {
        getArticuloNf5Seccion2s().add(articuloNf5Seccion2);
        articuloNf5Seccion2.setNf5Seccion2(this);

        return articuloNf5Seccion2;
    }

    public ArticuloNf5Seccion2DTO removeArticuloNf5Seccion2(ArticuloNf5Seccion2DTO articuloNf5Seccion2) {
        getArticuloNf5Seccion2s().remove(articuloNf5Seccion2);
        articuloNf5Seccion2.setNf5Seccion2(null);

        return articuloNf5Seccion2;
    }

    public Nf4Seccion1DTO getNf4Seccion1() {
        return this.nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    public List<Nf6Secnom6DTO> getNf6Secnom6s() {
        return this.nf6Secnom6s;
    }

    public void setNf6Secnom6s(List<Nf6Secnom6DTO> nf6Secnom6s) {
        this.nf6Secnom6s = nf6Secnom6s;
    }

    public Nf6Secnom6DTO addNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        getNf6Secnom6s().add(nf6Secnom6);
        nf6Secnom6.setNf5Seccion2(this);

        return nf6Secnom6;
    }

    public Nf6Secnom6DTO removeNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        getNf6Secnom6s().remove(nf6Secnom6);
        nf6Secnom6.setNf5Seccion2(null);

        return nf6Secnom6;
    }

    public List<FuncionarioNf5DTO> getFuncionarioNf5s() {
        return this.funcionarioNf5s;
    }

    public void setFuncionarioNf5s(List<FuncionarioNf5DTO> funcionarioNf5s) {
        this.funcionarioNf5s = funcionarioNf5s;
    }

    public FuncionarioNf5DTO addFuncionarioNf5(FuncionarioNf5DTO funcionarioNf5) {
        getFuncionarioNf5s().add(funcionarioNf5);
        funcionarioNf5.setNf5Seccion2(this);

        return funcionarioNf5;
    }

    public FuncionarioNf5DTO removeFuncionarioNf5(FuncionarioNf5DTO funcionarioNf5) {
        getFuncionarioNf5s().remove(funcionarioNf5);
        funcionarioNf5.setNf5Seccion2(null);

        return funcionarioNf5;
    }

    public List<DescuentoFielNf5DTO> getDescuentoFielNf5s() {
        return this.descuentoFielNf5s;
    }

    public void setDescuentoFielNf5s(List<DescuentoFielNf5DTO> descuentoFielNf5s) {
        this.descuentoFielNf5s = descuentoFielNf5s;
    }

    public DescuentoFielNf5DTO addDescuentoFielNf5(DescuentoFielNf5DTO descuentoFielNf5) {
        getDescuentoFielNf5s().add(descuentoFielNf5);
        descuentoFielNf5.setNf5Seccion2(this);

        return descuentoFielNf5;
    }

    public DescuentoFielNf5DTO removeDescuentoFielNf5(DescuentoFielNf5DTO descuentoFielNf5) {
        getDescuentoFielNf5s().remove(descuentoFielNf5);
        descuentoFielNf5.setNf5Seccion2(null);

        return descuentoFielNf5;
    }

    public List<PromoTemporadaNf5DTO> getPromoTemporadaNf5s() {
        return this.promoTemporadaNf5s;
    }

    public void setPromoTemporadaNf5s(List<PromoTemporadaNf5DTO> promoTemporadaNf5s) {
        this.promoTemporadaNf5s = promoTemporadaNf5s;
    }

    public PromoTemporadaNf5DTO addPromoTemporadaNf5(PromoTemporadaNf5DTO promoTemporadaNf5) {
        getPromoTemporadaNf5s().add(promoTemporadaNf5);
        promoTemporadaNf5.setNf5Seccion2(this);

        return promoTemporadaNf5;
    }

    public PromoTemporadaNf5DTO removePromoTemporadaNf5(PromoTemporadaNf5DTO promoTemporadaNf5) {
        getPromoTemporadaNf5s().remove(promoTemporadaNf5);
        promoTemporadaNf5.setNf5Seccion2(null);

        return promoTemporadaNf5;
    }

}
