package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the desc_tarjeta database table.
 *
 */
public class DescTarjetaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescTarjeta;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabTarjetaDTO facturaClienteCabTarjeta;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;

    public DescTarjetaDTO() {
    }

    public Long getIdDescTarjeta() {
        return this.idDescTarjeta;
    }

    public void setIdDescTarjeta(Long idDescTarjeta) {
        this.idDescTarjeta = idDescTarjeta;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjetaDTO getFacturaClienteCabTarjeta() {
        return this.facturaClienteCabTarjeta;
    }

    public void setFacturaClienteCabTarjeta(FacturaClienteCabTarjetaDTO facturaClienteCabTarjeta) {
        this.facturaClienteCabTarjeta = facturaClienteCabTarjeta;
    }
    
    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }
    
    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

}
