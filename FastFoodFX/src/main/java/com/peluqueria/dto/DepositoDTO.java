package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the marca database table.
 *
 */
public class DepositoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDeposito;
    private String descripcion;
    private List<ExistenciaDTO> existencia;

    public DepositoDTO() {
    }

    public Long getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Long idDeposito) {
        this.idDeposito = idDeposito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ExistenciaDTO> getExistencia() {
        if (this.existencia == null) {
            this.existencia = new ArrayList<>();
        }
        return existencia;
    }

    public void setExistencia(List<ExistenciaDTO> existencia) {
        this.existencia = existencia;
    }

}
