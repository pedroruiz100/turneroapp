package com.peluqueria.dto;

public class FacturaClienteCabChequeDTO {

    private Long idFacturaClienteCabCheque;

    private String descripcion;
    private FacturaClienteCabDTO facturaClienteCabDTO;
    private Integer montoCheque;
    private String nroCheque;

    public FacturaClienteCabChequeDTO() {
        super();
    }

    public Long getIdFacturaClienteCabCheque() {
        return idFacturaClienteCabCheque;
    }

    public void setIdFacturaClienteCabCheque(Long idFacturaClienteCabCheque) {
        this.idFacturaClienteCabCheque = idFacturaClienteCabCheque;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FacturaClienteCabDTO getFacturaClienteCabDTO() {
        return facturaClienteCabDTO;
    }

    public void setFacturaClienteCabDTO(
            FacturaClienteCabDTO facturaClienteCabDTO) {
        this.facturaClienteCabDTO = facturaClienteCabDTO;
    }

    public Integer getMontoCheque() {
        return montoCheque;
    }

    public void setMontoCheque(Integer montoCheque) {
        this.montoCheque = montoCheque;
    }

    public String getNroCheque() {
        return nroCheque;
    }

    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

}
