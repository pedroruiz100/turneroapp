package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the tipo_tarjeta database table.
 *
 */
public class TipoTarjetaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoTarjeta;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<TarjetaDTO> tarjetas;

    public TipoTarjetaDTO() {
    }

    public Long getIdTipoTarjeta() {
        return this.idTipoTarjeta;
    }

    public void setIdTipoTarjeta(Long idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<TarjetaDTO> getTarjetas() {
        return this.tarjetas;
    }

    public void setTarjetas(List<TarjetaDTO> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public TarjetaDTO addTarjeta(TarjetaDTO tarjeta) {
        getTarjetas().add(tarjeta);
        tarjeta.setTipoTarjeta(this);

        return tarjeta;
    }

    public TarjetaDTO removeTarjeta(TarjetaDTO tarjeta) {
        getTarjetas().remove(tarjeta);
        tarjeta.setTipoTarjeta(null);

        return tarjeta;
    }

}
