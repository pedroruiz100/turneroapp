package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the familia_tarj database table.
 *
 */
public class FamiliaTarjDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFamiliaTarj;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private BarrioDTO barrio;
    private CiudadDTO ciudad;
    private Integer nroLocal;
    private String primeraLateral;
    private String segundaLateral;
    private String telefono;
    private String usuAlta;
    private String usuMod;
    private List<TarjetaDTO> tarjetas;
    private List<TarjetaConvenioDTO> tarjetaConvenios;

    public FamiliaTarjDTO() {
    }

    public Long getIdFamiliaTarj() {
        return this.idFamiliaTarj;
    }

    public void setIdFamiliaTarj(Long idFamiliaTarj) {
        this.idFamiliaTarj = idFamiliaTarj;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<TarjetaDTO> getTarjetas() {
        return this.tarjetas;
    }

    public void setTarjetas(List<TarjetaDTO> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public TarjetaDTO addTarjeta(TarjetaDTO tarjeta) {
        getTarjetas().add(tarjeta);
        tarjeta.setFamiliaTarj(this);

        return tarjeta;
    }

    public TarjetaDTO removeTarjeta(TarjetaDTO tarjeta) {
        getTarjetas().remove(tarjeta);
        tarjeta.setFamiliaTarj(null);

        return tarjeta;
    }

    public List<TarjetaConvenioDTO> getTarjetaConvenios() {
        return this.tarjetaConvenios;
    }

    public void setTarjetaConvenios(List<TarjetaConvenioDTO> tarjetaConvenios) {
        this.tarjetaConvenios = tarjetaConvenios;
    }

    public TarjetaConvenioDTO addTarjetaConvenio(TarjetaConvenioDTO tarjetaConvenio) {
        getTarjetaConvenios().add(tarjetaConvenio);
        tarjetaConvenio.setFamiliaTarj(this);

        return tarjetaConvenio;
    }

    public TarjetaConvenioDTO removeTarjetaConvenio(TarjetaConvenioDTO tarjetaConvenio) {
        getTarjetaConvenios().remove(tarjetaConvenio);
        tarjetaConvenio.setFamiliaTarj(null);

        return tarjetaConvenio;
    }

    public BarrioDTO getBarrio() {
        return barrio;
    }

    public void setBarrio(BarrioDTO barrio) {
        this.barrio = barrio;
    }

    public CiudadDTO getCiudad() {
        return ciudad;
    }

    public void setCiudad(CiudadDTO ciudad) {
        this.ciudad = ciudad;
    }

}
