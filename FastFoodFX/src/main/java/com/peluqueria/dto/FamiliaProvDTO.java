package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the familia_prov database table.
 *
 */
public class FamiliaProvDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFamiliaProv;
    private String descripcion;
    private List<ProveedorDTO> proveedors;

    public FamiliaProvDTO() {
    }

    public Long getIdFamiliaProv() {
        return this.idFamiliaProv;
    }

    public void setIdFamiliaProv(Long idFamiliaProv) {
        this.idFamiliaProv = idFamiliaProv;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ProveedorDTO> getProveedors() {
        if (this.proveedors == null) {
            this.proveedors = new ArrayList<ProveedorDTO>();
        }
        return this.proveedors;
    }

    public void setProveedors(List<ProveedorDTO> proveedors) {
        this.proveedors = proveedors;
    }

    public ProveedorDTO addProveedor(ProveedorDTO proveedor) {
        getProveedors().add(proveedor);
        proveedor.setFamiliaProv(this);

        return proveedor;
    }

    public ProveedorDTO removeProveedor(ProveedorDTO proveedor) {
        getProveedors().remove(proveedor);
        proveedor.setFamiliaProv(null);

        return proveedor;
    }

}
