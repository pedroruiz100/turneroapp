package com.peluqueria.dto;

import java.io.Serializable;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab_tarjeta database table.
 *
 */
public class FacturaClienteCabTarjetaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabTarjeta;
    private String descripcionTarj;
    private String codAutorizacion;
    private Integer monto;
    private TarjetaDTO tarjeta;
    private List<DescTarjetaDTO> descTarjetas;
    private FacturaClienteCabDTO facturaClienteCab;

    public FacturaClienteCabTarjetaDTO() {
    }

    public Long getIdFacturaClienteCabTarjeta() {
        return this.idFacturaClienteCabTarjeta;
    }

    public void setIdFacturaClienteCabTarjeta(Long idFacturaClienteCabTarjeta) {
        this.idFacturaClienteCabTarjeta = idFacturaClienteCabTarjeta;
    }

    public String getDescripcionTarj() {
        return this.descripcionTarj;
    }

    public void setDescripcionTarj(String descripcionTarj) {
        this.descripcionTarj = descripcionTarj;
    }

    public List<DescTarjetaDTO> getDescTarjetas() {
        return this.descTarjetas;
    }

    public void setDescTarjetas(List<DescTarjetaDTO> descTarjetas) {
        this.descTarjetas = descTarjetas;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TarjetaDTO getTarjeta() {
        return tarjeta;
    }

    public String getCodAutorizacion() {
        return codAutorizacion;
    }

    public void setCodAutorizacion(String codAutorizacion) {
        this.codAutorizacion = codAutorizacion;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public void setTarjeta(TarjetaDTO tarjeta) {
        this.tarjeta = tarjeta;
    }

}
