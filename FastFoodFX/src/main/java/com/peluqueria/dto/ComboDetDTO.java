package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the combo_det database table.
 *
 */
public class ComboDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idComboDet;
    private Integer cantArticulo;
    private String descripcionArticulo;
    private String marcaArticulo;
    private Integer precioArticulo;
    private ArticuloDTO articulo;
    private ComboCabDTO comboCab;

    public ComboDetDTO() {
    }

    public Long getIdComboDet() {
        return this.idComboDet;
    }

    public void setIdComboDet(Long idComboDet) {
        this.idComboDet = idComboDet;
    }

    public Integer getCantArticulo() {
        return this.cantArticulo;
    }

    public void setCantArticulo(Integer cantArticulo) {
        this.cantArticulo = cantArticulo;
    }

    public String getDescripcionArticulo() {
        return this.descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public String getMarcaArticulo() {
        return this.marcaArticulo;
    }

    public void setMarcaArticulo(String marcaArticulo) {
        this.marcaArticulo = marcaArticulo;
    }

    public Integer getPrecioArticulo() {
        return this.precioArticulo;
    }

    public void setPrecioArticulo(Integer precioArticulo) {
        this.precioArticulo = precioArticulo;
    }

    public ArticuloDTO getArticulo() {
        return this.articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public ComboCabDTO getComboCab() {
        return this.comboCab;
    }

    public void setComboCab(ComboCabDTO comboCab) {
        this.comboCab = comboCab;
    }

}
