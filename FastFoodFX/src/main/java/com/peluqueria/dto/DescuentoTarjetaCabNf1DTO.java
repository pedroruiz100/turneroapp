package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf1;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf1TipoDTO nf1Tipo;
    private String descriSeccion;

    public DescuentoTarjetaCabNf1DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf1() {
        return this.idDescuentoTarjetaCabNf1;
    }

    public void setIdDescuentoTarjetaCabNf1(Long idDescuentoTarjetaCabNf1) {
        this.idDescuentoTarjetaCabNf1 = idDescuentoTarjetaCabNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

}
