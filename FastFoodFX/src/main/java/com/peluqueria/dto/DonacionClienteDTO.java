package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the donacion_cliente database table.
 *
 */
public class DonacionClienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDonacionCliente;
    private Integer montoDonacion;
    private FacturaClienteCabDTO facturaClienteCab;

    public DonacionClienteDTO() {
    }

    public Long getIdDonacionCliente() {
        return this.idDonacionCliente;
    }

    public void setIdDonacionCliente(Long idDonacionCliente) {
        this.idDonacionCliente = idDonacionCliente;
    }

    public Integer getMontoDonacion() {
        return this.montoDonacion;
    }

    public void setMontoDonacion(Integer montoDonacion) {
        this.montoDonacion = montoDonacion;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

}
