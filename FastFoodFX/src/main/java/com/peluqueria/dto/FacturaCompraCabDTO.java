package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;
import javax.persistence.Column;

public class FacturaCompraCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaCompraCab;

    private String tipoMovimiento;

    private String nroOrden;

    private String nroDoc;

    private String oc;

    private String cantidad;

    private Date fechaDoc;

    private String sucursal;

    private String observacion;

    private String tipoDocumento;

    private String claseDocumento;

    private ProveedorDTO proveedor;

    private int total;

    private int saldo;

    private boolean cc;

    private boolean sc;

    private boolean sl;

    private String estadoFactura;

    private PedidoCabDTO pedidoCab;

    private String moneda;

    private long cotizacion;

    public FacturaCompraCabDTO() {
    }

    public Long getIdFacturaCompraCab() {
        return idFacturaCompraCab;
    }

    public void setIdFacturaCompraCab(Long idFacturaCompraCab) {
        this.idFacturaCompraCab = idFacturaCompraCab;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public long getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(long cotizacion) {
        this.cotizacion = cotizacion;
    }

    public boolean isCc() {
        return cc;
    }

    public void setCc(boolean cc) {
        this.cc = cc;
    }

    public boolean isSc() {
        return sc;
    }

    public void setSc(boolean sc) {
        this.sc = sc;
    }

    public boolean isSl() {
        return sl;
    }

    public void setSl(boolean sl) {
        this.sl = sl;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getNroOrden() {
        return nroOrden;
    }

    public String getOc() {
        return oc;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public int getTotal() {
        return total;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public PedidoCabDTO getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCabDTO pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public void setNroOrden(String nroOrden) {
        this.nroOrden = nroOrden;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public Date getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(Date fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public ProveedorDTO getProveedor() {
        return proveedor;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

}
