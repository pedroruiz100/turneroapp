package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SupervisorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSupervisor;
    private Boolean activo;
    private String codSupervisor;
    private Integer nroSupervisor;
    private UsuarioDTO usuario;
    private List<ArqueoCajaDTO> arqueoCaja;
    private String usuAlta;
    private String usuMod;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;

    public SupervisorDTO() {
    }

    public Long getIdSupervisor() {
        return this.idSupervisor;
    }

    public void setIdSupervisor(Long idSupervisor) {
        this.idSupervisor = idSupervisor;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getCodSupervisor() {
        return this.codSupervisor;
    }

    public void setCodSupervisor(String codSupervisor) {
        this.codSupervisor = codSupervisor;
    }

    public Integer getNroSupervisor() {
        return this.nroSupervisor;
    }

    public void setNroSupervisor(Integer nroSupervisor) {
        this.nroSupervisor = nroSupervisor;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public List<ArqueoCajaDTO> getArqueoCaja() {
        if (this.arqueoCaja == null) {
            this.arqueoCaja = new ArrayList<ArqueoCajaDTO>();
        }
        return arqueoCaja;
    }

    public void setArqueoCaja(List<ArqueoCajaDTO> arqueoCaja) {
        this.arqueoCaja = arqueoCaja;
    }
    
    public String getUsuAlta() {
        return usuAlta;
    }
    
    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }
    
    public String getUsuMod() {
        return usuMod;
    }
    
    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }
    
    public Timestamp getFechaAlta() {
        return fechaAlta;
    }
    
    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }
    
    public Timestamp getFechaMod() {
        return fechaMod;
    }
    
    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }
}
