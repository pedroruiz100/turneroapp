package com.peluqueria.dto;

import java.io.Serializable;
import java.util.List;

public class IpBocaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idIpBoca;
    private String ipBoca;
    private List<TalonariosSucursaleDTO> talonariosSucursales;
    private List<CajaDTO> caja;

    public List<CajaDTO> getCaja() {
        return caja;
    }

    public void setCaja(List<CajaDTO> caja) {
        this.caja = caja;
    }

    public IpBocaDTO() {
    }

    public Long getIdIpBoca() {
        return this.idIpBoca;
    }

    public void setIdIpBoca(Long idIpBoca) {
        this.idIpBoca = idIpBoca;
    }

    public String getIpBoca() {
        return this.ipBoca;
    }

    public void setIpBoca(String ipBoca) {
        this.ipBoca = ipBoca;
    }

    public List<TalonariosSucursaleDTO> getTalonariosSucursales() {
        return this.talonariosSucursales;
    }

    public void setTalonariosSucursales(
            List<TalonariosSucursaleDTO> talonariosSucursales) {
        this.talonariosSucursales = talonariosSucursales;
    }

    public TalonariosSucursaleDTO addTalonariosSucursale(
            TalonariosSucursaleDTO talonariosSucursale) {
        getTalonariosSucursales().add(talonariosSucursale);
        talonariosSucursale.setIpBoca(this);

        return talonariosSucursale;
    }

    public TalonariosSucursaleDTO removeTalonariosSucursale(
            TalonariosSucursaleDTO talonariosSucursale) {
        getTalonariosSucursales().remove(talonariosSucursale);
        talonariosSucursale.setIpBoca(null);

        return talonariosSucursale;
    }

}
