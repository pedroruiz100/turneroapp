package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class CancelacionProductoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCancelacionProducto;
    private Timestamp fechaCancelacion;
    private ArticuloDTO articulo;
    private Integer precio;
    private BigDecimal cantidad;
    private UsuarioDTO usuarioCajero;
    private UsuarioDTO usuarioSupervisor;
    private Integer total;
    private MotivoCancelacionProductoDTO motivoCancelacionProducto;
    private FacturaClienteCabDTO facturaClienteCab;

    public CancelacionProductoDTO() {
    }

    public Long getIdCancelacionProducto() {
        return this.idCancelacionProducto;
    }

    public void setIdCancelacionProducto(Long idCancelacionProducto) {
        this.idCancelacionProducto = idCancelacionProducto;
    }

    public Timestamp getFechaCancelacion() {
        return this.fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public MotivoCancelacionProductoDTO getMotivoCancelacionProducto() {
        return this.motivoCancelacionProducto;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public void setMotivoCancelacionProducto(
            MotivoCancelacionProductoDTO motivoCancelacionProducto) {
        this.motivoCancelacionProducto = motivoCancelacionProducto;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public ArticuloDTO getArticulo() {
        return this.articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public UsuarioDTO getUsuarioCajero() {
        return this.usuarioCajero;
    }

    public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public UsuarioDTO getUsuarioSupervisor() {
        return this.usuarioSupervisor;
    }

    public void setUsuarioSupervisor(UsuarioDTO usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

}
