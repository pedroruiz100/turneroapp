package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the factura_cliente_cab_Vale database table.
 *
 */
public class FacturaClienteCabPorcValeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabPorcVale;
    private FacturaClienteCabDTO facturaClienteCab;
    private ValeDTO vale;
    private BigDecimal porcVale;
    private String nroVale;
    private String ci;
    private Integer monto;

    public FacturaClienteCabPorcValeDTO() {
    }

    public Long getIdFacturaClienteCabPorcVale() {
        return idFacturaClienteCabPorcVale;
    }

    public void setIdFacturaClienteCabPorcVale(Long idFacturaClienteCabPorcVale) {
        this.idFacturaClienteCabPorcVale = idFacturaClienteCabPorcVale;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public ValeDTO getVale() {
        return vale;
    }

    public void setVale(ValeDTO vale) {
        this.vale = vale;
    }

    public BigDecimal getPorcVale() {
        return porcVale;
    }

    public void setPorcVale(BigDecimal porcVale) {
        this.porcVale = porcVale;
    }

    public String getNroVale() {
        return nroVale;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public void setNroVale(String nroVale) {
        this.nroVale = nroVale;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

}
