package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class CancelFactDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCancelacionFactura;
    private Timestamp fechaCancelacion;
    private FacturaClienteCabDTO facturaClienteCab;
    private UsuarioDTO usuarioCajero;
    private UsuarioDTO usuarioSupervisor;
    private MotivoCancelacionFacturaDTO motivoCancelacionFactura;
    private String nroFactura;
    private Long monto;
    private SucursalDTO sucursal;

    public CancelFactDTO() {
    }

    public Long getIdCancelacionFactura() {
        return idCancelacionFactura;
    }

    public void setIdCancelacionFactura(Long idCancelacionFactura) {
        this.idCancelacionFactura = idCancelacionFactura;
    }

    public Timestamp getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public UsuarioDTO getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public UsuarioDTO getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(UsuarioDTO usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public MotivoCancelacionFacturaDTO getMotivoCancelacionFactura() {
        return motivoCancelacionFactura;
    }

    public void setMotivoCancelacionFactura(MotivoCancelacionFacturaDTO motivoCancelacionFactura) {
        this.motivoCancelacionFactura = motivoCancelacionFactura;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

}
