package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf2;
    private PromoTemporadaDTO promoTemporada;
    private Nf2SfamiliaDTO nf2Sfamilia;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf2DTO() {
    }

    public Long getIdPromoTemporadaNf2() {
        return this.idPromoTemporadaNf2;
    }

    public void setIdPromoTemporadaNf2(Long idPromoTemporadaNf2) {
        this.idPromoTemporadaNf2 = idPromoTemporadaNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
