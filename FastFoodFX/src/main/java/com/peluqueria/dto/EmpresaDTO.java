package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class EmpresaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEmpresa;
    private String descripcionEmpresa;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private String telefono;
    private String ruc;
    private List<SucursalDTO> sucursals;

    public EmpresaDTO() {
    }

    public Long getIdEmpresa() {
        return this.idEmpresa;
    }

    public void setIdEmpresa(Long idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getDescripcionEmpresa() {
        return this.descripcionEmpresa;
    }

    public void setDescripcionEmpresa(String descripcionEmpresa) {
        this.descripcionEmpresa = descripcionEmpresa;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<SucursalDTO> getSucursals() {
        if (this.sucursals == null) {
            this.sucursals = new ArrayList<SucursalDTO>();
        }
        return this.sucursals;
    }

    public void setSucursals(List<SucursalDTO> sucursals) {
        this.sucursals = sucursals;
    }

    public SucursalDTO addSucursal(SucursalDTO sucursal) {
        getSucursals().add(sucursal);
        sucursal.setEmpresaDTO(this);

        return sucursal;
    }

    public SucursalDTO removeSucursal(SucursalDTO sucursal) {
        getSucursals().remove(sucursal);
        sucursal.setEmpresaDTO(null);
        return sucursal;
    }

}
