package com.peluqueria.dto;

import java.math.BigDecimal;
import java.sql.Date;

public class PreparacionDetalleDTO {

    private Long idPreparacionDetalle;

    private Date fechaCompra;

    private String descripcion;

    private PreparacionCabeceraDTO preparacionCabecera;

    private DetalleGastoDTO detalleGasto;

    private Long costo;

    private BigDecimal cantidad;

    public PreparacionDetalleDTO() {
        super();
    }

    public Long getIdPreparacionDetalle() {
        return idPreparacionDetalle;
    }

    public void setIdPreparacionDetalle(Long idPreparacionDetalle) {
        this.idPreparacionDetalle = idPreparacionDetalle;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public DetalleGastoDTO getDetalleGasto() {
        return detalleGasto;
    }

    public void setDetalleGasto(DetalleGastoDTO detalleGasto) {
        this.detalleGasto = detalleGasto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public PreparacionCabeceraDTO getPreparacionCabecera() {
        return preparacionCabecera;
    }

    public void setPreparacionCabecera(PreparacionCabeceraDTO preparacionCabecera) {
        this.preparacionCabecera = preparacionCabecera;
    }

    public Long getCosto() {
        return costo;
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

}
