package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf6;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf6Secnom6DTO nf6Secnom6;
    private String descriSeccion;

    public DescuentoTarjetaCabNf6DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf6() {
        return this.idDescuentoTarjetaCabNf6;
    }

    public void setIdDescuentoTarjetaCabNf6(Long idDescuentoTarjetaCabNf6) {
        this.idDescuentoTarjetaCabNf6 = idDescuentoTarjetaCabNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

}
