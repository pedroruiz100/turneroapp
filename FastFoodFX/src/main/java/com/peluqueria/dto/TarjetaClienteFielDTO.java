package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the tarjeta_cliente_fiel database table.
 *
 */
public class TarjetaClienteFielDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTarjetaClienteFiel;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private ClienteDTO cliente;
    private String cipas;
    private String empresaSuc;
    private List<FacturaClienteCabDTO> facturaClienteCabs;

    public TarjetaClienteFielDTO() {
    }

    public Long getIdTarjetaClienteFiel() {
        return this.idTarjetaClienteFiel;
    }

    public void setIdTarjetaClienteFiel(Long idTarjetaClienteFiel) {
        this.idTarjetaClienteFiel = idTarjetaClienteFiel;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public String getCipas() {
        return cipas;
    }

    public void setCipas(String cipas) {
        this.cipas = cipas;
    }

    public String getEmpresaSuc() {
        return empresaSuc;
    }

    public void setEmpresaSuc(String empresaSuc) {
        this.empresaSuc = empresaSuc;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public ClienteDTO getCliente() {
        return this.cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    // ********************************************************************************
    public List<FacturaClienteCabDTO> getFacturaClienteCabs() {
        return facturaClienteCabs;
    }

    public void setFacturaClienteCabs(
            List<FacturaClienteCabDTO> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public FacturaClienteCabDTO addFacturaClienteCab(
            FacturaClienteCabDTO facturaClienteCab) {
        getFacturaClienteCabs().add(facturaClienteCab);
        return facturaClienteCab;
    }

    public FacturaClienteCabDTO removeFacturaClienteCab(
            FacturaClienteCabDTO facturaClienteCab) {
        getFacturaClienteCabs().remove(facturaClienteCab);
        return facturaClienteCab;
    }

    // ********************************************************************************
}
