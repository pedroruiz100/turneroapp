package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;

public class ArticuloCompraMatrizDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloCompraMatriz;

    private String codigo;

    private Date fecha;

    private Long cantidad;
    private Boolean cc;
    private Boolean sc;
    private Boolean sl;

    public ArticuloCompraMatrizDTO() {
        super();
    }

    public Long getIdArticuloCompraMatriz() {
        return idArticuloCompraMatriz;
    }

    public void setIdArticuloCompraMatriz(Long idArticuloCompraMatriz) {
        this.idArticuloCompraMatriz = idArticuloCompraMatriz;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

}
