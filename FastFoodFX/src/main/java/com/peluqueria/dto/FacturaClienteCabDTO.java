package com.peluqueria.dto;

import com.peluqueria.core.domain.NotaCreditoCabProveedor;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab database table.
 *
 */
public class FacturaClienteCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCab;
    private Timestamp fechaEmision;
    private String nroFactura;
    private SucursalDTO sucursal;
    private EstadoFacturaDTO estadoFactura;

    private Boolean cancelado;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Integer montoFactura;
    private String nroActual;
    private CajaDTO caja;
    private ClienteDTO cliente;
    private TipoComprobanteDTO tipoComprobante;
    private TipoMonedaDTO tipoMoneda;

    private List<FacturaClienteCabChequeDTO> facturaClienteCabCheque;
    private List<FacturaClienteCabEfectivoDTO> facturaClienteCabEfectivo;
    private List<FacturaClienteCabFuncionarioDTO> facturaClienteCabFuncionario;
    private List<FacturaClienteCabTarjFielDTO> facturaClienteCabTarjFiel;
    private List<FacturaClienteCabPromoTempDTO> facturaClienteCabPromoTemp;
    private List<CancelacionProductoDTO> cancelacionProducto;
    private List<FacturaClienteCabNotaCreditoDTO> facturaClienteCabNotaCredito;
    private List<FacturaClienteCabMonedaExtranjeraDTO> facturaClienteCabMonedaExtranjera;
    private List<FacturaClienteCabRetencionDTO> facturaClienteCabRetencion;
    private List<FacturaCabClientePendienteDTO> facturaCabClientePendiente;
    private List<FacturaClienteCabTarjConvenioDTO> facturaClienteCabTarjConvenios;
    private List<FacturaClienteCabTarjetaDTO> facturaClienteCabTarjetas;
    private List<FacturaClienteCabTipoPagoDTO> facturaClienteCabTipoPagos;
    private List<FacturaClienteDetDTO> facturaClienteDets;
    private List<NotaCreditoCabDTO> notaCreditoCabs;
    private List<DescValeDTO> descVales;
    private List<DonacionClienteDTO> donacionClientes;
    private List<NotaRemisionCabDTO> notaRemisionCabs;
    private List<CancelacionFacturaDTO> cancelacionFacturas;
    private List<FacturaClienteCabHistoricoDTO> facturaClienteCabHistorico;
    private List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTempArt;
//    private List<NotaCreditoCabProveedorDTO> notaCreditoCabProveedor;

    public FacturaClienteCabDTO() {
    }

    public Long getIdFacturaClienteCab() {
        return this.idFacturaClienteCab;
    }

//    public List<NotaCreditoCabProveedorDTO> getNotaCreditoCabProveedor() {
//        return notaCreditoCabProveedor;
//    }
//
//    public void setNotaCreditoCabProveedor(List<NotaCreditoCabProveedorDTO> notaCreditoCabProveedor) {
//        this.notaCreditoCabProveedor = notaCreditoCabProveedor;
//    }
    public List<FacturaClienteCabPromoTempDTO> getFacturaClienteCabPromoTemp() {
        if (this.facturaClienteCabPromoTemp == null) {
            this.facturaClienteCabPromoTemp = new ArrayList<FacturaClienteCabPromoTempDTO>();
        }
        return facturaClienteCabPromoTemp;
    }

    public void setFacturaClienteCabPromoTemp(
            List<FacturaClienteCabPromoTempDTO> facturaClienteCabPromoTemp) {
        this.facturaClienteCabPromoTemp = facturaClienteCabPromoTemp;
    }

    public void setIdFacturaClienteCab(Long idFacturaClienteCab) {
        this.idFacturaClienteCab = idFacturaClienteCab;
    }

    public Boolean getCancelado() {
        return this.cancelado;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public List<FacturaClienteCabHistoricoDTO> getFacturaClienteCabHistorico() {
        return facturaClienteCabHistorico;
    }

    public void setFacturaClienteCabHistorico(List<FacturaClienteCabHistoricoDTO> facturaClienteCabHistorico) {
        this.facturaClienteCabHistorico = facturaClienteCabHistorico;
    }

    public Timestamp getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Integer getMontoFactura() {
        return this.montoFactura;
    }

    public void setMontoFactura(Integer montoFactura) {
        this.montoFactura = montoFactura;
    }

    public String getNroFactura() {
        return this.nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public List<DescValeDTO> getDescVales() {
        if (this.descVales == null) {
            this.descVales = new ArrayList<DescValeDTO>();
        }
        return this.descVales;
    }

    public String getNroActual() {
        return nroActual;
    }

    public void setNroActual(String nroActual) {
        this.nroActual = nroActual;
    }

    public void setDescVales(List<DescValeDTO> descVales) {
        this.descVales = descVales;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DonacionClienteDTO> getDonacionClientes() {
        if (this.donacionClientes == null) {
            this.donacionClientes = new ArrayList<DonacionClienteDTO>();
        }
        return this.donacionClientes;
    }

    public void setDonacionClientes(List<DonacionClienteDTO> donacionClientes) {
        this.donacionClientes = donacionClientes;
    }

    public EstadoFacturaDTO getEstadoFactura() {
        return this.estadoFactura;
    }

    public void setEstadoFactura(EstadoFacturaDTO estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public TipoComprobanteDTO getTipoComprobante() {
        return this.tipoComprobante;
    }

    public void setTipoComprobante(TipoComprobanteDTO tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public TipoMonedaDTO getTipoMoneda() {
        return this.tipoMoneda;
    }

    public void setTipoMoneda(TipoMonedaDTO tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public List<FacturaClienteCabTarjConvenioDTO> getFacturaClienteCabTarjConvenios() {
        if (this.facturaClienteCabTarjConvenios == null) {
            this.facturaClienteCabTarjConvenios = new ArrayList<FacturaClienteCabTarjConvenioDTO>();
        }
        return this.facturaClienteCabTarjConvenios;
    }

    public void setFacturaClienteCabTarjConvenios(
            List<FacturaClienteCabTarjConvenioDTO> facturaClienteCabTarjConvenios) {
        this.facturaClienteCabTarjConvenios = facturaClienteCabTarjConvenios;
    }

    public List<FacturaClienteCabTarjetaDTO> getFacturaClienteCabTarjetas() {
        if (this.facturaClienteCabTarjetas == null) {
            this.facturaClienteCabTarjetas = new ArrayList<FacturaClienteCabTarjetaDTO>();
        }
        return this.facturaClienteCabTarjetas;
    }

    public void setFacturaClienteCabTarjetas(
            List<FacturaClienteCabTarjetaDTO> facturaClienteCabTarjetas) {
        this.facturaClienteCabTarjetas = facturaClienteCabTarjetas;
    }

    public List<FacturaClienteCabTipoPagoDTO> getFacturaClienteCabTipoPagos() {
        if (this.facturaClienteCabTipoPagos == null) {
            this.facturaClienteCabTipoPagos = new ArrayList<FacturaClienteCabTipoPagoDTO>();
        }
        return this.facturaClienteCabTipoPagos;
    }

    public void setFacturaClienteCabTipoPagos(
            List<FacturaClienteCabTipoPagoDTO> facturaClienteCabTipoPagos) {
        this.facturaClienteCabTipoPagos = facturaClienteCabTipoPagos;
    }

    public List<FacturaClienteDetDTO> getFacturaClienteDets() {
        if (this.facturaClienteDets == null) {
            this.facturaClienteDets = new ArrayList<FacturaClienteDetDTO>();
        }
        return this.facturaClienteDets;
    }

    public void setFacturaClienteDets(
            List<FacturaClienteDetDTO> facturaClienteDets) {
        this.facturaClienteDets = facturaClienteDets;
    }

    public List<NotaCreditoCabDTO> getNotaCreditoCabs() {
        if (this.notaCreditoCabs == null) {
            this.notaCreditoCabs = new ArrayList<NotaCreditoCabDTO>();
        }
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCabDTO> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public List<NotaRemisionCabDTO> getNotaRemisionCabs() {
        if (this.notaRemisionCabs == null) {
            this.notaRemisionCabs = new ArrayList<NotaRemisionCabDTO>();
        }
        return this.notaRemisionCabs;
    }

    public void setNotaRemisionCabs(List<NotaRemisionCabDTO> notaRemisionCabs) {
        this.notaRemisionCabs = notaRemisionCabs;
    }

    // *****************************************************************************************
    public List<CancelacionFacturaDTO> getCancelacionFacturas() {
        if (this.cancelacionFacturas == null) {
            this.cancelacionFacturas = new ArrayList<CancelacionFacturaDTO>();
        }
        return cancelacionFacturas;
    }

    public void setCancelacionFacturas(
            List<CancelacionFacturaDTO> cancelacionFacturas) {
        this.cancelacionFacturas = cancelacionFacturas;
    }

    public CajaDTO getCaja() {
        return caja;
    }

    public void setCaja(CajaDTO caja) {
        this.caja = caja;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

    public List<FacturaClienteCabChequeDTO> getFacturaClienteCabCheque() {
        if (this.facturaClienteCabCheque == null) {
            this.facturaClienteCabCheque = new ArrayList<FacturaClienteCabChequeDTO>();
        }
        return facturaClienteCabCheque;
    }

    public void setFacturaClienteCabCheque(
            List<FacturaClienteCabChequeDTO> facturaClienteCabCheque) {
        this.facturaClienteCabCheque = facturaClienteCabCheque;
    }

    public List<FacturaClienteCabEfectivoDTO> getFacturaClienteCabEfectivo() {
        if (this.facturaClienteCabEfectivo == null) {
            this.facturaClienteCabEfectivo = new ArrayList<FacturaClienteCabEfectivoDTO>();
        }
        return facturaClienteCabEfectivo;
    }

    public void setFacturaClienteCabEfectivo(
            List<FacturaClienteCabEfectivoDTO> facturaClienteCabEfectivo) {
        this.facturaClienteCabEfectivo = facturaClienteCabEfectivo;
    }

    public List<FacturaClienteCabFuncionarioDTO> getFacturaClienteCabFuncionario() {
        if (this.facturaClienteCabFuncionario == null) {
            this.facturaClienteCabFuncionario = new ArrayList<FacturaClienteCabFuncionarioDTO>();
        }
        return facturaClienteCabFuncionario;
    }

    public void setFacturaClienteCabFuncionario(
            List<FacturaClienteCabFuncionarioDTO> facturaClienteCabFuncionario) {
        this.facturaClienteCabFuncionario = facturaClienteCabFuncionario;
    }

    public List<FacturaClienteCabTarjFielDTO> getFacturaClienteCabTarjFiel() {
        if (this.facturaClienteCabTarjFiel == null) {
            this.facturaClienteCabTarjFiel = new ArrayList<FacturaClienteCabTarjFielDTO>();
        }
        return facturaClienteCabTarjFiel;
    }

    public void setFacturaClienteCabTarjFiel(
            List<FacturaClienteCabTarjFielDTO> facturaClienteCabTarjFiel) {
        this.facturaClienteCabTarjFiel = facturaClienteCabTarjFiel;
    }

    public List<CancelacionProductoDTO> getCancelacionProducto() {
        if (this.cancelacionProducto == null) {
            this.cancelacionProducto = new ArrayList<CancelacionProductoDTO>();
        }
        return cancelacionProducto;
    }

    public void setCancelacionProducto(
            List<CancelacionProductoDTO> cancelacionProducto) {
        this.cancelacionProducto = cancelacionProducto;
    }

    public List<FacturaClienteCabNotaCreditoDTO> getFacturaClienteCabNotaCredito() {
        if (this.facturaClienteCabNotaCredito == null) {
            this.facturaClienteCabNotaCredito = new ArrayList<FacturaClienteCabNotaCreditoDTO>();
        }
        return facturaClienteCabNotaCredito;
    }

    public void setFacturaClienteCabNotaCredito(
            List<FacturaClienteCabNotaCreditoDTO> facturaClienteCabNotaCredito) {
        this.facturaClienteCabNotaCredito = facturaClienteCabNotaCredito;
    }

    public List<FacturaClienteCabMonedaExtranjeraDTO> getFacturaClienteCabMonedaExtranjera() {
        if (this.facturaClienteCabMonedaExtranjera == null) {
            this.facturaClienteCabMonedaExtranjera = new ArrayList<FacturaClienteCabMonedaExtranjeraDTO>();
        }
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            List<FacturaClienteCabMonedaExtranjeraDTO> facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

    public List<FacturaClienteCabRetencionDTO> getFacturaClienteCabRetencion() {
        if (this.facturaClienteCabRetencion == null) {
            this.facturaClienteCabRetencion = new ArrayList<FacturaClienteCabRetencionDTO>();
        }
        return facturaClienteCabRetencion;
    }

    public void setFacturaClienteCabRetencion(
            List<FacturaClienteCabRetencionDTO> facturaClienteCabRetencion) {
        this.facturaClienteCabRetencion = facturaClienteCabRetencion;
    }

    // *****************************************************************************************
    public List<FacturaCabClientePendienteDTO> getFacturaCabClientePendiente() {
        if (this.facturaCabClientePendiente == null) {
            this.facturaCabClientePendiente = new ArrayList<>();
        }
        return facturaCabClientePendiente;
    }

    public void setFacturaCabClientePendiente(List<FacturaCabClientePendienteDTO> facturaCabClientePendiente) {
        this.facturaCabClientePendiente = facturaCabClientePendiente;
    }

    public List<FacturaClienteCabPromoTempArtDTO> getFacturaClienteCabPromoTempArt() {
        return facturaClienteCabPromoTempArt;
    }

    public void setFacturaClienteCabPromoTempArt(List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTempArt) {
        this.facturaClienteCabPromoTempArt = facturaClienteCabPromoTempArt;
    }
}
