package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf7;
    private PromoTemporadaDTO promoTemporada;
    private Nf7Secnom7DTO nf7Secnom7;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf7DTO() {
    }

    public Long getIdPromoTemporadaNf7() {
        return this.idPromoTemporadaNf7;
    }

    public void setIdPromoTemporadaNf7(Long idPromoTemporadaNf7) {
        this.idPromoTemporadaNf7 = idPromoTemporadaNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf7Secnom7DTO getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
