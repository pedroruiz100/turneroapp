package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the desc_combo database table.
 *
 */
public class DescComboDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescCombo;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteDetDTO facturaClienteDet;

    public DescComboDTO() {
    }

    public Long getIdDescCombo() {
        return this.idDescCombo;
    }

    public void setIdDescCombo(Long idDescCombo) {
        this.idDescCombo = idDescCombo;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteDetDTO getFacturaClienteDet() {
        return this.facturaClienteDet;
    }

    public void setFacturaClienteDet(FacturaClienteDetDTO facturaClienteDet) {
        this.facturaClienteDet = facturaClienteDet;
    }

}
