package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the tipo_cliente database table.
 *
 */
public class TipoClienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoCliente;

    public TipoClienteDTO() {
    }

    public Long getIdTipoCliente() {
        return this.idTipoCliente;
    }

    public void setIdTipoCliente(Long idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

}
