package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf4DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf4;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf4Seccion1DTO nf4Seccion1;

    public DescuentoFielNf4DTO() {
    }

    public Long getIdDescuentoFielNf4() {
        return this.idDescuentoFielNf4;
    }

    public void setIdDescuentoFielNf4(Long idDescuentoFielNf4) {
        this.idDescuentoFielNf4 = idDescuentoFielNf4;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf4Seccion1DTO getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

}
