package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SucursalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSucursal;
    private String callePrincipal;
    private String codSucursal;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Integer nroLocal;
    private String primeraLateral;
    private String segundaLateral;
    private String usuAlta;
    private String usuMod;
    private BarrioDTO barrioDTO;
    private CiudadDTO ciudadDTO;
    private DepartamentoDTO departamentoDTO;
    private EmpresaDTO empresaDTO;
    private String telefono;
    private List<TalonariosSucursaleDTO> talonariosSucursalesDTO;
    private List<ComboCabDTO> comboCabDTO;
    private List<FacturaClienteCabDTO> facturaClienteCabDTO;
    private List<NotaCreditoCabDTO> notaCreditoCabDTO;
    private List<CajaDTO> cajaDTO;

    public SucursalDTO() {
    }

    public List<ComboCabDTO> getComboCabDTO() {
        if (this.comboCabDTO == null) {
            this.comboCabDTO = new ArrayList<ComboCabDTO>();
        }
        return comboCabDTO;
    }

    public void setComboCabDTO(List<ComboCabDTO> comboCabDTO) {
        this.comboCabDTO = comboCabDTO;
    }

    public List<FacturaClienteCabDTO> getFacturaClienteCabDTO() {
        if (this.facturaClienteCabDTO == null) {
            this.facturaClienteCabDTO = new ArrayList<FacturaClienteCabDTO>();
        }
        return facturaClienteCabDTO;
    }

    public void setFacturaClienteCabDTO(
            List<FacturaClienteCabDTO> facturaClienteCabDTO) {
        this.facturaClienteCabDTO = facturaClienteCabDTO;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public List<NotaCreditoCabDTO> getNotaCreditoCabDTO() {
        if (this.notaCreditoCabDTO == null) {
            this.notaCreditoCabDTO = new ArrayList<NotaCreditoCabDTO>();
        }
        return notaCreditoCabDTO;
    }

    public void setNotaCreditoCabDTO(List<NotaCreditoCabDTO> notaCreditoCabDTO) {
        this.notaCreditoCabDTO = notaCreditoCabDTO;
    }

    public Long getIdSucursal() {
        return this.idSucursal;
    }

    public void setIdSucursal(Long idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getCallePrincipal() {
        return this.callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCodSucursal() {
        return this.codSucursal;
    }

    public List<CajaDTO> getCajaDTO() {
        if (this.cajaDTO == null) {
            this.cajaDTO = new ArrayList<CajaDTO>();
        }
        return cajaDTO;
    }

    public void setCajaDTO(List<CajaDTO> cajaDTO) {
        this.cajaDTO = cajaDTO;
    }

    public void setCodSucursal(String codSucursal) {
        this.codSucursal = codSucursal;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<TalonariosSucursaleDTO> getTalonariosSucursalesDTO() {
        if (this.talonariosSucursalesDTO == null) {
            this.talonariosSucursalesDTO = new ArrayList<TalonariosSucursaleDTO>();
        }
        return talonariosSucursalesDTO;
    }

    public void setTalonariosSucursalesDTO(
            List<TalonariosSucursaleDTO> talonariosSucursalesDTOs) {
        this.talonariosSucursalesDTO = talonariosSucursalesDTOs;
    }

    public BarrioDTO getBarrioDTO() {
        return barrioDTO;
    }

    public void setBarrioDTO(BarrioDTO barrioDTO) {
        this.barrioDTO = barrioDTO;
    }

    public CiudadDTO getCiudadDTO() {
        return ciudadDTO;
    }

    public void setCiudadDTO(CiudadDTO ciudadDTO) {
        this.ciudadDTO = ciudadDTO;
    }

    public DepartamentoDTO getDepartamentoDTO() {
        return departamentoDTO;
    }

    public void setDepartamentoDTO(DepartamentoDTO departamentoDTO) {
        this.departamentoDTO = departamentoDTO;
    }

    public EmpresaDTO getEmpresaDTO() {
        return empresaDTO;
    }

    public void setEmpresaDTO(EmpresaDTO empresaDTO) {
        this.empresaDTO = empresaDTO;
    }

    // *****************************************************************//
}
