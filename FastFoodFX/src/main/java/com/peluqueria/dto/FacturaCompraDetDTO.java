package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class FacturaCompraDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaCompraDet;
    private FacturaCompraCabDTO facturaCompraCab;
    private ArticuloDTO articulo;
    private BigDecimal cantidad;
    private long precio;
    private String descripcion;
    private Integer poriva;
    private long iva;
    private Integer orden;
    private String sec1;
    private String sec2;
    private Long codArticulo;
    private String deposito;
    private String tipo;
    private String medida;
    private String contenido;
    private String existencia;
    private String descuento;
    private String ivaExtranjero;
    private String precioExtranjero;

    public FacturaCompraDetDTO() {
    }

    public Long getIdFacturaCompraDet() {
        return idFacturaCompraDet;
    }

    public void setIdFacturaCompraDet(Long idFacturaCompraDet) {
        this.idFacturaCompraDet = idFacturaCompraDet;
    }

    public FacturaCompraCabDTO getFacturaCompraCab() {
        return facturaCompraCab;
    }

    public void setFacturaCompraCab(FacturaCompraCabDTO facturaCompraCab) {
        this.facturaCompraCab = facturaCompraCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public String getIvaExtranjero() {
        return ivaExtranjero;
    }

    public void setIvaExtranjero(String ivaExtranjero) {
        this.ivaExtranjero = ivaExtranjero;
    }

    public String getPrecioExtranjero() {
        return precioExtranjero;
    }

    public void setPrecioExtranjero(String precioExtranjero) {
        this.precioExtranjero = precioExtranjero;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPoriva() {
        return poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public long getPrecio() {
        return precio;
    }

    public void setPrecio(long precio) {
        this.precio = precio;
    }

    public long getIva() {
        return iva;
    }

    public void setIva(long iva) {
        this.iva = iva;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getSec1() {
        return sec1;
    }

    public void setSec1(String sec1) {
        this.sec1 = sec1;
    }

    public String getSec2() {
        return sec2;
    }

    public void setSec2(String sec2) {
        this.sec2 = sec2;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getDeposito() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getExistencia() {
        return existencia;
    }

    public void setExistencia(String existencia) {
        this.existencia = existencia;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

}
