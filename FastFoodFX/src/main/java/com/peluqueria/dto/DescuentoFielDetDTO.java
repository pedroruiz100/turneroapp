package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielDet;
    private DiaDTO dia;
    private DescuentoFielCabDTO descuentoFielCab;

    public DescuentoFielDetDTO() {
    }

    public Long getIdDescuentoFielDet() {
        return idDescuentoFielDet;
    }

    public void setIdDescuentoFielDet(Long idDescuentoFielDet) {
        this.idDescuentoFielDet = idDescuentoFielDet;
    }

    public DiaDTO getDia() {
        return dia;
    }

    public void setDia(DiaDTO dia) {
        this.dia = dia;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

}
