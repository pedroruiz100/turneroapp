package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the tarjeta_convenio database table.
 *
 */
public class TarjetaConvenioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTarjetaConvenio;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Boolean habilitado;
    private String usuAlta;
    private String usuMod;
    private FamiliaTarjDTO familiaTarj;
    private List<FacturaClienteCabTarjConvenioDTO> facturaClienteCabTarjConvenioDTO;

    public TarjetaConvenioDTO() {
    }

    public Long getIdTarjetaConvenio() {
        return this.idTarjetaConvenio;
    }

    public void setIdTarjetaConvenio(Long idTarjetaConvenio) {
        this.idTarjetaConvenio = idTarjetaConvenio;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public FamiliaTarjDTO getFamiliaTarj() {
        return this.familiaTarj;
    }

    public void setFamiliaTarj(FamiliaTarjDTO familiaTarj) {
        this.familiaTarj = familiaTarj;
    }

    public List<FacturaClienteCabTarjConvenioDTO> getFacturaClienteCabTarjConvenioDTO() {
        if (this.facturaClienteCabTarjConvenioDTO == null) {
            this.facturaClienteCabTarjConvenioDTO = new ArrayList<FacturaClienteCabTarjConvenioDTO>();
        }
        return facturaClienteCabTarjConvenioDTO;
    }

    public void setFacturaClienteCabTarjConvenioDTO(
            List<FacturaClienteCabTarjConvenioDTO> facturaClienteCabTarjConvenioDTO) {
        this.facturaClienteCabTarjConvenioDTO = facturaClienteCabTarjConvenioDTO;
    }

}
