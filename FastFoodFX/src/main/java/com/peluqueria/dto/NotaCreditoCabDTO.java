package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.List;

/**
 * The persistent class for the nota_credito_cab database table.
 *
 */
public class NotaCreditoCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaCreditoCab;
    private Time fechaAnulacion;
    private Time fechaEmision;
    private Integer idCajeroAnulacion;
    private ClienteDTO cliente;
    private SucursalDTO sucursal;
    private BigDecimal montoPago;
    private String motivoAnulacion;
    private String nroNotaCredito;
    private String observaciones;
    private String razon;
    private BigDecimal totalCredito;
    private FacturaClienteCabDTO facturaClienteCab;
    private TipoComprobanteDTO tipoComprobante;
    private TipoMonedaDTO tipoMoneda;
    private List<NotaCreditoDetDTO> notaCreditoDets;

    public NotaCreditoCabDTO() {
    }

    public Long getIdNotaCreditoCab() {
        return this.idNotaCreditoCab;
    }

    public void setIdNotaCreditoCab(Long idNotaCreditoCab) {
        this.idNotaCreditoCab = idNotaCreditoCab;
    }

    public Time getFechaAnulacion() {
        return this.fechaAnulacion;
    }

    public void setFechaAnulacion(Time fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public Time getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Time fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Integer getIdCajeroAnulacion() {
        return this.idCajeroAnulacion;
    }

    public void setIdCajeroAnulacion(Integer idCajeroAnulacion) {
        this.idCajeroAnulacion = idCajeroAnulacion;
    }

    public BigDecimal getMontoPago() {
        return this.montoPago;
    }

    public void setMontoPago(BigDecimal montoPago) {
        this.montoPago = montoPago;
    }

    public String getMotivoAnulacion() {
        return this.motivoAnulacion;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getNroNotaCredito() {
        return this.nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getRazon() {
        return this.razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public BigDecimal getTotalCredito() {
        return this.totalCredito;
    }

    public void setTotalCredito(BigDecimal totalCredito) {
        this.totalCredito = totalCredito;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoComprobanteDTO getTipoComprobante() {
        return this.tipoComprobante;
    }

    public void setTipoComprobante(TipoComprobanteDTO tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public TipoMonedaDTO getTipoMoneda() {
        return this.tipoMoneda;
    }

    public void setTipoMoneda(TipoMonedaDTO tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public List<NotaCreditoDetDTO> getNotaCreditoDets() {
        return this.notaCreditoDets;
    }

    public void setNotaCreditoDets(List<NotaCreditoDetDTO> notaCreditoDets) {
        this.notaCreditoDets = notaCreditoDets;
    }

    public NotaCreditoDetDTO addNotaCreditoDet(NotaCreditoDetDTO notaCreditoDet) {
        getNotaCreditoDets().add(notaCreditoDet);
        notaCreditoDet.setNotaCreditoCab(this);

        return notaCreditoDet;
    }

    public NotaCreditoDetDTO removeNotaCreditoDet(NotaCreditoDetDTO notaCreditoDet) {
        getNotaCreditoDets().remove(notaCreditoDet);
        notaCreditoDet.setNotaCreditoCab(null);

        return notaCreditoDet;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

}
