package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArqueoCajaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArqueoCaja;
    private CajaDTO caja;
    private SupervisorDTO supervisor;
    private Integer asociacion;
    private Integer cantAsociacion;
    private Integer cantChequeAdel;
    private Integer cantChequeDia;
    private Integer cantDolar;
    private Integer cantFactory;
    private Integer cantFco;
    private Integer zeta;
    private Integer cantFcoCanje;
    private Integer cantFcred;
    private Integer cantFcredCanje;
    private Integer cantGifcard;
    private Integer cantInfonet;
    private Integer cantNotaCred;
    private Integer cantPeso;
    private Integer cantPractipago;
    private Integer cantPronet;
    private String nroTimbrado;
    private String facturaIni;
    private String facturaFin;
    private Integer cantReal;
    private Integer cantSencillo;
    private Integer cantVale;
    private Integer chequeAdel;
    private Integer chequeDia;
    private Integer dolar;
    private Integer efectivo;
    private Integer factory;
    private Integer fco;
    private Integer fcoCanje;
    private Integer fcred;
    private Integer fcredCanje;
    private Timestamp fechaEmision;
    private Integer gifcard;
    private Integer infonet;
    private Integer notaCred;
    private Integer peso;
    private Integer practipago;
    private Integer pronet;
    private Integer real;
    private Integer sencillo;
    private Integer vale;
    private Integer contDesc;
    private Integer sumtDesc;
    private Integer contTarj;
    private Integer sumTarj;
    private Integer contEfectivo;
    private Integer sumEfectivo;
    private Integer cantEferecibida;
    private Integer contDonacion;
    private Integer sumDonacion;
    private Integer totgra;
    private Integer totexe;
    private Integer totgra5;
    private Integer totgra10;
    private Integer totales;
    private Integer numCliente;
    private Integer numArticulo;
    private Integer numFuncionario;
    private Integer v100000;
    private Integer v50000;
    private Integer v10000;
    private Integer v5000;
    private Integer v1000;
    private Integer v500;
    private Integer v100;
    private Integer v50;
    private Integer contNotaCred;
    private Integer sumNotaCred;
    private Integer contVale;
    private Integer sumVale;
    private Integer contFact;
    private Integer sumFact;
    private Integer contCheque;
    private Integer sumCheque;
    private Integer contAsoc;
    private Integer sumAsoc;
    private Integer contDolar;
    private Integer sumDolar;
    private Integer contReal;
    private Integer sumReal;
    private Integer contPeso;
    private Integer sumPeso;
    private Integer sumRetencion;
    private Integer contRetencion;

    public ArqueoCajaDTO() {
        super();
    }

    public Integer getV100000() {
        return v100000;
    }

    public void setV100000(Integer v100000) {
        this.v100000 = v100000;
    }

    public Integer getV50000() {
        return v50000;
    }

    public void setV50000(Integer v50000) {
        this.v50000 = v50000;
    }

    public Integer getV10000() {
        return v10000;
    }

    public void setV10000(Integer v10000) {
        this.v10000 = v10000;
    }

    public Integer getV5000() {
        return v5000;
    }

    public void setV5000(Integer v5000) {
        this.v5000 = v5000;
    }

    public Integer getV1000() {
        return v1000;
    }

    public void setV1000(Integer v1000) {
        this.v1000 = v1000;
    }

    public Integer getV500() {
        return v500;
    }

    public Integer getContRetencion() {
        return contRetencion;
    }

    public void setContRetencion(Integer contRetencion) {
        this.contRetencion = contRetencion;
    }

    public void setV500(Integer v500) {
        this.v500 = v500;
    }

    public Integer getContNotaCred() {
        return contNotaCred;
    }

    public Integer getContDolar() {
        return contDolar;
    }

    public void setContDolar(Integer contDolar) {
        this.contDolar = contDolar;
    }

    public Integer getSumDolar() {
        return sumDolar;
    }

    public void setSumDolar(Integer sumDolar) {
        this.sumDolar = sumDolar;
    }

    public Integer getContReal() {
        return contReal;
    }

    public void setContReal(Integer contReal) {
        this.contReal = contReal;
    }

    public Integer getSumReal() {
        return sumReal;
    }

    public void setSumReal(Integer sumReal) {
        this.sumReal = sumReal;
    }

    public Integer getContPeso() {
        return contPeso;
    }

    public void setContPeso(Integer contPeso) {
        this.contPeso = contPeso;
    }

    public Integer getSumPeso() {
        return sumPeso;
    }

    public void setSumPeso(Integer sumPeso) {
        this.sumPeso = sumPeso;
    }

    public Integer getSumRetencion() {
        return sumRetencion;
    }

    public void setSumRetencion(Integer sumRetencion) {
        this.sumRetencion = sumRetencion;
    }

    public void setContNotaCred(Integer contNotaCred) {
        this.contNotaCred = contNotaCred;
    }

    public Integer getSumNotaCred() {
        return sumNotaCred;
    }

    public void setSumNotaCred(Integer sumNotaCred) {
        this.sumNotaCred = sumNotaCred;
    }

    public Integer getContVale() {
        return contVale;
    }

    public void setContVale(Integer contVale) {
        this.contVale = contVale;
    }

    public Integer getSumVale() {
        return sumVale;
    }

    public void setSumVale(Integer sumVale) {
        this.sumVale = sumVale;
    }

    public Integer getContFact() {
        return contFact;
    }

    public void setContFact(Integer contFact) {
        this.contFact = contFact;
    }

    public Integer getSumFact() {
        return sumFact;
    }

    public void setSumFact(Integer sumFact) {
        this.sumFact = sumFact;
    }

    public Integer getContCheque() {
        return contCheque;
    }

    public void setContCheque(Integer contCheque) {
        this.contCheque = contCheque;
    }

    public Integer getSumCheque() {
        return sumCheque;
    }

    public void setSumCheque(Integer sumCheque) {
        this.sumCheque = sumCheque;
    }

    public Integer getContAsoc() {
        return contAsoc;
    }

    public void setContAsoc(Integer contAsoc) {
        this.contAsoc = contAsoc;
    }

    public Integer getSumAsoc() {
        return sumAsoc;
    }

    public void setSumAsoc(Integer sumAsoc) {
        this.sumAsoc = sumAsoc;
    }

    public Integer getV100() {
        return v100;
    }

    public void setV100(Integer v100) {
        this.v100 = v100;
    }

    public Integer getV50() {
        return v50;
    }

    public void setV50(Integer v50) {
        this.v50 = v50;
    }

    public Long getIdArqueoCaja() {
        return idArqueoCaja;
    }

    public void setIdArqueoCaja(Long idArqueoCaja) {
        this.idArqueoCaja = idArqueoCaja;
    }

    public CajaDTO getCaja() {
        return caja;
    }

    public Integer getZeta() {
        return zeta;
    }

    public void setZeta(Integer zeta) {
        this.zeta = zeta;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public Integer getContDesc() {
        return contDesc;
    }

    public void setContDesc(Integer contDesc) {
        this.contDesc = contDesc;
    }

    public Integer getSumtDesc() {
        return sumtDesc;
    }

    public void setSumtDesc(Integer sumtDesc) {
        this.sumtDesc = sumtDesc;
    }

    public Integer getContTarj() {
        return contTarj;
    }

    public void setContTarj(Integer contTarj) {
        this.contTarj = contTarj;
    }

    public Integer getSumTarj() {
        return sumTarj;
    }

    public void setSumTarj(Integer sumTarj) {
        this.sumTarj = sumTarj;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getFacturaIni() {
        return facturaIni;
    }

    public void setFacturaIni(String facturaIni) {
        this.facturaIni = facturaIni;
    }

    public String getFacturaFin() {
        return facturaFin;
    }

    public void setFacturaFin(String facturaFin) {
        this.facturaFin = facturaFin;
    }

    public void setCaja(CajaDTO caja) {
        this.caja = caja;
    }

    public SupervisorDTO getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(SupervisorDTO supervisor) {
        this.supervisor = supervisor;
    }

    public Integer getAsociacion() {
        return asociacion;
    }

    public void setAsociacion(Integer asociacion) {
        this.asociacion = asociacion;
    }

    public Integer getCantAsociacion() {
        return cantAsociacion;
    }

    public void setCantAsociacion(Integer cantAsociacion) {
        this.cantAsociacion = cantAsociacion;
    }

    public Integer getCantChequeAdel() {
        return cantChequeAdel;
    }

    public void setCantChequeAdel(Integer cantChequeAdel) {
        this.cantChequeAdel = cantChequeAdel;
    }

    public Integer getCantChequeDia() {
        return cantChequeDia;
    }

    public void setCantChequeDia(Integer cantChequeDia) {
        this.cantChequeDia = cantChequeDia;
    }

    public Integer getCantDolar() {
        return cantDolar;
    }

    public void setCantDolar(Integer cantDolar) {
        this.cantDolar = cantDolar;
    }

    public Integer getCantFactory() {
        return cantFactory;
    }

    public void setCantFactory(Integer cantFactory) {
        this.cantFactory = cantFactory;
    }

    public Integer getCantFco() {
        return cantFco;
    }

    public void setCantFco(Integer cantFco) {
        this.cantFco = cantFco;
    }

    public Integer getCantFcoCanje() {
        return cantFcoCanje;
    }

    public void setCantFcoCanje(Integer cantFcoCanje) {
        this.cantFcoCanje = cantFcoCanje;
    }

    public Integer getCantFcred() {
        return cantFcred;
    }

    public void setCantFcred(Integer cantFcred) {
        this.cantFcred = cantFcred;
    }

    public Integer getCantFcredCanje() {
        return cantFcredCanje;
    }

    public void setCantFcredCanje(Integer cantFcredCanje) {
        this.cantFcredCanje = cantFcredCanje;
    }

    public Integer getCantGifcard() {
        return cantGifcard;
    }

    public void setCantGifcard(Integer cantGifcard) {
        this.cantGifcard = cantGifcard;
    }

    public Integer getCantInfonet() {
        return cantInfonet;
    }

    public void setCantInfonet(Integer cantInfonet) {
        this.cantInfonet = cantInfonet;
    }

    public Integer getCantNotaCred() {
        return cantNotaCred;
    }

    public void setCantNotaCred(Integer cantNotaCred) {
        this.cantNotaCred = cantNotaCred;
    }

    public Integer getCantPeso() {
        return cantPeso;
    }

    public void setCantPeso(Integer cantPeso) {
        this.cantPeso = cantPeso;
    }

    public Integer getCantPractipago() {
        return cantPractipago;
    }

    public void setCantPractipago(Integer cantPractipago) {
        this.cantPractipago = cantPractipago;
    }

    public Integer getCantPronet() {
        return cantPronet;
    }

    public void setCantPronet(Integer cantPronet) {
        this.cantPronet = cantPronet;
    }

    public Integer getCantReal() {
        return cantReal;
    }

    public void setCantReal(Integer cantReal) {
        this.cantReal = cantReal;
    }

    public Integer getCantSencillo() {
        return cantSencillo;
    }

    public void setCantSencillo(Integer cantSencillo) {
        this.cantSencillo = cantSencillo;
    }

    public Integer getCantVale() {
        return cantVale;
    }

    public void setCantVale(Integer cantVale) {
        this.cantVale = cantVale;
    }

    public Integer getChequeAdel() {
        return chequeAdel;
    }

    public void setChequeAdel(Integer chequeAdel) {
        this.chequeAdel = chequeAdel;
    }

    public Integer getChequeDia() {
        return chequeDia;
    }

    public void setChequeDia(Integer chequeDia) {
        this.chequeDia = chequeDia;
    }

    public Integer getDolar() {
        return dolar;
    }

    public void setDolar(Integer dolar) {
        this.dolar = dolar;
    }

    public Integer getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(Integer efectivo) {
        this.efectivo = efectivo;
    }

    public Integer getFactory() {
        return factory;
    }

    public void setFactory(Integer factory) {
        this.factory = factory;
    }

    public Integer getFco() {
        return fco;
    }

    public void setFco(Integer fco) {
        this.fco = fco;
    }

    public Integer getFcoCanje() {
        return fcoCanje;
    }

    public void setFcoCanje(Integer fcoCanje) {
        this.fcoCanje = fcoCanje;
    }

    public Integer getFcred() {
        return fcred;
    }

    public void setFcred(Integer fcred) {
        this.fcred = fcred;
    }

    public Integer getFcredCanje() {
        return fcredCanje;
    }

    public void setFcredCanje(Integer fcredCanje) {
        this.fcredCanje = fcredCanje;
    }

    public Timestamp getFechaEmision() {
        return fechaEmision;
    }

    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Integer getGifcard() {
        return gifcard;
    }

    public void setGifcard(Integer gifcard) {
        this.gifcard = gifcard;
    }

    public Integer getInfonet() {
        return infonet;
    }

    public void setInfonet(Integer infonet) {
        this.infonet = infonet;
    }

    public Integer getNotaCred() {
        return notaCred;
    }

    public void setNotaCred(Integer notaCred) {
        this.notaCred = notaCred;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getPractipago() {
        return practipago;
    }

    public void setPractipago(Integer practipago) {
        this.practipago = practipago;
    }

    public Integer getPronet() {
        return pronet;
    }

    public void setPronet(Integer pronet) {
        this.pronet = pronet;
    }

    public Integer getReal() {
        return real;
    }

    public void setReal(Integer real) {
        this.real = real;
    }

    public Integer getSencillo() {
        return sencillo;
    }

    public void setSencillo(Integer sencillo) {
        this.sencillo = sencillo;
    }

    public Integer getVale() {
        return vale;
    }

    public void setVale(Integer vale) {
        this.vale = vale;
    }

    public Integer getContEfectivo() {
        return contEfectivo;
    }

    public void setContEfectivo(Integer contEfectivo) {
        this.contEfectivo = contEfectivo;
    }

    public Integer getSumEfectivo() {
        return sumEfectivo;
    }

    public void setSumEfectivo(Integer sumEfectivo) {
        this.sumEfectivo = sumEfectivo;
    }

    public Integer getCantEferecibida() {
        return cantEferecibida;
    }

    public void setCantEferecibida(Integer cantEferecibida) {
        this.cantEferecibida = cantEferecibida;
    }

    public Integer getContDonacion() {
        return contDonacion;
    }

    public void setContDonacion(Integer contDonacion) {
        this.contDonacion = contDonacion;
    }

    public Integer getSumDonacion() {
        return sumDonacion;
    }

    public void setSumDonacion(Integer sumDonacion) {
        this.sumDonacion = sumDonacion;
    }

    public Integer getTotgra() {
        return totgra;
    }

    public void setTotgra(Integer totgra) {
        this.totgra = totgra;
    }

    public Integer getTotexe() {
        return totexe;
    }

    public void setTotexe(Integer totexe) {
        this.totexe = totexe;
    }

    public Integer getTotgra5() {
        return totgra5;
    }

    public void setTotgra5(Integer totgra5) {
        this.totgra5 = totgra5;
    }

    public Integer getTotgra10() {
        return totgra10;
    }

    public void setTotgra10(Integer totgra10) {
        this.totgra10 = totgra10;
    }

    public Integer getTotales() {
        return totales;
    }

    public void setTotales(Integer totales) {
        this.totales = totales;
    }

    public Integer getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(Integer numCliente) {
        this.numCliente = numCliente;
    }

    public Integer getNumArticulo() {
        return numArticulo;
    }

    public void setNumArticulo(Integer numArticulo) {
        this.numArticulo = numArticulo;
    }

    public Integer getNumFuncionario() {
        return numFuncionario;
    }

    public void setNumFuncionario(Integer numFuncionario) {
        this.numFuncionario = numFuncionario;
    }

}
