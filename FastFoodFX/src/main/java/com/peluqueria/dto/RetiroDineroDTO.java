package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class RetiroDineroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRetiro;
    private Timestamp fechaRetiro;
    private UsuarioDTO usuarioCajero;
    private UsuarioDTO usuarioSupervisor;
    private BigDecimal montoRetiro;
    private CajaDTO caja;
    private MotivoRetiroDineroDTO motivoRetiroDinero;

    public RetiroDineroDTO() {
    }

    public Long getIdRetiro() {
        return this.idRetiro;
    }

    public void setIdRetiro(Long idRetiro) {
        this.idRetiro = idRetiro;
    }

    public Timestamp getFechaRetiro() {
        return this.fechaRetiro;
    }

    public void setFechaRetiro(Timestamp fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public BigDecimal getMontoRetiro() {
        return this.montoRetiro;
    }

    public void setMontoRetiro(BigDecimal montoRetiro) {
        this.montoRetiro = montoRetiro;
    }

    public CajaDTO getCaja() {
        return this.caja;
    }

    public void setCaja(CajaDTO caja) {
        this.caja = caja;
    }

    public MotivoRetiroDineroDTO getMotivoRetiroDinero() {
        return this.motivoRetiroDinero;
    }

    public void setMotivoRetiroDinero(MotivoRetiroDineroDTO motivoRetiroDinero) {
        this.motivoRetiroDinero = motivoRetiroDinero;
    }

    public UsuarioDTO getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public UsuarioDTO getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(UsuarioDTO usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

}
