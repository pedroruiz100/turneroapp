package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf4DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf4;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf4Seccion1DTO nf4Seccion1;
    private String descriSeccion;

    public DescuentoTarjetaCabNf4DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf4() {
        return this.idDescuentoTarjetaCabNf4;
    }

    public void setIdDescuentoTarjetaCabNf4(Long idDescuentoTarjetaCabNf4) {
        this.idDescuentoTarjetaCabNf4 = idDescuentoTarjetaCabNf4;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf4Seccion1DTO getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

}
