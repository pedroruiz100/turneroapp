package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the iva database table.
 *
 */
public class IvaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idIva;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Integer poriva;
    private String tipoImp;
    private String usuAlta;
    private String usuMod;
    private List<ArticuloDTO> articulos;
    private List<ComboCabDTO> comboCabs;

    public IvaDTO() {
    }

    public Long getIdIva() {
        return this.idIva;
    }

    public void setIdIva(Long idIva) {
        this.idIva = idIva;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getPoriva() {
        return this.poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public String getTipoImp() {
        return this.tipoImp;
    }

    public void setTipoImp(String tipoImp) {
        this.tipoImp = tipoImp;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloDTO> getArticulos() {
        if (this.articulos == null) {
            this.articulos = new ArrayList<ArticuloDTO>();
        }
        return this.articulos;
    }

    public void setArticulos(List<ArticuloDTO> articulos) {
        this.articulos = articulos;
    }

    public List<ComboCabDTO> getComboCabs() {
        if (this.comboCabs == null) {
            this.comboCabs = new ArrayList<ComboCabDTO>();
        }
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCabDTO> comboCabs) {
        this.comboCabs = comboCabs;
    }

}
