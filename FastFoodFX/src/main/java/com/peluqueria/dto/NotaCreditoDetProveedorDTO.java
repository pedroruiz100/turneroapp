package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the nota_credito_det database table.
 *
 */
public class NotaCreditoDetProveedorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaCreditoDetProveedor;
    private Long cantidad;
    private Long precio;
    private Long total;
    private String descripcion;
    private ArticuloDTO articulo;

    public NotaCreditoDetProveedorDTO() {
    }

    public Long getIdNotaCreditoDetProveedor() {
        return idNotaCreditoDetProveedor;
    }

    public void setIdNotaCreditoDetProveedor(Long idNotaCreditoDetProveedor) {
        this.idNotaCreditoDetProveedor = idNotaCreditoDetProveedor;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

}
