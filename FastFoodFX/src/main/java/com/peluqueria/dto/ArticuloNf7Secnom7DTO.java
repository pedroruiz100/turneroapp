package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf7Secnom7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf7Secnom7Articulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf7Secnom7DTO nf7Secnom7;

    public ArticuloNf7Secnom7DTO() {
    }

    public Long getIdNf7Secnom7Articulo() {
        return this.idNf7Secnom7Articulo;
    }

    public void setIdNf7Secnom7Articulo(Long idNf7Secnom7Articulo) {
        this.idNf7Secnom7Articulo = idNf7Secnom7Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf7Secnom7DTO getNf7Secnom7() {
        return this.nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

}
