package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PedidoCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPedidoCab;

    private String usuario;

    private String plazo;

    private String nroPedido;

    private Timestamp fecha;

    private String observacion;

    private String sucursal;

    private ProveedorDTO proveedor;

    private String tipo;

    private String visita;

    private String cuotas;

    private String frecuencia;

    private String oc;

    private List<RecepcionDTO> recepcion;

    private String moneda;

    private String cotizacion;

    private Long total;

    private Long saldo;

    public PedidoCabDTO() {
    }

    public Long getIdPedidoCab() {
        return idPedidoCab;
    }

    public void setIdPedidoCab(Long idPedidoCab) {
        this.idPedidoCab = idPedidoCab;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPlazo() {
        return plazo;
    }

    public String getOc() {
        return oc;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getVisita() {
        return visita;
    }

    public void setVisita(String visita) {
        this.visita = visita;
    }

    public String getCuotas() {
        return cuotas;
    }

    public void setCuotas(String cuotas) {
        this.cuotas = cuotas;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public String getNroPedido() {
        return nroPedido;
    }

    public void setNroPedido(String nroPedido) {
        this.nroPedido = nroPedido;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public ProveedorDTO getProveedor() {
        return proveedor;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

    public List<RecepcionDTO> getRecepcion() {
        if (this.recepcion == null) {
            this.recepcion = new ArrayList<>();
        }
        return recepcion;
    }

    public void setRecepcion(List<RecepcionDTO> recepcion) {
        this.recepcion = recepcion;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(String cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
