package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the tarjeta database table.
 *
 */
public class TarjetaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTarjeta;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Boolean habilitado;
    private Integer codtar;
    private String usuAlta;
    private String usuMod;
    private FamiliaTarjDTO familiaTarj;
    private TipoTarjetaDTO tipoTarjeta;
    private List<FacturaClienteCabTarjetaDTO> facturaClienteCabTarjetaDTO;
    private List<DescuentoTarjetaCabDTO> descuentoTarjetaCab;

    public TarjetaDTO() {
    }

    public Long getIdTarjeta() {
        return this.idTarjeta;
    }

    public void setIdTarjeta(Long idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Integer getCodtar() {
        return codtar;
    }

    public void setCodtar(Integer codtar) {
        this.codtar = codtar;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public List<DescuentoTarjetaCabDTO> getDescuentoTarjetaCab() {
        if (this.descuentoTarjetaCab == null) {
            this.descuentoTarjetaCab = new ArrayList<DescuentoTarjetaCabDTO>();
        }
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(
            List<DescuentoTarjetaCabDTO> descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public FamiliaTarjDTO getFamiliaTarj() {
        return this.familiaTarj;
    }

    public void setFamiliaTarj(FamiliaTarjDTO familiaTarj) {
        this.familiaTarj = familiaTarj;
    }

    public TipoTarjetaDTO getTipoTarjeta() {
        return this.tipoTarjeta;
    }

    public void setTipoTarjeta(TipoTarjetaDTO tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public List<FacturaClienteCabTarjetaDTO> getFacturaClienteCabTarjetaDTO() {
        if (this.facturaClienteCabTarjetaDTO == null) {
            this.facturaClienteCabTarjetaDTO = new ArrayList<FacturaClienteCabTarjetaDTO>();
        }
        return facturaClienteCabTarjetaDTO;
    }

    public void setFacturaClienteCabTarjetaDTO(
            List<FacturaClienteCabTarjetaDTO> facturaClienteCabTarjetaDTO) {
        this.facturaClienteCabTarjetaDTO = facturaClienteCabTarjetaDTO;
    }

}
