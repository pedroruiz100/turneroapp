package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf7Secnom7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf7Secnom7;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf6Secnom6DTO nf6Secnom6;
    private List<ArticuloNf7Secnom7DTO> articuloNf7Secnom7s;
    private List<FuncionarioNf7DTO> funcionarioNf7s;
    private List<DescuentoFielNf7DTO> descuentoFielNf7s;
    private List<PromoTemporadaNf7DTO> promoTemporadaNf7s;

    public Nf7Secnom7DTO() {
    }

    public Long getIdNf7Secnom7() {
        return this.idNf7Secnom7;
    }

    public void setIdNf7Secnom7(Long idNf7Secnom7) {
        this.idNf7Secnom7 = idNf7Secnom7;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return this.nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    public List<ArticuloNf7Secnom7DTO> getArticuloNf7Secnom7s() {
        return this.articuloNf7Secnom7s;
    }

    public void setArticuloNf7Secnom7s(List<ArticuloNf7Secnom7DTO> articuloNf7Secnom7s) {
        this.articuloNf7Secnom7s = articuloNf7Secnom7s;
    }

    public ArticuloNf7Secnom7DTO addArticuloNf7Secnom7(ArticuloNf7Secnom7DTO articuloNf7Secnom7) {
        getArticuloNf7Secnom7s().add(articuloNf7Secnom7);
        articuloNf7Secnom7.setNf7Secnom7(this);

        return articuloNf7Secnom7;
    }

    public ArticuloNf7Secnom7DTO removeArticuloNf7Secnom7(ArticuloNf7Secnom7DTO articuloNf7Secnom7) {
        getArticuloNf7Secnom7s().remove(articuloNf7Secnom7);
        articuloNf7Secnom7.setNf7Secnom7(null);

        return articuloNf7Secnom7;
    }

    public List<FuncionarioNf7DTO> getFuncionarioNf7s() {
        return this.funcionarioNf7s;
    }

    public void setFuncionarioNf7s(List<FuncionarioNf7DTO> funcionarioNf7s) {
        this.funcionarioNf7s = funcionarioNf7s;
    }

    public FuncionarioNf7DTO addFuncionarioNf7(FuncionarioNf7DTO funcionarioNf7) {
        getFuncionarioNf7s().add(funcionarioNf7);
        funcionarioNf7.setNf7Secnom7(this);

        return funcionarioNf7;
    }

    public FuncionarioNf7DTO removeFuncionarioNf7(FuncionarioNf7DTO funcionarioNf7) {
        getFuncionarioNf7s().remove(funcionarioNf7);
        funcionarioNf7.setNf7Secnom7(null);

        return funcionarioNf7;
    }

    public List<DescuentoFielNf7DTO> getDescuentoFielNf7s() {
        return this.descuentoFielNf7s;
    }

    public void setDescuentoFielNf7s(List<DescuentoFielNf7DTO> descuentoFielNf7s) {
        this.descuentoFielNf7s = descuentoFielNf7s;
    }

    public DescuentoFielNf7DTO addDescuentoFielNf7(DescuentoFielNf7DTO descuentoFielNf7) {
        getDescuentoFielNf7s().add(descuentoFielNf7);
        descuentoFielNf7.setNf7Secnom7(this);

        return descuentoFielNf7;
    }

    public DescuentoFielNf7DTO removeDescuentoFielNf7(DescuentoFielNf7DTO descuentoFielNf7) {
        getDescuentoFielNf7s().remove(descuentoFielNf7);
        descuentoFielNf7.setNf7Secnom7(null);

        return descuentoFielNf7;
    }

    public List<PromoTemporadaNf7DTO> getPromoTemporadaNf7s() {
        return this.promoTemporadaNf7s;
    }

    public void setPromoTemporadaNf7s(List<PromoTemporadaNf7DTO> promoTemporadaNf7s) {
        this.promoTemporadaNf7s = promoTemporadaNf7s;
    }

    public PromoTemporadaNf7DTO addPromoTemporadaNf7(PromoTemporadaNf7DTO promoTemporadaNf7) {
        getPromoTemporadaNf7s().add(promoTemporadaNf7);
        promoTemporadaNf7.setNf7Secnom7(this);

        return promoTemporadaNf7;
    }

    public PromoTemporadaNf7DTO removePromoTemporadaNf7(PromoTemporadaNf7DTO promoTemporadaNf7) {
        getPromoTemporadaNf7s().remove(promoTemporadaNf7);
        promoTemporadaNf7.setNf7Secnom7(null);

        return promoTemporadaNf7;
    }

}
