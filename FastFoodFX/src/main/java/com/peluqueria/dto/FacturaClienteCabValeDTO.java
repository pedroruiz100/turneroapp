package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_cab_Vale database table.
 *
 */
public class FacturaClienteCabValeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabVale;
    private FacturaClienteCabDTO facturaClienteCab;
    private ValeDTO Vale;
    private Integer monto;
    private String nroVale;
    private String ci;

    public FacturaClienteCabValeDTO() {
    }

    public Long getIdFacturaClienteCabVale() {
        return idFacturaClienteCabVale;
    }

    public void setIdFacturaClienteCabVale(Long idFacturaClienteCabVale) {
        this.idFacturaClienteCabVale = idFacturaClienteCabVale;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public ValeDTO getVale() {
        return Vale;
    }

    public void setVale(ValeDTO vale) {
        Vale = vale;
    }

    public String getNroVale() {
        return nroVale;
    }

    public void setNroVale(String nroVale) {
        this.nroVale = nroVale;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

}
