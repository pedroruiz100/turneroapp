package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the promo_temporada database table.
 *
 */
public class PromoTemporadaArtDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTemporadaArt;
    private String descripcionTemporadaArt;
    private Boolean estadoPromo;
    private Timestamp fechaFin;
    private Timestamp fechaInicio;
    private String usuAlta;
    private Timestamp fechaAlta;
    private String usuMod;
    private Timestamp fechaMod;
    private List<ArtPromoTemporadaDTO> artPromoTemporadaDTO;
    private List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTempArt;
    private List<ArtPromoTemporadaObsequioDTO> artPromoTemporadaObsequio;

    public PromoTemporadaArtDTO() {
    }

    public Long getIdTemporadaArt() {
        return idTemporadaArt;
    }

    public void setIdTemporadaArt(Long idTemporadaArt) {
        this.idTemporadaArt = idTemporadaArt;
    }

    public String getDescripcionTemporadaArt() {
        return descripcionTemporadaArt;
    }

    public List<FacturaClienteCabPromoTempArtDTO> getFacturaClienteCabPromoTempArt() {
        if (this.facturaClienteCabPromoTempArt == null) {
            this.facturaClienteCabPromoTempArt = new ArrayList<>();
        }
        return facturaClienteCabPromoTempArt;
    }

    public void setFacturaClienteCabPromoTempArt(
            List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTempArtArt) {
        this.facturaClienteCabPromoTempArt = facturaClienteCabPromoTempArt;
    }

    public List<ArtPromoTemporadaObsequioDTO> getArtPromoTemporadaObsequio() {
        if (this.artPromoTemporadaObsequio == null) {
            this.artPromoTemporadaObsequio = new ArrayList<>();
        }
        return artPromoTemporadaObsequio;
    }

    public void setArtPromoTemporadaObsequio(List<ArtPromoTemporadaObsequioDTO> artPromoTemporadaObsequio) {
        this.artPromoTemporadaObsequio = artPromoTemporadaObsequio;
    }

    public void setDescripcionTemporadaArt(String descripcionTemporadaArt) {
        this.descripcionTemporadaArt = descripcionTemporadaArt;
    }

    public Boolean getEstadoPromo() {
        return estadoPromo;
    }

    public void setEstadoPromo(Boolean estadoPromo) {
        this.estadoPromo = estadoPromo;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<ArtPromoTemporadaDTO> getArtPromoTemporadaDTO() {
        if (this.artPromoTemporadaDTO == null) {
            this.artPromoTemporadaDTO = new ArrayList<>();
        }
        return artPromoTemporadaDTO;
    }

    public void setArtPromoTemporadaDTO(
            List<ArtPromoTemporadaDTO> artPromoTemporadaDTO) {
        this.artPromoTemporadaDTO = artPromoTemporadaDTO;
    }

}
