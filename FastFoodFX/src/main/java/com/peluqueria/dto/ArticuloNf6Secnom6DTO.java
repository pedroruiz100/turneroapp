package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf6Secnom6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf6Secnom6Articulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf6Secnom6DTO nf6Secnom6;

    public ArticuloNf6Secnom6DTO() {
    }

    public Long getIdNf6Secnom6Articulo() {
        return this.idNf6Secnom6Articulo;
    }

    public void setIdNf6Secnom6Articulo(Long idNf6Secnom6Articulo) {
        this.idNf6Secnom6Articulo = idNf6Secnom6Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return this.nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

}
