package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf7;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf7Secnom7DTO nf7Secnom7;
    private String descriSeccion;

    public DescuentoTarjetaCabNf7DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf7() {
        return this.idDescuentoTarjetaCabNf7;
    }

    public void setIdDescuentoTarjetaCabNf7(Long idDescuentoTarjetaCabNf7) {
        this.idDescuentoTarjetaCabNf7 = idDescuentoTarjetaCabNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf7Secnom7DTO getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

}
