package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the estado_factura database table.
 *
 */
public class EstadoFacturaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEstadoFactura;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<FacturaClienteCabDTO> facturaClienteCabs;

    public EstadoFacturaDTO() {
    }

    public Long getIdEstadoFactura() {
        return this.idEstadoFactura;
    }

    public void setIdEstadoFactura(Long idEstadoFactura) {
        this.idEstadoFactura = idEstadoFactura;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCabDTO> getFacturaClienteCabs() {
        if (this.facturaClienteCabs == null) {
            this.facturaClienteCabs = new ArrayList<FacturaClienteCabDTO>();
        }
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(
            List<FacturaClienteCabDTO> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

}
