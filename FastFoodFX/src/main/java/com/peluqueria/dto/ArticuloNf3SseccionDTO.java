package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf3SseccionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf3SseccionArticulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf3SseccionDTO nf3Sseccion;

    public ArticuloNf3SseccionDTO() {
    }

    public Long getIdNf3SseccionArticulo() {
        return this.idNf3SseccionArticulo;
    }

    public void setIdNf3SseccionArticulo(Long idNf3SseccionArticulo) {
        this.idNf3SseccionArticulo = idNf3SseccionArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf3SseccionDTO getNf3Sseccion() {
        return this.nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

}
