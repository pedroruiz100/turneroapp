package com.peluqueria.dto;

import java.io.Serializable;

public class IpCajaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idIpCaja;
    private String ipCaja;

    public IpCajaDTO() {
    }

    public Long getIdIpCaja() {
        return this.idIpCaja;
    }

    public void setIdIpCaja(Long idIpCaja) {
        this.idIpCaja = idIpCaja;
    }

    public String getIpCaja() {
        return this.ipCaja;
    }

    public void setIpCaja(String ipCaja) {
        this.ipCaja = ipCaja;
    }

}
