package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloSrvComisionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloSrvComision;

    private ArticuloDTO articulo;

    private Integer porc;

    private Integer monto;

    private Boolean comisionPorc;

    private Timestamp fechaAlta;

    private Timestamp fechaMod;

    private String usuAlta;

    private String usuMod;

    public ArticuloSrvComisionDTO() {
        super();
    }

    public Long getIdArticuloSrvComision() {
        return idArticuloSrvComision;
    }

    public void setIdArticuloSrvComision(Long idArticuloSrvComision) {
        this.idArticuloSrvComision = idArticuloSrvComision;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public Integer getPorc() {
        return porc;
    }

    public void setPorc(Integer porc) {
        this.porc = porc;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Boolean getComisionPorc() {
        return comisionPorc;
    }

    public void setComisionPorc(Boolean comisionPorc) {
        this.comisionPorc = comisionPorc;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

}
