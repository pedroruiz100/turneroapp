package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CajaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCaja;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String nroSerie;
    private String usuAlta;
    private String usuMod;
    private TipoCajaDTO tipoCaja;
    private SucursalDTO sucursal;
    private List<CierreCajaDTO> cierreCajasDTO;
    private List<RetiroDineroDTO> retiroDinerosDTO;
    private List<FacturaClienteCabDTO> facturaClienteCabDTO;
    private List<AperturaCajaDTO> aperturaCajasDTO;
    private List<ArqueoCajaDTO> arqueoCaja;
    private Integer nroCaja;
    private String claveCaja;
    private IpBocaDTO ipBoca;

    public CajaDTO() {
    }

    public Integer getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(Integer nroCaja) {
        this.nroCaja = nroCaja;
    }

    public String getClaveCaja() {
        return claveCaja;
    }

    public void setClaveCaja(String claveCaja) {
        this.claveCaja = claveCaja;
    }

    public List<FacturaClienteCabDTO> getFacturaClienteCabDTO() {
        if (this.facturaClienteCabDTO == null) {
            this.facturaClienteCabDTO = new ArrayList<FacturaClienteCabDTO>();
        }
        return facturaClienteCabDTO;
    }

    public void setFacturaClienteCabDTO(
            List<FacturaClienteCabDTO> facturaClienteCabDTO) {
        this.facturaClienteCabDTO = facturaClienteCabDTO;
    }

    public Long getIdCaja() {
        return this.idCaja;
    }

    public void setIdCaja(Long idCaja) {
        this.idCaja = idCaja;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursalDTO) {
        this.sucursal = sucursalDTO;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getNroSerie() {
        return this.nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<AperturaCajaDTO> getAperturaCajasDTO() {
        if (this.aperturaCajasDTO == null) {
            this.aperturaCajasDTO = new ArrayList<AperturaCajaDTO>();
        }
        return this.aperturaCajasDTO;
    }

    public void setAperturaCajasDTO(List<AperturaCajaDTO> aperturaCajasDTO) {
        this.aperturaCajasDTO = aperturaCajasDTO;
    }

    public TipoCajaDTO getTipoCaja() {
        return tipoCaja;
    }

    public void setTipoCaja(TipoCajaDTO tipoCajaDTO) {
        this.tipoCaja = tipoCajaDTO;
    }

    public List<CierreCajaDTO> getCierreCajasDTO() {
        if (this.cierreCajasDTO == null) {
            this.cierreCajasDTO = new ArrayList<CierreCajaDTO>();
        }
        return this.cierreCajasDTO;
    }

    public void setCierreCajasDTO(List<CierreCajaDTO> cierreCajas) {
        this.cierreCajasDTO = cierreCajas;
    }

    public List<RetiroDineroDTO> getRetiroDinerosDTO() {
        if (this.retiroDinerosDTO == null) {
            this.retiroDinerosDTO = new ArrayList<RetiroDineroDTO>();
        }
        return this.retiroDinerosDTO;
    }

    public void setRetiroDinerosDTO(List<RetiroDineroDTO> retiroDineros) {
        this.retiroDinerosDTO = retiroDineros;
    }

    public IpBocaDTO getIpBoca() {
        return ipBoca;
    }

    public void setIpBoca(IpBocaDTO ipBoca) {
        this.ipBoca = ipBoca;
    }

    public List<ArqueoCajaDTO> getArqueoCaja() {
        if (this.arqueoCaja == null) {
            this.arqueoCaja = new ArrayList<ArqueoCajaDTO>();
        }
        return arqueoCaja;
    }

    public void setArqueoCaja(List<ArqueoCajaDTO> arqueoCaja) {
        this.arqueoCaja = arqueoCaja;
    }

}
