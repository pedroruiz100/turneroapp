package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the nota_credito_det database table.
 *
 */
public class NotaCreditoDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaCreditoDet;
    private Integer cantidad;
    private BigDecimal contenido;
    private Integer costoUnitario;
    private String descripcionArticulo;
    private ArticuloDTO articulo;
    private Integer idUnidad;
    private Integer porcDesc;
    private Integer porcIva;
    private Integer precioUnitario;
    private Integer totalNota;
    private NotaCreditoCabDTO notaCreditoCab;

    public NotaCreditoDetDTO() {
    }

    public Long getIdNotaCreditoDet() {
        return this.idNotaCreditoDet;
    }

    public void setIdNotaCreditoDet(Long idNotaCreditoDet) {
        this.idNotaCreditoDet = idNotaCreditoDet;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getContenido() {
        return this.contenido;
    }

    public void setContenido(BigDecimal contenido) {
        this.contenido = contenido;
    }

    public Integer getCostoUnitario() {
        return this.costoUnitario;
    }

    public void setCostoUnitario(Integer costoUnitario) {
        this.costoUnitario = costoUnitario;
    }

    public String getDescripcionArticulo() {
        return this.descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public Integer getIdUnidad() {
        return this.idUnidad;
    }

    public void setIdUnidad(Integer idUnidad) {
        this.idUnidad = idUnidad;
    }

    public Integer getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(Integer porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Integer getPorcIva() {
        return this.porcIva;
    }

    public void setPorcIva(Integer porcIva) {
        this.porcIva = porcIva;
    }

    public Integer getPrecioUnitario() {
        return this.precioUnitario;
    }

    public void setPrecioUnitario(Integer precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getTotalNota() {
        return this.totalNota;
    }

    public void setTotalNota(Integer totalNota) {
        this.totalNota = totalNota;
    }

    public NotaCreditoCabDTO getNotaCreditoCab() {
        return this.notaCreditoCab;
    }

    public void setNotaCreditoCab(NotaCreditoCabDTO notaCreditoCab) {
        this.notaCreditoCab = notaCreditoCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

}
