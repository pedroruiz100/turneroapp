package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class CancelacionFacturaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCancelacionFactura;
    private Timestamp fechaCancelacion;
    private FacturaClienteCabDTO facturaClienteCab;
    private UsuarioDTO usuarioCajero;
    private UsuarioDTO usuarioSupervisor;
    private MotivoCancelacionFacturaDTO motivoCancelacionFactura;

    public CancelacionFacturaDTO() {
    }

    public Long getIdCancelacionFactura() {
        return this.idCancelacionFactura;
    }

    public void setIdCancelacionFactura(Long idCancelacionFactura) {
        this.idCancelacionFactura = idCancelacionFactura;
    }

    public Timestamp getFechaCancelacion() {
        return this.fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public MotivoCancelacionFacturaDTO getMotivoCancelacionFactura() {
        return this.motivoCancelacionFactura;
    }

    public void setMotivoCancelacionFactura(MotivoCancelacionFacturaDTO motivoCancelacionFactura) {
        this.motivoCancelacionFactura = motivoCancelacionFactura;
    }

    public UsuarioDTO getUsuarioCajero() {
        return this.usuarioCajero;
    }

    public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public UsuarioDTO getUsuarioSupervisor() {
        return this.usuarioSupervisor;
    }

    public void setUsuarioSupervisor(UsuarioDTO usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

}
