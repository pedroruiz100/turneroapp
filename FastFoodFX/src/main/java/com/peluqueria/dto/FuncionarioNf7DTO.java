package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class FuncionarioNf7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncionarioNf7;
    private String descriSeccion;
    private Boolean estadoDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Nf7Secnom7DTO nf7Secnom7;
    private BigDecimal porcDesc;
    private String usuAlta;
    private String usuMod;

    public FuncionarioNf7DTO() {
    }

    public Long getIdFuncionarioNf7() {
        return this.idFuncionarioNf7;
    }

    public void setIdFuncionarioNf7(Long idFuncionarioNf7) {
        this.idFuncionarioNf7 = idFuncionarioNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf7Secnom7DTO getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

}
