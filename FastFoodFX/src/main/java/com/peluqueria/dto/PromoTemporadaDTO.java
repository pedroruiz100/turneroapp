package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the promo_temporada database table.
 *
 */
public class PromoTemporadaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTemporada;
    private String descripcionTemporada;
    private Boolean estadoPromo;
    private Timestamp fechaFin;
    private Timestamp fechaInicio;
    private String usuAlta;
    private Timestamp fechaAlta;
    private String usuMod;
    private Timestamp fechaMod;
    private List<SeccionPromoTemporadaDTO> seccionPromoTemporadaDTO;
    private List<FacturaClienteCabPromoTempDTO> facturaClienteCabPromoTemp;
    private List<PromoTemporadaNf1DTO> promoTemporadaNf1s;
    private List<PromoTemporadaNf2DTO> promoTemporadaNf2s;
    private List<PromoTemporadaNf3DTO> promoTemporadaNf3s;
    private List<PromoTemporadaNf4DTO> promoTemporadaNf4s;
    private List<PromoTemporadaNf5DTO> promoTemporadaNf5s;
    private List<PromoTemporadaNf6DTO> promoTemporadaNf6s;
    private List<PromoTemporadaNf7DTO> promoTemporadaNf7s;

    public PromoTemporadaDTO() {
    }

    public Long getIdTemporada() {
        return idTemporada;
    }

    public void setIdTemporada(Long idTemporada) {
        this.idTemporada = idTemporada;
    }

    public String getDescripcionTemporada() {
        return descripcionTemporada;
    }

    public List<FacturaClienteCabPromoTempDTO> getFacturaClienteCabPromoTemp() {
        if (this.facturaClienteCabPromoTemp == null) {
            this.facturaClienteCabPromoTemp = new ArrayList<FacturaClienteCabPromoTempDTO>();
        }
        return facturaClienteCabPromoTemp;
    }

    public void setFacturaClienteCabPromoTemp(
            List<FacturaClienteCabPromoTempDTO> facturaClienteCabPromoTemp) {
        this.facturaClienteCabPromoTemp = facturaClienteCabPromoTemp;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Boolean getEstadoPromo() {
        return estadoPromo;
    }

    public void setEstadoPromo(Boolean estadoPromo) {
        this.estadoPromo = estadoPromo;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<SeccionPromoTemporadaDTO> getSeccionPromoTemporadaDTO() {
        if (this.seccionPromoTemporadaDTO == null) {
            this.seccionPromoTemporadaDTO = new ArrayList<SeccionPromoTemporadaDTO>();
        }
        return seccionPromoTemporadaDTO;
    }

    public void setSeccionPromoTemporadaDTO(
            List<SeccionPromoTemporadaDTO> seccionPromoTemporadaDTO) {
        this.seccionPromoTemporadaDTO = seccionPromoTemporadaDTO;
    }

    public List<PromoTemporadaNf1DTO> getPromoTemporadaNf1s() {
        if (this.promoTemporadaNf1s == null) {
            return promoTemporadaNf1s;
        }
        return promoTemporadaNf1s;
    }

    public void setPromoTemporadaNf1s(List<PromoTemporadaNf1DTO> promoTemporadaNf1s) {
        this.promoTemporadaNf1s = promoTemporadaNf1s;
    }

    public List<PromoTemporadaNf2DTO> getPromoTemporadaNf2s() {
        if (this.promoTemporadaNf2s == null) {
            return promoTemporadaNf2s;
        }
        return promoTemporadaNf2s;
    }

    public void setPromoTemporadaNf2s(List<PromoTemporadaNf2DTO> promoTemporadaNf2s) {
        this.promoTemporadaNf2s = promoTemporadaNf2s;
    }

    public List<PromoTemporadaNf3DTO> getPromoTemporadaNf3s() {
        if (this.promoTemporadaNf3s == null) {
            return promoTemporadaNf3s;
        }
        return promoTemporadaNf3s;
    }

    public void setPromoTemporadaNf3s(List<PromoTemporadaNf3DTO> promoTemporadaNf3s) {
        this.promoTemporadaNf3s = promoTemporadaNf3s;
    }

    public List<PromoTemporadaNf4DTO> getPromoTemporadaNf4s() {
        if (this.promoTemporadaNf4s == null) {
            return promoTemporadaNf4s;
        }
        return promoTemporadaNf4s;
    }

    public void setPromoTemporadaNf4s(List<PromoTemporadaNf4DTO> promoTemporadaNf4s) {
        this.promoTemporadaNf4s = promoTemporadaNf4s;
    }

    public List<PromoTemporadaNf5DTO> getPromoTemporadaNf5s() {
        if (this.promoTemporadaNf5s == null) {
            return promoTemporadaNf5s;
        }
        return promoTemporadaNf5s;
    }

    public void setPromoTemporadaNf5s(List<PromoTemporadaNf5DTO> promoTemporadaNf5s) {
        this.promoTemporadaNf5s = promoTemporadaNf5s;
    }

    public List<PromoTemporadaNf6DTO> getPromoTemporadaNf6s() {
        if (this.promoTemporadaNf6s == null) {
            return promoTemporadaNf6s;
        }
        return promoTemporadaNf6s;
    }

    public void setPromoTemporadaNf6s(List<PromoTemporadaNf6DTO> promoTemporadaNf6s) {
        this.promoTemporadaNf6s = promoTemporadaNf6s;
    }

    public List<PromoTemporadaNf7DTO> getPromoTemporadaNf7s() {
        if (this.promoTemporadaNf7s == null) {
            return promoTemporadaNf7s;
        }
        return promoTemporadaNf7s;
    }

    public void setPromoTemporadaNf7s(List<PromoTemporadaNf7DTO> promoTemporadaNf7s) {
        this.promoTemporadaNf7s = promoTemporadaNf7s;
    }

}
