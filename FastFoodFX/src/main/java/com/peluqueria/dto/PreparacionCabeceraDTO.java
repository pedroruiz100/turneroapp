package com.peluqueria.dto;

import java.io.Serializable;
import java.util.Date;

public class PreparacionCabeceraDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPreparacionCab;
    private ArticuloDTO articulo;
    private String total;
    private Date fecha;
    private String descripcion;
    private String cantidad;

    public PreparacionCabeceraDTO() {
    }

    public Long getIdPreparacionCab() {
        return idPreparacionCab;
    }

    public void setIdPreparacionCab(Long idPreparacionCab) {
        this.idPreparacionCab = idPreparacionCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
