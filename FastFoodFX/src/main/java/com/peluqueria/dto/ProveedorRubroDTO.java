package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the proveedor_rubro database table.
 *
 */
public class ProveedorRubroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idProveedorRubro;
    private ProveedorDTO proveedor;
    private RubroDTO rubro;

    public ProveedorRubroDTO() {
    }

    public Long getIdProveedorRubro() {
        return this.idProveedorRubro;
    }

    public void setIdProveedorRubro(Long idProveedorRubro) {
        this.idProveedorRubro = idProveedorRubro;
    }

    public ProveedorDTO getProveedor() {
        return this.proveedor;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

    public RubroDTO getRubro() {
        return this.rubro;
    }

    public void setRubro(RubroDTO rubro) {
        this.rubro = rubro;
    }

}
