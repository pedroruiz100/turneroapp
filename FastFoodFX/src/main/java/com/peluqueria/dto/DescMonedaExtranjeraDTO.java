package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DescMonedaExtranjeraDTO implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private Long idDescMonedaExtranjera;

    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabMonedaExtranjeraDTO facturaClienteCabMonedaExtranjera;

    public DescMonedaExtranjeraDTO() {
        super();
    }

    public Long getIdDescMonedaExtranjera() {
        return idDescMonedaExtranjera;
    }

    public void setIdDescMonedaExtranjera(Long idDescMonedaExtranjera) {
        this.idDescMonedaExtranjera = idDescMonedaExtranjera;
    }

    public Integer getMontoDesc() {
        return montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabMonedaExtranjeraDTO getFacturaClienteCabMonedaExtranjera() {
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            FacturaClienteCabMonedaExtranjeraDTO facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

}
