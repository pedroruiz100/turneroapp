package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf3DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf3;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf3SseccionDTO nf3Sseccion;

    public DescuentoFielNf3DTO() {
    }

    public Long getIdDescuentoFielNf3() {
        return this.idDescuentoFielNf3;
    }

    public void setIdDescuentoFielNf3(Long idDescuentoFielNf3) {
        this.idDescuentoFielNf3 = idDescuentoFielNf3;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf3SseccionDTO getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

}
