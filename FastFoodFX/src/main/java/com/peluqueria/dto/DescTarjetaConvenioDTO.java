package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the desc_tarjeta_convenio database table.
 *
 */
public class DescTarjetaConvenioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescTarjetaConvenio;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabTarjConvenioDTO facturaClienteCabTarjConvenio;

    public DescTarjetaConvenioDTO() {
    }

    public Long getIdDescTarjetaConvenio() {
        return this.idDescTarjetaConvenio;
    }

    public void setIdDescTarjetaConvenio(Long idDescTarjetaConvenio) {
        this.idDescTarjetaConvenio = idDescTarjetaConvenio;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjConvenioDTO getFacturaClienteCabTarjConvenio() {
        return this.facturaClienteCabTarjConvenio;
    }

    public void setFacturaClienteCabTarjConvenio(
            FacturaClienteCabTarjConvenioDTO facturaClienteCabTarjConvenio) {
        this.facturaClienteCabTarjConvenio = facturaClienteCabTarjConvenio;
    }

}
