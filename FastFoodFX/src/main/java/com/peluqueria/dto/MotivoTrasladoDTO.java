package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the motivo_traslado database table.
 *
 */
public class MotivoTrasladoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idMotivoTraslado;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<NotaRemisionCabDTO> notaRemisionCabs;

    public MotivoTrasladoDTO() {
    }

    public Long getIdMotivoTraslado() {
        return this.idMotivoTraslado;
    }

    public void setIdMotivoTraslado(Long idMotivoTraslado) {
        this.idMotivoTraslado = idMotivoTraslado;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<NotaRemisionCabDTO> getNotaRemisionCabs() {
        return this.notaRemisionCabs;
    }

    public void setNotaRemisionCabs(List<NotaRemisionCabDTO> notaRemisionCabs) {
        this.notaRemisionCabs = notaRemisionCabs;
    }

    public NotaRemisionCabDTO addNotaRemisionCab(NotaRemisionCabDTO notaRemisionCab) {
        getNotaRemisionCabs().add(notaRemisionCab);
        notaRemisionCab.setMotivoTraslado(this);

        return notaRemisionCab;
    }

    public NotaRemisionCabDTO removeNotaRemisionCab(NotaRemisionCabDTO notaRemisionCab) {
        getNotaRemisionCabs().remove(notaRemisionCab);
        notaRemisionCab.setMotivoTraslado(null);

        return notaRemisionCab;
    }

}
