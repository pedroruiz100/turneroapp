package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the combo_cab database table.
 *
 */
public class ComboCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idComboCab;
    private Integer cantidad;
    private Integer codCombo;
    private String descripcionCombo;
    private String descripcionProveedor;
    private String descripcionSucursal;
    private Timestamp fechaAlta;
    private Timestamp fechaInicio;
    private Timestamp fechaMod;
    private ProveedorDTO proveedor;
    private SucursalDTO sucursal;
    private Integer montoProveedor;
    private String motivoCombo;
    private Integer precioComboSinDesc;
    private String usuAlta;
    private String usuMod;
    private IvaDTO iva;
    private TipoComboDTO tipoCombo;
    private List<ComboDetDTO> comboDets;

    public ComboCabDTO() {
    }

    public Long getIdComboCab() {
        return this.idComboCab;
    }

    public void setIdComboCab(Long idComboCab) {
        this.idComboCab = idComboCab;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCodCombo() {
        return this.codCombo;
    }

    public void setCodCombo(Integer codCombo) {
        this.codCombo = codCombo;
    }

    public String getDescripcionCombo() {
        return this.descripcionCombo;
    }

    public void setDescripcionCombo(String descripcionCombo) {
        this.descripcionCombo = descripcionCombo;
    }

    public String getDescripcionProveedor() {
        return this.descripcionProveedor;
    }

    public void setDescripcionProveedor(String descripcionProveedor) {
        this.descripcionProveedor = descripcionProveedor;
    }

    public String getDescripcionSucursal() {
        return this.descripcionSucursal;
    }

    public void setDescripcionSucursal(String descripcionSucursal) {
        this.descripcionSucursal = descripcionSucursal;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getMontoProveedor() {
        return this.montoProveedor;
    }

    public void setMontoProveedor(Integer montoProveedor) {
        this.montoProveedor = montoProveedor;
    }

    public String getMotivoCombo() {
        return this.motivoCombo;
    }

    public void setMotivoCombo(String motivoCombo) {
        this.motivoCombo = motivoCombo;
    }

    public Integer getPrecioComboSinDesc() {
        return this.precioComboSinDesc;
    }

    public void setPrecioComboSinDesc(Integer precioComboSinDesc) {
        this.precioComboSinDesc = precioComboSinDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public IvaDTO getIva() {
        return this.iva;
    }

    public void setIva(IvaDTO iva) {
        this.iva = iva;
    }

    public TipoComboDTO getTipoCombo() {
        return this.tipoCombo;
    }

    public void setTipoCombo(TipoComboDTO tipoCombo) {
        this.tipoCombo = tipoCombo;
    }

    public List<ComboDetDTO> getComboDets() {
        return this.comboDets;
    }

    public void setComboDets(List<ComboDetDTO> comboDets) {
        this.comboDets = comboDets;
    }

    public ComboDetDTO addComboDet(ComboDetDTO comboDet) {
        getComboDets().add(comboDet);
        comboDet.setComboCab(this);

        return comboDet;
    }

    public ComboDetDTO removeComboDet(ComboDetDTO comboDet) {
        getComboDets().remove(comboDet);
        comboDet.setComboCab(null);

        return comboDet;
    }

    public ProveedorDTO getProveedor() {
        return proveedor;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

}
