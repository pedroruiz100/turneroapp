package com.peluqueria.dto;

import java.io.Serializable;

public class TalonariosSucursaleDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTalonariosSucursales;
    private Long nroActual;
    private Long nroFinal;
    private Long nroInicial;
    private IpBocaDTO ipBoca;
    private SucursalDTO sucursal;
    private TimbradoDTO timbrado;
    private String primero;
    private String segundo;

    public TalonariosSucursaleDTO() {
    }

    public Long getIdTalonariosSucursales() {
        return this.idTalonariosSucursales;
    }

    public void setIdTalonariosSucursales(Long idTalonariosSucursales) {
        this.idTalonariosSucursales = idTalonariosSucursales;
    }

    public Long getNroActual() {
        return this.nroActual;
    }

    public void setNroActual(Long nroActual) {
        this.nroActual = nroActual;
    }

    public Long getNroFinal() {
        return this.nroFinal;
    }

    public void setNroFinal(Long nroFinal) {
        this.nroFinal = nroFinal;
    }

    public Long getNroInicial() {
        return this.nroInicial;
    }

    public String getPrimero() {
        return primero;
    }

    public void setPrimero(String primero) {
        this.primero = primero;
    }

    public String getSegundo() {
        return segundo;
    }

    public void setSegundo(String segundo) {
        this.segundo = segundo;
    }

    public void setNroInicial(Long nroInicial) {
        this.nroInicial = nroInicial;
    }

    public IpBocaDTO getIpBoca() {
        return ipBoca;
    }

    public void setIpBoca(IpBocaDTO ipBoca) {
        this.ipBoca = ipBoca;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

    public TimbradoDTO getTimbrado() {
        return timbrado;
    }

    public void setTimbrado(TimbradoDTO timbrado) {
        this.timbrado = timbrado;
    }

}
