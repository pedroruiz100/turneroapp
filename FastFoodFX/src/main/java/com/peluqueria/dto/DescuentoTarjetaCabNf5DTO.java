package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf5DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf5;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf5Seccion2DTO nf5Seccion2;
    private String descriSeccion;

    public DescuentoTarjetaCabNf5DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf5() {
        return this.idDescuentoTarjetaCabNf5;
    }

    public void setIdDescuentoTarjetaCabNf5(Long idDescuentoTarjetaCabNf5) {
        this.idDescuentoTarjetaCabNf5 = idDescuentoTarjetaCabNf5;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf5Seccion2DTO getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

}
