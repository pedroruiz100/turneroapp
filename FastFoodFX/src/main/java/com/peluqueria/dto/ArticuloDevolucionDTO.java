package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class ArticuloDevolucionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloDevolucion;

    private FacturaCompraCabDTO facturaCompraCab;

    private ArticuloDTO articulo;

    private String cantidad;

    private String descripcion;
    private String nroDevolucion;

    private Date fecha;

    private Long precio;

    private Long total;

    public ArticuloDevolucionDTO() {
    }

    public Long getIdArticuloDevolucion() {
        return idArticuloDevolucion;
    }

    public void setIdArticuloDevolucion(Long idArticuloDevolucion) {
        this.idArticuloDevolucion = idArticuloDevolucion;
    }

    public FacturaCompraCabDTO getFacturaCompraCab() {
        return facturaCompraCab;
    }

    public void setFacturaCompraCab(FacturaCompraCabDTO facturaCompraCab) {
        this.facturaCompraCab = facturaCompraCab;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNroDevolucion() {
        return nroDevolucion;
    }

    public void setNroDevolucion(String nroDevolucion) {
        this.nroDevolucion = nroDevolucion;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

}
