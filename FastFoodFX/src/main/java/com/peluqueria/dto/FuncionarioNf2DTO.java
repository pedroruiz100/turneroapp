package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;

public class FuncionarioNf2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncionarioNf2;
    private String descriSeccion;
    private Boolean estadoDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Nf2SfamiliaDTO nf2Sfamilia;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    public FuncionarioNf2DTO() {
    }

    public Long getIdFuncionarioNf2() {
        return this.idFuncionarioNf2;
    }

    public void setIdFuncionarioNf2(Long idFuncionarioNf2) {
        this.idFuncionarioNf2 = idFuncionarioNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

}
