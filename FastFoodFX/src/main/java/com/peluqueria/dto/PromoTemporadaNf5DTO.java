package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf5DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf5;
    private PromoTemporadaDTO promoTemporada;
    private Nf5Seccion2DTO nf5Seccion2;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf5DTO() {
    }

    public Long getIdPromoTemporadaNf5() {
        return this.idPromoTemporadaNf5;
    }

    public void setIdPromoTemporadaNf5(Long idPromoTemporadaNf5) {
        this.idPromoTemporadaNf5 = idPromoTemporadaNf5;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf5Seccion2DTO getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
