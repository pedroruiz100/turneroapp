package com.peluqueria.dto;

import java.io.Serializable;

public class FacturaCabClientePendienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFactPendiente;

    private ClientePendienteDTO clientePendiente;

    private FacturaClienteCabDTO facturaClienteCab;

    public FacturaCabClientePendienteDTO() {
    }

    public Long getIdFactPendiente() {
        return idFactPendiente;
    }

    public void setIdFactPendiente(Long idFactPendiente) {
        this.idFactPendiente = idFactPendiente;
    }

    public ClientePendienteDTO getClientePendiente() {
        return clientePendiente;
    }

    public void setClientePendiente(ClientePendienteDTO clientePendiente) {
        this.clientePendiente = clientePendiente;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

}
