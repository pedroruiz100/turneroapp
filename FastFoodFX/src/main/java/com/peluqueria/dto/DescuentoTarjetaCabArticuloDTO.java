/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.io.Serializable;

/**
 *
 * @author ExcelsisWalker
 */
public class DescuentoTarjetaCabArticuloDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabArticulo;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private ArticuloDTO articulo;

    public DescuentoTarjetaCabArticuloDTO() {
    }

    public Long getIdDescuentoTarjetaCabArticulo() {
        return this.idDescuentoTarjetaCabArticulo;
    }

    public void setIdDescuentoTarjetaCabArticulo(Long idDescuentoTarjetaCabArticulo) {
        this.idDescuentoTarjetaCabArticulo = idDescuentoTarjetaCabArticulo;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

}
