package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloAuxCodNfDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloAuxCodNf;
    private String codigo;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private ArticuloDTO articulo;
    private Boolean nf;
    private String usuAlta;
    private String usuMod;

    public ArticuloAuxCodNfDTO() {
    }

    public Long getIdArticuloAuxCodNf() {
        return this.idArticuloAuxCodNf;
    }

    public void setIdArticuloAuxCodNf(Long idArticuloAuxCodNf) {
        this.idArticuloAuxCodNf = idArticuloAuxCodNf;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public Boolean getNf() {
        return this.nf;
    }

    public void setNf(Boolean nf) {
        this.nf = nf;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }
}
