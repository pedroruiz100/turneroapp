package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf4Seccion1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf4Seccion1;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf3SseccionDTO nf3Sseccion;
    private List<ArticuloNf4Seccion1DTO> articuloNf4Seccion1s;
    private List<Nf5Seccion2DTO> nf5Seccion2s;
    private List<FuncionarioNf4DTO> funcionarioNf4s;
    private List<DescuentoFielNf4DTO> descuentoFielNf4s;
    private List<PromoTemporadaNf4DTO> promoTemporadaNf4s;

    public Nf4Seccion1DTO() {
    }

    public Long getIdNf4Seccion1() {
        return this.idNf4Seccion1;
    }

    public void setIdNf4Seccion1(Long idNf4Seccion1) {
        this.idNf4Seccion1 = idNf4Seccion1;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf4Seccion1DTO> getArticuloNf4Seccion1s() {
        return this.articuloNf4Seccion1s;
    }

    public void setArticuloNf4Seccion1s(List<ArticuloNf4Seccion1DTO> articuloNf4Seccion1s) {
        this.articuloNf4Seccion1s = articuloNf4Seccion1s;
    }

    public ArticuloNf4Seccion1DTO addArticuloNf4Seccion1(ArticuloNf4Seccion1DTO articuloNf4Seccion1) {
        getArticuloNf4Seccion1s().add(articuloNf4Seccion1);
        articuloNf4Seccion1.setNf4Seccion1(this);

        return articuloNf4Seccion1;
    }

    public ArticuloNf4Seccion1DTO removeArticuloNf4Seccion1(ArticuloNf4Seccion1DTO articuloNf4Seccion1) {
        getArticuloNf4Seccion1s().remove(articuloNf4Seccion1);
        articuloNf4Seccion1.setNf4Seccion1(null);

        return articuloNf4Seccion1;
    }

    public Nf3SseccionDTO getNf3Sseccion() {
        return this.nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    public List<Nf5Seccion2DTO> getNf5Seccion2s() {
        return this.nf5Seccion2s;
    }

    public void setNf5Seccion2s(List<Nf5Seccion2DTO> nf5Seccion2s) {
        this.nf5Seccion2s = nf5Seccion2s;
    }

    public Nf5Seccion2DTO addNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        getNf5Seccion2s().add(nf5Seccion2);
        nf5Seccion2.setNf4Seccion1(this);

        return nf5Seccion2;
    }

    public Nf5Seccion2DTO removeNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        getNf5Seccion2s().remove(nf5Seccion2);
        nf5Seccion2.setNf4Seccion1(null);

        return nf5Seccion2;
    }

    public List<FuncionarioNf4DTO> getFuncionarioNf4s() {
        return this.funcionarioNf4s;
    }

    public void setFuncionarioNf4s(List<FuncionarioNf4DTO> funcionarioNf4s) {
        this.funcionarioNf4s = funcionarioNf4s;
    }

    public FuncionarioNf4DTO addFuncionarioNf4(FuncionarioNf4DTO funcionarioNf4) {
        getFuncionarioNf4s().add(funcionarioNf4);
        funcionarioNf4.setNf4Seccion1(this);

        return funcionarioNf4;
    }

    public FuncionarioNf4DTO removeFuncionarioNf4(FuncionarioNf4DTO funcionarioNf4) {
        getFuncionarioNf4s().remove(funcionarioNf4);
        funcionarioNf4.setNf4Seccion1(null);

        return funcionarioNf4;
    }

    public List<DescuentoFielNf4DTO> getDescuentoFielNf4s() {
        return this.descuentoFielNf4s;
    }

    public void setDescuentoFielNf4s(List<DescuentoFielNf4DTO> descuentoFielNf4s) {
        this.descuentoFielNf4s = descuentoFielNf4s;
    }

    public DescuentoFielNf4DTO addDescuentoFielNf4(DescuentoFielNf4DTO descuentoFielNf4) {
        getDescuentoFielNf4s().add(descuentoFielNf4);
        descuentoFielNf4.setNf4Seccion1(this);

        return descuentoFielNf4;
    }

    public DescuentoFielNf4DTO removeDescuentoFielNf4(DescuentoFielNf4DTO descuentoFielNf4) {
        getDescuentoFielNf4s().remove(descuentoFielNf4);
        descuentoFielNf4.setNf4Seccion1(null);

        return descuentoFielNf4;
    }

    public List<PromoTemporadaNf4DTO> getPromoTemporadaNf4s() {
        return this.promoTemporadaNf4s;
    }

    public void setPromoTemporadaNf4s(List<PromoTemporadaNf4DTO> promoTemporadaNf4s) {
        this.promoTemporadaNf4s = promoTemporadaNf4s;
    }

    public PromoTemporadaNf4DTO addPromoTemporadaNf4(PromoTemporadaNf4DTO promoTemporadaNf4) {
        getPromoTemporadaNf4s().add(promoTemporadaNf4);
        promoTemporadaNf4.setNf4Seccion1(this);

        return promoTemporadaNf4;
    }

    public PromoTemporadaNf4DTO removePromoTemporadaNf4(PromoTemporadaNf4DTO promoTemporadaNf4) {
        getPromoTemporadaNf4s().remove(promoTemporadaNf4);
        promoTemporadaNf4.setNf4Seccion1(null);

        return promoTemporadaNf4;
    }

}
