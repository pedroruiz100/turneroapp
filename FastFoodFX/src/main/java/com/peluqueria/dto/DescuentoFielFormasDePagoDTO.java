/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author ExcelsisWalker
 */
public class DescuentoFielFormasDePagoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielFormasDePago;
    private Boolean efectivo;
    private Boolean tarjeta;
    private Boolean cheque;
    private Boolean monExtr;
    private Boolean notaCred;
    private Timestamp fechaMod;
    private String usuMod;
    private Boolean vale;
    private Boolean bajada;

    public Long getIdDescuentoFielFormasDePago() {
        return idDescuentoFielFormasDePago;
    }

    public void setIdDescuentoFielFormasDePago(Long idDescuentoFielFormasDePago) {
        this.idDescuentoFielFormasDePago = idDescuentoFielFormasDePago;
    }

    public Boolean getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(Boolean efectivo) {
        this.efectivo = efectivo;
    }

    public Boolean getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Boolean tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Boolean getCheque() {
        return cheque;
    }

    public void setCheque(Boolean cheque) {
        this.cheque = cheque;
    }

    public Boolean getMonExtr() {
        return monExtr;
    }

    public void setMonExtr(Boolean monExtr) {
        this.monExtr = monExtr;
    }

    public Boolean getNotaCred() {
        return notaCred;
    }

    public void setNotaCred(Boolean notaCred) {
        this.notaCred = notaCred;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getVale() {
        return vale;
    }

    public void setVale(Boolean vale) {
        this.vale = vale;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }
}
