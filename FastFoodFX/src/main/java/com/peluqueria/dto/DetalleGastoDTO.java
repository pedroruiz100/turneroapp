package com.peluqueria.dto;

import java.math.BigDecimal;

public class DetalleGastoDTO {

    private Long idDetalleGasto;

    private String descripcion;

    private String codigo;

    private GastosDTO gastos;

    private Long costo;

    private BigDecimal cantidad;

    private BigDecimal cantCompra;

    private Long costoCompra;

    public DetalleGastoDTO() {
        super();
    }

    public Long getIdDetalleGasto() {
        return idDetalleGasto;
    }

    public void setIdDetalleGasto(Long idDetalleGasto) {
        this.idDetalleGasto = idDetalleGasto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo() {
        return codigo;
    }

    public BigDecimal getCantCompra() {
        return cantCompra;
    }

    public void setCantCompra(BigDecimal cantCompra) {
        this.cantCompra = cantCompra;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public GastosDTO getGastos() {
        return gastos;
    }

    public Long getCostoCompra() {
        return costoCompra;
    }

    public void setCostoCompra(Long costoCompra) {
        this.costoCompra = costoCompra;
    }

    public void setGastos(GastosDTO gastos) {
        this.gastos = gastos;
    }

    public Long getCosto() {
        return costo;
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

}
