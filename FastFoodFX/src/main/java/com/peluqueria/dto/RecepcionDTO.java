package com.peluqueria.dto;

import com.peluqueria.core.domain.Recepcion;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class RecepcionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRecepcion;

    private String tipoMovimiento;

    private String nroOrden;

    private String nroDoc;

    private Date fechaDoc;

    private String sucursal;

    private String observacion;

    private String tipoDocumento;

    private String claseDocumento;

    private ProveedorDTO proveedor;

    private String ordenDevo;

    private String ordenDevosob;

    private String cantRec;

    private String cantAv;

    private String cantFalt;

    private String cantSob;

    private String cantNev;

    private int total;

    private int saldo;

    private String estadoFactura;

    private PedidoCabDTO pedidoCab;

    public RecepcionDTO() {
    }

    public Long getIdRecepcion() {
        return idRecepcion;
    }

    public void setIdRecepcion(Long idRecepcion) {
        this.idRecepcion = idRecepcion;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getNroOrden() {
        return nroOrden;
    }

    public int getTotal() {
        return total;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setNroOrden(String nroOrden) {
        this.nroOrden = nroOrden;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public PedidoCabDTO getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCabDTO pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public Date getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(Date fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getSucursal() {
        return sucursal;
    }

    public String getOrdenDevo() {
        return ordenDevo;
    }

    public void setOrdenDevo(String ordenDevo) {
        this.ordenDevo = ordenDevo;
    }

    public String getOrdenDevosob() {
        return ordenDevosob;
    }

    public void setOrdenDevosob(String ordenDevosob) {
        this.ordenDevosob = ordenDevosob;
    }

    public String getCantRec() {
        return cantRec;
    }

    public void setCantRec(String cantRec) {
        this.cantRec = cantRec;
    }

    public String getCantAv() {
        return cantAv;
    }

    public void setCantAv(String cantAv) {
        this.cantAv = cantAv;
    }

    public String getCantFalt() {
        return cantFalt;
    }

    public void setCantFalt(String cantFalt) {
        this.cantFalt = cantFalt;
    }

    public String getCantSob() {
        return cantSob;
    }

    public void setCantSob(String cantSob) {
        this.cantSob = cantSob;
    }

    public String getCantNev() {
        return cantNev;
    }

    public void setCantNev(String cantNev) {
        this.cantNev = cantNev;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public ProveedorDTO getProveedor() {
        return proveedor;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

}
