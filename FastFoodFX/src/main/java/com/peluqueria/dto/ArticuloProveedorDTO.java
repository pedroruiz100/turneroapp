package com.peluqueria.dto;

import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ProveedorDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * The persistent class for the articulo_proveedor database table.
 *
 */
public class ArticuloProveedorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloProveedor;

    private Timestamp fechaAlta;

    private Timestamp fechaMod;

    private Timestamp fechaRecep;

    private String usuAlta;

    private String usuMod;

    private ProveedorDTO proveedor;

    @JsonIgnore
    private ArticuloDTO articulo;

    public ArticuloProveedorDTO() {
    }

    public Long getIdArticuloProveedor() {
        return this.idArticuloProveedor;
    }

    public void setIdArticuloProveedor(Long idArticuloProveedor) {
        this.idArticuloProveedor = idArticuloProveedor;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Timestamp getFechaRecep() {
        return this.fechaRecep;
    }

    public void setFechaRecep(Timestamp fechaRecep) {
        this.fechaRecep = fechaRecep;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public ArticuloDTO getArticulo() {
        return this.articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public ProveedorDTO getProveedor() {
        return proveedor;
    }

    public void setProveedor(ProveedorDTO proveedor) {
        this.proveedor = proveedor;
    }

}
