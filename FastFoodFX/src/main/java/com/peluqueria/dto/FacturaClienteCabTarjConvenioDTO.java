package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab_tarj_convenio database
 * table.
 *
 */
public class FacturaClienteCabTarjConvenioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabTarjConvenio;
    private String descripcionTarj;
    private Integer monto;
    private TarjetaConvenioDTO tarjetaConvenio;
    private List<DescTarjetaConvenioDTO> descTarjetaConvenios;
    private FacturaClienteCabDTO facturaClienteCab;

    public FacturaClienteCabTarjConvenioDTO() {
    }

    public Long getIdFacturaClienteCabTarjConvenio() {
        return this.idFacturaClienteCabTarjConvenio;
    }

    public void setIdFacturaClienteCabTarjConvenio(
            Long idFacturaClienteCabTarjConvenio) {
        this.idFacturaClienteCabTarjConvenio = idFacturaClienteCabTarjConvenio;
    }

    public String getDescripcionTarj() {
        return this.descripcionTarj;
    }

    public void setDescripcionTarj(String descripcionTarj) {
        this.descripcionTarj = descripcionTarj;
    }

    public List<DescTarjetaConvenioDTO> getDescTarjetaConvenios() {
        if (this.descTarjetaConvenios == null) {
            this.descTarjetaConvenios = new ArrayList<DescTarjetaConvenioDTO>();
        }
        return this.descTarjetaConvenios;
    }

    public void setDescTarjetaConvenios(
            List<DescTarjetaConvenioDTO> descTarjetaConvenios) {
        this.descTarjetaConvenios = descTarjetaConvenios;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TarjetaConvenioDTO getTarjetaConvenio() {
        return tarjetaConvenio;
    }

    public void setTarjetaConvenio(TarjetaConvenioDTO tarjetaConvenio) {
        this.tarjetaConvenio = tarjetaConvenio;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
