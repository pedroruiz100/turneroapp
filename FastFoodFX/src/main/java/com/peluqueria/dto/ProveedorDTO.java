package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the proveedor database table.
 *
 */
public class ProveedorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idProveedor;
    private String callePrincipal;
    private String email;
    private String descripcion;
    private String descripcion2;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private BarrioDTO barrio;
    private CiudadDTO ciudad;
    private DepartamentoDTO departamento;
    private PaisDTO pais;
    private Integer nroLocal;
    private String primeraLateral;
    private String ruc;
    private String segundaLateral;
    private String telefono;
    private String telefono2;
    private Integer timbrado;
    private String usuAlta;
    private String usuMod;
    private Date inicioVigencia;
    private Date finVigencia;
    private FamiliaProvDTO familiaProv;
    private List<ProveedorRubroDTO> proveedorRubros;
    private List<ComboCabDTO> comboCabs;
    private List<RecepcionDTO> recepcion;
    private String condPago;
    private Long saldo;
    private String paisProv;
    private String departamentoProv;
    private String ciudadProv;
    private String barrioProv;

    public ProveedorDTO() {
    }

    public Long getIdProveedor() {
        return this.idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCallePrincipal() {
        return this.callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getEmail() {
        return this.email;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Date getInicioVigencia() {
        return inicioVigencia;
    }

    public String getPaisProv() {
        return paisProv;
    }

    public void setPaisProv(String paisProv) {
        this.paisProv = paisProv;
    }

    public String getDepartamentoProv() {
        return departamentoProv;
    }

    public void setDepartamentoProv(String departamentoProv) {
        this.departamentoProv = departamentoProv;
    }

    public String getCiudadProv() {
        return ciudadProv;
    }

    public void setCiudadProv(String ciudadProv) {
        this.ciudadProv = ciudadProv;
    }

    public String getBarrioProv() {
        return barrioProv;
    }

    public void setBarrioProv(String barrioProv) {
        this.barrioProv = barrioProv;
    }

    public void setInicioVigencia(Date inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public Date getFinVigencia() {
        return finVigencia;
    }

    public String getDescripcion2() {
        return descripcion2;
    }

    public void setDescripcion2(String descripcion2) {
        this.descripcion2 = descripcion2;
    }

    public String getCondPago() {
        return condPago;
    }

    public void setCondPago(String condPago) {
        this.condPago = condPago;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

    public void setFinVigencia(Date finVigencia) {
        this.finVigencia = finVigencia;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public List<RecepcionDTO> getRecepcion() {
        if (this.recepcion == null) {
            this.recepcion = new ArrayList<>();
        }
        return recepcion;
    }

    public void setRecepcion(List<RecepcionDTO> recepcion) {
        this.recepcion = recepcion;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getRuc() {
        return this.ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono2() {
        return this.telefono2;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public Integer getTimbrado() {
        return this.timbrado;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public FamiliaProvDTO getFamiliaProv() {
        return this.familiaProv;
    }

    public void setFamiliaProv(FamiliaProvDTO familiaProv) {
        this.familiaProv = familiaProv;
    }

    public List<ProveedorRubroDTO> getProveedorRubros() {
        return this.proveedorRubros;
    }

    public void setProveedorRubros(List<ProveedorRubroDTO> proveedorRubros) {
        this.proveedorRubros = proveedorRubros;
    }

    public ProveedorRubroDTO addProveedorRubro(ProveedorRubroDTO proveedorRubro) {
        getProveedorRubros().add(proveedorRubro);
        proveedorRubro.setProveedor(this);

        return proveedorRubro;
    }

    public ProveedorRubroDTO removeProveedorRubro(ProveedorRubroDTO proveedorRubro) {
        getProveedorRubros().remove(proveedorRubro);
        proveedorRubro.setProveedor(null);

        return proveedorRubro;
    }

    // **********************************************************************************
    public List<ComboCabDTO> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCabDTO> comboCabs) {
        this.comboCabs = comboCabs;
    }

    public ComboCabDTO addComboCab(ComboCabDTO comboCab) {
        getComboCabs().add(comboCab);
        comboCab.setProveedor(this);
        return comboCab;
    }

    public ComboCabDTO removeComboCab(ComboCabDTO comboCab) {
        getComboCabs().remove(comboCab);
        comboCab.setProveedor(null);
        return comboCab;
    }

    public BarrioDTO getBarrio() {
        return barrio;
    }

    public void setBarrio(BarrioDTO barrio) {
        this.barrio = barrio;
    }

    public CiudadDTO getCiudad() {
        return ciudad;
    }

    public void setCiudad(CiudadDTO ciudad) {
        this.ciudad = ciudad;
    }

    public DepartamentoDTO getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoDTO departamento) {
        this.departamento = departamento;
    }

    public PaisDTO getPais() {
        return pais;
    }

    public void setPais(PaisDTO pais) {
        this.pais = pais;
    }

    // **********************************************************************************
}
