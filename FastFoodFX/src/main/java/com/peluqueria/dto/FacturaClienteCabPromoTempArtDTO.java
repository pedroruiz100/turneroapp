package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_cab_promo_temp_art database table.
 *
 */
public class FacturaClienteCabPromoTempArtDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabPromoTempArt;
    private FacturaClienteCabDTO facturaClienteCab;
    private PromoTemporadaArtDTO promoTemporadaArt;
    private String descripcionTemporada;
    private Integer monto;

    public FacturaClienteCabPromoTempArtDTO() {
    }

    public Long getIdFacturaClienteCabPromoTempArt() {
        return idFacturaClienteCabPromoTempArt;
    }

    public void setIdFacturaClienteCabPromoTempArt(
            Long idFacturaClienteCabPromoTempArt) {
        this.idFacturaClienteCabPromoTempArt = idFacturaClienteCabPromoTempArt;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public PromoTemporadaArtDTO getPromoTemporadaArt() {
        return promoTemporadaArt;
    }

    public void setPromoTemporadaArt(PromoTemporadaArtDTO promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public String getDescripcionTemporada() {
        return descripcionTemporada;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
