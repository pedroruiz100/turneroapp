package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf5Seccion2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf5Seccion2Articulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf5Seccion2DTO nf5Seccion2;

    public ArticuloNf5Seccion2DTO() {
    }

    public Long getIdNf5Seccion2Articulo() {
        return this.idNf5Seccion2Articulo;
    }

    public void setIdNf5Seccion2Articulo(Long idNf5Seccion2Articulo) {
        this.idNf5Seccion2Articulo = idNf5Seccion2Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf5Seccion2DTO getNf5Seccion2() {
        return this.nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

}
