package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class ClientePendienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idClientePendiente;

    private ClienteDTO cliente;

    private Boolean procesado;

    private Timestamp fechaAlta;

    private Timestamp fechaMod;

    private String usuAlta;

    private String usuMod;

    private List<FacturaCabClientePendienteDTO> facturaCabClientePendiente;

    private List<ServPendienteDTO> servPendiente;

    public ClientePendienteDTO() {
    }

    public Long getIdClientePendiente() {
        return idClientePendiente;
    }

    public void setIdClientePendiente(Long idClientePendiente) {
        this.idClientePendiente = idClientePendiente;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public Boolean getProcesado() {
        return procesado;
    }

    public void setProcesado(Boolean procesado) {
        this.procesado = procesado;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaCabClientePendienteDTO> getFacturaCabClientePendiente() {
        if (this.facturaCabClientePendiente == null) {
            this.facturaCabClientePendiente = new ArrayList<FacturaCabClientePendienteDTO>();
        }
        return facturaCabClientePendiente;
    }

    public void setFacturaCabClientePendiente(
            List<FacturaCabClientePendienteDTO> facturaCabClientePendiente) {
        this.facturaCabClientePendiente = facturaCabClientePendiente;
    }

    public List<ServPendienteDTO> getServPendiente() {
        if (this.servPendiente == null) {
            this.servPendiente = new ArrayList<ServPendienteDTO>();
        }
        return servPendiente;
    }

    public void setServPendiente(List<ServPendienteDTO> servPendiente) {
        this.servPendiente = servPendiente;
    }

}
