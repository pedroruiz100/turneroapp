package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the tipo_combo database table.
 *
 */
public class TipoComboDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoCombo;
    private String descriTipoCombo;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<ComboCabDTO> comboCabs;

    public TipoComboDTO() {
    }

    public Long getIdTipoCombo() {
        return this.idTipoCombo;
    }

    public void setIdTipoCombo(Long idTipoCombo) {
        this.idTipoCombo = idTipoCombo;
    }

    public String getDescriTipoCombo() {
        return this.descriTipoCombo;
    }

    public void setDescriTipoCombo(String descriTipoCombo) {
        this.descriTipoCombo = descriTipoCombo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ComboCabDTO> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCabDTO> comboCabs) {
        this.comboCabs = comboCabs;
    }

    public ComboCabDTO addComboCab(ComboCabDTO comboCab) {
        getComboCabs().add(comboCab);
        comboCab.setTipoCombo(this);

        return comboCab;
    }

    public ComboCabDTO removeComboCab(ComboCabDTO comboCab) {
        getComboCabs().remove(comboCab);
        comboCab.setTipoCombo(null);

        return comboCab;
    }

}
