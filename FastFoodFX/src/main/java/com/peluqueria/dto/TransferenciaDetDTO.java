package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class TransferenciaDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTransferenciaDet;

    private TransferenciaCabDTO transferenciaCab;

    private ArticuloDTO articulo;

    private String cantidad;

    private String descripcion;

    private String observacion;

    public TransferenciaDetDTO() {
    }

    public Long getIdTransferenciaDet() {
        return idTransferenciaDet;
    }

    public void setIdTransferenciaDet(Long idTransferenciaDet) {
        this.idTransferenciaDet = idTransferenciaDet;
    }

    public TransferenciaCabDTO getTransferenciaCab() {
        return transferenciaCab;
    }

    public void setTransferenciaCab(TransferenciaCabDTO transferenciaCab) {
        this.transferenciaCab = transferenciaCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

}
