package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DepartamentoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDepartamento;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<CiudadDTO> ciudadsDTO;
    private PaisDTO paisDTO;
    private List<SucursalDTO> sucursalsDTO;
    private List<ClienteDTO> clienteDTO;
    private List<ProveedorDTO> proveedorDTO;

    public List<ClienteDTO> getClienteDTO() {
        if (this.clienteDTO == null) {
            this.clienteDTO = new ArrayList<ClienteDTO>();
        }
        return clienteDTO;
    }

    public void setClienteDTO(List<ClienteDTO> clienteDTO) {
        this.clienteDTO = clienteDTO;
    }

    public List<ProveedorDTO> getProveedorDTO() {
        if (this.proveedorDTO == null) {
            this.proveedorDTO = new ArrayList<ProveedorDTO>();
        }
        return proveedorDTO;
    }

    public void setProveedorDTO(List<ProveedorDTO> proveedorDTO) {
        this.proveedorDTO = proveedorDTO;
    }

    public DepartamentoDTO() {
    }

    public Long getIdDepartamento() {
        return this.idDepartamento;
    }

    public void setIdDepartamento(Long idDepartamento) {
        this.idDepartamento = idDepartamento;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CiudadDTO> getCiudadsDTO() {
        if (this.ciudadsDTO == null) {
            this.ciudadsDTO = new ArrayList<CiudadDTO>();
        }
        return ciudadsDTO;
    }

    public void setCiudadsDTO(List<CiudadDTO> ciudadsDTO) {
        this.ciudadsDTO = ciudadsDTO;
    }

    public PaisDTO getPaisDTO() {
        return paisDTO;
    }

    public void setPaisDTO(PaisDTO paisDTO) {
        this.paisDTO = paisDTO;
    }

    public List<SucursalDTO> getSucursalsDTO() {
        if (this.sucursalsDTO == null) {
            this.sucursalsDTO = new ArrayList<SucursalDTO>();
        }
        return sucursalsDTO;
    }

    public void setSucursalsDTO(List<SucursalDTO> sucursalsDTO) {
        this.sucursalsDTO = sucursalsDTO;
    }

}
