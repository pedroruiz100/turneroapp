package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;

public class ArticuloVentaMatrizDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticuloVentaMatriz;

    private String codigo;

    private Date fecha;

    private Long cantidad;

    private String sucursal;

    public ArticuloVentaMatrizDTO() {
        super();
    }

    public Long getIdArticuloVentaMatriz() {
        return idArticuloVentaMatriz;
    }

    public void setIdArticuloVentaMatriz(Long idArticuloVentaMatriz) {
        this.idArticuloVentaMatriz = idArticuloVentaMatriz;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

}
