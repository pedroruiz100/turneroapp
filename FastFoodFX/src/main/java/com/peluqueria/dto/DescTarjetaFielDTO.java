package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the desc_tarjeta_fiel database table.
 *
 */
public class DescTarjetaFielDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescTarjetaFiel;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabTarjFielDTO facturaClienteCabTarjFiel;

    public DescTarjetaFielDTO() {
    }

    public Long getIdDescTarjetaFiel() {
        return this.idDescTarjetaFiel;
    }

    public void setIdDescTarjetaFiel(Long idDescTarjetaFiel) {
        this.idDescTarjetaFiel = idDescTarjetaFiel;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjFielDTO getFacturaClienteCabTarjFiel() {
        return facturaClienteCabTarjFiel;
    }

    public void setFacturaClienteCabTarjFiel(
            FacturaClienteCabTarjFielDTO facturaClienteCabTarjFiel) {
        this.facturaClienteCabTarjFiel = facturaClienteCabTarjFiel;
    }

}
