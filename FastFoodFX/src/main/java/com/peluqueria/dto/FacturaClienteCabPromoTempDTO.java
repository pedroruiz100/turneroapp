package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_cab_promo_temp database table.
 *
 */
public class FacturaClienteCabPromoTempDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabPromoTemp;
    private FacturaClienteCabDTO facturaClienteCab;
    private PromoTemporadaDTO promoTemporada;
    private String descripcionTemporada;
    private Integer monto;

    public FacturaClienteCabPromoTempDTO() {
    }

    public Long getIdFacturaClienteCabPromoTemp() {
        return idFacturaClienteCabPromoTemp;
    }

    public void setIdFacturaClienteCabPromoTemp(
            Long idFacturaClienteCabPromoTemp) {
        this.idFacturaClienteCabPromoTemp = idFacturaClienteCabPromoTemp;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public String getDescripcionTemporada() {
        return descripcionTemporada;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
