package com.peluqueria.dto;

import java.io.Serializable;

public class RangoTesoreriaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRango;
    private Long rangoInicial;
    private Long rangoFinal;
    private Long rangoActual;

    public RangoTesoreriaDTO() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

}
