package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class PedidoDetConteoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPedidoDetConteo;

    private PedidoCabDTO pedidoCab;

    private ArticuloDTO articulo;

    private String descripcion;

    private Long precio;

    private String cantidad;

    private long total;

    private String sucursal;

    private Boolean cc;

    private Boolean sc;

    private Boolean sl;

    public PedidoDetConteoDTO() {
    }

    public PedidoCabDTO getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCabDTO pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getSc() {
        return sc;
    }

    public void setSc(Boolean sc) {
        this.sc = sc;
    }

    public Boolean getSl() {
        return sl;
    }

    public void setSl(Boolean sl) {
        this.sl = sl;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getIdPedidoDetConteo() {
        return idPedidoDetConteo;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setIdPedidoDetConteo(Long idPedidoDetConteo) {
        this.idPedidoDetConteo = idPedidoDetConteo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

}
