package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf3DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf3;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf3SseccionDTO nf3Sseccion;
    private String descriSeccion;

    public DescuentoTarjetaCabNf3DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf3() {
        return this.idDescuentoTarjetaCabNf3;
    }

    public void setIdDescuentoTarjetaCabNf3(Long idDescuentoTarjetaCabNf3) {
        this.idDescuentoTarjetaCabNf3 = idDescuentoTarjetaCabNf3;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf3SseccionDTO getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

}
