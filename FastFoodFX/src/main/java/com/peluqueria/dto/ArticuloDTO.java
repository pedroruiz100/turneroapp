package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the articulo database table.
 *
 */
public class ArticuloDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArticulo;
    private String codArticulo;
    private String descripcion;
    private String md;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Boolean permiteDesc;
    private Long precioMay;
    private Long precioMin;
    private Long costo;
    private String usuAlta;
    private String usuMod;
    private Boolean servicio;
    private Boolean stockeable;
    private Boolean importado;
    private Boolean bajada;
    private IvaDTO iva;
    private MarcaDTO marca;
    private SeccionSubDTO seccionSub;
    private UnidadDTO unidad;
    private SeccionDTO seccion;
    private Long outlet;
    private Long sanlo;
    private Long cacique;
    private Long descMaxMay;
    private String urlImagen;
    private List<ComboDetDTO> comboDets;
    private List<CancelacionProductoDTO> cancelacionProductos;
    private List<FacturaClienteDetDTO> facturaClienteDets;
    private List<NotaCreditoDetDTO> notaCreditoDets;
    private List<NotaRemisionDetDTO> notaRemisionDets;
    private List<ArticuloSrvComisionDTO> articuloSrvComision;
    private List<ServPendienteDTO> servPendiente;
    private List<ArtPromoTemporadaDTO> artPromoTemporada;
    private List<ArticuloAuxCodNfDTO> articuloAuxCodNf;
    private List<ArticuloNf1TipoDTO> articuloNf1Tipo;
    private List<ArticuloNf2SfamiliaDTO> articuloNf2Sfamilia;
    private List<ArticuloNf3SseccionDTO> articuloNf3Sseccion;
    private List<ArticuloNf4Seccion1DTO> articuloNf4Seccion1;
    private List<ArticuloNf5Seccion2DTO> articuloNf5Seccion2;
    private List<ArticuloNf6Secnom6DTO> articuloNf6Secnom6;
    private List<ArticuloNf7Secnom7DTO> articuloNf7Secnom7;
    private List<DescuentoTarjetaCabArticuloDTO> descuentoTarjetaCabArticulos;
    private List<ArtPromoTemporadaObsequioDTO> artPromoTemporadaObsequio;
    private List<ExistenciaDTO> existencia;
    private List<NotaCreditoDetProveedorDTO> notaCreditoDetProveedor;
    private String sec1;
    private String sec2;
    private String costoExtranjero;
    private String costoOrigenImportado;
    private String descImportado;
    private String increImportado;
    private String increParana;
    private String increParana2;
    private String increParana3;
    private String increMay;
    private String cotizacion;
    private String observacion;
    private String stockMin;
    private String stockMax;
    private String stockActual;
    private Long descMaxMin;
    private Long descProveedor;
    private byte[] imagen;

    public ArticuloDTO() {
    }

    public Long getIdArticulo() {
        return this.idArticulo;
    }

    public void setIdArticulo(Long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getStockMin() {
        return stockMin;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public Long getDescMaxMin() {
        return descMaxMin;
    }

    public void setDescMaxMin(Long descMaxMin) {
        this.descMaxMin = descMaxMin;
    }

    public Long getDescProveedor() {
        return descProveedor;
    }

    public void setDescProveedor(Long descProveedor) {
        this.descProveedor = descProveedor;
    }

    public void setStockMin(String stockMin) {
        this.stockMin = stockMin;
    }

    public String getStockMax() {
        return stockMax;
    }

    public void setStockMax(String stockMax) {
        this.stockMax = stockMax;
    }

    public String getStockActual() {
        return stockActual;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public void setStockActual(String stockActual) {
        this.stockActual = stockActual;
    }

    public String getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public String getCostoExtranjero() {
        return costoExtranjero;
    }

    public void setCostoExtranjero(String costoExtranjero) {
        this.costoExtranjero = costoExtranjero;
    }

    public String getCostoOrigenImportado() {
        return costoOrigenImportado;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(String cotizacion) {
        this.cotizacion = cotizacion;
    }

    public void setCostoOrigenImportado(String costoOrigenImportado) {
        this.costoOrigenImportado = costoOrigenImportado;
    }

    public String getDescImportado() {
        return descImportado;
    }

    public void setDescImportado(String descImportado) {
        this.descImportado = descImportado;
    }

    public String getIncreImportado() {
        return increImportado;
    }

    public void setIncreImportado(String increImportado) {
        this.increImportado = increImportado;
    }

    public String getIncreParana() {
        return increParana;
    }

    public void setIncreParana(String increParana) {
        this.increParana = increParana;
    }

    public String getIncreParana2() {
        return increParana2;
    }

    public void setIncreParana2(String increParana2) {
        this.increParana2 = increParana2;
    }

    public String getIncreParana3() {
        return increParana3;
    }

    public void setIncreParana3(String increParana3) {
        this.increParana3 = increParana3;
    }

    public String getIncreMay() {
        return increMay;
    }

    public void setIncreMay(String increMay) {
        this.increMay = increMay;
    }

    public List<NotaCreditoDetProveedorDTO> getNotaCreditoDetProveedor() {
        if (this.notaCreditoDetProveedor == null) {
            this.notaCreditoDetProveedor = new ArrayList<NotaCreditoDetProveedorDTO>();
        }
        return notaCreditoDetProveedor;
    }

    public void setNotaCreditoDetProveedor(List<NotaCreditoDetProveedorDTO> notaCreditoDetProveedor) {
        this.notaCreditoDetProveedor = notaCreditoDetProveedor;
    }

    public String getSec1() {
        return sec1;
    }

    public void setSec1(String sec1) {
        this.sec1 = sec1;
    }

    public String getSec2() {
        return sec2;
    }

    public void setSec2(String sec2) {
        this.sec2 = sec2;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getPermiteDesc() {
        return this.permiteDesc;
    }

    public void setPermiteDesc(Boolean permiteDesc) {
        this.permiteDesc = permiteDesc;
    }

    public Long getCosto() {
        return costo;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public Long getOutlet() {
        return outlet;
    }

    public void setOutlet(Long outlet) {
        if (outlet != null) {
            this.outlet = outlet;
        } else {
            this.outlet = 0l;
        }
    }

    public Long getSanlo() {
        return sanlo;
    }

    public void setSanlo(Long sanlo) {
        if (sanlo != null) {
            this.sanlo = sanlo;
        } else {
            this.sanlo = 0l;
        }
    }

    public Long getCacique() {
        return cacique;
    }

    public void setCacique(Long cacique) {
        if (cacique != null) {
            this.cacique = cacique;
        } else {
            this.cacique = 0l;
        }
    }

    public Long getDescMaxMay() {
        return descMaxMay;
    }

    public void setDescMaxMay(Long descMaxMay) {
        if (descMaxMay != null) {
            this.descMaxMay = descMaxMay;
        } else {
            this.descMaxMay = 0l;
        }
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public List<ExistenciaDTO> getExistencia() {
        if (this.existencia == null) {
            this.existencia = new ArrayList<>();
        }
        return existencia;
    }

    public void setExistencia(List<ExistenciaDTO> existencia) {
        this.existencia = existencia;
    }

    public Long getPrecioMay() {
        return this.precioMay;
    }

    public void setPrecioMay(Long precioMay) {
        this.precioMay = precioMay;
    }

    public Boolean getServicio() {
        return servicio;
    }

    public void setServicio(Boolean servicio) {
        this.servicio = servicio;
    }

    public Boolean getStockeable() {
        return stockeable;
    }

    public void setStockeable(Boolean stockeable) {
        this.stockeable = stockeable;
    }

    public Boolean getImportado() {
        return importado;
    }

    public void setImportado(Boolean importado) {
        this.importado = importado;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }

    public Long getPrecioMin() {
        return this.precioMin;
    }

    public void setPrecioMin(Long precioMin) {
        this.precioMin = precioMin;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public IvaDTO getIva() {
        return this.iva;
    }

    public void setIva(IvaDTO iva) {
        this.iva = iva;
    }

    public MarcaDTO getMarca() {
        return this.marca;
    }

    public void setMarca(MarcaDTO marca) {
        this.marca = marca;
    }

    public SeccionSubDTO getSeccionSub() {
        return this.seccionSub;
    }

    public void setSeccionSub(SeccionSubDTO seccionSub) {
        this.seccionSub = seccionSub;
    }

    public UnidadDTO getUnidad() {
        return this.unidad;
    }

    public void setUnidad(UnidadDTO unidad) {
        this.unidad = unidad;
    }

    public List<ComboDetDTO> getComboDets() {
        if (this.comboDets == null) {
            this.comboDets = new ArrayList<ComboDetDTO>();
        }
        return this.comboDets;
    }

    public void setComboDets(List<ComboDetDTO> comboDets) {
        this.comboDets = comboDets;
    }

    // **********************************************************************************
    public List<CancelacionProductoDTO> getCancelacionProductos() {
        if (this.cancelacionProductos == null) {
            this.cancelacionProductos = new ArrayList<CancelacionProductoDTO>();
        }
        return cancelacionProductos;
    }

    public void setCancelacionProductos(
            List<CancelacionProductoDTO> cancelacionProductos) {
        this.cancelacionProductos = cancelacionProductos;
    }

    // **********************************************************************************
    public List<FacturaClienteDetDTO> getFacturaClienteDets() {
        if (this.facturaClienteDets == null) {
            this.facturaClienteDets = new ArrayList<>();
        }
        return facturaClienteDets;
    }

    public void setFacturaClienteDets(
            List<FacturaClienteDetDTO> facturaClienteDets) {
        this.facturaClienteDets = facturaClienteDets;
    }

    // **********************************************************************************
    public List<NotaCreditoDetDTO> getNotaCreditoDets() {
        if (this.notaCreditoDets == null) {
            this.notaCreditoDets = new ArrayList<>();
        }
        return notaCreditoDets;
    }

    public void setNotaCreditoDets(List<NotaCreditoDetDTO> notaCreditoDets) {
        this.notaCreditoDets = notaCreditoDets;
    }

    // **********************************************************************************
    public SeccionDTO getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }

    public List<NotaRemisionDetDTO> getNotaRemisionDets() {
        if (this.notaRemisionDets == null) {
            this.notaRemisionDets = new ArrayList<>();
        }
        return notaRemisionDets;
    }

    public void setNotaRemisionDets(List<NotaRemisionDetDTO> notaRemisionDets) {
        this.notaRemisionDets = notaRemisionDets;
    }

    // **********************************************************************************
    public List<ArticuloSrvComisionDTO> getArticuloSrvComision() {
        if (this.articuloSrvComision == null) {
            this.articuloSrvComision = new ArrayList<>();
        }
        return articuloSrvComision;
    }

    public void setArticuloSrvComision(List<ArticuloSrvComisionDTO> articuloSrvComision) {
        this.articuloSrvComision = articuloSrvComision;
    }

    public List<ServPendienteDTO> getServPendiente() {
        if (this.servPendiente == null) {
            this.servPendiente = new ArrayList<>();
        }
        return servPendiente;
    }

    public void setServPendiente(List<ServPendienteDTO> servPendiente) {
        this.servPendiente = servPendiente;
    }

    public List<ArtPromoTemporadaDTO> getArtPromoTemporada() {
        if (this.artPromoTemporada == null) {
            this.artPromoTemporada = new ArrayList<>();
        }
        return artPromoTemporada;
    }

    public void setArtPromoTemporada(List<ArtPromoTemporadaDTO> artPromoTemporada) {
        this.artPromoTemporada = artPromoTemporada;
    }

    public List<ArticuloAuxCodNfDTO> getArticuloAuxCodNf() {
        if (this.articuloAuxCodNf == null) {
            this.articuloAuxCodNf = new ArrayList<>();
        }
        return articuloAuxCodNf;
    }

    public void setArticuloAuxCodNf(List<ArticuloAuxCodNfDTO> articuloAuxCodNf) {
        this.articuloAuxCodNf = articuloAuxCodNf;
    }

    public List<ArticuloNf1TipoDTO> getArticuloNf1Tipo() {
        if (this.articuloNf1Tipo == null) {
            this.articuloNf1Tipo = new ArrayList<>();
        }
        return articuloNf1Tipo;
    }

    public void setArticuloNf1Tipo(List<ArticuloNf1TipoDTO> articuloNf1Tipo) {
        this.articuloNf1Tipo = articuloNf1Tipo;
    }

    public List<ArticuloNf2SfamiliaDTO> getArticuloNf2Sfamilia() {
        if (this.articuloNf2Sfamilia == null) {
            this.articuloNf2Sfamilia = new ArrayList<>();
        }
        return articuloNf2Sfamilia;
    }

    public void setArticuloNf2Sfamilia(List<ArticuloNf2SfamiliaDTO> articuloNf2Sfamilia) {
        this.articuloNf2Sfamilia = articuloNf2Sfamilia;
    }

    public List<ArticuloNf3SseccionDTO> getArticuloNf3Sseccion() {
        if (this.articuloNf3Sseccion == null) {
            this.articuloNf3Sseccion = new ArrayList<>();
        }
        return articuloNf3Sseccion;
    }

    public void setArticuloNf3Sseccion(List<ArticuloNf3SseccionDTO> articuloNf3Sseccion) {
        this.articuloNf3Sseccion = articuloNf3Sseccion;
    }

    public List<ArticuloNf4Seccion1DTO> getArticuloNf4Seccion1() {
        if (this.articuloNf4Seccion1 == null) {
            this.articuloNf4Seccion1 = new ArrayList<>();
        }
        return articuloNf4Seccion1;
    }

    public void setArticuloNf4Seccion1(List<ArticuloNf4Seccion1DTO> articuloNf4Seccion1) {
        this.articuloNf4Seccion1 = articuloNf4Seccion1;
    }

    public List<ArticuloNf5Seccion2DTO> getArticuloNf5Seccion2() {
        if (this.articuloNf5Seccion2 == null) {
            this.articuloNf5Seccion2 = new ArrayList<>();
        }
        return articuloNf5Seccion2;
    }

    public void setArticuloNf5Seccion2(List<ArticuloNf5Seccion2DTO> articuloNf5Seccion2) {
        this.articuloNf5Seccion2 = articuloNf5Seccion2;
    }

    public List<ArticuloNf6Secnom6DTO> getArticuloNf6Secnom6() {
        if (this.articuloNf6Secnom6 == null) {
            this.articuloNf6Secnom6 = new ArrayList<>();
        }
        return articuloNf6Secnom6;
    }

    public void setArticuloNf6Secnom6(List<ArticuloNf6Secnom6DTO> articuloNf6Secnom6) {
        this.articuloNf6Secnom6 = articuloNf6Secnom6;
    }

    public List<ArticuloNf7Secnom7DTO> getArticuloNf7Secnom7() {
        if (this.articuloNf7Secnom7 == null) {
            this.articuloNf7Secnom7 = new ArrayList<>();
        }
        return articuloNf7Secnom7;
    }

    public void setArticuloNf7Secnom7(List<ArticuloNf7Secnom7DTO> articuloNf7Secnom7) {
        this.articuloNf7Secnom7 = articuloNf7Secnom7;
    }

    public List<DescuentoTarjetaCabArticuloDTO> getDescuentoTarjetaCabArticulos() {
        if (this.descuentoTarjetaCabArticulos == null) {
            this.descuentoTarjetaCabArticulos = new ArrayList<>();
        }
        return descuentoTarjetaCabArticulos;
    }

    public void setDescuentoTarjetaCabArticulos(List<DescuentoTarjetaCabArticuloDTO> descuentoTarjetaCabArticulos) {
        this.descuentoTarjetaCabArticulos = descuentoTarjetaCabArticulos;
    }

    public List<ArtPromoTemporadaObsequioDTO> getArtPromoTemporadaObsequio() {
        if (this.artPromoTemporadaObsequio == null) {
            this.artPromoTemporadaObsequio = new ArrayList<>();
        }
        return artPromoTemporadaObsequio;
    }

    public void setArtPromoTemporadaObsequio(List<ArtPromoTemporadaObsequioDTO> artPromoTemporadaObsequio) {
        this.artPromoTemporadaObsequio = artPromoTemporadaObsequio;
    }
}
