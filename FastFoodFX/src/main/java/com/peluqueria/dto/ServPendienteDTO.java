package com.peluqueria.dto;

import java.io.Serializable;

public class ServPendienteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idServPendiente;

    private ClientePendienteDTO clientePendiente;

    private ArticuloDTO articulo;

    private FuncionarioDTO funcionario;

    private Integer montoServ;

    private Integer montoComision;

    private Integer cantidad;

    private Boolean comisionPorc;

    private String descripcion;

    private Integer poriva;

    private Long codArticulo;

    private Integer porc;

    public ServPendienteDTO() {
    }

    public Long getIdServPendiente() {
        return idServPendiente;
    }

    public void setIdServPendiente(Long idServPendiente) {
        this.idServPendiente = idServPendiente;
    }

    public ClientePendienteDTO getClientePendiente() {
        return clientePendiente;
    }

    public void setClientePendiente(ClientePendienteDTO clientePendiente) {
        this.clientePendiente = clientePendiente;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPoriva() {
        return poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public Integer getPorc() {
        return porc;
    }

    public void setPorc(Integer porc) {
        this.porc = porc;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getMontoServ() {
        return montoServ;
    }

    public void setMontoServ(Integer montoServ) {
        this.montoServ = montoServ;
    }

    public Integer getMontoComision() {
        return montoComision;
    }

    public void setMontoComision(Integer montoComision) {
        this.montoComision = montoComision;
    }

    public Boolean getComisionPorc() {
        return comisionPorc;
    }

    public void setComisionPorc(Boolean comisionPorc) {
        this.comisionPorc = comisionPorc;
    }

}
