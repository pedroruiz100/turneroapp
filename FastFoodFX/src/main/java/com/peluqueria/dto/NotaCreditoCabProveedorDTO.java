package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Date;

/**
 * The persistent class for the nota_credito_cab database table.
 *
 */
public class NotaCreditoCabProveedorDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaCreditoCabProveedor;
    private Date fechaNota;
    private String nroNota;
    private String nroOc;
    private String nroDevolucion;

    public NotaCreditoCabProveedorDTO() {
    }

    public Long getIdNotaCreditoCabProveedor() {
        return idNotaCreditoCabProveedor;
    }

    public void setIdNotaCreditoCabProveedor(Long idNotaCreditoCabProveedor) {
        this.idNotaCreditoCabProveedor = idNotaCreditoCabProveedor;
    }

    public Date getFechaNota() {
        return fechaNota;
    }

    public void setFechaNota(Date fechaNota) {
        this.fechaNota = fechaNota;
    }

    public String getNroNota() {
        return nroNota;
    }

    public String getNroOc() {
        return nroOc;
    }

    public void setNroOc(String nroOc) {
        this.nroOc = nroOc;
    }

    public String getNroDevolucion() {
        return nroDevolucion;
    }

    public void setNroDevolucion(String nroDevolucion) {
        this.nroDevolucion = nroDevolucion;
    }

    public void setNroNota(String nroNota) {
        this.nroNota = nroNota;
    }

}
