package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the seccion_sub database table.
 *
 */
public class SeccionSubDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSeccionSub;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<ArticuloDTO> articulos;
    private SeccionDTO seccion;

    public SeccionSubDTO() {
    }

    public Long getIdSeccionSub() {
        return this.idSeccionSub;
    }

    public void setIdSeccionSub(Long idSeccionSub) {
        this.idSeccionSub = idSeccionSub;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloDTO> getArticulos() {
        return this.articulos;
    }

    public void setArticulos(List<ArticuloDTO> articulos) {
        if (this.articulos == null) {
            this.articulos = new ArrayList<ArticuloDTO>();
        }
        this.articulos = articulos;
    }

    public SeccionDTO getSeccion() {
        return this.seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }

}
