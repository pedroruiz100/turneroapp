package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class CierreCajaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCierre;
    private BigDecimal diferenciaCierre;
    private Timestamp fechaCierre;
    private UsuarioDTO usuarioCajero;
    private UsuarioDTO usuarioSupervisor;
    private BigDecimal montoCaja;
    private BigDecimal montoCierre;
    private String tipoCierre;
    private CajaDTO caja;

    public CierreCajaDTO() {
    }

    public Long getIdCierre() {
        return this.idCierre;
    }

    public void setIdCierre(Long idCierre) {
        this.idCierre = idCierre;
    }

    public BigDecimal getDiferenciaCierre() {
        return this.diferenciaCierre;
    }

    public void setDiferenciaCierre(BigDecimal diferenciaCierre) {
        this.diferenciaCierre = diferenciaCierre;
    }

    public Timestamp getFechaCierre() {
        return this.fechaCierre;
    }

    public void setFechaCierre(Timestamp fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public BigDecimal getMontoCaja() {
        return this.montoCaja;
    }

    public void setMontoCaja(BigDecimal montoCaja) {
        this.montoCaja = montoCaja;
    }

    public BigDecimal getMontoCierre() {
        return this.montoCierre;
    }

    public void setMontoCierre(BigDecimal montoCierre) {
        this.montoCierre = montoCierre;
    }

    public String getTipoCierre() {
        return this.tipoCierre;
    }

    public void setTipoCierre(String tipoCierre) {
        this.tipoCierre = tipoCierre;
    }

    public CajaDTO getCaja() {
        return this.caja;
    }

    public void setCaja(CajaDTO caja) {
        this.caja = caja;
    }

    // *******************************************************************
    public UsuarioDTO getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public UsuarioDTO getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(UsuarioDTO usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

}
