/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.sql.Timestamp;

/**
 *
 * @author PC
 */
public class TransferenciaCabDTO {

    private Long idTransferenciaCab;

    private Timestamp fecha;

    private String usuario;

    private DepositoDTO depositoOrigen;

    private DepositoDTO depositoDestino;

    public Long getIdTransferenciaCab() {
        return idTransferenciaCab;
    }

    public void setIdTransferenciaCab(Long idTransferenciaCab) {
        this.idTransferenciaCab = idTransferenciaCab;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public DepositoDTO getDepositoOrigen() {
        return depositoOrigen;
    }

    public void setDepositoOrigen(DepositoDTO depositoOrigen) {
        this.depositoOrigen = depositoOrigen;
    }

    public DepositoDTO getDepositoDestino() {
        return depositoDestino;
    }

    public void setDepositoDestino(DepositoDTO depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

}
