package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the vales database table.
 *
 */
public class ValeDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idVale;
    private String descripcionVale;
    private String usuAlta;
    private Timestamp fechaAlta;
    private String usuMod;
    private Timestamp fechaMod;
    private List<FacturaClienteCabValeDTO> facturaClienteCabVale;

    public ValeDTO() {
    }

    public Long getIdVale() {
        return this.idVale;
    }

    public void setIdVale(Long idVale) {
        this.idVale = idVale;
    }

    // **************************************************************************************************************
    public String getDescripcionVale() {
        return descripcionVale;
    }

    public void setDescripcionVale(String descripcionVale) {
        this.descripcionVale = descripcionVale;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<FacturaClienteCabValeDTO> getFacturaClienteCabVale() {
        if (this.facturaClienteCabVale == null) {
            this.facturaClienteCabVale = new ArrayList<FacturaClienteCabValeDTO>();
        }
        return facturaClienteCabVale;
    }

    public void setFacturaClienteCabVale(
            List<FacturaClienteCabValeDTO> facturaClienteCabVale) {
        this.facturaClienteCabVale = facturaClienteCabVale;
    }

}
