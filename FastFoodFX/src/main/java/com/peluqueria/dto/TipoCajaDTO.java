package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class TipoCajaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoCaja;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<CajaDTO> cajas;

    public TipoCajaDTO() {
    }

    public Long getIdTipoCaja() {
        return this.idTipoCaja;
    }

    public void setIdTipoCaja(Long idTipoCaja) {
        this.idTipoCaja = idTipoCaja;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CajaDTO> getCajas() {
        return this.cajas;
    }

    public void setCajas(List<CajaDTO> cajas) {
        this.cajas = cajas;
    }

    public CajaDTO addCaja(CajaDTO caja) {
        getCajas().add(caja);
        caja.setTipoCaja(this);

        return caja;
    }

    public CajaDTO removeCaja(CajaDTO caja) {
        getCajas().remove(caja);
        caja.setTipoCaja(null);

        return caja;
    }

}
