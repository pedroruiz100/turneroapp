package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;

public class Nf3SseccionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf3Sseccion;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf2SfamiliaDTO nf2Sfamilia;
    private List<ArticuloNf3SseccionDTO> articuloNf3Sseccions;
    private List<Nf4Seccion1DTO> nf4Seccion1s;
    private List<FuncionarioNf3DTO> funcionarioNf3s;
    private List<DescuentoFielNf3DTO> descuentoFielNf3s;
    private List<PromoTemporadaNf3DTO> promoTemporadaNf3s;

    public Nf3SseccionDTO() {
    }

    public Long getIdNf3Sseccion() {
        return this.idNf3Sseccion;
    }

    public void setIdNf3Sseccion(Long idNf3Sseccion) {
        this.idNf3Sseccion = idNf3Sseccion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf3SseccionDTO> getArticuloNf3Sseccions() {
        return this.articuloNf3Sseccions;
    }

    public void setArticuloNf3Sseccions(List<ArticuloNf3SseccionDTO> articuloNf3Sseccions) {
        this.articuloNf3Sseccions = articuloNf3Sseccions;
    }

    public ArticuloNf3SseccionDTO addArticuloNf3Sseccion(ArticuloNf3SseccionDTO articuloNf3Sseccion) {
        getArticuloNf3Sseccions().add(articuloNf3Sseccion);
        articuloNf3Sseccion.setNf3Sseccion(this);

        return articuloNf3Sseccion;
    }

    public ArticuloNf3Sseccion removeArticuloNf3Sseccion(ArticuloNf3Sseccion articuloNf3Sseccion) {
        getArticuloNf3Sseccions().remove(articuloNf3Sseccion);
        articuloNf3Sseccion.setNf3Sseccion(null);

        return articuloNf3Sseccion;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return this.nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    public List<Nf4Seccion1DTO> getNf4Seccion1s() {
        return this.nf4Seccion1s;
    }

    public void setNf4Seccion1s(List<Nf4Seccion1DTO> nf4Seccion1s) {
        this.nf4Seccion1s = nf4Seccion1s;
    }

    public Nf4Seccion1DTO addNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        getNf4Seccion1s().add(nf4Seccion1);
        nf4Seccion1.setNf3Sseccion(this);

        return nf4Seccion1;
    }

    public Nf4Seccion1 removeNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        getNf4Seccion1s().remove(nf4Seccion1);
        nf4Seccion1.setNf3Sseccion(null);

        return nf4Seccion1;
    }

    public List<FuncionarioNf3DTO> getFuncionarioNf3s() {
        return this.funcionarioNf3s;
    }

    public void setFuncionarioNf3s(List<FuncionarioNf3DTO> funcionarioNf3s) {
        this.funcionarioNf3s = funcionarioNf3s;
    }

    public FuncionarioNf3DTO addFuncionarioNf3(FuncionarioNf3DTO funcionarioNf3) {
        getFuncionarioNf3s().add(funcionarioNf3);
        funcionarioNf3.setNf3Sseccion(this);

        return funcionarioNf3;
    }

    public FuncionarioNf3DTO removeFuncionarioNf3(FuncionarioNf3DTO funcionarioNf3) {
        getFuncionarioNf3s().remove(funcionarioNf3);
        funcionarioNf3.setNf3Sseccion(null);

        return funcionarioNf3;
    }

    public List<DescuentoFielNf3DTO> getDescuentoFielNf3s() {
        return this.descuentoFielNf3s;
    }

    public void setDescuentoFielNf3s(List<DescuentoFielNf3DTO> descuentoFielNf3s) {
        this.descuentoFielNf3s = descuentoFielNf3s;
    }

    public DescuentoFielNf3DTO addDescuentoFielNf3(DescuentoFielNf3DTO descuentoFielNf3) {
        getDescuentoFielNf3s().add(descuentoFielNf3);
        descuentoFielNf3.setNf3Sseccion(this);

        return descuentoFielNf3;
    }

    public DescuentoFielNf3DTO removeDescuentoFielNf3(DescuentoFielNf3DTO descuentoFielNf3) {
        getDescuentoFielNf3s().remove(descuentoFielNf3);
        descuentoFielNf3.setNf3Sseccion(null);

        return descuentoFielNf3;
    }

    public List<PromoTemporadaNf3DTO> getPromoTemporadaNf3s() {
        return this.promoTemporadaNf3s;
    }

    public void setPromoTemporadaNf3s(List<PromoTemporadaNf3DTO> promoTemporadaNf3s) {
        this.promoTemporadaNf3s = promoTemporadaNf3s;
    }

    public PromoTemporadaNf3DTO addPromoTemporadaNf3(PromoTemporadaNf3DTO promoTemporadaNf3) {
        getPromoTemporadaNf3s().add(promoTemporadaNf3);
        promoTemporadaNf3.setNf3Sseccion(this);

        return promoTemporadaNf3;
    }

    public PromoTemporadaNf3DTO removePromoTemporadaNf3(PromoTemporadaNf3DTO promoTemporadaNf3) {
        getPromoTemporadaNf3s().remove(promoTemporadaNf3);
        promoTemporadaNf3.setNf3Sseccion(null);

        return promoTemporadaNf3;
    }

}
