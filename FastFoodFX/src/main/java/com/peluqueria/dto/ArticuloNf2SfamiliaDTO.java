package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf2SfamiliaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf2SfamiliaArticulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf2SfamiliaDTO nf2Sfamilia;

    public ArticuloNf2SfamiliaDTO() {
    }

    public Long getIdNf2SfamiliaArticulo() {
        return this.idNf2SfamiliaArticulo;
    }

    public void setIdNf2SfamiliaArticulo(Long idNf2SfamiliaArticulo) {
        this.idNf2SfamiliaArticulo = idNf2SfamiliaArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return this.nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

}
