package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the factura_cliente_cab_tipo_pago database table.
 *
 */
public class FacturaClienteCabTipoPagoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabTipoPago;
    private Integer montoPago;
    private FacturaClienteCabDTO facturaClienteCab;
    private TipoPagoDTO tipoPago;

    public FacturaClienteCabTipoPagoDTO() {
    }

    public Long getIdFacturaClienteCabTipoPago() {
        return this.idFacturaClienteCabTipoPago;
    }

    public void setIdFacturaClienteCabTipoPago(Long idFacturaClienteCabTipoPago) {
        this.idFacturaClienteCabTipoPago = idFacturaClienteCabTipoPago;
    }

    public Integer getMontoPago() {
        return this.montoPago;
    }

    public void setMontoPago(Integer montoPago) {
        this.montoPago = montoPago;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoPagoDTO getTipoPago() {
        return this.tipoPago;
    }

    public void setTipoPago(TipoPagoDTO tipoPago) {
        this.tipoPago = tipoPago;
    }

}
