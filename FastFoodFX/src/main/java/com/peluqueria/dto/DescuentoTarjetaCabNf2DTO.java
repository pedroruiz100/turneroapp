package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaCabNf2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabNf2;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Nf2SfamiliaDTO nf2Sfamilia;
    private String descriSeccion;

    public DescuentoTarjetaCabNf2DTO() {
    }

    public Long getIdDescuentoTarjetaCabNf2() {
        return this.idDescuentoTarjetaCabNf2;
    }

    public void setIdDescuentoTarjetaCabNf2(Long idDescuentoTarjetaCabNf2) {
        this.idDescuentoTarjetaCabNf2 = idDescuentoTarjetaCabNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

}
