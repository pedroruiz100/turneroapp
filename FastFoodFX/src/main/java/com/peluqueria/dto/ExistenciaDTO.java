package com.peluqueria.dto;

import java.io.Serializable;

public class ExistenciaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idExistencia;
    private long cantidad;
    private DepositoDTO deposito;
    private ArticuloDTO articulo;

    public ExistenciaDTO() {
    }

    public Long getIdExistencia() {
        return idExistencia;
    }

    public void setIdExistencia(Long idExistencia) {
        this.idExistencia = idExistencia;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public DepositoDTO getDeposito() {
        return deposito;
    }

    public void setDeposito(DepositoDTO deposito) {
        this.deposito = deposito;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

}
