package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf1TipoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf1TipoArticulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf1TipoDTO nf1Tipo;

    public ArticuloNf1TipoDTO() {
    }

    public Long getIdNf1TipoArticulo() {
        return this.idNf1TipoArticulo;
    }

    public void setIdNf1TipoArticulo(Long idNf1TipoArticulo) {
        this.idNf1TipoArticulo = idNf1TipoArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return this.nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

}
