package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class FuncionarioNf6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncionarioNf6;
    private String descriSeccion;
    private Boolean estadoDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Nf6Secnom6DTO nf6Secnom6;
    private BigDecimal porcDesc;
    private String usuAlta;
    private String usuMod;

    public FuncionarioNf6DTO() {
    }

    public Long getIdFuncionarioNf6() {
        return this.idFuncionarioNf6;
    }

    public void setIdFuncionarioNf6(Long idFuncionarioNf6) {
        this.idFuncionarioNf6 = idFuncionarioNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

}
