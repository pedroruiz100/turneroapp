package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class RolFuncionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRolFuncion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private FuncionDTO funcion;
    private RolDTO rolDTO;

    public RolFuncionDTO() {
    }

    public Long getIdRolFuncion() {
        return this.idRolFuncion;
    }

    public void setIdRolFuncion(Long idRolFuncion) {
        this.idRolFuncion = idRolFuncion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public FuncionDTO getFuncion() {
        return funcion;
    }

    public void setFuncion(FuncionDTO funcion) {
        this.funcion = funcion;
    }

    public RolDTO getRolDTO() {
        return rolDTO;
    }

    public void setRolDTO(RolDTO rolDTO) {
        this.rolDTO = rolDTO;
    }

    public RolDTO getRol() {
        return this.rolDTO;
    }

    public void setRol(RolDTO rol) {
        this.rolDTO = rol;
    }

}
