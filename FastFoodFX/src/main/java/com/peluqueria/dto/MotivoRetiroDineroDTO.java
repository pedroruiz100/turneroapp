package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MotivoRetiroDineroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idMotivoRetiro;
    private String descripcionMotivoRetiro;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<RetiroDineroDTO> retiroDineros;

    public MotivoRetiroDineroDTO() {
    }

    public Long getIdMotivoRetiro() {
        return this.idMotivoRetiro;
    }

    public void setIdMotivoRetiro(Long idMotivoRetiro) {
        this.idMotivoRetiro = idMotivoRetiro;
    }

    public String getDescripcionMotivoRetiro() {
        return this.descripcionMotivoRetiro;
    }

    public void setDescripcionMotivoRetiro(String descripcionMotivoRetiro) {
        this.descripcionMotivoRetiro = descripcionMotivoRetiro;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<RetiroDineroDTO> getRetiroDineros() {
        if (this.retiroDineros == null) {
            this.retiroDineros = new ArrayList<RetiroDineroDTO>();
        }
        return this.retiroDineros;
    }

    public void setRetiroDineros(List<RetiroDineroDTO> retiroDineros) {
        this.retiroDineros = retiroDineros;
    }

    public RetiroDineroDTO addRetiroDinero(RetiroDineroDTO retiroDinero) {
        getRetiroDineros().add(retiroDinero);
        retiroDinero.setMotivoRetiroDinero(this);

        return retiroDinero;
    }

    public RetiroDineroDTO removeRetiroDinero(RetiroDineroDTO retiroDinero) {
        getRetiroDineros().remove(retiroDinero);
        retiroDinero.setMotivoRetiroDinero(null);

        return retiroDinero;
    }

}
