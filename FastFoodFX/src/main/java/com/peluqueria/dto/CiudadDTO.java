package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class CiudadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCiudad;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private DepartamentoDTO departamento;
    private List<BarrioDTO> barrios;
    private List<SucursalDTO> sucursals;
    private List<ClienteDTO> clientes;
    private List<FamiliaTarjDTO> familiaTarjs;
    private List<ProveedorDTO> proveedors;

    public CiudadDTO() {
    }

    public Long getIdCiudad() {
        return this.idCiudad;
    }

    public void setIdCiudad(Long idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<BarrioDTO> getBarrios() {
        if (this.barrios == null) {
            this.barrios = new ArrayList<BarrioDTO>();
        }
        return this.barrios;
    }

    public void setBarrios(List<BarrioDTO> barrios) {
        this.barrios = barrios;
    }

    public List<SucursalDTO> getSucursals() {
        if (this.sucursals == null) {
            this.sucursals = new ArrayList<SucursalDTO>();
        }
        return this.sucursals;
    }

    public void setSucursals(List<SucursalDTO> sucursals) {
        this.sucursals = sucursals;
    }

    public DepartamentoDTO getDepartamento() {
        return departamento;
    }

    public void setDepartamento(DepartamentoDTO departamentoDTO) {
        this.departamento = departamentoDTO;
    }

    public List<ClienteDTO> getClientes() {
        if (this.clientes == null) {
            this.clientes = new ArrayList<ClienteDTO>();
        }
        return clientes;
    }

    public void setClientes(List<ClienteDTO> clientes) {
        this.clientes = clientes;
    }

    public List<FamiliaTarjDTO> getFamiliaTarjs() {
        if (this.familiaTarjs == null) {
            this.familiaTarjs = new ArrayList<FamiliaTarjDTO>();
        }
        return familiaTarjs;
    }

    public void setFamiliaTarjs(List<FamiliaTarjDTO> familiaTarjs) {
        this.familiaTarjs = familiaTarjs;
    }

    public List<ProveedorDTO> getProveedors() {
        if (this.proveedors == null) {
            this.proveedors = new ArrayList<ProveedorDTO>();
        }
        return proveedors;
    }

    public void setProveedors(List<ProveedorDTO> proveedors) {
        this.proveedors = proveedors;
    }

}
