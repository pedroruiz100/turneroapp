package com.peluqueria.dto;

import java.io.Serializable;

/**
 * The persistent class for the nota_remision_det database table.
 *
 */
public class NotaRemisionDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaRemisionDet;
    private Integer cantidad;
    private String descripcion;
    private ArticuloDTO articulo;
    private NotaRemisionCabDTO notaRemisionCab;

    public NotaRemisionDetDTO() {
    }

    public Long getIdNotaRemisionDet() {
        return this.idNotaRemisionDet;
    }

    public void setIdNotaRemisionDet(Long idNotaRemisionDet) {
        this.idNotaRemisionDet = idNotaRemisionDet;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public NotaRemisionCabDTO getNotaRemisionCab() {
        return this.notaRemisionCab;
    }

    public void setNotaRemisionCab(NotaRemisionCabDTO notaRemisionCab) {
        this.notaRemisionCab = notaRemisionCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

}
