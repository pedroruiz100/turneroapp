package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaConvenioDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaConvenioDet;
    private DiaDTO dia;
    private DescuentoTarjetaConvenioCabDTO descuentoTarjetaConvenioCab;

    public Long getIdDescuentoTarjetaConvenioDet() {
        return idDescuentoTarjetaConvenioDet;
    }

    public void setIdDescuentoTarjetaConvenioDet(Long idDescuentoTarjetaConvenioDet) {
        this.idDescuentoTarjetaConvenioDet = idDescuentoTarjetaConvenioDet;
    }

    public DescuentoTarjetaConvenioCabDTO getDescuentoTarjetaConvenioCab() {
        return descuentoTarjetaConvenioCab;
    }

    public void setDescuentoTarjetaConvenioCab(
            DescuentoTarjetaConvenioCabDTO descuentoTarjetaConvenioCab) {
        this.descuentoTarjetaConvenioCab = descuentoTarjetaConvenioCab;
    }

    public DiaDTO getDia() {
        return dia;
    }

    public void setDia(DiaDTO dia) {
        this.dia = dia;
    }

}
