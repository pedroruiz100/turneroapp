package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DescuentoDirectivoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoDirectivo;
    private FacturaClienteCabDTO facturaClienteCab;
    private BigDecimal porcDesc;
    private Integer montoDesc;
    private String ciDir;
    private String nombreDir;
    private String motivoDesc;

    public DescuentoDirectivoDTO() {
        super();
    }

    public Long getIdDescuentoDirectivo() {
        return idDescuentoDirectivo;
    }

    public void setIdDescuentoDirectivo(Long idDescuentoDirectivo) {
        this.idDescuentoDirectivo = idDescuentoDirectivo;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public BigDecimal getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Integer getMontoDesc() {
        return montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public String getCiDir() {
        return ciDir;
    }

    public void setCiDir(String ciDir) {
        this.ciDir = ciDir;
    }

    public String getNombreDir() {
        return nombreDir;
    }

    public void setNombreDir(String nombreDir) {
        this.nombreDir = nombreDir;
    }

    public String getMotivoDesc() {
        return motivoDesc;
    }

    public void setMotivoDesc(String motivoDesc) {
        this.motivoDesc = motivoDesc;
    }

}
