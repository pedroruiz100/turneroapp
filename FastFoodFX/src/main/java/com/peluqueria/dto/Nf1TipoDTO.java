package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf1TipoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf1Tipo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<ArticuloNf1TipoDTO> articuloNf1Tipos;
    private List<Nf2SfamiliaDTO> nf2Sfamilias;
    private List<FuncionarioNf1DTO> funcionarioNf1s;
    private List<DescuentoFielNf1DTO> descuentoFielNf1s;
    private List<PromoTemporadaNf1DTO> promoTemporadaNf1s;

    public Nf1TipoDTO() {
    }

    public Long getIdNf1Tipo() {
        return this.idNf1Tipo;
    }

    public void setIdNf1Tipo(Long idNf1Tipo) {
        this.idNf1Tipo = idNf1Tipo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf1TipoDTO> getArticuloNf1Tipos() {
        return this.articuloNf1Tipos;
    }

    public void setArticuloNf1Tipos(List<ArticuloNf1TipoDTO> articuloNf1Tipos) {
        this.articuloNf1Tipos = articuloNf1Tipos;
    }

    public ArticuloNf1TipoDTO addArticuloNf1Tipo(ArticuloNf1TipoDTO articuloNf1Tipo) {
        getArticuloNf1Tipos().add(articuloNf1Tipo);
        articuloNf1Tipo.setNf1Tipo(this);

        return articuloNf1Tipo;
    }

    public ArticuloNf1TipoDTO removeArticuloNf1Tipo(ArticuloNf1TipoDTO articuloNf1Tipo) {
        getArticuloNf1Tipos().remove(articuloNf1Tipo);
        articuloNf1Tipo.setNf1Tipo(null);

        return articuloNf1Tipo;
    }

    public List<Nf2SfamiliaDTO> getNf2Sfamilias() {
        return this.nf2Sfamilias;
    }

    public void setNf2Sfamilias(List<Nf2SfamiliaDTO> nf2Sfamilias) {
        this.nf2Sfamilias = nf2Sfamilias;
    }

    public Nf2SfamiliaDTO addNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        getNf2Sfamilias().add(nf2Sfamilia);
        nf2Sfamilia.setNf1Tipo(this);

        return nf2Sfamilia;
    }

    public Nf2SfamiliaDTO removeNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        getNf2Sfamilias().remove(nf2Sfamilia);
        nf2Sfamilia.setNf1Tipo(null);

        return nf2Sfamilia;
    }

    public List<FuncionarioNf1DTO> getFuncionarioNf1s() {
        return this.funcionarioNf1s;
    }

    public void setFuncionarioNf1s(List<FuncionarioNf1DTO> funcionarioNf1s) {
        this.funcionarioNf1s = funcionarioNf1s;
    }

    public FuncionarioNf1DTO addFuncionarioNf1(FuncionarioNf1DTO funcionarioNf1) {
        getFuncionarioNf1s().add(funcionarioNf1);
        funcionarioNf1.setNf1Tipo(this);

        return funcionarioNf1;
    }

    public FuncionarioNf1DTO removeFuncionarioNf1(FuncionarioNf1DTO funcionarioNf1) {
        getFuncionarioNf1s().remove(funcionarioNf1);
        funcionarioNf1.setNf1Tipo(null);

        return funcionarioNf1;
    }

    public List<DescuentoFielNf1DTO> getDescuentoFielNf1s() {
        return this.descuentoFielNf1s;
    }

    public void setDescuentoFielNf1s(List<DescuentoFielNf1DTO> descuentoFielNf1s) {
        this.descuentoFielNf1s = descuentoFielNf1s;
    }

    public DescuentoFielNf1DTO addDescuentoFielNf1(DescuentoFielNf1DTO descuentoFielNf1) {
        getDescuentoFielNf1s().add(descuentoFielNf1);
        descuentoFielNf1.setNf1Tipo(this);

        return descuentoFielNf1;
    }

    public DescuentoFielNf1DTO removeDescuentoFielNf1(DescuentoFielNf1DTO descuentoFielNf1) {
        getDescuentoFielNf1s().remove(descuentoFielNf1);
        descuentoFielNf1.setNf1Tipo(null);

        return descuentoFielNf1;
    }

    public List<PromoTemporadaNf1DTO> getPromoTemporadaNf1s() {
        return this.promoTemporadaNf1s;
    }

    public void setPromoTemporadaNf1s(List<PromoTemporadaNf1DTO> promoTemporadaNf1s) {
        this.promoTemporadaNf1s = promoTemporadaNf1s;
    }

    public PromoTemporadaNf1DTO addPromoTemporadaNf1(PromoTemporadaNf1DTO promoTemporadaNf1) {
        getPromoTemporadaNf1s().add(promoTemporadaNf1);
        promoTemporadaNf1.setNf1Tipo(this);

        return promoTemporadaNf1;
    }

    public PromoTemporadaNf1DTO removePromoTemporadaNf1(PromoTemporadaNf1DTO promoTemporadaNf1) {
        getPromoTemporadaNf1s().remove(promoTemporadaNf1);
        promoTemporadaNf1.setNf1Tipo(null);

        return promoTemporadaNf1;
    }

}
