package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class FacturaClienteDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteDet;
    private BigDecimal cantidad;
    private String descripcion;
    private ArticuloDTO articulo;
    private Integer orden;
    private Boolean permiteDesc;
    private Integer poriva;
    private Long precio;
    private Boolean bajada;
    private String seccion;
    private String codVendedor;
    private String seccionSub;
    private List<DescComboDTO> descCombos;
    private FacturaClienteCabDTO facturaClienteCab;
    private Long codArticulo;

    public FacturaClienteDetDTO() {
    }

    public Long getIdFacturaClienteDet() {
        return this.idFacturaClienteDet;
    }

    public void setIdFacturaClienteDet(Long idFacturaClienteDet) {
        this.idFacturaClienteDet = idFacturaClienteDet;
    }

    public BigDecimal getCantidad() {
        return this.cantidad;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Boolean getPermiteDesc() {
        return this.permiteDesc;
    }

    public void setPermiteDesc(Boolean permiteDesc) {
        this.permiteDesc = permiteDesc;
    }

    public Integer getPoriva() {
        return this.poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public Long getPrecio() {
        return this.precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getSeccion() {
        return this.seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getSeccionSub() {
        return this.seccionSub;
    }

    public void setSeccionSub(String seccionSub) {
        this.seccionSub = seccionSub;
    }

    public List<DescComboDTO> getDescCombos() {
        if (this.descCombos == null) {
            this.descCombos = new ArrayList<DescComboDTO>();
        }
        return this.descCombos;
    }

    public void setDescCombos(List<DescComboDTO> descCombos) {
        this.descCombos = descCombos;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }

}
