package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab_tarj_Fiel database table.
 *
 */
public class FacturaClienteCabTarjFielDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabTarjFiel;
    private Integer monto;
    private FacturaClienteCabDTO facturaClienteCab;
    private List<DescTarjetaFielDTO> descTarjetaFiels;
    private TarjetaClienteFielDTO tarjetaClienteFiel;

    public FacturaClienteCabTarjFielDTO() {
    }

    public Long getIdFacturaClienteCabTarjFiel() {
        return idFacturaClienteCabTarjFiel;
    }

    public void setIdFacturaClienteCabTarjFiel(Long idFacturaClienteCabTarjFiel) {
        this.idFacturaClienteCabTarjFiel = idFacturaClienteCabTarjFiel;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public List<DescTarjetaFielDTO> getDescTarjetaFiels() {
        if (this.descTarjetaFiels == null) {
            this.descTarjetaFiels = new ArrayList<DescTarjetaFielDTO>();
        }
        return descTarjetaFiels;
    }

    public void setDescTarjetaFiels(List<DescTarjetaFielDTO> descTarjetaFiels) {
        this.descTarjetaFiels = descTarjetaFiels;
    }

    public TarjetaClienteFielDTO getTarjetaClienteFiel() {
        return tarjetaClienteFiel;
    }

    public void setTarjetaClienteFiel(TarjetaClienteFielDTO tarjetaClienteFiel) {
        this.tarjetaClienteFiel = tarjetaClienteFiel;
    }

}
