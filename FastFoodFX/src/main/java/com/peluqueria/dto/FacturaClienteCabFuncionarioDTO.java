package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab_funcionario database table.
 *
 */
public class FacturaClienteCabFuncionarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabFuncionario;
    private Integer monto;
    private FacturaClienteCabDTO facturaClienteCab;
    private List<DescFuncionarioDTO> descFuncionario;
    private FuncionarioDTO funcionario;

    public FacturaClienteCabFuncionarioDTO() {
    }

    public Long getIdFacturaClienteCabFuncionario() {
        return idFacturaClienteCabFuncionario;
    }

    public void setIdFacturaClienteCabFuncionario(
            Long idFacturaClienteCabFuncionario) {
        this.idFacturaClienteCabFuncionario = idFacturaClienteCabFuncionario;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public List<DescFuncionarioDTO> getDescFuncionario() {
        if (this.descFuncionario == null) {
            this.descFuncionario = new ArrayList<DescFuncionarioDTO>();
        }
        return descFuncionario;
    }

    public void setDescFuncionario(List<DescFuncionarioDTO> descFuncionario) {
        this.descFuncionario = descFuncionario;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

}
