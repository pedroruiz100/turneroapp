package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class SeccionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSeccion;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<SeccionSubDTO> seccionSubs;
    private List<SeccionPromoTemporadaDTO> seccionPromoTemporadaDTO;
    private List<SeccionFuncDTO> seccionFuncDTO;
    private List<DescuentoFielCabDTO> DescuentoFielCabDTO;
    private List<ArticuloDTO> articuloDTO;
    private Boolean estado;

    public SeccionDTO() {
    }

    public Long getIdSeccion() {
        return idSeccion;
    }

    public void setIdSeccion(Long idSeccion) {
        this.idSeccion = idSeccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<ArticuloDTO> getArticuloDTO() {
        if (this.articuloDTO == null) {
            this.articuloDTO = new ArrayList<ArticuloDTO>();
        }
        return articuloDTO;
    }

    public void setArticuloDTO(List<ArticuloDTO> articuloDTO) {
        this.articuloDTO = articuloDTO;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<SeccionSubDTO> getSeccionSubs() {
        if (this.seccionSubs == null) {
            this.seccionSubs = new ArrayList<SeccionSubDTO>();
        }
        return seccionSubs;
    }

    public void setSeccionSubs(List<SeccionSubDTO> seccionSubs) {
        this.seccionSubs = seccionSubs;
    }

    public List<SeccionPromoTemporadaDTO> getSeccionPromoTemporadaDTO() {
        if (this.seccionPromoTemporadaDTO == null) {
            this.seccionPromoTemporadaDTO = new ArrayList<SeccionPromoTemporadaDTO>();
        }
        return seccionPromoTemporadaDTO;
    }

    public void setSeccionPromoTemporadaDTO(
            List<SeccionPromoTemporadaDTO> seccionPromoTemporadaDTO) {
        this.seccionPromoTemporadaDTO = seccionPromoTemporadaDTO;
    }

    public List<SeccionFuncDTO> getSeccionFuncDTO() {
        if (this.seccionFuncDTO == null) {
            this.seccionFuncDTO = new ArrayList<SeccionFuncDTO>();
        }
        return seccionFuncDTO;
    }

    public void setSeccionFuncDTO(List<SeccionFuncDTO> seccionFuncDTO) {
        this.seccionFuncDTO = seccionFuncDTO;
    }

    public List<DescuentoFielCabDTO> getDescuentoFielCabDTO() {
        if (this.DescuentoFielCabDTO == null) {
            this.DescuentoFielCabDTO = new ArrayList<DescuentoFielCabDTO>();
        }
        return DescuentoFielCabDTO;
    }

    public void setDescuentoFielCabDTO(
            List<DescuentoFielCabDTO> descuentoFielCabDTO) {
        DescuentoFielCabDTO = descuentoFielCabDTO;
    }

}
