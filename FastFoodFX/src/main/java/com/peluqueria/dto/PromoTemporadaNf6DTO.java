package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf6;
    private PromoTemporadaDTO promoTemporada;
    private Nf6Secnom6DTO nf6Secnom6;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf6DTO() {
    }

    public Long getIdPromoTemporadaNf6() {
        return this.idPromoTemporadaNf6;
    }

    public void setIdPromoTemporadaNf6(Long idPromoTemporadaNf6) {
        this.idPromoTemporadaNf6 = idPromoTemporadaNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
