/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 *
 * @author ExcelsisWalker
 */
public class DescuentoTarjetaCabEntidadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCabEntidad;
    private BigDecimal porcentajeEntidad;
    private Boolean retorno;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private EntidadDTO entidad;

    public DescuentoTarjetaCabEntidadDTO() {
    }

    public Long getIdDescuentoTarjetaCabEntidad() {
        return this.idDescuentoTarjetaCabEntidad;
    }

    public void setIdDescuentoTarjetaCabEntidad(Long idDescuentoTarjetaCabEntidad) {
        this.idDescuentoTarjetaCabEntidad = idDescuentoTarjetaCabEntidad;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public EntidadDTO getEntidad() {
        return entidad;
    }

    public void setEntidad(EntidadDTO entidad) {
        this.entidad = entidad;
    }

    public BigDecimal getPorcentajeEntidad() {
        return this.porcentajeEntidad;
    }

    public void setPorcentajeEntidad(BigDecimal porcentajeEntidad) {
        this.porcentajeEntidad = porcentajeEntidad;
    }

    public Boolean getRetorno() {
        return retorno;
    }

    public void setRetorno(Boolean retorno) {
        this.retorno = retorno;
    }

}
