package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ArtPromoTemporadaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idArtPromoTemporada;
    private ArticuloDTO articulo;
    private PromoTemporadaArtDTO promoTemporadaArt;
    private BigDecimal porcentajeDesc;
    private String descriArticulo;

    public ArtPromoTemporadaDTO() {
        super();
    }

    public Long getIdArtPromoTemporada() {
        return idArtPromoTemporada;
    }

    public void setIdArtPromoTemporada(Long idArtPromoTemporada) {
        this.idArtPromoTemporada = idArtPromoTemporada;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public PromoTemporadaArtDTO getPromoTemporadaArt() {
        return promoTemporadaArt;
    }

    public void setPromoTemporadaArt(PromoTemporadaArtDTO promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getDescriArticulo() {
        return descriArticulo;
    }

    public void setDescriArticulo(String descriArticulo) {
        this.descriArticulo = descriArticulo;
    }

}
