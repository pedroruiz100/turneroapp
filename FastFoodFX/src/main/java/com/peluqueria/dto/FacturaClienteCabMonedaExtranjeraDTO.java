package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the factura_cliente_cab database table.
 *
 */
public class FacturaClienteCabMonedaExtranjeraDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabMonedaExtranjera;
    private Integer montoEfectivo;
    private Integer cotizacion;
    private Integer monto_guaranies;
    private FacturaClienteCabDTO facturaClienteCab;
    private TipoMonedaDTO tipoMoneda;

    private List<DescMonedaExtranjeraDTO> descMonedaExtranjera;

    public FacturaClienteCabMonedaExtranjeraDTO() {
    }

    public Long getIdFacturaClienteCabMonedaExtranjera() {
        return idFacturaClienteCabMonedaExtranjera;
    }

    public void setIdFacturaClienteCabMonedaExtranjera(
            Long idFacturaClienteCabMonedaExtranjera) {
        this.idFacturaClienteCabMonedaExtranjera = idFacturaClienteCabMonedaExtranjera;
    }

    public Integer getMontoEfectivo() {
        return montoEfectivo;
    }

    public void setMontoEfectivo(Integer montoEfectivo) {
        this.montoEfectivo = montoEfectivo;
    }

    public Integer getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(Integer cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Integer getMonto_guaranies() {
        return monto_guaranies;
    }

    public void setMonto_guaranies(Integer monto_guaranies) {
        this.monto_guaranies = monto_guaranies;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoMonedaDTO getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMonedaDTO tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public List<DescMonedaExtranjeraDTO> getDescMonedaExtranjera() {
        if (this.descMonedaExtranjera == null) {
            this.descMonedaExtranjera = new ArrayList<DescMonedaExtranjeraDTO>();
        }
        return descMonedaExtranjera;
    }

    public void setDescMonedaExtranjera(
            List<DescMonedaExtranjeraDTO> descMonedaExtranjera) {
        this.descMonedaExtranjera = descMonedaExtranjera;
    }

}
