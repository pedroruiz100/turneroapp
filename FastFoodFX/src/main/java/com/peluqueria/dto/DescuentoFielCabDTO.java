package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DescuentoFielCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielCab;
    private String descriSeccion;
    private Boolean estadoDesc;
    private SeccionDTO seccion;
    private BigDecimal porcentajeDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<DescuentoFielDetDTO> descuentoFielDets;
    private List<DescuentoFielNf1DTO> descuentoFielNf1s;
    private List<DescuentoFielNf2DTO> descuentoFielNf2s;
    private List<DescuentoFielNf3DTO> descuentoFielNf3s;
    private List<DescuentoFielNf4DTO> descuentoFielNf4s;
    private List<DescuentoFielNf5DTO> descuentoFielNf5s;
    private List<DescuentoFielNf6DTO> descuentoFielNf6s;
    private List<DescuentoFielNf7DTO> descuentoFielNf7s;

    public DescuentoFielCabDTO() {
    }

    public Long getIdDescuentoFielCab() {
        return idDescuentoFielCab;
    }

    public void setIdDescuentoFielCab(Long idDescuentoFielCab) {
        this.idDescuentoFielCab = idDescuentoFielCab;
    }

    public String getDescriSeccion() {
        return descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public SeccionDTO getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public List<DescuentoFielDetDTO> getDescuentoFielDets() {
        if (this.descuentoFielDets == null) {
            this.descuentoFielDets = new ArrayList<DescuentoFielDetDTO>();
        }
        return descuentoFielDets;
    }

    public void setDescuentoFielDets(List<DescuentoFielDetDTO> descuentoFielDets) {
        this.descuentoFielDets = descuentoFielDets;
    }

    public List<DescuentoFielNf1DTO> getDescuentoFielNf1s() {
        if (this.descuentoFielNf1s == null) {
            return this.descuentoFielNf1s;
        }
        return descuentoFielNf1s;
    }

    public void setDescuentoFielNf1s(List<DescuentoFielNf1DTO> descuentoFielNf1s) {
        this.descuentoFielNf1s = descuentoFielNf1s;
    }

    public List<DescuentoFielNf2DTO> getDescuentoFielNf2s() {
        if (this.descuentoFielNf2s == null) {
            return this.descuentoFielNf2s;
        }
        return descuentoFielNf2s;
    }

    public void setDescuentoFielNf2s(List<DescuentoFielNf2DTO> descuentoFielNf2s) {
        this.descuentoFielNf2s = descuentoFielNf2s;
    }

    public List<DescuentoFielNf3DTO> getDescuentoFielNf3s() {
        if (this.descuentoFielNf3s == null) {
            return this.descuentoFielNf3s;
        }
        return descuentoFielNf3s;
    }

    public void setDescuentoFielNf3s(List<DescuentoFielNf3DTO> descuentoFielNf3s) {
        this.descuentoFielNf3s = descuentoFielNf3s;
    }

    public List<DescuentoFielNf4DTO> getDescuentoFielNf4s() {
        if (this.descuentoFielNf4s == null) {
            return this.descuentoFielNf4s;
        }
        return descuentoFielNf4s;
    }

    public void setDescuentoFielNf4s(List<DescuentoFielNf4DTO> descuentoFielNf4s) {
        this.descuentoFielNf4s = descuentoFielNf4s;
    }

    public List<DescuentoFielNf5DTO> getDescuentoFielNf5s() {
        if (this.descuentoFielNf5s == null) {
            return this.descuentoFielNf5s;
        }
        return descuentoFielNf5s;
    }

    public void setDescuentoFielNf5s(List<DescuentoFielNf5DTO> descuentoFielNf5s) {
        this.descuentoFielNf5s = descuentoFielNf5s;
    }

    public List<DescuentoFielNf6DTO> getDescuentoFielNf6s() {
        if (this.descuentoFielNf6s == null) {
            return this.descuentoFielNf6s;
        }
        return descuentoFielNf6s;
    }

    public void setDescuentoFielNf6s(List<DescuentoFielNf6DTO> descuentoFielNf6s) {
        this.descuentoFielNf6s = descuentoFielNf6s;
    }

    public List<DescuentoFielNf7DTO> getDescuentoFielNf7s() {
        if (this.descuentoFielNf7s == null) {
            return this.descuentoFielNf7s;
        }
        return descuentoFielNf7s;
    }

    public void setDescuentoFielNf7s(List<DescuentoFielNf7DTO> descuentoFielNf7s) {
        this.descuentoFielNf7s = descuentoFielNf7s;
    }

}
