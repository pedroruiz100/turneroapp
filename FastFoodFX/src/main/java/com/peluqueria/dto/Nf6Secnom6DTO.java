package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf6Secnom6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf6Secnom6;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf5Seccion2DTO nf5Seccion2;
    private List<ArticuloNf6Secnom6DTO> articuloNf6Secnom6s;
    private List<Nf7Secnom7DTO> nf7Secnom7s;
    private List<FuncionarioNf6DTO> funcionarioNf6s;
    private List<DescuentoFielNf6DTO> descuentoFielNf6s;
    private List<PromoTemporadaNf6DTO> promoTemporadaNf6s;

    public Nf6Secnom6DTO() {
    }

    public Long getIdNf6Secnom6() {
        return this.idNf6Secnom6;
    }

    public void setIdNf6Secnom6(Long idNf6Secnom6) {
        this.idNf6Secnom6 = idNf6Secnom6;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf6Secnom6DTO> getArticuloNf6Secnom6s() {
        return this.articuloNf6Secnom6s;
    }

    public void setArticuloNf6Secnom6s(List<ArticuloNf6Secnom6DTO> articuloNf6Secnom6s) {
        this.articuloNf6Secnom6s = articuloNf6Secnom6s;
    }

    public ArticuloNf6Secnom6DTO addArticuloNf6Secnom6(ArticuloNf6Secnom6DTO articuloNf6Secnom6) {
        getArticuloNf6Secnom6s().add(articuloNf6Secnom6);
        articuloNf6Secnom6.setNf6Secnom6(this);

        return articuloNf6Secnom6;
    }

    public ArticuloNf6Secnom6DTO removeArticuloNf6Secnom6(ArticuloNf6Secnom6DTO articuloNf6Secnom6) {
        getArticuloNf6Secnom6s().remove(articuloNf6Secnom6);
        articuloNf6Secnom6.setNf6Secnom6(null);

        return articuloNf6Secnom6;
    }

    public Nf5Seccion2DTO getNf5Seccion2() {
        return this.nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    public List<Nf7Secnom7DTO> getNf7Secnom7s() {
        return this.nf7Secnom7s;
    }

    public void setNf7Secnom7s(List<Nf7Secnom7DTO> nf7Secnom7s) {
        this.nf7Secnom7s = nf7Secnom7s;
    }

    public Nf7Secnom7DTO addNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        getNf7Secnom7s().add(nf7Secnom7);
        nf7Secnom7.setNf6Secnom6(this);

        return nf7Secnom7;
    }

    public Nf7Secnom7DTO removeNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        getNf7Secnom7s().remove(nf7Secnom7);
        nf7Secnom7.setNf6Secnom6(null);

        return nf7Secnom7;
    }

    public List<FuncionarioNf6DTO> getFuncionarioNf6s() {
        return this.funcionarioNf6s;
    }

    public void setFuncionarioNf6s(List<FuncionarioNf6DTO> funcionarioNf6s) {
        this.funcionarioNf6s = funcionarioNf6s;
    }

    public FuncionarioNf6DTO addFuncionarioNf6(FuncionarioNf6DTO funcionarioNf6) {
        getFuncionarioNf6s().add(funcionarioNf6);
        funcionarioNf6.setNf6Secnom6(this);

        return funcionarioNf6;
    }

    public FuncionarioNf6DTO removeFuncionarioNf6(FuncionarioNf6DTO funcionarioNf6) {
        getFuncionarioNf6s().remove(funcionarioNf6);
        funcionarioNf6.setNf6Secnom6(null);

        return funcionarioNf6;
    }

    public List<DescuentoFielNf6DTO> getDescuentoFielNf6s() {
        return this.descuentoFielNf6s;
    }

    public void setDescuentoFielNf6s(List<DescuentoFielNf6DTO> descuentoFielNf6s) {
        this.descuentoFielNf6s = descuentoFielNf6s;
    }

    public DescuentoFielNf6DTO addDescuentoFielNf6(DescuentoFielNf6DTO descuentoFielNf6) {
        getDescuentoFielNf6s().add(descuentoFielNf6);
        descuentoFielNf6.setNf6Secnom6(this);

        return descuentoFielNf6;
    }

    public DescuentoFielNf6DTO removeDescuentoFielNf6(DescuentoFielNf6DTO descuentoFielNf6) {
        getDescuentoFielNf6s().remove(descuentoFielNf6);
        descuentoFielNf6.setNf6Secnom6(null);

        return descuentoFielNf6;
    }

    public List<PromoTemporadaNf6DTO> getPromoTemporadaNf6s() {
        return this.promoTemporadaNf6s;
    }

    public void setPromoTemporadaNf6s(List<PromoTemporadaNf6DTO> promoTemporadaNf6s) {
        this.promoTemporadaNf6s = promoTemporadaNf6s;
    }

    public PromoTemporadaNf6DTO addPromoTemporadaNf6(PromoTemporadaNf6DTO promoTemporadaNf6) {
        getPromoTemporadaNf6s().add(promoTemporadaNf6);
        promoTemporadaNf6.setNf6Secnom6(this);

        return promoTemporadaNf6;
    }

    public PromoTemporadaNf6DTO removePromoTemporadaNf6(PromoTemporadaNf6DTO promoTemporadaNf6) {
        getPromoTemporadaNf6s().remove(promoTemporadaNf6);
        promoTemporadaNf6.setNf6Secnom6(null);

        return promoTemporadaNf6;
    }

}
