package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the funcionario database table.
 *
 */
public class FuncionarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncionario;
    private String nombre;
    private String apellido;
    private String ci;
    private Boolean activo;
    private Boolean habilitado;
    private List<UsuarioDTO> usuario;
    private List<ServPendienteDTO> servPendiente;

    public FuncionarioDTO() {
        super();
    }

    public Long getIdFuncionario() {
        return idFuncionario;
    }

    public void setIdFuncionario(Long idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public List<UsuarioDTO> getUsuario() {
        if (this.usuario == null) {
            this.usuario = new ArrayList<UsuarioDTO>();
        }
        return usuario;
    }

    public void setUsuario(List<UsuarioDTO> usuario) {
        this.usuario = usuario;
    }

    public List<ServPendienteDTO> getServPendiente() {
        if (this.servPendiente == null) {
            this.servPendiente = new ArrayList<>();
        }
        return servPendiente;
    }

    public void setServPendiente(List<ServPendienteDTO> servPendiente) {
        this.servPendiente = servPendiente;
    }

}
