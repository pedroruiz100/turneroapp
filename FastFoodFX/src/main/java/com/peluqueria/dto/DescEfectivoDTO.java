package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class DescEfectivoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescEfectivo;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabEfectivoDTO facturaClienteCabEfectivo;

    public DescEfectivoDTO() {
        super();
    }

    public Long getIdDescEfectivo() {
        return idDescEfectivo;
    }

    public void setIdDescEfectivo(Long idDescEfectivo) {
        this.idDescEfectivo = idDescEfectivo;
    }

    public Integer getMontoDesc() {
        return montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabEfectivoDTO getFacturaClienteCabEfectivo() {
        return facturaClienteCabEfectivo;
    }

    public void setFacturaClienteCabEfectivo(
            FacturaClienteCabEfectivoDTO facturaClienteCabEfectivo) {
        this.facturaClienteCabEfectivo = facturaClienteCabEfectivo;
    }

}
