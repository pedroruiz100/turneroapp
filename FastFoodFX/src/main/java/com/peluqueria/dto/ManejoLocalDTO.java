package com.peluqueria.dto;

import java.io.Serializable;

public class ManejoLocalDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idManejo;
    private String caja;
    private String usuario;
    private String factura;

    public ManejoLocalDTO() {
    }

    public Long getIdManejo() {
        return idManejo;
    }

    public void setIdManejo(Long idManejo) {
        this.idManejo = idManejo;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

}
