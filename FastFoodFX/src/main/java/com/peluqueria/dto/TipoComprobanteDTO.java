package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the tipo_comprobante database table.
 *
 */
public class TipoComprobanteDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoComprobante;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<FacturaClienteCabDTO> facturaClienteCabs;
    private List<NotaCreditoCabDTO> notaCreditoCabs;

    public TipoComprobanteDTO() {
    }

    public Long getIdTipoComprobante() {
        return this.idTipoComprobante;
    }

    public void setIdTipoComprobante(Long idTipoComprobante) {
        this.idTipoComprobante = idTipoComprobante;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCabDTO> getFacturaClienteCabs() {
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCabDTO> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public List<NotaCreditoCabDTO> getNotaCreditoCabs() {
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCabDTO> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

}
