package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class SeccionPromoTemporadaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSeccionPromoTemporada;
    private SeccionDTO seccion;
    private PromoTemporadaDTO promoTemporada;
    private BigDecimal porcentajeDesc;
    private String descriSeccion;

    public SeccionPromoTemporadaDTO() {
        super();
    }

    public Long getIdSeccionPromoTemporada() {
        return idSeccionPromoTemporada;
    }

    public void setIdSeccionPromoTemporada(Long idSeccionPromoTemporada) {
        this.idSeccionPromoTemporada = idSeccionPromoTemporada;
    }

    public SeccionDTO getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getDescriSeccion() {
        return descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

}
