package com.peluqueria.dto;

public class FacturaClienteCabRetencionDTO {

    private Long idFacturaClienteCabRetencion;

    private FacturaClienteCabDTO facturaClienteCab;
    private Integer monto;

    public FacturaClienteCabRetencionDTO() {
        super();
    }

    public Long getIdFacturaClienteCabRetencion() {
        return idFacturaClienteCabRetencion;
    }

    public void setIdFacturaClienteCabRetencion(
            Long idFacturaClienteCabRetencion) {
        this.idFacturaClienteCabRetencion = idFacturaClienteCabRetencion;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
