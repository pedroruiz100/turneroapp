package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf6DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf6;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf6Secnom6DTO nf6Secnom6;

    public DescuentoFielNf6DTO() {
    }

    public Long getIdDescuentoFielNf6() {
        return this.idDescuentoFielNf6;
    }

    public void setIdDescuentoFielNf6(Long idDescuentoFielNf6) {
        this.idDescuentoFielNf6 = idDescuentoFielNf6;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf6Secnom6DTO getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6DTO nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

}
