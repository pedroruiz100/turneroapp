package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf1;
    private PromoTemporadaDTO promoTemporada;
    private Nf1TipoDTO nf1Tipo;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf1DTO() {
    }

    public Long getIdPromoTemporadaNf1() {
        return this.idPromoTemporadaNf1;
    }

    public void setIdPromoTemporadaNf1(Long idPromoTemporadaNf1) {
        this.idPromoTemporadaNf1 = idPromoTemporadaNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
