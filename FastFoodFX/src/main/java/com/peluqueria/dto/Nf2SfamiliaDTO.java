package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class Nf2SfamiliaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf2Sfamilia;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private Nf1TipoDTO nf1Tipo;
    private List<ArticuloNf2SfamiliaDTO> articuloNf2Sfamilias;
    private List<Nf3SseccionDTO> nf3Sseccions;
    private List<FuncionarioNf2DTO> funcionarioNf2s;
    private List<DescuentoFielNf2DTO> descuentoFielNf2s;
    private List<PromoTemporadaNf2DTO> promoTemporadaNf2s;

    public Nf2SfamiliaDTO() {
    }

    public Long getIdNf2Sfamilia() {
        return this.idNf2Sfamilia;
    }

    public void setIdNf2Sfamilia(Long idNf2Sfamilia) {
        this.idNf2Sfamilia = idNf2Sfamilia;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf2SfamiliaDTO> getArticuloNf2Sfamilias() {
        return this.articuloNf2Sfamilias;
    }

    public void setArticuloNf2Sfamilias(List<ArticuloNf2SfamiliaDTO> articuloNf2Sfamilias) {
        this.articuloNf2Sfamilias = articuloNf2Sfamilias;
    }

    public ArticuloNf2SfamiliaDTO addArticuloNf2Sfamilia(ArticuloNf2SfamiliaDTO articuloNf2Sfamilia) {
        getArticuloNf2Sfamilias().add(articuloNf2Sfamilia);
        articuloNf2Sfamilia.setNf2Sfamilia(this);

        return articuloNf2Sfamilia;
    }

    public ArticuloNf2SfamiliaDTO removeArticuloNf2Sfamilia(ArticuloNf2SfamiliaDTO articuloNf2Sfamilia) {
        getArticuloNf2Sfamilias().remove(articuloNf2Sfamilia);
        articuloNf2Sfamilia.setNf2Sfamilia(null);

        return articuloNf2Sfamilia;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return this.nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    public List<Nf3SseccionDTO> getNf3Sseccions() {
        return this.nf3Sseccions;
    }

    public void setNf3Sseccions(List<Nf3SseccionDTO> nf3Sseccions) {
        this.nf3Sseccions = nf3Sseccions;
    }

    public Nf3SseccionDTO addNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        getNf3Sseccions().add(nf3Sseccion);
        nf3Sseccion.setNf2Sfamilia(this);

        return nf3Sseccion;
    }

    public Nf3SseccionDTO removeNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        getNf3Sseccions().remove(nf3Sseccion);
        nf3Sseccion.setNf2Sfamilia(null);

        return nf3Sseccion;
    }

    public List<FuncionarioNf2DTO> getFuncionarioNf2s() {
        return this.funcionarioNf2s;
    }

    public void setFuncionarioNf2s(List<FuncionarioNf2DTO> funcionarioNf2s) {
        this.funcionarioNf2s = funcionarioNf2s;
    }

    public FuncionarioNf2DTO addFuncionarioNf2(FuncionarioNf2DTO funcionarioNf2) {
        getFuncionarioNf2s().add(funcionarioNf2);
        funcionarioNf2.setNf2Sfamilia(this);

        return funcionarioNf2;
    }

    public FuncionarioNf2DTO removeFuncionarioNf2(FuncionarioNf2DTO funcionarioNf2) {
        getFuncionarioNf2s().remove(funcionarioNf2);
        funcionarioNf2.setNf2Sfamilia(null);

        return funcionarioNf2;
    }

    public List<DescuentoFielNf2DTO> getDescuentoFielNf2s() {
        return this.descuentoFielNf2s;
    }

    public void setDescuentoFielNf2s(List<DescuentoFielNf2DTO> descuentoFielNf2s) {
        this.descuentoFielNf2s = descuentoFielNf2s;
    }

    public DescuentoFielNf2DTO addDescuentoFielNf2(DescuentoFielNf2DTO descuentoFielNf2) {
        getDescuentoFielNf2s().add(descuentoFielNf2);
        descuentoFielNf2.setNf2Sfamilia(this);

        return descuentoFielNf2;
    }

    public DescuentoFielNf2DTO removeDescuentoFielNf2(DescuentoFielNf2DTO descuentoFielNf2) {
        getDescuentoFielNf2s().remove(descuentoFielNf2);
        descuentoFielNf2.setNf2Sfamilia(null);

        return descuentoFielNf2;
    }

    public List<PromoTemporadaNf2DTO> getPromoTemporadaNf2s() {
        return this.promoTemporadaNf2s;
    }

    public void setPromoTemporadaNf2s(List<PromoTemporadaNf2DTO> promoTemporadaNf2s) {
        this.promoTemporadaNf2s = promoTemporadaNf2s;
    }

    public PromoTemporadaNf2DTO addPromoTemporadaNf2(PromoTemporadaNf2DTO promoTemporadaNf2) {
        getPromoTemporadaNf2s().add(promoTemporadaNf2);
        promoTemporadaNf2.setNf2Sfamilia(this);

        return promoTemporadaNf2;
    }

    public PromoTemporadaNf2DTO removePromoTemporadaNf2(PromoTemporadaNf2DTO promoTemporadaNf2) {
        getPromoTemporadaNf2s().remove(promoTemporadaNf2);
        promoTemporadaNf2.setNf2Sfamilia(null);

        return promoTemporadaNf2;
    }

}
