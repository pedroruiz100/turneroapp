package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DiaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDia;
    private String descripcionDia;
    private List<DescuentoFielDetDTO> descuentoFielDets;
    private List<DescuentoTarjetaConvenioDetDTO> descuentoTarjetaConvenioDet;
    private List<DescuentoTarjetaDetDTO> descuentoTarjetaDet;

    public DiaDTO() {
    }

    public List<DescuentoFielDetDTO> getDescuentoFielDets() {
        if (this.descuentoFielDets == null) {
            this.descuentoFielDets = new ArrayList<DescuentoFielDetDTO>();
        }
        return descuentoFielDets;
    }

    public void setDescuentoFielDets(List<DescuentoFielDetDTO> descuentoFielDets) {
        this.descuentoFielDets = descuentoFielDets;
    }

    public Long getIdDia() {
        return this.idDia;
    }

    public List<DescuentoTarjetaDetDTO> getDescuentoTarjetaDet() {
        if (this.descuentoTarjetaDet == null) {
            this.descuentoTarjetaDet = new ArrayList<DescuentoTarjetaDetDTO>();
        }
        return descuentoTarjetaDet;
    }

    public void setDescuentoTarjetaDet(
            List<DescuentoTarjetaDetDTO> descuentoTarjetaDet) {
        this.descuentoTarjetaDet = descuentoTarjetaDet;
    }

    public void setIdDia(Long idDia) {
        this.idDia = idDia;
    }

    public List<DescuentoTarjetaConvenioDetDTO> getDescuentoTarjetaConvenioDet() {
        if (this.descuentoTarjetaConvenioDet == null) {
            this.descuentoTarjetaConvenioDet = new ArrayList<DescuentoTarjetaConvenioDetDTO>();
        }
        return descuentoTarjetaConvenioDet;
    }

    public void setDescuentoTarjetaConvenioDet(
            List<DescuentoTarjetaConvenioDetDTO> descuentoTarjetaConvenioDet) {
        this.descuentoTarjetaConvenioDet = descuentoTarjetaConvenioDet;
    }

    public String getDescripcionDia() {
        return this.descripcionDia;
    }

    public void setDescripcionDia(String descripcionDia) {
        this.descripcionDia = descripcionDia;
    }

}
