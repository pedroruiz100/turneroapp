package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class MotivoCancelacionFacturaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idMotivoCancelFact;
    private String descripcionMotivoCancelFact;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<CancelacionFacturaDTO> cancelacionFacturas;

    public MotivoCancelacionFacturaDTO() {
    }

    public Long getIdMotivoCancelFact() {
        return this.idMotivoCancelFact;
    }

    public void setIdMotivoCancelFact(Long idMotivoCancelFact) {
        this.idMotivoCancelFact = idMotivoCancelFact;
    }

    public String getDescripcionMotivoCancelFact() {
        return this.descripcionMotivoCancelFact;
    }

    public void setDescripcionMotivoCancelFact(String descripcionMotivoCancelFact) {
        this.descripcionMotivoCancelFact = descripcionMotivoCancelFact;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CancelacionFacturaDTO> getCancelacionFacturas() {
        return this.cancelacionFacturas;
    }

    public void setCancelacionFacturas(List<CancelacionFacturaDTO> cancelacionFacturas) {
        this.cancelacionFacturas = cancelacionFacturas;
    }

    public CancelacionFacturaDTO addCancelacionFactura(CancelacionFacturaDTO cancelacionFactura) {
        getCancelacionFacturas().add(cancelacionFactura);
        cancelacionFactura.setMotivoCancelacionFactura(this);

        return cancelacionFactura;
    }

    public CancelacionFacturaDTO removeCancelacionFactura(CancelacionFacturaDTO cancelacionFactura) {
        getCancelacionFacturas().remove(cancelacionFactura);
        cancelacionFactura.setMotivoCancelacionFactura(null);

        return cancelacionFactura;
    }

}
