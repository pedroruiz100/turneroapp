/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public class EntidadDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idEntidad;
    private String descripcion;
    private List<DescuentoTarjetaCabEntidadDTO> descuentoTarjetaCabEntidads;

    public EntidadDTO() {
    }

    public Long getIdEntidad() {
        return this.idEntidad;
    }

    public void setIdEntidad(Long idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<DescuentoTarjetaCabEntidadDTO> getDescuentoTarjetaCabEntidads() {
        if (this.descuentoTarjetaCabEntidads == null) {
            this.descuentoTarjetaCabEntidads = new ArrayList<>();
        }
        return descuentoTarjetaCabEntidads;
    }

    public void setDescuentoTarjetaCabEntidads(List<DescuentoTarjetaCabEntidadDTO> descuentoTarjetaCabEntidads) {
        this.descuentoTarjetaCabEntidads = descuentoTarjetaCabEntidads;
    }
}
