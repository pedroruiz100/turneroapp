package com.peluqueria.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class TimbradoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTimbrado;
    private Date fecInicial;
    private Date fecVencimiento;
    private String nroTimbrado;
    private List<TalonariosSucursaleDTO> talonariosSucursales;

    public TimbradoDTO() {
    }

    public Long getIdTimbrado() {
        return this.idTimbrado;
    }

    public void setIdTimbrado(Long idTimbrado) {
        this.idTimbrado = idTimbrado;
    }

    public Date getFecInicial() {
        return this.fecInicial;
    }

    public void setFecInicial(Date fecInicial) {
        this.fecInicial = fecInicial;
    }

    public Date getFecVencimiento() {
        return this.fecVencimiento;
    }

    public void setFecVencimiento(Date fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public String getNroTimbrado() {
        return this.nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public List<TalonariosSucursaleDTO> getTalonariosSucursales() {
        return this.talonariosSucursales;
    }

    public void setTalonariosSucursales(List<TalonariosSucursaleDTO> talonariosSucursales) {
        this.talonariosSucursales = talonariosSucursales;
    }

    public TalonariosSucursaleDTO addTalonariosSucursale(TalonariosSucursaleDTO talonariosSucursale) {
        getTalonariosSucursales().add(talonariosSucursale);
        talonariosSucursale.setTimbrado(this);

        return talonariosSucursale;
    }

    public TalonariosSucursaleDTO removeTalonariosSucursale(TalonariosSucursaleDTO talonariosSucursale) {
        getTalonariosSucursales().remove(talonariosSucursale);
        talonariosSucursale.setTimbrado(null);

        return talonariosSucursale;
    }

}
