package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf5DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf5;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf5Seccion2DTO nf5Seccion2;

    public DescuentoFielNf5DTO() {
    }

    public Long getIdDescuentoFielNf5() {
        return this.idDescuentoFielNf5;
    }

    public void setIdDescuentoFielNf5(Long idDescuentoFielNf5) {
        this.idDescuentoFielNf5 = idDescuentoFielNf5;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf5Seccion2DTO getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2DTO nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

}
