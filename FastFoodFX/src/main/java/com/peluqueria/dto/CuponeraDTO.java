/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 *
 * @author ExcelsisWalker
 */
public class CuponeraDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idCuponera;
    private String nombre;
    private Integer montoMin;
    private Timestamp fechaFin;
    private Timestamp fechaInicio;
    private Boolean clienteFiel;
    private Boolean activo;
    private String usuAlta;
    private String usuMod;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;

    public Long getIdCuponera() {
        return idCuponera;
    }

    public void setIdCuponera(Long idCuponera) {
        this.idCuponera = idCuponera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Boolean getClienteFiel() {
        return clienteFiel;
    }

    public void setClienteFiel(Boolean clienteFiel) {
        this.clienteFiel = clienteFiel;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }
}
