package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf4DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf4;
    private PromoTemporadaDTO promoTemporada;
    private Nf4Seccion1DTO nf4Seccion1;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf4DTO() {
    }

    public Long getIdPromoTemporadaNf4() {
        return this.idPromoTemporadaNf4;
    }

    public void setIdPromoTemporadaNf4(Long idPromoTemporadaNf4) {
        this.idPromoTemporadaNf4 = idPromoTemporadaNf4;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf4Seccion1DTO getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
