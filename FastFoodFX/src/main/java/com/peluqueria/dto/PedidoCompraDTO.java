package com.peluqueria.dto;

import java.io.Serializable;
import javax.persistence.Column;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
public class PedidoCompraDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPedidoCompra;

    private PedidoCabDTO pedidoCab;

    private ArticuloDTO articulo;

    private String descripcion;

    private Long precio;

    private String descuento;

    private String cantidad;

    private String tipo;

    private String exenta;

    private String grav5;

    private String grav10;

    private Long cantCc;

    private Long cantSc;

    private Long cantSl;

    public PedidoCompraDTO() {
    }

    public Long getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(Long idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    public PedidoCabDTO getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCabDTO pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public Long getCantCc() {
        return cantCc;
    }

    public void setCantCc(Long cantCc) {
        this.cantCc = cantCc;
    }

    public Long getCantSc() {
        return cantSc;
    }

    public void setCantSc(Long cantSc) {
        this.cantSc = cantSc;
    }

    public Long getCantSl() {
        return cantSl;
    }

    public void setCantSl(Long cantSl) {
        this.cantSl = cantSl;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getExenta() {
        return exenta;
    }

    public void setExenta(String exenta) {
        this.exenta = exenta;
    }

    public String getGrav5() {
        return grav5;
    }

    public void setGrav5(String grav5) {
        this.grav5 = grav5;
    }

    public String getGrav10() {
        return grav10;
    }

    public void setGrav10(String grav10) {
        this.grav10 = grav10;
    }

}
