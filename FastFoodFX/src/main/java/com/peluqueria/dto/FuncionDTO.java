package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FuncionDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncion;
    private String descripcion;
    private String urlPantalla;
    private ModuloDTO moduloDTO;
    private List<RolFuncionDTO> rolFuncions;

    public FuncionDTO() {
    }

    public Long getIdFuncion() {
        return this.idFuncion;
    }

    public void setIdFuncion(Long idFuncion) {
        this.idFuncion = idFuncion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlPantalla() {
        return this.urlPantalla;
    }

    public void setUrlPantalla(String urlPantalla) {
        this.urlPantalla = urlPantalla;
    }

    public ModuloDTO getModulo() {
        return this.moduloDTO;
    }

    public void setModulo(ModuloDTO modulo) {
        this.moduloDTO = modulo;
    }

    public List<RolFuncionDTO> getRolFuncions() {
        if (this.rolFuncions == null) {
            this.rolFuncions = new ArrayList<RolFuncionDTO>();
        }
        return this.rolFuncions;
    }

    public void setRolFuncions(List<RolFuncionDTO> rolFuncions) {
        this.rolFuncions = rolFuncions;
    }

}
