package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

public class MotivoCancelacionProductoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idMotivoCancelProd;
    private String descripcionMotivoCancelProd;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<CancelacionProductoDTO> cancelacionProductos;

    public MotivoCancelacionProductoDTO() {
    }

    public Long getIdMotivoCancelProd() {
        return this.idMotivoCancelProd;
    }

    public void setIdMotivoCancelProd(Long idMotivoCancelProd) {
        this.idMotivoCancelProd = idMotivoCancelProd;
    }

    public String getDescripcionMotivoCancelProd() {
        return this.descripcionMotivoCancelProd;
    }

    public void setDescripcionMotivoCancelProd(String descripcionMotivoCancelProd) {
        this.descripcionMotivoCancelProd = descripcionMotivoCancelProd;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CancelacionProductoDTO> getCancelacionProductos() {
        return this.cancelacionProductos;
    }

    public void setCancelacionProductos(List<CancelacionProductoDTO> cancelacionProductos) {
        this.cancelacionProductos = cancelacionProductos;
    }

    public CancelacionProductoDTO addCancelacionProducto(CancelacionProductoDTO cancelacionProducto) {
        getCancelacionProductos().add(cancelacionProducto);
        cancelacionProducto.setMotivoCancelacionProducto(this);

        return cancelacionProducto;
    }

    public CancelacionProductoDTO removeCancelacionProducto(CancelacionProductoDTO cancelacionProducto) {
        getCancelacionProductos().remove(cancelacionProducto);
        cancelacionProducto.setMotivoCancelacionProducto(null);

        return cancelacionProducto;
    }

}
