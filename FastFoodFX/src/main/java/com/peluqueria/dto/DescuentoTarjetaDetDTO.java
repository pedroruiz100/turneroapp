package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoTarjetaDetDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaDet;
    private DiaDTO dia;
    private DescuentoTarjetaCabDTO descuentoTarjetaCab;
    private Boolean diaEspecial;

    public DescuentoTarjetaDetDTO() {
        super();
    }

    public Boolean getDiaEspecial() {
        return diaEspecial;
    }

    public void setDiaEspecial(Boolean diaEspecial) {
        this.diaEspecial = diaEspecial;
    }

    public Long getIdDescuentoTarjetaDet() {
        return idDescuentoTarjetaDet;
    }

    public void setIdDescuentoTarjetaDet(Long idDescuentoTarjetaDet) {
        this.idDescuentoTarjetaDet = idDescuentoTarjetaDet;
    }

    public DiaDTO getDia() {
        return dia;
    }

    public void setDia(DiaDTO dia) {
        this.dia = dia;
    }

    public DescuentoTarjetaCabDTO getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(
            DescuentoTarjetaCabDTO descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

}
