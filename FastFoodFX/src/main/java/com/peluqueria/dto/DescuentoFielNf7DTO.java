package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf7DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf7;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf7Secnom7DTO nf7Secnom7;

    public DescuentoFielNf7DTO() {
    }

    public Long getIdDescuentoFielNf7() {
        return this.idDescuentoFielNf7;
    }

    public void setIdDescuentoFielNf7(Long idDescuentoFielNf7) {
        this.idDescuentoFielNf7 = idDescuentoFielNf7;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf7Secnom7DTO getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7DTO nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

}
