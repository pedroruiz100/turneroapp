package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the nota_remision_cab database table.
 *
 */
public class NotaRemisionCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNotaRemisionCab;
    private String direcPuntoLlegada;
    private String direcPuntoPartida;
    private Timestamp fechaFinTraslado;
    private Timestamp fechaInicioTraslado;
    private ClienteDTO cliente;
    private SucursalDTO sucursal;
    private FacturaClienteCabDTO facturaClienteCab;
    private MotivoTrasladoDTO motivoTraslado;
    private List<NotaRemisionDetDTO> notaRemisionDets;

    public NotaRemisionCabDTO() {
    }

    public Long getIdNotaRemisionCab() {
        return this.idNotaRemisionCab;
    }

    public void setIdNotaRemisionCab(Long idNotaRemisionCab) {
        this.idNotaRemisionCab = idNotaRemisionCab;
    }

    public String getDirecPuntoLlegada() {
        return this.direcPuntoLlegada;
    }

    public void setDirecPuntoLlegada(String direcPuntoLlegada) {
        this.direcPuntoLlegada = direcPuntoLlegada;
    }

    public String getDirecPuntoPartida() {
        return this.direcPuntoPartida;
    }

    public void setDirecPuntoPartida(String direcPuntoPartida) {
        this.direcPuntoPartida = direcPuntoPartida;
    }

    public Timestamp getFechaFinTraslado() {
        return this.fechaFinTraslado;
    }

    public void setFechaFinTraslado(Timestamp fechaFinTraslado) {
        this.fechaFinTraslado = fechaFinTraslado;
    }

    public Timestamp getFechaInicioTraslado() {
        return this.fechaInicioTraslado;
    }

    public void setFechaInicioTraslado(Timestamp fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public MotivoTrasladoDTO getMotivoTraslado() {
        return this.motivoTraslado;
    }

    public void setMotivoTraslado(MotivoTrasladoDTO motivoTraslado) {
        this.motivoTraslado = motivoTraslado;
    }

    public List<NotaRemisionDetDTO> getNotaRemisionDets() {
        return this.notaRemisionDets;
    }

    public void setNotaRemisionDets(List<NotaRemisionDetDTO> notaRemisionDets) {
        this.notaRemisionDets = notaRemisionDets;
    }

    public NotaRemisionDetDTO addNotaRemisionDet(NotaRemisionDetDTO notaRemisionDet) {
        getNotaRemisionDets().add(notaRemisionDet);
        notaRemisionDet.setNotaRemisionCab(this);

        return notaRemisionDet;
    }

    public NotaRemisionDetDTO removeNotaRemisionDet(NotaRemisionDetDTO notaRemisionDet) {
        getNotaRemisionDets().remove(notaRemisionDet);
        notaRemisionDet.setNotaRemisionCab(null);

        return notaRemisionDet;
    }

    public ClienteDTO getCliente() {
        return cliente;
    }

    public void setCliente(ClienteDTO cliente) {
        this.cliente = cliente;
    }

    public SucursalDTO getSucursal() {
        return sucursal;
    }

    public void setSucursal(SucursalDTO sucursal) {
        this.sucursal = sucursal;
    }

}
