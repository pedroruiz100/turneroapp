package com.peluqueria.dto;

import com.peluqueria.core.domain.Usuario;
import static com.peluqueria.core.domain.Usuario.fromUsuarioDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idUsuario;
    private Boolean activo;
    private String contrasenha;
    private String email;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String nomUsuario;
    private String usuAlta;
    private String usuMod;
    private boolean generico;
    private FuncionarioDTO funcionario;
    private List<UsuarioRolDTO> usuarioRols;
    private List<SupervisorDTO> supervisor;
    private List<AperturaCajaDTO> aperturaCajasCajero;
    private List<AperturaCajaDTO> aperturaCajasSupervisor;
    private List<CancelacionFacturaDTO> cancelacionFacturasCajero;
    private List<CancelacionFacturaDTO> cancelacionFacturasSupervisor;
    private List<CancelacionProductoDTO> cancelacionProductosCajero;
    private List<CancelacionProductoDTO> cancelacionProductosSupervisor;
    private List<CierreCajaDTO> cierreCajasCajero;
    private List<CierreCajaDTO> cierreCajasSupervisor;
    private List<RetiroDineroDTO> retiroDinerosCajero;
    private List<RetiroDineroDTO> retiroDinerosSupervisor;

    public UsuarioDTO() {
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public FuncionarioDTO getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(FuncionarioDTO funcionario) {
        this.funcionario = funcionario;
    }

    public String getContrasenha() {
        return contrasenha;
    }

    public void setContrasenha(String contrasenha) {
        this.contrasenha = contrasenha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getNomUsuario() {
        return nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<UsuarioRolDTO> getUsuarioRols() {
        if (this.usuarioRols == null) {
            this.usuarioRols = new ArrayList<UsuarioRolDTO>();
        }
        return usuarioRols;
    }

    public void setUsuarioRols(List<UsuarioRolDTO> usuarioRols) {
        this.usuarioRols = usuarioRols;
    }

    public List<SupervisorDTO> getSupervisor() {
        if (this.supervisor == null) {
            this.supervisor = new ArrayList<SupervisorDTO>();
        }
        return supervisor;
    }

    public void setSupervisor(List<SupervisorDTO> supervisor) {
        this.supervisor = supervisor;
    }

    public List<AperturaCajaDTO> getAperturaCajasCajero() {
        if (this.aperturaCajasCajero == null) {
            this.aperturaCajasCajero = new ArrayList<AperturaCajaDTO>();
        }
        return aperturaCajasCajero;
    }

    public void setAperturaCajasCajero(List<AperturaCajaDTO> aperturaCajasCajero) {
        this.aperturaCajasCajero = aperturaCajasCajero;
    }

    public List<AperturaCajaDTO> getAperturaCajasSupervisor() {
        if (this.aperturaCajasSupervisor == null) {
            this.aperturaCajasSupervisor = new ArrayList<AperturaCajaDTO>();
        }
        return aperturaCajasSupervisor;
    }

    public void setAperturaCajasSupervisor(
            List<AperturaCajaDTO> aperturaCajasSupervisor) {
        this.aperturaCajasSupervisor = aperturaCajasSupervisor;
    }

    public List<CancelacionFacturaDTO> getCancelacionFacturasCajero() {
        if (this.cancelacionFacturasCajero == null) {
            this.cancelacionFacturasCajero = new ArrayList<CancelacionFacturaDTO>();
        }
        return cancelacionFacturasCajero;
    }

    public boolean isGenerico() {
        return generico;
    }

    public void setGenerico(boolean generico) {
        this.generico = generico;
    }

    public void setCancelacionFacturasCajero(
            List<CancelacionFacturaDTO> cancelacionFacturasCajero) {
        this.cancelacionFacturasCajero = cancelacionFacturasCajero;
    }

    public List<CancelacionFacturaDTO> getCancelacionFacturasSupervisor() {
        if (this.cancelacionFacturasSupervisor == null) {
            this.cancelacionFacturasSupervisor = new ArrayList<CancelacionFacturaDTO>();
        }
        return cancelacionFacturasSupervisor;
    }

    public void setCancelacionFacturasSupervisor(
            List<CancelacionFacturaDTO> cancelacionFacturasSupervisor) {
        this.cancelacionFacturasSupervisor = cancelacionFacturasSupervisor;
    }

    public List<CancelacionProductoDTO> getCancelacionProductosCajero() {
        if (this.cancelacionProductosCajero == null) {
            this.cancelacionProductosCajero = new ArrayList<CancelacionProductoDTO>();
        }
        return cancelacionProductosCajero;
    }

    public void setCancelacionProductosCajero(
            List<CancelacionProductoDTO> cancelacionProductosCajero) {
        this.cancelacionProductosCajero = cancelacionProductosCajero;
    }

    public List<CancelacionProductoDTO> getCancelacionProductosSupervisor() {
        if (this.cancelacionProductosSupervisor == null) {
            this.cancelacionProductosSupervisor = new ArrayList<CancelacionProductoDTO>();
        }
        return cancelacionProductosSupervisor;
    }

    public void setCancelacionProductosSupervisor(
            List<CancelacionProductoDTO> cancelacionProductosSupervisor) {
        this.cancelacionProductosSupervisor = cancelacionProductosSupervisor;
    }

    public List<CierreCajaDTO> getCierreCajasCajero() {
        if (this.cierreCajasCajero == null) {
            this.cierreCajasCajero = new ArrayList<CierreCajaDTO>();
        }
        return cierreCajasCajero;
    }

    public void setCierreCajasCajero(List<CierreCajaDTO> cierreCajasCajero) {
        this.cierreCajasCajero = cierreCajasCajero;
    }

    public List<CierreCajaDTO> getCierreCajasSupervisor() {
        if (this.cierreCajasSupervisor == null) {
            this.cierreCajasSupervisor = new ArrayList<CierreCajaDTO>();
        }
        return cierreCajasSupervisor;
    }

    public void setCierreCajasSupervisor(List<CierreCajaDTO> cierreCajasSupervisor) {
        this.cierreCajasSupervisor = cierreCajasSupervisor;
    }

    public List<RetiroDineroDTO> getRetiroDinerosCajero() {
        if (this.retiroDinerosCajero == null) {
            this.retiroDinerosCajero = new ArrayList<RetiroDineroDTO>();
        }
        return retiroDinerosCajero;
    }

    public void setRetiroDinerosCajero(List<RetiroDineroDTO> retiroDinerosCajero) {
        this.retiroDinerosCajero = retiroDinerosCajero;
    }

    public List<RetiroDineroDTO> getRetiroDinerosSupervisor() {
        if (this.retiroDinerosSupervisor == null) {
            this.retiroDinerosSupervisor = new ArrayList<RetiroDineroDTO>();
        }
        return retiroDinerosSupervisor;
    }

    public void setRetiroDinerosSupervisor(
            List<RetiroDineroDTO> retiroDinerosSupervisor) {
        this.retiroDinerosSupervisor = retiroDinerosSupervisor;
    }

    public Usuario fromUsuarioSetNullDTO() {
        Usuario usu = fromUsuarioDTO(this);
        usu.setFuncionario(null);
        usu.setSupervisor(null);
        usu.setUsuarioRols(null);
        return usu;
    }

}
