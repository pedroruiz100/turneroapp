package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DescuentoTarjetaCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaCab;
    private TarjetaDTO tarjeta;
    private String descriTarjeta;
    private Timestamp fechaInicio;
    private Timestamp fechaFin;
    private Time horaInicio;
    private Time horaFin;
    private Boolean estadoDesc;
    private String usuAlta;
    private String usuMod;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private BigDecimal porcentajeDesc;
    private BigDecimal porcentajeParana;
    private Integer montoMin;
    private Boolean extracto;
    private List<DescTarjetaDTO> descTarjetas;
    private List<DescuentoTarjetaDetDTO> descuentoTarjetaDets;
    private List<DescuentoTarjetaCabEntidadDTO> descuentoTarjetaCabEntidads;
    private List<DescuentoTarjetaCabArticuloDTO> descuentoTarjetaCabArticulos;
    private List<DescuentoTarjetaCabNf1DTO> descuentoTarjetaCabNf1s;
    private List<DescuentoTarjetaCabNf2DTO> descuentoTarjetaCabNf2s;
    private List<DescuentoTarjetaCabNf3DTO> descuentoTarjetaCabNf3s;
    private List<DescuentoTarjetaCabNf4DTO> descuentoTarjetaCabNf4s;
    private List<DescuentoTarjetaCabNf5DTO> descuentoTarjetaCabNf5s;
    private List<DescuentoTarjetaCabNf6DTO> descuentoTarjetaCabNf6s;
    private List<DescuentoTarjetaCabNf7DTO> descuentoTarjetaCabNf7s;

    public DescuentoTarjetaCabDTO() {
        super();
    }

    public Long getIdDescuentoTarjetaCab() {
        return idDescuentoTarjetaCab;
    }

    public void setIdDescuentoTarjetaCab(Long idDescuentoTarjetaCab) {
        this.idDescuentoTarjetaCab = idDescuentoTarjetaCab;
    }

    public TarjetaDTO getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(TarjetaDTO tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getDescriTarjeta() {
        return descriTarjeta;
    }

    public void setDescriTarjeta(String descriTarjeta) {
        this.descriTarjeta = descriTarjeta;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Time getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(Time horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Time getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(Time horaFin) {
        this.horaFin = horaFin;
    }

    public Boolean getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Boolean getExtracto() {
        return extracto;
    }

    public void setExtracto(Boolean extracto) {
        this.extracto = extracto;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<DescuentoTarjetaDetDTO> getDescuentoTarjetaDets() {
        if (this.descuentoTarjetaDets == null) {
            this.descuentoTarjetaDets = new ArrayList<>();
        }
        return descuentoTarjetaDets;
    }

    public void setDescuentoTarjetaDets(
            List<DescuentoTarjetaDetDTO> descuentoTarjetaDets) {
        this.descuentoTarjetaDets = descuentoTarjetaDets;
    }

    public BigDecimal getPorcentajeParana() {
        return porcentajeParana;
    }

    public void setPorcentajeParana(BigDecimal porcentajeParana) {
        this.porcentajeParana = porcentajeParana;
    }

    public Integer getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }

    public List<DescuentoTarjetaCabEntidadDTO> getDescuentoTarjetaCabEntidads() {
        if (this.descuentoTarjetaCabEntidads == null) {
            this.descuentoTarjetaCabEntidads = new ArrayList<>();
        }
        return descuentoTarjetaCabEntidads;
    }

    public void setDescuentoTarjetaCabEntidads(
            List<DescuentoTarjetaCabEntidadDTO> descuentoTarjetaCabEntidads) {
        this.descuentoTarjetaCabEntidads = descuentoTarjetaCabEntidads;
    }

    public List<DescuentoTarjetaCabArticuloDTO> getDescuentoTarjetaCabArticulos() {
        if (this.descuentoTarjetaCabArticulos == null) {
            this.descuentoTarjetaCabArticulos = new ArrayList<>();
        }
        return descuentoTarjetaCabArticulos;
    }

    public void setDescuentoTarjetaCabArticulos(
            List<DescuentoTarjetaCabArticuloDTO> descuentoTarjetaCabArticulos) {
        this.descuentoTarjetaCabArticulos = descuentoTarjetaCabArticulos;
    }

    public List<DescTarjetaDTO> getDescTarjetas() {
        return descTarjetas;
    }

    public void setDescTarjetas(List<DescTarjetaDTO> descTarjetas) {
        this.descTarjetas = descTarjetas;
    }

    public List<DescuentoTarjetaCabNf1DTO> getDescuentoTarjetaCabNf1s() {
        if (this.descuentoTarjetaCabNf1s == null) {
            this.descuentoTarjetaCabNf1s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf1s;
    }

    public void setDescuentoTarjetaCabNf1s(List<DescuentoTarjetaCabNf1DTO> descuentoTarjetaCabNf1s) {
        this.descuentoTarjetaCabNf1s = descuentoTarjetaCabNf1s;
    }

    public List<DescuentoTarjetaCabNf2DTO> getDescuentoTarjetaCabNf2s() {
        if (this.descuentoTarjetaCabNf2s == null) {
            this.descuentoTarjetaCabNf2s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf2s;
    }

    public void setDescuentoTarjetaCabNf2s(List<DescuentoTarjetaCabNf2DTO> descuentoTarjetaCabNf2s) {
        this.descuentoTarjetaCabNf2s = descuentoTarjetaCabNf2s;
    }

    public List<DescuentoTarjetaCabNf3DTO> getDescuentoTarjetaCabNf3s() {
        if (this.descuentoTarjetaCabNf3s == null) {
            this.descuentoTarjetaCabNf3s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf3s;
    }

    public void setDescuentoTarjetaCabNf3s(List<DescuentoTarjetaCabNf3DTO> descuentoTarjetaCabNf3s) {
        this.descuentoTarjetaCabNf3s = descuentoTarjetaCabNf3s;
    }

    public List<DescuentoTarjetaCabNf4DTO> getDescuentoTarjetaCabNf4s() {
        if (this.descuentoTarjetaCabNf4s == null) {
            this.descuentoTarjetaCabNf4s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf4s;
    }

    public void setDescuentoTarjetaCabNf4s(List<DescuentoTarjetaCabNf4DTO> descuentoTarjetaCabNf4s) {
        this.descuentoTarjetaCabNf4s = descuentoTarjetaCabNf4s;
    }

    public List<DescuentoTarjetaCabNf5DTO> getDescuentoTarjetaCabNf5s() {
        if (this.descuentoTarjetaCabNf5s == null) {
            this.descuentoTarjetaCabNf5s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf5s;
    }

    public void setDescuentoTarjetaCabNf5s(List<DescuentoTarjetaCabNf5DTO> descuentoTarjetaCabNf5s) {
        this.descuentoTarjetaCabNf5s = descuentoTarjetaCabNf5s;
    }

    public List<DescuentoTarjetaCabNf6DTO> getDescuentoTarjetaCabNf6s() {
        if (this.descuentoTarjetaCabNf6s == null) {
            this.descuentoTarjetaCabNf6s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf6s;
    }

    public void setDescuentoTarjetaCabNf6s(List<DescuentoTarjetaCabNf6DTO> descuentoTarjetaCabNf6s) {
        this.descuentoTarjetaCabNf6s = descuentoTarjetaCabNf6s;
    }

    public List<DescuentoTarjetaCabNf7DTO> getDescuentoTarjetaCabNf7s() {
        if (this.descuentoTarjetaCabNf7s == null) {
            this.descuentoTarjetaCabNf7s = new ArrayList<>();
        }
        return descuentoTarjetaCabNf7s;
    }

    public void setDescuentoTarjetaCabNf7s(List<DescuentoTarjetaCabNf7DTO> descuentoTarjetaCabNf7s) {
        this.descuentoTarjetaCabNf7s = descuentoTarjetaCabNf7s;
    }

}
