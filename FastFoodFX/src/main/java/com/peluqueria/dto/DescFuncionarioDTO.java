package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * The persistent class for the desc_funcionario database table.
 *
 */
public class DescFuncionarioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescFuncionario;
    private Integer montoDesc;
    private BigDecimal porcentajeDesc;
    private FacturaClienteCabFuncionarioDTO facturaClienteCabFuncionarioDTO;

    public DescFuncionarioDTO() {
    }

    public Long getIdDescFuncionario() {
        return this.idDescFuncionario;
    }

    public void setIdDescFuncionario(Long idDescFuncionario) {
        this.idDescFuncionario = idDescFuncionario;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabFuncionarioDTO getFacturaClienteCabFuncionarioDTO() {
        return facturaClienteCabFuncionarioDTO;
    }

    public void setFacturaClienteCabFuncionarioDTO(
            FacturaClienteCabFuncionarioDTO facturaClienteCabFuncionarioDTO) {
        this.facturaClienteCabFuncionarioDTO = facturaClienteCabFuncionarioDTO;
    }

}
