package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf2DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf2;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf2SfamiliaDTO nf2Sfamilia;

    public DescuentoFielNf2DTO() {
    }

    public Long getIdDescuentoFielNf2() {
        return this.idDescuentoFielNf2;
    }

    public void setIdDescuentoFielNf2(Long idDescuentoFielNf2) {
        this.idDescuentoFielNf2 = idDescuentoFielNf2;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf2SfamiliaDTO getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2SfamiliaDTO nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

}
