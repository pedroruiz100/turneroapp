package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

/**
 * The persistent class for the seccion_func database table.
 *
 */
public class SeccionFuncDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idSeccionFunc;
    private Boolean estadoDesc;
    private SeccionDTO seccion;
    private BigDecimal porcDesc;
    private String descriSeccion;
    private String usuMod;
    private Timestamp fechaMod;
    private String usuAlta;
    private Timestamp fechaAlta;

    public SeccionFuncDTO() {
    }

    public Long getIdSeccionFunc() {
        return this.idSeccionFunc;
    }

    public void setIdSeccionFunc(Long idSeccionFunc) {
        this.idSeccionFunc = idSeccionFunc;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public SeccionDTO getSeccion() {
        return seccion;
    }

    public void setSeccion(SeccionDTO seccion) {
        this.seccion = seccion;
    }

    public String getDescriSeccion() {
        return descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

}
