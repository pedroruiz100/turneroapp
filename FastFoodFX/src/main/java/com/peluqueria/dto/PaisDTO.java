package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class PaisDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPais;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<DepartamentoDTO> departamentosDTO;
    private List<ClienteDTO> clienteDTO;
    private List<ProveedorDTO> proveedorDTO;

    public PaisDTO() {
    }

    public Long getIdPais() {
        return idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DepartamentoDTO> getDepartamentosDTO() {
        if (this.departamentosDTO == null) {
            this.departamentosDTO = new ArrayList<DepartamentoDTO>();
        }
        return departamentosDTO;
    }

    public void setDepartamentosDTO(List<DepartamentoDTO> departamentosDTO) {
        this.departamentosDTO = departamentosDTO;
    }

    public List<ClienteDTO> getClienteDTO() {
        if (this.clienteDTO == null) {
            this.clienteDTO = new ArrayList<ClienteDTO>();
        }
        return clienteDTO;
    }

    public void setClienteDTO(List<ClienteDTO> clienteDTO) {
        this.clienteDTO = clienteDTO;
    }

    public List<ProveedorDTO> getProveedorDTO() {
        if (this.proveedorDTO == null) {
            this.proveedorDTO = new ArrayList<ProveedorDTO>();
        }
        return proveedorDTO;
    }

    public void setProveedorDTO(List<ProveedorDTO> proveedorDTO) {
        this.proveedorDTO = proveedorDTO;
    }

}
