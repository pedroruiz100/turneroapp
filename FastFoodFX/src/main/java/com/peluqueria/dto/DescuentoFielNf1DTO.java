package com.peluqueria.dto;

import java.io.Serializable;

public class DescuentoFielNf1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoFielNf1;
    private DescuentoFielCabDTO descuentoFielCab;
    private Nf1TipoDTO nf1Tipo;

    public DescuentoFielNf1DTO() {
    }

    public Long getIdDescuentoFielNf1() {
        return this.idDescuentoFielNf1;
    }

    public void setIdDescuentoFielNf1(Long idDescuentoFielNf1) {
        this.idDescuentoFielNf1 = idDescuentoFielNf1;
    }

    public DescuentoFielCabDTO getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCabDTO descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

}
