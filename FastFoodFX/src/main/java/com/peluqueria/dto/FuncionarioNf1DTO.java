package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class FuncionarioNf1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFuncionarioNf1;
    private String descriSeccion;
    private Boolean estadoDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private Nf1TipoDTO nf1Tipo;
    private BigDecimal porcDesc;
    private String usuAlta;
    private String usuMod;

    public FuncionarioNf1DTO() {
    }

    public Long getIdFuncionarioNf1() {
        return this.idFuncionarioNf1;
    }

    public void setIdFuncionarioNf1(Long idFuncionarioNf1) {
        this.idFuncionarioNf1 = idFuncionarioNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf1TipoDTO getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1TipoDTO nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

}
