package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FacturaClienteCabEfectivoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabEfectivo;
    private Integer montoEfectivo;
    private FacturaClienteCabDTO facturaClienteCab;
    private List<DescEfectivoDTO> descEfectivo;

    public FacturaClienteCabEfectivoDTO() {
        super();
    }

    public Long getIdFacturaClienteCabEfectivo() {
        return idFacturaClienteCabEfectivo;
    }

    public void setIdFacturaClienteCabEfectivo(Long idFacturaClienteCabEfectivo) {
        this.idFacturaClienteCabEfectivo = idFacturaClienteCabEfectivo;
    }

    public Integer getMontoEfectivo() {
        return montoEfectivo;
    }

    public void setMontoEfectivo(Integer montoEfectivo) {
        this.montoEfectivo = montoEfectivo;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public List<DescEfectivoDTO> getDescEfectivo() {
        if (this.descEfectivo == null) {
            this.descEfectivo = new ArrayList<DescEfectivoDTO>();
        }
        return descEfectivo;
    }

    public void setDescEfectivo(List<DescEfectivoDTO> descEfectivo) {
        this.descEfectivo = descEfectivo;
    }

}
