package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PromoTemporadaNf3DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idPromoTemporadaNf3;
    private PromoTemporadaDTO promoTemporada;
    private Nf3SseccionDTO nf3Sseccion;
    private String descriSeccion;
    private BigDecimal porcentajeDesc;

    public PromoTemporadaNf3DTO() {
    }

    public Long getIdPromoTemporadaNf3() {
        return this.idPromoTemporadaNf3;
    }

    public void setIdPromoTemporadaNf3(Long idPromoTemporadaNf3) {
        this.idPromoTemporadaNf3 = idPromoTemporadaNf3;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporadaDTO getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporadaDTO promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf3SseccionDTO getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3SseccionDTO nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

}
