package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class BarrioDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idBarrio;
    private Boolean activo;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private CiudadDTO ciudadDTO;
    private List<SucursalDTO> sucursalsDTOs;
    private List<ClienteDTO> clientesDTO;
    private List<FamiliaTarjDTO> familiaTarjDTO;
    private List<ProveedorDTO> proveedorDTO;

    public BarrioDTO(Long idBarrio, Boolean activo, String descripcion,
            Timestamp fechaAlta, Timestamp fechaMod, String usuAlta,
            String usuMod, CiudadDTO ciudadDTO,
            List<SucursalDTO> sucursalsDTOs, List<ClienteDTO> clientesDTO,
            List<FamiliaTarjDTO> familiaTarjDTO, List<ProveedorDTO> proveedorDTO) {
        super();
        this.idBarrio = idBarrio;
        this.activo = activo;
        this.descripcion = descripcion;
        this.fechaAlta = fechaAlta;
        this.fechaMod = fechaMod;
        this.usuAlta = usuAlta;
        this.usuMod = usuMod;
        this.ciudadDTO = ciudadDTO;
        this.sucursalsDTOs = sucursalsDTOs;
        this.clientesDTO = clientesDTO;
        this.familiaTarjDTO = familiaTarjDTO;
        this.proveedorDTO = proveedorDTO;
    }

    public BarrioDTO() {
        super();
    }

    public Long getIdBarrio() {
        return idBarrio;
    }

    public void setIdBarrio(Long idBarrio) {
        this.idBarrio = idBarrio;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public CiudadDTO getCiudadDTO() {
        return ciudadDTO;
    }

    public void setCiudadDTO(CiudadDTO ciudadDTO) {
        this.ciudadDTO = ciudadDTO;
    }

    public List<SucursalDTO> getSucursalsDTOs() {
        if (this.sucursalsDTOs == null) {
            this.sucursalsDTOs = new ArrayList<SucursalDTO>();
        }
        return sucursalsDTOs;
    }

    public void setSucursalsDTOs(List<SucursalDTO> sucursalsDTOs) {
        this.sucursalsDTOs = sucursalsDTOs;
    }

    public List<ClienteDTO> getClientesDTO() {
        if (this.clientesDTO == null) {
            this.clientesDTO = new ArrayList<ClienteDTO>();
        }
        return clientesDTO;
    }

    public void setClientesDTO(List<ClienteDTO> clientesDTO) {
        this.clientesDTO = clientesDTO;
    }

    public List<FamiliaTarjDTO> getFamiliaTarjDTO() {
        if (this.familiaTarjDTO == null) {
            this.familiaTarjDTO = new ArrayList<FamiliaTarjDTO>();
        }
        return familiaTarjDTO;
    }

    public void setFamiliaTarjDTO(List<FamiliaTarjDTO> familiaTarjDTO) {
        this.familiaTarjDTO = familiaTarjDTO;
    }

    public List<ProveedorDTO> getProveedorDTO() {
        if (this.proveedorDTO == null) {
            this.proveedorDTO = new ArrayList<ProveedorDTO>();
        }
        return proveedorDTO;
    }

    public void setProveedorDTO(List<ProveedorDTO> proveedorDTO) {
        this.proveedorDTO = proveedorDTO;
    }

}
