package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class ArticuloNf4Seccion1DTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idNf4Seccion1Articulo;
    private Timestamp fechaAlta;
    private ArticuloDTO articulo;
    private String usuAlta;
    private Nf4Seccion1DTO nf4Seccion1;

    public ArticuloNf4Seccion1DTO() {
    }

    public Long getIdNf4Seccion1Articulo() {
        return this.idNf4Seccion1Articulo;
    }

    public void setIdNf4Seccion1Articulo(Long idNf4Seccion1Articulo) {
        this.idNf4Seccion1Articulo = idNf4Seccion1Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public ArticuloDTO getArticulo() {
        return articulo;
    }

    public void setArticulo(ArticuloDTO articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf4Seccion1DTO getNf4Seccion1() {
        return this.nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1DTO nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

}
