package com.peluqueria.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the tipo_pago database table.
 *
 */
public class TipoPagoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoPago;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private List<FacturaClienteCabTipoPagoDTO> facturaClienteCabTipoPagos;

    public TipoPagoDTO() {
    }

    public Long getIdTipoPago() {
        return this.idTipoPago;
    }

    public void setIdTipoPago(Long idTipoPago) {
        this.idTipoPago = idTipoPago;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCabTipoPagoDTO> getFacturaClienteCabTipoPagos() {
        return this.facturaClienteCabTipoPagos;
    }

    public void setFacturaClienteCabTipoPagos(List<FacturaClienteCabTipoPagoDTO> facturaClienteCabTipoPagos) {
        this.facturaClienteCabTipoPagos = facturaClienteCabTipoPagos;
    }

    public FacturaClienteCabTipoPagoDTO addFacturaClienteCabTipoPago(
            FacturaClienteCabTipoPagoDTO facturaClienteCabTipoPago) {
        getFacturaClienteCabTipoPagos().add(facturaClienteCabTipoPago);
        facturaClienteCabTipoPago.setTipoPago(this);

        return facturaClienteCabTipoPago;
    }

    public FacturaClienteCabTipoPagoDTO removeFacturaClienteCabTipoPago(
            FacturaClienteCabTipoPagoDTO facturaClienteCabTipoPago) {
        getFacturaClienteCabTipoPagos().remove(facturaClienteCabTipoPago);
        facturaClienteCabTipoPago.setTipoPago(null);

        return facturaClienteCabTipoPago;
    }

}
