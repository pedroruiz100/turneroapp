package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class DescuentoTarjetaConvenioCabDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idDescuentoTarjetaConvenioCab;
    private String descriTarjetaConvenio;
    private Boolean estadoDesc;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private TarjetaConvenioDTO tarjetaConvenio;
    private BigDecimal porcentajeDesc;
    private String usuAlta;
    private String usuMod;
    private List<DescuentoTarjetaConvenioDetDTO> descuentoTarjetaConvenioDetDTO;

    public DescuentoTarjetaConvenioCabDTO() {
        super();
    }

    public Long getIdDescuentoTarjetaConvenioCab() {
        return idDescuentoTarjetaConvenioCab;
    }

    public void setIdDescuentoTarjetaConvenioCab(
            Long idDescuentoTarjetaConvenioCab) {
        this.idDescuentoTarjetaConvenioCab = idDescuentoTarjetaConvenioCab;
    }

    public String getDescriTarjetaConvenio() {
        return descriTarjetaConvenio;
    }

    public void setDescriTarjetaConvenio(String descriTarjetaConvenio) {
        this.descriTarjetaConvenio = descriTarjetaConvenio;
    }

    public Boolean getEstadoDesc() {
        return estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public TarjetaConvenioDTO getTarjetaConvenio() {
        return tarjetaConvenio;
    }

    public void setTarjetaConvenio(TarjetaConvenioDTO tarjetaConvenio) {
        this.tarjetaConvenio = tarjetaConvenio;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DescuentoTarjetaConvenioDetDTO> getDescuentoTarjetaConvenioDetDTO() {
        if (this.descuentoTarjetaConvenioDetDTO == null) {
            this.descuentoTarjetaConvenioDetDTO = new ArrayList<DescuentoTarjetaConvenioDetDTO>();
        }
        return descuentoTarjetaConvenioDetDTO;
    }

    public void setDescuentoTarjetaConvenioDetDTO(
            List<DescuentoTarjetaConvenioDetDTO> descuentoTarjetaConvenioDetDTO) {
        this.descuentoTarjetaConvenioDetDTO = descuentoTarjetaConvenioDetDTO;
    }

}
