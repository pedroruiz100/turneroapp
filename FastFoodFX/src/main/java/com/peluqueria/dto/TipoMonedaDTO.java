package com.peluqueria.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the tipo_moneda database table.
 *
 */
public class TipoMonedaDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idTipoMoneda;
    private BigDecimal compra;
    private String descripcion;
    private Timestamp fechaAlta;
    private Timestamp fechaMod;
    private String usuAlta;
    private String usuMod;
    private BigDecimal venta;
    private List<FacturaClienteCabDTO> facturaClienteCabs;
    private List<NotaCreditoCabDTO> notaCreditoCabs;
    private List<FacturaClienteCabMonedaExtranjeraDTO> facturaClienteCabMonedaExtranjera;

    public TipoMonedaDTO() {
    }

    public Long getIdTipoMoneda() {
        return this.idTipoMoneda;
    }

    public void setIdTipoMoneda(Long idTipoMoneda) {
        this.idTipoMoneda = idTipoMoneda;
    }

    public BigDecimal getCompra() {
        return this.compra;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public BigDecimal getVenta() {
        return this.venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public List<FacturaClienteCabDTO> getFacturaClienteCabs() {
        if (this.facturaClienteCabs == null) {
            this.facturaClienteCabs = new ArrayList<FacturaClienteCabDTO>();
        }
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(
            List<FacturaClienteCabDTO> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public List<NotaCreditoCabDTO> getNotaCreditoCabs() {
        if (this.notaCreditoCabs == null) {
            this.notaCreditoCabs = new ArrayList<NotaCreditoCabDTO>();
        }
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCabDTO> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public List<FacturaClienteCabMonedaExtranjeraDTO> getFacturaClienteCabMonedaExtranjera() {
        if (this.facturaClienteCabMonedaExtranjera == null) {
            this.facturaClienteCabMonedaExtranjera = new ArrayList<FacturaClienteCabMonedaExtranjeraDTO>();
        }
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            List<FacturaClienteCabMonedaExtranjeraDTO> facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

}
