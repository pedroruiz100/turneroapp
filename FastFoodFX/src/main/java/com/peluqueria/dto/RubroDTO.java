package com.peluqueria.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The persistent class for the rubro database table.
 *
 */
public class RubroDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idRubro;
    private String descripcion;
    private List<ProveedorRubroDTO> proveedorRubros;

    public RubroDTO() {
    }

    public Long getIdRubro() {
        return this.idRubro;
    }

    public void setIdRubro(Long idRubro) {
        this.idRubro = idRubro;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ProveedorRubroDTO> getProveedorRubros() {
        if (this.proveedorRubros == null) {
            this.proveedorRubros = new ArrayList<ProveedorRubroDTO>();
        }
        return this.proveedorRubros;
    }

    public void setProveedorRubros(List<ProveedorRubroDTO> proveedorRubros) {
        this.proveedorRubros = proveedorRubros;
    }

    public ProveedorRubroDTO addProveedorRubro(ProveedorRubroDTO proveedorRubro) {
        getProveedorRubros().add(proveedorRubro);
        proveedorRubro.setRubro(this);

        return proveedorRubro;
    }

    public ProveedorRubroDTO removeProveedorRubro(
            ProveedorRubroDTO proveedorRubro) {
        getProveedorRubros().remove(proveedorRubro);
        proveedorRubro.setRubro(null);

        return proveedorRubro;
    }

}
