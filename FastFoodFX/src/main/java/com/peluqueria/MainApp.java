package com.peluqueria;

import com.javafx.configuracion.AppContextConfig;
import com.javafx.controllers.login.LoginFXMLController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.CheckPort;
import com.javafx.util.UpPort;
import com.javafx.util.Utilidades;
import static com.javafx.util.Utilidades.DESARROLLO;
import com.javafx.util.VerificandoInstancias;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.IOException;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.stage.Stage;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class MainApp extends Application implements Runnable {

    @Override
    @SuppressWarnings({"ResultOfObjectAllocationIgnored", "CallToThreadRun"})
    public void start(Stage stage) throws Exception {
        if (LoginFXMLController.isLlamarTask()) {
            AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(AppContextConfig.class);
            ScreensContoller bean = context.getBean(ScreensContoller.class);
            bean.init(stage);
            bean.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/login/LoginFXML.fxml", 599, 245, true);
        }
    }

    public static void main(String[] args) throws IOException {
        //EJEMPLO 2
        VerificandoInstancias vi = new VerificandoInstancias();
        if (DESARROLLO) {
            vi.onFinish();
        }
        Utilidades.adaptandoAnchor();
        adaptandoAnchor();
//        System.exit(0);
        if (vi.isRunning()) {
            System.out.println("AGUARDE UN MOMENTO.. YA SE HA INICIADO UNA INSTANCIA..");
            System.exit(0);
        } else {
            vi.onStart();

            //NUEVO CODIGO
//            JSONParser parser = new JSONParser();
//            Timer timer = new Timer();
//            TimerTask tt = new TimerTask() {
//                public void run() {
//                    Calendar cal = Calendar.getInstance(); //this is the method you should use, not the Date(), because it is desperated.
//
//                    int hour = cal.get(Calendar.HOUR_OF_DAY);//gete hour number of the day, from 0 to 23
//
//                    if (hour >= 9) {
//                        Map mapeo = consultarPendientes();
//                        List<JSONObject> listJSON;
//                        try {
//                            if (mapeo.size() > 0) {
//                                listJSON = (ArrayList<JSONObject>) parser.parse(mapeo.get("lista").toString());
//                                if (listJSON.size() > 0) {
//                                    for (JSONObject json : listJSON) {
//                                        actualizarArticulosPendientes(json);
//                                        eliminarDatos(mapeo.get("id").toString());
//                                    }
//                                }
//                            }
//                        } catch (ParseException ex) {
//                            System.out.println("-->> " + ex.getLocalizedMessage());
//                        } finally {
//                        }
//                    }
//                }
//
//                private Map consultarPendientes() {
//                    ConexionPostgres.conectarLocal();
//                    HashMap mapeo = new HashMap();
//                    Calendar today = Calendar.getInstance();
//                    List<JSONObject> listJSON = new ArrayList<>();
//                    today.add(Calendar.DATE, -1);
//                    java.sql.Date yesterday = new java.sql.Date(today.getTimeInMillis());
//                    String sql = "SELECT * FROM general.articulo_suba WHERE DATE(fecha)='" + yesterday + "'";
//                    try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
//                        ResultSet rs = ps.executeQuery();
//                        while (rs.next()) {
//                            listJSON.add((JSONObject) parser.parse(rs.getString("tabla")));
//                            mapeo.put("lista", listJSON);
//                            mapeo.put("id", rs.getLong("id_dato"));
//                        }
//                        ps.close();
//                    } catch (SQLException ex) {
//                        Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//                    } catch (ParseException ex) {
//                        Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//                    } finally {
//                    }
//                    ConexionPostgres.cerrarLocal();
//                    return mapeo;
//                }
//
//                private void actualizarArticulosPendientes(JSONObject json) {
//                    ConexionPostgres.conectarLocal();
////        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + datos.get("uuidCassandraActual").toString();
//                    String sql = "UPDATE stock.articulo SET precio_may=" + json.get("precioMay").toString() + ", precio_min=" + json.get("precioMin").toString() + " WHERE cod_articulo=" + json.get("codArticulo").toString();
//                    System.out.println("-->> " + sql);
//                    try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
//                        int op = ps.executeUpdate();
//                        if (op >= 1) {
//                            System.out.println("******* Haz actualizado un registro PARANA LOCAL ********");
//                        }
//                        ps.close();
//                        ConexionPostgres.getConLocal().commit();
//                    } catch (SQLException ex) {
//                        Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
//                        try {
//                            ConexionPostgres.getConLocal().rollback();
//                        } catch (SQLException ex1) {
//                            Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
//                        } finally {
//                        }
//                    } finally {
//                    }
//                }
//
//                private void eliminarDatos(String idDato) {
//                    ConexionPostgres.conectarLocal();
////        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + datos.get("uuidCassandraActual").toString();
//                    String sql = "DELETE FROM general.articulo_suba WHERE id_dato=" + idDato;
//                    System.out.println("-->> " + sql);
//                    try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
//                        int op = ps.executeUpdate();
//                        if (op >= 1) {
//                            System.out.println("******* Haz actualizado un registro PARANA LOCAL ********");
//                        }
//                        ps.close();
//                        ConexionPostgres.getConLocal().commit();
//                    } catch (SQLException ex) {
//                        Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
//                        try {
//                            ConexionPostgres.getConLocal().rollback();
//                        } catch (SQLException ex1) {
//                            Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
//                        } finally {
//                        }
//                    } finally {
//                    }
//                }
//
//            };
//            timer.schedule(tt, 1000, 1000 * 5);
            //FIN DEL NUEVO CODIGO

            (new Thread(new MainApp())).start();
            LoginFXMLController.setLlamarTask(true);
            launch(args);
        }
        //FIN DE EJEMPLO 2
//        ORIGINAL 1
//        if (!CheckPort.go()) {
//            (new Thread(new MainApp())).start();
//            LoginFXMLController.setLlamarTask(true);
//            launch(args);
//        } else {
//            Utilidades.log.info("-->> AGUARDE UN MOMENTO.. YA SE HA INICIADO UNA INSTANCIA..");
//            System.exit(0);
//        }
//        FIN DE ORIGINAL 1
    }

    @Override
    public void run() {
        if (!CheckPort.go()) {
            UpPort.go();
        }
    }

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    private static int width;
    private static int height;

    public static void adaptandoAnchor() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        width = gd.getDisplayMode().getWidth();
        height = gd.getDisplayMode().getHeight();
    }

//    private boolean guardando() {
//        FileChooser fileChooser = new FileChooser();
//        fileChooser.setTitle("Save File");
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF Document", Arrays.asList("*.pdf", "*.PDF")));
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG image", Arrays.asList("*.png", "*.PNG")));
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("DOCX Document", Arrays.asList("*.docx", "*.DOCX")));
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XLSX Document", Arrays.asList("*.xlsx", "*.XLSX")));
//        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("HTML Document", Arrays.asList("*.html", "*.HTML")));
//        File file = fileChooser.showSaveDialog(null);
//        if (fileChooser.getSelectedExtensionFilter() != null && fileChooser.getSelectedExtensionFilter().getExtensions() != null) {
//            List<String> selectedExtension = fileChooser.getSelectedExtensionFilter().getExtensions();
//            if (selectedExtension.contains("*.pdf")) {
//                try {
//                    JasperExportManager.exportReportToPdfFile(jasperPrint, file.getAbsolutePath());
//                } catch (JRException ex) {
//                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
//                }
//            } else if (selectedExtension.contains("*.png")) {
//                for (int i = 0; i < jasperPrint.getPages().size(); i++) {
//                    String fileNumber = "0000" + Integer.toString(i + 1);
//                    fileNumber = fileNumber.substring(fileNumber.length() - 4, fileNumber.length());
//                    WritableImage image = getImage(i);
//                    String[] fileTokens = file.getAbsolutePath().split("\\.");
//                    String filename = "";
//                    //add number to filename
//                    if (fileTokens.length > 0) {
//                        for (int i2 = 0; i2 < fileTokens.length - 1; i2++) {
//                            filename = filename + fileTokens[i2] + ((i2 < fileTokens.length - 2) ? "." : "");
//                        }
//                        filename = filename + fileNumber + "." + fileTokens[fileTokens.length - 1];
//                    } else {
//                        filename = file.getAbsolutePath() + fileNumber;
//                    }
//                    System.out.println(filename);
//                    File imageFile = new File(filename);
//                    try {
//                        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", imageFile);
//                        System.out.println(imageFile.getAbsolutePath());
//                    } catch (IOException ex) {
//                        TransactionResult t = new TransactionResult();
//                        t.setResultNumber(-1);
//                        t.setResult("Error Salvando Reporte");
//                        t.setResultDescription(ex.getMessage());
//                        setTransactionResult(t);
//                        Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
//                    }
//                }
//            } else if (selectedExtension.contains("*.html")) {
//                try {
//                    JasperExportManager.exportReportToHtmlFile(jasperPrint, file.getAbsolutePath());
//                } catch (JRException ex) {
//                    TransactionResult t = new TransactionResult();
//                    t.setResultNumber(-1);
//                    t.setResult("Error Salvando Reporte");
//                    t.setResultDescription(ex.getMessage());
//                    setTransactionResult(t);
//                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
//                }
//            } else if (selectedExtension.contains("*.docx")) {
//                JRDocxExporter exporter = new JRDocxExporter();
//                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
//                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file.getAbsolutePath());
//                try {
//                    exporter.exportReport();
//                } catch (JRException ex) {
//                    TransactionResult t = new TransactionResult();
//                    t.setResultNumber(-1);
//                    t.setResult("Error Salvando Reporte");
//                    t.setResultDescription(ex.getMessage());
//                    setTransactionResult(t);
//                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
//                }
//                System.out.println("docx");
//            } else if (selectedExtension.contains("*.xlsx")) {
//                JRXlsxExporter exporter = new JRXlsxExporter();
//                exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
//                exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, file.getAbsolutePath());
//                try {
//                    exporter.exportReport();
//                } catch (JRException ex) {
//                    TransactionResult t = new TransactionResult();
//                    t.setResultNumber(-1);
//                    t.setResult("Error Salvando Reporte");
//                    t.setResultDescription(ex.getMessage());
//                    setTransactionResult(t);
//                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
//                }
//                System.out.println("xlsx");
//            }
//        }
//        return false;
//    }
}
