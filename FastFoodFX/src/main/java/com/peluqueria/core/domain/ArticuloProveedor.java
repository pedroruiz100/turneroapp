package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloProveedorDTO;

/**
 * The persistent class for the articulo_proveedor database table.
 *
 */
@Entity
@Table(name = "articulo_proveedor", schema = "stock")
@NamedQuery(name = "ArticuloProveedor.findAll", query = "SELECT a FROM ArticuloProveedor a")
public class ArticuloProveedor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_proveedor")
    private Long idArticuloProveedor;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "fecha_recep")
    private Timestamp fechaRecep;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Proveedor
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    public ArticuloProveedor() {
    }

    public Long getIdArticuloProveedor() {
        return this.idArticuloProveedor;
    }

    public void setIdArticuloProveedor(Long idArticuloProveedor) {
        this.idArticuloProveedor = idArticuloProveedor;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Timestamp getFechaRecep() {
        return this.fechaRecep;
    }

    public void setFechaRecep(Timestamp fechaRecep) {
        this.fechaRecep = fechaRecep;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Articulo getArticulo() {
        return this.articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public ArticuloProveedorDTO toArticuloProveedorDTO() {
        ArticuloProveedorDTO arDTO = toArticuloProveedorDTO(this);
        return arDTO;

    }

    public static ArticuloProveedorDTO toArticuloProveedorDTO(
            ArticuloProveedor articuloProveedor) {
        ArticuloProveedorDTO arDTO = new ArticuloProveedorDTO();
        BeanUtils.copyProperties(articuloProveedor, arDTO);
        return arDTO;
    }

    public ArticuloProveedorDTO toArticuloProveedorDTOs() {
        ArticuloProveedorDTO arDTO = toArticuloProveedorDTOs(this);
        if (this.proveedor != null) {
            arDTO.setProveedor(this.getProveedor().toProveedorBdDTO());
        }
        if (this.articulo != null) {
            arDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return arDTO;
    }

    public static ArticuloProveedorDTO toArticuloProveedorDTOs(
            ArticuloProveedor articuloProveedor) {
        ArticuloProveedorDTO arDTO = new ArticuloProveedorDTO();
        BeanUtils.copyProperties(articuloProveedor, arDTO);
        return arDTO;
    }

    public static ArticuloProveedor fromArticuloProveedorDTO(
            ArticuloProveedorDTO arDTO) {
        ArticuloProveedor art = new ArticuloProveedor();
        BeanUtils.copyProperties(arDTO, art);
        return art;
    }

    public static ArticuloProveedor fromArticuloProveedorAsociadoDTO(
            ArticuloProveedorDTO artDTO) {
        ArticuloProveedor art = fromArticuloProveedorDTO(artDTO);
        art.setProveedor(Proveedor.fromProveedorDTO(artDTO.getProveedor()));
        art.setArticulo(Articulo.fromArticuloDTO(artDTO.getArticulo()));
        return art;
    }

}
