package com.peluqueria.core.domain;

import com.peluqueria.dto.FuncionDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the funcion database table.
 *
 */
@Entity
@Table(name = "funcion", schema = "seguridad")
@NamedQuery(name = "Funcion.findAll", query = "SELECT f FROM Funcion f")
public class Funcion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcion")
    private Long idFuncion;

    private String descripcion;

    @Column(name = "url_pantalla")
    private String urlPantalla;

    // bi-directional many-to-one association to Modulo
    @ManyToOne
    @JoinColumn(name = "id_modulo")
    private Modulo modulo;

    // bi-directional many-to-one association to RolFuncion
    @OneToMany(mappedBy = "funcion")
    private List<RolFuncion> rolFuncions;

    public Funcion() {
    }

    public Long getIdFuncion() {
        return this.idFuncion;
    }

    public void setIdFuncion(Long idFuncion) {
        this.idFuncion = idFuncion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUrlPantalla() {
        return this.urlPantalla;
    }

    public void setUrlPantalla(String urlPantalla) {
        this.urlPantalla = urlPantalla;
    }

    public Modulo getModulo() {
        return this.modulo;
    }

    public void setModulo(Modulo modulo) {
        this.modulo = modulo;
    }

    public List<RolFuncion> getRolFuncions() {
        return this.rolFuncions;
    }

    public void setRolFuncions(List<RolFuncion> rolFuncions) {
        this.rolFuncions = rolFuncions;
    }

    public FuncionDTO toBdFuncionDTO() {
        FuncionDTO funDTO = toFuncionDTO(this);
        funDTO.setModulo(null);
        funDTO.setRolFuncions(null);
        return funDTO;
    }

    public FuncionDTO toFuncionDTO() {
        FuncionDTO funDTO = toFuncionDTO(this);
        if (this.modulo != null) {
            funDTO.setModulo(this.getModulo().toBDModuloDTO());
        }
        funDTO.setModulo(null);
        funDTO.setRolFuncions(null);
        return funDTO;
    }

    public static FuncionDTO toFuncionDTO(Funcion fun) {
        FuncionDTO funcionDTO = new FuncionDTO();
        BeanUtils.copyProperties(fun, funcionDTO);
        return funcionDTO;
    }

    public static Funcion fromFuncionDTO(FuncionDTO funcionDTO) {
        Funcion funcion = new Funcion();
        BeanUtils.copyProperties(funcionDTO, funcion);
        return funcion;
    }

    public static Funcion fromFuncionAsociadoDTO(FuncionDTO funDTO) {
        Funcion f = fromFuncionDTO(funDTO);
        f.setModulo(Modulo.fromModuloDTO(funDTO.getModulo()));
        return f;
    }

}
