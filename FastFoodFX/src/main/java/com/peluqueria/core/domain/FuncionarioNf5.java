package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf5DTO;

/**
 * The persistent class for the funcionario_nf5 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf5", schema = "descuento")
@NamedQuery(name = "FuncionarioNf5.findAll", query = "SELECT f FROM FuncionarioNf5 f")
public class FuncionarioNf5 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf5")
    private Long idFuncionarioNf5;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    public FuncionarioNf5() {
    }

    public Long getIdFuncionarioNf5() {
        return this.idFuncionarioNf5;
    }

    public void setIdFuncionarioNf5(Long idFuncionarioNf5) {
        this.idFuncionarioNf5 = idFuncionarioNf5;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf5 fromFuncionarioNf5DTO(FuncionarioNf5DTO funcionarioNf5DTO) {
        FuncionarioNf5 funcionarioNf5 = new FuncionarioNf5();
        BeanUtils.copyProperties(funcionarioNf5DTO, funcionarioNf5);
        return funcionarioNf5;
    }

    // <-- SERVER
    public static FuncionarioNf5DTO toFuncionarioNf5DTO(FuncionarioNf5 funcionarioNf5) {
        FuncionarioNf5DTO funcionarioNf5DTO = new FuncionarioNf5DTO();
        BeanUtils.copyProperties(funcionarioNf5, funcionarioNf5DTO);
        return funcionarioNf5DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf5 fromFuncionarioNf5DTOEntitiesFull(FuncionarioNf5DTO funcionarioNf5DTO) {
        FuncionarioNf5 funcionarioNf5 = fromFuncionarioNf5DTO(funcionarioNf5DTO);
        if (funcionarioNf5DTO.getNf5Seccion2() != null) {
            funcionarioNf5.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(funcionarioNf5DTO.getNf5Seccion2()));
        }
        return funcionarioNf5;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf5DTO toFuncionarioNf5DTONf4() {
        FuncionarioNf5DTO funcionarioNf5DTO = toFuncionarioNf5DTO(this);
        if (this.getNf5Seccion2() != null) {
            funcionarioNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        }
        return funcionarioNf5DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
