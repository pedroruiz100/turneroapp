package com.peluqueria.core.domain;

import com.peluqueria.dto.DetalleGastoDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_cheque database table.
 *
 */
@Entity
@Table(name = "detalle_gasto", schema = "factura_cliente")
@NamedQuery(name = "DetalleGasto.findAll", query = "SELECT f FROM DetalleGasto f")
public class DetalleGasto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_detalle_gasto")
    private Long idDetalleGasto;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "codigo")
    private String codigo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_gasto")
    private Gastos gastos;

    @Column(name = "costo")
    private Long costo;

    @Column(name = "cantidad")
    private BigDecimal cantidad;

    @Column(name = "cant_compra")
    private BigDecimal cantCompra;

    @Column(name = "costo_compra")
    private Long costoCompra;

    public DetalleGasto() {
    }

    public Long getIdDetalleGasto() {
        return idDetalleGasto;
    }

    public void setIdDetalleGasto(Long idDetalleGasto) {
        this.idDetalleGasto = idDetalleGasto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getCostoCompra() {
        return costoCompra;
    }

    public void setCostoCompra(Long costoCompra) {
        this.costoCompra = costoCompra;
    }

    public String getCodigo() {
        return codigo;
    }

    public BigDecimal getCantCompra() {
        return cantCompra;
    }

    public void setCantCompra(BigDecimal cantCompra) {
        this.cantCompra = cantCompra;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Gastos getGastos() {
        return gastos;
    }

    public void setGastos(Gastos gastos) {
        this.gastos = gastos;
    }

    public Long getCosto() {
        return costo;
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public DetalleGastoDTO toBDDetalleGastoDTO() {
        DetalleGastoDTO facDTO = toDetalleGastoDTO(this);
        facDTO.setGastos(null);
        return facDTO;
    }

    public DetalleGastoDTO toDetalleGastoDTO() {
        DetalleGastoDTO facDTO = toDetalleGastoDTO(this);
        if (this.gastos != null) {
            Gastos fac = this.getGastos();
            facDTO.setGastos(fac.toGastosDTO());
        }
        return facDTO;
    }

    public static DetalleGastoDTO toDetalleGastoDTO(
            DetalleGasto facturaClienteCabCheque) {
        DetalleGastoDTO facDTO = new DetalleGastoDTO();
        BeanUtils.copyProperties(facturaClienteCabCheque, facDTO);
        return facDTO;
    }

    public static DetalleGasto fromDetalleGastoDTO(
            DetalleGastoDTO facDTO) {
        DetalleGasto fac = new DetalleGasto();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static DetalleGasto fromDetalleGastoAsociadoDTO(
            DetalleGastoDTO facDTO) {
        DetalleGasto fac = fromDetalleGastoDTO(facDTO);
        fac.setGastos(Gastos.fromGastosDTO(facDTO
                .getGastos()));
        return fac;
    }

}
