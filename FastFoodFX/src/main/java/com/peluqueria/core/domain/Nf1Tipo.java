package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf1TipoDTO;

/**
 * The persistent class for the nf1_tipo database table.
 *
 */
@Entity
@Table(name = "nf1_tipo", schema = "stock")
@NamedQuery(name = "Nf1Tipo.findAll", query = "SELECT n FROM Nf1Tipo n")
public class Nf1Tipo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf1_tipo")
    private Long idNf1Tipo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional one-to-many association to ArticuloNf1Tipo
    @OneToMany(mappedBy = "nf1Tipo")
    private List<ArticuloNf1Tipo> articuloNf1Tipos;

    // bi-directional one-to-many association to Nf2Sfamilia
    @OneToMany(mappedBy = "nf1Tipo")
    private List<Nf2Sfamilia> nf2Sfamilias;

    // bi-directional one-to-many association to FuncionarioNf1
    @OneToMany(mappedBy = "nf1Tipo")
    private List<FuncionarioNf1> funcionarioNf1s;

    // bi-directional one-to-many association to DescuentoFielNf1
    @OneToMany(mappedBy = "nf1Tipo")
    private List<DescuentoFielNf1> descuentoFielNf1s;

    // bi-directional one-to-many association to PromoTemporadaNf1
    @OneToMany(mappedBy = "nf1Tipo")
    private List<PromoTemporadaNf1> promoTemporadaNf1s;

    public Nf1Tipo() {
    }

    public Long getIdNf1Tipo() {
        return this.idNf1Tipo;
    }

    public void setIdNf1Tipo(Long idNf1Tipo) {
        this.idNf1Tipo = idNf1Tipo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf1Tipo> getArticuloNf1Tipos() {
        return this.articuloNf1Tipos;
    }

    public void setArticuloNf1Tipos(List<ArticuloNf1Tipo> articuloNf1Tipos) {
        this.articuloNf1Tipos = articuloNf1Tipos;
    }

    public ArticuloNf1Tipo addArticuloNf1Tipo(ArticuloNf1Tipo articuloNf1Tipo) {
        getArticuloNf1Tipos().add(articuloNf1Tipo);
        articuloNf1Tipo.setNf1Tipo(this);

        return articuloNf1Tipo;
    }

    public ArticuloNf1Tipo removeArticuloNf1Tipo(ArticuloNf1Tipo articuloNf1Tipo) {
        getArticuloNf1Tipos().remove(articuloNf1Tipo);
        articuloNf1Tipo.setNf1Tipo(null);

        return articuloNf1Tipo;
    }

    public List<Nf2Sfamilia> getNf2Sfamilias() {
        return this.nf2Sfamilias;
    }

    public void setNf2Sfamilias(List<Nf2Sfamilia> nf2Sfamilias) {
        this.nf2Sfamilias = nf2Sfamilias;
    }

    public Nf2Sfamilia addNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        getNf2Sfamilias().add(nf2Sfamilia);
        nf2Sfamilia.setNf1Tipo(this);

        return nf2Sfamilia;
    }

    public Nf2Sfamilia removeNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        getNf2Sfamilias().remove(nf2Sfamilia);
        nf2Sfamilia.setNf1Tipo(null);

        return nf2Sfamilia;
    }

    public List<FuncionarioNf1> getFuncionarioNf1s() {
        return this.funcionarioNf1s;
    }

    public void setFuncionarioNf1s(List<FuncionarioNf1> funcionarioNf1s) {
        this.funcionarioNf1s = funcionarioNf1s;
    }

    public FuncionarioNf1 addFuncionarioNf1(FuncionarioNf1 funcionarioNf1) {
        getFuncionarioNf1s().add(funcionarioNf1);
        funcionarioNf1.setNf1Tipo(this);

        return funcionarioNf1;
    }

    public FuncionarioNf1 removeFuncionarioNf1(FuncionarioNf1 funcionarioNf1) {
        getFuncionarioNf1s().remove(funcionarioNf1);
        funcionarioNf1.setNf1Tipo(null);

        return funcionarioNf1;
    }

    public List<PromoTemporadaNf1> getPromoTemporadaNf1s() {
        return this.promoTemporadaNf1s;
    }

    public void setPromoTemporadaNf1s(List<PromoTemporadaNf1> promoTemporadaNf1s) {
        this.promoTemporadaNf1s = promoTemporadaNf1s;
    }

    public PromoTemporadaNf1 addPromoTemporadaNf1(PromoTemporadaNf1 promoTemporadaNf1) {
        getPromoTemporadaNf1s().add(promoTemporadaNf1);
        promoTemporadaNf1.setNf1Tipo(this);

        return promoTemporadaNf1;
    }

    public PromoTemporadaNf1 removePromoTemporadaNf1(PromoTemporadaNf1 promoTemporadaNf1) {
        getPromoTemporadaNf1s().remove(promoTemporadaNf1);
        promoTemporadaNf1.setNf1Tipo(null);

        return promoTemporadaNf1;
    }

    public List<DescuentoFielNf1> getDescuentoFielNf1s() {
        return this.descuentoFielNf1s;
    }

    public void setDescuentoFielNf1s(List<DescuentoFielNf1> descuentoFielNf1s) {
        this.descuentoFielNf1s = descuentoFielNf1s;
    }

    public DescuentoFielNf1 addDescuentoFielNf1(DescuentoFielNf1 descuentoFielNf1) {
        getDescuentoFielNf1s().add(descuentoFielNf1);
        descuentoFielNf1.setNf1Tipo(this);

        return descuentoFielNf1;
    }

    public DescuentoFielNf1 removeDescuentoFielNf1(DescuentoFielNf1 descuentoFielNf1) {
        getDescuentoFielNf1s().remove(descuentoFielNf1);
        descuentoFielNf1.setNf1Tipo(null);

        return descuentoFielNf1;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf1Tipo fromNf1TipoDTO(Nf1TipoDTO nf1TipoDTO) {
        Nf1Tipo nf1Tipo = new Nf1Tipo();
        BeanUtils.copyProperties(nf1TipoDTO, nf1Tipo);
        return nf1Tipo;
    }

    // <-- SERVER
    public static Nf1TipoDTO toNf1TipoDTO(Nf1Tipo nf1Tipo) {
        Nf1TipoDTO nf1TipoDTO = new Nf1TipoDTO();
        BeanUtils.copyProperties(nf1Tipo, nf1TipoDTO);
        return nf1TipoDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, SALIDA ########################
    // <-- SERVER
    public static Nf1TipoDTO toNf1TipoDTOEntitiesNull(Nf1Tipo nf1Tipo) {
        Nf1TipoDTO nf1TipoDTO = toNf1TipoDTO(nf1Tipo);
        nf1TipoDTO.setArticuloNf1Tipos(null);// Entities
        nf1TipoDTO.setNf2Sfamilias(null);// Entities
        nf1TipoDTO.setFuncionarioNf1s(null);// Entities
        nf1TipoDTO.setDescuentoFielNf1s(null);// Entities
        nf1TipoDTO.setPromoTemporadaNf1s(null);// Entities
        return nf1TipoDTO;
    }

    // <-- SERVER
    public Nf1TipoDTO toNf1TipoDTOEntitiesNull() {
        Nf1TipoDTO nf1TipoDTO = toNf1TipoDTO(this);
        nf1TipoDTO.setArticuloNf1Tipos(null);// Entities
        nf1TipoDTO.setNf2Sfamilias(null);// Entities
        nf1TipoDTO.setFuncionarioNf1s(null);// Entities
        nf1TipoDTO.setDescuentoFielNf1s(null);// Entities
        nf1TipoDTO.setPromoTemporadaNf1s(null);// Entities
        return nf1TipoDTO;
    }
    // ######################## NULO, SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf1Tipo fromNf1TipoDTOEntitiesFull(Nf1TipoDTO nf1TipoDTO) {
        Nf1Tipo nf1Tipo = fromNf1TipoDTO(nf1TipoDTO);
        return nf1Tipo;
    }
    // ######################## FULL, ENTRADA ########################

}
