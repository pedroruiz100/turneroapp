package com.peluqueria.core.domain;

import com.peluqueria.dto.DescMonedaExtranjeraDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_efectivo database table.
 *
 */
@Entity
@Table(name = "desc_moneda_extranjera", schema = "factura_cliente")
@NamedQuery(name = "DescMonedaExtranjera.findAll", query = "SELECT d FROM DescMonedaExtranjera d")
public class DescMonedaExtranjera implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_moneda_extranjera")
    private Long idDescMonedaExtranjera;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteCabEfectivo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_moneda_extranjera")
    private FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjera;

    public DescMonedaExtranjera() {
    }

    public Long getIdDescMonedaExtranjera() {
        return idDescMonedaExtranjera;
    }

    public void setIdDescMonedaExtranjera(Long idDescMonedaExtranjera) {
        this.idDescMonedaExtranjera = idDescMonedaExtranjera;
    }

    public Integer getMontoDesc() {
        return montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabMonedaExtranjera getFacturaClienteCabMonedaExtranjera() {
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

    public DescMonedaExtranjeraDTO toDescMonedaExtranjeraDTO() {
        DescMonedaExtranjeraDTO descDTO = toDescMonedaExtranjeraDTO(this);
        if (this.getFacturaClienteCabMonedaExtranjera() != null) {
            descDTO.setFacturaClienteCabMonedaExtranjera(this
                    .getFacturaClienteCabMonedaExtranjera()
                    .toFacturaClienteCabMonedaExtranjeraBDDTO());
        }
        return descDTO;
    }

    private DescMonedaExtranjeraDTO toDescMonedaExtranjeraDTO(
            DescMonedaExtranjera descMonedaExtranjera) {
        DescMonedaExtranjeraDTO descDTO = new DescMonedaExtranjeraDTO();
        BeanUtils.copyProperties(descMonedaExtranjera, descDTO);
        return descDTO;
    }

    public static DescMonedaExtranjera fromDescMonedaExtranjera(
            DescMonedaExtranjeraDTO descDTO) {
        DescMonedaExtranjera desc = new DescMonedaExtranjera();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public static DescMonedaExtranjera fromDescMonedaExtranjeraAsociado(
            DescMonedaExtranjeraDTO descDTO) {
        DescMonedaExtranjera desc = fromDescMonedaExtranjera(descDTO);
        desc.setFacturaClienteCabMonedaExtranjera(FacturaClienteCabMonedaExtranjera
                .fromFacturaClienteCabMonedaExtranjera(descDTO
                        .getFacturaClienteCabMonedaExtranjera()));
        return desc;
    }

}
