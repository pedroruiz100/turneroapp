package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf6DTO;

/**
 * The persistent class for the funcionario_nf6 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf6", schema = "descuento")
@NamedQuery(name = "FuncionarioNf6.findAll", query = "SELECT f FROM FuncionarioNf6 f")
public class FuncionarioNf6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf6")
    private Long idFuncionarioNf6;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    public FuncionarioNf6() {
    }

    public Long getIdFuncionarioNf6() {
        return this.idFuncionarioNf6;
    }

    public void setIdFuncionarioNf6(Long idFuncionarioNf6) {
        this.idFuncionarioNf6 = idFuncionarioNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf6 fromFuncionarioNf6DTO(FuncionarioNf6DTO funcionarioNf6DTO) {
        FuncionarioNf6 funcionarioNf6 = new FuncionarioNf6();
        BeanUtils.copyProperties(funcionarioNf6DTO, funcionarioNf6);
        return funcionarioNf6;
    }

    // <-- SERVER
    public static FuncionarioNf6DTO toFuncionarioNf6DTO(FuncionarioNf6 funcionarioNf6) {
        FuncionarioNf6DTO funcionarioNf6DTO = new FuncionarioNf6DTO();
        BeanUtils.copyProperties(funcionarioNf6, funcionarioNf6DTO);
        return funcionarioNf6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf6 fromFuncionarioNf6DTOEntitiesFull(FuncionarioNf6DTO funcionarioNf6DTO) {
        FuncionarioNf6 funcionarioNf6 = fromFuncionarioNf6DTO(funcionarioNf6DTO);
        if (funcionarioNf6DTO.getNf6Secnom6() != null) {
            funcionarioNf6.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(funcionarioNf6DTO.getNf6Secnom6()));
        }
        return funcionarioNf6;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf6DTO toFuncionarioNf6DTONf5() {
        FuncionarioNf6DTO funcionarioNf6DTO = toFuncionarioNf6DTO(this);
        if (this.getNf6Secnom6() != null) {
            funcionarioNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        }
        return funcionarioNf6DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
