package com.peluqueria.core.domain;

import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.IpBocaDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the caja database table.
 *
 */
@Entity
@Table(name = "caja", schema = "caja")
@NamedQuery(name = "Caja.findAll", query = "SELECT c FROM Caja c")
public class Caja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_caja")
    private Long idCaja;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "nro_serie")
    private String nroSerie;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to AperturaCaja
    @OneToMany(mappedBy = "caja", fetch = FetchType.LAZY)
    private List<AperturaCaja> aperturaCajas;

    // bi-directional many-to-one association to TipoCaja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_caja")
    private TipoCaja tipoCaja;

    // bi-directional many-to-one association to CierreCaja
    @OneToMany(mappedBy = "caja", fetch = FetchType.LAZY)
    private List<CierreCaja> cierreCajas;

    // bi-directional many-to-one association to RetiroDinero
    @OneToMany(mappedBy = "caja", fetch = FetchType.LAZY)
    private List<RetiroDinero> retiroDineros;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "caja", fetch = FetchType.LAZY)
    private List<FacturaClienteCab> facturaClienteCabs;

    // bi-directional many-to-one association to ArqueoCaja
    @OneToMany(mappedBy = "caja", fetch = FetchType.LAZY)
    private List<ArqueoCaja> arqueoCaja;

    @Column(name = "nro_caja")
    private Integer nroCaja;

    @Column(name = "clave_caja")
    private String claveCaja;

    // bi-directional many-to-one association to IpBoca
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ip_boca")
    private IpBoca ipBoca;

    // bi-directional many-to-one association to Sucursal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    public Caja() {
    }

    public Long getIdCaja() {
        return this.idCaja;
    }

    public void setIdCaja(Long idCaja) {
        this.idCaja = idCaja;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getNroSerie() {
        return this.nroSerie;
    }

    public void setNroSerie(String nroSerie) {
        this.nroSerie = nroSerie;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<AperturaCaja> getAperturaCajas() {
        return this.aperturaCajas;
    }

    public void setAperturaCajas(List<AperturaCaja> aperturaCajas) {
        this.aperturaCajas = aperturaCajas;
    }

    public TipoCaja getTipoCaja() {
        return this.tipoCaja;
    }

    public void setTipoCaja(TipoCaja tipoCaja) {
        this.tipoCaja = tipoCaja;
    }

    // ********************************************************************************
    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public List<CierreCaja> getCierreCajas() {
        return this.cierreCajas;
    }

    public void setCierreCajas(List<CierreCaja> cierreCajas) {
        this.cierreCajas = cierreCajas;
    }

    // ********************************************************************************
    public List<RetiroDinero> getRetiroDineros() {
        return this.retiroDineros;
    }

    public void setRetiroDineros(List<RetiroDinero> retiroDineros) {
        this.retiroDineros = retiroDineros;
    }

    // ********************************************************************************
    public List<FacturaClienteCab> getFacturaClienteCabs() {
        return facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCab> facturaClienteCab) {
        this.facturaClienteCabs = facturaClienteCab;
    }

    // ********************************************************************************
    public Integer getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(Integer nroCaja) {
        this.nroCaja = nroCaja;
    }

    public String getClaveCaja() {
        return claveCaja;
    }

    public void setClaveCaja(String claveCaja) {
        this.claveCaja = claveCaja;
    }

    public IpBoca getIpBoca() {
        return ipBoca;
    }

    public void setIpBoca(IpBoca ipBoca) {
        this.ipBoca = ipBoca;
    }

    public CajaDTO toCajaSinRelacionDTO() {
        CajaDTO cajaDTO = toCajaDTO(this);
        cajaDTO.setIpBoca(null);
        cajaDTO.setCierreCajasDTO(null);
        cajaDTO.setAperturaCajasDTO(null);
        cajaDTO.setRetiroDinerosDTO(null);
        cajaDTO.setFacturaClienteCabDTO(null);
        cajaDTO.setTipoCaja(null);
        cajaDTO.setAperturaCajasDTO(null);
        return cajaDTO;
    }

    // DTOS para Apertura de CAJA
    public CajaDTO toCajaDTO() {
        CajaDTO cajaDTO = this.toCajaDTO(this);
        if (this.getIpBoca() != null) {
            IpBoca ipBoca = this.getIpBoca();
            IpBocaDTO ipBocaDTO = ipBoca.toBDIpBocaDTO();
            ipBocaDTO.setTalonariosSucursales(null);
            ipBocaDTO.setCaja(null);
            cajaDTO.setIpBoca(ipBocaDTO);
        }

        if (this.getTipoCaja() != null) {
            TipoCaja tp = this.getTipoCaja();
            cajaDTO.setTipoCaja(tp.toTipoCajaDTO());
        }

        if (this.getSucursal() != null) {
            Sucursal sucu = this.getSucursal();
            cajaDTO.setSucursal(sucu.toSucursalEmpresaCiudadDTO());
        }

        cajaDTO.setCierreCajasDTO(null);
        cajaDTO.setAperturaCajasDTO(null);
        cajaDTO.setRetiroDinerosDTO(null);
        cajaDTO.setAperturaCajasDTO(null);
        return cajaDTO;
    }

    public CajaDTO toBDCajaDTO() {
        CajaDTO cajaDTO = this.toCajaDTO(this);
        cajaDTO.setIpBoca(null);
        cajaDTO.setTipoCaja(null);
        cajaDTO.setCierreCajasDTO(null);
        cajaDTO.setAperturaCajasDTO(null);
        cajaDTO.setRetiroDinerosDTO(null);
        cajaDTO.setSucursal(null);
        cajaDTO.setAperturaCajasDTO(null);
        return cajaDTO;
    }

    public CajaDTO toCajaDTO(Caja caja) {
        CajaDTO cajaDTO = new CajaDTO();
        BeanUtils.copyProperties(caja, cajaDTO);
        return cajaDTO;
    }

    // FIN DE DTOS para Apertura de CAJA
    public static Caja fromCajaDTOAsociado(CajaDTO dto) {
        Caja caja = fromCajaDTO(dto);
        caja.setTipoCaja(TipoCaja.fromTipoCajaDTO(dto.getTipoCaja()));
        caja.setIpBoca(IpBoca.fromIpBocaDTO(dto.getIpBoca()));
        caja.setSucursal(Sucursal.fromSucursalDTO(dto.getSucursal()));
        return caja;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas)
    public static Caja fromCajaDTO(CajaDTO cajaDTO) {
        Caja caja = new Caja();
        BeanUtils.copyProperties(cajaDTO, caja);
        return caja;
    }
}
