package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.ArqueoCajaDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the arqueo_caja database table.
 *
 */
@Entity
@Table(schema = "caja", name = "arqueo_caja")
@NamedQuery(name = "ArqueoCaja.findAll", query = "SELECT a FROM ArqueoCaja a")
public class ArqueoCaja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_arqueo_caja")
    private Long idArqueoCaja;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caja")
    private Caja caja;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_supervisor")
    private Supervisor supervisor;

    private Integer asociacion;

    @Column(name = "cant_asociacion")
    private Integer cantAsociacion;

    @Column(name = "zeta")
    private Integer zeta;

    @Column(name = "nro_timbrado")
    private String nroTimbrado;

    @Column(name = "factura_ini")
    private String facturaIni;

    @Column(name = "factura_fin")
    private String facturaFin;

    @Column(name = "cant_cheque_adel")
    private Integer cantChequeAdel;

    @Column(name = "cant_cheque_dia")
    private Integer cantChequeDia;

    @Column(name = "cant_dolar")
    private Integer cantDolar;

    @Column(name = "cant_factory")
    private Integer cantFactory;

    @Column(name = "cant_fco")
    private Integer cantFco;

    @Column(name = "cant_fco_canje")
    private Integer cantFcoCanje;

    @Column(name = "cant_fcred")
    private Integer cantFcred;

    @Column(name = "cant_fcred_canje")
    private Integer cantFcredCanje;

    @Column(name = "cant_gifcard")
    private Integer cantGifcard;

    @Column(name = "cant_infonet")
    private Integer cantInfonet;

    @Column(name = "cant_nota_cred")
    private Integer cantNotaCred;

    @Column(name = "cant_peso")
    private Integer cantPeso;

    @Column(name = "cant_practipago")
    private Integer cantPractipago;

    @Column(name = "cant_pronet")
    private Integer cantPronet;

    @Column(name = "cant_real")
    private Integer cantReal;

    @Column(name = "cant_sencillo")
    private Integer cantSencillo;

    @Column(name = "cant_vale")
    private Integer cantVale;

    @Column(name = "cheque_adel")
    private Integer chequeAdel;

    @Column(name = "cheque_dia")
    private Integer chequeDia;

    private Integer dolar;

    private Integer efectivo;

    private Integer factory;

    private Integer fco;

    @Column(name = "fco_canje")
    private Integer fcoCanje;

    private Integer fcred;

    @Column(name = "fcred_canje")
    private Integer fcredCanje;

    @Column(name = "fecha_emision")
    private Timestamp fechaEmision;

    private Integer gifcard;

    private Integer infonet;

    @Column(name = "nota_cred")
    private Integer notaCred;

    private Integer peso;

    private Integer practipago;

    private Integer pronet;

    private Integer real;

    private Integer sencillo;

    private Integer vale;

    @Column(name = "cont_desc")
    private Integer contDesc;

    @Column(name = "sum_desc")
    private Integer sumtDesc;

    @Column(name = "cont_tarj")
    private Integer contTarj;

    @Column(name = "sum_tarj")
    private Integer sumTarj;

    @Column(name = "cont_efectivo")
    private Integer contEfectivo;

    @Column(name = "sum_efectivo")
    private Integer sumEfectivo;

    @Column(name = "cant_eferecibida")
    private Integer cantEferecibida;

    @Column(name = "cont_donacion")
    private Integer contDonacion;

    @Column(name = "sum_donacion")
    private Integer sumDonacion;

    @Column(name = "totgra")
    private Integer totgra;

    @Column(name = "totexe")
    private Integer totexe;

    @Column(name = "totgra5")
    private Integer totgra5;

    @Column(name = "totgra10")
    private Integer totgra10;

    @Column(name = "totales")
    private Integer totales;

    @Column(name = "num_cliente")
    private Integer numCliente;

    @Column(name = "num_articulo")
    private Integer numArticulo;

    @Column(name = "num_funcionario")
    private Integer numFuncionario;

    @Column(name = "v100000")
    private Integer v100000;

    @Column(name = "v50000")
    private Integer v50000;

    @Column(name = "v10000")
    private Integer v10000;

    @Column(name = "v5000")
    private Integer v5000;

    @Column(name = "v1000")
    private Integer v1000;

    @Column(name = "v500")
    private Integer v500;

    @Column(name = "v100")
    private Integer v100;

    @Column(name = "v50")
    private Integer v50;

    @Column(name = "cont_nota_cred")
    private Integer contNotaCred;

    @Column(name = "sum_nota_cred")
    private Integer sumNotaCred;

    @Column(name = "cont_vale")
    private Integer contVale;

    @Column(name = "sum_vale")
    private Integer sumVale;

    @Column(name = "cont_fact")
    private Integer contFact;

    @Column(name = "sum_fact")
    private Integer sumFact;

    @Column(name = "cont_cheque")
    private Integer contCheque;

    @Column(name = "sum_cheque")
    private Integer sumCheque;

    @Column(name = "cont_asoc")
    private Integer contAsoc;

    @Column(name = "sum_asoc")
    private Integer sumAsoc;

    @Column(name = "cont_dolar")
    private Integer contDolar;

    @Column(name = "sum_dolar")
    private Integer sumDolar;

    @Column(name = "cont_real")
    private Integer contReal;

    @Column(name = "sum_real")
    private Integer sumReal;

    @Column(name = "cont_peso")
    private Integer contPeso;

    @Column(name = "sum_peso")
    private Integer sumPeso;

    @Column(name = "sum_retencion")
    private Integer sumRetencion;

    @Column(name = "cont_retencion")
    private Integer contRetencion;

    public ArqueoCaja() {
    }

    public Long getIdArqueoCaja() {
        return this.idArqueoCaja;
    }

    public void setIdArqueoCaja(Long idArqueoCaja) {
        this.idArqueoCaja = idArqueoCaja;
    }

    public Integer getAsociacion() {
        return this.asociacion;
    }

    public void setAsociacion(Integer asociacion) {
        this.asociacion = asociacion;
    }

    public Integer getCantAsociacion() {
        return this.cantAsociacion;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public String getFacturaIni() {
        return facturaIni;
    }

    public Integer getContNotaCred() {
        return contNotaCred;
    }

    public void setContNotaCred(Integer contNotaCred) {
        this.contNotaCred = contNotaCred;
    }

    public Integer getSumNotaCred() {
        return sumNotaCred;
    }

    public Integer getContRetencion() {
        return contRetencion;
    }

    public void setContRetencion(Integer contRetencion) {
        this.contRetencion = contRetencion;
    }

    public void setSumNotaCred(Integer sumNotaCred) {
        this.sumNotaCred = sumNotaCred;
    }

    public Integer getContVale() {
        return contVale;
    }

    public void setContVale(Integer contVale) {
        this.contVale = contVale;
    }

    public Integer getSumVale() {
        return sumVale;
    }

    public void setSumVale(Integer sumVale) {
        this.sumVale = sumVale;
    }

    public Integer getContFact() {
        return contFact;
    }

    public void setContFact(Integer contFact) {
        this.contFact = contFact;
    }

    public Integer getSumFact() {
        return sumFact;
    }

    public void setSumFact(Integer sumFact) {
        this.sumFact = sumFact;
    }

    public Integer getContCheque() {
        return contCheque;
    }

    public void setContCheque(Integer contCheque) {
        this.contCheque = contCheque;
    }

    public Integer getSumCheque() {
        return sumCheque;
    }

    public void setSumCheque(Integer sumCheque) {
        this.sumCheque = sumCheque;
    }

    public Integer getContAsoc() {
        return contAsoc;
    }

    public void setContAsoc(Integer contAsoc) {
        this.contAsoc = contAsoc;
    }

    public Integer getSumAsoc() {
        return sumAsoc;
    }

    public void setSumAsoc(Integer sumAsoc) {
        this.sumAsoc = sumAsoc;
    }

    public void setFacturaIni(String facturaIni) {
        this.facturaIni = facturaIni;
    }

    public String getFacturaFin() {
        return facturaFin;
    }

    public Integer getV100000() {
        return v100000;
    }

    public void setV100000(Integer v100000) {
        this.v100000 = v100000;
    }

    public Integer getV50000() {
        return v50000;
    }

    public void setV50000(Integer v50000) {
        this.v50000 = v50000;
    }

    public Integer getV10000() {
        return v10000;
    }

    public void setV10000(Integer v10000) {
        this.v10000 = v10000;
    }

    public Integer getV5000() {
        return v5000;
    }

    public void setV5000(Integer v5000) {
        this.v5000 = v5000;
    }

    public Integer getV1000() {
        return v1000;
    }

    public void setV1000(Integer v1000) {
        this.v1000 = v1000;
    }

    public Integer getV500() {
        return v500;
    }

    public void setV500(Integer v500) {
        this.v500 = v500;
    }

    public Integer getV100() {
        return v100;
    }

    public void setV100(Integer v100) {
        this.v100 = v100;
    }

    public Integer getV50() {
        return v50;
    }

    public void setV50(Integer v50) {
        this.v50 = v50;
    }

    public void setFacturaFin(String facturaFin) {
        this.facturaFin = facturaFin;
    }

    public void setCantAsociacion(Integer cantAsociacion) {
        this.cantAsociacion = cantAsociacion;
    }

    public Integer getCantChequeAdel() {
        return this.cantChequeAdel;
    }

    public void setCantChequeAdel(Integer cantChequeAdel) {
        this.cantChequeAdel = cantChequeAdel;
    }

    public Integer getCantChequeDia() {
        return this.cantChequeDia;
    }

    public Integer getContDesc() {
        return contDesc;
    }

    public void setContDesc(Integer contDesc) {
        this.contDesc = contDesc;
    }

    public Integer getSumtDesc() {
        return sumtDesc;
    }

    public void setSumtDesc(Integer sumtDesc) {
        this.sumtDesc = sumtDesc;
    }

    public Integer getContTarj() {
        return contTarj;
    }

    public void setContTarj(Integer contTarj) {
        this.contTarj = contTarj;
    }

    public Integer getSumTarj() {
        return sumTarj;
    }

    public void setSumTarj(Integer sumTarj) {
        this.sumTarj = sumTarj;
    }

    public void setCantChequeDia(Integer cantChequeDia) {
        this.cantChequeDia = cantChequeDia;
    }

    public Integer getZeta() {
        return zeta;
    }

    public void setZeta(Integer zeta) {
        this.zeta = zeta;
    }

    public Integer getCantDolar() {
        return this.cantDolar;
    }

    public void setCantDolar(Integer cantDolar) {
        this.cantDolar = cantDolar;
    }

    public Integer getCantFactory() {
        return this.cantFactory;
    }

    public void setCantFactory(Integer cantFactory) {
        this.cantFactory = cantFactory;
    }

    public Integer getCantFco() {
        return this.cantFco;
    }

    public void setCantFco(Integer cantFco) {
        this.cantFco = cantFco;
    }

    public Integer getCantFcoCanje() {
        return this.cantFcoCanje;
    }

    public void setCantFcoCanje(Integer cantFcoCanje) {
        this.cantFcoCanje = cantFcoCanje;
    }

    public Integer getCantFcred() {
        return this.cantFcred;
    }

    public void setCantFcred(Integer cantFcred) {
        this.cantFcred = cantFcred;
    }

    public Integer getCantFcredCanje() {
        return this.cantFcredCanje;
    }

    public void setCantFcredCanje(Integer cantFcredCanje) {
        this.cantFcredCanje = cantFcredCanje;
    }

    public Integer getCantGifcard() {
        return this.cantGifcard;
    }

    public void setCantGifcard(Integer cantGifcard) {
        this.cantGifcard = cantGifcard;
    }

    public Integer getCantInfonet() {
        return this.cantInfonet;
    }

    public void setCantInfonet(Integer cantInfonet) {
        this.cantInfonet = cantInfonet;
    }

    public Integer getCantNotaCred() {
        return this.cantNotaCred;
    }

    public void setCantNotaCred(Integer cantNotaCred) {
        this.cantNotaCred = cantNotaCred;
    }

    public Integer getCantPeso() {
        return this.cantPeso;
    }

    public void setCantPeso(Integer cantPeso) {
        this.cantPeso = cantPeso;
    }

    public Integer getCantPractipago() {
        return this.cantPractipago;
    }

    public void setCantPractipago(Integer cantPractipago) {
        this.cantPractipago = cantPractipago;
    }

    public Integer getCantPronet() {
        return this.cantPronet;
    }

    public void setCantPronet(Integer cantPronet) {
        this.cantPronet = cantPronet;
    }

    public Integer getCantReal() {
        return this.cantReal;
    }

    public void setCantReal(Integer cantReal) {
        this.cantReal = cantReal;
    }

    public Integer getCantSencillo() {
        return this.cantSencillo;
    }

    public void setCantSencillo(Integer cantSencillo) {
        this.cantSencillo = cantSencillo;
    }

    public Integer getCantVale() {
        return this.cantVale;
    }

    public void setCantVale(Integer cantVale) {
        this.cantVale = cantVale;
    }

    public Integer getChequeAdel() {
        return this.chequeAdel;
    }

    public void setChequeAdel(Integer chequeAdel) {
        this.chequeAdel = chequeAdel;
    }

    public Integer getChequeDia() {
        return this.chequeDia;
    }

    public void setChequeDia(Integer chequeDia) {
        this.chequeDia = chequeDia;
    }

    public Integer getDolar() {
        return this.dolar;
    }

    public void setDolar(Integer dolar) {
        this.dolar = dolar;
    }

    public Integer getEfectivo() {
        return this.efectivo;
    }

    public void setEfectivo(Integer efectivo) {
        this.efectivo = efectivo;
    }

    public Integer getFactory() {
        return this.factory;
    }

    public void setFactory(Integer factory) {
        this.factory = factory;
    }

    public Integer getFco() {
        return this.fco;
    }

    public void setFco(Integer fco) {
        this.fco = fco;
    }

    public Integer getFcoCanje() {
        return this.fcoCanje;
    }

    public void setFcoCanje(Integer fcoCanje) {
        this.fcoCanje = fcoCanje;
    }

    public Integer getFcred() {
        return this.fcred;
    }

    public void setFcred(Integer fcred) {
        this.fcred = fcred;
    }

    public Integer getFcredCanje() {
        return this.fcredCanje;
    }

    public void setFcredCanje(Integer fcredCanje) {
        this.fcredCanje = fcredCanje;
    }

    public Timestamp getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Integer getGifcard() {
        return this.gifcard;
    }

    public void setGifcard(Integer gifcard) {
        this.gifcard = gifcard;
    }

    public Integer getInfonet() {
        return this.infonet;
    }

    public void setInfonet(Integer infonet) {
        this.infonet = infonet;
    }

    public Integer getNotaCred() {
        return this.notaCred;
    }

    public Integer getContEfectivo() {
        return contEfectivo;
    }

    public void setContEfectivo(Integer contEfectivo) {
        this.contEfectivo = contEfectivo;
    }

    public Integer getSumEfectivo() {
        return sumEfectivo;
    }

    public void setSumEfectivo(Integer sumEfectivo) {
        this.sumEfectivo = sumEfectivo;
    }

    public Integer getCantEferecibida() {
        return cantEferecibida;
    }

    public void setCantEferecibida(Integer cantEferecibida) {
        this.cantEferecibida = cantEferecibida;
    }

    public Integer getContDonacion() {
        return contDonacion;
    }

    public void setContDonacion(Integer contDonacion) {
        this.contDonacion = contDonacion;
    }

    public Integer getSumDonacion() {
        return sumDonacion;
    }

    public void setSumDonacion(Integer sumDonacion) {
        this.sumDonacion = sumDonacion;
    }

    public Integer getTotgra() {
        return totgra;
    }

    public void setTotgra(Integer totgra) {
        this.totgra = totgra;
    }

    public Integer getTotexe() {
        return totexe;
    }

    public void setTotexe(Integer totexe) {
        this.totexe = totexe;
    }

    public Integer getTotgra5() {
        return totgra5;
    }

    public void setTotgra5(Integer totgra5) {
        this.totgra5 = totgra5;
    }

    public Integer getTotgra10() {
        return totgra10;
    }

    public void setTotgra10(Integer totgra10) {
        this.totgra10 = totgra10;
    }

    public Integer getTotales() {
        return totales;
    }

    public void setTotales(Integer totales) {
        this.totales = totales;
    }

    public Integer getNumCliente() {
        return numCliente;
    }

    public void setNumCliente(Integer numCliente) {
        this.numCliente = numCliente;
    }

    public Integer getNumArticulo() {
        return numArticulo;
    }

    public void setNumArticulo(Integer numArticulo) {
        this.numArticulo = numArticulo;
    }

    public Integer getNumFuncionario() {
        return numFuncionario;
    }

    public void setNumFuncionario(Integer numFuncionario) {
        this.numFuncionario = numFuncionario;
    }

    public void setNotaCred(Integer notaCred) {
        this.notaCred = notaCred;
    }

    public Integer getPeso() {
        return this.peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getPractipago() {
        return this.practipago;
    }

    public void setPractipago(Integer practipago) {
        this.practipago = practipago;
    }

    public Integer getPronet() {
        return this.pronet;
    }

    public void setPronet(Integer pronet) {
        this.pronet = pronet;
    }

    public Integer getReal() {
        return this.real;
    }

    public void setReal(Integer real) {
        this.real = real;
    }

    public Integer getSencillo() {
        return this.sencillo;
    }

    public void setSencillo(Integer sencillo) {
        this.sencillo = sencillo;
    }

    public Integer getVale() {
        return this.vale;
    }

    public void setVale(Integer vale) {
        this.vale = vale;
    }

    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public Supervisor getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(Supervisor supervisor) {
        this.supervisor = supervisor;
    }

    public Integer getContDolar() {
        return contDolar;
    }

    public void setContDolar(Integer contDolar) {
        this.contDolar = contDolar;
    }

    public Integer getSumDolar() {
        return sumDolar;
    }

    public void setSumDolar(Integer sumDolar) {
        this.sumDolar = sumDolar;
    }

    public Integer getContReal() {
        return contReal;
    }

    public void setContReal(Integer contReal) {
        this.contReal = contReal;
    }

    public Integer getSumReal() {
        return sumReal;
    }

    public void setSumReal(Integer sumReal) {
        this.sumReal = sumReal;
    }

    public Integer getContPeso() {
        return contPeso;
    }

    public void setContPeso(Integer contPeso) {
        this.contPeso = contPeso;
    }

    public Integer getSumPeso() {
        return sumPeso;
    }

    public void setSumPeso(Integer sumPeso) {
        this.sumPeso = sumPeso;
    }

    public Integer getSumRetencion() {
        return sumRetencion;
    }

    public void setSumRetencion(Integer sumRetencion) {
        this.sumRetencion = sumRetencion;
    }

    public ArqueoCajaDTO toArqueoCajaDTO() {
        ArqueoCajaDTO arDTO = toArqueoCajaDTO(this);
        if (this.caja != null) {
            arDTO.setCaja(this.getCaja().toBDCajaDTO());
        }
        if (this.supervisor != null) {
            arDTO.setSupervisor(this.getSupervisor().toSupervisorDTO());
        }
        return arDTO;
    }

    public ArqueoCajaDTO toBDArqueoCajaDTO() {
        ArqueoCajaDTO arDTO = toArqueoCajaDTO(this);
        arDTO.setCaja(null);
        arDTO.setSupervisor(null);
        return arDTO;
    }

    public static ArqueoCajaDTO toArqueoCajaDTO(ArqueoCaja arqueoCaja) {
        ArqueoCajaDTO arDTO = new ArqueoCajaDTO();
        BeanUtils.copyProperties(arqueoCaja, arDTO);
        return arDTO;
    }

    public static ArqueoCaja fromArqueoCajaDTO(ArqueoCajaDTO arDTO) {
        ArqueoCaja ar = new ArqueoCaja();
        BeanUtils.copyProperties(arDTO, ar);
        return ar;
    }

    public static ArqueoCaja fromArqueoCajaAsociadoDTO(ArqueoCajaDTO arDTO) {
        ArqueoCaja ar = fromArqueoCajaDTO(arDTO);
        ar.setCaja(Caja.fromCajaDTO(arDTO.getCaja()));
        ar.setSupervisor(Supervisor.fromSupervisorDTO(arDTO.getSupervisor()));
        return ar;
    }
}
