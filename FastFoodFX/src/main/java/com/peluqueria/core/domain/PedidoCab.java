package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.PedidoCabDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the apertura_caja database table.
 *
 */
@Entity
@Table(name = "pedido_cab", schema = "factura_cliente")
@NamedQuery(name = "PedidoCab.findAll", query = "SELECT a FROM PedidoCab a")
public class PedidoCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido_cab")
    private Long idPedidoCab;

    @Column(name = "usuario")
    private String usuario;

    @Column(name = "plazo")
    private String plazo;

    @Column(name = "sucursal")
    private String sucursal;

    @Column(name = "nro_pedido")
    private String nroPedido;

    @Column(name = "fecha")
    private Timestamp fecha;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "tipo")
    private String tipo;
    @Column(name = "visita")
    private String visita;
    @Column(name = "cuotas")
    private String cuotas;
    @Column(name = "frecuencia")
    private String frecuencia;
    @Column(name = "oc")
    private String oc;

    @Column(name = "moneda")
    private String moneda;
    @Column(name = "cotizacion")
    private String cotizacion;
    @Column(name = "total")
    private Long total;
    @Column(name = "saldo")
    private Long saldo;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to UsuarioRol
    @OneToMany(mappedBy = "pedidoCab", fetch = FetchType.LAZY)
    private List<Recepcion> recepcion;

    public PedidoCab() {
    }

    public Long getIdPedidoCab() {
        return idPedidoCab;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

    public String getVisita() {
        return visita;
    }

    public String getOc() {
        return oc;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public void setVisita(String visita) {
        this.visita = visita;
    }

    public String getCuotas() {
        return cuotas;
    }

    public void setCuotas(String cuotas) {
        this.cuotas = cuotas;
    }

    public String getFrecuencia() {
        return frecuencia;
    }

    public void setFrecuencia(String frecuencia) {
        this.frecuencia = frecuencia;
    }

    public void setIdPedidoCab(Long idPedidoCab) {
        this.idPedidoCab = idPedidoCab;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public String getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(String cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<Recepcion> getRecepcion() {
        return recepcion;
    }

    public void setRecepcion(List<Recepcion> recepcion) {
        this.recepcion = recepcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPlazo() {
        return plazo;
    }

    public void setPlazo(String plazo) {
        this.plazo = plazo;
    }

    public String getNroPedido() {
        return nroPedido;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public void setNroPedido(String nroPedido) {
        this.nroPedido = nroPedido;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    // DTOs****************************************************************************************
    // RELACION CON UsuarioSupervisor
//    public AperturaCajaDTO toAperturaCajaDTO() {
//        AperturaCajaDTO apCajaDTO = this.toAperturaCajaDTO(this);
//        return apCajaDTO;
//    }
    public PedidoCabDTO toPedidoCabDTO() {
        PedidoCabDTO apDTO = toPedidoCabDTO(this);
//        apDTO.set(null);
//        apDTO.setUsuarioCajero(null);
        return apDTO;
    }

    public PedidoCabDTO toPedidoCabDTO(PedidoCab ap) {
        PedidoCabDTO acDTO = new PedidoCabDTO();
        BeanUtils.copyProperties(ap, acDTO);
        return acDTO;
    }

    // METODO QUE
    // Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
    // CAJERO Y USUARIO SUPERVISOR
    public static PedidoCab fromPedidoCabDTO(PedidoCabDTO dto) {
        PedidoCab apCa = new PedidoCab();
        BeanUtils.copyProperties(dto, apCa);
        apCa.setProveedor(Proveedor.fromProveedorDTO(dto.getProveedor()));
//        apCa.setCaja(Caja.fromCajaDTO(dto.getCaja()));
//        apCa.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(dto
//                .getUsuarioCajero()));
        return apCa;
    }

}
