package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.GastosDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the apertura_caja database table.
 *
 */
@Entity
@Table(name = "gatos", schema = "factura_cliente")
@NamedQuery(name = "Gastos.findAll", query = "SELECT a FROM Gastos a")
public class Gastos implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_gasto")
    private Long idGasto;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    @Column(name = "factura")
    private String factura;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "empresa")
    private String empresa;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "monto")
    private int monto;

    public Gastos() {
    }

    public Long getIdGasto() {
        return idGasto;
    }

    public void setIdGasto(Long idGasto) {
        this.idGasto = idGasto;
    }

    public String getFactura() {
        return factura;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public Date getFecha() {
        return fecha;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public GastosDTO toGastosDTO() {
        GastosDTO apCajaDTO = this.toGastosDTO(this);
        apCajaDTO.setProveedor(null);
        return apCajaDTO;
    }
    public GastosDTO toGastosProveedorDTO() {
        GastosDTO apCajaDTO = this.toGastosDTO(this);
        if (this.proveedor != null) {
            Proveedor fac = this.getProveedor();
            apCajaDTO.setProveedor(fac.toProveedorBdDTO());
        }
        return apCajaDTO;
    }

    public GastosDTO toGastosDTO(Gastos ap) {
        GastosDTO acDTO = new GastosDTO();
        BeanUtils.copyProperties(ap, acDTO);
        return acDTO;
    }

    // METODO QUE
    // Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
    // CAJERO Y USUARIO SUPERVISOR
    public static Gastos fromGastosDTO(GastosDTO dto) {
        Gastos apCa = new Gastos();
        BeanUtils.copyProperties(dto, apCa);
        apCa.setProveedor(Proveedor.fromProveedorDTO(dto
                .getProveedor()));
        return apCa;
    }

}
