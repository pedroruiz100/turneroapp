package com.peluqueria.core.domain;

import com.peluqueria.dto.CancelacionProductoDTO;
import com.peluqueria.dto.MotivoCancelacionProductoDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the motivo_cancelacion_producto database table.
 *
 */
@Entity
@Table(name = "motivo_cancelacion_producto", schema = "caja")
@NamedQuery(name = "MotivoCancelacionProducto.findAll", query = "SELECT m FROM MotivoCancelacionProducto m")
public class MotivoCancelacionProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motivo_cancel_prod")
    private Long idMotivoCancelProd;

    @Column(name = "descripcion_motivo_cancel_prod")
    private String descripcionMotivoCancelProd;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to CancelacionProducto
    @OneToMany(mappedBy = "motivoCancelacionProducto")
    private List<CancelacionProducto> cancelacionProductos;

    public MotivoCancelacionProducto() {
    }

    public Long getIdMotivoCancelProd() {
        return this.idMotivoCancelProd;
    }

    public void setIdMotivoCancelProd(Long idMotivoCancelProd) {
        this.idMotivoCancelProd = idMotivoCancelProd;
    }

    public String getDescripcionMotivoCancelProd() {
        return this.descripcionMotivoCancelProd;
    }

    public void setDescripcionMotivoCancelProd(
            String descripcionMotivoCancelProd) {
        this.descripcionMotivoCancelProd = descripcionMotivoCancelProd;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CancelacionProducto> getCancelacionProductos() {
        return this.cancelacionProductos;
    }

    public void setCancelacionProductos(
            List<CancelacionProducto> cancelacionProductos) {
        this.cancelacionProductos = cancelacionProductos;
    }

    public MotivoCancelacionProductoDTO toMotivoCancelacionProductoDTO() {
        MotivoCancelacionProductoDTO motDTO = toMotivoCancelacionProductoDTO(this);
        if (!this.cancelacionProductos.isEmpty()) {
            List<CancelacionProductoDTO> cancelDTO = new ArrayList<CancelacionProductoDTO>();
            List<CancelacionProducto> cancel = new ArrayList<CancelacionProducto>();
            for (CancelacionProducto can : cancel) {
                cancelDTO.add(can.toBDCancelacionProductoDTO());
            }
            motDTO.setCancelacionProductos(cancelDTO);
        }
        return motDTO;
    }

    public MotivoCancelacionProductoDTO toBDMotivoCancelacionProductoDTO() {
        MotivoCancelacionProductoDTO motDTO = toMotivoCancelacionProductoDTO(this);
        motDTO.setCancelacionProductos(null);
        return motDTO;
    }

    public static MotivoCancelacionProductoDTO toMotivoCancelacionProductoDTO(
            MotivoCancelacionProducto motivoCancelacionProducto) {
        MotivoCancelacionProductoDTO motDTO = new MotivoCancelacionProductoDTO();
        BeanUtils.copyProperties(motivoCancelacionProducto, motDTO);
        return motDTO;
    }

    public static MotivoCancelacionProducto fromMotivoCancelacionProductoDTO(MotivoCancelacionProductoDTO motDTO) {
        MotivoCancelacionProducto mot = new MotivoCancelacionProducto();
        BeanUtils.copyProperties(motDTO, mot);
        return mot;
    }

}
