package com.peluqueria.core.domain;

import com.peluqueria.dto.CancelacionFacturaDTO;
import com.peluqueria.dto.MotivoCancelacionFacturaDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the motivo_cancelacion_factura database table.
 *
 */
@Entity
@Table(name = "motivo_cancelacion_factura", schema = "caja")
@NamedQuery(name = "MotivoCancelacionFactura.findAll", query = "SELECT m FROM MotivoCancelacionFactura m")
public class MotivoCancelacionFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motivo_cancel_fact")
    private Long idMotivoCancelFact;

    @Column(name = "descripcion_motivo_cancel_fact")
    private String descripcionMotivoCancelFact;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "motivoCancelacionFactura", fetch = FetchType.LAZY)
    private List<CancelacionFactura> cancelacionFacturas;

    public MotivoCancelacionFactura() {
    }

    public Long getIdMotivoCancelFact() {
        return this.idMotivoCancelFact;
    }

    public void setIdMotivoCancelFact(Long idMotivoCancelFact) {
        this.idMotivoCancelFact = idMotivoCancelFact;
    }

    public String getDescripcionMotivoCancelFact() {
        return this.descripcionMotivoCancelFact;
    }

    public void setDescripcionMotivoCancelFact(
            String descripcionMotivoCancelFact) {
        this.descripcionMotivoCancelFact = descripcionMotivoCancelFact;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<CancelacionFactura> getCancelacionFacturas() {
        return this.cancelacionFacturas;
    }

    public void setCancelacionFacturas(
            List<CancelacionFactura> cancelacionFacturas) {
        this.cancelacionFacturas = cancelacionFacturas;
    }

    public MotivoCancelacionFacturaDTO toBDMotivoCancelacionFacturaDTO() {
        MotivoCancelacionFacturaDTO motDTO = toMotivoCancelacionFacturaDTO(this);
        motDTO.setCancelacionFacturas(null);
        return motDTO;
    }

    public MotivoCancelacionFacturaDTO toMotivoCancelacionFacturaDTO() {
        MotivoCancelacionFacturaDTO motDTO = toMotivoCancelacionFacturaDTO(this);
        if (!this.cancelacionFacturas.isEmpty()) {
            List<CancelacionFactura> cancel = new ArrayList<CancelacionFactura>();
            List<CancelacionFacturaDTO> cancelDTO = new ArrayList<CancelacionFacturaDTO>();
            for (CancelacionFactura can : cancel) {
                cancelDTO.add(can.toBDCancelacionFacturaDTO());
            }
            motDTO.setCancelacionFacturas(cancelDTO);
        }
        return motDTO;
    }

    public static MotivoCancelacionFacturaDTO toMotivoCancelacionFacturaDTO(
            MotivoCancelacionFactura motivoCancelacionFactura) {
        MotivoCancelacionFacturaDTO motDTO = new MotivoCancelacionFacturaDTO();
        BeanUtils.copyProperties(motivoCancelacionFactura, motDTO);
        return motDTO;
    }

    public static MotivoCancelacionFactura fromMotivoCancelacionFacturaDTO(
            MotivoCancelacionFacturaDTO motDTO) {
        MotivoCancelacionFactura mot = new MotivoCancelacionFactura();
        BeanUtils.copyProperties(motDTO, mot);
        return mot;
    }

}
