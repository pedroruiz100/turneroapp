package com.peluqueria.core.domain;

import com.peluqueria.dto.DescValeDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_vale database table.
 *
 */
@Entity
@Table(name = "desc_vale", schema = "factura_cliente")
@NamedQuery(name = "DescVale.findAll", query = "SELECT d FROM DescVale d")
public class DescVale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_vale")
    private Long idDescVale;

    // bi-directional many-to-one association to Vale
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_vale")
    private Vales vale;

    @Column(name = "monto_desc_vale")
    private Integer montoDescVale;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public DescVale() {
    }

    public Long getIdDescVale() {
        return this.idDescVale;
    }

    public void setIdDescVale(Long idDescVale) {
        this.idDescVale = idDescVale;
    }

    public Integer getMontoDescVale() {
        return this.montoDescVale;
    }

    public void setMontoDescVale(Integer montoDescVale) {
        this.montoDescVale = montoDescVale;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Vales getVale() {
        return vale;
    }

    public void setVale(Vales vale) {
        this.vale = vale;
    }

    public DescValeDTO toDescValeDTO() {
        DescValeDTO descDTO = toDescValeDTO(this);
        if (this.facturaClienteCab != null) {
            descDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        if (this.vale != null) {
            descDTO.setVale(this.getVale().toValeDTO());
        }
        return descDTO;
    }

    public DescValeDTO toBDDescValeDTO() {
        DescValeDTO descDTO = toDescValeDTO(this);
        descDTO.setFacturaClienteCab(null);
        descDTO.setVale(null);
        return descDTO;
    }

    public static DescValeDTO toDescValeDTO(DescVale descVale) {
        DescValeDTO descDTO = new DescValeDTO();
        BeanUtils.copyProperties(descVale, descDTO);
        return descDTO;
    }

    public static DescVale fromDescValeDTO(DescValeDTO descDTO) {
        DescVale desc = new DescVale();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public static DescVale fromDescValeAsociadoDTO(DescValeDTO descDTO) {
        DescVale desc = fromDescValeDTO(descDTO);
        desc.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(descDTO.getFacturaClienteCab()));
        desc.setVale(Vales.fromValeDTO(descDTO.getVale()));
        return desc;
    }

}
