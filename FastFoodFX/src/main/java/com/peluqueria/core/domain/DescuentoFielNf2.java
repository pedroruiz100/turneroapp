package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf2DTO;

/**
 * The persistent class for the descuento_fiel_nf2 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf2", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf2.findAll", query = "SELECT d FROM DescuentoFielNf2 d")
public class DescuentoFielNf2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf2")
    private Long idDescuentoFielNf2;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    public DescuentoFielNf2() {
    }

    public Long getIdDescuentoFielNf2() {
        return this.idDescuentoFielNf2;
    }

    public void setIdDescuentoFielNf2(Long idDescuentoFielNf2) {
        this.idDescuentoFielNf2 = idDescuentoFielNf2;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf2 fromDescuentoFielNf2DTO(DescuentoFielNf2DTO descuentoFielNf2DTO) {
        DescuentoFielNf2 descuentoFielNf2 = new DescuentoFielNf2();
        BeanUtils.copyProperties(descuentoFielNf2DTO, descuentoFielNf2);
        return descuentoFielNf2;
    }

    // <-- SERVER
    public static DescuentoFielNf2DTO toDescuentoFielNf2DTO(DescuentoFielNf2 descuentoFielNf2) {
        DescuentoFielNf2DTO descuentoFielNf2DTO = new DescuentoFielNf2DTO();
        BeanUtils.copyProperties(descuentoFielNf2, descuentoFielNf2DTO);
        return descuentoFielNf2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf2 fromDescuentoFielNf2DTOEntitiesFull(DescuentoFielNf2DTO descuentoFielNf2DTO) {
        DescuentoFielNf2 descuentoFielNf2 = fromDescuentoFielNf2DTO(descuentoFielNf2DTO);
        if (descuentoFielNf2DTO.getNf2Sfamilia() != null) {
            descuentoFielNf2.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(descuentoFielNf2DTO.getNf2Sfamilia()));
        }
        if (descuentoFielNf2DTO.getDescuentoFielCab() != null) {
            descuentoFielNf2.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf2DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf2;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf2DTO toDescuentoFielNf2DTOEntityFull() {
        DescuentoFielNf2DTO descuentoFielNf2DTO = toDescuentoFielNf2DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf2DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf2DTO.setDescuentoFielCab(null);
        }
        if (this.getNf2Sfamilia() != null) {
            descuentoFielNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNull());
        } else {
            descuentoFielNf2DTO.setNf2Sfamilia(null);
        }
        return descuentoFielNf2DTO;
    }

    // <-- SERVER
    public DescuentoFielNf2DTO toDescuentoFielNf2DTOEntityNull() {
        DescuentoFielNf2DTO descuentoFielNf2DTO = toDescuentoFielNf2DTO(this);
        descuentoFielNf2DTO.setDescuentoFielCab(null);
        descuentoFielNf2DTO.setNf2Sfamilia(null);
        return descuentoFielNf2DTO;
    }

    // <-- SERVER
    public DescuentoFielNf2DTO toDescuentoFielNf2DTONf1() {
        DescuentoFielNf2DTO descuentoFielNf2DTO = toDescuentoFielNf2DTO(this);
        descuentoFielNf2DTO.setDescuentoFielCab(null);
        if (this.getNf2Sfamilia() != null) {
            descuentoFielNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            descuentoFielNf2DTO.setNf2Sfamilia(null);
        }
        return descuentoFielNf2DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
