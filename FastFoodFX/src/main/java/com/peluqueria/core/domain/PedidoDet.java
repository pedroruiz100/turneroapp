package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaCompraDetDTO;
import com.peluqueria.dto.PedidoDetDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "pedido_det", schema = "factura_cliente")
@NamedQuery(name = "PedidoDet.findAll", query = "SELECT f FROM PedidoDet f")
public class PedidoDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido_det")
    private Long idPedidoDet;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_cab")
    private PedidoCab pedidoCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "precio")
    private Long precio;

    @Column(name = "descuento")
    private String descuento;

    @Column(name = "cant_central")
    private String cantCentral;

    @Column(name = "cant_cacique")
    private String cantCacique;

    @Column(name = "cant_sanlo")
    private String cantSanlo;

    public PedidoDet() {
    }

    public Long getIdPedidoDet() {
        return idPedidoDet;
    }

    public void setIdPedidoDet(Long idPedidoDet) {
        this.idPedidoDet = idPedidoDet;
    }

    public PedidoCab getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCab pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getCantCentral() {
        return cantCentral;
    }

    public void setCantCentral(String cantCentral) {
        this.cantCentral = cantCentral;
    }

    public String getCantCacique() {
        return cantCacique;
    }

    public void setCantCacique(String cantCacique) {
        this.cantCacique = cantCacique;
    }

    public String getCantSanlo() {
        return cantSanlo;
    }

    public void setCantSanlo(String cantSanlo) {
        this.cantSanlo = cantSanlo;
    }

    public PedidoDetDTO toPedidoDetDTO() {
        PedidoDetDTO facDTO = toPedidoDetDTO(this);
        if (this.pedidoCab != null) {
            facDTO.setPedidoCab(this.getPedidoCab()
                    .toPedidoCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return facDTO;
    }

    public PedidoDetDTO toDescriPedidoDetDTO() {
        PedidoDetDTO facDTO = toPedidoDetDTO(this);
        facDTO.setPedidoCab(null);
        facDTO.setArticulo(null);
        return facDTO;
    }

    public static PedidoDetDTO toPedidoDetDTO(
            PedidoDet facturaClienteDet) {
        PedidoDetDTO facDTO = new PedidoDetDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static PedidoDet fromPedidoDetDTO(
            PedidoDetDTO facDTO) {
        PedidoDet fac = new PedidoDet();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static PedidoDet fromPedidoDetAsociado(
            PedidoDetDTO facDTO) {
        PedidoDet fac = fromPedidoDetDTO(facDTO);
        fac.setPedidoCab(PedidoCab.fromPedidoCabDTO(facDTO.getPedidoCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }
}
