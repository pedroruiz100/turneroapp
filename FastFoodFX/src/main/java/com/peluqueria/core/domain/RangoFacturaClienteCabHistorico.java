/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import com.peluqueria.dto.RangoFacturaClienteCabHistoricoDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author ExcelsisWalker
 */
@Entity
@Table(name = "rango_factura_cliente_cab_historico", schema = "general")
@NamedQuery(name = "RangoFacturaClienteCabHistorico.findAll", query = "SELECT rfh FROM RangoFacturaClienteCabHistorico rfh")
public class RangoFacturaClienteCabHistorico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rango")
    private Long idRango;

    @Column(name = "rango_inicial")
    private Long rangoInicial;

    @Column(name = "rango_final")
    private Long rangoFinal;

    @Column(name = "rango_actual")
    private Long rangoActual;

    public RangoFacturaClienteCabHistorico() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

    public RangoFacturaClienteCabHistoricoDTO toRangoFacturaDTO() {
        RangoFacturaClienteCabHistoricoDTO rangoDTO = toRangoFacturaClienteCabHistoricoDTO(this);
        return rangoDTO;
    }

    public static RangoFacturaClienteCabHistoricoDTO toRangoFacturaClienteCabHistoricoDTO(RangoFacturaClienteCabHistorico rango) {
        RangoFacturaClienteCabHistoricoDTO dDTO = new RangoFacturaClienteCabHistoricoDTO();
        BeanUtils.copyProperties(rango, dDTO);
        return dDTO;
    }

    public static RangoFacturaClienteCabHistorico fromRangoFacturaClienteCabHistoricoDTO(RangoFacturaClienteCabHistoricoDTO rangoDTO) {
        RangoFacturaClienteCabHistorico rango = new RangoFacturaClienteCabHistorico();
        BeanUtils.copyProperties(rangoDTO, rango);
        return rango;
    }
}
