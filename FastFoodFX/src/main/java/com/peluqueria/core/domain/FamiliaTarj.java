package com.peluqueria.core.domain;

import com.peluqueria.dto.FamiliaTarjDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the familia_tarj database table.
 *
 */
@Entity
@Table(name = "familia_tarj", schema = "cuenta")
@NamedQuery(name = "FamiliaTarj.findAll", query = "SELECT f FROM FamiliaTarj f")
public class FamiliaTarj implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_familia_tarj")
    private Long idFamiliaTarj;

    private Boolean activo;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    // bi-directional many-to-one association to Barrio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_barrio")
    private Barrio barrio;

    // bi-directional many-to-one association to Ciudad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ciudad")
    private Ciudad ciudad;

    @Column(name = "nro_local")
    private Integer nroLocal;

    @Column(name = "primera_lateral")
    private String primeraLateral;

    @Column(name = "segunda_lateral")
    private String segundaLateral;

    private String telefono;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Tarjeta
    @OneToMany(mappedBy = "familiaTarj")
    private List<Tarjeta> tarjetas;

    // bi-directional many-to-one association to TarjetaConvenio
    @OneToMany(mappedBy = "familiaTarj")
    private List<TarjetaConvenio> tarjetaConvenios;

    public FamiliaTarj() {
    }

    public Long getIdFamiliaTarj() {
        return this.idFamiliaTarj;
    }

    public void setIdFamiliaTarj(Long idFamiliaTarj) {
        this.idFamiliaTarj = idFamiliaTarj;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Tarjeta> getTarjetas() {
        return this.tarjetas;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public List<TarjetaConvenio> getTarjetaConvenios() {
        return this.tarjetaConvenios;
    }

    public void setTarjetaConvenios(List<TarjetaConvenio> tarjetaConvenios) {
        this.tarjetaConvenios = tarjetaConvenios;
    }

    public Barrio getBarrio() {
        return barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    // Solo Familia Tarjeta
    public FamiliaTarjDTO toFamiliaTarjDTO() {
        FamiliaTarjDTO faDTO = toFamiliaTarjDTO(this);
        if (this.barrio != null) {
            faDTO.setBarrio(this.getBarrio().toBarrioDTOAFamiliaTarjDTO());
        }
        if (this.ciudad != null) {
            faDTO.setCiudad(this.getCiudad().toCiudadSinOtrosDatos());
        }
        faDTO.setTarjetaConvenios(null);
        faDTO.setTarjetas(null);
        return faDTO;
    }

    public FamiliaTarjDTO toBDFamiliaTarjDTO() {
        FamiliaTarjDTO faDTO = toFamiliaTarjDTO(this);
        faDTO.setBarrio(null);
        faDTO.setCiudad(null);
        faDTO.setTarjetaConvenios(null);
        faDTO.setTarjetas(null);
        return faDTO;
    }

    public static FamiliaTarjDTO toFamiliaTarjDTO(FamiliaTarj familia) {
        FamiliaTarjDTO tarDTO = new FamiliaTarjDTO();
        BeanUtils.copyProperties(familia, tarDTO);
        return tarDTO;
    }

    public static FamiliaTarj fromFamiliaTarjDTO(FamiliaTarjDTO faDTO) {
        FamiliaTarj fa = new FamiliaTarj();
        BeanUtils.copyProperties(faDTO, fa);
        return fa;
    }

    public static FamiliaTarj fromFamiliaTarjAsociadoDTO(FamiliaTarjDTO faDTO) {
        FamiliaTarj fa = fromFamiliaTarjDTO(faDTO);
        fa.setBarrio(Barrio.fromBarrioDTO(faDTO.getBarrio()));
        fa.setCiudad(Ciudad.fromCiudadDTO(faDTO.getCiudad()));
        return fa;
    }

}
