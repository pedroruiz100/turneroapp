package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.FuncionarioDTO;
import com.peluqueria.dto.ServPendienteDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rol database table.
 *
 */
@Entity
@Table(name = "serv_pendiente", schema = "estetica")
@NamedQuery(name = "ServPendiente.findAll", query = "SELECT r FROM ServPendiente r")
public class ServPendiente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_serv_pendiente")
    private Long idServPendiente;

    // bi-directional many-to-one association to ClientePendiente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente_pendiente")
    private ClientePendiente clientePendiente;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to Funcionario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario")
    private Funcionario funcionario;

    @Column(name = "monto_serv")
    private Integer montoServ;

    @Column(name = "cantidad")
    private Integer cantidad;

    @Column(name = "monto_comision")
    private Integer montoComision;

    @Column(name = "comision_porc")
    private Boolean comisionPorc;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "poriva")
    private Integer poriva;

    @Column(name = "cod_articulo")
    private Long codArticulo;

    @Column(name = "porc")
    private Integer porc;

    public ServPendiente() {
    }

    public Long getIdServPendiente() {
        return idServPendiente;
    }

    public void setIdServPendiente(Long idServPendiente) {
        this.idServPendiente = idServPendiente;
    }

    public ClientePendiente getClientePendiente() {
        return clientePendiente;
    }

    public void setClientePendiente(ClientePendiente clientePendiente) {
        this.clientePendiente = clientePendiente;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getPoriva() {
        return poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public Integer getPorc() {
        return porc;
    }

    public void setPorc(Integer porc) {
        this.porc = porc;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getMontoServ() {
        return montoServ;
    }

    public void setMontoServ(Integer montoServ) {
        this.montoServ = montoServ;
    }

    public Integer getMontoComision() {
        return montoComision;
    }

    public void setMontoComision(Integer montoComision) {
        this.montoComision = montoComision;
    }

    public Boolean getComisionPorc() {
        return comisionPorc;
    }

    public void setComisionPorc(Boolean comisionPorc) {
        this.comisionPorc = comisionPorc;
    }

    public ServPendienteDTO toServPendienteDTO() {
        ServPendienteDTO servDTO = toServPendienteDTO(this);
        if (this.clientePendiente != null) {
            servDTO.setClientePendiente(this.getClientePendiente()
                    .toBDClientePendienteDTO());
        }
        if (this.articulo != null) {
            servDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        if (this.funcionario != null) {
            servDTO.setFuncionario(this.getFuncionario().toFuncionarioDTO());
        }
        return servDTO;
    }

    public ServPendienteDTO toServPendienteSinCliPenDTO() {
        ServPendienteDTO servDTO = toServPendienteDTO(this);
        servDTO.setClientePendiente(null);
        try {
            if (this.articulo.getIdArticulo() != null) {
                //solo setea el id correspondiente para utilizarlo en estetica.serv_pendiente
                ArticuloDTO artDTO = new ArticuloDTO();
                artDTO.setIdArticulo(this.getArticulo().toBDArticuloDTO().getIdArticulo());
                servDTO.setArticulo(artDTO);
            }
        } catch (Exception e) {
        } finally {
        }
        try {
            if (this.funcionario.getIdFuncionario() != null) {
                //solo setea el id correspondiente para utilizarlo en estetica.serv_pendiente
                FuncionarioDTO funDTO = new FuncionarioDTO();
                funDTO.setIdFuncionario(this.getFuncionario().toFuncionarioDTO().getIdFuncionario());
                servDTO.setFuncionario(funDTO);
            }
        } catch (Exception e) {
        } finally {
        }
        return servDTO;
    }

    private ServPendienteDTO toServPendienteDTO(ServPendiente servPendiente) {
        ServPendienteDTO servDTO = new ServPendienteDTO();
        BeanUtils.copyProperties(servPendiente, servDTO);
        return servDTO;
    }

    public static ServPendiente fromServPendiente(ServPendienteDTO servDTO) {
        ServPendiente serv = new ServPendiente();
        BeanUtils.copyProperties(servDTO, serv);
        return serv;
    }

    public static ServPendiente fromServPendienteAsociado(
            ServPendienteDTO servDTO) {
        ServPendiente serv = fromServPendiente(servDTO);
        serv.setClientePendiente(ClientePendiente.fromClientePendiente(servDTO
                .getClientePendiente()));
        serv.setArticulo(Articulo.fromArticuloDTO(servDTO.getArticulo()));
        serv.setFuncionario(Funcionario.fromFuncionarioDTO(servDTO
                .getFuncionario()));
        return serv;
    }

}
