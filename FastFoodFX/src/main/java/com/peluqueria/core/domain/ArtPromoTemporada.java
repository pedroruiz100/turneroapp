package com.peluqueria.core.domain;

import com.peluqueria.dto.ArtPromoTemporadaDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

@Entity
@Table(name = "art_promo_temporada", schema = "cuenta")
@NamedQuery(name = "ArtPromoTemporada.findAll", query = "SELECT a FROM ArtPromoTemporada a")
public class ArtPromoTemporada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_art_promo_temporada")
    private Long idArtPromoTemporada;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada_art")
    private PromoTemporadaArt promoTemporadaArt;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    @Column(name = "descri_articulo")
    private String descriArticulo;

    public ArtPromoTemporada() {
    }

    public Long getIdArtPromoTemporada() {
        return idArtPromoTemporada;
    }

    public void setIdArtPromoTemporada(Long idArtPromoTemporada) {
        this.idArtPromoTemporada = idArtPromoTemporada;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public PromoTemporadaArt getPromoTemporadaArt() {
        return promoTemporadaArt;
    }

    public void setPromoTemporadaArt(PromoTemporadaArt promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getDescriArticulo() {
        return descriArticulo;
    }

    public void setDescriArticulo(String descriArticulo) {
        this.descriArticulo = descriArticulo;
    }

    public ArtPromoTemporadaDTO toSinArtPromoTemporadaDTO() {
        ArtPromoTemporadaDTO artDTO = toArtPromoTemporadaDTO(this);
        if (this.articulo != null) {
            artDTO.setArticulo(this.getArticulo().toArticuloDTO());
        }
        artDTO.setPromoTemporadaArt(null);
        return artDTO;
    }

    // Con Promo Temporada
    public ArtPromoTemporadaDTO toArtPromoTemporadaDTO() {
        ArtPromoTemporadaDTO artDTO = toArtPromoTemporadaDTO(this);
        if (this.articulo != null) {
            artDTO.setArticulo(this.getArticulo().toArticuloDTO());
        }
        if (this.promoTemporadaArt != null) {
            artDTO.setPromoTemporadaArt(this.getPromoTemporadaArt()
                    .toPromoTemporadaArtSinArtDTO());
        }
        return artDTO;
    }

    public ArtPromoTemporadaDTO toArtPromoTemporadaDTO(
            ArtPromoTemporada spt) {
        ArtPromoTemporadaDTO sptDTO = new ArtPromoTemporadaDTO();
        BeanUtils.copyProperties(spt, sptDTO);
        return sptDTO;
    }

    public static ArtPromoTemporada fromArtPromoTemporada(
            ArtPromoTemporadaDTO aptDTO) {
        ArtPromoTemporada spt = new ArtPromoTemporada();
        BeanUtils.copyProperties(aptDTO, spt);
        spt.setArticulo(Articulo.fromArticuloDTOAsociado(aptDTO.getArticulo()));
        spt.setPromoTemporadaArt(PromoTemporadaArt.fromPromoTemporadaArtDTO(aptDTO
                .getPromoTemporadaArt()));
        return spt;
    }

}
