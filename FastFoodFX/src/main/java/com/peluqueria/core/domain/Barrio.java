package com.peluqueria.core.domain;

import com.peluqueria.dto.BarrioDTO;
import com.peluqueria.dto.SucursalDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the barrio database table.
 *
 */
@Entity
@Table(name = "barrio", schema = "general")
@NamedQuery(name = "Barrio.findAll", query = "SELECT b FROM Barrio b")
public class Barrio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_barrio")
    private Long idBarrio;

    private Boolean activo;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Ciudad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ciudad")
    private Ciudad ciudad;

    // bi-directional many-to-one association to Sucursal
    @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
    private List<Sucursal> sucursals;

    // bi-directional many-to-one association to Cliente
    @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
    private List<Cliente> clientes;

    // bi-directional many-to-one association to FamiliaTarj
    @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
    private List<FamiliaTarj> familiaTarjs;

    // bi-directional many-to-one association to Proveedor
    @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
    private List<Proveedor> proveedors;

    public Barrio() {
    }

    public Long getIdBarrio() {
        return this.idBarrio;
    }

    public void setIdBarrio(Long idBarrio) {
        this.idBarrio = idBarrio;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Ciudad getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public List<Sucursal> getSucursals() {
        return this.sucursals;
    }

    public void setSucursals(List<Sucursal> sucursals) {
        this.sucursals = sucursals;
    }

    // ****************************************************************************************
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    // ****************************************************************************************
    public List<FamiliaTarj> getFamiliaTarjs() {
        return familiaTarjs;
    }

    public void setFamiliaTarjs(List<FamiliaTarj> familiaTarjs) {
        this.familiaTarjs = familiaTarjs;
    }

    // ****************************************************************************************
    public List<Proveedor> getProveedors() {
        return proveedors;
    }

    public void setProveedors(List<Proveedor> proveedors) {
        this.proveedors = proveedors;
    }

    // FIN PARA FAMILIA TARJETA
    // Este metodo nos sirve para que cargue las listas y las Clases que tiene
    // esta entidad
    // en este caso la clase Ciudad y la lista de Sucursales
    public BarrioDTO toBarrioDTO() {
        // convertimos los datos recuperados a DTO
        BarrioDTO barrioDTO = toBarrioDTO(this);
        // Verificamos que hay una Clase (entidad relacionada con Barrio, en
        // este caso Ciudad)
        // verificamos que los datos de la consulta no nos traiga ciudad con
        // valor null
        if (this.getCiudad() != null) {
            // Creamos una variable para cargar los datos recuperados
            Ciudad ciudad = this.getCiudad();
            // Colocar null a los campos que vuelve hacer referencia en los list
            // de la clase Ciudad
            // Para este caso Barrio y Sucursal
            ciudad.setBarrios(null);
            ciudad.setSucursals(null);
            ciudad.setClientes(null);
            ciudad.setFamiliaTarjs(null);
            ciudad.setProveedors(null);
            // ciudad.toCiudadDTO() este metodo convierte a DTO las entidades
            // recuperadas en la consulta
            // barrioDTO.setCiudad(ciudad.toCiudadDTO()); a BarrioDTO le agrega
            // la ciudadDTO que se obtuvo con el metodo explicado una linea
            // arriba
            barrioDTO.setCiudadDTO(ciudad.toCiudadDTO());
        }

        // Verificamos que la lista de Sucursales no es vacia
        if (!this.getSucursals().isEmpty()) {
            // Recorremos los datos obtenidos de sucursales por medio de un for
            // each
            for (Sucursal sucu : this.getSucursals()) {
                // cada vez que haya un recorrido se inicializa un List de
                // SucursalDTO para pasarlo al barrioDTO
                List<SucursalDTO> sDTO = new ArrayList<SucursalDTO>();
                // *************************//
                // Anteriormente teniamos de esta manera, esto nos servia para
                // que a la hora de llamarle a la lista de sucursal podamos a
                // traves de este
                // recuperar las entidades de Barrio, pero esto sería redundante
                // entonces lo dejamos en null
                // OBSERVACION : recordar que es conveniente utilizar este
                // metodo en el caso que la idea sea recuperar entidades de otra
                // clase por medio de listas
                // como ejemplo en este caso dentro de la lista sucursal, datos
                // de la clase Barrio
                // sucu.toSucursalDTO(this.toBarrioDTO(this)
                // *************************//
                // sucu.toSucursalDTO(null) este metodo nos sirve para indicar
                // que entidad son las que podemos obtener de esa lista, en este
                // caso si quisieramos obtener
                // las entidades de Barrio y TalonariosSucursales podemos
                // solicitarle, caso contrario si queremos que no nos traiga
                // colocar null, como lo hicimos en este caso
                // sDTO.add(sucu.toSucursalDTO(null)); agregamos al array sDTO
                // creado un poco mas arriba
                sDTO.add(sucu.toSucursalDescriDTO());
                // esto agregamos a setSucursals(sDTO)
                barrioDTO.setSucursalsDTOs(sDTO);
            }
            barrioDTO.setClientesDTO(null);
            barrioDTO.setFamiliaTarjDTO(null);
            barrioDTO.setProveedorDTO(null);
        }
        return barrioDTO;
    }

    // Metodo que sirve para cargar los datos de Barrio a BarrioDTO
    public static BarrioDTO toBarrioDTO(Barrio barrio) {
        BarrioDTO barrioDTO = new BarrioDTO();
        BeanUtils.copyProperties(barrio, barrioDTO);
        return barrioDTO;
    }

    public static Barrio fromBarrioDTO(BarrioDTO barrioDTO) {
        Barrio barrio = new Barrio();
        BeanUtils.copyProperties(barrioDTO, barrio);
        return barrio;
    }

    public static Barrio fromBarrioDTOAsociado(BarrioDTO barrioDTO) {
        Barrio bar = fromBarrioDTO(barrioDTO);
        bar.setCiudad(Ciudad.fromCiudadDTO(barrioDTO.getCiudadDTO()));
        return bar;
    }

    // BARRIO CLIENTE
    public BarrioDTO toBarrioAClienteDTO() {
        BarrioDTO barrioDTO = toBarrioDTO(this);
        barrioDTO.setCiudadDTO(null);
        barrioDTO.setSucursalsDTOs(null);
        barrioDTO.setClientesDTO(null);
        barrioDTO.setFamiliaTarjDTO(null);
        barrioDTO.setProveedorDTO(null);
        return barrioDTO;
    }

    // FIN DE BARRIO A CLIENTE
    // PARA FAMILIA TARJETA
    public BarrioDTO toBarrioDTOAFamiliaTarjDTO() {
        BarrioDTO barrioDTO = toBarrioDTO(this);
        barrioDTO.setCiudadDTO(null);
        barrioDTO.setClientesDTO(null);
        barrioDTO.setFamiliaTarjDTO(null);
        barrioDTO.setProveedorDTO(null);
        barrioDTO.setSucursalsDTOs(null);
        return barrioDTO;
    }
}
