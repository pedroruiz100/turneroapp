/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 *
 * @author ExcelsisWalker
 */
@Entity
@Table(name = "descuento_fiel_formas_de_pago", schema = "descuento")
@NamedQuery(name = "DescuentoFielFormasDePago.findAll", query = "SELECT d FROM DescuentoFielFormasDePago d")
public class DescuentoFielFormasDePago implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_formas_de_pago")
    private Long idDescuentoFielFormasDePago;

    @Column(name = "efectivo")
    private Boolean efectivo;

    @Column(name = "tarjeta")
    private Boolean tarjeta;

    @Column(name = "cheque")
    private Boolean cheque;

    @Column(name = "mon_extr")
    private Boolean monExtr;

    @Column(name = "nota_cred")
    private Boolean notaCred;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "vale")
    private Boolean vale;

    @Column(name = "bajada")
    private Boolean bajada;

    public Long getIdDescuentoFielFormasDePago() {
        return idDescuentoFielFormasDePago;
    }

    public void setIdDescuentoFielFormasDePago(Long idDescuentoFielFormasDePago) {
        this.idDescuentoFielFormasDePago = idDescuentoFielFormasDePago;
    }

    public Boolean getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(Boolean efectivo) {
        this.efectivo = efectivo;
    }

    public Boolean getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Boolean tarjeta) {
        this.tarjeta = tarjeta;
    }

    public Boolean getCheque() {
        return cheque;
    }

    public void setCheque(Boolean cheque) {
        this.cheque = cheque;
    }

    public Boolean getMonExtr() {
        return monExtr;
    }

    public void setMonExtr(Boolean monExtr) {
        this.monExtr = monExtr;
    }

    public Boolean getNotaCred() {
        return notaCred;
    }

    public void setNotaCred(Boolean notaCred) {
        this.notaCred = notaCred;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getVale() {
        return vale;
    }

    public void setVale(Boolean vale) {
        this.vale = vale;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }
}
