package com.peluqueria.core.domain;

import com.peluqueria.dto.ArtPromoTemporadaObsequioDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

@Entity
@Table(name = "art_promo_temporada_obsequio", schema = "descuento")
@NamedQuery(name = "ArtPromoTemporadaObsequio.findAll", query = "SELECT a FROM ArtPromoTemporadaObsequio a")
public class ArtPromoTemporadaObsequio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_art_promo_temporada_obsequio")
    private Long idArtPromoTemporadaObsequio;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada_art")
    private PromoTemporadaArt promoTemporadaArt;

    @Column(name = "min_req")
    private Integer minReq;

    @Column(name = "cant_obsequio")
    private Integer cantObsequio;

    @Column(name = "descri_articulo")
    private String descriArticulo;

    public ArtPromoTemporadaObsequio() {
    }

    public Long getIdArtPromoTemporadaObsequio() {
        return idArtPromoTemporadaObsequio;
    }

    public void setIdArtPromoTemporadaObsequio(Long idArtPromoTemporadaObsequio) {
        this.idArtPromoTemporadaObsequio = idArtPromoTemporadaObsequio;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public PromoTemporadaArt getPromoTemporadaArt() {
        return promoTemporadaArt;
    }

    public void setPromoTemporadaArt(PromoTemporadaArt promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public String getDescriArticulo() {
        return descriArticulo;
    }

    public void setDescriArticulo(String descriArticulo) {
        this.descriArticulo = descriArticulo;
    }

    public Integer getMinReq() {
        return minReq;
    }

    public void setMinReq(Integer minReq) {
        this.minReq = minReq;
    }

    public Integer getCantObsequio() {
        return cantObsequio;
    }

    public void setCantObsequio(Integer cantObsequio) {
        this.cantObsequio = cantObsequio;
    }

    public ArtPromoTemporadaObsequioDTO toSinArtPromoTemporadaObsequioDTO() {
        ArtPromoTemporadaObsequioDTO artDTO = toArtPromoTemporadaObsequioDTO(this);
        if (this.articulo != null) {
            artDTO.setArticulo(this.getArticulo().toArticuloDTO());
        }
        artDTO.setPromoTemporadaArt(null);
        return artDTO;
    }

    // Con Promo Temporada
    public ArtPromoTemporadaObsequioDTO toArtPromoTemporadaObsequioDTO() {
        ArtPromoTemporadaObsequioDTO artDTO = toArtPromoTemporadaObsequioDTO(this);
        if (this.articulo != null) {
            artDTO.setArticulo(this.getArticulo().toArticuloDTO());
        }
        if (this.promoTemporadaArt != null) {
            artDTO.setPromoTemporadaArt(this.getPromoTemporadaArt()
                    .toPromoTemporadaArtSinArtDTO());
        }
        return artDTO;
    }

    public ArtPromoTemporadaObsequioDTO toArtPromoTemporadaObsequioDTO(
            ArtPromoTemporadaObsequio spt) {
        ArtPromoTemporadaObsequioDTO sptDTO = new ArtPromoTemporadaObsequioDTO();
        BeanUtils.copyProperties(spt, sptDTO);
        return sptDTO;
    }

    public static ArtPromoTemporadaObsequio fromArtPromoTemporada(
            ArtPromoTemporadaObsequioDTO aptDTO) {
        ArtPromoTemporadaObsequio spt = new ArtPromoTemporadaObsequio();
        BeanUtils.copyProperties(aptDTO, spt);
        spt.setArticulo(Articulo.fromArticuloDTOAsociado(aptDTO.getArticulo()));
        spt.setPromoTemporadaArt(PromoTemporadaArt.fromPromoTemporadaArtDTO(aptDTO
                .getPromoTemporadaArt()));
        return spt;
    }

}
