package com.peluqueria.core.domain;

import com.peluqueria.dto.SeccionPromoTemporadaDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

@Entity
@Table(name = "seccion_promo_temporada", schema = "cuenta")
@NamedQuery(name = "SeccionPromoTemporada.findAll", query = "SELECT s FROM SeccionPromoTemporada s")
public class SeccionPromoTemporada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_seccion_promo_temporada")
    private Long idSeccionPromoTemporada;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion")
    private Seccion seccion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    public SeccionPromoTemporada() {
    }

    public Long getIdSeccionPromoTemporada() {
        return idSeccionPromoTemporada;
    }

    public void setIdSeccionPromoTemporada(Long idSeccionPromoTemporada) {
        this.idSeccionPromoTemporada = idSeccionPromoTemporada;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getDescriSeccion() {
        return descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public SeccionPromoTemporadaDTO toSinSeccionPromoTemporadaDTO() {
        SeccionPromoTemporadaDTO secDTO = toSeccionPromoTemporadaDTO(this);
        if (this.seccion != null) {
            secDTO.setSeccion(this.getSeccion().toSeccionDTO());
        }
        secDTO.setPromoTemporada(null);
        return secDTO;
    }

    // Con Promo Temporada
    public SeccionPromoTemporadaDTO toSeccionPromoTemporadaDTO() {
        SeccionPromoTemporadaDTO secDTO = toSeccionPromoTemporadaDTO(this);
        if (this.seccion != null) {
            secDTO.setSeccion(this.getSeccion().toSeccionDTO());
        }
        if (this.promoTemporada != null) {
            secDTO.setPromoTemporada(this.getPromoTemporada()
                    .toPromoTemporadaSinSecDTO());
        }
        return secDTO;
    }

    public SeccionPromoTemporadaDTO toSeccionPromoTemporadaDTO(
            SeccionPromoTemporada spt) {
        SeccionPromoTemporadaDTO sptDTO = new SeccionPromoTemporadaDTO();
        BeanUtils.copyProperties(spt, sptDTO);
        return sptDTO;
    }

    public static SeccionPromoTemporada fromSeccionPromoTemporada(
            SeccionPromoTemporadaDTO sptDTO) {
        SeccionPromoTemporada spt = new SeccionPromoTemporada();
        BeanUtils.copyProperties(sptDTO, spt);
        spt.setSeccion(Seccion.fromSeccionDTO(sptDTO.getSeccion()));
        spt.setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(sptDTO
                .getPromoTemporada()));
        return spt;
    }

}
