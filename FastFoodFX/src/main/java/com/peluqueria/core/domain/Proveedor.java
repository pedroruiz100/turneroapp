package com.peluqueria.core.domain;

import com.peluqueria.dto.ProveedorDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the proveedor database table.
 *
 */
@Entity
@Table(name = "proveedor", schema = "cuenta")
@NamedQuery(name = "Proveedor.findAll", query = "SELECT p FROM Proveedor p")
public class Proveedor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_proveedor")
    private Long idProveedor;

    @Column(name = "calle_principal")
    private String callePrincipal;

    private String email;

    private String descripcion;

    private String descripcion2;

    @Column(name = "pais_prov")
    private String paisProv;

    @Column(name = "departamento_prov")
    private String departamentoProv;

    @Column(name = "ciudad_prov")
    private String ciudadProv;

    @Column(name = "barrio_prov")
    private String barrioProv;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "cond_pago")
    private String condPago;

    @Column(name = "saldo")
    private Long saldo;

    // bi-directional many-to-one association to Barrio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_barrio")
    private Barrio barrio;

    // bi-directional many-to-one association to Ciudad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ciudad")
    private Ciudad ciudad;

    // bi-directional many-to-one association to Departamento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_departamento")
    private Departamento departamento;

    // bi-directional many-to-one association to Pais
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pais")
    private Pais pais;

    @Column(name = "nro_local")
    private Integer nroLocal;

    @Column(name = "primera_lateral")
    private String primeraLateral;

    @Column(name = "inicio_vigencia")
    private Date inicioVigencia;

    @Column(name = "fin_vigencia")
    private Date finVigencia;

    private String ruc;

    @Column(name = "segunda_lateral")
    private String segundaLateral;

    private String telefono;

    private String telefono2;

    private Integer timbrado;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FamiliaProv
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_familia_prov")
    private FamiliaProv familiaProv;

    // bi-directional many-to-one association to ProveedorRubro
    @OneToMany(mappedBy = "proveedor")
    private List<ProveedorRubro> proveedorRubros;

    // bi-directional many-to-one association to ComboCab
    @OneToMany(mappedBy = "proveedor")
    private List<ComboCab> comboCabs;

    // bi-directional many-to-one association to ComboCab
    @OneToMany(mappedBy = "proveedor")
    private List<Recepcion> recepcion;

    public Proveedor() {
    }

    public Long getIdProveedor() {
        return this.idProveedor;
    }

    public void setIdProveedor(Long idProveedor) {
        this.idProveedor = idProveedor;
    }

    public String getCallePrincipal() {
        return this.callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getPaisProv() {
        return paisProv;
    }

    public void setPaisProv(String paisProv) {
        this.paisProv = paisProv;
    }

    public String getDepartamentoProv() {
        return departamentoProv;
    }

    public void setDepartamentoProv(String departamentoProv) {
        this.departamentoProv = departamentoProv;
    }

    public String getCiudadProv() {
        return ciudadProv;
    }

    public void setCiudadProv(String ciudadProv) {
        this.ciudadProv = ciudadProv;
    }

    public String getBarrioProv() {
        return barrioProv;
    }

    public void setBarrioProv(String barrioProv) {
        this.barrioProv = barrioProv;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public String getCondPago() {
        return condPago;
    }

    public void setCondPago(String condPago) {
        this.condPago = condPago;
    }

    public Long getSaldo() {
        return saldo;
    }

    public void setSaldo(Long saldo) {
        this.saldo = saldo;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getRuc() {
        return this.ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getTelefono() {
        return this.telefono;
    }

    public String getDescripcion2() {
        return descripcion2;
    }

    public void setDescripcion2(String descripcion2) {
        this.descripcion2 = descripcion2;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getTelefono2() {
        return this.telefono2;
    }

    public Date getInicioVigencia() {
        return inicioVigencia;
    }

    public void setInicioVigencia(Date inicioVigencia) {
        this.inicioVigencia = inicioVigencia;
    }

    public Date getFinVigencia() {
        return finVigencia;
    }

    public void setFinVigencia(Date finVigencia) {
        this.finVigencia = finVigencia;
    }

    public void setTelefono2(String telefono2) {
        this.telefono2 = telefono2;
    }

    public Integer getTimbrado() {
        return this.timbrado;
    }

    public void setTimbrado(Integer timbrado) {
        this.timbrado = timbrado;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FamiliaProv getFamiliaProv() {
        return this.familiaProv;
    }

    public void setFamiliaProv(FamiliaProv familiaProv) {
        this.familiaProv = familiaProv;
    }

    public List<ProveedorRubro> getProveedorRubros() {
        return this.proveedorRubros;
    }

    public void setProveedorRubros(List<ProveedorRubro> proveedorRubros) {
        this.proveedorRubros = proveedorRubros;
    }

    public ProveedorRubro addProveedorRubro(ProveedorRubro proveedorRubro) {
        getProveedorRubros().add(proveedorRubro);
        proveedorRubro.setProveedor(this);
        return proveedorRubro;
    }

    public ProveedorRubro removeProveedorRubro(ProveedorRubro proveedorRubro) {
        getProveedorRubros().remove(proveedorRubro);
        proveedorRubro.setProveedor(null);
        return proveedorRubro;
    }

    // **********************************************************************************
    public List<ComboCab> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCab> comboCabs) {
        this.comboCabs = comboCabs;
    }

    public ComboCab addComboCab(ComboCab comboCab) {
        getComboCabs().add(comboCab);
        comboCab.setProveedor(this);
        return comboCab;
    }

    public ComboCab removeComboCab(ComboCab comboCab) {
        getComboCabs().remove(comboCab);
        comboCab.setProveedor(null);
        return comboCab;
    }

    public Barrio getBarrio() {
        return barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    public Ciudad getCiudad() {
        return ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public List<Recepcion> getRecepcion() {
        return recepcion;
    }

    public void setRecepcion(List<Recepcion> recepcion) {
        this.recepcion = recepcion;
    }

    public ProveedorDTO toProveedorDTO() {
        ProveedorDTO proDTO = toProveedorDTO(this);
        if (this.barrio != null) {
            proDTO.setBarrio(this.getBarrio().toBarrioAClienteDTO());
        }
        if (this.ciudad != null) {
            proDTO.setCiudad(this.getCiudad().toCiudadSinOtrosDatos());
        }
        if (this.departamento != null) {
            proDTO.setDepartamento(this.getDepartamento()
                    .toSinOtrosDatosDepartamentoDTO());
        }
        if (this.pais != null) {
            proDTO.setPais(this.getPais().toPaisDTO());
        }
        if (this.familiaProv != null) {
            proDTO.setFamiliaProv(this.getFamiliaProv().toFamiliaProvDTO());
        }
        proDTO.setComboCabs(null);
        proDTO.setProveedorRubros(null);
        proDTO.setRecepcion(null);
        return proDTO;
    }

    public ProveedorDTO toProveedorBdDTO() {
        ProveedorDTO proDTO = toProveedorDTO(this);
        proDTO.setBarrio(null);
        proDTO.setCiudad(null);
        proDTO.setDepartamento(null);
        proDTO.setPais(null);
        proDTO.setFamiliaProv(null);
        proDTO.setComboCabs(null);
        proDTO.setProveedorRubros(null);
        proDTO.setRecepcion(null);
        return proDTO;
    }

    public static ProveedorDTO toProveedorDTO(Proveedor proveedor) {
        ProveedorDTO proDTO = new ProveedorDTO();
        BeanUtils.copyProperties(proveedor, proDTO);
        return proDTO;
    }

    public static Proveedor fromProveedorDTO(ProveedorDTO proDTO) {
        Proveedor pro = new Proveedor();
        BeanUtils.copyProperties(proDTO, pro);
        return pro;
    }

    public static Proveedor fromProveedorAsociadoDTO(ProveedorDTO proDTO) {
        Proveedor pro = fromProveedorDTO(proDTO);
        pro.setBarrio(Barrio.fromBarrioDTO(proDTO.getBarrio()));
        pro.setCiudad(Ciudad.fromCiudadDTO(proDTO.getCiudad()));
        pro.setDepartamento(Departamento.fromDepartamentoDTO(proDTO
                .getDepartamento()));
        pro.setPais(Pais.fromPaisDTO(proDTO.getPais()));
        pro.setFamiliaProv(FamiliaProv.fromFamiliaProv(proDTO.getFamiliaProv()));
        pro.setRecepcion(null);
        return pro;
    }

}
