package com.peluqueria.core.domain;

import com.peluqueria.dto.RangoFuncionarioDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rango_funcionario_nf2 database table.
 *
 */
@Entity
@Table(name = "rango_funcionario_nf2", schema = "general")
@NamedQuery(name = "RangoFuncionarioNf2.findAll", query = "SELECT rangoFuncionarioNf2 FROM RangoFuncionarioNf2 rangoFuncionarioNf2")
public class RangoFuncionarioNf2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rango")
    private Long idRango;

    @Column(name = "rango_inicial")
    private Long rangoInicial;

    @Column(name = "rango_final")
    private Long rangoFinal;

    @Column(name = "rango_actual")
    private Long rangoActual;

    public RangoFuncionarioNf2() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

    public RangoFuncionarioDTO toRangoFuncionarioDTO() {
        RangoFuncionarioDTO rangoDTO = toRangoFuncionarioDTO(this);
        return rangoDTO;
    }

    public static RangoFuncionarioDTO toRangoFuncionarioDTO(RangoFuncionarioNf2 rango) {
        RangoFuncionarioDTO dDTO = new RangoFuncionarioDTO();
        BeanUtils.copyProperties(rango, dDTO);
        return dDTO;
    }

    public static RangoFuncionarioNf2 fromRangoFuncionarioDTO(RangoFuncionarioDTO rangoDTO) {
        RangoFuncionarioNf2 rango = new RangoFuncionarioNf2();
        BeanUtils.copyProperties(rangoDTO, rango);
        return rango;
    }

}
