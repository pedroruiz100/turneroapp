/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import com.peluqueria.dto.CuponeraDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author ExcelsisWalker
 */
@Entity
@Table(name = "cuponera", schema = "cuponera")
@NamedQuery(name = "Cuponera.findAll", query = "SELECT c FROM Cuponera c")
public class Cuponera implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cuponera")
    private Long idCuponera;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "monto_min")
    private Integer montoMin;

    @Column(name = "fecha_fin")
    private Timestamp fechaFin;

    @Column(name = "fecha_inicio")
    private Timestamp fechaInicio;

    @Column(name = "cliente_fiel")
    private Boolean clienteFiel;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    public Long getIdCuponera() {
        return idCuponera;
    }

    public void setIdCuponera(Long idCuponera) {
        this.idCuponera = idCuponera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }

    public Timestamp getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Boolean getClienteFiel() {
        return clienteFiel;
    }

    public void setClienteFiel(Boolean clienteFiel) {
        this.clienteFiel = clienteFiel;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static CuponeraDTO toCuponeraDTO(Cuponera cuponera) {
        CuponeraDTO cuponeraDTO = new CuponeraDTO();
        BeanUtils.copyProperties(cuponera, cuponeraDTO);
        return cuponeraDTO;
    }

    // <-- SERVER
    public static Cuponera fromCuponeraDTO(
            CuponeraDTO cuponeraDTO) {
        Cuponera cuponera = new Cuponera();
        BeanUtils.copyProperties(cuponeraDTO, cuponera);
        return cuponera;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

}
