package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabTipoPagoDTO;
import com.peluqueria.dto.TipoPagoDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_pago database table.
 *
 */
@Entity
@Table(name = "tipo_pago", schema = "factura_cliente")
@NamedQuery(name = "TipoPago.findAll", query = "SELECT t FROM TipoPago t")
public class TipoPago implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_pago")
    private Long idTipoPago;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FacturaClienteCabTipoPago
    @OneToMany(mappedBy = "tipoPago", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos;

    public TipoPago() {
    }

    public Long getIdTipoPago() {
        return this.idTipoPago;
    }

    public void setIdTipoPago(Long idTipoPago) {
        this.idTipoPago = idTipoPago;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCabTipoPago> getFacturaClienteCabTipoPagos() {
        return this.facturaClienteCabTipoPagos;
    }

    public void setFacturaClienteCabTipoPagos(
            List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos) {
        this.facturaClienteCabTipoPagos = facturaClienteCabTipoPagos;
    }

    public TipoPagoDTO toBDTipoPagoDTO() {
        TipoPagoDTO tpDTO = toTipoPagoDTO(this);
        tpDTO.setFacturaClienteCabTipoPagos(null);
        return tpDTO;
    }

    public TipoPagoDTO toTipoPagoDTO() {
        TipoPagoDTO tpDTO = toTipoPagoDTO(this);
        if (!this.facturaClienteCabTipoPagos.isEmpty()) {
            List<FacturaClienteCabTipoPago> fac = this
                    .getFacturaClienteCabTipoPagos();
            List<FacturaClienteCabTipoPagoDTO> facDTO = new ArrayList<FacturaClienteCabTipoPagoDTO>();
            for (FacturaClienteCabTipoPago f : fac) {
                facDTO.add(f.toBDFacturaClienteCabTipoPagoDTO());
            }
            tpDTO.setFacturaClienteCabTipoPagos(facDTO);
        }
        return tpDTO;
    }

    public static TipoPagoDTO toTipoPagoDTO(TipoPago tipoPago) {
        TipoPagoDTO tpDTO = new TipoPagoDTO();
        BeanUtils.copyProperties(tipoPago, tpDTO);
        return tpDTO;
    }

    public static TipoPago fromTipoPago(TipoPagoDTO tpDTO) {
        TipoPago tp = new TipoPago();
        BeanUtils.copyProperties(tpDTO, tp);
        return tp;
    }

}
