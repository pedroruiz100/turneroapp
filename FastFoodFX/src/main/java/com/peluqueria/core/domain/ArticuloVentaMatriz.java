package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloVentaMatrizDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the barrio database table.
 *
 */
@Entity
@Table(name = "articulo_venta_matriz", schema = "stock")
@NamedQuery(name = "ArticuloVentaMatriz.findAll", query = "SELECT b FROM ArticuloVentaMatriz b")
public class ArticuloVentaMatriz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_venta_matriz")
    private Long idArticuloVentaMatriz;

    private String codigo;

    private Date fecha;

    private Long cantidad;
    private String sucursal;

    public ArticuloVentaMatriz() {
    }

    public Long getIdArticuloVentaMatriz() {
        return idArticuloVentaMatriz;
    }

    public void setIdArticuloVentaMatriz(Long idArticuloVentaMatriz) {
        this.idArticuloVentaMatriz = idArticuloVentaMatriz;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    // Metodo que sirve para cargar los datos de Barrio a BarrioDTO
    public static ArticuloVentaMatrizDTO toArticuloVentaMatrizDTO(ArticuloVentaMatriz barrio) {
        ArticuloVentaMatrizDTO barrioDTO = new ArticuloVentaMatrizDTO();
        BeanUtils.copyProperties(barrio, barrioDTO);
        return barrioDTO;
    }

    public static ArticuloVentaMatriz fromArticuloVentaMatrizDTO(ArticuloVentaMatriz barrioDTO) {
        ArticuloVentaMatriz barrio = new ArticuloVentaMatriz();
        BeanUtils.copyProperties(barrioDTO, barrio);
        return barrio;
    }

    public static ArticuloVentaMatriz fromArticuloVentaMatriz(ArticuloVentaMatriz barrioDTO) {
        ArticuloVentaMatriz bar = fromArticuloVentaMatrizDTO(barrioDTO);
//        bar.set(Ciudad.fromCiudadDTO(barrioDTO.getCiudadDTO()));
        return bar;
    }

    // FIN DE BARRIO A CLIENTE
    // PARA FAMILIA TARJETA
//    public ArticuloVentaMatrizDTO toBarrioDTOAFamiliaTarjDTO() {
//        BarrioDTO barrioDTO = toBarrioDTO(this);
//        barrioDTO.setCiudadDTO(null);
//        barrioDTO.setClientesDTO(null);
//        barrioDTO.setFamiliaTarjDTO(null);
//        barrioDTO.setProveedorDTO(null);
//        barrioDTO.setSucursalsDTOs(null);
//        return barrioDTO;
//    }
}
