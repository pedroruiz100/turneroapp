package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf1DTO;

/**
 * The persistent class for the promo_temporada_nf1 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf1", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf1.findAll", query = "SELECT p FROM PromoTemporadaNf1 p")
public class PromoTemporadaNf1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf1")
    private Long idPromoTemporadaNf1;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    public PromoTemporadaNf1() {
    }

    public Long getIdPromoTemporadaNf1() {
        return this.idPromoTemporadaNf1;
    }

    public void setIdPromoTemporadaNf1(Long idPromoTemporadaNf1) {
        this.idPromoTemporadaNf1 = idPromoTemporadaNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf1Tipo getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf1 fromPromoTemporadaNf1DTO(PromoTemporadaNf1DTO promoTemporadaNf1DTO) {
        PromoTemporadaNf1 promoTemporadaNf1 = new PromoTemporadaNf1();
        BeanUtils.copyProperties(promoTemporadaNf1DTO, promoTemporadaNf1);
        return promoTemporadaNf1;
    }

    // <-- SERVER
    public static PromoTemporadaNf1DTO toPromoTemporadaNf1DTO(PromoTemporadaNf1 promoTemporadaNf1) {
        PromoTemporadaNf1DTO promoTemporadaNf1DTO = new PromoTemporadaNf1DTO();
        BeanUtils.copyProperties(promoTemporadaNf1, promoTemporadaNf1DTO);
        return promoTemporadaNf1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf1 fromPromoTemporadaNf1DTOEntitiesFull(PromoTemporadaNf1DTO promoTemporadaNf1DTO) {
        PromoTemporadaNf1 promoTemporadaNf1 = fromPromoTemporadaNf1DTO(promoTemporadaNf1DTO);
        if (promoTemporadaNf1DTO.getNf1Tipo() != null) {
            promoTemporadaNf1.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(promoTemporadaNf1DTO.getNf1Tipo()));
        }
        if (promoTemporadaNf1DTO.getPromoTemporada() != null) {
            promoTemporadaNf1
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf1DTO.getPromoTemporada()));
        }
        return promoTemporadaNf1;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf1DTO toPromoTemporadaNf1DTOEntityFull() {
        PromoTemporadaNf1DTO promoTemporadaNf1DTO = toPromoTemporadaNf1DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf1DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf1DTO.setPromoTemporada(null);
        }
        if (this.getNf1Tipo() != null) {
            promoTemporadaNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            promoTemporadaNf1DTO.setNf1Tipo(null);
        }
        return promoTemporadaNf1DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf1DTO toPromoTemporadaNf1DTOEntityNull() {
        PromoTemporadaNf1DTO promoTemporadaNf1DTO = toPromoTemporadaNf1DTO(this);
        promoTemporadaNf1DTO.setPromoTemporada(null);
        promoTemporadaNf1DTO.setNf1Tipo(null);
        return promoTemporadaNf1DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf1DTO toPromoTemporadaNf1DTO() {
        PromoTemporadaNf1DTO descuentoFielNf1DTO = toPromoTemporadaNf1DTO(this);
        descuentoFielNf1DTO.setPromoTemporada(null);
        if (this.getNf1Tipo() != null) {
            descuentoFielNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            descuentoFielNf1DTO.setNf1Tipo(null);
        }
        return descuentoFielNf1DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
