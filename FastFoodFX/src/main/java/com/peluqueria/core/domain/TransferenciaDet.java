package com.peluqueria.core.domain;

import com.peluqueria.dto.TransferenciaDetDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "transferencia_det", schema = "stock")
@NamedQuery(name = "TransferenciaDet.findAll", query = "SELECT f FROM TransferenciaDet f")
public class TransferenciaDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transferencia_det")
    private Long idTransferenciaDet;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_transferencia_cab")
    private TransferenciaCab transferenciaCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "cantidad")
    private String cantidad;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "observacion")
    private String observacion;

    private String existencia;

    public TransferenciaDet() {
    }

    public Long getIdTransferenciaDet() {
        return idTransferenciaDet;
    }

    public void setIdTransferenciaDet(Long idTransferenciaDet) {
        this.idTransferenciaDet = idTransferenciaDet;
    }

    public TransferenciaCab getTransferenciaCab() {
        return transferenciaCab;
    }

    public void setTransferenciaCab(TransferenciaCab transferenciaCab) {
        this.transferenciaCab = transferenciaCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getExistencia() {
        return existencia;
    }

    public void setExistencia(String existencia) {
        this.existencia = existencia;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public TransferenciaDetDTO toTransferenciaDetDTO() {
        TransferenciaDetDTO facDTO = toTransferenciaDetDTO(this);
        if (this.transferenciaCab != null) {
            facDTO.setTransferenciaCab(this.getTransferenciaCab().toTransferenciaCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
//        facDTO.setDescCombos(null);
        return facDTO;
    }

//    public TransferenciaDetDTO toTransferenciaDetDTO() {
//        TransferenciaDetDTO facDTO = toFacturaClienteDetDTO(this);
//        facDTO.setFacturaClienteCab(null);
//        facDTO.setArticulo(null);
//        facDTO.setDescCombos(null);
//        return facDTO;
//    }
    public static TransferenciaDetDTO toTransferenciaDetDTO(
            TransferenciaDet facturaClienteDet) {
        TransferenciaDetDTO facDTO = new TransferenciaDetDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static TransferenciaDet fromTransferenciaDetDTO(
            TransferenciaDetDTO facDTO) {
        TransferenciaDet fac = new TransferenciaDet();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static TransferenciaDet fromTransferenciaDetAsociado(
            TransferenciaDetDTO facDTO) {
        TransferenciaDet fac = fromTransferenciaDetDTO(facDTO);
        fac.setTransferenciaCab(TransferenciaCab
                .fromTransferenciaCabDTO(facDTO.getTransferenciaCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
