package com.peluqueria.core.domain;

import com.peluqueria.dto.RolDTO;
import com.peluqueria.dto.UsuarioDTO;
import com.peluqueria.dto.UsuarioRolDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the usuario_rol database table.
 *
 */
@Entity
@Table(name = "usuario_rol", schema = "seguridad")
@NamedQuery(name = "UsuarioRol.findAll", query = "SELECT u FROM UsuarioRol u")
public class UsuarioRol implements Serializable {

    private static final long serialVersionUID = 1L;

//    public UsuarioRolDTO toUsuarioRolDTO(UsuarioDTO usuario, RolDTO rol) {
//        UsuarioRolDTO urDTO = this.toUsuarioRolDTO();
//        urDTO.setUsuario(usuario);
//        // rol.setRolFuncions(rfDTO);
//        rol.setUsuarioRols(null);
//        urDTO.setRol(rol);
//        return urDTO;
//    }
    public static UsuarioRolDTO toBDUsuarioRolSeguridadDTO(UsuarioRol usuarioRol) {
        UsuarioRolDTO urDTO = toUsuarioRolDTO(usuarioRol);
        urDTO.setUsuario(Usuario.toBDUsuarioSeguridadDTO(usuarioRol.getUsuario()));
        urDTO.setRol(Rol.toBDRolDTO(usuarioRol.getRol()));
        return urDTO;
    }

    public static UsuarioRolDTO toUsuarioRolDTO(UsuarioRol usuarioRol) {
        UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
        BeanUtils.copyProperties(usuarioRol, usuarioRolDTO);
        return usuarioRolDTO;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario_rol")
    private Long idUsuarioRol;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Rol
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rol")
    private Rol rol;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    public UsuarioRol() {
    }

    public Long getIdUsuarioRol() {
        return this.idUsuarioRol;
    }

    public void setIdUsuarioRol(Long idUsuarioRol) {
        this.idUsuarioRol = idUsuarioRol;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Rol getRol() {
        return this.rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public Usuario getUsuario() {
        return this.usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public UsuarioRolDTO toUsuarioRolDTO(UsuarioDTO usuario, RolDTO rol) {
        UsuarioRolDTO urDTO = this.toUsuarioRolDTO();
        urDTO.setUsuario(usuario);
        rol.setUsuarioRols(null);
        urDTO.setRol(rol);
        return urDTO;
    }

    public UsuarioRolDTO toUsuarioRolDTO() {
        UsuarioRolDTO usuarioRol = new UsuarioRolDTO();
        BeanUtils.copyProperties(this, usuarioRol);
        return usuarioRol;
    }

    public static UsuarioRol fromUsuarioRolDTO(UsuarioRolDTO uDTO) {
        UsuarioRol ur = new UsuarioRol();
        BeanUtils.copyProperties(uDTO, ur);
        return ur;
    }

    public static UsuarioRol fromUsuarioRolAsociadoDTO(UsuarioRolDTO uDTO) {
        UsuarioRol ur = fromUsuarioRolDTO(uDTO);
        ur.setRol(Rol.fromRolDTO(uDTO.getRol()));
        ur.setUsuario(Usuario.fromUsuarioCajeroDTO(uDTO.getUsuario()));
        return ur;
    }

    public static UsuarioRol fromUsuarioRolSeguridadDTO(UsuarioRolDTO uDTO) {
        UsuarioRol ur = fromUsuarioRolDTO(uDTO);
        ur.setRol(Rol.fromRolDTO(uDTO.getRol()));
        ur.setUsuario(Usuario.fromUsuarioDTO(uDTO.getUsuario()));
        return ur;
    }

//    static UsuarioRolDTO toUsuarioRolDTO(UsuarioRol usuarioRol) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//    public UsuarioRolDTO toUsuarioRolDTO(UsuarioDTO usuario, RolDTO rol) {
//        UsuarioRolDTO urDTO = this.toUsuarioRolDTO();
//        urDTO.setUsuario(usuario);
//        // rol.setRolFuncions(rfDTO);
//        rol.setUsuarioRols(null);
//        urDTO.setRol(rol);
//        return urDTO;
//    }
    public UsuarioRolDTO toBDUsuarioRolSeguridadDTO() {
        UsuarioRolDTO urDTO = toUsuarioRolDTO(this.getUsuario().toBDUsuarioDTO(), this.getRol().toBDRolDTO());
        urDTO.setUsuario(Usuario.toBDUsuarioSeguridadDTO(this.getUsuario()));
        urDTO.setRol(this.getRol().toBDRolDTO());
        return urDTO;
    }
}
