/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import com.peluqueria.dto.EntidadDTO;
import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author ExcelsisWalker
 */
@Entity
@Table(name = "entidad", schema = "cuenta")
@NamedQuery(name = "Entidad.findAll", query = "SELECT e FROM Entidad e")
public class Entidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_entidad")
    private Long idEntidad;

    private String descripcion;

    // bi-directional one-to-many association to DescuentoTarjetaCabEntidad
    @OneToMany(mappedBy = "entidad", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabEntidad> descuentoTarjetaCabEntidads;

    public Entidad() {
    }

    public Long getIdEntidad() {
        return this.idEntidad;
    }

    public void setIdEntidad(Long idEntidad) {
        this.idEntidad = idEntidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<DescuentoTarjetaCabEntidad> getDescuentoTarjetaCabEntidads() {
        return descuentoTarjetaCabEntidads;
    }

    public void setDescuentoTarjetaCabEntidads(List<DescuentoTarjetaCabEntidad> descuentoTarjetaCabEntidads) {
        this.descuentoTarjetaCabEntidads = descuentoTarjetaCabEntidads;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Entidad fromEntidadDTO(EntidadDTO entidadDTO) {
        Entidad entidad = new Entidad();
        BeanUtils.copyProperties(entidadDTO, entidad);
        return entidad;
    }

    // <-- SERVER
    public static EntidadDTO toEntidadDTO(Entidad entidad) {
        EntidadDTO entidadDTO = new EntidadDTO();
        BeanUtils.copyProperties(entidad, entidadDTO);
        return entidadDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    
    public static Entidad fromEntidadDTONoList(EntidadDTO entidadDTO) {
        Entidad entidad = fromEntidadDTO(entidadDTO);
        entidad.setDescuentoTarjetaCabEntidads(null);
        return entidad;
    }
    
    public EntidadDTO toBDDescriEntidadDTO() {
        EntidadDTO entidadDTO = toEntidadDTO(this);
        entidadDTO.setDescuentoTarjetaCabEntidads(null);
        return entidadDTO;
    }
}
