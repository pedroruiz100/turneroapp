package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabNotaCreditoDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_vale database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_nota_credito", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabNotaCredito.findAll", query = "SELECT f FROM FacturaClienteCabNotaCredito f")
public class FacturaClienteCabNotaCredito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_nota_credito")
    private Long idFacturaClienteCabNotaCredito;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    @Column(name = "nro_nota")
    private String nroNota;

    @Column(name = "monto")
    private Integer monto;

    public FacturaClienteCabNotaCredito() {
    }

    public Long getIdFacturaClienteCabNotaCredito() {
        return idFacturaClienteCabNotaCredito;
    }

    public void setIdFacturaClienteCabNotaCredito(
            Long idFacturaClienteCabNotaCredito) {
        this.idFacturaClienteCabNotaCredito = idFacturaClienteCabNotaCredito;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public String getNroNota() {
        return nroNota;
    }

    public void setNroNota(String nroNota) {
        this.nroNota = nroNota;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabNotaCreditoDTO toBDFacClienteCabNotaCreditoDTO() {
        FacturaClienteCabNotaCreditoDTO facDTO = toFacClienteCabNotaCreditoDTO(this);
        if (this.getFacturaClienteCab() != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    private static FacturaClienteCabNotaCreditoDTO toFacClienteCabNotaCreditoDTO(
            FacturaClienteCabNotaCredito facturaClienteCabNotaCredito) {
        FacturaClienteCabNotaCreditoDTO facDTO = new FacturaClienteCabNotaCreditoDTO();
        BeanUtils.copyProperties(facturaClienteCabNotaCredito, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabNotaCredito fromFacturaClienteCabNotaCredito(FacturaClienteCabNotaCreditoDTO facDTO) {
        FacturaClienteCabNotaCredito fac = new FacturaClienteCabNotaCredito();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabNotaCredito fromFacturaClienteCabNotaCreditoAsociado(FacturaClienteCabNotaCreditoDTO facDTO) {
        FacturaClienteCabNotaCredito fac = fromFacturaClienteCabNotaCredito(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}
