package com.peluqueria.core.domain;

import com.peluqueria.dto.SeccionSubDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the seccion_sub database table.
 *
 */
@Entity
@Table(name = "seccion_sub", schema = "stock")
@NamedQuery(name = "SeccionSub.findAll", query = "SELECT s FROM SeccionSub s")
public class SeccionSub implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_seccion_sub")
    private Long idSeccionSub;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "seccionSub")
    private List<Articulo> articulos;

    // bi-directional many-to-one association to Seccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion")
    private Seccion seccion;

    public SeccionSub() {
    }

    public Long getIdSeccionSub() {
        return this.idSeccionSub;
    }

    public void setIdSeccionSub(Long idSeccionSub) {
        this.idSeccionSub = idSeccionSub;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Articulo> getArticulos() {
        return this.articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    public Seccion getSeccion() {
        return this.seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public SeccionSubDTO toSeccionSubDTO() {
        SeccionSubDTO seccionSubDTO = toSeccionSubDTO(this);
        seccionSubDTO.setArticulos(null);
        seccionSubDTO.setSeccion(null);
        return seccionSubDTO;
    }

    public static SeccionSubDTO toSeccionSubDTO(SeccionSub seccionSub) {
        SeccionSubDTO ssDTO = new SeccionSubDTO();
        BeanUtils.copyProperties(seccionSub, ssDTO);
        return ssDTO;
    }

    // CON Seccion
    public static SeccionSub fromSeccionSubDTO(SeccionSubDTO seccionSubDTO) {
        SeccionSub ss = new SeccionSub();
        BeanUtils.copyProperties(seccionSubDTO, ss);
        return ss;
    }

    public static SeccionSub fromSeccionSubAsociadoDTO(SeccionSubDTO seccionSubDTO) {
        SeccionSub ss = fromSeccionSubDTO(seccionSubDTO);
        ss.setSeccion(Seccion.fromSeccionDTO(seccionSubDTO.getSeccion()));
        return ss;
    }

}
