package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf4DTO;

/**
 * The persistent class for the funcionario_nf4 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf4", schema = "descuento")
@NamedQuery(name = "FuncionarioNf4.findAll", query = "SELECT f FROM FuncionarioNf4 f")
public class FuncionarioNf4 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf4")
    private Long idFuncionarioNf4;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    public FuncionarioNf4() {
    }

    public Long getIdFuncionarioNf4() {
        return this.idFuncionarioNf4;
    }

    public void setIdFuncionarioNf4(Long idFuncionarioNf4) {
        this.idFuncionarioNf4 = idFuncionarioNf4;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf4 fromFuncionarioNf4DTO(FuncionarioNf4DTO funcionarioNf4DTO) {
        FuncionarioNf4 funcionarioNf4 = new FuncionarioNf4();
        BeanUtils.copyProperties(funcionarioNf4DTO, funcionarioNf4);
        return funcionarioNf4;
    }

    // <-- SERVER
    public static FuncionarioNf4DTO toFuncionarioNf4DTO(FuncionarioNf4 funcionarioNf4) {
        FuncionarioNf4DTO funcionarioNf4DTO = new FuncionarioNf4DTO();
        BeanUtils.copyProperties(funcionarioNf4, funcionarioNf4DTO);
        return funcionarioNf4DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf4 fromFuncionarioNf4DTOEntitiesFull(FuncionarioNf4DTO funcionarioNf4DTO) {
        FuncionarioNf4 funcionarioNf4 = fromFuncionarioNf4DTO(funcionarioNf4DTO);
        if (funcionarioNf4DTO.getNf4Seccion1() != null) {
            funcionarioNf4.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(funcionarioNf4DTO.getNf4Seccion1()));
        }
        return funcionarioNf4;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf4DTO toFuncionarioNf4DTONf3() {
        FuncionarioNf4DTO funcionarioNf4DTO = toFuncionarioNf4DTO(this);
        if (this.getNf4Seccion1() != null) {
            funcionarioNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        }
        return funcionarioNf4DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
