package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf3SseccionDTO;

/**
 * The persistent class for the articulo_nf3_sseccion database table.
 *
 */
@Entity
@Table(name = "articulo_nf3_sseccion", schema = "stock")
@NamedQuery(name = "ArticuloNf3Sseccion.findAll", query = "SELECT a FROM ArticuloNf3Sseccion a")
public class ArticuloNf3Sseccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf3_sseccion_articulo")
    private Long idNf3SseccionArticulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    public ArticuloNf3Sseccion() {
    }

    public Long getIdNf3SseccionArticulo() {
        return this.idNf3SseccionArticulo;
    }

    public void setIdNf3SseccionArticulo(Long idNf3SseccionArticulo) {
        this.idNf3SseccionArticulo = idNf3SseccionArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return this.nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf3Sseccion fromArticuloNf3SseccionDTO(ArticuloNf3SseccionDTO articuloNf3SseccionDTO) {
        ArticuloNf3Sseccion articuloNf3Sseccion = new ArticuloNf3Sseccion();
        BeanUtils.copyProperties(articuloNf3SseccionDTO, articuloNf3Sseccion);
        return articuloNf3Sseccion;
    }

    // <-- SERVER
    public static ArticuloNf3SseccionDTO toArticuloNf3SseccionDTO(ArticuloNf3Sseccion articuloNf3Sseccion) {
        ArticuloNf3SseccionDTO articuloNf3SseccionDTO = new ArticuloNf3SseccionDTO();
        BeanUtils.copyProperties(articuloNf3Sseccion, articuloNf3SseccionDTO);
        return articuloNf3SseccionDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf3SseccionDTO toArticuloNf3SseccionDTOEntitiesNull(ArticuloNf3Sseccion articuloNf3Sseccion) {
        ArticuloNf3SseccionDTO articuloNf3SseccionDTO = toArticuloNf3SseccionDTO(articuloNf3Sseccion);
        articuloNf3SseccionDTO.setArticulo(null);
        articuloNf3SseccionDTO.setNf3Sseccion(null);
        return articuloNf3SseccionDTO;
    }

    // <-- SERVER
    public ArticuloNf3SseccionDTO toArticuloNf3SseccionDTOEntitiesNull() {
        ArticuloNf3SseccionDTO articuloNf3SseccionDTO = toArticuloNf3SseccionDTO(this);
        articuloNf3SseccionDTO.setArticulo(null);
        articuloNf3SseccionDTO.setNf3Sseccion(null);
        return articuloNf3SseccionDTO;
    }

    // <-- SERVER
    public ArticuloNf3SseccionDTO toArticuloNf3SseccionDTOEntitiesNf() {
        ArticuloNf3SseccionDTO articuloNf3SseccionDTO = toArticuloNf3SseccionDTO(this);
        articuloNf3SseccionDTO.setArticulo(null);
        if (this.getNf3Sseccion() != null) {
            articuloNf3SseccionDTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            articuloNf3SseccionDTO.setNf3Sseccion(null);
        }
        return articuloNf3SseccionDTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf3Sseccion fromArticuloNf3SseccionDTOEntitiesFull(
            ArticuloNf3SseccionDTO articuloNf3SseccionDTO) {
        ArticuloNf3Sseccion articuloNf3Sseccion = fromArticuloNf3SseccionDTO(articuloNf3SseccionDTO);
        if (articuloNf3SseccionDTO.getArticulo() != null) {
            articuloNf3Sseccion.setArticulo(Articulo.fromArticuloDTO(articuloNf3SseccionDTO.getArticulo()));
        }
        if (articuloNf3SseccionDTO.getNf3Sseccion() != null) {
            articuloNf3Sseccion.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(articuloNf3SseccionDTO.getNf3Sseccion()));
        }
        return articuloNf3Sseccion;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
