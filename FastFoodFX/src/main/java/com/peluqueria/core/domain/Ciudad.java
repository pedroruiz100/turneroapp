package com.peluqueria.core.domain;

import com.peluqueria.dto.CiudadDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the ciudad database table.
 *
 */
@Entity
@Table(name = "ciudad", schema = "general")
@NamedQuery(name = "Ciudad.findAll", query = "SELECT c FROM Ciudad c")
public class Ciudad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ciudad")
    private Long idCiudad;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Barrio
    @OneToMany(mappedBy = "ciudad", fetch = FetchType.LAZY)
    private List<Barrio> barrios;

    // bi-directional many-to-one association to Departamento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_departamento")
    private Departamento departamento;

    // bi-directional many-to-one association to Sucursal
    @OneToMany(mappedBy = "ciudad", fetch = FetchType.LAZY)
    private List<Sucursal> sucursals;

    // bi-directional many-to-one association to Cliente
    @OneToMany(mappedBy = "ciudad", fetch = FetchType.LAZY)
    private List<Cliente> clientes;

    // bi-directional many-to-one association to FamiliaTarj
    @OneToMany(mappedBy = "ciudad", fetch = FetchType.LAZY)
    private List<FamiliaTarj> familiaTarjs;

    // bi-directional many-to-one association to Proveedor
    @OneToMany(mappedBy = "ciudad", fetch = FetchType.LAZY)
    private List<Proveedor> proveedors;

    public Ciudad() {
    }

    public Long getIdCiudad() {
        return this.idCiudad;
    }

    public void setIdCiudad(Long idCiudad) {
        this.idCiudad = idCiudad;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Barrio> getBarrios() {
        return this.barrios;
    }

    public void setBarrios(List<Barrio> barrios) {
        this.barrios = barrios;
    }

    public Barrio addBarrio(Barrio barrio) {
        getBarrios().add(barrio);
        barrio.setCiudad(this);

        return barrio;
    }

    public Barrio removeBarrio(Barrio barrio) {
        getBarrios().remove(barrio);
        barrio.setCiudad(null);

        return barrio;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public List<Sucursal> getSucursals() {
        return this.sucursals;
    }

    public void setSucursals(List<Sucursal> sucursals) {
        this.sucursals = sucursals;
    }

    public Sucursal addSucursal(Sucursal sucursal) {
        getSucursals().add(sucursal);
        sucursal.setCiudad(this);

        return sucursal;
    }

    public Sucursal removeSucursal(Sucursal sucursal) {
        getSucursals().remove(sucursal);
        sucursal.setCiudad(null);

        return sucursal;
    }

    // ****************************************************************************************
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Cliente addClientes(Cliente clientes) {
        getClientes().add(clientes);
        clientes.setCiudad(this);
        return clientes;
    }

    public Cliente removeClientes(Cliente clientes) {
        getClientes().remove(clientes);
        clientes.setCiudad(null);
        return clientes;
    }

    // ****************************************************************************************
    public List<FamiliaTarj> getFamiliaTarjs() {
        return familiaTarjs;
    }

    public void setFamiliaTarjs(List<FamiliaTarj> familiaTarjs) {
        this.familiaTarjs = familiaTarjs;
    }

    public FamiliaTarj addFamiliaTarjs(FamiliaTarj familiaTarj) {
        getFamiliaTarjs().add(familiaTarj);
        familiaTarj.setCiudad(this);
        return familiaTarj;
    }

    public FamiliaTarj removeFamiliaTarjs(FamiliaTarj familiaTarj) {
        getFamiliaTarjs().remove(familiaTarj);
        familiaTarj.setCiudad(null);
        return familiaTarj;
    }

    // ****************************************************************************************
    public List<Proveedor> getProveedors() {
        return proveedors;
    }

    public void setProveedors(List<Proveedor> proveedors) {
        this.proveedors = proveedors;
    }

    public Proveedor addProveedor(Proveedor proveedor) {
        getProveedors().add(proveedor);
        proveedor.setCiudad(this);
        return proveedor;
    }

    public Proveedor removeProveedor(Proveedor proveedor) {
        getProveedors().remove(proveedor);
        proveedor.setCiudad(null);
        return proveedor;
    }

    // Convierte a DTO (ciudadDTO) las entidades que se tienen de Ciudad
    public CiudadDTO toCiudadDTO() {
        CiudadDTO ciudadDTO = new CiudadDTO();
        BeanUtils.copyProperties(this, ciudadDTO);
        return ciudadDTO;
    }

    public CiudadDTO toOnlyCiudadDTO() {
        CiudadDTO ciudadDTO = new CiudadDTO();
        BeanUtils.copyProperties(this, ciudadDTO);
        ciudadDTO.setBarrios(null);
        ciudadDTO.setClientes(null);
        ciudadDTO.setFamiliaTarjs(null);
        ciudadDTO.setProveedors(null);
        ciudadDTO.setSucursals(null);
        ciudadDTO.setDepartamento(null);
        return ciudadDTO;
    }

    public CiudadDTO toCiuDTO(Ciudad ciu) {
        CiudadDTO ciuDTO = new CiudadDTO();
        BeanUtils.copyProperties(ciu, ciuDTO);
        return ciuDTO;
    }

    public CiudadDTO toBDCiudadDTO() {
        CiudadDTO ciuDTO = toCiuDTO(this);
        if (this.departamento != null) {
            ciuDTO.setDepartamento(this.getDepartamento()
                    .toSinOtrosDatosDepartamentoDTO());
        }
        ciuDTO.setBarrios(null);
        ciuDTO.setClientes(null);
        ciuDTO.setFamiliaTarjs(null);
        ciuDTO.setProveedors(null);
        ciuDTO.setSucursals(null);
        return ciuDTO;
    }

    public static Ciudad fromCiudadDTO(CiudadDTO ciudadDTO) {
        Ciudad ciudad = new Ciudad();
        BeanUtils.copyProperties(ciudadDTO, ciudad);
        return ciudad;
    }

    public static Ciudad fromCiudadDTOAsociado(CiudadDTO ciudadDTO) {
        Ciudad ciudad = fromCiudadDTO(ciudadDTO);
        ciudad.setDepartamento(Departamento.fromDepartamentoDTO(ciudadDTO
                .getDepartamento()));
        return ciudad;
    }

    public CiudadDTO toCiudadSinOtrosDatos() {
        CiudadDTO ciuDTO = this.toCiudadDTO();
        ciuDTO.setBarrios(null);
        ciuDTO.setClientes(null);
        ciuDTO.setDepartamento(null);
        ciuDTO.setFamiliaTarjs(null);
        ciuDTO.setProveedors(null);
        ciuDTO.setSucursals(null);
        return ciuDTO;
    }

}
