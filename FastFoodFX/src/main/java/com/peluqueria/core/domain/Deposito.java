package com.peluqueria.core.domain;

import com.peluqueria.dto.DepositoDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the marca database table.
 *
 */
@Entity
@Table(name = "deposito", schema = "stock")
@NamedQuery(name = "Deposito.findAll", query = "SELECT m FROM Deposito m")
public class Deposito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_deposito")
    private Long idDeposito;

    @Column(name = "descripcion")
    private String descripcion;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "deposito")
    private List<Existencia> existencia;

    public Deposito() {
    }

    public Long getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Long idDeposito) {
        this.idDeposito = idDeposito;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Existencia> getExistencia() {
        return existencia;
    }

    public void setExistencia(List<Existencia> existencia) {
        this.existencia = existencia;
    }

    public DepositoDTO toDepositoDTO() {
        DepositoDTO marcaDTO = toDepositoDTO(this);
        marcaDTO.setExistencia(null);
        return marcaDTO;
    }

    public static DepositoDTO toDepositoDTO(Deposito marca) {
        DepositoDTO marcaDTO = new DepositoDTO();
        BeanUtils.copyProperties(marca, marcaDTO);
        return marcaDTO;
    }

    public static Deposito fromMarcaDTO(DepositoDTO marcaDTO) {
        Deposito marca = new Deposito();
        BeanUtils.copyProperties(marcaDTO, marca);
        return marca;
    }

}
