package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf7DTO;

/**
 * The persistent class for the promo_temporada_nf7 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf7", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf7.findAll", query = "SELECT p FROM PromoTemporadaNf7 p")
public class PromoTemporadaNf7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf7")
    private Long idPromoTemporadaNf7;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf7Secnom7
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf7_secnom7")
    private Nf7Secnom7 nf7Secnom7;

    public PromoTemporadaNf7() {
    }

    public Long getIdPromoTemporadaNf7() {
        return this.idPromoTemporadaNf7;
    }

    public void setIdPromoTemporadaNf7(Long idPromoTemporadaNf7) {
        this.idPromoTemporadaNf7 = idPromoTemporadaNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf7Secnom7 getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf7 fromPromoTemporadaNf7DTO(PromoTemporadaNf7DTO promoTemporadaNf7DTO) {
        PromoTemporadaNf7 promoTemporadaNf7 = new PromoTemporadaNf7();
        BeanUtils.copyProperties(promoTemporadaNf7DTO, promoTemporadaNf7);
        return promoTemporadaNf7;
    }

    // <-- SERVER
    public static PromoTemporadaNf7DTO toPromoTemporadaNf7DTO(PromoTemporadaNf7 promoTemporadaNf7) {
        PromoTemporadaNf7DTO promoTemporadaNf7DTO = new PromoTemporadaNf7DTO();
        BeanUtils.copyProperties(promoTemporadaNf7, promoTemporadaNf7DTO);
        return promoTemporadaNf7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf7 fromPromoTemporadaNf7DTOEntitiesFull(PromoTemporadaNf7DTO promoTemporadaNf7DTO) {
        PromoTemporadaNf7 promoTemporadaNf7 = fromPromoTemporadaNf7DTO(promoTemporadaNf7DTO);
        if (promoTemporadaNf7DTO.getNf7Secnom7() != null) {
            promoTemporadaNf7.setNf7Secnom7(Nf7Secnom7.fromNf7Secnom7DTO(promoTemporadaNf7DTO.getNf7Secnom7()));
        }
        if (promoTemporadaNf7DTO.getPromoTemporada() != null) {
            promoTemporadaNf7
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf7DTO.getPromoTemporada()));
        }
        return promoTemporadaNf7;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf7DTO toPromoTemporadaNf7DTOEntityFull() {
        PromoTemporadaNf7DTO promoTemporadaNf7DTO = toPromoTemporadaNf7DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf7DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf7DTO.setPromoTemporada(null);
        }
        if (this.getNf7Secnom7() != null) {
            promoTemporadaNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            promoTemporadaNf7DTO.setNf7Secnom7(null);
        }
        return promoTemporadaNf7DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf7DTO toPromoTemporadaNf7DTOEntityNull() {
        PromoTemporadaNf7DTO promoTemporadaNf7DTO = toPromoTemporadaNf7DTO(this);
        promoTemporadaNf7DTO.setPromoTemporada(null);
        promoTemporadaNf7DTO.setNf7Secnom7(null);
        return promoTemporadaNf7DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf7DTO toPromoTemporadaNf7DTONf6() {
        PromoTemporadaNf7DTO promoTemporadaNf7DTO = toPromoTemporadaNf7DTO(this);
        promoTemporadaNf7DTO.setPromoTemporada(null);
        if (this.getNf7Secnom7() != null) {
            promoTemporadaNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            promoTemporadaNf7DTO.setNf7Secnom7(null);
        }
        return promoTemporadaNf7DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
