package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaConvenioDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_tarjeta_convenio database table.
 *
 */
@Entity
@Table(name = "desc_tarjeta_convenio", schema = "factura_cliente")
@NamedQuery(name = "DescTarjetaConvenio.findAll", query = "SELECT d FROM DescTarjetaConvenio d")
public class DescTarjetaConvenio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_tarjeta_convenio")
    private Long idDescTarjetaConvenio;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteCabTarjConvenio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_tarjeta_convenio")
    private FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio;

    public DescTarjetaConvenio() {
    }

    public Long getIdDescTarjetaConvenio() {
        return this.idDescTarjetaConvenio;
    }

    public void setIdDescTarjetaConvenio(Long idDescTarjetaConvenio) {
        this.idDescTarjetaConvenio = idDescTarjetaConvenio;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjConvenio getFacturaClienteCabTarjConvenio() {
        return this.facturaClienteCabTarjConvenio;
    }

    public void setFacturaClienteCabTarjConvenio(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
        this.facturaClienteCabTarjConvenio = facturaClienteCabTarjConvenio;
    }

    public DescTarjetaConvenioDTO toDescTarjetaConvenioDTO() {
        DescTarjetaConvenioDTO descDTO = toDescTarjetaConvenioDTO(this);

        if (this.facturaClienteCabTarjConvenio != null) {
            descDTO.setFacturaClienteCabTarjConvenio(this
                    .getFacturaClienteCabTarjConvenio()
                    .toFacturaClienteCabTarjConvenioDTO());
        }

        return descDTO;
    }

    public DescTarjetaConvenioDTO toDescripcionDescTarjetaConvenioDTO() {
        DescTarjetaConvenioDTO descDTO = toDescTarjetaConvenioDTO(this);
        descDTO.setFacturaClienteCabTarjConvenio(null);
        return descDTO;
    }

    public static DescTarjetaConvenioDTO toDescTarjetaConvenioDTO(
            DescTarjetaConvenio descTarjetaConvenio) {
        DescTarjetaConvenioDTO descDTO = new DescTarjetaConvenioDTO();
        BeanUtils.copyProperties(descTarjetaConvenio, descDTO);
        return descDTO;
    }

    public static DescTarjetaConvenio fromDescTarjetaConvenioDTO(
            DescTarjetaConvenioDTO descDTO) {
        DescTarjetaConvenio desc = new DescTarjetaConvenio();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public static DescTarjetaConvenio fromDescTarjetaConvenioAsociadoDTO(
            DescTarjetaConvenioDTO descDTO) {
        DescTarjetaConvenio desc = fromDescTarjetaConvenioDTO(descDTO);
        desc.setFacturaClienteCabTarjConvenio(FacturaClienteCabTarjConvenio
                .fromFacturaClienteCabTarjConvenioDTO(descDTO
                        .getFacturaClienteCabTarjConvenio()));
        return desc;
    }
}
