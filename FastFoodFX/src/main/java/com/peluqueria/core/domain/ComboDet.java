package com.peluqueria.core.domain;

import com.peluqueria.dto.ComboDetDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the combo_det database table.
 *
 */
@Entity
@Table(name = "combo_det", schema = "stock")
@NamedQuery(name = "ComboDet.findAll", query = "SELECT c FROM ComboDet c")
public class ComboDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_combo_det")
    private Long idComboDet;

    @Column(name = "cant_articulo")
    private Integer cantArticulo;

    @Column(name = "descripcion_articulo")
    private String descripcionArticulo;

    @Column(name = "marca_articulo")
    private String marcaArticulo;

    @Column(name = "precio_articulo")
    private Integer precioArticulo;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to ComboCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_combo_cab")
    private ComboCab comboCab;

    public ComboDet() {
    }

    public Long getIdComboDet() {
        return this.idComboDet;
    }

    public void setIdComboDet(Long idComboDet) {
        this.idComboDet = idComboDet;
    }

    public Integer getCantArticulo() {
        return this.cantArticulo;
    }

    public void setCantArticulo(Integer cantArticulo) {
        this.cantArticulo = cantArticulo;
    }

    public String getDescripcionArticulo() {
        return this.descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public String getMarcaArticulo() {
        return this.marcaArticulo;
    }

    public void setMarcaArticulo(String marcaArticulo) {
        this.marcaArticulo = marcaArticulo;
    }

    public Integer getPrecioArticulo() {
        return this.precioArticulo;
    }

    public void setPrecioArticulo(Integer precioArticulo) {
        this.precioArticulo = precioArticulo;
    }

    public Articulo getArticulo() {
        return this.articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public ComboCab getComboCab() {
        return this.comboCab;
    }

    public void setComboCab(ComboCab comboCab) {
        this.comboCab = comboCab;
    }

    public ComboDetDTO toComboDetDTO() {
        ComboDetDTO cdDTO = toComboDetDTO(this);
        if (this.articulo != null) {
            cdDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }

        if (this.comboCab != null) {
            cdDTO.setComboCab(this.getComboCab().toBDComboCabDTO());
        }

        return cdDTO;
    }

    public ComboDetDTO toComboDetAsociadoDTO() {
        ComboDetDTO cdDTO = toComboDetDTO(this);
        cdDTO.setArticulo(null);
        cdDTO.setComboCab(null);
        return cdDTO;
    }

    public static ComboDetDTO toComboDetDTO(ComboDet comboDet) {
        ComboDetDTO cdDTO = new ComboDetDTO();
        BeanUtils.copyProperties(comboDet, cdDTO);
        return cdDTO;
    }

    public static ComboDet fromComboDetDTO(ComboDetDTO cdDTO) {
        ComboDet cd = new ComboDet();
        BeanUtils.copyProperties(cdDTO, cd);
        return cd;
    }

    public static ComboDet fromComboDetAsociadoDTO(ComboDetDTO cdDTO) {
        ComboDet cd = fromComboDetDTO(cdDTO);
        cd.setArticulo(Articulo.fromArticuloDTO(cdDTO.getArticulo()));
        cd.setComboCab(ComboCab.fromComboCabDTO(cdDTO.getComboCab()));
        return cd;
    }

}
