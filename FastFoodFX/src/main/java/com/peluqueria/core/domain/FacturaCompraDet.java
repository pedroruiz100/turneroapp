package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaCompraDetDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "factura_compra_det", schema = "factura_cliente")
@NamedQuery(name = "FacturaCompraDet.findAll", query = "SELECT f FROM FacturaCompraDet f")
public class FacturaCompraDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_compra_det")
    private Long idFacturaCompraDet;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_compra_cab")
    private FacturaCompraCab facturaCompraCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "cantidad")
    private BigDecimal cantidad;

    @Column(name = "precio")
    private long precio;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "poriva")
    private Integer poriva;

    @Column(name = "iva")
    private long iva;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "sec1")
    private String sec1;

    @Column(name = "sec2")
    private String sec2;

    @Column(name = "cod_articulo")
    private Long codArticulo;

    @Column(name = "deposito")
    private String deposito;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "medida")
    private String medida;

    @Column(name = "contenido")
    private String contenido;

    @Column(name = "existencia")
    private String existencia;

    @Column(name = "descuento")
    private String descuento;
    @Column(name = "iva_extranjero")
    private String ivaExtranjero;

    @Column(name = "precio_extranjero")
    private String precioExtranjero;

    public FacturaCompraDet() {
    }

    public Long getIdFacturaCompraDet() {
        return idFacturaCompraDet;
    }

    public void setIdFacturaCompraDet(Long idFacturaCompraDet) {
        this.idFacturaCompraDet = idFacturaCompraDet;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public String getIvaExtranjero() {
        return ivaExtranjero;
    }

    public void setIvaExtranjero(String ivaExtranjero) {
        this.ivaExtranjero = ivaExtranjero;
    }

    public String getPrecioExtranjero() {
        return precioExtranjero;
    }

    public void setPrecioExtranjero(String precioExtranjero) {
        this.precioExtranjero = precioExtranjero;
    }

    public long getPrecio() {
        return precio;
    }

    public void setPrecio(long precio) {
        this.precio = precio;
    }

    public long getIva() {
        return iva;
    }

    public void setIva(long iva) {
        this.iva = iva;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getPoriva() {
        return poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public String getSec1() {
        return sec1;
    }

    public void setSec1(String sec1) {
        this.sec1 = sec1;
    }

    public String getSec2() {
        return sec2;
    }

    public void setSec2(String sec2) {
        this.sec2 = sec2;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getDeposito() {
        return deposito;
    }

    public void setDeposito(String deposito) {
        this.deposito = deposito;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getContenido() {
        return contenido;
    }

    public FacturaCompraCab getFacturaCompraCab() {
        return facturaCompraCab;
    }

    public void setFacturaCompraCab(FacturaCompraCab facturaCompraCab) {
        this.facturaCompraCab = facturaCompraCab;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getExistencia() {
        return existencia;
    }

    public void setExistencia(String existencia) {
        this.existencia = existencia;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public FacturaCompraDetDTO toFacturaCompraDetDTO() {
        FacturaCompraDetDTO facDTO = toFacturaCompraDetDTO(this);
        if (this.facturaCompraCab != null) {
            facDTO.setFacturaCompraCab(this.getFacturaCompraCab().toFacturaCompraCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return facDTO;
    }

    public FacturaCompraDetDTO toDescriFacturaCompraDetDTO() {
        FacturaCompraDetDTO facDTO = toFacturaCompraDetDTO(this);
        facDTO.setFacturaCompraCab(null);
        facDTO.setArticulo(null);
        return facDTO;
    }

    public static FacturaCompraDetDTO toFacturaCompraDetDTO(
            FacturaCompraDet facturaClienteDet) {
        FacturaCompraDetDTO facDTO = new FacturaCompraDetDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static FacturaCompraDet fromFacturaCompraDetDTO(
            FacturaCompraDetDTO facDTO) {
        FacturaCompraDet fac = new FacturaCompraDet();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaCompraDet fromFacturaCompraDetAsociado(
            FacturaCompraDetDTO facDTO) {
        FacturaCompraDet fac = fromFacturaCompraDetDTO(facDTO);
        fac.setFacturaCompraCab(FacturaCompraCab.fromFacturaCompraCabDTO(facDTO.getFacturaCompraCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
