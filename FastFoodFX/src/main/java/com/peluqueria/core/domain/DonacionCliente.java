package com.peluqueria.core.domain;

import com.peluqueria.dto.DonacionClienteDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the donacion_cliente database table.
 *
 */
@Entity
@Table(name = "donacion_cliente", schema = "factura_cliente")
@NamedQuery(name = "DonacionCliente.findAll", query = "SELECT d FROM DonacionCliente d")
public class DonacionCliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_donacion_cliente")
    private Long idDonacionCliente;

    @Column(name = "monto_donacion")
    private Integer montoDonacion;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public DonacionCliente() {
    }

    public Long getIdDonacionCliente() {
        return this.idDonacionCliente;
    }

    public void setIdDonacionCliente(Long idDonacionCliente) {
        this.idDonacionCliente = idDonacionCliente;
    }

    public Integer getMontoDonacion() {
        return this.montoDonacion;
    }

    public void setMontoDonacion(Integer montoDonacion) {
        this.montoDonacion = montoDonacion;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public DonacionClienteDTO toDonacionClienteDTO() {
        DonacionClienteDTO donaDTO = toDonacionClienteDTO(this);
        if (this.facturaClienteCab != null) {
            donaDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return donaDTO;
    }

    public DonacionClienteDTO toBDDonacionClienteDTO() {
        DonacionClienteDTO donaDTO = toDonacionClienteDTO(this);
        donaDTO.setFacturaClienteCab(null);
        return donaDTO;
    }

    public static DonacionClienteDTO toDonacionClienteDTO(
            DonacionCliente donacionCliente) {
        DonacionClienteDTO donaDTO = new DonacionClienteDTO();
        BeanUtils.copyProperties(donacionCliente, donaDTO);
        return donaDTO;
    }

    public static DonacionCliente fromDonacionClienteDTO(
            DonacionClienteDTO donaDTO) {
        DonacionCliente dona = new DonacionCliente();
        BeanUtils.copyProperties(donaDTO, dona);
        return dona;
    }

    public static DonacionCliente fromBDDonacionCliente(
            DonacionClienteDTO donaDTO) {
        DonacionCliente dona = fromDonacionClienteDTO(donaDTO);
        dona.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(donaDTO.getFacturaClienteCab()));
        return dona;
    }

}
