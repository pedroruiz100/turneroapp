package com.peluqueria.core.domain;

import com.peluqueria.dto.CancelacionFacturaDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the cancelacion_factura database table.
 */
@Entity
@Table(name = "cancelacion_factura", schema = "caja")
@NamedQuery(name = "CancelacionFactura.findAll", query = "SELECT c FROM CancelacionFactura c")
public class CancelacionFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cancelacion_factura")
    private Long idCancelacionFactura;

    @Column(name = "fecha_cancelacion")
    private Timestamp fechaCancelacion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cajero")
    private Usuario usuarioCajero;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_supervisor")
    private Usuario usuarioSupervisor;

    // bi-directional many-to-one association to MotivoCancelacionFactura
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_motivo_cancelacion")
    private MotivoCancelacionFactura motivoCancelacionFactura;

    public CancelacionFactura() {
    }

    public Long getIdCancelacionFactura() {
        return this.idCancelacionFactura;
    }

    public void setIdCancelacionFactura(Long idCancelacionFactura) {
        this.idCancelacionFactura = idCancelacionFactura;
    }

    public Timestamp getFechaCancelacion() {
        return this.fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public MotivoCancelacionFactura getMotivoCancelacionFactura() {
        return this.motivoCancelacionFactura;
    }

    public void setMotivoCancelacionFactura(
            MotivoCancelacionFactura motivoCancelacionFactura) {
        this.motivoCancelacionFactura = motivoCancelacionFactura;
    }

    public Usuario getUsuarioCajero() {
        return this.usuarioCajero;
    }

    public void setUsuarioCajero(Usuario usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public Usuario getUsuarioSupervisor() {
        return this.usuarioSupervisor;
    }

    public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public CancelacionFacturaDTO toCancelacionFacturaDTO() {
        CancelacionFacturaDTO canDTO = toCancelacionFacturaDTO(this);
        if (this.facturaClienteCab != null) {
            canDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }

        if (this.motivoCancelacionFactura != null) {
            canDTO.setMotivoCancelacionFactura(this
                    .getMotivoCancelacionFactura()
                    .toBDMotivoCancelacionFacturaDTO());
        }

        if (this.usuarioCajero != null) {
            canDTO.setUsuarioCajero(this.getUsuarioCajero().toBDUsuarioDTO());
        }

        if (this.usuarioSupervisor != null) {
            canDTO.setUsuarioSupervisor(this.getUsuarioSupervisor()
                    .toBDUsuarioDTO());
        }
        return canDTO;
    }

    public static CancelacionFacturaDTO toCancelacionFacturaDTO(
            CancelacionFactura cancelacionFactura) {
        CancelacionFacturaDTO canDTO = new CancelacionFacturaDTO();
        BeanUtils.copyProperties(cancelacionFactura, canDTO);
        return canDTO;
    }

    public CancelacionFacturaDTO toBDCancelacionFacturaDTO() {
        CancelacionFacturaDTO canDTO = toCancelacionFacturaDTO(this);
        canDTO.setFacturaClienteCab(null);
        canDTO.setMotivoCancelacionFactura(null);
        canDTO.setUsuarioCajero(null);
        canDTO.setUsuarioSupervisor(null);
        return canDTO;
    }

    public static CancelacionFactura fromCancelacionFacturaDTO(
            CancelacionFacturaDTO canDTO) {
        CancelacionFactura can = new CancelacionFactura();
        BeanUtils.copyProperties(canDTO, can);
        return can;
    }

    public static CancelacionFactura fromCancelacionFacturaAsociadoDTO(
            CancelacionFacturaDTO canDTO) {
        CancelacionFactura can = fromCancelacionFacturaDTO(canDTO);
        can.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(canDTO.getFacturaClienteCab()));
        can.setMotivoCancelacionFactura(MotivoCancelacionFactura
                .fromMotivoCancelacionFacturaDTO(canDTO
                        .getMotivoCancelacionFactura()));
        can.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(canDTO
                .getUsuarioCajero()));
        can.setUsuarioSupervisor(Usuario.fromUsuarioSupervisorDTO(canDTO
                .getUsuarioSupervisor()));
        return can;
    }

}
