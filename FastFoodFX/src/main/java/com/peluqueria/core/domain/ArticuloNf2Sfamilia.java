package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf2SfamiliaDTO;

/**
 * The persistent class for the articulo_nf2_sfamilia database table.
 *
 */
@Entity
@Table(name = "articulo_nf2_sfamilia", schema = "stock")
@NamedQuery(name = "ArticuloNf2Sfamilia.findAll", query = "SELECT a FROM ArticuloNf2Sfamilia a")
public class ArticuloNf2Sfamilia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf2_sfamilia_articulo")
    private Long idNf2SfamiliaArticulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    public ArticuloNf2Sfamilia() {
    }

    public Long getIdNf2SfamiliaArticulo() {
        return this.idNf2SfamiliaArticulo;
    }

    public void setIdNf2SfamiliaArticulo(Long idNf2SfamiliaArticulo) {
        this.idNf2SfamiliaArticulo = idNf2SfamiliaArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return this.nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf2Sfamilia fromArticuloNf2SfamiliaDTO(ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO) {
        ArticuloNf2Sfamilia articuloNf2Sfamilia = new ArticuloNf2Sfamilia();
        BeanUtils.copyProperties(articuloNf2SfamiliaDTO, articuloNf2Sfamilia);
        return articuloNf2Sfamilia;
    }

    // <-- SERVER
    public static ArticuloNf2SfamiliaDTO toArticuloNf2SfamiliaDTO(ArticuloNf2Sfamilia articuloNf2Sfamilia) {
        ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO = new ArticuloNf2SfamiliaDTO();
        BeanUtils.copyProperties(articuloNf2Sfamilia, articuloNf2SfamiliaDTO);
        return articuloNf2SfamiliaDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf2SfamiliaDTO toArticuloNf2SfamiliaDTOEntitiesNull(ArticuloNf2Sfamilia articuloNf2Sfamilia) {
        ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO = toArticuloNf2SfamiliaDTO(articuloNf2Sfamilia);
        articuloNf2SfamiliaDTO.setArticulo(null);
        articuloNf2SfamiliaDTO.setNf2Sfamilia(null);
        return articuloNf2SfamiliaDTO;
    }

    // <-- SERVER
    public ArticuloNf2SfamiliaDTO toArticuloNf2SfamiliaDTOEntitiesNull() {
        ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO = toArticuloNf2SfamiliaDTO(this);
        articuloNf2SfamiliaDTO.setArticulo(null);
        articuloNf2SfamiliaDTO.setNf2Sfamilia(null);
        return articuloNf2SfamiliaDTO;
    }

    // <-- SERVER
    public ArticuloNf2SfamiliaDTO toArticuloNf2SfamiliaDTOEntitiesNf() {
        ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO = toArticuloNf2SfamiliaDTO(this);
        articuloNf2SfamiliaDTO.setArticulo(null);
        if (this.getNf2Sfamilia() != null) {
            articuloNf2SfamiliaDTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            articuloNf2SfamiliaDTO.setNf2Sfamilia(null);
        }
        return articuloNf2SfamiliaDTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf2Sfamilia fromArticuloNf2SfamiliaDTOEntitiesFull(
            ArticuloNf2SfamiliaDTO articuloNf2SfamiliaDTO) {
        ArticuloNf2Sfamilia articuloNf2Sfamilia = fromArticuloNf2SfamiliaDTO(articuloNf2SfamiliaDTO);
        if (articuloNf2SfamiliaDTO.getArticulo() != null) {
            articuloNf2Sfamilia.setArticulo(Articulo.fromArticuloDTO(articuloNf2SfamiliaDTO.getArticulo()));
        }
        if (articuloNf2SfamiliaDTO.getNf2Sfamilia() != null) {
            articuloNf2Sfamilia.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(articuloNf2SfamiliaDTO.getNf2Sfamilia()));
        }
        return articuloNf2Sfamilia;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
