package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabRetencionDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_cheque database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_retencion", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabRetencion.findAll", query = "SELECT f FROM FacturaClienteCabRetencion f")
public class FacturaClienteCabRetencion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_retencion")
    private Long idFacturaClienteCabRetencion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    @Column(name = "monto")
    private Integer monto;

    public FacturaClienteCabRetencion() {
    }

    public Long getIdFacturaClienteCabRetencion() {
        return idFacturaClienteCabRetencion;
    }

    public void setIdFacturaClienteCabRetencion(
            Long idFacturaClienteCabRetencion) {
        this.idFacturaClienteCabRetencion = idFacturaClienteCabRetencion;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabRetencionDTO toFacturaClienteCabRetencionDTO() {
        FacturaClienteCabRetencionDTO facDTO = toFacturaClienteCabRetencionDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    private FacturaClienteCabRetencionDTO toFacturaClienteCabRetencionDTO(
            FacturaClienteCabRetencion facturaClienteCabRetencion) {
        FacturaClienteCabRetencionDTO factDTO = new FacturaClienteCabRetencionDTO();
        BeanUtils.copyProperties(facturaClienteCabRetencion, factDTO);
        return factDTO;
    }

    public static FacturaClienteCabRetencion fromFacturaClienteCabRetencion(FacturaClienteCabRetencionDTO facDTO) {
        FacturaClienteCabRetencion fac = new FacturaClienteCabRetencion();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabRetencion fromFacturaClienteCabRetencionAsociado(FacturaClienteCabRetencionDTO facDTO) {
        FacturaClienteCabRetencion fac = fromFacturaClienteCabRetencion(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}
