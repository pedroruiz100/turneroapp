package com.peluqueria.core.domain;

import com.peluqueria.dto.PreparacionDetalleDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_cheque database table.
 *
 */
@Entity
@Table(name = "preparacion_detalle", schema = "factura_cliente")
@NamedQuery(name = "PreparacionDetalle.findAll", query = "SELECT f FROM PreparacionDetalle f")
public class PreparacionDetalle implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_preparacion_detalle")
    private Long idPreparacionDetalle;

    @Column(name = "fecha_compra")
    private Date fechaCompra;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_preparacion_cab")
    private PreparacionCabecera preparacionCabecera;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_detalle_gasto")
    private DetalleGasto detalleGasto;

    @Column(name = "costo")
    private Long costo;

    @Column(name = "cantidad")
    private BigDecimal cantidad;

    public PreparacionDetalle() {
    }

    public Long getIdPreparacionDetalle() {
        return idPreparacionDetalle;
    }

    public void setIdPreparacionDetalle(Long idPreparacionDetalle) {
        this.idPreparacionDetalle = idPreparacionDetalle;
    }

    public Date getFechaCompra() {
        return fechaCompra;
    }

    public void setFechaCompra(Date fechaCompra) {
        this.fechaCompra = fechaCompra;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public DetalleGasto getDetalleGasto() {
        return detalleGasto;
    }

    public void setDetalleGasto(DetalleGasto detalleGasto) {
        this.detalleGasto = detalleGasto;
    }

    public PreparacionCabecera getPreparacionCabecera() {
        return preparacionCabecera;
    }

    public void setPreparacionCabecera(PreparacionCabecera preparacionCabecera) {
        this.preparacionCabecera = preparacionCabecera;
    }

    public Long getCosto() {
        return costo;
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public PreparacionDetalleDTO toBDPreparacionDetalleDTO() {
        PreparacionDetalleDTO facDTO = toPreparacionDetalleDTO(this);
        facDTO.setPreparacionCabecera(null);
        facDTO.setDetalleGasto(null);
        return facDTO;
    }

    public PreparacionDetalleDTO toPreparacionDetalleDTO() {
        PreparacionDetalleDTO facDTO = toPreparacionDetalleDTO(this);
        if (this.preparacionCabecera != null) {
            PreparacionCabecera fac = this.getPreparacionCabecera();
            facDTO.setPreparacionCabecera(fac.toPreparacionCabeceraDTO());
        }
        if (this.detalleGasto != null) {
            DetalleGasto fac = this.getDetalleGasto();
            facDTO.setDetalleGasto(fac.toDetalleGastoDTO());
        }
        return facDTO;
    }

    public static PreparacionDetalleDTO toPreparacionDetalleDTO(
            PreparacionDetalle facturaClienteCabCheque) {
        PreparacionDetalleDTO facDTO = new PreparacionDetalleDTO();
        BeanUtils.copyProperties(facturaClienteCabCheque, facDTO);
        return facDTO;
    }

    public static PreparacionDetalle fromPreparacionDetalleDTO(
            PreparacionDetalleDTO facDTO) {
        PreparacionDetalle fac = new PreparacionDetalle();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static PreparacionDetalle fromPreparacionDetalleAsociadoDTO(
            PreparacionDetalleDTO facDTO) {
        PreparacionDetalle fac = fromPreparacionDetalleDTO(facDTO);
        fac.setPreparacionCabecera(PreparacionCabecera.fromPreparacionCabeceraDTO(facDTO
                .getPreparacionCabecera()));
        fac.setDetalleGasto(DetalleGasto.fromDetalleGastoDTO(facDTO
                .getDetalleGasto()));
        return fac;
    }

}
