package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf3DTO;

/**
 * The persistent class for the id_descuento_tarjeta_cab_nf3 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf3", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf3.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf3 d")
public class DescuentoTarjetaCabNf3 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf3")
    private Long idDescuentoTarjetaCabNf3;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    public DescuentoTarjetaCabNf3() {
    }

    public Long getIdDescuentoTarjetaCabNf3() {
        return this.idDescuentoTarjetaCabNf3;
    }

    public void setIdDescuentoTarjetaCabNf3(Long idDescuentoTarjetaCabNf3) {
        this.idDescuentoTarjetaCabNf3 = idDescuentoTarjetaCabNf3;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf3 fromDescuentoTarjetaCabNf3DTO(DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO) {
        DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3 = new DescuentoTarjetaCabNf3();
        BeanUtils.copyProperties(descuentoTarjetaCabNf3DTO, descuentoTarjetaCabNf3);
        return descuentoTarjetaCabNf3;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf3DTO toDescuentoTarjetaCabNf3DTO(DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3) {
        DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO = new DescuentoTarjetaCabNf3DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf3, descuentoTarjetaCabNf3DTO);
        return descuentoTarjetaCabNf3DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf3 fromDescuentoTarjetaCabNf3DTOEntitiesFull(DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO) {
        DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3 = fromDescuentoTarjetaCabNf3DTO(descuentoTarjetaCabNf3DTO);
        if (descuentoTarjetaCabNf3DTO.getNf3Sseccion() != null) {
            descuentoTarjetaCabNf3.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(descuentoTarjetaCabNf3DTO.getNf3Sseccion()));
        }
        if (descuentoTarjetaCabNf3DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf3
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf3DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf3;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf3DTO toDescuentoTarjetaCabNf3DTOEntityFull() {
        DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO = toDescuentoTarjetaCabNf3DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf3DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf3DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf3Sseccion() != null) {
            descuentoTarjetaCabNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            descuentoTarjetaCabNf3DTO.setNf3Sseccion(null);
        }
        return descuentoTarjetaCabNf3DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf3DTO toDescuentoTarjetaCabNf3DTOEntityNull() {
        DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO = toDescuentoTarjetaCabNf3DTO(this);
        descuentoTarjetaCabNf3DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf3DTO.setNf3Sseccion(null);
        return descuentoTarjetaCabNf3DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf3DTO toDescuentoTarjetaCabNf3DTONf2() {
        DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO = toDescuentoTarjetaCabNf3DTO(this);
        descuentoTarjetaCabNf3DTO.setDescuentoTarjetaCab(null);
        if (this.getNf3Sseccion() != null) {
            descuentoTarjetaCabNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            descuentoTarjetaCabNf3DTO.setNf3Sseccion(null);
        }
        return descuentoTarjetaCabNf3DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
