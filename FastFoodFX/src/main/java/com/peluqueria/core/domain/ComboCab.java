package com.peluqueria.core.domain;

import com.peluqueria.dto.ComboCabDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the combo_cab database table.
 *
 */
@Entity
@Table(name = "combo_cab", schema = "stock")
@NamedQuery(name = "ComboCab.findAll", query = "SELECT c FROM ComboCab c")
public class ComboCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_combo_cab")
    private Long idComboCab;

    private Integer cantidad;

    @Column(name = "cod_combo")
    private Integer codCombo;

    @Column(name = "descripcion_combo")
    private String descripcionCombo;

    @Column(name = "descripcion_proveedor")
    private String descripcionProveedor;

    @Column(name = "descripcion_sucursal")
    private String descripcionSucursal;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_inicio")
    private Timestamp fechaInicio;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    // bi-directional many-to-one association to Proveedor
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to Sucursal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    @Column(name = "monto_proveedor")
    private Integer montoProveedor;

    @Column(name = "motivo_combo")
    private String motivoCombo;

    @Column(name = "precio_combo_sin_desc")
    private Integer precioComboSinDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Iva
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_iva")
    private Iva iva;

    // bi-directional many-to-one association to TipoCombo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_combo")
    private TipoCombo tipoCombo;

    // bi-directional many-to-one association to ComboDet
    @OneToMany(mappedBy = "comboCab")
    private List<ComboDet> comboDets;

    public ComboCab() {
    }

    public Long getIdComboCab() {
        return this.idComboCab;
    }

    public void setIdComboCab(Long idComboCab) {
        this.idComboCab = idComboCab;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Integer getCodCombo() {
        return this.codCombo;
    }

    public void setCodCombo(Integer codCombo) {
        this.codCombo = codCombo;
    }

    public String getDescripcionCombo() {
        return this.descripcionCombo;
    }

    public void setDescripcionCombo(String descripcionCombo) {
        this.descripcionCombo = descripcionCombo;
    }

    public String getDescripcionProveedor() {
        return this.descripcionProveedor;
    }

    public void setDescripcionProveedor(String descripcionProveedor) {
        this.descripcionProveedor = descripcionProveedor;
    }

    public String getDescripcionSucursal() {
        return this.descripcionSucursal;
    }

    public void setDescripcionSucursal(String descripcionSucursal) {
        this.descripcionSucursal = descripcionSucursal;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getMontoProveedor() {
        return this.montoProveedor;
    }

    public void setMontoProveedor(Integer montoProveedor) {
        this.montoProveedor = montoProveedor;
    }

    public String getMotivoCombo() {
        return this.motivoCombo;
    }

    public void setMotivoCombo(String motivoCombo) {
        this.motivoCombo = motivoCombo;
    }

    public Integer getPrecioComboSinDesc() {
        return this.precioComboSinDesc;
    }

    public void setPrecioComboSinDesc(Integer precioComboSinDesc) {
        this.precioComboSinDesc = precioComboSinDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Iva getIva() {
        return this.iva;
    }

    public void setIva(Iva iva) {
        this.iva = iva;
    }

    public TipoCombo getTipoCombo() {
        return this.tipoCombo;
    }

    public void setTipoCombo(TipoCombo tipoCombo) {
        this.tipoCombo = tipoCombo;
    }

    public List<ComboDet> getComboDets() {
        return this.comboDets;
    }

    public void setComboDets(List<ComboDet> comboDets) {
        this.comboDets = comboDets;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public ComboCabDTO toComboCabDTO() {
        ComboCabDTO cDTO = toComboCabDTO(this);
        if (this.iva != null) {
            cDTO.setIva(this.getIva().toIvaDTO());
        }
        if (this.proveedor != null) {
            cDTO.setProveedor(this.getProveedor().toProveedorBdDTO());
        }

        if (this.sucursal != null) {
            cDTO.setSucursal(this.getSucursal().toSucursalDescriDTO());
        }

        if (this.tipoCombo != null) {
            cDTO.setTipoCombo(this.getTipoCombo().toBDTipoComboDTO());
        }
        cDTO.setComboDets(null);
        return cDTO;
    }

    public ComboCabDTO toBDComboCabDTO() {
        ComboCabDTO ccDTO = toComboCabDTO(this);
        ccDTO.setIva(null);
        ccDTO.setProveedor(null);
        ccDTO.setSucursal(null);
        ccDTO.setTipoCombo(null);
        return ccDTO;
    }

    public static ComboCabDTO toComboCabDTO(ComboCab comboCab) {
        ComboCabDTO cDTO = new ComboCabDTO();
        BeanUtils.copyProperties(comboCab, cDTO);
        return cDTO;
    }

    public static ComboCab fromComboCabDTO(ComboCabDTO ccDTO) {
        ComboCab cc = new ComboCab();
        BeanUtils.copyProperties(ccDTO, cc);
        return cc;
    }

    public static ComboCab fromComboCabAsociadoDTO(ComboCabDTO ccDTO) {
        ComboCab cc = fromComboCabDTO(ccDTO);
        cc.setIva(Iva.fromIvaDTO(ccDTO.getIva()));
        cc.setProveedor(Proveedor.fromProveedorDTO(ccDTO.getProveedor()));
        cc.setSucursal(Sucursal.fromSucursalDTO(ccDTO.getSucursal()));
        cc.setTipoCombo(TipoCombo.fromTipoComboDTO(ccDTO.getTipoCombo()));
        return cc;
    }

}
