package com.peluqueria.core.domain;

import com.peluqueria.dto.ProveedorRubroDTO;
import com.peluqueria.dto.RubroDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rubro database table.
 *
 */
@Entity
@Table(name = "rubro", schema = "cuenta")
@NamedQuery(name = "Rubro.findAll", query = "SELECT r FROM Rubro r")
public class Rubro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rubro")
    private Long idRubro;

    private String descripcion;

    // bi-directional many-to-one association to ProveedorRubro
    @OneToMany(mappedBy = "rubro")
    private List<ProveedorRubro> proveedorRubros;

    public Rubro() {
    }

    public Long getIdRubro() {
        return this.idRubro;
    }

    public void setIdRubro(Long idRubro) {
        this.idRubro = idRubro;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<ProveedorRubro> getProveedorRubros() {
        return this.proveedorRubros;
    }

    public void setProveedorRubros(List<ProveedorRubro> proveedorRubros) {
        this.proveedorRubros = proveedorRubros;
    }

    public RubroDTO toBDRubroDTO() {
        RubroDTO rubroDTO = toRubroDTO(this);
        rubroDTO.setProveedorRubros(null);
        return rubroDTO;
    }

    public RubroDTO toRubroDTO() {
        RubroDTO rubroDTO = toRubroDTO(this);
        if (!this.proveedorRubros.isEmpty()) {
            List<ProveedorRubro> pr = this.getProveedorRubros();
            List<ProveedorRubroDTO> prDTO = new ArrayList<ProveedorRubroDTO>();
            for (ProveedorRubro p : pr) {
                prDTO.add(p.toBDProveedorRubroDTO());
            }
            rubroDTO.setProveedorRubros(prDTO);
        }
        return rubroDTO;
    }

    public static RubroDTO toRubroDTO(Rubro rubro) {
        RubroDTO rubroDTO = new RubroDTO();
        BeanUtils.copyProperties(rubro, rubroDTO);
        return rubroDTO;
    }

    public static Rubro fromRubro(RubroDTO rubroDTO) {
        Rubro rubro = new Rubro();
        BeanUtils.copyProperties(rubroDTO, rubro);
        return rubro;
    }
}
