package com.peluqueria.core.domain;

import com.peluqueria.dto.RetiroDineroDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the retiro_dinero database table.
 *
 */
@Entity
@Table(name = "retiro_dinero", schema = "caja")
@NamedQuery(name = "RetiroDinero.findAll", query = "SELECT r FROM RetiroDinero r")
public class RetiroDinero implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_retiro")
    private Long idRetiro;

    @Column(name = "fecha_retiro")
    private Timestamp fechaRetiro;

    // bi-directional many-to-one association to MotivoRetiroDinero
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cajero")
    private Usuario usuarioCajero;

    // bi-directional many-to-one association to MotivoRetiroDinero
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_supervisor")
    private Usuario usuarioSupervisor;

    @Column(name = "monto_retiro")
    private BigDecimal montoRetiro;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caja")
    private Caja caja;

    // bi-directional many-to-one association to MotivoRetiroDinero
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_motivo_retiro")
    private MotivoRetiroDinero motivoRetiroDinero;

    public RetiroDinero() {
    }

    public Long getIdRetiro() {
        return this.idRetiro;
    }

    public void setIdRetiro(Long idRetiro) {
        this.idRetiro = idRetiro;
    }

    public Timestamp getFechaRetiro() {
        return this.fechaRetiro;
    }

    public void setFechaRetiro(Timestamp fechaRetiro) {
        this.fechaRetiro = fechaRetiro;
    }

    public BigDecimal getMontoRetiro() {
        return this.montoRetiro;
    }

    public void setMontoRetiro(BigDecimal montoRetiro) {
        this.montoRetiro = montoRetiro;
    }

    public Caja getCaja() {
        return this.caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public MotivoRetiroDinero getMotivoRetiroDinero() {
        return this.motivoRetiroDinero;
    }

    public void setMotivoRetiroDinero(MotivoRetiroDinero motivoRetiroDinero) {
        this.motivoRetiroDinero = motivoRetiroDinero;
    }

    public Usuario getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(Usuario usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public Usuario getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public RetiroDineroDTO toBDRetiroDineroDTO() {
        RetiroDineroDTO reDTO = toRetiroDineroDTO(this);
        reDTO.setCaja(null);
        reDTO.setMotivoRetiroDinero(null);
        reDTO.setUsuarioCajero(null);
        reDTO.setUsuarioSupervisor(null);
        return reDTO;
    }

    public RetiroDineroDTO toRetiroDineroDTO() {
        RetiroDineroDTO reDTO = toRetiroDineroDTO(this);
        if (this.caja != null) {
            reDTO.setCaja(this.getCaja().toBDCajaDTO());
        }

        if (this.motivoRetiroDinero != null) {
            reDTO.setMotivoRetiroDinero(this.getMotivoRetiroDinero()
                    .toBDMotivoRetiroDineroDTO());
        }

        if (this.usuarioCajero != null) {
            reDTO.setUsuarioCajero(this.getUsuarioCajero().toBDUsuarioDTO());
        }

        if (this.usuarioSupervisor != null) {
            reDTO.setUsuarioSupervisor(this.getUsuarioSupervisor()
                    .toBDUsuarioDTO());
        }
        return reDTO;
    }

    public static RetiroDineroDTO toRetiroDineroDTO(RetiroDinero retiroDinero) {
        RetiroDineroDTO rdDTO = new RetiroDineroDTO();
        BeanUtils.copyProperties(retiroDinero, rdDTO);
        return rdDTO;
    }

    // Se debe tener en cuenta que para el retiro de dinero tbn debe convertir a
    // entidad
    // las relaciones caja, usuarioSupervisor, usuarioCajero, motivoRetiro
    public static RetiroDinero fromRetiroDineroDTO(RetiroDineroDTO rdDTO) {
        RetiroDinero retiroDinero = new RetiroDinero();
        BeanUtils.copyProperties(rdDTO, retiroDinero);
        return retiroDinero;
    }

    public static RetiroDinero fromRetiroDineroAsociadoDTO(RetiroDineroDTO rdDTO) {
        RetiroDinero retiroDinero = fromRetiroDineroDTO(rdDTO);
        retiroDinero.setCaja(Caja.fromCajaDTO(rdDTO.getCaja()));
        retiroDinero.setMotivoRetiroDinero(MotivoRetiroDinero
                .fromMotivoRetiroDineroDTO(rdDTO.getMotivoRetiroDinero()));
        retiroDinero.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(rdDTO
                .getUsuarioCajero()));
        retiroDinero.setUsuarioSupervisor(Usuario
                .fromUsuarioSupervisorDTO(rdDTO.getUsuarioSupervisor()));
        return retiroDinero;
    }

}
