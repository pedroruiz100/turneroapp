package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.FacturaCompraCabDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the apertura_caja database table.
 *
 */
@Entity
@Table(name = "factura_compra_cab", schema = "factura_cliente")
@NamedQuery(name = "FacturaCompraCab.findAll", query = "SELECT a FROM FacturaCompraCab a")
public class FacturaCompraCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_compra_cab")
    private Long idFacturaCompraCab;

    @Column(name = "tipo_movimiento")
    private String tipoMovimiento;

    @Column(name = "nro_orden")
    private String nroOrden;

    @Column(name = "nro_doc")
    private String nroDoc;

    @Column(name = "fecha_doc")
    private Date fechaDoc;

    @Column(name = "oc")
    private String oc;

    @Column(name = "sucursal")
    private String sucursal;
    @Column(name = "saldo")
    private int saldo;
    @Column(name = "cantidad")
    private String cantidad;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "tipo_documento")
    private String tipoDocumento;

    @Column(name = "clase_documento")
    private String claseDocumento;

    @Column(name = "moneda")
    private String moneda;

    @Column(name = "cotizacion")
    private long cotizacion;

    @Column(name = "estado_factura")
    private String estadoFactura;

    @Column(name = "cc")
    private boolean cc;

    @Column(name = "sc")
    private boolean sc;

    @Column(name = "sl")
    private boolean sl;

    @Column(name = "total")
    private int total;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_cab")
    private PedidoCab pedidoCab;

    public FacturaCompraCab() {
    }

    public Long getIdFacturaCompraCab() {
        return idFacturaCompraCab;
    }

    public void setIdFacturaCompraCab(Long idFacturaCompraCab) {
        this.idFacturaCompraCab = idFacturaCompraCab;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    public long getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(long cotizacion) {
        this.cotizacion = cotizacion;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getOc() {
        return oc;
    }

    public boolean isCc() {
        return cc;
    }

    public void setCc(boolean cc) {
        this.cc = cc;
    }

    public boolean isSc() {
        return sc;
    }

    public void setSc(boolean sc) {
        this.sc = sc;
    }

    public boolean isSl() {
        return sl;
    }

    public void setSl(boolean sl) {
        this.sl = sl;
    }

    public void setOc(String oc) {
        this.oc = oc;
    }

    public String getNroOrden() {
        return nroOrden;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public PedidoCab getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCab pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public void setNroOrden(String nroOrden) {
        this.nroOrden = nroOrden;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

//    public PedidoCab getPedidoCab() {
//        return pedidoCab;
//    }
//
//    public void setPedidoCab(PedidoCab pedidoCab) {
//        this.pedidoCab = pedidoCab;
//    }
    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public Date getFechaDoc() {
        return fechaDoc;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public void setFechaDoc(Date fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    // DTOs****************************************************************************************
    // RELACION CON UsuarioSupervisor
//    public AperturaCajaDTO toAperturaCajaDTO() {
//        AperturaCajaDTO apCajaDTO = this.toAperturaCajaDTO(this);
//        return apCajaDTO;
//    }
    public FacturaCompraCabDTO toFacturaCompraCabDTO() {
        FacturaCompraCabDTO apDTO = toFacturaCompraCabDTO(this);
//        apDTO.set(null);
//        apDTO.setUsuarioCajero(null);
        return apDTO;
    }

    public FacturaCompraCabDTO toFacturaCompraCabDTO(FacturaCompraCab ap) {
        FacturaCompraCabDTO acDTO = new FacturaCompraCabDTO();
        BeanUtils.copyProperties(ap, acDTO);
        return acDTO;
    }

    // METODO QUE
    // Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
    // CAJERO Y USUARIO SUPERVISOR
    public static FacturaCompraCab fromFacturaCompraCabDTO(FacturaCompraCabDTO dto) {
        FacturaCompraCab apCa = new FacturaCompraCab();
        BeanUtils.copyProperties(dto, apCa);
        apCa.setProveedor(Proveedor.fromProveedorDTO(dto.getProveedor()));
//        apCa.setCaja(Caja.fromCajaDTO(dto.getCaja()));
//        apCa.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(dto
//                .getUsuarioCajero()));
        return apCa;
    }
}
