package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf2DTO;

/**
 * The persistent class for the id_descuento_tarjeta_cab_nf2 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf2", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf2.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf2 d")
public class DescuentoTarjetaCabNf2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf2")
    private Long idDescuentoTarjetaCabNf2;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    public DescuentoTarjetaCabNf2() {
    }

    public Long getIdDescuentoTarjetaCabNf2() {
        return this.idDescuentoTarjetaCabNf2;
    }

    public void setIdDescuentoTarjetaCabNf2(Long idDescuentoTarjetaCabNf2) {
        this.idDescuentoTarjetaCabNf2 = idDescuentoTarjetaCabNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf2 fromDescuentoTarjetaCabNf2DTO(DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO) {
        DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2 = new DescuentoTarjetaCabNf2();
        BeanUtils.copyProperties(descuentoTarjetaCabNf2DTO, descuentoTarjetaCabNf2);
        return descuentoTarjetaCabNf2;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf2DTO toDescuentoTarjetaCabNf2DTO(DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2) {
        DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO = new DescuentoTarjetaCabNf2DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf2, descuentoTarjetaCabNf2DTO);
        return descuentoTarjetaCabNf2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf2 fromDescuentoTarjetaCabNf2DTOEntitiesFull(DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO) {
        DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2 = fromDescuentoTarjetaCabNf2DTO(descuentoTarjetaCabNf2DTO);
        if (descuentoTarjetaCabNf2DTO.getNf2Sfamilia() != null) {
            descuentoTarjetaCabNf2.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(descuentoTarjetaCabNf2DTO.getNf2Sfamilia()));
        }
        if (descuentoTarjetaCabNf2DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf2
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf2DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf2;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf2DTO toDescuentoTarjetaCabNf2DTOEntityFull() {
        DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO = toDescuentoTarjetaCabNf2DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf2DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf2DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf2Sfamilia() != null) {
            descuentoTarjetaCabNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            descuentoTarjetaCabNf2DTO.setNf2Sfamilia(null);
        }
        return descuentoTarjetaCabNf2DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf2DTO toDescuentoTarjetaCabNf2DTOEntityNull() {
        DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO = toDescuentoTarjetaCabNf2DTO(this);
        descuentoTarjetaCabNf2DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf2DTO.setNf2Sfamilia(null);
        return descuentoTarjetaCabNf2DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf2DTO toDescuentoTarjetaCabNf2DTONf1() {
        DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO = toDescuentoTarjetaCabNf2DTO(this);
        descuentoTarjetaCabNf2DTO.setDescuentoTarjetaCab(null);
        if (this.getNf2Sfamilia() != null) {
            descuentoTarjetaCabNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            descuentoTarjetaCabNf2DTO.setNf2Sfamilia(null);
        }
        return descuentoTarjetaCabNf2DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
