package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoFielDetDTO;
import com.peluqueria.dto.DiaDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_fiel_det database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_det", schema = "cuenta")
@NamedQuery(name = "DescuentoFielDet.findAll", query = "SELECT d FROM DescuentoFielDet d")
public class DescuentoFielDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_det")
    private Long idDescuentoFielDet;

    // bi-directional many-to-one association to Dias
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dia")
    private Dias dia;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    public DescuentoFielDet() {
    }

    public Long getIdDescuentoFielDet() {
        return this.idDescuentoFielDet;
    }

    public void setIdDescuentoFielDet(Long idDescuentoFielDet) {
        this.idDescuentoFielDet = idDescuentoFielDet;
    }

    public Dias getDia() {
        return dia;
    }

    public void setDia(Dias dia) {
        this.dia = dia;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return this.descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public DescuentoFielDetDTO toDescuentoFielDetDTO() {
        DiaDTO diaDTO = this.dia.toDiaDTO();
        DescuentoFielDetDTO dfDTO = toDescuentoFielDetDTO(this);
        dfDTO.setDia(diaDTO);
        return dfDTO;
    }

    public DescuentoFielDetDTO toBDDescuentoFielDet() {
        DescuentoFielDetDTO descDTO = toDescuentoFielDetDTO(this);
        if (this.dia != null) {
            descDTO.setDia(this.getDia().toDiaDTO());
        }

        if (this.descuentoFielCab != null) {
            descDTO.setDescuentoFielCab(this.getDescuentoFielCab()
                    .toSinDescDescuentoFielCabDTO());
        }
        return descDTO;
    }

    public static DescuentoFielDetDTO toDescuentoFielDetDTO(DescuentoFielDet dfd) {
        DescuentoFielDetDTO dfdDTO = new DescuentoFielDetDTO();
        BeanUtils.copyProperties(dfd, dfdDTO);
        return dfdDTO;
    }

    public static DescuentoFielDet fromDescuentoFielDetDTO(
            DescuentoFielDetDTO dfdDTO) {
        DescuentoFielDet dfd = new DescuentoFielDet();
        BeanUtils.copyProperties(dfdDTO, dfd);
        dfd.setDescuentoFielCab(DescuentoFielCab
                .fromDescuentoFielCabSinSeccionDTO(dfdDTO.getDescuentoFielCab()));
        dfd.setDia(Dias.fromDiaDTO(dfdDTO.getDia()));
        return dfd;
    }
}
