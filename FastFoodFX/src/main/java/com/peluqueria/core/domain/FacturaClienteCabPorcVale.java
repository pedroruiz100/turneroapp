package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabPorcValeDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_porc_vale database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_porc_vale", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPorcVale.findAll", query = "SELECT f FROM FacturaClienteCabPorcVale f")
public class FacturaClienteCabPorcVale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_porc_vale")
    private Long idFacturaClienteCabPorcVale;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to vale
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_vale")
    private Vales vale;

    @Column(name = "porc_vale")
    private BigDecimal porcVale;

    @Column(name = "nro_vale")
    private String nroVale;

    @Column(name = "ci")
    private String ci;

    @Column(name = "monto")
    private Integer monto;

    public FacturaClienteCabPorcVale() {
    }

    public Long getIdFacturaClienteCabPorcVale() {
        return idFacturaClienteCabPorcVale;
    }

    public void setIdFacturaClienteCabPorcVale(Long idFacturaClienteCabPorcVale) {
        this.idFacturaClienteCabPorcVale = idFacturaClienteCabPorcVale;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Vales getVale() {
        return vale;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public void setVale(Vales vale) {
        this.vale = vale;
    }

    public BigDecimal getPorcVale() {
        return porcVale;
    }

    public void setPorcVale(BigDecimal porcVale) {
        this.porcVale = porcVale;
    }

    public String getNroVale() {
        return nroVale;
    }

    public void setNroVale(String nroVale) {
        this.nroVale = nroVale;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public FacturaClienteCabPorcValeDTO toDescripcionFacturaClienteCabPorcValeDTO() {
        FacturaClienteCabPorcValeDTO facDTO = toFacturaClienteCabPorcValeDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setVale(null);
        return facDTO;
    }

    public FacturaClienteCabPorcValeDTO toFacturaClienteCabPorcValeDTO() {
        FacturaClienteCabPorcValeDTO facDTO = toFacturaClienteCabPorcValeDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.vale != null) {
            Vales tar = this.getVale();
            facDTO.setVale(tar.toValeDTO());
        }
        return facDTO;
    }

    public static FacturaClienteCabPorcValeDTO toFacturaClienteCabPorcValeDTO(
            FacturaClienteCabPorcVale facturaClienteCabPorcVale) {
        FacturaClienteCabPorcValeDTO facDTO = new FacturaClienteCabPorcValeDTO();
        BeanUtils.copyProperties(facturaClienteCabPorcVale, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabPorcVale fromFacturaClienteCabPorcValeDTO(
            FacturaClienteCabPorcValeDTO tarDTO) {
        FacturaClienteCabPorcVale fac = new FacturaClienteCabPorcVale();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public static FacturaClienteCabPorcVale fromFacturaClienteCabPorcValeAsociadoDTO(
            FacturaClienteCabPorcValeDTO tarDTO) {
        FacturaClienteCabPorcVale fac = fromFacturaClienteCabPorcValeDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setVale(Vales.fromValeDTO(tarDTO.getVale()));
        return fac;
    }

}
