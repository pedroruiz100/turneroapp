package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabPromoTempDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_promo_temp database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_promo_temp", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPromoTemp.findAll", query = "SELECT f FROM FacturaClienteCabPromoTemp f")
public class FacturaClienteCabPromoTemp implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_promo_temp")
    private Long idFacturaClienteCabPromoTemp;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to PromoTemp
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    @Column(name = "descripcion_temporada")
    private String descripcionTemporada;

    @Column(name = "monto")
    private Integer monto;

    public FacturaClienteCabPromoTemp() {
    }

    public Long getIdFacturaClienteCabPromoTemp() {
        return idFacturaClienteCabPromoTemp;
    }

    public void setIdFacturaClienteCabPromoTemp(
            Long idFacturaClienteCabPromoTemp) {
        this.idFacturaClienteCabPromoTemp = idFacturaClienteCabPromoTemp;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public PromoTemporada getPromoTemp() {
        return promoTemporada;
    }

    public void setPromoTemp(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public String getDescripcionTemporada() {
        return descripcionTemporada;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabPromoTempDTO toBDFacturaClienteCabPromoTempDTO() {
        FacturaClienteCabPromoTempDTO facDTO = toFacturaClienteCabPromoTempDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.promoTemporada != null) {
            PromoTemporada tar = this.getPromoTemp();
            facDTO.setPromoTemporada(tar.toPromoTemporadaDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabPromoTempDTO toDescripcionFacturaClienteCabPromoTempDTO() {
        FacturaClienteCabPromoTempDTO facDTO = toFacturaClienteCabPromoTempDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setPromoTemporada(null);
        return facDTO;
    }

    public FacturaClienteCabPromoTempDTO toFacturaClienteCabPromoTempDTO() {
        FacturaClienteCabPromoTempDTO facDTO = toFacturaClienteCabPromoTempDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.promoTemporada != null) {
            PromoTemporada tar = this.getPromoTemp();
            facDTO.setPromoTemporada(tar.toPromoTemporadaDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabPromoTempDTO toSinCabFacturaClienteCabPromoTempDTO() {
        FacturaClienteCabPromoTempDTO facDTO = toFacturaClienteCabPromoTempDTO(this);
        facDTO.setFacturaClienteCab(null);

        if (this.promoTemporada != null) {
            PromoTemporada tar = this.getPromoTemp();
            facDTO.setPromoTemporada(tar.toPromoTemporadaDTOEntitiesNull());
        }
        return facDTO;
    }

    public static FacturaClienteCabPromoTempDTO toFacturaClienteCabPromoTempDTO(
            FacturaClienteCabPromoTemp facturaClienteCabPromoTemp) {
        FacturaClienteCabPromoTempDTO facDTO = new FacturaClienteCabPromoTempDTO();
        BeanUtils.copyProperties(facturaClienteCabPromoTemp, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabPromoTemp fromFacturaClienteCabPromoTempDTO(
            FacturaClienteCabPromoTempDTO tarDTO) {
        FacturaClienteCabPromoTemp fac = new FacturaClienteCabPromoTemp();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public static FacturaClienteCabPromoTemp fromFacturaClienteCabPromoTempAsociadoDTO(
            FacturaClienteCabPromoTempDTO tarDTO) {
        FacturaClienteCabPromoTemp fac = fromFacturaClienteCabPromoTempDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setPromoTemp(PromoTemporada.fromPromoTemporadaDTO(tarDTO
                .getPromoTemporada()));
        return fac;
    }
}
