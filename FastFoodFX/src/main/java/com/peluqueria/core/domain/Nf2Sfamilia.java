package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf2SfamiliaDTO;

/**
 * The persistent class for the nf2_sfamilia database table.
 *
 */
@Entity
@Table(name = "nf2_sfamilia", schema = "stock")
@NamedQuery(name = "Nf2Sfamilia.findAll", query = "SELECT n FROM Nf2Sfamilia n")
public class Nf2Sfamilia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf2_sfamilia")
    private Long idNf2Sfamilia;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional one-to-many association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    // bi-directional one-to-many association to ArticuloNf2Sfamilia
    @OneToMany(mappedBy = "nf2Sfamilia")
    private List<ArticuloNf2Sfamilia> articuloNf2Sfamilias;

    // bi-directional one-to-many association to Nf3Sseccion
    @OneToMany(mappedBy = "nf2Sfamilia")
    private List<Nf3Sseccion> nf3Sseccions;

    // bi-directional one-to-many association to FuncionarioNf2
    @OneToMany(mappedBy = "nf2Sfamilia")
    private List<FuncionarioNf2> funcionarioNf2s;

    // bi-directional one-to-many association to DescuentoFielNf2
    @OneToMany(mappedBy = "nf2Sfamilia")
    private List<DescuentoFielNf2> descuentoFielNf2s;

    // bi-directional one-to-many association to PromoTemporadaNf2
    @OneToMany(mappedBy = "nf2Sfamilia")
    private List<PromoTemporadaNf2> promoTemporadaNf2s;

    public Nf2Sfamilia() {
    }

    public Long getIdNf2Sfamilia() {
        return this.idNf2Sfamilia;
    }

    public void setIdNf2Sfamilia(Long idNf2Sfamilia) {
        this.idNf2Sfamilia = idNf2Sfamilia;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf2Sfamilia> getArticuloNf2Sfamilias() {
        return this.articuloNf2Sfamilias;
    }

    public void setArticuloNf2Sfamilias(List<ArticuloNf2Sfamilia> articuloNf2Sfamilias) {
        this.articuloNf2Sfamilias = articuloNf2Sfamilias;
    }

    public ArticuloNf2Sfamilia addArticuloNf2Sfamilia(ArticuloNf2Sfamilia articuloNf2Sfamilia) {
        getArticuloNf2Sfamilias().add(articuloNf2Sfamilia);
        articuloNf2Sfamilia.setNf2Sfamilia(this);

        return articuloNf2Sfamilia;
    }

    public ArticuloNf2Sfamilia removeArticuloNf2Sfamilia(ArticuloNf2Sfamilia articuloNf2Sfamilia) {
        getArticuloNf2Sfamilias().remove(articuloNf2Sfamilia);
        articuloNf2Sfamilia.setNf2Sfamilia(null);

        return articuloNf2Sfamilia;
    }

    public Nf1Tipo getNf1Tipo() {
        return this.nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    public List<Nf3Sseccion> getNf3Sseccions() {
        return this.nf3Sseccions;
    }

    public void setNf3Sseccions(List<Nf3Sseccion> nf3Sseccions) {
        this.nf3Sseccions = nf3Sseccions;
    }

    public Nf3Sseccion addNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        getNf3Sseccions().add(nf3Sseccion);
        nf3Sseccion.setNf2Sfamilia(this);

        return nf3Sseccion;
    }

    public Nf3Sseccion removeNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        getNf3Sseccions().remove(nf3Sseccion);
        nf3Sseccion.setNf2Sfamilia(null);

        return nf3Sseccion;
    }

    public List<FuncionarioNf2> getFuncionarioNf2s() {
        return this.funcionarioNf2s;
    }

    public void setFuncionarioNf2s(List<FuncionarioNf2> funcionarioNf2s) {
        this.funcionarioNf2s = funcionarioNf2s;
    }

    public FuncionarioNf2 addFuncionarioNf2(FuncionarioNf2 funcionarioNf2) {
        getFuncionarioNf2s().add(funcionarioNf2);
        funcionarioNf2.setNf2Sfamilia(this);

        return funcionarioNf2;
    }

    public FuncionarioNf2 removeFuncionarioNf2(FuncionarioNf2 funcionarioNf2) {
        getFuncionarioNf2s().remove(funcionarioNf2);
        funcionarioNf2.setNf2Sfamilia(null);

        return funcionarioNf2;
    }

    public List<PromoTemporadaNf2> getPromoTemporadaNf2s() {
        return this.promoTemporadaNf2s;
    }

    public void setPromoTemporadaNf2s(List<PromoTemporadaNf2> promoTemporadaNf2s) {
        this.promoTemporadaNf2s = promoTemporadaNf2s;
    }

    public PromoTemporadaNf2 addPromoTemporadaNf2(PromoTemporadaNf2 promoTemporadaNf2) {
        getPromoTemporadaNf2s().add(promoTemporadaNf2);
        promoTemporadaNf2.setNf2Sfamilia(this);

        return promoTemporadaNf2;
    }

    public PromoTemporadaNf2 removePromoTemporadaNf2(PromoTemporadaNf2 promoTemporadaNf2) {
        getPromoTemporadaNf2s().remove(promoTemporadaNf2);
        promoTemporadaNf2.setNf2Sfamilia(null);

        return promoTemporadaNf2;
    }

    public List<DescuentoFielNf2> getDescuentoFielNf2s() {
        return this.descuentoFielNf2s;
    }

    public void setDescuentoFielNf2s(List<DescuentoFielNf2> descuentoFielNf2s) {
        this.descuentoFielNf2s = descuentoFielNf2s;
    }

    public DescuentoFielNf2 addDescuentoFielNf2(DescuentoFielNf2 descuentoFielNf2) {
        getDescuentoFielNf2s().add(descuentoFielNf2);
        descuentoFielNf2.setNf2Sfamilia(this);

        return descuentoFielNf2;
    }

    public DescuentoFielNf2 removeDescuentoFielNf2(DescuentoFielNf2 descuentoFielNf2) {
        getDescuentoFielNf2s().remove(descuentoFielNf2);
        descuentoFielNf2.setNf2Sfamilia(null);

        return descuentoFielNf2;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf2Sfamilia fromNf2SfamiliaDTO(Nf2SfamiliaDTO nf2SfamiliaDTO) {
        Nf2Sfamilia nf2Sfamilia = new Nf2Sfamilia();
        BeanUtils.copyProperties(nf2SfamiliaDTO, nf2Sfamilia);
        return nf2Sfamilia;
    }

    // <-- SERVER
    public static Nf2SfamiliaDTO toNf2SfamiliaDTO(Nf2Sfamilia nf2Sfamilia) {
        Nf2SfamiliaDTO nf2SfamiliaDTO = new Nf2SfamiliaDTO();
        BeanUtils.copyProperties(nf2Sfamilia, nf2SfamiliaDTO);
        return nf2SfamiliaDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf2SfamiliaDTO toNf2SfamiliaDTOEntitiesNull(Nf2Sfamilia nf2Sfamilia) {
        Nf2SfamiliaDTO nf2SfamiliaDTO = toNf2SfamiliaDTO(nf2Sfamilia);
        nf2SfamiliaDTO.setNf1Tipo(null);// Entity
        nf2SfamiliaDTO.setNf3Sseccions(null);// Entities
        nf2SfamiliaDTO.setArticuloNf2Sfamilias(null);// Entities
        nf2SfamiliaDTO.setDescuentoFielNf2s(null);// Entities
        nf2SfamiliaDTO.setFuncionarioNf2s(null);// Entities
        nf2SfamiliaDTO.setPromoTemporadaNf2s(null); // Entities
        return nf2SfamiliaDTO;
    }

    // <-- SERVER
    public Nf2SfamiliaDTO toNf2SfamiliaDTOEntitiesNull() {
        Nf2SfamiliaDTO nf2SfamiliaDTO = toNf2SfamiliaDTO(this);
        nf2SfamiliaDTO.setNf1Tipo(null);// Entity
        nf2SfamiliaDTO.setNf3Sseccions(null);// Entities
        nf2SfamiliaDTO.setArticuloNf2Sfamilias(null);// Entities
        nf2SfamiliaDTO.setDescuentoFielNf2s(null);// Entities
        nf2SfamiliaDTO.setFuncionarioNf2s(null);// Entities
        nf2SfamiliaDTO.setPromoTemporadaNf2s(null); // Entities
        return nf2SfamiliaDTO;
    }

    // <-- SERVER
    public Nf2SfamiliaDTO toNf2SfamiliaDTOEntitiesNullNf1() {
        Nf2SfamiliaDTO nf2SfamiliaDTO = toNf2SfamiliaDTO(this);
        nf2SfamiliaDTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());// Entity
        nf2SfamiliaDTO.setNf3Sseccions(null);// Entities
        nf2SfamiliaDTO.setArticuloNf2Sfamilias(null);// Entities
        nf2SfamiliaDTO.setDescuentoFielNf2s(null);// Entities
        nf2SfamiliaDTO.setFuncionarioNf2s(null);// Entities
        nf2SfamiliaDTO.setPromoTemporadaNf2s(null); // Entities
        return nf2SfamiliaDTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf2Sfamilia fromNf2SfamiliaDTOEntitiesFull(Nf2SfamiliaDTO nf2SfamiliaDTO) {
        Nf2Sfamilia nf2Sfamilia = fromNf2SfamiliaDTO(nf2SfamiliaDTO);
        if (nf2SfamiliaDTO.getNf1Tipo() != null) {// Entity
            nf2Sfamilia.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(nf2SfamiliaDTO.getNf1Tipo()));
        }
        return nf2Sfamilia;
    }
    // ######################## FULL, ENTRADA ########################

}
