package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf7DTO;

/**
 * The persistent class for the funcionario_nf7 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf7", schema = "descuento")
@NamedQuery(name = "FuncionarioNf7.findAll", query = "SELECT f FROM FuncionarioNf7 f")
public class FuncionarioNf7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf7")
    private Long idFuncionarioNf7;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf7Secnom7
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf7_secnom7")
    private Nf7Secnom7 nf7Secnom7;

    public FuncionarioNf7() {
    }

    public Long getIdFuncionarioNf7() {
        return this.idFuncionarioNf7;
    }

    public void setIdFuncionarioNf7(Long idFuncionarioNf7) {
        this.idFuncionarioNf7 = idFuncionarioNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf7Secnom7 getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf7 fromFuncionarioNf7DTO(FuncionarioNf7DTO funcionarioNf7DTO) {
        FuncionarioNf7 funcionarioNf7 = new FuncionarioNf7();
        BeanUtils.copyProperties(funcionarioNf7DTO, funcionarioNf7);
        return funcionarioNf7;
    }

    // <-- SERVER
    public static FuncionarioNf7DTO toFuncionarioNf7DTO(FuncionarioNf7 funcionarioNf7) {
        FuncionarioNf7DTO funcionarioNf7DTO = new FuncionarioNf7DTO();
        BeanUtils.copyProperties(funcionarioNf7, funcionarioNf7DTO);
        return funcionarioNf7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf7 fromFuncionarioNf7DTOEntitiesFull(FuncionarioNf7DTO funcionarioNf7DTO) {
        FuncionarioNf7 funcionarioNf7 = fromFuncionarioNf7DTO(funcionarioNf7DTO);
        if (funcionarioNf7DTO.getNf7Secnom7() != null) {
            funcionarioNf7.setNf7Secnom7(Nf7Secnom7.fromNf7Secnom7DTO(funcionarioNf7DTO.getNf7Secnom7()));
        }
        return funcionarioNf7;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf7DTO toFuncionarioNf7DTONf6() {
        FuncionarioNf7DTO funcionarioNf7DTO = toFuncionarioNf7DTO(this);
        if (this.getNf7Secnom7() != null) {
            funcionarioNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        }
        return funcionarioNf7DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
