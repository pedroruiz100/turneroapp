package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaFielDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_tarjeta_fiel database table.
 *
 */
@Entity
@Table(name = "desc_tarjeta_fiel", schema = "factura_cliente")
@NamedQuery(name = "DescTarjetaFiel.findAll", query = "SELECT d FROM DescTarjetaFiel d")
public class DescTarjetaFiel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_tarjeta_fiel")
    private Long idDescTarjetaFiel;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteDet
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_tarj_fiel")
    private FacturaClienteCabTarjFiel facturaClienteCabTarjFiel;

    public DescTarjetaFiel() {
    }

    public Long getIdDescTarjetaFiel() {
        return this.idDescTarjetaFiel;
    }

    public void setIdDescTarjetaFiel(Long idDescTarjetaFiel) {
        this.idDescTarjetaFiel = idDescTarjetaFiel;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjFiel getFacturaClienteCabTarjFiel() {
        return facturaClienteCabTarjFiel;
    }

    public void setFacturaClienteCabTarjFiel(
            FacturaClienteCabTarjFiel facturaClienteCabTarjFiel) {
        this.facturaClienteCabTarjFiel = facturaClienteCabTarjFiel;
    }

    public DescTarjetaFielDTO toDescTarjetaFielDTO() {
        DescTarjetaFielDTO descDTO = toDescTarjetaFielDTO(this);
        if (this.facturaClienteCabTarjFiel != null) {
            FacturaClienteCabTarjFiel fac = this.getFacturaClienteCabTarjFiel();
            descDTO.setFacturaClienteCabTarjFiel(fac
                    .toDescripcionFacturaClienteCabTarjFielDTO());
        }
        return descDTO;
    }

    public static DescTarjetaFielDTO toDescTarjetaFielDTO(
            DescTarjetaFiel descTarjetaFiel) {
        DescTarjetaFielDTO descDTO = new DescTarjetaFielDTO();
        BeanUtils.copyProperties(descTarjetaFiel, descDTO);
        return descDTO;
    }

    public static DescTarjetaFiel fromDescTarjetaFielDTO(
            DescTarjetaFielDTO descDTO) {
        DescTarjetaFiel desc = new DescTarjetaFiel();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public DescTarjetaFielDTO toBDDescTarjetaFielDTO() {
        DescTarjetaFielDTO descDTO = toDescTarjetaFielDTO(this);
        descDTO.setFacturaClienteCabTarjFiel(null);
        return descDTO;
    }

    public static DescTarjetaFiel fromDescTarjetaFielAsociadoDTO(
            DescTarjetaFielDTO descDTO) {
        DescTarjetaFiel desc = fromDescTarjetaFielDTO(descDTO);
        desc.setFacturaClienteCabTarjFiel(FacturaClienteCabTarjFiel
                .fromFacturaClienteCabTarjFielDTO(descDTO
                        .getFacturaClienteCabTarjFiel()));
        return desc;
    }

}
