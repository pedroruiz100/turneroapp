package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaCompraDetDTO;
import com.peluqueria.dto.PedidoCompraDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "pedido_compra", schema = "factura_cliente")
@NamedQuery(name = "PedidoCompra.findAll", query = "SELECT f FROM PedidoCompra f")
public class PedidoCompra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido_compra")
    private Long idPedidoCompra;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_cab")
    private PedidoCab pedidoCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "precio")
    private Long precio;

    @Column(name = "cant_cc")
    private Long cantCc;

    @Column(name = "cant_sc")
    private Long cantSc;

    @Column(name = "cant_sl")
    private Long cantSl;

    @Column(name = "descuento")
    private String descuento;

    @Column(name = "cantidad")
    private String cantidad;

    @Column(name = "tipo")
    private String tipo;

    @Column(name = "exenta")
    private String exenta;

    @Column(name = "grav5")
    private String grav5;

    @Column(name = "grav10")
    private String grav10;

    public PedidoCompra() {
    }

    public Long getIdPedidoCompra() {
        return idPedidoCompra;
    }

    public void setIdPedidoCompra(Long idPedidoCompra) {
        this.idPedidoCompra = idPedidoCompra;
    }

    public PedidoCab getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCab pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getDescuento() {
        return descuento;
    }

    public void setDescuento(String descuento) {
        this.descuento = descuento;
    }

    public String getCantidad() {
        return cantidad;
    }

    public Long getCantCc() {
        return cantCc;
    }

    public void setCantCc(Long cantCc) {
        this.cantCc = cantCc;
    }

    public Long getCantSc() {
        return cantSc;
    }

    public void setCantSc(Long cantSc) {
        this.cantSc = cantSc;
    }

    public Long getCantSl() {
        return cantSl;
    }

    public void setCantSl(Long cantSl) {
        this.cantSl = cantSl;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getExenta() {
        return exenta;
    }

    public void setExenta(String exenta) {
        this.exenta = exenta;
    }

    public String getGrav5() {
        return grav5;
    }

    public void setGrav5(String grav5) {
        this.grav5 = grav5;
    }

    public String getGrav10() {
        return grav10;
    }

    public void setGrav10(String grav10) {
        this.grav10 = grav10;
    }

    public PedidoCompraDTO toPedidoCompraDTO() {
        PedidoCompraDTO facDTO = toPedidoCompraDTO(this);
        if (this.pedidoCab != null) {
            facDTO.setPedidoCab(this.getPedidoCab()
                    .toPedidoCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return facDTO;
    }

    public PedidoCompraDTO toDescriPedidoCompraDTO() {
        PedidoCompraDTO facDTO = toPedidoCompraDTO(this);
        facDTO.setPedidoCab(null);
        facDTO.setArticulo(null);
        return facDTO;
    }

    public static PedidoCompraDTO toPedidoCompraDTO(
            PedidoCompra facturaClienteDet) {
        PedidoCompraDTO facDTO = new PedidoCompraDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static PedidoCompra fromPedidoCompraDTO(
            PedidoCompraDTO facDTO) {
        PedidoCompra fac = new PedidoCompra();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static PedidoCompra fromPedidoCompraAsociado(
            PedidoCompraDTO facDTO) {
        PedidoCompra fac = fromPedidoCompraDTO(facDTO);
        fac.setPedidoCab(PedidoCab.fromPedidoCabDTO(facDTO.getPedidoCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
