package com.peluqueria.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the nota_credito_det database table.
 *
 */
@Entity
@Table(name = "nota_credito_det", schema = "factura_cliente")
@NamedQuery(name = "NotaCreditoDet.findAll", query = "SELECT n FROM NotaCreditoDet n")
public class NotaCreditoDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_credito_det")
    private Long idNotaCreditoDet;

    private Integer cantidad;

    private BigDecimal contenido;

    @Column(name = "costo_unitario")
    private Integer costoUnitario;

    @Column(name = "descripcion_articulo")
    private String descripcionArticulo;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "id_unidad")
    private Integer idUnidad;

    @Column(name = "porc_desc")
    private Integer porcDesc;

    @Column(name = "porc_iva")
    private Integer porcIva;

    @Column(name = "precio_unitario")
    private Integer precioUnitario;

    @Column(name = "total_nota")
    private Integer totalNota;

    // bi-directional many-to-one association to NotaCreditoCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nota_credito_cab")
    private NotaCreditoCab notaCreditoCab;

    public NotaCreditoDet() {
    }

    public Long getIdNotaCreditoDet() {
        return this.idNotaCreditoDet;
    }

    public void setIdNotaCreditoDet(Long idNotaCreditoDet) {
        this.idNotaCreditoDet = idNotaCreditoDet;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getContenido() {
        return this.contenido;
    }

    public void setContenido(BigDecimal contenido) {
        this.contenido = contenido;
    }

    public Integer getCostoUnitario() {
        return this.costoUnitario;
    }

    public void setCostoUnitario(Integer costoUnitario) {
        this.costoUnitario = costoUnitario;
    }

    public String getDescripcionArticulo() {
        return this.descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public Integer getIdUnidad() {
        return this.idUnidad;
    }

    public void setIdUnidad(Integer idUnidad) {
        this.idUnidad = idUnidad;
    }

    public Integer getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(Integer porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Integer getPorcIva() {
        return this.porcIva;
    }

    public void setPorcIva(Integer porcIva) {
        this.porcIva = porcIva;
    }

    public Integer getPrecioUnitario() {
        return this.precioUnitario;
    }

    public void setPrecioUnitario(Integer precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getTotalNota() {
        return this.totalNota;
    }

    public void setTotalNota(Integer totalNota) {
        this.totalNota = totalNota;
    }

    public NotaCreditoCab getNotaCreditoCab() {
        return this.notaCreditoCab;
    }

    public void setNotaCreditoCab(NotaCreditoCab notaCreditoCab) {
        this.notaCreditoCab = notaCreditoCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

}
