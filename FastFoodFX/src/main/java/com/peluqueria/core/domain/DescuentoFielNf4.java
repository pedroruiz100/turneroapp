package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf4DTO;

/**
 * The persistent class for the descuento_fiel_nf4 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf4", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf4.findAll", query = "SELECT d FROM DescuentoFielNf4 d")
public class DescuentoFielNf4 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf4")
    private Long idDescuentoFielNf4;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    public DescuentoFielNf4() {
    }

    public Long getIdDescuentoFielNf4() {
        return this.idDescuentoFielNf4;
    }

    public void setIdDescuentoFielNf4(Long idDescuentoFielNf4) {
        this.idDescuentoFielNf4 = idDescuentoFielNf4;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf4 fromDescuentoFielNf4DTO(DescuentoFielNf4DTO descuentoFielNf4DTO) {
        DescuentoFielNf4 descuentoFielNf4 = new DescuentoFielNf4();
        BeanUtils.copyProperties(descuentoFielNf4DTO, descuentoFielNf4);
        return descuentoFielNf4;
    }

    // <-- SERVER
    public static DescuentoFielNf4DTO toDescuentoFielNf4DTO(DescuentoFielNf4 descuentoFielNf4) {
        DescuentoFielNf4DTO descuentoFielNf4DTO = new DescuentoFielNf4DTO();
        BeanUtils.copyProperties(descuentoFielNf4, descuentoFielNf4DTO);
        return descuentoFielNf4DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf4 fromDescuentoFielNf4DTOEntitiesFull(DescuentoFielNf4DTO descuentoFielNf4DTO) {
        DescuentoFielNf4 descuentoFielNf4 = fromDescuentoFielNf4DTO(descuentoFielNf4DTO);
        if (descuentoFielNf4DTO.getNf4Seccion1() != null) {
            descuentoFielNf4.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(descuentoFielNf4DTO.getNf4Seccion1()));
        }
        if (descuentoFielNf4DTO.getDescuentoFielCab() != null) {
            descuentoFielNf4.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf4DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf4;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf4DTO toDescuentoFielNf4DTOEntityFull() {
        DescuentoFielNf4DTO descuentoFielNf4DTO = toDescuentoFielNf4DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf4DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf4DTO.setDescuentoFielCab(null);
        }
        if (this.getNf4Seccion1() != null) {
            descuentoFielNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNull());
        } else {
            descuentoFielNf4DTO.setNf4Seccion1(null);
        }
        return descuentoFielNf4DTO;
    }

    // <-- SERVER
    public DescuentoFielNf4DTO toDescuentoFielNf4DTOEntityNull() {
        DescuentoFielNf4DTO descuentoFielNf4DTO = toDescuentoFielNf4DTO(this);
        descuentoFielNf4DTO.setDescuentoFielCab(null);
        descuentoFielNf4DTO.setNf4Seccion1(null);
        return descuentoFielNf4DTO;
    }

    // <-- SERVER
    public DescuentoFielNf4DTO toDescuentoFielNf4DTONf3() {
        DescuentoFielNf4DTO descuentoFielNf4DTO = toDescuentoFielNf4DTO(this);
        descuentoFielNf4DTO.setDescuentoFielCab(null);
        if (this.getNf4Seccion1() != null) {
            descuentoFielNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            descuentoFielNf4DTO.setNf4Seccion1(null);
        }
        return descuentoFielNf4DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
