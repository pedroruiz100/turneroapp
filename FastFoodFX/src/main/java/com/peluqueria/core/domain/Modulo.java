package com.peluqueria.core.domain;

import com.peluqueria.dto.FuncionDTO;
import com.peluqueria.dto.ModuloDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the modulo database table.
 *
 */
@Entity
@Table(name = "modulo", schema = "seguridad")
@NamedQuery(name = "Modulo.findAll", query = "SELECT m FROM Modulo m")
public class Modulo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_modulo")
    private Long idModulo;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Funcion
    @OneToMany(mappedBy = "modulo")
    private List<Funcion> funcions;

    public Modulo() {
    }

    public Long getIdModulo() {
        return this.idModulo;
    }

    public void setIdModulo(Long idModulo) {
        this.idModulo = idModulo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Funcion> getFuncions() {
        return this.funcions;
    }

    public void setFuncions(List<Funcion> funcions) {
        this.funcions = funcions;
    }

    public ModuloDTO toModuloDTO() {
        ModuloDTO moDTO = toModuloDTO(this);
        if (!this.funcions.isEmpty()) {
            List<Funcion> fun = new ArrayList<Funcion>();
            List<FuncionDTO> funDTO = new ArrayList<FuncionDTO>();
            for (Funcion f : fun) {
                funDTO.add(f.toBdFuncionDTO());
            }
            moDTO.setFuncions(funDTO);
        }
        return moDTO;
    }

    public ModuloDTO toModuloDTOSeguridad() {
        ModuloDTO moduloDTO = toModuloDTO(this);
        if (!this.funcions.isEmpty()) {
            List<FuncionDTO> listFuncionDTO = new ArrayList<FuncionDTO>();
            for (Funcion funcion : this.funcions) {
                FuncionDTO fDTO = Funcion.toFuncionDTO(funcion);
                fDTO.setModulo(null);
                fDTO.setRolFuncions(null);
                listFuncionDTO.add(fDTO);
            }
            moduloDTO.setFuncions(listFuncionDTO);
        } else {
            moduloDTO.setFuncions(null);
        }
        return moduloDTO;
    }

    public ModuloDTO toBDModuloDTO() {
        ModuloDTO moDTO = toModuloDTO(this);
        moDTO.setFuncions(null);
        return moDTO;
    }

    public static ModuloDTO toModuloDTO(Modulo modulo) {
        ModuloDTO moDTO = new ModuloDTO();
        BeanUtils.copyProperties(modulo, moDTO);
        return moDTO;
    }

    public static Modulo fromModuloDTO(ModuloDTO moDTO) {
        Modulo m = new Modulo();
        BeanUtils.copyProperties(moDTO, m);
        return m;
    }

}
