package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf1DTO;

/**
 * The persistent class for the descuento_fiel_nf1 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf1", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf1.findAll", query = "SELECT d FROM DescuentoFielNf1 d")
public class DescuentoFielNf1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf1")
    private Long idDescuentoFielNf1;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    public DescuentoFielNf1() {
    }

    public Long getIdDescuentoFielNf1() {
        return this.idDescuentoFielNf1;
    }

    public void setIdDescuentoFielNf1(Long idDescuentoFielNf1) {
        this.idDescuentoFielNf1 = idDescuentoFielNf1;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf1Tipo getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf1 fromDescuentoFielNf1DTO(DescuentoFielNf1DTO descuentoFielNf1DTO) {
        DescuentoFielNf1 descuentoFielNf1 = new DescuentoFielNf1();
        BeanUtils.copyProperties(descuentoFielNf1DTO, descuentoFielNf1);
        return descuentoFielNf1;
    }

    // <-- SERVER
    public static DescuentoFielNf1DTO toDescuentoFielNf1DTO(DescuentoFielNf1 descuentoFielNf1) {
        DescuentoFielNf1DTO descuentoFielNf1DTO = new DescuentoFielNf1DTO();
        BeanUtils.copyProperties(descuentoFielNf1, descuentoFielNf1DTO);
        return descuentoFielNf1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf1 fromDescuentoFielNf1DTOEntitiesFull(DescuentoFielNf1DTO descuentoFielNf1DTO) {
        DescuentoFielNf1 descuentoFielNf1 = fromDescuentoFielNf1DTO(descuentoFielNf1DTO);
        if (descuentoFielNf1DTO.getNf1Tipo() != null) {
            descuentoFielNf1.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(descuentoFielNf1DTO.getNf1Tipo()));
        }
        if (descuentoFielNf1DTO.getDescuentoFielCab() != null) {
            descuentoFielNf1.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf1DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf1;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf1DTO toDescuentoFielNf1DTOEntityFull() {
        DescuentoFielNf1DTO descuentoFielNf1DTO = toDescuentoFielNf1DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf1DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf1DTO.setDescuentoFielCab(null);
        }
        if (this.getNf1Tipo() != null) {
            descuentoFielNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            descuentoFielNf1DTO.setNf1Tipo(null);
        }
        return descuentoFielNf1DTO;
    }

    // <-- SERVER
    public DescuentoFielNf1DTO toDescuentoFielNf1DTOEntityNull() {
        DescuentoFielNf1DTO descuentoFielNf1DTO = toDescuentoFielNf1DTO(this);
        descuentoFielNf1DTO.setDescuentoFielCab(null);
        descuentoFielNf1DTO.setNf1Tipo(null);
        return descuentoFielNf1DTO;
    }

    // <-- SERVER
    public DescuentoFielNf1DTO toDescuentoFielNf1DTO() {
        DescuentoFielNf1DTO descuentoFielNf1DTO = toDescuentoFielNf1DTO(this);
        descuentoFielNf1DTO.setDescuentoFielCab(null);
        if (this.getNf1Tipo() != null) {
            descuentoFielNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            descuentoFielNf1DTO.setNf1Tipo(null);
        }
        return descuentoFielNf1DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
