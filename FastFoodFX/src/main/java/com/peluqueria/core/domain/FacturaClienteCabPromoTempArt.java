package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabPromoTempArtDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_promo_temp_art database
 * table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_promo_temp_art", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPromoTempArt.findAll", query = "SELECT f FROM FacturaClienteCabPromoTempArt f")
public class FacturaClienteCabPromoTempArt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_promo_temp_art")
    private Long idFacturaClienteCabPromoTempArt;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to PromoTemp
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada_art")
    private PromoTemporadaArt promoTemporadaArt;

    @Column(name = "descripcion_temporada")
    private String descripcionTemporada;

    @Column(name = "monto")
    private Integer monto;

    public FacturaClienteCabPromoTempArt() {
    }

    public Long getIdFacturaClienteCabPromoTempArt() {
        return idFacturaClienteCabPromoTempArt;
    }

    public void setIdFacturaClienteCabPromoTempArt(
            Long idFacturaClienteCabPromoTempArt) {
        this.idFacturaClienteCabPromoTempArt = idFacturaClienteCabPromoTempArt;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public PromoTemporadaArt getPromoTempArt() {
        return promoTemporadaArt;
    }

    public void setPromoTempArt(PromoTemporadaArt promoTemporadaArt) {
        this.promoTemporadaArt = promoTemporadaArt;
    }

    public String getDescripcionTemporada() {
        return descripcionTemporada;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public FacturaClienteCabPromoTempArtDTO toBDFacturaClienteCabPromoTempArtDTO() {
        FacturaClienteCabPromoTempArtDTO facDTO = toFacturaClienteCabPromoTempArtDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.promoTemporadaArt != null) {
            PromoTemporadaArt tar = this.getPromoTempArt();
            facDTO.setPromoTemporadaArt(tar.toPromoTemporadaArtDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabPromoTempArtDTO toDescripcionFacturaClienteCabPromoTempArtDTO() {
        FacturaClienteCabPromoTempArtDTO facDTO = toFacturaClienteCabPromoTempArtDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setPromoTemporadaArt(null);
        return facDTO;
    }

    public FacturaClienteCabPromoTempArtDTO toFacturaClienteCabPromoTempArtDTO() {
        FacturaClienteCabPromoTempArtDTO facDTO = toFacturaClienteCabPromoTempArtDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.promoTemporadaArt != null) {
            PromoTemporadaArt tar = this.getPromoTempArt();
            facDTO.setPromoTemporadaArt(tar.toPromoTemporadaArtDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabPromoTempArtDTO toSinCabBDFacturaClienteCabPromoTempArtDTO() {
        FacturaClienteCabPromoTempArtDTO facDTO = toFacturaClienteCabPromoTempArtDTO(this);
        facDTO.setFacturaClienteCab(null);

        if (this.promoTemporadaArt != null) {
            PromoTemporadaArt tar = this.getPromoTempArt();
            facDTO.setPromoTemporadaArt(tar.toPromoTemporadaArtSinArtDTO());
        }
        return facDTO;
    }

    public static FacturaClienteCabPromoTempArtDTO toFacturaClienteCabPromoTempArtDTO(
            FacturaClienteCabPromoTempArt facturaClienteCabPromoTempArt) {
        FacturaClienteCabPromoTempArtDTO facDTO = new FacturaClienteCabPromoTempArtDTO();
        BeanUtils.copyProperties(facturaClienteCabPromoTempArt, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabPromoTempArt fromFacturaClienteCabPromoTempArtDTO(
            FacturaClienteCabPromoTempArtDTO tarDTO) {
        FacturaClienteCabPromoTempArt fac = new FacturaClienteCabPromoTempArt();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public static FacturaClienteCabPromoTempArt fromFacturaClienteCabPromoTempArtAsociadoDTO(
            FacturaClienteCabPromoTempArtDTO tarDTO) {
        FacturaClienteCabPromoTempArt fac = fromFacturaClienteCabPromoTempArtDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setPromoTempArt(PromoTemporadaArt.fromPromoTemporadaArtDTO(tarDTO
                .getPromoTemporadaArt()));
        return fac;
    }
}
