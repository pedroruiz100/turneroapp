package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf6Secnom6DTO;

/**
 * The persistent class for the articulo_nf6_secnom6 database table.
 *
 */
@Entity
@Table(name = "articulo_nf6_secnom6", schema = "stock")
@NamedQuery(name = "ArticuloNf6Secnom6.findAll", query = "SELECT a FROM ArticuloNf6Secnom6 a")
public class ArticuloNf6Secnom6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf6_secnom6_articulo")
    private Long idNf6Secnom6Articulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    public ArticuloNf6Secnom6() {
    }

    public Long getIdNf6Secnom6Articulo() {
        return this.idNf6Secnom6Articulo;
    }

    public void setIdNf6Secnom6Articulo(Long idNf6Secnom6Articulo) {
        this.idNf6Secnom6Articulo = idNf6Secnom6Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return this.nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf6Secnom6 fromArticuloNf6Secnom6DTO(ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO) {
        ArticuloNf6Secnom6 articuloNf6Secnom6 = new ArticuloNf6Secnom6();
        BeanUtils.copyProperties(articuloNf6Secnom6DTO, articuloNf6Secnom6);
        return articuloNf6Secnom6;
    }

    // <-- SERVER
    public static ArticuloNf6Secnom6DTO toArticuloNf6Secnom6DTO(ArticuloNf6Secnom6 articuloNf6Secnom6) {
        ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO = new ArticuloNf6Secnom6DTO();
        BeanUtils.copyProperties(articuloNf6Secnom6, articuloNf6Secnom6DTO);
        return articuloNf6Secnom6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf6Secnom6DTO toArticuloNf6Secnom6DTOEntitiesNull(ArticuloNf6Secnom6 articuloNf6Secnom6) {
        ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO = toArticuloNf6Secnom6DTO(articuloNf6Secnom6);
        articuloNf6Secnom6DTO.setArticulo(null);
        articuloNf6Secnom6DTO.setNf6Secnom6(null);
        return articuloNf6Secnom6DTO;
    }

    // <-- SERVER
    public ArticuloNf6Secnom6DTO toArticuloNf6Secnom6DTOEntitiesNull() {
        ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO = toArticuloNf6Secnom6DTO(this);
        articuloNf6Secnom6DTO.setArticulo(null);
        articuloNf6Secnom6DTO.setNf6Secnom6(null);
        return articuloNf6Secnom6DTO;
    }

    // <-- SERVER
    public ArticuloNf6Secnom6DTO toArticuloNf6Secnom6DTOEntitiesNf() {
        ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO = toArticuloNf6Secnom6DTO(this);
        articuloNf6Secnom6DTO.setArticulo(null);
        if (this.getNf6Secnom6() != null) {
            articuloNf6Secnom6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            articuloNf6Secnom6DTO.setNf6Secnom6(null);
        }
        return articuloNf6Secnom6DTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf6Secnom6 fromArticuloNf6Secnom6DTOEntitiesFull(
            ArticuloNf6Secnom6DTO articuloNf6Secnom6DTO) {
        ArticuloNf6Secnom6 articuloNf6Secnom6 = fromArticuloNf6Secnom6DTO(articuloNf6Secnom6DTO);
        if (articuloNf6Secnom6DTO.getArticulo() != null) {
            articuloNf6Secnom6.setArticulo(Articulo.fromArticuloDTO(articuloNf6Secnom6DTO.getArticulo()));
        }
        if (articuloNf6Secnom6DTO.getNf6Secnom6() != null) {
            articuloNf6Secnom6.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(articuloNf6Secnom6DTO.getNf6Secnom6()));
        }
        return articuloNf6Secnom6;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
