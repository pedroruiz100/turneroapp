package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaConvenioDetDTO;
import com.peluqueria.dto.DiaDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_tarjeta_convenio_det database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_convenio_det", schema = "cuenta")
@NamedQuery(name = "DescuentoTarjetaConvenioDet.findAll", query = "SELECT d FROM DescuentoTarjetaConvenioDet d")
public class DescuentoTarjetaConvenioDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_convenio_det")
    private Long idDescuentoTarjetaConvenioDet;

    // bi-directional many-to-one association to Dia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dia")
    private Dias dia;

    // bi-directional many-to-one association to DescuentoTarjetaConvenioCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_convenio_cab")
    private DescuentoTarjetaConvenioCab descuentoTarjetaConvenioCab;

    public DescuentoTarjetaConvenioDet() {
    }

    public Long getIdDescuentoTarjetaConvenioDet() {
        return this.idDescuentoTarjetaConvenioDet;
    }

    public void setIdDescuentoTarjetaConvenioDet(
            Long idDescuentoTarjetaConvenioDet) {
        this.idDescuentoTarjetaConvenioDet = idDescuentoTarjetaConvenioDet;
    }

    public DescuentoTarjetaConvenioCab getDescuentoTarjetaConvenioCab() {
        return this.descuentoTarjetaConvenioCab;
    }

    public void setDescuentoTarjetaConvenioCab(
            DescuentoTarjetaConvenioCab descuentoTarjetaConvenioCab) {
        this.descuentoTarjetaConvenioCab = descuentoTarjetaConvenioCab;
    }

    public Dias getDia() {
        return dia;
    }

    public void setDia(Dias dia) {
        this.dia = dia;
    }

    // Para registrar dia dentro del detalle consultado desde la cabecera
    public DescuentoTarjetaConvenioDetDTO toDescuentoTarjetaConvenioDetDTO() {
        DiaDTO diaDTO = this.dia.toDiaDTO();
        DescuentoTarjetaConvenioDetDTO dfDTO = toDescuentoTarjetaConvenioDetDTO(this);
        if (this.descuentoTarjetaConvenioCab != null) {
            DescuentoTarjetaConvenioCab desCab = this
                    .getDescuentoTarjetaConvenioCab();
            dfDTO.setDescuentoTarjetaConvenioCab(desCab
                    .toDescuentoParaDetalleDTO());
        }
        dfDTO.setDia(diaDTO);
        return dfDTO;
    }

    public DescuentoTarjetaConvenioDetDTO toDescripcionDescuentoTarjetaConvenioDetDTO() {
        DiaDTO diaDTO = this.dia.toDiaDTO();
        DescuentoTarjetaConvenioDetDTO dfDTO = toDescuentoTarjetaConvenioDetDTO(this);
        dfDTO.setDescuentoTarjetaConvenioCab(null);
        dfDTO.setDia(diaDTO);
        return dfDTO;
    }

    public static DescuentoTarjetaConvenioDetDTO toDescuentoTarjetaConvenioDetDTO(
            DescuentoTarjetaConvenioDet dfd) {
        DescuentoTarjetaConvenioDetDTO dfdDTO = new DescuentoTarjetaConvenioDetDTO();
        BeanUtils.copyProperties(dfd, dfdDTO);
        return dfdDTO;
    }

    public static DescuentoTarjetaConvenioDet fromDescuentoTarjetaConvenioDetDTO(
            DescuentoTarjetaConvenioDetDTO dfdDTO) {
        DescuentoTarjetaConvenioDet dfd = new DescuentoTarjetaConvenioDet();
        BeanUtils.copyProperties(dfdDTO, dfd);
        dfd.setDescuentoTarjetaConvenioCab(DescuentoTarjetaConvenioCab
                .fromDescuentoTarjetaConvenioCabSinTarjetaDTO(dfdDTO
                        .getDescuentoTarjetaConvenioCab()));
        dfd.setDia(Dias.fromDiaDTO(dfdDTO.getDia()));
        return dfd;
    }

}
