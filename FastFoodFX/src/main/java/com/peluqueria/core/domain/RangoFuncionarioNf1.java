package com.peluqueria.core.domain;

import com.peluqueria.dto.RangoFuncionarioDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rango_funcionario_nf1 database table.
 *
 */
@Entity
@Table(name = "rango_funcionario_nf1", schema = "general")
@NamedQuery(name = "RangoFuncionarioNf1.findAll", query = "SELECT rangoFuncionarioNf1 FROM RangoFuncionarioNf1 rangoFuncionarioNf1")
public class RangoFuncionarioNf1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rango")
    private Long idRango;

    @Column(name = "rango_inicial")
    private Long rangoInicial;

    @Column(name = "rango_final")
    private Long rangoFinal;

    @Column(name = "rango_actual")
    private Long rangoActual;

    public RangoFuncionarioNf1() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

    public RangoFuncionarioDTO toRangoFuncionarioDTO() {
        RangoFuncionarioDTO rangoDTO = toRangoFuncionarioDTO(this);
        return rangoDTO;
    }

    public static RangoFuncionarioDTO toRangoFuncionarioDTO(RangoFuncionarioNf1 rango) {
        RangoFuncionarioDTO dDTO = new RangoFuncionarioDTO();
        BeanUtils.copyProperties(rango, dDTO);
        return dDTO;
    }

    public static RangoFuncionarioNf1 fromRangoFuncionarioDTO(RangoFuncionarioDTO rangoDTO) {
        RangoFuncionarioNf1 rango = new RangoFuncionarioNf1();
        BeanUtils.copyProperties(rangoDTO, rango);
        return rango;
    }

}
