package com.peluqueria.core.domain;

import com.peluqueria.dto.CierreCajaDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the cierre_caja database table.
 *
 */
@Entity
@Table(name = "cierre_caja", schema = "caja")
@NamedQuery(name = "CierreCaja.findAll", query = "SELECT c FROM CierreCaja c")
public class CierreCaja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cierre")
    private Long idCierre;

    @Column(name = "diferencia_cierre")
    private BigDecimal diferenciaCierre;

    @Column(name = "fecha_cierre")
    private Timestamp fechaCierre;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cajero")
    private Usuario usuarioCajero;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_supervisor")
    private Usuario usuarioSupervisor;

    @Column(name = "monto_caja")
    private BigDecimal montoCaja;

    @Column(name = "monto_cierre")
    private BigDecimal montoCierre;

    @Column(name = "tipo_cierre")
    private String tipoCierre;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caja")
    private Caja caja;

    public CierreCaja() {
    }

    public Long getIdCierre() {
        return this.idCierre;
    }

    public void setIdCierre(Long idCierre) {
        this.idCierre = idCierre;
    }

    public BigDecimal getDiferenciaCierre() {
        return this.diferenciaCierre;
    }

    public void setDiferenciaCierre(BigDecimal diferenciaCierre) {
        this.diferenciaCierre = diferenciaCierre;
    }

    public Timestamp getFechaCierre() {
        return this.fechaCierre;
    }

    public void setFechaCierre(Timestamp fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public BigDecimal getMontoCaja() {
        return this.montoCaja;
    }

    public void setMontoCaja(BigDecimal montoCaja) {
        this.montoCaja = montoCaja;
    }

    public BigDecimal getMontoCierre() {
        return this.montoCierre;
    }

    public void setMontoCierre(BigDecimal montoCierre) {
        this.montoCierre = montoCierre;
    }

    public String getTipoCierre() {
        return this.tipoCierre;
    }

    public void setTipoCierre(String tipoCierre) {
        this.tipoCierre = tipoCierre;
    }

    public Caja getCaja() {
        return this.caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    // *******************************************************************
    public Usuario getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(Usuario usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public Usuario getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public CierreCajaDTO toCierreCajaDTO() {
        CierreCajaDTO cjDTO = toCierreCajaDTO(this);
        if (this.caja != null) {
            cjDTO.setCaja(this.getCaja().toBDCajaDTO());
        }

        if (this.usuarioCajero != null) {
            cjDTO.setUsuarioCajero(this.getUsuarioCajero().toBDUsuarioDTO());
        }

        if (this.usuarioSupervisor != null) {
            cjDTO.setUsuarioSupervisor(this.getUsuarioSupervisor()
                    .toBDUsuarioDTO());
        }
        return cjDTO;
    }

    public CierreCajaDTO toBDCierreCajaDTO() {
        CierreCajaDTO cjDTO = toCierreCajaDTO(this);
        cjDTO.setCaja(null);
        cjDTO.setUsuarioCajero(null);
        cjDTO.setUsuarioSupervisor(null);
        return cjDTO;
    }

    public static CierreCajaDTO toCierreCajaDTO(CierreCaja cierreCaja) {
        CierreCajaDTO cjDTO = new CierreCajaDTO();
        BeanUtils.copyProperties(cierreCaja, cjDTO);
        return cjDTO;
    }

    public static CierreCaja fromCierreCajaDTO(CierreCajaDTO cjDTO) {
        CierreCaja cj = new CierreCaja();
        BeanUtils.copyProperties(cjDTO, cj);
        return cj;
    }

    public static CierreCaja fromCierreCajaAsociadoDTO(CierreCajaDTO cjDTO) {
        CierreCaja cj = fromCierreCajaDTO(cjDTO);
        cj.setCaja(Caja.fromCajaDTO(cjDTO.getCaja()));
        cj.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(cjDTO
                .getUsuarioCajero()));
        cj.setUsuarioSupervisor(Usuario.fromUsuarioSupervisorDTO(cjDTO
                .getUsuarioSupervisor()));
        return cj;
    }

}
