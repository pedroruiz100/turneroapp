package com.peluqueria.core.domain;

import com.peluqueria.dto.DiaDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the dias database table.
 *
 */
@Entity
@Table(name = "dias", schema = "general")
@NamedQuery(name = "Dias.findAll", query = "SELECT d FROM Dias d")
public class Dias implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_dia")
    private Long idDia;

    @Column(name = "descripcion_dia")
    private String descripcionDia;

    // bi-directional many-to-one association to DescuentoFielDet
    @OneToMany(mappedBy = "dia")
    private List<DescuentoFielDet> descuentoFielDets;

    // bi-directional many-to-one association to DescuentoFielDet
    @OneToMany(mappedBy = "dia")
    private List<DescuentoTarjetaConvenioDet> descuentoTarjetaConvenioDet;

    // bi-directional many-to-one association to DescuentoTarjetaDet
    @OneToMany(mappedBy = "dia")
    private List<DescuentoTarjetaDet> descuentoTarjetaDet;

    public Dias() {
    }

    public List<DescuentoTarjetaConvenioDet> getDescuentoTarjetaConvenioDet() {
        return descuentoTarjetaConvenioDet;
    }

    public void setDescuentoTarjetaConvenioDet(
            List<DescuentoTarjetaConvenioDet> descuentoTarjetaConvenioDet) {
        this.descuentoTarjetaConvenioDet = descuentoTarjetaConvenioDet;
    }

    public List<DescuentoFielDet> getDescuentoFielDets() {
        return descuentoFielDets;
    }

    public void setDescuentoFielDets(List<DescuentoFielDet> descuentoFielDets) {
        this.descuentoFielDets = descuentoFielDets;
    }

    public Long getIdDia() {
        return this.idDia;
    }

    public void setIdDia(Long idDia) {
        this.idDia = idDia;
    }

    public String getDescripcionDia() {
        return this.descripcionDia;
    }

    public void setDescripcionDia(String descripcionDia) {
        this.descripcionDia = descripcionDia;
    }

    // **************************************************************************************************************
    public List<DescuentoTarjetaDet> getDescuentoTarjetaDet() {
        return descuentoTarjetaDet;
    }

    public void setDescuentoTarjetaDet(
            List<DescuentoTarjetaDet> descuentoTarjetaDet) {
        this.descuentoTarjetaDet = descuentoTarjetaDet;
    }

    public DiaDTO toDiaDTO() {
        DiaDTO diaDTO = toDiaDTO(this);
        diaDTO.setDescuentoFielDets(null);
        diaDTO.setDescuentoTarjetaConvenioDet(null);
        diaDTO.setDescuentoTarjetaDet(null);
        return diaDTO;
    }

    public static DiaDTO toDiaDTO(Dias dia) {
        DiaDTO dDTO = new DiaDTO();
        BeanUtils.copyProperties(dia, dDTO);
        return dDTO;
    }

    public static Dias fromDiaDTO(DiaDTO diaDTO) {
        Dias dia = new Dias();
        BeanUtils.copyProperties(diaDTO, dia);
        return dia;
    }

}
