package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabValeDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_vale database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_vale", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabVale.findAll", query = "SELECT f FROM FacturaClienteCabVale f")
public class FacturaClienteCabVale implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_vale")
    private Long idFacturaClienteCabVale;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to vale
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_vale")
    private Vales vale;

    @Column(name = "monto")
    private Integer monto;

    @Column(name = "nro_vale")
    private String nroVale;

    @Column(name = "ci")
    private String ci;

    public FacturaClienteCabVale() {
    }

    public Long getIdFacturaClienteCabVale() {
        return idFacturaClienteCabVale;
    }

    public void setIdFacturaClienteCabVale(Long idFacturaClienteCabVale) {
        this.idFacturaClienteCabVale = idFacturaClienteCabVale;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Vales getVale() {
        return vale;
    }

    public void setVale(Vales vale) {
        this.vale = vale;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public String getNroVale() {
        return nroVale;
    }

    public void setNroVale(String nroVale) {
        this.nroVale = nroVale;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public FacturaClienteCabValeDTO toBDFacturaClienteCabValeDTO() {
        FacturaClienteCabValeDTO facDTO = toFacturaClienteCabValeDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.vale != null) {
            Vales tar = this.getVale();
            facDTO.setVale(tar.toValeDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabValeDTO toDescripcionFacturaClienteCabValeDTO() {
        FacturaClienteCabValeDTO facDTO = toFacturaClienteCabValeDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setVale(null);
        return facDTO;
    }

    public static FacturaClienteCabValeDTO toFacturaClienteCabValeDTO(
            FacturaClienteCabVale facturaClienteCabVale) {
        FacturaClienteCabValeDTO facDTO = new FacturaClienteCabValeDTO();
        BeanUtils.copyProperties(facturaClienteCabVale, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabVale fromFacturaClienteCabValeDTO(
            FacturaClienteCabValeDTO tarDTO) {
        FacturaClienteCabVale fac = new FacturaClienteCabVale();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public static FacturaClienteCabVale fromFacturaClienteCabValeAsociadoDTO(
            FacturaClienteCabValeDTO tarDTO) {
        FacturaClienteCabVale fac = fromFacturaClienteCabValeDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setVale(Vales.fromValeDTO(tarDTO.getVale()));
        return fac;
    }
}
