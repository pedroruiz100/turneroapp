package com.peluqueria.core.domain;

import com.peluqueria.dto.NotaCreditoDetProveedorDTO;
import java.io.Serializable;
import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the nota_credito_det database table.
 *
 */
@Entity
@Table(name = "nota_credito_det_proveedor", schema = "factura_cliente")
@NamedQuery(name = "NotaCreditoDetProveedor.findAll", query = "SELECT n FROM NotaCreditoDetProveedor n")
public class NotaCreditoDetProveedor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_credito_det_proveedor")
    private Long idNotaCreditoDetProveedor;

    private Long cantidad;

    private Long precio;
    private Long total;
    private String descripcion;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    public NotaCreditoDetProveedor() {
    }

    public Long getIdNotaCreditoDetProveedor() {
        return idNotaCreditoDetProveedor;
    }

    public void setIdNotaCreditoDetProveedor(Long idNotaCreditoDetProveedor) {
        this.idNotaCreditoDetProveedor = idNotaCreditoDetProveedor;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public NotaCreditoDetProveedorDTO toNotaCreditoDetProveedorDTO() {
        NotaCreditoDetProveedorDTO facDTO = toNotaCreditoDetProveedorDTO(this);
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo()
                    .toBDArticuloDTO());
        }
        return facDTO;
    }

    public static NotaCreditoDetProveedorDTO toNotaCreditoDetProveedorDTO(
            NotaCreditoDetProveedor facturaClienteDet) {
        NotaCreditoDetProveedorDTO facDTO = new NotaCreditoDetProveedorDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static NotaCreditoDetProveedor fromNotaCreditoDetProveedorDTO(
            NotaCreditoDetProveedorDTO facDTO) {
        NotaCreditoDetProveedor fac = new NotaCreditoDetProveedor();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static NotaCreditoDetProveedor fromNotaCreditoDetProveedorAsociado(
            NotaCreditoDetProveedorDTO facDTO) {
        NotaCreditoDetProveedor fac = fromNotaCreditoDetProveedorDTO(facDTO);
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
