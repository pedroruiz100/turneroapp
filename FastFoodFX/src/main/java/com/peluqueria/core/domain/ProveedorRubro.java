package com.peluqueria.core.domain;

import com.peluqueria.dto.ProveedorRubroDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the proveedor_rubro database table.
 *
 */
@Entity
@Table(name = "proveedor_rubro", schema = "cuenta")
@NamedQuery(name = "ProveedorRubro.findAll", query = "SELECT p FROM ProveedorRubro p")
public class ProveedorRubro implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_proveedor_rubro")
    private Long idProveedorRubro;

    // bi-directional many-to-one association to Proveedor
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to Rubro
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rubro")
    private Rubro rubro;

    public ProveedorRubro() {
    }

    public Long getIdProveedorRubro() {
        return this.idProveedorRubro;
    }

    public void setIdProveedorRubro(Long idProveedorRubro) {
        this.idProveedorRubro = idProveedorRubro;
    }

    public Proveedor getProveedor() {
        return this.proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public Rubro getRubro() {
        return this.rubro;
    }

    public void setRubro(Rubro rubro) {
        this.rubro = rubro;
    }

    public ProveedorRubroDTO toProveedorRubroDTO() {
        ProveedorRubroDTO proDTO = toProveedorRubroDTO(this);
        if (this.proveedor != null) {
            proDTO.setProveedor(this.getProveedor().toProveedorBdDTO());
        }
        if (this.rubro != null) {
            proDTO.setRubro(this.getRubro().toBDRubroDTO());
        }
        return proDTO;
    }

    public ProveedorRubroDTO toBDProveedorRubroDTO() {
        ProveedorRubroDTO proDTO = toProveedorRubroDTO(this);
        proDTO.setProveedor(null);
        proDTO.setRubro(null);
        return null;
    }

    public static ProveedorRubroDTO toProveedorRubroDTO(
            ProveedorRubro proveedorRubro) {
        ProveedorRubroDTO proDTO = new ProveedorRubroDTO();
        BeanUtils.copyProperties(proveedorRubro, proDTO);
        return proDTO;
    }

    public static ProveedorRubro fromProveedorRubroDTO(ProveedorRubroDTO proDTO) {
        ProveedorRubro pro = new ProveedorRubro();
        BeanUtils.copyProperties(proDTO, pro);
        return pro;
    }

    public static ProveedorRubro fromProveedorRubroAsociadoDTO(
            ProveedorRubroDTO proDTO) {
        ProveedorRubro pro = fromProveedorRubroDTO(proDTO);
        pro.setProveedor(Proveedor.fromProveedorDTO(proDTO.getProveedor()));
        pro.setRubro(Rubro.fromRubro(proDTO.getRubro()));
        return pro;
    }

}
