package com.peluqueria.core.domain;

import com.peluqueria.dto.FamiliaProvDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the familia_prov database table.
 *
 */
@Entity
@Table(name = "familia_prov", schema = "cuenta")
@NamedQuery(name = "FamiliaProv.findAll", query = "SELECT f FROM FamiliaProv f")
public class FamiliaProv implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_familia_prov")
    private Long idFamiliaProv;

    private String descripcion;

    // bi-directional many-to-one association to Proveedor
    @OneToMany(mappedBy = "familiaProv", fetch = FetchType.LAZY)
    private List<Proveedor> proveedors;

    public FamiliaProv() {
    }

    public Long getIdFamiliaProv() {
        return this.idFamiliaProv;
    }

    public void setIdFamiliaProv(Long idFamiliaProv) {
        this.idFamiliaProv = idFamiliaProv;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Proveedor> getProveedors() {
        return this.proveedors;
    }

    public void setProveedors(List<Proveedor> proveedors) {
        this.proveedors = proveedors;
    }

    public FamiliaProvDTO toFamiliaProvDTO() {
        FamiliaProvDTO faDTO = toFamiliaProvDTO(this);
        faDTO.setProveedors(null);
        return faDTO;
    }

    public static FamiliaProvDTO toFamiliaProvDTO(FamiliaProv familiaProv) {
        FamiliaProvDTO faDTO = new FamiliaProvDTO();
        BeanUtils.copyProperties(familiaProv, faDTO);
        return faDTO;
    }

    public static FamiliaProv fromFamiliaProv(FamiliaProvDTO faDTO) {
        FamiliaProv fa = new FamiliaProv();
        BeanUtils.copyProperties(faDTO, fa);
        return fa;
    }
}
