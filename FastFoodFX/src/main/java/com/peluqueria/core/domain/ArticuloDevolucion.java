package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloDevolucionDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "articulo_devolucion", schema = "stock")
@NamedQuery(name = "ArticuloDevolucion.findAll", query = "SELECT f FROM ArticuloDevolucion f")
public class ArticuloDevolucion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_devolucion")
    private Long idArticuloDevolucion;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_compra_cab")
    private FacturaCompraCab facturaCompraCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "cantidad")
    private String cantidad;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "precio")
    private Long precio;
    @Column(name = "nro_devolucion")
    private String nroDevolucion;

    @Column(name = "total")
    private Long total;
    
    @Column(name = "fecha")
    private Date fecha;

    public ArticuloDevolucion() {
    }

    public Long getIdArticuloDevolucion() {
        return idArticuloDevolucion;
    }

    public void setIdArticuloDevolucion(Long idArticuloDevolucion) {
        this.idArticuloDevolucion = idArticuloDevolucion;
    }

    public FacturaCompraCab getFacturaCompraCab() {
        return facturaCompraCab;
    }

    public void setFacturaCompraCab(FacturaCompraCab facturaCompraCab) {
        this.facturaCompraCab = facturaCompraCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getNroDevolucion() {
        return nroDevolucion;
    }

    public void setNroDevolucion(String nroDevolucion) {
        this.nroDevolucion = nroDevolucion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public ArticuloDevolucionDTO toArticuloDevolucionTO() {
        ArticuloDevolucionDTO facDTO = toArticuloDevolucionDTO(this);
        if (this.facturaCompraCab != null) {
            facDTO.setFacturaCompraCab(this.getFacturaCompraCab()
                    .toFacturaCompraCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return facDTO;
    }

    public ArticuloDevolucionDTO toDescriArticuloDevolucionDTO() {
        ArticuloDevolucionDTO facDTO = toArticuloDevolucionDTO(this);
        facDTO.setFacturaCompraCab(null);
        facDTO.setArticulo(null);
        return facDTO;
    }

    public static ArticuloDevolucionDTO toArticuloDevolucionDTO(
            ArticuloDevolucion facturaClienteDet) {
        ArticuloDevolucionDTO facDTO = new ArticuloDevolucionDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static ArticuloDevolucion fromArticuloDevolucionDTO(
            ArticuloDevolucionDTO facDTO) {
        ArticuloDevolucion fac = new ArticuloDevolucion();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static ArticuloDevolucion fromArticuloDevolucionAsociado(
            ArticuloDevolucionDTO facDTO) {
        ArticuloDevolucion fac = fromArticuloDevolucionDTO(facDTO);
        fac.setFacturaCompraCab(FacturaCompraCab.fromFacturaCompraCabDTO(facDTO.getFacturaCompraCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }
}
