/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaCabArticuloDTO;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_tarjeta_cab_articulo database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_articulo", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabArticulo.findAll", query = "SELECT dtca FROM DescuentoTarjetaCabArticulo dtca")
public class DescuentoTarjetaCabArticulo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_articulo")
    private Long idDescuentoTarjetaCabArticulo;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    public DescuentoTarjetaCabArticulo() {
    }

    public Long getIdDescuentoTarjetaCabArticulo() {
        return this.idDescuentoTarjetaCabArticulo;
    }

    public void setIdDescuentoTarjetaCabArticulo(Long idDescuentoTarjetaCabArticulo) {
        this.idDescuentoTarjetaCabArticulo = idDescuentoTarjetaCabArticulo;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabArticulo fromDescuentoTarjetaCabArticuloDTO(DescuentoTarjetaCabArticuloDTO descuentoTarjetaCabArticuloDTO) {
        DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo = new DescuentoTarjetaCabArticulo();
        BeanUtils.copyProperties(descuentoTarjetaCabArticuloDTO, descuentoTarjetaCabArticulo);
        return descuentoTarjetaCabArticulo;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabArticuloDTO toDescuentoTarjetaCabArticuloDTO(DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo) {
        DescuentoTarjetaCabArticuloDTO descuentoTarjetaCabArticuloDTO = new DescuentoTarjetaCabArticuloDTO();
        BeanUtils.copyProperties(descuentoTarjetaCabArticulo, descuentoTarjetaCabArticuloDTO);
        return descuentoTarjetaCabArticuloDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    public static DescuentoTarjetaCabArticulo fromDescuentoTarjetaCabArticuloNoListDTO(
            DescuentoTarjetaCabArticuloDTO descuentoTarjetaCabArticuloDTO) {
        DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo = new DescuentoTarjetaCabArticulo();
        BeanUtils.copyProperties(descuentoTarjetaCabArticuloDTO, descuentoTarjetaCabArticulo);
        descuentoTarjetaCabArticulo.setArticulo(Articulo.fromArticuloDTONoList(descuentoTarjetaCabArticuloDTO.getArticulo()));
        descuentoTarjetaCabArticulo.setDescuentoTarjetaCab(DescuentoTarjetaCab
                .fromDescTarSinTarjetaCabDTO(descuentoTarjetaCabArticuloDTO.getDescuentoTarjetaCab()));
        return descuentoTarjetaCabArticulo;
    }
    
    public DescuentoTarjetaCabArticuloDTO toDescuentoTarjetaCabArticuloDTO() {
        DescuentoTarjetaCabArticuloDTO descuentoTarjetaCabArticuloDTO = toDescuentoTarjetaCabArticuloDTO(this);
        if (this.articulo != null) {
            descuentoTarjetaCabArticuloDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        descuentoTarjetaCabArticuloDTO.setDescuentoTarjetaCab(null);
        return descuentoTarjetaCabArticuloDTO;
    }
}
