package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf3SseccionDTO;

/**
 * The persistent class for the nf3_sseccion database table.
 *
 */
@Entity
@Table(name = "nf3_sseccion", schema = "stock")
@NamedQuery(name = "Nf3Sseccion.findAll", query = "SELECT n FROM Nf3Sseccion n")
public class Nf3Sseccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf3_sseccion")
    private Long idNf3Sseccion;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    // bi-directional many-to-one association to ArticuloNf3Sseccion
    @OneToMany(mappedBy = "nf3Sseccion")
    private List<ArticuloNf3Sseccion> articuloNf3Sseccions;

    // bi-directional one-to-many association to Nf4Seccion1
    @OneToMany(mappedBy = "nf3Sseccion")
    private List<Nf4Seccion1> nf4Seccion1s;

    // bi-directional one-to-many association to FuncionarioNf3
    @OneToMany(mappedBy = "nf3Sseccion")
    private List<FuncionarioNf3> funcionarioNf3s;

    // bi-directional one-to-many association to DescuentoFielNf3
    @OneToMany(mappedBy = "nf3Sseccion")
    private List<DescuentoFielNf3> descuentoFielNf3s;

    // bi-directional one-to-many association to PromoTemporadaNf3
    @OneToMany(mappedBy = "nf3Sseccion")
    private List<PromoTemporadaNf3> promoTemporadaNf3s;

    public Nf3Sseccion() {
    }

    public Long getIdNf3Sseccion() {
        return this.idNf3Sseccion;
    }

    public void setIdNf3Sseccion(Long idNf3Sseccion) {
        this.idNf3Sseccion = idNf3Sseccion;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf3Sseccion> getArticuloNf3Sseccions() {
        return this.articuloNf3Sseccions;
    }

    public void setArticuloNf3Sseccions(List<ArticuloNf3Sseccion> articuloNf3Sseccions) {
        this.articuloNf3Sseccions = articuloNf3Sseccions;
    }

    public ArticuloNf3Sseccion addArticuloNf3Sseccion(ArticuloNf3Sseccion articuloNf3Sseccion) {
        getArticuloNf3Sseccions().add(articuloNf3Sseccion);
        articuloNf3Sseccion.setNf3Sseccion(this);

        return articuloNf3Sseccion;
    }

    public ArticuloNf3Sseccion removeArticuloNf3Sseccion(ArticuloNf3Sseccion articuloNf3Sseccion) {
        getArticuloNf3Sseccions().remove(articuloNf3Sseccion);
        articuloNf3Sseccion.setNf3Sseccion(null);

        return articuloNf3Sseccion;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return this.nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    public List<Nf4Seccion1> getNf4Seccion1s() {
        return this.nf4Seccion1s;
    }

    public void setNf4Seccion1s(List<Nf4Seccion1> nf4Seccion1s) {
        this.nf4Seccion1s = nf4Seccion1s;
    }

    public Nf4Seccion1 addNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        getNf4Seccion1s().add(nf4Seccion1);
        nf4Seccion1.setNf3Sseccion(this);

        return nf4Seccion1;
    }

    public Nf4Seccion1 removeNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        getNf4Seccion1s().remove(nf4Seccion1);
        nf4Seccion1.setNf3Sseccion(null);

        return nf4Seccion1;
    }

    public List<FuncionarioNf3> getFuncionarioNf3s() {
        return this.funcionarioNf3s;
    }

    public void setFuncionarioNf3s(List<FuncionarioNf3> funcionarioNf3s) {
        this.funcionarioNf3s = funcionarioNf3s;
    }

    public FuncionarioNf3 addFuncionarioNf3(FuncionarioNf3 funcionarioNf3) {
        getFuncionarioNf3s().add(funcionarioNf3);
        funcionarioNf3.setNf3Sseccion(this);

        return funcionarioNf3;
    }

    public FuncionarioNf3 removeFuncionarioNf3(FuncionarioNf3 funcionarioNf3) {
        getFuncionarioNf3s().remove(funcionarioNf3);
        funcionarioNf3.setNf3Sseccion(null);

        return funcionarioNf3;
    }

    public List<DescuentoFielNf3> getDescuentoFielNf3s() {
        return this.descuentoFielNf3s;
    }

    public void setDescuentoFielNf3s(List<DescuentoFielNf3> descuentoFielNf3s) {
        this.descuentoFielNf3s = descuentoFielNf3s;
    }

    public DescuentoFielNf3 addDescuentoFielNf3(DescuentoFielNf3 descuentoFielNf3) {
        getDescuentoFielNf3s().add(descuentoFielNf3);
        descuentoFielNf3.setNf3Sseccion(this);

        return descuentoFielNf3;
    }

    public DescuentoFielNf3 removeDescuentoFielNf3(DescuentoFielNf3 descuentoFielNf3) {
        getDescuentoFielNf3s().remove(descuentoFielNf3);
        descuentoFielNf3.setNf3Sseccion(null);

        return descuentoFielNf3;
    }

    public List<PromoTemporadaNf3> getPromoTemporadaNf3s() {
        return this.promoTemporadaNf3s;
    }

    public void setPromoTemporadaNf3s(List<PromoTemporadaNf3> promoTemporadaNf3s) {
        this.promoTemporadaNf3s = promoTemporadaNf3s;
    }

    public PromoTemporadaNf3 addPromoTemporadaNf3(PromoTemporadaNf3 promoTemporadaNf3) {
        getPromoTemporadaNf3s().add(promoTemporadaNf3);
        promoTemporadaNf3.setNf3Sseccion(this);

        return promoTemporadaNf3;
    }

    public PromoTemporadaNf3 removePromoTemporadaNf3(PromoTemporadaNf3 promoTemporadaNf3) {
        getPromoTemporadaNf3s().remove(promoTemporadaNf3);
        promoTemporadaNf3.setNf3Sseccion(null);

        return promoTemporadaNf3;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf3Sseccion fromNf3SseccionDTO(Nf3SseccionDTO nf3SseccionDTO) {
        Nf3Sseccion nf3Sseccion = new Nf3Sseccion();
        BeanUtils.copyProperties(nf3SseccionDTO, nf3Sseccion);
        return nf3Sseccion;
    }

    // <-- SERVER
    public static Nf3SseccionDTO toNf3SseccionDTO(Nf3Sseccion nf3Sseccion) {
        Nf3SseccionDTO nf3SseccionDTO = new Nf3SseccionDTO();
        BeanUtils.copyProperties(nf3Sseccion, nf3SseccionDTO);
        return nf3SseccionDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf3SseccionDTO toNf3SseccionDTOEntitiesNull(Nf3Sseccion nf3Sseccion) {
        Nf3SseccionDTO nf3SseccionDTO = toNf3SseccionDTO(nf3Sseccion);
        nf3SseccionDTO.setNf2Sfamilia(null);// Entity
        nf3SseccionDTO.setNf4Seccion1s(null);// Entities
        nf3SseccionDTO.setArticuloNf3Sseccions(null);// Entities
        nf3SseccionDTO.setDescuentoFielNf3s(null);// Entities
        nf3SseccionDTO.setFuncionarioNf3s(null);// Entities
        nf3SseccionDTO.setPromoTemporadaNf3s(null); // Entities
        return nf3SseccionDTO;
    }

    // <-- SERVER
    public Nf3SseccionDTO toNf3SseccionDTOEntitiesNull() {
        Nf3SseccionDTO nf3SseccionDTO = toNf3SseccionDTO(this);
        nf3SseccionDTO.setNf2Sfamilia(null);// Entity
        nf3SseccionDTO.setNf4Seccion1s(null);// Entities
        nf3SseccionDTO.setArticuloNf3Sseccions(null);// Entities
        nf3SseccionDTO.setDescuentoFielNf3s(null);// Entities
        nf3SseccionDTO.setFuncionarioNf3s(null);// Entities
        nf3SseccionDTO.setPromoTemporadaNf3s(null); // Entities
        return nf3SseccionDTO;
    }

    // <-- SERVER
    public Nf3SseccionDTO toNf3SseccionDTOEntitiesNullNf2() {
        Nf3SseccionDTO nf3SseccionDTO = toNf3SseccionDTO(this);
        nf3SseccionDTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());// Entity
        nf3SseccionDTO.setNf4Seccion1s(null);// Entities
        nf3SseccionDTO.setArticuloNf3Sseccions(null);// Entities
        nf3SseccionDTO.setDescuentoFielNf3s(null);// Entities
        nf3SseccionDTO.setFuncionarioNf3s(null);// Entities
        nf3SseccionDTO.setPromoTemporadaNf3s(null); // Entities
        return nf3SseccionDTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf3Sseccion fromNf3SseccionDTOEntitiesFull(Nf3SseccionDTO nf3SseccionDTO) {
        Nf3Sseccion nf3Sseccion = fromNf3SseccionDTO(nf3SseccionDTO);
        if (nf3SseccionDTO.getNf2Sfamilia() != null) {
            nf3Sseccion.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(nf3SseccionDTO.getNf2Sfamilia()));
        }
        return nf3Sseccion;
    }
    // ######################## FULL, ENTRADA ########################

}
