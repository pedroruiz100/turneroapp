package com.peluqueria.core.domain;

import com.peluqueria.dto.TipoCajaDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_caja database table.
 *
 */
@Entity
@Table(name = "tipo_caja", schema = "caja")
@NamedQuery(name = "TipoCaja.findAll", query = "SELECT t FROM TipoCaja t")
public class TipoCaja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_caja")
    private Long idTipoCaja;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Caja
    @OneToMany(mappedBy = "tipoCaja", fetch = FetchType.LAZY)
    private List<Caja> cajas;

    public TipoCaja() {
    }

    public Long getIdTipoCaja() {
        return this.idTipoCaja;
    }

    public void setIdTipoCaja(Long idTipoCaja) {
        this.idTipoCaja = idTipoCaja;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Caja> getCajas() {
        return this.cajas;
    }

    public void setCajas(List<Caja> cajas) {
        this.cajas = cajas;
    }

    public TipoCajaDTO toTipoCajaDTO() {
        TipoCajaDTO tcDTO = toTipoCajaDTO(this);
        tcDTO.setCajas(null);
        return tcDTO;
    }

    public static TipoCajaDTO toTipoCajaDTO(TipoCaja tc) {
        TipoCajaDTO tcDTO = new TipoCajaDTO();
        BeanUtils.copyProperties(tc, tcDTO);
        return tcDTO;
    }

    public static TipoCaja fromTipoCajaDTO(TipoCajaDTO tcDTO) {
        TipoCaja tc = new TipoCaja();
        BeanUtils.copyProperties(tcDTO, tc);
        return tc;
    }

}
