package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf5DTO;

/**
 * The persistent class for the promo_temporada_nf5 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf5", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf5.findAll", query = "SELECT p FROM PromoTemporadaNf5 p")
public class PromoTemporadaNf5 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf5")
    private Long idPromoTemporadaNf5;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    public PromoTemporadaNf5() {
    }

    public Long getIdPromoTemporadaNf5() {
        return this.idPromoTemporadaNf5;
    }

    public void setIdPromoTemporadaNf5(Long idPromoTemporadaNf5) {
        this.idPromoTemporadaNf5 = idPromoTemporadaNf5;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf5 fromPromoTemporadaNf5DTO(PromoTemporadaNf5DTO promoTemporadaNf5DTO) {
        PromoTemporadaNf5 promoTemporadaNf5 = new PromoTemporadaNf5();
        BeanUtils.copyProperties(promoTemporadaNf5DTO, promoTemporadaNf5);
        return promoTemporadaNf5;
    }

    // <-- SERVER
    public static PromoTemporadaNf5DTO toPromoTemporadaNf5DTO(PromoTemporadaNf5 promoTemporadaNf5) {
        PromoTemporadaNf5DTO promoTemporadaNf5DTO = new PromoTemporadaNf5DTO();
        BeanUtils.copyProperties(promoTemporadaNf5, promoTemporadaNf5DTO);
        return promoTemporadaNf5DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf5 fromPromoTemporadaNf5DTOEntitiesFull(PromoTemporadaNf5DTO promoTemporadaNf5DTO) {
        PromoTemporadaNf5 promoTemporadaNf5 = fromPromoTemporadaNf5DTO(promoTemporadaNf5DTO);
        if (promoTemporadaNf5DTO.getNf5Seccion2() != null) {
            promoTemporadaNf5.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(promoTemporadaNf5DTO.getNf5Seccion2()));
        }
        if (promoTemporadaNf5DTO.getPromoTemporada() != null) {
            promoTemporadaNf5
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf5DTO.getPromoTemporada()));
        }
        return promoTemporadaNf5;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf5DTO toPromoTemporadaNf5DTOEntityFull() {
        PromoTemporadaNf5DTO promoTemporadaNf5DTO = toPromoTemporadaNf5DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf5DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf5DTO.setPromoTemporada(null);
        }
        if (this.getNf5Seccion2() != null) {
            promoTemporadaNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            promoTemporadaNf5DTO.setNf5Seccion2(null);
        }
        return promoTemporadaNf5DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf5DTO toPromoTemporadaNf5DTOEntityNull() {
        PromoTemporadaNf5DTO promoTemporadaNf5DTO = toPromoTemporadaNf5DTO(this);
        promoTemporadaNf5DTO.setPromoTemporada(null);
        promoTemporadaNf5DTO.setNf5Seccion2(null);
        return promoTemporadaNf5DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf5DTO toPromoTemporadaNf5DTONf4() {
        PromoTemporadaNf5DTO promoTemporadaNf5DTO = toPromoTemporadaNf5DTO(this);
        promoTemporadaNf5DTO.setPromoTemporada(null);
        if (this.getNf5Seccion2() != null) {
            promoTemporadaNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            promoTemporadaNf5DTO.setNf5Seccion2(null);
        }
        return promoTemporadaNf5DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
