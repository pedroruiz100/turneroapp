package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabFuncionarioDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.FacturaClienteCabPromoTempArtDTO;
import com.peluqueria.dto.FacturaClienteCabPromoTempDTO;
import com.peluqueria.dto.FacturaClienteCabTarjFielDTO;
import com.peluqueria.dto.FacturaClienteCabTarjetaDTO;
import com.peluqueria.dto.FacturaClienteDetDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCab.findAll", query = "SELECT f FROM FacturaClienteCab f")
public class FacturaClienteCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab")
    private Long idFacturaClienteCab;

    @Column(name = "cancelado")
    private Boolean cancelado;

    @Column(name = "fecha_emision")
    private Timestamp fechaEmision;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_caja")
    private Caja caja;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    // bi-directional many-to-one association to Sucursal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    @Column(name = "monto_factura")
    private Integer montoFactura;

    @Column(name = "nro_factura")
    private String nroFactura;

    // bi-directional many-to-one association to DescVale
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<DescVale> descVales;

    // bi-directional many-to-one association to DonacionCliente
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<DonacionCliente> donacionClientes;

    // bi-directional many-to-one association to EstadoFactura
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_estado_factura")
    private EstadoFactura estadoFactura;

    // bi-directional many-to-one association to TipoComprobante
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_comprobante")
    private TipoComprobante tipoComprobante;

    // bi-directional many-to-one association to TipoMoneda
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_moneda")
    private TipoMoneda tipoMoneda;

    // bi-directional many-to-one association to FacturaClienteCabTarjConvenio
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTarjConvenio> facturaClienteCabTarjConvenios;

    // bi-directional many-to-one association to FacturaClienteCabTarjeta
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTarjeta> facturaClienteCabTarjetas;

    // bi-directional many-to-one association to FacturaClienteCabTipoPago
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos;

    // bi-directional many-to-one association to FacturaClienteDet
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteDet> facturaClienteDets;

    // bi-directional many-to-one association to NotaCreditoCab
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<NotaCreditoCab> notaCreditoCabs;

    // bi-directional many-to-one association to NotaRemisionCab
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<NotaRemisionCab> notaRemisionCabs;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<CancelacionFactura> cancelacionFacturas;

    // bi-directional many-to-one association to FacturaClienteCabCheque
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabCheque> facturaClienteCabCheque;

    // bi-directional many-to-one association to FacturaClienteCabEfectivo
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabEfectivo> facturaClienteCabEfectivo;

    // bi-directional many-to-one association to FacturaClienteCabFuncionario
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabFuncionario> facturaClienteCabFuncionario;

    // bi-directional many-to-one association to FacturaClienteCabTarjFiel
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTarjFiel> facturaClienteCabTarjFiel;

    // bi-directional many-to-one association to FacturaClienteCabPromoTemp
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabPromoTemp> facturaClienteCabPromoTemp;

    // bi-directional many-to-one association to CancelacionProducto
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<CancelacionProducto> cancelacionProducto;

    // bi-directional many-to-one association to facturaClienteCabNotaCredito
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabNotaCredito> facturaClienteCabNotaCredito;

    // bi-directional many-to-one association to
    // FacturaClienteCabMonedaExtranjera
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjera;

    // bi-directional many-to-one association to
    // FacturaClienteCabRetencion
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabRetencion> facturaClienteCabRetencion;

    // bi-directional many-to-one association to
    // FacturaCabClientePendiente
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaCabClientePendiente> facturaCabClientePendiente;

    // bi-directional many-to-one association to FacturaClienteCabPromoTempArt
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabPromoTempArt> facturaClienteCabPromoTempArt;

    // bi-directional many-to-one association to
    // FacturaClienteCabRetencion
    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
    private List<FacturaClienteCabHistorico> facturaClienteCabHistorico;

    // bi-directional many-to-one association to FacturaClienteCabPromoTempArt
//    @OneToMany(mappedBy = "facturaClienteCab", fetch = FetchType.LAZY)
//    private List<NotaCreditoCabProveedor> notaCreditoCabProveedor;
    public List<FacturaClienteCabPromoTemp> getFacturaClienteCabPromoTemp() {
        return facturaClienteCabPromoTemp;
    }

    public void setFacturaClienteCabPromoTemp(
            List<FacturaClienteCabPromoTemp> facturaClienteCabPromoTemp) {
        this.facturaClienteCabPromoTemp = facturaClienteCabPromoTemp;
    }

    public List<FacturaClienteCabPromoTempArt> getFacturaClienteCabPromoTempArt() {
        return facturaClienteCabPromoTempArt;
    }

    public void setFacturaClienteCabPromoTempArt(List<FacturaClienteCabPromoTempArt> facturaClienteCabPromoTempArt) {
        this.facturaClienteCabPromoTempArt = facturaClienteCabPromoTempArt;
    }

    public List<CancelacionProducto> getCancelacionProducto() {
        return cancelacionProducto;
    }

//    public List<NotaCreditoCabProveedor> getNotaCreditoCabProveedor() {
//        return notaCreditoCabProveedor;
//    }
//
//    public void setNotaCreditoCabProveedor(List<NotaCreditoCabProveedor> notaCreditoCabProveedor) {
//        this.notaCreditoCabProveedor = notaCreditoCabProveedor;
//    }
    public void setCancelacionProducto(
            List<CancelacionProducto> cancelacionProducto) {
        this.cancelacionProducto = cancelacionProducto;
    }

    public FacturaClienteCab() {
    }

    public List<FacturaClienteCabHistorico> getFacturaClienteCabHistorico() {
        return facturaClienteCabHistorico;
    }

    public void setFacturaClienteCabHistorico(List<FacturaClienteCabHistorico> facturaClienteCabHistorico) {
        this.facturaClienteCabHistorico = facturaClienteCabHistorico;
    }

    public Long getIdFacturaClienteCab() {
        return this.idFacturaClienteCab;
    }

    public void setIdFacturaClienteCab(Long idFacturaClienteCab) {
        this.idFacturaClienteCab = idFacturaClienteCab;
    }

    public Boolean getCancelado() {
        return this.cancelado;
    }

    public List<FacturaClienteCabMonedaExtranjera> getFacturaClienteCabMonedaExtranjera() {
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

    public void setCancelado(Boolean cancelado) {
        this.cancelado = cancelado;
    }

    public Timestamp getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Timestamp fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public List<FacturaClienteCabRetencion> getFacturaClienteCabRetencion() {
        return facturaClienteCabRetencion;
    }

    public void setFacturaClienteCabRetencion(
            List<FacturaClienteCabRetencion> facturaClienteCabRetencion) {
        this.facturaClienteCabRetencion = facturaClienteCabRetencion;
    }

    public List<FacturaCabClientePendiente> getFacturaCabClientePendiente() {
        return facturaCabClientePendiente;
    }

    public void setFacturaCabClientePendiente(List<FacturaCabClientePendiente> facturaCabClientePendiente) {
        this.facturaCabClientePendiente = facturaCabClientePendiente;
    }

    public Integer getMontoFactura() {
        return this.montoFactura;
    }

    public List<FacturaClienteCabNotaCredito> getFacturaClienteCabNotaCredito() {
        return facturaClienteCabNotaCredito;
    }

    public void setFacturaClienteCabNotaCredito(
            List<FacturaClienteCabNotaCredito> facturaClienteCabNotaCredito) {
        this.facturaClienteCabNotaCredito = facturaClienteCabNotaCredito;
    }

    public void setMontoFactura(Integer montoFactura) {
        this.montoFactura = montoFactura;
    }

    public List<FacturaClienteCabFuncionario> getFacturaClienteCabFuncionario() {
        return facturaClienteCabFuncionario;
    }

    public void setFacturaClienteCabFuncionario(
            List<FacturaClienteCabFuncionario> facturaClienteCabFuncionario) {
        this.facturaClienteCabFuncionario = facturaClienteCabFuncionario;
    }

    public List<FacturaClienteCabTarjFiel> getFacturaClienteCabTarjFiel() {
        return facturaClienteCabTarjFiel;
    }

    public void setFacturaClienteCabTarjFiel(
            List<FacturaClienteCabTarjFiel> facturaClienteCabTarjFiel) {
        this.facturaClienteCabTarjFiel = facturaClienteCabTarjFiel;
    }

    public String getNroFactura() {
        return this.nroFactura;
    }

    public List<FacturaClienteCabCheque> getFacturaClienteCabCheque() {
        return facturaClienteCabCheque;
    }

    public void setFacturaClienteCabCheque(
            List<FacturaClienteCabCheque> facturaClienteCabCheque) {
        this.facturaClienteCabCheque = facturaClienteCabCheque;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public List<FacturaClienteCabEfectivo> getFacturaClienteCabEfectivo() {
        return facturaClienteCabEfectivo;
    }

    public void setFacturaClienteCabEfectivo(
            List<FacturaClienteCabEfectivo> facturaClienteCabEfectivo) {
        this.facturaClienteCabEfectivo = facturaClienteCabEfectivo;
    }

    public List<DescVale> getDescVales() {
        return this.descVales;
    }

    public void setDescVales(List<DescVale> descVales) {
        this.descVales = descVales;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public DescVale addDescVale(DescVale descVale) {
        getDescVales().add(descVale);
        descVale.setFacturaClienteCab(this);
        return descVale;
    }

    public DescVale removeDescVale(DescVale descVale) {
        getDescVales().remove(descVale);
        descVale.setFacturaClienteCab(null);
        return descVale;
    }

    public List<DonacionCliente> getDonacionClientes() {
        return this.donacionClientes;
    }

    public void setDonacionClientes(List<DonacionCliente> donacionClientes) {
        this.donacionClientes = donacionClientes;
    }

    public DonacionCliente addDonacionCliente(DonacionCliente donacionCliente) {
        getDonacionClientes().add(donacionCliente);
        donacionCliente.setFacturaClienteCab(this);
        return donacionCliente;
    }

    public DonacionCliente removeDonacionCliente(DonacionCliente donacionCliente) {
        getDonacionClientes().remove(donacionCliente);
        donacionCliente.setFacturaClienteCab(null);
        return donacionCliente;
    }

    public EstadoFactura getEstadoFactura() {
        return this.estadoFactura;
    }

    public void setEstadoFactura(EstadoFactura estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public TipoComprobante getTipoComprobante() {
        return this.tipoComprobante;
    }

    public void setTipoComprobante(TipoComprobante tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public TipoMoneda getTipoMoneda() {
        return this.tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public List<FacturaClienteCabTarjConvenio> getFacturaClienteCabTarjConvenios() {
        return this.facturaClienteCabTarjConvenios;
    }

    public void setFacturaClienteCabTarjConvenios(
            List<FacturaClienteCabTarjConvenio> facturaClienteCabTarjConvenios) {
        this.facturaClienteCabTarjConvenios = facturaClienteCabTarjConvenios;
    }

    public FacturaClienteCabTarjConvenio addFacturaClienteCabTarjConvenio(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
        getFacturaClienteCabTarjConvenios().add(facturaClienteCabTarjConvenio);
        facturaClienteCabTarjConvenio.setFacturaClienteCab(this);
        return facturaClienteCabTarjConvenio;
    }

    public FacturaClienteCabTarjConvenio removeFacturaClienteCabTarjConvenio(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
        getFacturaClienteCabTarjConvenios().remove(
                facturaClienteCabTarjConvenio);
        facturaClienteCabTarjConvenio.setFacturaClienteCab(null);
        return facturaClienteCabTarjConvenio;
    }

    public List<FacturaClienteCabTarjeta> getFacturaClienteCabTarjetas() {
        return this.facturaClienteCabTarjetas;
    }

    public void setFacturaClienteCabTarjetas(
            List<FacturaClienteCabTarjeta> facturaClienteCabTarjetas) {
        this.facturaClienteCabTarjetas = facturaClienteCabTarjetas;
    }

    public FacturaClienteCabTarjeta addFacturaClienteCabTarjeta(
            FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
        getFacturaClienteCabTarjetas().add(facturaClienteCabTarjeta);
        facturaClienteCabTarjeta.setFacturaClienteCab(this);
        return facturaClienteCabTarjeta;
    }

    public FacturaClienteCabTarjeta removeFacturaClienteCabTarjeta(
            FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
        getFacturaClienteCabTarjetas().remove(facturaClienteCabTarjeta);
        facturaClienteCabTarjeta.setFacturaClienteCab(null);
        return facturaClienteCabTarjeta;
    }

    public List<FacturaClienteCabTipoPago> getFacturaClienteCabTipoPagos() {
        return this.facturaClienteCabTipoPagos;
    }

    public void setFacturaClienteCabTipoPagos(
            List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos) {
        this.facturaClienteCabTipoPagos = facturaClienteCabTipoPagos;
    }

    public FacturaClienteCabTipoPago addFacturaClienteCabTipoPago(
            FacturaClienteCabTipoPago facturaClienteCabTipoPago) {
        getFacturaClienteCabTipoPagos().add(facturaClienteCabTipoPago);
        facturaClienteCabTipoPago.setFacturaClienteCab(this);
        return facturaClienteCabTipoPago;
    }

    public FacturaClienteCabTipoPago removeFacturaClienteCabTipoPago(
            FacturaClienteCabTipoPago facturaClienteCabTipoPago) {
        getFacturaClienteCabTipoPagos().remove(facturaClienteCabTipoPago);
        facturaClienteCabTipoPago.setFacturaClienteCab(null);
        return facturaClienteCabTipoPago;
    }

    public List<FacturaClienteDet> getFacturaClienteDets() {
        return this.facturaClienteDets;
    }

    public void setFacturaClienteDets(List<FacturaClienteDet> facturaClienteDets) {
        this.facturaClienteDets = facturaClienteDets;
    }

    public FacturaClienteDet addFacturaClienteDet(
            FacturaClienteDet facturaClienteDet) {
        getFacturaClienteDets().add(facturaClienteDet);
        facturaClienteDet.setFacturaClienteCab(this);
        return facturaClienteDet;
    }

    public FacturaClienteDet removeFacturaClienteDet(
            FacturaClienteDet facturaClienteDet) {
        getFacturaClienteDets().remove(facturaClienteDet);
        facturaClienteDet.setFacturaClienteCab(null);
        return facturaClienteDet;
    }

    public List<NotaCreditoCab> getNotaCreditoCabs() {
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCab> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public NotaCreditoCab addNotaCreditoCab(NotaCreditoCab notaCreditoCab) {
        getNotaCreditoCabs().add(notaCreditoCab);
        notaCreditoCab.setFacturaClienteCab(this);
        return notaCreditoCab;
    }

    public NotaCreditoCab removeNotaCreditoCab(NotaCreditoCab notaCreditoCab) {
        getNotaCreditoCabs().remove(notaCreditoCab);
        notaCreditoCab.setFacturaClienteCab(null);
        return notaCreditoCab;
    }

    public List<NotaRemisionCab> getNotaRemisionCabs() {
        return this.notaRemisionCabs;
    }

    public void setNotaRemisionCabs(List<NotaRemisionCab> notaRemisionCabs) {
        this.notaRemisionCabs = notaRemisionCabs;
    }

    public NotaRemisionCab addNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
        getNotaRemisionCabs().add(notaRemisionCab);
        notaRemisionCab.setFacturaClienteCab(this);
        return notaRemisionCab;
    }

    public NotaRemisionCab removeNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
        getNotaRemisionCabs().remove(notaRemisionCab);
        notaRemisionCab.setFacturaClienteCab(null);
        return notaRemisionCab;
    }

    // *****************************************************************************************
    public List<CancelacionFactura> getCancelacionFacturas() {
        return cancelacionFacturas;
    }

    public void setCancelacionFacturas(
            List<CancelacionFactura> cancelacionFacturas) {
        this.cancelacionFacturas = cancelacionFacturas;
    }

    public CancelacionFactura addCancelacionFactura(
            CancelacionFactura cancelacionFactura) {
        getCancelacionFacturas().add(cancelacionFactura);
        cancelacionFactura.setFacturaClienteCab(this);
        return cancelacionFactura;
    }

    public CancelacionFactura removeCancelacionFactura(
            CancelacionFactura cancelacionFactura) {
        getCancelacionFacturas().remove(cancelacionFactura);
        cancelacionFactura.setFacturaClienteCab(null);
        return cancelacionFactura;
    }

    public Caja getCaja() {
        return caja;
    }

    public void setCaja(Caja caja) {
        this.caja = caja;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public FacturaClienteCabDTO toFacturaClienteCabDTO() {
        FacturaClienteCabDTO facDTO = toFacturaClienteCabDTO(this);
        if (this.tipoComprobante != null) {
            facDTO.setTipoComprobante(this.getTipoComprobante()
                    .toTipoComprobanteDTO());
        }
        if (this.cliente != null) {
            facDTO.setCliente(this.cliente.toClienteBDDTO());
        }
        if (this.caja != null) {
            facDTO.setCaja(this.getCaja().toBDCajaDTO());
        }
        if (this.estadoFactura != null) {
            facDTO.setEstadoFactura(this.getEstadoFactura()
                    .toEstadoFacturaDTO());
        }
        if (this.tipoMoneda != null) {
            facDTO.setTipoMoneda(this.getTipoMoneda().toTipoMonedaDTO());
        }
        if (this.sucursal != null) {
            facDTO.setSucursal(this.getSucursal().toSucursalDescriDTO());
        }
        facDTO.setCancelacionFacturas(null);
        facDTO.setDescVales(null);
        facDTO.setDonacionClientes(null);
        facDTO.setFacturaClienteCabTarjConvenios(null);
        facDTO.setFacturaClienteCabTarjetas(null);
        facDTO.setFacturaClienteCabTipoPagos(null);
        facDTO.setFacturaClienteDets(null);
        facDTO.setNotaCreditoCabs(null);
        facDTO.setNotaRemisionCabs(null);
        facDTO.setFacturaClienteCabCheque(null);
        facDTO.setFacturaClienteCabEfectivo(null);
        facDTO.setFacturaClienteCabTarjFiel(null);
        facDTO.setFacturaClienteCabFuncionario(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setCancelacionProducto(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setFacturaClienteCabNotaCredito(null);
        facDTO.setFacturaClienteCabMonedaExtranjera(null);
        facDTO.setFacturaClienteCabRetencion(null);
        facDTO.setFacturaCabClientePendiente(null);
        facDTO.setFacturaClienteCabPromoTempArt(null);
//        facDTO.setNotaCreditoCabProveedor(null);
        return facDTO;
    }

    // Solo lista los basicos como id y descripcion, sin importar los asociados
    public FacturaClienteCabDTO toBDFacturaClienteCabDTO() {
        FacturaClienteCabDTO facDTO = toFacturaClienteCabDTO(this);
        facDTO.setTipoComprobante(null);
        facDTO.setCliente(null);
        facDTO.setCaja(null);
        facDTO.setEstadoFactura(null);
        facDTO.setTipoMoneda(null);
        facDTO.setSucursal(null);
        facDTO.setCancelacionFacturas(null);
        facDTO.setDescVales(null);
        facDTO.setDonacionClientes(null);
        facDTO.setFacturaClienteCabTarjConvenios(null);
        facDTO.setFacturaClienteCabTarjetas(null);
        facDTO.setFacturaClienteCabTipoPagos(null);
        facDTO.setFacturaClienteDets(null);
        facDTO.setNotaCreditoCabs(null);
        facDTO.setNotaRemisionCabs(null);
        facDTO.setFacturaClienteCabCheque(null);
        facDTO.setFacturaClienteCabEfectivo(null);
        facDTO.setFacturaClienteCabTarjFiel(null);
        facDTO.setFacturaClienteCabFuncionario(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setCancelacionProducto(null);
        facDTO.setFacturaClienteCabNotaCredito(null);
        facDTO.setFacturaClienteCabMonedaExtranjera(null);
        facDTO.setFacturaClienteCabRetencion(null);
        facDTO.setFacturaCabClientePendiente(null);
        facDTO.setFacturaClienteCabPromoTempArt(null);
//        facDTO.setNotaCreditoCabProveedor(null);
        return facDTO;
    }

    public FacturaClienteCabDTO toBDHIstoricoCajaDetalleDTO() {
        FacturaClienteCabDTO facDTO = toFacturaClienteCabDTO(this);
        facDTO.setTipoComprobante(null);
        facDTO.setCliente(null);
        if (this.caja != null) {
            facDTO.setCaja(this.getCaja().toBDCajaDTO());
        } else {
            facDTO.setCaja(null);
        }
        facDTO.setEstadoFactura(null);
        facDTO.setTipoMoneda(null);
        facDTO.setSucursal(null);
        facDTO.setCancelacionFacturas(null);
        facDTO.setDescVales(null);
        facDTO.setDonacionClientes(null);
        facDTO.setFacturaClienteCabTarjConvenios(null);
        facDTO.setFacturaClienteCabTarjetas(null);
        facDTO.setFacturaClienteCabTipoPagos(null);
        if (!this.facturaClienteDets.isEmpty()) {
            List<FacturaClienteDetDTO> listDetail = new ArrayList<FacturaClienteDetDTO>();
            for (FacturaClienteDet factDetalle : this.getFacturaClienteDets()) {
                listDetail.add(factDetalle.toDescriFacturaClienteDetDTO());
            }
            facDTO.setFacturaClienteDets(listDetail);
        }
        facDTO.setNotaCreditoCabs(null);
        facDTO.setNotaRemisionCabs(null);
        facDTO.setFacturaClienteCabCheque(null);
        facDTO.setFacturaClienteCabEfectivo(null);
        facDTO.setFacturaClienteCabTarjFiel(null);
        facDTO.setFacturaClienteCabFuncionario(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setCancelacionProducto(null);
        facDTO.setFacturaClienteCabNotaCredito(null);
        facDTO.setFacturaClienteCabMonedaExtranjera(null);
        facDTO.setFacturaClienteCabRetencion(null);
        List<FacturaClienteCabHistoricoDTO> listFacturaClienteCabHistoricoDTO = new ArrayList<>();
        if (!this.facturaClienteCabHistorico.isEmpty()) {
            for (FacturaClienteCabHistorico facHisto : this
                    .getFacturaClienteCabHistorico()) {
                listFacturaClienteCabHistoricoDTO.add(facHisto
                        .toFacturaClienteCabDTO());
            }
        }
        facDTO.setFacturaClienteCabHistorico(listFacturaClienteCabHistoricoDTO);
        facDTO.setFacturaCabClientePendiente(null);
        facDTO.setFacturaClienteCabPromoTempArt(null);
        return facDTO;
    }
    

    public FacturaClienteCabDTO toFacturaClienteCabAndDetalleDTO() {
        FacturaClienteCabDTO facDTO = toFacturaClienteCabDTO(this);
        if (this.tipoComprobante != null) {
            facDTO.setTipoComprobante(this.getTipoComprobante()
                    .toTipoComprobanteDTO());
        }
        if (this.cliente != null) {
            facDTO.setCliente(this.cliente.toClienteBDDTO());
        }
        if (this.caja != null) {
            facDTO.setCaja(this.getCaja().toBDCajaDTO());
        }
        if (this.estadoFactura != null) {
            facDTO.setEstadoFactura(this.getEstadoFactura()
                    .toEstadoFacturaDTO());
        }
        if (this.tipoMoneda != null) {
            facDTO.setTipoMoneda(this.getTipoMoneda().toTipoMonedaDTO());
        }
        if (this.sucursal != null) {
            facDTO.setSucursal(this.getSucursal().toSucursalDescriDTO());
        }
        facDTO.setCancelacionFacturas(null);
        facDTO.setDescVales(null);
        facDTO.setDonacionClientes(null);
        facDTO.setFacturaClienteCabTarjConvenios(null);
        facDTO.setFacturaClienteCabTarjetas(null);
        facDTO.setFacturaClienteCabTipoPagos(null);
        // facDTO.setFacturaClienteDets(null);

        if (!this.facturaClienteDets.isEmpty()) {
            List<FacturaClienteDetDTO> listDetail = new ArrayList<FacturaClienteDetDTO>();
            for (FacturaClienteDet factDetalle : this.getFacturaClienteDets()) {
                listDetail.add(factDetalle.toDescriFacturaClienteDetDTO());
            }
            facDTO.setFacturaClienteDets(listDetail);
        }

        facDTO.setNotaCreditoCabs(null);
        facDTO.setNotaRemisionCabs(null);
        facDTO.setFacturaClienteCabCheque(null);
        facDTO.setFacturaClienteCabEfectivo(null);
        facDTO.setFacturaClienteCabTarjFiel(null);
        facDTO.setFacturaClienteCabFuncionario(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setCancelacionProducto(null);
        facDTO.setFacturaClienteCabPromoTemp(null);
        facDTO.setFacturaClienteCabNotaCredito(null);
        facDTO.setFacturaClienteCabMonedaExtranjera(null);
        facDTO.setFacturaClienteCabRetencion(null);
        facDTO.setFacturaClienteCabHistorico(null);
        facDTO.setFacturaCabClientePendiente(null);
        facDTO.setFacturaClienteCabPromoTempArt(null);
        return facDTO;
    }

    public FacturaClienteCabDTO toDescuentoFacturaClienteCabDTO() {
        FacturaClienteCabDTO facDTO = toFacturaClienteCabDTO(this);
        facDTO.setTipoComprobante(null);
        facDTO.setCliente(null);
        if (this.caja != null) {
            facDTO.setCaja(this.getCaja().toBDCajaDTO());
        } else {
            facDTO.setCaja(null);
        }
        facDTO.setEstadoFactura(null);
        facDTO.setTipoMoneda(null);
        facDTO.setSucursal(null);
        facDTO.setCancelacionFacturas(null);
        facDTO.setDescVales(null);
        facDTO.setDonacionClientes(null);
        facDTO.setFacturaClienteCabTarjConvenios(null);

        List<FacturaClienteCabTarjetaDTO> listFacturaClienteCabTarjetaDTO = new ArrayList<>();
        if (!this.facturaClienteCabTarjetas.isEmpty()) {
            for (FacturaClienteCabTarjeta facTarj : this
                    .getFacturaClienteCabTarjetas()) {
                listFacturaClienteCabTarjetaDTO.add(facTarj.toDescTarjetaDTO());
            }
        }
        facDTO.setFacturaClienteCabTarjetas(listFacturaClienteCabTarjetaDTO);

        facDTO.setFacturaClienteCabTarjetas(null);
        facDTO.setFacturaClienteCabTipoPagos(null);
        facDTO.setFacturaClienteDets(null);
        facDTO.setNotaCreditoCabs(null);
        facDTO.setNotaRemisionCabs(null);
        facDTO.setFacturaClienteCabCheque(null);
        facDTO.setFacturaClienteCabEfectivo(null);
        List<FacturaClienteCabTarjFielDTO> listFactFielDTO = new ArrayList<>();
        if (!this.facturaClienteCabTarjFiel.isEmpty()) {
            for (FacturaClienteCabTarjFiel facFiel : this
                    .getFacturaClienteCabTarjFiel()) {
                listFactFielDTO.add(facFiel.toDescTarjFielDTO());
            }
        }
        facDTO.setFacturaClienteCabTarjFiel(listFactFielDTO);

        List<FacturaClienteCabFuncionarioDTO> listFuncDTO = new ArrayList<>();
        if (!this.facturaClienteCabFuncionario.isEmpty()) {
            for (FacturaClienteCabFuncionario facFunc : this
                    .getFacturaClienteCabFuncionario()) {
                listFuncDTO.add(facFunc.toFactCliFuncDTO());
            }
        }
        facDTO.setFacturaClienteCabFuncionario(listFuncDTO);

        List<FacturaClienteCabPromoTempDTO> listPromoTempDTO = new ArrayList<>();
        if (!this.facturaClienteCabPromoTemp.isEmpty()) {
            for (FacturaClienteCabPromoTemp facPromo : this
                    .getFacturaClienteCabPromoTemp()) {
                listPromoTempDTO.add(facPromo
                        .toSinCabFacturaClienteCabPromoTempDTO());
            }
        }
        facDTO.setFacturaClienteCabPromoTemp(listPromoTempDTO);

        List<FacturaClienteCabPromoTempArtDTO> listPromoTempArtDTO = new ArrayList<>();
        if (!this.facturaClienteCabPromoTempArt.isEmpty()) {
            for (FacturaClienteCabPromoTempArt facPromo : this
                    .getFacturaClienteCabPromoTempArt()) {
                listPromoTempArtDTO.add(facPromo
                        .toSinCabBDFacturaClienteCabPromoTempArtDTO());
            }
        }
        facDTO.setFacturaClienteCabPromoTempArt(listPromoTempArtDTO);

        facDTO.setCancelacionProducto(null);
        facDTO.setFacturaClienteCabNotaCredito(null);
        facDTO.setFacturaClienteCabMonedaExtranjera(null);
        facDTO.setFacturaClienteCabRetencion(null);

        List<FacturaClienteCabHistoricoDTO> listFacturaClienteCabHistoricoDTO = new ArrayList<>();
        if (!this.facturaClienteCabHistorico.isEmpty()) {
            for (FacturaClienteCabHistorico facHisto : this
                    .getFacturaClienteCabHistorico()) {
                listFacturaClienteCabHistoricoDTO.add(facHisto
                        .toFacturaClienteCabDTO());
            }
        }
        facDTO.setFacturaClienteCabHistorico(listFacturaClienteCabHistoricoDTO);
        facDTO.setFacturaCabClientePendiente(null);

        return facDTO;
    }

    public static FacturaClienteCabDTO toFacturaClienteCabDTO(
            FacturaClienteCab facturaClienteCab) {
        FacturaClienteCabDTO facDTO = new FacturaClienteCabDTO();
        BeanUtils.copyProperties(facturaClienteCab, facDTO);
        return facDTO;
    }

    public static FacturaClienteCab fromFacturaClienteCabDTO(
            FacturaClienteCabDTO facDTO) {
        FacturaClienteCab fac = new FacturaClienteCab();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCab fromFacturaClienteCabAsociado(
            FacturaClienteCabDTO facDTO) {
        FacturaClienteCab fac = fromFacturaClienteCabDTO(facDTO);

        fac.setTipoComprobante(TipoComprobante.fromTipoComprobanteDTO(facDTO
                .getTipoComprobante()));
        fac.setCliente(Cliente.fromClienteDTO(facDTO.getCliente()));
        fac.setCaja(Caja.fromCajaDTO(facDTO.getCaja()));
        fac.setEstadoFactura(EstadoFactura.fromEstadoFacturaDTO(facDTO
                .getEstadoFactura()));
        fac.setTipoMoneda(TipoMoneda.fromTipoMonedaDTO(facDTO.getTipoMoneda()));
        fac.setSucursal(Sucursal.fromSucursalDTO(facDTO.getSucursal()));
        return fac;
    }

    public static FacturaClienteCab fromEstadoAndSucursal(
            FacturaClienteCabDTO facDTO) {
        FacturaClienteCab fac = fromFacturaClienteCabDTO(facDTO);
        fac.setEstadoFactura(EstadoFactura.fromEstadoFacturaDTO(facDTO
                .getEstadoFactura()));
        fac.setSucursal(Sucursal.fromSucursalDTO(facDTO.getSucursal()));
        return fac;
    }

}
