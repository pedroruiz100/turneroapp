package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoDirectivoDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the donacion_cliente database table.
 *
 */
@Entity
@Table(name = "descuento_directivo", schema = "factura_cliente")
@NamedQuery(name = "DescuentoDirectivo.findAll", query = "SELECT dd FROM DescuentoDirectivo dd")
public class DescuentoDirectivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_directivo")
    private Long idDescuentoDirectivo;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "ci_dir")
    private String ciDir;

    @Column(name = "nombre_dir")
    private String nombreDir;

    @Column(name = "motivo_desc")
    private String motivoDesc;

    public DescuentoDirectivo() {
    }

    public Long getIdDescuentoDirectivo() {
        return idDescuentoDirectivo;
    }

    public void setIdDescuentoDirectivo(Long idDescuentoDirectivo) {
        this.idDescuentoDirectivo = idDescuentoDirectivo;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public BigDecimal getPorcDesc() {
        return porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Integer getMontoDesc() {
        return montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public String getCiDir() {
        return ciDir;
    }

    public void setCiDir(String ciDir) {
        this.ciDir = ciDir;
    }

    public String getNombreDir() {
        return nombreDir;
    }

    public void setNombreDir(String nombreDir) {
        this.nombreDir = nombreDir;
    }

    public String getMotivoDesc() {
        return motivoDesc;
    }

    public void setMotivoDesc(String motivoDesc) {
        this.motivoDesc = motivoDesc;
    }

    public DescuentoDirectivoDTO toDescuentoDirectivoDTO() {
        DescuentoDirectivoDTO facDTO = toDescuentoDirectivoDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    public DescuentoDirectivoDTO toDescriDescuentoDirectivoDTO() {
        DescuentoDirectivoDTO facDTO = toDescuentoDirectivoDTO(this);
        facDTO.setFacturaClienteCab(null);
        return facDTO;
    }

    public static DescuentoDirectivoDTO toDescuentoDirectivoDTO(
            DescuentoDirectivo DescuentoDirectivo) {
        DescuentoDirectivoDTO facDTO = new DescuentoDirectivoDTO();
        BeanUtils.copyProperties(DescuentoDirectivo, facDTO);
        return facDTO;
    }

    public static DescuentoDirectivo fromDescuentoDirectivoDTO(
            DescuentoDirectivoDTO facDTO) {
        DescuentoDirectivo fac = new DescuentoDirectivo();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static DescuentoDirectivo fromDescuentoDirectivoAsociadoDTO(
            DescuentoDirectivoDTO facDTO) {
        DescuentoDirectivo fac = fromDescuentoDirectivoDTO(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}
