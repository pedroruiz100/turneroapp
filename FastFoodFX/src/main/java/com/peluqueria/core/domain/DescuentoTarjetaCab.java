package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaCabArticuloDTO;
import com.peluqueria.dto.DescuentoTarjetaCabDTO;
import com.peluqueria.dto.DescuentoTarjetaCabEntidadDTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf1DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf2DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf3DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf4DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf5DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf6DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf7DTO;
import com.peluqueria.dto.DescuentoTarjetaDetDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.*;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_tarjeta_cab database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab", schema = "cuenta")
@NamedQuery(name = "DescuentoTarjetaCab.findAll", query = "SELECT d FROM DescuentoTarjetaCab d")
public class DescuentoTarjetaCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab")
    private Long idDescuentoTarjetaCab;

    // bi-directional many-to-one association to Tarjeta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarjeta")
    private Tarjeta tarjeta;

    @Column(name = "descri_tarjeta")
    private String descriTarjeta;

    @Column(name = "fecha_inicio")
    private Timestamp fechaInicio;

    @Column(name = "fecha_fin")
    private Timestamp fechaFin;

    @Column(name = "hora_inicio")
    private Time horaInicio;

    @Column(name = "hora_fin")
    private Time horaFin;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    @Column(name = "extracto")
    private Boolean extracto;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porcentaje_parana")
    private BigDecimal porcentajeParana;

    @Column(name = "monto_min")
    private Integer montoMin;

    // bi-directional one-to-many association to DescuentoTarjetaDet
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaDet> descuentoTarjetaDets;

    // bi-directional one-to-many association to DescuentoTarjetaCabEntidad
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabEntidad> descuentoTarjetaCabEntidads;

    // bi-directional one-to-many association to DescuentoTarjetaCabArticulo
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabArticulo> descuentoTarjetaCabArticulos;

    // bi-directional one-to-many association to DescTarjeta
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescTarjeta> descTarjetas;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf1
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf1> descuentoTarjetaCabNf1s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf2
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf2> descuentoTarjetaCabNf2s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf3
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf3> descuentoTarjetaCabNf3s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf4
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf4> descuentoTarjetaCabNf4s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf5
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf5> descuentoTarjetaCabNf5s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf6
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf6> descuentoTarjetaCabNf6s;

    // bi-directional one-to-many association to DescuentoTarjetaCabNf7
    @OneToMany(mappedBy = "descuentoTarjetaCab", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabNf7> descuentoTarjetaCabNf7s;

    public DescuentoTarjetaCab() {
    }

    public Long getIdDescuentoTarjetaCab() {
        return this.idDescuentoTarjetaCab;
    }

    public void setIdDescuentoTarjetaCab(Long idDescuentoTarjetaCab) {
        this.idDescuentoTarjetaCab = idDescuentoTarjetaCab;
    }

    public String getDescriTarjeta() {
        return this.descriTarjeta;
    }

    public void setDescriTarjeta(String descriTarjeta) {
        this.descriTarjeta = descriTarjeta;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public BigDecimal getPorcentajeDesc() {
        return porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public BigDecimal getPorcentajeParana() {
        return porcentajeParana;
    }

    public void setPorcentajeParana(BigDecimal porcentajeParana) {
        this.porcentajeParana = porcentajeParana;
    }

    public Integer getMontoMin() {
        return montoMin;
    }

    public void setMontoMin(Integer montoMin) {
        this.montoMin = montoMin;
    }

    public Boolean getExtracto() {
        return extracto;
    }

    public void setExtracto(Boolean extracto) {
        this.extracto = extracto;
    }

    public Timestamp getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Time getHoraFin() {
        return this.horaFin;
    }

    public void setHoraFin(Time horaFin) {
        this.horaFin = horaFin;
    }

    public Time getHoraInicio() {
        return this.horaInicio;
    }

    public void setHoraInicio(Time horaInicio) {
        this.horaInicio = horaInicio;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DescuentoTarjetaDet> getDescuentoTarjetaDets() {
        return this.descuentoTarjetaDets;
    }

    public void setDescuentoTarjetaDets(
            List<DescuentoTarjetaDet> descuentoTarjetaDets) {
        this.descuentoTarjetaDets = descuentoTarjetaDets;
    }

    public List<DescuentoTarjetaCabEntidad> getDescuentoTarjetaCabEntidads() {
        return descuentoTarjetaCabEntidads;
    }

    public void setDescuentoTarjetaCabEntidads(
            List<DescuentoTarjetaCabEntidad> descuentoTarjetaCabEntidads) {
        this.descuentoTarjetaCabEntidads = descuentoTarjetaCabEntidads;
    }

    public List<DescuentoTarjetaCabArticulo> getDescuentoTarjetaCabArticulos() {
        return descuentoTarjetaCabArticulos;
    }

    public void setDescuentoTarjetaCabArticulos(
            List<DescuentoTarjetaCabArticulo> descuentoTarjetaCabArticulos) {
        this.descuentoTarjetaCabArticulos = descuentoTarjetaCabArticulos;
    }

    public List<DescTarjeta> getDescTarjetas() {
        return descTarjetas;
    }

    public void setDescTarjetas(List<DescTarjeta> descTarjetas) {
        this.descTarjetas = descTarjetas;
    }

    public List<DescuentoTarjetaCabNf1> getDescuentoTarjetaCabNf1s() {
        return descuentoTarjetaCabNf1s;
    }

    public void setDescuentoTarjetaCabNf1s(List<DescuentoTarjetaCabNf1> descuentoTarjetaCabNf1s) {
        this.descuentoTarjetaCabNf1s = descuentoTarjetaCabNf1s;
    }

    public List<DescuentoTarjetaCabNf2> getDescuentoTarjetaCabNf2s() {
        return descuentoTarjetaCabNf2s;
    }

    public void setDescuentoTarjetaCabNf2s(List<DescuentoTarjetaCabNf2> descuentoTarjetaCabNf2s) {
        this.descuentoTarjetaCabNf2s = descuentoTarjetaCabNf2s;
    }

    public List<DescuentoTarjetaCabNf3> getDescuentoTarjetaCabNf3s() {
        return descuentoTarjetaCabNf3s;
    }

    public void setDescuentoTarjetaCabNf3s(List<DescuentoTarjetaCabNf3> descuentoTarjetaCabNf3s) {
        this.descuentoTarjetaCabNf3s = descuentoTarjetaCabNf3s;
    }

    public List<DescuentoTarjetaCabNf4> getDescuentoTarjetaCabNf4s() {
        return descuentoTarjetaCabNf4s;
    }

    public void setDescuentoTarjetaCabNf4s(List<DescuentoTarjetaCabNf4> descuentoTarjetaCabNf4s) {
        this.descuentoTarjetaCabNf4s = descuentoTarjetaCabNf4s;
    }

    public List<DescuentoTarjetaCabNf5> getDescuentoTarjetaCabNf5s() {
        return descuentoTarjetaCabNf5s;
    }

    public void setDescuentoTarjetaCabNf5s(List<DescuentoTarjetaCabNf5> descuentoTarjetaCabNf5s) {
        this.descuentoTarjetaCabNf5s = descuentoTarjetaCabNf5s;
    }

    public List<DescuentoTarjetaCabNf6> getDescuentoTarjetaCabNf6s() {
        return descuentoTarjetaCabNf6s;
    }

    public void setDescuentoTarjetaCabNf6s(List<DescuentoTarjetaCabNf6> descuentoTarjetaCabNf6s) {
        this.descuentoTarjetaCabNf6s = descuentoTarjetaCabNf6s;
    }

    public List<DescuentoTarjetaCabNf7> getDescuentoTarjetaCabNf7s() {
        return descuentoTarjetaCabNf7s;
    }

    public void setDescuentoTarjetaCabNf7s(List<DescuentoTarjetaCabNf7> descuentoTarjetaCabNf7s) {
        this.descuentoTarjetaCabNf7s = descuentoTarjetaCabNf7s;
    }

    public DescuentoTarjetaCabDTO toDescuentoTarjetaCabDTO() {
        DescuentoTarjetaCabDTO descuentoTarjetaCabDTO = toDescuentoTarjetaCabDTO(this);
        if (this.tarjeta != null) {
            Tarjeta t = this.getTarjeta();
            descuentoTarjetaCabDTO.setTarjeta(t.toTarjetaDTO());
        }
        if (!this.descuentoTarjetaDets.isEmpty()) {
            List<DescuentoTarjetaDet> descDet = this.getDescuentoTarjetaDets();
            List<DescuentoTarjetaDetDTO> descDetDTO = new ArrayList<>();
            for (DescuentoTarjetaDet dDet : descDet) {
                descDetDTO.add(dDet.toDescuentoTarjetaDetDTO());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaDets(descDetDTO);
        }
        if (!this.descuentoTarjetaCabArticulos.isEmpty()) {
            List<DescuentoTarjetaCabArticulo> descuentoTarjetaCabArts = this.getDescuentoTarjetaCabArticulos();
            List<DescuentoTarjetaCabArticuloDTO> descuentoTarjetaCabArticuloDTO = new ArrayList<>();
            for (DescuentoTarjetaCabArticulo descuentoTarjetaCabArticulo : descuentoTarjetaCabArts) {
                descuentoTarjetaCabArticuloDTO.add(descuentoTarjetaCabArticulo.toDescuentoTarjetaCabArticuloDTO());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabArticulos(descuentoTarjetaCabArticuloDTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabArticulos(null);
        }
        if (!this.descuentoTarjetaCabEntidads.isEmpty()) {
            List<DescuentoTarjetaCabEntidad> descuentoTarjetaCabEnts = this.getDescuentoTarjetaCabEntidads();
            List<DescuentoTarjetaCabEntidadDTO> descuentoTarjetaCabEntidadDTO = new ArrayList<>();
            for (DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad : descuentoTarjetaCabEnts) {
                descuentoTarjetaCabEntidadDTO.add(descuentoTarjetaCabEntidad.toDescuentoTarjetaCabEntidadDTO());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabEntidads(descuentoTarjetaCabEntidadDTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabEntidads(null);
        }
        if (!this.descuentoTarjetaCabNf1s.isEmpty()) {
            List<DescuentoTarjetaCabNf1> descuentoTarjetaCabNf1s = this.getDescuentoTarjetaCabNf1s();
            List<DescuentoTarjetaCabNf1DTO> descuentoTarjetaCabNf1DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1 : descuentoTarjetaCabNf1s) {
                descuentoTarjetaCabNf1DTO.add(descuentoTarjetaCabNf1.toDescuentoTarjetaCabNf1DTO());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf1s(descuentoTarjetaCabNf1DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf1s(null);
        }
        if (!this.descuentoTarjetaCabNf2s.isEmpty()) {
            List<DescuentoTarjetaCabNf2> descuentoTarjetaCabNf2s = this.getDescuentoTarjetaCabNf2s();
            List<DescuentoTarjetaCabNf2DTO> descuentoTarjetaCabNf2DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf2 descuentoTarjetaCabNf2 : descuentoTarjetaCabNf2s) {
                descuentoTarjetaCabNf2DTO.add(descuentoTarjetaCabNf2.toDescuentoTarjetaCabNf2DTONf1());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf2s(descuentoTarjetaCabNf2DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf2s(null);
        }
        if (!this.descuentoTarjetaCabNf3s.isEmpty()) {
            List<DescuentoTarjetaCabNf3> descuentoTarjetaCabNf3s = this.getDescuentoTarjetaCabNf3s();
            List<DescuentoTarjetaCabNf3DTO> descuentoTarjetaCabNf3DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf3 descuentoTarjetaCabNf3 : descuentoTarjetaCabNf3s) {
                descuentoTarjetaCabNf3DTO.add(descuentoTarjetaCabNf3.toDescuentoTarjetaCabNf3DTONf2());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf3s(descuentoTarjetaCabNf3DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf3s(null);
        }
        if (!this.descuentoTarjetaCabNf4s.isEmpty()) {
            List<DescuentoTarjetaCabNf4> descuentoTarjetaCabNf4s = this.getDescuentoTarjetaCabNf4s();
            List<DescuentoTarjetaCabNf4DTO> descuentoTarjetaCabNf4DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4 : descuentoTarjetaCabNf4s) {
                descuentoTarjetaCabNf4DTO.add(descuentoTarjetaCabNf4.toDescuentoTarjetaCabNf4DTONf3());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf4s(descuentoTarjetaCabNf4DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf4s(null);
        }
        if (!this.descuentoTarjetaCabNf5s.isEmpty()) {
            List<DescuentoTarjetaCabNf5> descuentoTarjetaCabNf5s = this.getDescuentoTarjetaCabNf5s();
            List<DescuentoTarjetaCabNf5DTO> descuentoTarjetaCabNf5DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5 : descuentoTarjetaCabNf5s) {
                descuentoTarjetaCabNf5DTO.add(descuentoTarjetaCabNf5.toDescuentoTarjetaCabNf5DTONf4());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf5s(descuentoTarjetaCabNf5DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf5s(null);
        }
        if (!this.descuentoTarjetaCabNf6s.isEmpty()) {
            List<DescuentoTarjetaCabNf6> descuentoTarjetaCabNf6s = this.getDescuentoTarjetaCabNf6s();
            List<DescuentoTarjetaCabNf6DTO> descuentoTarjetaCabNf6DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6 : descuentoTarjetaCabNf6s) {
                descuentoTarjetaCabNf6DTO.add(descuentoTarjetaCabNf6.toDescuentoTarjetaCabNf6DTONf5());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf6s(descuentoTarjetaCabNf6DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf6s(null);
        }
        if (!this.descuentoTarjetaCabNf7s.isEmpty()) {
            List<DescuentoTarjetaCabNf7> descuentoTarjetaCabNf7s = this.getDescuentoTarjetaCabNf7s();
            List<DescuentoTarjetaCabNf7DTO> descuentoTarjetaCabNf7DTO = new ArrayList<>();
            for (DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7 : descuentoTarjetaCabNf7s) {
                descuentoTarjetaCabNf7DTO.add(descuentoTarjetaCabNf7.toDescuentoTarjetaCabNf7DTONf6());
            }
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf7s(descuentoTarjetaCabNf7DTO);
        } else {
            descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf7s(null);
        }
        descuentoTarjetaCabDTO.setDescTarjetas(null);
        return descuentoTarjetaCabDTO;
    }

    public DescuentoTarjetaCabDTO toBDDescuentoTarjetaCabDTO() {
        DescuentoTarjetaCabDTO descDTO = toDescuentoTarjetaCabDTO(this);
        if (this.tarjeta != null) {
            descDTO.setTarjeta(this.getTarjeta().toTarjetaDescriDTO());
        }
        descDTO.setDescuentoTarjetaDets(null);
        return descDTO;
    }

    public DescuentoTarjetaCabDTO toDescTarjetaCabDTO() {
        DescuentoTarjetaCabDTO descDTO = toDescuentoTarjetaCabDTO(this);
        descDTO.setTarjeta(null);
        descDTO.setDescuentoTarjetaDets(null);
        descDTO.setDescTarjetas(null);
        descDTO.setDescuentoTarjetaCabEntidads(null);
        descDTO.setDescuentoTarjetaCabArticulos(null);
        descDTO.setDescuentoTarjetaCabNf1s(null);
        descDTO.setDescuentoTarjetaCabNf2s(null);
        descDTO.setDescuentoTarjetaCabNf3s(null);
        descDTO.setDescuentoTarjetaCabNf4s(null);
        descDTO.setDescuentoTarjetaCabNf5s(null);
        descDTO.setDescuentoTarjetaCabNf6s(null);
        descDTO.setDescuentoTarjetaCabNf7s(null);
        return descDTO;
    }

    public static DescuentoTarjetaCabDTO toDescuentoTarjetaCabDTO(
            DescuentoTarjetaCab dtc) {
        DescuentoTarjetaCabDTO dDTO = new DescuentoTarjetaCabDTO();
        BeanUtils.copyProperties(dtc, dDTO);
        return dDTO;
    }

    public static DescuentoTarjetaCab fromDescuentoTarjetaCabDTO(
            DescuentoTarjetaCabDTO dtcDTO) {
        DescuentoTarjetaCab dCab = new DescuentoTarjetaCab();
        BeanUtils.copyProperties(dtcDTO, dCab);
        dCab.setTarjeta(Tarjeta.fromTarjetaDTO(dtcDTO.getTarjeta()));
        return dCab;
    }

    // Sin la tarjeta
    public static DescuentoTarjetaCab fromDescTarSinTarjetaCabDTO(
            DescuentoTarjetaCabDTO dtcDTO) {
        DescuentoTarjetaCab dCab = new DescuentoTarjetaCab();
        BeanUtils.copyProperties(dtcDTO, dCab);
        return dCab;
    }

    // <-- SERVER
    public DescuentoTarjetaCabDTO toDescuentoTarjetaCabDTOEntitiesNull() {
        DescuentoTarjetaCabDTO descuentoTarjetaCabDTO = toDescuentoTarjetaCabDTO(this);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabArticulos(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabEntidads(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaDets(null);
        descuentoTarjetaCabDTO.setDescTarjetas(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf1s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf2s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf3s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf4s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf5s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf6s(null);
        descuentoTarjetaCabDTO.setDescuentoTarjetaCabNf7s(null);
        return descuentoTarjetaCabDTO;
    }

}
