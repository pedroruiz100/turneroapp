package com.peluqueria.core.domain;

import com.peluqueria.dto.TarjetaDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tarjeta database table.
 *
 */
@Entity
@Table(name = "tarjeta", schema = "cuenta")
@NamedQuery(name = "Tarjeta.findAll", query = "SELECT t FROM Tarjeta t")
public class Tarjeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tarjeta")
    private Long idTarjeta;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "habilitado")
    private Boolean habilitado;

    @Column(name = "codtar")
    private Integer codtar;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FamiliaTarj
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_familia_tarj")
    private FamiliaTarj familiaTarj;

    // bi-directional many-to-one association to TipoTarjeta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_tarjeta")
    private TipoTarjeta tipoTarjeta;

    @OneToMany(mappedBy = "tarjeta", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCab> descuentoTarjetaCab;
//
//    // bi-directional many-to-one association to DescuentoTarjeta
    @OneToMany(mappedBy = "tarjeta", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTarjeta> facturaClienteCabTarjetas;

    public Tarjeta() {
    }

    public Long getIdTarjeta() {
        return this.idTarjeta;
    }

    public void setIdTarjeta(Long idTarjeta) {
        this.idTarjeta = idTarjeta;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public List<DescuentoTarjetaCab> getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(
            List<DescuentoTarjetaCab> descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Integer getCodtar() {
        return codtar;
    }

    public void setCodtar(Integer codtar) {
        this.codtar = codtar;
    }

    public FamiliaTarj getFamiliaTarj() {
        return this.familiaTarj;
    }

    public void setFamiliaTarj(FamiliaTarj familiaTarj) {
        this.familiaTarj = familiaTarj;
    }

    public TipoTarjeta getTipoTarjeta() {
        return this.tipoTarjeta;
    }

    public void setTipoTarjeta(TipoTarjeta tipoTarjeta) {
        this.tipoTarjeta = tipoTarjeta;
    }

    public List<FacturaClienteCabTarjeta> getFacturaClienteCabTarjetas() {
        return facturaClienteCabTarjetas;
    }

    public void setFacturaClienteCabTarjetas(
            List<FacturaClienteCabTarjeta> facturaClienteCabTarjetas) {
        this.facturaClienteCabTarjetas = facturaClienteCabTarjetas;
    }

    public TarjetaDTO toTarjetaDTO() {
        TarjetaDTO tarDTO = toTarjetaDTO(this);
        if (this.tipoTarjeta != null) {
            TipoTarjeta tp = this.getTipoTarjeta();
            tarDTO.setTipoTarjeta(tp.toTipoTarjetaDTO());
        }
        tarDTO.setFamiliaTarj(null);
        tarDTO.setDescuentoTarjetaCab(null);
        tarDTO.setFacturaClienteCabTarjetaDTO(null);
        return tarDTO;
    }

    public TarjetaDTO toBDDescriTarjetaDTO() {
        TarjetaDTO tarDTO = toTarjetaDTO(this);
        if (this.tipoTarjeta != null) {
            TipoTarjeta tp = this.getTipoTarjeta();
            tarDTO.setTipoTarjeta(tp.toTipoTarjetaDTO());
        }
        if (this.familiaTarj != null) {
            tarDTO.setFamiliaTarj(this.getFamiliaTarj().toBDFamiliaTarjDTO());
        }
        tarDTO.setDescuentoTarjetaCab(null);
        tarDTO.setFacturaClienteCabTarjetaDTO(null);
        return tarDTO;
    }

    // solo descripcion para utilizar en el combo descuento tarjeta debito
    public TarjetaDTO toTarjetaDescriDTO() {
        TarjetaDTO tarDTO = toTarjetaDTO(this);
        tarDTO.setTipoTarjeta(null);
        tarDTO.setFamiliaTarj(null);
        tarDTO.setDescuentoTarjetaCab(null);
        tarDTO.setFacturaClienteCabTarjetaDTO(null);
        return tarDTO;
    }

    public TarjetaDTO toTarjetaDTO(Tarjeta tar) {
        TarjetaDTO tarDTO = new TarjetaDTO();
        BeanUtils.copyProperties(tar, tarDTO);
        return tarDTO;
    }

    // sin familia ni tipo de tarjeta solo para el uso de Descuento Tarjeta Cab
    public static Tarjeta fromTarjetaDTO(TarjetaDTO tarDTO) {
        Tarjeta tar = new Tarjeta();
        BeanUtils.copyProperties(tarDTO, tar);
        return tar;
    }

    public static Tarjeta fromTarjetaAsociadoDTO(TarjetaDTO tarDTO) {
        Tarjeta tar = fromTarjetaDTO(tarDTO);
        tar.setTipoTarjeta(TipoTarjeta.fromTipoTarjetaDTO(tarDTO
                .getTipoTarjeta()));
        tar.setFamiliaTarj(FamiliaTarj.fromFamiliaTarjDTO(tarDTO
                .getFamiliaTarj()));
        return tar;
    }
}
