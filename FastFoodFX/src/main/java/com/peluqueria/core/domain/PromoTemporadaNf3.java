package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf3DTO;

/**
 * The persistent class for the promo_temporada_nf3 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf3", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf3.findAll", query = "SELECT p FROM PromoTemporadaNf3 p")
public class PromoTemporadaNf3 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf3")
    private Long idPromoTemporadaNf3;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    public PromoTemporadaNf3() {
    }

    public Long getIdPromoTemporadaNf3() {
        return this.idPromoTemporadaNf3;
    }

    public void setIdPromoTemporadaNf3(Long idPromoTemporadaNf3) {
        this.idPromoTemporadaNf3 = idPromoTemporadaNf3;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf3 fromPromoTemporadaNf3DTO(PromoTemporadaNf3DTO promoTemporadaNf3DTO) {
        PromoTemporadaNf3 promoTemporadaNf3 = new PromoTemporadaNf3();
        BeanUtils.copyProperties(promoTemporadaNf3DTO, promoTemporadaNf3);
        return promoTemporadaNf3;
    }

    // <-- SERVER
    public static PromoTemporadaNf3DTO toPromoTemporadaNf3DTO(PromoTemporadaNf3 promoTemporadaNf3) {
        PromoTemporadaNf3DTO promoTemporadaNf3DTO = new PromoTemporadaNf3DTO();
        BeanUtils.copyProperties(promoTemporadaNf3, promoTemporadaNf3DTO);
        return promoTemporadaNf3DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf3 fromPromoTemporadaNf3DTOEntitiesFull(PromoTemporadaNf3DTO promoTemporadaNf3DTO) {
        PromoTemporadaNf3 promoTemporadaNf3 = fromPromoTemporadaNf3DTO(promoTemporadaNf3DTO);
        if (promoTemporadaNf3DTO.getNf3Sseccion() != null) {
            promoTemporadaNf3.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(promoTemporadaNf3DTO.getNf3Sseccion()));
        }
        if (promoTemporadaNf3DTO.getPromoTemporada() != null) {
            promoTemporadaNf3
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf3DTO.getPromoTemporada()));
        }
        return promoTemporadaNf3;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf3DTO toPromoTemporadaNf3DTOEntityFull() {
        PromoTemporadaNf3DTO promoTemporadaNf3DTO = toPromoTemporadaNf3DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf3DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf3DTO.setPromoTemporada(null);
        }
        if (this.getNf3Sseccion() != null) {
            promoTemporadaNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            promoTemporadaNf3DTO.setNf3Sseccion(null);
        }
        return promoTemporadaNf3DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf3DTO toPromoTemporadaNf3DTOEntityNull() {
        PromoTemporadaNf3DTO promoTemporadaNf3DTO = toPromoTemporadaNf3DTO(this);
        promoTemporadaNf3DTO.setPromoTemporada(null);
        promoTemporadaNf3DTO.setNf3Sseccion(null);
        return promoTemporadaNf3DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf3DTO toPromoTemporadaNf3DTONf2() {
        PromoTemporadaNf3DTO promoTemporadaNf3DTO = toPromoTemporadaNf3DTO(this);
        promoTemporadaNf3DTO.setPromoTemporada(null);
        if (this.getNf3Sseccion() != null) {
            promoTemporadaNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            promoTemporadaNf3DTO.setNf3Sseccion(null);
        }
        return promoTemporadaNf3DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
