package com.peluqueria.core.domain;

import com.peluqueria.dto.TarjetaConvenioDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tarjeta_convenio database table.
 *
 */
@Entity
@Table(name = "tarjeta_convenio", schema = "cuenta")
@NamedQuery(name = "TarjetaConvenio.findAll", query = "SELECT t FROM TarjetaConvenio t")
public class TarjetaConvenio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tarjeta_convenio")
    private Long idTarjetaConvenio;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "habilitado")
    private Boolean habilitado;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FamiliaTarj
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_familia_tarj")
    private FamiliaTarj familiaTarj;

    // bi-directional many-to-one association to FacturaClienteCabTarjConvenio
    @OneToMany(mappedBy = "tarjetaConvenio", fetch = FetchType.LAZY)
    private List<FacturaClienteCabTarjConvenio> facturaClienteCabTarjConvenios;

    // bi-directional many-to-one association to DescuentoTarjetaConvenioCab
    @OneToMany(mappedBy = "tarjetaConvenio", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaConvenioCab> descuentoTarjetaConvenioCab;

    public TarjetaConvenio() {
    }

    public Long getIdTarjetaConvenio() {
        return this.idTarjetaConvenio;
    }

    public void setIdTarjetaConvenio(Long idTarjetaConvenio) {
        this.idTarjetaConvenio = idTarjetaConvenio;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getHabilitado() {
        return this.habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DescuentoTarjetaConvenioCab> getDescuentoTarjetaConvenioCab() {
        return descuentoTarjetaConvenioCab;
    }

    public void setDescuentoTarjetaConvenioCab(
            List<DescuentoTarjetaConvenioCab> descuentoTarjetaConvenioCab) {
        this.descuentoTarjetaConvenioCab = descuentoTarjetaConvenioCab;
    }

    public FamiliaTarj getFamiliaTarj() {
        return this.familiaTarj;
    }

    public void setFamiliaTarj(FamiliaTarj familiaTarj) {
        this.familiaTarj = familiaTarj;
    }

    // ******************************************************************************************************************
    public List<FacturaClienteCabTarjConvenio> getFacturaClienteCabTarjConvenios() {
        return facturaClienteCabTarjConvenios;
    }

    public void setFacturaClienteCabTarjConvenios(
            List<FacturaClienteCabTarjConvenio> facturaClienteCabTarjConvenios) {
        this.facturaClienteCabTarjConvenios = facturaClienteCabTarjConvenios;
    }

    public TarjetaConvenioDTO toTarjetaConvenioNombreIdDTO() {
        TarjetaConvenioDTO tarDTO = toTarjetaConvenioDTO(this);
        tarDTO.setFacturaClienteCabTarjConvenioDTO(null);
        tarDTO.setFamiliaTarj(null);
        return tarDTO;
    }

    public TarjetaConvenioDTO toTarjetaConvenioDTO() {
        TarjetaConvenioDTO tarDTO = toTarjetaConvenioDTO(this);
        tarDTO.setFacturaClienteCabTarjConvenioDTO(null);
        return tarDTO;
    }

    public static TarjetaConvenioDTO toTarjetaConvenioDTO(TarjetaConvenio tar) {
        TarjetaConvenioDTO tarDTO = new TarjetaConvenioDTO();
        BeanUtils.copyProperties(tar, tarDTO);
        return tarDTO;
    }

    public static TarjetaConvenio fromTarjetaConvenioDTO(
            TarjetaConvenioDTO tarDTO) {
        TarjetaConvenio tar = new TarjetaConvenio();
        BeanUtils.copyProperties(tarDTO, tar);
        tar.setFamiliaTarj(null);
        return tar;
    }

    public static TarjetaConvenio fromTarjetaConvenioAsociadoDTO(
            TarjetaConvenioDTO tarDTO) {
        TarjetaConvenio tar = fromTarjetaConvenioDTO(tarDTO);
        tar.setFamiliaTarj(FamiliaTarj.fromFamiliaTarjDTO(tarDTO
                .getFamiliaTarj()));
        return tar;
    }
}
