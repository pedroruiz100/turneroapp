package com.peluqueria.core.domain;

import com.peluqueria.dto.PromoTemporadaDTO;
import com.peluqueria.dto.PromoTemporadaNf1DTO;
import com.peluqueria.dto.PromoTemporadaNf2DTO;
import com.peluqueria.dto.PromoTemporadaNf3DTO;
import com.peluqueria.dto.PromoTemporadaNf4DTO;
import com.peluqueria.dto.PromoTemporadaNf5DTO;
import com.peluqueria.dto.PromoTemporadaNf6DTO;
import com.peluqueria.dto.PromoTemporadaNf7DTO;
import com.peluqueria.dto.SeccionPromoTemporadaDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the promo_temporada database table.
 *
 */
@Entity
@Table(name = "promo_temporada", schema = "cuenta")
@NamedQuery(name = "PromoTemporada.findAll", query = "SELECT p FROM PromoTemporada p")
public class PromoTemporada implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_temporada")
    private Long idTemporada;

    @Column(name = "descripcion_temporada")
    private String descripcionTemporada;

    @Column(name = "estado_promo")
    private Boolean estadoPromo;

    @Column(name = "fecha_fin")
    private Timestamp fechaFin;

    @Column(name = "fecha_inicio")
    private Timestamp fechaInicio;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    // bi-directional one-to-many association to SeccionPromoTemporada
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<SeccionPromoTemporada> seccionPromoTemporada;

    // bi-directional one-to-many association to FacturaClienteCabPromoTemp
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<FacturaClienteCabPromoTemp> facturaClienteCabPromoTemp;

    // bi-directional one-to-many association to PromoTemporadaNf1
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf1> promoTemporadaNf1s;

    // bi-directional one-to-many association to PromoTemporadaNf2
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf2> promoTemporadaNf2s;

    // bi-directional one-to-many association to PromoTemporadaNf3
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf3> promoTemporadaNf3s;

    // bi-directional one-to-many association to PromoTemporadaNf4
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf4> promoTemporadaNf4s;

    // bi-directional one-to-many association to PromoTemporadaNf5
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf5> promoTemporadaNf5s;

    // bi-directional one-to-many association to PromoTemporadaNf6
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf6> promoTemporadaNf6s;

    // bi-directional one-to-many association to PromoTemporadaNf7
    @OneToMany(mappedBy = "promoTemporada", fetch = FetchType.LAZY)
    private List<PromoTemporadaNf7> promoTemporadaNf7s;

    public PromoTemporada() {
    }

    public Long getIdTemporada() {
        return this.idTemporada;
    }

    public void setIdTemporada(Long idTemporada) {
        this.idTemporada = idTemporada;
    }

    public String getDescripcionTemporada() {
        return this.descripcionTemporada;
    }

    public void setDescripcionTemporada(String descripcionTemporada) {
        this.descripcionTemporada = descripcionTemporada;
    }

    public Boolean getEstadoPromo() {
        return this.estadoPromo;
    }

    public void setEstadoPromo(Boolean estadoPromo) {
        this.estadoPromo = estadoPromo;
    }

    public Timestamp getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<SeccionPromoTemporada> getSeccionPromoTemporada() {
        return seccionPromoTemporada;
    }

    public void setSeccionPromoTemporada(
            List<SeccionPromoTemporada> seccionPromoTemporada) {
        this.seccionPromoTemporada = seccionPromoTemporada;
    }

    public List<FacturaClienteCabPromoTemp> getFacturaClienteCabPromoTemp() {
        return facturaClienteCabPromoTemp;
    }

    public void setFacturaClienteCabPromoTemp(
            List<FacturaClienteCabPromoTemp> facturaClienteCabPromoTemp) {
        this.facturaClienteCabPromoTemp = facturaClienteCabPromoTemp;
    }

    public List<PromoTemporadaNf1> getPromoTemporadaNf1() {
        return promoTemporadaNf1s;
    }

    public void setPromoTemporadaNf1(List<PromoTemporadaNf1> promoTemporadaNf1s) {
        this.promoTemporadaNf1s = promoTemporadaNf1s;
    }

    public List<PromoTemporadaNf2> getPromoTemporadaNf2() {
        return promoTemporadaNf2s;
    }

    public void setPromoTemporadaNf2(List<PromoTemporadaNf2> promoTemporadaNf2s) {
        this.promoTemporadaNf2s = promoTemporadaNf2s;
    }

    public List<PromoTemporadaNf3> getPromoTemporadaNf3() {
        return promoTemporadaNf3s;
    }

    public void setPromoTemporadaNf3(List<PromoTemporadaNf3> promoTemporadaNf3s) {
        this.promoTemporadaNf3s = promoTemporadaNf3s;
    }

    public List<PromoTemporadaNf4> getPromoTemporadaNf4() {
        return promoTemporadaNf4s;
    }

    public void setPromoTemporadaNf4(List<PromoTemporadaNf4> promoTemporadaNf4s) {
        this.promoTemporadaNf4s = promoTemporadaNf4s;
    }

    public List<PromoTemporadaNf5> getPromoTemporadaNf5() {
        return promoTemporadaNf5s;
    }

    public void setPromoTemporadaNf5(List<PromoTemporadaNf5> promoTemporadaNf5s) {
        this.promoTemporadaNf5s = promoTemporadaNf5s;
    }

    public List<PromoTemporadaNf6> getPromoTemporadaNf6() {
        return promoTemporadaNf6s;
    }

    public void setPromoTemporadaNf6(List<PromoTemporadaNf6> promoTemporadaNf6s) {
        this.promoTemporadaNf6s = promoTemporadaNf6s;
    }

    public List<PromoTemporadaNf7> getPromoTemporadaNf7() {
        return promoTemporadaNf7s;
    }

    public void setPromoTemporadaNf7(List<PromoTemporadaNf7> promoTemporadaNf7s) {
        this.promoTemporadaNf7s = promoTemporadaNf7s;
    }

    public PromoTemporadaDTO toPromoTemporadaDTO() {
        PromoTemporadaDTO promoTemporadaDTO = toPromoTemporadaDTO(this);
        if (!this.seccionPromoTemporada.isEmpty()) {
            List<SeccionPromoTemporada> pro = this.getSeccionPromoTemporada();
            List<SeccionPromoTemporadaDTO> secPromoDTO = new ArrayList<SeccionPromoTemporadaDTO>();
            for (SeccionPromoTemporada secPromo : pro) {
                secPromoDTO.add(secPromo.toSinSeccionPromoTemporadaDTO());
            }
            promoTemporadaDTO.setSeccionPromoTemporadaDTO(secPromoDTO);
        }
        promoTemporadaDTO.setFacturaClienteCabPromoTemp(null);
        // NIVELES
        if (this.getPromoTemporadaNf1() != null) {
            if (!this.getPromoTemporadaNf1().isEmpty()) {
                List<PromoTemporadaNf1DTO> promoTemporadaNf1DTOs = new ArrayList<>();
                for (PromoTemporadaNf1 promoTemporadaNf1 : this.getPromoTemporadaNf1()) {
                    promoTemporadaNf1DTOs.add(promoTemporadaNf1.toPromoTemporadaNf1DTO());
                }
                promoTemporadaDTO.setPromoTemporadaNf1s(promoTemporadaNf1DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf1s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf1s(null);
        }
        if (this.getPromoTemporadaNf2() != null) {
            if (!this.getPromoTemporadaNf2().isEmpty()) {
                List<PromoTemporadaNf2DTO> promoTemporadaNf2DTOs = new ArrayList<>();
                for (PromoTemporadaNf2 promoTemporadaNf2 : this.getPromoTemporadaNf2()) {
                    promoTemporadaNf2DTOs.add(promoTemporadaNf2.toPromoTemporadaNf2DTONf1());
                }
                promoTemporadaDTO.setPromoTemporadaNf2s(promoTemporadaNf2DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf2s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf2s(null);
        }
        if (this.getPromoTemporadaNf3() != null) {
            if (!this.getPromoTemporadaNf3().isEmpty()) {
                List<PromoTemporadaNf3DTO> promoTemporadaNf3DTOs = new ArrayList<>();
                for (PromoTemporadaNf3 promoTemporadaNf3 : this.getPromoTemporadaNf3()) {
                    promoTemporadaNf3DTOs.add(promoTemporadaNf3.toPromoTemporadaNf3DTONf2());
                }
                promoTemporadaDTO.setPromoTemporadaNf3s(promoTemporadaNf3DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf3s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf3s(null);
        }
        if (this.getPromoTemporadaNf4() != null) {
            if (!this.getPromoTemporadaNf4().isEmpty()) {
                List<PromoTemporadaNf4DTO> promoTemporadaNf4DTOs = new ArrayList<>();
                for (PromoTemporadaNf4 promoTemporadaNf4 : this.getPromoTemporadaNf4()) {
                    promoTemporadaNf4DTOs.add(promoTemporadaNf4.toPromoTemporadaNf4DTONf3());
                }
                promoTemporadaDTO.setPromoTemporadaNf4s(promoTemporadaNf4DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf4s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf4s(null);
        }
        if (this.getPromoTemporadaNf5() != null) {
            if (!this.getPromoTemporadaNf5().isEmpty()) {
                List<PromoTemporadaNf5DTO> promoTemporadaNf5DTOs = new ArrayList<>();
                for (PromoTemporadaNf5 promoTemporadaNf5 : this.getPromoTemporadaNf5()) {
                    promoTemporadaNf5DTOs.add(promoTemporadaNf5.toPromoTemporadaNf5DTONf4());
                }
                promoTemporadaDTO.setPromoTemporadaNf5s(promoTemporadaNf5DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf5s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf5s(null);
        }
        if (this.getPromoTemporadaNf6() != null) {
            if (!this.getPromoTemporadaNf6().isEmpty()) {
                List<PromoTemporadaNf6DTO> promoTemporadaNf6DTOs = new ArrayList<>();
                for (PromoTemporadaNf6 promoTemporadaNf6 : this.getPromoTemporadaNf6()) {
                    promoTemporadaNf6DTOs.add(promoTemporadaNf6.toPromoTemporadaNf6DTONf5());
                }
                promoTemporadaDTO.setPromoTemporadaNf6s(promoTemporadaNf6DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf6s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf6s(null);
        }
        if (this.getPromoTemporadaNf7() != null) {
            if (!this.getPromoTemporadaNf7().isEmpty()) {
                List<PromoTemporadaNf7DTO> promoTemporadaNf7DTOs = new ArrayList<>();
                for (PromoTemporadaNf7 promoTemporadaNf7 : this.getPromoTemporadaNf7()) {
                    promoTemporadaNf7DTOs.add(promoTemporadaNf7.toPromoTemporadaNf7DTONf6());
                }
                promoTemporadaDTO.setPromoTemporadaNf7s(promoTemporadaNf7DTOs);
            } else {
                promoTemporadaDTO.setPromoTemporadaNf7s(null);
            }
        } else {
            promoTemporadaDTO.setPromoTemporadaNf7s(null);
        }
        return promoTemporadaDTO;
    }

    // Sin la seccion Promo Temporada
    public PromoTemporadaDTO toPromoTemporadaSinSecDTO() {
        PromoTemporadaDTO proDTO = toPromoTemporadaDTO(this);
        proDTO.setSeccionPromoTemporadaDTO(null);
        proDTO.setFacturaClienteCabPromoTemp(null);
        proDTO.setPromoTemporadaNf1s(null);
        proDTO.setPromoTemporadaNf2s(null);
        proDTO.setPromoTemporadaNf3s(null);
        proDTO.setPromoTemporadaNf4s(null);
        proDTO.setPromoTemporadaNf5s(null);
        proDTO.setPromoTemporadaNf6s(null);
        proDTO.setPromoTemporadaNf7s(null);
        return proDTO;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaDTO toPromoTemporadaDTO(PromoTemporada pro) {
        PromoTemporadaDTO proDTO = new PromoTemporadaDTO();
        BeanUtils.copyProperties(pro, proDTO);
        return proDTO;
    }

    // <-- SERVER
    public static PromoTemporada fromPromoTemporadaDTO(PromoTemporadaDTO proDTO) {
        PromoTemporada pro = new PromoTemporada();
        BeanUtils.copyProperties(proDTO, pro);
        return pro;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaDTO toPromoTemporadaDTOEntitiesFullFactNull() {
        PromoTemporadaDTO promoTemporadaDTO = toPromoTemporadaDTO(this);
        // acaité, días contados
        if (this.seccionPromoTemporada != null) {
            if (!this.seccionPromoTemporada.isEmpty()) {
                List<SeccionPromoTemporada> pro = this.getSeccionPromoTemporada();
                List<SeccionPromoTemporadaDTO> secPromoDTO = new ArrayList<>();
                for (SeccionPromoTemporada secPromo : pro) {
                    secPromoDTO.add(secPromo.toSinSeccionPromoTemporadaDTO());
                }
                promoTemporadaDTO.setSeccionPromoTemporadaDTO(secPromoDTO);
            }
        }
        if (this.getPromoTemporadaNf1() != null) {
            if (!this.getPromoTemporadaNf1().isEmpty()) {
                List<PromoTemporadaNf1DTO> promoTemporadaNf1DTOs = new ArrayList<PromoTemporadaNf1DTO>();
                for (PromoTemporadaNf1 promoTemporadaNf1 : this.getPromoTemporadaNf1()) {
                    promoTemporadaNf1DTOs.add(promoTemporadaNf1.toPromoTemporadaNf1DTO());
                }
                promoTemporadaDTO.setPromoTemporadaNf1s(promoTemporadaNf1DTOs);
            }
        }
        if (this.getPromoTemporadaNf2() != null) {
            if (!this.getPromoTemporadaNf2().isEmpty()) {
                List<PromoTemporadaNf2DTO> promoTemporadaNf2DTOs = new ArrayList<PromoTemporadaNf2DTO>();
                for (PromoTemporadaNf2 promoTemporadaNf2 : this.getPromoTemporadaNf2()) {
                    promoTemporadaNf2DTOs.add(promoTemporadaNf2.toPromoTemporadaNf2DTONf1());
                }
                promoTemporadaDTO.setPromoTemporadaNf2s(promoTemporadaNf2DTOs);
            }
        }
        if (this.getPromoTemporadaNf3() != null) {
            if (!this.getPromoTemporadaNf3().isEmpty()) {
                List<PromoTemporadaNf3DTO> promoTemporadaNf3DTOs = new ArrayList<PromoTemporadaNf3DTO>();
                for (PromoTemporadaNf3 promoTemporadaNf3 : this.getPromoTemporadaNf3()) {
                    promoTemporadaNf3DTOs.add(promoTemporadaNf3.toPromoTemporadaNf3DTONf2());
                }
                promoTemporadaDTO.setPromoTemporadaNf3s(promoTemporadaNf3DTOs);
            }
        }
        if (this.getPromoTemporadaNf4() != null) {
            if (!this.getPromoTemporadaNf4().isEmpty()) {
                List<PromoTemporadaNf4DTO> promoTemporadaNf4DTOs = new ArrayList<PromoTemporadaNf4DTO>();
                for (PromoTemporadaNf4 promoTemporadaNf4 : this.getPromoTemporadaNf4()) {
                    promoTemporadaNf4DTOs.add(promoTemporadaNf4.toPromoTemporadaNf4DTONf3());
                }
                promoTemporadaDTO.setPromoTemporadaNf4s(promoTemporadaNf4DTOs);
            }
        }
        if (this.getPromoTemporadaNf5() != null) {
            if (!this.getPromoTemporadaNf5().isEmpty()) {
                List<PromoTemporadaNf5DTO> promoTemporadaNf5DTOs = new ArrayList<PromoTemporadaNf5DTO>();
                for (PromoTemporadaNf5 promoTemporadaNf5 : this.getPromoTemporadaNf5()) {
                    promoTemporadaNf5DTOs.add(promoTemporadaNf5.toPromoTemporadaNf5DTONf4());
                }
                promoTemporadaDTO.setPromoTemporadaNf5s(promoTemporadaNf5DTOs);
            }
        }
        if (this.getPromoTemporadaNf6() != null) {
            if (!this.getPromoTemporadaNf6().isEmpty()) {
                List<PromoTemporadaNf6DTO> promoTemporadaNf6DTOs = new ArrayList<PromoTemporadaNf6DTO>();
                for (PromoTemporadaNf6 promoTemporadaNf6 : this.getPromoTemporadaNf6()) {
                    promoTemporadaNf6DTOs.add(promoTemporadaNf6.toPromoTemporadaNf6DTONf5());
                }
                promoTemporadaDTO.setPromoTemporadaNf6s(promoTemporadaNf6DTOs);
            }
        }
        if (this.getPromoTemporadaNf7() != null) {
            if (!this.getPromoTemporadaNf7().isEmpty()) {
                List<PromoTemporadaNf7DTO> promoTemporadaNf7DTOs = new ArrayList<PromoTemporadaNf7DTO>();
                for (PromoTemporadaNf7 promoTemporadaNf7 : this.getPromoTemporadaNf7()) {
                    promoTemporadaNf7DTOs.add(promoTemporadaNf7.toPromoTemporadaNf7DTONf6());
                }
                promoTemporadaDTO.setPromoTemporadaNf7s(promoTemporadaNf7DTOs);
            }
        }
        promoTemporadaDTO.setFacturaClienteCabPromoTemp(null);
        return promoTemporadaDTO;
    }

    // <-- SERVER
    public PromoTemporadaDTO toPromoTemporadaDTOEntitiesNull() {
        PromoTemporadaDTO promoTemporadaDTO = toPromoTemporadaDTO(this);
        promoTemporadaDTO.setSeccionPromoTemporadaDTO(null);
        promoTemporadaDTO.setFacturaClienteCabPromoTemp(null);
        promoTemporadaDTO.setPromoTemporadaNf1s(null);
        promoTemporadaDTO.setPromoTemporadaNf2s(null);
        promoTemporadaDTO.setPromoTemporadaNf3s(null);
        promoTemporadaDTO.setPromoTemporadaNf4s(null);
        promoTemporadaDTO.setPromoTemporadaNf5s(null);
        promoTemporadaDTO.setPromoTemporadaNf6s(null);
        promoTemporadaDTO.setPromoTemporadaNf7s(null);
        return promoTemporadaDTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

    // ######################## NULL, FULL; ENTRADA ########################
    // <-- SERVER
    public static PromoTemporada fromPromoTemporadaDTOEntitiesNull(PromoTemporadaDTO proDTO) {
        PromoTemporada promoTemporada = fromPromoTemporadaDTO(proDTO);
        promoTemporada.setPromoTemporadaNf1(null);
        promoTemporada.setPromoTemporadaNf2(null);
        promoTemporada.setPromoTemporadaNf3(null);
        promoTemporada.setPromoTemporadaNf4(null);
        promoTemporada.setPromoTemporadaNf5(null);
        promoTemporada.setPromoTemporadaNf6(null);
        promoTemporada.setPromoTemporadaNf7(null);
        promoTemporada.setFacturaClienteCabPromoTemp(null);
        //días contados...
        promoTemporada.setSeccionPromoTemporada(null);
        return promoTemporada;
    }
    // ######################## NULL, FULL; ENTRADA ########################

}
