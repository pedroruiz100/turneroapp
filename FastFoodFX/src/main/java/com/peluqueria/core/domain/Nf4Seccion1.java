package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf4Seccion1DTO;

/**
 * The persistent class for the nf4_seccion1 database table.
 *
 */
@Entity
@Table(name = "nf4_seccion1", schema = "stock")
@NamedQuery(name = "Nf4Seccion1.findAll", query = "SELECT n FROM Nf4Seccion1 n")
public class Nf4Seccion1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf4_seccion1")
    private Long idNf4Seccion1;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    // bi-directional one-to-many association to ArticuloNf4Seccion1
    @OneToMany(mappedBy = "nf4Seccion1")
    private List<ArticuloNf4Seccion1> articuloNf4Seccion1s;

    // bi-directional one-to-many association to Nf5Seccion2
    @OneToMany(mappedBy = "nf4Seccion1")
    private List<Nf5Seccion2> nf5Seccion2s;

    // bi-directional one-to-many association to FuncionarioNf4
    @OneToMany(mappedBy = "nf4Seccion1")
    private List<FuncionarioNf4> funcionarioNf4s;

    // bi-directional one-to-many association to DescuentoFielNf4
    @OneToMany(mappedBy = "nf4Seccion1")
    private List<DescuentoFielNf4> descuentoFielNf4s;

    // bi-directional one-to-many association to PromoTemporadaNf4
    @OneToMany(mappedBy = "nf4Seccion1")
    private List<PromoTemporadaNf4> promoTemporadaNf4s;

    public Nf4Seccion1() {
    }

    public Long getIdNf4Seccion1() {
        return this.idNf4Seccion1;
    }

    public void setIdNf4Seccion1(Long idNf4Seccion1) {
        this.idNf4Seccion1 = idNf4Seccion1;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf4Seccion1> getArticuloNf4Seccion1s() {
        return this.articuloNf4Seccion1s;
    }

    public void setArticuloNf4Seccion1s(List<ArticuloNf4Seccion1> articuloNf4Seccion1s) {
        this.articuloNf4Seccion1s = articuloNf4Seccion1s;
    }

    public ArticuloNf4Seccion1 addArticuloNf4Seccion1(ArticuloNf4Seccion1 articuloNf4Seccion1) {
        getArticuloNf4Seccion1s().add(articuloNf4Seccion1);
        articuloNf4Seccion1.setNf4Seccion1(this);

        return articuloNf4Seccion1;
    }

    public ArticuloNf4Seccion1 removeArticuloNf4Seccion1(ArticuloNf4Seccion1 articuloNf4Seccion1) {
        getArticuloNf4Seccion1s().remove(articuloNf4Seccion1);
        articuloNf4Seccion1.setNf4Seccion1(null);

        return articuloNf4Seccion1;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return this.nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    public List<Nf5Seccion2> getNf5Seccion2s() {
        return this.nf5Seccion2s;
    }

    public void setNf5Seccion2s(List<Nf5Seccion2> nf5Seccion2s) {
        this.nf5Seccion2s = nf5Seccion2s;
    }

    public Nf5Seccion2 addNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        getNf5Seccion2s().add(nf5Seccion2);
        nf5Seccion2.setNf4Seccion1(this);

        return nf5Seccion2;
    }

    public Nf5Seccion2 removeNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        getNf5Seccion2s().remove(nf5Seccion2);
        nf5Seccion2.setNf4Seccion1(null);

        return nf5Seccion2;
    }

    public List<FuncionarioNf4> getFuncionarioNf4s() {
        return this.funcionarioNf4s;
    }

    public void setFuncionarioNf4s(List<FuncionarioNf4> funcionarioNf4s) {
        this.funcionarioNf4s = funcionarioNf4s;
    }

    public FuncionarioNf4 addFuncionarioNf4(FuncionarioNf4 funcionarioNf4) {
        getFuncionarioNf4s().add(funcionarioNf4);
        funcionarioNf4.setNf4Seccion1(this);

        return funcionarioNf4;
    }

    public FuncionarioNf4 removeFuncionarioNf4(FuncionarioNf4 funcionarioNf4) {
        getFuncionarioNf4s().remove(funcionarioNf4);
        funcionarioNf4.setNf4Seccion1(null);

        return funcionarioNf4;
    }

    public List<PromoTemporadaNf4> getPromoTemporadaNf4s() {
        return this.promoTemporadaNf4s;
    }

    public void setPromoTemporadaNf4s(List<PromoTemporadaNf4> promoTemporadaNf4s) {
        this.promoTemporadaNf4s = promoTemporadaNf4s;
    }

    public PromoTemporadaNf4 addPromoTemporadaNf4(PromoTemporadaNf4 promoTemporadaNf4) {
        getPromoTemporadaNf4s().add(promoTemporadaNf4);
        promoTemporadaNf4.setNf4Seccion1(this);

        return promoTemporadaNf4;
    }

    public PromoTemporadaNf4 removePromoTemporadaNf4(PromoTemporadaNf4 promoTemporadaNf4) {
        getPromoTemporadaNf4s().remove(promoTemporadaNf4);
        promoTemporadaNf4.setNf4Seccion1(null);

        return promoTemporadaNf4;
    }

    public List<DescuentoFielNf4> getDescuentoFielNf4s() {
        return this.descuentoFielNf4s;
    }

    public void setDescuentoFielNf4s(List<DescuentoFielNf4> descuentoFielNf4s) {
        this.descuentoFielNf4s = descuentoFielNf4s;
    }

    public DescuentoFielNf4 addDescuentoFielNf4(DescuentoFielNf4 descuentoFielNf4) {
        getDescuentoFielNf4s().add(descuentoFielNf4);
        descuentoFielNf4.setNf4Seccion1(this);

        return descuentoFielNf4;
    }

    public DescuentoFielNf4 removeDescuentoFielNf4(DescuentoFielNf4 descuentoFielNf4) {
        getDescuentoFielNf4s().remove(descuentoFielNf4);
        descuentoFielNf4.setNf4Seccion1(null);

        return descuentoFielNf4;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf4Seccion1 fromNf4Seccion1DTO(Nf4Seccion1DTO nf4Seccion1DTO) {
        Nf4Seccion1 nf4Seccion1 = new Nf4Seccion1();
        BeanUtils.copyProperties(nf4Seccion1DTO, nf4Seccion1);
        return nf4Seccion1;
    }

    // <-- SERVER
    public static Nf4Seccion1DTO toNf4Seccion1DTO(Nf4Seccion1 nf4Seccion1) {
        Nf4Seccion1DTO nf4Seccion1DTO = new Nf4Seccion1DTO();
        BeanUtils.copyProperties(nf4Seccion1, nf4Seccion1DTO);
        return nf4Seccion1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf4Seccion1DTO toNf4Seccion1DTOEntitiesNull(Nf4Seccion1 nf4Seccion1) {
        Nf4Seccion1DTO nf4Seccion1DTO = toNf4Seccion1DTO(nf4Seccion1);
        nf4Seccion1DTO.setNf3Sseccion(null);// Entity
        nf4Seccion1DTO.setNf5Seccion2s(null);// Entities
        nf4Seccion1DTO.setArticuloNf4Seccion1s(null);// Entities
        nf4Seccion1DTO.setDescuentoFielNf4s(null);// Entities
        nf4Seccion1DTO.setFuncionarioNf4s(null);// Entities
        nf4Seccion1DTO.setPromoTemporadaNf4s(null); // Entities
        return nf4Seccion1DTO;
    }

    // <-- SERVER
    public Nf4Seccion1DTO toNf4Seccion1DTOEntitiesNull() {
        Nf4Seccion1DTO nf4Seccion1DTO = toNf4Seccion1DTO(this);
        nf4Seccion1DTO.setNf3Sseccion(null);// Entity
        nf4Seccion1DTO.setNf5Seccion2s(null);// Entities
        nf4Seccion1DTO.setArticuloNf4Seccion1s(null);// Entities
        nf4Seccion1DTO.setDescuentoFielNf4s(null);// Entities
        nf4Seccion1DTO.setFuncionarioNf4s(null);// Entities
        nf4Seccion1DTO.setPromoTemporadaNf4s(null); // Entities
        return nf4Seccion1DTO;
    }

    // <-- SERVER
    public Nf4Seccion1DTO toNf4Seccion1DTOEntitiesNullNf3() {
        Nf4Seccion1DTO nf4Seccion1DTO = toNf4Seccion1DTO(this);
        nf4Seccion1DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());// Entity
        nf4Seccion1DTO.setNf5Seccion2s(null);// Entities
        nf4Seccion1DTO.setArticuloNf4Seccion1s(null);// Entities
        nf4Seccion1DTO.setDescuentoFielNf4s(null);// Entities
        nf4Seccion1DTO.setFuncionarioNf4s(null);// Entities
        nf4Seccion1DTO.setPromoTemporadaNf4s(null); // Entities
        return nf4Seccion1DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf4Seccion1 fromNf4Seccion1DTOEntitiesFull(Nf4Seccion1DTO nf4Seccion1DTO) {
        Nf4Seccion1 nf4Seccion1 = fromNf4Seccion1DTO(nf4Seccion1DTO);
        if (nf4Seccion1DTO.getNf3Sseccion() != null) {
            nf4Seccion1.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(nf4Seccion1DTO.getNf3Sseccion()));
        }
        return nf4Seccion1;
    }
    // ######################## FULL, ENTRADA ########################

}
