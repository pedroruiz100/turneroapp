package com.peluqueria.core.domain;

import com.peluqueria.dto.DescFuncionarioDTO;
import com.peluqueria.dto.FacturaClienteCabFuncionarioDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_funcionario database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_funcionario", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabFuncionario.findAll", query = "SELECT f FROM FacturaClienteCabFuncionario f")
public class FacturaClienteCabFuncionario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_funcionario")
    private Long idFacturaClienteCabFuncionario;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to funcionario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario")
    private Funcionario funcionario;

    @Column(name = "monto")
    private Integer monto;

    // bi-directional many-to-one association to DescFuncionario
    @OneToMany(mappedBy = "facturaClienteCabFuncionario", fetch = FetchType.LAZY)
    private List<DescFuncionario> DescFuncionario;

    public FacturaClienteCabFuncionario() {
    }

    public Long getIdFacturaClienteCabFuncionario() {
        return idFacturaClienteCabFuncionario;
    }

    public void setIdFacturaClienteCabFuncionario(Long idFacturaClienteCabFuncionario) {
        this.idFacturaClienteCabFuncionario = idFacturaClienteCabFuncionario;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Funcionario getfuncionario() {
        return funcionario;
    }

    public void setfuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public List<DescFuncionario> getDescFuncionario() {
        return DescFuncionario;
    }

    public void setDescFuncionario(List<DescFuncionario> DescFuncionario) {
        this.DescFuncionario = DescFuncionario;
    }

    public FacturaClienteCabFuncionarioDTO toBDFacturaClienteCabFuncionarioDTO() {
        FacturaClienteCabFuncionarioDTO facDTO = toFacturaClienteCabFuncionarioDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.funcionario != null) {
            Funcionario tar = this.getfuncionario();
            facDTO.setFuncionario(tar.toFuncionarioDTO());
        }
        facDTO.setDescFuncionario(null);
        return facDTO;
    }

    public FacturaClienteCabFuncionarioDTO toFactCliFuncDTO() {
        FacturaClienteCabFuncionarioDTO facDTO = toFacturaClienteCabFuncionarioDTO(this);
//		if (this.facturaClienteCab != null) {
//			FacturaClienteCab facCab = this.getFacturaClienteCab();
//			facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
//		}

        facDTO.setFacturaClienteCab(null);

        if (this.funcionario != null) {
            Funcionario tar = this.getfuncionario();
            facDTO.setFuncionario(tar.toFuncionarioDTO());
        }
        List<DescFuncionarioDTO> descFielDTO = new ArrayList<DescFuncionarioDTO>();
        if (!this.DescFuncionario.isEmpty()) {
            for (DescFuncionario descFunc : this.DescFuncionario) {
                descFielDTO.add(descFunc.toBDDescFuncionarioDTO());
            }
        }
        facDTO.setDescFuncionario(descFielDTO);
        return facDTO;
    }

    public FacturaClienteCabFuncionarioDTO toDescripcionFacturaClienteCabFuncionarioDTO() {
        FacturaClienteCabFuncionarioDTO facDTO = toFacturaClienteCabFuncionarioDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setFuncionario(null);
        facDTO.setDescFuncionario(null);
        return facDTO;
    }

    public FacturaClienteCabFuncionarioDTO toFacturaClienteCabFuncionarioDTO() {
        FacturaClienteCabFuncionarioDTO facDTO = toFacturaClienteCabFuncionarioDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.funcionario != null) {
            Funcionario tar = this.getfuncionario();
            facDTO.setFuncionario(tar.toFuncionarioDTO());
        }
        facDTO.setDescFuncionario(null);
        return facDTO;
    }

    public static FacturaClienteCabFuncionarioDTO toFacturaClienteCabFuncionarioDTO(
            FacturaClienteCabFuncionario facturaClienteCabFuncionario) {
        FacturaClienteCabFuncionarioDTO facDTO = new FacturaClienteCabFuncionarioDTO();
        BeanUtils.copyProperties(facturaClienteCabFuncionario, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabFuncionario fromFacturaClienteCabFuncionarioDTO(
            FacturaClienteCabFuncionarioDTO tarDTO) {
        FacturaClienteCabFuncionario fac = new FacturaClienteCabFuncionario();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }
    

    public static FacturaClienteCabFuncionario fromFacturaClienteCabFuncionarioAsociadoDTO(
            FacturaClienteCabFuncionarioDTO tarDTO) {
        FacturaClienteCabFuncionario fac = fromFacturaClienteCabFuncionarioDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setfuncionario(Funcionario.fromFuncionarioDTO(tarDTO
                .getFuncionario()));
        return fac;
    }

}
