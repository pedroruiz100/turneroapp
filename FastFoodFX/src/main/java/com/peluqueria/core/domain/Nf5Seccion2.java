package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf5Seccion2DTO;

/**
 * The persistent class for the nf5_seccion2 database table.
 *
 */
@Entity
@Table(name = "nf5_seccion2", schema = "stock")
@NamedQuery(name = "Nf5Seccion2.findAll", query = "SELECT n FROM Nf5Seccion2 n")
public class Nf5Seccion2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf5_seccion2")
    private Long idNf5Seccion2;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    // bi-directional one-to-many association to ArticuloNf5Seccion2
    @OneToMany(mappedBy = "nf5Seccion2")
    private List<ArticuloNf5Seccion2> articuloNf5Seccion2s;

    // bi-directional one-to-many association to Nf6Secnom6
    @OneToMany(mappedBy = "nf5Seccion2")
    private List<Nf6Secnom6> nf6Secnom6s;

    // bi-directional one-to-many association to FuncionarioNf5
    @OneToMany(mappedBy = "nf5Seccion2")
    private List<FuncionarioNf5> funcionarioNf5s;

    // bi-directional one-to-many association to DescuentoFielNf5
    @OneToMany(mappedBy = "nf5Seccion2")
    private List<DescuentoFielNf5> descuentoFielNf5s;

    // bi-directional one-to-many association to PromoTemporadaNf5
    @OneToMany(mappedBy = "nf5Seccion2")
    private List<PromoTemporadaNf5> promoTemporadaNf5s;

    public Nf5Seccion2() {
    }

    public Long getIdNf5Seccion2() {
        return this.idNf5Seccion2;
    }

    public void setIdNf5Seccion2(Long idNf5Seccion2) {
        this.idNf5Seccion2 = idNf5Seccion2;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf5Seccion2> getArticuloNf5Seccion2s() {
        return this.articuloNf5Seccion2s;
    }

    public void setArticuloNf5Seccion2s(List<ArticuloNf5Seccion2> articuloNf5Seccion2s) {
        this.articuloNf5Seccion2s = articuloNf5Seccion2s;
    }

    public ArticuloNf5Seccion2 addArticuloNf5Seccion2(ArticuloNf5Seccion2 articuloNf5Seccion2) {
        getArticuloNf5Seccion2s().add(articuloNf5Seccion2);
        articuloNf5Seccion2.setNf5Seccion2(this);

        return articuloNf5Seccion2;
    }

    public ArticuloNf5Seccion2 removeArticuloNf5Seccion2(ArticuloNf5Seccion2 articuloNf5Seccion2) {
        getArticuloNf5Seccion2s().remove(articuloNf5Seccion2);
        articuloNf5Seccion2.setNf5Seccion2(null);

        return articuloNf5Seccion2;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return this.nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    public List<Nf6Secnom6> getNf6Secnom6s() {
        return this.nf6Secnom6s;
    }

    public void setNf6Secnom6s(List<Nf6Secnom6> nf6Secnom6s) {
        this.nf6Secnom6s = nf6Secnom6s;
    }

    public Nf6Secnom6 addNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        getNf6Secnom6s().add(nf6Secnom6);
        nf6Secnom6.setNf5Seccion2(this);

        return nf6Secnom6;
    }

    public Nf6Secnom6 removeNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        getNf6Secnom6s().remove(nf6Secnom6);
        nf6Secnom6.setNf5Seccion2(null);

        return nf6Secnom6;
    }

    public List<FuncionarioNf5> getFuncionarioNf5s() {
        return this.funcionarioNf5s;
    }

    public void setFuncionarioNf5s(List<FuncionarioNf5> funcionarioNf5s) {
        this.funcionarioNf5s = funcionarioNf5s;
    }

    public FuncionarioNf5 addFuncionarioNf5(FuncionarioNf5 funcionarioNf5) {
        getFuncionarioNf5s().add(funcionarioNf5);
        funcionarioNf5.setNf5Seccion2(this);

        return funcionarioNf5;
    }

    public FuncionarioNf5 removeFuncionarioNf5(FuncionarioNf5 funcionarioNf5) {
        getFuncionarioNf5s().remove(funcionarioNf5);
        funcionarioNf5.setNf5Seccion2(null);

        return funcionarioNf5;
    }

    public List<PromoTemporadaNf5> getPromoTemporadaNf5s() {
        return this.promoTemporadaNf5s;
    }

    public void setPromoTemporadaNf5s(List<PromoTemporadaNf5> promoTemporadaNf5s) {
        this.promoTemporadaNf5s = promoTemporadaNf5s;
    }

    public PromoTemporadaNf5 addPromoTemporadaNf5(PromoTemporadaNf5 promoTemporadaNf5) {
        getPromoTemporadaNf5s().add(promoTemporadaNf5);
        promoTemporadaNf5.setNf5Seccion2(this);

        return promoTemporadaNf5;
    }

    public PromoTemporadaNf5 removePromoTemporadaNf5(PromoTemporadaNf5 promoTemporadaNf5) {
        getPromoTemporadaNf5s().remove(promoTemporadaNf5);
        promoTemporadaNf5.setNf5Seccion2(null);

        return promoTemporadaNf5;
    }

    public List<DescuentoFielNf5> getDescuentoFielNf5s() {
        return this.descuentoFielNf5s;
    }

    public void setDescuentoFielNf5s(List<DescuentoFielNf5> descuentoFielNf5s) {
        this.descuentoFielNf5s = descuentoFielNf5s;
    }

    public DescuentoFielNf5 addDescuentoFielNf5(DescuentoFielNf5 descuentoFielNf5) {
        getDescuentoFielNf5s().add(descuentoFielNf5);
        descuentoFielNf5.setNf5Seccion2(this);

        return descuentoFielNf5;
    }

    public DescuentoFielNf5 removeDescuentoFielNf5(DescuentoFielNf5 descuentoFielNf5) {
        getDescuentoFielNf5s().remove(descuentoFielNf5);
        descuentoFielNf5.setNf5Seccion2(null);

        return descuentoFielNf5;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf5Seccion2 fromNf5Seccion2DTO(Nf5Seccion2DTO nf5Seccion2DTO) {
        Nf5Seccion2 nf5Seccion2 = new Nf5Seccion2();
        BeanUtils.copyProperties(nf5Seccion2DTO, nf5Seccion2);
        return nf5Seccion2;
    }

    // <-- SERVER
    public static Nf5Seccion2DTO toNf5Seccion2DTO(Nf5Seccion2 nf5Seccion2) {
        Nf5Seccion2DTO nf5Seccion2DTO = new Nf5Seccion2DTO();
        BeanUtils.copyProperties(nf5Seccion2, nf5Seccion2DTO);
        return nf5Seccion2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf5Seccion2DTO toNf5Seccion2DTOEntitiesNull(Nf5Seccion2 nf5Seccion2) {
        Nf5Seccion2DTO nf5Seccion2DTO = toNf5Seccion2DTO(nf5Seccion2);
        nf5Seccion2DTO.setNf4Seccion1(null);// Entity
        nf5Seccion2DTO.setArticuloNf5Seccion2s(null);// Entities
        nf5Seccion2DTO.setNf6Secnom6s(null);// Entities
        nf5Seccion2DTO.setDescuentoFielNf5s(null);// Entities
        nf5Seccion2DTO.setFuncionarioNf5s(null);// Entities
        nf5Seccion2DTO.setPromoTemporadaNf5s(null);// Entities
        return nf5Seccion2DTO;
    }

    // <-- SERVER
    public Nf5Seccion2DTO toNf5Seccion2DTOEntitiesNull() {
        Nf5Seccion2DTO nf5Seccion2DTO = toNf5Seccion2DTO(this);
        nf5Seccion2DTO.setNf4Seccion1(null);// Entity
        nf5Seccion2DTO.setArticuloNf5Seccion2s(null);// Entities
        nf5Seccion2DTO.setNf6Secnom6s(null);// Entities
        nf5Seccion2DTO.setDescuentoFielNf5s(null);// Entities
        nf5Seccion2DTO.setFuncionarioNf5s(null);// Entities
        nf5Seccion2DTO.setPromoTemporadaNf5s(null);// Entities
        return nf5Seccion2DTO;
    }

    // <-- SERVER
    public Nf5Seccion2DTO toNf5Seccion2DTOEntitiesNullNf4() {
        Nf5Seccion2DTO nf5Seccion2DTO = toNf5Seccion2DTO(this);
        nf5Seccion2DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());// Entity
        nf5Seccion2DTO.setNf6Secnom6s(null);// Entities
        nf5Seccion2DTO.setArticuloNf5Seccion2s(null);// Entities
        nf5Seccion2DTO.setDescuentoFielNf5s(null);// Entities
        nf5Seccion2DTO.setFuncionarioNf5s(null);// Entities
        nf5Seccion2DTO.setPromoTemporadaNf5s(null); // Entities
        return nf5Seccion2DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf5Seccion2 fromNf5Seccion2DTOEntitiesFull(Nf5Seccion2DTO nf5Seccion2DTO) {
        Nf5Seccion2 nf5Seccion2 = fromNf5Seccion2DTO(nf5Seccion2DTO);
        if (nf5Seccion2DTO.getNf4Seccion1() != null) {
            nf5Seccion2.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(nf5Seccion2DTO.getNf4Seccion1()));
        }
        return nf5Seccion2;
    }
    // ######################## FULL, ENTRADA ########################

}
