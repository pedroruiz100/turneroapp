package com.peluqueria.core.domain;

import com.peluqueria.dto.UnidadDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the unidad database table.
 *
 */
@Entity
@Table(name = "unidad", schema = "stock")
@NamedQuery(name = "Unidad.findAll", query = "SELECT u FROM Unidad u")
public class Unidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_unidad")
    private Long idUnidad;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "decimal")
    private Integer decimal;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "descripcion_det")
    private String descripcionDet;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "unidad", fetch = FetchType.LAZY)
    private List<Articulo> articulos;

    public Unidad() {
    }

    public Long getIdUnidad() {
        return this.idUnidad;
    }

    public void setIdUnidad(Long idUnidad) {
        this.idUnidad = idUnidad;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Integer getDecimal() {
        return this.decimal;
    }

    public void setDecimal(Integer decimal) {
        this.decimal = decimal;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDescripcionDet() {
        return this.descripcionDet;
    }

    public void setDescripcionDet(String descripcionDet) {
        this.descripcionDet = descripcionDet;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Articulo> getArticulos() {
        return this.articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    public UnidadDTO toUnidadDTO() {
        UnidadDTO uniDTO = toUnidadDTO(this);
        uniDTO.setArticulos(null);
        return uniDTO;
    }

    public static UnidadDTO toUnidadDTO(Unidad unidad) {
        UnidadDTO uniDTO = new UnidadDTO();
        BeanUtils.copyProperties(unidad, uniDTO);
        return uniDTO;
    }

    public static Unidad fromUnidadDTO(UnidadDTO uniDTO) {
        Unidad uni = new Unidad();
        BeanUtils.copyProperties(uniDTO, uni);
        return uni;
    }

}
