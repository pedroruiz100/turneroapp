package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf6Secnom6DTO;

/**
 * The persistent class for the nf6_secnom6 database table.
 *
 */
@Entity
@Table(name = "nf6_secnom6", schema = "stock")
@NamedQuery(name = "Nf6Secnom6.findAll", query = "SELECT n FROM Nf6Secnom6 n")
public class Nf6Secnom6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf6_secnom6")
    private Long idNf6Secnom6;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to ArticuloNf6Secnom6
    @OneToMany(mappedBy = "nf6Secnom6")
    private List<ArticuloNf6Secnom6> articuloNf6Secnom6s;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    // bi-directional one-to-many association to Nf7Secnom7
    @OneToMany(mappedBy = "nf6Secnom6")
    private List<Nf7Secnom7> nf7Secnom7s;

    // bi-directional one-to-many association to FuncionarioNf6
    @OneToMany(mappedBy = "nf6Secnom6")
    private List<FuncionarioNf6> funcionarioNf6s;

    // bi-directional one-to-many association to DescuentoFielNf6
    @OneToMany(mappedBy = "nf6Secnom6")
    private List<DescuentoFielNf6> descuentoFielNf6s;

    // bi-directional one-to-many association to PromoTemporadaNf6
    @OneToMany(mappedBy = "nf6Secnom6")
    private List<PromoTemporadaNf6> promoTemporadaNf6s;

    public Nf6Secnom6() {
    }

    public Long getIdNf6Secnom6() {
        return this.idNf6Secnom6;
    }

    public void setIdNf6Secnom6(Long idNf6Secnom6) {
        this.idNf6Secnom6 = idNf6Secnom6;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf6Secnom6> getArticuloNf6Secnom6s() {
        return this.articuloNf6Secnom6s;
    }

    public void setArticuloNf6Secnom6s(List<ArticuloNf6Secnom6> articuloNf6Secnom6s) {
        this.articuloNf6Secnom6s = articuloNf6Secnom6s;
    }

    public ArticuloNf6Secnom6 addArticuloNf6Secnom6(ArticuloNf6Secnom6 articuloNf6Secnom6) {
        getArticuloNf6Secnom6s().add(articuloNf6Secnom6);
        articuloNf6Secnom6.setNf6Secnom6(this);

        return articuloNf6Secnom6;
    }

    public ArticuloNf6Secnom6 removeArticuloNf6Secnom6(ArticuloNf6Secnom6 articuloNf6Secnom6) {
        getArticuloNf6Secnom6s().remove(articuloNf6Secnom6);
        articuloNf6Secnom6.setNf6Secnom6(null);

        return articuloNf6Secnom6;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return this.nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    public List<Nf7Secnom7> getNf7Secnom7s() {
        return this.nf7Secnom7s;
    }

    public void setNf7Secnom7s(List<Nf7Secnom7> nf7Secnom7s) {
        this.nf7Secnom7s = nf7Secnom7s;
    }

    public Nf7Secnom7 addNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        getNf7Secnom7s().add(nf7Secnom7);
        nf7Secnom7.setNf6Secnom6(this);

        return nf7Secnom7;
    }

    public Nf7Secnom7 removeNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        getNf7Secnom7s().remove(nf7Secnom7);
        nf7Secnom7.setNf6Secnom6(null);

        return nf7Secnom7;
    }

    public List<FuncionarioNf6> getFuncionarioNf6s() {
        return this.funcionarioNf6s;
    }

    public void setFuncionarioNf6s(List<FuncionarioNf6> funcionarioNf6s) {
        this.funcionarioNf6s = funcionarioNf6s;
    }

    public FuncionarioNf6 addFuncionarioNf6(FuncionarioNf6 funcionarioNf6) {
        getFuncionarioNf6s().add(funcionarioNf6);
        funcionarioNf6.setNf6Secnom6(this);

        return funcionarioNf6;
    }

    public FuncionarioNf6 removeFuncionarioNf6(FuncionarioNf6 funcionarioNf6) {
        getFuncionarioNf6s().remove(funcionarioNf6);
        funcionarioNf6.setNf6Secnom6(null);

        return funcionarioNf6;
    }

    public List<DescuentoFielNf6> getDescuentoFielNf6s() {
        return this.descuentoFielNf6s;
    }

    public void setDescuentoFielNf6s(List<DescuentoFielNf6> descuentoFielNf6s) {
        this.descuentoFielNf6s = descuentoFielNf6s;
    }

    public DescuentoFielNf6 addDescuentoFielNf6(DescuentoFielNf6 descuentoFielNf6) {
        getDescuentoFielNf6s().add(descuentoFielNf6);
        descuentoFielNf6.setNf6Secnom6(this);

        return descuentoFielNf6;
    }

    public DescuentoFielNf6 removeDescuentoFielNf6(DescuentoFielNf6 descuentoFielNf6) {
        getDescuentoFielNf6s().remove(descuentoFielNf6);
        descuentoFielNf6.setNf6Secnom6(null);

        return descuentoFielNf6;
    }

    public List<PromoTemporadaNf6> getPromoTemporadaNf6s() {
        return this.promoTemporadaNf6s;
    }

    public void setPromoTemporadaNf6s(List<PromoTemporadaNf6> promoTemporadaNf6s) {
        this.promoTemporadaNf6s = promoTemporadaNf6s;
    }

    public PromoTemporadaNf6 addPromoTemporadaNf6(PromoTemporadaNf6 promoTemporadaNf6) {
        getPromoTemporadaNf6s().add(promoTemporadaNf6);
        promoTemporadaNf6.setNf6Secnom6(this);

        return promoTemporadaNf6;
    }

    public PromoTemporadaNf6 removePromoTemporadaNf6(PromoTemporadaNf6 promoTemporadaNf6) {
        getPromoTemporadaNf6s().remove(promoTemporadaNf6);
        promoTemporadaNf6.setNf6Secnom6(null);

        return promoTemporadaNf6;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf6Secnom6 fromNf6Secnom6DTO(Nf6Secnom6DTO nf6Secnom6DTO) {
        Nf6Secnom6 nf6Secnom6 = new Nf6Secnom6();
        BeanUtils.copyProperties(nf6Secnom6DTO, nf6Secnom6);
        return nf6Secnom6;
    }

    // <-- SERVER
    public static Nf6Secnom6DTO toNf6Secnom6DTO(Nf6Secnom6 nf6Secnom6) {
        Nf6Secnom6DTO nf6Secnom6DTO = new Nf6Secnom6DTO();
        BeanUtils.copyProperties(nf6Secnom6, nf6Secnom6DTO);
        return nf6Secnom6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf6Secnom6DTO toNf6Secnom6DTOEntitiesNull(Nf6Secnom6 nf6Secnom6) {
        Nf6Secnom6DTO nf6Secnom6DTO = toNf6Secnom6DTO(nf6Secnom6);
        nf6Secnom6DTO.setNf5Seccion2(null);// Entity
        nf6Secnom6DTO.setArticuloNf6Secnom6s(null);// Entities
        nf6Secnom6DTO.setNf7Secnom7s(null);// Entities
        nf6Secnom6DTO.setDescuentoFielNf6s(null);// Entities
        nf6Secnom6DTO.setFuncionarioNf6s(null);// Entities
        nf6Secnom6DTO.setPromoTemporadaNf6s(null);// Entities
        return nf6Secnom6DTO;
    }

    // <-- SERVER
    public Nf6Secnom6DTO toNf6Secnom6DTOEntitiesNull() {
        Nf6Secnom6DTO nf6Secnom6DTO = toNf6Secnom6DTO(this);
        nf6Secnom6DTO.setNf5Seccion2(null);// Entity
        nf6Secnom6DTO.setArticuloNf6Secnom6s(null);// Entities
        nf6Secnom6DTO.setNf7Secnom7s(null);// Entities
        nf6Secnom6DTO.setDescuentoFielNf6s(null);// Entities
        nf6Secnom6DTO.setFuncionarioNf6s(null);// Entities
        nf6Secnom6DTO.setPromoTemporadaNf6s(null);// Entities
        return nf6Secnom6DTO;
    }

    // <-- SERVER
    public Nf6Secnom6DTO toNf6Secnom6DTOEntitiesNullNf5() {
        Nf6Secnom6DTO nf6Secnom6DTO = toNf6Secnom6DTO(this);
        nf6Secnom6DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());// Entity
        nf6Secnom6DTO.setNf7Secnom7s(null);// Entities
        nf6Secnom6DTO.setArticuloNf6Secnom6s(null);// Entities
        nf6Secnom6DTO.setDescuentoFielNf6s(null);// Entities
        nf6Secnom6DTO.setFuncionarioNf6s(null);// Entities
        nf6Secnom6DTO.setPromoTemporadaNf6s(null); // Entities
        return nf6Secnom6DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf6Secnom6 fromNf6Secnom6DTOEntitiesFull(Nf6Secnom6DTO nf6Secnom6DTO) {
        Nf6Secnom6 nf6Secnom6 = fromNf6Secnom6DTO(nf6Secnom6DTO);
        if (nf6Secnom6DTO.getNf5Seccion2() != null) {
            nf6Secnom6.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(nf6Secnom6DTO.getNf5Seccion2()));
        }
        return nf6Secnom6;
    }
    // ######################## FULL, ENTRADA ########################

}
