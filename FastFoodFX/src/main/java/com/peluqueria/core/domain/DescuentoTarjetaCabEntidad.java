/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaCabEntidadDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 *
 * @author ExcelsisWalker
 */
@Entity
@Table(name = "descuento_tarjeta_cab_entidad", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabEntidad.findAll", query = "SELECT dtce FROM DescuentoTarjetaCabEntidad dtce")
public class DescuentoTarjetaCabEntidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_entidad")
    private Long idDescuentoTarjetaCabEntidad;

    @Column(name = "porcentaje_entidad")
    private BigDecimal porcentajeEntidad;

    @Column(name = "retorno")
    private Boolean retorno;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Entidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_entidad")
    private Entidad entidad;

    public DescuentoTarjetaCabEntidad() {
    }

    public Long getIdDescuentoTarjetaCabEntidad() {
        return this.idDescuentoTarjetaCabEntidad;
    }

    public void setIdDescuentoTarjetaCabEntidad(Long idDescuentoTarjetaCabEntidad) {
        this.idDescuentoTarjetaCabEntidad = idDescuentoTarjetaCabEntidad;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Entidad getEntidad() {
        return entidad;
    }

    public void setEntidad(Entidad entidad) {
        this.entidad = entidad;
    }

    public BigDecimal getPorcentajeEntidad() {
        return this.porcentajeEntidad;
    }

    public void setPorcentajeEntidad(BigDecimal porcentajeEntidad) {
        this.porcentajeEntidad = porcentajeEntidad;
    }

    public Boolean getRetorno() {
        return retorno;
    }

    public void setRetorno(Boolean retorno) {
        this.retorno = retorno;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabEntidad fromDescuentoTarjetaCabEntidadDTO(DescuentoTarjetaCabEntidadDTO descuentoTarjetaCabEntidadDTO) {
        DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad = new DescuentoTarjetaCabEntidad();
        BeanUtils.copyProperties(descuentoTarjetaCabEntidadDTO, descuentoTarjetaCabEntidad);
        return descuentoTarjetaCabEntidad;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabEntidadDTO toDescuentoTarjetaCabEntidadDTO(DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad) {
        DescuentoTarjetaCabEntidadDTO descuentoTarjetaCabEntidadDTO = new DescuentoTarjetaCabEntidadDTO();
        BeanUtils.copyProperties(descuentoTarjetaCabEntidad, descuentoTarjetaCabEntidadDTO);
        return descuentoTarjetaCabEntidadDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    public static DescuentoTarjetaCabEntidad fromDescuentoTarjetaCabEntidadNoListDTO(
            DescuentoTarjetaCabEntidadDTO descuentoTarjetaCabEntidadDTO) {
        DescuentoTarjetaCabEntidad descuentoTarjetaCabEntidad = new DescuentoTarjetaCabEntidad();
        BeanUtils.copyProperties(descuentoTarjetaCabEntidadDTO, descuentoTarjetaCabEntidad);
        descuentoTarjetaCabEntidad.setEntidad(Entidad.fromEntidadDTONoList(descuentoTarjetaCabEntidadDTO.getEntidad()));
        descuentoTarjetaCabEntidad.setDescuentoTarjetaCab(DescuentoTarjetaCab
                .fromDescTarSinTarjetaCabDTO(descuentoTarjetaCabEntidadDTO.getDescuentoTarjetaCab()));
        return descuentoTarjetaCabEntidad;
    }

    public DescuentoTarjetaCabEntidadDTO toDescuentoTarjetaCabEntidadDTO() {
        DescuentoTarjetaCabEntidadDTO descuentoTarjetaCabArticuloDTO = toDescuentoTarjetaCabEntidadDTO(this);
        if (this.entidad != null) {
            descuentoTarjetaCabArticuloDTO.setEntidad(this.getEntidad().toBDDescriEntidadDTO());
        }
        descuentoTarjetaCabArticuloDTO.setDescuentoTarjetaCab(null);
        return descuentoTarjetaCabArticuloDTO;
    }

}
