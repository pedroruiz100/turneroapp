package com.peluqueria.core.domain;

import com.peluqueria.dto.TimbradoDTO;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the timbrado database table.
 *
 */
@Entity
@Table(name = "timbrado", schema = "general")
@NamedQuery(name = "Timbrado.findAll", query = "SELECT t FROM Timbrado t")
public class Timbrado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_timbrado")
    private Long idTimbrado;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_inicial")
    private Date fecInicial;

    @Temporal(TemporalType.DATE)
    @Column(name = "fec_vencimiento")
    private Date fecVencimiento;

    @Column(name = "nro_timbrado")
    private String nroTimbrado;

    // bi-directional many-to-one association to TalonariosSucursale
    @OneToMany(mappedBy = "timbrado")
    private List<TalonariosSucursales> talonariosSucursales;

    public Timbrado() {
    }

    public Long getIdTimbrado() {
        return this.idTimbrado;
    }

    public void setIdTimbrado(Long idTimbrado) {
        this.idTimbrado = idTimbrado;
    }

    public Date getFecInicial() {
        return this.fecInicial;
    }

    public void setFecInicial(Date fecInicial) {
        this.fecInicial = fecInicial;
    }

    public Date getFecVencimiento() {
        return this.fecVencimiento;
    }

    public void setFecVencimiento(Date fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public String getNroTimbrado() {
        return this.nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public List<TalonariosSucursales> getTalonariosSucursales() {
        return this.talonariosSucursales;
    }

    public void setTalonariosSucursales(
            List<TalonariosSucursales> talonariosSucursales) {
        this.talonariosSucursales = talonariosSucursales;
    }

    public TimbradoDTO toBDTimbradoDTO() {
        TimbradoDTO timDTO = toTimbradoDTO(this);
        timDTO.setTalonariosSucursales(null);
        return timDTO;
    }

    public static TimbradoDTO toTimbradoDTO(Timbrado timbrado) {
        TimbradoDTO timDTO = new TimbradoDTO();
        BeanUtils.copyProperties(timbrado, timDTO);
        return timDTO;
    }

    public static Timbrado fromTimbrado(TimbradoDTO tDTO) {
        Timbrado tim = new Timbrado();
        BeanUtils.copyProperties(tDTO, tim);
        return tim;
    }

}
