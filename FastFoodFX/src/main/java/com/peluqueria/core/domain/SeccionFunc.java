package com.peluqueria.core.domain;

import com.peluqueria.dto.SeccionFuncDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the seccion_func database table.
 *
 */
@Entity
@Table(name = "seccion_func", schema = "cuenta")
@NamedQuery(name = "SeccionFunc.findAll", query = "SELECT s FROM SeccionFunc s")
public class SeccionFunc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_seccion_func")
    private Long idSeccionFunc;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    // bi-directional many-to-one association to Seccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion")
    private Seccion seccion;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    public SeccionFunc() {
    }

    public Long getIdSeccionFunc() {
        return this.idSeccionFunc;
    }

    public void setIdSeccionFunc(Long idSeccionFunc) {
        this.idSeccionFunc = idSeccionFunc;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public String getDescriSeccion() {
        return descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public SeccionFuncDTO toSeccionFuncDTO() {
        SeccionFuncDTO sfDTO = toSeccionFuncDTO(this);
        if (this.seccion != null) {
            Seccion s = this.getSeccion();
            sfDTO.setSeccion(s.toSeccionDTO());
        }
        return sfDTO;
    }

    public static SeccionFuncDTO toSeccionFuncDTO(SeccionFunc sf) {
        SeccionFuncDTO sfDTO = new SeccionFuncDTO();
        BeanUtils.copyProperties(sf, sfDTO);
        return sfDTO;
    }

    // Con Seccion para que pueda insertar sus datos
    public static SeccionFunc fromSeccionFuncDTO(SeccionFuncDTO sfDTO) {
        SeccionFunc sf = new SeccionFunc();
        BeanUtils.copyProperties(sfDTO, sf);
        return sf;
    }

    public static SeccionFunc fromSeccionFuncAsociadoDTO(SeccionFuncDTO sfDTO) {
        SeccionFunc sf = fromSeccionFuncDTO(sfDTO);
        sf.setSeccion(Seccion.fromSeccionDTO(sfDTO.getSeccion()));
        return sf;
    }

}
