package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoFielCabDTO;
import com.peluqueria.dto.DescuentoFielDetDTO;
import com.peluqueria.dto.DescuentoFielNf1DTO;
import com.peluqueria.dto.DescuentoFielNf2DTO;
import com.peluqueria.dto.DescuentoFielNf3DTO;
import com.peluqueria.dto.DescuentoFielNf4DTO;
import com.peluqueria.dto.DescuentoFielNf5DTO;
import com.peluqueria.dto.DescuentoFielNf6DTO;
import com.peluqueria.dto.DescuentoFielNf7DTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_fiel_cab database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_cab", schema = "cuenta")
@NamedQuery(name = "DescuentoFielCab.findAll", query = "SELECT d FROM DescuentoFielCab d")
public class DescuentoFielCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_cab")
    private Long idDescuentoFielCab;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion")
    private Seccion seccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional one-to-many association to DescuentoFielDet
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielDet> descuentoFielDets;

    // bi-directional one-to-many association to DescuentoFielNf1
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf1> descuentoFielNf1s;

    // bi-directional one-to-many association to DescuentoFielNf2
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf2> descuentoFielNf2s;

    // bi-directional one-to-many association to DescuentoFielNf3
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf3> descuentoFielNf3s;

    // bi-directional one-to-many association to DescuentoFielNf4
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf4> descuentoFielNf4s;

    // bi-directional one-to-many association to DescuentoFielNf5
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf5> descuentoFielNf5s;

    // bi-directional one-to-many association to DescuentoFielNf6
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf6> descuentoFielNf6s;

    // bi-directional one-to-many association to DescuentoFielNf7
    @OneToMany(mappedBy = "descuentoFielCab")
    private List<DescuentoFielNf7> descuentoFielNf7s;

    public DescuentoFielCab() {
    }

    public Long getIdDescuentoFielCab() {
        return this.idDescuentoFielCab;
    }

    public void setIdDescuentoFielCab(Long idDescuentoFielCab) {
        this.idDescuentoFielCab = idDescuentoFielCab;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public List<DescuentoFielDet> getDescuentoFielDets() {
        return this.descuentoFielDets;
    }

    public void setDescuentoFielDets(List<DescuentoFielDet> descuentoFielDets) {
        this.descuentoFielDets = descuentoFielDets;
    }

    public List<DescuentoFielNf1> getDescuentoFielNf1s() {
        return this.descuentoFielNf1s;
    }

    public void setDescuentoFielNf1s(List<DescuentoFielNf1> descuentoFielNf1s) {
        this.descuentoFielNf1s = descuentoFielNf1s;
    }

    public DescuentoFielNf1 addDescuentoFielNf1(DescuentoFielNf1 descuentoFielNf1) {
        getDescuentoFielNf1s().add(descuentoFielNf1);
        descuentoFielNf1.setDescuentoFielCab(this);

        return descuentoFielNf1;
    }

    public DescuentoFielNf1 removeDescuentoFielNf1(DescuentoFielNf1 descuentoFielNf1) {
        getDescuentoFielNf1s().remove(descuentoFielNf1);
        descuentoFielNf1.setDescuentoFielCab(null);

        return descuentoFielNf1;
    }

    public List<DescuentoFielNf2> getDescuentoFielNf2s() {
        return this.descuentoFielNf2s;
    }

    public void setDescuentoFielNf2s(List<DescuentoFielNf2> descuentoFielNf2s) {
        this.descuentoFielNf2s = descuentoFielNf2s;
    }

    public DescuentoFielNf2 addDescuentoFielNf2(DescuentoFielNf2 descuentoFielNf2) {
        getDescuentoFielNf2s().add(descuentoFielNf2);
        descuentoFielNf2.setDescuentoFielCab(this);

        return descuentoFielNf2;
    }

    public DescuentoFielNf2 removeDescuentoFielNf2(DescuentoFielNf2 descuentoFielNf2) {
        getDescuentoFielNf2s().remove(descuentoFielNf2);
        descuentoFielNf2.setDescuentoFielCab(null);

        return descuentoFielNf2;
    }

    public List<DescuentoFielNf3> getDescuentoFielNf3s() {
        return this.descuentoFielNf3s;
    }

    public void setDescuentoFielNf3s(List<DescuentoFielNf3> descuentoFielNf3s) {
        this.descuentoFielNf3s = descuentoFielNf3s;
    }

    public DescuentoFielNf3 addDescuentoFielNf3(DescuentoFielNf3 descuentoFielNf3) {
        getDescuentoFielNf3s().add(descuentoFielNf3);
        descuentoFielNf3.setDescuentoFielCab(this);

        return descuentoFielNf3;
    }

    public DescuentoFielNf3 removeDescuentoFielNf3(DescuentoFielNf3 descuentoFielNf3) {
        getDescuentoFielNf3s().remove(descuentoFielNf3);
        descuentoFielNf3.setDescuentoFielCab(null);

        return descuentoFielNf3;
    }

    public List<DescuentoFielNf4> getDescuentoFielNf4s() {
        return this.descuentoFielNf4s;
    }

    public void setDescuentoFielNf4s(List<DescuentoFielNf4> descuentoFielNf4s) {
        this.descuentoFielNf4s = descuentoFielNf4s;
    }

    public DescuentoFielNf4 addDescuentoFielNf4(DescuentoFielNf4 descuentoFielNf4) {
        getDescuentoFielNf4s().add(descuentoFielNf4);
        descuentoFielNf4.setDescuentoFielCab(this);

        return descuentoFielNf4;
    }

    public DescuentoFielNf4 removeDescuentoFielNf4(DescuentoFielNf4 descuentoFielNf4) {
        getDescuentoFielNf4s().remove(descuentoFielNf4);
        descuentoFielNf4.setDescuentoFielCab(null);

        return descuentoFielNf4;
    }

    public List<DescuentoFielNf5> getDescuentoFielNf5s() {
        return this.descuentoFielNf5s;
    }

    public void setDescuentoFielNf5s(List<DescuentoFielNf5> descuentoFielNf5s) {
        this.descuentoFielNf5s = descuentoFielNf5s;
    }

    public DescuentoFielNf5 addDescuentoFielNf5(DescuentoFielNf5 descuentoFielNf5) {
        getDescuentoFielNf5s().add(descuentoFielNf5);
        descuentoFielNf5.setDescuentoFielCab(this);

        return descuentoFielNf5;
    }

    public DescuentoFielNf5 removeDescuentoFielNf5(DescuentoFielNf5 descuentoFielNf5) {
        getDescuentoFielNf5s().remove(descuentoFielNf5);
        descuentoFielNf5.setDescuentoFielCab(null);

        return descuentoFielNf5;
    }

    public List<DescuentoFielNf6> getDescuentoFielNf6s() {
        return this.descuentoFielNf6s;
    }

    public void setDescuentoFielNf6s(List<DescuentoFielNf6> descuentoFielNf6s) {
        this.descuentoFielNf6s = descuentoFielNf6s;
    }

    public DescuentoFielNf6 addDescuentoFielNf6(DescuentoFielNf6 descuentoFielNf6) {
        getDescuentoFielNf6s().add(descuentoFielNf6);
        descuentoFielNf6.setDescuentoFielCab(this);

        return descuentoFielNf6;
    }

    public DescuentoFielNf6 removeDescuentoFielNf6(DescuentoFielNf6 descuentoFielNf6) {
        getDescuentoFielNf6s().remove(descuentoFielNf6);
        descuentoFielNf6.setDescuentoFielCab(null);

        return descuentoFielNf6;
    }

    public List<DescuentoFielNf7> getDescuentoFielNf7s() {
        return this.descuentoFielNf7s;
    }

    public void setDescuentoFielNf7s(List<DescuentoFielNf7> descuentoFielNf7s) {
        this.descuentoFielNf7s = descuentoFielNf7s;
    }

    public DescuentoFielNf7 addDescuentoFielNf7(DescuentoFielNf7 descuentoFielNf7) {
        getDescuentoFielNf7s().add(descuentoFielNf7);
        descuentoFielNf7.setDescuentoFielCab(this);

        return descuentoFielNf7;
    }

    public DescuentoFielNf7 removeDescuentoFielNf7(DescuentoFielNf7 descuentoFielNf7) {
        getDescuentoFielNf7s().remove(descuentoFielNf7);
        descuentoFielNf7.setDescuentoFielCab(null);

        return descuentoFielNf7;
    }

    public DescuentoFielCabDTO toDescuentoFielCabDTO() {
        DescuentoFielCabDTO dfcDTO = toDescuentoFielCabDTO(this);
        if (!this.descuentoFielDets.isEmpty()) {
            List<DescuentoFielDet> dfDET = this.descuentoFielDets;
            List<DescuentoFielDetDTO> dfDETDTO = new ArrayList<DescuentoFielDetDTO>();
            for (DescuentoFielDet dfd : dfDET) {
                DescuentoFielDetDTO dto = dfd.toDescuentoFielDetDTO();
                dfDETDTO.add(dto);
            }
            dfcDTO.setDescuentoFielDets(dfDETDTO);
        }
        if (this.seccion != null) {
            Seccion s = this.getSeccion();
            dfcDTO.setSeccion(s.toSeccionDTO());
        }
        return dfcDTO;
    }

    public DescuentoFielCabDTO toSinDescDescuentoFielCabDTO() {
        DescuentoFielCabDTO dfcDTO = toDescuentoFielCabDTO(this);

        dfcDTO.setDescuentoFielDets(null);
        dfcDTO.setSeccion(null);
        return dfcDTO;
    }

    public static DescuentoFielCabDTO toDescuentoFielCabDTO(DescuentoFielCab dfd) {
        DescuentoFielCabDTO dfcDTO = new DescuentoFielCabDTO();
        BeanUtils.copyProperties(dfd, dfcDTO);
        return dfcDTO;
    }

    public static DescuentoFielCab fromDescuentoFielCabDTO(
            DescuentoFielCabDTO dfdDTO) {
        DescuentoFielCab dfd = new DescuentoFielCab();
        BeanUtils.copyProperties(dfdDTO, dfd);
        dfd.setSeccion(Seccion.fromSeccionDTO(dfdDTO.getSeccion()));
        return dfd;
    }

    // Para Cabecera descuento cliente fiel sin seccion
    public static DescuentoFielCab fromDescuentoFielCabSinSeccionDTO(
            DescuentoFielCabDTO dfdDTO) {
        DescuentoFielCab dfd = new DescuentoFielCab();
        BeanUtils.copyProperties(dfdDTO, dfd);
        return dfd;
    }

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielCabDTO toDescuentoFielCabDTOEntitiesFull() {
        DescuentoFielCabDTO descuentoFielCabDTO = toDescuentoFielCabDTO(this);
        // DÍAS
        if (this.getDescuentoFielDets() != null) {
            if (!this.getDescuentoFielDets().isEmpty()) {
                List<DescuentoFielDetDTO> descuentoFielDetDTOs = new ArrayList<>();
                for (DescuentoFielDet descuentoFielDet : this.getDescuentoFielDets()) {
                    descuentoFielDetDTOs.add(descuentoFielDet.toDescuentoFielDetDTO());
                }
                descuentoFielCabDTO.setDescuentoFielDets(descuentoFielDetDTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielDets(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielDets(null);
        }
        // NIVELES
        if (this.getDescuentoFielNf1s() != null) {
            if (!this.getDescuentoFielNf1s().isEmpty()) {
                List<DescuentoFielNf1DTO> descuentoFielNf1DTOs = new ArrayList<>();
                for (DescuentoFielNf1 descuentoFielNf1 : this.getDescuentoFielNf1s()) {
                    descuentoFielNf1DTOs.add(descuentoFielNf1.toDescuentoFielNf1DTO());
                }
                descuentoFielCabDTO.setDescuentoFielNf1s(descuentoFielNf1DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf1s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf1s(null);
        }
        if (this.getDescuentoFielNf2s() != null) {
            if (!this.getDescuentoFielNf2s().isEmpty()) {
                List<DescuentoFielNf2DTO> descuentoFielNf2DTOs = new ArrayList<>();
                for (DescuentoFielNf2 descuentoFielNf2 : this.getDescuentoFielNf2s()) {
                    descuentoFielNf2DTOs.add(descuentoFielNf2.toDescuentoFielNf2DTONf1());
                }
                descuentoFielCabDTO.setDescuentoFielNf2s(descuentoFielNf2DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf2s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf2s(null);
        }
        if (this.getDescuentoFielNf3s() != null) {
            if (!this.getDescuentoFielNf3s().isEmpty()) {
                List<DescuentoFielNf3DTO> descuentoFielNf3DTOs = new ArrayList<>();
                for (DescuentoFielNf3 descuentoFielNf3 : this.getDescuentoFielNf3s()) {
                    descuentoFielNf3DTOs.add(descuentoFielNf3.toDescuentoFielNf3DTONf2());
                }
                descuentoFielCabDTO.setDescuentoFielNf3s(descuentoFielNf3DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf3s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf3s(null);
        }
        if (this.getDescuentoFielNf4s() != null) {
            if (!this.getDescuentoFielNf4s().isEmpty()) {
                List<DescuentoFielNf4DTO> descuentoFielNf4DTOs = new ArrayList<>();
                for (DescuentoFielNf4 descuentoFielNf4 : this.getDescuentoFielNf4s()) {
                    descuentoFielNf4DTOs.add(descuentoFielNf4.toDescuentoFielNf4DTONf3());
                }
                descuentoFielCabDTO.setDescuentoFielNf4s(descuentoFielNf4DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf4s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf4s(null);
        }
        if (this.getDescuentoFielNf5s() != null) {
            if (!this.getDescuentoFielNf5s().isEmpty()) {
                List<DescuentoFielNf5DTO> descuentoFielNf5DTOs = new ArrayList<>();
                for (DescuentoFielNf5 descuentoFielNf5 : this.getDescuentoFielNf5s()) {
                    descuentoFielNf5DTOs.add(descuentoFielNf5.toDescuentoFielNf5DTONf4());
                }
                descuentoFielCabDTO.setDescuentoFielNf5s(descuentoFielNf5DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf5s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf5s(null);
        }
        if (this.getDescuentoFielNf6s() != null) {
            if (!this.getDescuentoFielNf6s().isEmpty()) {
                List<DescuentoFielNf6DTO> descuentoFielNf6DTOs = new ArrayList<>();
                for (DescuentoFielNf6 descuentoFielNf6 : this.getDescuentoFielNf6s()) {
                    descuentoFielNf6DTOs.add(descuentoFielNf6.toDescuentoFielNf6DTONf5());
                }
                descuentoFielCabDTO.setDescuentoFielNf6s(descuentoFielNf6DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf6s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf6s(null);
        }
        if (this.getDescuentoFielNf7s() != null) {
            if (!this.getDescuentoFielNf7s().isEmpty()) {
                List<DescuentoFielNf7DTO> descuentoFielNf7DTOs = new ArrayList<>();
                for (DescuentoFielNf7 descuentoFielNf7 : this.getDescuentoFielNf7s()) {
                    descuentoFielNf7DTOs.add(descuentoFielNf7.toDescuentoFielNf7DTONf6());
                }
                descuentoFielCabDTO.setDescuentoFielNf7s(descuentoFielNf7DTOs);
            } else {
                descuentoFielCabDTO.setDescuentoFielNf7s(null);
            }
        } else {
            descuentoFielCabDTO.setDescuentoFielNf7s(null);
        }
        if (this.seccion != null) {// acaité, días contados
            Seccion s = this.getSeccion();
            descuentoFielCabDTO.setSeccion(s.toSeccionDTO());
        } else {
            descuentoFielCabDTO.setSeccion(null);
        }
        return descuentoFielCabDTO;
    }

    public DescuentoFielCabDTO toDescuentoFielCabDTOEntitiesNull() {
        DescuentoFielCabDTO descuentoFielCabDTO = toDescuentoFielCabDTO(this);
        descuentoFielCabDTO.setDescuentoFielDets(null);
        descuentoFielCabDTO.setSeccion(null);
        descuentoFielCabDTO.setDescuentoFielNf1s(null);
        descuentoFielCabDTO.setDescuentoFielNf2s(null);
        descuentoFielCabDTO.setDescuentoFielNf3s(null);
        descuentoFielCabDTO.setDescuentoFielNf4s(null);
        descuentoFielCabDTO.setDescuentoFielNf5s(null);
        descuentoFielCabDTO.setDescuentoFielNf6s(null);
        descuentoFielCabDTO.setDescuentoFielNf7s(null);
        return descuentoFielCabDTO;
    }
    // ######################## NULL, FULL; SALIDA ########################
}
