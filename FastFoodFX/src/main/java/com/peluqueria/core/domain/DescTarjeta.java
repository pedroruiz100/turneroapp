package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_tarjeta database table.
 *
 */
@Entity
@Table(name = "desc_tarjeta", schema = "factura_cliente")
@NamedQuery(name = "DescTarjeta.findAll", query = "SELECT d FROM DescTarjeta d")
public class DescTarjeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_tarjeta")
    private Long idDescTarjeta;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteCabTarjeta
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_tarjeta")
    private FacturaClienteCabTarjeta facturaClienteCabTarjeta;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    public DescTarjeta() {
    }

    public Long getIdDescTarjeta() {
        return this.idDescTarjeta;
    }

    public void setIdDescTarjeta(Long idDescTarjeta) {
        this.idDescTarjeta = idDescTarjeta;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabTarjeta getFacturaClienteCabTarjeta() {
        return this.facturaClienteCabTarjeta;
    }

    public void setFacturaClienteCabTarjeta(
            FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
        this.facturaClienteCabTarjeta = facturaClienteCabTarjeta;
    }

    public DescTarjetaDTO toDescTarjetaDTO() {
        DescTarjetaDTO descDTO = toDescTarjetaDTO(this);
        if (this.facturaClienteCabTarjeta != null) {
            FacturaClienteCabTarjeta desc = this.getFacturaClienteCabTarjeta();
            descDTO.setFacturaClienteCabTarjeta(desc
                    .toBDFacturaClienteCabTarjetaDTO());
        }
        if (this.descuentoTarjetaCab != null) {
            DescuentoTarjetaCab desc = this.getDescuentoTarjetaCab();
            descDTO.setDescuentoTarjetaCab(desc
                    .toBDDescuentoTarjetaCabDTO());
        }
        return descDTO;
    }

    public DescTarjetaDTO toDescriDescTarjetaDTO() {
        DescTarjetaDTO descDTO = toDescTarjetaDTO(this);
        descDTO.setFacturaClienteCabTarjeta(null);
        descDTO.setDescuentoTarjetaCab(null);
        return descDTO;
    }

    public static DescTarjetaDTO toDescTarjetaDTO(DescTarjeta descTarjeta) {
        DescTarjetaDTO descDTO = new DescTarjetaDTO();
        BeanUtils.copyProperties(descTarjeta, descDTO);
        return descDTO;
    }

    public static DescTarjeta fromDescTarjetaDTO(DescTarjetaDTO descTarjetaDTO) {
        DescTarjeta desc = new DescTarjeta();
        BeanUtils.copyProperties(descTarjetaDTO, desc);
        return desc;
    }

    public static DescTarjeta fromDescTarjetaAsociadoDTO(
            DescTarjetaDTO descTarjetaDTO) {
        DescTarjeta desc = fromDescTarjetaDTO(descTarjetaDTO);
        desc.setFacturaClienteCabTarjeta(FacturaClienteCabTarjeta
                .fromFacturaClienteCabTarjetaDTO(descTarjetaDTO
                        .getFacturaClienteCabTarjeta()));
        desc.setDescuentoTarjetaCab(null);
        return desc;
    }

    public DescTarjetaDTO toDescTarjetaConDetalleFacturaDTO() {
        DescTarjetaDTO descDTO = toDescTarjetaDTO(this);
        if (this.facturaClienteCabTarjeta != null) {
            FacturaClienteCabTarjeta desc = this.getFacturaClienteCabTarjeta();
            descDTO.setFacturaClienteCabTarjeta(desc
                    .toBDFacturaClienteCabTarjetaConDetalleDTO());
        }
        if (this.descuentoTarjetaCab != null) {
            DescuentoTarjetaCab desc = this.getDescuentoTarjetaCab();
            descDTO.setDescuentoTarjetaCab(desc.toDescTarjetaCabDTO());
        }
        return descDTO;
    }

    public DescTarjetaDTO toDescSinCabTarjDTO() {
        DescTarjetaDTO descDTO = toDescTarjetaDTO(this);
        descDTO.setFacturaClienteCabTarjeta(null);
        if (this.descuentoTarjetaCab != null) {
            DescuentoTarjetaCab desc = this.getDescuentoTarjetaCab();
            descDTO.setDescuentoTarjetaCab(desc.toDescTarjetaCabDTO());
        }
        return descDTO;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

}
