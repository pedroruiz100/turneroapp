package com.peluqueria.core.domain;

import com.peluqueria.dto.ValeDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the vales database table.
 *
 */
@Entity
@Table(name = "vales", schema = "cuenta")
@NamedQuery(name = "Vales.findAll", query = "SELECT v FROM Vales v")
public class Vales implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_vale")
    private Long idVale;

    @Column(name = "descripcion_vale")
    private String descripcionVale;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    // bi-directional many-to-one association to DescuentoTarjetaConvenio
    @OneToMany(mappedBy = "vale", fetch = FetchType.LAZY)
    private List<FacturaClienteCabVale> facturaClienteCabVale;

    public Vales() {
    }

    public Long getIdVale() {
        return this.idVale;
    }

    public void setIdVale(Long idVale) {
        this.idVale = idVale;
    }

    // **************************************************************************************************************
    public String getDescripcionVale() {
        return descripcionVale;
    }

    public void setDescripcionVale(String descripcionVale) {
        this.descripcionVale = descripcionVale;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    // **************************************************************************************************************
    public List<FacturaClienteCabVale> getFacturaClienteCabVale() {
        return facturaClienteCabVale;
    }

    public void setFacturaClienteCabVale(
            List<FacturaClienteCabVale> facturaClienteCabVale) {
        this.facturaClienteCabVale = facturaClienteCabVale;
    }

    public ValeDTO toValeDTO() {
        ValeDTO valeDTO = toValeDTO(this);
        valeDTO.setFacturaClienteCabVale(null);
        return valeDTO;
    }

    public static ValeDTO toValeDTO(Vales vale) {
        ValeDTO valeDTO = new ValeDTO();
        BeanUtils.copyProperties(vale, valeDTO);
        return valeDTO;
    }

    public static Vales fromValeDTO(ValeDTO valeDTO) {
        Vales vale = new Vales();
        BeanUtils.copyProperties(valeDTO, vale);
        return vale;
    }

}
