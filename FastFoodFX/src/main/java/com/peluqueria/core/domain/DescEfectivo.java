package com.peluqueria.core.domain;

import com.peluqueria.dto.DescEfectivoDTO;
import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_efectivo database table.
 *
 */
@Entity
@Table(name = "desc_efectivo", schema = "factura_cliente")
@NamedQuery(name = "DescEfectivo.findAll", query = "SELECT d FROM DescEfectivo d")
public class DescEfectivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_efectivo")
    private Long idDescEfectivo;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteCabEfectivo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_efectivo")
    private FacturaClienteCabEfectivo facturaClienteCabEfectivo;

    public DescEfectivo() {
    }

    public Long getIdDescEfectivo() {
        return this.idDescEfectivo;
    }

    public void setIdDescEfectivo(Long idDescEfectivo) {
        this.idDescEfectivo = idDescEfectivo;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabEfectivo getFacturaClienteCabEfectivo() {
        return this.facturaClienteCabEfectivo;
    }

    public void setFacturaClienteCabEfectivo(
            FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
        this.facturaClienteCabEfectivo = facturaClienteCabEfectivo;
    }

    public DescEfectivoDTO toDescEfectivoDTO() {
        DescEfectivoDTO descDTO = toDescEfectivoDTO(this);

        if (this.facturaClienteCabEfectivo != null) {
            descDTO.setFacturaClienteCabEfectivo(this
                    .getFacturaClienteCabEfectivo()
                    .toFacturaClienteCabEfectivoDTO());
        }

        return descDTO;
    }

    public DescEfectivoDTO toDescripcionDescEfectivoDTO() {
        DescEfectivoDTO descDTO = toDescEfectivoDTO(this);
        descDTO.setFacturaClienteCabEfectivo(null);
        return descDTO;
    }

    public static DescEfectivoDTO toDescEfectivoDTO(DescEfectivo descEfectivo) {
        DescEfectivoDTO descDTO = new DescEfectivoDTO();
        BeanUtils.copyProperties(descEfectivo, descDTO);
        return descDTO;
    }

    public static DescEfectivo fromDescEfectivoDTO(DescEfectivoDTO descDTO) {
        DescEfectivo desc = new DescEfectivo();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public static DescEfectivo fromDescEfectivoAsociadoDTO(
            DescEfectivoDTO descDTO) {
        DescEfectivo desc = fromDescEfectivoDTO(descDTO);
        desc.setFacturaClienteCabEfectivo(FacturaClienteCabEfectivo
                .fromFacturaClienteCabEfectivoDTO(descDTO
                        .getFacturaClienteCabEfectivo()));
        return desc;
    }

}
