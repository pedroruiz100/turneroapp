package com.peluqueria.core.domain;

import com.peluqueria.dto.DescComboDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_combo database table.
 *
 */
@Entity
@Table(name = "desc_combo", schema = "factura_cliente")
@NamedQuery(name = "DescCombo.findAll", query = "SELECT d FROM DescCombo d")
public class DescCombo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_combo")
    private Long idDescCombo;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteDet
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_det")
    private FacturaClienteDet facturaClienteDet;

    public DescCombo() {
    }

    public Long getIdDescCombo() {
        return this.idDescCombo;
    }

    public void setIdDescCombo(Long idDescCombo) {
        this.idDescCombo = idDescCombo;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteDet getFacturaClienteDet() {
        return this.facturaClienteDet;
    }

    public void setFacturaClienteDet(FacturaClienteDet facturaClienteDet) {
        this.facturaClienteDet = facturaClienteDet;
    }

    public DescComboDTO toDescriDescComboDTO() {
        DescComboDTO descDTO = toDescComboDTO(this);
        descDTO.setFacturaClienteDet(null);
        return descDTO;
    }

    public DescComboDTO toDescComboDTO() {
        DescComboDTO descDTO = toDescComboDTO(this);
        if (this.facturaClienteDet != null) {
            FacturaClienteDet facDet = this.getFacturaClienteDet();
            descDTO.setFacturaClienteDet(facDet.toFacturaClienteDetDTO());
        }
        return descDTO;
    }

    public static DescComboDTO toDescComboDTO(DescCombo descCombo) {
        DescComboDTO descDTO = new DescComboDTO();
        BeanUtils.copyProperties(descCombo, descDTO);
        return descDTO;
    }

    public static DescCombo fromDescComboDTO(DescComboDTO descComboDTO) {
        DescCombo desc = new DescCombo();
        BeanUtils.copyProperties(descComboDTO, desc);
        return desc;
    }

    public static DescCombo fromDescComboAsociadoDTO(DescComboDTO descDTO) {
        DescCombo desc = fromDescComboDTO(descDTO);
        desc.setFacturaClienteDet(FacturaClienteDet
                .fromFacturaClienteDetDTO(descDTO.getFacturaClienteDet()));
        return desc;
    }

}
