package com.peluqueria.core.domain;

import com.peluqueria.dto.TarjetaClienteFielDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tarjeta_cliente_fiel database table.
 *
 */
@Entity
@Table(name = "tarjeta_cliente_fiel", schema = "cuenta")
@NamedQuery(name = "TarjetaClienteFiel.findAll", query = "SELECT t FROM TarjetaClienteFiel t")
public class TarjetaClienteFiel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tarjeta_cliente_fiel")
    private Long idTarjetaClienteFiel;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "cipas")
    private String cipas;

    @Column(name = "empresa_suc")
    private String empresaSuc;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    public TarjetaClienteFiel() {
    }

    public Long getIdTarjetaClienteFiel() {
        return this.idTarjetaClienteFiel;
    }

    public void setIdTarjetaClienteFiel(Long idTarjetaClienteFiel) {
        this.idTarjetaClienteFiel = idTarjetaClienteFiel;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public String getCipas() {
        return cipas;
    }

    public void setCipas(String cipas) {
        this.cipas = cipas;
    }

    public String getEmpresaSuc() {
        return empresaSuc;
    }

    public void setEmpresaSuc(String empresaSuc) {
        this.empresaSuc = empresaSuc;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public TarjetaClienteFielDTO toTarjetaClienteFielDTO() {
        TarjetaClienteFielDTO tarDTO = toTarjetaClienteFielDTO(this);
        if (this.cliente != null) {
            tarDTO.setCliente(this.cliente.toClienteBDDTO());
        }
        tarDTO.setFacturaClienteCabs(null);
        return tarDTO;
    }

    public TarjetaClienteFielDTO toBDTarjetaClienteFielDTO() {
        TarjetaClienteFielDTO tarDTO = toTarjetaClienteFielDTO(this);
        tarDTO.setCliente(null);
        tarDTO.setFacturaClienteCabs(null);
        return tarDTO;
    }

    public static TarjetaClienteFielDTO toTarjetaClienteFielDTO(
            TarjetaClienteFiel tarjetaClienteFiel) {
        TarjetaClienteFielDTO tarDTO = new TarjetaClienteFielDTO();
        BeanUtils.copyProperties(tarjetaClienteFiel, tarDTO);
        return tarDTO;
    }

    public static TarjetaClienteFiel fromTarjetaClienteFielDTO(
            TarjetaClienteFielDTO tarjetaClienteFielDTO) {
        TarjetaClienteFiel tar = new TarjetaClienteFiel();
        BeanUtils.copyProperties(tarjetaClienteFielDTO, tar);
        return tar;
    }

    public static TarjetaClienteFiel fromTarjetaClienteFielDTOAsociado(
            TarjetaClienteFielDTO tarjetaClienteFielDTO) {
        TarjetaClienteFiel tarFiel = fromTarjetaClienteFielDTO(tarjetaClienteFielDTO);
        tarFiel.setCliente(Cliente.fromClienteDTO(tarjetaClienteFielDTO
                .getCliente()));
        return tarFiel;
    }
}
