package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloCompraMatrizDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the barrio database table.
 *
 */
@Entity
@Table(name = "articulo_compra_matriz", schema = "stock")
@NamedQuery(name = "ArticuloCompraMatriz.findAll", query = "SELECT b FROM ArticuloCompraMatriz b")
public class ArticuloCompraMatriz implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_compra_matriz")
    private Long idArticuloCompraMatriz;

    private String codigo;

    private Date fecha;

    private Long cantidad;
    private Boolean cc;
    private Boolean sc;
    private Boolean sl;

    public ArticuloCompraMatriz() {
    }

    public Long getIdArticuloCompraMatriz() {
        return idArticuloCompraMatriz;
    }

    public void setIdArticuloCompraMatriz(Long idArticuloCompraMatriz) {
        this.idArticuloCompraMatriz = idArticuloCompraMatriz;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getSc() {
        return sc;
    }

    public void setSc(Boolean sc) {
        this.sc = sc;
    }

    public Boolean getSl() {
        return sl;
    }

    public void setSl(Boolean sl) {
        this.sl = sl;
    }

    public Long getCantidad() {
        return cantidad;
    }

    public void setCantidad(Long cantidad) {
        this.cantidad = cantidad;
    }

    // Metodo que sirve para cargar los datos de Barrio a BarrioDTO
    public static ArticuloCompraMatrizDTO toArticuloCompraMatrizDTO(ArticuloCompraMatriz barrio) {
        ArticuloCompraMatrizDTO barrioDTO = new ArticuloCompraMatrizDTO();
        BeanUtils.copyProperties(barrio, barrioDTO);
        return barrioDTO;
    }

    public static ArticuloCompraMatriz fromArticuloCompraMatrizDTO(ArticuloCompraMatriz barrioDTO) {
        ArticuloCompraMatriz barrio = new ArticuloCompraMatriz();
        BeanUtils.copyProperties(barrioDTO, barrio);
        return barrio;
    }

    public static ArticuloCompraMatriz fromArticuloCompraMatriz(ArticuloCompraMatriz barrioDTO) {
        ArticuloCompraMatriz bar = fromArticuloCompraMatrizDTO(barrioDTO);
//        bar.set(Ciudad.fromCiudadDTO(barrioDTO.getCiudadDTO()));
        return bar;
    }

    // FIN DE BARRIO A CLIENTE
    // PARA FAMILIA TARJETA
//    public ArticuloCompraMatrizDTO toBarrioDTOAFamiliaTarjDTO() {
//        BarrioDTO barrioDTO = toBarrioDTO(this);
//        barrioDTO.setCiudadDTO(null);
//        barrioDTO.setClientesDTO(null);
//        barrioDTO.setFamiliaTarjDTO(null);
//        barrioDTO.setProveedorDTO(null);
//        barrioDTO.setSucursalsDTOs(null);
//        return barrioDTO;
//    }
}
