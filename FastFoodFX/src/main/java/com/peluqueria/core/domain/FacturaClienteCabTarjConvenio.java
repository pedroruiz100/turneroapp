package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaConvenioDTO;
import com.peluqueria.dto.FacturaClienteCabTarjConvenioDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_tarj_convenio database
 * table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_tarj_convenio", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabTarjConvenio.findAll", query = "SELECT f FROM FacturaClienteCabTarjConvenio f")
public class FacturaClienteCabTarjConvenio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_tarj_convenio")
    private Long idFacturaClienteCabTarjConvenio;

    @Column(name = "descripcion_tarj")
    private String descripcionTarj;

    @Column(name = "monto")
    private Integer monto;

    // bi-directional many-to-one association to TarjetaConvenio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarjeta_convenio")
    private TarjetaConvenio tarjetaConvenio;

    // bi-directional many-to-one association to DescTarjetaConvenio
    @OneToMany(mappedBy = "facturaClienteCabTarjConvenio", fetch = FetchType.LAZY)
    private List<DescTarjetaConvenio> descTarjetaConvenios;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public FacturaClienteCabTarjConvenio() {
    }

    public Long getIdFacturaClienteCabTarjConvenio() {
        return this.idFacturaClienteCabTarjConvenio;
    }

    public void setIdFacturaClienteCabTarjConvenio(
            Long idFacturaClienteCabTarjConvenio) {
        this.idFacturaClienteCabTarjConvenio = idFacturaClienteCabTarjConvenio;
    }

    public String getDescripcionTarj() {
        return this.descripcionTarj;
    }

    public void setDescripcionTarj(String descripcionTarj) {
        this.descripcionTarj = descripcionTarj;
    }

    public List<DescTarjetaConvenio> getDescTarjetaConvenios() {
        return this.descTarjetaConvenios;
    }

    public void setDescTarjetaConvenios(
            List<DescTarjetaConvenio> descTarjetaConvenios) {
        this.descTarjetaConvenios = descTarjetaConvenios;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TarjetaConvenio getTarjetaConvenio() {
        return tarjetaConvenio;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public void setTarjetaConvenio(TarjetaConvenio tarjetaConvenio) {
        this.tarjetaConvenio = tarjetaConvenio;
    }

    public FacturaClienteCabTarjConvenioDTO toBDFacturaClienteCabTarjConvenioDTO() {
        FacturaClienteCabTarjConvenioDTO facDTO = toFacturaClienteCabTarjConvenioDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.tarjetaConvenio != null) {
            TarjetaConvenio tar = this.getTarjetaConvenio();
            facDTO.setTarjetaConvenio(tar.toTarjetaConvenioNombreIdDTO());
        }
        if (!this.descTarjetaConvenios.isEmpty()) {
            List<DescTarjetaConvenio> descTar = this.getDescTarjetaConvenios();
            List<DescTarjetaConvenioDTO> descTarDTO = new ArrayList<DescTarjetaConvenioDTO>();
            for (DescTarjetaConvenio desc : descTar) {
                descTarDTO.add(desc.toDescripcionDescTarjetaConvenioDTO());
            }
        }
        facDTO.setDescTarjetaConvenios(null);
        return facDTO;
    }

    public FacturaClienteCabTarjConvenioDTO toDescripcionFacturaClienteCabTarjConvenioDTO() {
        FacturaClienteCabTarjConvenioDTO facDTO = toFacturaClienteCabTarjConvenioDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setTarjetaConvenio(null);
        facDTO.setDescTarjetaConvenios(null);
        return facDTO;
    }

    public FacturaClienteCabTarjConvenioDTO toFacturaClienteCabTarjConvenioDTO() {
        FacturaClienteCabTarjConvenioDTO facDTO = toFacturaClienteCabTarjConvenioDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.tarjetaConvenio != null) {
            TarjetaConvenio tar = this.getTarjetaConvenio();
            facDTO.setTarjetaConvenio(tar.toTarjetaConvenioNombreIdDTO());
        }
        facDTO.setDescTarjetaConvenios(null);
        return facDTO;
    }

    public static FacturaClienteCabTarjConvenioDTO toFacturaClienteCabTarjConvenioDTO(
            FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
        FacturaClienteCabTarjConvenioDTO facDTO = new FacturaClienteCabTarjConvenioDTO();
        BeanUtils.copyProperties(facturaClienteCabTarjConvenio, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabTarjConvenio fromFacturaClienteCabTarjConvenioDTO(
            FacturaClienteCabTarjConvenioDTO tarDTO) {
        FacturaClienteCabTarjConvenio fac = new FacturaClienteCabTarjConvenio();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public static FacturaClienteCabTarjConvenio fromFacturaClienteCabTarjConvenioAsociadoDTO(
            FacturaClienteCabTarjConvenioDTO tarDTO) {
        FacturaClienteCabTarjConvenio fac = fromFacturaClienteCabTarjConvenioDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setTarjetaConvenio(TarjetaConvenio.fromTarjetaConvenioDTO(tarDTO
                .getTarjetaConvenio()));
        return fac;
    }

}
