package com.peluqueria.core.domain;

import com.peluqueria.dto.MarcaDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the marca database table.
 *
 */
@Entity
@Table(name = "marca", schema = "stock")
@NamedQuery(name = "Marca.findAll", query = "SELECT m FROM Marca m")
public class Marca implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_marca")
    private Long idMarca;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "marca")
    private List<Articulo> articulos;

    public Marca() {
    }

    public Long getIdMarca() {
        return this.idMarca;
    }

    public void setIdMarca(Long idMarca) {
        this.idMarca = idMarca;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Articulo> getArticulos() {
        return this.articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    public MarcaDTO toMarcaDTO() {
        MarcaDTO marcaDTO = toMarcaDTO(this);
        marcaDTO.setArticulos(null);
        return marcaDTO;
    }

    public static MarcaDTO toMarcaDTO(Marca marca) {
        MarcaDTO marcaDTO = new MarcaDTO();
        BeanUtils.copyProperties(marca, marcaDTO);
        return marcaDTO;
    }

    public static Marca fromMarcaDTO(MarcaDTO marcaDTO) {
        Marca marca = new Marca();
        BeanUtils.copyProperties(marcaDTO, marca);
        return marca;
    }

}
