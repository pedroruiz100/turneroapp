package com.peluqueria.core.domain;

import com.peluqueria.dto.IpCajaDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the ip_caja database table.
 *
 */
@Entity
@Table(name = "ip_caja", schema = "general")
@NamedQuery(name = "IpCaja.findAll", query = "SELECT i FROM IpCaja i")
public class IpCaja implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ip_caja")
    private Long idIpCaja;

    @Column(name = "ip_caja")
    private String ipCaja;

    public IpCaja() {
    }

    public Long getIdIpCaja() {
        return this.idIpCaja;
    }

    public void setIdIpCaja(Long idIpCaja) {
        this.idIpCaja = idIpCaja;
    }

    public String getIpCaja() {
        return this.ipCaja;
    }

    public void setIpCaja(String ipCaja) {
        this.ipCaja = ipCaja;
    }

    //SIN OTROS DATOS, IGUAL A LA BD
    public IpCajaDTO toBDIpCajaDTO() {
        IpCajaDTO ipDTO = toIpCajaDTO(this);
        return ipDTO;
    }

    public static IpCajaDTO toIpCajaDTO(IpCaja ip) {
        IpCajaDTO ipCajaDTO = new IpCajaDTO();
        BeanUtils.copyProperties(ip, ipCajaDTO);
        return ipCajaDTO;
    }

    public static IpCaja fromIpCajaDTO(IpCajaDTO ipCajaDTO) {
        IpCaja ipCaja = new IpCaja();
        BeanUtils.copyProperties(ipCajaDTO, ipCaja);
        return ipCaja;
    }

}
