package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf4DTO;

/**
 * The persistent class for the id_descuento_tarjeta_cab_nf4 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf4", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf4.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf4 d")
public class DescuentoTarjetaCabNf4 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf4")
    private Long idDescuentoTarjetaCabNf4;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    public DescuentoTarjetaCabNf4() {
    }

    public Long getIdDescuentoTarjetaCabNf4() {
        return this.idDescuentoTarjetaCabNf4;
    }

    public void setIdDescuentoTarjetaCabNf4(Long idDescuentoTarjetaCabNf4) {
        this.idDescuentoTarjetaCabNf4 = idDescuentoTarjetaCabNf4;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf4 fromDescuentoTarjetaCabNf4DTO(DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO) {
        DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4 = new DescuentoTarjetaCabNf4();
        BeanUtils.copyProperties(descuentoTarjetaCabNf4DTO, descuentoTarjetaCabNf4);
        return descuentoTarjetaCabNf4;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf4DTO toDescuentoTarjetaCabNf4DTO(DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4) {
        DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO = new DescuentoTarjetaCabNf4DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf4, descuentoTarjetaCabNf4DTO);
        return descuentoTarjetaCabNf4DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf4 fromDescuentoTarjetaCabNf4DTOEntitiesFull(DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO) {
        DescuentoTarjetaCabNf4 descuentoTarjetaCabNf4 = fromDescuentoTarjetaCabNf4DTO(descuentoTarjetaCabNf4DTO);
        if (descuentoTarjetaCabNf4DTO.getNf4Seccion1() != null) {
            descuentoTarjetaCabNf4.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(descuentoTarjetaCabNf4DTO.getNf4Seccion1()));
        }
        if (descuentoTarjetaCabNf4DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf4
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf4DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf4;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf4DTO toDescuentoTarjetaCabNf4DTOEntityFull() {
        DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO = toDescuentoTarjetaCabNf4DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf4DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf4DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf4Seccion1() != null) {
            descuentoTarjetaCabNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            descuentoTarjetaCabNf4DTO.setNf4Seccion1(null);
        }
        return descuentoTarjetaCabNf4DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf4DTO toDescuentoTarjetaCabNf4DTOEntityNull() {
        DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO = toDescuentoTarjetaCabNf4DTO(this);
        descuentoTarjetaCabNf4DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf4DTO.setNf4Seccion1(null);
        return descuentoTarjetaCabNf4DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf4DTO toDescuentoTarjetaCabNf4DTONf3() {
        DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO = toDescuentoTarjetaCabNf4DTO(this);
        descuentoTarjetaCabNf4DTO.setDescuentoTarjetaCab(null);
        if (this.getNf4Seccion1() != null) {
            descuentoTarjetaCabNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            descuentoTarjetaCabNf4DTO.setNf4Seccion1(null);
        }
        return descuentoTarjetaCabNf4DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
