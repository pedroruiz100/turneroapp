package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf4DTO;

/**
 * The persistent class for the promo_temporada_nf4 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf4", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf4.findAll", query = "SELECT p FROM PromoTemporadaNf4 p")
public class PromoTemporadaNf4 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf4")
    private Long idPromoTemporadaNf4;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    public PromoTemporadaNf4() {
    }

    public Long getIdPromoTemporadaNf4() {
        return this.idPromoTemporadaNf4;
    }

    public void setIdPromoTemporadaNf4(Long idPromoTemporadaNf4) {
        this.idPromoTemporadaNf4 = idPromoTemporadaNf4;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf4 fromPromoTemporadaNf4DTO(PromoTemporadaNf4DTO promoTemporadaNf4DTO) {
        PromoTemporadaNf4 promoTemporadaNf4 = new PromoTemporadaNf4();
        BeanUtils.copyProperties(promoTemporadaNf4DTO, promoTemporadaNf4);
        return promoTemporadaNf4;
    }

    // <-- SERVER
    public static PromoTemporadaNf4DTO toPromoTemporadaNf4DTO(PromoTemporadaNf4 promoTemporadaNf4) {
        PromoTemporadaNf4DTO promoTemporadaNf4DTO = new PromoTemporadaNf4DTO();
        BeanUtils.copyProperties(promoTemporadaNf4, promoTemporadaNf4DTO);
        return promoTemporadaNf4DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf4 fromPromoTemporadaNf4DTOEntitiesFull(PromoTemporadaNf4DTO promoTemporadaNf4DTO) {
        PromoTemporadaNf4 promoTemporadaNf4 = fromPromoTemporadaNf4DTO(promoTemporadaNf4DTO);
        if (promoTemporadaNf4DTO.getNf4Seccion1() != null) {
            promoTemporadaNf4.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(promoTemporadaNf4DTO.getNf4Seccion1()));
        }
        if (promoTemporadaNf4DTO.getPromoTemporada() != null) {
            promoTemporadaNf4
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf4DTO.getPromoTemporada()));
        }
        return promoTemporadaNf4;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf4DTO toPromoTemporadaNf4DTOEntityFull() {
        PromoTemporadaNf4DTO promoTemporadaNf4DTO = toPromoTemporadaNf4DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf4DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf4DTO.setPromoTemporada(null);
        }
        if (this.getNf4Seccion1() != null) {
            promoTemporadaNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            promoTemporadaNf4DTO.setNf4Seccion1(null);
        }
        return promoTemporadaNf4DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf4DTO toPromoTemporadaNf4DTOEntityNull() {
        PromoTemporadaNf4DTO promoTemporadaNf4DTO = toPromoTemporadaNf4DTO(this);
        promoTemporadaNf4DTO.setPromoTemporada(null);
        promoTemporadaNf4DTO.setNf4Seccion1(null);
        return promoTemporadaNf4DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf4DTO toPromoTemporadaNf4DTONf3() {
        PromoTemporadaNf4DTO promoTemporadaNf4DTO = toPromoTemporadaNf4DTO(this);
        promoTemporadaNf4DTO.setPromoTemporada(null);
        if (this.getNf4Seccion1() != null) {
            promoTemporadaNf4DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            promoTemporadaNf4DTO.setNf4Seccion1(null);
        }
        return promoTemporadaNf4DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
