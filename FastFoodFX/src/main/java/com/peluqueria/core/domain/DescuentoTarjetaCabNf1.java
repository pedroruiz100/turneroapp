package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf1DTO;

/**
 * The persistent class for the descuento_tarjeta_cab_nf1 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf1", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf1.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf1 d")
public class DescuentoTarjetaCabNf1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf1")
    private Long idDescuentoTarjetaCabNf1;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    public DescuentoTarjetaCabNf1() {
    }

    public Long getIdDescuentoTarjetaCabNf1() {
        return this.idDescuentoTarjetaCabNf1;
    }

    public void setIdDescuentoTarjetaCabNf1(Long idDescuentoTarjetaCabNf1) {
        this.idDescuentoTarjetaCabNf1 = idDescuentoTarjetaCabNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf1Tipo getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf1 fromDescuentoTarjetaCabNf1DTO(DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO) {
        DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1 = new DescuentoTarjetaCabNf1();
        BeanUtils.copyProperties(descuentoTarjetaCabNf1DTO, descuentoTarjetaCabNf1);
        return descuentoTarjetaCabNf1;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf1DTO toDescuentoTarjetaCabNf1DTO(DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1) {
        DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO = new DescuentoTarjetaCabNf1DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf1, descuentoTarjetaCabNf1DTO);
        return descuentoTarjetaCabNf1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf1 fromDescuentoTarjetaCabNf1DTOEntitiesFull(DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO) {
        DescuentoTarjetaCabNf1 descuentoTarjetaCabNf1 = fromDescuentoTarjetaCabNf1DTO(descuentoTarjetaCabNf1DTO);
        if (descuentoTarjetaCabNf1DTO.getNf1Tipo() != null) {
            descuentoTarjetaCabNf1.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(descuentoTarjetaCabNf1DTO.getNf1Tipo()));
        }
        if (descuentoTarjetaCabNf1DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf1
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf1DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf1;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf1DTO toDescuentoTarjetaCabNf1DTOEntityFull() {
        DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO = toDescuentoTarjetaCabNf1DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf1DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf1DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf1Tipo() != null) {
            descuentoTarjetaCabNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf1DTO.setNf1Tipo(null);
        }
        return descuentoTarjetaCabNf1DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf1DTO toDescuentoTarjetaCabNf1DTOEntityNull() {
        DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO = toDescuentoTarjetaCabNf1DTO(this);
        descuentoTarjetaCabNf1DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf1DTO.setNf1Tipo(null);
        return descuentoTarjetaCabNf1DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf1DTO toDescuentoTarjetaCabNf1DTO() {
        DescuentoTarjetaCabNf1DTO descuentoFielNf1DTO = toDescuentoTarjetaCabNf1DTO(this);
        descuentoFielNf1DTO.setDescuentoTarjetaCab(null);
        if (this.getNf1Tipo() != null) {
            descuentoFielNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            descuentoFielNf1DTO.setNf1Tipo(null);
        }
        return descuentoFielNf1DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
