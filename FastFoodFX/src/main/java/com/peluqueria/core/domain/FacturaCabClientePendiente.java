package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaCabClientePendienteDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;


/**
 * The persistent class for the rol database table.
 *
 */
@Entity
@Table(name = "factura_cab_cliente_pendiente", schema = "factura_cliente")
@NamedQuery(name = "FacturaCabClientePendiente.findAll", query = "SELECT r FROM FacturaCabClientePendiente r")
public class FacturaCabClientePendiente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_fact_pendiente")
    private Long idFactPendiente;

    // bi-directional many-to-one association to ClientePendiente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente_pendiente")
    private ClientePendiente clientePendiente;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public FacturaCabClientePendiente() {
    }

    public Long getIdFactPendiente() {
        return idFactPendiente;
    }

    public void setIdFactPendiente(Long idFactPendiente) {
        this.idFactPendiente = idFactPendiente;
    }

    public ClientePendiente getClientePendiente() {
        return clientePendiente;
    }

    public void setClientePendiente(ClientePendiente clientePendiente) {
        this.clientePendiente = clientePendiente;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public FacturaCabClientePendienteDTO toFacturaCabClientePendienteDTO() {
        FacturaCabClientePendienteDTO facDTO = toFacturaCabClientePendienteDTO(this);
        if (this.clientePendiente != null) {
            facDTO.setClientePendiente(this.getClientePendiente()
                    .toBDClientePendienteDTO());
        }
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    public FacturaCabClientePendienteDTO toBDFacturaCabClientePendienteDTO() {
        FacturaCabClientePendienteDTO facDTO = toFacturaCabClientePendienteDTO(this);
        facDTO.setClientePendiente(null);
        facDTO.setFacturaClienteCab(null);
        return facDTO;
    }

    private FacturaCabClientePendienteDTO toFacturaCabClientePendienteDTO(
            FacturaCabClientePendiente facturaCabClientePendiente) {
        FacturaCabClientePendienteDTO facDTO = new FacturaCabClientePendienteDTO();
        BeanUtils.copyProperties(facturaCabClientePendiente, facDTO);
        return facDTO;
    }

    public static FacturaCabClientePendiente fromFacturaCabClientePendiente(
            FacturaCabClientePendienteDTO facDTO) {
        FacturaCabClientePendiente fac = new FacturaCabClientePendiente();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaCabClientePendiente fromFacturaCabClientePendienteAsociado(
            FacturaCabClientePendienteDTO facDTO) {
        FacturaCabClientePendiente fac = fromFacturaCabClientePendiente(facDTO);
        fac.setClientePendiente(ClientePendiente.fromClientePendiente(facDTO
                .getClientePendiente()));
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }
}
