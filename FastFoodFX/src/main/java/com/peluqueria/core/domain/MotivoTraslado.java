package com.peluqueria.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the motivo_traslado database table.
 *
 */
@Entity
@Table(name = "motivo_traslado", schema = "factura_cliente")
@NamedQuery(name = "MotivoTraslado.findAll", query = "SELECT m FROM MotivoTraslado m")
public class MotivoTraslado implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motivo_traslado")
    private Long idMotivoTraslado;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    //bi-directional many-to-one association to NotaRemisionCab
    @OneToMany(mappedBy = "motivoTraslado", fetch = FetchType.LAZY)
    private List<NotaRemisionCab> notaRemisionCabs;

    public MotivoTraslado() {
    }

    public Long getIdMotivoTraslado() {
        return this.idMotivoTraslado;
    }

    public void setIdMotivoTraslado(Long idMotivoTraslado) {
        this.idMotivoTraslado = idMotivoTraslado;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<NotaRemisionCab> getNotaRemisionCabs() {
        return this.notaRemisionCabs;
    }

    public void setNotaRemisionCabs(List<NotaRemisionCab> notaRemisionCabs) {
        this.notaRemisionCabs = notaRemisionCabs;
    }

    public NotaRemisionCab addNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
        getNotaRemisionCabs().add(notaRemisionCab);
        notaRemisionCab.setMotivoTraslado(this);
        return notaRemisionCab;
    }

    public NotaRemisionCab removeNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
        getNotaRemisionCabs().remove(notaRemisionCab);
        notaRemisionCab.setMotivoTraslado(null);
        return notaRemisionCab;
    }

}
