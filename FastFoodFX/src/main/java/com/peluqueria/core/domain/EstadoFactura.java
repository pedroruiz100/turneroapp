package com.peluqueria.core.domain;

import com.peluqueria.dto.EstadoFacturaDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the estado_factura database table.
 *
 */
@Entity
@Table(name = "estado_factura", schema = "factura_cliente")
@NamedQuery(name = "EstadoFactura.findAll", query = "SELECT e FROM EstadoFactura e")
public class EstadoFactura implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_estado_factura")
    private Long idEstadoFactura;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "estadoFactura")
    private List<FacturaClienteCab> facturaClienteCabs;

    public EstadoFactura() {
    }

    public Long getIdEstadoFactura() {
        return this.idEstadoFactura;
    }

    public void setIdEstadoFactura(Long idEstadoFactura) {
        this.idEstadoFactura = idEstadoFactura;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCab> getFacturaClienteCabs() {
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCab> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public EstadoFacturaDTO toEstadoFacturaDTO() {
        EstadoFacturaDTO eDTO = toEstadoFacturaDTO(this);
        eDTO.setFacturaClienteCabs(null);
        return eDTO;
    }

    public static EstadoFacturaDTO toEstadoFacturaDTO(
            EstadoFactura estadoFactura) {
        EstadoFacturaDTO eDTO = new EstadoFacturaDTO();
        BeanUtils.copyProperties(estadoFactura, eDTO);
        return eDTO;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que tenga)
    public static EstadoFactura fromEstadoFacturaDTO(
            EstadoFacturaDTO estadoFacturaDTO) {
        EstadoFactura e = new EstadoFactura();
        BeanUtils.copyProperties(estadoFacturaDTO, e);
        return e;
    }

}
