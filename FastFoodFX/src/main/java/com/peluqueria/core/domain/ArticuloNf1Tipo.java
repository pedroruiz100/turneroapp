package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf1TipoDTO;

/**
 * The persistent class for the articulo_nf1_tipo database table.
 *
 */
@Entity
@Table(name = "articulo_nf1_tipo", schema = "stock")
@NamedQuery(name = "ArticuloNf1Tipo.findAll", query = "SELECT a FROM ArticuloNf1Tipo a")
public class ArticuloNf1Tipo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf1_tipo_articulo")
    private Long idNf1TipoArticulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    public ArticuloNf1Tipo() {
    }

    public Long getIdNf1TipoArticulo() {
        return this.idNf1TipoArticulo;
    }

    public void setIdNf1TipoArticulo(Long idNf1TipoArticulo) {
        this.idNf1TipoArticulo = idNf1TipoArticulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Nf1Tipo getNf1Tipo() {
        return this.nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf1Tipo fromArticuloNf1TipoDTO(ArticuloNf1TipoDTO articuloNf1TipoDTO) {
        ArticuloNf1Tipo articuloNf1Tipo = new ArticuloNf1Tipo();
        BeanUtils.copyProperties(articuloNf1TipoDTO, articuloNf1Tipo);
        return articuloNf1Tipo;
    }

    // <-- SERVER
    public static ArticuloNf1TipoDTO toArticuloNf1TipoDTO(ArticuloNf1Tipo articuloNf1Tipo) {
        ArticuloNf1TipoDTO articuloNf1TipoDTO = new ArticuloNf1TipoDTO();
        BeanUtils.copyProperties(articuloNf1Tipo, articuloNf1TipoDTO);
        return articuloNf1TipoDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf1TipoDTO toArticuloNf1TipoDTOEntitiesNull(ArticuloNf1Tipo articuloNf1Tipo) {
        ArticuloNf1TipoDTO articuloNf1TipoDTO = toArticuloNf1TipoDTO(articuloNf1Tipo);
        articuloNf1TipoDTO.setArticulo(null);
        articuloNf1TipoDTO.setNf1Tipo(null);
        return articuloNf1TipoDTO;
    }

    // <-- SERVER
    public ArticuloNf1TipoDTO toArticuloNf1TipoDTOEntitiesNull() {
        ArticuloNf1TipoDTO articuloNf1TipoDTO = toArticuloNf1TipoDTO(this);
        articuloNf1TipoDTO.setArticulo(null);
        articuloNf1TipoDTO.setNf1Tipo(null);
        return articuloNf1TipoDTO;
    }

    // <-- SERVER
    public ArticuloNf1TipoDTO toArticuloNf1TipoDTOEntitiesNf() {
        ArticuloNf1TipoDTO articuloNf1TipoDTO = toArticuloNf1TipoDTO(this);
        articuloNf1TipoDTO.setArticulo(null);
        if (this.getNf1Tipo() != null) {
            articuloNf1TipoDTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        } else {
            articuloNf1TipoDTO.setNf1Tipo(null);
        }
        return articuloNf1TipoDTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf1Tipo fromArticuloNf1TipoDTOEntitiesFull(ArticuloNf1TipoDTO articuloNf1TipoDTO) {
        ArticuloNf1Tipo articuloNf1Tipo = fromArticuloNf1TipoDTO(articuloNf1TipoDTO);
        if (articuloNf1TipoDTO.getArticulo() != null) {
            articuloNf1Tipo.setArticulo(Articulo.fromArticuloDTO(articuloNf1TipoDTO.getArticulo()));
        }
        if (articuloNf1TipoDTO.getNf1Tipo() != null) {
            articuloNf1Tipo.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(articuloNf1TipoDTO.getNf1Tipo()));
        }
        return articuloNf1Tipo;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
