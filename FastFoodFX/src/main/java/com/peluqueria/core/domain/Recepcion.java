package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.RecepcionDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the apertura_caja database table.
 *
 */
@Entity
@Table(name = "recepcion", schema = "stock")
@NamedQuery(name = "Recepcion.findAll", query = "SELECT a FROM Recepcion a")
public class Recepcion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_recepcion")
    private Long idRecepcion;

    @Column(name = "tipo_movimiento")
    private String tipoMovimiento;

    @Column(name = "nro_orden")
    private String nroOrden;

    @Column(name = "nro_doc")
    private String nroDoc;

    @Column(name = "fecha_doc")
    private Date fechaDoc;

    @Column(name = "sucursal")
    private String sucursal;
    @Column(name = "saldo")
    private int saldo;

    @Column(name = "observacion")
    private String observacion;

    @Column(name = "tipo_documento")
    private String tipoDocumento;

    @Column(name = "clase_documento")
    private String claseDocumento;

    @Column(name = "estado_factura")
    private String estadoFactura;

    @Column(name = "orden_devo")
    private String ordenDevo;

    @Column(name = "orden_devosob")
    private String ordenDevosob;

    @Column(name = "cant_rec")
    private String cantRec;

    @Column(name = "cant_av")
    private String cantAv;

    @Column(name = "cant_falt")
    private String cantFalt;

    @Column(name = "cant_sob")
    private String cantSob;

    @Column(name = "cant_nev")
    private String cantNev;

    @Column(name = "total")
    private int total;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_proveedor")
    private Proveedor proveedor;

    // bi-directional many-to-one association to Caja
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_cab")
    private PedidoCab pedidoCab;

    public Recepcion() {
    }

    public Long getIdRecepcion() {
        return idRecepcion;
    }

    public void setIdRecepcion(Long idRecepcion) {
        this.idRecepcion = idRecepcion;
    }

    public String getTipoMovimiento() {
        return tipoMovimiento;
    }

    public void setTipoMovimiento(String tipoMovimiento) {
        this.tipoMovimiento = tipoMovimiento;
    }

    public String getNroOrden() {
        return nroOrden;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public void setNroOrden(String nroOrden) {
        this.nroOrden = nroOrden;
    }

    public int getSaldo() {
        return saldo;
    }

    public void setSaldo(int saldo) {
        this.saldo = saldo;
    }

    public String getEstadoFactura() {
        return estadoFactura;
    }

    public PedidoCab getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCab pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public void setEstadoFactura(String estadoFactura) {
        this.estadoFactura = estadoFactura;
    }

    public String getOrdenDevo() {
        return ordenDevo;
    }

    public void setOrdenDevo(String ordenDevo) {
        this.ordenDevo = ordenDevo;
    }

    public String getOrdenDevosob() {
        return ordenDevosob;
    }

    public void setOrdenDevosob(String ordenDevosob) {
        this.ordenDevosob = ordenDevosob;
    }

    public String getCantRec() {
        return cantRec;
    }

    public void setCantRec(String cantRec) {
        this.cantRec = cantRec;
    }

    public String getCantAv() {
        return cantAv;
    }

    public void setCantAv(String cantAv) {
        this.cantAv = cantAv;
    }

    public String getCantFalt() {
        return cantFalt;
    }

    public void setCantFalt(String cantFalt) {
        this.cantFalt = cantFalt;
    }

    public String getCantSob() {
        return cantSob;
    }

    public void setCantSob(String cantSob) {
        this.cantSob = cantSob;
    }

    public String getCantNev() {
        return cantNev;
    }

    public void setCantNev(String cantNev) {
        this.cantNev = cantNev;
    }

    public String getNroDoc() {
        return nroDoc;
    }

    public void setNroDoc(String nroDoc) {
        this.nroDoc = nroDoc;
    }

    public Date getFechaDoc() {
        return fechaDoc;
    }

    public void setFechaDoc(Date fechaDoc) {
        this.fechaDoc = fechaDoc;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getTipoDocumento() {
        return tipoDocumento;
    }

    public void setTipoDocumento(String tipoDocumento) {
        this.tipoDocumento = tipoDocumento;
    }

    public String getClaseDocumento() {
        return claseDocumento;
    }

    public void setClaseDocumento(String claseDocumento) {
        this.claseDocumento = claseDocumento;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    // DTOs****************************************************************************************
    // RELACION CON UsuarioSupervisor
//    public AperturaCajaDTO toAperturaCajaDTO() {
//        AperturaCajaDTO apCajaDTO = this.toAperturaCajaDTO(this);
//        return apCajaDTO;
//    }
    public RecepcionDTO toRecepcionDTO() {
        RecepcionDTO apDTO = toRecepcionDTO(this);
//        apDTO.set(null);
//        apDTO.setUsuarioCajero(null);
        return apDTO;
    }

    public RecepcionDTO toRecepcionDTO(Recepcion ap) {
        RecepcionDTO acDTO = new RecepcionDTO();
        BeanUtils.copyProperties(ap, acDTO);
        return acDTO;
    }

    // METODO QUE
    // Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
    // CAJERO Y USUARIO SUPERVISOR
    public static Recepcion fromAperturaCajaDTO(RecepcionDTO dto) {
        Recepcion apCa = new Recepcion();
        BeanUtils.copyProperties(dto, apCa);
        apCa.setProveedor(Proveedor.fromProveedorDTO(dto.getProveedor()));
//        apCa.setCaja(Caja.fromCajaDTO(dto.getCaja()));
//        apCa.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(dto
//                .getUsuarioCajero()));
        return apCa;
    }
}
