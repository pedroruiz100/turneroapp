package com.peluqueria.core.domain;

import com.peluqueria.dto.RolDTO;
import com.peluqueria.dto.RolFuncionDTO;
import com.peluqueria.dto.UsuarioRolDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rol database table.
 *
 */
@Entity
@Table(name = "rol", schema = "seguridad")
@NamedQuery(name = "Rol.findAll", query = "SELECT r FROM Rol r")
public class Rol implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rol")
    private Long idRol;

    private Boolean activo;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    private String tipo;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to RolFuncion
    @OneToMany(mappedBy = "rol", fetch = FetchType.LAZY)
    private List<RolFuncion> rolFuncions;

    // bi-directional many-to-one association to UsuarioRol
    @OneToMany(mappedBy = "rol", fetch = FetchType.LAZY)
    private List<UsuarioRol> usuarioRols;

    public Rol() {
    }

    public Long getIdRol() {
        return this.idRol;
    }

    public void setIdRol(Long idRol) {
        this.idRol = idRol;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getTipo() {
        return this.tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<RolFuncion> getRolFuncions() {
        return this.rolFuncions;
    }

    public void setRolFuncions(List<RolFuncion> rolFuncions) {
        this.rolFuncions = rolFuncions;
    }

    public RolFuncion addRolFuncion(RolFuncion rolFuncion) {
        getRolFuncions().add(rolFuncion);
        rolFuncion.setRol(this);

        return rolFuncion;
    }

    public RolFuncion removeRolFuncion(RolFuncion rolFuncion) {
        getRolFuncions().remove(rolFuncion);
        rolFuncion.setRol(null);
        return rolFuncion;
    }

    public List<UsuarioRol> getUsuarioRols() {
        return this.usuarioRols;
    }

    public void setUsuarioRols(List<UsuarioRol> usuarioRols) {
        this.usuarioRols = usuarioRols;
    }

    public UsuarioRol addUsuarioRol(UsuarioRol usuarioRol) {
        getUsuarioRols().add(usuarioRol);
        usuarioRol.setRol(this);
        return usuarioRol;
    }

    public UsuarioRol removeUsuarioRol(UsuarioRol usuarioRol) {
        getUsuarioRols().remove(usuarioRol);
        usuarioRol.setRol(null);
        return usuarioRol;
    }

    public static UsuarioRolDTO toUsuarioRolDTO(UsuarioRol usuarioRol) {
        UsuarioRolDTO usuarioRolDTO = new UsuarioRolDTO();
        BeanUtils.copyProperties(usuarioRol, usuarioRolDTO);
        return usuarioRolDTO;
    }

    public RolDTO toBDRolDTO() {
        RolDTO rolDTO = toRolDTO(this);
        rolDTO.setRolFuncions(null);
        rolDTO.setUsuarioRols(null);
        return rolDTO;
    }

    public RolDTO toRolDTO() {
        RolDTO rolDTO = toRolDTO(this);
        if (!this.rolFuncions.isEmpty()) {
            List<RolFuncion> rf = new ArrayList<RolFuncion>();
            List<RolFuncionDTO> rfDTO = new ArrayList<RolFuncionDTO>();
            for (RolFuncion roFu : rf) {
                rfDTO.add(roFu.toRolFuncionDTO());
                //QUEDAMOS AQUI
            }
        }
        if (!this.usuarioRols.isEmpty()) {
        }
        return rolDTO;
    }

    public static RolDTO toRolDTO(Rol rol) {
        RolDTO rolDTO = new RolDTO();
        BeanUtils.copyProperties(rol, rolDTO);
        return rolDTO;
    }

    public static Rol fromRolDTO(RolDTO rolDTO) {
        Rol rol = new Rol();
        BeanUtils.copyProperties(rolDTO, rol);
        return rol;
    }

    public static RolDTO toBDRolDTO(Rol rol) {
        RolDTO rolDTO = toRolDTO(rol);
        rolDTO.setRolFuncions(null);
        rolDTO.setUsuarioRols(null);
        return rolDTO;
    }

    public RolDTO toRolSinFechaDTO() {
        RolDTO rolDTO = toRolDTO(this);
        if (!this.rolFuncions.isEmpty()) {
            List<RolFuncion> rf = this.getRolFuncions();
            List<RolFuncionDTO> rfDTO = new ArrayList<RolFuncionDTO>();
            for (RolFuncion roFu : rf) {
                RolFuncionDTO rfuncionDTO = roFu.toRolFuncionDTO();
                rfuncionDTO.setFechaAlta(null);
                rfuncionDTO.setFechaMod(null);
                rfuncionDTO.getRol().setFechaAlta(null);
                rfuncionDTO.getRol().setFechaMod(null);
                rfDTO.add(rfuncionDTO);
                //QUEDAMOS AQUI
            }
            rolDTO.setRolFuncions(rfDTO);
        }
        return rolDTO;
    }

}
