package com.peluqueria.core.domain;

import com.peluqueria.dto.FuncionarioDTO;
import com.peluqueria.dto.UsuarioDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the funcionario database table.
 *
 */
@Entity
@Table(name = "funcionario", schema = "rrhh")
@NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")
public class Funcionario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario")
    private Long idFuncionario;

    @Column(name = "nombre")
    private String nombre;

    @Column(name = "apellido")
    private String apellido;

    @Column(name = "activo")
    private Boolean activo;

    @Column(name = "habilitado")
    private Boolean habilitado;

    @Column(name = "ci")
    private String ci;

    // bi-directional many-to-one association to Usuario
    @OneToMany(mappedBy = "funcionario", fetch = FetchType.LAZY)
    private List<Usuario> usuario;

    // bi-directional many-to-one association to ServPendiente
    @OneToMany(mappedBy = "funcionario", fetch = FetchType.LAZY)
    private List<ServPendiente> servPendiente;

    public Funcionario() {
    }

    public Long getIdFuncionario() {
        return this.idFuncionario;
    }

    public void setIdFuncionario(Long idFuncionario) {
        this.idFuncionario = idFuncionario;
    }

    // ********************************************************************************
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    // ********************************************************************************
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Boolean getActivo() {
        return activo;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(Boolean habilitado) {
        this.habilitado = habilitado;
    }

    public List<Usuario> getUsuario() {
        return usuario;
    }

    public void setUsuario(List<Usuario> usuario) {
        this.usuario = usuario;
    }

    public List<ServPendiente> getServPendiente() {
        return servPendiente;
    }

    public void setServPendiente(List<ServPendiente> servPendiente) {
        this.servPendiente = servPendiente;
    }

    public FuncionarioDTO toFuncionarioDTO() {
        FuncionarioDTO funDTO = toFuncionarioDTO(this);
        funDTO.setUsuario(null);
        funDTO.setServPendiente(null);
        return funDTO;
    }

    public FuncionarioDTO toBDFuncionarioDTO() {
        FuncionarioDTO funDTO = toFuncionarioDTO(this);
        if (!this.getUsuario().isEmpty()) {
            List<UsuarioDTO> lista = new ArrayList<UsuarioDTO>();
            for (Usuario u : this.getUsuario()) {
                lista.add(u.toBDUsuarioDTO());
            }
            funDTO.setUsuario(lista);
        }
        funDTO.setServPendiente(null);
        return funDTO;
    }

    public static FuncionarioDTO toFuncionarioDTO(Funcionario funcionario) {
        FuncionarioDTO funDTO = new FuncionarioDTO();
        BeanUtils.copyProperties(funcionario, funDTO);
        return funDTO;
    }

    public static Funcionario fromFuncionarioDTO(FuncionarioDTO funDTO) {
        Funcionario fun = new Funcionario();
        BeanUtils.copyProperties(funDTO, fun);
        return fun;
    }

    public static Funcionario fromFuncionarioSeguridadDTO(FuncionarioDTO funDTO) {
        Funcionario fun = fromFuncionarioDTO(funDTO);
        fun.setUsuario(null);
        funDTO.setServPendiente(null);
        return fun;
    }

    public static FuncionarioDTO toFuncionarioSeguridadDTO(
            Funcionario funcionario) {
        FuncionarioDTO funDTO = toFuncionarioDTO(funcionario);
        funDTO.setUsuario(null);
        funDTO.setServPendiente(null);
        return funDTO;
    }

}
