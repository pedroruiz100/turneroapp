package com.peluqueria.core.domain;

import com.peluqueria.dto.CancelFactDTO;
import com.peluqueria.dto.CancelacionFacturaDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the cancelacion_factura database table.
 */
@Entity
@Table(name = "cancel_fact", schema = "caja")
@NamedQuery(name = "CancelFact.findAll", query = "SELECT c FROM CancelFact c")
public class CancelFact implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cancelacion_factura")
    private Long idCancelacionFactura;

    @Column(name = "fecha_cancelacion")
    private Timestamp fechaCancelacion;

    @Column(name = "nro_factura")
    private String nroFactura;

    @Column(name = "monto")
    private Long monto;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cajero")
    private Usuario usuarioCajero;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_supervisor")
    private Usuario usuarioSupervisor;

    // bi-directional many-to-one association to MotivoCancelacionFactura
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_motivo_cancelacion")
    private MotivoCancelacionFactura motivoCancelacionFactura;

    // bi-directional many-to-one association to CancelFact
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    public CancelFact() {
    }

    public Long getIdCancelacionFactura() {
        return idCancelacionFactura;
    }

    public void setIdCancelacionFactura(Long idCancelacionFactura) {
        this.idCancelacionFactura = idCancelacionFactura;
    }

    public Timestamp getFechaCancelacion() {
        return fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public String getNroFactura() {
        return nroFactura;
    }

    public void setNroFactura(String nroFactura) {
        this.nroFactura = nroFactura;
    }

    public Long getMonto() {
        return monto;
    }

    public void setMonto(Long monto) {
        this.monto = monto;
    }

    public Usuario getUsuarioCajero() {
        return usuarioCajero;
    }

    public void setUsuarioCajero(Usuario usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public Usuario getUsuarioSupervisor() {
        return usuarioSupervisor;
    }

    public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public MotivoCancelacionFactura getMotivoCancelacionFactura() {
        return motivoCancelacionFactura;
    }

    public void setMotivoCancelacionFactura(MotivoCancelacionFactura motivoCancelacionFactura) {
        this.motivoCancelacionFactura = motivoCancelacionFactura;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public CancelFactDTO toCancelFactDTO() {
        CancelFactDTO canDTO = toCancelFactDTO(this);

        if (this.sucursal != null) {
            canDTO.setSucursal(this.getSucursal()
                    .toSucursalDescriDTO());
        }

        if (this.motivoCancelacionFactura != null) {
            canDTO.setMotivoCancelacionFactura(this
                    .getMotivoCancelacionFactura()
                    .toBDMotivoCancelacionFacturaDTO());
        }

        if (this.usuarioCajero != null) {
            canDTO.setUsuarioCajero(this.getUsuarioCajero().toBDUsuarioDTO());
        }

        if (this.usuarioSupervisor != null) {
            canDTO.setUsuarioSupervisor(this.getUsuarioSupervisor()
                    .toBDUsuarioDTO());
        }
        return canDTO;
    }

    public static CancelFactDTO toCancelFactDTO(
            CancelFact cancelacionFactura) {
        CancelFactDTO canDTO = new CancelFactDTO();
        BeanUtils.copyProperties(cancelacionFactura, canDTO);
        return canDTO;
    }

    public CancelFactDTO toBDCancelacionFacturaDTO() {
        CancelFactDTO canDTO = toCancelFactDTO(this);
        canDTO.setFacturaClienteCab(null);
        canDTO.setMotivoCancelacionFactura(null);
        canDTO.setUsuarioCajero(null);
        canDTO.setUsuarioSupervisor(null);
        canDTO.setSucursal(null);
        return canDTO;
    }

    public static CancelFact fromCancelacionFacturaDTO(
            CancelFactDTO canDTO) {
        CancelFact can = new CancelFact();
        BeanUtils.copyProperties(canDTO, can);
        return can;
    }

    public static CancelFact fromCancelacionFacturaAsociadoDTO(
            CancelFactDTO canDTO) {
        CancelFact can = fromCancelacionFacturaDTO(canDTO);
        can.setSucursal(Sucursal
                .fromSucursalDTO(canDTO.getSucursal()));
        can.setMotivoCancelacionFactura(MotivoCancelacionFactura
                .fromMotivoCancelacionFacturaDTO(canDTO
                        .getMotivoCancelacionFactura()));
        can.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(canDTO
                .getUsuarioCajero()));
        can.setUsuarioSupervisor(Usuario.fromUsuarioSupervisorDTO(canDTO
                .getUsuarioSupervisor()));
        return can;
    }

}
