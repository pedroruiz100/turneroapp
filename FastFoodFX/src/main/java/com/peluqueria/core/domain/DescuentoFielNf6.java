package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf6DTO;

/**
 * The persistent class for the descuento_fiel_nf6 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf6", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf6.findAll", query = "SELECT d FROM DescuentoFielNf6 d")
public class DescuentoFielNf6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf6")
    private Long idDescuentoFielNf6;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    public DescuentoFielNf6() {
    }

    public Long getIdDescuentoFielNf6() {
        return this.idDescuentoFielNf6;
    }

    public void setIdDescuentoFielNf6(Long idDescuentoFielNf6) {
        this.idDescuentoFielNf6 = idDescuentoFielNf6;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf6 fromDescuentoFielNf6DTO(DescuentoFielNf6DTO descuentoFielNf6DTO) {
        DescuentoFielNf6 descuentoFielNf6 = new DescuentoFielNf6();
        BeanUtils.copyProperties(descuentoFielNf6DTO, descuentoFielNf6);
        return descuentoFielNf6;
    }

    // <-- SERVER
    public static DescuentoFielNf6DTO toDescuentoFielNf6DTO(DescuentoFielNf6 descuentoFielNf6) {
        DescuentoFielNf6DTO descuentoFielNf6DTO = new DescuentoFielNf6DTO();
        BeanUtils.copyProperties(descuentoFielNf6, descuentoFielNf6DTO);
        return descuentoFielNf6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf6 fromDescuentoFielNf6DTOEntitiesFull(DescuentoFielNf6DTO descuentoFielNf6DTO) {
        DescuentoFielNf6 descuentoFielNf6 = fromDescuentoFielNf6DTO(descuentoFielNf6DTO);
        if (descuentoFielNf6DTO.getNf6Secnom6() != null) {
            descuentoFielNf6.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(descuentoFielNf6DTO.getNf6Secnom6()));
        }
        if (descuentoFielNf6DTO.getDescuentoFielCab() != null) {
            descuentoFielNf6.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf6DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf6;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf6DTO toDescuentoFielNf6DTOEntityFull() {
        DescuentoFielNf6DTO descuentoFielNf6DTO = toDescuentoFielNf6DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf6DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf6DTO.setDescuentoFielCab(null);
        }
        if (this.getNf6Secnom6() != null) {
            descuentoFielNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNull());
        } else {
            descuentoFielNf6DTO.setNf6Secnom6(null);
        }
        return descuentoFielNf6DTO;
    }

    // <-- SERVER
    public DescuentoFielNf6DTO toDescuentoFielNf6DTOEntityNull() {
        DescuentoFielNf6DTO descuentoFielNf6DTO = toDescuentoFielNf6DTO(this);
        descuentoFielNf6DTO.setDescuentoFielCab(null);
        descuentoFielNf6DTO.setNf6Secnom6(null);
        return descuentoFielNf6DTO;
    }

    // <-- SERVER
    public DescuentoFielNf6DTO toDescuentoFielNf6DTONf5() {
        DescuentoFielNf6DTO descuentoFielNf6DTO = toDescuentoFielNf6DTO(this);
        descuentoFielNf6DTO.setDescuentoFielCab(null);
        if (this.getNf6Secnom6() != null) {
            descuentoFielNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            descuentoFielNf6DTO.setNf6Secnom6(null);
        }
        return descuentoFielNf6DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
