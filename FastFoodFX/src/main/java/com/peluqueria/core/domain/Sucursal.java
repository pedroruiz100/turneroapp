package com.peluqueria.core.domain;

import com.peluqueria.dto.SucursalDTO;
import com.peluqueria.dto.TalonariosSucursaleDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the sucursal database table.
 *
 */
@Entity
@Table(name = "sucursal", schema = "general")
@NamedQuery(name = "Sucursal.findAll", query = "SELECT s FROM Sucursal s")
public class Sucursal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_sucursal")
    private Long idSucursal;

    @Column(name = "calle_principal")
    private String callePrincipal;

    @Column(name = "cod_sucursal")
    private String codSucursal;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "nro_local")
    private Integer nroLocal;

    @Column(name = "primera_lateral")
    private String primeraLateral;

    @Column(name = "segunda_lateral")
    private String segundaLateral;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "telefono")
    private String telefono;

    // bi-directional many-to-one association to Barrio
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_barrio")
    private Barrio barrio;

    // bi-directional many-to-one association to Ciudad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_ciudad")
    private Ciudad ciudad;

    // bi-directional many-to-one association to Departamento
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_departamento")
    private Departamento departamento;

    // bi-directional many-to-one association to Empresa
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_empresa")
    private Empresa empresa;

    // bi-directional many-to-one association to TalonariosSucursale
    @OneToMany(mappedBy = "sucursal", fetch = FetchType.LAZY)
    private List<TalonariosSucursales> talonariosSucursales;

    // bi-directional many-to-one association to ComboCab
    @OneToMany(mappedBy = "sucursal", fetch = FetchType.LAZY)
    private List<ComboCab> comboCabs;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "sucursal", fetch = FetchType.LAZY)
    private List<FacturaClienteCab> facturaClienteCabs;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "sucursal", fetch = FetchType.LAZY)
    private List<NotaCreditoCab> notaCreditoCabs;

    // bi-directional many-to-one association to Caja
    @OneToMany(mappedBy = "sucursal", fetch = FetchType.LAZY)
    private List<Caja> caja;

    public Sucursal() {
    }

    public Long getIdSucursal() {
        return this.idSucursal;
    }

    public void setIdSucursal(Long idSucursal) {
        this.idSucursal = idSucursal;
    }

    public String getCallePrincipal() {
        return this.callePrincipal;
    }

    public void setCallePrincipal(String callePrincipal) {
        this.callePrincipal = callePrincipal;
    }

    public String getCodSucursal() {
        return this.codSucursal;
    }

    public void setCodSucursal(String codSucursal) {
        this.codSucursal = codSucursal;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getNroLocal() {
        return this.nroLocal;
    }

    public void setNroLocal(Integer nroLocal) {
        this.nroLocal = nroLocal;
    }

    public List<Caja> getCaja() {
        return caja;
    }

    public void setCaja(List<Caja> caja) {
        this.caja = caja;
    }

    public String getPrimeraLateral() {
        return this.primeraLateral;
    }

    public void setPrimeraLateral(String primeraLateral) {
        this.primeraLateral = primeraLateral;
    }

    public String getSegundaLateral() {
        return this.segundaLateral;
    }

    public void setSegundaLateral(String segundaLateral) {
        this.segundaLateral = segundaLateral;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Barrio getBarrio() {
        return this.barrio;
    }

    public void setBarrio(Barrio barrio) {
        this.barrio = barrio;
    }

    public Ciudad getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(Ciudad ciudad) {
        this.ciudad = ciudad;
    }

    public Departamento getDepartamento() {
        return this.departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Empresa getEmpresa() {
        return this.empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public List<TalonariosSucursales> getTalonariosSucursales() {
        return this.talonariosSucursales;
    }

    public void setTalonariosSucursales(
            List<TalonariosSucursales> talonariosSucursales) {
        this.talonariosSucursales = talonariosSucursales;
    }

    // **********************************************************************************
    public List<ComboCab> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCab> comboCabs) {
        this.comboCabs = comboCabs;
    }

    // ********************************************************************************
    public List<FacturaClienteCab> getFacturaClienteCabs() {
        return facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCab> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    // ********************************************************************************
    public List<NotaCreditoCab> getNotaCreditoCabs() {
        return notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCab> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public SucursalDTO toSucursalDescriDTO() {
        SucursalDTO sucuDTO = toSucursalDTO(this);
        sucuDTO.setBarrioDTO(null);
        sucuDTO.setCiudadDTO(null);
        sucuDTO.setComboCabDTO(null);
        sucuDTO.setDepartamentoDTO(null);
        sucuDTO.setEmpresaDTO(null);
        sucuDTO.setFacturaClienteCabDTO(null);
        sucuDTO.setNotaCreditoCabDTO(null);
        sucuDTO.setTalonariosSucursalesDTO(null);
        sucuDTO.setCajaDTO(null);
        return sucuDTO;
    }

    // TODOS LOS DATOS DE SUCURSAL PERO CON EMPRESA, CIUDAD Y LISTA DE TALONARIO
    // SUCURSAL
    public SucursalDTO toSucursalEmpresaCiudadDTO() {
        SucursalDTO sucuDTO = toSucursalDTO(this);
        if (this.empresa != null) {
            sucuDTO.setEmpresaDTO(this.getEmpresa().toEmpresaDTO());
        }
        if (this.ciudad != null) {
            sucuDTO.setCiudadDTO(this.getCiudad().toCiudadSinOtrosDatos());
        }
        if (!this.talonariosSucursales.isEmpty()) {
            List<TalonariosSucursales> talonario = this
                    .getTalonariosSucursales();
            List<TalonariosSucursaleDTO> talDTO = new ArrayList<TalonariosSucursaleDTO>();
            for (TalonariosSucursales ts : talonario) {
                talDTO.add(ts.toTalonariosDTO());
            }
            sucuDTO.setTalonariosSucursalesDTO(talDTO);
        }
        sucuDTO.setBarrioDTO(null);
        sucuDTO.setComboCabDTO(null);
        sucuDTO.setDepartamentoDTO(null);
        sucuDTO.setFacturaClienteCabDTO(null);
        sucuDTO.setNotaCreditoCabDTO(null);
        sucuDTO.setCajaDTO(null);
        return sucuDTO;
    }

    public SucursalDTO toSucursalDTO(Sucursal sucursal) {
        SucursalDTO sucursalDTO = new SucursalDTO();
        BeanUtils.copyProperties(sucursal, sucursalDTO);
        return sucursalDTO;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
    // tenga)
    public static Sucursal fromSucursalDTO(SucursalDTO sucursalDTO) {
        Sucursal sucursal = new Sucursal();
        BeanUtils.copyProperties(sucursalDTO, sucursal);
        return sucursal;
    }

    public Sucursal fromSucursalDTOAsociado(SucursalDTO sucuDTO) {
        Sucursal sucu = fromSucursalDTO(sucuDTO);
        sucu.setDepartamento(Departamento.fromDepartamentoDTO(sucuDTO
                .getDepartamentoDTO()));
        sucu.setCiudad(Ciudad.fromCiudadDTO(sucuDTO.getCiudadDTO()));
        sucu.setBarrio(Barrio.fromBarrioDTO(sucuDTO.getBarrioDTO()));
        sucu.setEmpresa(Empresa.fromEmpresaDTO(sucuDTO.getEmpresaDTO()));
        return sucu;
    }

}
