package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf7DTO;

/**
 * The persistent class for the descuento_fiel_nf7 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf7", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf7.findAll", query = "SELECT d FROM DescuentoFielNf7 d")
public class DescuentoFielNf7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf7")
    private Long idDescuentoFielNf7;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf7Secnom7
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf7_secnom7")
    private Nf7Secnom7 nf7Secnom7;

    public DescuentoFielNf7() {
    }

    public Long getIdDescuentoFielNf7() {
        return this.idDescuentoFielNf7;
    }

    public void setIdDescuentoFielNf7(Long idDescuentoFielNf7) {
        this.idDescuentoFielNf7 = idDescuentoFielNf7;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf7Secnom7 getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf7 fromDescuentoFielNf7DTO(DescuentoFielNf7DTO descuentoFielNf7DTO) {
        DescuentoFielNf7 descuentoFielNf7 = new DescuentoFielNf7();
        BeanUtils.copyProperties(descuentoFielNf7DTO, descuentoFielNf7);
        return descuentoFielNf7;
    }

    // <-- SERVER
    public static DescuentoFielNf7DTO toDescuentoFielNf7DTO(DescuentoFielNf7 descuentoFielNf7) {
        DescuentoFielNf7DTO descuentoFielNf7DTO = new DescuentoFielNf7DTO();
        BeanUtils.copyProperties(descuentoFielNf7, descuentoFielNf7DTO);
        return descuentoFielNf7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf7 fromDescuentoFielNf7DTOEntitiesFull(DescuentoFielNf7DTO descuentoFielNf7DTO) {
        DescuentoFielNf7 descuentoFielNf7 = fromDescuentoFielNf7DTO(descuentoFielNf7DTO);
        if (descuentoFielNf7DTO.getNf7Secnom7() != null) {
            descuentoFielNf7.setNf7Secnom7(Nf7Secnom7.fromNf7Secnom7DTO(descuentoFielNf7DTO.getNf7Secnom7()));
        }
        if (descuentoFielNf7DTO.getDescuentoFielCab() != null) {
            descuentoFielNf7.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf7DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf7;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf7DTO toDescuentoFielNf7DTOEntityFull() {
        DescuentoFielNf7DTO descuentoFielNf7DTO = toDescuentoFielNf7DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf7DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf7DTO.setDescuentoFielCab(null);
        }
        if (this.getNf7Secnom7() != null) {
            descuentoFielNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNull());
        } else {
            descuentoFielNf7DTO.setNf7Secnom7(null);
        }
        return descuentoFielNf7DTO;
    }

    // <-- SERVER
    public DescuentoFielNf7DTO toDescuentoFielNf7DTOEntityNull() {
        DescuentoFielNf7DTO descuentoFielNf7DTO = toDescuentoFielNf7DTO(this);
        descuentoFielNf7DTO.setDescuentoFielCab(null);
        descuentoFielNf7DTO.setNf7Secnom7(null);
        return descuentoFielNf7DTO;
    }

    // <-- SERVER
    public DescuentoFielNf7DTO toDescuentoFielNf7DTONf6() {
        DescuentoFielNf7DTO descuentoFielNf7DTO = toDescuentoFielNf7DTO(this);
        descuentoFielNf7DTO.setDescuentoFielCab(null);
        if (this.getNf7Secnom7() != null) {
            descuentoFielNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            descuentoFielNf7DTO.setNf7Secnom7(null);
        }
        return descuentoFielNf7DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
