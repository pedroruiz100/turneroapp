package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;


/**
 * The persistent class for the factura_cliente_cab_funcionario database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_historico", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabHistorico.findAll", query = "SELECT f FROM FacturaClienteCabHistorico f")
public class FacturaClienteCabHistorico implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id_factura_cliente_cab_historico")
    private Long idFacturaClienteCabHistorico;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    @Column(name = "empresa")
    private String empresa;

    @Column(name = "sucursal")
    private String sucursal;

    @Column(name = "nro_caja")
    private Integer nroCaja;

    @Column(name = "ruc")
    private String ruc;

    @Column(name = "telef")
    private String telef;

    @Column(name = "direccion")
    private String direccion;

    @Column(name = "nro_timbrado")
    private String nroTimbrado;

    @Column(name = "fec_inicial")
    private Date fecInicial;

    @Column(name = "fec_vencimiento")
    private Date fecVencimiento;

    @Column(name = "grav10")
    private Integer grav10;

    @Column(name = "grav5")
    private Integer grav5;

    @Column(name = "exenta")
    private Integer exenta;

    @Column(name = "liqui10")
    private Integer liqui10;

    @Column(name = "liqui5")
    private Integer liqui5;

    @Column(name = "ruc_cliente")
    private String rucCliente;

    @Column(name = "giftcard")
    private Integer giftcard;

    @Column(name = "cliente")
    private String cliente;

    @Column(name = "descuento")
    private Integer descuento;

    @Column(name = "total")
    private Integer total;

    @Column(name = "efectivo")
    private Integer efectivo;

    @Column(name = "tarj_cred")
    private Integer tarjCred;

    @Column(name = "tarj_deb")
    private Integer tarjDeb;

    @Column(name = "cheque")
    private Integer cheque;

    @Column(name = "vale")
    private Integer vale;

    @Column(name = "asoc")
    private Integer asoc;

    @Column(name = "not_cre")
    private Integer notCre;

    @Column(name = "redondeo")
    private Integer redondeo;

    @Column(name = "vuelto")
    private Integer vuelto;

    @Column(name = "dolar")
    private Integer dolar;

    @Column(name = "peso")
    private Integer peso;

    @Column(name = "realb")
    private Integer realb;

    @Column(name = "retencion")
    private Integer retencion;

    @Column(name = "coti_dolar")
    private Long cotiDolar;

    @Column(name = "coti_peso")
    private Long cotiPeso;

    @Column(name = "coti_real")
    private Long cotiReal;

    public FacturaClienteCabHistorico() {
    }

    public Long getIdFacturaClienteCabHistorico() {
        return idFacturaClienteCabHistorico;
    }

    public void setIdFacturaClienteCabHistorico(Long idFacturaClienteCabHistorico) {
        this.idFacturaClienteCabHistorico = idFacturaClienteCabHistorico;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public String getEmpresa() {
        return empresa;
    }

    public Integer getGiftcard() {
        return giftcard;
    }

    public void setGiftcard(Integer giftcard) {
        this.giftcard = giftcard;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getSucursal() {
        return sucursal;
    }

    public Long getCotiDolar() {
        return cotiDolar;
    }

    public void setCotiDolar(Long cotiDolar) {
        this.cotiDolar = cotiDolar;
    }

    public Long getCotiPeso() {
        return cotiPeso;
    }

    public void setCotiPeso(Long cotiPeso) {
        this.cotiPeso = cotiPeso;
    }

    public Long getCotiReal() {
        return cotiReal;
    }

    public void setCotiReal(Long cotiReal) {
        this.cotiReal = cotiReal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public Integer getNroCaja() {
        return nroCaja;
    }

    public void setNroCaja(Integer nroCaja) {
        this.nroCaja = nroCaja;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getTelef() {
        return telef;
    }

    public void setTelef(String telef) {
        this.telef = telef;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getNroTimbrado() {
        return nroTimbrado;
    }

    public void setNroTimbrado(String nroTimbrado) {
        this.nroTimbrado = nroTimbrado;
    }

    public Date getFecInicial() {
        return fecInicial;
    }

    public void setFecInicial(Date fecInicial) {
        this.fecInicial = fecInicial;
    }

    public Date getFecVencimiento() {
        return fecVencimiento;
    }

    public void setFecVencimiento(Date fecVencimiento) {
        this.fecVencimiento = fecVencimiento;
    }

    public Integer getGrav10() {
        return grav10;
    }

    public void setGrav10(Integer grav10) {
        this.grav10 = grav10;
    }

    public Integer getGrav5() {
        return grav5;
    }

    public void setGrav5(Integer grav5) {
        this.grav5 = grav5;
    }

    public Integer getExenta() {
        return exenta;
    }

    public void setExenta(Integer exenta) {
        this.exenta = exenta;
    }

    public Integer getLiqui10() {
        return liqui10;
    }

    public void setLiqui10(Integer liqui10) {
        this.liqui10 = liqui10;
    }

    public Integer getLiqui5() {
        return liqui5;
    }

    public void setLiqui5(Integer liqui5) {
        this.liqui5 = liqui5;
    }

    public String getRucCliente() {
        return rucCliente;
    }

    public void setRucCliente(String rucCliente) {
        this.rucCliente = rucCliente;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getEfectivo() {
        return efectivo;
    }

    public void setEfectivo(Integer efectivo) {
        this.efectivo = efectivo;
    }

    public Integer getTarjCred() {
        return tarjCred;
    }

    public void setTarjCred(Integer tarjCred) {
        this.tarjCred = tarjCred;
    }

    public Integer getTarjDeb() {
        return tarjDeb;
    }

    public void setTarjDeb(Integer tarjDeb) {
        this.tarjDeb = tarjDeb;
    }

    public Integer getCheque() {
        return cheque;
    }

    public void setCheque(Integer cheque) {
        this.cheque = cheque;
    }

    public Integer getVale() {
        return vale;
    }

    public void setVale(Integer vale) {
        this.vale = vale;
    }

    public Integer getAsoc() {
        return asoc;
    }

    public void setAsoc(Integer asoc) {
        this.asoc = asoc;
    }

    public Integer getNotCre() {
        return notCre;
    }

    public void setNotCre(Integer notCre) {
        this.notCre = notCre;
    }

    public Integer getRedondeo() {
        return redondeo;
    }

    public void setRedondeo(Integer redondeo) {
        this.redondeo = redondeo;
    }

    public Integer getVuelto() {
        return vuelto;
    }

    public void setVuelto(Integer vuelto) {
        this.vuelto = vuelto;
    }

    public Integer getDolar() {
        return dolar;
    }

    public void setDolar(Integer dolar) {
        this.dolar = dolar;
    }

    public Integer getPeso() {
        return peso;
    }

    public void setPeso(Integer peso) {
        this.peso = peso;
    }

    public Integer getRealb() {
        return realb;
    }

    public void setRealb(Integer realb) {
        this.realb = realb;
    }

    public Integer getRetencion() {
        return retencion;
    }

    public void setRetencion(Integer retencion) {
        this.retencion = retencion;
    }

    public FacturaClienteCabHistoricoDTO toFacturaClienteCabHistoricoDTO() {
        FacturaClienteCabHistoricoDTO factDTO = toFacturaClienteCabHistoricoDTO(this);
        if (this.facturaClienteCab != null) {
            factDTO.setFacturaClienteCab(this.getFacturaClienteCab().toFacturaClienteCabAndDetalleDTO());
        }
        return factDTO;
    }

    public FacturaClienteCabHistoricoDTO toFacturaClienteCabDTO() {
        FacturaClienteCabHistoricoDTO factDTO = toFacturaClienteCabHistoricoDTO(this);
        factDTO.setFacturaClienteCab(null);
        return factDTO;
    }

    private FacturaClienteCabHistoricoDTO toFacturaClienteCabHistoricoDTO(
            FacturaClienteCabHistorico facturaClienteCabHistorico) {
        FacturaClienteCabHistoricoDTO factDTO = new FacturaClienteCabHistoricoDTO();
        BeanUtils.copyProperties(facturaClienteCabHistorico, factDTO);
        return factDTO;
    }

    public static FacturaClienteCabHistorico fromFacturaClienteCabHistoricoDTO(FacturaClienteCabHistoricoDTO factDTO) {
        FacturaClienteCabHistorico fact = new FacturaClienteCabHistorico();
        BeanUtils.copyProperties(factDTO, fact);
        return fact;
    }

    public static FacturaClienteCabHistorico fromFacturaClienteCabHistoricoAsociadoDTO(
            FacturaClienteCabHistoricoDTO factDTO) {
        FacturaClienteCabHistorico fact = fromFacturaClienteCabHistoricoDTO(factDTO);
        fact.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(factDTO.getFacturaClienteCab()));
        return fact;
    }

}
