package com.peluqueria.core.domain;
// Generated 24/11/2016 11:59:15 AM by Hibernate Tools 4.3.1

import com.peluqueria.dto.PreparacionCabeceraDTO;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the apertura_caja database table.
 *
 */
@Entity
@Table(name = "preparacion_cabecera", schema = "factura_cliente")
@NamedQuery(name = "PreparacionCabecera.findAll", query = "SELECT a FROM PreparacionCabecera a")
public class PreparacionCabecera implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_preparacion_cab")
    private Long idPreparacionCab;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "total")
    private String total;

    @Column(name = "fecha")
    private Date fecha;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "cantidad")
    private String cantidad;

    public PreparacionCabecera() {
    }

    public Long getIdPreparacionCab() {
        return idPreparacionCab;
    }

    public void setIdPreparacionCab(Long idPreparacionCab) {
        this.idPreparacionCab = idPreparacionCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public PreparacionCabeceraDTO toPreparacionCabeceraDTO() {
        PreparacionCabeceraDTO apCajaDTO = this.toPreparacionCabeceraDTO(this);
        apCajaDTO.setArticulo(null);
        return apCajaDTO;
    }

    public PreparacionCabeceraDTO toPreparacionCabeceraProveedorDTO() {
        PreparacionCabeceraDTO apCajaDTO = this.toPreparacionCabeceraDTO(this);
        if (this.articulo != null) {
            Articulo fac = this.getArticulo();
            apCajaDTO.setArticulo(fac.toBDArticuloDTO());
        }
        return apCajaDTO;
    }

    public PreparacionCabeceraDTO toPreparacionCabeceraDTO(PreparacionCabecera ap) {
        PreparacionCabeceraDTO acDTO = new PreparacionCabeceraDTO();
        BeanUtils.copyProperties(ap, acDTO);
        return acDTO;
    }

    // METODO QUE
    // Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
    // CAJERO Y USUARIO SUPERVISOR
    public static PreparacionCabecera fromPreparacionCabeceraDTO(PreparacionCabeceraDTO dto) {
        PreparacionCabecera apCa = new PreparacionCabecera();
        BeanUtils.copyProperties(dto, apCa);
        apCa.setArticulo(Articulo.fromArticuloDTO(dto
                .getArticulo()));
        return apCa;
    }

}
