package com.peluqueria.core.domain;

import com.peluqueria.dto.RangoTarjfielDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the dias database table.
 *
 */
@Entity
@Table(name = "rango_tarjfiel", schema = "general")
@NamedQuery(name = "RangoTarjfiel.findAll", query = "SELECT d FROM RangoTarjfiel d")
public class RangoTarjfiel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rango")
    private Long idRango;

    @Column(name = "rango_inicial")
    private Long rangoInicial;

    @Column(name = "rango_final")
    private Long rangoFinal;

    @Column(name = "rango_actual")
    private Long rangoActual;

    public RangoTarjfiel() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

    public RangoTarjfielDTO toRangoTarjfielDTO() {
        RangoTarjfielDTO rangoDTO = toRangoTarjfielDTO(this);
        return rangoDTO;
    }

    public static RangoTarjfielDTO toRangoTarjfielDTO(RangoTarjfiel rango) {
        RangoTarjfielDTO dDTO = new RangoTarjfielDTO();
        BeanUtils.copyProperties(rango, dDTO);
        return dDTO;
    }

    public static RangoTarjfiel fromRangoTarjfielDTO(RangoTarjfielDTO rangoDTO) {
        RangoTarjfiel rango = new RangoTarjfiel();
        BeanUtils.copyProperties(rangoDTO, rango);
        return rango;
    }

}
