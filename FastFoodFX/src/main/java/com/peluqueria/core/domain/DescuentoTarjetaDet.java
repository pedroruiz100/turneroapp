package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaDetDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_tarjeta_det database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_det", schema = "cuenta")
@NamedQuery(name = "DescuentoTarjetaDet.findAll", query = "SELECT d FROM DescuentoTarjetaDet d")
public class DescuentoTarjetaDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_det")
    private Long idDescuentoTarjetaDet;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_dia")
    private Dias dia;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    @Column(name = "dia_especial")
    private Boolean diaEspecial;

    public DescuentoTarjetaDet() {
    }

    public Long getIdDescuentoTarjetaDet() {
        return this.idDescuentoTarjetaDet;
    }

    public void setIdDescuentoTarjetaDet(Long idDescuentoTarjetaDet) {
        this.idDescuentoTarjetaDet = idDescuentoTarjetaDet;
    }

    public Dias getDias() {
        return dia;
    }

    public void setDias(Dias dia) {
        this.dia = dia;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return this.descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Boolean getDiaEspecial() {
        return diaEspecial;
    }

    public void setDiaEspecial(Boolean diaEspecial) {
        this.diaEspecial = diaEspecial;
    }

    public DescuentoTarjetaDetDTO toDescuentoTarjetaDetDTO() {
        DescuentoTarjetaDetDTO descDTO = toDescuentoTarjetaDetDTO(this);
        if (this.dia != null) {
            descDTO.setDia(this.getDias().toDiaDTO());
        }
        descDTO.setDescuentoTarjetaCab(null);
        return descDTO;
    }

    public static DescuentoTarjetaDetDTO toDescuentoTarjetaDetDTO(
            DescuentoTarjetaDet dtc) {
        DescuentoTarjetaDetDTO dDTO = new DescuentoTarjetaDetDTO();
        BeanUtils.copyProperties(dtc, dDTO);
        return dDTO;
    }

    public static DescuentoTarjetaDet fromDescuentoTarjetaDetDTO(
            DescuentoTarjetaDetDTO dtcDTO) {
        DescuentoTarjetaDet dCab = new DescuentoTarjetaDet();
        BeanUtils.copyProperties(dtcDTO, dCab);
        dCab.setDiaEspecial(dtcDTO.getDiaEspecial());
        dCab.setDias(Dias.fromDiaDTO(dtcDTO.getDia()));
        dCab.setDescuentoTarjetaCab(DescuentoTarjetaCab
                .fromDescTarSinTarjetaCabDTO(dtcDTO.getDescuentoTarjetaCab()));
        return dCab;
    }

}
