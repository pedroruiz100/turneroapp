package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf5DTO;

/**
 * The persistent class for the descuento_fiel_nf5 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf5", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf5.findAll", query = "SELECT d FROM DescuentoFielNf5 d")
public class DescuentoFielNf5 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf5")
    private Long idDescuentoFielNf5;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    public DescuentoFielNf5() {
    }

    public Long getIdDescuentoFielNf5() {
        return this.idDescuentoFielNf5;
    }

    public void setIdDescuentoFielNf5(Long idDescuentoFielNf5) {
        this.idDescuentoFielNf5 = idDescuentoFielNf5;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf5 fromDescuentoFielNf5DTO(DescuentoFielNf5DTO descuentoFielNf5DTO) {
        DescuentoFielNf5 descuentoFielNf5 = new DescuentoFielNf5();
        BeanUtils.copyProperties(descuentoFielNf5DTO, descuentoFielNf5);
        return descuentoFielNf5;
    }

    // <-- SERVER
    public static DescuentoFielNf5DTO toDescuentoFielNf5DTO(DescuentoFielNf5 descuentoFielNf5) {
        DescuentoFielNf5DTO descuentoFielNf5DTO = new DescuentoFielNf5DTO();
        BeanUtils.copyProperties(descuentoFielNf5, descuentoFielNf5DTO);
        return descuentoFielNf5DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf5 fromDescuentoFielNf5DTOEntitiesFull(DescuentoFielNf5DTO descuentoFielNf5DTO) {
        DescuentoFielNf5 descuentoFielNf5 = fromDescuentoFielNf5DTO(descuentoFielNf5DTO);
        if (descuentoFielNf5DTO.getNf5Seccion2() != null) {
            descuentoFielNf5.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(descuentoFielNf5DTO.getNf5Seccion2()));
        }
        if (descuentoFielNf5DTO.getDescuentoFielCab() != null) {
            descuentoFielNf5.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf5DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf5;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf5DTO toDescuentoFielNf5DTOEntityFull() {
        DescuentoFielNf5DTO descuentoFielNf5DTO = toDescuentoFielNf5DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf5DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf5DTO.setDescuentoFielCab(null);
        }
        if (this.getNf5Seccion2() != null) {
            descuentoFielNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNull());
        } else {
            descuentoFielNf5DTO.setNf5Seccion2(null);
        }
        return descuentoFielNf5DTO;
    }

    // <-- SERVER
    public DescuentoFielNf5DTO toDescuentoFielNf5DTOEntityNull() {
        DescuentoFielNf5DTO descuentoFielNf5DTO = toDescuentoFielNf5DTO(this);
        descuentoFielNf5DTO.setDescuentoFielCab(null);
        descuentoFielNf5DTO.setNf5Seccion2(null);
        return descuentoFielNf5DTO;
    }

    // <-- SERVER
    public DescuentoFielNf5DTO toDescuentoFielNf5DTONf4() {
        DescuentoFielNf5DTO descuentoFielNf5DTO = toDescuentoFielNf5DTO(this);
        descuentoFielNf5DTO.setDescuentoFielCab(null);
        if (this.getNf5Seccion2() != null) {
            descuentoFielNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            descuentoFielNf5DTO.setNf5Seccion2(null);
        }
        return descuentoFielNf5DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
