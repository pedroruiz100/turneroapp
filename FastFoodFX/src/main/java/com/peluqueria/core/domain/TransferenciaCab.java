package com.peluqueria.core.domain;

import com.peluqueria.dto.TransferenciaCabDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab database table.
 *
 */
@Entity
@Table(name = "transferencia_cab", schema = "stock")
@NamedQuery(name = "TransferenciaCab.findAll", query = "SELECT f FROM TransferenciaCab f")
public class TransferenciaCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_transferencia_cab")
    private Long idTransferenciaCab;

    @Column(name = "fecha")
    private Timestamp fecha;

    @Column(name = "usuario")
    private String usuario;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_origen")
    private Deposito depositoOrigen;

    // bi-directional many-to-one association to Sucursal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_destino")
    private Deposito depositoDestino;

    public Long getIdTransferenciaCab() {
        return idTransferenciaCab;
    }

    public void setIdTransferenciaCab(Long idTransferenciaCab) {
        this.idTransferenciaCab = idTransferenciaCab;
    }

    public Timestamp getFecha() {
        return fecha;
    }

    public void setFecha(Timestamp fecha) {
        this.fecha = fecha;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Deposito getDepositoOrigen() {
        return depositoOrigen;
    }

    public void setDepositoOrigen(Deposito depositoOrigen) {
        this.depositoOrigen = depositoOrigen;
    }

    public Deposito getDepositoDestino() {
        return depositoDestino;
    }

    public void setDepositoDestino(Deposito depositoDestino) {
        this.depositoDestino = depositoDestino;
    }

    public TransferenciaCabDTO toTransferenciaCabDTO() {
        TransferenciaCabDTO tranDTO = toTransferenciaCabDTO(this);
        if (this.depositoOrigen != null) {
            tranDTO.setDepositoOrigen(this.getDepositoOrigen()
                    .toDepositoDTO());
        }
        if (this.depositoDestino != null) {
            tranDTO.setDepositoDestino(this.getDepositoDestino()
                    .toDepositoDTO());
        }
        return tranDTO;
    }

    public static TransferenciaCabDTO toTransferenciaCabDTO(
            TransferenciaCab transferenciaCab) {
        TransferenciaCabDTO facDTO = new TransferenciaCabDTO();
        BeanUtils.copyProperties(transferenciaCab, facDTO);
        return facDTO;
    }

    public static TransferenciaCab fromTransferenciaCabDTO(
            TransferenciaCabDTO facDTO) {
        TransferenciaCab fac = new TransferenciaCab();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static TransferenciaCab fromTransferenciaCabAsociado(
            TransferenciaCabDTO facDTO) {
        TransferenciaCab fac = fromTransferenciaCabDTO(facDTO);
        fac.setDepositoOrigen(Deposito.fromMarcaDTO(facDTO
                .getDepositoOrigen()));
        fac.setDepositoDestino(Deposito.fromMarcaDTO(facDTO.getDepositoDestino()));
        return fac;
    }

//    public static TransferenciaCab fromEstadoAndSucursal(
//            FacturaClienteCabDTO facDTO) {
//        TransferenciaCab fac = fromFacturaClienteCabDTO(facDTO);
//        fac.setEstadoFactura(EstadoFactura.fromEstadoFacturaDTO(facDTO
//                .getEstadoFactura()));
//        fac.setSucursal(Sucursal.fromSucursalDTO(facDTO.getSucursal()));
//        return fac;
//    }
}
