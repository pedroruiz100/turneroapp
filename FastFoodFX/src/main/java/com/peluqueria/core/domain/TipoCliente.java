package com.peluqueria.core.domain;

import com.peluqueria.dto.TipoClienteDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_cliente database table.
 *
 */
@Entity
@Table(name = "tipo_cliente", schema = "cuenta")
@NamedQuery(name = "TipoCliente.findAll", query = "SELECT t FROM TipoCliente t")
public class TipoCliente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_cliente")
    private Long idTipoCliente;

    public TipoCliente() {
    }

    public Long getIdTipoCliente() {
        return this.idTipoCliente;
    }

    public void setIdTipoCliente(Long idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public TipoClienteDTO toTipoClienteDTO() {
        TipoClienteDTO tcDTO = toTipoClienteDTO(this);

        return tcDTO;
    }

    public static TipoClienteDTO toTipoClienteDTO(TipoCliente tipoCliente) {
        TipoClienteDTO tpDTO = new TipoClienteDTO();
        BeanUtils.copyProperties(tipoCliente, tpDTO);
        return tpDTO;
    }

    public static TipoCliente fromTipoClienteDTO(TipoClienteDTO tcDTO) {
        TipoCliente tc = new TipoCliente();
        BeanUtils.copyProperties(tcDTO, tc);
        return tc;
    }

}
