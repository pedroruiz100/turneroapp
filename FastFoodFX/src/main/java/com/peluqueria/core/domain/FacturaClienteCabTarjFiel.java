package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaFielDTO;
import com.peluqueria.dto.FacturaClienteCabTarjFielDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_tarj_fiel database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_tarj_fiel", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabTarjFiel.findAll", query = "SELECT f FROM FacturaClienteCabTarjFiel f")
public class FacturaClienteCabTarjFiel implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_tarj_fiel")
    private Long idFacturaClienteCabTarjFiel;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to Tarjetafiel
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarjeta_fiel")
    private TarjetaClienteFiel tarjetafiel;

    @Column(name = "monto")
    private Integer monto;

    // bi-directional many-to-one association to DescTarjetafiel
    @OneToMany(mappedBy = "facturaClienteCabTarjFiel", fetch = FetchType.LAZY)
    private List<DescTarjetaFiel> descTarjetafiel;

    public FacturaClienteCabTarjFiel() {
    }

    public Long getIdFacturaClienteCabTarjFiel() {
        return idFacturaClienteCabTarjFiel;
    }

    public void setIdFacturaClienteCabTarjFiel(Long idFacturaClienteCabTarjFiel) {
        this.idFacturaClienteCabTarjFiel = idFacturaClienteCabTarjFiel;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TarjetaClienteFiel getTarjetafiel() {
        return tarjetafiel;
    }

    public void setTarjetafiel(TarjetaClienteFiel tarjetafiel) {
        this.tarjetafiel = tarjetafiel;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public List<DescTarjetaFiel> getDescTarjetafiel() {
        return descTarjetafiel;
    }

    public void setDescTarjetafiel(List<DescTarjetaFiel> descTarjetafiel) {
        this.descTarjetafiel = descTarjetafiel;
    }

    public FacturaClienteCabTarjFielDTO toBDFacturaClienteCabTarjFielDTO() {
        FacturaClienteCabTarjFielDTO facDTO = toFacturaClienteCabTarjFielDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.tarjetafiel != null) {
            TarjetaClienteFiel tar = this.getTarjetafiel();
            facDTO.setTarjetaClienteFiel(tar.toTarjetaClienteFielDTO());
        }
        facDTO.setDescTarjetaFiels(null);
        return facDTO;
    }

    public FacturaClienteCabTarjFielDTO toDescripcionFacturaClienteCabTarjFielDTO() {
        FacturaClienteCabTarjFielDTO facDTO = toFacturaClienteCabTarjFielDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setTarjetaClienteFiel(null);
        facDTO.setDescTarjetaFiels(null);
        return facDTO;
    }

    public FacturaClienteCabTarjFielDTO toFacturaClienteCabTarjfielDTO() {
        FacturaClienteCabTarjFielDTO facDTO = toFacturaClienteCabTarjFielDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab facCab = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
        }
        if (this.tarjetafiel != null) {
            TarjetaClienteFiel tar = this.getTarjetafiel();
            facDTO.setTarjetaClienteFiel(tar.toTarjetaClienteFielDTO());
        }
        facDTO.setDescTarjetaFiels(null);
        return facDTO;
    }

    public static FacturaClienteCabTarjFielDTO toFacturaClienteCabTarjFielDTO(
            FacturaClienteCabTarjFiel facturaClienteCabTarjfiel) {
        FacturaClienteCabTarjFielDTO facDTO = new FacturaClienteCabTarjFielDTO();
        BeanUtils.copyProperties(facturaClienteCabTarjfiel, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabTarjFiel fromFacturaClienteCabTarjFielDTO(
            FacturaClienteCabTarjFielDTO tarDTO) {
        FacturaClienteCabTarjFiel fac = new FacturaClienteCabTarjFiel();
        BeanUtils.copyProperties(tarDTO, fac);
        return fac;
    }

    public FacturaClienteCabTarjFielDTO toDescTarjFielDTO() {
        FacturaClienteCabTarjFielDTO facDTO = toFacturaClienteCabTarjFielDTO(this);
//		if (this.facturaClienteCab != null) {
//			FacturaClienteCab facCab = this.getFacturaClienteCab();
//			facDTO.setFacturaClienteCab(facCab.toBDFacturaClienteCabDTO());
//		}
        facDTO.setFacturaClienteCab(null);

        if (this.tarjetafiel != null) {
            TarjetaClienteFiel tar = this.getTarjetafiel();
            facDTO.setTarjetaClienteFiel(tar.toBDTarjetaClienteFielDTO());
        }
        List<DescTarjetaFielDTO> descFielDTO = new ArrayList<DescTarjetaFielDTO>();
        if (!this.descTarjetafiel.isEmpty()) {
            for (DescTarjetaFiel descTarjetaFiel : this.descTarjetafiel) {
                descFielDTO.add(descTarjetaFiel.toBDDescTarjetaFielDTO());
            }
        }
        facDTO.setDescTarjetaFiels(descFielDTO);
        return facDTO;
    }

    public static FacturaClienteCabTarjFiel fromFacturaClienteCabTarjfielAsociadoDTO(
            FacturaClienteCabTarjFielDTO tarDTO) {
        FacturaClienteCabTarjFiel fac = fromFacturaClienteCabTarjFielDTO(tarDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(tarDTO.getFacturaClienteCab()));
        fac.setTarjetafiel(TarjetaClienteFiel.fromTarjetaClienteFielDTO(tarDTO
                .getTarjetaClienteFiel()));
        return fac;
    }
}
