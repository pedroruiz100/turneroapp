package com.peluqueria.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the nota_remision_det database table.
 *
 */
@Entity
@Table(name = "nota_remision_det", schema = "factura_cliente")
@NamedQuery(name = "NotaRemisionDet.findAll", query = "SELECT n FROM NotaRemisionDet n")
public class NotaRemisionDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_remision_det")
    private Long idNotaRemisionDet;

    private Integer cantidad;

    private String descripcion;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    // bi-directional many-to-one association to NotaRemisionCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nota_remision_cab")
    private NotaRemisionCab notaRemisionCab;

    public NotaRemisionDet() {
    }

    public Long getIdNotaRemisionDet() {
        return this.idNotaRemisionDet;
    }

    public void setIdNotaRemisionDet(Long idNotaRemisionDet) {
        this.idNotaRemisionDet = idNotaRemisionDet;
    }

    public Integer getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public NotaRemisionCab getNotaRemisionCab() {
        return this.notaRemisionCab;
    }

    public void setNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
        this.notaRemisionCab = notaRemisionCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

}
