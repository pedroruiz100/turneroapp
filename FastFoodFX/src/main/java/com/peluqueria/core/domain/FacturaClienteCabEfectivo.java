package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabEfectivoDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the donacion_cliente database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_efectivo", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabEfectivo.findAll", query = "SELECT fcce FROM FacturaClienteCabEfectivo fcce")
public class FacturaClienteCabEfectivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_efectivo")
    private Long idFacturaClienteCabEfectivo;

    @Column(name = "monto_efectivo")
    private Integer montoEfectivo;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to DescEfectivo
    @OneToMany(mappedBy = "facturaClienteCabEfectivo", fetch = FetchType.LAZY)
    private List<DescEfectivo> descEfectivo;

    public FacturaClienteCabEfectivo() {
    }

    public Long getIdFacturaClienteCabEfectivo() {
        return idFacturaClienteCabEfectivo;
    }

    public void setIdFacturaClienteCabEfectivo(Long idFacturaClienteCabEfectivo) {
        this.idFacturaClienteCabEfectivo = idFacturaClienteCabEfectivo;
    }

    public Integer getMontoEfectivo() {
        return montoEfectivo;
    }

    public void setMontoEfectivo(Integer montoEfectivo) {
        this.montoEfectivo = montoEfectivo;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public List<DescEfectivo> getDescEfectivo() {
        return descEfectivo;
    }

    public void setDescEfectivo(List<DescEfectivo> descEfectivo) {
        this.descEfectivo = descEfectivo;
    }

    public FacturaClienteCabEfectivoDTO toFacturaClienteCabEfectivoDTO() {
        FacturaClienteCabEfectivoDTO facDTO = toFacturaClienteCabEfectivoDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        facDTO.setDescEfectivo(null);
        return facDTO;
    }

    public FacturaClienteCabEfectivoDTO toDescriFacturaClienteCabEfectivoDTO() {
        FacturaClienteCabEfectivoDTO facDTO = toFacturaClienteCabEfectivoDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setDescEfectivo(null);
        return facDTO;
    }

    public static FacturaClienteCabEfectivoDTO toFacturaClienteCabEfectivoDTO(
            FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
        FacturaClienteCabEfectivoDTO facDTO = new FacturaClienteCabEfectivoDTO();
        BeanUtils.copyProperties(facturaClienteCabEfectivo, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabEfectivo fromFacturaClienteCabEfectivoDTO(
            FacturaClienteCabEfectivoDTO facDTO) {
        FacturaClienteCabEfectivo fac = new FacturaClienteCabEfectivo();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabEfectivo fromFacturaClienteCabEfectivoAsociadoDTO(
            FacturaClienteCabEfectivoDTO facDTO) {
        FacturaClienteCabEfectivo fac = fromFacturaClienteCabEfectivoDTO(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}
