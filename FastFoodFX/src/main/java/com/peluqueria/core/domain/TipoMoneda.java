package com.peluqueria.core.domain;

import com.peluqueria.dto.TipoMonedaDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_moneda database table.
 *
 */
@Entity
@Table(name = "tipo_moneda", schema = "factura_cliente")
@NamedQuery(name = "TipoMoneda.findAll", query = "SELECT t FROM TipoMoneda t")
public class TipoMoneda implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_moneda")
    private Long idTipoMoneda;

    @Column(name = "compra")
    private BigDecimal compra;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "venta")
    private BigDecimal venta;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "tipoMoneda", fetch = FetchType.LAZY)
    private List<FacturaClienteCab> facturaClienteCabs;

    // bi-directional many-to-one association to NotaCreditoCab
    @OneToMany(mappedBy = "tipoMoneda", fetch = FetchType.LAZY)
    private List<NotaCreditoCab> notaCreditoCabs;

    // bi-directional many-to-one association to NotaCreditoCab
    @OneToMany(mappedBy = "tipoMoneda", fetch = FetchType.LAZY)
    private List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjera;

    public TipoMoneda() {
    }

    public Long getIdTipoMoneda() {
        return this.idTipoMoneda;
    }

    public void setIdTipoMoneda(Long idTipoMoneda) {
        this.idTipoMoneda = idTipoMoneda;
    }

    public BigDecimal getCompra() {
        return this.compra;
    }

    public void setCompra(BigDecimal compra) {
        this.compra = compra;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public List<FacturaClienteCabMonedaExtranjera> getFacturaClienteCabMonedaExtranjera() {
        return facturaClienteCabMonedaExtranjera;
    }

    public void setFacturaClienteCabMonedaExtranjera(
            List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjera) {
        this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public BigDecimal getVenta() {
        return this.venta;
    }

    public void setVenta(BigDecimal venta) {
        this.venta = venta;
    }

    public List<FacturaClienteCab> getFacturaClienteCabs() {
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCab> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public FacturaClienteCab addFacturaClienteCab(
            FacturaClienteCab facturaClienteCab) {
        getFacturaClienteCabs().add(facturaClienteCab);
        facturaClienteCab.setTipoMoneda(this);
        return facturaClienteCab;
    }

    public FacturaClienteCab removeFacturaClienteCab(
            FacturaClienteCab facturaClienteCab) {
        getFacturaClienteCabs().remove(facturaClienteCab);
        facturaClienteCab.setTipoMoneda(null);
        return facturaClienteCab;
    }

    public List<NotaCreditoCab> getNotaCreditoCabs() {
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCab> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public TipoMonedaDTO toTipoMonedaDTO() {
        TipoMonedaDTO tmDTO = toTipoMonedaDTO(this);
        tmDTO.setFacturaClienteCabs(null);
        tmDTO.setNotaCreditoCabs(null);
        tmDTO.setFacturaClienteCabMonedaExtranjera(null);
        return tmDTO;
    }

    public static TipoMonedaDTO toTipoMonedaDTO(TipoMoneda tipoMoneda) {
        TipoMonedaDTO tmDTO = new TipoMonedaDTO();
        BeanUtils.copyProperties(tipoMoneda, tmDTO);
        return tmDTO;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
    // tenga)
    public static TipoMoneda fromTipoMonedaDTO(TipoMonedaDTO tmDTO) {
        TipoMoneda tm = new TipoMoneda();
        BeanUtils.copyProperties(tmDTO, tm);
        return tm;
    }

}
