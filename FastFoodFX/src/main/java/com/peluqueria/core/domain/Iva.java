package com.peluqueria.core.domain;

import com.peluqueria.dto.IvaDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the iva database table.
 *
 */
@Entity
@Table(name = "iva", schema = "stock")
@NamedQuery(name = "Iva.findAll", query = "SELECT i FROM Iva i")
public class Iva implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_iva")
    private Long idIva;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "poriva")
    private Integer poriva;

    @Column(name = "tipo_imp")
    private String tipoImp;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "iva")
    private List<Articulo> articulos;

    // bi-directional many-to-one association to ComboCab
    @OneToMany(mappedBy = "iva")
    private List<ComboCab> comboCabs;

    public Iva() {
    }

    public Long getIdIva() {
        return this.idIva;
    }

    public void setIdIva(Long idIva) {
        this.idIva = idIva;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Integer getPoriva() {
        return this.poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public String getTipoImp() {
        return this.tipoImp;
    }

    public void setTipoImp(String tipoImp) {
        this.tipoImp = tipoImp;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Articulo> getArticulos() {
        return this.articulos;
    }

    public void setArticulos(List<Articulo> articulos) {
        this.articulos = articulos;
    }

    public Articulo addArticulo(Articulo articulo) {
        getArticulos().add(articulo);
        articulo.setIva(this);
        return articulo;
    }

    public Articulo removeArticulo(Articulo articulo) {
        getArticulos().remove(articulo);
        articulo.setIva(null);
        return articulo;
    }

    public List<ComboCab> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCab> comboCabs) {
        this.comboCabs = comboCabs;
    }

    public IvaDTO toIvaDTO() {
        IvaDTO ivaDTO = toIvaDTO(this);
        ivaDTO.setArticulos(null);
        ivaDTO.setComboCabs(null);
        return ivaDTO;
    }

    public static IvaDTO toIvaDTO(Iva iva) {
        IvaDTO ivaDTO = new IvaDTO();
        BeanUtils.copyProperties(iva, ivaDTO);
        return ivaDTO;
    }

    public static Iva fromIvaDTO(IvaDTO ivaDTO) {
        Iva iva = new Iva();
        BeanUtils.copyProperties(ivaDTO, iva);
        return iva;
    }

}
