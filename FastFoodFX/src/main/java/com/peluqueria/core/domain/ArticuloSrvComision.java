package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloSrvComisionDTO;
import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;


@Entity
@Table(name = "articulo_srv_comision", schema = "stock")
@NamedQuery(name = "ArticuloSrvComision.findAll", query = "SELECT asr FROM ArticuloSrvComision asr")
public class ArticuloSrvComision implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_srv_comision")
    private Long idArticuloSrvComision;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo", unique = true)
    private Articulo articulo;

    @Column(name = "porc")
    private Integer porc;

    @Column(name = "monto")
    private Integer monto;

    @Column(name = "comision_porc")
    private Boolean comisionPorc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    public ArticuloSrvComision() {
    }

    public Long getIdArticuloSrvComision() {
        return idArticuloSrvComision;
    }

    public void setIdArticuloSrvComision(Long idArticuloSrvComision) {
        this.idArticuloSrvComision = idArticuloSrvComision;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Integer getPorc() {
        return porc;
    }

    public void setPorc(Integer porc) {
        this.porc = porc;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public Boolean getComisionPorc() {
        return comisionPorc;
    }

    public void setComisionPorc(Boolean comisionPorc) {
        this.comisionPorc = comisionPorc;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public ArticuloSrvComisionDTO toArticuloSrvComisionDTO() {
        ArticuloSrvComisionDTO ascDTO = toArticuloSrvComisionDTO(this);
        if (this.articulo != null) {
            ascDTO.setArticulo(this.articulo.toBDArticuloDTO());
        }
        return ascDTO;
    }

    public ArticuloSrvComisionDTO toBDArticuloSrvComisionDTO() {
        ArticuloSrvComisionDTO ascDTO = toArticuloSrvComisionDTO(this);
        ascDTO.setArticulo(null);
        return ascDTO;
    }

    private ArticuloSrvComisionDTO toArticuloSrvComisionDTO(
            ArticuloSrvComision articuloSrvComision) {
        ArticuloSrvComisionDTO ascDTO = new ArticuloSrvComisionDTO();
        BeanUtils.copyProperties(articuloSrvComision, ascDTO);
        return ascDTO;
    }

    public static ArticuloSrvComision fromArticuloSrvComision(
            ArticuloSrvComisionDTO ascDTO) {
        ArticuloSrvComision asc = new ArticuloSrvComision();
        BeanUtils.copyProperties(ascDTO, asc);
        return asc;
    }

    public static ArticuloSrvComision fromArticuloSrvComisionAsociado(
            ArticuloSrvComisionDTO ascDTO) {
        ArticuloSrvComision asc = fromArticuloSrvComision(ascDTO);
        asc.setArticulo(Articulo.fromArticuloDTO(ascDTO.getArticulo()));
        return asc;
    }

}
