package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf6DTO;

/**
 * The persistent class for the descuento_tarjeta_cab_nf6 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf6", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf6.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf6 d")
public class DescuentoTarjetaCabNf6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf6")
    private Long idDescuentoTarjetaCabNf6;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    public DescuentoTarjetaCabNf6() {
    }

    public Long getIdDescuentoTarjetaCabNf6() {
        return this.idDescuentoTarjetaCabNf6;
    }

    public void setIdDescuentoTarjetaCabNf6(Long idDescuentoTarjetaCabNf6) {
        this.idDescuentoTarjetaCabNf6 = idDescuentoTarjetaCabNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf6 fromDescuentoTarjetaCabNf6DTO(DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO) {
        DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6 = new DescuentoTarjetaCabNf6();
        BeanUtils.copyProperties(descuentoTarjetaCabNf6DTO, descuentoTarjetaCabNf6);
        return descuentoTarjetaCabNf6;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf6DTO toDescuentoTarjetaCabNf6DTO(DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6) {
        DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO = new DescuentoTarjetaCabNf6DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf6, descuentoTarjetaCabNf6DTO);
        return descuentoTarjetaCabNf6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf6 fromDescuentoTarjetaCabNf6DTOEntitiesFull(DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO) {
        DescuentoTarjetaCabNf6 descuentoTarjetaCabNf6 = fromDescuentoTarjetaCabNf6DTO(descuentoTarjetaCabNf6DTO);
        if (descuentoTarjetaCabNf6DTO.getNf6Secnom6() != null) {
            descuentoTarjetaCabNf6.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(descuentoTarjetaCabNf6DTO.getNf6Secnom6()));
        }
        if (descuentoTarjetaCabNf6DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf6
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf6DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf6;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf6DTO toDescuentoTarjetaCabNf6DTOEntityFull() {
        DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO = toDescuentoTarjetaCabNf6DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf6DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf6DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf6Secnom6() != null) {
            descuentoTarjetaCabNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            descuentoTarjetaCabNf6DTO.setNf6Secnom6(null);
        }
        return descuentoTarjetaCabNf6DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf6DTO toDescuentoTarjetaCabNf6DTOEntityNull() {
        DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO = toDescuentoTarjetaCabNf6DTO(this);
        descuentoTarjetaCabNf6DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf6DTO.setNf6Secnom6(null);
        return descuentoTarjetaCabNf6DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf6DTO toDescuentoTarjetaCabNf6DTONf5() {
        DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO = toDescuentoTarjetaCabNf6DTO(this);
        descuentoTarjetaCabNf6DTO.setDescuentoTarjetaCab(null);
        if (this.getNf6Secnom6() != null) {
            descuentoTarjetaCabNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            descuentoTarjetaCabNf6DTO.setNf6Secnom6(null);
        }
        return descuentoTarjetaCabNf6DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
