package com.peluqueria.core.domain;

import com.peluqueria.dto.ManejoLocalDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the dias database table.
 *
 */
@Entity
@Table(name = "manejo_local", schema = "ande")
@NamedQuery(name = "ManejoLocal.findAll", query = "SELECT d FROM ManejoLocal d")
public class ManejoLocal implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_manejo")
    private Long idManejo;

    @Column(name = "caja", length = 10485760)
    private String caja;

    @Column(name = "usuario", length = 10485760)
    private String usuario;

    @Column(name = "factura", length = 10485760)
    private String factura;

    public ManejoLocal() {
    }

    public Long getIdManejo() {
        return idManejo;
    }

    public void setIdManejo(Long idManejo) {
        this.idManejo = idManejo;
    }

    public String getCaja() {
        return caja;
    }

    public void setCaja(String caja) {
        this.caja = caja;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public ManejoLocalDTO toManejoLocalDTO() {
        ManejoLocalDTO manejoDTO = this.toManejoLocalDTO(this);
        return manejoDTO;
    }

    private ManejoLocalDTO toManejoLocalDTO(ManejoLocal manejoLocal) {
        ManejoLocalDTO manejoDTO = new ManejoLocalDTO();
        BeanUtils.copyProperties(manejoLocal, manejoDTO);
        return manejoDTO;
    }

    public static ManejoLocal fromManejoLocalDTO(ManejoLocalDTO manejoDTO) {
        ManejoLocal manejo = new ManejoLocal();
        BeanUtils.copyProperties(manejoDTO, manejo);
        return manejo;
    }

}
