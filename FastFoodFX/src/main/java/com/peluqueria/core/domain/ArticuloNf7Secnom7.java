package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf7Secnom7DTO;

/**
 * The persistent class for the articulo_nf7_secnom7 database table.
 *
 */
@Entity
@Table(name = "articulo_nf7_secnom7", schema = "stock")
@NamedQuery(name = "ArticuloNf7Secnom7.findAll", query = "SELECT a FROM ArticuloNf7Secnom7 a")
public class ArticuloNf7Secnom7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf7_secnom7_articulo")
    private Long idNf7Secnom7Articulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Nf7Secnom7
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf7_secnom7")
    private Nf7Secnom7 nf7Secnom7;

    public ArticuloNf7Secnom7() {
    }

    public Long getIdNf7Secnom7Articulo() {
        return this.idNf7Secnom7Articulo;
    }

    public void setIdNf7Secnom7Articulo(Long idNf7Secnom7Articulo) {
        this.idNf7Secnom7Articulo = idNf7Secnom7Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf7Secnom7 getNf7Secnom7() {
        return this.nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf7Secnom7 fromArticuloNf7Secnom7DTO(ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO) {
        ArticuloNf7Secnom7 articuloNf7Secnom7 = new ArticuloNf7Secnom7();
        BeanUtils.copyProperties(articuloNf7Secnom7DTO, articuloNf7Secnom7);
        return articuloNf7Secnom7;
    }

    // <-- SERVER
    public static ArticuloNf7Secnom7DTO toArticuloNf7Secnom7DTO(ArticuloNf7Secnom7 articuloNf7Secnom7) {
        ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO = new ArticuloNf7Secnom7DTO();
        BeanUtils.copyProperties(articuloNf7Secnom7, articuloNf7Secnom7DTO);
        return articuloNf7Secnom7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf7Secnom7DTO toArticuloNf7Secnom7DTOEntitiesNull(ArticuloNf7Secnom7 articuloNf7Secnom7) {
        ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO = toArticuloNf7Secnom7DTO(articuloNf7Secnom7);
        articuloNf7Secnom7DTO.setArticulo(null);
        articuloNf7Secnom7DTO.setNf7Secnom7(null);
        return articuloNf7Secnom7DTO;
    }

    // <-- SERVER
    public ArticuloNf7Secnom7DTO toArticuloNf7Secnom7DTOEntitiesNull() {
        ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO = toArticuloNf7Secnom7DTO(this);
        articuloNf7Secnom7DTO.setArticulo(null);
        articuloNf7Secnom7DTO.setNf7Secnom7(null);
        return articuloNf7Secnom7DTO;
    }

    // <-- SERVER
    public ArticuloNf7Secnom7DTO toArticuloNf7Secnom7DTOEntitiesNf() {
        ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO = toArticuloNf7Secnom7DTO(this);
        articuloNf7Secnom7DTO.setArticulo(null);
        if (this.getNf7Secnom7() != null) {
            articuloNf7Secnom7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            articuloNf7Secnom7DTO.setNf7Secnom7(null);
        }
        return articuloNf7Secnom7DTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf7Secnom7 fromArticuloNf7Secnom7DTOEntitiesFull(
            ArticuloNf7Secnom7DTO articuloNf7Secnom7DTO) {
        ArticuloNf7Secnom7 articuloNf7Secnom7 = fromArticuloNf7Secnom7DTO(articuloNf7Secnom7DTO);
        if (articuloNf7Secnom7DTO.getArticulo() != null) {
            articuloNf7Secnom7.setArticulo(Articulo.fromArticuloDTO(articuloNf7Secnom7DTO.getArticulo()));
        }
        if (articuloNf7Secnom7DTO.getNf7Secnom7() != null) {
            articuloNf7Secnom7.setNf7Secnom7(Nf7Secnom7.fromNf7Secnom7DTO(articuloNf7Secnom7DTO.getNf7Secnom7()));
        }
        return articuloNf7Secnom7;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
