package com.peluqueria.core.domain;

import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ArticuloNf1TipoDTO;
import com.peluqueria.dto.ArticuloNf2SfamiliaDTO;
import com.peluqueria.dto.ArticuloNf3SseccionDTO;
import com.peluqueria.dto.ArticuloNf4Seccion1DTO;
import com.peluqueria.dto.ArticuloNf5Seccion2DTO;
import com.peluqueria.dto.ArticuloNf6Secnom6DTO;
import com.peluqueria.dto.ArticuloNf7Secnom7DTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the articulo database table.
 *
 */
@Entity
@Table(name = "articulo", schema = "stock")
@NamedQuery(name = "Articulo.findAll", query = "SELECT a FROM Articulo a")
public class Articulo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo")
    private Long idArticulo;

    @Column(name = "cod_articulo", unique = true)
    private String codArticulo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "observacion")
    private String observacion;

    private byte[] imagen;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "permite_desc")
    private Boolean permiteDesc;

    @Column(name = "precio_may")
    private Long precioMay;

    @Column(name = "precio_min")
    private Long precioMin;

    @Column(name = "costo")
    private Long costo;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "md")
    private String md;

    @Column(name = "servicio")
    private Boolean servicio;

    @Column(name = "stockeable")
    private Boolean stockeable;

    @Column(name = "importado")
    private Boolean importado;

    @Column(name = "bajada")
    private Boolean bajada;

    @Column(name = "outlet", nullable = true)
    private Long outlet;

    @Column(name = "sanlo", nullable = true)
    private Long sanlo;

    @Column(name = "sec1")
    private String sec1;

    @Column(name = "sec2")
    private String sec2;

    @Column(name = "cacique", nullable = true)
    private Long cacique;

    @Column(name = "desc_max_may", nullable = true)
    private Long descMaxMay;

    @Column(name = "desc_max_min", nullable = true)
    private Long descMaxMin;

    @Column(name = "desc_proveedor", nullable = true)
    private Long descProveedor;

    @Column(name = "costo_extranjero")
    private String costoExtranjero;

    @Column(name = "stock_min")
    private String stockMin;

    @Column(name = "stock_max")
    private String stockMax;

    @Column(name = "stock_actual")
    private String stockActual;

    @Column(name = "costo_origen_importado")
    private String costoOrigenImportado;

    @Column(name = "desc_importado")
    private String descImportado;

    @Column(name = "incre_importado")
    private String increImportado;

    @Column(name = "incre_parana")
    private String increParana;

    @Column(name = "url_imagen")
    private String urlImagen;

    @Column(name = "incre_parana2")
    private String increParana2;

    @Column(name = "incre_parana3")
    private String increParana3;

    @Column(name = "incre_may")
    private String increMay;

    @Column(name = "cotizacion")
    private String cotizacion;

    // bi-directional many-to-one association to Iva
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_iva")
    private Iva iva;

    // bi-directional many-to-one association to Marca
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_marca")
    private Marca marca;

    // bi-directional many-to-one association to SeccionSub
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion_sub")
    private SeccionSub seccionSub;

    // bi-directional many-to-one association to Unidad
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_unidad")
    private Unidad unidad;

    // bi-directional many-to-one association to Seccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_seccion")
    private Seccion seccion;

    // bi-directional one-to-many association to ComboDet
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ComboDet> comboDets;

    // bi-directional one-to-many association to CancelacionProducto
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<CancelacionProducto> cancelacionProductos;

    // bi-directional one-to-many association to FacturaClienteDet
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<FacturaClienteDet> facturaClienteDets;

    // bi-directional one-to-many association to FacturaClienteDet
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<NotaCreditoDet> notaCreditoDets;

    // bi-directional one-to-many association to NotaRemisionDet
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<NotaRemisionDet> notaRemisionDets;

    // bi-directional one-to-many association to ArticuloSrvComision
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloSrvComision> articuloSrvComision;

    // bi-directional one-to-many association to ServPendiente
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ServPendiente> servPendiente;

    // bi-directional one-to-many association to ArtPromoTemporada
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArtPromoTemporada> artPromoTemporada;

    // bi-directional one-to-many association to ArticuloAuxCodNf
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloAuxCodNf> articuloAuxCodNf;

    // bi-directional one-to-many association to ArticuloNf1Tipo
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf1Tipo> articuloNf1Tipo;

    // bi-directional one-to-many association to ArticuloNf2Sfamilia
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf2Sfamilia> articuloNf2Sfamilia;

    // bi-directional one-to-many association to ArticuloNf3Sseccion
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf3Sseccion> articuloNf3Sseccion;

    // bi-directional one-to-many association to ArticuloNf4Seccion1
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf4Seccion1> articuloNf4Seccion1;

    // bi-directional one-to-many association to ArticuloNf5Seccion2
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf5Seccion2> articuloNf5Seccion2;

    // bi-directional one-to-many association to ArticuloNf6Secnom6
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf6Secnom6> articuloNf6Secnom6;

    // bi-directional one-to-many association to ArticuloNf7Secnom7
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArticuloNf7Secnom7> articuloNf7Secnom7;

    // bi-directional one-to-many association to DescuentoTarjetaCabArticulo
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<DescuentoTarjetaCabArticulo> descuentoTarjetaCabArticulos;

    // bi-directional one-to-many association to ArtPromoTemporada
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<ArtPromoTemporadaObsequio> artPromoTemporadaObsequio;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<Existencia> existencia;

    // bi-directional many-to-one association to Articulo
    @OneToMany(mappedBy = "articulo", fetch = FetchType.LAZY)
    private List<NotaCreditoDetProveedor> notaCreditoDetProveedor;

    public Articulo() {
    }

    public Long getIdArticulo() {
        return this.idArticulo;
    }

    public void setIdArticulo(Long idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(String codArticulo) {
        this.codArticulo = codArticulo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public Long getDescMaxMin() {
        return descMaxMin;
    }

    public void setDescMaxMin(Long descMaxMin) {
        this.descMaxMin = descMaxMin;
    }

    public Long getDescProveedor() {
        return descProveedor;
    }

    public void setDescProveedor(Long descProveedor) {
        this.descProveedor = descProveedor;
    }

    public List<NotaCreditoDetProveedor> getNotaCreditoDetProveedor() {
        return notaCreditoDetProveedor;
    }

    public void setNotaCreditoDetProveedor(List<NotaCreditoDetProveedor> notaCreditoDetProveedor) {
        this.notaCreditoDetProveedor = notaCreditoDetProveedor;
    }

    public List<ArticuloSrvComision> getArticuloSrvComision() {
        return articuloSrvComision;
    }

    public String getUrlImagen() {
        return urlImagen;
    }

    public void setUrlImagen(String urlImagen) {
        this.urlImagen = urlImagen;
    }

    public void setArticuloSrvComision(List<ArticuloSrvComision> articuloSrvComision) {
        this.articuloSrvComision = articuloSrvComision;
    }

    public List<ServPendiente> getServPendiente() {
        return servPendiente;
    }

    public byte[] getImagen() {
        return imagen;
    }

    public void setImagen(byte[] imagen) {
        this.imagen = imagen;
    }

    public String getMd() {
        return md;
    }

    public void setMd(String md) {
        this.md = md;
    }

    public void setServPendiente(List<ServPendiente> servPendiente) {
        this.servPendiente = servPendiente;
    }

    public String getObservacion() {
        return observacion;
    }

    public String getStockMin() {
        return stockMin;
    }

    public void setStockMin(String stockMin) {
        this.stockMin = stockMin;
    }

    public String getStockMax() {
        return stockMax;
    }

    public void setStockMax(String stockMax) {
        this.stockMax = stockMax;
    }

    public String getStockActual() {
        return stockActual;
    }

    public void setStockActual(String stockActual) {
        this.stockActual = stockActual;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Existencia> getExistencia() {
        return existencia;
    }

    public void setExistencia(List<Existencia> existencia) {
        this.existencia = existencia;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Long getCosto() {
        return costo;
    }

    public void setCosto(Long costo) {
        this.costo = costo;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public Boolean getServicio() {
        return servicio;
    }

    public void setServicio(Boolean servicio) {
        this.servicio = servicio;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Boolean getPermiteDesc() {
        return this.permiteDesc;
    }

    public void setPermiteDesc(Boolean permiteDesc) {
        this.permiteDesc = permiteDesc;
    }

    public Long getPrecioMay() {
        return this.precioMay;
    }

    public void setPrecioMay(Long precioMay) {
        this.precioMay = precioMay;
    }

    public Long getPrecioMin() {
        return this.precioMin;
    }

    public void setPrecioMin(Long precioMin) {
        this.precioMin = precioMin;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Boolean getStockeable() {
        return stockeable;
    }

    public void setStockeable(Boolean stockeable) {
        this.stockeable = stockeable;
    }

    public Boolean getImportado() {
        return importado;
    }

    public void setImportado(Boolean importado) {
        this.importado = importado;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }

    public Iva getIva() {
        return this.iva;
    }

    public void setIva(Iva iva) {
        this.iva = iva;
    }

    public Marca getMarca() {
        return this.marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public SeccionSub getSeccionSub() {
        return this.seccionSub;
    }

    public void setSeccionSub(SeccionSub seccionSub) {
        this.seccionSub = seccionSub;
    }

    public Unidad getUnidad() {
        return this.unidad;
    }

    public void setUnidad(Unidad unidad) {
        this.unidad = unidad;
    }

    public List<ComboDet> getComboDets() {
        return this.comboDets;
    }

    public void setComboDets(List<ComboDet> comboDets) {
        this.comboDets = comboDets;
    }

    public List<CancelacionProducto> getCancelacionProductos() {
        return cancelacionProductos;
    }

    public void setCancelacionProductos(
            List<CancelacionProducto> cancelacionProductos) {
        this.cancelacionProductos = cancelacionProductos;
    }

    public List<FacturaClienteDet> getFacturaClienteDets() {
        return facturaClienteDets;
    }

    public void setFacturaClienteDets(List<FacturaClienteDet> facturaClienteDets) {
        this.facturaClienteDets = facturaClienteDets;
    }

    public List<NotaCreditoDet> getNotaCreditoDets() {
        return notaCreditoDets;
    }

    public void setNotaCreditoDets(List<NotaCreditoDet> notaCreditoDets) {
        this.notaCreditoDets = notaCreditoDets;
    }

    public Seccion getSeccion() {
        return seccion;
    }

    public void setSeccion(Seccion seccion) {
        this.seccion = seccion;
    }

    public List<NotaRemisionDet> getNotaRemisionDets() {
        return notaRemisionDets;
    }

    public void setNotaRemisionDets(List<NotaRemisionDet> notaRemisionDets) {
        this.notaRemisionDets = notaRemisionDets;
    }

    public List<ArtPromoTemporada> getArtPromoTemporada() {
        return artPromoTemporada;
    }

    public void setArtPromoTemporada(List<ArtPromoTemporada> artPromoTemporada) {
        this.artPromoTemporada = artPromoTemporada;
    }

    public List<ArticuloAuxCodNf> getArticuloAuxCodNf() {
        return articuloAuxCodNf;
    }

    public void setArticuloAuxCodNf(List<ArticuloAuxCodNf> articuloAuxCodNf) {
        this.articuloAuxCodNf = articuloAuxCodNf;
    }

    public List<ArticuloNf1Tipo> getArticuloNf1Tipo() {
        return articuloNf1Tipo;
    }

    public void setArticuloNf1Tipo(List<ArticuloNf1Tipo> articuloNf1Tipo) {
        this.articuloNf1Tipo = articuloNf1Tipo;
    }

    public List<ArticuloNf2Sfamilia> getArticuloNf2Sfamilia() {
        return articuloNf2Sfamilia;
    }

    public void setArticuloNf2Sfamilia(List<ArticuloNf2Sfamilia> articuloNf2Sfamilia) {
        this.articuloNf2Sfamilia = articuloNf2Sfamilia;
    }

    public List<ArticuloNf3Sseccion> getArticuloNf3Sseccion() {
        return articuloNf3Sseccion;
    }

    public void setArticuloNf3Sseccion(List<ArticuloNf3Sseccion> articuloNf3Sseccion) {
        this.articuloNf3Sseccion = articuloNf3Sseccion;
    }

    public List<ArticuloNf4Seccion1> getArticuloNf4Seccion1() {
        return articuloNf4Seccion1;
    }

    public void setArticuloNf4Seccion1(List<ArticuloNf4Seccion1> articuloNf4Seccion1) {
        this.articuloNf4Seccion1 = articuloNf4Seccion1;
    }

    public List<ArticuloNf5Seccion2> getArticuloNf5Seccion2() {
        return articuloNf5Seccion2;
    }

    public void setArticuloNf5Seccion2(List<ArticuloNf5Seccion2> articuloNf5Seccion2) {
        this.articuloNf5Seccion2 = articuloNf5Seccion2;
    }

    public List<ArticuloNf6Secnom6> getArticuloNf6Secnom6() {
        return articuloNf6Secnom6;
    }

    public String getCostoExtranjero() {
        return costoExtranjero;
    }

    public void setCostoExtranjero(String costoExtranjero) {
        this.costoExtranjero = costoExtranjero;
    }

    public String getCostoOrigenImportado() {
        return costoOrigenImportado;
    }

    public void setCostoOrigenImportado(String costoOrigenImportado) {
        this.costoOrigenImportado = costoOrigenImportado;
    }

    public String getDescImportado() {
        return descImportado;
    }

    public void setDescImportado(String descImportado) {
        this.descImportado = descImportado;
    }

    public String getIncreImportado() {
        return increImportado;
    }

    public void setIncreImportado(String increImportado) {
        this.increImportado = increImportado;
    }

    public String getIncreParana() {
        return increParana;
    }

    public void setIncreParana(String increParana) {
        this.increParana = increParana;
    }

    public String getIncreParana2() {
        return increParana2;
    }

    public void setIncreParana2(String increParana2) {
        this.increParana2 = increParana2;
    }

    public String getIncreParana3() {
        return increParana3;
    }

    public void setIncreParana3(String increParana3) {
        this.increParana3 = increParana3;
    }

    public String getIncreMay() {
        return increMay;
    }

    public void setIncreMay(String increMay) {
        this.increMay = increMay;
    }

    public void setArticuloNf6Secnom6(List<ArticuloNf6Secnom6> articuloNf6Secnom6) {
        this.articuloNf6Secnom6 = articuloNf6Secnom6;
    }

    public List<ArticuloNf7Secnom7> getArticuloNf7Secnom7() {
        return articuloNf7Secnom7;
    }

    public void setArticuloNf7Secnom7(List<ArticuloNf7Secnom7> articuloNf7Secnom7) {
        this.articuloNf7Secnom7 = articuloNf7Secnom7;
    }

    public List<DescuentoTarjetaCabArticulo> getDescuentoTarjetaCabArticulos() {
        return descuentoTarjetaCabArticulos;
    }

    public void setDescuentoTarjetaCabArticulos(List<DescuentoTarjetaCabArticulo> descuentoTarjetaCabArticulos) {
        this.descuentoTarjetaCabArticulos = descuentoTarjetaCabArticulos;
    }

    public Long getOutlet() {
        return outlet;
    }

    public void setOutlet(Long outlet) {
        if (outlet != null) {
            this.outlet = outlet;
        } else {
            this.outlet = 0l;
        }
    }

    public Long getSanlo() {
        return sanlo;
    }

    public void setSanlo(Long sanlo) {
        if (sanlo != null) {
            this.sanlo = sanlo;
        } else {
            this.sanlo = 0l;
        }
    }

    public Long getCacique() {
        return cacique;
    }

    public void setCacique(Long cacique) {
        if (cacique != null) {
            this.cacique = cacique;
        } else {
            this.cacique = 0l;
        }

    }

    public String getCotizacion() {
        return cotizacion;
    }

    public void setCotizacion(String cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Long getDescMaxMay() {
        return descMaxMay;
    }

    public void setDescMaxMay(Long descMaxMay) {
        if (descMaxMay != null) {
            this.descMaxMay = descMaxMay;
        } else {
            this.descMaxMay = 0l;
        }
    }

    public List<ArtPromoTemporadaObsequio> getArtPromoTemporadaObsequio() {
        return artPromoTemporadaObsequio;
    }

    public void setArtPromoTemporadaObsequio(List<ArtPromoTemporadaObsequio> artPromoTemporadaObsequio) {
        this.artPromoTemporadaObsequio = artPromoTemporadaObsequio;
    }

    public String getSec1() {
        return sec1;
    }

    public void setSec1(String sec1) {
        this.sec1 = sec1;
    }

    public String getSec2() {
        return sec2;
    }

    public void setSec2(String sec2) {
        this.sec2 = sec2;
    }

    // **********************************************************************************
    public ArticuloDTO toArticuloDTO() {
        ArticuloDTO articuloDTO = toArticuloDTO(this);
        if (this.iva != null) {
            Iva iva = this.getIva();
            articuloDTO.setIva(iva.toIvaDTO());
        }
        if (this.marca != null) {
            Marca marca = this.getMarca();
            articuloDTO.setMarca(marca.toMarcaDTO());
        }
        if (this.unidad != null) {
            Unidad u = this.getUnidad();
            articuloDTO.setUnidad(u.toUnidadDTO());
        }
        if (this.seccionSub != null) {
            SeccionSub ss = this.getSeccionSub();
            articuloDTO.setSeccionSub(ss.toSeccionSubDTO());
        }
        if (this.seccion != null) {
            Seccion s = this.getSeccion();
            articuloDTO.setSeccion(s.toSeccionDTO());
        }
        articuloDTO.setCancelacionProductos(null);
        articuloDTO.setComboDets(null);
        articuloDTO.setFacturaClienteDets(null);
        articuloDTO.setNotaCreditoDets(null);
        articuloDTO.setNotaRemisionDets(null);
        articuloDTO.setServPendiente(null);
        articuloDTO.setArticuloSrvComision(null);
        articuloDTO.setArtPromoTemporada(null);
        articuloDTO.setArtPromoTemporadaObsequio(null);
        articuloDTO.setArticuloAuxCodNf(null);
        articuloDTO.setDescuentoTarjetaCabArticulos(null);
        articuloDTO.setExistencia(null);
        //nf, solo una carga en escalera...
        boolean nfListo = false;
        //nf7
        if (this.getArticuloNf7Secnom7() != null) {
            if (!this.getArticuloNf7Secnom7().isEmpty()) {
                List<ArticuloNf7Secnom7DTO> articuloNf7Secnom7DTOs = new ArrayList<>();
                for (ArticuloNf7Secnom7 articuloNf7Secnom7 : this.getArticuloNf7Secnom7()) {
                    articuloNf7Secnom7DTOs.add(articuloNf7Secnom7.toArticuloNf7Secnom7DTOEntitiesNf());
                }
                articuloDTO.setArticuloNf7Secnom7(articuloNf7Secnom7DTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf7Secnom7(null);
            }
        } else {
            articuloDTO.setArticuloNf7Secnom7(null);
        }
        //nf6
        if (this.getArticuloNf6Secnom6() != null && !nfListo) {
            if (!this.getArticuloNf6Secnom6().isEmpty()) {
                List<ArticuloNf6Secnom6DTO> articuloNf6Secnom6DTOs = new ArrayList<>();
                for (ArticuloNf6Secnom6 articuloNf6Secnom6 : this.getArticuloNf6Secnom6()) {
                    articuloNf6Secnom6DTOs.add(articuloNf6Secnom6.toArticuloNf6Secnom6DTOEntitiesNf());
                }
                articuloDTO.setArticuloNf6Secnom6(articuloNf6Secnom6DTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf6Secnom6(null);
            }
        } else {
            articuloDTO.setArticuloNf6Secnom6(null);
        }
        //nf5
        if (this.getArticuloNf5Seccion2() != null && !nfListo) {
            if (!this.getArticuloNf5Seccion2().isEmpty()) {
                List<ArticuloNf5Seccion2DTO> articuloNf5Seccion2DTOs = new ArrayList<>();
                for (ArticuloNf5Seccion2 articuloNf5Seccion2 : this.getArticuloNf5Seccion2()) {
                    articuloNf5Seccion2DTOs.add(articuloNf5Seccion2.toArticuloNf5Seccion2DTOEntitiesNf());
                }
                articuloDTO.setArticuloNf5Seccion2(articuloNf5Seccion2DTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf5Seccion2(null);
            }
        } else {
            articuloDTO.setArticuloNf5Seccion2(null);
        }
        //nf4
        if (this.getArticuloNf4Seccion1() != null && !nfListo) {
            if (!this.getArticuloNf4Seccion1().isEmpty()) {
                List<ArticuloNf4Seccion1DTO> articuloNf4Seccion1DTOs = new ArrayList<>();
                for (ArticuloNf4Seccion1 articuloNf4Seccion1 : this.getArticuloNf4Seccion1()) {
                    articuloNf4Seccion1DTOs.add(articuloNf4Seccion1.toArticuloNf4Seccion1DTOEntitiesNf());
                }
                articuloDTO.setArticuloNf4Seccion1(articuloNf4Seccion1DTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf4Seccion1(null);
            }
        } else {
            articuloDTO.setArticuloNf4Seccion1(null);
        }
        //nf3
        if (this.getArticuloNf3Sseccion() != null && !nfListo) {
            if (!this.getArticuloNf3Sseccion().isEmpty()) {
                List<ArticuloNf3SseccionDTO> articuloNf3SseccionDTOs = new ArrayList<>();
                for (ArticuloNf3Sseccion articuloNf3Sseccion : this.getArticuloNf3Sseccion()) {
                    articuloNf3SseccionDTOs.add(articuloNf3Sseccion.toArticuloNf3SseccionDTOEntitiesNf());
                }
                articuloDTO.setArticuloNf3Sseccion(articuloNf3SseccionDTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf3Sseccion(null);
            }
        } else {
            articuloDTO.setArticuloNf3Sseccion(null);
        }
        //nf2
        if (this.getArticuloNf2Sfamilia() != null && !nfListo) {
            if (!this.getArticuloNf2Sfamilia().isEmpty()) {
                List<ArticuloNf2SfamiliaDTO> articuloNf2SfamiliaDTOs = new ArrayList<>();
                for (ArticuloNf2Sfamilia articuloNf2Sfamilia : this.getArticuloNf2Sfamilia()) {
                    articuloNf2SfamiliaDTOs.add(articuloNf2Sfamilia.toArticuloNf2SfamiliaDTOEntitiesNf());
                }
                articuloDTO.setArticuloNf2Sfamilia(articuloNf2SfamiliaDTOs);
                nfListo = true;
            } else {
                articuloDTO.setArticuloNf2Sfamilia(null);
            }
        } else {
            articuloDTO.setArticuloNf2Sfamilia(null);
        }
        //nf1
        if (this.getArticuloNf1Tipo() != null && !nfListo) {
            if (!this.getArticuloNf1Tipo().isEmpty()) {
                List<ArticuloNf1TipoDTO> articuloNf1TipoDTOs = new ArrayList<>();
                for (ArticuloNf1Tipo articuloNf1Tipo : this.getArticuloNf1Tipo()) {
                    articuloNf1TipoDTOs.add(articuloNf1Tipo.toArticuloNf1TipoDTOEntitiesNf());
                }
                articuloDTO.setArticuloNf1Tipo(articuloNf1TipoDTOs);
            } else {
                articuloDTO.setArticuloNf1Tipo(null);
            }
        } else {
            articuloDTO.setArticuloNf1Tipo(null);
        }
        articuloDTO.setNotaCreditoDetProveedor(null);
        return articuloDTO;
    }

    public ArticuloDTO toSeccionArticuloDTO() {
        ArticuloDTO aDTO = toArticuloDTO(this);
        aDTO.setIva(null);
        aDTO.setMarca(null);
        aDTO.setUnidad(null);
        aDTO.setSeccionSub(null);
        if (this.seccion != null) {
            Seccion s = this.getSeccion();
            aDTO.setSeccion(s.toSeccionDTO());
        }
        aDTO.setCancelacionProductos(null);
        aDTO.setComboDets(null);
        aDTO.setFacturaClienteDets(null);
        aDTO.setNotaCreditoDets(null);
        aDTO.setNotaRemisionDets(null);
        aDTO.setServPendiente(null);
        aDTO.setArticuloSrvComision(null);
        aDTO.setArtPromoTemporada(null);
        aDTO.setArticuloAuxCodNf(null);
        aDTO.setArticuloNf1Tipo(null);
        aDTO.setArticuloNf2Sfamilia(null);
        aDTO.setArticuloNf3Sseccion(null);
        aDTO.setArticuloNf4Seccion1(null);
        aDTO.setArticuloNf5Seccion2(null);
        aDTO.setArticuloNf6Secnom6(null);
        aDTO.setArticuloNf7Secnom7(null);
        aDTO.setDescuentoTarjetaCabArticulos(null);
        aDTO.setArtPromoTemporadaObsequio(null);
        aDTO.setExistencia(null);
        aDTO.setNotaCreditoDetProveedor(null);
        return aDTO;
    }

    public ArticuloDTO toBDArticuloDTO() {
        ArticuloDTO aDTO = toArticuloDTO(this);
        aDTO.setIva(null);
        aDTO.setMarca(null);
        aDTO.setUnidad(null);
        aDTO.setSeccionSub(null);
        aDTO.setSeccion(null);
        aDTO.setCancelacionProductos(null);
        aDTO.setComboDets(null);
        aDTO.setFacturaClienteDets(null);
        aDTO.setNotaCreditoDets(null);
        aDTO.setNotaRemisionDets(null);
        aDTO.setServPendiente(null);
        aDTO.setArticuloSrvComision(null);
        aDTO.setArtPromoTemporada(null);
        aDTO.setArticuloAuxCodNf(null);
        aDTO.setArticuloNf1Tipo(null);
        aDTO.setArticuloNf2Sfamilia(null);
        aDTO.setArticuloNf3Sseccion(null);
        aDTO.setArticuloNf4Seccion1(null);
        aDTO.setArticuloNf5Seccion2(null);
        aDTO.setArticuloNf6Secnom6(null);
        aDTO.setArticuloNf7Secnom7(null);
        aDTO.setDescuentoTarjetaCabArticulos(null);
        aDTO.setArtPromoTemporadaObsequio(null);
        aDTO.setExistencia(null);
        aDTO.setNotaCreditoDetProveedor(null);
        return aDTO;
    }

    public ArticuloDTO toBDWithIvaArticuloDTO() {
        ArticuloDTO aDTO = toArticuloDTO(this);
        if (this.iva != null) {
            Iva iva = this.getIva();
            aDTO.setIva(iva.toIvaDTO());
        }
        aDTO.setMarca(null);
        aDTO.setUnidad(null);
        aDTO.setSeccionSub(null);
        aDTO.setSeccion(null);
        aDTO.setCancelacionProductos(null);
        aDTO.setComboDets(null);
        aDTO.setFacturaClienteDets(null);
        aDTO.setNotaCreditoDets(null);
        aDTO.setNotaRemisionDets(null);
        aDTO.setServPendiente(null);
        aDTO.setArticuloSrvComision(null);
        aDTO.setArtPromoTemporada(null);
        aDTO.setArticuloAuxCodNf(null);
        aDTO.setArticuloNf1Tipo(null);
        aDTO.setArticuloNf2Sfamilia(null);
        aDTO.setArticuloNf3Sseccion(null);
        aDTO.setArticuloNf4Seccion1(null);
        aDTO.setArticuloNf5Seccion2(null);
        aDTO.setArticuloNf6Secnom6(null);
        aDTO.setArticuloNf7Secnom7(null);
        aDTO.setDescuentoTarjetaCabArticulos(null);
        aDTO.setArtPromoTemporadaObsequio(null);
        aDTO.setExistencia(null);
        aDTO.setNotaCreditoDetProveedor(null);
        return aDTO;
    }

    public static ArticuloDTO toArticuloDTO(Articulo articulo) {
        ArticuloDTO arDTO = new ArticuloDTO();
        BeanUtils.copyProperties(articulo, arDTO);
        return arDTO;
    }

    public static Articulo fromArticuloDTOAsociado(ArticuloDTO articuloDTO) {
        Articulo ar = fromArticuloDTO(articuloDTO);
        ar.setIva(Iva.fromIvaDTO(articuloDTO.getIva()));
        ar.setMarca(Marca.fromMarcaDTO(articuloDTO.getMarca()));
        ar.setUnidad(Unidad.fromUnidadDTO(articuloDTO.getUnidad()));
        ar.setSeccionSub(SeccionSub.fromSeccionSubDTO(articuloDTO
                .getSeccionSub()));
        return ar;
    }

    public static Articulo fromArticuloDTO(ArticuloDTO articuloDTO) {
        Articulo ar = new Articulo();
        BeanUtils.copyProperties(articuloDTO, ar);
        return ar;
    }

    public static Articulo fromArticuloDTONoList(ArticuloDTO entidadDTO) {
        Articulo articulo = fromArticuloDTO(entidadDTO);
        articulo.setMarca(null);
        articulo.setUnidad(null);
        articulo.setSeccionSub(null);
        articulo.setSeccion(null);
        articulo.setCancelacionProductos(null);
        articulo.setComboDets(null);
        articulo.setFacturaClienteDets(null);
        articulo.setNotaCreditoDets(null);
        articulo.setNotaRemisionDets(null);
        articulo.setServPendiente(null);
        articulo.setArticuloSrvComision(null);
        articulo.setArtPromoTemporada(null);
        articulo.setArticuloAuxCodNf(null);
        articulo.setArticuloNf1Tipo(null);
        articulo.setArticuloNf2Sfamilia(null);
        articulo.setArticuloNf3Sseccion(null);
        articulo.setArticuloNf4Seccion1(null);
        articulo.setArticuloNf5Seccion2(null);
        articulo.setArticuloNf6Secnom6(null);
        articulo.setArticuloNf7Secnom7(null);
        articulo.setDescuentoTarjetaCabArticulos(null);
        articulo.setIva(null);
        articulo.setArtPromoTemporadaObsequio(null);
        articulo.setExistencia(null);
        articulo.setNotaCreditoDetProveedor(null);
        return articulo;
    }

}
