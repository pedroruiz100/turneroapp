package com.peluqueria.core.domain;

import com.peluqueria.dto.ArtPromoTemporadaDTO;
import com.peluqueria.dto.ArtPromoTemporadaObsequioDTO;
import com.peluqueria.dto.PromoTemporadaArtDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the promo_temporada_art database table.
 *
 */
@Entity
@Table(name = "promo_temporada_art", schema = "cuenta")
@NamedQuery(name = "PromoTemporadaArt.findAll", query = "SELECT a FROM PromoTemporadaArt a")
public class PromoTemporadaArt implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_temporada_art")
    private Long idTemporadaArt;

    @Column(name = "descripcion_temporada_art")
    private String descripcionTemporadaArt;

    @Column(name = "estado_promo")
    private Boolean estadoPromo;

    @Column(name = "fecha_fin")
    private Timestamp fechaFin;

    @Column(name = "fecha_inicio")
    private Timestamp fechaInicio;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    // bi-directional many-to-one association to ArtPromoTemporada
    @OneToMany(mappedBy = "promoTemporadaArt", fetch = FetchType.LAZY)
    private List<ArtPromoTemporada> artPromoTemporada;

    // bi-directional many-to-one association to FacturaClienteCabPromoTemp
    @OneToMany(mappedBy = "promoTemporadaArt", fetch = FetchType.LAZY)
    private List<FacturaClienteCabPromoTempArt> facturaClienteCabPromoTempArt;

    // bi-directional many-to-one association to ArtPromoTemporada
    @OneToMany(mappedBy = "promoTemporadaArt", fetch = FetchType.LAZY)
    private List<ArtPromoTemporadaObsequio> artPromoTemporadaObsequio;

    public PromoTemporadaArt() {
    }

    public List<ArtPromoTemporada> getArtPromoTemporada() {
        return artPromoTemporada;
    }

    public void setArtPromoTemporada(
            List<ArtPromoTemporada> artPromoTemporada) {
        this.artPromoTemporada = artPromoTemporada;
    }

    public Long getIdTemporadaArt() {
        return this.idTemporadaArt;
    }

    public void setIdTemporadaArt(Long idTemporadaArt) {
        this.idTemporadaArt = idTemporadaArt;
    }

    public List<FacturaClienteCabPromoTempArt> getFacturaClienteCabPromoTempArt() {
        return facturaClienteCabPromoTempArt;
    }

    public void setFacturaClienteCabPromoTempArt(
            List<FacturaClienteCabPromoTempArt> facturaClienteCabPromoTempArt) {
        this.facturaClienteCabPromoTempArt = facturaClienteCabPromoTempArt;
    }

    public List<ArtPromoTemporadaObsequio> getArtPromoTemporadaObsequio() {
        return artPromoTemporadaObsequio;
    }

    public void setArtPromoTemporadaObsequio(List<ArtPromoTemporadaObsequio> artPromoTemporadaObsequio) {
        this.artPromoTemporadaObsequio = artPromoTemporadaObsequio;
    }

    public String getDescripcionTemporadaArt() {
        return this.descripcionTemporadaArt;
    }

    public void setDescripcionTemporadaArt(String descripcionTemporadaArt) {
        this.descripcionTemporadaArt = descripcionTemporadaArt;
    }

    public Boolean getEstadoPromo() {
        return this.estadoPromo;
    }

    public void setEstadoPromo(Boolean estadoPromo) {
        this.estadoPromo = estadoPromo;
    }

    public Timestamp getFechaFin() {
        return this.fechaFin;
    }

    public void setFechaFin(Timestamp fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Timestamp getFechaInicio() {
        return this.fechaInicio;
    }

    public void setFechaInicio(Timestamp fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public PromoTemporadaArtDTO toPromoTemporadaArtDTO() {
        PromoTemporadaArtDTO proDTO = toPromoTemporadaArtDTO(this);
        if (!this.artPromoTemporada.isEmpty()) {
            List<ArtPromoTemporada> pro = this.getArtPromoTemporada();
            List<ArtPromoTemporadaDTO> artPromoDTO = new ArrayList<>();
            for (ArtPromoTemporada artPromo : pro) {
                artPromoDTO.add(artPromo.toSinArtPromoTemporadaDTO());
            }
            proDTO.setArtPromoTemporadaDTO(artPromoDTO);
        }
        if (!this.artPromoTemporadaObsequio.isEmpty()) {
            List<ArtPromoTemporadaObsequio> pro = this.getArtPromoTemporadaObsequio();
            List<ArtPromoTemporadaObsequioDTO> artPromoDTO = new ArrayList<>();
            for (ArtPromoTemporadaObsequio artPromo : pro) {
                artPromoDTO.add(artPromo.toSinArtPromoTemporadaObsequioDTO());
            }
            proDTO.setArtPromoTemporadaObsequio(artPromoDTO);
        }
        proDTO.setFacturaClienteCabPromoTempArt(null);
        return proDTO;
    }

    // Sin el artículo Promo Temporada
    public PromoTemporadaArtDTO toPromoTemporadaArtSinArtDTO() {
        PromoTemporadaArtDTO proDTO = toPromoTemporadaArtDTO(this);
        proDTO.setArtPromoTemporadaDTO(null);
        proDTO.setArtPromoTemporadaObsequio(null);
        proDTO.setFacturaClienteCabPromoTempArt(null);
        return proDTO;
    }

    public static PromoTemporadaArtDTO toPromoTemporadaArtDTO(PromoTemporadaArt pro) {
        PromoTemporadaArtDTO proDTO = new PromoTemporadaArtDTO();
        BeanUtils.copyProperties(pro, proDTO);
        return proDTO;
    }

    public static PromoTemporadaArt fromPromoTemporadaArtDTO(PromoTemporadaArtDTO proDTO) {
        PromoTemporadaArt pro = new PromoTemporadaArt();
        BeanUtils.copyProperties(proDTO, pro);
        return pro;
    }

}
