package com.peluqueria.core.domain;

import com.peluqueria.dto.PedidoDetConteoDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "pedido_det_conteo", schema = "factura_cliente")
@NamedQuery(name = "PedidoDetConteo.findAll", query = "SELECT f FROM PedidoDetConteo f")
public class PedidoDetConteo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pedido_det_conteo")
    private Long idPedidoDetConteo;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_pedido_cab")
    private PedidoCab pedidoCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "precio")
    private Long precio;

    @Column(name = "cantidad")
    private String cantidad;

    @Column(name = "total")
    private long total;

    @Column(name = "sucursal")
    private String sucursal;

    @Column(name = "cc")
    private Boolean cc;

    @Column(name = "sc")
    private Boolean sc;

    @Column(name = "sl")
    private Boolean sl;

    public PedidoDetConteo() {
    }

    public PedidoCab getPedidoCab() {
        return pedidoCab;
    }

    public void setPedidoCab(PedidoCab pedidoCab) {
        this.pedidoCab = pedidoCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Boolean getCc() {
        return cc;
    }

    public void setCc(Boolean cc) {
        this.cc = cc;
    }

    public Boolean getSc() {
        return sc;
    }

    public void setSc(Boolean sc) {
        this.sc = sc;
    }

    public Boolean getSl() {
        return sl;
    }

    public void setSl(Boolean sl) {
        this.sl = sl;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Long getPrecio() {
        return precio;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public Long getIdPedidoDetConteo() {
        return idPedidoDetConteo;
    }

    public void setIdPedidoDetConteo(Long idPedidoDetConteo) {
        this.idPedidoDetConteo = idPedidoDetConteo;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public PedidoDetConteoDTO toPedidoDetConteoDTO() {
        PedidoDetConteoDTO facDTO = toPedidoDetConteoDTO(this);
        if (this.pedidoCab != null) {
            facDTO.setPedidoCab(this.getPedidoCab()
                    .toPedidoCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        return facDTO;
    }

    public PedidoDetConteoDTO toDescriPedidoDetConteoDTO() {
        PedidoDetConteoDTO facDTO = toPedidoDetConteoDTO(this);
        facDTO.setPedidoCab(null);
        facDTO.setArticulo(null);
        return facDTO;
    }

    public static PedidoDetConteoDTO toPedidoDetConteoDTO(
            PedidoDetConteo facturaClienteDet) {
        PedidoDetConteoDTO facDTO = new PedidoDetConteoDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static PedidoDetConteo fromPedidoDetConteoDTO(
            PedidoDetConteoDTO facDTO) {
        PedidoDetConteo fac = new PedidoDetConteo();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static PedidoDetConteo fromPedidoDetConteoAsociado(
            PedidoDetConteoDTO facDTO) {
        PedidoDetConteo fac = fromPedidoDetConteoDTO(facDTO);
        fac.setPedidoCab(PedidoCab.fromPedidoCabDTO(facDTO.getPedidoCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }
}
