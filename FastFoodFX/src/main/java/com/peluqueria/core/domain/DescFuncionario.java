package com.peluqueria.core.domain;

import com.peluqueria.dto.DescFuncionarioDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the desc_funcionario database table.
 *
 */
@Entity
@Table(name = "desc_funcionario", schema = "factura_cliente")
@NamedQuery(name = "DescFuncionario.findAll", query = "SELECT d FROM DescFuncionario d")
public class DescFuncionario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_desc_funcionario")
    private Long idDescFuncionario;

    @Column(name = "monto_desc")
    private Integer montoDesc;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to FacturaClienteCabFuncionario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab_funcionario")
    private FacturaClienteCabFuncionario facturaClienteCabFuncionario;

    public DescFuncionario() {
    }

    public Long getIdDescFuncionario() {
        return this.idDescFuncionario;
    }

    public void setIdDescFuncionario(Long idDescFuncionario) {
        this.idDescFuncionario = idDescFuncionario;
    }

    public Integer getMontoDesc() {
        return this.montoDesc;
    }

    public void setMontoDesc(Integer montoDesc) {
        this.montoDesc = montoDesc;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public FacturaClienteCabFuncionario getFacturaClienteCabFuncionario() {
        return this.facturaClienteCabFuncionario;
    }

    public void setFacturaClienteCabFuncionario(
            FacturaClienteCabFuncionario facturaClienteCabFuncionario) {
        this.facturaClienteCabFuncionario = facturaClienteCabFuncionario;
    }

    public DescFuncionarioDTO toDescFuncionarioDTO() {
        DescFuncionarioDTO descDTO = toDescFuncionarioDTO(this);
        if (this.facturaClienteCabFuncionario != null) {
            FacturaClienteCabFuncionario fac = this
                    .getFacturaClienteCabFuncionario();
            descDTO.setFacturaClienteCabFuncionarioDTO(fac
                    .toDescripcionFacturaClienteCabFuncionarioDTO());
        }
        return descDTO;
    }

    public DescFuncionarioDTO toBDDescFuncionarioDTO() {
        DescFuncionarioDTO descDTO = toDescFuncionarioDTO(this);
        descDTO.setFacturaClienteCabFuncionarioDTO(null);
        return descDTO;
    }

    public DescFuncionarioDTO toDescriDescFuncionarioDTO() {
        DescFuncionarioDTO descDTO = toDescFuncionarioDTO(this);
        descDTO.setFacturaClienteCabFuncionarioDTO(null);
        return descDTO;
    }

    public static DescFuncionarioDTO toDescFuncionarioDTO(
            DescFuncionario descFuncionario) {
        DescFuncionarioDTO descDTO = new DescFuncionarioDTO();
        BeanUtils.copyProperties(descFuncionario, descDTO);
        return descDTO;
    }

    public static DescFuncionario fromDescFuncionarioDTO(
            DescFuncionarioDTO descDTO) {
        DescFuncionario desc = new DescFuncionario();
        BeanUtils.copyProperties(descDTO, desc);
        return desc;
    }

    public static DescFuncionario fromDescFuncionarioAsociadoDTO(
            DescFuncionarioDTO descDTO) {
        DescFuncionario desc = fromDescFuncionarioDTO(descDTO);
        desc.setFacturaClienteCabFuncionario(FacturaClienteCabFuncionario
                .fromFacturaClienteCabFuncionarioDTO(descDTO
                        .getFacturaClienteCabFuncionarioDTO()));
        return desc;
    }

}
