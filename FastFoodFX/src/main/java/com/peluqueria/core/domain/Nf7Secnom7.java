package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.Nf7Secnom7DTO;

/**
 * The persistent class for the nf7_secnom7 database table.
 *
 */
@Entity
@Table(name = "nf7_secnom7", schema = "stock")
@NamedQuery(name = "Nf7Secnom7.findAll", query = "SELECT n FROM Nf7Secnom7 n")
public class Nf7Secnom7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf7_secnom7")
    private Long idNf7Secnom7;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    // bi-directional one-to-many association to ArticuloNf7Secnom7
    @OneToMany(mappedBy = "nf7Secnom7")
    private List<ArticuloNf7Secnom7> articuloNf7Secnom7s;

    // bi-directional one-to-many association to FuncionarioNf7
    @OneToMany(mappedBy = "nf7Secnom7")
    private List<FuncionarioNf7> funcionarioNf7s;

    // bi-directional one-to-many association to DescuentoFielNf7
    @OneToMany(mappedBy = "nf7Secnom7")
    private List<DescuentoFielNf7> descuentoFielNf7s;

    // bi-directional one-to-many association to PromoTemporadaNf7
    @OneToMany(mappedBy = "nf7Secnom7")
    private List<PromoTemporadaNf7> promoTemporadaNf7s;

    public Nf7Secnom7() {
    }

    public Long getIdNf7Secnom7() {
        return this.idNf7Secnom7;
    }

    public void setIdNf7Secnom7(Long idNf7Secnom7) {
        this.idNf7Secnom7 = idNf7Secnom7;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ArticuloNf7Secnom7> getArticuloNf7Secnom7s() {
        return this.articuloNf7Secnom7s;
    }

    public void setArticuloNf7Secnom7s(List<ArticuloNf7Secnom7> articuloNf7Secnom7s) {
        this.articuloNf7Secnom7s = articuloNf7Secnom7s;
    }

    public ArticuloNf7Secnom7 addArticuloNf7Secnom7(ArticuloNf7Secnom7 articuloNf7Secnom7) {
        getArticuloNf7Secnom7s().add(articuloNf7Secnom7);
        articuloNf7Secnom7.setNf7Secnom7(this);

        return articuloNf7Secnom7;
    }

    public ArticuloNf7Secnom7 removeArticuloNf7Secnom7(ArticuloNf7Secnom7 articuloNf7Secnom7) {
        getArticuloNf7Secnom7s().remove(articuloNf7Secnom7);
        articuloNf7Secnom7.setNf7Secnom7(null);

        return articuloNf7Secnom7;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return this.nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    public List<FuncionarioNf7> getFuncionarioNf7s() {
        return this.funcionarioNf7s;
    }

    public void setFuncionarioNf7s(List<FuncionarioNf7> funcionarioNf7s) {
        this.funcionarioNf7s = funcionarioNf7s;
    }

    public FuncionarioNf7 addFuncionarioNf7(FuncionarioNf7 funcionarioNf7) {
        getFuncionarioNf7s().add(funcionarioNf7);
        funcionarioNf7.setNf7Secnom7(this);

        return funcionarioNf7;
    }

    public FuncionarioNf7 removeFuncionarioNf7(FuncionarioNf7 funcionarioNf7) {
        getFuncionarioNf7s().remove(funcionarioNf7);
        funcionarioNf7.setNf7Secnom7(null);

        return funcionarioNf7;
    }

    public List<DescuentoFielNf7> getDescuentoFielNf7s() {
        return this.descuentoFielNf7s;
    }

    public void setDescuentoFielNf7s(List<DescuentoFielNf7> descuentoFielNf7s) {
        this.descuentoFielNf7s = descuentoFielNf7s;
    }

    public DescuentoFielNf7 addDescuentoFielNf7(DescuentoFielNf7 descuentoFielNf7) {
        getDescuentoFielNf7s().add(descuentoFielNf7);
        descuentoFielNf7.setNf7Secnom7(this);

        return descuentoFielNf7;
    }

    public DescuentoFielNf7 removeDescuentoFielNf7(DescuentoFielNf7 descuentoFielNf7) {
        getDescuentoFielNf7s().remove(descuentoFielNf7);
        descuentoFielNf7.setNf7Secnom7(null);

        return descuentoFielNf7;
    }

    public List<PromoTemporadaNf7> getPromoTemporadaNf7s() {
        return this.promoTemporadaNf7s;
    }

    public void setPromoTemporadaNf7s(List<PromoTemporadaNf7> promoTemporadaNf7s) {
        this.promoTemporadaNf7s = promoTemporadaNf7s;
    }

    public PromoTemporadaNf7 addPromoTemporadaNf7(PromoTemporadaNf7 promoTemporadaNf7) {
        getPromoTemporadaNf7s().add(promoTemporadaNf7);
        promoTemporadaNf7.setNf7Secnom7(this);

        return promoTemporadaNf7;
    }

    public PromoTemporadaNf7 removePromoTemporadaNf7(PromoTemporadaNf7 promoTemporadaNf7) {
        getPromoTemporadaNf7s().remove(promoTemporadaNf7);
        promoTemporadaNf7.setNf7Secnom7(null);

        return promoTemporadaNf7;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Nf7Secnom7 fromNf7Secnom7DTO(Nf7Secnom7DTO nf7Secnom7DTO) {
        Nf7Secnom7 nf7Secnom7 = new Nf7Secnom7();
        BeanUtils.copyProperties(nf7Secnom7DTO, nf7Secnom7);
        return nf7Secnom7;
    }

    // <-- SERVER
    public static Nf7Secnom7DTO toNf7Secnom7DTO(Nf7Secnom7 nf7Secnom7) {
        Nf7Secnom7DTO nf7Secnom7DTO = new Nf7Secnom7DTO();
        BeanUtils.copyProperties(nf7Secnom7, nf7Secnom7DTO);
        return nf7Secnom7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public static Nf7Secnom7DTO toNf7Secnom7DTOEntitiesNull(Nf7Secnom7 nf7Secnom7) {
        Nf7Secnom7DTO nf7Secnom7DTO = toNf7Secnom7DTO(nf7Secnom7);
        nf7Secnom7DTO.setNf6Secnom6(null);// Entity
        nf7Secnom7DTO.setArticuloNf7Secnom7s(null);// Entities
        nf7Secnom7DTO.setDescuentoFielNf7s(null);// Entities
        nf7Secnom7DTO.setFuncionarioNf7s(null);// Entities
        nf7Secnom7DTO.setPromoTemporadaNf7s(null);// Entities
        return nf7Secnom7DTO;
    }

    // <-- SERVER
    public Nf7Secnom7DTO toNf7Secnom7DTOEntitiesNull() {
        Nf7Secnom7DTO nf7Secnom7DTO = toNf7Secnom7DTO(this);
        nf7Secnom7DTO.setNf6Secnom6(null);// Entity
        nf7Secnom7DTO.setArticuloNf7Secnom7s(null);// Entities
        nf7Secnom7DTO.setDescuentoFielNf7s(null);// Entities
        nf7Secnom7DTO.setFuncionarioNf7s(null);// Entities
        nf7Secnom7DTO.setPromoTemporadaNf7s(null);// Entities
        return nf7Secnom7DTO;
    }

    // <-- SERVER
    public Nf7Secnom7DTO toNf7Secnom7DTOEntitiesNullNf6() {
        Nf7Secnom7DTO nf7Secnom7DTO = toNf7Secnom7DTO(this);
        nf7Secnom7DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());// Entity
        nf7Secnom7DTO.setArticuloNf7Secnom7s(null);// Entities
        nf7Secnom7DTO.setDescuentoFielNf7s(null);// Entities
        nf7Secnom7DTO.setFuncionarioNf7s(null);// Entities
        nf7Secnom7DTO.setPromoTemporadaNf7s(null); // Entities
        return nf7Secnom7DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Nf7Secnom7 fromNf7Secnom7DTOEntitiesFull(Nf7Secnom7DTO nf7Secnom7DTO) {
        Nf7Secnom7 nf7Secnom7 = fromNf7Secnom7DTO(nf7Secnom7DTO);
        if (nf7Secnom7DTO.getNf6Secnom6() != null) {
            nf7Secnom7.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(nf7Secnom7DTO.getNf6Secnom6()));
        }
        return nf7Secnom7;
    }
    // ######################## FULL, ENTRADA ########################

}
