package com.peluqueria.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the nota_remision_cab database table.
 *
 */
@Entity
@Table(name = "nota_remision_cab", schema = "factura_cliente")
@NamedQuery(name = "NotaRemisionCab.findAll", query = "SELECT n FROM NotaRemisionCab n")
public class NotaRemisionCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_remision_cab")
    private Long idNotaRemisionCab;

    @Column(name = "direc_punto_llegada")
    private String direcPuntoLlegada;

    @Column(name = "direc_punto_partida")
    private String direcPuntoPartida;

    @Column(name = "fecha_fin_traslado")
    private Timestamp fechaFinTraslado;

    @Column(name = "fecha_inicio_traslado")
    private Timestamp fechaInicioTraslado;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to MotivoTraslado
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_motivo_traslado")
    private MotivoTraslado motivoTraslado;

    // bi-directional many-to-one association to NotaRemisionDet
    @OneToMany(mappedBy = "notaRemisionCab")
    private List<NotaRemisionDet> notaRemisionDets;

    public NotaRemisionCab() {
    }

    public Long getIdNotaRemisionCab() {
        return this.idNotaRemisionCab;
    }

    public void setIdNotaRemisionCab(Long idNotaRemisionCab) {
        this.idNotaRemisionCab = idNotaRemisionCab;
    }

    public String getDirecPuntoLlegada() {
        return this.direcPuntoLlegada;
    }

    public void setDirecPuntoLlegada(String direcPuntoLlegada) {
        this.direcPuntoLlegada = direcPuntoLlegada;
    }

    public String getDirecPuntoPartida() {
        return this.direcPuntoPartida;
    }

    public void setDirecPuntoPartida(String direcPuntoPartida) {
        this.direcPuntoPartida = direcPuntoPartida;
    }

    public Timestamp getFechaFinTraslado() {
        return this.fechaFinTraslado;
    }

    public void setFechaFinTraslado(Timestamp fechaFinTraslado) {
        this.fechaFinTraslado = fechaFinTraslado;
    }

    public Timestamp getFechaInicioTraslado() {
        return this.fechaInicioTraslado;
    }

    public void setFechaInicioTraslado(Timestamp fechaInicioTraslado) {
        this.fechaInicioTraslado = fechaInicioTraslado;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public MotivoTraslado getMotivoTraslado() {
        return this.motivoTraslado;
    }

    public void setMotivoTraslado(MotivoTraslado motivoTraslado) {
        this.motivoTraslado = motivoTraslado;
    }

    public List<NotaRemisionDet> getNotaRemisionDets() {
        return this.notaRemisionDets;
    }

    public void setNotaRemisionDets(List<NotaRemisionDet> notaRemisionDets) {
        this.notaRemisionDets = notaRemisionDets;
    }

    public NotaRemisionDet addNotaRemisionDet(NotaRemisionDet notaRemisionDet) {
        getNotaRemisionDets().add(notaRemisionDet);
        notaRemisionDet.setNotaRemisionCab(this);
        return notaRemisionDet;
    }

    public NotaRemisionDet removeNotaRemisionDet(NotaRemisionDet notaRemisionDet) {
        getNotaRemisionDets().remove(notaRemisionDet);
        notaRemisionDet.setNotaRemisionCab(null);
        return notaRemisionDet;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

}
