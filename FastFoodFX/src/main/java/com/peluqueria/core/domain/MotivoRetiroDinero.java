package com.peluqueria.core.domain;

import com.peluqueria.dto.MotivoRetiroDineroDTO;
import com.peluqueria.dto.RetiroDineroDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the motivo_retiro_dinero database table.
 *
 */
@Entity
@Table(name = "motivo_retiro_dinero", schema = "caja")
@NamedQuery(name = "MotivoRetiroDinero.findAll", query = "SELECT m FROM MotivoRetiroDinero m")
public class MotivoRetiroDinero implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_motivo_retiro")
    private Long idMotivoRetiro;

    @Column(name = "descripcion_motivo_retiro")
    private String descripcionMotivoRetiro;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to RetiroDinero
    @OneToMany(mappedBy = "motivoRetiroDinero")
    private List<RetiroDinero> retiroDineros;

    public MotivoRetiroDinero() {
    }

    public Long getIdMotivoRetiro() {
        return this.idMotivoRetiro;
    }

    public void setIdMotivoRetiro(Long idMotivoRetiro) {
        this.idMotivoRetiro = idMotivoRetiro;
    }

    public String getDescripcionMotivoRetiro() {
        return this.descripcionMotivoRetiro;
    }

    public void setDescripcionMotivoRetiro(String descripcionMotivoRetiro) {
        this.descripcionMotivoRetiro = descripcionMotivoRetiro;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<RetiroDinero> getRetiroDineros() {
        return this.retiroDineros;
    }

    public void setRetiroDineros(List<RetiroDinero> retiroDineros) {
        this.retiroDineros = retiroDineros;
    }

    public MotivoRetiroDineroDTO toBDMotivoRetiroDineroDTO() {
        MotivoRetiroDineroDTO mrdDTO = toMotivoRetiroDineroDTO(this);
        mrdDTO.setRetiroDineros(null);
        return mrdDTO;
    }

    public MotivoRetiroDineroDTO toMotivoRetiroDineroDTO() {
        MotivoRetiroDineroDTO mrdDTO = toMotivoRetiroDineroDTO(this);
        if (!this.retiroDineros.isEmpty()) {
            List<RetiroDinero> re = new ArrayList<RetiroDinero>();
            List<RetiroDineroDTO> reDTO = new ArrayList<RetiroDineroDTO>();
            for (RetiroDinero r : re) {
                reDTO.add(r.toBDRetiroDineroDTO());
            }
            mrdDTO.setRetiroDineros(reDTO);
        }
        return mrdDTO;
    }

    // metodo para copiar de Entity a DTO
    public static MotivoRetiroDineroDTO toMotivoRetiroDineroDTO(
            MotivoRetiroDinero mrd) {
        MotivoRetiroDineroDTO mrdDTO = new MotivoRetiroDineroDTO();
        BeanUtils.copyProperties(mrd, mrdDTO);
        return mrdDTO;
    }

    public static MotivoRetiroDinero fromMotivoRetiroDineroDTO(
            MotivoRetiroDineroDTO mrdDTO) {
        MotivoRetiroDinero mrd = new MotivoRetiroDinero();
        BeanUtils.copyProperties(mrdDTO, mrd);
        return mrd;
    }

}
