package com.peluqueria.core.domain;

import com.peluqueria.dto.RolFuncionDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rol_funcion database table.
 *
 */
@Entity
@Table(name = "rol_funcion", schema = "seguridad")
@NamedQuery(name = "RolFuncion.findAll", query = "SELECT r FROM RolFuncion r")
public class RolFuncion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rol_funcion")
    private Long idRolFuncion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Funcion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcion")
    private Funcion funcion;

    // bi-directional many-to-one association to Rol
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rol")
    private Rol rol;

    public RolFuncion() {
    }

    public Long getIdRolFuncion() {
        return this.idRolFuncion;
    }

    public void setIdRolFuncion(Long idRolFuncion) {
        this.idRolFuncion = idRolFuncion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Funcion getFuncion() {
        return this.funcion;
    }

    public void setFuncion(Funcion funcion) {
        this.funcion = funcion;
    }

    public Rol getRol() {
        return this.rol;
    }

    public void setRol(Rol rol) {
        this.rol = rol;
    }

    public RolFuncionDTO toRolFuncionDTO() {
        RolFuncionDTO rfDTO = toRolFuncionDTO(this);
        if (this.rol != null) {
            rfDTO.setRol(this.getRol().toBDRolDTO());
        }
        if (this.funcion != null) {
            rfDTO.setFuncion(this.getFuncion().toBdFuncionDTO());
        }
        return rfDTO;
    }

    public RolFuncionDTO toRolFuncionDTO(RolFuncion rf) {
        RolFuncionDTO rfDTO = new RolFuncionDTO();
        BeanUtils.copyProperties(rf, rfDTO);
        return rfDTO;
    }

    public static RolFuncion fromRolFuncionDTO(RolFuncionDTO rfDTO) {
        RolFuncion rf = new RolFuncion();
        BeanUtils.copyProperties(rfDTO, rf);
        return rf;
    }

    public static RolFuncion fromRolFuncionAsociadoDTO(RolFuncionDTO rfDTO) {
        RolFuncion rf = fromRolFuncionDTO(rfDTO);
        rf.setRol(Rol.fromRolDTO(rfDTO.getRol()));
        rf.setFuncion(Funcion.fromFuncionDTO(rfDTO.getFuncion()));
        return rf;
    }

    public static List<RolFuncion> fromRolFuncionAsociadoDTO(List<RolFuncionDTO> listRolFuncionDTO) {
        List<RolFuncion> listRolFuncion = new ArrayList<>();
        for (RolFuncionDTO rolFuncionDTO : listRolFuncionDTO) {
            RolFuncion rolFuncion = fromRolFuncionDTO(rolFuncionDTO);
            rolFuncion.setRol(Rol.fromRolDTO(rolFuncionDTO.getRol()));
            rolFuncion.setFuncion(Funcion.fromFuncionDTO(rolFuncionDTO.getFuncion()));
            listRolFuncion.add(rolFuncion);
        }
        return listRolFuncion;
    }

}
