package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf1DTO;

/**
 * The persistent class for the funcionario_nf1 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf1", schema = "descuento")
@NamedQuery(name = "FuncionarioNf1.findAll", query = "SELECT f FROM FuncionarioNf1 f")
public class FuncionarioNf1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf1")
    private Long idFuncionarioNf1;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf1_tipo")
    private Nf1Tipo nf1Tipo;

    public FuncionarioNf1() {
    }

    public Long getIdFuncionarioNf1() {
        return this.idFuncionarioNf1;
    }

    public void setIdFuncionarioNf1(Long idFuncionarioNf1) {
        this.idFuncionarioNf1 = idFuncionarioNf1;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf1Tipo getNf1Tipo() {
        return nf1Tipo;
    }

    public void setNf1Tipo(Nf1Tipo nf1Tipo) {
        this.nf1Tipo = nf1Tipo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf1 fromFuncionarioNf1DTO(FuncionarioNf1DTO funcionarioNf1DTO) {
        FuncionarioNf1 funcionarioNf1 = new FuncionarioNf1();
        BeanUtils.copyProperties(funcionarioNf1DTO, funcionarioNf1);
        return funcionarioNf1;
    }

    // <-- SERVER
    public static FuncionarioNf1DTO toFuncionarioNf1DTO(FuncionarioNf1 funcionarioNf1) {
        FuncionarioNf1DTO funcionarioNf1DTO = new FuncionarioNf1DTO();
        BeanUtils.copyProperties(funcionarioNf1, funcionarioNf1DTO);
        return funcionarioNf1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf1 fromFuncionarioNf1DTOEntitiesFull(FuncionarioNf1DTO funcionarioNf1DTO) {
        FuncionarioNf1 funcionarioNf1 = fromFuncionarioNf1DTO(funcionarioNf1DTO);
        if (funcionarioNf1DTO.getNf1Tipo() != null) {
            funcionarioNf1.setNf1Tipo(Nf1Tipo.fromNf1TipoDTO(funcionarioNf1DTO.getNf1Tipo()));
        }
        return funcionarioNf1;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf1DTO toFuncionarioNf1DTO() {
        FuncionarioNf1DTO funcionarioNf1DTO = toFuncionarioNf1DTO(this);
        if (this.getNf1Tipo() != null) {
            funcionarioNf1DTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());
        }
        return funcionarioNf1DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
