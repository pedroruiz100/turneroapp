package com.peluqueria.core.domain;

import com.peluqueria.dto.ClientePendienteDTO;
import com.peluqueria.dto.ServPendienteDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the rol database table.
 *
 */
@Entity
@Table(name = "cliente_pendiente", schema = "estetica")
@NamedQuery(name = "ClientePendiente.findAll", query = "SELECT r FROM ClientePendiente r")
public class ClientePendiente implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cliente_pendiente")
    private Long idClientePendiente;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    @Column(name = "procesado")
    private Boolean procesado;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FacturaCabClientePendiente
    @OneToMany(mappedBy = "clientePendiente", fetch = FetchType.LAZY)
    private List<FacturaCabClientePendiente> facturaCabClientePendiente;

    // bi-directional many-to-one association to ServPendiente
    @OneToMany(mappedBy = "clientePendiente", fetch = FetchType.LAZY)
    private List<ServPendiente> servPendiente;

    public ClientePendiente() {
    }

    public Long getIdClientePendiente() {
        return idClientePendiente;
    }

    public void setIdClientePendiente(Long idClientePendiente) {
        this.idClientePendiente = idClientePendiente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Boolean getProcesado() {
        return procesado;
    }

    public void setProcesado(Boolean procesado) {
        this.procesado = procesado;
    }

    public Timestamp getFechaAlta() {
        return fechaAlta;
    }

    public List<FacturaCabClientePendiente> getFacturaCabClientePendiente() {
        return facturaCabClientePendiente;
    }

    public void setFacturaCabClientePendiente(
            List<FacturaCabClientePendiente> facturaCabClientePendiente) {
        this.facturaCabClientePendiente = facturaCabClientePendiente;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public List<ServPendiente> getServPendiente() {
        return servPendiente;
    }

    public void setServPendiente(List<ServPendiente> servPendiente) {
        this.servPendiente = servPendiente;
    }

    public String getUsuAlta() {
        return usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public ClientePendienteDTO toClientePendienteDTO() {
        ClientePendienteDTO cliDTO = toClientePendienteDTO(this);
        if (this.cliente != null) {
            cliDTO.setCliente(this.getCliente().toClienteBDDTO());
        }
        cliDTO.setServPendiente(null);
        cliDTO.setFacturaCabClientePendiente(null);
        return cliDTO;
    }

    public ClientePendienteDTO toEliminarPendienteDTO() {
        ClientePendienteDTO cliDTO = toClientePendienteDTO(this);
        if (this.cliente != null) {
            cliDTO.setCliente(this.getCliente().toClienteBDDTO());
        }
        if (!this.servPendiente.isEmpty()) {
            List<ServPendienteDTO> listSrv = new ArrayList<>();
            for (ServPendiente sp : this.getServPendiente()) {
                listSrv.add(sp.toServPendienteSinCliPenDTO());
            }
            cliDTO.setServPendiente(listSrv);
        }
        cliDTO.setFacturaCabClientePendiente(null);
        return cliDTO;
    }

    public ClientePendienteDTO toBDClientePendienteDTO() {
        ClientePendienteDTO cliDTO = toClientePendienteDTO(this);
        cliDTO.setCliente(null);
        cliDTO.setServPendiente(null);
        cliDTO.setFacturaCabClientePendiente(null);
        return cliDTO;
    }

    private ClientePendienteDTO toClientePendienteDTO(
            ClientePendiente clientePendiente) {
        ClientePendienteDTO cliDTO = new ClientePendienteDTO();
        BeanUtils.copyProperties(clientePendiente, cliDTO);
        return cliDTO;
    }

    public static ClientePendiente fromClientePendiente(
            ClientePendienteDTO cliDTO) {
        ClientePendiente cli = new ClientePendiente();
        BeanUtils.copyProperties(cliDTO, cli);
        return cli;
    }

    public static ClientePendiente fromClientePendienteAsociado(
            ClientePendienteDTO cliDTO) {
        ClientePendiente cli = fromClientePendiente(cliDTO);
        cli.setCliente(Cliente.fromClienteDTO(cliDTO.getCliente()));
        return cli;
    }

}
