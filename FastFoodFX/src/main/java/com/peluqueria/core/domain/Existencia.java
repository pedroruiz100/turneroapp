package com.peluqueria.core.domain;

import com.peluqueria.dto.ExistenciaDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the nf2_sfamilia database table.
 *
 */
@Entity
@Table(name = "existencia", schema = "stock")
@NamedQuery(name = "Existencia.findAll", query = "SELECT n FROM Existencia n")
public class Existencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_existencia")
    private Long idExistencia;

    @Column(name = "cantidad")
    private long cantidad;

    // bi-directional one-to-many association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_deposito")
    private Deposito deposito;

    // bi-directional one-to-many association to Nf1Tipo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    public Existencia() {
    }

    public Long getIdExistencia() {
        return idExistencia;
    }

    public void setIdExistencia(Long idExistencia) {
        this.idExistencia = idExistencia;
    }

    public long getCantidad() {
        return cantidad;
    }

    public void setCantidad(long cantidad) {
        this.cantidad = cantidad;
    }

    public Deposito getDeposito() {
        return deposito;
    }

    public void setDeposito(Deposito deposito) {
        this.deposito = deposito;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static Existencia fromExistenciaDTO(ExistenciaDTO exiDTO) {
        Existencia exi = new Existencia();
        BeanUtils.copyProperties(exiDTO, exi);
        return exi;
    }

    // <-- SERVER
    public static ExistenciaDTO toExistenciaDTO(Existencia exis) {
        ExistenciaDTO exiDTO = new ExistenciaDTO();
        BeanUtils.copyProperties(exis, exiDTO);
        return exiDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public ExistenciaDTO toExistenciaDTO() {
        ExistenciaDTO existenciaDTO = toExistenciaDTO(this);
        if (this.articulo != null) {
            existenciaDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        if (this.deposito != null) {
            existenciaDTO.setDeposito(this.getDeposito().toDepositoDTO());
        }
        return existenciaDTO;
    }

    // <-- SERVER
//    public Nf2SfamiliaDTO toNf2SfamiliaDTOEntitiesNull() {
//        Nf2SfamiliaDTO nf2SfamiliaDTO = toNf2SfamiliaDTO(this);
//        nf2SfamiliaDTO.setNf1Tipo(null);// Entity
//        nf2SfamiliaDTO.setNf3Sseccions(null);// Entities
//        nf2SfamiliaDTO.setArticuloNf2Sfamilias(null);// Entities
//        nf2SfamiliaDTO.setDescuentoFielNf2s(null);// Entities
//        nf2SfamiliaDTO.setFuncionarioNf2s(null);// Entities
//        nf2SfamiliaDTO.setPromoTemporadaNf2s(null); // Entities
//        return nf2SfamiliaDTO;
//    }
    // <-- SERVER
//    public Nf2SfamiliaDTO toNf2SfamiliaDTOEntitiesNullNf1() {
//        Nf2SfamiliaDTO nf2SfamiliaDTO = toNf2SfamiliaDTO(this);
//        nf2SfamiliaDTO.setNf1Tipo(this.getNf1Tipo().toNf1TipoDTOEntitiesNull());// Entity
//        nf2SfamiliaDTO.setNf3Sseccions(null);// Entities
//        nf2SfamiliaDTO.setArticuloNf2Sfamilias(null);// Entities
//        nf2SfamiliaDTO.setDescuentoFielNf2s(null);// Entities
//        nf2SfamiliaDTO.setFuncionarioNf2s(null);// Entities
//        nf2SfamiliaDTO.setPromoTemporadaNf2s(null); // Entities
//        return nf2SfamiliaDTO;
//    }
    // ######################## NULO, FULL; SALIDA ########################
    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static Existencia fromExistenciaAsociadoDTO(ExistenciaDTO exiDTO) {
        Existencia exi = fromExistenciaDTO(exiDTO);
        exi.setArticulo(Articulo.fromArticuloDTO(exiDTO.getArticulo()));
        exi.setDeposito(Deposito.fromMarcaDTO(exiDTO.getDeposito()));
//        ar.setMarca(Marca.fromMarcaDTO(articuloDTO.getMarca()));
//        ar.setUnidad(Unidad.fromUnidadDTO(articuloDTO.getUnidad()));
//        ar.setSeccionSub(SeccionSub.fromSeccionSubDTO(articuloDTO
//                .getSeccionSub()));
//        return ar;
        return exi;
    }
    // ######################## FULL, ENTRADA ########################

}
