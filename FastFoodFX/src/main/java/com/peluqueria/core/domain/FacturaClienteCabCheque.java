package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabChequeDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_cheque database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_cheque", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabCheque.findAll", query = "SELECT f FROM FacturaClienteCabCheque f")
public class FacturaClienteCabCheque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_cheque")
    private Long idFacturaClienteCabCheque;

    @Column(name = "descripcion")
    private String descripcion;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    @Column(name = "monto_cheque")
    private Integer montoCheque;

    @Column(name = "nro_cheque")
    private String nroCheque;

    public FacturaClienteCabCheque() {
    }

    public Long getIdFacturaClienteCabCheque() {
        return this.idFacturaClienteCabCheque;
    }

    public void setIdFacturaClienteCabCheque(Long idFacturaClienteCabCheque) {
        this.idFacturaClienteCabCheque = idFacturaClienteCabCheque;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Integer getMontoCheque() {
        return this.montoCheque;
    }

    public void setMontoCheque(Integer montoCheque) {
        this.montoCheque = montoCheque;
    }

    public String getNroCheque() {
        return this.nroCheque;
    }

    public void setNroCheque(String nroCheque) {
        this.nroCheque = nroCheque;
    }

    public FacturaClienteCabChequeDTO toBDFacturaClienteCabChequeDTO() {
        FacturaClienteCabChequeDTO facDTO = toFacturaClienteCabChequeDTO(this);
        facDTO.setFacturaClienteCabDTO(null);
        return facDTO;
    }

    public FacturaClienteCabChequeDTO toFacturaClienteCabChequeDTO() {
        FacturaClienteCabChequeDTO facDTO = toFacturaClienteCabChequeDTO(this);
        if (this.facturaClienteCab != null) {
            FacturaClienteCab fac = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCabDTO(fac.toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    public static FacturaClienteCabChequeDTO toFacturaClienteCabChequeDTO(
            FacturaClienteCabCheque facturaClienteCabCheque) {
        FacturaClienteCabChequeDTO facDTO = new FacturaClienteCabChequeDTO();
        BeanUtils.copyProperties(facturaClienteCabCheque, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabCheque fromFacturaClienteCabChequeDTO(
            FacturaClienteCabChequeDTO facDTO) {
        FacturaClienteCabCheque fac = new FacturaClienteCabCheque();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabCheque fromFacturaClienteCabChequeAsociadoDTO(
            FacturaClienteCabChequeDTO facDTO) {
        FacturaClienteCabCheque fac = fromFacturaClienteCabChequeDTO(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(facDTO
                .getFacturaClienteCabDTO()));
        return fac;
    }

}
