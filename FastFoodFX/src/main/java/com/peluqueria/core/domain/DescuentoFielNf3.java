package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoFielNf3DTO;

/**
 * The persistent class for the descuento_fiel_nf3 database table.
 *
 */
@Entity
@Table(name = "descuento_fiel_nf3", schema = "descuento")
@NamedQuery(name = "DescuentoFielNf3.findAll", query = "SELECT d FROM DescuentoFielNf3 d")
public class DescuentoFielNf3 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_fiel_nf3")
    private Long idDescuentoFielNf3;

    // bi-directional many-to-one association to DescuentoFielCab
    @ManyToOne
    @JoinColumn(name = "id_descuento_fiel_cab")
    private DescuentoFielCab descuentoFielCab;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    public DescuentoFielNf3() {
    }

    public Long getIdDescuentoFielNf3() {
        return this.idDescuentoFielNf3;
    }

    public void setIdDescuentoFielNf3(Long idDescuentoFielNf3) {
        this.idDescuentoFielNf3 = idDescuentoFielNf3;
    }

    public DescuentoFielCab getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(DescuentoFielCab descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoFielNf3 fromDescuentoFielNf3DTO(DescuentoFielNf3DTO descuentoFielNf3DTO) {
        DescuentoFielNf3 descuentoFielNf3 = new DescuentoFielNf3();
        BeanUtils.copyProperties(descuentoFielNf3DTO, descuentoFielNf3);
        return descuentoFielNf3;
    }

    // <-- SERVER
    public static DescuentoFielNf3DTO toDescuentoFielNf3DTO(DescuentoFielNf3 descuentoFielNf3) {
        DescuentoFielNf3DTO descuentoFielNf3DTO = new DescuentoFielNf3DTO();
        BeanUtils.copyProperties(descuentoFielNf3, descuentoFielNf3DTO);
        return descuentoFielNf3DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoFielNf3 fromDescuentoFielNf3DTOEntitiesFull(DescuentoFielNf3DTO descuentoFielNf3DTO) {
        DescuentoFielNf3 descuentoFielNf3 = fromDescuentoFielNf3DTO(descuentoFielNf3DTO);
        if (descuentoFielNf3DTO.getNf3Sseccion() != null) {
            descuentoFielNf3.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(descuentoFielNf3DTO.getNf3Sseccion()));
        }
        if (descuentoFielNf3DTO.getDescuentoFielCab() != null) {
            descuentoFielNf3.setDescuentoFielCab(
                    DescuentoFielCab.fromDescuentoFielCabSinSeccionDTO(descuentoFielNf3DTO.getDescuentoFielCab()));
        }
        return descuentoFielNf3;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoFielNf3DTO toDescuentoFielNf3DTOEntityFull() {
        DescuentoFielNf3DTO descuentoFielNf3DTO = toDescuentoFielNf3DTO(this);
        if (this.getDescuentoFielCab() != null) {
            descuentoFielNf3DTO.setDescuentoFielCab(this.getDescuentoFielCab().toDescuentoFielCabDTOEntitiesNull());
        } else {
            descuentoFielNf3DTO.setDescuentoFielCab(null);
        }
        if (this.getNf3Sseccion() != null) {
            descuentoFielNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNull());
        } else {
            descuentoFielNf3DTO.setNf3Sseccion(null);
        }
        return descuentoFielNf3DTO;
    }

    // <-- SERVER
    public DescuentoFielNf3DTO toDescuentoFielNf3DTOEntityNull() {
        DescuentoFielNf3DTO descuentoFielNf3DTO = toDescuentoFielNf3DTO(this);
        descuentoFielNf3DTO.setDescuentoFielCab(null);
        descuentoFielNf3DTO.setNf3Sseccion(null);
        return descuentoFielNf3DTO;
    }

    // <-- SERVER
    public DescuentoFielNf3DTO toDescuentoFielNf3DTONf2() {
        DescuentoFielNf3DTO descuentoFielNf3DTO = toDescuentoFielNf3DTO(this);
        descuentoFielNf3DTO.setDescuentoFielCab(null);
        if (this.getNf3Sseccion() != null) {
            descuentoFielNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        } else {
            descuentoFielNf3DTO.setNf3Sseccion(null);
        }
        return descuentoFielNf3DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
