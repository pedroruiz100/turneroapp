package com.peluqueria.core.domain;

import com.peluqueria.dto.DescTarjetaDTO;
import com.peluqueria.dto.FacturaClienteCabTarjetaDTO;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_tarjeta database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_tarjeta", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabTarjeta.findAll", query = "SELECT f FROM FacturaClienteCabTarjeta f")
public class FacturaClienteCabTarjeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_tarjeta")
    private Long idFacturaClienteCabTarjeta;

    @Column(name = "descripcion_tarj")
    private String descripcionTarj;

    @Column(name = "cod_autorizacion")
    private String codAutorizacion;

    @Column(name = "monto")
    private Integer monto;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarjeta")
    private Tarjeta tarjeta;

    // bi-directional many-to-one association to DescTarjeta
    @OneToMany(mappedBy = "facturaClienteCabTarjeta")
    private List<DescTarjeta> descTarjetas;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public FacturaClienteCabTarjeta() {
    }

    public Long getIdFacturaClienteCabTarjeta() {
        return this.idFacturaClienteCabTarjeta;
    }

    public void setIdFacturaClienteCabTarjeta(Long idFacturaClienteCabTarjeta) {
        this.idFacturaClienteCabTarjeta = idFacturaClienteCabTarjeta;
    }

    public String getDescripcionTarj() {
        return this.descripcionTarj;
    }

    public void setDescripcionTarj(String descripcionTarj) {
        this.descripcionTarj = descripcionTarj;
    }

    public List<DescTarjeta> getDescTarjetas() {
        return this.descTarjetas;
    }

    public void setDescTarjetas(List<DescTarjeta> descTarjetas) {
        this.descTarjetas = descTarjetas;
    }

    public DescTarjeta addDescTarjeta(DescTarjeta descTarjeta) {
        getDescTarjetas().add(descTarjeta);
        descTarjeta.setFacturaClienteCabTarjeta(this);
        return descTarjeta;
    }

    public String getCodAutorizacion() {
        return codAutorizacion;
    }

    public void setCodAutorizacion(String codAutorizacion) {
        this.codAutorizacion = codAutorizacion;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

    public DescTarjeta removeDescTarjeta(DescTarjeta descTarjeta) {
        getDescTarjetas().remove(descTarjeta);
        descTarjeta.setFacturaClienteCabTarjeta(null);
        return descTarjeta;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Tarjeta getTarjeta() {
        return tarjeta;
    }

    public void setTarjeta(Tarjeta tarjeta) {
        this.tarjeta = tarjeta;
    }

    public FacturaClienteCabTarjetaDTO toBDFacturaClienteCabTarjetaDTO() {
        FacturaClienteCabTarjetaDTO facDTO = toFacturaClienteCabTarjetaDTO(this);
        if (this.tarjeta != null) {
            Tarjeta tar = this.getTarjeta();
            facDTO.setTarjeta(tar.toTarjetaDescriDTO());
        }
        if (this.facturaClienteCab != null) {
            FacturaClienteCab fac = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(fac.toBDFacturaClienteCabDTO());
        }
        facDTO.setDescTarjetas(null);
        return facDTO;
    }

    public FacturaClienteCabTarjetaDTO toFacturaClienteCabTarjetaDTO() {
        FacturaClienteCabTarjetaDTO facDTO = toFacturaClienteCabTarjetaDTO(this);
        if (this.tarjeta != null) {
            Tarjeta tar = this.getTarjeta();
            facDTO.setTarjeta(tar.toTarjetaDescriDTO());
        }
        if (this.facturaClienteCab != null) {
            FacturaClienteCab fac = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(fac.toBDFacturaClienteCabDTO());
        }
        if (!this.descTarjetas.isEmpty()) {
            List<DescTarjetaDTO> tar = new ArrayList<DescTarjetaDTO>();
            for (DescTarjeta desc : this.getDescTarjetas()) {
                tar.add(desc.toDescriDescTarjetaDTO());
            }
            facDTO.setDescTarjetas(tar);
        }
        return facDTO;
    }

    public FacturaClienteCabTarjetaDTO toBDFacturaClienteCabTarjetaConDetalleDTO() {
        FacturaClienteCabTarjetaDTO facDTO = toFacturaClienteCabTarjetaDTO(this);
        if (this.tarjeta != null) {
            Tarjeta tar = this.getTarjeta();
            facDTO.setTarjeta(tar.toTarjetaDescriDTO());
        }
        if (this.facturaClienteCab != null) {
            FacturaClienteCab fac = this.getFacturaClienteCab();
            facDTO.setFacturaClienteCab(fac.toBDHIstoricoCajaDetalleDTO());
        }
        facDTO.setDescTarjetas(null);
        return facDTO;
    }

    public FacturaClienteCabTarjetaDTO toFacturaClienteCabTarjetaSinDetalleDTO() {
        FacturaClienteCabTarjetaDTO facDTO = toFacturaClienteCabTarjetaDTO(this);
        facDTO.setTarjeta(null);
        facDTO.setFacturaClienteCab(null);
        facDTO.setDescTarjetas(null);
        return facDTO;
    }

    public static FacturaClienteCabTarjetaDTO toFacturaClienteCabTarjetaDTO(
            FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
        FacturaClienteCabTarjetaDTO facDTO = new FacturaClienteCabTarjetaDTO();
        BeanUtils.copyProperties(facturaClienteCabTarjeta, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabTarjeta fromFacturaClienteCabTarjetaDTO(
            FacturaClienteCabTarjetaDTO facturaClienteCabTarjetaDTO) {
        FacturaClienteCabTarjeta fac = new FacturaClienteCabTarjeta();
        BeanUtils.copyProperties(facturaClienteCabTarjetaDTO, fac);
        return fac;
    }

    public FacturaClienteCabTarjetaDTO toDescTarjetaDTO() {
        FacturaClienteCabTarjetaDTO facDTO = toFacturaClienteCabTarjetaDTO(this);
        if (this.tarjeta != null) {
            Tarjeta tar = this.getTarjeta();
            facDTO.setTarjeta(tar.toTarjetaDescriDTO());
        }
        facDTO.setFacturaClienteCab(null);
        if (!this.descTarjetas.isEmpty()) {
            List<DescTarjetaDTO> tar = new ArrayList<DescTarjetaDTO>();
            for (DescTarjeta desc : this.getDescTarjetas()) {
                tar.add(desc.toDescSinCabTarjDTO());
            }
            facDTO.setDescTarjetas(tar);
        }
        return facDTO;
    }

    public static FacturaClienteCabTarjeta fromFacturaClienteCabTarjetaAsociadoDTO(
            FacturaClienteCabTarjetaDTO facturaClienteCabTarjetaDTO) {
        FacturaClienteCabTarjeta fac = fromFacturaClienteCabTarjetaDTO(facturaClienteCabTarjetaDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facturaClienteCabTarjetaDTO
                        .getFacturaClienteCab()));
        fac.setTarjeta(Tarjeta.fromTarjetaDTO(facturaClienteCabTarjetaDTO
                .getTarjeta()));
        return fac;
    }

}
