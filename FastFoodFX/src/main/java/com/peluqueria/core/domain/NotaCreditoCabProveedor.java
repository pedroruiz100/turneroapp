package com.peluqueria.core.domain;

import com.peluqueria.dto.NotaCreditoCabProveedorDTO;
import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the nota_credito_cab database table.
 *
 */
@Entity
@Table(name = "nota_credito_cab_proveedor", schema = "factura_cliente")
@NamedQuery(name = "NotaCreditoCabProveedor.findAll", query = "SELECT n FROM NotaCreditoCabProveedor n")
public class NotaCreditoCabProveedor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_credito_cab_proveedor")
    private Long idNotaCreditoCabProveedor;

    @Column(name = "fecha_nota")
    private Date fechaNota;

    @Column(name = "nro_nota")
    private String nroNota;

    @Column(name = "nro_devolucion")
    private String nroDevolucion;

    @Column(name = "nro_oc")
    private String nroOc;

    public NotaCreditoCabProveedor() {
    }

    public Long getIdNotaCreditoCabProveedor() {
        return idNotaCreditoCabProveedor;
    }

    public void setIdNotaCreditoCabProveedor(Long idNotaCreditoCabProveedor) {
        this.idNotaCreditoCabProveedor = idNotaCreditoCabProveedor;
    }

    public Date getFechaNota() {
        return fechaNota;
    }

    public void setFechaNota(Date fechaNota) {
        this.fechaNota = fechaNota;
    }

    public String getNroNota() {
        return nroNota;
    }

    public void setNroNota(String nroNota) {
        this.nroNota = nroNota;
    }

    public String getNroOc() {
        return nroOc;
    }

    public void setNroOc(String nroOc) {
        this.nroOc = nroOc;
    }

    public String getNroDevolucion() {
        return nroDevolucion;
    }

    public void setNroDevolucion(String nroDevolucion) {
        this.nroDevolucion = nroDevolucion;
    }

    public NotaCreditoCabProveedorDTO toNotaCreditoCabProveedorDTO() {
        NotaCreditoCabProveedorDTO facDTO = toNotaCreditoCabProveedorDTO(this);
//        if (this.facturaClienteCab != null) {
//            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
//                    .toBDFacturaClienteCabDTO());
//        }
        return facDTO;
    }

//    public ArticuloDevolucionDTO toDescriArticuloDevolucionDTO() {
//        ArticuloDevolucionDTO facDTO = toArticuloDevolucionDTO(this);
//        facDTO.setFacturaCompraCab(null);
//        facDTO.setArticulo(null);
//        return facDTO;
//    }
    public static NotaCreditoCabProveedorDTO toNotaCreditoCabProveedorDTO(
            NotaCreditoCabProveedor facturaClienteDet) {
        NotaCreditoCabProveedorDTO facDTO = new NotaCreditoCabProveedorDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static NotaCreditoCabProveedor fromNotaCreditoCabProveedorDTO(
            NotaCreditoCabProveedorDTO facDTO) {
        NotaCreditoCabProveedor fac = new NotaCreditoCabProveedor();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static NotaCreditoCabProveedor fromNotaCreditoCabProveedorAsociado(
            NotaCreditoCabProveedorDTO facDTO) {
        NotaCreditoCabProveedor fac = fromNotaCreditoCabProveedorDTO(facDTO);
//        fac.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
//        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
