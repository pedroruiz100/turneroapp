package com.peluqueria.core.domain;

import com.peluqueria.dto.PaisDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the pais database table.
 *
 */
@Entity
@Table(name = "pais", schema = "general")
@NamedQuery(name = "Pai.findAll", query = "SELECT p FROM Pais p")
public class Pais implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_pais")
    private Long idPais;

    private Boolean activo;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Departamento
    @OneToMany(mappedBy = "pais")
    private List<Departamento> departamentos;

    // bi-directional many-to-one association to Departamento
    @OneToMany(mappedBy = "pais")
    private List<Cliente> clientes;

    // bi-directional many-to-one association to Proveedor
    @OneToMany(mappedBy = "pais")
    private List<Proveedor> proveedors;

    public Pais() {
    }

    public Long getIdPais() {
        return this.idPais;
    }

    public void setIdPais(Long idPais) {
        this.idPais = idPais;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Departamento> getDepartamentos() {
        return this.departamentos;
    }

    public void setDepartamentos(List<Departamento> departamentos) {
        this.departamentos = departamentos;
    }

    public Departamento addDepartamento(Departamento departamento) {
        getDepartamentos().add(departamento);
        departamento.setPais(this);
        return departamento;
    }

    public Departamento removeDepartamento(Departamento departamento) {
        getDepartamentos().remove(departamento);
        departamento.setPais(null);
        return departamento;
    }

    // ****************************************************************************************
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public Cliente addClientes(Cliente clientes) {
        getClientes().add(clientes);
        clientes.setPais(this);
        return clientes;
    }

    public Cliente removeClientes(Cliente clientes) {
        getClientes().remove(clientes);
        clientes.setPais(null);
        return clientes;
    }

    // ****************************************************************************************
    public List<Proveedor> getProveedors() {
        return proveedors;
    }

    public void setProveedors(List<Proveedor> proveedors) {
        this.proveedors = proveedors;
    }

    // DTOs****************************************************************************************
    public PaisDTO toPaisDTO() {
        PaisDTO paisDTO = this.toPaisDTO(this);
        paisDTO.setClienteDTO(null);
        paisDTO.setDepartamentosDTO(null);
        paisDTO.setProveedorDTO(null);
        return paisDTO;
    }

    public PaisDTO toPaisDTO(Pais Pais) {
        PaisDTO PaisDTO = new PaisDTO();
        BeanUtils.copyProperties(Pais, PaisDTO);
        return PaisDTO;
    }

    public static Pais fromPaisDTO(PaisDTO PaisDTO) {
        Pais Pais = new Pais();
        BeanUtils.copyProperties(PaisDTO, Pais);
        return Pais;
    }

}
