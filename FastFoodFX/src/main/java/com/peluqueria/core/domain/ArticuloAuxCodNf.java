package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloAuxCodNfDTO;

/**
 * The persistent class for the articulo_aux_cod_nf database table.
 *
 */
@Entity
@Table(name = "articulo_aux_cod_nf", schema = "stock")
@NamedQuery(name = "ArticuloAuxCodNf.findAll", query = "SELECT a FROM ArticuloAuxCodNf a")
public class ArticuloAuxCodNf implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_articulo_aux_cod_nf")
    private Long idArticuloAuxCodNf;

    @Column(name = "codigo")
    private String codigo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "nf")
    private Boolean nf;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    public ArticuloAuxCodNf() {
    }

    public Long getIdArticuloAuxCodNf() {
        return this.idArticuloAuxCodNf;
    }

    public void setIdArticuloAuxCodNf(Long idArticuloAuxCodNf) {
        this.idArticuloAuxCodNf = idArticuloAuxCodNf;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Boolean getNf() {
        return this.nf;
    }

    public void setNf(Boolean nf) {
        this.nf = nf;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloAuxCodNf fromArticuloAuxCodNfDTO(ArticuloAuxCodNfDTO articuloAuxCodNfDTO) {
        ArticuloAuxCodNf ar = new ArticuloAuxCodNf();
        BeanUtils.copyProperties(articuloAuxCodNfDTO, ar);
        return ar;
    }

    // <-- SERVER
    public static ArticuloAuxCodNfDTO toArticuloAuxCodNfDTO(ArticuloAuxCodNf articuloAuxCodNf) {
        ArticuloAuxCodNfDTO ArticuloAuxCodNfDTO = new ArticuloAuxCodNfDTO();
        BeanUtils.copyProperties(articuloAuxCodNf, ArticuloAuxCodNfDTO);
        return ArticuloAuxCodNfDTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, SALIDA ########################
    // <-- SERVER
    public static ArticuloAuxCodNfDTO toArticuloAuxCodNfDTOEntitiesNull(ArticuloAuxCodNf articuloAuxCodNf) {
        ArticuloAuxCodNfDTO articuloAuxCodNfDTO = toArticuloAuxCodNfDTO(articuloAuxCodNf);
        articuloAuxCodNfDTO.setArticulo(null);
        return articuloAuxCodNfDTO;
    }

    // <-- SERVER
    public ArticuloAuxCodNfDTO toArticuloAuxCodNfDTOEntitiesNull() {
        ArticuloAuxCodNfDTO articuloAuxCodNfDTO = toArticuloAuxCodNfDTO(this);
        articuloAuxCodNfDTO.setArticulo(null);
        return articuloAuxCodNfDTO;
    }
    // ######################## NULO, SALIDA ########################

    // ######################## FULL, ENTRADA ########################
    // --> SERVER
    public static ArticuloAuxCodNf fromArticuloAuxCodNfDTOEntitiesFull(ArticuloAuxCodNfDTO articuloAuxCodNfDTO) {
        ArticuloAuxCodNf articuloAuxCodNf = fromArticuloAuxCodNfDTO(articuloAuxCodNfDTO);
        if (articuloAuxCodNfDTO.getArticulo() != null) {
            articuloAuxCodNf.setArticulo(Articulo.fromArticuloDTO(articuloAuxCodNfDTO.getArticulo()));
        }
        return articuloAuxCodNf;
    }
    // ######################## FULL, ENTRADA ########################

}
