package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf2DTO;

/**
 * The persistent class for the funcionario_nf2 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf2", schema = "descuento")
@NamedQuery(name = "FuncionarioNf2.findAll", query = "SELECT f FROM FuncionarioNf2 f")
public class FuncionarioNf2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf2")
    private Long idFuncionarioNf2;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    public FuncionarioNf2() {
    }

    public Long getIdFuncionarioNf2() {
        return this.idFuncionarioNf2;
    }

    public void setIdFuncionarioNf2(Long idFuncionarioNf2) {
        this.idFuncionarioNf2 = idFuncionarioNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf2 fromFuncionarioNf2DTO(FuncionarioNf2DTO funcionarioNf2DTO) {
        FuncionarioNf2 funcionarioNf2 = new FuncionarioNf2();
        BeanUtils.copyProperties(funcionarioNf2DTO, funcionarioNf2);
        return funcionarioNf2;
    }

    // <-- SERVER
    public static FuncionarioNf2DTO toFuncionarioNf2DTO(FuncionarioNf2 funcionarioNf2) {
        FuncionarioNf2DTO funcionarioNf2DTO = new FuncionarioNf2DTO();
        BeanUtils.copyProperties(funcionarioNf2, funcionarioNf2DTO);
        return funcionarioNf2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf2 fromFuncionarioNf2DTOEntitiesFull(FuncionarioNf2DTO funcionarioNf2DTO) {
        FuncionarioNf2 funcionarioNf2 = fromFuncionarioNf2DTO(funcionarioNf2DTO);
        if (funcionarioNf2DTO.getNf2Sfamilia() != null) {
            funcionarioNf2.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(funcionarioNf2DTO.getNf2Sfamilia()));
        }
        return funcionarioNf2;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf2DTO toFuncionarioNf2DTONf1() {
        FuncionarioNf2DTO funcionarioNf2DTO = toFuncionarioNf2DTO(this);
        if (this.getNf2Sfamilia() != null) {
            funcionarioNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        }
        return funcionarioNf2DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
