package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabTipoPagoDTO;
import java.io.Serializable;

import javax.persistence.*;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_cab_tipo_pago database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_tipo_pago", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabTipoPago.findAll", query = "SELECT f FROM FacturaClienteCabTipoPago f")
public class FacturaClienteCabTipoPago implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_tipo_pago")
    private Long idFacturaClienteCabTipoPago;

    @Column(name = "monto_pago")
    private Integer montoPago;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to TipoPago
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_pago")
    private TipoPago tipoPago;

    public FacturaClienteCabTipoPago() {
    }

    public Long getIdFacturaClienteCabTipoPago() {
        return this.idFacturaClienteCabTipoPago;
    }

    public void setIdFacturaClienteCabTipoPago(Long idFacturaClienteCabTipoPago) {
        this.idFacturaClienteCabTipoPago = idFacturaClienteCabTipoPago;
    }

    public Integer getMontoPago() {
        return this.montoPago;
    }

    public void setMontoPago(Integer montoPago) {
        this.montoPago = montoPago;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoPago getTipoPago() {
        return this.tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    public FacturaClienteCabTipoPagoDTO toFacturaClienteCabTipoPagoDTO() {
        FacturaClienteCabTipoPagoDTO facDTO = toFacturaClienteCabTipoPagoDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        if (this.tipoPago != null) {
            facDTO.setTipoPago(this.getTipoPago().toBDTipoPagoDTO());
        }
        return facDTO;
    }

    public FacturaClienteCabTipoPagoDTO toBDFacturaClienteCabTipoPagoDTO() {
        FacturaClienteCabTipoPagoDTO facDTO = toFacturaClienteCabTipoPagoDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setTipoPago(null);
        return facDTO;
    }

    public static FacturaClienteCabTipoPagoDTO toFacturaClienteCabTipoPagoDTO(
            FacturaClienteCabTipoPago facturaClienteCabTipoPago) {
        FacturaClienteCabTipoPagoDTO facDTO = new FacturaClienteCabTipoPagoDTO();
        BeanUtils.copyProperties(facturaClienteCabTipoPago, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabTipoPago fromFacturaClienteCabTipoPago(
            FacturaClienteCabTipoPagoDTO facDTO) {
        FacturaClienteCabTipoPago fac = new FacturaClienteCabTipoPago();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabTipoPago fromFacturaClienteCabTipoPagoAsociadoDTO(
            FacturaClienteCabTipoPagoDTO facDTO) {
        FacturaClienteCabTipoPago fac = fromFacturaClienteCabTipoPago(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        fac.setTipoPago(TipoPago.fromTipoPago(facDTO.getTipoPago()));
        return fac;
    }
}
