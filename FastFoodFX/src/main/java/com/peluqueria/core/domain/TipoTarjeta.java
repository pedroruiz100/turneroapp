package com.peluqueria.core.domain;

import com.peluqueria.dto.TipoTarjetaDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_tarjeta database table.
 *
 */
@Entity
@Table(name = "tipo_tarjeta", schema = "cuenta")
@NamedQuery(name = "TipoTarjeta.findAll", query = "SELECT t FROM TipoTarjeta t")
public class TipoTarjeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_tarjeta")
    private Long idTipoTarjeta;

    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Tarjeta
    @OneToMany(mappedBy = "tipoTarjeta", fetch = FetchType.LAZY)
    private List<Tarjeta> tarjetas;

    public TipoTarjeta() {
    }

    public Long getIdTipoTarjeta() {
        return this.idTipoTarjeta;
    }

    public void setIdTipoTarjeta(Long idTipoTarjeta) {
        this.idTipoTarjeta = idTipoTarjeta;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<Tarjeta> getTarjetas() {
        return this.tarjetas;
    }

    public void setTarjetas(List<Tarjeta> tarjetas) {
        this.tarjetas = tarjetas;
    }

    public TipoTarjetaDTO toTipoTarjetaDTO() {
        TipoTarjetaDTO ttDTO = toTipoTarjetaDTO(this);
        ttDTO.setTarjetas(null);
        return ttDTO;
    }

    public static TipoTarjetaDTO toTipoTarjetaDTO(TipoTarjeta tp) {
        TipoTarjetaDTO tpDTO = new TipoTarjetaDTO();
        BeanUtils.copyProperties(tp, tpDTO);
        return tpDTO;
    }

    public static TipoTarjeta fromTipoTarjetaDTO(TipoTarjetaDTO tpDTO) {
        TipoTarjeta tp = new TipoTarjeta();
        BeanUtils.copyProperties(tpDTO, tp);
        return tp;
    }

}
