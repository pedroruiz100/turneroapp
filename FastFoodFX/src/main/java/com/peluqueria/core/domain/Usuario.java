package com.peluqueria.core.domain;

import com.peluqueria.dto.FuncionarioDTO;
import com.peluqueria.dto.RolDTO;
import com.peluqueria.dto.RolFuncionDTO;
import com.peluqueria.dto.SupervisorDTO;
import com.peluqueria.dto.UsuarioDTO;
import com.peluqueria.dto.UsuarioRolDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

@Entity
@Table(name = "usuario", schema = "seguridad")
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;

    public static UsuarioDTO toUsuarioDTOSeguridadRead(Usuario usuario) {
        UsuarioDTO usuarioDTO = toUsuarioDTO(usuario);
        usuarioDTO.setUsuarioRols(null);
        if (usuario.getFuncionario() != null) {
            usuarioDTO.setFuncionario(Funcionario.toFuncionarioSeguridadDTO(usuario.getFuncionario()));
        } else {
            usuarioDTO.setFuncionario(null);
        }
        usuarioDTO.setSupervisor(null);
        usuarioDTO = setNullSeguridadRead(usuarioDTO);
        return usuarioDTO;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_usuario")
    private Long idUsuario;

    @Column(name = "activo", nullable = false)
    private Boolean activo;

    @Column(name = "contrasenha", nullable = false, length = 64)
    private String contrasenha;

    @Column(name = "email", unique = true, nullable = false, length = 100)
    private String email;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "nom_usuario")
    private String nomUsuario;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    @Column(name = "generico")
    private boolean generico;

    // bi-directional many-to-one association to Funcionario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_funcionario")
    private Funcionario funcionario;

    // bi-directional many-to-one association to UsuarioRol
    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
    private List<UsuarioRol> usuarioRols;

    // bi-directional many-to-one association to AperturaCaja
    @OneToMany(mappedBy = "usuarioCajero", fetch = FetchType.LAZY)
    private List<AperturaCaja> aperturaCajasCajero;

    // bi-directional many-to-one association to AperturaCaja
    @OneToMany(mappedBy = "usuarioSupervisor", fetch = FetchType.LAZY)
    private List<AperturaCaja> aperturaCajasSupervisor;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioCajero", fetch = FetchType.LAZY)
    private List<CancelacionFactura> cancelacionFacturasCajero;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioSupervisor", fetch = FetchType.LAZY)
    private List<CancelacionFactura> cancelacionFacturasSupervisor;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioCajero", fetch = FetchType.LAZY)
    private List<CancelacionProducto> cancelacionProductosCajero;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioSupervisor", fetch = FetchType.LAZY)
    private List<CancelacionProducto> cancelacionProductosSupervisor;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioCajero", fetch = FetchType.LAZY)
    private List<CierreCaja> cierreCajasCajero;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioSupervisor", fetch = FetchType.LAZY)
    private List<CierreCaja> cierreCajasSupervisor;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioCajero", fetch = FetchType.LAZY)
    private List<RetiroDinero> retiroDinerosCajero;

    // bi-directional many-to-one association to CancelacionFactura
    @OneToMany(mappedBy = "usuarioSupervisor", fetch = FetchType.LAZY)
    private List<RetiroDinero> retiroDinerosSupervisor;

    // bi-directional many-to-one association to Supervisor
    @OneToMany(mappedBy = "usuario", fetch = FetchType.LAZY)
    private List<Supervisor> supervisor;

    public Usuario() {
    }

    public List<Supervisor> getSupervisor() {
        return supervisor;
    }

    public void setSupervisor(List<Supervisor> supervisor) {
        this.supervisor = supervisor;
    }

    public Long getIdUsuario() {
        return this.idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Boolean getActivo() {
        return this.activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public boolean isGenerico() {
        return generico;
    }

    public void setGenerico(boolean generico) {
        this.generico = generico;
    }

    public String getContrasenha() {
        return this.contrasenha;
    }

    public void setContrasenha(String contrasenha) {
        this.contrasenha = contrasenha;
    }

    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getNomUsuario() {
        return this.nomUsuario;
    }

    public void setNomUsuario(String nomUsuario) {
        this.nomUsuario = nomUsuario;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<UsuarioRol> getUsuarioRols() {
        return this.usuarioRols;
    }

    public void setUsuarioRols(List<UsuarioRol> usuarioRols) {
        this.usuarioRols = usuarioRols;
    }

    // ****************************************************************************************
    public List<AperturaCaja> getAperturaCajasCajero() {
        return aperturaCajasCajero;
    }

    public void setAperturaCajasCajero(List<AperturaCaja> aperturaCajasCajero) {
        this.aperturaCajasCajero = aperturaCajasCajero;
    }

    // ****************************************************************************************
    public List<AperturaCaja> getAperturaCajasSupervisor() {
        return aperturaCajasSupervisor;
    }

    public void setAperturaCajasSupervisor(
            List<AperturaCaja> aperturaCajasSupervisor) {
        this.aperturaCajasSupervisor = aperturaCajasSupervisor;
    }

    // ****************************************************************************************
    public List<CancelacionFactura> getCancelacionFacturasCajero() {
        return cancelacionFacturasCajero;
    }

    public void setCancelacionFacturasCajero(
            List<CancelacionFactura> cancelacionFacturasCajero) {
        this.cancelacionFacturasCajero = cancelacionFacturasCajero;
    }

    // ****************************************************************************************
    public List<CancelacionFactura> getCancelacionFacturasSupervisor() {
        return cancelacionFacturasSupervisor;
    }

    public void setCancelacionFacturasSupervisor(
            List<CancelacionFactura> cancelacionFacturasSupervisor) {
        this.cancelacionFacturasSupervisor = cancelacionFacturasSupervisor;
    }

    // ****************************************************************************************
    public List<CancelacionProducto> getCancelacionProductosCajero() {
        return cancelacionProductosCajero;
    }

    public void setCancelacionProductosCajero(
            List<CancelacionProducto> cancelacionProductosCajero) {
        this.cancelacionProductosCajero = cancelacionProductosCajero;
    }

    // ****************************************************************************************
    public List<CancelacionProducto> getCancelacionProductosSupervisor() {
        return cancelacionProductosSupervisor;
    }

    public void setCancelacionProductosSupervisor(
            List<CancelacionProducto> cancelacionProductosSupervisor) {
        this.cancelacionProductosSupervisor = cancelacionProductosSupervisor;
    }

    // ****************************************************************************************
    public List<CierreCaja> getCierreCajasCajero() {
        return cierreCajasCajero;
    }

    public void setCierreCajasCajero(List<CierreCaja> cierreCajasCajero) {
        this.cierreCajasCajero = cierreCajasCajero;
    }

    // ****************************************************************************************
    public List<CierreCaja> getCierreCajasSupervisor() {
        return cierreCajasSupervisor;
    }

    public void setCierreCajasSupervisor(List<CierreCaja> cierreCajasSupervisor) {
        this.cierreCajasSupervisor = cierreCajasSupervisor;
    }

    // ****************************************************************************************
    public List<RetiroDinero> getRetiroDinerosCajero() {
        return retiroDinerosCajero;
    }

    public void setRetiroDinerosCajero(List<RetiroDinero> retiroDinerosCajero) {
        this.retiroDinerosCajero = retiroDinerosCajero;
    }

    // ****************************************************************************************
    public List<RetiroDinero> getRetiroDinerosSupervisor() {
        return retiroDinerosSupervisor;
    }

    public void setRetiroDinerosSupervisor(
            List<RetiroDinero> retiroDinerosSupervisor) {
        this.retiroDinerosSupervisor = retiroDinerosSupervisor;
    }

    public UsuarioDTO toUsuarioDTO() {
        UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
        if (this.aperturaCajasCajero != null) {
            System.out.println("APERTURA CAJA CAJERO");
        }
        if (this.aperturaCajasSupervisor != null) {
            System.out.println("APERTURA CAJA SUPERVISOR");
        }
        if (this.cancelacionFacturasCajero != null) {
            System.out.println("APERTURA CAJA FACTURA CAJERO");
        }
        if (this.cancelacionFacturasSupervisor != null) {
            System.out.println("APERTURA CAJA FACTURA SUPERVISoR");
        }
        if (this.cancelacionProductosCajero != null) {
            System.out.println("CABNCELACION PROD CAJERO");
        }
        if (this.cancelacionProductosSupervisor != null) {
            System.out.println("CABNCELACION PROD  SUPERVISoR");
        }
        if (this.cierreCajasCajero != null) {
            System.out.println("CIERRE CAJA CAJERO");
        }
        if (this.cierreCajasSupervisor != null) {
            System.out.println("CIERRE CAJA SUPERVISOR");
        }

        // Verificar si exite datos recuperados de la BD para el campo
        // UsuarioRol
        if (this.usuarioRols != null) {
            // Crear un List de UsuarioRolDTO para que finalmente sea
            // visualizado en formato JSON
            List<UsuarioRolDTO> urDTO = new ArrayList<UsuarioRolDTO>();
            // Recorrer los datos recuperados de la tabla usuarioRol
            for (UsuarioRol usuRol : this.getUsuarioRols()) {
                RolDTO rolDTO = null;
                // Verificar si tiene datos, la clase Rol que esta dentro de la
                // Clase UsuarioRol
                if (usuRol.getRol() != null) {
                    // Recuperar los datos y almacenar en un objeto
                    Rol rol = usuRol.getRol();
                    // convertir rol a rolDTO
                    rolDTO = rol.toRolDTO();
                    // Crear List rolFuncionDTO para luego pasarsela a rolDTO ya
                    // que la clase rol tiene una lista de la clase RolFuncion
                    List<RolFuncionDTO> rfDTO = new ArrayList<RolFuncionDTO>();
                    // Recorremos la lista de RolFuncion que haya
                    for (RolFuncion rf : rol.getRolFuncions()) {
                        // verificamos si hay datos dentro de la clase
                        if (rf.getFuncion() != null) {
                            // recuperar las funciones y almacenar en un objeto
                            // Funcion funcion = rf.getFuncion();
                            // a la clase funcion le agregamos un rolFuncion
                            // null porque es reiterativo
                            rfDTO.add(rf.toRolFuncionDTO());
                        }
                    }
                    rolDTO.setRolFuncions(rfDTO);
                }
                urDTO.add(usuRol.toUsuarioRolDTO(null, rolDTO));
            }
            usuarioDTO.setSupervisor(null);
            usuarioDTO.setUsuarioRols(urDTO);
            usuarioDTO.setAperturaCajasCajero(null);
            usuarioDTO.setAperturaCajasSupervisor(null);
            usuarioDTO.setCancelacionFacturasCajero(null);
            usuarioDTO.setCancelacionFacturasSupervisor(null);
            usuarioDTO.setCancelacionProductosCajero(null);
            usuarioDTO.setCancelacionProductosSupervisor(null);
            usuarioDTO.setCierreCajasCajero(null);
            usuarioDTO.setCierreCajasSupervisor(null);
            usuarioDTO.setRetiroDinerosCajero(null);
            usuarioDTO.setRetiroDinerosSupervisor(null);
        }

        if (this.retiroDinerosCajero != null) {
            System.out.println("Retiro Dineros Cajero");
        }
        if (this.retiroDinerosSupervisor != null) {
            System.out.println("Retiro Dineros SUPERVISOR");
        }

        if (this.getFuncionario() != null) {
            usuarioDTO.setFuncionario(this.getFuncionario().toFuncionarioDTO());
        }
        return usuarioDTO;
    }

    public UsuarioDTO toUsuarioFuncionarioDTO() {
        UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
        if (this.getFuncionario() != null) {
            usuarioDTO.setFuncionario(this.getFuncionario().toFuncionarioDTO());
        }
        usuarioDTO.setSupervisor(null);
        usuarioDTO.setUsuarioRols(null);
        usuarioDTO.setAperturaCajasCajero(null);
        usuarioDTO.setAperturaCajasSupervisor(null);
        usuarioDTO.setCancelacionFacturasCajero(null);
        usuarioDTO.setCancelacionFacturasSupervisor(null);
        usuarioDTO.setCancelacionProductosCajero(null);
        usuarioDTO.setCancelacionProductosSupervisor(null);
        usuarioDTO.setCierreCajasCajero(null);
        usuarioDTO.setCierreCajasSupervisor(null);
        usuarioDTO.setRetiroDinerosCajero(null);
        usuarioDTO.setRetiroDinerosSupervisor(null);
        return usuarioDTO;
    }

    public static UsuarioDTO toUsuarioDTO(Usuario usuario) {
        UsuarioDTO usuarioDTO = new UsuarioDTO();
        BeanUtils.copyProperties(usuario, usuarioDTO);
        return usuarioDTO;
    }

    public static Usuario fromUsuarioCajeroDTO(UsuarioDTO usuarioDTO) {
        Usuario usuarioCajero = new Usuario();
        BeanUtils.copyProperties(usuarioDTO, usuarioCajero);
        return usuarioCajero;
    }

    /* SUPERVISOR */
    public UsuarioDTO toUsuarioSupervisorDTO() {
        UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
        usuarioDTO.setUsuarioRols(null);
        usuarioDTO.setSupervisor(null);
        return usuarioDTO;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
    // tenga)
    public static Usuario fromUsuarioSupervisorDTO(UsuarioDTO usuarioDTO) {
        Usuario usuarioSup = new Usuario();
        BeanUtils.copyProperties(usuarioDTO, usuarioSup);
        return usuarioSup;
    }

    /* FIN SUPERVISOR */
    // CON FUNCIONARIO
    public static Usuario fromUsuarioSupervisorAsociadoDTO(UsuarioDTO usuDTO) {
        Usuario usu = fromUsuarioSupervisorDTO(usuDTO);
        usu.setFuncionario(Funcionario.fromFuncionarioDTO(usuDTO
                .getFuncionario()));
        return usu;
    }

    public static UsuarioDTO toBDUsuarioSeguridadDTO(Usuario usuario) {
        UsuarioDTO usuarioDTO = toUsuarioDTO(usuario);
        usuarioDTO = setNullSeguridadRead(usuarioDTO);
        usuarioDTO.setSupervisor(null);
        usuarioDTO.setUsuarioRols(null);
        return usuarioDTO;
    }

    private static UsuarioDTO setNullSeguridadRead(UsuarioDTO usuarioDTO) {
        usuarioDTO.setAperturaCajasCajero(null);
        usuarioDTO.setAperturaCajasSupervisor(null);
        usuarioDTO.setCancelacionFacturasCajero(null);
        usuarioDTO.setCancelacionFacturasSupervisor(null);
        usuarioDTO.setCancelacionProductosCajero(null);
        usuarioDTO.setCancelacionProductosSupervisor(null);
        usuarioDTO.setCierreCajasCajero(null);
        usuarioDTO.setCierreCajasSupervisor(null);
        usuarioDTO.setRetiroDinerosCajero(null);
        usuarioDTO.setRetiroDinerosSupervisor(null);
        return usuarioDTO;
    }

    public UsuarioDTO toBDUsuarioDTO() {
        UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
        usuarioDTO.setSupervisor(null);
        usuarioDTO.setUsuarioRols(null);
        usuarioDTO.setAperturaCajasCajero(null);
        usuarioDTO.setAperturaCajasSupervisor(null);
        usuarioDTO.setCancelacionFacturasCajero(null);
        usuarioDTO.setCancelacionFacturasSupervisor(null);
        usuarioDTO.setCancelacionProductosCajero(null);
        usuarioDTO.setCancelacionProductosSupervisor(null);
        usuarioDTO.setCierreCajasCajero(null);
        usuarioDTO.setCierreCajasSupervisor(null);
        usuarioDTO.setRetiroDinerosCajero(null);
        usuarioDTO.setRetiroDinerosSupervisor(null);
        return usuarioDTO;
    }

    public static Usuario fromUsuarioFuncDTO(UsuarioDTO usuDTO) {
        Usuario usu = fromUsuarioDTO(usuDTO);
        usu.setFuncionario(Funcionario.fromFuncionarioSeguridadDTO(usuDTO.getFuncionario()));
        usu.setSupervisor(null);
        usu.setUsuarioRols(null);
        return usu;
    }

    public static Usuario fromUsuarioDTO(UsuarioDTO usuarioDTO) {
        Usuario usuarioSup = new Usuario();
        BeanUtils.copyProperties(usuarioDTO, usuarioSup);
        return usuarioSup;
    }

    public UsuarioDTO toUsuarioDTOSeguridadRead() {
        UsuarioDTO usuarioDTO = toUsuarioDTO(this);
        // usuario - rol
        if (this.usuarioRols == null) {
            this.usuarioRols = new ArrayList<>();
        }
        if (!this.usuarioRols.isEmpty()) {
            List<UsuarioRolDTO> listUsuarioRolDTO = new ArrayList<>();
            int indexUR = 0;
            for (UsuarioRol usuarioRol : this.usuarioRols) {
                UsuarioRolDTO usuarioRolDTO = usuarioRol.toUsuarioRolDTO();
                usuarioRolDTO.setUsuario(null);
                RolDTO rolDTO = Rol.toRolDTO(this.usuarioRols.get(indexUR).getRol());
                indexUR++;
                rolDTO.setRolFuncions(null);
                rolDTO.setUsuarioRols(null);
                usuarioRolDTO.setRol(rolDTO);
                listUsuarioRolDTO.add(usuarioRolDTO);
            }
            usuarioDTO.setUsuarioRols(listUsuarioRolDTO);
        } else {
            usuarioDTO.setUsuarioRols(null);
        }
        // usuario - rol
        // funcionario
        if (this.funcionario != null) {
            FuncionarioDTO funcionarioDTO = Funcionario.toFuncionarioSeguridadDTO(this.funcionario);
            // funcionarioDTO.setUsuario(null);
            usuarioDTO.setFuncionario(funcionarioDTO);
        } else {
            usuarioDTO.setFuncionario(null);
        }
        // funcionario
        // supervisor...
        // y me pregunto ¿no puede supervisor ser un rol más? NO, ya que este
        // tiene aparte funciones cómo -> nro. supervisor y código
        if (this.supervisor == null) {
            this.supervisor = new ArrayList<>();
        }
        if (!this.supervisor.isEmpty()) {
            List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
            for (Supervisor supervisor : this.supervisor) {
                SupervisorDTO supervisorDTO = Supervisor.toSupervisorDTO(supervisor);
                supervisorDTO.setUsuario(null);
                supervisorDTO.setArqueoCaja(null);
                listSupervisorDTO.add(supervisorDTO);
            }
            usuarioDTO.setSupervisor(listSupervisorDTO);
        } else {
            usuarioDTO.setSupervisor(null);
        }
        // supervisor
        // otros campos
        usuarioDTO = setNullSeguridadRead(usuarioDTO);
        // otros campos
        return usuarioDTO;
    }

}
