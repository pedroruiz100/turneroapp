package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf4Seccion1DTO;

/**
 * The persistent class for the articulo_nf4_seccion1 database table.
 *
 */
@Entity
@Table(name = "articulo_nf4_seccion1", schema = "stock")
@NamedQuery(name = "ArticuloNf4Seccion1.findAll", query = "SELECT a FROM ArticuloNf4Seccion1 a")
public class ArticuloNf4Seccion1 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf4_seccion1_articulo")
    private Long idNf4Seccion1Articulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Nf4Seccion1
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf4_seccion1")
    private Nf4Seccion1 nf4Seccion1;

    public ArticuloNf4Seccion1() {
    }

    public Long getIdNf4Seccion1Articulo() {
        return this.idNf4Seccion1Articulo;
    }

    public void setIdNf4Seccion1Articulo(Long idNf4Seccion1Articulo) {
        this.idNf4Seccion1Articulo = idNf4Seccion1Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf4Seccion1 getNf4Seccion1() {
        return this.nf4Seccion1;
    }

    public void setNf4Seccion1(Nf4Seccion1 nf4Seccion1) {
        this.nf4Seccion1 = nf4Seccion1;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf4Seccion1 fromArticuloNf4Seccion1DTO(ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO) {
        ArticuloNf4Seccion1 articuloNf4Seccion1 = new ArticuloNf4Seccion1();
        BeanUtils.copyProperties(articuloNf4Seccion1DTO, articuloNf4Seccion1);
        return articuloNf4Seccion1;
    }

    // <-- SERVER
    public static ArticuloNf4Seccion1DTO toArticuloNf4Seccion1DTO(ArticuloNf4Seccion1 articuloNf4Seccion1) {
        ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO = new ArticuloNf4Seccion1DTO();
        BeanUtils.copyProperties(articuloNf4Seccion1, articuloNf4Seccion1DTO);
        return articuloNf4Seccion1DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf4Seccion1DTO toArticuloNf4Seccion1DTOEntitiesNull(ArticuloNf4Seccion1 articuloNf4Seccion1) {
        ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO = toArticuloNf4Seccion1DTO(articuloNf4Seccion1);
        articuloNf4Seccion1DTO.setArticulo(null);
        articuloNf4Seccion1DTO.setNf4Seccion1(null);
        return articuloNf4Seccion1DTO;
    }

    // <-- SERVER
    public ArticuloNf4Seccion1DTO toArticuloNf4Seccion1DTOEntitiesNull() {
        ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO = toArticuloNf4Seccion1DTO(this);
        articuloNf4Seccion1DTO.setArticulo(null);
        articuloNf4Seccion1DTO.setNf4Seccion1(null);
        return articuloNf4Seccion1DTO;
    }

    // <-- SERVER
    public ArticuloNf4Seccion1DTO toArticuloNf4Seccion1DTOEntitiesNf() {
        ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO = toArticuloNf4Seccion1DTO(this);
        articuloNf4Seccion1DTO.setArticulo(null);
        if (this.getNf4Seccion1() != null) {
            articuloNf4Seccion1DTO.setNf4Seccion1(this.getNf4Seccion1().toNf4Seccion1DTOEntitiesNullNf3());
        } else {
            articuloNf4Seccion1DTO.setNf4Seccion1(null);
        }
        return articuloNf4Seccion1DTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf4Seccion1 fromArticuloNf4Seccion1DTOEntitiesFull(
            ArticuloNf4Seccion1DTO articuloNf4Seccion1DTO) {
        ArticuloNf4Seccion1 articuloNf4Seccion1 = fromArticuloNf4Seccion1DTO(articuloNf4Seccion1DTO);
        if (articuloNf4Seccion1DTO.getArticulo() != null) {
            articuloNf4Seccion1.setArticulo(Articulo.fromArticuloDTO(articuloNf4Seccion1DTO.getArticulo()));
        }
        if (articuloNf4Seccion1DTO.getNf4Seccion1() != null) {
            articuloNf4Seccion1.setNf4Seccion1(Nf4Seccion1.fromNf4Seccion1DTO(articuloNf4Seccion1DTO.getNf4Seccion1()));
        }
        return articuloNf4Seccion1;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
