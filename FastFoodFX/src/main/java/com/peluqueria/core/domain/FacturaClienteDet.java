package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteDetDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the factura_cliente_det database table.
 *
 */
@Entity
@Table(name = "factura_cliente_det", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteDet.findAll", query = "SELECT f FROM FacturaClienteDet f")
public class FacturaClienteDet implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_det")
    private Long idFacturaClienteDet;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "cantidad")
    private BigDecimal cantidad;

    @Column(name = "precio")
    private Long precio;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "poriva")
    private Integer poriva;

    @Column(name = "cod_vendedor")
    private String codVendedor;

    @Column(name = "cod_articulo")
    private Long codArticulo;

    @Column(name = "orden")
    private Integer orden;

    @Column(name = "permite_desc")
    private Boolean permiteDesc;

    @Column(name = "seccion")
    private String seccion;

    @Column(name = "seccion_sub")
    private String seccionSub;

    @Column(name = "bajada")
    private Boolean bajada;

    // bi-directional many-to-one association to DescCombo
    @OneToMany(mappedBy = "facturaClienteDet")
    private List<DescCombo> descCombos;

    public FacturaClienteDet() {
    }

    public Long getIdFacturaClienteDet() {
        return this.idFacturaClienteDet;
    }

    public void setIdFacturaClienteDet(Long idFacturaClienteDet) {
        this.idFacturaClienteDet = idFacturaClienteDet;
    }

    public Long getCodArticulo() {
        return codArticulo;
    }

    public void setCodArticulo(Long codArticulo) {
        this.codArticulo = codArticulo;
    }

    public BigDecimal getCantidad() {
        return this.cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public String getCodVendedor() {
        return codVendedor;
    }

    public void setCodVendedor(String codVendedor) {
        this.codVendedor = codVendedor;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getOrden() {
        return this.orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    public Boolean getPermiteDesc() {
        return this.permiteDesc;
    }

    public void setPermiteDesc(Boolean permiteDesc) {
        this.permiteDesc = permiteDesc;
    }

    public Boolean getBajada() {
        return bajada;
    }

    public void setBajada(Boolean bajada) {
        this.bajada = bajada;
    }

    public Integer getPoriva() {
        return this.poriva;
    }

    public void setPoriva(Integer poriva) {
        this.poriva = poriva;
    }

    public Long getPrecio() {
        return this.precio;
    }

    public void setPrecio(Long precio) {
        this.precio = precio;
    }

    public String getSeccion() {
        return this.seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getSeccionSub() {
        return this.seccionSub;
    }

    public void setSeccionSub(String seccionSub) {
        this.seccionSub = seccionSub;
    }

    public List<DescCombo> getDescCombos() {
        return this.descCombos;
    }

    public void setDescCombos(List<DescCombo> descCombos) {
        this.descCombos = descCombos;
    }

    public DescCombo addDescCombo(DescCombo descCombo) {
        getDescCombos().add(descCombo);
        descCombo.setFacturaClienteDet(this);
        return descCombo;
    }

    public DescCombo removeDescCombo(DescCombo descCombo) {
        getDescCombos().remove(descCombo);
        descCombo.setFacturaClienteDet(null);
        return descCombo;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public FacturaClienteDetDTO toFacturaClienteDetDTO() {
        FacturaClienteDetDTO facDTO = toFacturaClienteDetDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        if (this.articulo != null) {
            facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }
        facDTO.setDescCombos(null);
        return facDTO;
    }

    public FacturaClienteDetDTO toDescriFacturaClienteDetDTO() {
        FacturaClienteDetDTO facDTO = toFacturaClienteDetDTO(this);
        facDTO.setFacturaClienteCab(null);
        facDTO.setArticulo(null);
        facDTO.setDescCombos(null);
        return facDTO;
    }

    public static FacturaClienteDetDTO toFacturaClienteDetDTO(
            FacturaClienteDet facturaClienteDet) {
        FacturaClienteDetDTO facDTO = new FacturaClienteDetDTO();
        BeanUtils.copyProperties(facturaClienteDet, facDTO);
        return facDTO;
    }

    public static FacturaClienteDet fromFacturaClienteDetDTO(
            FacturaClienteDetDTO facDTO) {
        FacturaClienteDet fac = new FacturaClienteDet();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteDet fromFacturaClienteDetAsociado(
            FacturaClienteDetDTO facDTO) {
        FacturaClienteDet fac = fromFacturaClienteDetDTO(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
        return fac;
    }

}
