package com.peluqueria.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Time;
import java.util.List;

/**
 * The persistent class for the nota_credito_cab database table.
 *
 */
@Entity
@Table(name = "nota_credito_cab", schema = "factura_cliente")
@NamedQuery(name = "NotaCreditoCab.findAll", query = "SELECT n FROM NotaCreditoCab n")
public class NotaCreditoCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nota_credito_cab")
    private Long idNotaCreditoCab;

    @Column(name = "fecha_anulacion")
    private Time fechaAnulacion;

    @Column(name = "fecha_emision")
    private Time fechaEmision;

    @Column(name = "id_cajero_anulacion")
    private Integer idCajeroAnulacion;

    // bi-directional many-to-one association to Cliente
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_cliente")
    private Cliente cliente;

    // bi-directional many-to-one association to Sucursal
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_sucursal")
    private Sucursal sucursal;

    @Column(name = "monto_pago")
    private BigDecimal montoPago;

    @Column(name = "motivo_anulacion")
    private String motivoAnulacion;

    @Column(name = "nro_nota_credito")
    private String nroNotaCredito;

    private String observaciones;

    private String razon;

    @Column(name = "total_credito")
    private BigDecimal totalCredito;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to TipoComprobante
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_comprobante")
    private TipoComprobante tipoComprobante;

    // bi-directional many-to-one association to TipoMoneda
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_moneda")
    private TipoMoneda tipoMoneda;

    // bi-directional many-to-one association to NotaCreditoDet
    @OneToMany(mappedBy = "notaCreditoCab")
    private List<NotaCreditoDet> notaCreditoDets;

    public NotaCreditoCab() {
    }

    public Long getIdNotaCreditoCab() {
        return this.idNotaCreditoCab;
    }

    public void setIdNotaCreditoCab(Long idNotaCreditoCab) {
        this.idNotaCreditoCab = idNotaCreditoCab;
    }

    public Time getFechaAnulacion() {
        return this.fechaAnulacion;
    }

    public void setFechaAnulacion(Time fechaAnulacion) {
        this.fechaAnulacion = fechaAnulacion;
    }

    public Time getFechaEmision() {
        return this.fechaEmision;
    }

    public void setFechaEmision(Time fechaEmision) {
        this.fechaEmision = fechaEmision;
    }

    public Integer getIdCajeroAnulacion() {
        return this.idCajeroAnulacion;
    }

    public void setIdCajeroAnulacion(Integer idCajeroAnulacion) {
        this.idCajeroAnulacion = idCajeroAnulacion;
    }

    public BigDecimal getMontoPago() {
        return this.montoPago;
    }

    public void setMontoPago(BigDecimal montoPago) {
        this.montoPago = montoPago;
    }

    public String getMotivoAnulacion() {
        return this.motivoAnulacion;
    }

    public void setMotivoAnulacion(String motivoAnulacion) {
        this.motivoAnulacion = motivoAnulacion;
    }

    public String getNroNotaCredito() {
        return this.nroNotaCredito;
    }

    public void setNroNotaCredito(String nroNotaCredito) {
        this.nroNotaCredito = nroNotaCredito;
    }

    public String getObservaciones() {
        return this.observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getRazon() {
        return this.razon;
    }

    public void setRazon(String razon) {
        this.razon = razon;
    }

    public BigDecimal getTotalCredito() {
        return this.totalCredito;
    }

    public void setTotalCredito(BigDecimal totalCredito) {
        this.totalCredito = totalCredito;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return this.facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoComprobante getTipoComprobante() {
        return this.tipoComprobante;
    }

    public void setTipoComprobante(TipoComprobante tipoComprobante) {
        this.tipoComprobante = tipoComprobante;
    }

    public TipoMoneda getTipoMoneda() {
        return this.tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public List<NotaCreditoDet> getNotaCreditoDets() {
        return this.notaCreditoDets;
    }

    public void setNotaCreditoDets(List<NotaCreditoDet> notaCreditoDets) {
        this.notaCreditoDets = notaCreditoDets;
    }

    public NotaCreditoDet addNotaCreditoDet(NotaCreditoDet notaCreditoDet) {
        getNotaCreditoDets().add(notaCreditoDet);
        notaCreditoDet.setNotaCreditoCab(this);
        return notaCreditoDet;
    }

    public NotaCreditoDet removeNotaCreditoDet(NotaCreditoDet notaCreditoDet) {
        getNotaCreditoDets().remove(notaCreditoDet);
        notaCreditoDet.setNotaCreditoCab(null);
        return notaCreditoDet;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Sucursal getSucursal() {
        return sucursal;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

}
