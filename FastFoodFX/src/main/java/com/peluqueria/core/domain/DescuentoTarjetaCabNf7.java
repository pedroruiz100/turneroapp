package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf7DTO;

/**
 * The persistent class for the descuento_tarjeta_cab_nf7 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf7", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf7.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf7 d")
public class DescuentoTarjetaCabNf7 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf7")
    private Long idDescuentoTarjetaCabNf7;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf7Secnom7
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf7_secnom7")
    private Nf7Secnom7 nf7Secnom7;

    public DescuentoTarjetaCabNf7() {
    }

    public Long getIdDescuentoTarjetaCabNf7() {
        return this.idDescuentoTarjetaCabNf7;
    }

    public void setIdDescuentoTarjetaCabNf7(Long idDescuentoTarjetaCabNf7) {
        this.idDescuentoTarjetaCabNf7 = idDescuentoTarjetaCabNf7;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf7Secnom7 getNf7Secnom7() {
        return nf7Secnom7;
    }

    public void setNf7Secnom7(Nf7Secnom7 nf7Secnom7) {
        this.nf7Secnom7 = nf7Secnom7;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf7 fromDescuentoTarjetaCabNf7DTO(DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO) {
        DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7 = new DescuentoTarjetaCabNf7();
        BeanUtils.copyProperties(descuentoTarjetaCabNf7DTO, descuentoTarjetaCabNf7);
        return descuentoTarjetaCabNf7;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf7DTO toDescuentoTarjetaCabNf7DTO(DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7) {
        DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO = new DescuentoTarjetaCabNf7DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf7, descuentoTarjetaCabNf7DTO);
        return descuentoTarjetaCabNf7DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf7 fromDescuentoTarjetaCabNf7DTOEntitiesFull(DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO) {
        DescuentoTarjetaCabNf7 descuentoTarjetaCabNf7 = fromDescuentoTarjetaCabNf7DTO(descuentoTarjetaCabNf7DTO);
        if (descuentoTarjetaCabNf7DTO.getNf7Secnom7() != null) {
            descuentoTarjetaCabNf7.setNf7Secnom7(Nf7Secnom7.fromNf7Secnom7DTO(descuentoTarjetaCabNf7DTO.getNf7Secnom7()));
        }
        if (descuentoTarjetaCabNf7DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf7
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf7DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf7;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf7DTO toDescuentoTarjetaCabNf7DTOEntityFull() {
        DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO = toDescuentoTarjetaCabNf7DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf7DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf7DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf7Secnom7() != null) {
            descuentoTarjetaCabNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            descuentoTarjetaCabNf7DTO.setNf7Secnom7(null);
        }
        return descuentoTarjetaCabNf7DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf7DTO toDescuentoTarjetaCabNf7DTOEntityNull() {
        DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO = toDescuentoTarjetaCabNf7DTO(this);
        descuentoTarjetaCabNf7DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf7DTO.setNf7Secnom7(null);
        return descuentoTarjetaCabNf7DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf7DTO toDescuentoTarjetaCabNf7DTONf6() {
        DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO = toDescuentoTarjetaCabNf7DTO(this);
        descuentoTarjetaCabNf7DTO.setDescuentoTarjetaCab(null);
        if (this.getNf7Secnom7() != null) {
            descuentoTarjetaCabNf7DTO.setNf7Secnom7(this.getNf7Secnom7().toNf7Secnom7DTOEntitiesNullNf6());
        } else {
            descuentoTarjetaCabNf7DTO.setNf7Secnom7(null);
        }
        return descuentoTarjetaCabNf7DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
