package com.peluqueria.core.domain;

import com.peluqueria.dto.RangoEfectivoDTO;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the dias database table.
 *
 */
@Entity
@Table(name = "rango_efectivo", schema = "general")
@NamedQuery(name = "RangoEfectivo.findAll", query = "SELECT d FROM RangoEfectivo d")
public class RangoEfectivo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_rango")
    private Long idRango;

    @Column(name = "rango_inicial")
    private Long rangoInicial;

    @Column(name = "rango_final")
    private Long rangoFinal;

    @Column(name = "rango_actual")
    private Long rangoActual;

    public RangoEfectivo() {
    }

    public Long getIdRango() {
        return idRango;
    }

    public void setIdRango(Long idRango) {
        this.idRango = idRango;
    }

    public Long getRangoInicial() {
        return rangoInicial;
    }

    public void setRangoInicial(Long rangoInicial) {
        this.rangoInicial = rangoInicial;
    }

    public Long getRangoFinal() {
        return rangoFinal;
    }

    public void setRangoFinal(Long rangoFinal) {
        this.rangoFinal = rangoFinal;
    }

    public Long getRangoActual() {
        return rangoActual;
    }

    public void setRangoActual(Long rangoActual) {
        this.rangoActual = rangoActual;
    }

    public RangoEfectivoDTO toRangoEfectivoDTO() {
        RangoEfectivoDTO rangoDTO = toRangoEfectivoDTO(this);
        return rangoDTO;
    }

    public static RangoEfectivoDTO toRangoEfectivoDTO(RangoEfectivo rango) {
        RangoEfectivoDTO dDTO = new RangoEfectivoDTO();
        BeanUtils.copyProperties(rango, dDTO);
        return dDTO;
    }

    public static RangoEfectivo fromRangoEfectivoDTO(RangoEfectivoDTO rangoDTO) {
        RangoEfectivo rango = new RangoEfectivo();
        BeanUtils.copyProperties(rangoDTO, rango);
        return rango;
    }

}
