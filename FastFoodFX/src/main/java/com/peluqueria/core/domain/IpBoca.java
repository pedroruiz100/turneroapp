package com.peluqueria.core.domain;

import com.peluqueria.dto.IpBocaDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the ip_boca database table.
 *
 */
@Entity
@Table(name = "ip_boca", schema = "general")
@NamedQuery(name = "IpBoca.findAll", query = "SELECT i FROM IpBoca i")
public class IpBoca implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_ip_boca")
    private Long idIpBoca;

    @Column(name = "ip_boca")
    private String ipBoca;

    // bi-directional many-to-one association to TalonariosSucursale
    @OneToMany(mappedBy = "ipBoca")
    private List<TalonariosSucursales> talonariosSucursales;

    // bi-directional many-to-one association to Caja
    @OneToMany(mappedBy = "ipBoca")
    private List<Caja> caja;

    public IpBoca() {
    }

    public Long getIdIpBoca() {
        return this.idIpBoca;
    }

    public void setIdIpBoca(Long idIpBoca) {
        this.idIpBoca = idIpBoca;
    }

    public String getIpBoca() {
        return this.ipBoca;
    }

    public void setIpBoca(String ipBoca) {
        this.ipBoca = ipBoca;
    }

    public List<TalonariosSucursales> getTalonariosSucursales() {
        return this.talonariosSucursales;
    }

    public void setTalonariosSucursales(
            List<TalonariosSucursales> talonariosSucursales) {
        this.talonariosSucursales = talonariosSucursales;
    }

    public List<Caja> getCaja() {
        return caja;
    }

    public void setCaja(List<Caja> caja) {
        this.caja = caja;
    }

    //SIN OTROS DATOS, IGUAL A LA BD
    public IpBocaDTO toBDIpBocaDTO() {
        IpBocaDTO ipDTO = toIpBocaDTO(this);
        ipDTO.setCaja(null);
        ipDTO.setTalonariosSucursales(null);
        return ipDTO;
    }

    public static IpBocaDTO toIpBocaDTO(IpBoca ip) {
        IpBocaDTO ipBocaDTO = new IpBocaDTO();
        BeanUtils.copyProperties(ip, ipBocaDTO);
        return ipBocaDTO;
    }

    public static IpBoca fromIpBocaDTO(IpBocaDTO ipBocaDTO) {
        IpBoca ipBoca = new IpBoca();
        BeanUtils.copyProperties(ipBocaDTO, ipBoca);
        return ipBoca;
    }

}
