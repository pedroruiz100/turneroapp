package com.peluqueria.core.domain;

import com.peluqueria.dto.SeccionDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the seccion database table.
 *
 */
@Entity
@Table(name = "seccion", schema = "stock")
@NamedQuery(name = "Seccion.findAll", query = "SELECT s FROM Seccion s")
public class Seccion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_seccion")
    private Long idSeccion;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    private Boolean estado;

    // bi-directional many-to-one association to SeccionSub
    @OneToMany(mappedBy = "seccion", fetch = FetchType.LAZY)
    private List<SeccionSub> seccionSubs;

    // bi-directional many-to-one association to SeccionPromoTemporada
    @OneToMany(mappedBy = "seccion", fetch = FetchType.LAZY)
    private List<SeccionPromoTemporada> seccionPromoTemporada;

    // bi-directional many-to-one association to SeccionFunc
    @OneToMany(mappedBy = "seccion", fetch = FetchType.LAZY)
    private List<SeccionFunc> seccionFuncs;

    // bi-directional many-to-one association to DescuentoFielCab
    @OneToMany(mappedBy = "seccion", fetch = FetchType.LAZY)
    private List<DescuentoFielCab> descuentoFielCab;

    // bi-directional many-to-one association to DescuentoFielCab
    @OneToMany(mappedBy = "seccion", fetch = FetchType.LAZY)
    private List<Articulo> articulo;

    public Seccion() {
    }

    public Long getIdSeccion() {
        return this.idSeccion;
    }

    public void setIdSeccion(Long idSeccion) {
        this.idSeccion = idSeccion;
    }

    public List<DescuentoFielCab> getDescuentoFielCab() {
        return descuentoFielCab;
    }

    public void setDescuentoFielCab(List<DescuentoFielCab> descuentoFielCab) {
        this.descuentoFielCab = descuentoFielCab;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public List<Articulo> getArticulo() {
        return articulo;
    }

    public void setArticulo(List<Articulo> articulo) {
        this.articulo = articulo;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<SeccionSub> getSeccionSubs() {
        return this.seccionSubs;
    }

    public void setSeccionSubs(List<SeccionSub> seccionSubs) {
        this.seccionSubs = seccionSubs;
    }

    // ***************************************************************************
    public List<SeccionPromoTemporada> getSeccionPromoTemporada() {
        return seccionPromoTemporada;
    }

    public void setSeccionPromoTemporada(
            List<SeccionPromoTemporada> seccionPromoTemporada) {
        this.seccionPromoTemporada = seccionPromoTemporada;
    }

    // ***************************************************************************
    public List<SeccionFunc> getSeccionFuncs() {
        return seccionFuncs;
    }

    public void setSeccionFuncs(List<SeccionFunc> seccionFuncs) {
        this.seccionFuncs = seccionFuncs;
    }

    public SeccionDTO toSeccionDTO() {
        SeccionDTO secDTO = toSeccionDTO(this);
        secDTO.setSeccionSubs(null);
        secDTO.setSeccionFuncDTO(null);
        secDTO.setFechaAlta(null);
        secDTO.setFechaMod(null);
        secDTO.setUsuAlta(null);
        secDTO.setUsuMod(null);
        secDTO.setSeccionPromoTemporadaDTO(null);
        secDTO.setArticuloDTO(null);
        return secDTO;
    }

    public static SeccionDTO toSeccionDTO(Seccion sec) {
        SeccionDTO secDTO = new SeccionDTO();
        BeanUtils.copyProperties(sec, secDTO);
        return secDTO;
    }

    public static Seccion fromSeccionDTO(SeccionDTO secDTO) {
        Seccion sec = new Seccion();
        BeanUtils.copyProperties(secDTO, sec);
        return sec;
    }
}
