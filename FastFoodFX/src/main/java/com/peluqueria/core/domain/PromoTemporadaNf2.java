package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf2DTO;

/**
 * The persistent class for the promo_temporada_nf2 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf2", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf2.findAll", query = "SELECT p FROM PromoTemporadaNf2 p")
public class PromoTemporadaNf2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf2")
    private Long idPromoTemporadaNf2;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf2Sfamilia
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf2_sfamilia")
    private Nf2Sfamilia nf2Sfamilia;

    public PromoTemporadaNf2() {
    }

    public Long getIdPromoTemporadaNf2() {
        return this.idPromoTemporadaNf2;
    }

    public void setIdPromoTemporadaNf2(Long idPromoTemporadaNf2) {
        this.idPromoTemporadaNf2 = idPromoTemporadaNf2;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf2Sfamilia getNf2Sfamilia() {
        return nf2Sfamilia;
    }

    public void setNf2Sfamilia(Nf2Sfamilia nf2Sfamilia) {
        this.nf2Sfamilia = nf2Sfamilia;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf2 fromPromoTemporadaNf2DTO(PromoTemporadaNf2DTO promoTemporadaNf2DTO) {
        PromoTemporadaNf2 promoTemporadaNf2 = new PromoTemporadaNf2();
        BeanUtils.copyProperties(promoTemporadaNf2DTO, promoTemporadaNf2);
        return promoTemporadaNf2;
    }

    // <-- SERVER
    public static PromoTemporadaNf2DTO toPromoTemporadaNf2DTO(PromoTemporadaNf2 promoTemporadaNf2) {
        PromoTemporadaNf2DTO promoTemporadaNf2DTO = new PromoTemporadaNf2DTO();
        BeanUtils.copyProperties(promoTemporadaNf2, promoTemporadaNf2DTO);
        return promoTemporadaNf2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf2 fromPromoTemporadaNf2DTOEntitiesFull(PromoTemporadaNf2DTO promoTemporadaNf2DTO) {
        PromoTemporadaNf2 promoTemporadaNf2 = fromPromoTemporadaNf2DTO(promoTemporadaNf2DTO);
        if (promoTemporadaNf2DTO.getNf2Sfamilia() != null) {
            promoTemporadaNf2.setNf2Sfamilia(Nf2Sfamilia.fromNf2SfamiliaDTO(promoTemporadaNf2DTO.getNf2Sfamilia()));
        }
        if (promoTemporadaNf2DTO.getPromoTemporada() != null) {
            promoTemporadaNf2
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf2DTO.getPromoTemporada()));
        }
        return promoTemporadaNf2;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf2DTO toPromoTemporadaNf2DTOEntityFull() {
        PromoTemporadaNf2DTO promoTemporadaNf2DTO = toPromoTemporadaNf2DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf2DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf2DTO.setPromoTemporada(null);
        }
        if (this.getNf2Sfamilia() != null) {
            promoTemporadaNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            promoTemporadaNf2DTO.setNf2Sfamilia(null);
        }
        return promoTemporadaNf2DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf2DTO toPromoTemporadaNf2DTOEntityNull() {
        PromoTemporadaNf2DTO promoTemporadaNf2DTO = toPromoTemporadaNf2DTO(this);
        promoTemporadaNf2DTO.setPromoTemporada(null);
        promoTemporadaNf2DTO.setNf2Sfamilia(null);
        return promoTemporadaNf2DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf2DTO toPromoTemporadaNf2DTONf1() {
        PromoTemporadaNf2DTO promoTemporadaNf2DTO = toPromoTemporadaNf2DTO(this);
        promoTemporadaNf2DTO.setPromoTemporada(null);
        if (this.getNf2Sfamilia() != null) {
            promoTemporadaNf2DTO.setNf2Sfamilia(this.getNf2Sfamilia().toNf2SfamiliaDTOEntitiesNullNf1());
        } else {
            promoTemporadaNf2DTO.setNf2Sfamilia(null);
        }
        return promoTemporadaNf2DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
