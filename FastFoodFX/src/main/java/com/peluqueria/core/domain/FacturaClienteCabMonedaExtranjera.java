package com.peluqueria.core.domain;

import com.peluqueria.dto.FacturaClienteCabMonedaExtranjeraDTO;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the donacion_cliente database table.
 *
 */
@Entity
@Table(name = "factura_cliente_cab_moneda_extranjera", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabMonedaExtranjera.findAll", query = "SELECT fcce FROM FacturaClienteCabMonedaExtranjera fcce")
public class FacturaClienteCabMonedaExtranjera implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_factura_cliente_cab_moneda_extranjera")
    private Long idFacturaClienteCabMonedaExtranjera;

    @Column(name = "monto_extranjero")
    private Integer montoEfectivo;

    @Column(name = "cotizacion")
    private Integer cotizacion;

    @Column(name = "monto_guaranies")
    private Integer monto_guaranies;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    // bi-directional many-to-one association to TipoMoneda
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tipo_moneda")
    private TipoMoneda tipoMoneda;

    // bi-directional many-to-one association to ArticuloProveedor
    @OneToMany(mappedBy = "facturaClienteCabMonedaExtranjera", fetch = FetchType.LAZY)
    private List<DescMonedaExtranjera> descMonedaExtranjera;

    public FacturaClienteCabMonedaExtranjera() {
    }

    public Long getIdFacturaClienteCabMonedaExtranjera() {
        return idFacturaClienteCabMonedaExtranjera;
    }

    public void setIdFacturaClienteCabMonedaExtranjera(
            Long idFacturaClienteCabMonedaExtranjera) {
        this.idFacturaClienteCabMonedaExtranjera = idFacturaClienteCabMonedaExtranjera;
    }

    public Integer getMontoEfectivo() {
        return montoEfectivo;
    }

    public void setMontoEfectivo(Integer montoEfectivo) {
        this.montoEfectivo = montoEfectivo;
    }

    public Integer getCotizacion() {
        return cotizacion;
    }

    public List<DescMonedaExtranjera> getDescMonedaExtranjera() {
        return descMonedaExtranjera;
    }

    public void setDescMonedaExtranjera(
            List<DescMonedaExtranjera> descMonedaExtranjera) {
        this.descMonedaExtranjera = descMonedaExtranjera;
    }

    public void setCotizacion(Integer cotizacion) {
        this.cotizacion = cotizacion;
    }

    public Integer getMonto_guaranies() {
        return monto_guaranies;
    }

    public void setMonto_guaranies(Integer monto_guaranies) {
        this.monto_guaranies = monto_guaranies;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public TipoMoneda getTipoMoneda() {
        return tipoMoneda;
    }

    public void setTipoMoneda(TipoMoneda tipoMoneda) {
        this.tipoMoneda = tipoMoneda;
    }

    public FacturaClienteCabMonedaExtranjeraDTO toFacturaClienteCabMonedaExtranjeraDTO() {
        FacturaClienteCabMonedaExtranjeraDTO factDTO = toFacturaClienteCabMonedaExtranjeraDTO(this);
        if (this.getFacturaClienteCab() != null) {
            factDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        if (this.getTipoMoneda() != null) {
            factDTO.setTipoMoneda(this.getTipoMoneda().toTipoMonedaDTO());
        }
        factDTO.setDescMonedaExtranjera(null);
        return factDTO;
    }

    //Solo obtiene el id y otros datos de esta tabla, pero no obtiene datos de otras tablas asociadas.
    public FacturaClienteCabMonedaExtranjeraDTO toFacturaClienteCabMonedaExtranjeraBDDTO() {
        FacturaClienteCabMonedaExtranjeraDTO factDTO = toFacturaClienteCabMonedaExtranjeraDTO(this);
        factDTO.setFacturaClienteCab(null);
        factDTO.setTipoMoneda(null);
        factDTO.setDescMonedaExtranjera(null);
        return factDTO;
    }

    public FacturaClienteCabMonedaExtranjeraDTO toFacturaClienteCabMonedaExtranjeraDTO(
            FacturaClienteCabMonedaExtranjera fact) {
        FacturaClienteCabMonedaExtranjeraDTO factDTO = new FacturaClienteCabMonedaExtranjeraDTO();
        BeanUtils.copyProperties(fact, factDTO);
        return factDTO;
    }

    public static FacturaClienteCabMonedaExtranjera fromFacturaClienteCabMonedaExtranjera(
            FacturaClienteCabMonedaExtranjeraDTO factDTO) {
        FacturaClienteCabMonedaExtranjera fact = new FacturaClienteCabMonedaExtranjera();
        BeanUtils.copyProperties(factDTO, fact);
        return fact;
    }

    public static FacturaClienteCabMonedaExtranjera fromFacturaClienteCabMonedaExtranjeraAsociado(
            FacturaClienteCabMonedaExtranjeraDTO factDTO) {
        FacturaClienteCabMonedaExtranjera fact = fromFacturaClienteCabMonedaExtranjera(factDTO);
        fact.setTipoMoneda(TipoMoneda.fromTipoMonedaDTO(factDTO.getTipoMoneda()));
        fact.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(factDTO.getFacturaClienteCab()));
        return fact;
    }

}
