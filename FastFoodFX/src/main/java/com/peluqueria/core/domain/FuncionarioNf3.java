package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.FuncionarioNf3DTO;

/**
 * The persistent class for the funcionario_nf3 database table.
 *
 */
@Entity
@Table(name = "funcionario_nf3", schema = "descuento")
@NamedQuery(name = "FuncionarioNf3.findAll", query = "SELECT f FROM FuncionarioNf3 f")
public class FuncionarioNf3 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_funcionario_nf3")
    private Long idFuncionarioNf3;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porc_desc")
    private BigDecimal porcDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to Nf3Sseccion
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf3_sseccion")
    private Nf3Sseccion nf3Sseccion;

    public FuncionarioNf3() {
    }

    public Long getIdFuncionarioNf3() {
        return this.idFuncionarioNf3;
    }

    public void setIdFuncionarioNf3(Long idFuncionarioNf3) {
        this.idFuncionarioNf3 = idFuncionarioNf3;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcDesc() {
        return this.porcDesc;
    }

    public void setPorcDesc(BigDecimal porcDesc) {
        this.porcDesc = porcDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public Nf3Sseccion getNf3Sseccion() {
        return nf3Sseccion;
    }

    public void setNf3Sseccion(Nf3Sseccion nf3Sseccion) {
        this.nf3Sseccion = nf3Sseccion;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static FuncionarioNf3 fromFuncionarioNf3DTO(FuncionarioNf3DTO funcionarioNf3DTO) {
        FuncionarioNf3 funcionarioNf3 = new FuncionarioNf3();
        BeanUtils.copyProperties(funcionarioNf3DTO, funcionarioNf3);
        return funcionarioNf3;
    }

    // <-- SERVER
    public static FuncionarioNf3DTO toFuncionarioNf3DTO(FuncionarioNf3 funcionarioNf3) {
        FuncionarioNf3DTO funcionarioNf3DTO = new FuncionarioNf3DTO();
        BeanUtils.copyProperties(funcionarioNf3, funcionarioNf3DTO);
        return funcionarioNf3DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static FuncionarioNf3 fromFuncionarioNf3DTOEntitiesFull(FuncionarioNf3DTO funcionarioNf3DTO) {
        FuncionarioNf3 funcionarioNf3 = fromFuncionarioNf3DTO(funcionarioNf3DTO);
        if (funcionarioNf3DTO.getNf3Sseccion() != null) {
            funcionarioNf3.setNf3Sseccion(Nf3Sseccion.fromNf3SseccionDTO(funcionarioNf3DTO.getNf3Sseccion()));
        }
        return funcionarioNf3;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULL, FULL; SALIDA ########################
    // <-- SERVER
    public FuncionarioNf3DTO toFuncionarioNf3DTONf2() {
        FuncionarioNf3DTO funcionarioNf3DTO = toFuncionarioNf3DTO(this);
        if (this.getNf3Sseccion() != null) {
            funcionarioNf3DTO.setNf3Sseccion(this.getNf3Sseccion().toNf3SseccionDTOEntitiesNullNf2());
        }
        return funcionarioNf3DTO;
    }
    // ######################## NULL, FULL; SALIDA ########################

}
