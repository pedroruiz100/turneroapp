package com.peluqueria.core.domain;

import com.peluqueria.dto.CancelacionProductoDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the cancelacion_producto database table.
 *
 */
@Entity
@Table(name = "cancelacion_producto", schema = "caja")
@NamedQuery(name = "CancelacionProducto.findAll", query = "SELECT c FROM CancelacionProducto c")
public class CancelacionProducto implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cancelacion_producto")
    private Long idCancelacionProducto;

    @Column(name = "fecha_cancelacion")
    private Timestamp fechaCancelacion;

    @Column(name = "cantidad")
    private BigDecimal cantidad;

    @Column(name = "precio")
    private Integer precio;

    @Column(name = "total")
    private Integer total;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_producto")
    private Articulo articulo;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_cajero")
    private Usuario usuarioCajero;

    // bi-directional many-to-one association to Usuario
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_usuario_supervisor")
    private Usuario usuarioSupervisor;

    // bi-directional many-to-one association to MotivoCancelacionProducto
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_motivo_cancelacion")
    private MotivoCancelacionProducto motivoCancelacionProducto;

    // bi-directional many-to-one association to FacturaClienteCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_factura_cliente_cab")
    private FacturaClienteCab facturaClienteCab;

    public CancelacionProducto() {
    }

    public Long getIdCancelacionProducto() {
        return this.idCancelacionProducto;
    }

    public void setIdCancelacionProducto(Long idCancelacionProducto) {
        this.idCancelacionProducto = idCancelacionProducto;
    }

    public Timestamp getFechaCancelacion() {
        return this.fechaCancelacion;
    }

    public void setFechaCancelacion(Timestamp fechaCancelacion) {
        this.fechaCancelacion = fechaCancelacion;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public MotivoCancelacionProducto getMotivoCancelacionProducto() {
        return this.motivoCancelacionProducto;
    }

    public void setMotivoCancelacionProducto(
            MotivoCancelacionProducto motivoCancelacionProducto) {
        this.motivoCancelacionProducto = motivoCancelacionProducto;
    }

    public Integer getPrecio() {
        return precio;
    }

    public void setPrecio(Integer precio) {
        this.precio = precio;
    }

    public FacturaClienteCab getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public Articulo getArticulo() {
        return this.articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public Usuario getUsuarioCajero() {
        return this.usuarioCajero;
    }

    public void setUsuarioCajero(Usuario usuarioCajero) {
        this.usuarioCajero = usuarioCajero;
    }

    public BigDecimal getCantidad() {
        return cantidad;
    }

    public void setCantidad(BigDecimal cantidad) {
        this.cantidad = cantidad;
    }

    public Usuario getUsuarioSupervisor() {
        return this.usuarioSupervisor;
    }

    public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
        this.usuarioSupervisor = usuarioSupervisor;
    }

    public CancelacionProductoDTO toCancelacionProductoDTO() {
        CancelacionProductoDTO canDTO = toCancelacionProductoDTO(this);
        if (this.articulo != null) {
            canDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
        }

        if (this.facturaClienteCab != null) {
            canDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }

        if (this.motivoCancelacionProducto != null) {
            canDTO.setMotivoCancelacionProducto(this
                    .getMotivoCancelacionProducto()
                    .toBDMotivoCancelacionProductoDTO());
        }

        if (this.usuarioCajero != null) {
            canDTO.setUsuarioCajero(this.getUsuarioCajero().toBDUsuarioDTO());
        }

        if (this.usuarioSupervisor != null) {
            canDTO.setUsuarioSupervisor(this.getUsuarioSupervisor()
                    .toBDUsuarioDTO());
        }

        return canDTO;
    }

    public CancelacionProductoDTO toBDCancelacionProductoDTO() {
        CancelacionProductoDTO canDTO = toCancelacionProductoDTO(this);
        canDTO.setArticulo(null);
        canDTO.setMotivoCancelacionProducto(null);
        canDTO.setUsuarioCajero(null);
        canDTO.setUsuarioSupervisor(null);
        canDTO.setFacturaClienteCab(null);
        return canDTO;
    }

    public static CancelacionProductoDTO toCancelacionProductoDTO(
            CancelacionProducto cancelacionProducto) {
        CancelacionProductoDTO canDTO = new CancelacionProductoDTO();
        BeanUtils.copyProperties(cancelacionProducto, canDTO);
        return canDTO;
    }

    public static CancelacionProducto fromCancelacionProductoDTO(
            CancelacionProductoDTO canDTO) {
        CancelacionProducto can = new CancelacionProducto();
        BeanUtils.copyProperties(canDTO, can);
        return can;
    }

    public static CancelacionProducto fromCancelacionProductoAsociadoDTO(
            CancelacionProductoDTO canDTO) {
        CancelacionProducto can = fromCancelacionProductoDTO(canDTO);
        can.setArticulo(Articulo.fromArticuloDTO(canDTO.getArticulo()));
        can.setMotivoCancelacionProducto(MotivoCancelacionProducto
                .fromMotivoCancelacionProductoDTO(canDTO
                        .getMotivoCancelacionProducto()));
        can.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(canDTO
                .getUsuarioCajero()));
        can.setUsuarioSupervisor(Usuario.fromUsuarioSupervisorDTO(canDTO
                .getUsuarioSupervisor()));
        can.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(canDTO.getFacturaClienteCab()));
        return can;
    }

}
