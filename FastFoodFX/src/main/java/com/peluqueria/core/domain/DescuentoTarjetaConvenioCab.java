package com.peluqueria.core.domain;

import com.peluqueria.dto.DescuentoTarjetaConvenioCabDTO;
import com.peluqueria.dto.DescuentoTarjetaConvenioDetDTO;
import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the descuento_tarjeta_convenio_cab database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_convenio_cab", schema = "cuenta")
@NamedQuery(name = "DescuentoTarjetaConvenioCab.findAll", query = "SELECT d FROM DescuentoTarjetaConvenioCab d")
public class DescuentoTarjetaConvenioCab implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_convenio_cab")
    private Long idDescuentoTarjetaConvenioCab;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_tarjeta_convenio")
    private TarjetaConvenio tarjetaConvenio;

    @Column(name = "descri_tarjeta_convenio")
    private String descriTarjetaConvenio;

    @Column(name = "estado_desc")
    private Boolean estadoDesc;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to DescuentoTarjetaConvenioDet
    @OneToMany(mappedBy = "descuentoTarjetaConvenioCab")
    private List<DescuentoTarjetaConvenioDet> descuentoTarjetaConvenioDets;

    public DescuentoTarjetaConvenioCab() {
    }

    public Long getIdDescuentoTarjetaConvenioCab() {
        return this.idDescuentoTarjetaConvenioCab;
    }

    public void setIdDescuentoTarjetaConvenioCab(
            Long idDescuentoTarjetaConvenioCab) {
        this.idDescuentoTarjetaConvenioCab = idDescuentoTarjetaConvenioCab;
    }

    public String getDescriTarjetaConvenio() {
        return this.descriTarjetaConvenio;
    }

    public void setDescriTarjetaConvenio(String descriTarjetaConvenio) {
        this.descriTarjetaConvenio = descriTarjetaConvenio;
    }

    public Boolean getEstadoDesc() {
        return this.estadoDesc;
    }

    public void setEstadoDesc(Boolean estadoDesc) {
        this.estadoDesc = estadoDesc;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public TarjetaConvenio getTarjetaConvenio() {
        return tarjetaConvenio;
    }

    public void setTarjetaConvenio(TarjetaConvenio tarjetaConvenio) {
        this.tarjetaConvenio = tarjetaConvenio;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<DescuentoTarjetaConvenioDet> getDescuentoTarjetaConvenioDets() {
        return this.descuentoTarjetaConvenioDets;
    }

    public void setDescuentoTarjetaConvenioDets(
            List<DescuentoTarjetaConvenioDet> descuentoTarjetaConvenioDets) {
        this.descuentoTarjetaConvenioDets = descuentoTarjetaConvenioDets;
    }

    public DescuentoTarjetaConvenioCabDTO toDescuentoParaDetalleDTO() {
        DescuentoTarjetaConvenioCabDTO dfcDTO = toDescuentoTarjetaConvenioCabDTO(this);
        dfcDTO.setDescuentoTarjetaConvenioDetDTO(null);
        if (this.tarjetaConvenio != null) {
            TarjetaConvenio tc = this.getTarjetaConvenio();
            dfcDTO.setTarjetaConvenio(tc.toTarjetaConvenioDTO());
        }

        return dfcDTO;
    }

    public DescuentoTarjetaConvenioCabDTO toDescuentoTarjetaConvenioCabDTO() {
        DescuentoTarjetaConvenioCabDTO dfcDTO = toDescuentoTarjetaConvenioCabDTO(this);
        if (!this.descuentoTarjetaConvenioDets.isEmpty()) {
            List<DescuentoTarjetaConvenioDet> d = this.descuentoTarjetaConvenioDets;
            List<DescuentoTarjetaConvenioDetDTO> dDTO = new ArrayList<DescuentoTarjetaConvenioDetDTO>();
            for (DescuentoTarjetaConvenioDet dfd : d) {
                DescuentoTarjetaConvenioDetDTO dto = dfd
                        .toDescripcionDescuentoTarjetaConvenioDetDTO();
                dDTO.add(dto);
            }
            dfcDTO.setDescuentoTarjetaConvenioDetDTO(dDTO);
        }
        if (this.tarjetaConvenio != null) {
            TarjetaConvenio tc = this.getTarjetaConvenio();
            dfcDTO.setTarjetaConvenio(tc.toTarjetaConvenioDTO());
        }
        return dfcDTO;
    }

    public static DescuentoTarjetaConvenioCabDTO toDescuentoTarjetaConvenioCabDTO(
            DescuentoTarjetaConvenioCab dfd) {
        DescuentoTarjetaConvenioCabDTO dfcDTO = new DescuentoTarjetaConvenioCabDTO();
        BeanUtils.copyProperties(dfd, dfcDTO);
        return dfcDTO;
    }

    public static DescuentoTarjetaConvenioCab fromDescuentoTarjetaConvenioCabDTO(
            DescuentoTarjetaConvenioCabDTO dfdDTO) {
        DescuentoTarjetaConvenioCab dfd = new DescuentoTarjetaConvenioCab();
        BeanUtils.copyProperties(dfdDTO, dfd);
        dfd.setTarjetaConvenio(TarjetaConvenio.fromTarjetaConvenioDTO(dfdDTO
                .getTarjetaConvenio()));
        return dfd;
    }

    // Para Cabecera descuento cliente fiel sin tarjetaConvenio
    public static DescuentoTarjetaConvenioCab fromDescuentoTarjetaConvenioCabSinTarjetaDTO(
            DescuentoTarjetaConvenioCabDTO dfdDTO) {
        DescuentoTarjetaConvenioCab dfd = new DescuentoTarjetaConvenioCab();
        BeanUtils.copyProperties(dfdDTO, dfd);
        return dfd;
    }
}
