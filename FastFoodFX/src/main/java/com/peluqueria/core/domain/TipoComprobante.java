package com.peluqueria.core.domain;

import com.peluqueria.dto.TipoComprobanteDTO;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_comprobante database table.
 *
 */
@Entity
@Table(name = "tipo_comprobante", schema = "factura_cliente")
@NamedQuery(name = "TipoComprobante.findAll", query = "SELECT t FROM TipoComprobante t")
public class TipoComprobante implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_comprobante")
    private Long idTipoComprobante;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to FacturaClienteCab
    @OneToMany(mappedBy = "tipoComprobante", fetch = FetchType.LAZY)
    private List<FacturaClienteCab> facturaClienteCabs;

    // bi-directional many-to-one association to NotaCreditoCab
    @OneToMany(mappedBy = "tipoComprobante", fetch = FetchType.LAZY)
    private List<NotaCreditoCab> notaCreditoCabs;

    public TipoComprobante() {
    }

    public Long getIdTipoComprobante() {
        return this.idTipoComprobante;
    }

    public void setIdTipoComprobante(Long idTipoComprobante) {
        this.idTipoComprobante = idTipoComprobante;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<FacturaClienteCab> getFacturaClienteCabs() {
        return this.facturaClienteCabs;
    }

    public void setFacturaClienteCabs(List<FacturaClienteCab> facturaClienteCabs) {
        this.facturaClienteCabs = facturaClienteCabs;
    }

    public FacturaClienteCab addFacturaClienteCab(
            FacturaClienteCab facturaClienteCab) {
        getFacturaClienteCabs().add(facturaClienteCab);
        facturaClienteCab.setTipoComprobante(this);
        return facturaClienteCab;
    }

    public FacturaClienteCab removeFacturaClienteCab(
            FacturaClienteCab facturaClienteCab) {
        getFacturaClienteCabs().remove(facturaClienteCab);
        facturaClienteCab.setTipoComprobante(null);
        return facturaClienteCab;
    }

    public List<NotaCreditoCab> getNotaCreditoCabs() {
        return this.notaCreditoCabs;
    }

    public void setNotaCreditoCabs(List<NotaCreditoCab> notaCreditoCabs) {
        this.notaCreditoCabs = notaCreditoCabs;
    }

    public TipoComprobanteDTO toTipoComprobanteDTO() {
        TipoComprobanteDTO tipoComDTO = toTipoComprobanteDTO(this);
        tipoComDTO.setFacturaClienteCabs(null);
        tipoComDTO.setNotaCreditoCabs(null);
        return tipoComDTO;
    }

    public static TipoComprobanteDTO toTipoComprobanteDTO(
            TipoComprobante tipoComprobante) {
        TipoComprobanteDTO tcDTO = new TipoComprobanteDTO();
        BeanUtils.copyProperties(tipoComprobante, tcDTO);
        return tcDTO;
    }

    // SOLO PARA ID Y DESCRIPCION (no para columnas asociadas en el caso que lo tenga)
    public static TipoComprobante fromTipoComprobanteDTO(
            TipoComprobanteDTO tcDTO) {
        TipoComprobante tc = new TipoComprobante();
        BeanUtils.copyProperties(tcDTO, tc);
        return tc;
    }

}
