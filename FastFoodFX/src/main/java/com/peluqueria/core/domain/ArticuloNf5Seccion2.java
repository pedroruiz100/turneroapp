package com.peluqueria.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.ArticuloNf5Seccion2DTO;

/**
 * The persistent class for the articulo_nf5_seccion2 database table.
 *
 */
@Entity
@Table(name = "articulo_nf5_seccion2", schema = "stock")
@NamedQuery(name = "ArticuloNf5Seccion2.findAll", query = "SELECT a FROM ArticuloNf5Seccion2 a")
public class ArticuloNf5Seccion2 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_nf5_seccion2_articulo")
    private Long idNf5Seccion2Articulo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    // bi-directional many-to-one association to Articulo
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_articulo")
    private Articulo articulo;

    @Column(name = "usu_alta")
    private String usuAlta;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    public ArticuloNf5Seccion2() {
    }

    public Long getIdNf5Seccion2Articulo() {
        return this.idNf5Seccion2Articulo;
    }

    public void setIdNf5Seccion2Articulo(Long idNf5Seccion2Articulo) {
        this.idNf5Seccion2Articulo = idNf5Seccion2Articulo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Articulo getArticulo() {
        return articulo;
    }

    public void setArticulo(Articulo articulo) {
        this.articulo = articulo;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return this.nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static ArticuloNf5Seccion2 fromArticuloNf5Seccion2DTO(ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO) {
        ArticuloNf5Seccion2 articuloNf5Seccion2 = new ArticuloNf5Seccion2();
        BeanUtils.copyProperties(articuloNf5Seccion2DTO, articuloNf5Seccion2);
        return articuloNf5Seccion2;
    }

    // <-- SERVER
    public static ArticuloNf5Seccion2DTO toArticuloNf5Seccion2DTO(ArticuloNf5Seccion2 articuloNf5Seccion2) {
        ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO = new ArticuloNf5Seccion2DTO();
        BeanUtils.copyProperties(articuloNf5Seccion2, articuloNf5Seccion2DTO);
        return articuloNf5Seccion2DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULO, FULL SALIDA ########################
    // <-- SERVER
    public static ArticuloNf5Seccion2DTO toArticuloNf5Seccion2DTOEntitiesNull(ArticuloNf5Seccion2 articuloNf5Seccion2) {
        ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO = toArticuloNf5Seccion2DTO(articuloNf5Seccion2);
        articuloNf5Seccion2DTO.setArticulo(null);
        articuloNf5Seccion2DTO.setNf5Seccion2(null);
        return articuloNf5Seccion2DTO;
    }

    // <-- SERVER
    public ArticuloNf5Seccion2DTO toArticuloNf5Seccion2DTOEntitiesNull() {
        ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO = toArticuloNf5Seccion2DTO(this);
        articuloNf5Seccion2DTO.setArticulo(null);
        articuloNf5Seccion2DTO.setNf5Seccion2(null);
        return articuloNf5Seccion2DTO;
    }

    // <-- SERVER
    public ArticuloNf5Seccion2DTO toArticuloNf5Seccion2DTOEntitiesNf() {
        ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO = toArticuloNf5Seccion2DTO(this);
        articuloNf5Seccion2DTO.setArticulo(null);
        if (this.getNf5Seccion2() != null) {
            articuloNf5Seccion2DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            articuloNf5Seccion2DTO.setNf5Seccion2(null);
        }
        return articuloNf5Seccion2DTO;
    }
    // ######################## NULO, FULL SALIDA ########################

    // ######################## NULO, FULL ENTRADA ########################
    // --> SERVER
    public static ArticuloNf5Seccion2 fromArticuloNf5Seccion2DTOEntitiesFull(
            ArticuloNf5Seccion2DTO articuloNf5Seccion2DTO) {
        ArticuloNf5Seccion2 articuloNf5Seccion2 = fromArticuloNf5Seccion2DTO(articuloNf5Seccion2DTO);
        if (articuloNf5Seccion2DTO.getArticulo() != null) {
            articuloNf5Seccion2.setArticulo(Articulo.fromArticuloDTO(articuloNf5Seccion2DTO.getArticulo()));
        }
        if (articuloNf5Seccion2DTO.getNf5Seccion2() != null) {
            articuloNf5Seccion2.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(articuloNf5Seccion2DTO.getNf5Seccion2()));
        }
        return articuloNf5Seccion2;
    }
    // ######################## NULO, FULL ENTRADA ########################

}
