package com.peluqueria.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.PromoTemporadaNf6DTO;

/**
 * The persistent class for the promo_temporada_nf6 database table.
 *
 */
@Entity
@Table(name = "promo_temporada_nf6", schema = "descuento")
@NamedQuery(name = "PromoTemporadaNf6.findAll", query = "SELECT p FROM PromoTemporadaNf6 p")
public class PromoTemporadaNf6 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_promo_temporada_nf6")
    private Long idPromoTemporadaNf6;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    @Column(name = "porcentaje_desc")
    private BigDecimal porcentajeDesc;

    // bi-directional many-to-one association to PromoTemporada
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_temporada")
    private PromoTemporada promoTemporada;

    // bi-directional many-to-one association to Nf6Secnom6
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf6_secnom6")
    private Nf6Secnom6 nf6Secnom6;

    public PromoTemporadaNf6() {
    }

    public Long getIdPromoTemporadaNf6() {
        return this.idPromoTemporadaNf6;
    }

    public void setIdPromoTemporadaNf6(Long idPromoTemporadaNf6) {
        this.idPromoTemporadaNf6 = idPromoTemporadaNf6;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public PromoTemporada getPromoTemporada() {
        return promoTemporada;
    }

    public void setPromoTemporada(PromoTemporada promoTemporada) {
        this.promoTemporada = promoTemporada;
    }

    public Nf6Secnom6 getNf6Secnom6() {
        return nf6Secnom6;
    }

    public void setNf6Secnom6(Nf6Secnom6 nf6Secnom6) {
        this.nf6Secnom6 = nf6Secnom6;
    }

    public BigDecimal getPorcentajeDesc() {
        return this.porcentajeDesc;
    }

    public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
        this.porcentajeDesc = porcentajeDesc;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static PromoTemporadaNf6 fromPromoTemporadaNf6DTO(PromoTemporadaNf6DTO promoTemporadaNf6DTO) {
        PromoTemporadaNf6 promoTemporadaNf6 = new PromoTemporadaNf6();
        BeanUtils.copyProperties(promoTemporadaNf6DTO, promoTemporadaNf6);
        return promoTemporadaNf6;
    }

    // <-- SERVER
    public static PromoTemporadaNf6DTO toPromoTemporadaNf6DTO(PromoTemporadaNf6 promoTemporadaNf6) {
        PromoTemporadaNf6DTO promoTemporadaNf6DTO = new PromoTemporadaNf6DTO();
        BeanUtils.copyProperties(promoTemporadaNf6, promoTemporadaNf6DTO);
        return promoTemporadaNf6DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static PromoTemporadaNf6 fromPromoTemporadaNf6DTOEntitiesFull(PromoTemporadaNf6DTO promoTemporadaNf6DTO) {
        PromoTemporadaNf6 promoTemporadaNf6 = fromPromoTemporadaNf6DTO(promoTemporadaNf6DTO);
        if (promoTemporadaNf6DTO.getNf6Secnom6() != null) {
            promoTemporadaNf6.setNf6Secnom6(Nf6Secnom6.fromNf6Secnom6DTO(promoTemporadaNf6DTO.getNf6Secnom6()));
        }
        if (promoTemporadaNf6DTO.getPromoTemporada() != null) {
            promoTemporadaNf6
                    .setPromoTemporada(PromoTemporada.fromPromoTemporadaDTO(promoTemporadaNf6DTO.getPromoTemporada()));
        }
        return promoTemporadaNf6;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public PromoTemporadaNf6DTO toPromoTemporadaNf6DTOEntityFull() {
        PromoTemporadaNf6DTO promoTemporadaNf6DTO = toPromoTemporadaNf6DTO(this);
        if (this.getPromoTemporada() != null) {
            promoTemporadaNf6DTO.setPromoTemporada(this.getPromoTemporada().toPromoTemporadaDTOEntitiesNull());
        } else {
            promoTemporadaNf6DTO.setPromoTemporada(null);
        }
        if (this.getNf6Secnom6() != null) {
            promoTemporadaNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            promoTemporadaNf6DTO.setNf6Secnom6(null);
        }
        return promoTemporadaNf6DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf6DTO toPromoTemporadaNf6DTOEntityNull() {
        PromoTemporadaNf6DTO promoTemporadaNf6DTO = toPromoTemporadaNf6DTO(this);
        promoTemporadaNf6DTO.setPromoTemporada(null);
        promoTemporadaNf6DTO.setNf6Secnom6(null);
        return promoTemporadaNf6DTO;
    }

    // <-- SERVER
    public PromoTemporadaNf6DTO toPromoTemporadaNf6DTONf5() {
        PromoTemporadaNf6DTO promoTemporadaNf6DTO = toPromoTemporadaNf6DTO(this);
        promoTemporadaNf6DTO.setPromoTemporada(null);
        if (this.getNf6Secnom6() != null) {
            promoTemporadaNf6DTO.setNf6Secnom6(this.getNf6Secnom6().toNf6Secnom6DTOEntitiesNullNf5());
        } else {
            promoTemporadaNf6DTO.setNf6Secnom6(null);
        }
        return promoTemporadaNf6DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
