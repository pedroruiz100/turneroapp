package com.peluqueria.core.domain;

import com.peluqueria.dto.ComboCabDTO;
import com.peluqueria.dto.TipoComboDTO;
import java.io.Serializable;

import javax.persistence.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.BeanUtils;

/**
 * The persistent class for the tipo_combo database table.
 *
 */
@Entity
@Table(name = "tipo_combo", schema = "stock")
@NamedQuery(name = "TipoCombo.findAll", query = "SELECT t FROM TipoCombo t")
public class TipoCombo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipo_combo")
    private Long idTipoCombo;

    @Column(name = "descri_tipo_combo")
    private String descriTipoCombo;

    @Column(name = "fecha_alta")
    private Timestamp fechaAlta;

    @Column(name = "fecha_mod")
    private Timestamp fechaMod;

    @Column(name = "usu_alta")
    private String usuAlta;

    @Column(name = "usu_mod")
    private String usuMod;

    // bi-directional many-to-one association to ComboCab
    @OneToMany(mappedBy = "tipoCombo", fetch = FetchType.LAZY)
    private List<ComboCab> comboCabs;

    public TipoCombo() {
    }

    public Long getIdTipoCombo() {
        return this.idTipoCombo;
    }

    public void setIdTipoCombo(Long idTipoCombo) {
        this.idTipoCombo = idTipoCombo;
    }

    public String getDescriTipoCombo() {
        return this.descriTipoCombo;
    }

    public void setDescriTipoCombo(String descriTipoCombo) {
        this.descriTipoCombo = descriTipoCombo;
    }

    public Timestamp getFechaAlta() {
        return this.fechaAlta;
    }

    public void setFechaAlta(Timestamp fechaAlta) {
        this.fechaAlta = fechaAlta;
    }

    public Timestamp getFechaMod() {
        return this.fechaMod;
    }

    public void setFechaMod(Timestamp fechaMod) {
        this.fechaMod = fechaMod;
    }

    public String getUsuAlta() {
        return this.usuAlta;
    }

    public void setUsuAlta(String usuAlta) {
        this.usuAlta = usuAlta;
    }

    public String getUsuMod() {
        return this.usuMod;
    }

    public void setUsuMod(String usuMod) {
        this.usuMod = usuMod;
    }

    public List<ComboCab> getComboCabs() {
        return this.comboCabs;
    }

    public void setComboCabs(List<ComboCab> comboCabs) {
        this.comboCabs = comboCabs;
    }

    public TipoComboDTO toTipoComboDTO() {
        TipoComboDTO tcDTO = toTipoComboDTO(this);
        if (!this.comboCabs.isEmpty()) {
            List<ComboCab> cc = new ArrayList<ComboCab>();
            List<ComboCabDTO> ccDTO = new ArrayList<ComboCabDTO>();
            for (ComboCab cCab : cc) {
                ccDTO.add(cCab.toBDComboCabDTO());
            }
            tcDTO.setComboCabs(ccDTO);
        }
        return tcDTO;
    }

    public TipoComboDTO toBDTipoComboDTO() {
        TipoComboDTO tcDTO = toTipoComboDTO(this);
        tcDTO.setComboCabs(null);
        return tcDTO;
    }

    public static TipoComboDTO toTipoComboDTO(TipoCombo tipoCombo) {
        TipoComboDTO tcDTO = new TipoComboDTO();
        BeanUtils.copyProperties(tipoCombo, tcDTO);
        return tcDTO;
    }

    public static TipoCombo fromTipoComboDTO(TipoComboDTO tcDTO) {
        TipoCombo tc = new TipoCombo();
        BeanUtils.copyProperties(tcDTO, tc);
        return tc;
    }

}
