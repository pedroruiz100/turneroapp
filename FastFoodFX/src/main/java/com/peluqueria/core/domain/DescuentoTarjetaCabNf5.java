package com.peluqueria.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.peluqueria.dto.DescuentoTarjetaCabNf5DTO;

/**
 * The persistent class for the descuento_tarjeta_cab_nf5 database table.
 *
 */
@Entity
@Table(name = "descuento_tarjeta_cab_nf5", schema = "descuento")
@NamedQuery(name = "DescuentoTarjetaCabNf5.findAll", query = "SELECT d FROM DescuentoTarjetaCabNf5 d")
public class DescuentoTarjetaCabNf5 implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_descuento_tarjeta_cab_nf5")
    private Long idDescuentoTarjetaCabNf5;

    @Column(name = "descri_seccion")
    private String descriSeccion;

    // bi-directional many-to-one association to DescuentoTarjetaCab
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_descuento_tarjeta_cab")
    private DescuentoTarjetaCab descuentoTarjetaCab;

    // bi-directional many-to-one association to Nf5Seccion2
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_nf5_seccion2")
    private Nf5Seccion2 nf5Seccion2;

    public DescuentoTarjetaCabNf5() {
    }

    public Long getIdDescuentoTarjetaCabNf5() {
        return this.idDescuentoTarjetaCabNf5;
    }

    public void setIdDescuentoTarjetaCabNf5(Long idDescuentoTarjetaCabNf5) {
        this.idDescuentoTarjetaCabNf5 = idDescuentoTarjetaCabNf5;
    }

    public String getDescriSeccion() {
        return this.descriSeccion;
    }

    public void setDescriSeccion(String descriSeccion) {
        this.descriSeccion = descriSeccion;
    }

    public DescuentoTarjetaCab getDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public void setDescuentoTarjetaCab(DescuentoTarjetaCab descuentoTarjetaCab) {
        this.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public Nf5Seccion2 getNf5Seccion2() {
        return nf5Seccion2;
    }

    public void setNf5Seccion2(Nf5Seccion2 nf5Seccion2) {
        this.nf5Seccion2 = nf5Seccion2;
    }

    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################
    // --> SERVER
    public static DescuentoTarjetaCabNf5 fromDescuentoTarjetaCabNf5DTO(DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO) {
        DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5 = new DescuentoTarjetaCabNf5();
        BeanUtils.copyProperties(descuentoTarjetaCabNf5DTO, descuentoTarjetaCabNf5);
        return descuentoTarjetaCabNf5;
    }

    // <-- SERVER
    public static DescuentoTarjetaCabNf5DTO toDescuentoTarjetaCabNf5DTO(DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5) {
        DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO = new DescuentoTarjetaCabNf5DTO();
        BeanUtils.copyProperties(descuentoTarjetaCabNf5, descuentoTarjetaCabNf5DTO);
        return descuentoTarjetaCabNf5DTO;
    }
    // ################# ESTÁNDAR, ENTRADA Y SALIDA #################

    // ######################## NULL, FULL; ENTRADA ########################
    // --> SERVER
    public static DescuentoTarjetaCabNf5 fromDescuentoTarjetaCabNf5DTOEntitiesFull(DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO) {
        DescuentoTarjetaCabNf5 descuentoTarjetaCabNf5 = fromDescuentoTarjetaCabNf5DTO(descuentoTarjetaCabNf5DTO);
        if (descuentoTarjetaCabNf5DTO.getNf5Seccion2() != null) {
            descuentoTarjetaCabNf5.setNf5Seccion2(Nf5Seccion2.fromNf5Seccion2DTO(descuentoTarjetaCabNf5DTO.getNf5Seccion2()));
        }
        if (descuentoTarjetaCabNf5DTO.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf5
                    .setDescuentoTarjetaCab(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabNf5DTO.getDescuentoTarjetaCab()));
        }
        return descuentoTarjetaCabNf5;
    }
    // ######################## NULL, FULL; ENTRADA ########################

    // ######################## NULO, FULL; SALIDA ########################
    // <-- SERVER
    public DescuentoTarjetaCabNf5DTO toDescuentoTarjetaCabNf5DTOEntityFull() {
        DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO = toDescuentoTarjetaCabNf5DTO(this);
        if (this.getDescuentoTarjetaCab() != null) {
            descuentoTarjetaCabNf5DTO.setDescuentoTarjetaCab(this.getDescuentoTarjetaCab().toDescuentoTarjetaCabDTOEntitiesNull());
        } else {
            descuentoTarjetaCabNf5DTO.setDescuentoTarjetaCab(null);
        }
        if (this.getNf5Seccion2() != null) {
            descuentoTarjetaCabNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            descuentoTarjetaCabNf5DTO.setNf5Seccion2(null);
        }
        return descuentoTarjetaCabNf5DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf5DTO toDescuentoTarjetaCabNf5DTOEntityNull() {
        DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO = toDescuentoTarjetaCabNf5DTO(this);
        descuentoTarjetaCabNf5DTO.setDescuentoTarjetaCab(null);
        descuentoTarjetaCabNf5DTO.setNf5Seccion2(null);
        return descuentoTarjetaCabNf5DTO;
    }

    // <-- SERVER
    public DescuentoTarjetaCabNf5DTO toDescuentoTarjetaCabNf5DTONf4() {
        DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO = toDescuentoTarjetaCabNf5DTO(this);
        descuentoTarjetaCabNf5DTO.setDescuentoTarjetaCab(null);
        if (this.getNf5Seccion2() != null) {
            descuentoTarjetaCabNf5DTO.setNf5Seccion2(this.getNf5Seccion2().toNf5Seccion2DTOEntitiesNullNf4());
        } else {
            descuentoTarjetaCabNf5DTO.setNf5Seccion2(null);
        }
        return descuentoTarjetaCabNf5DTO;
    }
    // ######################## NULO, FULL; SALIDA ########################

}
