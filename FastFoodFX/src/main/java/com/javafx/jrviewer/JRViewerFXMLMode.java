package com.javafx.jrviewer;

public enum JRViewerFXMLMode {
    REPORT_PRINT, REPORT_VIEW
}
