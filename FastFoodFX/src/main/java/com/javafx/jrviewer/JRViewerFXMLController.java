package com.javafx.jrviewer;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Slider;
import javafx.scene.control.TitledPane;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javax.imageio.ImageIO;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class JRViewerFXMLController extends BaseScreenController implements Initializable {

    private JRViewerFXMLMode printMode;
    private String reportFilename;
    private JRDataSource reportDataset;
    @SuppressWarnings("rawtypes")
    private Map reportParameters;
    private ChangeListener<Number> zoomListener;
    private JasperPrint jasperPrint;
    private Stage parentStage;
    private Double zoomFactor;
    private double imageHeight;
    private double imageWidth;
    private List<Integer> pages;
    private Popup popup;
    private Label errorLabel;
    boolean showingToast;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private ImageView imageView;
    @FXML
    ComboBox<Integer> pageList;
    @FXML
    Slider zoomLevel;
    @FXML
    private TitledPane resultPane;
    @FXML
    private Accordion resultAccordion;
    @FXML
    private Label resultDescription;
    @FXML
    private Button buttonVolverJRViewerFX;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Pane paneVarios;

    /**
     * Initializes the controller class.
     *
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverJRViewerFXAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    public boolean save() {
        return guardando();
    }

    @FXML
    private void print() {
        imprimiendo();
    }

    @FXML
    private void pageListSelected(final ActionEvent event) {
        borderPanePage(pageList.getSelectionModel().getSelectedItem() - 1);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    public void cargandoInicial() {
        jasperPrint = JRViewerFXML.getJasperPrint();
        if (reportParameters == null) {
            reportParameters = new HashMap();
        }
        if (printMode == null || printMode == JRViewerFXMLMode.REPORT_VIEW) {
            popup = new Popup();
            errorLabel = new Label("Error");
            errorLabel.setWrapText(true);
            errorLabel.setMaxHeight(200);
            errorLabel.setMinSize(100, 100);
            errorLabel.setMaxWidth(100);
            errorLabel.setAlignment(Pos.TOP_LEFT);
            errorLabel.getStyleClass().add("errorToastLabel");
            popup.getContent().add(errorLabel);
            errorLabel.opacityProperty().bind(popup.opacityProperty());
            zoomFactor = 1d;
            zoomLevel.setValue(100d);
            imageView.setX(0);
            imageView.setY(0);
            imageHeight = jasperPrint.getPageHeight();
            imageWidth = jasperPrint.getPageWidth();
            if (zoomListener != null) {
                zoomLevel.valueProperty().removeListener(zoomListener);
            }
            zoomListener = new ChangeListener<Number>() {
                @Override
                public void changed(ObservableValue<? extends Number> observable,
                        Number oldValue, Number newValue) {
                    zoomFactor = newValue.doubleValue() / 100;
                    imageView.setFitHeight(imageHeight * zoomFactor);
                    imageView.setFitWidth(imageWidth * zoomFactor);
                    //***
                    anchorPane.getChildren()
                            .get(anchorPane.getChildren().indexOf(scrollPane))
                            .relocate(((MainApp.getWidth() - imageView.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageView.getLayoutBounds().getHeight()) / 2));
                }
            };
            zoomLevel.valueProperty().addListener(zoomListener);
            if (jasperPrint.getPages().size() > 0) {
                borderPanePage(0);
                pages = new ArrayList<>();
                for (int i = 0; i < jasperPrint.getPages().size(); i++) {
                    pages.add(i + 1);
                }
                pageList.setItems(FXCollections.observableArrayList(pages));
                pageList.getSelectionModel().select(0);
            }
        } else if (printMode == JRViewerFXMLMode.REPORT_PRINT) {
            print();
        }
        ubicandoContenedorSecundario();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (ScreensContoller.getFxml().equals("/vista/stock/BuscarOrdenCompraFXML.fxml") || ScreensContoller.getFxml().equals("/vista/stock/MatrizFXML.fxml")
                || ScreensContoller.getFxml().equals("/vista/stock/PedidoCompraFXML.fxml") || ScreensContoller.getFxml().equals("/vista/stock/DevolucionFXML.fxml")) {
            if (StageSecond.getStageData().isShowing()) {
                StageSecond.getStageData().close();
            }
        } else {
            this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
                    ScreensContoller.getHeight(), ScreensContoller.getFxml(),
                    ScreensContoller.getWidth(), ScreensContoller.getHeight(), false);
        }

//        a
    }
//    private void volviendo() {|| ScreensContoller.getFxml().equals("/vista/stock/CruceFXML.fxml")
//        this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
//                ScreensContoller.getHeight(), ScreensContoller.getFxml(),
//                ScreensContoller.getWidth(), ScreensContoller.getHeight(), false);
//    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    private void ubicandoContenedorSecundario() {
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(scrollPane))
                .resize(imageView.getLayoutBounds().getWidth(), imageView.getLayoutBounds().getHeight());
        //***
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(scrollPane))
                .relocate(((MainApp.getWidth() - imageView.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageView.getLayoutBounds().getHeight()) / 2));
        //***
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(paneVarios))
                .relocate(((MainApp.getWidth() - paneVarios.getPrefWidth()) / 2), 0);
        //***
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(resultAccordion))
                .relocate(((MainApp.getWidth() - resultAccordion.getPrefWidth()) / 2), (MainApp.getHeight() - resultAccordion.getPrefHeight()));
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(paneVarios)).toFront();
    }

    private boolean guardando() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PDF Document", Arrays.asList("*.pdf", "*.PDF")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PNG image", Arrays.asList("*.png", "*.PNG")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("DOCX Document", Arrays.asList("*.docx", "*.DOCX")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XLSX Document", Arrays.asList("*.xlsx", "*.XLSX")));
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("HTML Document", Arrays.asList("*.html", "*.HTML")));
        File file = fileChooser.showSaveDialog(parentStage);
        if (fileChooser.getSelectedExtensionFilter() != null && fileChooser.getSelectedExtensionFilter().getExtensions() != null) {
            List<String> selectedExtension = fileChooser.getSelectedExtensionFilter().getExtensions();
            if (selectedExtension.contains("*.pdf")) {
                try {
                    JasperExportManager.exportReportToPdfFile(jasperPrint, file.getAbsolutePath());
                } catch (JRException ex) {
                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
                }
            } else if (selectedExtension.contains("*.png")) {
                for (int i = 0; i < jasperPrint.getPages().size(); i++) {
                    String fileNumber = "0000" + Integer.toString(i + 1);
                    fileNumber = fileNumber.substring(fileNumber.length() - 4, fileNumber.length());
                    WritableImage image = getImage(i);
                    String[] fileTokens = file.getAbsolutePath().split("\\.");
                    String filename = "";
                    //add number to filename
                    if (fileTokens.length > 0) {
                        for (int i2 = 0; i2 < fileTokens.length - 1; i2++) {
                            filename = filename + fileTokens[i2] + ((i2 < fileTokens.length - 2) ? "." : "");
                        }
                        filename = filename + fileNumber + "." + fileTokens[fileTokens.length - 1];
                    } else {
                        filename = file.getAbsolutePath() + fileNumber;
                    }
                    System.out.println(filename);
                    File imageFile = new File(filename);
                    try {
                        ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", imageFile);
                        System.out.println(imageFile.getAbsolutePath());
                    } catch (IOException ex) {
                        TransactionResult t = new TransactionResult();
                        t.setResultNumber(-1);
                        t.setResult("Error Salvando Reporte");
                        t.setResultDescription(ex.getMessage());
                        setTransactionResult(t);
                        Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
                    }
                }
            } else if (selectedExtension.contains("*.html")) {
                try {
                    JasperExportManager.exportReportToHtmlFile(jasperPrint, file.getAbsolutePath());
                } catch (JRException ex) {
                    TransactionResult t = new TransactionResult();
                    t.setResultNumber(-1);
                    t.setResult("Error Salvando Reporte");
                    t.setResultDescription(ex.getMessage());
                    setTransactionResult(t);
                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
                }
            } else if (selectedExtension.contains("*.docx")) {
                JRDocxExporter exporter = new JRDocxExporter();
                exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRExporterParameter.OUTPUT_FILE_NAME, file.getAbsolutePath());
                try {
                    exporter.exportReport();
                } catch (JRException ex) {
                    TransactionResult t = new TransactionResult();
                    t.setResultNumber(-1);
                    t.setResult("Error Salvando Reporte");
                    t.setResultDescription(ex.getMessage());
                    setTransactionResult(t);
                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
                }
                System.out.println("docx");
            } else if (selectedExtension.contains("*.xlsx")) {
                JRXlsxExporter exporter = new JRXlsxExporter();
                exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
                exporter.setParameter(JRXlsExporterParameter.OUTPUT_FILE_NAME, file.getAbsolutePath());
                try {
                    exporter.exportReport();
                } catch (JRException ex) {
                    TransactionResult t = new TransactionResult();
                    t.setResultNumber(-1);
                    t.setResult("Error Salvando Reporte");
                    t.setResultDescription(ex.getMessage());
                    setTransactionResult(t);
                    Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
                }
                System.out.println("xlsx");
            }
        }
        return false;
    }

    private void imprimiendo() {
        try {
            JasperPrintManager.printReport(jasperPrint, true);
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }

    private WritableImage getImage(int pageNumber) {
        BufferedImage image = null;
        try {
            image = (BufferedImage) JasperPrintManager.printPageToImage(jasperPrint, pageNumber, 2);
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
        WritableImage fxImage = new WritableImage(jasperPrint.getPageWidth(), jasperPrint.getPageHeight());
        return SwingFXUtils.toFXImage(image, fxImage);

    }

    private void borderPanePage(int pageNumber) {
        imageView.setFitHeight(imageHeight * zoomFactor);
        imageView.setFitWidth(imageWidth * zoomFactor);
        imageView.setImage(getImage(pageNumber));
    }

    public void setTransactionResult(String result, String description,
            int resultNum) {
        TransactionResult t = new TransactionResult();
        t.setResult(result);
        t.setResultDescription(description);
        t.setResultNumber(resultNum);
        t.setTransactionTime(new Date());
        setTransactionResult(t);
    }

    public void setTransactionResult(TransactionResult t) {
        if (t != null) {
            if (t.getTransactionTime() == null) {
                resultPane.setText(t.getResult() + "  Time: " + new Date());
            } else {
                resultPane.setText(t.getResult() + "  Time: "
                        + t.getTransactionTime());
            }
            resultDescription.setText(t.getResultDescription());
            resultPane.setVisible(true);
            resultAccordion.setVisible(true);
        } else {
            resultPane.setText("Error Genérico" + "  Time: "
                    + new Date());
            resultDescription.setText("Sin retorno de Datos.");
            resultPane.setVisible(true);
            resultAccordion.setVisible(true);
        }
        if (t.getResultNumber() != 0 && !showingToast) {
            showingToast = true;
            errorLabel.setText(t.getResult());
            popup.show(parentStage);
            popup.setOpacity(1.0d);
            WarningToast task = new WarningToast();
            task.progressProperty().addListener(new ChangeListener<Number>() {
                @Override
                public void changed(
                        ObservableValue<? extends Number> observable,
                        Number oldValue, Number newValue) {
                    popup.setOpacity(newValue.doubleValue());
                    if (newValue.doubleValue() <= 0.01d) {
                        popup.hide();
                        showingToast = false;
                    }
                }

            });
//            popup.setX(borderPane.getScene().getWindow().getX()
//                    + borderPane.getScene().getWindow().getWidth() - 100);
//            popup.setY(borderPane.getScene().getWindow().getY());
//            new Thread(task).start();
        }
    }

    public void clearTransactionResult() {
        resultPane.setText("");
        resultDescription.setText("");
        resultPane.setVisible(false);
        resultAccordion.setVisible(false);
    }

    public JRViewerFXMLMode getPrintMode() {
        return printMode;
    }

    public void setPrintMode(JRViewerFXMLMode printMode) {
        this.printMode = printMode;
    }

    public String getReportFilename() {
        return reportFilename;
    }

    public void setReportFilename(String reportFilename) {
        this.reportFilename = reportFilename;
    }

    public JRDataSource getReportDataset() {
        return reportDataset;
    }

    public void setReportDataset(JRDataSource reportDataset) {
        this.reportDataset = reportDataset;
    }

    public Map getReportParameters() {
        return reportParameters;
    }

    public void setReportParameters(Map reportParameters) {
        this.reportParameters = reportParameters;
    }

//    public BorderPane getView() {
//        return borderPane;
//    }
//
//    public void setView(BorderPane borderPane) {
//        this.borderPane = borderPane;
//    }
    public void close() {
        parentStage.close();
    }

    public JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public void setJasperPrint(JasperPrint jasperPrint) {
        this.jasperPrint = jasperPrint;
    }
}
