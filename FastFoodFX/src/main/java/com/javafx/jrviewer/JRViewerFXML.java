package com.javafx.jrviewer;

import net.sf.jasperreports.engine.JasperPrint;

public class JRViewerFXML {

    private static JasperPrint jasperPrint;
    @SuppressWarnings("FieldMayBeFinal")
    private JRViewerFXMLMode printMode;
    private static double pW, pH;

    public JRViewerFXML(JasperPrint jasperPrint, JRViewerFXMLMode printMode, int w, int h) {
        JRViewerFXML.jasperPrint = jasperPrint;
        this.printMode = printMode;
        pW = w;
        pH = h;
    }

    public static JasperPrint getJasperPrint() {
        return jasperPrint;
    }

    public static double getpW() {
        return pW;
    }

    public static double getpH() {
        return pH;
    }

}
