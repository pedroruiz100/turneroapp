package com.javafx.screen;

import com.javafx.scope.ScreenScope;
import com.javafx.scope.ScreenScoped;
import com.javafx.util.StageSecond;
import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;
import javafx.util.Duration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

@Service
public class ScreensContoller implements ApplicationContextAware {

    @Autowired
    ScreenScope screenScope;
    private ApplicationContext applicationContext;
    private Stage stage;
    private String currentScreenId;
    private final Map<String, BaseScreenController> screens = Collections.synchronizedMap(new HashMap<String, BaseScreenController>());

    //mantener referencia del screen "actual"
    private static double width = 0;
    private static double height = 0;
    private static String fxml = "";
    //***************************************

    public void init(Stage stage) {
        this.setStage(stage);
        Group root = new Group();
        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/styles/Styles.css").toExternalForm());
        this.getStage().setScene(scene);
        getStage().setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                t.consume();
                Alert alert = new Alert(AlertType.WARNING, "PARA CERRAR SESIÓN, REALICE LOS PASOS ADECUADOS.", ButtonType.FINISH);
                alert.showAndWait();
                if (alert.getResult() == ButtonType.FINISH) {
                    alert.close();
                } else {
                    alert.close();
                }
            }
        });
    }

    //                    ||            [[Siguiente]]             ||              [[Actual]]               || [[reset campos -> Siguiente Form]]
    public void loadScreen(String fxml, double width, double height, String fxmlAnt, double wAnt, double hAnt, boolean screenScoped) {
        this.width = wAnt;
        this.height = hAnt;
        this.fxml = fxmlAnt;
        BaseScreenController oldScreenController = this.getCurrentController();
        try {
            Class controllerClass = FXMLUtils.getControllerClass(fxml);
            final BaseScreenController fxmlController = (BaseScreenController) applicationContext.getBean(controllerClass);
            if (this.screens.get(fxmlController.getScreenId()) == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
                loader.setControllerFactory(new Callback<Class<?>, Object>() {
                    @Override
                    public Object call(Class<?> aClass) {
                        return fxmlController;
                    }
                });
                Parent root = loader.load();//root
                fxmlController.setRoot(root);//scene
                this.screens.put(fxmlController.getScreenId(), fxmlController);//stage, mapeo que imita a stage...
            } else if (screenScoped) {
                //limpiar campos del formulario a navegar, formularios especiales...
                this.screens.remove(fxmlController.getScreenId());
                this.screenScope.remove(fxmlController.getScreenId());
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
                loader.setControllerFactory(new Callback<Class<?>, Object>() {
                    @Override
                    public Object call(Class<?> aClass) {
                        return fxmlController;
                    }
                });
                Parent root = loader.load();//root
                fxmlController.setRoot(root);//scene
                this.screens.put(fxmlController.getScreenId(), fxmlController);//stage, mapeo que imita a stage...
            }
            this.currentScreenId = fxmlController.getScreenId();
            swapScreen(getCurrentController().getRoot(),
                    (fxml.contentEquals("/vista/caja/FacturaDeVentaFXML.fxml") || fxml.contentEquals("/vista/cajamay/FacturaDeVentaMayFXML.fxml") || fxml.contentEquals("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml")
                    || fxml.contentEquals("/vista/stock/MatrizFXML.fxml") || fxml.contentEquals("/vista/stock/FacturaCompraFXML.fxml")));

            redimensionando(width, height);
            if (oldScreenController != null) {
                if (oldScreenController.getClass().isAnnotationPresent(ScreenScoped.class) /*|| screenScoped*/) {
                    this.screens.remove(oldScreenController.getScreenId());
                    this.screenScope.remove(oldScreenController.getScreenId());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void loadScreenModal(String fxml, double width, double height, String fxmlAnt, double wAnt, double hAnt, boolean screenScoped) {
        this.width = wAnt;
        this.height = hAnt;
        this.fxml = fxmlAnt;
        BaseScreenController oldScreenController = this.getCurrentController();
        try {
            Class controllerClass = FXMLUtils.getControllerClass(fxml);
            final BaseScreenController fxmlController = (BaseScreenController) applicationContext.getBean(controllerClass);
            if (this.screens.get(fxmlController.getScreenId()) == null) {
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
                loader.setControllerFactory(new Callback<Class<?>, Object>() {
                    @Override
                    public Object call(Class<?> aClass) {
                        return fxmlController;
                    }
                });
                Parent root = loader.load();//root
                fxmlController.setRoot(root);//scene
                this.screens.put(fxmlController.getScreenId(), fxmlController);//stage, mapeo que imita a stage...
            } else if (screenScoped) {
                //limpiar campos del formulario a navegar, formularios especiales...
                this.screens.remove(fxmlController.getScreenId());
                this.screenScope.remove(fxmlController.getScreenId());
                FXMLLoader loader = new FXMLLoader(getClass().getResource(fxml));
                loader.setControllerFactory(new Callback<Class<?>, Object>() {
                    @Override
                    public Object call(Class<?> aClass) {
                        return fxmlController;
                    }
                });
                Parent root = loader.load();//root
                fxmlController.setRoot(root);//scene
                this.screens.put(fxmlController.getScreenId(), fxmlController);//stage, mapeo que imita a stage...
            }
            this.currentScreenId = fxmlController.getScreenId();
            swapScreenFacturaVenta(getCurrentController().getRoot(), width, height);
            redimensionando(wAnt, hAnt);
            if (oldScreenController != null) {
                if (oldScreenController.getClass().isAnnotationPresent(ScreenScoped.class) /*|| screenScoped*/) {
                    this.screens.remove(oldScreenController.getScreenId());
                    this.screenScope.remove(oldScreenController.getScreenId());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void redimensionando(double width, double height) {
        //gracias a la ignorancia, se puede hacer un alambrado bien artesanal...
        if (this.currentScreenId.contentEquals("loginFXMLController")) {
            this.stage.setWidth(width - 48/*+ 15*/);
            this.stage.setHeight(height + 13/*+ 40*/);
        } else {
            this.stage.setWidth(width + 15);
            this.stage.setHeight(height + 40);
        }
        this.stage.centerOnScreen();
        this.stage.requestFocus();
    }

    private boolean swapScreen(final Parent root, boolean noFade) {
        final Group rootGroup = getScreenRoot();
        final DoubleProperty opacity = rootGroup.opacityProperty();
        if (!isScreenEmpty()) {
            if (noFade) {
                rootGroup.getChildren().remove(0);
                rootGroup.getChildren().add(0, root);
            } else {
                Timeline fade = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 1.0)),
                        new KeyFrame(new Duration(250), new EventHandler<ActionEvent>() {
                            @Override
                            public void handle(ActionEvent t) {
                                rootGroup.getChildren().remove(0);
                                rootGroup.getChildren().add(0, root);
                                Timeline fadeIn = new Timeline(
                                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                                        new KeyFrame(new Duration(350), new KeyValue(opacity, 1.0)));
                                fadeIn.play();
                            }
                        }, new KeyValue(opacity, 0.0)));
                fade.play();
            }
            return true;
        } else {
            if (noFade) {
                rootGroup.getChildren().add(0, root);
            } else {
                opacity.set(0.0);
                rootGroup.getChildren().add(0, root);
                Timeline fadeIn = new Timeline(
                        new KeyFrame(Duration.ZERO, new KeyValue(opacity, 0.0)),
                        new KeyFrame(new Duration(350), new KeyValue(opacity, 1.0)));
                fadeIn.play();
            }
        }
        if (!this.stage.isShowing()) {
            this.getStage().show();
        }
        return true;
    }

    private boolean swapScreenFacturaVenta(final Parent root, double width, double height) {
        if (!isScreenEmpty()) {
            StackPane secondaryLayout = new StackPane();
            secondaryLayout.getChildren().add(root);
            secondaryLayout.setAlignment(Pos.CENTER_LEFT);
            Scene secondScene = new Scene(secondaryLayout, width, height);

            Stage secondStage = new Stage();
            secondStage.initModality(Modality.APPLICATION_MODAL);
//                            secondStage.setTitle("Sistema de Facturación");
            secondStage.setScene(secondScene);
            StageSecond.setStageData(secondStage);

            secondStage.show();
            return true;
        }
        if (!this.stage.isShowing()) {
            this.getStage().show();
        }
        return true;
    }

    private Group getScreenRoot() {
        return (Group) this.getStage().getScene().getRoot();
    }

    private boolean isScreenEmpty() {
        return getScreenRoot().getChildren().isEmpty();
    }

    public BaseScreenController getCurrentController() {
        return screens.get(getCurrentScreenId());
    }

    public String getCurrentScreenId() {
        return currentScreenId;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public static double getWidth() {
        return width;
    }

    public static double getHeight() {
        return height;
    }

    public static String getFxml() {
        return fxml;
    }
}
