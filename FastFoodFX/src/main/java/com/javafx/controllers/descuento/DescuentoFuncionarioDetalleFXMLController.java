/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class DescuentoFuncionarioDetalleFXMLController extends BaseScreenController implements Initializable {

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private SplitPane splitPane;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelDescFuncionarioImg;
    @FXML
    private Label labelDescFuncionarioNf;
    @FXML
    private AnchorPane anchorPaneBody;
    @FXML
    private TreeView<String> treeViewSecciones;
    @FXML
    private Label labelPorcentaje;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        String arrayPunto[] = DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("porcDesc").toString().split("\\.");
        String porc = arrayPunto[0] + "% de descuento";
        labelPorcentaje.setText(porc);
        switch (Integer.valueOf(DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf").toString())) {
            case 1:
                JSONObject jsonNf1 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf1Tipo");
                labelDescFuncionarioNf.setText(jsonNf1.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
                cargandoNf1Detalle();
                break;
            case 2:
                JSONObject jsonNf2 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf2Sfamilia");
                labelDescFuncionarioNf.setText(jsonNf2.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png"))));
                cargandoNf2Detalle();
                break;
            case 3:
                JSONObject jsonNf3 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf3Sseccion");
                labelDescFuncionarioNf.setText(jsonNf3.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png"))));
                cargandoNf3Detalle();
                break;
            case 4:
                JSONObject jsonNf4 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf4Seccion1");
                labelDescFuncionarioNf.setText(jsonNf4.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png"))));
                cargandoNf4Detalle();
                break;
            case 5:
                JSONObject jsonNf5 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf5Seccion2");
                labelDescFuncionarioNf.setText(jsonNf5.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png"))));
                cargandoNf5Detalle();
                break;
            case 6:
                JSONObject jsonNf6 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf6Secnom6");
                labelDescFuncionarioNf.setText(jsonNf6.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png"))));
                cargandoNf6Detalle();
                break;
            case 7:
                JSONObject jsonNf7 = (JSONObject) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nf7Secnom7");
                labelDescFuncionarioNf.setText(jsonNf7.get("descripcion").toString());
                labelDescFuncionarioImg.setGraphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png"))));
                cargandoNf7Detalle();
                break;
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/descuento/DescuentoFuncionarioFXML.fxml", 1157, 827, "/vista/descuento/DescuentoFuncionarioDetalleFXML.fxml", 441, 743, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoNf1Detalle() {
        Nf1Tipo nf1Tipo = (Nf1Tipo) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
            ImageView imageViewNf2Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf2 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
            HBox hBoxButtonsNf2 = new HBox();
            hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
            hBoxButtonsNf2.getChildren().add(imageViewNf2);
            hBoxButtonsNf2.setSpacing(6);
            TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), hBoxButtonsNf2);
            for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                ImageView imageViewNf3Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf3 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                HBox hBoxButtonsNf3 = new HBox();
                hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
                hBoxButtonsNf3.getChildren().add(imageViewNf3);
                hBoxButtonsNf3.setSpacing(6);
                TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
                for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                    ImageView imageViewNf4Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf4 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                    HBox hBoxButtonsNf4 = new HBox();
                    hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
                    hBoxButtonsNf4.getChildren().add(imageViewNf4);
                    hBoxButtonsNf4.setSpacing(6);
                    TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
                    for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                        ImageView imageViewNf5Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf5 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                        HBox hBoxButtonsNf5 = new HBox();
                        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                        hBoxButtonsNf5.getChildren().add(imageViewNf5);
                        hBoxButtonsNf5.setSpacing(6);
                        TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                        for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                            ImageView imageViewNf6Check = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                            ImageView imageViewNf6 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                            HBox hBoxButtonsNf6 = new HBox();
                            hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                            hBoxButtonsNf6.getChildren().add(imageViewNf6);
                            hBoxButtonsNf6.setSpacing(6);
                            TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                            for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                ImageView imageViewNf7Check = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                                ImageView imageViewNf7 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                HBox hBoxButtonsNf7 = new HBox();
                                hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                                hBoxButtonsNf7.getChildren().add(imageViewNf7);
                                hBoxButtonsNf7.setSpacing(6);
                                TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                                treeItemNf6.getChildren().add(treeItemNf7);
                            }
                            treeItemNf5.getChildren().add(treeItemNf6);
                        }
                        treeItemNf4.getChildren().add(treeItemNf5);
                    }
                    treeItemNf3.getChildren().add(treeItemNf4);
                }
                treeItemNf2.getChildren().add(treeItemNf3);
            }
            treeItemNf1.getChildren().add(treeItemNf2);
        }
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf2Detalle() {
        Nf2Sfamilia nf2Sfamilia = (Nf2Sfamilia) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf2Sfamilia.getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
            ImageView imageViewNf3Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf3 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
            HBox hBoxButtonsNf3 = new HBox();
            hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
            hBoxButtonsNf3.getChildren().add(imageViewNf3);
            hBoxButtonsNf3.setSpacing(6);
            TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
            for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                ImageView imageViewNf4Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf4 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                HBox hBoxButtonsNf4 = new HBox();
                hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
                hBoxButtonsNf4.getChildren().add(imageViewNf4);
                hBoxButtonsNf4.setSpacing(6);
                TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
                for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                    ImageView imageViewNf5Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf5 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                    HBox hBoxButtonsNf5 = new HBox();
                    hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                    hBoxButtonsNf5.getChildren().add(imageViewNf5);
                    hBoxButtonsNf5.setSpacing(6);
                    TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                    for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                        ImageView imageViewNf6Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf6 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                        HBox hBoxButtonsNf6 = new HBox();
                        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                        hBoxButtonsNf6.getChildren().add(imageViewNf6);
                        hBoxButtonsNf6.setSpacing(6);
                        TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                        for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                            ImageView imageViewNf7Check = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                            ImageView imageViewNf7 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                            HBox hBoxButtonsNf7 = new HBox();
                            hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                            hBoxButtonsNf7.getChildren().add(imageViewNf7);
                            hBoxButtonsNf7.setSpacing(6);
                            TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                            treeItemNf6.getChildren().add(treeItemNf7);
                        }
                        treeItemNf5.getChildren().add(treeItemNf6);
                    }
                    treeItemNf4.getChildren().add(treeItemNf5);
                }
                treeItemNf3.getChildren().add(treeItemNf4);
            }
            treeItemNf2.getChildren().add(treeItemNf3);
        }
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf3Detalle() {
        Nf3Sseccion nf3Sseccion = (Nf3Sseccion) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf3Sseccion.getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf3Sseccion.getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
            ImageView imageViewNf4Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf4 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
            HBox hBoxButtonsNf4 = new HBox();
            hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
            hBoxButtonsNf4.getChildren().add(imageViewNf4);
            hBoxButtonsNf4.setSpacing(6);
            TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
            for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                ImageView imageViewNf5Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf5 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                HBox hBoxButtonsNf5 = new HBox();
                hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                hBoxButtonsNf5.getChildren().add(imageViewNf5);
                hBoxButtonsNf5.setSpacing(6);
                TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                    ImageView imageViewNf6Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf6 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                    HBox hBoxButtonsNf6 = new HBox();
                    hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                    hBoxButtonsNf6.getChildren().add(imageViewNf6);
                    hBoxButtonsNf6.setSpacing(6);
                    TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                    for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                        ImageView imageViewNf7Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf7 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                        HBox hBoxButtonsNf7 = new HBox();
                        hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                        hBoxButtonsNf7.getChildren().add(imageViewNf7);
                        hBoxButtonsNf7.setSpacing(6);
                        TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                        treeItemNf6.getChildren().add(treeItemNf7);
                    }
                    treeItemNf5.getChildren().add(treeItemNf6);
                }
                treeItemNf4.getChildren().add(treeItemNf5);
            }
            treeItemNf3.getChildren().add(treeItemNf4);
        }
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf4Detalle() {
        Nf4Seccion1 nf4Seccion1 = (Nf4Seccion1) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
            ImageView imageViewNf5Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf5 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
            HBox hBoxButtonsNf5 = new HBox();
            hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
            hBoxButtonsNf5.getChildren().add(imageViewNf5);
            hBoxButtonsNf5.setSpacing(6);
            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                ImageView imageViewNf6Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf6 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                HBox hBoxButtonsNf6 = new HBox();
                hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                hBoxButtonsNf6.getChildren().add(imageViewNf6);
                hBoxButtonsNf6.setSpacing(6);
                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                    ImageView imageViewNf7Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf7 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                    HBox hBoxButtonsNf7 = new HBox();
                    hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                    hBoxButtonsNf7.getChildren().add(imageViewNf7);
                    hBoxButtonsNf7.setSpacing(6);
                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                    treeItemNf6.getChildren().add(treeItemNf7);
                }
                treeItemNf5.getChildren().add(treeItemNf6);
            }
            treeItemNf4.getChildren().add(treeItemNf5);
        }
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf5Detalle() {
        Nf5Seccion2 nf5Seccion2 = (Nf5Seccion2) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
            ImageView imageViewNf6Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf6 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
            HBox hBoxButtonsNf6 = new HBox();
            hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
            hBoxButtonsNf6.getChildren().add(imageViewNf6);
            hBoxButtonsNf6.setSpacing(6);
            TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
            for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                ImageView imageViewNf7Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf7 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                HBox hBoxButtonsNf7 = new HBox();
                hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                hBoxButtonsNf7.getChildren().add(imageViewNf7);
                hBoxButtonsNf7.setSpacing(6);
                TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                treeItemNf6.getChildren().add(treeItemNf7);
            }
            treeItemNf5.getChildren().add(treeItemNf6);
        }
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf6Detalle() {
        Nf6Secnom6 nf6Secnom6 = (Nf6Secnom6) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        ImageView imageViewNf6Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf6 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
        HBox hBoxButtonsNf6 = new HBox();
        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
        hBoxButtonsNf6.getChildren().add(imageViewNf6);
        hBoxButtonsNf6.setSpacing(6);
        TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
        treeItemNf6.setExpanded(true);
        for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
            ImageView imageViewNf7Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf7 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
            HBox hBoxButtonsNf7 = new HBox();
            hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
            hBoxButtonsNf7.getChildren().add(imageViewNf7);
            hBoxButtonsNf7.setSpacing(6);
            TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
            treeItemNf6.getChildren().add(treeItemNf7);
        }
        treeItemNf5.getChildren().add(treeItemNf6);
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }

    private void cargandoNf7Detalle() {
        Nf7Secnom7 nf7Secnom7 = (Nf7Secnom7) DescuentoFuncionarioFXMLController.getJsonCabDetalleNf().get("nfChilds");
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        ImageView imageViewNf6Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf6 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
        HBox hBoxButtonsNf6 = new HBox();
        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
        hBoxButtonsNf6.getChildren().add(imageViewNf6);
        hBoxButtonsNf6.setSpacing(6);
        TreeItem<String> treeItemNf6 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getDescripcion(), hBoxButtonsNf6);
        treeItemNf6.setExpanded(true);
        ImageView imageViewNf7Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf7 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
        HBox hBoxButtonsNf7 = new HBox();
        hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
        hBoxButtonsNf7.getChildren().add(imageViewNf7);
        hBoxButtonsNf7.setSpacing(6);
        TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
        treeItemNf6.getChildren().add(treeItemNf7);
        treeItemNf5.getChildren().add(treeItemNf6);
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewSecciones.setRoot(treeItemNf1);
    }
}
