package com.javafx.controllers.descuento;

import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
public class MenuConfiguracionFXMLController extends BaseScreenController implements Initializable {

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Label labelTarjClienteFiel;
    @FXML
    private Label labelTarjConvenio;
    @FXML
    private Label labelTarjCreDeb;
    @FXML
    private Label labelFuncionario;
    @FXML
    private Button buttonTarjClienteFiel;
    @FXML
    private Button buttonTarjetaConvenio;
    @FXML
    private Button buttonTarjetas;
    @FXML
    private Button buttonFuncionario;
    @FXML
    private Button buttonPromTemp;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneConfDesc;
    @FXML
    private TextFlow textFlowTarj;
    @FXML
    private Label labelTarjCreDeb1;
    @FXML
    private Button buttonPromTempArt;
    @FXML
    private TextFlow textFlowPromTempArt;
    @FXML
    private Label labelPromTempArt;
    @FXML
    private Label labelPromTempArtDos;
    @FXML
    private TextFlow textFlowPromTempSecc;
    @FXML
    private Label labelPromTempSecc;
    @FXML
    private Label labelPromTempSeccDos;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonTarjClienteFielAction(ActionEvent event) {
        configurandoTarjetaFielDes();
    }

    @FXML
    private void buttonTarjetaConvenioAction(ActionEvent event) {
        configurandoTarjetasConvenioDes();
    }

    @FXML
    private void buttonTarjetasAction(ActionEvent event) {
        configurandoTarjetasDes();
    }

    @FXML
    private void buttonFuncionarioAction(ActionEvent event) {
        configurandoFuncionarioDes();
    }

    @FXML
    private void buttonPromTempAction(ActionEvent event) {
        configurandoPromTemp();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneConfDescKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonPromTempArtAction(ActionEvent event) {
        configurandoPromTempArt();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, false);
    }

    private void configurandoTarjetaFielDes() {
//        if ("config_tarj_cliente_fiel_desc")) {
        this.sc.loadScreen("/vista/descuento/DescuentoClienteFielFXML.fxml", 1263, 707, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }

    private void configurandoFuncionarioDes() {
//        if ("config_func_desc")) {
        this.sc.loadScreen("/vista/descuento/DescuentoFuncionarioFXML.fxml", 1157, 832, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }

    private void configurandoTarjetasConvenioDes() {
//        if ("config_tarj_conv_desc")) {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaConvenioFXML.fxml", 867, 707, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }

    private void configurandoPromTemp() {
//        if ("config_prom_temp_desc")) {
        this.sc.loadScreen("/vista/descuento/PromocionTemporadaFXML.fxml", 982, 875, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }

    private void configurandoPromTempArt() {
//        if ("config_prom_temp_desc")) {//mismo permiso sección / artículos
        this.sc.loadScreen("/vista/descuento/PromocionTemporadaArtFXML.fxml", 982, 875, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }

    private void configurandoTarjetasDes() {
//        if ("config_tarj_desc")) {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, true);
//        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            configurandoTarjetaFielDes();
        }
        if (keyCode == event.getCode().F2) {
            configurandoTarjetasConvenioDes();
        }
        if (keyCode == event.getCode().F3) {
            configurandoPromTemp();
        }
        if (keyCode == event.getCode().F4) {
            configurandoTarjetasDes();
        }
        if (keyCode == event.getCode().F5) {
            configurandoFuncionarioDes();
        }
        if (keyCode == event.getCode().F6) {
            configurandoPromTempArt();
        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

}
