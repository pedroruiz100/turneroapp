/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.NumberValidator;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class DescuentoTarjetaDiaEspecialFXMLController extends BaseScreenController implements Initializable {

    //TABLE VIEW
    private List<JSONObject> tarjCabArt;
    private ObservableList<JSONObject> detalleData;
    //TABLE VIEW

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Label labelPrimerDia;
    @FXML
    private CheckBox checkBoxDomingoPrimero;
    @FXML
    private CheckBox checkBoxLunesPrimero;
    @FXML
    private CheckBox checkBoxMartesPrimero;
    @FXML
    private CheckBox checkBoxMiercolesPrimero;
    @FXML
    private CheckBox checkBoxJuevesPrimero;
    @FXML
    private CheckBox checkBoxViernesPrimero;
    @FXML
    private CheckBox checkBoxSabadoPrimero;
    @FXML
    private Label lableUltimoDia;
    @FXML
    private CheckBox checkBoxDomingoUltimo;
    @FXML
    private CheckBox checkBoxLunesUltimo;
    @FXML
    private CheckBox checkBoxMartesUltimo;
    @FXML
    private CheckBox checkBoxMiercolesUltimo;
    @FXML
    private CheckBox checkBoxJuevesUltimo;
    @FXML
    private CheckBox checkBoxViernesUltimo;
    @FXML
    private CheckBox checkBoxSabadoUltimo;
    @FXML
    private Label labelTitulo;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneDiaEspecial;
    @FXML
    private AnchorPane anchorPaneBody;
    @FXML
    private TableView<JSONObject> tableViewArticuloTemp;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodArt;
    @FXML
    private TableColumn<JSONObject, String> tableColumnArticulo;
    @FXML
    private HBox hBoxEntidad;
    @FXML
    private Label labelEntidadNombre;
    @FXML
    private Label labelEntidadPorc;
    @FXML
    private Label labelEmpresaNombre;
    @FXML
    private Label labelEmpresaPorc;
    @FXML
    private CheckBox checkBoxRetorno;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelTarjDetalle;
    @FXML
    private SplitPane splitPaneHeader;
    @FXML
    private AnchorPane anchorPaneHead;
    @FXML
    private Label labelTodosArt;
    @FXML
    private RadioButton radioButtonCaja;
    @FXML
    private RadioButton radioButtonExtracto;
    @FXML
    private Label labelMontoMin;
    @FXML
    private Label labelDtoTarjSec1;
    @FXML
    private SplitPane splitPane;
    @FXML
    private AnchorPane anchorPaneHeaderSec;
    @FXML
    private Label labelDtoTarjSec;
    @FXML
    private AnchorPane anchorPaneBodySec;
    @FXML
    private Accordion accordionNf;
    @FXML
    private TitledPane titledPaneNf1;
    @FXML
    private ScrollPane scrollPaneNf1;
    @FXML
    private HBox hBoxNf1;
    @FXML
    private TitledPane titledPaneNf2;
    @FXML
    private ScrollPane scrollPaneNf2;
    @FXML
    private HBox hBoxNf2;
    @FXML
    private TitledPane titledPaneNf3;
    @FXML
    private ScrollPane scrollPaneNf3;
    @FXML
    private HBox hBoxNf3;
    @FXML
    private TitledPane titledPaneNf4;
    @FXML
    private ScrollPane scrollPaneNf4;
    @FXML
    private HBox hBoxNf4;
    @FXML
    private TitledPane titledPaneNf5;
    @FXML
    private ScrollPane scrollPaneNf5;
    @FXML
    private HBox hBoxNf5;
    @FXML
    private TitledPane titledPaneNf6;
    @FXML
    private ScrollPane scrollPaneNf6;
    @FXML
    private HBox hBoxNf6;
    @FXML
    private TitledPane titledPaneNf7;
    @FXML
    private ScrollPane scrollPaneNf7;
    @FXML
    private HBox hBoxNf7;
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab tabSecciones;
    @FXML
    private Tab tabArticulos;
    @FXML
    private Tab tabPrimerUltimoDia;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneDiaEspecialKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        checkInicial();
        asignandoValores();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, "/vista/descuento/DescuentoTarjetaDiaEspecialFXML.fxml", 1286, 818, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void asignandoValores() {
        labelTitulo.setText(String.valueOf(DescuentoTarjetaFXMLController.getTarjCab().get("descriTarjeta")).toUpperCase());
        for (int i = 0; i < DescuentoTarjetaFXMLController.getTarjDetJSONObjList().size(); i++) {
            if (DescuentoTarjetaFXMLController.getTarjDetJSONObjList().get(i).get("diaEspecial") != null) {
                JSONObject jsonDia = (JSONObject) DescuentoTarjetaFXMLController.getTarjDetJSONObjList().get(i).get("dia");
                if ((Boolean) DescuentoTarjetaFXMLController.getTarjDetJSONObjList().get(i).get("diaEspecial")) {
                    primerJSONObjectTarjDet((long) jsonDia.get("idDia"));
                } else {
                    ultimoJSONObjectTarjDet((long) jsonDia.get("idDia"));
                }
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().containsKey("descuentoTarjetaCabEntidads")) {
            JSONArray jsonArrayTarjetaCabEntidads = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabEntidads");
            for (Object objectTarjetaCabEntidads : jsonArrayTarjetaCabEntidads) {
                JSONObject jsonTarjetaCabEntidads = (JSONObject) objectTarjetaCabEntidads;
                JSONObject jsonEntidad = (JSONObject) jsonTarjetaCabEntidads.get("entidad");
                labelEntidadNombre.setText(jsonEntidad.get("descripcion").toString().toUpperCase());
                labelEntidadPorc.setText(jsonTarjetaCabEntidads.get("porcentajeEntidad") + " %");
                checkBoxRetorno.setSelected((Boolean) jsonTarjetaCabEntidads.get("retorno"));
            }
        } else {
            labelEntidadNombre.setText("  N/A");
            labelEntidadPorc.setText("0 %");
        }
        labelEmpresaNombre.setText("CASA PARANÁ");
        labelEmpresaPorc.setText(DescuentoTarjetaFXMLController.getTarjCab().get("porcentajeParana") + " %");
        checkRetorno();
        if (DescuentoTarjetaFXMLController.getTarjCab().containsKey("descuentoTarjetaCabArticulos")) {
            labelTodosArt.setVisible(false);
            tarjCabArt = new ArrayList<>();
            JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabArticulos");
            for (int i = 0; i < jsonArrayDescuentoTarjetaCabArticulos.size(); i++) {
                JSONObject jsonDescuentoTarjetaCabArticulo = (JSONObject) jsonArrayDescuentoTarjetaCabArticulos.get(i);
                JSONObject jsonArticulo = (JSONObject) jsonDescuentoTarjetaCabArticulo.get("articulo");
                tarjCabArt.add(jsonArticulo);
            }
            vistaJSONObjectTarjArt();
        } else {
            labelTodosArt.setVisible(true);
        }
        if (Integer.valueOf(DescuentoTarjetaFXMLController.getTarjCab().get("montoMin").toString()) != -1) {
            NumberValidator num = new NumberValidator();
            labelMontoMin.setText("Monto mínimo para acceder al descuento: "
                    + num.numberFormat("Gs ###,###.###", Double.parseDouble(DescuentoTarjetaFXMLController.getTarjCab().get("montoMin").toString())));
        }
        radioButtonExtracto.setSelected((Boolean) DescuentoTarjetaFXMLController.getTarjCab().get("extracto"));
        if (radioButtonExtracto.isSelected()) {
            radioButtonCaja.setSelected(false);
        } else {
            radioButtonCaja.setSelected(true);
        }
        radioButtonExtracto();
        gestionandoSecciones();
    }

    private void checkInicial() {
        checkBoxDomingoPrimero.setSelected(false);
        checkBoxDomingoPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxLunesPrimero.setSelected(false);
        checkBoxLunesPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxMartesPrimero.setSelected(false);
        checkBoxMartesPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxMiercolesPrimero.setSelected(false);
        checkBoxMiercolesPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxJuevesPrimero.setSelected(false);
        checkBoxJuevesPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxViernesPrimero.setSelected(false);
        checkBoxViernesPrimero.setEffect(new DropShadow(10, Color.RED));
        checkBoxSabadoPrimero.setSelected(false);
        checkBoxSabadoPrimero.setEffect(new DropShadow(10, Color.RED));
        //
        checkBoxDomingoUltimo.setSelected(false);
        checkBoxDomingoUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxLunesUltimo.setSelected(false);
        checkBoxLunesUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxMartesUltimo.setSelected(false);
        checkBoxMartesUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxMiercolesUltimo.setSelected(false);
        checkBoxMiercolesUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxJuevesUltimo.setSelected(false);
        checkBoxJuevesUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxViernesUltimo.setSelected(false);
        checkBoxViernesUltimo.setEffect(new DropShadow(10, Color.RED));
        checkBoxSabadoUltimo.setSelected(false);
        checkBoxSabadoUltimo.setEffect(new DropShadow(10, Color.RED));
        //
    }

    private void primerJSONObjectTarjDet(long idDia) {
        switch ((int) idDia) {
            case 1:
                checkBoxDomingoPrimero.setSelected(true);
                checkBoxDomingoPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 2:
                checkBoxLunesPrimero.setSelected(true);
                checkBoxLunesPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 3:
                checkBoxMartesPrimero.setSelected(true);
                checkBoxMartesPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 4:
                checkBoxMiercolesPrimero.setSelected(true);
                checkBoxMiercolesPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 5:
                checkBoxJuevesPrimero.setSelected(true);
                checkBoxJuevesPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 6:
                checkBoxViernesPrimero.setSelected(true);
                checkBoxViernesPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 7:
                checkBoxSabadoPrimero.setSelected(true);
                checkBoxSabadoPrimero.setEffect(new DropShadow(10, Color.GREEN));
                break;
            default:
                break;
        }
    }

    private void ultimoJSONObjectTarjDet(long idDia) {
        switch ((int) idDia) {
            case 1:
                checkBoxDomingoUltimo.setSelected(true);
                checkBoxDomingoUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 2:
                checkBoxLunesUltimo.setSelected(true);
                checkBoxLunesUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 3:
                checkBoxMartesUltimo.setSelected(true);
                checkBoxMartesUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 4:
                checkBoxMiercolesUltimo.setSelected(true);
                checkBoxMiercolesUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 5:
                checkBoxJuevesUltimo.setSelected(true);
                checkBoxJuevesUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 6:
                checkBoxViernesUltimo.setSelected(true);
                checkBoxViernesUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            case 7:
                checkBoxSabadoUltimo.setSelected(true);
                checkBoxSabadoUltimo.setEffect(new DropShadow(10, Color.GREEN));
                break;
            default:
                break;
        }
    }

    private void radioButtonExtracto() {
        if (radioButtonCaja.isSelected()) {
            radioButtonExtracto.setEffect(null);
            radioButtonCaja.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            radioButtonExtracto.setEffect(new DropShadow(10, Color.GREEN));
            radioButtonCaja.setEffect(null);
        }
    }

    private void checkRetorno() {
        if (checkBoxRetorno.isSelected()) {
            checkBoxRetorno.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            checkBoxRetorno.setText("Sin retorno");
            checkBoxRetorno.setEffect(new DropShadow(10, Color.ORANGERED));
        }
    }

    private void gestionandoSecciones() {
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf1s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf1s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf1s");
            for (Object objectDescuentoTarjetaCabNf1 : jsonArrayDescuentoTarjetaCabNf1s) {
                JSONObject jsonDescuentoTarjetaCabNf1 = (JSONObject) objectDescuentoTarjetaCabNf1;
                JSONObject jsonNf1Tipo = (JSONObject) jsonDescuentoTarjetaCabNf1.get("nf1Tipo");
                Nf1Tipo nf1Tipo = (Nf1Tipo) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("1->" + jsonNf1Tipo.get("idNf1Tipo").toString());
                VBox vBoxNf1 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf1Tipo.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf1 = cargandoNf1Detalle(nf1Tipo);
                treeViewNf1.setMaxWidth(Double.MAX_VALUE);
                treeViewNf1.setMaxHeight(Double.MAX_VALUE);
                vBoxNf1.getChildren().add(labelDescripcion);
                vBoxNf1.getChildren().add(hBox);
                vBoxNf1.getChildren().add(treeViewNf1);
                vBoxNf1.setSpacing(6);
                vBoxNf1.setMaxWidth(Double.MAX_VALUE);
                vBoxNf1.setMaxHeight(Double.MAX_VALUE);
                vBoxNf1.setAlignment(Pos.CENTER);
                vBoxNf1.setStyle("-fx-background-color: #ededed;");
                hBoxNf1.getChildren().add(vBoxNf1);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf2s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf2s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf2s");
            for (Object objectDescuentoTarjetaCabNf2 : jsonArrayDescuentoTarjetaCabNf2s) {
                JSONObject jsonDescuentoTarjetaCabNf2 = (JSONObject) objectDescuentoTarjetaCabNf2;
                JSONObject jsonNf2Sfamilia = (JSONObject) jsonDescuentoTarjetaCabNf2.get("nf2Sfamilia");
                Nf2Sfamilia nf2Sfamilia = (Nf2Sfamilia) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("2->" + jsonNf2Sfamilia.get("idNf2Sfamilia").toString());
                VBox vBoxNf2 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf2Sfamilia.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf2 = cargandoNf2Detalle(nf2Sfamilia);
                treeViewNf2.setMaxWidth(Double.MAX_VALUE);
                treeViewNf2.setMaxHeight(Double.MAX_VALUE);
                vBoxNf2.getChildren().add(labelDescripcion);
                vBoxNf2.getChildren().add(hBox);
                vBoxNf2.getChildren().add(treeViewNf2);
                vBoxNf2.setSpacing(6);
                vBoxNf2.setMaxWidth(Double.MAX_VALUE);
                vBoxNf2.setMaxHeight(Double.MAX_VALUE);
                vBoxNf2.setAlignment(Pos.CENTER);
                vBoxNf2.setStyle("-fx-background-color: #ededed;");
                hBoxNf2.getChildren().add(vBoxNf2);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf3s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf3s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf3s");
            for (Object objectDescuentoTarjetaCabNf3 : jsonArrayDescuentoTarjetaCabNf3s) {
                JSONObject jsonDescuentoTarjetaCabNf3 = (JSONObject) objectDescuentoTarjetaCabNf3;
                JSONObject jsonNf3Sseccion = (JSONObject) jsonDescuentoTarjetaCabNf3.get("nf3Sseccion");
                Nf3Sseccion nf3Sseccion = (Nf3Sseccion) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("3->" + jsonNf3Sseccion.get("idNf3Sseccion").toString());
                VBox vBoxNf3 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf3Sseccion.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf3 = cargandoNf3Detalle(nf3Sseccion);
                treeViewNf3.setMaxWidth(Double.MAX_VALUE);
                treeViewNf3.setMaxHeight(Double.MAX_VALUE);
                vBoxNf3.getChildren().add(labelDescripcion);
                vBoxNf3.getChildren().add(hBox);
                vBoxNf3.getChildren().add(treeViewNf3);
                vBoxNf3.setSpacing(6);
                vBoxNf3.setMaxWidth(Double.MAX_VALUE);
                vBoxNf3.setMaxHeight(Double.MAX_VALUE);
                vBoxNf3.setAlignment(Pos.CENTER);
                vBoxNf3.setStyle("-fx-background-color: #ededed;");
                hBoxNf3.getChildren().add(vBoxNf3);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf4s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf4s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf4s");
            for (Object objectDescuentoTarjetaCabNf4 : jsonArrayDescuentoTarjetaCabNf4s) {
                JSONObject jsonDescuentoTarjetaCabNf4 = (JSONObject) objectDescuentoTarjetaCabNf4;
                JSONObject jsonNf4Seccion1 = (JSONObject) jsonDescuentoTarjetaCabNf4.get("nf4Seccion1");
                Nf4Seccion1 nf4Seccion1 = (Nf4Seccion1) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("4->" + jsonNf4Seccion1.get("idNf4Seccion1").toString());
                VBox vBoxNf4 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf4Seccion1.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf4 = cargandoNf4Detalle(nf4Seccion1);
                treeViewNf4.setMaxWidth(Double.MAX_VALUE);
                treeViewNf4.setMaxHeight(Double.MAX_VALUE);
                vBoxNf4.getChildren().add(labelDescripcion);
                vBoxNf4.getChildren().add(hBox);
                vBoxNf4.getChildren().add(treeViewNf4);
                vBoxNf4.setSpacing(6);
                vBoxNf4.setMaxWidth(Double.MAX_VALUE);
                vBoxNf4.setMaxHeight(Double.MAX_VALUE);
                vBoxNf4.setAlignment(Pos.CENTER);
                vBoxNf4.setStyle("-fx-background-color: #ededed;");
                hBoxNf4.getChildren().add(vBoxNf4);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf5s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf5s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf5s");
            for (Object objectDescuentoTarjetaCabNf5 : jsonArrayDescuentoTarjetaCabNf5s) {
                JSONObject jsonDescuentoTarjetaCabNf5 = (JSONObject) objectDescuentoTarjetaCabNf5;
                JSONObject jsonNf5Seccion2 = (JSONObject) jsonDescuentoTarjetaCabNf5.get("nf5Seccion2");
                Nf5Seccion2 nf5Seccion2 = (Nf5Seccion2) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("5->" + jsonNf5Seccion2.get("idNf5Seccion2").toString());
                VBox vBoxNf5 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf5Seccion2.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf5 = cargandoNf5Detalle(nf5Seccion2);
                treeViewNf5.setMaxWidth(Double.MAX_VALUE);
                treeViewNf5.setMaxHeight(Double.MAX_VALUE);
                vBoxNf5.getChildren().add(labelDescripcion);
                vBoxNf5.getChildren().add(hBox);
                vBoxNf5.getChildren().add(treeViewNf5);
                vBoxNf5.setSpacing(6);
                vBoxNf5.setMaxWidth(Double.MAX_VALUE);
                vBoxNf5.setMaxHeight(Double.MAX_VALUE);
                vBoxNf5.setAlignment(Pos.CENTER);
                vBoxNf5.setStyle("-fx-background-color: #ededed;");
                hBoxNf5.getChildren().add(vBoxNf5);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf6s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf6s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf6s");
            for (Object objectDescuentoTarjetaCabNf6 : jsonArrayDescuentoTarjetaCabNf6s) {
                JSONObject jsonDescuentoTarjetaCabNf6 = (JSONObject) objectDescuentoTarjetaCabNf6;
                JSONObject jsonNf6Secnom6 = (JSONObject) jsonDescuentoTarjetaCabNf6.get("nf6Secnom6");
                Nf6Secnom6 nf6Secnom6 = (Nf6Secnom6) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("6->" + jsonNf6Secnom6.get("idNf6Secnom6").toString());
                VBox vBoxNf6 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf6Secnom6.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf6 = cargandoNf6Detalle(nf6Secnom6);
                treeViewNf6.setMaxWidth(Double.MAX_VALUE);
                treeViewNf6.setMaxHeight(Double.MAX_VALUE);
                vBoxNf6.getChildren().add(labelDescripcion);
                vBoxNf6.getChildren().add(hBox);
                vBoxNf6.getChildren().add(treeViewNf6);
                vBoxNf6.setSpacing(6);
                vBoxNf6.setMaxWidth(Double.MAX_VALUE);
                vBoxNf6.setMaxHeight(Double.MAX_VALUE);
                vBoxNf6.setAlignment(Pos.CENTER);
                vBoxNf6.setStyle("-fx-background-color: #ededed;");
                hBoxNf6.getChildren().add(vBoxNf6);
            }
        }
        if (DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf7s") != null) {
            JSONArray jsonArrayDescuentoTarjetaCabNf7s = (JSONArray) DescuentoTarjetaFXMLController.getTarjCab().get("descuentoTarjetaCabNf7s");
            for (Object objectDescuentoTarjetaCabNf7 : jsonArrayDescuentoTarjetaCabNf7s) {
                JSONObject jsonDescuentoTarjetaCabNf7 = (JSONObject) objectDescuentoTarjetaCabNf7;
                JSONObject jsonNf7Secnom7 = (JSONObject) jsonDescuentoTarjetaCabNf7.get("nf7Secnom7");
                Nf7Secnom7 nf7Secnom7 = (Nf7Secnom7) DescuentoTarjetaFXMLController.getHashMapFormularioDetalleChilds().get("7->" + jsonNf7Secnom7.get("idNf7Secnom7").toString());
                VBox vBoxNf7 = new VBox();
                Label labelDescripcion = new Label();
                labelDescripcion.setText(jsonNf7Secnom7.get("descripcion").toString());
                labelDescripcion.setStyle("-fx-text-fill: black; "
                        + "-fx-font-size: 12px; "
                        + "-fx-font-weight: bold;");
                HBox hBox = new HBox();
                hBox.getChildren().add(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                hBox.setSpacing(6);
                hBox.alignmentProperty().set(Pos.CENTER);
                TreeView<String> treeViewNf7 = cargandoNf7Detalle(nf7Secnom7);
                treeViewNf7.setMaxWidth(Double.MAX_VALUE);
                treeViewNf7.setMaxHeight(Double.MAX_VALUE);
                vBoxNf7.getChildren().add(labelDescripcion);
                vBoxNf7.getChildren().add(hBox);
                vBoxNf7.getChildren().add(treeViewNf7);
                vBoxNf7.setSpacing(6);
                vBoxNf7.setMaxWidth(Double.MAX_VALUE);
                vBoxNf7.setMaxHeight(Double.MAX_VALUE);
                vBoxNf7.setAlignment(Pos.CENTER);
                vBoxNf7.setStyle("-fx-background-color: #ededed;");
                hBoxNf7.getChildren().add(vBoxNf7);
            }
        }
    }

    private TreeView cargandoNf1Detalle(Nf1Tipo nf1Tipo) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
            ImageView imageViewNf2Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf2 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
            HBox hBoxButtonsNf2 = new HBox();
            hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
            hBoxButtonsNf2.getChildren().add(imageViewNf2);
            hBoxButtonsNf2.setSpacing(6);
            TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), hBoxButtonsNf2);
            for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                ImageView imageViewNf3Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf3 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                HBox hBoxButtonsNf3 = new HBox();
                hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
                hBoxButtonsNf3.getChildren().add(imageViewNf3);
                hBoxButtonsNf3.setSpacing(6);
                TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
                for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                    ImageView imageViewNf4Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf4 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                    HBox hBoxButtonsNf4 = new HBox();
                    hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
                    hBoxButtonsNf4.getChildren().add(imageViewNf4);
                    hBoxButtonsNf4.setSpacing(6);
                    TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
                    for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                        ImageView imageViewNf5Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf5 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                        HBox hBoxButtonsNf5 = new HBox();
                        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                        hBoxButtonsNf5.getChildren().add(imageViewNf5);
                        hBoxButtonsNf5.setSpacing(6);
                        TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                        for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                            ImageView imageViewNf6Check = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                            ImageView imageViewNf6 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                            HBox hBoxButtonsNf6 = new HBox();
                            hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                            hBoxButtonsNf6.getChildren().add(imageViewNf6);
                            hBoxButtonsNf6.setSpacing(6);
                            TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                            for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                ImageView imageViewNf7Check = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                                ImageView imageViewNf7 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                HBox hBoxButtonsNf7 = new HBox();
                                hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                                hBoxButtonsNf7.getChildren().add(imageViewNf7);
                                hBoxButtonsNf7.setSpacing(6);
                                TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                                treeItemNf6.getChildren().add(treeItemNf7);
                            }
                            treeItemNf5.getChildren().add(treeItemNf6);
                        }
                        treeItemNf4.getChildren().add(treeItemNf5);
                    }
                    treeItemNf3.getChildren().add(treeItemNf4);
                }
                treeItemNf2.getChildren().add(treeItemNf3);
            }
            treeItemNf1.getChildren().add(treeItemNf2);
        }
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf2Detalle(Nf2Sfamilia nf2Sfamilia) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf2Sfamilia.getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
            ImageView imageViewNf3Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf3 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
            HBox hBoxButtonsNf3 = new HBox();
            hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
            hBoxButtonsNf3.getChildren().add(imageViewNf3);
            hBoxButtonsNf3.setSpacing(6);
            TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
            for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                ImageView imageViewNf4Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf4 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                HBox hBoxButtonsNf4 = new HBox();
                hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
                hBoxButtonsNf4.getChildren().add(imageViewNf4);
                hBoxButtonsNf4.setSpacing(6);
                TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
                for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                    ImageView imageViewNf5Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf5 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                    HBox hBoxButtonsNf5 = new HBox();
                    hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                    hBoxButtonsNf5.getChildren().add(imageViewNf5);
                    hBoxButtonsNf5.setSpacing(6);
                    TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                    for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                        ImageView imageViewNf6Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf6 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                        HBox hBoxButtonsNf6 = new HBox();
                        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                        hBoxButtonsNf6.getChildren().add(imageViewNf6);
                        hBoxButtonsNf6.setSpacing(6);
                        TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                        for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                            ImageView imageViewNf7Check = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                            ImageView imageViewNf7 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                            HBox hBoxButtonsNf7 = new HBox();
                            hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                            hBoxButtonsNf7.getChildren().add(imageViewNf7);
                            hBoxButtonsNf7.setSpacing(6);
                            TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                            treeItemNf6.getChildren().add(treeItemNf7);
                        }
                        treeItemNf5.getChildren().add(treeItemNf6);
                    }
                    treeItemNf4.getChildren().add(treeItemNf5);
                }
                treeItemNf3.getChildren().add(treeItemNf4);
            }
            treeItemNf2.getChildren().add(treeItemNf3);
        }
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf3Detalle(Nf3Sseccion nf3Sseccion) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf3Sseccion.getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf3Sseccion.getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
            ImageView imageViewNf4Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf4 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
            HBox hBoxButtonsNf4 = new HBox();
            hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
            hBoxButtonsNf4.getChildren().add(imageViewNf4);
            hBoxButtonsNf4.setSpacing(6);
            TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
            for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                ImageView imageViewNf5Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf5 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                HBox hBoxButtonsNf5 = new HBox();
                hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
                hBoxButtonsNf5.getChildren().add(imageViewNf5);
                hBoxButtonsNf5.setSpacing(6);
                TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
                for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                    ImageView imageViewNf6Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf6 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                    HBox hBoxButtonsNf6 = new HBox();
                    hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                    hBoxButtonsNf6.getChildren().add(imageViewNf6);
                    hBoxButtonsNf6.setSpacing(6);
                    TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                    for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                        ImageView imageViewNf7Check = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                        ImageView imageViewNf7 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                        HBox hBoxButtonsNf7 = new HBox();
                        hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                        hBoxButtonsNf7.getChildren().add(imageViewNf7);
                        hBoxButtonsNf7.setSpacing(6);
                        TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                        treeItemNf6.getChildren().add(treeItemNf7);
                    }
                    treeItemNf5.getChildren().add(treeItemNf6);
                }
                treeItemNf4.getChildren().add(treeItemNf5);
            }
            treeItemNf3.getChildren().add(treeItemNf4);
        }
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf4Detalle(Nf4Seccion1 nf4Seccion1) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf4Seccion1.getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
            ImageView imageViewNf5Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf5 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
            HBox hBoxButtonsNf5 = new HBox();
            hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
            hBoxButtonsNf5.getChildren().add(imageViewNf5);
            hBoxButtonsNf5.setSpacing(6);
            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                ImageView imageViewNf6Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf6 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                HBox hBoxButtonsNf6 = new HBox();
                hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
                hBoxButtonsNf6.getChildren().add(imageViewNf6);
                hBoxButtonsNf6.setSpacing(6);
                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                    ImageView imageViewNf7Check = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                    ImageView imageViewNf7 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                    HBox hBoxButtonsNf7 = new HBox();
                    hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                    hBoxButtonsNf7.getChildren().add(imageViewNf7);
                    hBoxButtonsNf7.setSpacing(6);
                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                    treeItemNf6.getChildren().add(treeItemNf7);
                }
                treeItemNf5.getChildren().add(treeItemNf6);
            }
            treeItemNf4.getChildren().add(treeItemNf5);
        }
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf5Detalle(Nf5Seccion2 nf5Seccion2) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf5Seccion2.getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
            ImageView imageViewNf6Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf6 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
            HBox hBoxButtonsNf6 = new HBox();
            hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
            hBoxButtonsNf6.getChildren().add(imageViewNf6);
            hBoxButtonsNf6.setSpacing(6);
            TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
            for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                ImageView imageViewNf7Check = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
                ImageView imageViewNf7 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                HBox hBoxButtonsNf7 = new HBox();
                hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
                hBoxButtonsNf7.getChildren().add(imageViewNf7);
                hBoxButtonsNf7.setSpacing(6);
                TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
                treeItemNf6.getChildren().add(treeItemNf7);
            }
            treeItemNf5.getChildren().add(treeItemNf6);
        }
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf6Detalle(Nf6Secnom6 nf6Secnom6) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf6Secnom6.getNf5Seccion2().getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        ImageView imageViewNf6Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf6 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
        HBox hBoxButtonsNf6 = new HBox();
        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
        hBoxButtonsNf6.getChildren().add(imageViewNf6);
        hBoxButtonsNf6.setSpacing(6);
        TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), hBoxButtonsNf6);
        treeItemNf6.setExpanded(true);
        for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
            ImageView imageViewNf7Check = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
            ImageView imageViewNf7 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
            HBox hBoxButtonsNf7 = new HBox();
            hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
            hBoxButtonsNf7.getChildren().add(imageViewNf7);
            hBoxButtonsNf7.setSpacing(6);
            TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
            treeItemNf6.getChildren().add(treeItemNf7);
        }
        treeItemNf5.getChildren().add(treeItemNf6);
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    private TreeView cargandoNf7Detalle(Nf7Secnom7 nf7Secnom7) {
        TreeView<String> treeViewNf = new TreeView<>();
        ImageView imageViewNf1Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf1 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
        HBox hBoxButtonsNf1 = new HBox();
        hBoxButtonsNf1.getChildren().add(imageViewNf1Check);
        hBoxButtonsNf1.getChildren().add(imageViewNf1);
        hBoxButtonsNf1.setSpacing(6);
        TreeItem<String> treeItemNf1 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getNf1Tipo().getDescripcion(), hBoxButtonsNf1);
        treeItemNf1.setExpanded(true);
        ImageView imageViewNf2Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf2 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
        HBox hBoxButtonsNf2 = new HBox();
        hBoxButtonsNf2.getChildren().add(imageViewNf2Check);
        hBoxButtonsNf2.getChildren().add(imageViewNf2);
        hBoxButtonsNf2.setSpacing(6);
        TreeItem<String> treeItemNf2 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getNf2Sfamilia().getDescripcion(), hBoxButtonsNf2);
        treeItemNf2.setExpanded(true);
        ImageView imageViewNf3Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf3 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
        HBox hBoxButtonsNf3 = new HBox();
        hBoxButtonsNf3.getChildren().add(imageViewNf3Check);
        hBoxButtonsNf3.getChildren().add(imageViewNf3);
        hBoxButtonsNf3.setSpacing(6);
        TreeItem<String> treeItemNf3 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getNf3Sseccion().getDescripcion(), hBoxButtonsNf3);
        treeItemNf3.setExpanded(true);
        ImageView imageViewNf4Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf4 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
        HBox hBoxButtonsNf4 = new HBox();
        hBoxButtonsNf4.getChildren().add(imageViewNf4Check);
        hBoxButtonsNf4.getChildren().add(imageViewNf4);
        hBoxButtonsNf4.setSpacing(6);
        TreeItem<String> treeItemNf4 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getNf4Seccion1().getDescripcion(), hBoxButtonsNf4);
        treeItemNf4.setExpanded(true);
        ImageView imageViewNf5Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf5 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
        HBox hBoxButtonsNf5 = new HBox();
        hBoxButtonsNf5.getChildren().add(imageViewNf5Check);
        hBoxButtonsNf5.getChildren().add(imageViewNf5);
        hBoxButtonsNf5.setSpacing(6);
        TreeItem<String> treeItemNf5 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getNf5Seccion2().getDescripcion(), hBoxButtonsNf5);
        treeItemNf5.setExpanded(true);
        ImageView imageViewNf6Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/no-icon.png")));
        ImageView imageViewNf6 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
        HBox hBoxButtonsNf6 = new HBox();
        hBoxButtonsNf6.getChildren().add(imageViewNf6Check);
        hBoxButtonsNf6.getChildren().add(imageViewNf6);
        hBoxButtonsNf6.setSpacing(6);
        TreeItem<String> treeItemNf6 = new TreeItem<>(nf7Secnom7.getNf6Secnom6().getDescripcion(), hBoxButtonsNf6);
        treeItemNf6.setExpanded(true);
        ImageView imageViewNf7Check = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/ok.png")));
        ImageView imageViewNf7 = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
        HBox hBoxButtonsNf7 = new HBox();
        hBoxButtonsNf7.getChildren().add(imageViewNf7Check);
        hBoxButtonsNf7.getChildren().add(imageViewNf7);
        hBoxButtonsNf7.setSpacing(6);
        TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), hBoxButtonsNf7);
        treeItemNf6.getChildren().add(treeItemNf7);
        treeItemNf5.getChildren().add(treeItemNf6);
        treeItemNf4.getChildren().add(treeItemNf5);
        treeItemNf3.getChildren().add(treeItemNf4);
        treeItemNf2.getChildren().add(treeItemNf3);
        treeItemNf1.getChildren().add(treeItemNf2);
        treeViewNf.setRoot(treeItemNf1);
        return treeViewNf;
    }

    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
    private void vistaJSONObjectTarjArt() {
        //......................................................................
        detalleData = FXCollections.observableArrayList(tarjCabArt);
        //columna código..............................................
        tableColumnCodArt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodArt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("codArticulo").toString());
            }
        });
        //columna código......................................................
        //columna artículo..............................................
        tableColumnArticulo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnArticulo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna artículo......................................................
        tableViewArticuloTemp.setItems(detalleData);
    }
    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->

    @FXML
    private void radioButtonCajaAction(ActionEvent event) {
    }

    @FXML
    private void radioButtonExtractoAction(ActionEvent event) {
    }
}
