/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.dao.ArticuloDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import org.apache.commons.validator.GenericValidator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class DescuentoTarjetaArticuloFXMLController extends BaseScreenController implements Initializable {

    private boolean alert;
    private ObservableList<String> articulosAsignados;
    private static HashMap<Long, JSONObject> hashMapCodArt;
    private final Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();
    Task copyWorkerExcel;
    private String excepcionExcelBD;
    private String exitoExcel;
    private boolean procesandoExcel;
    NumberValidator num;

    @Autowired
    private ArticuloDAO articuloDAO;

    @FXML
    private Label labelDescuentoTarjArt;
    @FXML
    private ListView<String> listViewArticulos;
    @FXML
    private VBox vBoxButtons;
    @FXML
    private Button buttonAgregar;
    @FXML
    private Button buttonQuitar;
    @FXML
    private Button buttonQuitarTodos;
    @FXML
    private HBox hBoxComboSeccion;
    @FXML
    private Label labelArticulo;
    @FXML
    private TextField textFieldCodArt;
    @FXML
    private Button buttonExcel;
    @FXML
    private ProgressBar progressIndicatorExcel;
    @FXML
    private Label labelProgressBarExcel;
    @FXML
    private Label labelPorc;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private AnchorPane anchorPaneArt;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonAgregarAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void buttonQuitarAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void buttonQuitarTodosAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void textFieldCodArtKeyReleased(KeyEvent event) {
    }

    @FXML
    private void buttonExcelAction(ActionEvent event) {
        asignandoDesdeExcel();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        progressIndicatorExcel.setVisible(false);
        labelProgressBarExcel.setVisible(false);
        labelPorc.setVisible(false);
        excepcionExcelBD = "";
        alert = false;
        ObservableList<String> list = FXCollections.observableArrayList();
        articulosAsignados = FXCollections.observableArrayList(list);
        listViewArticulos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        validandoCodArt();
        num = new NumberValidator();
        setHashMapCodArt(new HashMap<>());
        procesandoExcel = false;
        DescuentoTarjetaFXMLController.setScreenScopedTarjArt(false);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, "/vista/descuento/DescuentoTarjetaArticuloFXML.fxml", 992, 319, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        if (excepcionExcelBD.isEmpty()) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else {
            TextArea textArea = new TextArea(msj);
            textArea.setEditable(false);
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "", ButtonType.OK);
            alert2.getDialogPane().setContent(textArea);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        if (!procesandoExcel) {
            KeyCode keyCode = event.getCode();
            if (keyCode == event.getCode().F4) {
                asignandoDesdeExcel();
            }
            if (keyCode == KeyCode.ESCAPE) {
                if (alert) {
                    alert = false;
                } else {
                    volviendo();
                }
            }
        }
    }

    private void validandoCodArt() {
        textFieldCodArt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isLong(aux)) {
                    try {
                        if (GenericValidator.isInRange(Long.valueOf(newValue), 0l, 9223372036854775807l)) {
                            Platform.runLater(() -> {
                                textFieldCodArt.setText(newValue.toString());
                                textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                            });
                        } else {
                            Platform.runLater(() -> {
                                textFieldCodArt.setText(oldValue.toString());
                                textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                            });
                        }
                    } catch (NumberFormatException e) {
                        if (GenericValidator.isLong(oldValue)) {
                            textFieldCodArt.setText(oldValue);
                            textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                        }
                    }
                } else {
                    Platform.runLater(() -> {
                        textFieldCodArt.setText(oldValue.toString());
                        textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                    });
                }
            }
        }
        );
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    public void ocultandoProgressExcel() {
        listViewArticulos.setDisable(false);
        vBoxButtons.setDisable(false);
        hBoxComboSeccion.setDisable(false);
        buttonExcel.setDisable(false);
        buttonVolver.setDisable(false);
        //
        progressIndicatorExcel.setVisible(false);
        labelProgressBarExcel.setVisible(false);
        labelPorc.setVisible(false);
        textFieldCodArt.setText("");
        if (!excepcionExcelBD.isEmpty()) {
            mensajeError(excepcionExcelBD);
            quitandoTodos();
        }
        procesandoExcel = false;
    }

    public void viendoProgressExcel() {
        listViewArticulos.setDisable(true);
        vBoxButtons.setDisable(true);
        hBoxComboSeccion.setDisable(true);
        buttonExcel.setDisable(true);
        buttonVolver.setDisable(true);
        //
        progressIndicatorExcel.setVisible(true);
        labelProgressBarExcel.setVisible(true);
        labelPorc.setVisible(true);
        procesandoExcel = true;
    }

    private void quitandoTodos() {
        articulosAsignados.removeAll(articulosAsignados);
        listViewArticulos.setItems(articulosAsignados);
        setHashMapCodArt(new HashMap<>());
    }

    private void quitando() {
        for (int i = 0; i < listViewArticulos.getSelectionModel().getSelectedItems().size(); i++) {
            String aux = listViewArticulos.getSelectionModel().getSelectedItems().get(i).toString();
            String[] parts = aux.split(" - ");
            hashMapCodArt.remove(Long.valueOf(num.numberValidator(parts[0])));
        }
        articulosAsignados.removeAll(listViewArticulos.getSelectionModel().getSelectedItems());
        listViewArticulos.setItems(articulosAsignados);
    }

    private void asignandoDesdeExcel() {
        if (!listViewArticulos.getItems().isEmpty()) {
            mensajeError("PARA IMPORTAR DESDE UN EXCEL\nLA LISTA DE ARTÍCULOS DEBE ESTAR VACÍA.");
        } else {
            Map<Long, String> hmCodPor = fileChooserFX();
            if (!hmCodPor.isEmpty()) {
                copyWorkerExcel = createWorkerExcel(hmCodPor);
                progressIndicatorExcel.progressProperty().unbind();
                progressIndicatorExcel.progressProperty().bind(copyWorkerExcel.progressProperty());
                new Thread(copyWorkerExcel).start();
            } else {
                if (exitoExcel.isEmpty()) {
                    mensajeError("EXCEPCIÓN DESCONOCIDA.");
                } else {
                    if (exitoExcel.contentEquals("empty")) {
                        mensajeError("EXCEL VACÍO.");
                    } else if (exitoExcel.contentEquals("misc")) {
                        mensajeError("EXCEPCIÓN DESCONOCIDA.");
                    } else if (exitoExcel.contentEquals("null")) {
                        mensajeError("EXCEL NULO.");
                    } else if (exitoExcel.contentEquals("formato")) {
                        mensajeError("EL EXCEL DEBE ESTAR EN FORMATO NÚMERICO\n(COLUMNA 'A' -> CÓDIGO ARTÍCULO)");
                    } else {
                        String msjCelda[] = exitoExcel.split(" - ");
                        if (msjCelda[0].contentEquals("cod")) {
                            mensajeError("SE DETECTÓ UNA EXCEPCIÓN EN CÓDIGO ARTÍCULO [CELDA - " + msjCelda[1] + "]");
                        } else if (msjCelda[0].contentEquals("por")) {
                            mensajeError("SE DETECTÓ UNA EXCEPCIÓN EN PORCENTAJE DTO. [CELDA - " + msjCelda[1] + "]");
                        } else if (msjCelda[0].contentEquals("overflow")) {
                            mensajeError("SUPERA EL LÍMITE PORCENTAJE DTO. [CELDA - " + msjCelda[1] + "]");
                        } else if (msjCelda[0].contentEquals("codRep")) {
                            if (msjCelda.length == 3) {
                                mensajeError("[CELDA - " + msjCelda[1] + "]     [CELDA - " + msjCelda[2] + "]\nCÓDIGOS REPETIDOS.");
                            } else {
                                mensajeError("EXISTEN CÓDIGOS REPETIDOS EN EL EXCEL.");
                            }
                        } else {
                            mensajeError("EXCEPCIÓN DESCONOCIDA.");
                        }
                    }
                }
            }
        }
    }

    private void asignando() {
        if (textFieldCodArt.getText().isEmpty()) {
            textFieldCodArt.setText("");
            mensajeError("EL CAMPO CÓD. ARTÍCULO NO DEBE ESTAR VACÍO.");
        } else {
            if (hashMapCodArt.containsKey(Long.valueOf(num.numberValidator(textFieldCodArt.getText())))) {
                textFieldCodArt.setText("");
                mensajeError("EL CAMPO CÓD. ARTÍCULO YA SE AGREGÓ A LA LISTA.");
            } else {
                if (ConexionPostgres.conectarLocal()) {
                    long cod = Long.valueOf(num.numberValidator(textFieldCodArt.getText()));//para recontra asegurar
                    JSONObject jsonArt = generarArticuloLocal(cod);
                    if (jsonArt != null) {
                        articulosAsignados.addAll(jsonArt.get("codArticulo").toString() + " - " + jsonArt.get("descripcion").toString().toUpperCase());
                        listViewArticulos.setItems(articulosAsignados);
                        hashMapCodArt.put(cod, jsonArt);
                        textFieldCodArt.setText("");
                    } else {
                        textFieldCodArt.setText("");
                        mensajeError("EL CAMPO CÓD. ARTÍCULO NO EXISTE.");
                    }
                } else {
                    mensajeError("EXCEPCIÓN [BD].");
                }
                if (ConexionPostgres.getCon() != null) {
                    ConexionPostgres.cerrarLocal();
                }
            }
        }
    }

    @SuppressWarnings("null")
    private Map<Long, String> fileChooserFX() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("SELECCIÓN EXCEL");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel", "*.xls", /*"*.xml", "*.xlt",*/ "*.xlsx"));
        File file = fileChooser.showOpenDialog(this.sc.getStage());
        exitoExcel = "";
        HashMap<Long, String> hmCodRep = new HashMap<>();
        Map<Long, String> hashMapCodCell = new LinkedHashMap<>();
        if (file != null) {
            String[] parts = file.getName().split("\\.");
            XSSFWorkbook xssfWb = null;
            HSSFWorkbook hssfWb = null;
            try {
                if (parts[parts.length - 1].contentEquals("xls")) {
                    POIFSFileSystem fs = new POIFSFileSystem(file);
                    hssfWb = new HSSFWorkbook(fs);
                    HSSFSheet sheet = hssfWb.getSheetAt(0);
                    if (sheet.getPhysicalNumberOfRows() > 0) {
                        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
                            try {
                                if (hmCodRep.containsKey(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()))) {
                                    exitoExcel = "codRep - " + sheet.getRow(i).getCell(0).getAddress().toString() + " - "
                                            + hmCodRep.get(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()));
                                    break;
                                } else {
                                    double cod = sheet.getRow(i).getCell(0).getNumericCellValue();
                                    long codigo = Math.round(cod);
                                    if (codigo == 0) {
                                        exitoExcel = "cod - " + sheet.getRow(i).getCell(0).getAddress();
                                        break;
                                    } else {
                                        hashMapCodCell.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                        hmCodRep.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                    }
                                }
                            } catch (IllegalStateException e) {
                                exitoExcel = "formato";
                                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                                break;
                            }
                        }
                        hssfWb.close();
                    } else {
                        hssfWb.close();
                        exitoExcel = "empty";
                    }
                } else if (parts[parts.length - 1].contentEquals("xlsx")) {
                    OPCPackage op = OPCPackage.open(file);
                    xssfWb = new XSSFWorkbook(op);
                    XSSFSheet sheet = xssfWb.getSheetAt(0);
                    if (sheet.getPhysicalNumberOfRows() > 0) {
                        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
                            try {
                                if (hmCodRep.containsKey(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()))) {
                                    exitoExcel = "codRep - " + sheet.getRow(i).getCell(0).getAddress().toString() + " - "
                                            + hmCodRep.get(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()));
                                    break;
                                } else {
                                    double cod = sheet.getRow(i).getCell(0).getNumericCellValue();
                                    long codigo = Math.round(cod);
                                    if (codigo == 0) {
                                        exitoExcel = "cod - " + sheet.getRow(i).getCell(0).getAddress();
                                        break;
                                    } else {
                                        hashMapCodCell.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                        hmCodRep.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                    }
                                }
                            } catch (IllegalStateException e) {
                                exitoExcel = "formato";
                                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                                break;
                            }
                        }
                        xssfWb.close();
                    } else {
                        xssfWb.close();
                        exitoExcel = "empty";
                    }
                }
            } catch (IOException | InvalidFormatException e) {
                System.out.println(e.fillInStackTrace());
                exitoExcel = "misc";
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                return new LinkedHashMap<>();
            } finally {
                if (xssfWb != null) {
                    try {
                        xssfWb.close();
                    } catch (IOException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                if (hssfWb != null) {
                    try {
                        hssfWb.close();
                    } catch (IOException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        } else {
            exitoExcel = "null";
        }
        return hashMapCodCell;
    }

    //////READ, ARTÍCULO
    private JSONObject generarArticuloLocal(Long codigo) {
        JSONObject jsonArt = new JSONObject();
        JSONParser parser = new JSONParser();
        Articulo artRevancha = articuloDAO.buscarCod(String.valueOf(codigo));
        if (artRevancha != null) {
            try {
                jsonArt = (JSONObject) parser.parse(gson.toJson(artRevancha.toArticuloDTO()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        } else {
            jsonArt = null;
        }
        return jsonArt;
    }
    //////READ, ARTÍCULO

    //PROGRESS TASK EXCEL**************************** -> -> -> -> -> -> -> -> ->
    public Task createWorkerExcel(Map<Long, String> hmCodPor) {
        return new Task() {
            @Override
            protected Object call() {
                int i = 1;
                int size = hmCodPor.size();
                Platform.runLater(() -> {
                    viendoProgressExcel();
                });
                excepcionExcelBD = "";
                if (ConexionPostgres.conectarLocal()) {
                    for (Map.Entry<Long, String> entry : hmCodPor.entrySet()) {
                        System.out.println("código = " + entry.getKey() + ", celda = " + entry.getValue());
                        JSONObject jsonArt = generarArticuloLocal(entry.getKey());
                        if (jsonArt != null) {
                            hashMapCodArt.put(entry.getKey(), jsonArt);
                            updateProgress(i, size);
                            int porc = (i * 100) / size;
                            i++;
                            Platform.runLater(() -> {
                                articulosAsignados.addAll(jsonArt.get("codArticulo").toString() + " - "
                                        + jsonArt.get("descripcion").toString().toUpperCase());
                                listViewArticulos.setItems(articulosAsignados);
                                labelPorc.setText(porc + " %");
                            });
                        } else {
                            excepcionExcelBD = "EL CÓD. ARTÍCULO [" + entry.getKey() + "] NO EXISTE.";
                            break;
                        }
                    }
                } else {
                    excepcionExcelBD = "EXCEPCIÓN [BD]";
                }
                if (ConexionPostgres.getCon() != null) {
                    ConexionPostgres.cerrarLocal();
                }
                Platform.runLater(() -> {
                    ocultandoProgressExcel();
                });
                return true;
            }
        };
    }
    //PROGRESS TASK EXCEL**************************** -> -> -> -> -> -> -> -> ->

    public static HashMap<Long, JSONObject> getHashMapCodArt() {
        return hashMapCodArt;
    }

    public static void setHashMapCodArt(HashMap<Long, JSONObject> aHashMapCodArt) {
        hashMapCodArt = aHashMapCodArt;
    }

}
