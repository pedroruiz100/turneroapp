package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.DescuentoTarjetaConvenioCab;
import com.peluqueria.core.domain.DescuentoTarjetaConvenioDet;
import com.peluqueria.core.domain.Dias;
import com.peluqueria.core.domain.TarjetaConvenio;
import com.peluqueria.dto.DescuentoTarjetaConvenioCabDTO;
import com.peluqueria.dto.DescuentoTarjetaConvenioDetDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.DescuentoTarjetaConvenioCabDAO;
import com.peluqueria.dao.DescuentoTarjetaConvenioDetDAO;
import com.peluqueria.dao.DiaDAO;
import com.peluqueria.dao.RangoDescTarjConvCabDAO;
import com.peluqueria.dao.TarjetaConvenioDAO;
import com.javafx.util.RemindTask;
import java.util.Timer;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class DescuentoTarjetaConvenioFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private DescuentoTarjetaConvenioCabDAO descConvenioDAO;
    @Autowired
    private DescuentoTarjetaConvenioDetDAO descConvDetDAO;
    @Autowired
    private DiaDAO diaDAO;
    @Autowired
    private RangoDescTarjConvCabDAO rangoDescTarjConvCabDAO;
    @Autowired
    private TarjetaConvenioDAO tarjetaDAO;

    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    private boolean exitoBaja;
    private boolean diasCargar;
    private boolean buscarTodosTarjConv;
    private List<JSONObject> diasList;
    private boolean cargarTarjConv;
    private JSONArray tarjConvJSONArray;
    private JSONArray diaJSONArray;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();
    private List<JSONObject> cabTarjConvList;
    //PAGINATION
    private int filaIniCabTarjConv, filaCantCabTarjConv, filaLimitCabTarjConv;
    private int pageCountCabTarjConv, pageActualCabTarjConv;
    private ObservableList<JSONObject> cabTarjConvData;
    private TableView<JSONObject> tableCabTarjConv;
    private TableColumn<JSONObject, String> columnTarjetaConvenio;
    private TableColumn<JSONObject, String> columnDescuento;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvDomingo;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvLunes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvMartes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvMiercoles;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvJueves;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvViernes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjConvSabado;
    private TableColumn<JSONObject, Boolean> columnEliminar;
    //PAGINATION

    HashMap<String, JSONObject> hashMapTarjConv;
    Timer timer;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneDescTarjConvenio;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private AnchorPane anchorPaneListView;
    @FXML
    private Label labelDescuento;
    @FXML
    private VBox vBoxCheckBox;
    @FXML
    private CheckBox checkBoxDomingo;
    @FXML
    private CheckBox checkBoxLunes;
    @FXML
    private CheckBox checkBoxMartes;
    @FXML
    private CheckBox checkBoxMiercoles;
    @FXML
    private VBox vBoxCheckBox1;
    @FXML
    private Label labelDescuento1;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonInsertar;
    @FXML
    private ComboBox<String> comboBoxTarjConvenio;
    @FXML
    private Pagination paginationTarjConvenio;
    @FXML
    private Label labelFiltro;
    @FXML
    private HBox vBoxFiltro;
    @FXML
    private Label labelTarjConvenio;
    @FXML
    private TextField textFielFiltroTarjConvenio;
    @FXML
    private Button buttonBuscarTarjConvenio;
    @FXML
    private Button buttonTodos;
    @FXML
    private SplitPane splitPaneConfigTarjConvDesc;
    @FXML
    private Label labelConfigTarjConv;
    @FXML
    private ChoiceBox<String> choiceBoxDesc;
    @FXML
    private CheckBox checkBoxJueves;
    @FXML
    private CheckBox checkBoxViernes;
    @FXML
    private CheckBox checkBoxSabado;
    @FXML
    private HBox hBoxContainer;
    @FXML
    private Button buttonCancelarTodas;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Label labelDiasSemana;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private CheckBox checkBoxSincro;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoDescTarjConv();
    }

    @FXML
    private void buttonBuscarTarjConvenioAction(ActionEvent event) {
        buscandoDescuentoFiltro();
    }

    @FXML
    private void buttonTodosAction(ActionEvent event) {
        buscandoDescuentoTodo();
    }

    @FXML
    private void comboBoxTarjConvenioKeyReleased(KeyEvent event) {
        keyPressComboBox(event);
    }

    @FXML
    private void anchorPaneDescTarjConvenioKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void comboBoxTarjConvenioMouseClicked(MouseEvent event) {
        mouseEventComboBox(event);
    }

    @FXML
    private void buttonCancelarTodasAction(ActionEvent event) {
        cancelandoDescuentoTarjetaConvTodo();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        comboBoxTarjConvenio.setEditable(true);
        choiceBoxDesc.getItems().add("-");
        for (int i = 1; i < 51; i++) {
            if (i % 5 == 0) {
                choiceBoxDesc.getItems().add(i + " %");
            }
        }
        choiceBoxDesc.getSelectionModel().select(0);
        cargarTarjConv = true;
        alert = false;
        diasCargar = false;
        exitoInsertarCab = false;
        exitoBaja = false;
        buscarTodosTarjConv = true;
        comboBoxTarjConvenio.setPromptText("Filtro tarjeta convenio...");
        primeraPaginacion();
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/DescuentoTarjetaConvenioFXML.fxml", 867, 647, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void mouseEventComboBox(MouseEvent event) {
        if (cargarTarjConv) {
            jsonCargandoTarjConv();
            if (tarjConvJSONArray.isEmpty()) {
                mensajeError("NO SE ENCONTRÓ NINGUNA TARJETA CONVENIO DISPONIBLE.");
                cargarTarjConv = true;
            }
        }
    }

    private void keyPressComboBox(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (cargarTarjConv) {
            if (keyCode != event.getCode().TAB) {
                jsonCargandoTarjConv();
                if (tarjConvJSONArray.isEmpty()) {
                    mensajeError("NO SE ENCONTRÓ NINGUNA TARJETA CONVENIO DISPONIBLE.");
                    cargarTarjConv = true;
                }
            }
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoDescTarjConv();
        }
        if (keyCode == event.getCode().F2) {
            buscandoDescuentoFiltro();
        }
        if (keyCode == event.getCode().F3) {
            buscandoDescuentoTodo();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().F12) {
            cancelandoDescuentoTarjetaConvTodo();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void insertandoDescTarjConv() {
        if (!checkBoxDomingo.isSelected() && !checkBoxLunes.isSelected()
                && !checkBoxMartes.isSelected() && !checkBoxMiercoles.isSelected()
                && !checkBoxJueves.isSelected() && !checkBoxViernes.isSelected()
                && !checkBoxSabado.isSelected()) {
            mensajeError("DEBE SELECCIONAR AL MENOS UN DÍA DE LA SEMANA.");
        } else if (comboBoxTarjConvenio.getSelectionModel().isEmpty()) {
            mensajeError("DEBE SELECCIONAR UNA TARJETA CONVENIO.");
        } else if (!comboBoxTarjConvenio.getEditor().getText().contentEquals(comboBoxTarjConvenio.getItems().get(comboBoxTarjConvenio.getSelectionModel().getSelectedIndex()))) {
            //condición para el autocomplete en el comboBox...
            mensajeError("DEBE SELECCIONAR UNA TARJETA CONVENIO.");
            comboBoxTarjConvenio.getEditor().setText("");
            comboBoxTarjConvenio.setPromptText("Filtro tarjeta convenio...");
        } else if (choiceBoxDesc.getSelectionModel().getSelectedItem().contentEquals("-")) {
            mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR DESCUENTO TARJETA CONVENIO "
                    + comboBoxTarjConvenio.getSelectionModel().getSelectedItem() + "?", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
//                if ("guardar_tarjeta_convenio_desc")) {
                if (creandoCabDescTarjConv()) {
                    exitoInsertarCab = false;
                    buscandoDescuentoTodo();
                    limpiando();
                } else {
                    mensajeError("NO SE PUDO INSERTAR DESCUENTO TARJETA CONVENIO.\nVERIFIQUE LOS CAMPOS.");
                }
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/DescuentoTarjetaConvenioFXML.fxml", 867, 647, false);
//                }
            } else if (alert.getResult() == cancel) {
                alert.close();
            }
        }
    }

    private void resetComboBox() {
        cargarTarjConv = true;
        if (!comboBoxTarjConvenio.getItems().isEmpty()) {
            ObservableList<String> list = FXCollections.observableArrayList();
            comboBoxTarjConvenio.valueProperty().set(null);
            comboBoxTarjConvenio.setItems(list);
            comboBoxTarjConvenio.getSelectionModel().clearSelection();
            comboBoxTarjConvenio.getProperties().clear();
            comboBoxTarjConvenio.getEditor().appendText("");
            comboBoxTarjConvenio.setPromptText("Filtro Tarjeta Convenio...");
            new AutoCompleteComboBoxListener<>(comboBoxTarjConvenio);
            hashMapTarjConv = new HashMap<>();
        }
    }

    private void resetDescTarjConvBaja() {
        exitoBaja = false;
        primeraPaginacion();
        resetComboBox();
    }

    private void cancelandoDescuentoTarjetaConvTodo() {
//        if ("baja_desc_tarj_conv") {
//            
//        }
//        
//            ) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN TARJETA CONVENIO?", ok, cancel);
            alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ok) {
                if (bajaDescTarjetaConvTodo()) {
                    exitoBaja = false;
                }
                alert2.close();
            } else if (alert2.getResult() == cancel) {
                alert2.close();
            }
//        }else {
//            mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//        }
    }

    private void limpiandoBusqueda() {
        textFielFiltroTarjConvenio.setText("");
    }

    private void limpiando() {
        checkBoxDomingo.setSelected(false);
        checkBoxLunes.setSelected(false);
        checkBoxMartes.setSelected(false);
        checkBoxMiercoles.setSelected(false);
        checkBoxJueves.setSelected(false);
        checkBoxViernes.setSelected(false);
        checkBoxSabado.setSelected(false);
        choiceBoxDesc.getSelectionModel().select(0);
        resetComboBox();
    }

    private Integer jsonRowCount() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoConvenioCab();
        return rowCount;
    }

    private Integer jsonRowCountFilter(String tarjConvFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFilterTarjConvenioLocal(tarjConvFilter);
        return rowCountFilter;
    }

    private List<JSONObject> readJsonDias() {
        List<JSONObject> diaJSONObjList = new ArrayList<JSONObject>();
        diaJSONArray = generardDiasLocal();
        diasCargar = true;
        for (Object diaObj : diaJSONArray) {
            JSONObject diaJSONObj = (JSONObject) diaObj;
            diaJSONObjList.add(diaJSONObj);
        }
        return diaJSONObjList;
    }

    private List<JSONObject> jsonArrayCabTarjConvFetch(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabTarjConvJSONObjList = new ArrayList<>();
        cabTarjConvJSONObjList = generarFetchDescuentoTarjetaConv(limRowS, offSetS);
        return cabTarjConvJSONObjList;
    }

    private List<JSONObject> jsonArrayCabTarjConvFetchFiltro(int limRow, int offSet, String tarjConvFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String tarjConv = "null";
        if (!tarjConvFiltro.contentEquals("")) {
            tarjConv = tarjConvFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONObjList = generarFetchFiltroDescuentoConvLocal(limRowS, offSetS, tarjConv);
        return cabFielJSONObjList;
    }

    private void jsonCargandoTarjConv() {
        if (cargarTarjConv) {
            tarjConvJSONArray = null;
            tarjConvJSONArray = generarNomIdTarjConvenio();
            cargarTarjConv = false;
            hashMapTarjConv = new HashMap();
            for (Object obj : tarjConvJSONArray) {
                JSONObject tarjConv = (JSONObject) obj;
                hashMapTarjConv.put(tarjConv.get("descripcion").toString(), tarjConv);
                comboBoxTarjConvenio.getItems().add(tarjConv.get("descripcion").toString());
            }
            new AutoCompleteComboBoxListener<>(comboBoxTarjConvenio);
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, DESCUENTO TARJETA CONVENIO ID -> GET
    private JSONObject recuperarDatoTarjetaConvenio(long idDesc) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioCab/getById/" + idDesc);
            URLConnection uc = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            while ((inputLine = br.readLine()) != null) {
                descuento = (JSONObject) parser.parse(inputLine);
            }
            br.close();
        } catch (IOException ex) {
            descuento = null;
            Utilidades.log.error("ERROR IOException: ", ex.fillInStackTrace());
        } catch (ParseException ex) {
            descuento = null;
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        return descuento;
    }
    //////READ, DESCUENTO TARJETA CONVENIO ID -> GET

    //////UPDATE, DESCUENTO TARJETA CONVENIO -> PUT
    private boolean bajaCabTarjConv(JSONObject tarjConvCab) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            //Verifica si existe el dato en el servidor para cambiar de estado
            JSONObject obj = recuperarDatoTarjetaConvenio(Long.parseLong(tarjConvCab.get("idDescuentoTarjetaConvenioCab").toString()));
            boolean status = false;
            if (obj != null) {
                obj.remove("tarjetaConvenio");
                obj.remove("fechaAlta");
                obj.remove("fechaMod");
                DescuentoTarjetaConvenioCabDTO descDTO = gson.fromJson(obj.toString(), DescuentoTarjetaConvenioCabDTO.class);
                if (descDTO.getIdDescuentoTarjetaConvenioCab() != null) {
                    status = true;
                }
            }
            // mapeo para recuperar fechaAlta y fechaMod
            Map mapeo = recuperarFechaLong(Long.valueOf(tarjConvCab.get("idDescuentoTarjetaConvenioCab").toString()));
            JSONObject tarjJSON = (JSONObject) parser.parse(tarjConvCab.get("tarjetaConvenio").toString());
            tarjJSON.remove("fechaAlta");
            tarjJSON.remove("fechaMod");
            tarjConvCab.put("tarjetaConvenio", tarjJSON);
            tarjConvCab.put("fechaAlta", mapeo.get("fechaAlta"));
            tarjConvCab.put("fechaMod", mapeo.get("fechaMod"));
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(tarjConvCab.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescuentoTarjetaConv(tarjConvCab);
                }
            } else {
                exitoBaja = actualizarDescuentoTarjetaConv(tarjConvCab);
            }
        } catch (IOException e) {
            exitoBaja = actualizarDescuentoTarjetaConv(tarjConvCab);
        } catch (ParseException ex) {
            exitoBaja = actualizarDescuentoTarjetaConv(tarjConvCab);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (tarjConvCab.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(tarjConvCab.get("fechaAlta").toString()));
                tarjConvCab.put("fechaAlta", null);
            }
            if (tarjConvCab.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(tarjConvCab.get("fechaMod").toString()));
                tarjConvCab.put("fechaMod", null);
            }
            DescuentoTarjetaConvenioCabDTO descuentoConvCabDTO = gson.fromJson(tarjConvCab.toString(), DescuentoTarjetaConvenioCabDTO.class);
            descuentoConvCabDTO.setFechaAlta(fechaAlta);
            descuentoConvCabDTO.setFechaMod(fechaMod);
            descuentoConvCabDTO.setEstadoDesc(false);
            if (descConvenioDAO.actualizarObtenerEstado(DescuentoTarjetaConvenioCab.fromDescuentoTarjetaConvenioCabDTO(descuentoConvCabDTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO TARJETA CONVENIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetDescTarjConvBaja();
            } else {
                alert2.close();
                resetDescTarjConvBaja();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO TARJETA CONVENIO.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO TARJETA CONVENIO -> PUT

    //////CREATE, DESCUENTO TARJETA CONVENIO CAB. -> POST
    private boolean creandoCabDescTarjConv() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject cabDescTarjConv = new JSONObject();
        try {
            cabDescTarjConv = creandoJsonCabTarjConv();
            if (cabDescTarjConv.isEmpty()) {
                return false;
            }
            CajaDatos.setIdDescuentoTarjetaConvenio(rangoDescTarjConvCabDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal()));
            cabDescTarjConv.put("idDescuentoTarjetaConvenioCab", CajaDatos.getIdDescuentoTarjetaConvenio());
            //PARA EVITAR EL FORMATO DE FECHA QUE NO ES NECESARIO
            cabDescTarjConv = evitarFormatoFechaTarjConvenio(cabDescTarjConv);
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(cabDescTarjConv.toString());
                System.out.println("-->> " + cabDescTarjConv.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        cabDescTarjConv = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setDescuentoTarjetaConvCab(true);
                        CajaDatos.setIdDescuentoTarjetaConvenio(Long.parseLong(cabDescTarjConv.get("idDescuentoTarjetaConvenioCab").toString()));
                        exitoInsertarCab = true;
                    }
                    br.close();
                } else {
                    cabDescTarjConv = registrarDescuentoConvenioCabLocal(cabDescTarjConv);
                }
            } else {
                cabDescTarjConv = registrarDescuentoConvenioCabLocal(cabDescTarjConv);
            }
        } catch (IOException | ParseException ex) {
            cabDescTarjConv = registrarDescuentoConvenioCabLocal(cabDescTarjConv);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (cabDescTarjConv != null) {
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            cabDescTarjConv.put("fechaAlta", null);
            cabDescTarjConv.put("fechaMod", null);
            //PARA EVITAR EL FORMATO DE FECHA QUE NO ES NECESARIO
            cabDescTarjConv = evitarFormatoFechaTarjConvenio(cabDescTarjConv);
            System.out.println("-->> " + cabDescTarjConv.toString());
            DescuentoTarjetaConvenioCabDTO descTarjCabDTO = gson.fromJson(cabDescTarjConv.toString(), DescuentoTarjetaConvenioCabDTO.class);
            descTarjCabDTO.setFechaAlta(timestamp);
            descTarjCabDTO.setFechaMod(timestamp);
            DescuentoTarjetaConvenioCab valor = descConvenioDAO.insertarObtenerObjeto(DescuentoTarjetaConvenioCab.fromDescuentoTarjetaConvenioCabDTO(descTarjCabDTO));
            if (valor != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO TARJETA CONVENIO CABECERA");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO TARJETA CONVENIO CABECERA");
            }
        }
        if (exitoInsertarCab) {
            if (!diasCargar) {
                diasList = readJsonDias();
            }
            if (checkBoxDomingo.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(0)));
            }
            if (checkBoxLunes.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(1)));
            }
            if (checkBoxMartes.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(2)));
            }
            if (checkBoxMiercoles.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(3)));
            }
            if (checkBoxJueves.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(4)));
            }
            if (checkBoxViernes.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(5)));
            }
            if (checkBoxSabado.isSelected()) {
                creandoDetDescTarjConv(creandoJsonDetTarjConv(cabDescTarjConv, diasList.get(6)));
            }
        }
        CajaDatos.setDescuentoTarjetaConvCab(false);
        return exitoInsertarCab;
    }
    //////CREATE, DESCUENTO TARJETA CONVENIO CAB. -> POST

    //////CREATE, DESCUENTO TARJETA CONVENIO DET. -> POST
    private boolean creandoDetDescTarjConv(JSONObject detDescTarjConv) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            detDescTarjConv.put("idDescuentoTarjetaConvenioCab", CajaDatos.getIdDescuentoTarjetaConvenio());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoTarjetaConvCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioDet");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(detDescTarjConv.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarDescConvenioDetLocal(detDescTarjConv.toString());
                }
            } else {
                exitoInsertarDet = registrarDescConvenioDetLocal(detDescTarjConv.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarDescConvenioDetLocal(detDescTarjConv.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoTarjetaConvenioDetDTO descDTO = gson.fromJson(detDescTarjConv.toString(), DescuentoTarjetaConvenioDetDTO.class);
        if (descConvDetDAO.insertarObtenerEstado(DescuentoTarjetaConvenioDet.fromDescuentoTarjetaConvenioDetDTO(descDTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL DET");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL DET");
        }
        return exitoInsertarDet;
    }
    //////CREATE, DESCUENTO TARJETA CONVENIO DET. -> POST

    //////UPDATE, DESCUENTO TARJETA CONVENIO CAB. -> GET
    private boolean bajaDescTarjetaConvTodo() {
        JSONParser parser = new JSONParser();
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioCab/baja/" + u + "/" + ts);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    exitoBaja = (Boolean) parser.parse(inputLine);
                }
                if (exitoBaja) {
                    boolean valor = descConvenioDAO.bajasLocal(Identity.getNomFun(), timestamp);
                    if (valor) {
                        System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                    }
                }
                br.close();
            } else {
                exitoBaja = recuperarBajaDescuentoTarjConvLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException e) {
            exitoBaja = recuperarBajaDescuentoTarjConvLocal(Identity.getNomFun(), timestamp);
        } catch (ParseException ex) {
            exitoBaja = recuperarBajaDescuentoTarjConvLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN TARJETA CONVENIO HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetDescTarjConvBaja();
            } else {
                alert2.close();
                resetDescTarjConvBaja();
            }
        } else {
            mensajeError("LOS DESCUENTOS EN TARJETA CONVENIO NO SE CANCELARON.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO TARJETA CONVENIO CAB. -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, FETCH DESCUENTO TARJETA CONVENIO
    private List<JSONObject> generarFetchDescuentoTarjetaConv(String limRowS, String offSetS) {
        List<JSONObject> listaObj = new ArrayList<JSONObject>();
        JSONParser parser = new JSONParser();
        for (DescuentoTarjetaConvenioCab desc : descConvenioDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoTarjetaConvenioCabDTO()));
                listaObj.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObj;
    }
    //////READ, FETCH DESCUENTO TARJETA CONVENIO

    //////READ, ROW COUNT DESCUENTO TARJETA CONVENIO
    private Integer generarCountDescuentoConvenioCab() {
        return descConvenioDAO.rowCount().intValue();
    }
    //////READ, ROW COUNT DESCUENTO TARJETA CONVENIO

    //////READ, ROW COUNT FILTER DESCUENTO TARJETA CONVENIO
    private Integer generarCountFilterTarjConvenioLocal(String tarjConvFilter) {
        return descConvenioDAO.rowCountFiler(tarjConvFilter).intValue();
    }
    //////READ, ROW COUNT FILTER DESCUENTO TARJETA CONVENIO

    //////READ, FETCH FILTRO DESCUENTO TARJETA CONVENIO
    private List<JSONObject> generarFetchFiltroDescuentoConvLocal(String limRowS, String offSetS, String tarjConv) {
        List<JSONObject> listaJSON = new ArrayList<>();
        JSONParser parser = new JSONParser();
        for (DescuentoTarjetaConvenioCab desc : descConvenioDAO.listarFETCHTarjeta(Long.parseLong(limRowS), Long.parseLong(offSetS), tarjConv)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoTarjetaConvenioCabDTO()));
                listaJSON.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaJSON;
    }
    //////READ, FETCH FILTRO DESCUENTO TARJETA CONVENIO

    //////READ, DÍAS
    private JSONArray generardDiasLocal() {
        JSONArray listaJson = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Dias dia : diaDAO.listar()) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(dia.toDiaDTO()));
                listaJson.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaJson;
    }
    //////READ, DÍAS

    //////READ, DESCUENTO TARJETA CONVENIO
    private JSONArray generarNomIdTarjConvenio() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (TarjetaConvenio tarj : tarjetaDAO.listarNombreId()) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(tarj.toTarjetaConvenioDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, DESCUENTO TARJETA CONVENIO

    //////READ, DESCUENTO TARJETA CONVENIO CAB. ID
    private Map recuperarFechaLong(long id) {
        Map mapeo = new HashMap();
        DescuentoTarjetaConvenioCab desc = descConvenioDAO.getById(id);
        mapeo.put("fechaAlta", desc.getFechaAlta().getTime());
        mapeo.put("fechaMod", desc.getFechaMod().getTime());
        return mapeo;
    }
    //////READ, DESCUENTO TARJETA CONVENIO CAB. ID

    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB.
    private boolean actualizarDescuentoTarjetaConv(JSONObject toStringJson) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_convenio_cab");
        j.put("data", toStringJson.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB.

    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB. MASIVO
    private boolean recuperarBajaDescuentoTarjConvLocal(String u, Timestamp ts) {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        boolean valor = false;
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_convenio_cab_masivo");
        j.put("data", "{\"estadoDesc\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U', '" + x + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a PARANA LOCAL BD ********");
                valor = descConvenioDAO.bajasLocal(u, ts);
                if (valor) {
                    System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB. MASIVO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB.
    private JSONObject registrarDescuentoConvenioCabLocal(JSONObject cabDescTarjConv) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_convenio_cab");
        j.put("data", cabDescTarjConv.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a CASSANDRA BD ********");
                obj = cabDescTarjConv;
                exitoInsertarCab = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO CAB.

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO DET.
    private boolean registrarDescConvenioDetLocal(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_convenio_det");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO DET.
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREANDO DESCUENTO TARJETA CONV. CAB.
    private JSONObject creandoJsonCabTarjConv() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject descTarjConvCab = new JSONObject();
        //**********************************************************************
        String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
        int porc = Integer.valueOf(parts[0]);
        descTarjConvCab.put("porcentajeDesc", porc);
        descTarjConvCab.put("estadoDesc", true);
        if (hashMapTarjConv.containsKey(comboBoxTarjConvenio.getSelectionModel().getSelectedItem())) {
            descTarjConvCab.put("descriTarjetaConvenio", comboBoxTarjConvenio.getSelectionModel().getSelectedItem());
            descTarjConvCab.put("tarjetaConvenio", hashMapTarjConv.get(comboBoxTarjConvenio.getSelectionModel().getSelectedItem()));
        } else {
            return new JSONObject();
        }
        //**********************************************************************
        descTarjConvCab.put("usuAlta", Identity.getNomFun());
        descTarjConvCab.put("fechaAlta", timestampJSON);
        descTarjConvCab.put("usuMod", Identity.getNomFun());
        descTarjConvCab.put("fechaMod", timestampJSON);
        //**********************************************************************
        return descTarjConvCab;
    }
    //JSON CREANDO DESCUENTO TARJETA CONV. CAB.

    //JSON CREANDO DESCUENTO TARJETA CONV. DET.
    private JSONObject creandoJsonDetTarjConv(JSONObject descTarjConvCab, JSONObject dia) {
        JSONObject descTarjConvDet = new JSONObject();
        //**********************************************************************
        descTarjConvDet.put("descuentoTarjetaConvenioCab", descTarjConvCab);
        descTarjConvDet.put("dia", dia);
        //**********************************************************************
        return descTarjConvDet;
    }
    //JSON CREANDO DESCUENTO TARJETA CONV. DET.

    //JSON BAJA DESCUENTO TARJETA CONV. CAB.
    private JSONObject bajaJsonCabTarjConv(JSONObject descTarjConvCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        descTarjConvCab.put("usuMod", Identity.getNomFun());
        descTarjConvCab.put("fechaMod", timestampJSON);
        descTarjConvCab.put("estadoDesc", false);
        descTarjConvCab.put("descuentoTarjetaConvenioDetDTO", null);
        //**********************************************************************
        return descTarjConvCab;
    }
    //JSON BAJA DESCUENTO TARJETA CONV. CAB.

    //JSON FORMATO
    private JSONObject evitarFormatoFechaTarjConvenio(JSONObject cabDescTarjConv) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonTar = (JSONObject) parser.parse(cabDescTarjConv.get("tarjetaConvenio").toString());
            jsonTar.remove("fechaAlta");
            cabDescTarjConv.put("tarjetaConvenio", jsonTar);
            return cabDescTarjConv;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }
    //JSON FORMATO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantCabTarjConv = 5;
        filaIniCabTarjConv = 0;
        pageActualCabTarjConv = 0;
        buscarTodosTarjConv = true;
        buscandoDescuentoTodo();
    }

    private void buscandoDescuentoTodo() {
        filaLimitCabTarjConv = jsonRowCount();
        buscarTodosTarjConv = true;
        paginandoCabTarjConv();
    }

    private void buscandoDescuentoFiltro() {
        String filterParam = "null";
        if (!textFielFiltroTarjConvenio.getText().contentEquals("")) {
            filterParam = textFielFiltroTarjConvenio.getText();
        }
        filaLimitCabTarjConv = jsonRowCountFilter(filterParam);
        buscarTodosTarjConv = false;
        paginandoCabTarjConv();
    }

    private void paginandoCabTarjConv() {
        paginationTarjConvenio.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabTarjConv(pageIndex);
                //**************************************************************
            }
        });
        paginationTarjConvenio.setCurrentPageIndex(this.pageActualCabTarjConv);//index inicial paginación
        paginationTarjConvenio.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabTarjConv = filaLimitCabTarjConv / filaCantCabTarjConv;//cantidad index paginación
        if (filaLimitCabTarjConv % filaCantCabTarjConv > 0) {//index EXTRA (resto de filas)***********
            pageCountCabTarjConv++;
        }
        paginationTarjConvenio.setPageCount(pageCountCabTarjConv);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabTarjConv(int pageIndex) {
        filaIniCabTarjConv = pageIndex * filaCantCabTarjConv;
        if (buscarTodosTarjConv) {
            cabTarjConvList = jsonArrayCabTarjConvFetch(filaCantCabTarjConv, filaIniCabTarjConv);
        } else {
            String filterParam = "null";
            if (!textFielFiltroTarjConvenio.getText().contentEquals("")) {
                filterParam = textFielFiltroTarjConvenio.getText();
            }
            cabTarjConvList = jsonArrayCabTarjConvFetchFiltro(filaCantCabTarjConv, filaIniCabTarjConv, filterParam);
        }
        actualizandoTablaCabTarjConv();
        return tableCabTarjConv;
    }

    private void actualizandoTablaCabTarjConv() {
        tableCabTarjConv = new TableView<>();
        cabTarjConvData = FXCollections.observableArrayList(cabTarjConvList);
        //columna Tarjeta Convenio ....................................................
        columnTarjetaConvenio = new TableColumn("Tarjeta Convenio");
        columnTarjetaConvenio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTarjetaConvenio.setMinWidth(220);
        columnTarjetaConvenio.setEditable(false);
        columnTarjetaConvenio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriTarjetaConvenio").toString());
            }
        });
        //columna Tarjeta Convenio ....................................................
        //columna Desc ....................................................
        columnDescuento = new TableColumn("Descuento");
        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuento.setMinWidth(30);
        columnDescuento.setEditable(false);
        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Lunes .......................................................
        columnCabTarjConvLunes = new TableColumn("Lunes");
        columnCabTarjConvLunes.setMinWidth(60);
        columnCabTarjConvLunes.setEditable(false);
        columnCabTarjConvLunes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvLunes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabTarjConvMartes = new TableColumn("Martes");
        columnCabTarjConvMartes.setMinWidth(60);
        columnCabTarjConvMartes.setEditable(false);
        columnCabTarjConvMartes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvMartes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabTarjConvMiercoles = new TableColumn("Miércoles");
        columnCabTarjConvMiercoles.setMinWidth(60);
        columnCabTarjConvMiercoles.setEditable(false);
        columnCabTarjConvMiercoles.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvMiercoles.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabTarjConvJueves = new TableColumn("Jueves");
        columnCabTarjConvJueves.setMinWidth(60);
        columnCabTarjConvJueves.setEditable(false);
        columnCabTarjConvJueves.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvJueves.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabTarjConvViernes = new TableColumn("Viernes");
        columnCabTarjConvViernes.setMinWidth(60);
        columnCabTarjConvViernes.setEditable(false);
        columnCabTarjConvViernes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvViernes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabTarjConvSabado = new TableColumn("Sábado");
        columnCabTarjConvSabado.setMinWidth(60);
        columnCabTarjConvSabado.setEditable(false);
        columnCabTarjConvSabado.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvSabado.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Domingo .......................................................
        columnCabTarjConvDomingo = new TableColumn("Domingo");
        columnCabTarjConvDomingo.setMinWidth(60);
        columnCabTarjConvDomingo.setEditable(false);
        columnCabTarjConvDomingo.setStyle("-fx-alignment: CENTER;");
        columnCabTarjConvDomingo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descTarjConvDetList = (JSONArray) data.getValue().get("descuentoTarjetaConvenioDetDTO");
                boolean diaBool = false;
                for (Object jsonObj : descTarjConvDetList) {
                    JSONObject descTarjConvDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descTarjConvDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Cancelar .......................................................
        columnEliminar = new TableColumn("Cancelar");
        columnEliminar.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabTarjConvBooleanTableColumn) {
                return new DescuentoTarjetaConvenioFXMLController.AddObjectsCell(tableCabTarjConv);
            }
        });
        //columna Cancelar .......................................................
        tableCabTarjConv.getColumns().addAll(columnTarjetaConvenio, columnDescuento, columnCabTarjConvDomingo,
                columnCabTarjConvLunes, columnCabTarjConvMartes, columnCabTarjConvMiercoles,
                columnCabTarjConvJueves, columnCabTarjConvViernes, columnCabTarjConvSabado, columnEliminar);
        tableCabTarjConv.setItems(cabTarjConvData);
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_tarj_conv") {
//                        
//                    }
//                    
//                        ) {
                        JSONObject tarjConvCab = tableCabTarjConv.getItems().get(index);
                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO TARJETA CONVENIO "
                                + tarjConvCab.get("descriTarjetaConvenio").toString() + "?", ok, cancel);
                        alert = true;
                        alert2.showAndWait();
                        if (alert2.getResult() == ok) {
                            tarjConvCab = bajaJsonCabTarjConv(tarjConvCab);
                            bajaCabTarjConv(tarjConvCab);
                        } else if (alert2.getResult() == cancel) {
                            alert2.close();
                        }
//                    }else {
//                        Alert alert2 = new Alert(Alert.AlertType.ERROR, "No dispone de los\npermisos necesarios.", ButtonType.CLOSE);
//                        alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.CLOSE) {
//                            alert2.close();
//                        }
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }
}
