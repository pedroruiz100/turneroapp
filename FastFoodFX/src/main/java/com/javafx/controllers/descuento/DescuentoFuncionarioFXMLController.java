/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.FuncionarioNf1;
import com.peluqueria.core.domain.FuncionarioNf2;
import com.peluqueria.core.domain.FuncionarioNf3;
import com.peluqueria.core.domain.FuncionarioNf4;
import com.peluqueria.core.domain.FuncionarioNf5;
import com.peluqueria.core.domain.FuncionarioNf6;
import com.peluqueria.core.domain.FuncionarioNf7;
import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.Seccion;
import com.peluqueria.dao.FuncionarioNf1DAO;
import com.peluqueria.dao.FuncionarioNf2DAO;
import com.peluqueria.dao.FuncionarioNf3DAO;
import com.peluqueria.dao.FuncionarioNf4DAO;
import com.peluqueria.dao.FuncionarioNf5DAO;
import com.peluqueria.dao.FuncionarioNf6DAO;
import com.peluqueria.dao.FuncionarioNf7DAO;
import com.peluqueria.dao.Nf1TipoDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.RangoFuncionarioNf1DAO;
import com.peluqueria.dao.RangoFuncionarioNf2DAO;
import com.peluqueria.dao.RangoFuncionarioNf3DAO;
import com.peluqueria.dao.RangoFuncionarioNf4DAO;
import com.peluqueria.dao.RangoFuncionarioNf5DAO;
import com.peluqueria.dao.RangoFuncionarioNf6DAO;
import com.peluqueria.dao.RangoFuncionarioNf7DAO;
import com.peluqueria.dao.SeccionDAO;
import com.peluqueria.dto.FuncionarioNf1DTO;
import com.peluqueria.dto.FuncionarioNf2DTO;
import com.peluqueria.dto.FuncionarioNf3DTO;
import com.peluqueria.dto.FuncionarioNf4DTO;
import com.peluqueria.dto.FuncionarioNf5DTO;
import com.peluqueria.dto.FuncionarioNf6DTO;
import com.peluqueria.dto.FuncionarioNf7DTO;
import com.javafx.util.Descuento;
import com.javafx.util.RemindTask;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class DescuentoFuncionarioFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private RangoFuncionarioNf1DAO rangoFuncionarioNf1DAO;
    @Autowired
    private RangoFuncionarioNf2DAO rangoFuncionarioNf2DAO;
    @Autowired
    private RangoFuncionarioNf3DAO rangoFuncionarioNf3DAO;
    @Autowired
    private RangoFuncionarioNf4DAO rangoFuncionarioNf4DAO;
    @Autowired
    private RangoFuncionarioNf5DAO rangoFuncionarioNf5DAO;
    @Autowired
    private RangoFuncionarioNf6DAO rangoFuncionarioNf6DAO;
    @Autowired
    private RangoFuncionarioNf7DAO rangoFuncionarioNf7DAO;
    @Autowired
    private FuncionarioNf1DAO funcionarioNf1DAO;
    @Autowired
    private FuncionarioNf2DAO funcionarioNf2DAO;
    @Autowired
    private FuncionarioNf3DAO funcionarioNf3DAO;
    @Autowired
    private FuncionarioNf4DAO funcionarioNf4DAO;
    @Autowired
    private FuncionarioNf5DAO funcionarioNf5DAO;
    @Autowired
    private FuncionarioNf6DAO funcionarioNf6DAO;
    @Autowired
    private FuncionarioNf7DAO funcionarioNf7DAO;
    @Autowired
    private SeccionDAO seccionDAO;

    Task copyWorker;
    private boolean alert;
    private boolean exitoInsertarDesc;
    private boolean exitoBaja;

    //PAGINATION NF1
    private int filaIniDescFuncionarioNf1, filaCantDescFuncionarioNf1, filaLimitDescFuncionarioNf1;
    private int pageCountDescFuncionarioNf1, pageActualDescFuncionarioNf1;
    private boolean buscarTodosDescFuncionarioNf1;
    private ObservableList<JSONObject> descFuncionarioDataNf1;
    private List<JSONObject> descFuncionarioListNf1;
    private TableView<JSONObject> tableDescFuncionarioNf1;
    private TableColumn<JSONObject, String> columnSeccionNf1;
    private TableColumn<JSONObject, String> columnDescuentoNf1;
    private TableColumn<JSONObject, Boolean> columnAccionesNf1;
    //PAGINATION NF1

    //PAGINATION NF2
    private int filaIniDescFuncionarioNf2, filaCantDescFuncionarioNf2, filaLimitDescFuncionarioNf2;
    private int pageCountDescFuncionarioNf2, pageActualDescFuncionarioNf2;
    private boolean buscarTodosDescFuncionarioNf2;
    private ObservableList<JSONObject> descFuncionarioDataNf2;
    private List<JSONObject> descFuncionarioListNf2;
    private TableView<JSONObject> tableDescFuncionarioNf2;
    private TableColumn<JSONObject, String> columnSeccionNf2;
    private TableColumn<JSONObject, String> columnDescuentoNf2;
    private TableColumn<JSONObject, Boolean> columnAccionesNf2;
    //PAGINATION NF2

    //PAGINATION NF3
    private int filaIniDescFuncionarioNf3, filaCantDescFuncionarioNf3, filaLimitDescFuncionarioNf3;
    private int pageCountDescFuncionarioNf3, pageActualDescFuncionarioNf3;
    private boolean buscarTodosDescFuncionarioNf3;
    private ObservableList<JSONObject> descFuncionarioDataNf3;
    private List<JSONObject> descFuncionarioListNf3;
    private TableView<JSONObject> tableDescFuncionarioNf3;
    private TableColumn<JSONObject, String> columnSeccionNf3;
    private TableColumn<JSONObject, String> columnDescuentoNf3;
    private TableColumn<JSONObject, Boolean> columnAccionesNf3;
    //PAGINATION NF3

    //PAGINATION NF4
    private int filaIniDescFuncionarioNf4, filaCantDescFuncionarioNf4, filaLimitDescFuncionarioNf4;
    private int pageCountDescFuncionarioNf4, pageActualDescFuncionarioNf4;
    private boolean buscarTodosDescFuncionarioNf4;
    private ObservableList<JSONObject> descFuncionarioDataNf4;
    private List<JSONObject> descFuncionarioListNf4;
    private TableView<JSONObject> tableDescFuncionarioNf4;
    private TableColumn<JSONObject, String> columnSeccionNf4;
    private TableColumn<JSONObject, String> columnDescuentoNf4;
    private TableColumn<JSONObject, Boolean> columnAccionesNf4;
    //PAGINATION NF4

    //PAGINATION NF5
    private int filaIniDescFuncionarioNf5, filaCantDescFuncionarioNf5, filaLimitDescFuncionarioNf5;
    private int pageCountDescFuncionarioNf5, pageActualDescFuncionarioNf5;
    private boolean buscarTodosDescFuncionarioNf5;
    private ObservableList<JSONObject> descFuncionarioDataNf5;
    private List<JSONObject> descFuncionarioListNf5;
    private TableView<JSONObject> tableDescFuncionarioNf5;
    private TableColumn<JSONObject, String> columnSeccionNf5;
    private TableColumn<JSONObject, String> columnDescuentoNf5;
    private TableColumn<JSONObject, Boolean> columnAccionesNf5;
    //PAGINATION NF5

    //PAGINATION NF6
    private int filaIniDescFuncionarioNf6, filaCantDescFuncionarioNf6, filaLimitDescFuncionarioNf6;
    private int pageCountDescFuncionarioNf6, pageActualDescFuncionarioNf6;
    private boolean buscarTodosDescFuncionarioNf6;
    private ObservableList<JSONObject> descFuncionarioDataNf6;
    private List<JSONObject> descFuncionarioListNf6;
    private TableView<JSONObject> tableDescFuncionarioNf6;
    private TableColumn<JSONObject, String> columnSeccionNf6;
    private TableColumn<JSONObject, String> columnDescuentoNf6;
    private TableColumn<JSONObject, Boolean> columnAccionesNf6;
    //PAGINATION NF6

    //PAGINATION NF7
    private int filaIniDescFuncionarioNf7, filaCantDescFuncionarioNf7, filaLimitDescFuncionarioNf7;
    private int pageCountDescFuncionarioNf7, pageActualDescFuncionarioNf7;
    private boolean buscarTodosDescFuncionarioNf7;
    private ObservableList<JSONObject> descFuncionarioDataNf7;
    private List<JSONObject> descFuncionarioListNf7;
    private TableView<JSONObject> tableDescFuncionarioNf7;
    private TableColumn<JSONObject, String> columnSeccionNf7;
    private TableColumn<JSONObject, String> columnDescuentoNf7;
    private TableColumn<JSONObject, Boolean> columnAccionesNf7;
    //PAGINATION NF7

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();

    HashMap<String, JSONObject> hashMapSeccion;
    Timer timer;
    boolean habilitar;

    @Autowired
    private Nf1TipoDAO nf1TipoDAO;

    //para inserción
    HashMap<ImageView, Nf1Tipo> hashMapNf1;
    HashMap<ImageView, Nf2Sfamilia> hashMapNf2;
    HashMap<ImageView, Nf3Sseccion> hashMapNf3;
    HashMap<ImageView, Nf4Seccion1> hashMapNf4;
    HashMap<ImageView, Nf5Seccion2> hashMapNf5;
    HashMap<ImageView, Nf6Secnom6> hashMapNf6;
    HashMap<ImageView, Nf7Secnom7> hashMapNf7;
    //para inserción
    //para consulta detalle
    HashMap<Long, Nf1Tipo> hashMapDetalleNf1;
    HashMap<Long, Nf2Sfamilia> hashMapDetalleNf2;
    HashMap<Long, Nf3Sseccion> hashMapDetalleNf3;
    HashMap<Long, Nf4Seccion1> hashMapDetalleNf4;
    HashMap<Long, Nf5Seccion2> hashMapDetalleNf5;
    HashMap<Long, Nf6Secnom6> hashMapDetalleNf6;
    HashMap<Long, Nf7Secnom7> hashMapDetalleNf7;
    //para consulta detalle
    private static JSONObject jsonCabDetalleNf;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneDescFuncionario;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelConfigFuncionario;
    @FXML
    private Label labelDescuento;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonInsertar;
    @FXML
    private CheckBox checkBoxTodasLasSecciones;
    @FXML
    private ChoiceBox<String> choiceBoxDesc;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private SplitPane splitPaneTreeView;
    @FXML
    private AnchorPane anchorPaneTitleTreeView;
    @FXML
    private Label labelTreeView;
    @FXML
    private AnchorPane anchorPaneTreeView;
    @FXML
    private TreeView<String> treeViewSecciones;
    @FXML
    private SplitPane splitPaneNf1;
    @FXML
    private AnchorPane anchorPaneHeaderNf1;
    @FXML
    private HBox vBoxFiltroNf1;
    @FXML
    private Label labelSeccionFiltroNf1;
    @FXML
    private TextField textFieldFiltroSeccionNf1;
    @FXML
    private Button buttonBuscarSeccionNf1;
    @FXML
    private Button buttonTodosNf1;
    @FXML
    private Button buttonLimpiarNf1;
    @FXML
    private AnchorPane anchorPaneNf1;
    @FXML
    private Pagination paginationDescFuncionarioNf1;
    @FXML
    private ProgressIndicator progressIndicatorNf1;
    @FXML
    private Label labelProgressNf1;
    @FXML
    private AnchorPane anchorPaneBottomNf1;
    @FXML
    private Button buttonCancelarTodasNf1;
    @FXML
    private SplitPane splitPaneNf2;
    @FXML
    private AnchorPane anchorPaneHeaderNf2;
    @FXML
    private HBox vBoxFiltroNf2;
    @FXML
    private Label labelSeccionFiltroNf2;
    @FXML
    private TextField textFieldFiltroSeccionNf2;
    @FXML
    private Button buttonBuscarSeccionNf2;
    @FXML
    private Button buttonTodosNf2;
    @FXML
    private Button buttonLimpiarNf2;
    @FXML
    private AnchorPane anchorPaneNf2;
    @FXML
    private Pagination paginationDescFuncionarioNf2;
    @FXML
    private AnchorPane anchorPaneBottomNf2;
    @FXML
    private Button buttonCancelarTodasNf2;
    @FXML
    private SplitPane splitPaneNf3;
    @FXML
    private AnchorPane anchorPaneHeaderNf3;
    @FXML
    private HBox vBoxFiltroNf3;
    @FXML
    private Label labelSeccionFiltroNf3;
    @FXML
    private TextField textFieldFiltroSeccionNf3;
    @FXML
    private Button buttonBuscarSeccionNf3;
    @FXML
    private Button buttonTodosNf3;
    @FXML
    private Button buttonLimpiarNf3;
    @FXML
    private AnchorPane anchorPaneNf3;
    @FXML
    private Pagination paginationDescFuncionarioNf3;
    @FXML
    private AnchorPane anchorPaneBottomNf3;
    @FXML
    private Button buttonCancelarTodasNf3;
    @FXML
    private SplitPane splitPaneNf4;
    @FXML
    private AnchorPane anchorPaneHeaderNf4;
    @FXML
    private HBox vBoxFiltroNf4;
    @FXML
    private Label labelSeccionFiltroNf4;
    @FXML
    private TextField textFieldFiltroSeccionNf4;
    @FXML
    private Button buttonBuscarSeccionNf4;
    @FXML
    private Button buttonTodosNf4;
    @FXML
    private Button buttonLimpiarNf4;
    @FXML
    private AnchorPane anchorPaneNf4;
    @FXML
    private Pagination paginationDescFuncionarioNf4;
    @FXML
    private AnchorPane anchorPaneBottomNf4;
    @FXML
    private Button buttonCancelarTodasNf4;
    @FXML
    private SplitPane splitPaneNf5;
    @FXML
    private AnchorPane anchorPaneHeaderNf5;
    @FXML
    private HBox vBoxFiltroNf5;
    @FXML
    private Label labelSeccionFiltroNf5;
    @FXML
    private TextField textFieldFiltroSeccionNf5;
    @FXML
    private Button buttonBuscarSeccionNf5;
    @FXML
    private Button buttonTodosNf5;
    @FXML
    private Button buttonLimpiarNf5;
    @FXML
    private AnchorPane anchorPaneNf5;
    @FXML
    private Pagination paginationDescFuncionarioNf5;
    @FXML
    private AnchorPane anchorPaneBottomNf5;
    @FXML
    private Button buttonCancelarTodasNf5;
    @FXML
    private SplitPane splitPaneNf6;
    @FXML
    private AnchorPane anchorPaneHeaderNf6;
    @FXML
    private HBox vBoxFiltroNf6;
    @FXML
    private TextField textFieldFiltroSeccionNf6;
    @FXML
    private Button buttonBuscarSeccionNf6;
    @FXML
    private Button buttonTodosNf6;
    @FXML
    private Button buttonLimpiarNf6;
    @FXML
    private AnchorPane anchorPaneNf6;
    @FXML
    private Label labelSeccionFiltroNf6;
    @FXML
    private Pagination paginationDescFuncionarioNf6;
    @FXML
    private AnchorPane anchorPaneBottomNf6;
    @FXML
    private Button buttonCancelarTodasNf6;
    @FXML
    private SplitPane splitPaneNf7;
    @FXML
    private AnchorPane anchorPaneHeaderNf7;
    @FXML
    private HBox vBoxFiltroNf7;
    @FXML
    private Label labelSeccionFiltroNf7;
    @FXML
    private TextField textFieldFiltroSeccionNf7;
    @FXML
    private Button buttonBuscarSeccionNf7;
    @FXML
    private Button buttonTodosNf7;
    @FXML
    private Button buttonLimpiarNf7;
    @FXML
    private AnchorPane anchorPaneNf7;
    @FXML
    private Pagination paginationDescFuncionarioNf7;
    @FXML
    private AnchorPane anchorPaneBottomNf7;
    @FXML
    private Button buttonCancelarTodasNf7;
    @FXML
    private CheckBox checkBoxSincro;
    @FXML
    private TabPane tabPaneNf;
    @FXML
    private Tab tabNivel1;
    @FXML
    private Tab tabNivel2;
    @FXML
    private Tab tabNivel3;
    @FXML
    private Tab tabNivel4;
    @FXML
    private Tab tabNivel5;
    @FXML
    private Tab tabNivel6;
    @FXML
    private Tab tabNivel7;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoDescFuncionario();
    }

    @FXML
    private void anchorPaneDescFuncionarioKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void checkBoxTodasLasSeccionesAction(ActionEvent event) {
        checkBoxTodasSecciones();
    }

    @FXML
    private void buttonBuscarSeccionNf1Action(ActionEvent event) {
        buscandoDescuentoFiltroNf1();
    }

    @FXML
    private void buttonTodosNf1Action(ActionEvent event) {
        buscandoDescuentoTodoNf1();
    }

    @FXML
    private void buttonLimpiarNf1Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf1Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf2Action(ActionEvent event) {
        buscandoDescuentoFiltroNf2();
    }

    @FXML
    private void buttonTodosNf2Action(ActionEvent event) {
        buscandoDescuentoTodoNf2();
    }

    @FXML
    private void buttonLimpiarNf2Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf2Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf3Action(ActionEvent event) {
        buscandoDescuentoFiltroNf3();
    }

    @FXML
    private void buttonTodosNf3Action(ActionEvent event) {
        buscandoDescuentoTodoNf3();
    }

    @FXML
    private void buttonLimpiarNf3Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf3Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf4Action(ActionEvent event) {
        buscandoDescuentoFiltroNf4();
    }

    @FXML
    private void buttonTodosNf4Action(ActionEvent event) {
        buscandoDescuentoTodoNf4();
    }

    @FXML
    private void buttonLimpiarNf4Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf4Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf5Action(ActionEvent event) {
        buscandoDescuentoFiltroNf5();
    }

    @FXML
    private void buttonTodosNf5Action(ActionEvent event) {
        buscandoDescuentoTodoNf5();
    }

    @FXML
    private void buttonLimpiarNf5Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf5Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf6Action(ActionEvent event) {
        buscandoDescuentoFiltroNf6();
    }

    @FXML
    private void buttonTodosNf6Action(ActionEvent event) {
        buscandoDescuentoTodoNf6();
    }

    @FXML
    private void buttonLimpiarNf6Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf6Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf7Action(ActionEvent event) {
        buscandoDescuentoFiltroNf7();
    }

    @FXML
    private void buttonTodosNf7Action(ActionEvent event) {
        buscandoDescuentoTodoNf7();
    }

    @FXML
    private void buttonLimpiarNf7Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf7Action(ActionEvent event) {
        cancelandoDescuentoFuncionarioTodo();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        ocultandoProgress();
        cargandoChoiceDescuento();
        cargandoNf();
        checkBoxTodasSecciones();
        alert = false;
        exitoInsertarDesc = false;
        exitoBaja = false;
        buscarTodosDescFuncionarioNf1 = true;
        listenTab();
        tabeandoNf("tabNivel1");
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/DescuentoFuncionarioFXML.fxml", 1157, 832, false);
    }

    private void viendoDetalleSeccion() {
        this.sc.loadScreen("/vista/descuento/DescuentoFuncionarioDetalleFXML.fxml", 441, 743, "/vista/descuento/DescuentoFuncionarioFXML.fxml", 1157, 832, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenTab() {
        tabPaneNf.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {
            tabeandoNf(newTab.getId());
        });
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (habilitar) {
                insertandoDescFuncionario();
            }
        }
        if (keyCode == event.getCode().F2) {
            if (habilitar) {
                if (tabPaneNf.getSelectionModel().isSelected(0)) {
                    buscandoDescuentoFiltroNf1();
                } else if (tabPaneNf.getSelectionModel().isSelected(1)) {
                    buscandoDescuentoFiltroNf2();
                } else if (tabPaneNf.getSelectionModel().isSelected(2)) {
                    buscandoDescuentoFiltroNf3();
                } else if (tabPaneNf.getSelectionModel().isSelected(3)) {
                    buscandoDescuentoFiltroNf4();
                } else if (tabPaneNf.getSelectionModel().isSelected(4)) {
                    buscandoDescuentoFiltroNf5();
                } else if (tabPaneNf.getSelectionModel().isSelected(5)) {
                    buscandoDescuentoFiltroNf6();
                } else if (tabPaneNf.getSelectionModel().isSelected(6)) {
                    buscandoDescuentoFiltroNf7();
                }
            }
        }
        if (keyCode == event.getCode().F3) {
            if (habilitar) {
                if (tabPaneNf.getSelectionModel().isSelected(0)) {
                    buscandoDescuentoTodoNf1();
                } else if (tabPaneNf.getSelectionModel().isSelected(1)) {
                    buscandoDescuentoTodoNf2();
                } else if (tabPaneNf.getSelectionModel().isSelected(2)) {
                    buscandoDescuentoTodoNf3();
                } else if (tabPaneNf.getSelectionModel().isSelected(3)) {
                    buscandoDescuentoTodoNf4();
                } else if (tabPaneNf.getSelectionModel().isSelected(4)) {
                    buscandoDescuentoTodoNf5();
                } else if (tabPaneNf.getSelectionModel().isSelected(5)) {
                    buscandoDescuentoTodoNf6();
                } else if (tabPaneNf.getSelectionModel().isSelected(6)) {
                    buscandoDescuentoTodoNf7();
                }
            }
        }
        if (keyCode == event.getCode().F5) {
            if (habilitar) {
                limpiandoBusqueda();
            }
        }
        if (keyCode == event.getCode().F12) {
            if (habilitar) {
                cancelandoDescuentoFuncionarioTodo();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (habilitar) {
                if (alert) {
                    alert = false;
                } else {
                    volviendo();
                }
            }
        }
    }

    private void checkBoxTodasSecciones() {
        if (checkBoxTodasLasSecciones.isSelected()) {
            checkBoxTodasLasSecciones.setText("Todas las secciones\n      disponibles");
            checkBoxTodasLasSecciones.setWrapText(true);
            checkBoxTodasLasSecciones.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            checkBoxTodasLasSecciones.setText("Todas las secciones\n      disponibles");
            checkBoxTodasLasSecciones.setWrapText(true);
            checkBoxTodasLasSecciones.setEffect(null);
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void limpiandoCampos(Integer nf) {
        limpiandoBusqueda();
        choiceBoxDesc.getSelectionModel().select(0);
        checkBoxTodasLasSecciones.setSelected(false);
        tabeandoNf(nf);
    }

    private void cargandoChoiceDescuento() {
        //nada de loops y mod...
        choiceBoxDesc.getItems().add("-");
        choiceBoxDesc.getItems().add("5 %");
        choiceBoxDesc.getItems().add("10 %");
        choiceBoxDesc.getItems().add("15 %");
        choiceBoxDesc.getItems().add("20 %");
        choiceBoxDesc.getItems().add("25 %");
        choiceBoxDesc.getItems().add("30 %");
        choiceBoxDesc.getItems().add("35 %");
        choiceBoxDesc.getItems().add("40 %");
        choiceBoxDesc.getItems().add("45 %");
        choiceBoxDesc.getItems().add("50 %");
        choiceBoxDesc.getSelectionModel().select(0);
    }

    private void tabeandoNf(Object param) {
        String tab = "";
        if (param instanceof Integer) {
            int p = Integer.valueOf(param.toString());
            switch (p) {
                case 1:
                    tab = "tabNivel1";
                    tabPaneNf.getSelectionModel().select(0);
                    break;
                case 2:
                    tab = "tabNivel2";
                    tabPaneNf.getSelectionModel().select(1);
                    break;
                case 3:
                    tab = "tabNivel3";
                    tabPaneNf.getSelectionModel().select(2);
                    break;
                case 4:
                    tab = "tabNivel4";
                    tabPaneNf.getSelectionModel().select(3);
                    break;
                case 5:
                    tab = "tabNivel5";
                    tabPaneNf.getSelectionModel().select(4);
                    break;
                case 6:
                    tab = "tabNivel6";
                    tabPaneNf.getSelectionModel().select(5);
                    break;
                case 7:
                    tab = "tabNivel7";
                    tabPaneNf.getSelectionModel().select(6);
                    break;
            }
        } else {
            tab = String.valueOf(param);
        }
        switch (tab) {
            case "tabNivel1":
                primeraPaginacionNf1();
                tabNivel1.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel2":
                primeraPaginacionNf2();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel3":
                primeraPaginacionNf3();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel4":
                primeraPaginacionNf4();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel5":
                primeraPaginacionNf5();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel6":
                primeraPaginacionNf6();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel7":
                primeraPaginacionNf7();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                break;
        }
    }

    private void insertandoDescFuncionario() {
        Integer nf = 0;
        String descripcionNivel = "";
        Long idNf = 0l;
        if (treeViewSecciones.getSelectionModel().getSelectedItem() != null) {
            if (hashMapNf1.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 1;
                descripcionNivel = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf1Tipo();
            } else if (hashMapNf2.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 2;
                descripcionNivel = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf2Sfamilia();
            } else if (hashMapNf3.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 3;
                descripcionNivel = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf3Sseccion();
            } else if (hashMapNf4.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 4;
                descripcionNivel = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf4Seccion1();
            } else if (hashMapNf5.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 5;
                descripcionNivel = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf5Seccion2();
            } else if (hashMapNf6.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 6;
                descripcionNivel = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf6Secnom6();
            } else if (hashMapNf7.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 7;
                descripcionNivel = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf7Secnom7();
            }
        }
        if (!checkBoxTodasLasSecciones.isSelected()) {
            if (nf == 0) {
                mensajeError("DEBE SELECCIONAR UNA SECCIÓN.");
            } else if (validacionInsercion(nf, idNf)) {
                mensajeError("LA SECCIÓN [ " + descripcionNivel.toUpperCase() + " ] DEL NIVEL [ " + nf + "]\n"
                        + "ACTUALMENTE SE ENCUENTRA EN USO.");
            } else if (choiceBoxDesc.getSelectionModel().getSelectedItem().contentEquals("-")) {
                mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
            } else {
                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR [ " + choiceBoxDesc.getValue() + " ] DESCUENTO FUNCIONARIO\nEN NIVEL "
                        + nf + ", " + descripcionNivel + " Y SUBSECCIONES ?", ok, cancel);
                this.alert = true;
                alert.showAndWait();
                if (alert.getResult() == ok) {
//                    if ("guardar_func_desc")) {
                    JSONObject descFuncionario = creandoJsonDesFuncionario(nf);
                    if (descFuncionario.isEmpty()) {
                        mensajeError("NO SE PUDO INSERTAR DESCUENTO FUNCIONARIO.\nVERIFIQUE LOS CAMPOS.");
                    } else {
                        switch (nf) {
                            case 1:
                                if (creandoDescFuncionarioNf1(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 2:
                                if (creandoDescFuncionarioNf2(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 3:
                                if (creandoDescFuncionarioNf3(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 4:
                                if (creandoDescFuncionarioNf4(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 5:
                                if (creandoDescFuncionarioNf5(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 6:
                                if (creandoDescFuncionarioNf6(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            case 7:
                                if (creandoDescFuncionarioNf7(descFuncionario)) {
                                    exitoInsertarDesc = false;
                                    limpiandoCampos(nf);
                                }
                                break;
                            default:
                                break;
                        }
                    }
//                    } else {
//                        this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/DescuentoFuncionarioFXML.fxml", 711, 849, false);
//                    }
                } else if (alert.getResult() == cancel) {
                    alert.close();
                }
            }
        } else if (choiceBoxDesc.getSelectionModel().getSelectedItem().contentEquals("-")) {
            mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR [ " + choiceBoxDesc.getValue() + " ] DESCUENTO FUNCIONARIO EN TODAS LAS SECCIONES?"
                    + "\nLAS SECCIONES CON DESCUENTO SE CANCELARÁN, ANTES DE APLICAR EL DESCUENTO GLOBAL.", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
//                if ("guardar_func_desc") {
//                    
//                }
//                
//                    ) {
                copyWorker = createWorker();
                progressIndicatorNf1.progressProperty().unbind();
                progressIndicatorNf1.progressProperty().bind(copyWorker.progressProperty());
                new Thread(copyWorker).start();
//                }else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/DescuentoFuncionarioFXML.fxml", 711, 849, false);
//                }
            } else if (alert.getResult() == cancel) {
                alert.close();
            }
        }
    }

    private void cancelandoDescuentoFuncionarioTodo() {
//        if ("baja_desc_func") {
//            
//        }
//        
//            ) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN FUNCIONARIO?", ok, cancel);
        alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            if (bajaDescFuncionarioTodo()) {
                exitoBaja = false;
                limpiandoCampos(1);
            }
            alert2.close();
        } else if (alert2.getResult() == cancel) {
            alert2.close();
        }
//        }else {
//            mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//        }
    }

    private void limpiandoBusqueda() {
        textFieldFiltroSeccionNf1.setText("");
        textFieldFiltroSeccionNf2.setText("");
        textFieldFiltroSeccionNf3.setText("");
        textFieldFiltroSeccionNf4.setText("");
        textFieldFiltroSeccionNf5.setText("");
        textFieldFiltroSeccionNf6.setText("");
        textFieldFiltroSeccionNf7.setText("");
    }

    public void viendoProgress() {
        habilitar = false;
        tabPaneNf.getSelectionModel().select(0);
        tabNivel2.setDisable(true);
        tabNivel3.setDisable(true);
        tabNivel4.setDisable(true);
        tabNivel5.setDisable(true);
        tabNivel6.setDisable(true);
        tabNivel7.setDisable(true);
        anchorPaneBottom.setDisable(true);
        paginationDescFuncionarioNf1.setDisable(true);
        vBoxFiltroNf1.setDisable(true);
        buttonCancelarTodasNf1.setDisable(true);
        progressIndicatorNf1.setVisible(true);
        labelProgressNf1.setVisible(true);
        splitPaneTreeView.setDisable(true);
    }

    public void ocultandoProgress() {
        habilitar = true;
        tabNivel2.setDisable(false);
        tabNivel3.setDisable(false);
        tabNivel4.setDisable(false);
        tabNivel5.setDisable(false);
        tabNivel6.setDisable(false);
        tabNivel7.setDisable(false);
        anchorPaneBottom.setDisable(false);
        paginationDescFuncionarioNf1.setDisable(false);
        vBoxFiltroNf1.setDisable(false);
        buttonCancelarTodasNf1.setDisable(false);
        progressIndicatorNf1.setVisible(false);
        labelProgressNf1.setVisible(false);
        anchorPaneBottom.setDisable(false);
        splitPaneTreeView.setDisable(false);
    }

    private void cargandoNf() {
        tabNivel1.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
        tabNivel1.setStyle("-fx-font-weight: bold;");
        tabNivel2.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png"))));
        tabNivel2.setStyle("-fx-font-weight: bold;");
        tabNivel3.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png"))));
        tabNivel3.setStyle("-fx-font-weight: bold;");
        tabNivel4.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png"))));
        tabNivel4.setStyle("-fx-font-weight: bold;");
        tabNivel5.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png"))));
        tabNivel5.setStyle("-fx-font-weight: bold;");
        tabNivel6.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png"))));
        tabNivel6.setStyle("-fx-font-weight: bold;");
        tabNivel7.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png"))));
        tabNivel7.setStyle("-fx-font-weight: bold;");
        generarNivelFamiliaLocal();
    }

    private Integer jsonRowCountNf1() {
        return generarCountFuncionarioLocalNf1();
    }

    private Integer jsonRowCountFilterNf1(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf1(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf1(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf1 = new ArrayList<>();
        for (Object descFuncionarioObjNf1 : generarFetchDescuentoFuncionarioNf1(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf1 = (JSONObject) descFuncionarioObjNf1;
            descFuncionarioJSONObjListNf1.add(descFuncionarioJSONObjNf1);
        }
        return descFuncionarioJSONObjListNf1;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf1(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf1 = new ArrayList<>();
        for (Object descFuncionarioObjNf1 : generarFetchFiltroDescuentoFuncionarioNf1(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf1 = (JSONObject) descFuncionarioObjNf1;
            descFuncionarioJSONObjListNf1.add(descFuncionarioJSONObjNf1);
        }
        return descFuncionarioJSONObjListNf1;
    }

    private Integer jsonRowCountNf2() {
        return generarCountFuncionarioLocalNf2();
    }

    private Integer jsonRowCountFilterNf2(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf2(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf2(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf2 = new ArrayList<>();
        for (Object descFuncionarioObjNf2 : generarFetchDescuentoFuncionarioNf2(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf2 = (JSONObject) descFuncionarioObjNf2;
            descFuncionarioJSONObjListNf2.add(descFuncionarioJSONObjNf2);
        }
        return descFuncionarioJSONObjListNf2;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf2(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf2 = new ArrayList<>();
        for (Object descFuncionarioObjNf2 : generarFetchFiltroDescuentoFuncionarioNf2(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf2 = (JSONObject) descFuncionarioObjNf2;
            descFuncionarioJSONObjListNf2.add(descFuncionarioJSONObjNf2);
        }
        return descFuncionarioJSONObjListNf2;
    }

    private Integer jsonRowCountNf3() {
        return generarCountFuncionarioLocalNf3();
    }

    private Integer jsonRowCountFilterNf3(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf3(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf3(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf3 = new ArrayList<>();
        for (Object descFuncionarioObjNf3 : generarFetchDescuentoFuncionarioNf3(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf3 = (JSONObject) descFuncionarioObjNf3;
            descFuncionarioJSONObjListNf3.add(descFuncionarioJSONObjNf3);
        }
        return descFuncionarioJSONObjListNf3;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf3(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf3 = new ArrayList<>();
        for (Object descFuncionarioObjNf3 : generarFetchFiltroDescuentoFuncionarioNf3(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf3 = (JSONObject) descFuncionarioObjNf3;
            descFuncionarioJSONObjListNf3.add(descFuncionarioJSONObjNf3);
        }
        return descFuncionarioJSONObjListNf3;
    }

    private Integer jsonRowCountNf4() {
        return generarCountFuncionarioLocalNf4();
    }

    private Integer jsonRowCountFilterNf4(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf4(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf4(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf4 = new ArrayList<>();
        for (Object descFuncionarioObjNf4 : generarFetchDescuentoFuncionarioNf4(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf4 = (JSONObject) descFuncionarioObjNf4;
            descFuncionarioJSONObjListNf4.add(descFuncionarioJSONObjNf4);
        }
        return descFuncionarioJSONObjListNf4;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf4(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf4 = new ArrayList<>();
        for (Object descFuncionarioObjNf4 : generarFetchFiltroDescuentoFuncionarioNf4(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf4 = (JSONObject) descFuncionarioObjNf4;
            descFuncionarioJSONObjListNf4.add(descFuncionarioJSONObjNf4);
        }
        return descFuncionarioJSONObjListNf4;
    }

    private Integer jsonRowCountNf5() {
        return generarCountFuncionarioLocalNf5();
    }

    private Integer jsonRowCountFilterNf5(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf5(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf5(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf5 = new ArrayList<>();
        for (Object descFuncionarioObjNf5 : generarFetchDescuentoFuncionarioNf5(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf5 = (JSONObject) descFuncionarioObjNf5;
            descFuncionarioJSONObjListNf5.add(descFuncionarioJSONObjNf5);
        }
        return descFuncionarioJSONObjListNf5;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf5(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf5 = new ArrayList<>();
        for (Object descFuncionarioObjNf5 : generarFetchFiltroDescuentoFuncionarioNf5(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf5 = (JSONObject) descFuncionarioObjNf5;
            descFuncionarioJSONObjListNf5.add(descFuncionarioJSONObjNf5);
        }
        return descFuncionarioJSONObjListNf5;
    }

    private Integer jsonRowCountNf6() {
        return generarCountFuncionarioLocalNf6();
    }

    private Integer jsonRowCountFilterNf6(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf6(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf6(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf6 = new ArrayList<>();
        for (Object descFuncionarioObjNf6 : generarFetchDescuentoFuncionarioNf6(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf6 = (JSONObject) descFuncionarioObjNf6;
            descFuncionarioJSONObjListNf6.add(descFuncionarioJSONObjNf6);
        }
        return descFuncionarioJSONObjListNf6;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf6(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf6 = new ArrayList<>();
        for (Object descFuncionarioObjNf6 : generarFetchFiltroDescuentoFuncionarioNf6(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf6 = (JSONObject) descFuncionarioObjNf6;
            descFuncionarioJSONObjListNf6.add(descFuncionarioJSONObjNf6);
        }
        return descFuncionarioJSONObjListNf6;
    }

    private Integer jsonRowCountNf7() {
        return generarCountFuncionarioLocalNf7();
    }

    private Integer jsonRowCountFilterNf7(String seccionFilter) {
        return generarCountFilterFuncionarioLocalNf7(seccionFilter);
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchNf7(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> descFuncionarioJSONObjListNf7 = new ArrayList<>();
        for (Object descFuncionarioObjNf7 : generarFetchDescuentoFuncionarioNf7(limRowS, offSetS)) {
            JSONObject descFuncionarioJSONObjNf7 = (JSONObject) descFuncionarioObjNf7;
            descFuncionarioJSONObjListNf7.add(descFuncionarioJSONObjNf7);
        }
        return descFuncionarioJSONObjListNf7;
    }

    private List<JSONObject> jsonArrayDescFuncionarioFetchFiltroNf7(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> descFuncionarioJSONObjListNf7 = new ArrayList<>();
        for (Object descFuncionarioObjNf7 : generarFetchFiltroDescuentoFuncionarioNf7(limRowS, offSetS, seccion)) {
            JSONObject descFuncionarioJSONObjNf7 = (JSONObject) descFuncionarioObjNf7;
            descFuncionarioJSONObjListNf7.add(descFuncionarioJSONObjNf7);
        }
        return descFuncionarioJSONObjListNf7;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, FUNCIONARIO Nf1 -> GET
    private JSONObject recuperarDatoFuncionarioNf1(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf1/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf1 -> GET

    //////READ, FUNCIONARIO Nf2 -> GET
    private JSONObject recuperarDatoFuncionarioNf2(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf2/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf2 -> GET

    //////READ, FUNCIONARIO Nf3 -> GET
    private JSONObject recuperarDatoFuncionarioNf3(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf3/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf3 -> GET

    //////READ, FUNCIONARIO Nf4 -> GET
    private JSONObject recuperarDatoFuncionarioNf4(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf4/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf4 -> GET

    //////READ, FUNCIONARIO Nf5 -> GET
    private JSONObject recuperarDatoFuncionarioNf5(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf5/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf5 -> GET

    //////READ, FUNCIONARIO Nf6 -> GET
    private JSONObject recuperarDatoFuncionarioNf6(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf6/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf6 -> GET

    //////READ, FUNCIONARIO Nf7 -> GET
    private JSONObject recuperarDatoFuncionarioNf7(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf7/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, FUNCIONARIO Nf7 -> GET

    //////UPDATE, DESCUENTO FUNCIONARIO Nf1 -> PUT
    private boolean bajaDescFuncionarioNf1(JSONObject descFuncionarioNf1) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf1(Long.parseLong(descFuncionarioNf1.get("idFuncionarioNf1").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf1.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf1.get("fechaAlta").toString());
                descFuncionarioNf1.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf1 = descFuncionarioNf1;
                JSONObject jsonNf1Tipo = (JSONObject) jsonDescFuncionarioNf1.get("nf1Tipo");
                jsonNf1Tipo.put("fechaAlta", null);
                jsonNf1Tipo.put("fechaMod", null);
                jsonDescFuncionarioNf1.put("nf1Tipo", jsonNf1Tipo);
                wr.write(jsonDescFuncionarioNf1.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf1);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf1);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf1);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf1.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf1.get("fechaAlta").toString()));
                descFuncionarioNf1.put("fechaAlta", null);
            }
            if (descFuncionarioNf1.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf1.get("fechaMod").toString()));
                descFuncionarioNf1.put("fechaMod", null);
            }
            FuncionarioNf1DTO funcionarioNf1DTO = gson.fromJson(descFuncionarioNf1.toString(), FuncionarioNf1DTO.class);
            funcionarioNf1DTO.setFechaAlta(fechaAlta);
            funcionarioNf1DTO.setFechaMod(fechaMod);
            funcionarioNf1DTO.setEstadoDesc(false);
            if (funcionarioNf1DAO.actualizarObtenerEstado(FuncionarioNf1.fromFuncionarioNf1DTOEntitiesFull(funcionarioNf1DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf1.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf1 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf2 -> PUT
    private boolean bajaDescFuncionarioNf2(JSONObject descFuncionarioNf2) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf2(Long.parseLong(descFuncionarioNf2.get("idFuncionarioNf2").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf2.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf2.get("fechaAlta").toString());
                descFuncionarioNf2.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf2 = descFuncionarioNf2;
                JSONObject jsonNf2Sfamilia = (JSONObject) jsonDescFuncionarioNf2.get("nf2Sfamilia");
                jsonNf2Sfamilia.put("fechaAlta", null);
                jsonNf2Sfamilia.put("fechaMod", null);
                jsonNf2Sfamilia.put("nf1Tipo", null);
                jsonDescFuncionarioNf2.put("nf2Sfamilia", jsonNf2Sfamilia);
                wr.write(jsonDescFuncionarioNf2.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf2);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf2);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf2);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf2.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf2.get("fechaAlta").toString()));
                descFuncionarioNf2.put("fechaAlta", null);
            }
            if (descFuncionarioNf2.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf2.get("fechaMod").toString()));
                descFuncionarioNf2.put("fechaMod", null);
            }
            FuncionarioNf2DTO funcionarioNf2DTO = gson.fromJson(descFuncionarioNf2.toString(), FuncionarioNf2DTO.class);
            funcionarioNf2DTO.setFechaAlta(fechaAlta);
            funcionarioNf2DTO.setFechaMod(fechaMod);
            funcionarioNf2DTO.setEstadoDesc(false);
            if (funcionarioNf2DAO.actualizarObtenerEstado(FuncionarioNf2.fromFuncionarioNf2DTOEntitiesFull(funcionarioNf2DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf2.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf2 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf3 -> PUT
    private boolean bajaDescFuncionarioNf3(JSONObject descFuncionarioNf3) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf3(Long.parseLong(descFuncionarioNf3.get("idFuncionarioNf3").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf3.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf3.get("fechaAlta").toString());
                descFuncionarioNf3.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf3");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf3 = descFuncionarioNf3;
                JSONObject jsonNf3Sseccion = (JSONObject) jsonDescFuncionarioNf3.get("nf3Sseccion");
                jsonNf3Sseccion.put("fechaAlta", null);
                jsonNf3Sseccion.put("fechaMod", null);
                jsonNf3Sseccion.put("nf2Sfamilia", null);
                jsonDescFuncionarioNf3.put("nf3Sseccion", jsonNf3Sseccion);
                wr.write(jsonDescFuncionarioNf3.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf3);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf3);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf3);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf3.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf3.get("fechaAlta").toString()));
                descFuncionarioNf3.put("fechaAlta", null);
            }
            if (descFuncionarioNf3.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf3.get("fechaMod").toString()));
                descFuncionarioNf3.put("fechaMod", null);
            }
            FuncionarioNf3DTO funcionarioNf3DTO = gson.fromJson(descFuncionarioNf3.toString(), FuncionarioNf3DTO.class);
            funcionarioNf3DTO.setFechaAlta(fechaAlta);
            funcionarioNf3DTO.setFechaMod(fechaMod);
            funcionarioNf3DTO.setEstadoDesc(false);
            if (funcionarioNf3DAO.actualizarObtenerEstado(FuncionarioNf3.fromFuncionarioNf3DTOEntitiesFull(funcionarioNf3DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf3.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf3 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf4 -> PUT
    private boolean bajaDescFuncionarioNf4(JSONObject descFuncionarioNf4) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf4(Long.parseLong(descFuncionarioNf4.get("idFuncionarioNf4").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf4.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf4.get("fechaAlta").toString());
                descFuncionarioNf4.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf4");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf4 = descFuncionarioNf4;
                JSONObject jsonNf4Seccion1 = (JSONObject) jsonDescFuncionarioNf4.get("nf4Seccion1");
                jsonNf4Seccion1.put("fechaAlta", null);
                jsonNf4Seccion1.put("fechaMod", null);
                jsonNf4Seccion1.put("nf3Sseccion", null);
                jsonDescFuncionarioNf4.put("nf4Seccion1", jsonNf4Seccion1);
                wr.write(jsonDescFuncionarioNf4.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf4);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf4);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf4);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf4.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf4.get("fechaAlta").toString()));
                descFuncionarioNf4.put("fechaAlta", null);
            }
            if (descFuncionarioNf4.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf4.get("fechaMod").toString()));
                descFuncionarioNf4.put("fechaMod", null);
            }
            FuncionarioNf4DTO funcionarioNf4DTO = gson.fromJson(descFuncionarioNf4.toString(), FuncionarioNf4DTO.class);
            funcionarioNf4DTO.setFechaAlta(fechaAlta);
            funcionarioNf4DTO.setFechaMod(fechaMod);
            funcionarioNf4DTO.setEstadoDesc(false);
            if (funcionarioNf4DAO.actualizarObtenerEstado(FuncionarioNf4.fromFuncionarioNf4DTOEntitiesFull(funcionarioNf4DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf4.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf4 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf5 -> PUT
    private boolean bajaDescFuncionarioNf5(JSONObject descFuncionarioNf5) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf5(Long.parseLong(descFuncionarioNf5.get("idFuncionarioNf5").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf5.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf5.get("fechaAlta").toString());
                descFuncionarioNf5.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf5");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf5 = descFuncionarioNf5;
                JSONObject jsonNf5Seccion2 = (JSONObject) jsonDescFuncionarioNf5.get("nf5Seccion2");
                jsonNf5Seccion2.put("fechaAlta", null);
                jsonNf5Seccion2.put("fechaMod", null);
                jsonNf5Seccion2.put("nf4Seccion1", null);
                jsonDescFuncionarioNf5.put("nf5Seccion2", jsonNf5Seccion2);
                wr.write(jsonDescFuncionarioNf5.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf5);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf5);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf5);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf5.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf5.get("fechaAlta").toString()));
                descFuncionarioNf5.put("fechaAlta", null);
            }
            if (descFuncionarioNf5.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf5.get("fechaMod").toString()));
                descFuncionarioNf5.put("fechaMod", null);
            }
            FuncionarioNf5DTO funcionarioNf5DTO = gson.fromJson(descFuncionarioNf5.toString(), FuncionarioNf5DTO.class);
            funcionarioNf5DTO.setFechaAlta(fechaAlta);
            funcionarioNf5DTO.setFechaMod(fechaMod);
            funcionarioNf5DTO.setEstadoDesc(false);
            if (funcionarioNf5DAO.actualizarObtenerEstado(FuncionarioNf5.fromFuncionarioNf5DTOEntitiesFull(funcionarioNf5DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf5.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf5 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf6 -> PUT
    private boolean bajaDescFuncionarioNf6(JSONObject descFuncionarioNf6) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf6(Long.parseLong(descFuncionarioNf6.get("idFuncionarioNf6").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf6.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf6.get("fechaAlta").toString());
                descFuncionarioNf6.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf6");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf6 = descFuncionarioNf6;
                JSONObject jsonNf6Secnom6 = (JSONObject) jsonDescFuncionarioNf6.get("nf6Secnom6");
                jsonNf6Secnom6.put("fechaAlta", null);
                jsonNf6Secnom6.put("fechaMod", null);
                jsonNf6Secnom6.put("nf5Seccion2", null);
                jsonDescFuncionarioNf6.put("nf6Secnom6", jsonNf6Secnom6);
                wr.write(jsonDescFuncionarioNf6.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf6);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf6);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf6);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf6.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf6.get("fechaAlta").toString()));
                descFuncionarioNf6.put("fechaAlta", null);
            }
            if (descFuncionarioNf6.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf6.get("fechaMod").toString()));
                descFuncionarioNf6.put("fechaMod", null);
            }
            FuncionarioNf6DTO funcionarioNf6DTO = gson.fromJson(descFuncionarioNf6.toString(), FuncionarioNf6DTO.class);
            funcionarioNf6DTO.setFechaAlta(fechaAlta);
            funcionarioNf6DTO.setFechaMod(fechaMod);
            funcionarioNf6DTO.setEstadoDesc(false);
            if (funcionarioNf6DAO.actualizarObtenerEstado(FuncionarioNf6.fromFuncionarioNf6DTOEntitiesFull(funcionarioNf6DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf6.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf6 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO Nf7 -> PUT
    private boolean bajaDescFuncionarioNf7(JSONObject descFuncionarioNf7) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoFuncionarioNf7(Long.parseLong(descFuncionarioNf7.get("idFuncionarioNf7").toString())) != null) {
                status = true;
            }
            if (descFuncionarioNf7.get("fechaAlta") != null) {
                Timestamp t = Utilidades.objectToTimestamp(descFuncionarioNf7.get("fechaAlta").toString());
                descFuncionarioNf7.put("fechaAlta", t.getTime());
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf7");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf7 = descFuncionarioNf7;
                JSONObject jsonNf7Secnom7 = (JSONObject) jsonDescFuncionarioNf7.get("nf7Secnom7");
                jsonNf7Secnom7.put("fechaAlta", null);
                jsonNf7Secnom7.put("fechaMod", null);
                jsonNf7Secnom7.put("nf6Secnom6", null);
                jsonDescFuncionarioNf7.put("nf7Secnom7", jsonNf7Secnom7);
                wr.write(jsonDescFuncionarioNf7.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescNfs(descFuncionarioNf7);
                }
            } else {
                exitoBaja = actualizarDescNfs(descFuncionarioNf7);
            }

        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescNfs(descFuncionarioNf7);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFuncionarioNf7.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(descFuncionarioNf7.get("fechaAlta").toString()));
                descFuncionarioNf7.put("fechaAlta", null);
            }
            if (descFuncionarioNf7.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFuncionarioNf7.get("fechaMod").toString()));
                descFuncionarioNf7.put("fechaMod", null);
            }
            FuncionarioNf7DTO funcionarioNf7DTO = gson.fromJson(descFuncionarioNf7.toString(), FuncionarioNf7DTO.class);
            funcionarioNf7DTO.setFechaAlta(fechaAlta);
            funcionarioNf7DTO.setFechaMod(fechaMod);
            funcionarioNf7DTO.setEstadoDesc(false);
            if (funcionarioNf7DAO.actualizarObtenerEstado(FuncionarioNf7.fromFuncionarioNf7DTOEntitiesFull(funcionarioNf7DTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FUNCIONARIO CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FUNCIONARIO Nf7.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO Nf7 -> PUT

    //////UPDATE, DESCUENTO FUNCIONARIO BAJA MASIVA -> GET
    private boolean bajaDescFuncionarioTodo() {
        JSONParser parser = new JSONParser();
        String inputLine;
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/bajaMasiva/baja");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(creandoJsonBajaDescMasivo(u, ts).toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    if (exitoBaja) {
                        if (funcionarioNf1DAO.bajasLocal(Identity.getNomFun(), timestamp) && funcionarioNf2DAO.bajasLocal(Identity.getNomFun(), timestamp)
                                && funcionarioNf3DAO.bajasLocal(Identity.getNomFun(), timestamp) && funcionarioNf4DAO.bajasLocal(Identity.getNomFun(), timestamp)
                                && funcionarioNf5DAO.bajasLocal(Identity.getNomFun(), timestamp) && funcionarioNf6DAO.bajasLocal(Identity.getNomFun(), timestamp)
                                && funcionarioNf7DAO.bajasLocal(Identity.getNomFun(), timestamp)) {
                            System.out.println("-->> DATOS ACTUALIZADOS PARANA LOCAL BD <<-- ");
                        }
                    }
                    br.close();
                } else {
                    exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
                }
            } else {
                exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN FUNCIONARIO HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LOS DESCUENTOS EN FUNCIONARIO NO SE CANCELARON.", ButtonType.CLOSE);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.CLOSE) {
                alert2.close();
            }
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO FUNCIONARIO BAJA MASIVA -> GET

    //////CREATE, DESCUENTO FUNCIONARIO NF1 -> POST
    private boolean creandoDescFuncionarioNf1(JSONObject descFuncionarioNf1) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf1(rangoFuncionarioNf1DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf1.put("idFuncionarioNf1", CajaDatos.getIdDescuentoFuncionarioNf1());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf1/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf1 = descFuncionarioNf1;
                JSONObject jsonNf1Tipo = (JSONObject) jsonDescFuncionarioNf1.get("nf1Tipo");
                jsonNf1Tipo.put("fechaAlta", null);
                jsonNf1Tipo.put("fechaMod", null);
                jsonDescFuncionarioNf1.put("nf1Tipo", jsonNf1Tipo);
                wr.write(jsonDescFuncionarioNf1.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf1 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf1.get("idFuncionarioNf1") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf1 = registrarDescuentoFuncLocalNf(descFuncionarioNf1, 1);
                }
            } else {
                descFuncionarioNf1 = registrarDescuentoFuncLocalNf(descFuncionarioNf1, 1);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf1 = registrarDescuentoFuncLocalNf(descFuncionarioNf1, 1);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf1.put("fechaAlta", null);
            descFuncionarioNf1.put("fechaMod", null);
            FuncionarioNf1DTO funcionarioNf1DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf1.toString()), FuncionarioNf1DTO.class);
            funcionarioNf1DTO.setFechaAlta(timestamp);
            funcionarioNf1DTO.setFechaMod(timestamp);
            FuncionarioNf1 funcionarioNf1 = funcionarioNf1DAO.insertarObtenerObjeto(FuncionarioNf1.fromFuncionarioNf1DTOEntitiesFull(funcionarioNf1DTO));
            if (funcionarioNf1 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF1 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF2 -> POST
    private boolean creandoDescFuncionarioNf2(JSONObject descFuncionarioNf2) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf2(rangoFuncionarioNf2DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf2.put("idFuncionarioNf2", CajaDatos.getIdDescuentoFuncionarioNf2());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf2/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf2 = descFuncionarioNf2;
                JSONObject jsonNf2Sfamilia = (JSONObject) jsonDescFuncionarioNf2.get("nf2Sfamilia");
                jsonNf2Sfamilia.put("fechaAlta", null);
                jsonNf2Sfamilia.put("fechaMod", null);
                jsonDescFuncionarioNf2.put("nf2Sfamilia", jsonNf2Sfamilia);
                wr.write(jsonDescFuncionarioNf2.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf2 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf2.get("idFuncionarioNf2") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf2 = registrarDescuentoFuncLocalNf(descFuncionarioNf2, 2);
                }
            } else {
                descFuncionarioNf2 = registrarDescuentoFuncLocalNf(descFuncionarioNf2, 2);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf2 = registrarDescuentoFuncLocalNf(descFuncionarioNf2, 2);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf2.put("fechaAlta", null);
            descFuncionarioNf2.put("fechaMod", null);
            FuncionarioNf2DTO funcionarioNf2DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf2.toString()), FuncionarioNf2DTO.class);
            funcionarioNf2DTO.setFechaAlta(timestamp);
            funcionarioNf2DTO.setFechaMod(timestamp);
            FuncionarioNf2 funcionarioNf2 = funcionarioNf2DAO.insertarObtenerObjeto(FuncionarioNf2.fromFuncionarioNf2DTOEntitiesFull(funcionarioNf2DTO));
            if (funcionarioNf2 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF2 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF3 -> POST
    private boolean creandoDescFuncionarioNf3(JSONObject descFuncionarioNf3) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf3(rangoFuncionarioNf3DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf3.put("idFuncionarioNf3", CajaDatos.getIdDescuentoFuncionarioNf3());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf3/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf3 = descFuncionarioNf3;
                JSONObject jsonNf3Sseccion = (JSONObject) jsonDescFuncionarioNf3.get("nf3Sseccion");
                jsonNf3Sseccion.put("fechaAlta", null);
                jsonNf3Sseccion.put("fechaMod", null);
                jsonDescFuncionarioNf3.put("nf3Sseccion", jsonNf3Sseccion);
                wr.write(jsonDescFuncionarioNf3.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf3 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf3.get("idFuncionarioNf3") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf3 = registrarDescuentoFuncLocalNf(descFuncionarioNf3, 3);
                }
            } else {
                descFuncionarioNf3 = registrarDescuentoFuncLocalNf(descFuncionarioNf3, 3);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf3 = registrarDescuentoFuncLocalNf(descFuncionarioNf3, 3);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf3.put("fechaAlta", null);
            descFuncionarioNf3.put("fechaMod", null);
            FuncionarioNf3DTO funcionarioNf3DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf3.toString()), FuncionarioNf3DTO.class);
            funcionarioNf3DTO.setFechaAlta(timestamp);
            funcionarioNf3DTO.setFechaMod(timestamp);
            FuncionarioNf3 funcionarioNf3 = funcionarioNf3DAO.insertarObtenerObjeto(FuncionarioNf3.fromFuncionarioNf3DTOEntitiesFull(funcionarioNf3DTO));
            if (funcionarioNf3 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF3 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF4 -> POST
    private boolean creandoDescFuncionarioNf4(JSONObject descFuncionarioNf4) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf4(rangoFuncionarioNf4DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf4.put("idFuncionarioNf4", CajaDatos.getIdDescuentoFuncionarioNf4());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf4/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf4 = descFuncionarioNf4;
                JSONObject jsonNf4Seccion1 = (JSONObject) jsonDescFuncionarioNf4.get("nf4Seccion1");
                jsonNf4Seccion1.put("fechaAlta", null);
                jsonNf4Seccion1.put("fechaMod", null);
                jsonDescFuncionarioNf4.put("nf4Seccion1", jsonNf4Seccion1);
                wr.write(jsonDescFuncionarioNf4.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf4 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf4.get("idFuncionarioNf4") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf4 = registrarDescuentoFuncLocalNf(descFuncionarioNf4, 4);
                }
            } else {
                descFuncionarioNf4 = registrarDescuentoFuncLocalNf(descFuncionarioNf4, 4);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf4 = registrarDescuentoFuncLocalNf(descFuncionarioNf4, 4);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf4.put("fechaAlta", null);
            descFuncionarioNf4.put("fechaMod", null);
            FuncionarioNf4DTO funcionarioNf4DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf4.toString()), FuncionarioNf4DTO.class);
            funcionarioNf4DTO.setFechaAlta(timestamp);
            funcionarioNf4DTO.setFechaMod(timestamp);
            FuncionarioNf4 funcionarioNf4 = funcionarioNf4DAO.insertarObtenerObjeto(FuncionarioNf4.fromFuncionarioNf4DTOEntitiesFull(funcionarioNf4DTO));
            if (funcionarioNf4 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF4 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF5 -> POST
    private boolean creandoDescFuncionarioNf5(JSONObject descFuncionarioNf5) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf5(rangoFuncionarioNf5DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf5.put("idFuncionarioNf5", CajaDatos.getIdDescuentoFuncionarioNf5());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf5/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf5 = descFuncionarioNf5;
                JSONObject jsonNf5Seccion2 = (JSONObject) jsonDescFuncionarioNf5.get("nf5Seccion2");
                jsonNf5Seccion2.put("fechaAlta", null);
                jsonNf5Seccion2.put("fechaMod", null);
                jsonDescFuncionarioNf5.put("nf5Seccion2", jsonNf5Seccion2);
                wr.write(jsonDescFuncionarioNf5.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf5 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf5.get("idFuncionarioNf5") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf5 = registrarDescuentoFuncLocalNf(descFuncionarioNf5, 5);
                }
            } else {
                descFuncionarioNf5 = registrarDescuentoFuncLocalNf(descFuncionarioNf5, 5);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf5 = registrarDescuentoFuncLocalNf(descFuncionarioNf5, 5);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf5.put("fechaAlta", null);
            descFuncionarioNf5.put("fechaMod", null);
            FuncionarioNf5DTO funcionarioNf5DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf5.toString()), FuncionarioNf5DTO.class);
            funcionarioNf5DTO.setFechaAlta(timestamp);
            funcionarioNf5DTO.setFechaMod(timestamp);
            FuncionarioNf5 funcionarioNf5 = funcionarioNf5DAO.insertarObtenerObjeto(FuncionarioNf5.fromFuncionarioNf5DTOEntitiesFull(funcionarioNf5DTO));
            if (funcionarioNf5 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF5 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF6 -> POST
    private boolean creandoDescFuncionarioNf6(JSONObject descFuncionarioNf6) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf6(rangoFuncionarioNf6DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf6.put("idFuncionarioNf6", CajaDatos.getIdDescuentoFuncionarioNf6());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf6/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf6 = descFuncionarioNf6;
                JSONObject jsonNf6Secnom6 = (JSONObject) jsonDescFuncionarioNf6.get("nf6Secnom6");
                jsonNf6Secnom6.put("fechaAlta", null);
                jsonNf6Secnom6.put("fechaMod", null);
                jsonDescFuncionarioNf6.put("nf6Secnom6", jsonNf6Secnom6);
                wr.write(jsonDescFuncionarioNf6.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf6 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf6.get("idFuncionarioNf6") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf6 = registrarDescuentoFuncLocalNf(descFuncionarioNf6, 6);
                }
            } else {
                descFuncionarioNf6 = registrarDescuentoFuncLocalNf(descFuncionarioNf6, 6);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf6 = registrarDescuentoFuncLocalNf(descFuncionarioNf6, 6);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf6.put("fechaAlta", null);
            descFuncionarioNf6.put("fechaMod", null);
            FuncionarioNf6DTO funcionarioNf6DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf6.toString()), FuncionarioNf6DTO.class);
            funcionarioNf6DTO.setFechaAlta(timestamp);
            funcionarioNf6DTO.setFechaMod(timestamp);
            FuncionarioNf6 funcionarioNf6 = funcionarioNf6DAO.insertarObtenerObjeto(FuncionarioNf6.fromFuncionarioNf6DTOEntitiesFull(funcionarioNf6DTO));
            if (funcionarioNf6 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF6 -> POST

    //////CREATE, DESCUENTO FUNCIONARIO NF7 -> POST
    private boolean creandoDescFuncionarioNf7(JSONObject descFuncionarioNf7) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            CajaDatos.setIdDescuentoFuncionarioNf7(rangoFuncionarioNf7DAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            descFuncionarioNf7.put("idFuncionarioNf7", CajaDatos.getIdDescuentoFuncionarioNf7());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf7/insertarObtenerObj");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFuncionarioNf7 = descFuncionarioNf7;
                JSONObject jsonNf7Secnom7 = (JSONObject) jsonDescFuncionarioNf7.get("nf7Secnom7");
                jsonNf7Secnom7.put("fechaAlta", null);
                jsonNf7Secnom7.put("fechaMod", null);
                jsonDescFuncionarioNf7.put("nf7Secnom7", jsonNf7Secnom7);
                wr.write(jsonDescFuncionarioNf7.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        descFuncionarioNf7 = (JSONObject) parser.parse(inputLine);
                        if (descFuncionarioNf7.get("idFuncionarioNf7") != null) {
                            exitoInsertarDesc = true;
                        }
                    }
                    br.close();
                } else {
                    descFuncionarioNf7 = registrarDescuentoFuncLocalNf(descFuncionarioNf7, 7);
                }
            } else {
                descFuncionarioNf7 = registrarDescuentoFuncLocalNf(descFuncionarioNf7, 7);
            }
        } catch (IOException | ParseException ex) {
            descFuncionarioNf7 = registrarDescuentoFuncLocalNf(descFuncionarioNf7, 7);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarDesc) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            descFuncionarioNf7.put("fechaAlta", null);
            descFuncionarioNf7.put("fechaMod", null);
            FuncionarioNf7DTO funcionarioNf7DTO = gson.fromJson(Utilidades.setToJson(descFuncionarioNf7.toString()), FuncionarioNf7DTO.class);
            funcionarioNf7DTO.setFechaAlta(timestamp);
            funcionarioNf7DTO.setFechaMod(timestamp);
            FuncionarioNf7 funcionarioNf7 = funcionarioNf7DAO.insertarObtenerObjeto(FuncionarioNf7.fromFuncionarioNf7DTOEntitiesFull(funcionarioNf7DTO));
            if (funcionarioNf7 != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FUNCIONARIO");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FUNCIONARIO");
            }
            //CIERRE INSERCION food_local
        }
        return exitoInsertarDesc;
    }
    //////CREATE, DESCUENTO FUNCIONARIO NF7 -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, VALIDACIÓN INSERCIÓN
    private boolean validacionInsercion(int nf, long idNf) {
        boolean insercion = true;
        switch (nf) {
            case 1:
                insercion = funcionarioNf1DAO.validacionInsercion(idNf);
                break;
            case 2:
                insercion = funcionarioNf2DAO.validacionInsercion(idNf);
                break;
            case 3:
                insercion = funcionarioNf3DAO.validacionInsercion(idNf);
                break;
            case 4:
                insercion = funcionarioNf4DAO.validacionInsercion(idNf);
                break;
            case 5:
                insercion = funcionarioNf5DAO.validacionInsercion(idNf);
                break;
            case 6:
                insercion = funcionarioNf6DAO.validacionInsercion(idNf);
                break;
            case 7:
                insercion = funcionarioNf7DAO.validacionInsercion(idNf);
                break;
        }
        return insercion;
    }
    //////READ, VALIDACIÓN INSERCIÓN

    //////UPDATE, PENDIENTES - FUNCIONARIO NF
    private boolean actualizarDescNfs(JSONObject descFuncionario) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        if (descFuncionario.containsKey("nf1Tipo")) {
            j.put("table", "funcionario_nf1");
        } else if (descFuncionario.containsKey("nf2Sfamilia")) {
            j.put("table", "funcionario_nf2");
        } else if (descFuncionario.containsKey("nf3Sseccion")) {
            j.put("table", "funcionario_nf3");
        } else if (descFuncionario.containsKey("nf4Seccion1")) {
            j.put("table", "funcionario_nf4");
        } else if (descFuncionario.containsKey("nf5Seccion2")) {
            j.put("table", "funcionario_nf5");
        } else if (descFuncionario.containsKey("nf6Secnom6")) {
            j.put("table", "funcionario_nf6");
        } else if (descFuncionario.containsKey("nf7Secnom7")) {
            j.put("table", "funcionario_nf7");
        }
        j.put("data", descFuncionario.toString());
        String msj = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U','" + msj + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - FUNCIONARIO NF

    //////UPDATE, PENDIENTES - SECCION FUNC MASIVO
    private boolean recuperarBajavLocal(String u, Timestamp ts) {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        boolean valor = false;
        JSONObject j = new JSONObject();
        j.put("table", "funcionario_nf_masivo");
        j.put("data", "{\"estadoDesc\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U', '" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro en HANDLER PARANA BD ********");
                if (funcionarioNf1DAO.bajasLocal(u, ts) && funcionarioNf2DAO.bajasLocal(u, ts)
                        && funcionarioNf3DAO.bajasLocal(u, ts) && funcionarioNf4DAO.bajasLocal(u, ts)
                        && funcionarioNf5DAO.bajasLocal(u, ts) && funcionarioNf6DAO.bajasLocal(u, ts) && funcionarioNf7DAO.bajasLocal(u, ts)) {
                    valor = true;
                    System.out.println("-->> DATOS ACTUALIZADOS PARANA LOCAL BD <<-- " + valor);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - SECCION FUNC MASIVO

    //////CREATE, PENDIENTES - SECCION FUNC
    private JSONObject registrarDescuentoFuncLocalNf(JSONObject descFuncionario, int nf) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        switch (nf) {
            case 1:
                j.put("table", "funcionario_nf1");
                break;
            case 2:
                j.put("table", "funcionario_nf2");
                break;
            case 3:
                j.put("table", "funcionario_nf3");
                break;
            case 4:
                j.put("table", "funcionario_nf4");
                break;
            case 5:
                j.put("table", "funcionario_nf5");
                break;
            case 6:
                j.put("table", "funcionario_nf6");
                break;
            case 7:
                j.put("table", "funcionario_nf7");
                break;
            default:
                break;
        }
        j.put("data", descFuncionario.toString());
        String msj = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + msj + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                obj = descFuncionario;
                exitoInsertarDesc = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////CREATE, PENDIENTES - SECCION FUNC

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf1
    private Integer generarCountFilterFuncionarioLocalNf1(String seccionFilter) {
        return (int) funcionarioNf1DAO.rowCountFilterNf1(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf1

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf1
    private Integer generarCountFuncionarioLocalNf1() {
        return funcionarioNf1DAO.rowCountNf1().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf1

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf1
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf1(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf1 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf1 funcionarioNf1 : funcionarioNf1DAO.listarFetchFilterNf1(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf1 = (JSONObject) parser.parse(gson.toJson(funcionarioNf1.toFuncionarioNf1DTO()));
                jsonArrayNf1.add(jsonObjectNf1);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf1;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf1

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf1
    private JSONArray generarFetchDescuentoFuncionarioNf1(String limRowS, String offSetS) {
        JSONArray jsonArrayNf1 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf1 funcionarioNf1 : funcionarioNf1DAO.listarFetchNf1(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf1 = (JSONObject) parser.parse(gson.toJson(funcionarioNf1.toFuncionarioNf1DTO()));
                jsonArrayNf1.add(jsonObjectNf1);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf1;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf1

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf2
    private Integer generarCountFilterFuncionarioLocalNf2(String seccionFilter) {
        return (int) funcionarioNf2DAO.rowCountFilterNf2(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf2

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf2
    private Integer generarCountFuncionarioLocalNf2() {
        return funcionarioNf2DAO.rowCountNf2().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf2

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf2
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf2(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf2 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf2 funcionarioNf2 : funcionarioNf2DAO.listarFetchFilterNf2(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf2 = (JSONObject) parser.parse(gson.toJson(funcionarioNf2.toFuncionarioNf2DTONf1()));
                jsonArrayNf2.add(jsonObjectNf2);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf2;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf2

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf2
    private JSONArray generarFetchDescuentoFuncionarioNf2(String limRowS, String offSetS) {
        JSONArray jsonArrayNf2 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf2 funcionarioNf2 : funcionarioNf2DAO.listarFetchNf2(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf2 = (JSONObject) parser.parse(gson.toJson(funcionarioNf2.toFuncionarioNf2DTONf1()));
                jsonArrayNf2.add(jsonObjectNf2);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf2;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf2

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf3
    private Integer generarCountFilterFuncionarioLocalNf3(String seccionFilter) {
        return (int) funcionarioNf3DAO.rowCountFilterNf3(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf3

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf3
    private Integer generarCountFuncionarioLocalNf3() {
        return funcionarioNf3DAO.rowCountNf3().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf3

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf3
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf3(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf3 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf3 funcionarioNf3 : funcionarioNf3DAO.listarFetchFilterNf3(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf3 = (JSONObject) parser.parse(gson.toJson(funcionarioNf3.toFuncionarioNf3DTONf2()));
                jsonArrayNf3.add(jsonObjectNf3);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf3;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf3

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf3
    private JSONArray generarFetchDescuentoFuncionarioNf3(String limRowS, String offSetS) {
        JSONArray jsonArrayNf3 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf3 funcionarioNf3 : funcionarioNf3DAO.listarFetchNf3(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf3 = (JSONObject) parser.parse(gson.toJson(funcionarioNf3.toFuncionarioNf3DTONf2()));
                jsonArrayNf3.add(jsonObjectNf3);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf3;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf3

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf4
    private Integer generarCountFilterFuncionarioLocalNf4(String seccionFilter) {
        return (int) funcionarioNf4DAO.rowCountFilterNf4(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf4

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf4
    private Integer generarCountFuncionarioLocalNf4() {
        return funcionarioNf4DAO.rowCountNf4().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf4

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf4
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf4(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf4 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf4 funcionarioNf4 : funcionarioNf4DAO.listarFetchFilterNf4(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf4 = (JSONObject) parser.parse(gson.toJson(funcionarioNf4.toFuncionarioNf4DTONf3()));
                jsonArrayNf4.add(jsonObjectNf4);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf4;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf4

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf4
    private JSONArray generarFetchDescuentoFuncionarioNf4(String limRowS, String offSetS) {
        JSONArray jsonArrayNf4 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf4 funcionarioNf4 : funcionarioNf4DAO.listarFetchNf4(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf4 = (JSONObject) parser.parse(gson.toJson(funcionarioNf4.toFuncionarioNf4DTONf3()));
                jsonArrayNf4.add(jsonObjectNf4);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf4;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf4

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf5
    private Integer generarCountFilterFuncionarioLocalNf5(String seccionFilter) {
        return (int) funcionarioNf5DAO.rowCountFilterNf5(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf5

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf5
    private Integer generarCountFuncionarioLocalNf5() {
        return funcionarioNf5DAO.rowCountNf5().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf5

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf5
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf5(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf5 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf5 funcionarioNf5 : funcionarioNf5DAO.listarFetchFilterNf5(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf5 = (JSONObject) parser.parse(gson.toJson(funcionarioNf5.toFuncionarioNf5DTONf4()));
                jsonArrayNf5.add(jsonObjectNf5);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf5;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf5

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf5
    private JSONArray generarFetchDescuentoFuncionarioNf5(String limRowS, String offSetS) {
        JSONArray listaObjNf5 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf5 funcionarioNf5 : funcionarioNf5DAO.listarFetchNf5(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf5 = (JSONObject) parser.parse(gson.toJson(funcionarioNf5.toFuncionarioNf5DTONf4()));
                listaObjNf5.add(jsonObjectNf5);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObjNf5;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf5

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf6
    private Integer generarCountFilterFuncionarioLocalNf6(String seccionFilter) {
        return (int) funcionarioNf6DAO.rowCountFilterNf6(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf6

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf6
    private Integer generarCountFuncionarioLocalNf6() {
        return funcionarioNf6DAO.rowCountNf6().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf6

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf6
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf6(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf6 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf6 funcionarioNf6 : funcionarioNf6DAO.listarFetchFilterNf6(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf6 = (JSONObject) parser.parse(gson.toJson(funcionarioNf6.toFuncionarioNf6DTONf5()));
                jsonArrayNf6.add(jsonObjectNf6);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf6;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf6

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf6
    private JSONArray generarFetchDescuentoFuncionarioNf6(String limRowS, String offSetS) {
        JSONArray listaObjNf6 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf6 funcionarioNf6 : funcionarioNf6DAO.listarFetchNf6(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf6 = (JSONObject) parser.parse(gson.toJson(funcionarioNf6.toFuncionarioNf6DTONf5()));
                listaObjNf6.add(jsonObjectNf6);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObjNf6;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf6

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf7
    private Integer generarCountFilterFuncionarioLocalNf7(String seccionFilter) {
        return (int) funcionarioNf7DAO.rowCountFilterNf7(seccionFilter);
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO FILTER Nf7

    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf7
    private Integer generarCountFuncionarioLocalNf7() {
        return funcionarioNf7DAO.rowCountNf7().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FUNCIONARIO Nf7

    //////READ, FETCH DESCUENTO FUNCIONARIO Nf7
    private JSONArray generarFetchFiltroDescuentoFuncionarioNf7(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArrayNf7 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf7 funcionarioNf7 : funcionarioNf7DAO.listarFetchFilterNf7(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject jsonObjectNf7 = (JSONObject) parser.parse(gson.toJson(funcionarioNf7.toFuncionarioNf7DTONf6()));
                jsonArrayNf7.add(jsonObjectNf7);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArrayNf7;
    }
    //////READ, FETCH DESCUENTO FUNCIONARIO Nf7

    //////READ, DESCUENTO FUNCIONARIO FETCH Nf7
    private JSONArray generarFetchDescuentoFuncionarioNf7(String limRowS, String offSetS) {
        JSONArray listaObjNf7 = new JSONArray();
        JSONParser parser = new JSONParser();
        for (FuncionarioNf7 funcionarioNf7 : funcionarioNf7DAO.listarFetchNf7(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject jsonObjectNf7 = (JSONObject) parser.parse(gson.toJson(funcionarioNf7.toFuncionarioNf7DTONf6()));
                listaObjNf7.add(jsonObjectNf7);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObjNf7;
    }
    //////READ, DESCUENTO FUNCIONARIO FETCH Nf7

    //////READ, SECCIÓN
    private JSONArray generarSeccionLocal() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Seccion sec : seccionDAO.listarFuncionarioNombreId()) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(sec.toSeccionDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, SECCIÓN

    //////READ, NF
    private void generarNivelFamiliaLocal() {
        TreeItem<String> treeItemRoot = new TreeItem<>("ÁRBOL DE SECCIONES");
        treeItemRoot.setExpanded(true);
        //y sí... mucho recurso...
        hashMapNf1 = new HashMap<>();
        hashMapNf2 = new HashMap<>();
        hashMapNf3 = new HashMap<>();
        hashMapNf4 = new HashMap<>();
        hashMapNf5 = new HashMap<>();
        hashMapNf6 = new HashMap<>();
        hashMapNf7 = new HashMap<>();
        hashMapDetalleNf1 = new HashMap<>();
        hashMapDetalleNf2 = new HashMap<>();
        hashMapDetalleNf3 = new HashMap<>();
        hashMapDetalleNf4 = new HashMap<>();
        hashMapDetalleNf5 = new HashMap<>();
        hashMapDetalleNf6 = new HashMap<>();
        hashMapDetalleNf7 = new HashMap<>();
        //la descripción e id, pueden coincidir en niveles distintos a la hora de mapear... 
        //si se instancia una vez y se setea a todos los items del treeview con el mismo objeto del tipo ImageView
        //no funcionará correctamente
        //se puede rastrear también con una concatenación id + descripción, como nombre del item, pero estéticamente no queda muy bien
        for (Nf1Tipo nf1Tipo : nf1TipoDAO.listar()) {
            ImageView imageViewNf1 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
            hashMapNf1.put(imageViewNf1, nf1Tipo);//rastreo para inserción
            hashMapDetalleNf1.put(nf1Tipo.getIdNf1Tipo(), nf1Tipo);
            TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), imageViewNf1);
            for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
                ImageView imageViewNf2 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
                hashMapNf2.put(imageViewNf2, nf2Sfamilia);
                hashMapDetalleNf2.put(nf2Sfamilia.getIdNf2Sfamilia(), nf2Sfamilia);
                TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), imageViewNf2);
                for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                    ImageView imageViewNf3 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                    hashMapNf3.put(imageViewNf3, nf3Sseccion);
                    hashMapDetalleNf3.put(nf3Sseccion.getIdNf3Sseccion(), nf3Sseccion);
                    TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), imageViewNf3);
                    for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                        ImageView imageViewNf4 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                        hashMapNf4.put(imageViewNf4, nf4Seccion1);
                        hashMapDetalleNf4.put(nf4Seccion1.getIdNf4Seccion1(), nf4Seccion1);
                        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), imageViewNf4);
                        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                            ImageView imageViewNf5 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                            hashMapNf5.put(imageViewNf5, nf5Seccion2);
                            hashMapDetalleNf5.put(nf5Seccion2.getIdNf5Seccion2(), nf5Seccion2);
                            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), imageViewNf5);
                            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                                ImageView imageViewNf6 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                                hashMapNf6.put(imageViewNf6, nf6Secnom6);
                                hashMapDetalleNf6.put(nf6Secnom6.getIdNf6Secnom6(), nf6Secnom6);
                                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), imageViewNf6);
                                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                    ImageView imageViewNf7 = new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                    hashMapNf7.put(imageViewNf7, nf7Secnom7);
                                    hashMapDetalleNf7.put(nf7Secnom7.getIdNf7Secnom7(), nf7Secnom7);
                                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), imageViewNf7);
                                    treeItemNf6.getChildren().add(treeItemNf7);
                                }
                                treeItemNf5.getChildren().add(treeItemNf6);
                            }
                            treeItemNf4.getChildren().add(treeItemNf5);
                        }
                        treeItemNf3.getChildren().add(treeItemNf4);
                    }
                    treeItemNf2.getChildren().add(treeItemNf3);
                }
                treeItemNf1.getChildren().add(treeItemNf2);
            }
            treeItemRoot.getChildren().add(treeItemNf1);
        }
        treeViewSecciones.setRoot(treeItemRoot);
    }
    //////READ, NF
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREATE DESCUENTO FUNCIONARIO
    private JSONObject creandoJsonDesFuncionario(int nf) {
        Date date = new Date();
        JSONParser parser = new JSONParser();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject jsonDescFuncionarioNf = new JSONObject();
        //**********************************************************************
        String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
        int porc = Integer.valueOf(parts[0]);
        jsonDescFuncionarioNf.put("porcDesc", porc);
        jsonDescFuncionarioNf.put("estadoDesc", true);
        switch (nf) {
            case 1:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf1Tipo;
                    jsonNf1Tipo = (JSONObject) parser.parse(gson.toJson(
                            Nf1Tipo.toNf1TipoDTOEntitiesNull(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf1Tipo", jsonNf1Tipo);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 2:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf2Sfamilia;
                    jsonNf2Sfamilia = (JSONObject) parser.parse(gson.toJson(
                            Nf2Sfamilia.toNf2SfamiliaDTOEntitiesNull(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf2Sfamilia", jsonNf2Sfamilia);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 3:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf3Sseccion;
                    jsonNf3Sseccion = (JSONObject) parser.parse(gson.toJson(
                            Nf3Sseccion.toNf3SseccionDTOEntitiesNull(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf3Sseccion", jsonNf3Sseccion);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 4:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf4Seccion1;
                    jsonNf4Seccion1 = (JSONObject) parser.parse(gson.toJson(
                            Nf4Seccion1.toNf4Seccion1DTOEntitiesNull(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf4Seccion1", jsonNf4Seccion1);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 5:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf5Seccion2;
                    jsonNf5Seccion2 = (JSONObject) parser.parse(gson.toJson(
                            Nf5Seccion2.toNf5Seccion2DTOEntitiesNull(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf5Seccion2", jsonNf5Seccion2);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 6:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf6Secnom6;
                    jsonNf6Secnom6 = (JSONObject) parser.parse(gson.toJson(
                            Nf6Secnom6.toNf6Secnom6DTOEntitiesNull(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf6Secnom6", jsonNf6Secnom6);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            case 7:
                jsonDescFuncionarioNf.put("descriSeccion", hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion());
                try {
                    JSONObject jsonNf7Secnom7;
                    jsonNf7Secnom7 = (JSONObject) parser.parse(gson.toJson(
                            Nf7Secnom7.toNf7Secnom7DTOEntitiesNull(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                    jsonDescFuncionarioNf.put("nf7Secnom7", jsonNf7Secnom7);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException: ", ex.fillInStackTrace());
                }
                break;
            default:
                return new JSONObject();
        }
        //**********************************************************************
        jsonDescFuncionarioNf.put("usuAlta", Identity.getNomFun());
        jsonDescFuncionarioNf.put("fechaAlta", timestampJSON);
        jsonDescFuncionarioNf.put("usuMod", Identity.getNomFun());
        jsonDescFuncionarioNf.put("fechaMod", timestampJSON);
        //**********************************************************************
        return jsonDescFuncionarioNf;
    }
    //JSON CREATE DESCUENTO FUNCIONARIO

    //JSON BAJA DESCUENTO FUNCIONARIO MASIVO
    private JSONObject creandoJsonBajaDescMasivo(String u, String ts) {
        JSONObject jsonBajaMasiva = new JSONObject();
        jsonBajaMasiva.put("usuMod", u);
        jsonBajaMasiva.put("fechaMod", ts);
        jsonBajaMasiva.put("descripcionDescuento", UtilLoaderBase.msjIda(Descuento.f5));
        return jsonBajaMasiva;
    }
    //JSON BAJA DESCUENTO FUNCIONARIO MASIVO

    //JSON CREATE DESCUENTO FUNCIONARIO ITE
    private JSONObject creandoJsonDesFuncionarioIterativoNf1(JSONObject nf1Tipo) {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject descFuncionario = new JSONObject();
        //**********************************************************************
        String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
        int porc = Integer.valueOf(parts[0]);
        descFuncionario.put("porcDesc", porc);
        descFuncionario.put("estadoDesc", true);
        //acaité
        descFuncionario.put("nf1Tipo", nf1Tipo);
        descFuncionario.put("descriSeccion", nf1Tipo.get("descripcion").toString());
        //**********************************************************************
        descFuncionario.put("usuAlta", Identity.getNomFun());
        descFuncionario.put("fechaAlta", timestampJSON);
        descFuncionario.put("usuMod", Identity.getNomFun());
        descFuncionario.put("fechaMod", timestampJSON);
        //**********************************************************************
        return descFuncionario;
    }
    //JSON CREATE DESCUENTO FUNCIONARIO ITE

    //JSON BAJA DESCUENTO FUNCIONARIO
    private JSONObject bajaJsonDesFuncionario(JSONObject descFuncionario) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        descFuncionario.put("usuMod", Identity.getNomFun());
        descFuncionario.put("fechaMod", timestampJSON);
        descFuncionario.put("estadoDesc", false);
        //**********************************************************************
        return descFuncionario;
    }
    //JSON BAJA DESCUENTO FUNCIONARIO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF1 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf1() {
        filaCantDescFuncionarioNf1 = 10;
        filaIniDescFuncionarioNf1 = 0;
        pageActualDescFuncionarioNf1 = 0;
        buscarTodosDescFuncionarioNf1 = true;
        buscandoDescuentoTodoNf1();
    }

    private void buscandoDescuentoTodoNf1() {
        filaLimitDescFuncionarioNf1 = jsonRowCountNf1();
        buscarTodosDescFuncionarioNf1 = true;
        paginandoCabFielNf1();
    }

    private void buscandoDescuentoFiltroNf1() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf1.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf1.getText();
        }
        filaLimitDescFuncionarioNf1 = jsonRowCountFilterNf1(filterParam);
        buscarTodosDescFuncionarioNf1 = false;
        paginandoCabFielNf1();
    }

    private void paginandoCabFielNf1() {
        paginationDescFuncionarioNf1.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf1(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf1.setCurrentPageIndex(this.pageActualDescFuncionarioNf1);//index inicial paginación
        paginationDescFuncionarioNf1.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf1 = filaLimitDescFuncionarioNf1 / filaCantDescFuncionarioNf1;//cantidad index paginación
        if (filaLimitDescFuncionarioNf1 % filaCantDescFuncionarioNf1 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf1++;
        }
        paginationDescFuncionarioNf1.setPageCount(pageCountDescFuncionarioNf1);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf1(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf1 = pageIndex * filaCantDescFuncionarioNf1;
        if (buscarTodosDescFuncionarioNf1) {
            descFuncionarioListNf1 = jsonArrayDescFuncionarioFetchNf1(filaCantDescFuncionarioNf1, filaIniDescFuncionarioNf1);
        } else {
            String filterParamNf1 = "null";
            if (!textFieldFiltroSeccionNf1.getText().contentEquals("")) {
                filterParamNf1 = textFieldFiltroSeccionNf1.getText();
            }
            descFuncionarioListNf1 = jsonArrayDescFuncionarioFetchFiltroNf1(filaCantDescFuncionarioNf1, filaIniDescFuncionarioNf1, filterParamNf1);
        }
        actualizandoTablaDescFuncionarioNf1();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf1;
    }

    private void actualizandoTablaDescFuncionarioNf1() {
        tableDescFuncionarioNf1 = new TableView<JSONObject>();
        descFuncionarioDataNf1 = FXCollections.observableArrayList(descFuncionarioListNf1);
        //columna Sección ....................................................
        columnSeccionNf1 = new TableColumn("Sección Nvl. 1");
        columnSeccionNf1.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf1.setMinWidth(380);
        columnSeccionNf1.setEditable(false);
        columnSeccionNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf1 = new TableColumn("Descuento");
        columnDescuentoNf1.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf1.setMinWidth(30);
        columnDescuentoNf1.setEditable(false);
        columnDescuentoNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf1 = new TableColumn("Acciones");
        columnAccionesNf1.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf1(tableDescFuncionarioNf1);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf1.getColumns().addAll(columnSeccionNf1, columnDescuentoNf1, columnAccionesNf1);
        tableDescFuncionarioNf1.setItems(descFuncionarioDataNf1);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf1 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf1(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf1.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 1);
                    JSONObject jsonNf1 = (JSONObject) jsonCabDetalleNf.get("nf1Tipo");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf1.get(Long.valueOf(jsonNf1.get("idNf1Tipo").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf1 = tableDescFuncionarioNf1.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 1 "
                            + descFuncionarioNf1.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf1 = bajaJsonDesFuncionario(descFuncionarioNf1);
                        if (bajaDescFuncionarioNf1(descFuncionarioNf1)) {
                            exitoBaja = false;
                            limpiandoCampos(1);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF1 **************** -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF2 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf2() {
        filaCantDescFuncionarioNf2 = 10;
        filaIniDescFuncionarioNf2 = 0;
        pageActualDescFuncionarioNf2 = 0;
        buscarTodosDescFuncionarioNf2 = true;
        buscandoDescuentoTodoNf2();
    }

    private void buscandoDescuentoTodoNf2() {
        filaLimitDescFuncionarioNf2 = jsonRowCountNf2();
        buscarTodosDescFuncionarioNf2 = true;
        paginandoCabFielNf2();
    }

    private void buscandoDescuentoFiltroNf2() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf2.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf2.getText();
        }
        filaLimitDescFuncionarioNf2 = jsonRowCountFilterNf2(filterParam);
        buscarTodosDescFuncionarioNf2 = false;
        paginandoCabFielNf2();
    }

    private void paginandoCabFielNf2() {
        paginationDescFuncionarioNf2.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf2(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf2.setCurrentPageIndex(this.pageActualDescFuncionarioNf2);//index inicial paginación
        paginationDescFuncionarioNf2.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf2 = filaLimitDescFuncionarioNf2 / filaCantDescFuncionarioNf2;//cantidad index paginación
        if (filaLimitDescFuncionarioNf2 % filaCantDescFuncionarioNf2 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf2++;
        }
        paginationDescFuncionarioNf2.setPageCount(pageCountDescFuncionarioNf2);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf2(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf2 = pageIndex * filaCantDescFuncionarioNf2;
        if (buscarTodosDescFuncionarioNf2) {
            descFuncionarioListNf2 = jsonArrayDescFuncionarioFetchNf2(filaCantDescFuncionarioNf2, filaIniDescFuncionarioNf2);
        } else {
            String filterParamNf2 = "null";
            if (!textFieldFiltroSeccionNf2.getText().contentEquals("")) {
                filterParamNf2 = textFieldFiltroSeccionNf2.getText();
            }
            descFuncionarioListNf2 = jsonArrayDescFuncionarioFetchFiltroNf2(filaCantDescFuncionarioNf2, filaIniDescFuncionarioNf2, filterParamNf2);
        }
        actualizandoTablaDescFuncionarioNf2();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf2;
    }

    private void actualizandoTablaDescFuncionarioNf2() {
        tableDescFuncionarioNf2 = new TableView<JSONObject>();
        descFuncionarioDataNf2 = FXCollections.observableArrayList(descFuncionarioListNf2);
        //columna Sección ....................................................
        columnSeccionNf2 = new TableColumn("Sección Nvl. 2");
        columnSeccionNf2.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf2.setMinWidth(380);
        columnSeccionNf2.setEditable(false);
        columnSeccionNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf2 = new TableColumn("Descuento");
        columnDescuentoNf2.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf2.setMinWidth(30);
        columnDescuentoNf2.setEditable(false);
        columnDescuentoNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf2 = new TableColumn("Acciones");
        columnAccionesNf2.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf2(tableDescFuncionarioNf2);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf2.getColumns().addAll(columnSeccionNf2, columnDescuentoNf2, columnAccionesNf2);
        tableDescFuncionarioNf2.setItems(descFuncionarioDataNf2);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf2 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf2(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf2.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 2);
                    JSONObject jsonNf2 = (JSONObject) jsonCabDetalleNf.get("nf2Sfamilia");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf2.get(Long.valueOf(jsonNf2.get("idNf2Sfamilia").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf2 = tableDescFuncionarioNf2.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 2 "
                            + descFuncionarioNf2.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf2 = bajaJsonDesFuncionario(descFuncionarioNf2);
                        if (bajaDescFuncionarioNf2(descFuncionarioNf2)) {
                            exitoBaja = false;
                            limpiandoCampos(2);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF2 **************** -> -> -> -> -> -> -> 

    //PAGINATION PAGINATION PAGINATION NF3 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf3() {
        filaCantDescFuncionarioNf3 = 10;
        filaIniDescFuncionarioNf3 = 0;
        pageActualDescFuncionarioNf3 = 0;
        buscarTodosDescFuncionarioNf3 = true;
        buscandoDescuentoTodoNf3();
    }

    private void buscandoDescuentoTodoNf3() {
        filaLimitDescFuncionarioNf3 = jsonRowCountNf3();
        buscarTodosDescFuncionarioNf3 = true;
        paginandoCabFielNf3();
    }

    private void buscandoDescuentoFiltroNf3() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf3.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf3.getText();
        }
        filaLimitDescFuncionarioNf3 = jsonRowCountFilterNf3(filterParam);
        buscarTodosDescFuncionarioNf3 = false;
        paginandoCabFielNf3();
    }

    private void paginandoCabFielNf3() {
        paginationDescFuncionarioNf3.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf3(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf3.setCurrentPageIndex(this.pageActualDescFuncionarioNf3);//index inicial paginación
        paginationDescFuncionarioNf3.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf3 = filaLimitDescFuncionarioNf3 / filaCantDescFuncionarioNf3;//cantidad index paginación
        if (filaLimitDescFuncionarioNf3 % filaCantDescFuncionarioNf3 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf3++;
        }
        paginationDescFuncionarioNf3.setPageCount(pageCountDescFuncionarioNf3);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf3(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf3 = pageIndex * filaCantDescFuncionarioNf3;
        if (buscarTodosDescFuncionarioNf3) {
            descFuncionarioListNf3 = jsonArrayDescFuncionarioFetchNf3(filaCantDescFuncionarioNf3, filaIniDescFuncionarioNf3);
        } else {
            String filterParamNf3 = "null";
            if (!textFieldFiltroSeccionNf3.getText().contentEquals("")) {
                filterParamNf3 = textFieldFiltroSeccionNf3.getText();
            }
            descFuncionarioListNf3 = jsonArrayDescFuncionarioFetchFiltroNf3(filaCantDescFuncionarioNf3, filaIniDescFuncionarioNf3, filterParamNf3);
        }
        actualizandoTablaDescFuncionarioNf3();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf3;
    }

    private void actualizandoTablaDescFuncionarioNf3() {
        tableDescFuncionarioNf3 = new TableView<JSONObject>();
        descFuncionarioDataNf3 = FXCollections.observableArrayList(descFuncionarioListNf3);
        //columna Sección ....................................................
        columnSeccionNf3 = new TableColumn("Sección Nvl. 3");
        columnSeccionNf3.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf3.setMinWidth(380);
        columnSeccionNf3.setEditable(false);
        columnSeccionNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf3 = new TableColumn("Descuento");
        columnDescuentoNf3.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf3.setMinWidth(30);
        columnDescuentoNf3.setEditable(false);
        columnDescuentoNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf3 = new TableColumn("Acciones");
        columnAccionesNf3.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf3(tableDescFuncionarioNf3);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf3.getColumns().addAll(columnSeccionNf3, columnDescuentoNf3, columnAccionesNf3);
        tableDescFuncionarioNf3.setItems(descFuncionarioDataNf3);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf3 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf3(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf3.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 3);
                    JSONObject jsonNf3 = (JSONObject) jsonCabDetalleNf.get("nf3Sseccion");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf3.get(Long.valueOf(jsonNf3.get("idNf3Sseccion").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf3 = tableDescFuncionarioNf3.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 3 "
                            + descFuncionarioNf3.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf3 = bajaJsonDesFuncionario(descFuncionarioNf3);
                        if (bajaDescFuncionarioNf3(descFuncionarioNf3)) {
                            exitoBaja = false;
                            limpiandoCampos(3);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF3 **************** -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF4 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf4() {
        filaCantDescFuncionarioNf4 = 10;
        filaIniDescFuncionarioNf4 = 0;
        pageActualDescFuncionarioNf4 = 0;
        buscarTodosDescFuncionarioNf4 = true;
        buscandoDescuentoTodoNf4();
    }

    private void buscandoDescuentoTodoNf4() {
        filaLimitDescFuncionarioNf4 = jsonRowCountNf4();
        buscarTodosDescFuncionarioNf4 = true;
        paginandoCabFielNf4();
    }

    private void buscandoDescuentoFiltroNf4() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf4.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf4.getText();
        }
        filaLimitDescFuncionarioNf4 = jsonRowCountFilterNf4(filterParam);
        buscarTodosDescFuncionarioNf4 = false;
        paginandoCabFielNf4();
    }

    private void paginandoCabFielNf4() {
        paginationDescFuncionarioNf4.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf4(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf4.setCurrentPageIndex(this.pageActualDescFuncionarioNf4);//index inicial paginación
        paginationDescFuncionarioNf4.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf4 = filaLimitDescFuncionarioNf4 / filaCantDescFuncionarioNf4;//cantidad index paginación
        if (filaLimitDescFuncionarioNf4 % filaCantDescFuncionarioNf4 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf4++;
        }
        paginationDescFuncionarioNf4.setPageCount(pageCountDescFuncionarioNf4);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf4(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf4 = pageIndex * filaCantDescFuncionarioNf4;
        if (buscarTodosDescFuncionarioNf4) {
            descFuncionarioListNf4 = jsonArrayDescFuncionarioFetchNf4(filaCantDescFuncionarioNf4, filaIniDescFuncionarioNf4);
        } else {
            String filterParamNf4 = "null";
            if (!textFieldFiltroSeccionNf4.getText().contentEquals("")) {
                filterParamNf4 = textFieldFiltroSeccionNf4.getText();
            }
            descFuncionarioListNf4 = jsonArrayDescFuncionarioFetchFiltroNf4(filaCantDescFuncionarioNf4, filaIniDescFuncionarioNf4, filterParamNf4);
        }
        actualizandoTablaDescFuncionarioNf4();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf4;
    }

    private void actualizandoTablaDescFuncionarioNf4() {
        tableDescFuncionarioNf4 = new TableView<JSONObject>();
        descFuncionarioDataNf4 = FXCollections.observableArrayList(descFuncionarioListNf4);
        //columna Sección ....................................................
        columnSeccionNf4 = new TableColumn("Sección Nvl. 4");
        columnSeccionNf4.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf4.setMinWidth(380);
        columnSeccionNf4.setEditable(false);
        columnSeccionNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf4 = new TableColumn("Descuento");
        columnDescuentoNf4.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf4.setMinWidth(30);
        columnDescuentoNf4.setEditable(false);
        columnDescuentoNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf4 = new TableColumn("Acciones");
        columnAccionesNf4.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf4(tableDescFuncionarioNf4);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf4.getColumns().addAll(columnSeccionNf4, columnDescuentoNf4, columnAccionesNf4);
        tableDescFuncionarioNf4.setItems(descFuncionarioDataNf4);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf4 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf4(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf4.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 4);
                    JSONObject jsonNf4 = (JSONObject) jsonCabDetalleNf.get("nf4Seccion1");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf4.get(Long.valueOf(jsonNf4.get("idNf4Seccion1").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf4 = tableDescFuncionarioNf4.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 4 "
                            + descFuncionarioNf4.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf4 = bajaJsonDesFuncionario(descFuncionarioNf4);
                        if (bajaDescFuncionarioNf4(descFuncionarioNf4)) {
                            exitoBaja = false;
                            limpiandoCampos(4);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF4 **************** -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF5 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf5() {
        filaCantDescFuncionarioNf5 = 10;
        filaIniDescFuncionarioNf5 = 0;
        pageActualDescFuncionarioNf5 = 0;
        buscarTodosDescFuncionarioNf5 = true;
        buscandoDescuentoTodoNf5();
    }

    private void buscandoDescuentoTodoNf5() {
        filaLimitDescFuncionarioNf5 = jsonRowCountNf5();
        buscarTodosDescFuncionarioNf5 = true;
        paginandoCabFielNf5();
    }

    private void buscandoDescuentoFiltroNf5() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf5.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf5.getText();
        }
        filaLimitDescFuncionarioNf5 = jsonRowCountFilterNf5(filterParam);
        buscarTodosDescFuncionarioNf5 = false;
        paginandoCabFielNf5();
    }

    private void paginandoCabFielNf5() {
        paginationDescFuncionarioNf5.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf5(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf5.setCurrentPageIndex(this.pageActualDescFuncionarioNf5);//index inicial paginación
        paginationDescFuncionarioNf5.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf5 = filaLimitDescFuncionarioNf5 / filaCantDescFuncionarioNf5;//cantidad index paginación
        if (filaLimitDescFuncionarioNf5 % filaCantDescFuncionarioNf5 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf5++;
        }
        paginationDescFuncionarioNf5.setPageCount(pageCountDescFuncionarioNf5);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf5(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf5 = pageIndex * filaCantDescFuncionarioNf5;
        if (buscarTodosDescFuncionarioNf5) {
            descFuncionarioListNf5 = jsonArrayDescFuncionarioFetchNf5(filaCantDescFuncionarioNf5, filaIniDescFuncionarioNf5);
        } else {
            String filterParamNf5 = "null";
            if (!textFieldFiltroSeccionNf5.getText().contentEquals("")) {
                filterParamNf5 = textFieldFiltroSeccionNf5.getText();
            }
            descFuncionarioListNf5 = jsonArrayDescFuncionarioFetchFiltroNf5(filaCantDescFuncionarioNf5, filaIniDescFuncionarioNf5, filterParamNf5);
        }
        actualizandoTablaDescFuncionarioNf5();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf5;
    }

    private void actualizandoTablaDescFuncionarioNf5() {
        tableDescFuncionarioNf5 = new TableView<JSONObject>();
        descFuncionarioDataNf5 = FXCollections.observableArrayList(descFuncionarioListNf5);
        //columna Sección ....................................................
        columnSeccionNf5 = new TableColumn("Sección Nvl. 5");
        columnSeccionNf5.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf5.setMinWidth(380);
        columnSeccionNf5.setEditable(false);
        columnSeccionNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf5 = new TableColumn("Descuento");
        columnDescuentoNf5.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf5.setMinWidth(30);
        columnDescuentoNf5.setEditable(false);
        columnDescuentoNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        columnAccionesNf5 = new TableColumn("Acciones");
        columnAccionesNf5.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf5(tableDescFuncionarioNf5);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf5.getColumns().addAll(columnSeccionNf5, columnDescuentoNf5, columnAccionesNf5);
        tableDescFuncionarioNf5.setItems(descFuncionarioDataNf5);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf5 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf5(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf5.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 5);
                    JSONObject jsonNf5 = (JSONObject) jsonCabDetalleNf.get("nf5Seccion2");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf5.get(Long.valueOf(jsonNf5.get("idNf5Seccion2").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf5 = tableDescFuncionarioNf5.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 5 "
                            + descFuncionarioNf5.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf5 = bajaJsonDesFuncionario(descFuncionarioNf5);
                        if (bajaDescFuncionarioNf5(descFuncionarioNf5)) {
                            exitoBaja = false;
                            limpiandoCampos(5);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF5 **************** -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF6 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf6() {
        filaCantDescFuncionarioNf6 = 10;
        filaIniDescFuncionarioNf6 = 0;
        pageActualDescFuncionarioNf6 = 0;
        buscarTodosDescFuncionarioNf6 = true;
        buscandoDescuentoTodoNf6();
    }

    private void buscandoDescuentoTodoNf6() {
        filaLimitDescFuncionarioNf6 = jsonRowCountNf6();
        buscarTodosDescFuncionarioNf6 = true;
        paginandoCabFielNf6();
    }

    private void buscandoDescuentoFiltroNf6() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf6.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf6.getText();
        }
        filaLimitDescFuncionarioNf6 = jsonRowCountFilterNf6(filterParam);
        buscarTodosDescFuncionarioNf6 = false;
        paginandoCabFielNf6();
    }

    private void paginandoCabFielNf6() {
        paginationDescFuncionarioNf6.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf6(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf6.setCurrentPageIndex(this.pageActualDescFuncionarioNf6);//index inicial paginación
        paginationDescFuncionarioNf6.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf6 = filaLimitDescFuncionarioNf6 / filaCantDescFuncionarioNf6;//cantidad index paginación
        if (filaLimitDescFuncionarioNf6 % filaCantDescFuncionarioNf6 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf6++;
        }
        paginationDescFuncionarioNf6.setPageCount(pageCountDescFuncionarioNf6);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf6(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf6 = pageIndex * filaCantDescFuncionarioNf6;
        if (buscarTodosDescFuncionarioNf6) {
            descFuncionarioListNf6 = jsonArrayDescFuncionarioFetchNf6(filaCantDescFuncionarioNf6, filaIniDescFuncionarioNf6);
        } else {
            String filterParamNf6 = "null";
            if (!textFieldFiltroSeccionNf6.getText().contentEquals("")) {
                filterParamNf6 = textFieldFiltroSeccionNf6.getText();
            }
            descFuncionarioListNf6 = jsonArrayDescFuncionarioFetchFiltroNf6(filaCantDescFuncionarioNf6, filaIniDescFuncionarioNf6, filterParamNf6);
        }
        actualizandoTablaDescFuncionarioNf6();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf6;
    }

    private void actualizandoTablaDescFuncionarioNf6() {
        tableDescFuncionarioNf6 = new TableView<JSONObject>();
        descFuncionarioDataNf6 = FXCollections.observableArrayList(descFuncionarioListNf6);
        //columna Sección ....................................................
        columnSeccionNf6 = new TableColumn("Sección Nvl. 6");
        columnSeccionNf6.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf6.setMinWidth(380);
        columnSeccionNf6.setEditable(false);
        columnSeccionNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf6 = new TableColumn("Descuento");
        columnDescuentoNf6.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf6.setMinWidth(30);
        columnDescuentoNf6.setEditable(false);
        columnDescuentoNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf6 = new TableColumn("Acciones");
        columnAccionesNf6.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf6(tableDescFuncionarioNf6);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf6.getColumns().addAll(columnSeccionNf6, columnDescuentoNf6, columnAccionesNf6);
        tableDescFuncionarioNf6.setItems(descFuncionarioDataNf6);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf6 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf6(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf6.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 6);
                    JSONObject jsonNf6 = (JSONObject) jsonCabDetalleNf.get("nf6Secnom6");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf6.get(Long.valueOf(jsonNf6.get("idNf6Secnom6").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf6 = tableDescFuncionarioNf6.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 6 "
                            + descFuncionarioNf6.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf6 = bajaJsonDesFuncionario(descFuncionarioNf6);
                        if (bajaDescFuncionarioNf6(descFuncionarioNf6)) {
                            exitoBaja = false;
                            limpiandoCampos(6);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF6 **************** -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION NF7 **************** -> -> -> -> -> -> ->
    private void primeraPaginacionNf7() {
        filaCantDescFuncionarioNf7 = 10;
        filaIniDescFuncionarioNf7 = 0;
        pageActualDescFuncionarioNf7 = 0;
        buscarTodosDescFuncionarioNf7 = true;
        buscandoDescuentoTodoNf7();
    }

    private void buscandoDescuentoTodoNf7() {
        filaLimitDescFuncionarioNf7 = jsonRowCountNf7();
        buscarTodosDescFuncionarioNf7 = true;
        paginandoCabFielNf7();
    }

    private void buscandoDescuentoFiltroNf7() {
        String filterParam = "null";
        if (!textFieldFiltroSeccionNf7.getText().contentEquals("")) {
            filterParam = textFieldFiltroSeccionNf7.getText();
        }
        filaLimitDescFuncionarioNf7 = jsonRowCountFilterNf7(filterParam);
        buscarTodosDescFuncionarioNf7 = false;
        paginandoCabFielNf7();
    }

    private void paginandoCabFielNf7() {
        paginationDescFuncionarioNf7.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf7(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFuncionarioNf7.setCurrentPageIndex(this.pageActualDescFuncionarioNf7);//index inicial paginación
        paginationDescFuncionarioNf7.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountDescFuncionarioNf7 = filaLimitDescFuncionarioNf7 / filaCantDescFuncionarioNf7;//cantidad index paginación
        if (filaLimitDescFuncionarioNf7 % filaCantDescFuncionarioNf7 > 0) {//index EXTRA (resto de filas)***********
            pageCountDescFuncionarioNf7++;
        }
        paginationDescFuncionarioNf7.setPageCount(pageCountDescFuncionarioNf7);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf7(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaIniDescFuncionarioNf7 = pageIndex * filaCantDescFuncionarioNf7;
        if (buscarTodosDescFuncionarioNf7) {
            descFuncionarioListNf7 = jsonArrayDescFuncionarioFetchNf7(filaCantDescFuncionarioNf7, filaIniDescFuncionarioNf7);
        } else {
            String filterParamNf7 = "null";
            if (!textFieldFiltroSeccionNf7.getText().contentEquals("")) {
                filterParamNf7 = textFieldFiltroSeccionNf7.getText();
            }
            descFuncionarioListNf7 = jsonArrayDescFuncionarioFetchFiltroNf7(filaCantDescFuncionarioNf7, filaIniDescFuncionarioNf7, filterParamNf7);
        }
        actualizandoTablaDescFuncionarioNf7();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableDescFuncionarioNf7;
    }

    private void actualizandoTablaDescFuncionarioNf7() {
        tableDescFuncionarioNf7 = new TableView<JSONObject>();
        descFuncionarioDataNf7 = FXCollections.observableArrayList(descFuncionarioListNf7);
        //columna Sección ....................................................
        columnSeccionNf7 = new TableColumn("Sección Nvl. 7");
        columnSeccionNf7.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccionNf7.setMinWidth(380);
        columnSeccionNf7.setEditable(false);
        columnSeccionNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriSeccion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf7 = new TableColumn("Descuento");
        columnDescuentoNf7.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf7.setMinWidth(30);
        columnDescuentoNf7.setEditable(false);
        columnDescuentoNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................

        columnAccionesNf7 = new TableColumn("Acciones");
        columnAccionesNf7.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoFuncionarioFXMLController.AddObjectsCellNf7(tableDescFuncionarioNf7);
            }
        });
        //columna Domingo .......................................................
        //
        tableDescFuncionarioNf7.getColumns().addAll(columnSeccionNf7, columnDescuentoNf7, columnAccionesNf7);
        tableDescFuncionarioNf7.setItems(descFuncionarioDataNf7);
        //
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellNf7 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf7(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableDescFuncionarioNf7.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 7);
                    JSONObject jsonNf7 = (JSONObject) jsonCabDetalleNf.get("nf7Secnom7");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf7.get(Long.valueOf(jsonNf7.get("idNf7Secnom7").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_func") {
//
//                    }
//                    )
//                    {
                    JSONObject descFuncionarioNf7 = tableDescFuncionarioNf7.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FUNCIONARIO, EN NIVEL 7 "
                            + descFuncionarioNf7.get("descriSeccion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFuncionarioNf7 = bajaJsonDesFuncionario(descFuncionarioNf7);
                        if (bajaDescFuncionarioNf7(descFuncionarioNf7)) {
                            exitoBaja = false;
                            limpiandoCampos(7);
                        }
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    }else {
//                        mensajeError("NO DISPONE DE LOS PERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION NF7 **************** -> -> -> -> -> -> ->

    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker() {
        return new Task() {
            @Override
            protected Object call() {
                int i = 1;
                int size = hashMapDetalleNf1.size();
                Platform.runLater(() -> {
                    viendoProgress();
                });
                if (ConexionPostgres.getCon() == null) {
                    ConexionPostgres.conectar();
                }
                JSONParser parser = new JSONParser();
                //NIVEL FAMILIA BAJA, TODOS LOS DESCUENTOS EXISTENTES...
                try {
                    bajaDescFuncionarioTodo();
                } catch (Exception ex) {
                    Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
                } finally {
                    Utilidades.log.error("TASK FINALLY Funcionario descuento");
                }
                //NIVEL FAMILIA (RAÍZ), AFECTANDO A TODAS LAS SUBSECCIONES...
                for (Map.Entry<Long, Nf1Tipo> hmJsonNf1 : hashMapDetalleNf1.entrySet()) {
                    try {
                        JSONObject jsonNf1Tipo = (JSONObject) parser.parse(gson.toJson(Nf1Tipo.toNf1TipoDTOEntitiesNull(hmJsonNf1.getValue())));
                        creandoDescFuncionarioNf1(creandoJsonDesFuncionarioIterativoNf1(jsonNf1Tipo));
                        updateProgress(i, hashMapDetalleNf1.size());
                        if (i == size) {
                            exitoInsertarDesc = false;
                            Platform.runLater(() -> {
                                limpiandoCampos(1);
                            });
                        } else {
                            i++;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
                if (ConexionPostgres.getCon() != null) {
                    ConexionPostgres.cerrar();
                }
                Platform.runLater(() -> {
                    ocultandoProgress();
                });
                return true;
            }
        };
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public static JSONObject getJsonCabDetalleNf() {
        return jsonCabDetalleNf;
    }
}
