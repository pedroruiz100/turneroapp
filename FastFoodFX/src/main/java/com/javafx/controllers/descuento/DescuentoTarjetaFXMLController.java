/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.DescuentoTarjetaCab;
import com.peluqueria.core.domain.DescuentoTarjetaCabArticulo;
import com.peluqueria.core.domain.DescuentoTarjetaCabEntidad;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf1;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf2;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf3;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf4;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf5;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf6;
import com.peluqueria.core.domain.DescuentoTarjetaCabNf7;
import com.peluqueria.core.domain.DescuentoTarjetaDet;
import com.peluqueria.core.domain.Dias;
import com.peluqueria.core.domain.Entidad;
import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.DescuentoTarjetaCabArticuloDAO;
import com.peluqueria.dto.DescuentoTarjetaCabDTO;
import com.peluqueria.dto.DescuentoTarjetaDetDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.function.UnaryOperator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.control.SplitPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.converter.IntegerStringConverter;
import org.apache.commons.validator.GenericValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.DescuentoTarjetaCabDAO;
import com.peluqueria.dao.DescuentoTarjetaCabEntidadDAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf1DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf2DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf3DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf4DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf5DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf6DAO;
import com.peluqueria.dao.DescuentoTarjetaCabNf7DAO;
import com.peluqueria.dao.DescuentoTarjetaDetDAO;
import com.peluqueria.dao.DiaDAO;
import com.peluqueria.dao.EntidadDAO;
import com.peluqueria.dao.Nf1TipoDAO;
import com.peluqueria.dao.RangoTarjetaDAO;
import com.peluqueria.dao.TarjetaDAO;
import com.peluqueria.dto.DescuentoTarjetaCabArticuloDTO;
import com.peluqueria.dto.DescuentoTarjetaCabEntidadDTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf1DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf2DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf3DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf4DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf5DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf6DTO;
import com.peluqueria.dto.DescuentoTarjetaCabNf7DTO;
import com.javafx.util.NumberValidator;
import com.javafx.util.RemindTask;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.shape.Line;
import org.apache.commons.validator.routines.IntegerValidator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class DescuentoTarjetaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private DescuentoTarjetaCabDAO descuentoTarjCabDAO;
    @Autowired
    private DescuentoTarjetaDetDAO descuentoTarjDetDAO;
    @Autowired
    private DescuentoTarjetaCabEntidadDAO descuentoTarjetaCabEntidadDAO;
    @Autowired
    private DescuentoTarjetaCabArticuloDAO descuentoTarjetaCabArticuloDAO;
    @Autowired
    private DescuentoTarjetaCabNf1DAO descuentoTarjetaCabNf1DAO;
    @Autowired
    private DescuentoTarjetaCabNf2DAO descuentoTarjetaCabNf2DAO;
    @Autowired
    private DescuentoTarjetaCabNf3DAO descuentoTarjetaCabNf3DAO;
    @Autowired
    private DescuentoTarjetaCabNf4DAO descuentoTarjetaCabNf4DAO;
    @Autowired
    private DescuentoTarjetaCabNf5DAO descuentoTarjetaCabNf5DAO;
    @Autowired
    private DescuentoTarjetaCabNf6DAO descuentoTarjetaCabNf6DAO;
    @Autowired
    private DescuentoTarjetaCabNf7DAO descuentoTarjetaCabNf7DAO;
    @Autowired
    private RangoTarjetaDAO rangoDAO;
    @Autowired
    private TarjetaDAO tarjDAO;
    @Autowired
    private EntidadDAO entidadDAO;
    @Autowired
    private DiaDAO diaDAO;
    @Autowired
    private ArticuloDAO articuloDAO;
    @Autowired
    private Nf1TipoDAO nf1TipoDAO;

    private ToggleGroup group;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();
    private final String patternDateReadFecha = "dd/MMM/yyyy";
    private final String patternDateFechaFiltro = "yyyy-MM-dd";
    private final SimpleDateFormat patternDateReadHora = new SimpleDateFormat("HH:mm");
    SimpleDateFormat parseFormat = new SimpleDateFormat("hh:mm:ss a");
    private Time tInicio;
    private Time tFin;
    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    private boolean exitoInsertarCabEntidad;
    private boolean exitoInsertarCabArticulo;
    private boolean exitoInsertarCabNf;
    private boolean exitoBaja;
    private boolean diasCargar;
    private boolean buscarTodosDescTarj;
    private List<JSONObject> diasList;
    private boolean cargarTarjeta;
    private boolean cargarEntidad;
    private static boolean screenScopedTarjArt;
    private static boolean screenScopedTarjSec;
    private JSONArray tarjetaJSONArray;
    private JSONArray entidadJSONArray;
    private JSONArray diaJSONArray;
    private JSONArray cabTarjJSONArray;
    private List<JSONObject> cabTarjList;
    //para visualizar en el formulario dto. tarjeta detalle
    private static HashMap<String, Object> hashMapFormularioDetalleChilds;
    HashMap<Long, Nf1Tipo> hashMapDetalleNf1;
    HashMap<Long, Nf2Sfamilia> hashMapDetalleNf2;
    HashMap<Long, Nf3Sseccion> hashMapDetalleNf3;
    HashMap<Long, Nf4Seccion1> hashMapDetalleNf4;
    HashMap<Long, Nf5Seccion2> hashMapDetalleNf5;
    HashMap<Long, Nf6Secnom6> hashMapDetalleNf6;
    HashMap<Long, Nf7Secnom7> hashMapDetalleNf7;
    //para visualizar en el formulario dto. tarjeta detalle
    //PAGINATION
    private int filaIniCabTarj, filaCantCabTarj, filaLimitCabTarj;
    private int pageCountCabTarj, pageActualCabTarj;
    private ObservableList<JSONObject> cabTarjData;
    private TableView<JSONObject> tableCabTarj;
    private TableColumn<JSONObject, String> columnTarjeta;
    private TableColumn<JSONObject, String> columnDescuento;
    private TableColumn<JSONObject, CheckBox> columnCabTarjDomingo;
    private TableColumn<JSONObject, CheckBox> columnCabTarjLunes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjMartes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjMiercoles;
    private TableColumn<JSONObject, CheckBox> columnCabTarjJueves;
    private TableColumn<JSONObject, CheckBox> columnCabTarjViernes;
    private TableColumn<JSONObject, CheckBox> columnCabTarjSabado;
    private TableColumn<JSONObject, String> columnCabTarjFechaIni;
    private TableColumn<JSONObject, String> columnCabTarjFechaFin;
    private TableColumn<JSONObject, String> columnCabTarjHoraIni;
    private TableColumn<JSONObject, String> columnCabTarjHoraFin;
    private TableColumn<JSONObject, Boolean> columnEliminar;
    private TableColumn<JSONObject, Boolean> columnVer;
    //PAGINATION
    private static List<JSONObject> tarjDetJSONObjList = new ArrayList<>();
    private static JSONObject tarjCab = new JSONObject();

    HashMap<String, JSONObject> hashMapTarjeta;
    HashMap<String, JSONObject> hashMapEntidad;
    Timer timer;

    private boolean patternMonto;
    String param;
    IntegerValidator intValidator;
    private NumberValidator numValidator;

    private static boolean articulo;
    private static boolean excepcionArt;
    Task copyWorker;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneDescTarj;
    @FXML
    private SplitPane splitPaneConfigFuncDesc;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private AnchorPane anchorPaneListView;
    @FXML
    private Label labelDescuento;
    @FXML
    private VBox vBoxCheckBox;
    @FXML
    private CheckBox checkBoxDomingo;
    @FXML
    private CheckBox checkBoxLunes;
    @FXML
    private CheckBox checkBoxMartes;
    @FXML
    private CheckBox checkBoxMiercoles;
    @FXML
    private VBox vBoxCheckBox1;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelConfigTarjeta;
    @FXML
    private Button buttonInsertar;
    @FXML
    private Label labelTarjeta;
    @FXML
    private ChoiceBox<String> choiceBoxDesc;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private Label labelHoraInicio;
    @FXML
    private Label labelHoraFin;
    @FXML
    private Spinner<Integer> spinnerHoraInicioHora;
    @FXML
    private Spinner<Integer> spinnerHoraInicioMin;
    @FXML
    private Spinner<Integer> spinnerHoraFinMin;
    @FXML
    private Spinner<Integer> spinnerHoraFinHora;
    @FXML
    private CheckBox checkBoxTodaHora;
    @FXML
    private CheckBox checkBoxJueves;
    @FXML
    private CheckBox checkBoxViernes;
    @FXML
    private CheckBox checkBoxSabado;
    @FXML
    private ComboBox<String> comboBoxTarjeta;
    @FXML
    private Pagination paginationTarjeta;
    @FXML
    private HBox vBoxFiltro;
    @FXML
    private Label labelTarjetaFiltro;
    @FXML
    private TextField textFieldFiltroTarjeta;
    @FXML
    private Label labelFechaInicioFiltro;
    @FXML
    private DatePicker datePickerFechaInicioFiltro;
    @FXML
    private Label labelFechaFinFiltro;
    @FXML
    private DatePicker datePickerFechaFinFiltro;
    @FXML
    private Button buttonBuscar;
    @FXML
    private Button buttonTodos;
    @FXML
    private AnchorPane anchorPaneTiempo;
    @FXML
    private HBox hBoxContainer;
    @FXML
    private Button buttonCancelarTodas;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Label labelDiasSemana;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private TabPane tabPaneDia;
    @FXML
    private Tab tabPrimer;
    @FXML
    private AnchorPane anchorPanePrimerDia;
    @FXML
    private HBox hBoxContainerPrimeroDia;
    @FXML
    private CheckBox checkBoxDomingoPrimero;
    @FXML
    private CheckBox checkBoxLunesPrimero;
    @FXML
    private CheckBox checkBoxMartesPrimero;
    @FXML
    private CheckBox checkBoxMiercolesPrimero;
    @FXML
    private VBox vBoxCheckBox11;
    @FXML
    private CheckBox checkBoxJuevesPrimero;
    @FXML
    private CheckBox checkBoxViernesPrimero;
    @FXML
    private CheckBox checkBoxSabadoPrimero;
    @FXML
    private Tab tabUltimo;
    @FXML
    private AnchorPane anchorPaneUltimoDia;
    @FXML
    private HBox hBoxContainerUltimoDia;
    @FXML
    private CheckBox checkBoxDomingoUltimo;
    @FXML
    private CheckBox checkBoxLunesUltimo;
    @FXML
    private CheckBox checkBoxMartesUltimo;
    @FXML
    private CheckBox checkBoxMiercolesUltimo;
    @FXML
    private VBox vBoxCheckBox111;
    @FXML
    private CheckBox checkBoxJuevesUltimo;
    @FXML
    private CheckBox checkBoxViernesUltimo;
    @FXML
    private CheckBox checkBoxSabadoUltimo;
    @FXML
    private CheckBox checkBoxSincro;
    @FXML
    private Label labelNombreDesc;
    @FXML
    private AnchorPane anchorPaneMisc;
    @FXML
    private CheckBox checkBoxRetorno;
    @FXML
    private HBox hBoxDesc;
    @FXML
    private Label labelPorc;
    @FXML
    private Label labelDescuentoParana;
    @FXML
    private Spinner<Integer> spinnerParanaDesc;
    @FXML
    private Label labelDescuentoEntidad;
    @FXML
    private Spinner<Integer> spinnerEntidadDesc;
    @FXML
    private VBox vBoxArticulos;
    @FXML
    private Label labelArticulos;
    @FXML
    private Button buttonArticulos;
    @FXML
    private ComboBox<String> comboBoxEntidad;
    @FXML
    private Label labelPorc1;
    @FXML
    private TextField textFieldMontoMin;
    @FXML
    private CheckBox checkBoxMontoMin;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private Label labelProgress;
    @FXML
    private HBox hBoxRadio;
    @FXML
    private RadioButton radioButtonCaja;
    @FXML
    private RadioButton radioButtonExtracto;
    @FXML
    private Line lineVBoxDet;
    @FXML
    private Label labelSecciones;
    @FXML
    private Button buttonSecciones;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoDescTarj();
    }

    @FXML
    private void comboBoxTarjetaMouseClicked(MouseEvent event) {
        mouseEventComboBoxTarjeta(event);
    }

    @FXML
    private void comboBoxTarjetaKeyReleased(KeyEvent event) {
        keyPressComboBoxTarjeta(event);
    }

    @FXML
    private void buttonBuscarAction(ActionEvent event) {
        buscandoDescuentoFiltro();
    }

    @FXML
    private void buttonTodosAction(ActionEvent event) {
        buscandoDescuentoTodo();
    }

    @FXML
    private void anchorPaneDescTarjKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void checkBoxTodaHoraAction(ActionEvent event) {
        checkBoxTodaHora();
    }

    @FXML
    private void buttonCancelarTodasAction(ActionEvent event) {
        cancelandoDescuentoTarjetaTodo();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void checkBoxDomingoAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxLunesAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxMartesAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxMiercolesAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxJuevesAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxViernesAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxSabadoAction(ActionEvent event) {
        validandoPrimerUltDiaCheck();
    }

    @FXML
    private void checkBoxRetornoAction(ActionEvent event) {
        checkBoxRetorno();
    }

    @FXML
    private void buttonArticulosAction(ActionEvent event) {
        verArticulos();
    }

    @FXML
    private void comboBoxEntidadMouseClicked(MouseEvent event) {
        mouseEventComboBoxEntidad(event);
    }

    @FXML
    private void comboBoxEntidadKeyReleased(KeyEvent event) {
        keyPressComboBoxEntidad(event);
    }

    @FXML
    private void checkBoxMontoMinAction(ActionEvent event) {
        checkBoxMontoMin();
    }

    @FXML
    private void radioButtonCajaAction(ActionEvent event) {
        radioButtonExtracto();
    }

    @FXML
    private void radioButtonExtractoAction(ActionEvent event) {
        radioButtonExtracto();
    }

    @FXML
    private void buttonSeccionesAction(ActionEvent event) {
        verSecciones();
    }

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        screenScopedTarjArt = true;
        screenScopedTarjSec = true;
        comboBoxTarjeta.setEditable(true);
        comboBoxEntidad.setEditable(true);
        comboBoxTarjeta.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        comboBoxEntidad.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        choiceBoxDesc.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        spinnerParanaDesc.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        spinnerEntidadDesc.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        cargandoChoiceDescuento();
        choiceBoxDesc.getSelectionModel().select(0);
        cargarTarjeta = true;
        cargarEntidad = true;
        alert = false;
        diasCargar = false;
        exitoInsertarCab = false;
        exitoBaja = false;
        buscarTodosDescTarj = true;
        excepcionArt = false;
        formateandoSpinner();
        deshabilitandoSpinnerDesc();
        primeraPaginacion();
        checkBoxTodaHora.setSelected(true);
        checkBoxTodaHora();
        checkBoxRetorno();
        preparandoDatePicker();
        listenerComboBox();
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        numValidator = new NumberValidator();
        intValidator = new IntegerValidator();
        listenTextField();
        checkBoxMontoMin();
        ocultandoProgress();
        buttonGroup();
        radioButtonExtracto();
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
        generarNivelFamiliaLocal();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, true);
    }

    private void verDetalle() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaDiaEspecialFXML.fxml", 1286, 818, "/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, true);
    }

    private void verArticulos() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaArticuloFXML.fxml", 992, 319, "/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, screenScopedTarjArt);
    }

    private void verSecciones() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaSeccionFXML.fxml", 982, 677, "/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, screenScopedTarjSec);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void mouseEventComboBoxTarjeta(MouseEvent event) {
        if (cargarTarjeta) {
            jsonCargandoTarjeta();
            if (tarjetaJSONArray.isEmpty()) {
                mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                cargarTarjeta = true;
            }
        }
    }

    private void keyPressComboBoxTarjeta(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTarjeta) {
                if (keyCode != event.getCode().TAB) {
                    jsonCargandoTarjeta();
                    if (tarjetaJSONArray.isEmpty()) {
                        mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                        cargarTarjeta = true;
                    }
                }
            }
        }
    }

    private void mouseEventComboBoxEntidad(MouseEvent event) {
        if (cargarEntidad) {
            jsonCargandoEntidad();
            if (entidadJSONArray.isEmpty()) {
                mensajeError("NO SE ENCONTRARON ENTIDADES DISPONIBLES.");
                cargarEntidad = true;
            }
        }
    }

    private void keyPressComboBoxEntidad(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarEntidad) {
                if (keyCode != event.getCode().TAB) {
                    jsonCargandoEntidad();
                    if (entidadJSONArray.isEmpty()) {
                        mensajeError("NO SE ENCONTRARON ENTIDADES DISPONIBLES.");
                        cargarEntidad = true;
                    }
                }
            }
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoDescTarj();
        }
        if (keyCode == event.getCode().F2) {
            buscandoDescuentoFiltro();
        }
        if (keyCode == event.getCode().F3) {
            buscandoDescuentoTodo();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().F6) {
            verArticulos();
        }
        if (keyCode == event.getCode().F7) {
            verSecciones();
        }
        if (keyCode == event.getCode().F12) {
            cancelandoDescuentoTarjetaTodo();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    private void checkBoxTodaHora() {
        if (checkBoxTodaHora.isSelected()) {
            checkBoxTodaHora.setEffect(new DropShadow(10, Color.GREEN));
            labelHoraInicio.setDisable(true);
            labelHoraFin.setDisable(true);
            spinnerHoraInicioHora.setDisable(true);
            spinnerHoraInicioMin.setDisable(true);
            spinnerHoraFinHora.setDisable(true);
            spinnerHoraFinMin.setDisable(true);
            resetSpinner();
        } else {
            checkBoxTodaHora.setEffect(null);
            labelHoraInicio.setDisable(false);
            labelHoraFin.setDisable(false);
            spinnerHoraInicioHora.setDisable(false);
            spinnerHoraInicioMin.setDisable(false);
            spinnerHoraFinHora.setDisable(false);
            spinnerHoraFinMin.setDisable(false);
        }
    }

//    private void checkBoxArticulo() {
//        if (checkBoxArticulos.isSelected()) {
//            checkBoxArticulos.setEffect(new DropShadow(10, Color.GREEN));
//        } else {
//            checkBoxArticulos.setEffect(new DropShadow(10, Color.ORANGE));
//        }
//    }
//
//    private void checkBoxSeccion() {
//        if (checkBoxSecciones.isSelected()) {
//            checkBoxSecciones.setEffect(new DropShadow(10, Color.GREEN));
//        } else {
//            checkBoxSecciones.setEffect(new DropShadow(10, Color.ORANGE));
//        }
//    }
    private void checkBoxRetorno() {
        if (checkBoxRetorno.isSelected()) {
            checkBoxRetorno.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            checkBoxRetorno.setEffect(null);
        }
    }

    private void radioButtonExtracto() {
        if (radioButtonCaja.isSelected()) {
            radioButtonExtracto.setEffect(null);
            radioButtonCaja.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            radioButtonExtracto.setEffect(new DropShadow(10, Color.GREEN));
            radioButtonCaja.setEffect(null);
        }
    }

    private void checkBoxMontoMin() {
        if (checkBoxMontoMin.isSelected()) {
            checkBoxMontoMin.setEffect(new DropShadow(10, Color.ORANGERED));
            textFieldMontoMin.setPromptText("Monto mínimo...");
            textFieldMontoMin.setDisable(false);
        } else {
            checkBoxMontoMin.setEffect(new DropShadow(10, Color.YELLOWGREEN));
            textFieldMontoMin.setText("");
            textFieldMontoMin.setPromptText("Monto sin límite...");
            textFieldMontoMin.setDisable(true);
        }
    }

    private void buttonGroup() {
        group = new ToggleGroup();
        radioButtonCaja.setSelected(true);
        radioButtonCaja.setToggleGroup(group);
        radioButtonExtracto.setToggleGroup(group);
    }

    private void listenerComboBox() {
        comboBoxEntidad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                try {
                    if (comboBoxEntidad.getSelectionModel().getSelectedItem().contentEquals("  N/A")
                            || comboBoxEntidad.getSelectionModel().getSelectedItem().isEmpty()) {
                        labelDescuentoEntidad.setText("Entidad:");
                        checkBoxRetorno.setSelected(false);
                        checkBoxRetorno();
                        checkBoxRetorno.setDisable(true);
                        deshabilitandoSpinnerDesc();
                    } else {
                        labelDescuentoEntidad.setText(comboBoxEntidad.getSelectionModel().getSelectedItem() + ":");
                        checkBoxRetorno();
                        if (checkBoxRetorno.isDisable()) {
                            checkBoxRetorno.setDisable(false);
                        }
                        if (choiceBoxDesc.getSelectionModel().getSelectedIndex() != 0) {
                            habilitandoSpinnerDesc();
                        }
                    }
                } catch (Exception e) {
                    labelDescuentoEntidad.setText("Entidad:");
                    deshabilitandoSpinnerDesc();
                } finally {
                }
            }
        });
        choiceBoxDesc.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                try {
                    if (choiceBoxDesc.getSelectionModel().getSelectedIndex() == 0) {
                        deshabilitandoSpinnerDesc();
                    } else {
                        labelDescuentoEntidad.setText(comboBoxEntidad.getSelectionModel().getSelectedItem() + ":");
                        if (!comboBoxEntidad.getSelectionModel().getSelectedItem().contentEquals("  N/A")) {
                            habilitandoSpinnerDesc();
                        }
                    }
                } catch (Exception e) {
                    labelDescuentoEntidad.setText("Entidad:");
                    deshabilitandoSpinnerDesc();
                } finally {
                }
            }
        });
    }

    private void listenTextField() {
        textFieldMontoMin.textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (oldValue.length() == 0) {
                    patternMonto = true;
                }
                String newV = "";
                String oldV = "";
                if (!newValue.contentEquals("")) {
                    newV = numValidator.numberValidator(newValue);
                    oldV = numValidator.numberValidator(oldValue);
                    long lim = -1l;
                    boolean limite = true;
                    if (!newV.contentEquals("")) {//límite del monto...
                        if (newV.length() < 19) {
                            lim = Long.valueOf(newV);
                            if (lim > 0 && lim < 2147483647) {
                                limite = false;
                            }
                        }
                    }
                    if (limite) {
                        if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                            if (patternMonto) {
                                if (oldV.length() != 0) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                } else {
                                    param = oldValue;
                                }
                                patternMonto = false;
                            } else {
                                patternMonto = true;
                                param = oldValue;
                            }
                            Platform.runLater(() -> {
                                textFieldMontoMin.setText(param);
                                textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                            });
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoMin.setText("");
                                textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                            });
                        }
                    } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                        if (!oldValue.contentEquals("")) {
                            if (!newValue.contentEquals("Gs ")) {
                                if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                    if (patternMonto) {
                                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                        patternMonto = false;
                                    } else {
                                        patternMonto = true;
                                        param = oldValue;
                                    }
                                    Platform.runLater(() -> {
                                        textFieldMontoMin.setText(param);
                                        textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                                    });
                                }
                            } else {
                                Platform.runLater(() -> {
                                    textFieldMontoMin.setText("");
                                    textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoMin.setText(oldValue);
                                textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                            });
                        }
                    } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                        }
                        Platform.runLater(() -> {
                            textFieldMontoMin.setText(param);
                            textFieldMontoMin.positionCaret(textFieldMontoMin.getLength());
                        });
                    }
                }
            } catch (Exception e) {
                textFieldMontoMin.setText("");
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //////READ, NF
    private void generarNivelFamiliaLocal() {
        //y sí... mucho recurso...
        hashMapDetalleNf1 = new HashMap<>();
        hashMapDetalleNf2 = new HashMap<>();
        hashMapDetalleNf3 = new HashMap<>();
        hashMapDetalleNf4 = new HashMap<>();
        hashMapDetalleNf5 = new HashMap<>();
        hashMapDetalleNf6 = new HashMap<>();
        hashMapDetalleNf7 = new HashMap<>();
        //la descripción e id, pueden coincidir en niveles distintos a la hora de mapear... 
        //si se instancia una vez y se setea a todos los items del treeview con el mismo objeto del tipo ImageView
        //no funcionará correctamente
        //se puede rastrear también con una concatenación id + descripción, como nombre del item, pero estéticamente no queda muy bien
        for (Nf1Tipo nf1Tipo : nf1TipoDAO.listar()) {
            hashMapDetalleNf1.put(nf1Tipo.getIdNf1Tipo(), nf1Tipo);
            for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
                hashMapDetalleNf2.put(nf2Sfamilia.getIdNf2Sfamilia(), nf2Sfamilia);
                for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                    hashMapDetalleNf3.put(nf3Sseccion.getIdNf3Sseccion(), nf3Sseccion);
                    for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                        hashMapDetalleNf4.put(nf4Seccion1.getIdNf4Seccion1(), nf4Seccion1);
                        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                            hashMapDetalleNf5.put(nf5Seccion2.getIdNf5Seccion2(), nf5Seccion2);
                            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                                hashMapDetalleNf6.put(nf6Secnom6.getIdNf6Secnom6(), nf6Secnom6);
                                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                    hashMapDetalleNf7.put(nf7Secnom7.getIdNf7Secnom7(), nf7Secnom7);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //////READ, NF

    private void habilitandoSpinnerDesc() {
        spinnerParanaDesc.setDisable(false);
        spinnerEntidadDesc.setDisable(false);
    }

    private void deshabilitandoSpinnerDesc() {
        spinnerParanaDesc.setDisable(true);
        spinnerEntidadDesc.setDisable(true);
        spinnerParanaDesc.getValueFactory().setValue(0);
        spinnerEntidadDesc.getValueFactory().setValue(0);
    }

    private void cargandoChoiceDescuento() {
        //nada de loops y mod...
        choiceBoxDesc.getItems().add("-");
        choiceBoxDesc.getItems().add("5 %");
        choiceBoxDesc.getItems().add("10 %");
        choiceBoxDesc.getItems().add("15 %");
        choiceBoxDesc.getItems().add("20 %");
        choiceBoxDesc.getItems().add("25 %");
        choiceBoxDesc.getItems().add("27 %");
        choiceBoxDesc.getItems().add("30 %");
        choiceBoxDesc.getItems().add("35 %");
        choiceBoxDesc.getItems().add("40 %");
        choiceBoxDesc.getItems().add("45 %");
        choiceBoxDesc.getItems().add("50 %");
        choiceBoxDesc.getSelectionModel().select(0);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicioFiltro.setEditable(false);
        datePickerFechaInicio.setStyle("-fx-font-size: 12px; -fx-font-weight: bold;");
        datePickerFechaFin.setStyle("-fx-font-size: 12px; -fx-font-weight: bold;");
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        long p = ChronoUnit.DAYS.between(
                                datePickerFechaInicio.getValue(), item
                        );
                        setTooltip(new Tooltip(
                                "Descuento Tarjeta por " + p + " días.")
                        );
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFinFiltro.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void resetSpinner() {
        spinnerHoraFinHora.getValueFactory().setValue(0);
        spinnerHoraFinMin.getValueFactory().setValue(0);
        spinnerHoraInicioHora.getValueFactory().setValue(0);
        spinnerHoraInicioMin.getValueFactory().setValue(0);
        tInicio = null;
        tFin = null;
    }

    private void resetComboBoxTarjeta() {
        cargarTarjeta = true;
        ObservableList<String> list = FXCollections.observableArrayList();
        comboBoxTarjeta.valueProperty().set(null);
        comboBoxTarjeta.setItems(list);
        comboBoxTarjeta.getSelectionModel().clearSelection();
        comboBoxTarjeta.getProperties().clear();
        comboBoxTarjeta.getEditor().appendText("");
        comboBoxTarjeta.setPromptText("Filtro Tarjeta...");
        new AutoCompleteComboBoxListener<>(comboBoxTarjeta);
        hashMapTarjeta = new HashMap<>();
    }

    private void resetComboBoxEntidad() {
        cargarEntidad = true;
        ObservableList<String> list = FXCollections.observableArrayList();
        comboBoxEntidad.valueProperty().set(null);
        comboBoxEntidad.setItems(list);
        comboBoxEntidad.getSelectionModel().clearSelection();
        comboBoxEntidad.getProperties().clear();
        comboBoxEntidad.getEditor().appendText("");
        comboBoxEntidad.setPromptText("Filtro Entidad...");
        new AutoCompleteComboBoxListener<>(comboBoxEntidad);
        hashMapEntidad = new HashMap<>();
    }

    private void resetDescTarjBaja() {
        exitoBaja = false;
        primeraPaginacion();
        resetComboBoxTarjeta();
        checkBoxTodaHora.setSelected(true);
        checkBoxTodaHora();
//        datePickerFechaInicio.setValue(null);
        datePickerFechaFin.setValue(null);
        choiceBoxDesc.getSelectionModel().select(0);
        resetCheckBoxDias();
        resetComboBoxEntidad();
    }

    private void resetCheckBoxDias() {
        checkBoxDomingo.selectedProperty().setValue(false);
        checkBoxLunes.selectedProperty().setValue(false);
        checkBoxMartes.selectedProperty().setValue(false);
        checkBoxMiercoles.selectedProperty().setValue(false);
        checkBoxJueves.selectedProperty().setValue(false);
        checkBoxViernes.selectedProperty().setValue(false);
        checkBoxSabado.selectedProperty().setValue(false);
        //
        checkBoxDomingoPrimero.selectedProperty().setValue(false);
        checkBoxLunesPrimero.selectedProperty().setValue(false);
        checkBoxMartesPrimero.selectedProperty().setValue(false);
        checkBoxMiercolesPrimero.selectedProperty().setValue(false);
        checkBoxJuevesPrimero.selectedProperty().setValue(false);
        checkBoxViernesPrimero.selectedProperty().setValue(false);
        checkBoxSabadoPrimero.selectedProperty().setValue(false);
        //
        checkBoxDomingoUltimo.selectedProperty().setValue(false);
        checkBoxLunesUltimo.selectedProperty().setValue(false);
        checkBoxMartesUltimo.selectedProperty().setValue(false);
        checkBoxMiercolesUltimo.selectedProperty().setValue(false);
        checkBoxJuevesUltimo.selectedProperty().setValue(false);
        checkBoxViernesUltimo.selectedProperty().setValue(false);
        checkBoxSabadoUltimo.selectedProperty().setValue(false);
    }

    private void limpiandoCampos() {
        primeraPaginacion();
        resetComboBoxTarjeta();
        checkBoxTodaHora.setSelected(true);
        checkBoxTodaHora();
        datePickerFechaInicio.setValue(LocalDate.now());
        datePickerFechaFin.setValue(null);
        choiceBoxDesc.getSelectionModel().select(0);
        resetCheckBoxDias();
        resetComboBoxEntidad();
        screenScopedTarjArt = true;
        screenScopedTarjSec = true;
        if (DescuentoTarjetaArticuloFXMLController.getHashMapCodArt() != null) {
            if (!DescuentoTarjetaArticuloFXMLController.getHashMapCodArt().isEmpty()) {
                DescuentoTarjetaArticuloFXMLController.setHashMapCodArt(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf1Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf2Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf3Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf4Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf5Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf6Id(new HashMap<>());
            }
        }
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id().isEmpty()) {
                DescuentoTarjetaSeccionFXMLController.setHashMapNf7Id(new HashMap<>());
            }
        }
        checkBoxMontoMin.setSelected(false);
        checkBoxMontoMin();
        checkBoxRetorno.setSelected(false);
        checkBoxRetorno();
        radioButtonCaja.setSelected(true);
        radioButtonExtracto();
        articulo = false;
    }

    private void validandoPrimerUltDiaCheck() {
        if (checkBoxDomingo.isSelected()) {
            checkBoxDomingoPrimero.setSelected(false);
            checkBoxDomingoPrimero.setDisable(true);
            checkBoxDomingoUltimo.setSelected(false);
            checkBoxDomingoUltimo.setDisable(true);
        } else {
            checkBoxDomingoPrimero.setDisable(false);
            checkBoxDomingoUltimo.setDisable(false);
        }
        if (checkBoxLunes.isSelected()) {
            checkBoxLunesPrimero.setSelected(false);
            checkBoxLunesPrimero.setDisable(true);
            checkBoxLunesUltimo.setSelected(false);
            checkBoxLunesUltimo.setDisable(true);
        } else {
            checkBoxLunesPrimero.setDisable(false);
            checkBoxLunesUltimo.setDisable(false);
        }
        if (checkBoxMartes.isSelected()) {
            checkBoxMartesPrimero.setSelected(false);
            checkBoxMartesPrimero.setDisable(true);
            checkBoxMartesUltimo.setSelected(false);
            checkBoxMartesUltimo.setDisable(true);
        } else {
            checkBoxMartesPrimero.setDisable(false);
            checkBoxMartesUltimo.setDisable(false);
        }
        if (checkBoxMiercoles.isSelected()) {
            checkBoxMiercolesPrimero.setSelected(false);
            checkBoxMiercolesPrimero.setDisable(true);
            checkBoxMiercolesUltimo.setSelected(false);
            checkBoxMiercolesUltimo.setDisable(true);
        } else {
            checkBoxMiercolesPrimero.setDisable(false);
            checkBoxMiercolesUltimo.setDisable(false);
        }
        if (checkBoxJueves.isSelected()) {
            checkBoxJuevesPrimero.setSelected(false);
            checkBoxJuevesPrimero.setDisable(true);
            checkBoxJuevesUltimo.setSelected(false);
            checkBoxJuevesUltimo.setDisable(true);
        } else {
            checkBoxJuevesPrimero.setDisable(false);
            checkBoxJuevesUltimo.setDisable(false);
        }
        if (checkBoxViernes.isSelected()) {
            checkBoxViernesPrimero.setSelected(false);
            checkBoxViernesPrimero.setDisable(true);
            checkBoxViernesUltimo.setSelected(false);
            checkBoxViernesUltimo.setDisable(true);
        } else {
            checkBoxViernesPrimero.setDisable(false);
            checkBoxViernesUltimo.setDisable(false);
        }
        if (checkBoxSabado.isSelected()) {
            checkBoxSabadoPrimero.setSelected(false);
            checkBoxSabadoPrimero.setDisable(true);
            checkBoxSabadoUltimo.setSelected(false);
            checkBoxSabadoUltimo.setDisable(true);
        } else {
            checkBoxSabadoUltimo.setDisable(false);
            checkBoxSabadoPrimero.setDisable(false);
        }
    }

    private void formateandoSpinner() {
        //spinner horas inicio
        NumberFormat formatHI = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterHI = spinnerHoraInicioHora -> {
            if (spinnerHoraInicioHora.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatHI.parse(spinnerHoraInicioHora.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerHoraInicioHora.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerHoraInicioHora;
        };
        TextFormatter<Integer> formatterHI = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterHI);
        spinnerHoraInicioHora.getEditor().setTextFormatter(formatterHI);
        spinnerHoraInicioHora.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, 0));
        spinnerHoraInicioHora.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerHoraInicioHora.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerHoraInicioHora.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner horas inicio
        //spinner minutos inicio
        NumberFormat formatMI = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterMI = spinnerHoraInicioMin -> {
            if (spinnerHoraInicioMin.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatMI.parse(spinnerHoraInicioMin.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerHoraInicioMin.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerHoraInicioMin;
        };
        TextFormatter<Integer> formatterMI = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterMI);
        spinnerHoraInicioMin.getEditor().setTextFormatter(formatterMI);
        spinnerHoraInicioMin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, 0));
        spinnerHoraInicioMin.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerHoraInicioMin.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerHoraInicioMin.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner minutos inicio
        //spinner horas fin
        NumberFormat formatHF = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterHF = spinnerHoraFinHora -> {
            if (spinnerHoraFinHora.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatHF.parse(spinnerHoraFinHora.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerHoraFinHora.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerHoraFinHora;
        };
        TextFormatter<Integer> formatterHF = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterHF);
        spinnerHoraFinHora.getEditor().setTextFormatter(formatterHF);
        spinnerHoraFinHora.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 23, 0));
        spinnerHoraFinHora.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerHoraFinHora.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerHoraFinHora.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner horas fin
        //spinner minutos fin
        NumberFormat formatMF = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterMF = spinnerHoraFinMin -> {
            if (spinnerHoraFinMin.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatMF.parse(spinnerHoraFinMin.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerHoraFinMin.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerHoraFinMin;
        };
        TextFormatter<Integer> formatterMF = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterMF);
        spinnerHoraFinMin.getEditor().setTextFormatter(formatterMF);
        spinnerHoraFinMin.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 59, 0));
        spinnerHoraFinMin.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerHoraFinMin.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerHoraFinMin.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner minutos fin
        //spinner porcentaje entidad
        NumberFormat formatEntDesc = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterEntDesc = spinnerEntidadDesc -> {
            if (spinnerEntidadDesc.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatEntDesc.parse(spinnerEntidadDesc.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerEntidadDesc.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerEntidadDesc;
        };
        TextFormatter<Integer> formatterEntDesc = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterEntDesc);
        spinnerEntidadDesc.getEditor().setTextFormatter(formatterEntDesc);
        spinnerEntidadDesc.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 0));
        spinnerEntidadDesc.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerEntidadDesc.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerEntidadDesc.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner porcentaje entidad
        //spinner porcentaje parana
        NumberFormat formatParDesc = NumberFormat.getIntegerInstance();
        UnaryOperator<TextFormatter.Change> filterParDesc = spinnerParanaDesc -> {
            if (spinnerParanaDesc.isContentChange()) {
                ParsePosition parsePosition = new ParsePosition(0);
                // NumberFormat evaluates the beginning of the text
                formatParDesc.parse(spinnerParanaDesc.getControlNewText(), parsePosition);
                if (parsePosition.getIndex() == 0
                        || parsePosition.getIndex() < spinnerParanaDesc.getControlNewText().length()) {
                    // reject parsing the complete text failed
                    return null;
                }
            }
            return spinnerParanaDesc;
        };
        TextFormatter<Integer> formatterParDesc = new TextFormatter<Integer>(
                new IntegerStringConverter(), 0, filterParDesc);
        spinnerParanaDesc.getEditor().setTextFormatter(formatterParDesc);
        spinnerParanaDesc.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 50, 0));
        spinnerParanaDesc.getEditor().textProperty().addListener((observable, oldValue, newValue) -> {
            try {
                if (GenericValidator.isInt(newValue)) {
                    spinnerParanaDesc.getValueFactory().setValue(Integer.parseInt(newValue));
                }
            } catch (NumberFormatException e) {
                if (GenericValidator.isInt(oldValue)) {
                    spinnerParanaDesc.getValueFactory().setValue(Integer.parseInt(oldValue));
                }
            }
        });
        //spinner porcentaje parana
    }

    public void viendoProgress() {
        anchorPaneListView.setDisable(true);
        anchorPaneBottom.setDisable(true);
        vBoxFiltro.setDisable(true);
        paginationTarjeta.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        buttonArticulos.setDisable(true);
        progressIndicator.setVisible(true);
        labelProgress.setVisible(true);
    }

    public void ocultandoProgress() {
        anchorPaneListView.setDisable(false);
        anchorPaneBottom.setDisable(false);
        vBoxFiltro.setDisable(false);
        paginationTarjeta.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        buttonArticulos.setDisable(false);
        progressIndicator.setVisible(false);
        labelProgress.setVisible(false);
        if (excepcionArt) {
            excepcionArt = false;
            mensajeError("NO SE PUDO INSERTAR DESCUENTO TARJETA.\nVERIFIQUE LOS CAMPOS.");
        }
    }

    private void insertandoDescTarj() {
        boolean artNf = false;
        if (DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id() != null) {
            if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id().isEmpty()) {
                artNf = true;
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id() != null) {
                if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id().isEmpty()) {
                    artNf = true;
                }
            }
        }
        if (!artNf) {
            if (!artNf) {
                if (DescuentoTarjetaArticuloFXMLController.getHashMapCodArt() != null) {
                    if (!DescuentoTarjetaArticuloFXMLController.getHashMapCodArt().isEmpty()) {
                        artNf = true;
                    }
                }
            }
        }
        if (artNf) {
            if (!checkBoxDomingo.isSelected() && !checkBoxLunes.isSelected()
                    && !checkBoxMartes.isSelected() && !checkBoxMiercoles.isSelected()
                    && !checkBoxJueves.isSelected() && !checkBoxViernes.isSelected()
                    && !checkBoxSabado.isSelected() && !checkBoxDomingoPrimero.isSelected() && !checkBoxLunesPrimero.isSelected()
                    && !checkBoxMartesPrimero.isSelected() && !checkBoxMiercolesPrimero.isSelected()
                    && !checkBoxJuevesPrimero.isSelected() && !checkBoxViernesPrimero.isSelected()
                    && !checkBoxSabadoPrimero.isSelected() && !checkBoxDomingoUltimo.isSelected() && !checkBoxLunesUltimo.isSelected()
                    && !checkBoxMartesUltimo.isSelected() && !checkBoxMiercolesUltimo.isSelected()
                    && !checkBoxJuevesUltimo.isSelected() && !checkBoxViernesUltimo.isSelected()
                    && !checkBoxSabadoUltimo.isSelected()) {//NO ME SIMPATIZA....... POCO PRÁCTICO.... ¬¬
                mensajeError("DEBE SELECCIONAR AL MENOS UN DÍA DE LA SEMANA.");
            } else if (comboBoxEntidad.getSelectionModel().isEmpty()) {
                mensajeError("DEBE SELECCIONAR UN ÍTEM EN ENTIDAD.");
            } else if (comboBoxTarjeta.getSelectionModel().isEmpty()) {
                mensajeError("DEBE SELECCIONAR UNA TARJETA.");
            } else if (!comboBoxTarjeta.getEditor().getText().contentEquals(comboBoxTarjeta.getItems().get(comboBoxTarjeta.getSelectionModel().getSelectedIndex()))) {
                //condición para el autocomplete en el comboBox...
                mensajeError("DEBE SELECCIONAR UNA TARJETA.");
            } else if (!comboBoxEntidad.getEditor().getText().contentEquals(comboBoxEntidad.getItems().get(comboBoxEntidad.getSelectionModel().getSelectedIndex()))) {
                //condición para el autocomplete en el comboBox...
                mensajeError("DEBE SELECCIONAR UN ÍTEM EN ENTIDAD.");
            } else if (datePickerFechaInicio.getValue() == null) {
                mensajeError("DEBE ASIGNAR UNA FECHA INICIO.");
            } else if (datePickerFechaFin.getValue() == null) {
                mensajeError("DEBE ASIGNAR UNA FECHA FIN.");
            } else if (datePickerFechaFin.getValue().isBefore(datePickerFechaInicio.getValue())) {
                mensajeError("LA FECHA FIN NO DEBE SER MENOR A FECHA INICIO.");
            } else if (choiceBoxDesc.getSelectionModel().getSelectedItem().contentEquals("-")) {
                mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
            } else if (checkBoxMontoMin.isSelected() && textFieldMontoMin.getText().isEmpty()) {
                mensajeError("MONTO MÍNIMO FUE SELECCIONADO\nNO OLVIDES ASIGNAR UN MONTO.");
            } else if (checkBoxMontoMin.isSelected() && textFieldMontoMin.getText().isEmpty()) {//acaité
                mensajeError("MONTO MÍNIMO FUE SELECCIONADO\nNO OLVIDES ASIGNAR UN MONTO.");
            } else if (!checkBoxTodaHora.isSelected()) {
                tInicio = new Time(spinnerHoraInicioHora.getValue(), spinnerHoraInicioMin.getValue(), 0);
                tFin = new Time(spinnerHoraFinHora.getValue(), spinnerHoraFinMin.getValue(), 0);
                if (tInicio.getTime() >= tFin.getTime()) {
                    mensajeError("LA HORA FIN DEBE SER MAYOR A LA HORA INICIO.");
                } else {
                    procesandoInsercion();
                }
            } else if (!spinnerParanaDesc.isDisable() && !spinnerEntidadDesc.isDisable()) {
                String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
                int porc = Integer.valueOf(parts[0]);
                if (porc != (spinnerParanaDesc.getValue() + spinnerEntidadDesc.getValue())) {
                    mensajeError("DEBE ASIGNAR UN PORCENTAJE A [" + comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + "],\n"
                            + "ASÍ TAMBIÉN A [CASA PARANÁ], LA SUMA DE AMBOS DEBE SER IGUAL AL TOTAL DEL DTO. "
                            + "[" + choiceBoxDesc.getSelectionModel().getSelectedItem() + "]");
                } else {
                    LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
                    Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
                    Timestamp timestampIni = new Timestamp(dateIni.getTime());
                    String arrayTarjeta[] = comboBoxTarjeta.getSelectionModel().getSelectedItem().split(" \\|\\| ");
                    if (descuentoTarjCabDAO.verificandoInsercion(comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + " || "
                            + arrayTarjeta[1].toUpperCase(), timestampIni)) {
                        mensajeError("LA PROCESADORA / TARJETA " + comboBoxTarjeta.getSelectionModel().getSelectedItem().toUpperCase() + "\n"
                                + "CON NOMBRE " + comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + "\nYA TIENE COMO FECHA DE INICIO " + dateIni.toString());
                    } else {
                        procesandoInsercion();
                    }
                }
            } else {
                LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
                Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
                Timestamp timestampIni = new Timestamp(dateIni.getTime());
                String arrayTarjeta[] = comboBoxTarjeta.getSelectionModel().getSelectedItem().split(" \\|\\| ");
                if (descuentoTarjCabDAO.verificandoInsercion(comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + " || "
                        + arrayTarjeta[1].toUpperCase(), timestampIni)) {
                    mensajeError("LA PROCESADORA / TARJETA " + comboBoxTarjeta.getSelectionModel().getSelectedItem().toUpperCase() + "\n"
                            + "CON NOMBRE " + comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + "\nYA TIENE COMO FECHA DE INICIO " + dateIni.toString());
                } else {
                    procesandoInsercion();
                }
            }
        } else {
            mensajeError("DEBE ASIGNAR CÓMO MÍNIMO UNA SECCIÓN Y/O ARTÍCULO.");
        }
    }

    private void procesandoInsercion() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR DESCUENTO TARJETA "
                + comboBoxTarjeta.getSelectionModel().getSelectedItem() + "?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
//            if ("guardar_tarjeta_desc")) {
//                if (!articulo) {
//                    if (creandoCabDescTarj()) {
//                        exitoInsertarCab = false;
//                        buscandoDescuentoTodo();
//                        limpiandoCampos();
//                        datePickerFechaInicio.setValue(LocalDate.now());
//                        validandoPrimerUltDiaCheck();
//                    } else {
//                        mensajeError("NO SE PUDO INSERTAR DESCUENTO TARJETA.\nVERIFIQUE LOS CAMPOS.");
//                    }
//                }
                creandoCabDescTarj();
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/DescuentoTarjetaFXML.fxml", 1180, 760, false);
//            }
        } else if (alert.getResult() == cancel) {
            alert.close();
        }
    }

    private void limpiandoBusqueda() {
        textFieldFiltroTarjeta.setText("");
        datePickerFechaInicioFiltro.setValue(null);
        datePickerFechaFinFiltro.setValue(null);
    }

    private void cancelandoDescuentoTarjetaTodo() {
//        if ("baja_desc_tarj")) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN TARJETA CRÉDITO / DÉBITO?", ok, cancel);
            alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ok) {
                if (bajaDescTarjetaTodo()) {
                    exitoBaja = false;
                }
                alert2.close();
            } else if (alert2.getResult() == cancel) {
                alert2.close();
            }
//        } else {
//            mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//        }
    }

    private Integer jsonRowCount() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoTarjetaCab();
        return rowCount;
    }

    private Integer jsonRowCountFilter(String tarjetaFilter, String fechaDesde, String fechaHasta) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFilterTarjLocal(tarjetaFilter, fechaDesde, fechaHasta);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabTarjetaFetch(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabTarjJSONArray = generarFetchDescuentoTarjeta(limRowS, offSetS);
        for (Object cabFielObj : cabTarjJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabTarjFetchFiltro(int limRow, int offSet, String tarjeta, String fechaIni, String fechaFin) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabTarjJSONArray = generarFetchFiltroDescuentoTarjetaLocal(limRowS, offSetS, tarjeta, fechaIni, fechaFin);
        for (Object cabFielObj : cabTarjJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private void jsonCargandoTarjeta() {
        if (cargarTarjeta) {
            tarjetaJSONArray = null;
            tarjetaJSONArray = generarNomIdTarjeta();
        }
        cargarTarjeta = false;
        hashMapTarjeta = new HashMap<>();
        for (Object obj : tarjetaJSONArray) {
            JSONObject tarjeta = (JSONObject) obj;
            JSONObject familiaTarj = (JSONObject) tarjeta.get("familiaTarj");
            hashMapTarjeta.put(familiaTarj.get("descripcion").toString().toUpperCase() + " || " + tarjeta.get("descripcion").toString().toUpperCase(), tarjeta);
            comboBoxTarjeta.getItems().add(familiaTarj.get("descripcion").toString().toUpperCase() + " || " + tarjeta.get("descripcion").toString().toUpperCase());
        }
        new AutoCompleteComboBoxListener<>(comboBoxTarjeta);
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void jsonCargandoEntidad() {
        if (cargarEntidad) {
            entidadJSONArray = null;
            entidadJSONArray = generarNomIdEntidad();
        }
        cargarEntidad = false;
        hashMapEntidad = new HashMap<>();
        for (Object obj : entidadJSONArray) {
            JSONObject entidad = (JSONObject) obj;
            hashMapEntidad.put(entidad.get("descripcion").toString().toUpperCase(), entidad);
            comboBoxEntidad.getItems().add(entidad.get("descripcion").toString());
        }
        new AutoCompleteComboBoxListener<>(comboBoxEntidad);
    }

    private List<JSONObject> readJsonDias() {
        List<JSONObject> diaJSONObjList = new ArrayList<JSONObject>();
        diaJSONArray = generarDiasLocal();
        diasCargar = true;
        for (Object diaObj : diaJSONArray) {
            JSONObject diaJSONObj = (JSONObject) diaObj;
            diaJSONObjList.add(diaJSONObj);
        }
        return diaJSONObjList;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, DESCUENTO TARJETA CAB. ID -> GET
    private JSONObject recuperarDatoTarjetaCab(long idDesc) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCab/getById/" + idDesc);
            URLConnection uc = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            while ((inputLine = br.readLine()) != null) {
                descuento = (JSONObject) parser.parse(inputLine);
            }
            br.close();
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, DESCUENTO TARJETA CAB. ID -> GET

    //////UPDATE, DESCUENTO TARJETA CAB. -> PUT
    private boolean bajaCabTarj(JSONObject descTarjCab) {
        String inputLine;
        JSONParser parser = new JSONParser();
        java.sql.Timestamp fechaAl = null;
        java.sql.Timestamp fechaIn = null;
        java.sql.Timestamp fechaF = null;
        java.sql.Time horaIn = null;
        java.sql.Time horaFi = null;
        JSONObject jsonTarjeta = null;
        try {
            //Verifica si existe el dato en el servidor para cambiar de estado
            boolean status = false;
            if (recuperarDatoTarjetaCab(Long.parseLong(descTarjCab.get("idDescuentoTarjetaCab").toString())) != null) {
                status = true;
            }
            //CASTEANDO FECHAS
            if (descTarjCab.get("fechaAlta") != null) {
                fechaAl = Utilidades.objectToTimestamp(descTarjCab.get("fechaAlta"));
                descTarjCab.put("fechaAlta", fechaAl.getTime());
            }
            if (descTarjCab.get("fechaInicio") != null) {
                fechaIn = Utilidades.objectToTimestamp(descTarjCab.get("fechaInicio"));
                descTarjCab.put("fechaInicio", fechaIn.getTime());
            }
            if (descTarjCab.get("fechaFin") != null) {
                fechaF = Utilidades.objectToTimestamp(descTarjCab.get("fechaFin"));
                descTarjCab.put("fechaFin", fechaF.getTime());
            }
            if (descTarjCab.get("horaInicio") != null) {
                String hor = descTarjCab.get("horaInicio").toString().substring(0, 8);
                horaIn = Time.valueOf(hor);
                descTarjCab.put("horaInicio", horaIn.getTime());
            }
            if (descTarjCab.get("horaFin") != null) {
                String hor = descTarjCab.get("horaFin").toString().substring(0, 8);
                horaFi = Time.valueOf(hor);
                descTarjCab.put("horaFin", horaFi.getTime());
            }
            if (descTarjCab.containsKey("tarjeta")) {
                jsonTarjeta = (JSONObject) descTarjCab.get("tarjeta");
                JSONObject jsonTarjetaTs = jsonTarjeta;
                jsonTarjetaTs.put("fechaAlta", Utilidades.objectToTimestamp(descTarjCab.get("fechaAlta")).getTime());
                jsonTarjetaTs.put("fechaMod", Utilidades.objectToTimestamp(descTarjCab.get("fechaMod")).getTime());
                descTarjCab.put("tarjeta", jsonTarjetaTs);
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(descTarjCab.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescuentoTarjeta(descTarjCab);
                }
            } else {
                exitoBaja = actualizarDescuentoTarjeta(descTarjCab);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescuentoTarjeta(descTarjCab);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            descTarjCab.put("fechaAlta", fechaAl);
            descTarjCab.put("fechaFin", fechaF);
            descTarjCab.put("fechaInicio", fechaIn);
            descTarjCab.put("horaInicio", null);
            descTarjCab.put("horaFin", null);
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            java.sql.Timestamp fechaInicio = null;
            java.sql.Timestamp fechaFin = null;
            if (descTarjCab.get("fechaAlta") != null) {
                String fecha = descTarjCab.get("fechaAlta").toString().substring(0, 19);
                fechaAlta = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaAlta", null);
            }
            if (descTarjCab.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descTarjCab.get("fechaMod").toString()));
                descTarjCab.put("fechaMod", null);
            }
            if (descTarjCab.get("fechaInicio") != null) {
                String fecha = descTarjCab.get("fechaInicio").toString().substring(0, 19);
                fechaInicio = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaInicio", null);
            }
            if (descTarjCab.get("fechaFin") != null) {
                String fecha = descTarjCab.get("fechaFin").toString().substring(0, 19);
                fechaFin = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaFin", null);
            }
            if (jsonTarjeta != null) {
                jsonTarjeta.put("fechaAlta", null);
                jsonTarjeta.put("fechaMod", null);
                descTarjCab.put("tarjeta", jsonTarjeta);
            }
            DescuentoTarjetaCabDTO descuentoTarjetaCabDTO = gson.fromJson(descTarjCab.toString(), DescuentoTarjetaCabDTO.class);
            descuentoTarjetaCabDTO.setFechaAlta(fechaAlta);
            descuentoTarjetaCabDTO.setFechaMod(fechaMod);
            descuentoTarjetaCabDTO.setFechaInicio(fechaInicio);
            descuentoTarjetaCabDTO.setFechaFin(fechaFin);
            descuentoTarjetaCabDTO.setHoraInicio(horaIn);
            descuentoTarjetaCabDTO.setHoraFin(horaFi);
            descuentoTarjetaCabDTO.setEstadoDesc(false);
            if (descuentoTarjCabDAO.actualizarObtenerEstado(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descuentoTarjetaCabDTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO TARJETA CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetDescTarjBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            } else {
                alert2.close();
                resetDescTarjBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO TARJETA.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO TARJETA CAB. -> PUT

    //////CREATE, DESCUENTO TARJETA CAB. -> POST
    @SuppressWarnings("SleepWhileInLoop")
    private boolean creandoCabDescTarj() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject cabDescTarj = new JSONObject();
        try {
            cabDescTarj = creandoJsonCabTarj();
            if (cabDescTarj.isEmpty()) {
                return false;
            }
            CajaDatos.setIdDescuentoTarjeta(rangoDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal()));
            cabDescTarj.put("idDescuentoTarjetaCab", CajaDatos.getIdDescuentoTarjeta());
            JSONObject tarjeta = (JSONObject) cabDescTarj.get("tarjeta");
            tarjeta.remove("fechaAlta");
            tarjeta.remove("fechaMod");
            cabDescTarj.remove("tarjeta");
            cabDescTarj.put("tarjeta", tarjeta);
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(cabDescTarj.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        cabDescTarj = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setDescuentoTarjetaCab(true);
                        CajaDatos.setIdDescuentoTarjeta(Long.parseLong(cabDescTarj.get("idDescuentoTarjetaCab").toString()));
                        exitoInsertarCab = true;
                    }
                    br.close();
                } else {
                    cabDescTarj = registrarDescuentoTarjetaLocal(cabDescTarj);
                }
            } else {
                cabDescTarj = registrarDescuentoTarjetaLocal(cabDescTarj);
            }
        } catch (IOException | ParseException ex) {
            cabDescTarj = registrarDescuentoTarjetaLocal(cabDescTarj);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoInsertarCab) {
            // PARA LA PERSISTENCIA DE MANERA LOCAL PARANA LOCAL
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            long longInicio = Long.parseLong(cabDescTarj.get("fechaInicio").toString());
            long longFin = Long.parseLong(cabDescTarj.get("fechaFin").toString());
            Time horaIni = null;
            Time horaFin = null;
            if (cabDescTarj.get("horaInicio") != null) {
                horaIni = Time.valueOf(cabDescTarj.get("horaInicio").toString());
            }
            if (cabDescTarj.get("horaFin") != null) {
                horaFin = Time.valueOf(cabDescTarj.get("horaFin").toString());
            }
            cabDescTarj.put("fechaAlta", null);
            cabDescTarj.put("fechaMod", null);
            cabDescTarj.put("fechaInicio", null);
            cabDescTarj.put("fechaFin", null);
            cabDescTarj.put("horaInicio", null);
            cabDescTarj.put("horaFin", null);
            System.out.println("-->> " + cabDescTarj.toString());
            DescuentoTarjetaCabDTO descTarjCabDTO = gson.fromJson(cabDescTarj.toString(), DescuentoTarjetaCabDTO.class);
            descTarjCabDTO.setFechaAlta(timestamp);
            descTarjCabDTO.setFechaMod(timestamp);
            descTarjCabDTO.setFechaInicio(new Timestamp(longInicio));
            descTarjCabDTO.setFechaFin(new Timestamp(longFin));
            descTarjCabDTO.setHoraInicio(horaIni);
            descTarjCabDTO.setHoraFin(horaFin);
            DescuentoTarjetaCab valor = descuentoTarjCabDAO.insertarObtenerObjeto(DescuentoTarjetaCab.fromDescuentoTarjetaCabDTO(descTarjCabDTO));
            if (valor != null) {
                Utilidades.log.info("DATOS PERSISTIDOS EN BD LOCAL DESCUENTO TARJETA CABECERA");
            } else {
                Utilidades.log.info("DATOS NO...!! PERSISTIDOS EN BD LOCAL DESCUENTO TARJETA CABECERA");
            }
            exitoInsertarCab = false;
            if (!diasCargar) {
                diasList = readJsonDias();
            }
            //todos los días...
            if (checkBoxDomingo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(0), null));
            }
            if (checkBoxLunes.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(1), null));
            }
            if (checkBoxMartes.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(2), null));
            }
            if (checkBoxMiercoles.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(3), null));
            }
            if (checkBoxJueves.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(4), null));
            }
            if (checkBoxViernes.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(5), null));
            }
            if (checkBoxSabado.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(6), null));
            }
            //todos los días...
            //primer día del mes...
            if (checkBoxDomingoPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(0), true));
            }
            if (checkBoxLunesPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(1), true));
            }
            if (checkBoxMartesPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(2), true));
            }
            if (checkBoxMiercolesPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(3), true));
            }
            if (checkBoxJuevesPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(4), true));
            }
            if (checkBoxViernesPrimero.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(5), true));
            }
            if (checkBoxSabadoPrimero.isSelected()) {
                creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(6), true));
            }
            //primer día del mes...
            //último día del mes...
            if (checkBoxDomingoUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(0), false));
            }
            if (checkBoxLunesUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(1), false));
            }
            if (checkBoxMartesUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(2), false));
            }
            if (checkBoxMiercolesUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(3), false));
            }
            if (checkBoxJuevesUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(4), false));
            }
            if (checkBoxViernesUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(5), false));
            }
            if (checkBoxSabadoUltimo.isSelected()) {
                exitoInsertarCab = creandoDetDescTarj(creandoJsonDetTarj(cabDescTarj, diasList.get(6), false));
            }
            //entidad en caso de existir...
            if (!hashMapEntidad.isEmpty()) {
                if (!comboBoxEntidad.getSelectionModel().getSelectedItem().contentEquals("  N/A")) {
                    exitoInsertarCab = creandoCabDescTarjEntidad(creandoJsonTarjCabEntidad(cabDescTarj,
                            hashMapEntidad.get(comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase()), checkBoxRetorno.isSelected(), spinnerEntidadDesc.getValue()));
                }
            }
            //artículos en caso de seleccionar algunos... (validado previamente, debe estar cargado, mínimo 1 sección o 1 artículo)
            articulo = true;
            copyWorker = createWorker(cabDescTarj);
            progressIndicator.progressProperty().unbind();
            progressIndicator.progressProperty().bind(copyWorker.progressProperty());
            new Thread(copyWorker).start();
        }
        CajaDatos.setDescuentoTarjetaCab(false);
        return exitoInsertarCab;
    }
    //////CREATE, DESCUENTO TARJETA CAB. -> POST

    //////CREATE, DESCUENTO TARJETA CAB. ENTIDAD -> POST
    private boolean creandoCabDescTarjEntidad(JSONObject tarjCabEntidad) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            tarjCabEntidad.put("idDescuentoTarjetaCab", CajaDatos.getIdDescuentoTarjeta());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoTarjetaCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabEntidad");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(tarjCabEntidad.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarCabEntidad = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarCabEntidad = registrarDescuentoTarjCabEntidadLocal(tarjCabEntidad.toString());
                }
            } else {
                exitoInsertarCabEntidad = registrarDescuentoTarjCabEntidadLocal(tarjCabEntidad.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarCabEntidad = registrarDescuentoTarjCabEntidadLocal(tarjCabEntidad.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoTarjetaCabEntidadDTO descuentoTarjetaCabEntidadDTO = gson.fromJson(tarjCabEntidad.toString(), DescuentoTarjetaCabEntidadDTO.class);
        if (descuentoTarjetaCabEntidadDAO.insertarObtenerEstado(DescuentoTarjetaCabEntidad.fromDescuentoTarjetaCabEntidadNoListDTO(descuentoTarjetaCabEntidadDTO))) {
            Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB ENTIDAD");
        } else {
            Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB ENTIDAD");
        }
        return exitoInsertarCabEntidad;
    }
    //////CREATE, DESCUENTO TARJETA CAB. ENTIDAD -> POST

    //////CREATE, DESCUENTO TARJETA CAB. ARTÍCULOS -> POST
    private boolean creandoCabDescTarjArticulo(JSONObject tarjCabArticulo) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            tarjCabArticulo.put("idDescuentoTarjetaCab", CajaDatos.getIdDescuentoTarjeta());
            JSONObject artAux = (JSONObject) tarjCabArticulo.get("articulo");
            JSONObject articulo = new JSONObject();
            articulo.put("idArticulo", Long.valueOf(artAux.get("idArticulo").toString()));
            tarjCabArticulo.remove("articulo");
            tarjCabArticulo.put("articulo", articulo);
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoTarjetaCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabArticulo");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(tarjCabArticulo.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarCabArticulo = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarCabArticulo = registrarDescuentoTarjCabArticuloLocal(tarjCabArticulo.toString());
                }
            } else {
                exitoInsertarCabArticulo = registrarDescuentoTarjCabArticuloLocal(tarjCabArticulo.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarCabArticulo = registrarDescuentoTarjCabArticuloLocal(tarjCabArticulo.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoTarjetaCabArticuloDTO descuentoTarjetaCabArticuloDTO = gson.fromJson(tarjCabArticulo.toString(), DescuentoTarjetaCabArticuloDTO.class);
        if (descuentoTarjetaCabArticuloDAO.insertarObtenerEstado(DescuentoTarjetaCabArticulo.fromDescuentoTarjetaCabArticuloNoListDTO(descuentoTarjetaCabArticuloDTO))) {
            Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB ARTÍCULO");
        } else {
            Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB ARTÍCULO");
        }
        return exitoInsertarCabArticulo;
    }
    //////CREATE, DESCUENTO TARJETA CAB. ARTÍCULOS -> POST

    //////CREATE, DESCUENTO TARJETA CAB. NF -> POST
    @SuppressWarnings({"null", "ConvertToTryWithResources"})
    private boolean creandoCabDescTarjNf(JSONObject tarjCabNf, int nf) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            tarjCabNf.put("idDescuentoTarjetaCab", CajaDatos.getIdDescuentoTarjeta());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoTarjetaCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = null;
                switch (nf) {
                    case 1:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf1");
                        break;
                    case 2:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf2");
                        break;
                    case 3:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf3");
                        break;
                    case 4:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf4");
                        break;
                    case 5:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf5");
                        break;
                    case 6:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf6");
                        break;
                    case 7:
                        url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabNf7");
                        break;
                }
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(tarjCabNf.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarCabNf = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarCabNf = registrarDescuentoTarjCabNfLocal(tarjCabNf.toString(), nf);
                }
            } else {
                exitoInsertarCabNf = registrarDescuentoTarjCabNfLocal(tarjCabNf.toString(), nf);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarCabNf = registrarDescuentoTarjCabNfLocal(tarjCabNf.toString(), nf);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        switch (nf) {
            case 1:
                DescuentoTarjetaCabNf1DTO descuentoTarjetaCabNf1DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf1DTO.class);
                if (descuentoTarjetaCabNf1DAO.insertarObtenerEstado(DescuentoTarjetaCabNf1.fromDescuentoTarjetaCabNf1DTOEntitiesFull(descuentoTarjetaCabNf1DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF1");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF1");
                }
                break;
            case 2:
                DescuentoTarjetaCabNf2DTO descuentoTarjetaCabNf2DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf2DTO.class);
                if (descuentoTarjetaCabNf2DAO.insertarObtenerEstado(DescuentoTarjetaCabNf2.fromDescuentoTarjetaCabNf2DTOEntitiesFull(descuentoTarjetaCabNf2DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF2");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF2");
                }
                break;
            case 3:
                DescuentoTarjetaCabNf3DTO descuentoTarjetaCabNf3DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf3DTO.class);
                if (descuentoTarjetaCabNf3DAO.insertarObtenerEstado(DescuentoTarjetaCabNf3.fromDescuentoTarjetaCabNf3DTOEntitiesFull(descuentoTarjetaCabNf3DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF3");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF3");
                }
                break;
            case 4:
                DescuentoTarjetaCabNf4DTO descuentoTarjetaCabNf4DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf4DTO.class);
                if (descuentoTarjetaCabNf4DAO.insertarObtenerEstado(DescuentoTarjetaCabNf4.fromDescuentoTarjetaCabNf4DTOEntitiesFull(descuentoTarjetaCabNf4DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF4");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF4");
                }
                break;
            case 5:
                DescuentoTarjetaCabNf5DTO descuentoTarjetaCabNf5DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf5DTO.class);
                if (descuentoTarjetaCabNf5DAO.insertarObtenerEstado(DescuentoTarjetaCabNf5.fromDescuentoTarjetaCabNf5DTOEntitiesFull(descuentoTarjetaCabNf5DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF5");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF5");
                }
                break;
            case 6:
                DescuentoTarjetaCabNf6DTO descuentoTarjetaCabNf6DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf6DTO.class);
                if (descuentoTarjetaCabNf6DAO.insertarObtenerEstado(DescuentoTarjetaCabNf6.fromDescuentoTarjetaCabNf6DTOEntitiesFull(descuentoTarjetaCabNf6DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF6");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF6");
                }
                break;
            case 7:
                DescuentoTarjetaCabNf7DTO descuentoTarjetaCabNf7DTO = gson.fromJson(tarjCabNf.toString(), DescuentoTarjetaCabNf7DTO.class);
                if (descuentoTarjetaCabNf7DAO.insertarObtenerEstado(DescuentoTarjetaCabNf7.fromDescuentoTarjetaCabNf7DTOEntitiesFull(descuentoTarjetaCabNf7DTO))) {
                    Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO CAB NF7");
                } else {
                    Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO CAB NF7");
                }
                break;
        }
        return exitoInsertarCabNf;
    }
    //////CREATE, DESCUENTO TARJETA CAB. NF -> POST

    //////CREATE, DESCUENTO TARJETA DET. -> POST
    private boolean creandoDetDescTarj(JSONObject detDescTarj) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            detDescTarj.put("idDescuentoTarjetaCab", CajaDatos.getIdDescuentoTarjeta());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoTarjetaCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaDet");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(detDescTarj.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarDescuentoTarjDetLocal(detDescTarj.toString());
                }
            } else {
                exitoInsertarDet = registrarDescuentoTarjDetLocal(detDescTarj.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarDescuentoTarjDetLocal(detDescTarj.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        boolean diaEspecial = false;
        boolean diaEstado = false;
        if (detDescTarj.get("diaEspecial") != null) {
            diaEspecial = Boolean.parseBoolean(detDescTarj.get("diaEspecial").toString());
        } else {
            diaEstado = true;
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoTarjetaDetDTO descDTO = gson.fromJson(detDescTarj.toString(), DescuentoTarjetaDetDTO.class);
        if (diaEstado) {
            descDTO.setDiaEspecial(null);
        } else {
            descDTO.setDiaEspecial(diaEspecial);
        }
        if (descuentoTarjDetDAO.insertarObtenerEstado(DescuentoTarjetaDet.fromDescuentoTarjetaDetDTO(descDTO))) {
            Utilidades.log.info("DATOS PERSISTIDOS EN DESCUENTO FIEL DET");
        } else {
            Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL DET");
        }
        return exitoInsertarDet;
    }
    //////CREATE, DESCUENTO TARJETA DET. -> POST

    //////UPDATE, DESCUENTO TARJETA CAB. -> GET
    private boolean bajaDescTarjetaTodo() {
        JSONParser parser = new JSONParser();
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCab/baja/" + u + "/" + ts);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    exitoBaja = (Boolean) parser.parse(inputLine);
                }
                if (exitoBaja) {
                    boolean valor = descuentoTarjCabDAO.bajasLocal(Identity.getNomFun(), timestamp);
                    if (valor) {
                        System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                    }
                }
                br.close();
            } else {
                exitoBaja = recuperarBajaDescuentoTarjLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = recuperarBajaDescuentoTarjLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN TARJETA CRÉDITO / DÉBITO\n       HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetDescTarjBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            } else {
                alert2.close();
                resetDescTarjBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            }
        } else {
            mensajeError("LOS DESCUENTOS EN EN TARJETA CRÉDITO / DÉBITO NO SE CANCELARON.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO TARJETA CAB. -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, ROW COUNT DESCUENTO TARJETA
    private Integer generarCountDescuentoTarjetaCab() {
        return descuentoTarjCabDAO.rowCount().intValue();
    }
    //////READ, ROW COUNT DESCUENTO TARJETA

    //////READ, ROW COUNT FILTER DESCUENTO TARJETA
    private Integer generarCountFilterTarjLocal(String tarjetaFilter, String fechaDesde, String fechaHasta) {
        return descuentoTarjCabDAO.rowCountFiler(tarjetaFilter, fechaDesde, fechaHasta).intValue();
    }
    //////READ, ROW COUNT FILTER DESCUENTO TARJETA

    //////READ, FETCH DESCUENTO TARJETA
    private JSONArray generarFetchDescuentoTarjeta(String limRowS, String offSetS) {
        JSONArray listaObj = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoTarjetaCab desc : descuentoTarjCabDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoTarjetaCabDTO()));
                listaObj.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObj;
    }
    //////READ, FETCH FILTRO DESCUENTO TARJETA CONVENIO

    //////READ, FETCH FILTRO DESCUENTO TARJETA
    private JSONArray generarFetchFiltroDescuentoTarjetaLocal(String limRowS, String offSetS, String tarjeta, String fechaIni, String fechaFin) {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoTarjetaCab desc : descuentoTarjCabDAO.listarFETCHTarjeta(Long.parseLong(limRowS), Long.parseLong(offSetS), tarjeta, fechaIni, fechaFin)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoTarjetaCabDTO()));
                array.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, FETCH FILTRO DESCUENTO TARJETA

    //////READ, DÍAS
    private JSONArray generarDiasLocal() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Dias d : diaDAO.listar()) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(d.toDiaDTO()));
                array.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, DÍAS

    //////READ, TARJETA ID
    private JSONArray generarNomIdTarjeta() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Tarjeta tarj : tarjDAO.listarNombreId()) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(tarj.toBDDescriTarjetaDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, TARJETA ID

    //////READ, ENTIDAD ID
    private JSONArray generarNomIdEntidad() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Entidad entidad : entidadDAO.listarNombreId()) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(entidad.toBDDescriEntidadDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }
    //////READ, ENTIDAD ID

    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB. MASIVO
    private boolean recuperarBajaDescuentoTarjLocal(String u, Timestamp ts) {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        boolean valor = false;
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_cab_masivo");
        j.put("data", "{\"estadoDesc\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U', '" + x + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a PARANA LOCAL BD ********");
                valor = descuentoTarjCabDAO.bajasLocal(u, ts);
                if (valor) {
                    System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB. MASIVO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA DET.
    private boolean registrarDescuentoTarjDetLocal(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_det");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA DET.

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. ENTIDAD
    private boolean registrarDescuentoTarjCabEntidadLocal(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_cab_entidad");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. ENTIDAD

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. ARTÍCULO
    private boolean registrarDescuentoTarjCabArticuloLocal(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_cab_articulo");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. ARTÍCULO

    /////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. NF
    private boolean registrarDescuentoTarjCabNfLocal(String toString, int nf) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        switch (nf) {
            case 1:
                j.put("table", "descuento_tarjeta_cab_nf1");
                break;
            case 2:
                j.put("table", "descuento_tarjeta_cab_nf2");
                break;
            case 3:
                j.put("table", "descuento_tarjeta_cab_nf3");
                break;
            case 4:
                j.put("table", "descuento_tarjeta_cab_nf4");
                break;
            case 5:
                j.put("table", "descuento_tarjeta_cab_nf5");
                break;
            case 6:
                j.put("table", "descuento_tarjeta_cab_nf6");
                break;
            case 7:
                j.put("table", "descuento_tarjeta_cab_nf7");
                break;
        }
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB. NF

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB.
    private JSONObject registrarDescuentoTarjetaLocal(JSONObject cabDescTarj) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_cab");
        j.put("data", cabDescTarj.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a la BD ********");
                obj = cabDescTarj;
                exitoInsertarCab = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CAB.

    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB.
    private boolean actualizarDescuentoTarjeta(JSONObject descTarjCab) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_tarjeta_cab");
        j.put("data", descTarjCab.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB.
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREANDO DESCUENTO TARJETA CAB.
    private JSONObject creandoJsonCabTarj() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject descTarjCab = new JSONObject();
        //**********************************************************************
        String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
        int porc = Integer.valueOf(parts[0]);
        descTarjCab.put("porcentajeDesc", porc);
        descTarjCab.put("estadoDesc", true);
        if (spinnerParanaDesc.isDisable()) {
            descTarjCab.put("porcentajeParana", porc);
        } else {
            descTarjCab.put("porcentajeParana", spinnerParanaDesc.getValue());
        }
        if (textFieldMontoMin.getText().isEmpty()) {
            descTarjCab.put("montoMin", -1);
        } else {
            descTarjCab.put("montoMin", Integer.valueOf(numValidator.numberValidator(textFieldMontoMin.getText())));
        }
        descTarjCab.put("extracto", radioButtonExtracto.isSelected());
        if (hashMapTarjeta.containsKey(comboBoxTarjeta.getSelectionModel().getSelectedItem())) {
            //lo ideal sería tener una tabla para las entidades - tarjetas
            String arrayTarjeta[] = comboBoxTarjeta.getSelectionModel().getSelectedItem().split(" \\|\\| ");
            descTarjCab.put("descriTarjeta", comboBoxEntidad.getSelectionModel().getSelectedItem().toUpperCase() + " || "
                    + arrayTarjeta[1].toUpperCase());
            descTarjCab.put("tarjeta", hashMapTarjeta.get(comboBoxTarjeta.getSelectionModel().getSelectedItem()));
        } else {
            return new JSONObject();
        }
        //************** Fecha Inicio / Fecha Fin
        LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
        LocalDate fechaFinLD = datePickerFechaFin.getValue();
        Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
        Timestamp timestampIni = new Timestamp(dateIni.getTime());
        Long timestampIniJSON = timestampIni.getTime();
        Date dateFin = java.sql.Date.valueOf(fechaFinLD);
        Timestamp timestampFin = new Timestamp(dateFin.getTime());
        Long timestampFinJSON = timestampFin.getTime();
        //************** Fecha Inicio / Fecha Fin / Hora
        descTarjCab.put("fechaInicio", timestampIniJSON);
        descTarjCab.put("fechaFin", timestampFinJSON);
        if (tInicio != null) {
            descTarjCab.put("horaInicio", tInicio.toString());
        } else {
            descTarjCab.put("horaInicio", null);
        }
        if (tFin != null) {
            descTarjCab.put("horaFin", tFin.toString());
        } else {
            descTarjCab.put("horaFin", null);
        }
        //**********************************************************************
        descTarjCab.put("usuAlta", Identity.getNomFun());
        descTarjCab.put("fechaAlta", timestampJSON);
        descTarjCab.put("usuMod", Identity.getNomFun());
        descTarjCab.put("fechaMod", timestampJSON);
        //**********************************************************************
        return descTarjCab;
    }
    //JSON CREANDO DESCUENTO TARJETA CAB.

    //JSON CREANDO DESCUENTO TARJETA DET.
    private JSONObject creandoJsonDetTarj(JSONObject descTarjCab, JSONObject dia, Boolean diaEspecial) {
        JSONObject descTarjDet = new JSONObject();
        //**********************************************************************
        descTarjDet.put("descuentoTarjetaCab", descTarjCab);
        descTarjDet.put("dia", dia);
        descTarjDet.put("diaEspecial", diaEspecial);
        //**********************************************************************
        return descTarjDet;
    }
    //JSON CREANDO DESCUENTO TARJETA DET.

    //JSON CREANDO DESCUENTO TARJETA CAB ENTIDAD.
    private JSONObject creandoJsonTarjCabEntidad(JSONObject descTarjCab, JSONObject entidad, Boolean retorno, Integer porcEntidad) {
        JSONObject descTarjCabEntidad = new JSONObject();
        //**********************************************************************
        descTarjCabEntidad.put("descuentoTarjetaCab", descTarjCab);
        descTarjCabEntidad.put("entidad", entidad);
        descTarjCabEntidad.put("retorno", retorno);
        descTarjCabEntidad.put("porcentajeEntidad", porcEntidad);
        //**********************************************************************
        return descTarjCabEntidad;
    }
    //JSON CREANDO DESCUENTO TARJETA CAB ENTIDAD.

    //JSON CREANDO DESCUENTO TARJETA CAB ARTÍCULO.
    private JSONObject creandoJsonTarjCabArticulo(JSONObject descTarjCab, JSONObject articulo) {
        JSONObject descTarjCabArticulo = new JSONObject();
        //**********************************************************************
        descTarjCabArticulo.put("descuentoTarjetaCab", descTarjCab);
        descTarjCabArticulo.put("articulo", articulo);
        //**********************************************************************
        return descTarjCabArticulo;
    }
    //JSON CREANDO DESCUENTO TARJETA CAB ARTÍCULO.

    //JSON CREANDO DESCUENTO TARJETA CAB NF.
    private JSONObject creandoJsonTarjCabNf(JSONObject descTarjCab, int nf, long idNf, String descripcion) {
        JSONObject descTarjCabNf = new JSONObject();
        JSONObject jsonNf = new JSONObject();
        //**********************************************************************
        descTarjCabNf.put("descuentoTarjetaCab", descTarjCab);
        descTarjCabNf.put("descriSeccion", descripcion);
        switch (nf) {
            case 1:
                jsonNf.put("idNf1Tipo", idNf);
                descTarjCabNf.put("nf1Tipo", jsonNf);
                break;
            case 2:
                jsonNf.put("idNf2Sfamilia", idNf);
                descTarjCabNf.put("nf2Sfamilia", jsonNf);
                break;
            case 3:
                jsonNf.put("idNf3Sseccion", idNf);
                descTarjCabNf.put("nf3Sseccion", jsonNf);
                break;
            case 4:
                jsonNf.put("idNf4Seccion1", idNf);
                descTarjCabNf.put("nf4Seccion1", jsonNf);
                break;
            case 5:
                jsonNf.put("idNf5Seccion2", idNf);
                descTarjCabNf.put("nf5Seccion2", jsonNf);
                break;
            case 6:
                jsonNf.put("idNf6Secnom6", idNf);
                descTarjCabNf.put("nf6Secnom6", jsonNf);
                break;
            case 7:
                jsonNf.put("idNf7Secnom7", idNf);
                descTarjCabNf.put("nf7Secnom7", jsonNf);
                break;
        }
        //**********************************************************************
        return descTarjCabNf;
    }
    //JSON CREANDO DESCUENTO TARJETA CAB NF.

    //JSON BAJA DESCUENTO TARJETA
    private JSONObject bajaJsonCabTarj(JSONObject descTarjCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        descTarjCab.put("usuMod", Identity.getNomFun());
        descTarjCab.put("fechaMod", timestampJSON);
        descTarjCab.put("estadoDesc", false);
        descTarjCab.put("descuentoTarjetaDets", null);
        descTarjCab.put("descuentoTarjetaCabEntidads", null);
        descTarjCab.put("descuentoTarjetaCabArticulos", null);
        descTarjCab.put("descuentoTarjetaCabNf1s", null);
        descTarjCab.put("descuentoTarjetaCabNf2s", null);
        descTarjCab.put("descuentoTarjetaCabNf3s", null);
        descTarjCab.put("descuentoTarjetaCabNf4s", null);
        descTarjCab.put("descuentoTarjetaCabNf5s", null);
        descTarjCab.put("descuentoTarjetaCabNf6s", null);
        descTarjCab.put("descuentoTarjetaCabNf7s", null);
        //**********************************************************************
        return descTarjCab;
    }
    //JSON BAJA DESCUENTO TARJETA
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantCabTarj = 5;
        filaIniCabTarj = 0;
        pageActualCabTarj = 0;
        buscarTodosDescTarj = true;
        buscandoDescuentoTodo();
    }

    private void buscandoDescuentoTodo() {
        filaLimitCabTarj = jsonRowCount();
        buscarTodosDescTarj = true;
        paginandoCabTarj();
    }

    private void buscandoDescuentoFiltro() {
        String filterParam = "null";
        String filterFechaDesde = "null";
        String filterFechaHasta = "null";
        LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
        LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
        if (fechaDesdeLD != null) {
            Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
            filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
        }
        if (fechaHastaLD != null) {
            Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
            filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
        }
        if (!textFieldFiltroTarjeta.getText().contentEquals("")) {
            filterParam = textFieldFiltroTarjeta.getText();
        }
        filaLimitCabTarj = jsonRowCountFilter(filterParam, filterFechaDesde, filterFechaHasta);
        buscarTodosDescTarj = false;
        paginandoCabTarj();
    }

    private void paginandoCabTarj() {
        paginationTarjeta.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabTarj(pageIndex);
                //**************************************************************
            }
        });
        paginationTarjeta.setCurrentPageIndex(this.pageActualCabTarj);//index inicial paginación
        paginationTarjeta.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabTarj = filaLimitCabTarj / filaCantCabTarj;//cantidad index paginación
        if (filaLimitCabTarj % filaCantCabTarj > 0) {//index EXTRA (resto de filas)***********
            pageCountCabTarj++;
        }
        paginationTarjeta.setPageCount(pageCountCabTarj);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabTarj(int pageIndex) {
        filaIniCabTarj = pageIndex * filaCantCabTarj;
        if (buscarTodosDescTarj) {
            cabTarjList = jsonArrayCabTarjetaFetch(filaCantCabTarj, filaIniCabTarj);
        } else {
            String filterParam = "null";
            String filterFechaDesde = "null";
            String filterFechaHasta = "null";
            LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
            LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
            if (fechaDesdeLD != null) {
                Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
                filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
            }
            if (fechaHastaLD != null) {
                Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
                filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
            }
            if (!textFieldFiltroTarjeta.getText().contentEquals("")) {
                filterParam = textFieldFiltroTarjeta.getText();
            }
            cabTarjList = jsonArrayCabTarjFetchFiltro(filaCantCabTarj, filaIniCabTarj, filterParam, filterFechaDesde, filterFechaHasta);
        }
        actualizandoTablaCabTarj();
        return tableCabTarj;
    }

    private void actualizandoTablaCabTarj() {
        tableCabTarj = new TableView<>();
        cabTarjData = FXCollections.observableArrayList(cabTarjList);
        //columna Sección ....................................................
        columnTarjeta = new TableColumn("Tarjeta");
        columnTarjeta.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTarjeta.setMinWidth(280);
        columnTarjeta.setEditable(false);
        columnTarjeta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descriTarjeta").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuento = new TableColumn("Dto.");
        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuento.setMinWidth(20);
        columnDescuento.setEditable(false);
        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabTarjDomingo = new TableColumn("Domingo");
        columnCabTarjDomingo.setMinWidth(60);
        columnCabTarjDomingo.setEditable(false);
        columnCabTarjDomingo.setStyle("-fx-alignment: CENTER;");
        columnCabTarjDomingo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("1") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabTarjLunes = new TableColumn("Lunes");
        columnCabTarjLunes.setMinWidth(60);
        columnCabTarjLunes.setEditable(false);
        columnCabTarjLunes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjLunes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("2") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabTarjMartes = new TableColumn("Martes");
        columnCabTarjMartes.setMinWidth(60);
        columnCabTarjMartes.setEditable(false);
        columnCabTarjMartes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjMartes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("3") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabTarjMiercoles = new TableColumn("Miércoles");
        columnCabTarjMiercoles.setMinWidth(60);
        columnCabTarjMiercoles.setEditable(false);
        columnCabTarjMiercoles.setStyle("-fx-alignment: CENTER;");
        columnCabTarjMiercoles.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("4") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabTarjJueves = new TableColumn("Jueves");
        columnCabTarjJueves.setMinWidth(60);
        columnCabTarjJueves.setEditable(false);
        columnCabTarjJueves.setStyle("-fx-alignment: CENTER;");
        columnCabTarjJueves.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("5") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabTarjViernes = new TableColumn("Viernes");
        columnCabTarjViernes.setMinWidth(60);
        columnCabTarjViernes.setEditable(false);
        columnCabTarjViernes.setStyle("-fx-alignment: CENTER;");
        columnCabTarjViernes.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("6") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabTarjSabado = new TableColumn("Sábado");
        columnCabTarjSabado.setMinWidth(60);
        columnCabTarjSabado.setEditable(false);
        columnCabTarjSabado.setStyle("-fx-alignment: CENTER;");
        columnCabTarjSabado.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoTarjetaDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    //2da condición para primer y último día (formulario detalle para ese caso)
                    if (dia.get("idDia").toString().contentEquals("7") && descFielDet.get("diaEspecial") == null) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Fecha Inicio..................................................
        columnCabTarjFechaIni = new TableColumn("Fecha inicio");
        columnCabTarjFechaIni.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCabTarjFechaIni.setMinWidth(40);
        columnCabTarjFechaIni.setEditable(false);
        columnCabTarjFechaIni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaInicio"))));
            }
        });
        //columna Fecha Inicio..................................................
        //columna Fecha Fin.....................................................
        columnCabTarjFechaFin = new TableColumn("Fecha fin");
        columnCabTarjFechaFin.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCabTarjFechaFin.setMinWidth(40);
        columnCabTarjFechaFin.setEditable(false);
        columnCabTarjFechaFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaFin"))));
            }
        });
        //columna Fecha Fin.....................................................
        //columna Hora Inicio...................................................
        columnCabTarjHoraIni = new TableColumn("Hora inicio");
        columnCabTarjHoraIni.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCabTarjHoraIni.setMinWidth(40);
        columnCabTarjHoraIni.setEditable(false);
        columnCabTarjHoraIni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String horaInicio = "toda hora";
                Date date = null;
                if (data.getValue().get("horaInicio") != null) {
                    try {
                        date = parseFormat.parse(data.getValue().get("horaInicio").toString());
                        horaInicio = patternDateReadHora.format(date);
                    } catch (java.text.ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
                return new ReadOnlyStringWrapper(horaInicio);
            }
        });
        //columna Hora Inicio...................................................
        //columna Hora Fin......................................................
        columnCabTarjHoraFin = new TableColumn("Hora fin");
        columnCabTarjHoraFin.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCabTarjHoraFin.setMinWidth(40);
        columnCabTarjHoraFin.setEditable(false);
        columnCabTarjHoraFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String horaFin = "toda hora";
                Date date = null;
                if (data.getValue().get("horaFin") != null) {
                    try {
                        date = parseFormat.parse(data.getValue().get("horaFin").toString());
                        horaFin = patternDateReadHora.format(date);
                    } catch (java.text.ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
                return new ReadOnlyStringWrapper(horaFin);
            }
        });
        //columna Hora Fin......................................................
        //columna Cancelar .....................................................
        columnEliminar = new TableColumn("Cancelar");
        columnEliminar.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoTarjetaFXMLController.AddObjectsCell(tableCabTarj);
            }
        });
        //columna Cancelar .....................................................
        //columna Ver primer, último día .......................................
        columnVer = new TableColumn("Detalle");
        columnVer.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoTarjetaFXMLController.AddObjectsCellVer(tableCabTarj);
            }
        });
        //columna Ver primer, último día .......................................
        tableCabTarj.getColumns().addAll(columnTarjeta, columnDescuento, columnCabTarjDomingo,
                columnCabTarjLunes, columnCabTarjMartes, columnCabTarjMiercoles,
                columnCabTarjJueves, columnCabTarjViernes, columnCabTarjSabado,
                columnCabTarjFechaIni, columnCabTarjFechaFin, columnCabTarjHoraIni,
                columnCabTarjHoraFin, columnVer, columnEliminar);
        tableCabTarj.setItems(cabTarjData);
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCellVer extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellVer(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    JSONParser parser = new JSONParser();
                    int index = table.getSelectionModel().getFocusedIndex();
                    tarjCab = new JSONObject();
                    hashMapFormularioDetalleChilds = new HashMap<>();
                    tarjCab = tableCabTarj.getItems().get(index);
                    if (tarjCab.containsKey("descuentoTarjetaCabNf1s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf1 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf1s");
                        for (Object objectDescuentoTarjetaCabNf1 : jsonArrayDescuentoTarjetaCabNf1) {
                            JSONObject jsonDescuentoTarjetaCabNf1 = (JSONObject) objectDescuentoTarjetaCabNf1;
                            JSONObject jsonNf1Tipo = (JSONObject) jsonDescuentoTarjetaCabNf1.get("nf1Tipo");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("1->" + jsonNf1Tipo.get("idNf1Tipo").toString(),
                                    hashMapDetalleNf1.get(Long.valueOf(jsonNf1Tipo.get("idNf1Tipo").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf2s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf2 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf2s");
                        for (Object objectDescuentoTarjetaCabNf2 : jsonArrayDescuentoTarjetaCabNf2) {
                            JSONObject jsonDescuentoTarjetaCabNf2 = (JSONObject) objectDescuentoTarjetaCabNf2;
                            JSONObject jsonNf2Sfamilia = (JSONObject) jsonDescuentoTarjetaCabNf2.get("nf2Sfamilia");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("2->" + jsonNf2Sfamilia.get("idNf2Sfamilia").toString(),
                                    hashMapDetalleNf2.get(Long.valueOf(jsonNf2Sfamilia.get("idNf2Sfamilia").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf3s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf3 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf3s");
                        for (Object objectDescuentoTarjetaCabNf3 : jsonArrayDescuentoTarjetaCabNf3) {
                            JSONObject jsonDescuentoTarjetaCabNf3 = (JSONObject) objectDescuentoTarjetaCabNf3;
                            JSONObject jsonNf3Sseccion = (JSONObject) jsonDescuentoTarjetaCabNf3.get("nf3Sseccion");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("3->" + jsonNf3Sseccion.get("idNf3Sseccion").toString(),
                                    hashMapDetalleNf3.get(Long.valueOf(jsonNf3Sseccion.get("idNf3Sseccion").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf4s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf4 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf4s");
                        for (Object objectDescuentoTarjetaCabNf4 : jsonArrayDescuentoTarjetaCabNf4) {
                            JSONObject jsonDescuentoTarjetaCabNf4 = (JSONObject) objectDescuentoTarjetaCabNf4;
                            JSONObject jsonNf4Seccion1 = (JSONObject) jsonDescuentoTarjetaCabNf4.get("nf4Seccion1");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("4->" + jsonNf4Seccion1.get("idNf4Seccion1").toString(),
                                    hashMapDetalleNf4.get(Long.valueOf(jsonNf4Seccion1.get("idNf4Seccion1").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf5s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf5 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf5s");
                        for (Object objectDescuentoTarjetaCabNf5 : jsonArrayDescuentoTarjetaCabNf5) {
                            JSONObject jsonDescuentoTarjetaCabNf5 = (JSONObject) objectDescuentoTarjetaCabNf5;
                            JSONObject jsonNf5Seccion2 = (JSONObject) jsonDescuentoTarjetaCabNf5.get("nf5Seccion2");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("5->" + jsonNf5Seccion2.get("idNf5Seccion2").toString(),
                                    hashMapDetalleNf5.get(Long.valueOf(jsonNf5Seccion2.get("idNf5Seccion2").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf6s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf6 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf6s");
                        for (Object objectDescuentoTarjetaCabNf6 : jsonArrayDescuentoTarjetaCabNf6) {
                            JSONObject jsonDescuentoTarjetaCabNf6 = (JSONObject) objectDescuentoTarjetaCabNf6;
                            JSONObject jsonNf6Secnom6 = (JSONObject) jsonDescuentoTarjetaCabNf6.get("nf6Secnom6");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("6->" + jsonNf6Secnom6.get("idNf6Secnom6").toString(),
                                    hashMapDetalleNf6.get(Long.valueOf(jsonNf6Secnom6.get("idNf6Secnom6").toString())));
                        }
                    }
                    if (tarjCab.containsKey("descuentoTarjetaCabNf7s")) {
                        JSONArray jsonArrayDescuentoTarjetaCabNf7 = (JSONArray) tarjCab.get("descuentoTarjetaCabNf7s");
                        for (Object objectDescuentoTarjetaCabNf7 : jsonArrayDescuentoTarjetaCabNf7) {
                            JSONObject jsonDescuentoTarjetaCabNf7 = (JSONObject) objectDescuentoTarjetaCabNf7;
                            JSONObject jsonNf7Secnom7 = (JSONObject) jsonDescuentoTarjetaCabNf7.get("nf7Secnom7");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("7->" + jsonNf7Secnom7.get("idNf7Secnom7").toString(),
                                    hashMapDetalleNf7.get(Long.valueOf(jsonNf7Secnom7.get("idNf7Secnom7").toString())));
                        }
                    }
                    JSONArray tarjDetArray = (JSONArray) tarjCab.get("descuentoTarjetaDets");
                    tarjDetJSONObjList = new ArrayList<>();
                    for (Object tarjDetObj : tarjDetArray) {
                        JSONObject tarjDetJSONObj = (JSONObject) tarjDetObj;
                        long id = Long.parseLong(tarjDetJSONObj.get("idDescuentoTarjetaDet").toString());
                        DescuentoTarjetaDetDTO descDTO = descuentoTarjDetDAO.getById(id).toDescuentoTarjetaDetDTO();
                        try {
                            JSONObject json = (JSONObject) parser.parse(gson.toJson(descDTO).toString());
                            tarjDetJSONObjList.add(json);
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                            tarjDetJSONObjList.add(tarjDetJSONObj);
                        }
                    }
                    verDetalle();
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }

    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_tarj")) {
                        JSONObject descTarjCab = tableCabTarj.getItems().get(index);
                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO TARJETA "
                                + descTarjCab.get("descriTarjeta").toString() + "?", ok, cancel);
                        alert = true;
                        alert2.showAndWait();
                        if (alert2.getResult() == ok) {
                            descTarjCab = bajaJsonCabTarj(descTarjCab);
                            bajaCabTarj(descTarjCab);
                        } else if (alert2.getResult() == cancel) {
                            alert2.close();
                        }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    public static JSONObject getTarjCab() {
        return tarjCab;
    }

    public static List<JSONObject> getTarjDetJSONObjList() {
        return tarjDetJSONObjList;
    }

    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker(JSONObject cabDescTarj) {
        return new Task() {
            @Override
            protected Object call() {
                viendoProgress();
                int j = 0;
                int size = 0;
                boolean art = false;
                boolean nf1 = false;
                boolean nf2 = false;
                boolean nf3 = false;
                boolean nf4 = false;
                boolean nf5 = false;
                boolean nf6 = false;
                boolean nf7 = false;
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id().isEmpty()) {
                        size = DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id().size();
                        nf1 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id().size();
                        nf2 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id().size();
                        nf3 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id().size();
                        nf4 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id().size();
                        nf5 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id().size();
                        nf6 = true;
                    }
                }
                if (DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id() != null) {
                    if (!DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id().isEmpty()) {
                        size = size + DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id().size();
                        nf7 = true;
                    }
                }
                if (DescuentoTarjetaArticuloFXMLController.getHashMapCodArt() != null) {
                    if (!DescuentoTarjetaArticuloFXMLController.getHashMapCodArt().isEmpty()) {
                        size = size + DescuentoTarjetaArticuloFXMLController.getHashMapCodArt().size();
                        art = true;
                    }
                }
                if (nf1) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf1Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                1, entry.getKey(), entry.getValue()), 1);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf2) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf2Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                2, entry.getKey(), entry.getValue()), 2);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf3) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf3Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                3, entry.getKey(), entry.getValue()), 3);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf4) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf4Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                4, entry.getKey(), entry.getValue()), 4);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf5) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf5Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                5, entry.getKey(), entry.getValue()), 5);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf6) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf6Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                6, entry.getKey(), entry.getValue()), 6);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (nf7) {
                    for (Map.Entry<Long, String> entry : DescuentoTarjetaSeccionFXMLController.getHashMapNf7Id().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjNf(creandoJsonTarjCabNf(cabDescTarj,
                                7, entry.getKey(), entry.getValue()), 7);
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (art) {
                    for (Map.Entry<Long, JSONObject> entry : DescuentoTarjetaArticuloFXMLController.getHashMapCodArt().entrySet()) {
                        exitoInsertarCab = creandoCabDescTarjArticulo(creandoJsonTarjCabArticulo(cabDescTarj,
                                entry.getValue()));
                        ++j;
                        updateProgress(j, size);
                    }
                }
                if (exitoInsertarCab) {
                    Platform.runLater(() -> {
                        ocultandoProgress();
                        exitoInsertarCab = false;
                        buscandoDescuentoTodo();
                        limpiandoCampos();
                        datePickerFechaInicio.setValue(LocalDate.now());
                        validandoPrimerUltDiaCheck();
                    });
                } else {
                    Platform.runLater(() -> {
                        excepcionArt = true;
                        ocultandoProgress();
                    });
                }
                return true;
            }
        };
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public static void setScreenScopedTarjArt(boolean aScreenScopedTarjArt) {
        screenScopedTarjArt = aScreenScopedTarjArt;
    }

    public static void setScreenScopedTarjSec(boolean aScreenScopedTarjSec) {
        screenScopedTarjSec = aScreenScopedTarjSec;
    }

    public static HashMap<String, Object> getHashMapFormularioDetalleChilds() {
        return hashMapFormularioDetalleChilds;
    }
}
