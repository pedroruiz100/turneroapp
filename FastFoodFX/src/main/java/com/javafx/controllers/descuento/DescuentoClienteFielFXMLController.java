/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.DescuentoFielCab;
import com.peluqueria.core.domain.DescuentoFielDet;
import com.peluqueria.core.domain.DescuentoFielFormasDePago;
import com.peluqueria.core.domain.DescuentoFielNf1;
import com.peluqueria.core.domain.DescuentoFielNf2;
import com.peluqueria.core.domain.DescuentoFielNf3;
import com.peluqueria.core.domain.DescuentoFielNf4;
import com.peluqueria.core.domain.DescuentoFielNf5;
import com.peluqueria.core.domain.DescuentoFielNf6;
import com.peluqueria.core.domain.DescuentoFielNf7;
import com.peluqueria.core.domain.Dias;
import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.Seccion;
import com.peluqueria.dto.DescuentoFielCabDTO;
import com.peluqueria.dto.DescuentoFielDetDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.DescuentoFielCabDAO;
import com.peluqueria.dao.DescuentoFielDetDAO;
import com.peluqueria.dao.DescuentoFielFormasDePagoDAO;
import com.peluqueria.dao.DescuentoFielNf1DAO;
import com.peluqueria.dao.DescuentoFielNf2DAO;
import com.peluqueria.dao.DescuentoFielNf3DAO;
import com.peluqueria.dao.DescuentoFielNf4DAO;
import com.peluqueria.dao.DescuentoFielNf5DAO;
import com.peluqueria.dao.DescuentoFielNf6DAO;
import com.peluqueria.dao.DescuentoFielNf7DAO;
import com.peluqueria.dao.DiaDAO;
import com.peluqueria.dao.Nf1TipoDAO;
import com.peluqueria.dao.Nf2SfamiliaDAO;
import com.peluqueria.dao.Nf3SseccionDAO;
import com.peluqueria.dao.Nf4Seccion1DAO;
import com.peluqueria.dao.Nf5Seccion2DAO;
import com.peluqueria.dao.Nf6Secnom6DAO;
import com.peluqueria.dao.Nf7Secnom7DAO;
import com.peluqueria.dao.RangoDescFielCabDAO;
import com.peluqueria.dao.SeccionDAO;
import com.peluqueria.dto.DescuentoFielNf1DTO;
import com.peluqueria.dto.DescuentoFielNf2DTO;
import com.peluqueria.dto.DescuentoFielNf3DTO;
import com.peluqueria.dto.DescuentoFielNf4DTO;
import com.peluqueria.dto.DescuentoFielNf5DTO;
import com.peluqueria.dto.DescuentoFielNf6DTO;
import com.peluqueria.dto.DescuentoFielNf7DTO;
import com.javafx.util.Descuento;
import com.javafx.util.RemindTask;
import java.util.HashMap;
import java.util.Timer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class DescuentoClienteFielFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private DescuentoFielCabDAO descFielDAO;
    @Autowired
    private DescuentoFielDetDAO descFielDetDAO;
    @Autowired
    private DescuentoFielNf1DAO descuentoFielNf1DAO;
    @Autowired
    private DescuentoFielNf2DAO descuentoFielNf2DAO;
    @Autowired
    private DescuentoFielNf3DAO descuentoFielNf3DAO;
    @Autowired
    private DescuentoFielNf4DAO descuentoFielNf4DAO;
    @Autowired
    private DescuentoFielNf5DAO descuentoFielNf5DAO;
    @Autowired
    private DescuentoFielNf6DAO descuentoFielNf6DAO;
    @Autowired
    private DescuentoFielNf7DAO descuentoFielNf7DAO;
    @Autowired
    private DiaDAO diaDAO;
    @Autowired
    private SeccionDAO seccionDAO;
    @Autowired
    private Nf1TipoDAO nf1TipoDAO;
    @Autowired
    private Nf2SfamiliaDAO nf2SfamiliaDAO;
    @Autowired
    private Nf3SseccionDAO nf3SseccionDAO;
    @Autowired
    private Nf4Seccion1DAO nf4Seccion1DAO;
    @Autowired
    private Nf5Seccion2DAO nf5Seccion2DAO;
    @Autowired
    private Nf6Secnom6DAO nf6Secnom6DAO;
    @Autowired
    private Nf7Secnom7DAO nf7Secnom7DAO;
    @Autowired
    private RangoDescFielCabDAO rangoDescFielDAO;
    @Autowired
    private DescuentoFielFormasDePagoDAO descuentoFielFormasDePagoDAO;

    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    private boolean exitoBaja;
    private boolean exitoFormaPago;
    private boolean diasCargar;
    private List<JSONObject> diasList;
    //para inserción
    HashMap<ImageView, Nf1Tipo> hashMapNf1;
    HashMap<ImageView, Nf2Sfamilia> hashMapNf2;
    HashMap<ImageView, Nf3Sseccion> hashMapNf3;
    HashMap<ImageView, Nf4Seccion1> hashMapNf4;
    HashMap<ImageView, Nf5Seccion2> hashMapNf5;
    HashMap<ImageView, Nf6Secnom6> hashMapNf6;
    HashMap<ImageView, Nf7Secnom7> hashMapNf7;
    //para inserción
    //para consulta detalle
    HashMap<Long, Nf1Tipo> hashMapDetalleNf1;
    HashMap<Long, Nf2Sfamilia> hashMapDetalleNf2;
    HashMap<Long, Nf3Sseccion> hashMapDetalleNf3;
    HashMap<Long, Nf4Seccion1> hashMapDetalleNf4;
    HashMap<Long, Nf5Seccion2> hashMapDetalleNf5;
    HashMap<Long, Nf6Secnom6> hashMapDetalleNf6;
    HashMap<Long, Nf7Secnom7> hashMapDetalleNf7;
    //para consulta detalle
    private static JSONObject jsonCabDetalleNf;
    //PAGINATION NF1
    private JSONArray cabFielJSONArrayNf1;
    private List<JSONObject> cabFielListNf1;
    private boolean buscarTodosDescFielNf1;
    private int filaInicabFielNf1, filaCantCabFielNf1, filaLimitCabFielNf1;
    private int pageCountCabFielNf1, pageActualCabFielNf1;
    private ObservableList<JSONObject> cabFielDataNf1;
    private TableView<JSONObject> tableCabFielNf1;
    private TableColumn<JSONObject, String> columnNf1;
    private TableColumn<JSONObject, String> columnDescuentoNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf1;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf1;
    private TableColumn<JSONObject, Boolean> columnAccionesNf1;
    //PAGINATION NF1
    //PAGINATION NF2
    private JSONArray cabFielJSONArrayNf2;
    private List<JSONObject> cabFielListNf2;
    private boolean buscarTodosDescFielNf2;
    private int filaInicabFielNf2, filaCantCabFielNf2, filaLimitCabFielNf2;
    private int pageCountCabFielNf2, pageActualCabFielNf2;
    private ObservableList<JSONObject> cabFielDataNf2;
    private TableView<JSONObject> tableCabFielNf2;
    private TableColumn<JSONObject, String> columnNf2;
    private TableColumn<JSONObject, String> columnDescuentoNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf2;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf2;
    private TableColumn<JSONObject, Boolean> columnAccionesNf2;
    //PAGINATION NF2
    //PAGINATION NF3
    private JSONArray cabFielJSONArrayNf3;
    private List<JSONObject> cabFielListNf3;
    private boolean buscarTodosDescFielNf3;
    private int filaInicabFielNf3, filaCantCabFielNf3, filaLimitCabFielNf3;
    private int pageCountCabFielNf3, pageActualCabFielNf3;
    private ObservableList<JSONObject> cabFielDataNf3;
    private TableView<JSONObject> tableCabFielNf3;
    private TableColumn<JSONObject, String> columnNf3;
    private TableColumn<JSONObject, String> columnDescuentoNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf3;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf3;
    private TableColumn<JSONObject, Boolean> columnAccionesNf3;
    //PAGINATION NF3
    //PAGINATION NF4
    private JSONArray cabFielJSONArrayNf4;
    private List<JSONObject> cabFielListNf4;
    private boolean buscarTodosDescFielNf4;
    private int filaInicabFielNf4, filaCantCabFielNf4, filaLimitCabFielNf4;
    private int pageCountCabFielNf4, pageActualCabFielNf4;
    private ObservableList<JSONObject> cabFielDataNf4;
    private TableView<JSONObject> tableCabFielNf4;
    private TableColumn<JSONObject, String> columnNf4;
    private TableColumn<JSONObject, String> columnDescuentoNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf4;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf4;
    private TableColumn<JSONObject, Boolean> columnAccionesNf4;
    //PAGINATION NF4
    //PAGINATION NF5
    private JSONArray cabFielJSONArrayNf5;
    private List<JSONObject> cabFielListNf5;
    private boolean buscarTodosDescFielNf5;
    private int filaInicabFielNf5, filaCantCabFielNf5, filaLimitCabFielNf5;
    private int pageCountCabFielNf5, pageActualCabFielNf5;
    private ObservableList<JSONObject> cabFielDataNf5;
    private TableView<JSONObject> tableCabFielNf5;
    private TableColumn<JSONObject, String> columnNf5;
    private TableColumn<JSONObject, String> columnDescuentoNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf5;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf5;
    private TableColumn<JSONObject, Boolean> columnAccionesNf5;
    //PAGINATION NF5
    //PAGINATION NF6
    private JSONArray cabFielJSONArrayNf6;
    private List<JSONObject> cabFielListNf6;
    private boolean buscarTodosDescFielNf6;
    private int filaInicabFielNf6, filaCantCabFielNf6, filaLimitCabFielNf6;
    private int pageCountCabFielNf6, pageActualCabFielNf6;
    private ObservableList<JSONObject> cabFielDataNf6;
    private TableView<JSONObject> tableCabFielNf6;
    private TableColumn<JSONObject, String> columnNf6;
    private TableColumn<JSONObject, String> columnDescuentoNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf6;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf6;
    private TableColumn<JSONObject, Boolean> columnAccionesNf6;
    //PAGINATION NF6
    //PAGINATION NF7
    private JSONArray cabFielJSONArrayNf7;
    private List<JSONObject> cabFielListNf7;
    private boolean buscarTodosDescFielNf7;
    private int filaInicabFielNf7, filaCantCabFielNf7, filaLimitCabFielNf7;
    private int pageCountCabFielNf7, pageActualCabFielNf7;
    private ObservableList<JSONObject> cabFielDataNf7;
    private TableView<JSONObject> tableCabFielNf7;
    private TableColumn<JSONObject, String> columnNf7;
    private TableColumn<JSONObject, String> columnDescuentoNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielDomingoNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielLunesNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielMartesNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielMiercolesNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielJuevesNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielViernesNf7;
    private TableColumn<JSONObject, CheckBox> columnCabFielSabadoNf7;
    private TableColumn<JSONObject, Boolean> columnAccionesNf7;
    //PAGINATION NF7

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();

    HashMap<String, JSONObject> hashMapSeccion;
    Timer timer;

    private SwitchButton switchButtonEfectivo;
    private SwitchButton switchButtonCheque;
    private SwitchButton switchButtonTarjDebCred;
    private SwitchButton switchButtonMonExtranj;
    private SwitchButton switchButtonNotaCred;
    private SwitchButton switchButtonVale;
    private SwitchButton switchButtonBajada;
    //
    DescuentoFielFormasDePago descuentoFielFormasDePago;
    boolean primeraEntrada;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneDescClienteFiel;
    @FXML
    private SplitPane splitPaneConfigClientFiel;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelConfigClienteFiel;
    @FXML
    private Label labelDescuento;
    @FXML
    private VBox vBoxCheckBox;
    @FXML
    private CheckBox checkBoxDomingo;
    @FXML
    private CheckBox checkBoxLunes;
    @FXML
    private CheckBox checkBoxMartes;
    @FXML
    private CheckBox checkBoxMiercoles;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneParam;
    @FXML
    private Button buttonInsertar;
    @FXML
    private VBox vBoxCheckBox1;
    @FXML
    private ChoiceBox<String> choiceBoxDesc;
    @FXML
    private CheckBox checkBoxJueves;
    @FXML
    private CheckBox checkBoxViernes;
    @FXML
    private CheckBox checkBoxSabado;
    @FXML
    private HBox vBoxFiltro;
    @FXML
    private HBox hBoxContainer;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Label labelDiasSemana;
    @FXML
    private SplitPane splitPaneTreeView;
    @FXML
    private AnchorPane anchorPaneTitleTreeView;
    @FXML
    private Label labelTreeView;
    @FXML
    private AnchorPane anchorPaneTreeView;
    @FXML
    private TreeView<String> treeViewSecciones;
    @FXML
    private TabPane tabPaneNf;
    @FXML
    private Tab tabNivel1;
    @FXML
    private AnchorPane anchorPaneTableViewNf1;
    @FXML
    private Pagination paginationDescFielNf1;
    @FXML
    private Label labelFiltroNf1;
    @FXML
    private Label labelSeccionNf1;
    @FXML
    private TextField textFielFiltroSeccionNf1;
    @FXML
    private Button buttonBuscarSeccionNf1;
    @FXML
    private Button buttonTodosNf1;
    @FXML
    private Button buttonLimpiarNf1;
    @FXML
    private Button buttonCancelarTodasNf1;
    @FXML
    private Tab tabNivel2;
    @FXML
    private AnchorPane anchorPaneTableViewNf2;
    @FXML
    private Pagination paginationDescFielNf2;
    @FXML
    private Label labelFiltroNf2;
    @FXML
    private HBox vBoxFiltro2;
    @FXML
    private Label labelSeccionNf2;
    @FXML
    private TextField textFielFiltroSeccionNf2;
    @FXML
    private Button buttonBuscarSeccionNf2;
    @FXML
    private Button buttonTodosNf2;
    @FXML
    private Button buttonLimpiarNf2;
    @FXML
    private Button buttonCancelarTodasNf2;
    @FXML
    private Tab tabNivel3;
    @FXML
    private AnchorPane anchorPaneTableViewNf3;
    @FXML
    private Pagination paginationDescFielNf3;
    @FXML
    private Label labelFiltroNf3;
    @FXML
    private HBox vBoxFiltro3;
    @FXML
    private Label labelSeccionNf3;
    @FXML
    private TextField textFielFiltroSeccionNf3;
    @FXML
    private Button buttonBuscarSeccionNf3;
    @FXML
    private Button buttonTodosNf3;
    @FXML
    private Button buttonLimpiarNf3;
    @FXML
    private Button buttonCancelarTodasNf3;
    @FXML
    private Tab tabNivel4;
    @FXML
    private AnchorPane anchorPaneTableViewNf4;
    @FXML
    private Pagination paginationDescFielNf4;
    @FXML
    private Label labelFiltroNf4;
    @FXML
    private HBox vBoxFiltro4;
    @FXML
    private Label labelSeccionNf4;
    @FXML
    private TextField textFielFiltroSeccionNf4;
    @FXML
    private Button buttonBuscarSeccionNf4;
    @FXML
    private Button buttonTodosNf4;
    @FXML
    private Button buttonLimpiarNf4;
    @FXML
    private Button buttonCancelarTodasNf4;
    @FXML
    private Tab tabNivel5;
    @FXML
    private AnchorPane anchorPaneTableViewNf5;
    @FXML
    private Pagination paginationDescFielNf5;
    @FXML
    private Label labelFiltroNf5;
    @FXML
    private HBox vBoxFiltro5;
    @FXML
    private Label labelSeccionNf5;
    @FXML
    private TextField textFielFiltroSeccionNf5;
    @FXML
    private Button buttonBuscarSeccionNf5;
    @FXML
    private Button buttonTodosNf5;
    @FXML
    private Button buttonLimpiarNf5;
    @FXML
    private Button buttonCancelarTodasNf5;
    @FXML
    private Tab tabNivel6;
    @FXML
    private AnchorPane anchorPaneTableViewNf6;
    @FXML
    private Pagination paginationDescFielNf6;
    @FXML
    private Label labelFiltroNf6;
    @FXML
    private HBox vBoxFiltro6;
    @FXML
    private Label labelSeccionNf6;
    @FXML
    private TextField textFielFiltroSeccionNf6;
    @FXML
    private Button buttonBuscarSeccionNf6;
    @FXML
    private Button buttonTodosNf6;
    @FXML
    private Button buttonLimpiarNf6;
    @FXML
    private Button buttonCancelarTodasNf6;
    @FXML
    private Tab tabNivel7;
    @FXML
    private AnchorPane anchorPaneTableViewNf7;
    @FXML
    private Pagination paginationDescFielNf7;
    @FXML
    private Label labelFiltroNf7;
    @FXML
    private HBox vBoxFiltro7;
    @FXML
    private Label labelSeccionNf7;
    @FXML
    private TextField textFielFiltroSeccionNf7;
    @FXML
    private Button buttonBuscarSeccionNf7;
    @FXML
    private Button buttonTodosNf7;
    @FXML
    private Button buttonLimpiarNf7;
    @FXML
    private Button buttonCancelarTodasNf7;
    private HBox hBoxContainerFormaPago;
    @FXML
    private VBox vBoxCheckBoxFormaPago;
    @FXML
    private VBox vBoxCheckBoxFormaPago2;
    @FXML
    private Label labelFormasDePago;
    @FXML
    private CheckBox checkBoxSincro;
    @FXML
    private Button buttonActualizarFormasDePago;
    @FXML
    private VBox vBoxBajada;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoDescFiel();
    }

    @FXML
    private void anchorPaneDescClienteFielKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonBuscarSeccionNf1Action(ActionEvent event) {
        buscandoDescuentoFiltroNf1();
    }

    @FXML
    private void buttonTodosNf1Action(ActionEvent event) {
        buscandoDescuentoTodoNf1();
    }

    @FXML
    private void buttonLimpiarNf1Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf1Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf2Action(ActionEvent event) {
        buscandoDescuentoFiltroNf2();
    }

    @FXML
    private void buttonTodosNf2Action(ActionEvent event) {
        buscandoDescuentoTodoNf2();
    }

    @FXML
    private void buttonLimpiarNf2Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf2Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf3Action(ActionEvent event) {
        buscandoDescuentoFiltroNf3();
    }

    @FXML
    private void buttonTodosNf3Action(ActionEvent event) {
        buscandoDescuentoTodoNf3();
    }

    @FXML
    private void buttonLimpiarNf3Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf3Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf4Action(ActionEvent event) {
        buscandoDescuentoFiltroNf4();
    }

    @FXML
    private void buttonTodosNf4Action(ActionEvent event) {
        buscandoDescuentoTodoNf4();
    }

    @FXML
    private void buttonLimpiarNf4Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf4Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf5Action(ActionEvent event) {
        buscandoDescuentoFiltroNf5();
    }

    @FXML
    private void buttonTodosNf5Action(ActionEvent event) {
        buscandoDescuentoTodoNf5();
    }

    @FXML
    private void buttonLimpiarNf5Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf5Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf6Action(ActionEvent event) {
        buscandoDescuentoFiltroNf6();
    }

    @FXML
    private void buttonTodosNf6Action(ActionEvent event) {
        buscandoDescuentoTodoNf6();
    }

    @FXML
    private void buttonLimpiarNf6Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf6Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonBuscarSeccionNf7Action(ActionEvent event) {
        buscandoDescuentoFiltroNf7();
    }

    @FXML
    private void buttonTodosNf7Action(ActionEvent event) {
        buscandoDescuentoTodoNf7();
    }

    @FXML
    private void buttonLimpiarNf7Action(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonCancelarTodasNf7Action(ActionEvent event) {
        cancelandoDescuentoClienteFielTodo();
    }

    @FXML
    private void buttonActualizarFormasDePagoAction(ActionEvent event) {
        actualizandoFormasPago();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        cargandoChoiceDescuento();
        cargandoNf();
        alert = false;
        diasCargar = false;
        exitoInsertarCab = false;
        exitoBaja = false;
        buscarTodosDescFielNf1 = true;
        listenTab();
        tabeandoNf("tabNivel1");
        agregandoToggleButton();
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/DescuentoClienteFielFXML.fxml", 1263, 707, false);
    }

    private void viendoDetalleSeccion() {
        this.sc.loadScreen("/vista/descuento/DescuentoClienteFielDetalleFXML.fxml", 581, 743, "/vista/descuento/DescuentoClienteFielFXML.fxml", 1263, 707, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void agregandoToggleButton() {
        primeraEntrada = true;
        descuentoFielFormasDePago = descuentoFielFormasDePagoDAO.getById(1l);
        switchButtonEfectivo = new SwitchButton("EFECTIVO");
        switchButtonEfectivo.primeraCarga(descuentoFielFormasDePago.getEfectivo());
        switchButtonCheque = new SwitchButton("CHEQUE");
        switchButtonCheque.primeraCarga(descuentoFielFormasDePago.getCheque());
        switchButtonTarjDebCred = new SwitchButton("TARJETAS");
        switchButtonTarjDebCred.primeraCarga(descuentoFielFormasDePago.getTarjeta());
        switchButtonMonExtranj = new SwitchButton("MON. EXTR.");
        switchButtonMonExtranj.primeraCarga(descuentoFielFormasDePago.getMonExtr());
        switchButtonNotaCred = new SwitchButton("NOTA CRÉD.");
        switchButtonNotaCred.primeraCarga(descuentoFielFormasDePago.getNotaCred());
        switchButtonVale = new SwitchButton("VALE");
        switchButtonVale.primeraCarga(descuentoFielFormasDePago.getVale());
        switchButtonBajada = new SwitchButton("BAJADA");
        switchButtonBajada.primeraCarga(descuentoFielFormasDePago.getBajada());
        HBox hBoxFormaPago1 = new HBox();
        HBox hBoxFormaPago2 = new HBox();
        HBox hBoxFormaPago3 = new HBox();
        HBox hBoxFormaPago4 = new HBox();
        HBox hBoxFormaPago5 = new HBox();
        HBox hBoxFormaPago6 = new HBox();
        HBox hBoxFormaPago7 = new HBox();
        Label labelEfe = new Label("efectivo");
        labelEfe.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago1.getChildren().add(labelEfe);
        hBoxFormaPago1.getChildren().add(switchButtonEfectivo);
        hBoxFormaPago1.setSpacing(10);
        hBoxFormaPago1.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago.getChildren().add(hBoxFormaPago1);
        Label labelTarjDebCred = new Label("tarjeta");
        labelTarjDebCred.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago2.getChildren().add(labelTarjDebCred);
        hBoxFormaPago2.getChildren().add(switchButtonTarjDebCred);
        hBoxFormaPago2.setSpacing(10);
        hBoxFormaPago2.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago.getChildren().add(hBoxFormaPago2);
        Label labelMonExtran = new Label("m. extr.");
        labelMonExtran.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago3.getChildren().add(labelMonExtran);
        hBoxFormaPago3.getChildren().add(switchButtonMonExtranj);
        hBoxFormaPago3.setSpacing(10);
        hBoxFormaPago3.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago.getChildren().add(hBoxFormaPago3);
        Label labelNotaCred = new Label("nota créd.");
        labelNotaCred.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago4.getChildren().add(labelNotaCred);
        hBoxFormaPago4.getChildren().add(switchButtonNotaCred);
        hBoxFormaPago4.setSpacing(10);
        hBoxFormaPago4.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago2.getChildren().add(hBoxFormaPago4);
        Label labelCheque = new Label("cheque");
        labelCheque.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago5.getChildren().add(labelCheque);
        hBoxFormaPago5.getChildren().add(switchButtonCheque);
        hBoxFormaPago5.setSpacing(10);
        hBoxFormaPago5.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago2.getChildren().add(hBoxFormaPago5);
        Label labelVale = new Label("vale func.");
        labelVale.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago6.getChildren().add(labelVale);
        hBoxFormaPago6.getChildren().add(switchButtonVale);
        hBoxFormaPago6.setSpacing(10);
        hBoxFormaPago6.setAlignment(Pos.CENTER_RIGHT);
        vBoxCheckBoxFormaPago2.getChildren().add(hBoxFormaPago6);
        Label labelBajada = new Label("bajada");
        labelBajada.setStyle("-fx-font-weight: bold;");
        hBoxFormaPago7.getChildren().add(labelBajada);
        hBoxFormaPago7.getChildren().add(switchButtonBajada);
        hBoxFormaPago7.setSpacing(10);
        hBoxFormaPago7.setAlignment(Pos.CENTER_RIGHT);
        vBoxBajada.getChildren().add(hBoxFormaPago7);
        primeraEntrada = false;
    }

    private void listenTab() {
        tabPaneNf.getSelectionModel().selectedItemProperty().addListener((ov, oldTab, newTab) -> {
            tabeandoNf(newTab.getId());
        });
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoDescFiel();
        }
        if (keyCode == event.getCode().F2) {
            if (tabPaneNf.getSelectionModel().isSelected(0)) {
                buscandoDescuentoFiltroNf1();
            } else if (tabPaneNf.getSelectionModel().isSelected(1)) {
                buscandoDescuentoFiltroNf2();
            } else if (tabPaneNf.getSelectionModel().isSelected(2)) {
                buscandoDescuentoFiltroNf3();
            } else if (tabPaneNf.getSelectionModel().isSelected(3)) {
                buscandoDescuentoFiltroNf4();
            } else if (tabPaneNf.getSelectionModel().isSelected(4)) {
                buscandoDescuentoFiltroNf5();
            } else if (tabPaneNf.getSelectionModel().isSelected(5)) {
                buscandoDescuentoFiltroNf6();
            } else if (tabPaneNf.getSelectionModel().isSelected(6)) {
                buscandoDescuentoFiltroNf7();
            }
        }
        if (keyCode == event.getCode().F3) {
            if (tabPaneNf.getSelectionModel().isSelected(0)) {
                buscandoDescuentoTodoNf1();
            } else if (tabPaneNf.getSelectionModel().isSelected(1)) {
                buscandoDescuentoTodoNf2();
            } else if (tabPaneNf.getSelectionModel().isSelected(2)) {
                buscandoDescuentoTodoNf3();
            } else if (tabPaneNf.getSelectionModel().isSelected(3)) {
                buscandoDescuentoTodoNf4();
            } else if (tabPaneNf.getSelectionModel().isSelected(4)) {
                buscandoDescuentoTodoNf5();
            } else if (tabPaneNf.getSelectionModel().isSelected(5)) {
                buscandoDescuentoTodoNf6();
            } else if (tabPaneNf.getSelectionModel().isSelected(6)) {
                buscandoDescuentoTodoNf7();
            }
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().F12) {
            cancelandoDescuentoClienteFielTodo();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoChoiceDescuento() {
        //nada de loops y mod...
        choiceBoxDesc.getItems().add("-");
        choiceBoxDesc.getItems().add("5 %");
        choiceBoxDesc.getItems().add("10 %");
        choiceBoxDesc.getItems().add("15 %");
        choiceBoxDesc.getItems().add("20 %");
        choiceBoxDesc.getItems().add("25 %");
        choiceBoxDesc.getItems().add("30 %");
        choiceBoxDesc.getItems().add("35 %");
        choiceBoxDesc.getItems().add("40 %");
        choiceBoxDesc.getItems().add("45 %");
        choiceBoxDesc.getItems().add("50 %");
        choiceBoxDesc.getSelectionModel().select(0);
    }

    private void tabeandoNf(Object param) {
        String tab = "";
        if (param instanceof Integer) {
            int p = Integer.valueOf(param.toString());
            switch (p) {
                case 1:
                    tab = "tabNivel1";
                    tabPaneNf.getSelectionModel().select(0);
                    break;
                case 2:
                    tab = "tabNivel2";
                    tabPaneNf.getSelectionModel().select(1);
                    break;
                case 3:
                    tab = "tabNivel3";
                    tabPaneNf.getSelectionModel().select(2);
                    break;
                case 4:
                    tab = "tabNivel4";
                    tabPaneNf.getSelectionModel().select(3);
                    break;
                case 5:
                    tab = "tabNivel5";
                    tabPaneNf.getSelectionModel().select(4);
                    break;
                case 6:
                    tab = "tabNivel6";
                    tabPaneNf.getSelectionModel().select(5);
                    break;
                case 7:
                    tab = "tabNivel7";
                    tabPaneNf.getSelectionModel().select(6);
                    break;
            }
        } else {
            tab = String.valueOf(param);
        }
        switch (tab) {
            case "tabNivel1":
                primeraPaginacionNf1();
                tabNivel1.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel2":
                primeraPaginacionNf2();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel3":
                primeraPaginacionNf3();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel4":
                primeraPaginacionNf4();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel5":
                primeraPaginacionNf5();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel6":
                primeraPaginacionNf6();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                tabNivel7.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                break;
            case "tabNivel7":
                primeraPaginacionNf7();
                tabNivel1.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel2.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel3.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel4.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel5.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel6.setStyle("-fx-opacity: 0.3; -fx-background-color: #f2f2f2;");
                tabNivel7.setStyle("-fx-opacity: 1; -fx-background-color: #39aedd;");
                break;
        }
    }

    private void limpiandoCampos(Integer nf) {
        choiceBoxDesc.getSelectionModel().select(0);
        limpiandoBusqueda();
        checkBoxDomingo.setSelected(false);
        checkBoxLunes.setSelected(false);
        checkBoxMartes.setSelected(false);
        checkBoxMiercoles.setSelected(false);
        checkBoxJueves.setSelected(false);
        checkBoxViernes.setSelected(false);
        checkBoxSabado.setSelected(false);
        tabeandoNf(nf);
    }

    private void limpiandoBusqueda() {
        textFielFiltroSeccionNf1.setText("");
        textFielFiltroSeccionNf2.setText("");
        textFielFiltroSeccionNf3.setText("");
        textFielFiltroSeccionNf4.setText("");
        textFielFiltroSeccionNf5.setText("");
        textFielFiltroSeccionNf6.setText("");
        textFielFiltroSeccionNf7.setText("");
    }

    private void insertandoDescFiel() {
        Integer nf = 0;
        String descripcionNivel = "";
        Long idNf = 0l;
        if (treeViewSecciones.getSelectionModel().getSelectedItem() != null) {
            if (hashMapNf1.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 1;
                descripcionNivel = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf1Tipo();
            } else if (hashMapNf2.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 2;
                descripcionNivel = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf2Sfamilia();
            } else if (hashMapNf3.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 3;
                descripcionNivel = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf3Sseccion();
            } else if (hashMapNf4.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 4;
                descripcionNivel = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf4Seccion1();
            } else if (hashMapNf5.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 5;
                descripcionNivel = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf5Seccion2();
            } else if (hashMapNf6.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 6;
                descripcionNivel = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf6Secnom6();
            } else if (hashMapNf7.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 7;
                descripcionNivel = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf7Secnom7();
            }
        }
        if (!checkBoxDomingo.isSelected() && !checkBoxLunes.isSelected()
                && !checkBoxMartes.isSelected() && !checkBoxMiercoles.isSelected()
                && !checkBoxJueves.isSelected() && !checkBoxViernes.isSelected()
                && !checkBoxSabado.isSelected()) {
            mensajeError("DEBE SELECCIONAR AL MENOS UN DÍA DE LA SEMANA.");
        } else if (nf == 0) {
            mensajeError("DEBE SELECCIONAR UNA SECCIÓN.");
        } else if (choiceBoxDesc.getSelectionModel().getSelectedItem().contentEquals("-")) {
            mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
        } else if (validacionInsercion(nf, idNf)) {
            mensajeError("LA SECCIÓN [ " + descripcionNivel.toUpperCase() + " ] DEL NIVEL [ " + nf + "]\n"
                    + "ACTUALMENTE SE ENCUENTRA EN USO.");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR [ " + choiceBoxDesc.getValue() + " ] DESCUENTO FIEL \nEN NIVEL "
                    + nf + ", " + descripcionNivel + " Y SUBSECCIONES ?", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
//                if ("guardar_tarjeta_desc_fiel") {
//                    
//                }
//                
//                    ) {
                if (creandoCabDescFiel(nf)) {
                    exitoInsertarCab = false;
                    limpiandoCampos(nf);
                } else {
                    mensajeError("NO SE PUDO INSERTAR DESCUENTO FIEL.\nVERIFIQUE LOS CAMPOS.");
                }
//                }else {
//                    mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                }
            } else if (alert.getResult() == cancel) {
                alert.close();
            }
        }
    }

    private void cargandoNf() {
        tabNivel1.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
        tabNivel1.setStyle("-fx-font-weight: bold;");
        tabNivel2.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png"))));
        tabNivel2.setStyle("-fx-font-weight: bold;");
        tabNivel3.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png"))));
        tabNivel3.setStyle("-fx-font-weight: bold;");
        tabNivel4.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png"))));
        tabNivel4.setStyle("-fx-font-weight: bold;");
        tabNivel5.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png"))));
        tabNivel5.setStyle("-fx-font-weight: bold;");
        tabNivel6.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png"))));
        tabNivel6.setStyle("-fx-font-weight: bold;");
        tabNivel7.setGraphic(new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png"))));
        tabNivel7.setStyle("-fx-font-weight: bold;");
        generarNivelFamiliaLocal();
    }

    private void cancelandoDescuentoClienteFielTodo() {
//        if ("baja_desc_fiel")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN CLIENTE FIEL?", ok, cancel);
        alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            if (bajaDescClienteFielTodo()) {
                exitoBaja = false;
                limpiandoCampos(1);
            }
            alert2.close();
        } else if (alert2.getResult() == cancel) {
            alert2.close();
        }
//        } else {
//            mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//        }
    }

    private void actualizandoFormasPago() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿ACTUALIZAR FORMAS DE PAGO EN DTO. FIEL?", ok, cancel);
        alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            if (actualizarCabFielFormaPago(descuentoFielFormasDePago)) {
                exitoFormaPago = false;
            }
            alert2.close();
        } else if (alert2.getResult() == cancel) {
            alert2.close();
        }
    }

    private Integer jsonRowCountNf1() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf1Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf1(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf1Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf1(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf1 = generarDescuentoFetchFiltroDescuentoFielCabNf1(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf1) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf1(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf1 = generarFetchDescuentoFielCabNf1(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf1) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf2() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf2Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf2(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf2Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf2(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf2 = generarDescuentoFetchFiltroDescuentoFielCabNf2(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf2) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf2(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf2 = generarFetchDescuentoFielCabNf2(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf2) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf3() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf3Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf3(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf3Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf3(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf3 = generarDescuentoFetchFiltroDescuentoFielCabNf3(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf3) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf3(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf3 = generarFetchDescuentoFielCabNf3(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf3) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf4() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf4Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf4(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf4Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf4(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf4 = generarDescuentoFetchFiltroDescuentoFielCabNf4(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf4) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf4(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf4 = generarFetchDescuentoFielCabNf4(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf4) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf5() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf5Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf5(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf5Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf5(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf5 = generarDescuentoFetchFiltroDescuentoFielCabNf5(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf5) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf5(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf5 = generarFetchDescuentoFielCabNf5(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf5) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf6() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf6Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf6(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf6Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf6(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf6 = generarDescuentoFetchFiltroDescuentoFielCabNf6(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf6) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf6(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf6 = generarFetchDescuentoFielCabNf6(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf6) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Integer jsonRowCountNf7() {
        Integer rowCount = 0;
        rowCount = generarCountDescuentoClienteFielNf7Local();
        return rowCount;
    }

    private Integer jsonRowCountFilterNf7(String seccionFilter) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFiltroDescuentoClienteFielNf7Local(seccionFilter);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayCabFielFetchFiltroNf7(int limRow, int offSet, String seccionFiltro) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String seccion = "null";
        if (!seccionFiltro.contentEquals("")) {
            seccion = seccionFiltro;
        }
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf7 = generarDescuentoFetchFiltroDescuentoFielCabNf7(limRowS, offSetS, seccion);
        for (Object cabFielObj : cabFielJSONArrayNf7) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> jsonArrayCabFielFetchNf7(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cabFielJSONArrayNf7 = generarFetchDescuentoFielCabNf7(limRowS, offSetS);
        for (Object cabFielObj : cabFielJSONArrayNf7) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private List<JSONObject> readJsonDias() {
        List<JSONObject> diaJSONObjList = new ArrayList<>();
        diaJSONObjList = generarDiasLocal();
        return diaJSONObjList;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE FIEL CAB. -> POST
    private boolean creandoCabDescFiel(int nf) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject cabDescFiel = new JSONObject();
        try {
            cabDescFiel = creandoJsonCabFiel();
            if (cabDescFiel.isEmpty()) {
                return false;
            }
            CajaDatos.setIdDescuentoClienteFiel(rangoDescFielDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal()));
            cabDescFiel.put("idDescuentoFielCab", CajaDatos.getIdDescuentoClienteFiel());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(cabDescFiel.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        CajaDatos.setDescuentoClienteFielCab(true);
                        cabDescFiel = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setIdDescuentoClienteFiel(Long.parseLong(cabDescFiel.get("idDescuentoFielCab").toString()));
                        exitoInsertarCab = true;
                    }
                    br.close();
                } else {
                    cabDescFiel = registrarDescuentoFielCab(cabDescFiel);
                }
            } else {
                cabDescFiel = registrarDescuentoFielCab(cabDescFiel);
            }
        } catch (IOException | ParseException ex) {
            cabDescFiel = registrarDescuentoFielCab(cabDescFiel);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        if (cabDescFiel != null) {
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            cabDescFiel.put("fechaAlta", null);
            cabDescFiel.put("fechaMod", null);
            DescuentoFielCabDTO descFielDTO = gson.fromJson(cabDescFiel.toString(), DescuentoFielCabDTO.class);
            descFielDTO.setFechaAlta(timestamp);
            descFielDTO.setFechaMod(timestamp);
            boolean valor = descFielDAO.insertarObtenerEstado(DescuentoFielCab.fromDescuentoFielCabDTO(descFielDTO));
            if (valor) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL DESCUENTO FIEL CABECERA");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL DESCUENTO FIEL CABECERA");
            }
        }
        if (exitoInsertarCab) {
            if (!diasCargar) {
                diasList = readJsonDias();
            }
            if (checkBoxDomingo.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(0)));
            }
            if (checkBoxLunes.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(1)));
            }
            if (checkBoxMartes.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(2)));
            }
            if (checkBoxMiercoles.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(3)));
            }
            if (checkBoxJueves.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(4)));
            }
            if (checkBoxViernes.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(5)));
            }
            if (checkBoxSabado.isSelected()) {
                creandoDetDescFiel(creandoJsonDetFiel(cabDescFiel, diasList.get(6)));
            }
            switch (nf) {
                case 1:
                    try {
                        JSONObject nf1 = (JSONObject) parser.parse(gson.toJson(
                                Nf1Tipo.toNf1TipoDTOEntitiesNull(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf1DescFiel(creandoJsonNf1Fiel(cabDescFiel, nf1));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf1: ", ex.fillInStackTrace());
                    }
                    break;
                case 2:
                    try {
                        JSONObject nf2 = (JSONObject) parser.parse(gson.toJson(
                                Nf2Sfamilia.toNf2SfamiliaDTOEntitiesNull(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf2DescFiel(creandoJsonNf2Fiel(cabDescFiel, nf2));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf2: ", ex.fillInStackTrace());
                    }
                    break;
                case 3:
                    try {
                        JSONObject nf3 = (JSONObject) parser.parse(gson.toJson(
                                Nf3Sseccion.toNf3SseccionDTOEntitiesNull(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf3DescFiel(creandoJsonNf3Fiel(cabDescFiel, nf3));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf3: ", ex.fillInStackTrace());
                    }
                    break;
                case 4:
                    try {
                        JSONObject nf4 = (JSONObject) parser.parse(gson.toJson(
                                Nf4Seccion1.toNf4Seccion1DTOEntitiesNull(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf4DescFiel(creandoJsonNf4Fiel(cabDescFiel, nf4));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf4: ", ex.fillInStackTrace());
                    }
                    break;
                case 5:
                    try {
                        JSONObject nf5 = (JSONObject) parser.parse(gson.toJson(
                                Nf5Seccion2.toNf5Seccion2DTOEntitiesNull(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf5DescFiel(creandoJsonNf5Fiel(cabDescFiel, nf5));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf5: ", ex.fillInStackTrace());
                    }
                    break;
                case 6:
                    try {
                        JSONObject nf6 = (JSONObject) parser.parse(gson.toJson(
                                Nf6Secnom6.toNf6Secnom6DTOEntitiesNull(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf6DescFiel(creandoJsonNf6Fiel(cabDescFiel, nf6));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf6: ", ex.fillInStackTrace());
                    }
                    break;
                case 7:
                    try {
                        JSONObject nf7 = (JSONObject) parser.parse(gson.toJson(
                                Nf7Secnom7.toNf7Secnom7DTOEntitiesNull(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))));
                        creandoNf7DescFiel(creandoJsonNf7Fiel(cabDescFiel, nf7));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException nf7: ", ex.fillInStackTrace());
                    }
                    break;
                default:
                    break;
            }
        }
        CajaDatos.setDescuentoClienteFielCab(false);
        CajaDatos.setIdDescuentoClienteFiel(0);
        return exitoInsertarCab;
    }
    //////CREATE, CLIENTE FIEL CAB. -> POST

    //////CREATE, CLIENTE FIEL DET. -> POST
    private boolean creandoDetDescFiel(JSONObject detDescFiel) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            detDescFiel.put("idDescuentoFielCab", CajaDatos.getIdDescuentoClienteFiel());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielDet");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(detDescFiel.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarDescuentoFielDetLocal(detDescFiel.toString());
                }
            } else {
                exitoInsertarDet = registrarDescuentoFielDetLocal(detDescFiel.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarDescuentoFielDetLocal(detDescFiel.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielDetDTO descDTO = gson.fromJson(detDescFiel.toString(), DescuentoFielDetDTO.class);
        if (descFielDetDAO.insertarObtenerEstado(DescuentoFielDet.fromDescuentoFielDetDTO(descDTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL DET");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL DET");
        }
        return exitoInsertarDet;
    }
    //////CREATE, CLIENTE FIEL DET. -> POST

    //////CREATE, CLIENTE FIEL NF1. -> POST
    private boolean creandoNf1DescFiel(JSONObject jsonDescuentoFielNf1) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf1 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf1 = jsonDescuentoFielNf1;
                JSONObject jsonNf1Tipo = (JSONObject) jsonDescFielNf1.get("nf1Tipo");
                jsonNf1Tipo.put("fechaAlta", null);
                jsonNf1Tipo.put("fechaMod", null);
                jsonDescFielNf1.put("nf1Tipo", jsonNf1Tipo);
                wr.write(jsonDescFielNf1.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf1 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf1 = registrarDescuentoFielNf1Local(jsonDescuentoFielNf1.toString());
                }
            } else {
                exitoInsertarNf1 = registrarDescuentoFielNf1Local(jsonDescuentoFielNf1.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf1 = registrarDescuentoFielNf1Local(jsonDescuentoFielNf1.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf1DTO descuentoFielNf1DTO = gson.fromJson(jsonDescuentoFielNf1.toString(), DescuentoFielNf1DTO.class);
        if (descuentoFielNf1DAO.insertarObtenerEstado(DescuentoFielNf1.fromDescuentoFielNf1DTOEntitiesFull(descuentoFielNf1DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF1");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF1");
        }
        return exitoInsertarNf1;
    }
    //////CREATE, CLIENTE FIEL NF1. -> POST

    //////CREATE, CLIENTE FIEL NF2. -> POST
    private boolean creandoNf2DescFiel(JSONObject jsonDescuentoFielNf2) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf2 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf2 = jsonDescuentoFielNf2;
                JSONObject jsonNf2Sfamilia = (JSONObject) jsonDescFielNf2.get("nf2Sfamilia");
                jsonNf2Sfamilia.put("fechaAlta", null);
                jsonNf2Sfamilia.put("fechaMod", null);
                jsonDescFielNf2.put("nf2Sfamilia", jsonNf2Sfamilia);
                wr.write(jsonDescFielNf2.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf2 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf2 = registrarDescuentoFielNf2Local(jsonDescuentoFielNf2.toString());
                }
            } else {
                exitoInsertarNf2 = registrarDescuentoFielNf2Local(jsonDescuentoFielNf2.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf2 = registrarDescuentoFielNf2Local(jsonDescuentoFielNf2.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf2DTO descuentoFielNf2DTO = gson.fromJson(jsonDescuentoFielNf2.toString(), DescuentoFielNf2DTO.class);
        if (descuentoFielNf2DAO.insertarObtenerEstado(DescuentoFielNf2.fromDescuentoFielNf2DTOEntitiesFull(descuentoFielNf2DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF2");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF2");
        }
        return exitoInsertarNf2;
    }
    //////CREATE, CLIENTE FIEL NF2. -> POST

    //////CREATE, CLIENTE FIEL NF3. -> POST
    private boolean creandoNf3DescFiel(JSONObject jsonDescuentoFielNf3) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf3 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf3");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf3 = jsonDescuentoFielNf3;
                JSONObject jsonNf3Sseccion = (JSONObject) jsonDescFielNf3.get("nf3Sseccion");
                jsonNf3Sseccion.put("fechaAlta", null);
                jsonNf3Sseccion.put("fechaMod", null);
                jsonDescFielNf3.put("nf3Sseccion", jsonNf3Sseccion);
                wr.write(jsonDescFielNf3.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf3 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf3 = registrarDescuentoFielNf3Local(jsonDescuentoFielNf3.toString());
                }
            } else {
                exitoInsertarNf3 = registrarDescuentoFielNf3Local(jsonDescuentoFielNf3.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf3 = registrarDescuentoFielNf3Local(jsonDescuentoFielNf3.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf3DTO descuentoFielNf3DTO = gson.fromJson(jsonDescuentoFielNf3.toString(), DescuentoFielNf3DTO.class);
        if (descuentoFielNf3DAO.insertarObtenerEstado(DescuentoFielNf3.fromDescuentoFielNf3DTOEntitiesFull(descuentoFielNf3DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF3");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF3");
        }
        return exitoInsertarNf3;
    }
    //////CREATE, CLIENTE FIEL NF3. -> POST

    //////CREATE, CLIENTE FIEL NF4. -> POST
    private boolean creandoNf4DescFiel(JSONObject jsonDescuentoFielNf4) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf4 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf4");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf4 = jsonDescuentoFielNf4;
                JSONObject jsonNf4Seccion1 = (JSONObject) jsonDescFielNf4.get("nf4Seccion1");
                jsonNf4Seccion1.put("fechaAlta", null);
                jsonNf4Seccion1.put("fechaMod", null);
                jsonDescFielNf4.put("nf4Seccion1", jsonNf4Seccion1);
                wr.write(jsonDescFielNf4.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf4 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf4 = registrarDescuentoFielNf4Local(jsonDescuentoFielNf4.toString());
                }
            } else {
                exitoInsertarNf4 = registrarDescuentoFielNf4Local(jsonDescuentoFielNf4.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf4 = registrarDescuentoFielNf4Local(jsonDescuentoFielNf4.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf4DTO descuentoFielNf4DTO = gson.fromJson(jsonDescuentoFielNf4.toString(), DescuentoFielNf4DTO.class);
        if (descuentoFielNf4DAO.insertarObtenerEstado(DescuentoFielNf4.fromDescuentoFielNf4DTOEntitiesFull(descuentoFielNf4DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF4");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF4");
        }
        return exitoInsertarNf4;
    }
    //////CREATE, CLIENTE FIEL NF4. -> POST

    //////CREATE, CLIENTE FIEL NF5. -> POST
    private boolean creandoNf5DescFiel(JSONObject jsonDescuentoFielNf5) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf5 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf5");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf5 = jsonDescuentoFielNf5;
                JSONObject jsonNf5Seccion2 = (JSONObject) jsonDescFielNf5.get("nf5Seccion2");
                jsonNf5Seccion2.put("fechaAlta", null);
                jsonNf5Seccion2.put("fechaMod", null);
                jsonDescFielNf5.put("nf5Seccion2", jsonNf5Seccion2);
                wr.write(jsonDescFielNf5.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf5 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf5 = registrarDescuentoFielNf5Local(jsonDescuentoFielNf5.toString());
                }
            } else {
                exitoInsertarNf5 = registrarDescuentoFielNf5Local(jsonDescuentoFielNf5.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf5 = registrarDescuentoFielNf5Local(jsonDescuentoFielNf5.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf5DTO descuentoFielNf5DTO = gson.fromJson(jsonDescuentoFielNf5.toString(), DescuentoFielNf5DTO.class);
        if (descuentoFielNf5DAO.insertarObtenerEstado(DescuentoFielNf5.fromDescuentoFielNf5DTOEntitiesFull(descuentoFielNf5DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF5");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF5");
        }
        return exitoInsertarNf5;
    }
    //////CREATE, CLIENTE FIEL NF5. -> POST

    //////CREATE, CLIENTE FIEL NF6. -> POST
    private boolean creandoNf6DescFiel(JSONObject jsonDescuentoFielNf6) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf6 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf6");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf6 = jsonDescuentoFielNf6;
                JSONObject jsonNf6Secnom6 = (JSONObject) jsonDescFielNf6.get("nf6Secnom6");
                jsonNf6Secnom6.put("fechaAlta", null);
                jsonNf6Secnom6.put("fechaMod", null);
                jsonDescFielNf6.put("nf6Secnom6", jsonNf6Secnom6);
                wr.write(jsonDescFielNf6.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf6 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf6 = registrarDescuentoFielNf6Local(jsonDescuentoFielNf6.toString());
                }
            } else {
                exitoInsertarNf6 = registrarDescuentoFielNf6Local(jsonDescuentoFielNf6.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf6 = registrarDescuentoFielNf6Local(jsonDescuentoFielNf6.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf6DTO descuentoFielNf6DTO = gson.fromJson(jsonDescuentoFielNf6.toString(), DescuentoFielNf6DTO.class);
        if (descuentoFielNf6DAO.insertarObtenerEstado(DescuentoFielNf6.fromDescuentoFielNf6DTOEntitiesFull(descuentoFielNf6DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF6");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF6");
        }
        return exitoInsertarNf6;
    }
    //////CREATE, CLIENTE FIEL NF6. -> POST

    //////CREATE, CLIENTE FIEL NF7. -> POST
    private boolean creandoNf7DescFiel(JSONObject jsonDescuentoFielNf7) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarNf7 = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoClienteFielCab() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielNf7");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonDescFielNf7 = jsonDescuentoFielNf7;
                JSONObject jsonNf7Secnom7 = (JSONObject) jsonDescFielNf7.get("nf7Secnom7");
                jsonNf7Secnom7.put("fechaAlta", null);
                jsonNf7Secnom7.put("fechaMod", null);
                jsonDescFielNf7.put("nf7Secnom7", jsonNf7Secnom7);
                wr.write(jsonDescFielNf7.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarNf7 = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarNf7 = registrarDescuentoFielNf7Local(jsonDescuentoFielNf7.toString());
                }
            } else {
                exitoInsertarNf7 = registrarDescuentoFielNf7Local(jsonDescuentoFielNf7.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarNf7 = registrarDescuentoFielNf7Local(jsonDescuentoFielNf7.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        DescuentoFielNf7DTO descuentoFielNf7DTO = gson.fromJson(jsonDescuentoFielNf7.toString(), DescuentoFielNf7DTO.class);
        if (descuentoFielNf7DAO.insertarObtenerEstado(DescuentoFielNf7.fromDescuentoFielNf7DTOEntitiesFull(descuentoFielNf7DTO))) {
            System.out.println("DATOS PERSISTIDOS EN DESCUENTO FIEL NF7");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN DESCUENTO FIEL NF7");
        }
        return exitoInsertarNf7;
    }
    //////CREATE, CLIENTE FIEL NF7. -> POST

    //////READ, CLIENTE FIEL -> GET
    private JSONObject recuperarDatoFielCab(long idDescuentoFielCab) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielCab/getById/" + idDescuentoFielCab);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR IOException | ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, CLIENTE FIEL -> GET

    //////UPDATE, BAJA CLIENTE FIEL -> PUT
    private boolean bajaCabFiel(JSONObject descFielCab) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            //VERIFICAR
            boolean status = false;
            if (recuperarDatoFielCab(Long.parseLong(descFielCab.get("idDescuentoFielCab").toString())) != null) {
                status = true;
            }
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielCab");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                long idDesc = Long.valueOf(descFielCab.get("idDescuentoFielCab").toString());
                DescuentoFielCab descu = descFielDAO.getById(idDesc);
                long fechaAlta = descu.getFechaAlta().getTime();
                descFielCab.put("fechaAlta", fechaAlta);
                wr.write(descFielCab.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarDescuentoFielCab(descFielCab.toString());
                }
            } else {
                exitoBaja = actualizarDescuentoFielCab(descFielCab.toString());
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarDescuentoFielCab(descFielCab.toString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            if (descFielCab.get("fechaAlta") != null) {
                DescuentoFielCab descuentoCab = descFielDAO.getById(Long.parseLong(descFielCab.get("idDescuentoFielCab").toString()));
                fechaAlta = descuentoCab.getFechaAlta();
                descFielCab.put("fechaAlta", null);
            }
            if (descFielCab.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descFielCab.get("fechaMod").toString()));
                descFielCab.put("fechaMod", null);
            }
            DescuentoFielCabDTO descuentoFielCabDTO = gson.fromJson(descFielCab.toString(), DescuentoFielCabDTO.class);
            descuentoFielCabDTO.setFechaAlta(fechaAlta);
            descuentoFielCabDTO.setFechaMod(fechaMod);
            descuentoFielCabDTO.setEstadoDesc(false);
            descFielDAO.actualizar(DescuentoFielCab.fromDescuentoFielCabDTO(descuentoFielCabDTO));
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO FIEL CANCELADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE CANCELÓ EL DESCUENTO FIEL.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA CLIENTE FIEL -> PUT

    //////UPDATE, CLIENTE FIEL FORMA DE PAGO -> PUT
    @SuppressWarnings("null")
    private boolean actualizarCabFielFormaPago(DescuentoFielFormasDePago descuentoFielFormasDePago) {
        String inputLine;
        JSONParser parser = new JSONParser();
        Jsonb jsonb = JsonbBuilder.create();
        descuentoFielFormasDePago.setUsuMod(Identity.getNomFun());
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        descuentoFielFormasDePago.setFechaMod(ts);
        String result = jsonb.toJson(descuentoFielFormasDePago);
        JSONObject json = null;
        try {
            json = (JSONObject) parser.parse(result);
            json.put("fechaMod", ts.getTime());
        } catch (ParseException ex) {
            Logger.getLogger(DescuentoClienteFielFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && json != null
                    && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielFormasDePago");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(json.toJSONString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoFormaPago = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoFormaPago = actualizarDescuentoFielCabFormaPago(json.toJSONString());
                }
            } else {
                exitoFormaPago = actualizarDescuentoFielCabFormaPago(json.toJSONString());
            }
        } catch (IOException | ParseException ex) {
            exitoFormaPago = actualizarDescuentoFielCabFormaPago(json.toJSONString());
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoFormaPago) {
            descuentoFielFormasDePagoDAO.actualizar(descuentoFielFormasDePago);
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡FORMAS DE PAGO ACTUALIZADO!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("NO SE ACTUALIZÓ LAS FORMAS DE PAGO.");
        }
        return exitoFormaPago;
    }
    //////UPDATE, CLIENTE FIEL FORMA DE PAGO -> PUT

    //////UPDATE, BAJA CLIENTE FIEL MASIVO -> GET
    private boolean bajaDescClienteFielTodo() {
        JSONParser parser = new JSONParser();
        String inputLine;
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/bajaMasiva/baja");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(creandoJsonBajaDescMasivo(u, ts).toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    if (exitoBaja) {
                        if (persistirDescuentoFielCabBaja(Identity.getNomFun(), timestamp)) {
                            System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- ");
                        }
                    }
                    br.close();
                } else {
                    exitoBaja = recuperarDescuentoFielCabBajasLocal(Identity.getNomFun(), timestamp);
                }
            } else {
                exitoBaja = recuperarDescuentoFielCabBajasLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = recuperarDescuentoFielCabBajasLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN CLIENTE FIEL HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            } else {
                alert2.close();
            }
        } else {
            mensajeError("LOS DESCUENTOS EN CLIENTE FIEL NO SE CANCELARON.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA CLIENTE FIEL MASIVO -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, VALIDACIÓN INSERCIÓN
    private boolean validacionInsercion(int nf, long idNf) {
        return descFielDAO.validacionInsercion(nf, idNf);
    }
    //////READ, VALIDACIÓN INSERCIÓN

    //////UPDATE, BAJA DESUENTO FIEL CAB.
    private boolean persistirDescuentoFielCabBaja(String u, Timestamp ts) {
        return descFielDAO.bajasLocal(u, ts);
    }
    //////UPDATE, BAJA DESUENTO FIEL CAB.

    //////UPDATE, PENDIENTES - DESCUENTO FIEL, BAJA MASIVA
    private boolean recuperarDescuentoFielCabBajasLocal(String u, Timestamp ts) {
        boolean valor = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_cab_masivo");
        j.put("data", "{\"estadoDesc\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(ip, dml, msj, fecha_registro) VALUES ('" + Utilidades.host + "', 'U','" + x + "' , now())";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro en HANDLER PARANA BD ********");
                if (persistirDescuentoFielCabBaja(u, ts)) {
                    valor = true;
                    System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- ");
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - DESCUENTO FIEL, BAJA MASIVA

    //////UPDATE, PENDIENTES - DESCUENTO FIEL CAB.
    private boolean actualizarDescuentoFielCab(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_cab");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(ip, dml, msj, fecha_registro) VALUES ('" + Utilidades.host + "', 'U', '" + x + "', now())";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz actualizado un registro ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - DESCUENTO FIEL CAB.

    //////UPDATE, PENDIENTES - DESCUENTO FIEL CAB.
    private boolean actualizarDescuentoFielCabFormaPago(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_formas_de_pago");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(ip, dml, msj, fecha_registro) VALUES ('" + Utilidades.host + "', 'U', '" + x + "', now())";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz actualizado un registro ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - DESCUENTO FIEL CAB.

    //////INSERT, PENDIENTES - DESCUENTO FIEL CAB.
    private JSONObject registrarDescuentoFielCab(JSONObject json) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_cab");
        j.put("data", json.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                obj = json;
                exitoInsertarCab = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL CAB.

    //////INSERT, PENDIENTES - DESCUENTO FIEL DET.
    private boolean registrarDescuentoFielDetLocal(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_det");
        j.put("data", toString);
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL DET.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF1.
    private boolean registrarDescuentoFielNf1Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf1");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF1.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF2.
    private boolean registrarDescuentoFielNf2Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf2");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF2.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF3.
    private boolean registrarDescuentoFielNf3Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf3");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF3.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF4.
    private boolean registrarDescuentoFielNf4Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf4");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF4.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF5.
    private boolean registrarDescuentoFielNf5Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf5");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF5.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF6.
    private boolean registrarDescuentoFielNf6Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf6");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF6.

    //////INSERT, PENDIENTES - DESCUENTO FIEL NF7.
    private boolean registrarDescuentoFielNf7Local(String toString) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "descuento_fiel_nf7");
        j.put("data", toString);
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - DESCUENTO FIEL NF7.

    //////READ, DÍAS
    private List<JSONObject> generarDiasLocal() {
        List<JSONObject> JSONDias = new ArrayList<>();
        JSONParser parser = new JSONParser();
        for (Dias dia : diaDAO.listar()) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(dia.toDiaDTO()));
                JSONDias.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return JSONDias;
    }
    //////READ, DÍAS

    //////READ, SECCIÓN
    private JSONArray generarSeccionLocal() {
        JSONArray jSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Seccion sec : seccionDAO.listarNombreId()) {
            try {
                String dato = gson.toJson(sec.toSeccionDTO());
                JSONObject obj = (JSONObject) parser.parse(dato);
                jSONArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jSONArray;
    }
    //////READ, SECCIÓN

    //////READ, NF
    private void generarNivelFamiliaLocal() {
        TreeItem<String> treeItemRoot = new TreeItem<>("ÁRBOL DE SECCIONES");
        treeItemRoot.setExpanded(true);
        //y sí... mucho recurso...
        hashMapNf1 = new HashMap<>();
        hashMapNf2 = new HashMap<>();
        hashMapNf3 = new HashMap<>();
        hashMapNf4 = new HashMap<>();
        hashMapNf5 = new HashMap<>();
        hashMapNf6 = new HashMap<>();
        hashMapNf7 = new HashMap<>();
        hashMapDetalleNf1 = new HashMap<>();
        hashMapDetalleNf2 = new HashMap<>();
        hashMapDetalleNf3 = new HashMap<>();
        hashMapDetalleNf4 = new HashMap<>();
        hashMapDetalleNf5 = new HashMap<>();
        hashMapDetalleNf6 = new HashMap<>();
        hashMapDetalleNf7 = new HashMap<>();
        //la descripción e id, pueden coincidir en niveles distintos a la hora de mapear... 
        //si se instancia una vez y se setea a todos los items del treeview con el mismo objeto del tipo ImageView
        //no funcionará correctamente
        //se puede rastrear también con una concatenación id + descripción, como nombre del item, pero estéticamente no queda muy bien
        for (Nf1Tipo nf1Tipo : nf1TipoDAO.listar()) {
            ImageView imageViewNf1 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
            hashMapNf1.put(imageViewNf1, nf1Tipo);//rastreo para inserción
            hashMapDetalleNf1.put(nf1Tipo.getIdNf1Tipo(), nf1Tipo);
            TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), imageViewNf1);
            for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
                ImageView imageViewNf2 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
                hashMapNf2.put(imageViewNf2, nf2Sfamilia);
                hashMapDetalleNf2.put(nf2Sfamilia.getIdNf2Sfamilia(), nf2Sfamilia);
                TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), imageViewNf2);
                for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                    ImageView imageViewNf3 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                    hashMapNf3.put(imageViewNf3, nf3Sseccion);
                    hashMapDetalleNf3.put(nf3Sseccion.getIdNf3Sseccion(), nf3Sseccion);
                    TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), imageViewNf3);
                    for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                        ImageView imageViewNf4 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                        hashMapNf4.put(imageViewNf4, nf4Seccion1);
                        hashMapDetalleNf4.put(nf4Seccion1.getIdNf4Seccion1(), nf4Seccion1);
                        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), imageViewNf4);
                        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                            ImageView imageViewNf5 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                            hashMapNf5.put(imageViewNf5, nf5Seccion2);
                            hashMapDetalleNf5.put(nf5Seccion2.getIdNf5Seccion2(), nf5Seccion2);
                            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), imageViewNf5);
                            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                                ImageView imageViewNf6 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                                hashMapNf6.put(imageViewNf6, nf6Secnom6);
                                hashMapDetalleNf6.put(nf6Secnom6.getIdNf6Secnom6(), nf6Secnom6);
                                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), imageViewNf6);
                                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                    ImageView imageViewNf7 = new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                    hashMapNf7.put(imageViewNf7, nf7Secnom7);
                                    hashMapDetalleNf7.put(nf7Secnom7.getIdNf7Secnom7(), nf7Secnom7);
                                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), imageViewNf7);
                                    treeItemNf6.getChildren().add(treeItemNf7);
                                }
                                treeItemNf5.getChildren().add(treeItemNf6);
                            }
                            treeItemNf4.getChildren().add(treeItemNf5);
                        }
                        treeItemNf3.getChildren().add(treeItemNf4);
                    }
                    treeItemNf2.getChildren().add(treeItemNf3);
                }
                treeItemNf1.getChildren().add(treeItemNf2);
            }
            treeItemRoot.getChildren().add(treeItemNf1);
        }
        treeViewSecciones.setRoot(treeItemRoot);
    }
    //////READ, NF

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCab(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab desc : descFielDAO.listarFETCHSeccion(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoFielCabDTO()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB.

    //////READ, FETCH DESCUENTO FIEL CAB.
    private JSONArray generarFetchDescuentoFielCab(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab desc : descFielDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoFielCabDTO()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB.
    private Integer generarCountFiltroDescuentoClienteFielLocal(String nombre) {
        return descFielDAO.rowCountFiler(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB.

    //////READ, ROW COUNT DESCUENTO FIEL CAB.
    private Integer generarCountDescuentoClienteFielLocal() {
        return descFielDAO.rowCount().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf1.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf1(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf1(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf1.

    //////READ, FETCH DESCUENTO FIEL CAB Nf1.
    private JSONArray generarFetchDescuentoFielCabNf1(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf1(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf1.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf1.
    private Integer generarCountFiltroDescuentoClienteFielNf1Local(String nombre) {
        return descFielDAO.rowCountFilterNf1(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf1.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf1.
    private Integer generarCountDescuentoClienteFielNf1Local() {
        return descFielDAO.rowCountNf1().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf1.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf2.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf2(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf2(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf2.

    //////READ, FETCH DESCUENTO FIEL CAB Nf2.
    private JSONArray generarFetchDescuentoFielCabNf2(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf2(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf2.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf2.
    private Integer generarCountFiltroDescuentoClienteFielNf2Local(String nombre) {
        return descFielDAO.rowCountFilterNf2(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf2.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf2.
    private Integer generarCountDescuentoClienteFielNf2Local() {
        return descFielDAO.rowCountNf2().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf2.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf3.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf3(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf3(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf3.

    //////READ, FETCH DESCUENTO FIEL CAB Nf3.
    private JSONArray generarFetchDescuentoFielCabNf3(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf3(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf3.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf3.
    private Integer generarCountFiltroDescuentoClienteFielNf3Local(String nombre) {
        return descFielDAO.rowCountFilterNf3(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf3.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf3.
    private Integer generarCountDescuentoClienteFielNf3Local() {
        return descFielDAO.rowCountNf3().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf3.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf4.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf4(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf4(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf4.

    //////READ, FETCH DESCUENTO FIEL CAB Nf4.
    private JSONArray generarFetchDescuentoFielCabNf4(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf4(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf4.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf4.
    private Integer generarCountFiltroDescuentoClienteFielNf4Local(String nombre) {
        return descFielDAO.rowCountFilterNf4(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf4.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf4.
    private Integer generarCountDescuentoClienteFielNf4Local() {
        return descFielDAO.rowCountNf4().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf4.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf5.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf5(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf5(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf5.

    //////READ, FETCH DESCUENTO FIEL CAB Nf5.
    private JSONArray generarFetchDescuentoFielCabNf5(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf5(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf5.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf5.
    private Integer generarCountFiltroDescuentoClienteFielNf5Local(String nombre) {
        return descFielDAO.rowCountFilterNf5(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf5.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf5.
    private Integer generarCountDescuentoClienteFielNf5Local() {
        return descFielDAO.rowCountNf5().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf5.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf6.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf6(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf6(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf6.

    //////READ, FETCH DESCUENTO FIEL CAB Nf6.
    private JSONArray generarFetchDescuentoFielCabNf6(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf6(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf6.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf6.
    private Integer generarCountFiltroDescuentoClienteFielNf6Local(String nombre) {
        return descFielDAO.rowCountFilterNf6(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf6.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf6.
    private Integer generarCountDescuentoClienteFielNf6Local() {
        return descFielDAO.rowCountNf6().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf6.

    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf7.
    private JSONArray generarDescuentoFetchFiltroDescuentoFielCabNf7(String limRowS, String offSetS, String seccion) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchFilterNf7(Long.parseLong(limRowS), Long.parseLong(offSetS), seccion)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH FILTRO DESCUENTO FIEL CAB Nf7.

    //////READ, FETCH DESCUENTO FIEL CAB Nf7.
    private JSONArray generarFetchDescuentoFielCabNf7(String limRowS, String offSetS) {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (DescuentoFielCab descuentoFielCab : descFielDAO.listarFetchNf7(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(descuentoFielCab.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, FETCH DESCUENTO FIEL CAB Nf7.

    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf7.
    private Integer generarCountFiltroDescuentoClienteFielNf7Local(String nombre) {
        return descFielDAO.rowCountFilterNf7(nombre).intValue();
    }
    //////READ, ROW COUNT FILTRO DESCUENTO FIEL CAB Nf7.

    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf7.
    private Integer generarCountDescuentoClienteFielNf7Local() {
        return descFielDAO.rowCountNf7().intValue();
    }
    //////READ, ROW COUNT DESCUENTO FIEL CAB Nf7.
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREANDO DESCUENTO CAB. FIEL
    private JSONObject creandoJsonCabFiel() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject descFielCab = new JSONObject();
        //**********************************************************************
        String[] parts = String.valueOf(choiceBoxDesc.getSelectionModel().getSelectedItem()).split(" ");
        int porc = Integer.valueOf(parts[0]);
        descFielCab.put("porcentajeDesc", porc);
        descFielCab.put("estadoDesc", true);
        //acaité, días contados
        descFielCab.put("descriSeccion", "NINGUNA");
        JSONObject seccion = new JSONObject();
        seccion.put("idSeccion", 0l);
        descFielCab.put("seccion", seccion);
        //acaité, días contados
        //**********************************************************************
        descFielCab.put("usuAlta", Identity.getNomFun());
        descFielCab.put("fechaAlta", timestampJSON);
        descFielCab.put("usuMod", Identity.getNomFun());
        descFielCab.put("fechaMod", timestampJSON);
        //**********************************************************************
        return descFielCab;
    }
    //JSON CREANDO DESCUENTO CAB. FIEL

    //JSON CREANDO DESCUENTO DET. FIEL
    private JSONObject creandoJsonDetFiel(JSONObject descFielCab, JSONObject dia) {
        JSONObject descFielDet = new JSONObject();
        //**********************************************************************
        descFielDet.put("descuentoFielCab", descFielCab);
        descFielDet.put("dia", dia);
        //**********************************************************************
        return descFielDet;
    }
    //JSON CREANDO DESCUENTO DET. FIEL

    //JSON CREANDO DESCUENTO NF1 FIEL
    private JSONObject creandoJsonNf1Fiel(JSONObject descFielCab, JSONObject descFielNf1) {
        JSONObject descuentoFielNf1 = new JSONObject();
        //**********************************************************************
        descuentoFielNf1.put("descuentoFielCab", descFielCab);
        descuentoFielNf1.put("nf1Tipo", descFielNf1);
        //**********************************************************************
        return descuentoFielNf1;
    }
    //JSON CREANDO DESCUENTO NF1 FIEL

    //JSON CREANDO DESCUENTO NF2 FIEL
    private JSONObject creandoJsonNf2Fiel(JSONObject descFielCab, JSONObject descFielNf2) {
        JSONObject descuentoFielNf2 = new JSONObject();
        //**********************************************************************
        descuentoFielNf2.put("descuentoFielCab", descFielCab);
        descuentoFielNf2.put("nf2Sfamilia", descFielNf2);
        //**********************************************************************
        return descuentoFielNf2;
    }
    //JSON CREANDO DESCUENTO NF2 FIEL

    //JSON CREANDO DESCUENTO NF3 FIEL
    private JSONObject creandoJsonNf3Fiel(JSONObject descFielCab, JSONObject descFielNf3) {
        JSONObject descuentoFielNf3 = new JSONObject();
        //**********************************************************************
        descuentoFielNf3.put("descuentoFielCab", descFielCab);
        descuentoFielNf3.put("nf3Sseccion", descFielNf3);
        //**********************************************************************
        return descuentoFielNf3;
    }
    //JSON CREANDO DESCUENTO NF3 FIEL

    //JSON CREANDO DESCUENTO NF4 FIEL
    private JSONObject creandoJsonNf4Fiel(JSONObject descFielCab, JSONObject descFielNf4) {
        JSONObject descuentoFielNf4 = new JSONObject();
        //**********************************************************************
        descuentoFielNf4.put("descuentoFielCab", descFielCab);
        descuentoFielNf4.put("nf4Seccion1", descFielNf4);
        //**********************************************************************
        return descuentoFielNf4;
    }
    //JSON CREANDO DESCUENTO NF4 FIEL

    //JSON CREANDO DESCUENTO NF5 FIEL
    private JSONObject creandoJsonNf5Fiel(JSONObject descFielCab, JSONObject descFielNf5) {
        JSONObject descuentoFielNf5 = new JSONObject();
        //**********************************************************************
        descuentoFielNf5.put("descuentoFielCab", descFielCab);
        descuentoFielNf5.put("nf5Seccion2", descFielNf5);
        //**********************************************************************
        return descuentoFielNf5;
    }
    //JSON CREANDO DESCUENTO NF5 FIEL

    //JSON CREANDO DESCUENTO NF6 FIEL
    private JSONObject creandoJsonNf6Fiel(JSONObject descFielCab, JSONObject descFielNf6) {
        JSONObject descuentoFielNf6 = new JSONObject();
        //**********************************************************************
        descuentoFielNf6.put("descuentoFielCab", descFielCab);
        descuentoFielNf6.put("nf6Secnom6", descFielNf6);
        //**********************************************************************
        return descuentoFielNf6;
    }
    //JSON CREANDO DESCUENTO NF6 FIEL

    //JSON CREANDO DESCUENTO NF7 FIEL
    private JSONObject creandoJsonNf7Fiel(JSONObject descFielCab, JSONObject descFielNf7) {
        JSONObject descuentoFielNf7 = new JSONObject();
        //**********************************************************************
        descuentoFielNf7.put("descuentoFielCab", descFielCab);
        descuentoFielNf7.put("nf7Secnom7", descFielNf7);
        //**********************************************************************
        return descuentoFielNf7;
    }
    //JSON CREANDO DESCUENTO NF7 FIEL

    //JSON BAJA DESCUENTO CAB. FIEL
    private JSONObject bajaJsonCabFiel(JSONObject descFielCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        descFielCab.put("usuMod", Identity.getNomFun());
        descFielCab.put("fechaMod", timestampJSON);
        descFielCab.put("estadoDesc", false);
        //días
        descFielCab.put("descuentoFielDets", null);
        //nivel - familia
        descFielCab.put("descuentoFielNf1s", null);
        descFielCab.put("descuentoFielNf2s", null);
        descFielCab.put("descuentoFielNf3s", null);
        descFielCab.put("descuentoFielNf4s", null);
        descFielCab.put("descuentoFielNf5s", null);
        descFielCab.put("descuentoFielNf6s", null);
        descFielCab.put("descuentoFielNf7s", null);
        //**********************************************************************
        return descFielCab;
    }
    //JSON BAJA DESCUENTO CAB. FIEL

    //JSON BAJA DESCUENTO FIEL MASIVO
    private JSONObject creandoJsonBajaDescMasivo(String u, String ts) {
        JSONObject jsonBajaMasiva = new JSONObject();
        jsonBajaMasiva.put("usuMod", u);
        jsonBajaMasiva.put("fechaMod", ts);
        jsonBajaMasiva.put("descripcionDescuento", UtilLoaderBase.msjIda(Descuento.f1));
        return jsonBajaMasiva;
    }
    //JSON BAJA DESCUENTO FIEL MASIVO

    //JSON BAJA DESCUENTO CAB. FIEL FORMA PAGO
    private JSONObject cambioJsonCabFielFormaPago(JSONObject descFielCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        descFielCab.put("usuMod", Identity.getNomFun());
        descFielCab.put("fechaMod", timestampJSON);
        descFielCab.put("estadoDesc", false);
        //días
        descFielCab.put("descuentoFielDets", null);
        //nivel - familia
        descFielCab.put("descuentoFielNf1s", null);
        descFielCab.put("descuentoFielNf2s", null);
        descFielCab.put("descuentoFielNf3s", null);
        descFielCab.put("descuentoFielNf4s", null);
        descFielCab.put("descuentoFielNf5s", null);
        descFielCab.put("descuentoFielNf6s", null);
        descFielCab.put("descuentoFielNf7s", null);
        //**********************************************************************
        return descFielCab;
    }
    //JSON BAJA DESCUENTO CAB. FIEL FORMA PAGO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf1 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf1() {
        filaCantCabFielNf1 = 5;
        filaInicabFielNf1 = 0;
        pageActualCabFielNf1 = 0;
        buscarTodosDescFielNf1 = true;
        buscandoDescuentoTodoNf1();
    }

    private void buscandoDescuentoTodoNf1() {
        filaLimitCabFielNf1 = jsonRowCountNf1();
        buscarTodosDescFielNf1 = true;
        paginandoCabFielNf1();
    }

    private void buscandoDescuentoFiltroNf1() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf1.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf1.getText();
        }
        filaLimitCabFielNf1 = jsonRowCountFilterNf1(filterParam);
        buscarTodosDescFielNf1 = false;
        paginandoCabFielNf1();
    }

    private void paginandoCabFielNf1() {
        paginationDescFielNf1.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf1(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf1.setCurrentPageIndex(this.pageActualCabFielNf1);//index inicial paginación
        paginationDescFielNf1.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf1 = filaLimitCabFielNf1 / filaCantCabFielNf1;//cantidad index paginación
        if (filaLimitCabFielNf1 % filaCantCabFielNf1 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf1++;
        }
        paginationDescFielNf1.setPageCount(pageCountCabFielNf1);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf1(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf1 = pageIndex * filaCantCabFielNf1;
        if (buscarTodosDescFielNf1) {
            cabFielListNf1 = jsonArrayCabFielFetchNf1(filaCantCabFielNf1, filaInicabFielNf1);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf1.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf1.getText();
            }
            cabFielListNf1 = jsonArrayCabFielFetchFiltroNf1(filaCantCabFielNf1, filaInicabFielNf1, filterParam);
        }
        actualizandoTablaCabFielNf1();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf1;
    }

    private void actualizandoTablaCabFielNf1() {
        tableCabFielNf1 = new TableView<>();
        cabFielDataNf1 = FXCollections.observableArrayList(cabFielListNf1);
        //columna Sección ....................................................
        columnNf1 = new TableColumn("Sección Nvl. 1");
        columnNf1.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf1.setMinWidth(220);
        columnNf1.setEditable(false);
        columnNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf1 = (JSONArray) data.getValue().get("descuentoFielNf1s");
                JSONObject jsonObjectDescuentoFielNf1 = (JSONObject) jsonArrayDetalleNf1.get(0);
                JSONObject jsonObjectNf1Tipo = (JSONObject) jsonObjectDescuentoFielNf1.get("nf1Tipo");
                return new ReadOnlyStringWrapper(jsonObjectNf1Tipo.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf1 = new TableColumn("Descuento");
        columnDescuentoNf1.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf1.setMinWidth(30);
        columnDescuentoNf1.setEditable(false);
        columnDescuentoNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf1 = new TableColumn("Domingo");
        columnCabFielDomingoNf1.setMinWidth(60);
        columnCabFielDomingoNf1.setEditable(false);
        columnCabFielDomingoNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf1 = new TableColumn("Lunes");
        columnCabFielLunesNf1.setMinWidth(60);
        columnCabFielLunesNf1.setEditable(false);
        columnCabFielLunesNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf1 = new TableColumn("Martes");
        columnCabFielMartesNf1.setMinWidth(60);
        columnCabFielMartesNf1.setEditable(false);
        columnCabFielMartesNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf1 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf1.setMinWidth(60);
        columnCabFielMiercolesNf1.setEditable(false);
        columnCabFielMiercolesNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf1 = new TableColumn("Jueves");
        columnCabFielJuevesNf1.setMinWidth(60);
        columnCabFielJuevesNf1.setEditable(false);
        columnCabFielJuevesNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf1 = new TableColumn("Viernes");
        columnCabFielViernesNf1.setMinWidth(60);
        columnCabFielViernesNf1.setEditable(false);
        columnCabFielViernesNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf1 = new TableColumn("Sábado");
        columnCabFielSabadoNf1.setMinWidth(60);
        columnCabFielSabadoNf1.setEditable(false);
        columnCabFielSabadoNf1.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf1.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf1 = new TableColumn("Acciones");
        columnAccionesNf1.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf1(tableCabFielNf1);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf1.getColumns().addAll(columnNf1, columnDescuentoNf1, columnCabFielDomingoNf1,
                columnCabFielLunesNf1, columnCabFielMartesNf1, columnCabFielMiercolesNf1,
                columnCabFielJuevesNf1, columnCabFielViernesNf1, columnCabFielSabadoNf1, columnAccionesNf1);
        tableCabFielNf1.setItems(cabFielDataNf1);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf1 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf1(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf1.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 1);
                    JSONArray jsonArrayDescuentoFielNf1s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf1s");
                    JSONObject jsonDescuentoFielNf1 = (JSONObject) jsonArrayDescuentoFielNf1s.get(0);
                    JSONObject jsonNf1 = (JSONObject) jsonDescuentoFielNf1.get("nf1Tipo");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf1.get(Long.valueOf(jsonNf1.get("idNf1Tipo").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf1.getItems().get(index);
                    JSONArray descuentoFielNf1s = (JSONArray) descFielCab.get("descuentoFielNf1s");
                    JSONObject descuentoFielNf1 = (JSONObject) descuentoFielNf1s.get(0);
                    JSONObject jsonNf1Tipo = (JSONObject) descuentoFielNf1.get("nf1Tipo");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 1 "
                            + jsonNf1Tipo.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(1);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf1 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf2 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf2() {
        filaCantCabFielNf2 = 5;
        filaInicabFielNf2 = 0;
        pageActualCabFielNf2 = 0;
        buscarTodosDescFielNf2 = true;
        buscandoDescuentoTodoNf2();
    }

    private void buscandoDescuentoTodoNf2() {
        filaLimitCabFielNf2 = jsonRowCountNf2();
        buscarTodosDescFielNf2 = true;
        paginandoCabFielNf2();
    }

    private void buscandoDescuentoFiltroNf2() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf2.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf2.getText();
        }
        filaLimitCabFielNf2 = jsonRowCountFilterNf2(filterParam);
        buscarTodosDescFielNf2 = false;
        paginandoCabFielNf2();
    }

    private void paginandoCabFielNf2() {
        paginationDescFielNf2.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf2(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf2.setCurrentPageIndex(this.pageActualCabFielNf2);//index inicial paginación
        paginationDescFielNf2.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf2 = filaLimitCabFielNf2 / filaCantCabFielNf2;//cantidad index paginación
        if (filaLimitCabFielNf2 % filaCantCabFielNf2 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf2++;
        }
        paginationDescFielNf2.setPageCount(pageCountCabFielNf2);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf2(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf2 = pageIndex * filaCantCabFielNf2;
        if (buscarTodosDescFielNf2) {
            cabFielListNf2 = jsonArrayCabFielFetchNf2(filaCantCabFielNf2, filaInicabFielNf2);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf2.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf2.getText();
            }
            cabFielListNf2 = jsonArrayCabFielFetchFiltroNf2(filaCantCabFielNf2, filaInicabFielNf2, filterParam);
        }
        actualizandoTablaCabFielNf2();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf2;
    }

    private void actualizandoTablaCabFielNf2() {
        tableCabFielNf2 = new TableView<>();
        cabFielDataNf2 = FXCollections.observableArrayList(cabFielListNf2);
        //columna Sección ....................................................
        columnNf2 = new TableColumn("Sección Nvl. 2");
        columnNf2.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf2.setMinWidth(220);
        columnNf2.setEditable(false);
        columnNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf2 = (JSONArray) data.getValue().get("descuentoFielNf2s");
                JSONObject jsonObjectDescuentoFielNf2 = (JSONObject) jsonArrayDetalleNf2.get(0);
                JSONObject jsonObjectNf2Sfamilia = (JSONObject) jsonObjectDescuentoFielNf2.get("nf2Sfamilia");
                return new ReadOnlyStringWrapper(jsonObjectNf2Sfamilia.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf2 = new TableColumn("Descuento");
        columnDescuentoNf2.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf2.setMinWidth(30);
        columnDescuentoNf2.setEditable(false);
        columnDescuentoNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf2 = new TableColumn("Domingo");
        columnCabFielDomingoNf2.setMinWidth(60);
        columnCabFielDomingoNf2.setEditable(false);
        columnCabFielDomingoNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf2 = new TableColumn("Lunes");
        columnCabFielLunesNf2.setMinWidth(60);
        columnCabFielLunesNf2.setEditable(false);
        columnCabFielLunesNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf2 = new TableColumn("Martes");
        columnCabFielMartesNf2.setMinWidth(60);
        columnCabFielMartesNf2.setEditable(false);
        columnCabFielMartesNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf2 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf2.setMinWidth(60);
        columnCabFielMiercolesNf2.setEditable(false);
        columnCabFielMiercolesNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf2 = new TableColumn("Jueves");
        columnCabFielJuevesNf2.setMinWidth(60);
        columnCabFielJuevesNf2.setEditable(false);
        columnCabFielJuevesNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf2 = new TableColumn("Viernes");
        columnCabFielViernesNf2.setMinWidth(60);
        columnCabFielViernesNf2.setEditable(false);
        columnCabFielViernesNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf2 = new TableColumn("Sábado");
        columnCabFielSabadoNf2.setMinWidth(60);
        columnCabFielSabadoNf2.setEditable(false);
        columnCabFielSabadoNf2.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf2.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf2 = new TableColumn("Acciones");
        columnAccionesNf2.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf2(tableCabFielNf2);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf2.getColumns().addAll(columnNf2, columnDescuentoNf2, columnCabFielDomingoNf2,
                columnCabFielLunesNf2, columnCabFielMartesNf2, columnCabFielMiercolesNf2,
                columnCabFielJuevesNf2, columnCabFielViernesNf2, columnCabFielSabadoNf2, columnAccionesNf2);
        tableCabFielNf2.setItems(cabFielDataNf2);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf2 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf2(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf2.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 2);
                    JSONArray jsonArrayDescuentoFielNf2s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf2s");
                    JSONObject jsonDescuentoFielNf2 = (JSONObject) jsonArrayDescuentoFielNf2s.get(0);
                    JSONObject jsonNf2 = (JSONObject) jsonDescuentoFielNf2.get("nf2Sfamilia");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf2.get(Long.valueOf(jsonNf2.get("idNf2Sfamilia").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf2.getItems().get(index);
                    JSONArray descuentoFielNf2s = (JSONArray) descFielCab.get("descuentoFielNf2s");
                    JSONObject descuentoFielNf2 = (JSONObject) descuentoFielNf2s.get(0);
                    JSONObject jsonNf2Sfamilia = (JSONObject) descuentoFielNf2.get("nf2Sfamilia");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 2 "
                            + jsonNf2Sfamilia.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(2);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf2 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf3 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf3() {
        filaCantCabFielNf3 = 5;
        filaInicabFielNf3 = 0;
        pageActualCabFielNf3 = 0;
        buscarTodosDescFielNf3 = true;
        buscandoDescuentoTodoNf3();
    }

    private void buscandoDescuentoTodoNf3() {
        filaLimitCabFielNf3 = jsonRowCountNf3();
        buscarTodosDescFielNf3 = true;
        paginandoCabFielNf3();
    }

    private void buscandoDescuentoFiltroNf3() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf3.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf3.getText();
        }
        filaLimitCabFielNf3 = jsonRowCountFilterNf3(filterParam);
        buscarTodosDescFielNf3 = false;
        paginandoCabFielNf3();
    }

    private void paginandoCabFielNf3() {
        paginationDescFielNf3.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf3(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf3.setCurrentPageIndex(this.pageActualCabFielNf3);//index inicial paginación
        paginationDescFielNf3.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf3 = filaLimitCabFielNf3 / filaCantCabFielNf3;//cantidad index paginación
        if (filaLimitCabFielNf3 % filaCantCabFielNf3 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf3++;
        }
        paginationDescFielNf3.setPageCount(pageCountCabFielNf3);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf3(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf3 = pageIndex * filaCantCabFielNf3;
        if (buscarTodosDescFielNf3) {
            cabFielListNf3 = jsonArrayCabFielFetchNf3(filaCantCabFielNf3, filaInicabFielNf3);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf3.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf3.getText();
            }
            cabFielListNf3 = jsonArrayCabFielFetchFiltroNf3(filaCantCabFielNf3, filaInicabFielNf3, filterParam);
        }
        actualizandoTablaCabFielNf3();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf3;
    }

    private void actualizandoTablaCabFielNf3() {
        tableCabFielNf3 = new TableView<JSONObject>();
        cabFielDataNf3 = FXCollections.observableArrayList(cabFielListNf3);
        //columna Sección ....................................................
        columnNf3 = new TableColumn("Sección Nvl. 3");
        columnNf3.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf3.setMinWidth(220);
        columnNf3.setEditable(false);
        columnNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf3 = (JSONArray) data.getValue().get("descuentoFielNf3s");
                JSONObject jsonObjectDescuentoFielNf3 = (JSONObject) jsonArrayDetalleNf3.get(0);
                JSONObject jsonObjectNf3Sseccion = (JSONObject) jsonObjectDescuentoFielNf3.get("nf3Sseccion");
                return new ReadOnlyStringWrapper(jsonObjectNf3Sseccion.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf3 = new TableColumn("Descuento");
        columnDescuentoNf3.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf3.setMinWidth(30);
        columnDescuentoNf3.setEditable(false);
        columnDescuentoNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf3 = new TableColumn("Domingo");
        columnCabFielDomingoNf3.setMinWidth(60);
        columnCabFielDomingoNf3.setEditable(false);
        columnCabFielDomingoNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf3 = new TableColumn("Lunes");
        columnCabFielLunesNf3.setMinWidth(60);
        columnCabFielLunesNf3.setEditable(false);
        columnCabFielLunesNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf3 = new TableColumn("Martes");
        columnCabFielMartesNf3.setMinWidth(60);
        columnCabFielMartesNf3.setEditable(false);
        columnCabFielMartesNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf3 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf3.setMinWidth(60);
        columnCabFielMiercolesNf3.setEditable(false);
        columnCabFielMiercolesNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf3 = new TableColumn("Jueves");
        columnCabFielJuevesNf3.setMinWidth(60);
        columnCabFielJuevesNf3.setEditable(false);
        columnCabFielJuevesNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf3 = new TableColumn("Viernes");
        columnCabFielViernesNf3.setMinWidth(60);
        columnCabFielViernesNf3.setEditable(false);
        columnCabFielViernesNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf3 = new TableColumn("Sábado");
        columnCabFielSabadoNf3.setMinWidth(60);
        columnCabFielSabadoNf3.setEditable(false);
        columnCabFielSabadoNf3.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf3.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf3 = new TableColumn("Acciones");
        columnAccionesNf3.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf3(tableCabFielNf3);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf3.getColumns().addAll(columnNf3, columnDescuentoNf3, columnCabFielDomingoNf3,
                columnCabFielLunesNf3, columnCabFielMartesNf3, columnCabFielMiercolesNf3,
                columnCabFielJuevesNf3, columnCabFielViernesNf3, columnCabFielSabadoNf3, columnAccionesNf3);
        tableCabFielNf3.setItems(cabFielDataNf3);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf3 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf3(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf3.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 3);
                    JSONArray jsonArrayDescuentoFielNf3s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf3s");
                    JSONObject jsonDescuentoFielNf3 = (JSONObject) jsonArrayDescuentoFielNf3s.get(0);
                    JSONObject jsonNf3 = (JSONObject) jsonDescuentoFielNf3.get("nf3Sseccion");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf3.get(Long.valueOf(jsonNf3.get("idNf3Sseccion").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf3.getItems().get(index);
                    JSONArray descuentoFielNf3s = (JSONArray) descFielCab.get("descuentoFielNf3s");
                    JSONObject descuentoFielNf3 = (JSONObject) descuentoFielNf3s.get(0);
                    JSONObject jsonNf3Sseccion = (JSONObject) descuentoFielNf3.get("nf3Sseccion");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 3 "
                            + jsonNf3Sseccion.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(3);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf3 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf4 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf4() {
        filaCantCabFielNf4 = 5;
        filaInicabFielNf4 = 0;
        pageActualCabFielNf4 = 0;
        buscarTodosDescFielNf4 = true;
        buscandoDescuentoTodoNf4();
    }

    private void buscandoDescuentoTodoNf4() {
        filaLimitCabFielNf4 = jsonRowCountNf4();
        buscarTodosDescFielNf4 = true;
        paginandoCabFielNf4();
    }

    private void buscandoDescuentoFiltroNf4() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf4.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf4.getText();
        }
        filaLimitCabFielNf4 = jsonRowCountFilterNf4(filterParam);
        buscarTodosDescFielNf4 = false;
        paginandoCabFielNf4();
    }

    private void paginandoCabFielNf4() {
        paginationDescFielNf4.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf4(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf4.setCurrentPageIndex(this.pageActualCabFielNf4);//index inicial paginación
        paginationDescFielNf4.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf4 = filaLimitCabFielNf4 / filaCantCabFielNf4;//cantidad index paginación
        if (filaLimitCabFielNf4 % filaCantCabFielNf4 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf4++;
        }
        paginationDescFielNf4.setPageCount(pageCountCabFielNf4);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf4(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf4 = pageIndex * filaCantCabFielNf4;
        if (buscarTodosDescFielNf4) {
            cabFielListNf4 = jsonArrayCabFielFetchNf4(filaCantCabFielNf4, filaInicabFielNf4);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf4.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf4.getText();
            }
            cabFielListNf4 = jsonArrayCabFielFetchFiltroNf4(filaCantCabFielNf4, filaInicabFielNf4, filterParam);
        }
        actualizandoTablaCabFielNf4();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf4;
    }

    private void actualizandoTablaCabFielNf4() {
        tableCabFielNf4 = new TableView<JSONObject>();
        cabFielDataNf4 = FXCollections.observableArrayList(cabFielListNf4);
        //columna Sección ....................................................
        columnNf4 = new TableColumn("Sección Nvl. 4");
        columnNf4.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf4.setMinWidth(220);
        columnNf4.setEditable(false);
        columnNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf4 = (JSONArray) data.getValue().get("descuentoFielNf4s");
                JSONObject jsonObjectDescuentoFielNf4 = (JSONObject) jsonArrayDetalleNf4.get(0);
                JSONObject jsonObjectNf4Seccion1 = (JSONObject) jsonObjectDescuentoFielNf4.get("nf4Seccion1");
                return new ReadOnlyStringWrapper(jsonObjectNf4Seccion1.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf4 = new TableColumn("Descuento");
        columnDescuentoNf4.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf4.setMinWidth(30);
        columnDescuentoNf4.setEditable(false);
        columnDescuentoNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf4 = new TableColumn("Domingo");
        columnCabFielDomingoNf4.setMinWidth(60);
        columnCabFielDomingoNf4.setEditable(false);
        columnCabFielDomingoNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf4 = new TableColumn("Lunes");
        columnCabFielLunesNf4.setMinWidth(60);
        columnCabFielLunesNf4.setEditable(false);
        columnCabFielLunesNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf4 = new TableColumn("Martes");
        columnCabFielMartesNf4.setMinWidth(60);
        columnCabFielMartesNf4.setEditable(false);
        columnCabFielMartesNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf4 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf4.setMinWidth(60);
        columnCabFielMiercolesNf4.setEditable(false);
        columnCabFielMiercolesNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf4 = new TableColumn("Jueves");
        columnCabFielJuevesNf4.setMinWidth(60);
        columnCabFielJuevesNf4.setEditable(false);
        columnCabFielJuevesNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf4 = new TableColumn("Viernes");
        columnCabFielViernesNf4.setMinWidth(60);
        columnCabFielViernesNf4.setEditable(false);
        columnCabFielViernesNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf4 = new TableColumn("Sábado");
        columnCabFielSabadoNf4.setMinWidth(60);
        columnCabFielSabadoNf4.setEditable(false);
        columnCabFielSabadoNf4.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf4.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf4 = new TableColumn("Acciones");
        columnAccionesNf4.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf4(tableCabFielNf4);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf4.getColumns().addAll(columnNf4, columnDescuentoNf4, columnCabFielDomingoNf4,
                columnCabFielLunesNf4, columnCabFielMartesNf4, columnCabFielMiercolesNf4,
                columnCabFielJuevesNf4, columnCabFielViernesNf4, columnCabFielSabadoNf4, columnAccionesNf4);
        tableCabFielNf4.setItems(cabFielDataNf4);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf4 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf4(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf4.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 4);
                    JSONArray jsonArrayDescuentoFielNf4s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf4s");
                    JSONObject jsonDescuentoFielNf4 = (JSONObject) jsonArrayDescuentoFielNf4s.get(0);
                    JSONObject jsonNf4 = (JSONObject) jsonDescuentoFielNf4.get("nf4Seccion1");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf4.get(Long.valueOf(jsonNf4.get("idNf4Seccion1").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf4.getItems().get(index);
                    JSONArray descuentoFielNf4s = (JSONArray) descFielCab.get("descuentoFielNf4s");
                    JSONObject descuentoFielNf4 = (JSONObject) descuentoFielNf4s.get(0);
                    JSONObject jsonNf4Seccion1 = (JSONObject) descuentoFielNf4.get("nf4Seccion1");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 4 "
                            + jsonNf4Seccion1.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(4);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf4 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf5 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf5() {
        filaCantCabFielNf5 = 5;
        filaInicabFielNf5 = 0;
        pageActualCabFielNf5 = 0;
        buscarTodosDescFielNf5 = true;
        buscandoDescuentoTodoNf5();
    }

    private void buscandoDescuentoTodoNf5() {
        filaLimitCabFielNf5 = jsonRowCountNf5();
        buscarTodosDescFielNf5 = true;
        paginandoCabFielNf5();
    }

    private void buscandoDescuentoFiltroNf5() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf5.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf5.getText();
        }
        filaLimitCabFielNf5 = jsonRowCountFilterNf5(filterParam);
        buscarTodosDescFielNf5 = false;
        paginandoCabFielNf5();
    }

    private void paginandoCabFielNf5() {
        paginationDescFielNf5.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf5(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf5.setCurrentPageIndex(this.pageActualCabFielNf5);//index inicial paginación
        paginationDescFielNf5.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf5 = filaLimitCabFielNf5 / filaCantCabFielNf5;//cantidad index paginación
        if (filaLimitCabFielNf5 % filaCantCabFielNf5 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf5++;
        }
        paginationDescFielNf5.setPageCount(pageCountCabFielNf5);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf5(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf5 = pageIndex * filaCantCabFielNf5;
        if (buscarTodosDescFielNf5) {
            cabFielListNf5 = jsonArrayCabFielFetchNf5(filaCantCabFielNf5, filaInicabFielNf5);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf5.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf5.getText();
            }
            cabFielListNf5 = jsonArrayCabFielFetchFiltroNf5(filaCantCabFielNf5, filaInicabFielNf5, filterParam);
        }
        actualizandoTablaCabFielNf5();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf5;
    }

    private void actualizandoTablaCabFielNf5() {
        tableCabFielNf5 = new TableView<>();
        cabFielDataNf5 = FXCollections.observableArrayList(cabFielListNf5);
        //columna Sección ....................................................
        columnNf5 = new TableColumn("Sección Nvl. 5");
        columnNf5.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf5.setMinWidth(220);
        columnNf5.setEditable(false);
        columnNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf5 = (JSONArray) data.getValue().get("descuentoFielNf5s");
                JSONObject jsonObjectDescuentoFielNf5 = (JSONObject) jsonArrayDetalleNf5.get(0);
                JSONObject jsonObjectNf5Seccion2 = (JSONObject) jsonObjectDescuentoFielNf5.get("nf5Seccion2");
                return new ReadOnlyStringWrapper(jsonObjectNf5Seccion2.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf5 = new TableColumn("Descuento");
        columnDescuentoNf5.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf5.setMinWidth(30);
        columnDescuentoNf5.setEditable(false);
        columnDescuentoNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf5 = new TableColumn("Domingo");
        columnCabFielDomingoNf5.setMinWidth(60);
        columnCabFielDomingoNf5.setEditable(false);
        columnCabFielDomingoNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf5 = new TableColumn("Lunes");
        columnCabFielLunesNf5.setMinWidth(60);
        columnCabFielLunesNf5.setEditable(false);
        columnCabFielLunesNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf5 = new TableColumn("Martes");
        columnCabFielMartesNf5.setMinWidth(60);
        columnCabFielMartesNf5.setEditable(false);
        columnCabFielMartesNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf5 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf5.setMinWidth(60);
        columnCabFielMiercolesNf5.setEditable(false);
        columnCabFielMiercolesNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf5 = new TableColumn("Jueves");
        columnCabFielJuevesNf5.setMinWidth(60);
        columnCabFielJuevesNf5.setEditable(false);
        columnCabFielJuevesNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf5 = new TableColumn("Viernes");
        columnCabFielViernesNf5.setMinWidth(60);
        columnCabFielViernesNf5.setEditable(false);
        columnCabFielViernesNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf5 = new TableColumn("Sábado");
        columnCabFielSabadoNf5.setMinWidth(60);
        columnCabFielSabadoNf5.setEditable(false);
        columnCabFielSabadoNf5.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf5.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf5 = new TableColumn("Acciones");
        columnAccionesNf5.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf5(tableCabFielNf5);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf5.getColumns().addAll(columnNf5, columnDescuentoNf5, columnCabFielDomingoNf5,
                columnCabFielLunesNf5, columnCabFielMartesNf5, columnCabFielMiercolesNf5,
                columnCabFielJuevesNf5, columnCabFielViernesNf5, columnCabFielSabadoNf5, columnAccionesNf5);
        tableCabFielNf5.setItems(cabFielDataNf5);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf5 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf5(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf5.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 5);
                    JSONArray jsonArrayDescuentoFielNf5s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf5s");
                    JSONObject jsonDescuentoFielNf5 = (JSONObject) jsonArrayDescuentoFielNf5s.get(0);
                    JSONObject jsonNf5 = (JSONObject) jsonDescuentoFielNf5.get("nf5Seccion2");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf5.get(Long.valueOf(jsonNf5.get("idNf5Seccion2").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf5.getItems().get(index);
                    JSONArray descuentoFielNf5s = (JSONArray) descFielCab.get("descuentoFielNf5s");
                    JSONObject descuentoFielNf5 = (JSONObject) descuentoFielNf5s.get(0);
                    JSONObject jsonNf5Seccion2 = (JSONObject) descuentoFielNf5.get("nf5Seccion2");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 5 "
                            + jsonNf5Seccion2.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(5);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf5 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf6 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf6() {
        filaCantCabFielNf6 = 5;
        filaInicabFielNf6 = 0;
        pageActualCabFielNf6 = 0;
        buscarTodosDescFielNf6 = true;
        buscandoDescuentoTodoNf6();
    }

    private void buscandoDescuentoTodoNf6() {
        filaLimitCabFielNf6 = jsonRowCountNf6();
        buscarTodosDescFielNf6 = true;
        paginandoCabFielNf6();
    }

    private void buscandoDescuentoFiltroNf6() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf6.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf6.getText();
        }
        filaLimitCabFielNf6 = jsonRowCountFilterNf6(filterParam);
        buscarTodosDescFielNf6 = false;
        paginandoCabFielNf6();
    }

    private void paginandoCabFielNf6() {
        paginationDescFielNf6.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf6(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf6.setCurrentPageIndex(this.pageActualCabFielNf6);//index inicial paginación
        paginationDescFielNf6.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf6 = filaLimitCabFielNf6 / filaCantCabFielNf6;//cantidad index paginación
        if (filaLimitCabFielNf6 % filaCantCabFielNf6 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf6++;
        }
        paginationDescFielNf6.setPageCount(pageCountCabFielNf6);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf6(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf6 = pageIndex * filaCantCabFielNf6;
        if (buscarTodosDescFielNf6) {
            cabFielListNf6 = jsonArrayCabFielFetchNf6(filaCantCabFielNf6, filaInicabFielNf6);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf6.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf6.getText();
            }
            cabFielListNf6 = jsonArrayCabFielFetchFiltroNf6(filaCantCabFielNf6, filaInicabFielNf6, filterParam);
        }
        actualizandoTablaCabFielNf6();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf6;
    }

    private void actualizandoTablaCabFielNf6() {
        tableCabFielNf6 = new TableView<>();
        cabFielDataNf6 = FXCollections.observableArrayList(cabFielListNf6);
        //columna Sección ....................................................
        columnNf6 = new TableColumn("Sección Nvl. 6");
        columnNf6.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf6.setMinWidth(220);
        columnNf6.setEditable(false);
        columnNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf6 = (JSONArray) data.getValue().get("descuentoFielNf6s");
                JSONObject jsonObjectDescuentoFielNf6 = (JSONObject) jsonArrayDetalleNf6.get(0);
                JSONObject jsonObjectNf6Secnom6 = (JSONObject) jsonObjectDescuentoFielNf6.get("nf6Secnom6");
                return new ReadOnlyStringWrapper(jsonObjectNf6Secnom6.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf6 = new TableColumn("Descuento");
        columnDescuentoNf6.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf6.setMinWidth(30);
        columnDescuentoNf6.setEditable(false);
        columnDescuentoNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf6 = new TableColumn("Domingo");
        columnCabFielDomingoNf6.setMinWidth(60);
        columnCabFielDomingoNf6.setEditable(false);
        columnCabFielDomingoNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf6 = new TableColumn("Lunes");
        columnCabFielLunesNf6.setMinWidth(60);
        columnCabFielLunesNf6.setEditable(false);
        columnCabFielLunesNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf6 = new TableColumn("Martes");
        columnCabFielMartesNf6.setMinWidth(60);
        columnCabFielMartesNf6.setEditable(false);
        columnCabFielMartesNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf6 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf6.setMinWidth(60);
        columnCabFielMiercolesNf6.setEditable(false);
        columnCabFielMiercolesNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf6 = new TableColumn("Jueves");
        columnCabFielJuevesNf6.setMinWidth(60);
        columnCabFielJuevesNf6.setEditable(false);
        columnCabFielJuevesNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf6 = new TableColumn("Viernes");
        columnCabFielViernesNf6.setMinWidth(60);
        columnCabFielViernesNf6.setEditable(false);
        columnCabFielViernesNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf6 = new TableColumn("Sábado");
        columnCabFielSabadoNf6.setMinWidth(60);
        columnCabFielSabadoNf6.setEditable(false);
        columnCabFielSabadoNf6.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf6.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf6 = new TableColumn("Acciones");
        columnAccionesNf6.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf6(tableCabFielNf6);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf6.getColumns().addAll(columnNf6, columnDescuentoNf6, columnCabFielDomingoNf6,
                columnCabFielLunesNf6, columnCabFielMartesNf6, columnCabFielMiercolesNf6,
                columnCabFielJuevesNf6, columnCabFielViernesNf6, columnCabFielSabadoNf6, columnAccionesNf6);
        tableCabFielNf6.setItems(cabFielDataNf6);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf6 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf6(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf6.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 6);
                    JSONArray jsonArrayDescuentoFielNf6s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf6s");
                    JSONObject jsonDescuentoFielNf6 = (JSONObject) jsonArrayDescuentoFielNf6s.get(0);
                    JSONObject jsonNf6 = (JSONObject) jsonDescuentoFielNf6.get("nf6Secnom6");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf6.get(Long.valueOf(jsonNf6.get("idNf6Secnom6").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf6.getItems().get(index);
                    JSONArray descuentoFielNf6s = (JSONArray) descFielCab.get("descuentoFielNf6s");
                    JSONObject descuentoFielNf6 = (JSONObject) descuentoFielNf6s.get(0);
                    JSONObject jsonNf6Secnom6 = (JSONObject) descuentoFielNf6.get("nf6Secnom6");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 6 "
                            + jsonNf6Secnom6.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(6);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf6 -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************* Nf7 -> -> -> -> -> -> -> ->
    private void primeraPaginacionNf7() {
        filaCantCabFielNf7 = 5;
        filaInicabFielNf7 = 0;
        pageActualCabFielNf7 = 0;
        buscarTodosDescFielNf7 = true;
        buscandoDescuentoTodoNf7();
    }

    private void buscandoDescuentoTodoNf7() {
        filaLimitCabFielNf7 = jsonRowCountNf7();
        buscarTodosDescFielNf7 = true;
        paginandoCabFielNf7();
    }

    private void buscandoDescuentoFiltroNf7() {
        String filterParam = "null";
        if (!textFielFiltroSeccionNf7.getText().contentEquals("")) {
            filterParam = textFielFiltroSeccionNf7.getText();
        }
        filaLimitCabFielNf7 = jsonRowCountFilterNf7(filterParam);
        buscarTodosDescFielNf7 = false;
        paginandoCabFielNf7();
    }

    private void paginandoCabFielNf7() {
        paginationDescFielNf7.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFielNf7(pageIndex);
                //**************************************************************
            }
        });
        paginationDescFielNf7.setCurrentPageIndex(this.pageActualCabFielNf7);//index inicial paginación
        paginationDescFielNf7.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCabFielNf7 = filaLimitCabFielNf7 / filaCantCabFielNf7;//cantidad index paginación
        if (filaLimitCabFielNf7 % filaCantCabFielNf7 > 0) {//index EXTRA (resto de filas)***********
            pageCountCabFielNf7++;
        }
        paginationDescFielNf7.setPageCount(pageCountCabFielNf7);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFielNf7(int pageIndex) {
        long ini = System.currentTimeMillis();
        filaInicabFielNf7 = pageIndex * filaCantCabFielNf7;
        if (buscarTodosDescFielNf7) {
            cabFielListNf7 = jsonArrayCabFielFetchNf7(filaCantCabFielNf7, filaInicabFielNf7);
        } else {
            String filterParam = "null";
            if (!textFielFiltroSeccionNf7.getText().contentEquals("")) {
                filterParam = textFielFiltroSeccionNf7.getText();
            }
            cabFielListNf7 = jsonArrayCabFielFetchFiltroNf7(filaCantCabFielNf7, filaInicabFielNf7, filterParam);
        }
        actualizandoTablaCabFielNf7();
        System.out.println("Tiempo en ms.: " + (System.currentTimeMillis() - ini));
        return tableCabFielNf7;
    }

    private void actualizandoTablaCabFielNf7() {
        tableCabFielNf7 = new TableView<>();
        cabFielDataNf7 = FXCollections.observableArrayList(cabFielListNf7);
        //columna Sección ....................................................
        columnNf7 = new TableColumn("Sección Nvl. 7");
        columnNf7.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNf7.setMinWidth(220);
        columnNf7.setEditable(false);
        columnNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONArray jsonArrayDetalleNf7 = (JSONArray) data.getValue().get("descuentoFielNf7s");
                JSONObject jsonObjectDescuentoFielNf7 = (JSONObject) jsonArrayDetalleNf7.get(0);
                JSONObject jsonObjectNf7Secnom7 = (JSONObject) jsonObjectDescuentoFielNf7.get("nf7Secnom7");
                return new ReadOnlyStringWrapper(jsonObjectNf7Secnom7.get("descripcion").toString());
            }
        });
        //columna Sección ....................................................
        //columna Desc ....................................................
        columnDescuentoNf7 = new TableColumn("Descuento");
        columnDescuentoNf7.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuentoNf7.setMinWidth(30);
        columnDescuentoNf7.setEditable(false);
        columnDescuentoNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                return new ReadOnlyStringWrapper(parts[0] + " %");
            }
        });
        //columna Desc ....................................................
        //columna Domingo .......................................................
        columnCabFielDomingoNf7 = new TableColumn("Domingo");
        columnCabFielDomingoNf7.setMinWidth(60);
        columnCabFielDomingoNf7.setEditable(false);
        columnCabFielDomingoNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielDomingoNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("1")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Domingo .......................................................
        //columna Lunes .......................................................
        columnCabFielLunesNf7 = new TableColumn("Lunes");
        columnCabFielLunesNf7.setMinWidth(60);
        columnCabFielLunesNf7.setEditable(false);
        columnCabFielLunesNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielLunesNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("2")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Lunes .......................................................
        //columna Martes .......................................................
        columnCabFielMartesNf7 = new TableColumn("Martes");
        columnCabFielMartesNf7.setMinWidth(60);
        columnCabFielMartesNf7.setEditable(false);
        columnCabFielMartesNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielMartesNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("3")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Martes .......................................................
        //columna Miércoles .......................................................
        columnCabFielMiercolesNf7 = new TableColumn("Miércoles");
        columnCabFielMiercolesNf7.setMinWidth(60);
        columnCabFielMiercolesNf7.setEditable(false);
        columnCabFielMiercolesNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielMiercolesNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("4")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Miércoles .......................................................
        //columna Jueves .......................................................
        columnCabFielJuevesNf7 = new TableColumn("Jueves");
        columnCabFielJuevesNf7.setMinWidth(60);
        columnCabFielJuevesNf7.setEditable(false);
        columnCabFielJuevesNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielJuevesNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("5")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Jueves .......................................................
        //columna Viernes .......................................................
        columnCabFielViernesNf7 = new TableColumn("Viernes");
        columnCabFielViernesNf7.setMinWidth(60);
        columnCabFielViernesNf7.setEditable(false);
        columnCabFielViernesNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielViernesNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("6")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Viernes .......................................................
        //columna Sabado .......................................................
        columnCabFielSabadoNf7 = new TableColumn("Sábado");
        columnCabFielSabadoNf7.setMinWidth(60);
        columnCabFielSabadoNf7.setEditable(false);
        columnCabFielSabadoNf7.setStyle("-fx-alignment: CENTER;");
        columnCabFielSabadoNf7.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray descFielDetList = (JSONArray) data.getValue().get("descuentoFielDets");
                boolean diaBool = false;
                for (Object jsonObj : descFielDetList) {
                    JSONObject descFielDet = (JSONObject) jsonObj;
                    JSONObject dia = (JSONObject) descFielDet.get("dia");
                    if (dia.get("idDia").toString().contentEquals("7")) {
                        diaBool = true;
                        break;
                    }
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(diaBool);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Sabado .......................................................
        //columna Acciones .....................................................
        columnAccionesNf7 = new TableColumn("Acciones");
        columnAccionesNf7.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new DescuentoClienteFielFXMLController.AddObjectsCellNf7(tableCabFielNf7);
            }
        });
        //columna Acciones .....................................................
        //
        tableCabFielNf7.getColumns().addAll(columnNf7, columnDescuentoNf7, columnCabFielDomingoNf7,
                columnCabFielLunesNf7, columnCabFielMartesNf7, columnCabFielMiercolesNf7,
                columnCabFielJuevesNf7, columnCabFielViernesNf7, columnCabFielSabadoNf7, columnAccionesNf7);
        tableCabFielNf7.setItems(cabFielDataNf7);
        //
    }

    //AÑADIR BOTONES A CADA FILA************************************************
    private class AddObjectsCellNf7 extends TableCell<JSONObject, Boolean> {

        Button verButton = new Button("ver subsecciones");
        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCellNf7(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            verButton.setStyle("-fx-background-color: linear-gradient(#39aedd, #38c1dd);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 12px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    jsonCabDetalleNf = new JSONObject();
                    jsonCabDetalleNf = tableCabFielNf7.getItems().get(index);
                    jsonCabDetalleNf.put("nf", 7);
                    JSONArray jsonArrayDescuentoFielNf7s = (JSONArray) jsonCabDetalleNf.get("descuentoFielNf7s");
                    JSONObject jsonDescuentoFielNf7 = (JSONObject) jsonArrayDescuentoFielNf7s.get(0);
                    JSONObject jsonNf7 = (JSONObject) jsonDescuentoFielNf7.get("nf7Secnom7");
                    jsonCabDetalleNf.put("nfChilds", hashMapDetalleNf7.get(Long.valueOf(jsonNf7.get("idNf7Secnom7").toString())));
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_fiel")) {
                    JSONObject descFielCab = tableCabFielNf7.getItems().get(index);
                    JSONArray descuentoFielNf7s = (JSONArray) descFielCab.get("descuentoFielNf7s");
                    JSONObject descuentoFielNf7 = (JSONObject) descuentoFielNf7s.get(0);
                    JSONObject jsonNf7Secnom7 = (JSONObject) descuentoFielNf7.get("nf7Secnom7");
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR DESCUENTO FIEL, EN NIVEL 7 "
                            + jsonNf7Secnom7.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descFielCab = bajaJsonCabFiel(descFielCab);
                        bajaCabFiel(descFielCab);
                        exitoBaja = false;
                        limpiandoCampos(7);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************* Nf7 -> -> -> -> -> -> -> ->

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    //no pega hacer esto...
    class SwitchButton extends Label {

        private SimpleBooleanProperty switchedOn = new SimpleBooleanProperty(true);

        public void primeraCarga(boolean on) {
            if (on) {
                setText("ON");
                setStyle("-fx-background-color: green;-fx-text-fill:white;");
                setContentDisplay(ContentDisplay.RIGHT);
            } else {
                setText("OFF");
                setStyle("-fx-background-color: red;-fx-text-fill:black;");
                setContentDisplay(ContentDisplay.LEFT);
            }
        }

        public SwitchButton(String nombre) {
            Button switchBtn = new Button();
            switchBtn.setPrefWidth(40);
            switchBtn.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent t) {
                    switchedOn.set(!switchedOn.get());
                }
            });
            setGraphic(switchBtn);
            switchedOn.addListener(new ChangeListener<Boolean>() {
                @Override
                public void changed(ObservableValue<? extends Boolean> ov,
                        Boolean t, Boolean t1) {
                    if (!primeraEntrada) {
                        if (t) {
                            setText("ON");
                            setStyle("-fx-background-color: green;-fx-text-fill:white;");
                            setContentDisplay(ContentDisplay.RIGHT);
                            System.out.println("ON: " + nombre);
                            switch (nombre) {
                                case "TARJETAS":
                                    descuentoFielFormasDePago.setTarjeta(true);
                                    break;
                                case "EFECTIVO":
                                    descuentoFielFormasDePago.setEfectivo(true);
                                    break;
                                case "MON. EXTR.":
                                    descuentoFielFormasDePago.setMonExtr(true);
                                    break;
                                case "NOTA CRÉD.":
                                    descuentoFielFormasDePago.setNotaCred(true);
                                    break;
                                case "CHEQUE":
                                    descuentoFielFormasDePago.setCheque(true);
                                    break;
                                case "VALE":
                                    descuentoFielFormasDePago.setVale(true);
                                    break;
                                case "BAJADA":
                                    descuentoFielFormasDePago.setBajada(true);
                                    break;
                            }
                        } else {
                            setText("OFF");
                            setStyle("-fx-background-color: red;-fx-text-fill:black;");
                            setContentDisplay(ContentDisplay.LEFT);
                            System.out.println("OFF: " + nombre);
                            switch (nombre) {
                                case "TARJETAS":
                                    descuentoFielFormasDePago.setTarjeta(false);
                                    break;
                                case "EFECTIVO":
                                    descuentoFielFormasDePago.setEfectivo(false);
                                    break;
                                case "MON. EXTR.":
                                    descuentoFielFormasDePago.setMonExtr(false);
                                    break;
                                case "NOTA CRÉD.":
                                    descuentoFielFormasDePago.setNotaCred(false);
                                    break;
                                case "CHEQUE":
                                    descuentoFielFormasDePago.setCheque(false);
                                    break;
                                case "VALE":
                                    descuentoFielFormasDePago.setVale(false);
                                    break;
                                case "BAJADA":
                                    descuentoFielFormasDePago.setBajada(false);
                                    break;
                            }
                        }
                    }
                }
            });
            switchedOn.set(false);
        }

        public SimpleBooleanProperty switchOnProperty() {
            return switchedOn;
        }
    }

    public static JSONObject getJsonCabDetalleNf() {
        return jsonCabDetalleNf;
    }
}
