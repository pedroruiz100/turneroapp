package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.ArtPromoTemporada;
import com.peluqueria.core.domain.ArtPromoTemporadaObsequio;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.PromoTemporadaArt;
import com.peluqueria.dao.ArtPromoTemporadaDAO;
import com.peluqueria.dao.ArtPromoTemporadaObsequioDAO;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.PromoTemporadaArtDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.RangoPromoTempArtDAO;
import com.peluqueria.dto.ArtPromoTemporadaDTO;
import com.peluqueria.dto.ArtPromoTemporadaObsequioDTO;
import com.peluqueria.dto.PromoTemporadaArtDTO;
import com.javafx.util.NumberValidator;
import com.javafx.util.RemindTask;
import com.javafx.util.queries.PromoTemporadaArtLocalQueries;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import java.io.File;
import java.util.LinkedHashMap;
import java.util.Timer;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import org.apache.commons.validator.GenericValidator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
public class PromocionTemporadaArtFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private PromoTemporadaArtDAO promoTemporadaArtDAO;
    @Autowired
    private ArticuloDAO articuloDAO;
    @Autowired
    private ArtPromoTemporadaDAO artPromoTemporadaDAO;
    @Autowired
    private RangoPromoTempArtDAO rangoPromoTempArtDAO;
    @Autowired
    private ArtPromoTemporadaObsequioDAO artPromoTemporadaObsequioDAO;

    NumberValidator num;
    private String exitoExcel;

    Task copyWorker;
    Task copyWorkerExcel;
    private String excepcionExcelBD;
    private final String patternDateReadFecha = "dd/MMM/yyyy";
    private final String patternDateFechaFiltro = "yyyy-MM-dd";
    private static List<JSONObject> promTempDetJSONObjList = new ArrayList<>();
    private static List<JSONObject> promTempDetObsequioJSONObjList = new ArrayList<>();
    private static JSONObject promTempCab = new JSONObject();
    public static JSONArray arrayDetalles = new JSONArray();
    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    private boolean exitoInsertarDetObsequio;
    private boolean exitoBaja;
    private boolean buscarTodosPromTempArt;
    private ObservableList<String> articulosAsignados;
    private HashMap<Long, JSONObject> hashMapCodArt;
    private JSONArray promTempArtJSONArray;
    private List<JSONObject> promTempArtList;
    //PAGINATION
    private int filaIniPromTempArt, filaCantPromTempArt, filaLimitPromTempArt;
    private int pageCountPromTempArt, pageActualPromTempArt;
    private ObservableList<JSONObject> promTempArtData;
    private TableView<JSONObject> tablePromTempArt;
    private TableColumn<JSONObject, String> columnPromTempArt;
    private TableColumn<JSONObject, String> columnPromTempArtFechaIni;
    private TableColumn<JSONObject, String> columnPromTempArtFechaFin;
    private TableColumn<JSONObject, Boolean> columnAcciones;
    //PAGINATION
    //DESCUENTOS
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();
    Timer timer;

    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private AnchorPane anchorPaneListView;
    @FXML
    private Button buttonInsertar;
    @FXML
    private Label labelDescuento;
    @FXML
    private ChoiceBox<String> choiceBoxDescuento;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private VBox vBoxButtons;
    @FXML
    private Button buttonAgregar;
    @FXML
    private Button buttonQuitar;
    @FXML
    private Button buttonQuitarTodos;
    @FXML
    private GridPane gridPaneFecha;
    @FXML
    private Label labelFiltro;
    @FXML
    private HBox vBoxFiltro;
    @FXML
    private Button buttonTodos;
    @FXML
    private Label labelFechaInicioFiltro;
    @FXML
    private DatePicker datePickerFechaInicioFiltro;
    @FXML
    private Label labelFechaFinFiltro;
    @FXML
    private DatePicker datePickerFechaFinFiltro;
    @FXML
    private HBox hBoxComboSeccion;
    @FXML
    private HBox hBoxDescuento;
    @FXML
    private AnchorPane anchorPaneDisable;
    @FXML
    private Button buttonCancelarTodas;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private Label labelProgress;
    @FXML
    private AnchorPane anchorPanePromoTempArt;
    @FXML
    private SplitPane splitPaneConfigPromTempArt;
    @FXML
    private Label labelConfigPromTemporadaArt;
    @FXML
    private Label labelNomPromTemArt;
    @FXML
    private TextField textFieldNomPromTemArt;
    @FXML
    private ListView<String> listViewArticulos;
    @FXML
    private Pagination paginationPromTempArt;
    @FXML
    private Label labelPromTempArt;
    @FXML
    private TextField textFielFiltroPromTempArt;
    @FXML
    private Button buttonBuscarPromTempArt;
    @FXML
    private Label labelArticulo;
    @FXML
    private TextField textFieldCodArt;
    @FXML
    private CheckBox checkBoxSincro;
    @FXML
    private Button buttonExcel;
    @FXML
    private ProgressBar progressIndicatorExcel;
    @FXML
    private Label labelProgressBarExcel;
    @FXML
    private Label labelPorc;
    @FXML
    private CheckBox checkBoxArtNulo;
    @FXML
    private CheckBox checkBoxArtEnProm;
    @FXML
    private HBox vBoxFiltro1;
    @FXML
    private JFXTextField jfxTextFieldCodBuscar;
    @FXML
    private JFXButton jfxButtonBuscarCod;
    @FXML
    private VBox vBoxObsequiar;
    @FXML
    private HBox hBoxObsequiarTitulo;
    @FXML
    private Label labelObsequiar;
    @FXML
    private HBox hBoxObsequiar;
    @FXML
    private Label labelCantMin;
    @FXML
    private ChoiceBox<String> choiceBoxCantMin;
    @FXML
    private Label labelCantObsequiar;
    @FXML
    private ChoiceBox<String> choiceBoxObsequiar;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoPromTempArt();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonAgregarAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void buttonQuitarAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void buttonQuitarTodosAction(ActionEvent event) {
        quitandoTodos();
    }

    private void buttonBuscarPromTempAction(ActionEvent event) {
        buscandoPromTempArtFiltro();
    }

    @FXML
    private void buttonTodosAction(ActionEvent event) {
        buscandoPromTempArtTodo();
    }

    private void comboBoxSeccionMouseClicked(MouseEvent event) {
        mouseEventComboBox(event);
    }

    private void comboBoxSeccionKeyReleased(KeyEvent event) {
        keyPressComboBox(event);
    }

    @FXML
    private void anchorPanePromoTempKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonCancelarTodasAction(ActionEvent event) {
        cancelandoDescuentoPromTempArtTodo();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void buttonBuscarPromTempActionArt(ActionEvent event) {
    }

    @FXML
    private void textFieldCodArtKeyReleased(KeyEvent event) {
        keyPressCodArt(event);
    }

    @FXML
    private void buttonExcelAction(ActionEvent event) {
        asignandoDesdeExcel();
    }

    @FXML
    private void jfxButtonBuscarCodAction(ActionEvent event) {
        buscandoPorCodArt();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        ocultandoProgress();
        progressIndicatorExcel.setVisible(false);
        labelProgressBarExcel.setVisible(false);
        labelPorc.setVisible(false);
        cargandoChoiceDescuento();
        cargandoChoiceCantMinCantObsequio();
        textFieldCodArt.setText("");
        excepcionExcelBD = "";
        choiceBoxDescuento.getSelectionModel().select(0);
        alert = false;
        exitoInsertarCab = false;
        exitoBaja = false;
        buscarTodosPromTempArt = true;
        //list view artículos con descuento...
        ObservableList<String> list = FXCollections.observableArrayList();
        articulosAsignados = FXCollections.observableArrayList(list);
        listViewArticulos.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //list view artículos con descuento...
        preparandoDatePicker();
        primeraPaginacion();
        validandoCodArt();
        validacionObsequio();
        num = new NumberValidator();
        hashMapCodArt = new HashMap<>();
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/PromocionTemporadaArtFXML.fxml", 982, 812, false);
    }

    private void viendoDetalleArticulo() {
        this.sc.loadScreen("/vista/descuento/PromocionTemporadaDetalleArtFXML.fxml", 802, 743, "/vista/descuento/PromocionTemporadaArtFXML.fxml", 982, 812, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        if (excepcionExcelBD.isEmpty()) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else {
            TextArea textArea = new TextArea(msj);
            textArea.setEditable(false);
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "", ButtonType.OK);
            alert2.getDialogPane().setContent(textArea);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        }
    }

    private void mensajeAdv(String msj) {
        if (excepcionExcelBD.isEmpty()) {
            Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else {
            TextArea textArea = new TextArea(msj);
            textArea.setEditable(false);
            Alert alert2 = new Alert(Alert.AlertType.WARNING, "", ButtonType.OK);
            alert2.getDialogPane().setContent(textArea);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void mouseEventComboBox(MouseEvent event) {
        if (verificacionFechaArticulo()) {
        }
    }

    private void keyPressComboBox(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (verificacionFechaArticulo()) {
            }
        }
    }

    private void keyPressCodArt(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                asignando();
            }
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoPromTempArt();
        }
        if (keyCode == event.getCode().F2) {
            buscandoPromTempArtFiltro();
        }
        if (keyCode == event.getCode().F3) {
            buscandoPromTempArtTodo();
        }
        if (keyCode == event.getCode().F4) {
            if (!buttonExcel.isDisable()) {
                asignandoDesdeExcel();
            }
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().F12) {
            cancelandoDescuentoPromTempArtTodo();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    private void validandoCodArt() {
        textFieldCodArt.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isLong(aux)) {
                    try {
                        if (GenericValidator.isInRange(Long.valueOf(newValue), 0l, 9223372036854775807l)) {
                            Platform.runLater(() -> {
                                textFieldCodArt.setText(newValue.toString());
                                textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                            });
                        } else {
                            Platform.runLater(() -> {
                                textFieldCodArt.setText(oldValue.toString());
                                textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                            });
                        }
                    } catch (NumberFormatException e) {
                        if (GenericValidator.isLong(oldValue)) {
                            textFieldCodArt.setText(oldValue);
                            textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                        }
                    }
                } else {
                    Platform.runLater(() -> {
                        textFieldCodArt.setText(oldValue.toString());
                        textFieldCodArt.positionCaret(textFieldCodArt.getLength());
                    });
                }
            }
        }
        );
    }

    private void validacionObsequio() {
        choiceBoxDescuento.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (!newValue.toString().equalsIgnoreCase("-")) {
                    vBoxObsequiar.setDisable(true);
                    choiceBoxCantMin.getSelectionModel().select(0);
                    choiceBoxObsequiar.getSelectionModel().select(0);
                    checkBoxArtEnProm.setDisable(false);
                    checkBoxArtNulo.setDisable(false);
                    buttonExcel.setDisable(false);
                } else if (choiceBoxCantMin.getSelectionModel().isSelected(0) && choiceBoxCantMin.getSelectionModel().isSelected(0)) {
                    vBoxObsequiar.setDisable(false);
                }
            }
        });
        choiceBoxCantMin.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (!newValue.toString().equalsIgnoreCase("  -")) {
                    if (!hBoxDescuento.isDisable()) {
                        hBoxDescuento.setDisable(true);
                        checkBoxArtEnProm.setDisable(true);
                        checkBoxArtNulo.setDisable(true);
                        checkBoxArtEnProm.setSelected(false);
                        checkBoxArtNulo.setSelected(false);
                        buttonExcel.setDisable(true);
                    }
                } else if (choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                    if (hBoxDescuento.isDisable()) {
                        hBoxDescuento.setDisable(false);
                        checkBoxArtEnProm.setDisable(false);
                        checkBoxArtNulo.setDisable(false);
                        buttonExcel.setDisable(false);
                    }
                }
            }
        });
        choiceBoxObsequiar.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observable, Object oldValue, Object newValue) {
                if (!newValue.toString().equalsIgnoreCase("  -")) {
                    if (!hBoxDescuento.isDisable()) {
                        hBoxDescuento.setDisable(true);
                        checkBoxArtEnProm.setDisable(true);
                        checkBoxArtNulo.setDisable(true);
                        checkBoxArtEnProm.setSelected(false);
                        checkBoxArtNulo.setSelected(false);
                        buttonExcel.setDisable(true);
                    }
                } else if (choiceBoxCantMin.getSelectionModel().isSelected(0)) {
                    if (hBoxDescuento.isDisable()) {
                        hBoxDescuento.setDisable(false);
                        checkBoxArtEnProm.setDisable(false);
                        checkBoxArtNulo.setDisable(false);
                        buttonExcel.setDisable(false);
                    }
                }
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoPorCodArt() {
        if (jfxTextFieldCodBuscar.getText().isEmpty()) {
            mensajeAdv("EL CAMPO CÓD. ARTÍCULO NO DEBE ESTAR VACÍO.");
        } else {
            promTempCab = new JSONObject();
            if (ConexionPostgres.conectarLocal()) {
                promTempCab = PromoTemporadaArtLocalQueries.promocionCodArticulosBusqueda(jfxTextFieldCodBuscar.getText());
            }
            if (ConexionPostgres.getConLocal() != null) {
                ConexionPostgres.cerrarLocal();
            }
            if (promTempCab.isEmpty()) {
                mensajeAdv("EL CÓD. ARTÍCULO NO ESTÁ EN PROMOCIÓN TEMPORADA.");
            } else {
                JSONArray promTempDetArray = (JSONArray) promTempCab.get("artPromoTemporadaDTO");
                promTempDetJSONObjList = new ArrayList<>();
                for (Object promTempDetObj : promTempDetArray) {
                    JSONObject promTempDetJSONObj = (JSONObject) promTempDetObj;
                    promTempDetJSONObjList.add(promTempDetJSONObj);
                }
                jfxTextFieldCodBuscar.setText("");
                viendoDetalleArticulo();
            }
        }
    }

    private void cargandoChoiceDescuento() {
        //nada de loops y mod...
        choiceBoxDescuento.getItems().add("-");
        choiceBoxDescuento.getItems().add("5 %");
        choiceBoxDescuento.getItems().add("10 %");
        choiceBoxDescuento.getItems().add("15 %");
        choiceBoxDescuento.getItems().add("20 %");
        choiceBoxDescuento.getItems().add("25 %");
        choiceBoxDescuento.getItems().add("27 %");
        choiceBoxDescuento.getItems().add("30 %");
        choiceBoxDescuento.getItems().add("35 %");
        choiceBoxDescuento.getItems().add("40 %");
        choiceBoxDescuento.getItems().add("45 %");
        choiceBoxDescuento.getItems().add("50 %");
        choiceBoxDescuento.getItems().add("55 %");
        choiceBoxDescuento.getItems().add("60 %");
        choiceBoxDescuento.getItems().add("65 %");
        choiceBoxDescuento.getItems().add("70 %");
        choiceBoxDescuento.getSelectionModel().select(0);
    }

    private void cargandoChoiceCantMinCantObsequio() {
        choiceBoxCantMin.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        choiceBoxObsequiar.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        //nada de loops y mod...
        choiceBoxCantMin.getItems().add("  -");
        choiceBoxCantMin.getItems().add("1");
        choiceBoxCantMin.getItems().add("2");
        choiceBoxCantMin.getItems().add("3");
        choiceBoxCantMin.getItems().add("4");
        choiceBoxCantMin.getItems().add("5");
        choiceBoxCantMin.getItems().add("6");
        choiceBoxCantMin.getItems().add("7");
        choiceBoxCantMin.getItems().add("8");
        choiceBoxCantMin.getItems().add("9");
        choiceBoxCantMin.getItems().add("10");
        choiceBoxObsequiar.getItems().add("  -");
        choiceBoxObsequiar.getItems().add("1");
        choiceBoxObsequiar.getItems().add("2");
        choiceBoxObsequiar.getItems().add("3");
        choiceBoxObsequiar.getItems().add("4");
        choiceBoxObsequiar.getItems().add("5");
        choiceBoxObsequiar.getItems().add("6");
        choiceBoxObsequiar.getItems().add("7");
        choiceBoxObsequiar.getItems().add("8");
        choiceBoxObsequiar.getItems().add("9");
        choiceBoxObsequiar.getItems().add("10");
        choiceBoxCantMin.getSelectionModel().select(0);
        choiceBoxObsequiar.getSelectionModel().select(0);
    }

    @SuppressWarnings("null")
    private Map<Long, Long> fileChooserFX() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("SELECCIÓN EXCEL");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel", "*.xls", /*"*.xml", "*.xlt",*/ "*.xlsx"));
        File file = fileChooser.showOpenDialog(this.sc.getStage());
        exitoExcel = "";
        HashMap<Long, String> hmCodRep = new HashMap<>();
        Map<Long, Long> hashMapCodPorc = new LinkedHashMap<>();
        if (file != null) {
            String[] parts = file.getName().split("\\.");
            XSSFWorkbook xssfWb = null;
            HSSFWorkbook hssfWb = null;
            try {
                if (parts[parts.length - 1].contentEquals("xls")) {
                    POIFSFileSystem fs = new POIFSFileSystem(file);
                    hssfWb = new HSSFWorkbook(fs);
                    HSSFSheet sheet = hssfWb.getSheetAt(0);
                    try {

                    } catch (IllegalStateException e) {
                    }
                    if (sheet.getPhysicalNumberOfRows() > 0) {
                        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
                            try {
                                if (hmCodRep.containsKey(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()))) {
                                    exitoExcel = "codRep - " + sheet.getRow(i).getCell(0).getAddress().toString() + " - "
                                            + hmCodRep.get(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()));
                                    break;
                                } else {
                                    double cod = sheet.getRow(i).getCell(0).getNumericCellValue();
                                    double porc = sheet.getRow(i).getCell(1).getNumericCellValue();
                                    if (porc < 1) {
                                        porc = porc * 100;
                                    }
                                    long codigo = Math.round(cod);
                                    long porcentaje = Math.round(porc);
                                    if (codigo == 0) {
                                        exitoExcel = "cod - " + sheet.getRow(i).getCell(0).getAddress();
                                        break;
                                    } else if (porcentaje == 0) {
                                        exitoExcel = "por - " + sheet.getRow(i).getCell(1).getAddress();
                                        break;
                                    } else if (porcentaje > 70) {
                                        exitoExcel = "overflow - " + sheet.getRow(i).getCell(1).getAddress();
                                        break;
                                    } else {
                                        hashMapCodPorc.put(codigo, porcentaje);
                                        hmCodRep.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                    }
                                }
                            } catch (IllegalStateException e) {
                                exitoExcel = "formato";
                                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                                break;
                            }
                        }
                        hssfWb.close();
                        if (!exitoExcel.isEmpty()) {
                            hashMapCodPorc = new LinkedHashMap<>();
                        }
                    } else {
                        hssfWb.close();
                        exitoExcel = "empty";
                        hashMapCodPorc = new LinkedHashMap<>();
                    }
                } else if (parts[parts.length - 1].contentEquals("xlsx")) {
                    OPCPackage op = OPCPackage.open(file);
                    xssfWb = new XSSFWorkbook(op);
                    XSSFSheet sheet = xssfWb.getSheetAt(0);
                    if (sheet.getPhysicalNumberOfRows() > 0) {
                        for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
                            try {
                                if (hmCodRep.containsKey(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()))) {
                                    exitoExcel = "codRep - " + sheet.getRow(i).getCell(0).getAddress().toString() + " - "
                                            + hmCodRep.get(Math.round(sheet.getRow(i).getCell(0).getNumericCellValue()));
                                    break;
                                } else {
                                    double cod = sheet.getRow(i).getCell(0).getNumericCellValue();
                                    double porc = sheet.getRow(i).getCell(1).getNumericCellValue();
                                    if (porc < 1) {
                                        porc = porc * 100;
                                    }
                                    long codigo = Math.round(cod);
                                    long porcentaje = Math.round(porc);
                                    if (codigo == 0) {
                                        exitoExcel = "cod - " + sheet.getRow(i).getCell(0).getAddress();
                                        break;
                                    } else if (porcentaje == 0) {
                                        exitoExcel = "por - " + sheet.getRow(i).getCell(1).getAddress();
                                        break;
                                    } else if (porcentaje > 70) {
                                        exitoExcel = "overflow - " + sheet.getRow(i).getCell(1).getAddress();
                                        break;
                                    } else {
                                        hashMapCodPorc.put(codigo, porcentaje);
                                        hmCodRep.put(codigo, sheet.getRow(i).getCell(0).getAddress().toString());
                                    }
                                }
                            } catch (IllegalStateException e) {
                                exitoExcel = "formato";
                                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                                break;
                            }
                        }
                        xssfWb.close();
                        if (!exitoExcel.isEmpty()) {
                            hashMapCodPorc = new LinkedHashMap<>();
                        }
                    } else {
                        xssfWb.close();
                        exitoExcel = "empty";
                        hashMapCodPorc = new LinkedHashMap<>();
                    }
                }
            } catch (IOException | InvalidFormatException e) {
                exitoExcel = "misc";
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                return new LinkedHashMap<>();
            } finally {
                if (xssfWb != null) {
                    try {
                        xssfWb.close();
                    } catch (IOException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                if (hssfWb != null) {
                    try {
                        hssfWb.close();
                    } catch (IOException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        } else {
            exitoExcel = "null";
        }
        return hashMapCodPorc;
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaFin.setEditable(false);
        datePickerFechaInicioFiltro.setEditable(false);
        datePickerFechaFinFiltro.setEditable(false);
        datePickerFechaInicio.setOnAction(event -> {
        });
        datePickerFechaFin.setOnAction(event -> {
        });
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        long p = ChronoUnit.DAYS.between(
                                datePickerFechaInicio.getValue(), item
                        );
                        setTooltip(new Tooltip(
                                "Descuento Promoción Temporada por " + p + " días.")
                        );
                    }
                };
            }
        };
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private boolean verificacionFechaArticulo() {
        if (datePickerFechaInicio.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA INICIO, PARA INGRESAR ARTÍCULO.");
            return false;
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA FIN, PARA OBTENER ARTÍCULO.");
            return false;
        } else if (datePickerFechaFin.getValue().isBefore(datePickerFechaInicio.getValue())) {
            mensajeError("LA FECHA FIN NO DEBE SER MENOR A FECHA INICIO.");
            return false;
        } else {
            return true;
        }
    }

    private void bloqueandoFecha() {
        if (listViewArticulos.getItems().isEmpty()) {
            datePickerFechaInicio.setDisable(false);
            datePickerFechaFin.setDisable(false);
        } else {
            datePickerFechaInicio.setDisable(true);
            datePickerFechaFin.setDisable(true);
        }
    }

    private String fechaParam(LocalDate fechaDesdeLD) {
        String filterFecha = "null";
        if (fechaDesdeLD != null) {
            Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
            filterFecha = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
        }
        return filterFecha;
    }

    private void insertandoPromTempArt() {
        if (textFieldNomPromTemArt.getText().contentEquals("")) {
            mensajeError("DEBE ASIGNAR UN NOMBRE A PROMOCIÓN TEMPORADA.");
        } else if (listViewArticulos.getItems().isEmpty()) {
            mensajeError("DEBE AGREGAR UN ARTÍCULO COMO MÍNIMO.");
        } else if (datePickerFechaInicio.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA INICIO.");
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA FIN.");
        } else if (datePickerFechaFin.getValue().isBefore(datePickerFechaInicio.getValue())) {
            mensajeError("LA FECHA FIN NO DEBE SER MENOR A FECHA INICIO.");
        } else {
            procesandoInsercion();
        }
    }

    private void procesandoInsercion() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR PROMOCIÓN TEMPORADA "
                + textFieldNomPromTemArt.getText() + "?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
//            if ("guardar_prom_temp")) {
            creandoCabPromTempArt();
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/PromocionTemporadaArtFXML.fxml", 982, 812, false);
//            }
        } else if (alert.getResult() == cancel) {
            alert.close();
        }
    }

    private void asignandoDesdeExcel() {
        if (!listViewArticulos.getItems().isEmpty()) {
            mensajeError("PARA IMPORTAR DESDE UN EXCEL\nLA LISTA DE ARTÍCULOS DEBE ESTAR VACÍA.");
        } else {
            if (verificacionFechaArticulo()) {
                Map<Long, Long> hmCodPor = fileChooserFX();
                if (!hmCodPor.isEmpty()) {
                    copyWorkerExcel = createWorkerExcel(hmCodPor);
                    progressIndicatorExcel.progressProperty().unbind();
                    progressIndicatorExcel.progressProperty().bind(copyWorkerExcel.progressProperty());
                    new Thread(copyWorkerExcel).start();
                } else {
                    if (exitoExcel.isEmpty()) {
                        mensajeError("EXCEPCIÓN DESCONOCIDA.");
                    } else {
                        if (exitoExcel.contentEquals("empty")) {
                            mensajeError("EXCEL VACÍO.");
                        } else if (exitoExcel.contentEquals("misc")) {
                            mensajeError("EXCEPCIÓN DESCONOCIDA.");
                        } else if (exitoExcel.contentEquals("null")) {
                            mensajeError("EXCEL NULO.");
                        } else if (exitoExcel.contentEquals("formato")) {
                            mensajeError("EL EXCEL DEBE ESTAR EN FORMATO NÚMERICO\n(COLUMNA 'A' -> CÓDIGO ARTÍCULO)\n(COLUMNA 'B' -> PORCENTAJE DTO.)");
                        } else {
                            String msjCelda[] = exitoExcel.split(" - ");
                            if (msjCelda[0].contentEquals("cod")) {
                                mensajeError("SE DETECTÓ UNA EXCEPCIÓN EN CÓDIGO ARTÍCULO [CELDA - " + msjCelda[1] + "]");
                            } else if (msjCelda[0].contentEquals("por")) {
                                mensajeError("SE DETECTÓ UNA EXCEPCIÓN EN PORCENTAJE DTO. [CELDA - " + msjCelda[1] + "]");
                            } else if (msjCelda[0].contentEquals("overflow")) {
                                mensajeError("SUPERA EL LÍMITE PORCENTAJE DTO. [CELDA - " + msjCelda[1] + "]");
                            } else if (msjCelda[0].contentEquals("codRep")) {
                                if (msjCelda.length == 3) {
                                    mensajeError("[CELDA - " + msjCelda[1] + "]     [CELDA - " + msjCelda[2] + "]\nCÓDIGOS REPETIDOS.");
                                } else {
                                    mensajeError("EXISTEN CÓDIGOS REPETIDOS EN EL EXCEL.");
                                }
                            } else {
                                mensajeError("EXCEPCIÓN DESCONOCIDA.");
                            }
                        }
                    }
                }
            }
        }
    }

    private void derivandoAsignacion() {
        if (hashMapCodArt.containsKey(Long.valueOf(num.numberValidator(textFieldCodArt.getText())))) {
            textFieldCodArt.setText("");
            mensajeError("EL CAMPO CÓD. ARTÍCULO YA SE AGREGÓ A LA LISTA.");
        } else {
            if (ConexionPostgres.conectarLocal()) {
                long cod = Long.valueOf(num.numberValidator(textFieldCodArt.getText()));//para recontra asegurar
                JSONObject jsonArt = generarArticuloLocal(cod,
                        fechaParam(datePickerFechaInicio.getValue()), fechaParam(datePickerFechaFin.getValue()));
                JSONObject jsonArtObs = generarArticuloObsLocal(cod,
                        fechaParam(datePickerFechaInicio.getValue()), fechaParam(datePickerFechaFin.getValue()));
                if (jsonArt != null && jsonArtObs != null) {
                    if (jsonArt.containsKey("promocionNom")) {
                        mensajeAdv("EL CÓDIGO ARTÍCULO ESTÁ SIENDO USADO\nPOR OTRA PROMOCIÓN" + jsonArt.get("promocionNom") + ".");
                    } else if (jsonArtObs.containsKey("promocionNom")) {
                        mensajeAdv("EL CÓDIGO ARTÍCULO ESTÁ SIENDO USADO\nPOR OTRA PROMOCIÓN" + jsonArtObs.get("promocionNom") + "\nEN CONDICIÓN DE OBSEQUIO.");
                    } else {
                        if (!choiceBoxDescuento.getSelectionModel().isSelected(0)) {
                            articulosAsignados.addAll(jsonArt.get("codArticulo").toString() + " - "
                                    + "[ " + choiceBoxDescuento.getSelectionModel().getSelectedItem() + " ] " + jsonArt.get("descripcion").toString().toUpperCase());
                        } else if (!choiceBoxCantMin.getSelectionModel().isSelected(0)
                                && !choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                            articulosAsignados.addAll(jsonArt.get("codArticulo").toString() + " - "
                                    + "[ " + choiceBoxCantMin.getSelectionModel().getSelectedItem()
                                    + " regalo-> " + choiceBoxObsequiar.getSelectionModel().getSelectedItem() + " ] " + jsonArt.get("descripcion").toString().toUpperCase());
                        }
                        listViewArticulos.setItems(articulosAsignados);
                        hashMapCodArt.put(cod, jsonArt);
                        bloqueandoFecha();
                    }
                    textFieldCodArt.setText("");
                } else {
                    textFieldCodArt.setText("");
                    mensajeError("EL CAMPO CÓD. ARTÍCULO NO EXISTE.");
                }
            } else {
                mensajeError("EXCEPCIÓN [BD].");
            }
            if (ConexionPostgres.getCon() != null) {
                ConexionPostgres.cerrarLocal();
            }
        }
    }

    private void asignando() {
        if (verificacionFechaArticulo()) {
            if (choiceBoxDescuento.getSelectionModel().isSelected(0)
                    && choiceBoxCantMin.getSelectionModel().isSelected(0)
                    && choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                textFieldCodArt.setText("");
                mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO\nO\nSELECCIONAR CANT. MÍN. PARA OBSEQUIAR Y CANT. A OBSEQUIAR.");
            } else if (choiceBoxDescuento.getSelectionModel().isSelected(0)
                    && !choiceBoxCantMin.getSelectionModel().isSelected(0)
                    && choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                mensajeError("DEBE SELECCIONAR LA CANT. A OBSEQUIAR.");
            } else if (choiceBoxDescuento.getSelectionModel().isSelected(0)
                    && choiceBoxCantMin.getSelectionModel().isSelected(0)
                    && !choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                mensajeError("DEBE SELECCIONAR LA CANT. MÍN PARA OBSEQUIAR.");
            } else if (textFieldCodArt.getText().isEmpty()) {
                mensajeError("EL CAMPO CÓD. ARTÍCULO NO DEBE ESTAR VACÍO.");
            } else if (choiceBoxDescuento.getSelectionModel().isSelected(0)
                    && !choiceBoxCantMin.getSelectionModel().isSelected(0)
                    && !choiceBoxObsequiar.getSelectionModel().isSelected(0)) {
                if (Integer.valueOf(choiceBoxCantMin.getSelectionModel().getSelectedItem())
                        <= Integer.valueOf(choiceBoxObsequiar.getSelectionModel().getSelectedItem())) {
                    mensajeError("LA CANT. MÍN PARA OBSEQUIAR\nNO DEBE SER MENOR NI IGUAL\nA LA CANT. A OBSEQUIAR.");
                } else {
                    derivandoAsignacion();
                }
            } else {
                derivandoAsignacion();
            }
        }

    }

    private void quitando() {
        for (int i = 0; i < listViewArticulos.getSelectionModel().getSelectedItems().size(); i++) {
            String aux = listViewArticulos.getSelectionModel().getSelectedItems().get(i).toString();
            String[] parts = aux.split(" - ");
            hashMapCodArt.remove(Long.valueOf(num.numberValidator(parts[0])));
        }
        articulosAsignados.removeAll(listViewArticulos.getSelectionModel().getSelectedItems());
        listViewArticulos.setItems(articulosAsignados);
        bloqueandoFecha();
    }

    private void quitandoTodos() {
        articulosAsignados.removeAll(articulosAsignados);
        listViewArticulos.setItems(articulosAsignados);
        bloqueandoFecha();
        hashMapCodArt = new HashMap<>();
    }

    private void limpiandoCampos() {
        textFieldNomPromTemArt.setText("");
        quitandoTodos();
//        datePickerFechaInicio.setValue(null);
        datePickerFechaFin.setValue(null);
        choiceBoxDescuento.getSelectionModel().select(0);
        checkBoxArtNulo.setSelected(false);
        checkBoxArtEnProm.setSelected(false);
    }

    private void resetPromTempArtBaja() {
        exitoBaja = false;
        primeraPaginacion();
        quitandoTodos();
    }

    private void limpiandoBusqueda() {
        textFielFiltroPromTempArt.setText("");
        datePickerFechaInicioFiltro.setValue(null);
        datePickerFechaFinFiltro.setValue(null);
    }

    private void cancelandoDescuentoPromTempArtTodo() {
//        if ("baja_prom_temp")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA?", ok, cancel);
        alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            if (bajaDescPromTempArtTodo()) {
                exitoBaja = false;
            }
            alert2.close();
        } else if (alert2.getResult() == cancel) {
            alert2.close();
        }
//        } else {
//            mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//        }
    }

    public void viendoProgress() {
        anchorPaneListView.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        paginationPromTempArt.setDisable(true);
        anchorPaneBottom.setDisable(true);
        buttonLimpiar.setDisable(true);
        vBoxFiltro.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        checkBoxArtNulo.setDisable(true);
        checkBoxArtEnProm.setDisable(true);
        //
        progressIndicator.setVisible(true);
        labelProgress.setVisible(true);
    }

    public void viendoProgressExcel() {
        labelNomPromTemArt.setDisable(true);
        textFieldNomPromTemArt.setDisable(true);
        gridPaneFecha.setDisable(true);
        listViewArticulos.setDisable(true);
        vBoxButtons.setDisable(true);
        hBoxComboSeccion.setDisable(true);
        hBoxDescuento.setDisable(true);
        buttonInsertar.setDisable(true);
        buttonExcel.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        paginationPromTempArt.setDisable(true);
        anchorPaneBottom.setDisable(true);
        buttonLimpiar.setDisable(true);
        vBoxFiltro.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        checkBoxArtNulo.setDisable(true);
        checkBoxArtEnProm.setDisable(true);
        //
        progressIndicatorExcel.setVisible(true);
        labelProgressBarExcel.setVisible(true);
        labelPorc.setVisible(true);
    }

    public void ocultandoProgress() {
        anchorPaneListView.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        paginationPromTempArt.setDisable(false);
        anchorPaneBottom.setDisable(false);
        buttonLimpiar.setDisable(false);
        vBoxFiltro.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        checkBoxArtNulo.setDisable(false);
        checkBoxArtEnProm.setDisable(false);
        //
        progressIndicator.setVisible(false);
        labelProgress.setVisible(false);
    }

    public void ocultandoProgressExcel() {
        labelNomPromTemArt.setDisable(false);
        textFieldNomPromTemArt.setDisable(false);
        gridPaneFecha.setDisable(false);
        listViewArticulos.setDisable(false);
        vBoxButtons.setDisable(false);
        hBoxComboSeccion.setDisable(false);
        hBoxDescuento.setDisable(false);
        buttonInsertar.setDisable(false);
        buttonExcel.setDisable(false);
        anchorPaneListView.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        paginationPromTempArt.setDisable(false);
        anchorPaneBottom.setDisable(false);
        buttonLimpiar.setDisable(false);
        vBoxFiltro.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        checkBoxArtNulo.setDisable(false);
        checkBoxArtEnProm.setDisable(false);
        //
        progressIndicatorExcel.setVisible(false);
        labelProgressBarExcel.setVisible(false);
        labelPorc.setVisible(false);
        textFieldCodArt.setText("");
        if (!excepcionExcelBD.isEmpty()) {
            mensajeError(excepcionExcelBD);
            quitandoTodos();
        }
    }

    private Integer jsonRowCount() {
        Integer rowCount = 0;
        rowCount = generarCountPromoTemp();
        return rowCount;
    }

    private Integer jsonRowCountFilter(String promTempFilter, String fechaDesde, String fechaHasta) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFilterPromoTempLocal(promTempFilter, fechaDesde, fechaHasta);
        return rowCountFilter;
    }

    private List<JSONObject> jsonArrayPromTempArtFetch(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> promTempJSONObjList = new ArrayList<>();
        promTempArtJSONArray = generarFetchPromoTemp(limRowS, offSetS);
        for (Object promTempObj : promTempArtJSONArray) {
            JSONObject promTempJSONObj = (JSONObject) promTempObj;
            promTempJSONObjList.add(promTempJSONObj);
        }
        return promTempJSONObjList;
    }

    private List<JSONObject> jsonArrayPromTempArtFetchFiltro(int limRow, int offSet, String promTempFiltro, String fechaDesde, String fechaHasta) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        promTempArtJSONArray = generarFetchFiltroPromoTempLocal(limRowS, offSetS, promTempFiltro, fechaDesde, fechaHasta);
        for (Object cabFielObj : promTempArtJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Map recuperarFechaLong(Long id) {
        Map mapeo = new HashMap();
        PromoTemporadaArt desc = promoTemporadaArtDAO.getById(id);
        mapeo.put("fechaAlta", desc.getFechaAlta().getTime());
        mapeo.put("fechaMod", desc.getFechaMod().getTime());
        mapeo.put("fechaInicio", desc.getFechaInicio().getTime());
        mapeo.put("fechaFin", desc.getFechaFin().getTime());
        return mapeo;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, PROMO TEMP. -> POST
    private boolean creandoCabPromTempArt() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject cabPromTempArt = new JSONObject();
        try {
            cabPromTempArt = creandoJsonPromTempArt();
            CajaDatos.setIdPromoTemporadaArt(rangoPromoTempArtDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            cabPromTempArt.put("idTemporadaArt", CajaDatos.getIdPromoTemporadaArt());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaArt");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(cabPromTempArt.toString());
                Utilidades.log.info("PROMO TEMPORADA ART. ->> " + cabPromTempArt.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        cabPromTempArt = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setDescuentoPromoArt(true);
                        exitoInsertarCab = true;
                    }
                    br.close();
                } else {
                    cabPromTempArt = registrarPromoTemporadaArtLocal(cabPromTempArt);
                }
            } else {
                cabPromTempArt = registrarPromoTemporadaArtLocal(cabPromTempArt);
            }
        } catch (IOException | ParseException ex) {
            cabPromTempArt = registrarPromoTemporadaArtLocal(cabPromTempArt);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoInsertarCab) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            long longInicio = Long.parseLong(cabPromTempArt.get("fechaInicio").toString());
            long longFin = Long.parseLong(cabPromTempArt.get("fechaFin").toString());
            cabPromTempArt.put("fechaAlta", null);
            cabPromTempArt.put("fechaMod", null);
            cabPromTempArt.put("fechaInicio", null);
            cabPromTempArt.put("fechaFin", null);
            Utilidades.log.info("-->> " + cabPromTempArt.toString());
            PromoTemporadaArtDTO promoTemporadaArtDTO = gson.fromJson(cabPromTempArt.toString(), PromoTemporadaArtDTO.class);
            promoTemporadaArtDTO.setFechaAlta(timestamp);
            promoTemporadaArtDTO.setFechaMod(timestamp);
            promoTemporadaArtDTO.setFechaInicio(new Timestamp(longInicio));
            promoTemporadaArtDTO.setFechaFin(new Timestamp(longFin));
            PromoTemporadaArt valor = promoTemporadaArtDAO.insertarObtenerObjeto(PromoTemporadaArt.fromPromoTemporadaArtDTO(promoTemporadaArtDTO));
            if (valor != null) {
                Utilidades.log.info("DATOS PERSISTIDOS EN POSTGRES LOCAL PROMO TEMPORADA ART. CABECERA");
            } else {
                Utilidades.log.info("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL PROMO TEMPORADA ART. CABECERA");
            }
            //CIERRE INSERCION food_local
            JSONArray jsonArrayPromTempDet = new JSONArray();
            for (int i = 0; i < articulosAsignados.size(); i++) {
                String[] parts = articulosAsignados.get(i).toString().split(" - ");
                String[] parts2 = parts[1].split(" ");
                if (parts2[2].contentEquals("%")) {
                    int porcentajeDesc = Integer.valueOf(parts2[1]);
                    jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTempArt, hashMapCodArt.get(Long.valueOf(parts[0])), porcentajeDesc));
                } else {
                    int minReq = Integer.valueOf(parts2[1]);
                    int cantObsequio = Integer.valueOf(parts2[3]);
                    jsonArrayPromTempDet.add(creandoJsonDetPromTempObsequio(cabPromTempArt, hashMapCodArt.get(Long.valueOf(parts[0])), minReq, cantObsequio));
                }
            }
            copyWorker = createWorker();
            progressIndicator.progressProperty().unbind();
            progressIndicator.progressProperty().bind(copyWorker.progressProperty());
            new Thread(copyWorker).start();
            arrayDetalles = jsonArrayPromTempDet;
        }
        return exitoInsertarCab;
    }
    //////CREATE, PROMO TEMP. -> POST

    //////CREATE, PROMO TEMP. ARTÍCULO -> POST
    private boolean creandoDetPromTempArt(JSONObject detPromTempArt) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/artPromoTemporada");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(detPromTempArt.toString());
                Utilidades.log.info("-->> " + detPromTempArt.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempArtLocal(detPromTempArt);
                }
            } else {
                exitoInsertarDet = registrarPromoTempArtLocal(detPromTempArt);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempArtLocal(detPromTempArt);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        ArtPromoTemporadaDTO artDTO = gson.fromJson(detPromTempArt.toString(), ArtPromoTemporadaDTO.class);
        ArtPromoTemporada art = ArtPromoTemporada.fromArtPromoTemporada(artDTO);
        if (artPromoTemporadaDAO.insercionMasivaEstado(art)) {
            Utilidades.log.info("DATOS PERSISTIDOS EN ARTÍCULO PROMO TEMPORADA");
        } else {
            Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN ARTÍCULO PROMO TEMPORADA");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. ARTÍCULO -> POST

    //////CREATE, PROMO TEMP. ARTÍCULO OBSEQUIO -> POST
    private boolean creandoDetPromTempArtObsequio(JSONObject detPromTempArtObsequio) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/artPromoTemporadaObsequio");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(detPromTempArtObsequio.toString());
                Utilidades.log.info("-->> " + detPromTempArtObsequio.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDetObsequio = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDetObsequio = registrarPromoTempArtObsequioLocal(detPromTempArtObsequio);
                }
            } else {
                exitoInsertarDetObsequio = registrarPromoTempArtObsequioLocal(detPromTempArtObsequio);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDetObsequio = registrarPromoTempArtObsequioLocal(detPromTempArtObsequio);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        ArtPromoTemporadaObsequioDTO artDTO = gson.fromJson(detPromTempArtObsequio.toString(), ArtPromoTemporadaObsequioDTO.class);
        ArtPromoTemporadaObsequio art = ArtPromoTemporadaObsequio.fromArtPromoTemporada(artDTO);
        if (artPromoTemporadaObsequioDAO.insercionMasivaEstado(art)) {
            Utilidades.log.info("DATOS PERSISTIDOS EN ARTÍCULO PROMO TEMPORADA OBSEQUIO");
        } else {
            Utilidades.log.info("DATOS NO!.... PERSISTIDOS EN ARTÍCULO PROMO TEMPORADA OBSEQUIO");
        }
        return exitoInsertarDetObsequio;
    }
    //////CREATE, PROMO TEMP. ARTÍCULO OBSEQUIO -> POST

    //////READ, PROMO TEMP. ID -> GET
    private JSONObject recuperarDatoPromoTempArt(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaArt/getById/" + id);
            URLConnection uc = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            while ((inputLine = br.readLine()) != null) {
                descuento = (JSONObject) parser.parse(inputLine);
            }
            br.close();
        } catch (IOException | ParseException ex) {
            descuento = null;
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return descuento;
    }
    //////READ, PROMO TEMP. ID -> GET

    //////UPDATE, BAJA -> PUT
    private boolean bajaPromTempArt(JSONObject promTempArt) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            JSONObject obj = recuperarDatoPromoTempArt(Long.parseLong(promTempArt.get("idTemporadaArt").toString()));
            boolean status = false;
            if (obj != null) {
                obj.remove("fechaInicio");
                obj.remove("fechaFin");
                obj.remove("fechaAlta");
                obj.remove("fechaMod");
                obj.remove("artPromoTemporadaDTO");
                PromoTemporadaArtDTO proDTO = gson.fromJson(obj.toString(), PromoTemporadaArtDTO.class);
                if (proDTO.getIdTemporadaArt() != null) {
                    status = true;
                }
            }
            Map mapeo = recuperarFechaLong(Long.valueOf(promTempArt.get("idTemporadaArt").toString()));
            promTempArt.put("fechaInicio", mapeo.get("fechaInicio"));
            promTempArt.put("fechaFin", mapeo.get("fechaFin"));
            promTempArt.put("fechaAlta", mapeo.get("fechaAlta"));
            promTempArt.put("fechaMod", mapeo.get("fechaMod"));
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaArt");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(promTempArt.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarPromo(promTempArt);
                }
            } else {
                exitoBaja = actualizarPromo(promTempArt);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarPromo(promTempArt);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            java.sql.Timestamp fechaInicio = null;
            java.sql.Timestamp fechaFin = null;
            if (promTempArt.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(promTempArt.get("fechaAlta").toString()));
                promTempArt.put("fechaAlta", null);
            }
            if (promTempArt.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(promTempArt.get("fechaMod").toString()));
                promTempArt.put("fechaMod", null);
            }
            if (promTempArt.get("fechaInicio") != null) {
                fechaInicio = new Timestamp(Long.parseLong(promTempArt.get("fechaInicio").toString()));
                promTempArt.put("fechaInicio", null);
            }
            if (promTempArt.get("fechaFin") != null) {
                fechaFin = new Timestamp(Long.parseLong(promTempArt.get("fechaFin").toString()));
                promTempArt.put("fechaFin", null);
            }
            PromoTemporadaArtDTO promoDTO = gson.fromJson(promTempArt.toString(), PromoTemporadaArtDTO.class);
            promoDTO.setFechaAlta(fechaAlta);
            promoDTO.setFechaMod(fechaMod);
            promoDTO.setFechaInicio(fechaInicio);
            promoDTO.setFechaFin(fechaFin);
            promoDTO.setEstadoPromo(false);
            if (promoTemporadaArtDAO.actualizarObtenerEstado(PromoTemporadaArt.fromPromoTemporadaArtDTO(promoDTO))) {
                Utilidades.log.info("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                Utilidades.log.info("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡PROMOCIÓN POR TEMPORADA CANCELADA!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetPromTempArtBaja();
            } else {
                alert2.close();
                resetPromTempArtBaja();
            }
        } else {
            mensajeError("NO SE CANCELÓ LA PROMOCIÓN TEMPORADA.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA -> PUT

    //////UPDATE, BAJA MASIVA -> GET
    private boolean bajaDescPromTempArtTodo() {
        JSONParser parser = new JSONParser();
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaArt/baja/" + u + "/" + ts);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    exitoBaja = (Boolean) parser.parse(inputLine);
                }
                if (exitoBaja) {
                    boolean valor = promoTemporadaArtDAO.bajasLocal(Identity.getNomFun(), timestamp);
                    if (valor) {
                        Utilidades.log.info("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                    }
                }
                br.close();
            } else {
                exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA ARTÍCULO HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetPromTempArtBaja();
            } else {
                alert2.close();
                resetPromTempArtBaja();
            }
        } else {
            mensajeError("LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA ARTÍCULO NO SE CANCELARON.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA MASIVA -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, ROW COUNT PROMO TEMP.
    private Integer generarCountPromoTemp() {
        return (int) promoTemporadaArtDAO.rowCount();
    }
    //////READ, ROW COUNT PROMO TEMP.

    //////READ, ROW COUNT FILTER PROMO TEMP.
    private Integer generarCountFilterPromoTempLocal(String promTempFilter, String fechaDesde, String fechaHasta) {
        return (int) promoTemporadaArtDAO.rowCountFilter(promTempFilter, fechaDesde, fechaHasta);
    }
    //////READ, ROW COUNT FILTER PROMO TEMP.

    //////READ, FETCH PROMO TEMP.
    private JSONArray generarFetchPromoTemp(String limRowS, String offSetS) {
        JSONArray listaObj = new JSONArray();
        /*JSONParser parser = new JSONParser();
        for (PromoTemporadaArt pro : promoTemporadaArtDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(pro.toPromoTemporadaArtDTO()));
                listaObj.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }*/
        if (ConexionPostgres.conectarLocal()) {
            listaObj = PromoTemporadaArtLocalQueries.promocionArticulosFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS));
        }
        if (ConexionPostgres.getConLocal() != null) {
            ConexionPostgres.cerrarLocal();
        }
        return listaObj;
    }
    //////READ, FETCH PROMO TEMP.

    //////READ, FETCH FILTRO PROMO TEMP.
    private JSONArray generarFetchFiltroPromoTempLocal(String limRowS, String offSetS, String promTempFiltro, String fechaDesde, String fechaHasta) {
        JSONArray listaJSON = new JSONArray();
        /*JSONParser parser = new JSONParser();
        for (PromoTemporadaArt desc : promoTemporadaArtDAO.listarFETCHFilter(Long.parseLong(limRowS), Long.parseLong(offSetS), promTempFiltro, fechaDesde, fechaHasta)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(desc.toPromoTemporadaArtDTO()));
                listaJSON.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }*/
        if (ConexionPostgres.conectarLocal()) {
            listaJSON = PromoTemporadaArtLocalQueries.promocionArticulosFETCHFilter(Long.parseLong(limRowS), Long.parseLong(offSetS), promTempFiltro, fechaDesde, fechaHasta);
        }
        if (ConexionPostgres.getConLocal() != null) {
            ConexionPostgres.cerrarLocal();
        }
        return listaJSON;
    }
    //////READ, FETCH FILTRO PROMO TEMP.

    //////READ, ARTÍCULO
    private JSONObject generarArticuloLocal(Long codigo, String fechaIniParam, String fechaFinParam) {
        JSONObject jsonArt = new JSONObject();
        JSONParser parser = new JSONParser();
        long codArt = PromoTemporadaArtLocalQueries.verificandoCodArt(codigo, fechaIniParam, fechaFinParam);
        if (codArt != -1l) {
            Articulo artRevancha = articuloDAO.buscarCod(String.valueOf(codArt));
            if (artRevancha != null) {
                try {
                    jsonArt = (JSONObject) parser.parse(gson.toJson(artRevancha.toArticuloDTO()));
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            } else {
                jsonArt = null;
            }
        } else {
            jsonArt.put("promocionNom", PromoTemporadaArtLocalQueries.promocionNom(codigo));
            //"empty", para saber qué msj mostrar... (existe, pero en uso)
        }
        return jsonArt;
    }
    //////READ, ARTÍCULO

    //////READ, ARTÍCULO OBSEQUIO
    private JSONObject generarArticuloObsLocal(Long codigo, String fechaIniParam, String fechaFinParam) {
        JSONObject jsonArt = new JSONObject();
        JSONParser parser = new JSONParser();
        long codArt = PromoTemporadaArtLocalQueries.verificandoCodArtObsequio(codigo, fechaIniParam, fechaFinParam);
        if (codArt != -1l) {
            Articulo artRevancha = articuloDAO.buscarCod(String.valueOf(codArt));
            if (artRevancha != null) {
                try {
                    jsonArt = (JSONObject) parser.parse(gson.toJson(artRevancha.toArticuloDTO()));
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            } else {
                jsonArt = null;
            }
        } else {
            jsonArt.put("promocionNom", PromoTemporadaArtLocalQueries.promocionNomObsequio(codigo));
            //"empty", para saber qué msj mostrar... (existe, pero en uso)
        }
        return jsonArt;
    }
    //////READ, ARTÍCULO OBSEQUIO

    //////CREATE, PENDIENTES PROM TEMP.
    private JSONObject registrarPromoTemporadaArtLocal(JSONObject cabPromTempArt) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada_art");
        j.put("data", cabPromTempArt.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                obj = cabPromTempArt;
                exitoInsertarCab = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////CREATE, PENDIENTES PROM TEMP. 

    //////UPDATE, PENDIENTES - PROMO TEMP. MASIVO
    private boolean recuperarBajavLocal(String u, Timestamp ts) {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        boolean valor = false;
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada_art_masivo");
        j.put("data", "{\"estadoPromo\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U', '" + x + "');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a PARANA LOCAL BD ********");
                valor = promoTemporadaArtDAO.bajasLocal(u, ts);
                if (valor) {
                    Utilidades.log.info("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- " + valor);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - PROMO TEMP. MASIVO

    //////UPDATE, PENDIENTES - PROMO TEMP.
    private boolean actualizarPromo(JSONObject promTempArt) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada_art");
        j.put("data", promTempArt.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U','" + x + "')";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - PROMO TEMP.

    //////INSERT, PENDIENTES - ARTÍCULO_PROMO TEMP.
    private boolean registrarPromoTempArtLocal(JSONObject array) {
        try {
            boolean estado = false;
            JSONParser parser = new JSONParser();
            if (ConexionPostgres.getCon() == null) {
                ConexionPostgres.conectar();
            }
            JSONObject json = (JSONObject) parser.parse(array.toString());
            JSONObject j = new JSONObject();
            j.put("table", "art_promo_temporada");
            j.put("data", json.toJSONString());
            String x = Utilidades.setToJson(j.toString());
            String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
            Utilidades.log.info("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES handler_food BD ********");
                    estado = true;
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
            if (ConexionPostgres.getCon() != null) {
                ConexionPostgres.cerrar();
            }
            return estado;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return false;
        }
    }
    //////INSERT, PENDIENTES - ARTÍCULO_PROMO TEMP.

    //////INSERT, PENDIENTES - ARTÍCULO_PROMO TEMP.
    private boolean registrarPromoTempArtObsequioLocal(JSONObject array) {
        try {
            boolean estado = false;
            JSONParser parser = new JSONParser();
            if (ConexionPostgres.getCon() == null) {
                ConexionPostgres.conectar();
            }
            JSONObject json = (JSONObject) parser.parse(array.toString());
            JSONObject j = new JSONObject();
            j.put("table", "art_promo_temporada_obsequio");
            j.put("data", json.toJSONString());
            String x = Utilidades.setToJson(j.toString());
            String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + x + "')";
            Utilidades.log.info("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES handler_food BD ********");
                    estado = true;
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
            if (ConexionPostgres.getCon() != null) {
                ConexionPostgres.cerrar();
            }
            return estado;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return false;
        }
    }
    //////INSERT, PENDIENTES - ARTÍCULO_PROMO TEMP.
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON PROMO TEMP.
    private JSONObject creandoJsonPromTempArt() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject promTempArt = new JSONObject();
        //**********************************************************************
        promTempArt.put("descripcionTemporadaArt", textFieldNomPromTemArt.getText());
        promTempArt.put("estadoPromo", true);
        //************** Fecha Inicio / Fecha Fin
        LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
        LocalDate fechaFinLD = datePickerFechaFin.getValue();
        Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
        Timestamp timestampIni = new Timestamp(dateIni.getTime());
        Long timestampIniJSON = timestampIni.getTime();
        Date dateFin = java.sql.Date.valueOf(fechaFinLD);
        Timestamp timestampFin = new Timestamp(dateFin.getTime());
        Long timestampFinJSON = timestampFin.getTime();
        //************** Fecha Inicio / Fecha Fin
        promTempArt.put("fechaInicio", timestampIniJSON);
        promTempArt.put("fechaFin", timestampFinJSON);
        //**********************************************************************
        promTempArt.put("usuAlta", Identity.getNomFun());
        promTempArt.put("fechaAlta", timestampJSON);
        promTempArt.put("usuMod", Identity.getNomFun());
        promTempArt.put("fechaMod", timestampJSON);
        //**********************************************************************
        return promTempArt;
    }
    //JSON PROMO TEMP.

    //JSON PROMO DET.
    private JSONObject creandoJsonDetPromTemp(JSONObject promTempCab, JSONObject articulo, int porcentajeDesc) {
        JSONObject promTempDet = new JSONObject();
        //**********************************************************************
        promTempDet.put("promoTemporadaArt", promTempCab);
        promTempDet.put("articulo", articulo);
        promTempDet.put("descriArticulo", articulo.get("descripcion"));
        promTempDet.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return promTempDet;
    }
    //JSON PROMO DET.

    //JSON PROMO DET. OBSEQUIO
    private JSONObject creandoJsonDetPromTempObsequio(JSONObject promTempCab, JSONObject articulo, int minReq, int cantObsequio) {
        JSONObject promTempDet = new JSONObject();
        //**********************************************************************
        promTempDet.put("promoTemporadaArt", promTempCab);
        promTempDet.put("articulo", articulo);
        promTempDet.put("descriArticulo", articulo.get("descripcion"));
        promTempDet.put("minReq", minReq);
        promTempDet.put("cantObsequio", cantObsequio);
        //**********************************************************************
        return promTempDet;
    }
    //JSON PROMO DET. OBSEQUIO

    //JSON PROMO BAJA
    private JSONObject bajaJsonCabPromTempArt(JSONObject promTempCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        promTempCab.put("usuMod", Identity.getNomFun());
        promTempCab.put("fechaMod", timestampJSON);
        promTempCab.put("estadoPromo", false);
        //**********************************************************************
        return promTempCab;
    }
    //JSON PROMO BAJA
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantPromTempArt = 5;
        filaIniPromTempArt = 0;
        pageActualPromTempArt = 0;
        buscarTodosPromTempArt = true;
        buscandoPromTempArtTodo();
    }

    private void buscandoPromTempArtTodo() {
        filaLimitPromTempArt = jsonRowCount();
        buscarTodosPromTempArt = true;
        paginandoPromTemp();
    }

    private void buscandoPromTempArtFiltro() {
        String filterParam = "null";
        String filterFechaDesde = "null";
        String filterFechaHasta = "null";
        LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
        LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
        if (fechaDesdeLD != null) {
            Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
            filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
        }
        if (fechaHastaLD != null) {
            Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
            filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
        }
        if (!textFielFiltroPromTempArt.getText().contentEquals("")) {
            filterParam = textFielFiltroPromTempArt.getText();
        }
        filaLimitPromTempArt = jsonRowCountFilter(filterParam, filterFechaDesde, filterFechaHasta);
        buscarTodosPromTempArt = false;
        paginandoPromTemp();
    }

    private void paginandoPromTemp() {
        paginationPromTempArt.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFiel(pageIndex);
                //**************************************************************
            }
        });
        paginationPromTempArt.setCurrentPageIndex(this.pageActualPromTempArt);//index inicial paginación
        paginationPromTempArt.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountPromTempArt = filaLimitPromTempArt / filaCantPromTempArt;//cantidad index paginación
        if (filaLimitPromTempArt % filaCantPromTempArt > 0) {//index EXTRA (resto de filas)***********
            pageCountPromTempArt++;
        }
        paginationPromTempArt.setPageCount(pageCountPromTempArt);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFiel(int pageIndex) {
        filaIniPromTempArt = pageIndex * filaCantPromTempArt;
        if (buscarTodosPromTempArt) {
            promTempArtList = jsonArrayPromTempArtFetch(filaCantPromTempArt, filaIniPromTempArt);
        } else {
            String filterParam = "null";
            String filterFechaDesde = "null";
            String filterFechaHasta = "null";
            LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
            LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
            if (fechaDesdeLD != null) {
                Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
                filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
            }
            if (fechaHastaLD != null) {
                Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
                filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
            }
            if (!textFielFiltroPromTempArt.getText().contentEquals("")) {
                filterParam = textFielFiltroPromTempArt.getText();
            }
            promTempArtList = jsonArrayPromTempArtFetchFiltro(filaCantPromTempArt, filaIniPromTempArt, filterParam, filterFechaDesde, filterFechaHasta);
        }
        actualizandoTablaPromTemp();
        return tablePromTempArt;
    }

    private void actualizandoTablaPromTemp() {
        tablePromTempArt = new TableView<JSONObject>();
        promTempArtData = FXCollections.observableArrayList(promTempArtList);
        //columna Sección ....................................................
        columnPromTempArt = new TableColumn("Promoción temporada (Art.)");
        columnPromTempArt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPromTempArt.setMinWidth(540);
        columnPromTempArt.setEditable(false);
        columnPromTempArt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descripcionTemporadaArt").toString());
            }
        });
        //columna Sección ....................................................
        //columna Fecha Inicio..................................................
        columnPromTempArtFechaIni = new TableColumn("Fecha inicio");
        columnPromTempArtFechaIni.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPromTempArtFechaIni.setMinWidth(120);
        columnPromTempArtFechaIni.setEditable(false);
        columnPromTempArtFechaIni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaInicio"))));
            }
        });
        //columna Fecha Inicio..................................................
        //columna Fecha Fin.....................................................
        columnPromTempArtFechaFin = new TableColumn("Fecha fin");
        columnPromTempArtFechaFin.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPromTempArtFechaFin.setMinWidth(120);
        columnPromTempArtFechaFin.setEditable(false);
        columnPromTempArtFechaFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaFin"))));
            }
        });
        //columna Fecha Fin.....................................................
        //columna Cancelar .....................................................
        columnAcciones = new TableColumn("Acciones");
        columnAcciones.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new PromocionTemporadaArtFXMLController.AddObjectsCell(tablePromTempArt);
            }
        });
        //columna Cancelar .....................................................
        tablePromTempArt.getColumns().addAll(columnPromTempArt, columnPromTempArtFechaIni, columnPromTempArtFechaFin, columnAcciones);
        tablePromTempArt.setItems(promTempArtData);
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button("cancelar");
        Button verButton = new Button("ver artículos");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    promTempCab = new JSONObject();
                    promTempCab = tablePromTempArt.getItems().get(index);
                    JSONArray promTempDetArray = (JSONArray) promTempCab.get("artPromoTemporadaDTO");
                    promTempDetJSONObjList = new ArrayList<>();
                    if (promTempDetArray != null) {
                        for (Object promTempDetObj : promTempDetArray) {
                            JSONObject promTempDetJSONObj = (JSONObject) promTempDetObj;
                            promTempDetJSONObjList.add(promTempDetJSONObj);
                        }
                    }
                    JSONArray promTempDetObsequioArray = (JSONArray) promTempCab.get("artPromoTemporadaObsequio");
                    promTempDetObsequioJSONObjList = new ArrayList<>();
                    if (promTempDetObsequioArray != null) {
                        for (Object promTempDetObsequioObj : promTempDetObsequioArray) {
                            JSONObject promTempDetObsequioJSONObj = (JSONObject) promTempDetObsequioObj;
                            getPromTempDetObsequioJSONObjList().add(promTempDetObsequioJSONObj);
                        }
                    }
                    viendoDetalleArticulo();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_prom_temp")) {
                    JSONObject descPromTemp = tablePromTempArt.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR PROMOCIÓN TEMPORADA "
                            + descPromTemp.get("descripcionTemporadaArt").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descPromTemp = bajaJsonCabPromTempArt(descPromTemp);
                        bajaPromTempArt(descPromTemp);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker() {
        return new Task() {
            @Override
            protected Object call() {
                JSONParser parser = new JSONParser();
                int i = 1;
                int size = arrayDetalles.size();
                Platform.runLater(() -> {
                    viendoProgress();
                });
                for (int j = 0; j < size; j++) {
                    try {
                        JSONObject obj = (JSONObject) parser.parse(arrayDetalles.get(j).toString());
                        Utilidades.log.info("-->> " + obj);
                        if (obj.containsKey("minReq") && obj.containsKey("cantObsequio")) {
                            creandoDetPromTempArtObsequio(obj);
                        } else {
                            creandoDetPromTempArt(obj);
                        }
                        updateProgress((j + 1), size);
                        if (i == size) {
                            exitoInsertarCab = false;
                            Platform.runLater(() -> {
                                buscandoPromTempArtTodo();
                                limpiandoCampos();
                                datePickerFechaInicio.setValue(LocalDate.now());
                            });
                        } else {
                            i++;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                Platform.runLater(() -> {
                    ocultandoProgress();
                });
                CajaDatos.setDescuentoPromo(false);
                return true;
            }
        };
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    //PROGRESS TASK EXCEL**************************** -> -> -> -> -> -> -> -> ->
    public Task createWorkerExcel(Map<Long, Long> hmCodPor) {
        return new Task() {
            @Override
            protected Object call() {
                int i = 1;
                int size = hmCodPor.size();
                Platform.runLater(() -> {
                    viendoProgressExcel();
                });
                excepcionExcelBD = "";
                if (ConexionPostgres.conectarLocal()) {
                    for (Map.Entry<Long, Long> entry : hmCodPor.entrySet()) {
                        Utilidades.log.info("código = " + entry.getKey() + ", porcentaje = " + entry.getValue());
                        JSONObject jsonArt = generarArticuloLocal(entry.getKey(),
                                fechaParam(datePickerFechaInicio.getValue()), fechaParam(datePickerFechaFin.getValue()));
                        if (jsonArt != null) {
                            if (jsonArt.containsKey("promocionNom")) {
                                if (!checkBoxArtEnProm.isSelected()) {
                                    excepcionExcelBD = "EL CÓD. ARTÍCULO [" + entry.getKey() + "] ESTÁ SIENDO USADO\nPOR OTRA PROMOCIÓN" + jsonArt.get("promocionNom") + ".";
                                    break;
                                } else {
                                    Utilidades.log.info("EL CÓD. ARTÍCULO [" + entry.getKey() + "] ESTÁ SIENDO USADO\nPOR OTRA PROMOCIÓN" + jsonArt.get("promocionNom") + ".");
                                }
                            } else {
                                hashMapCodArt.put(entry.getKey(), jsonArt);
                                Utilidades.log.info(entry.getKey() + " " + entry.getValue());
                                updateProgress(i, size);
                                int porc = (i * 100) / size;
                                i++;
                                Platform.runLater(() -> {
                                    articulosAsignados.addAll(jsonArt.get("codArticulo").toString() + " - "
                                            + "[ " + entry.getValue() + " % ] "
                                            + jsonArt.get("descripcion").toString().toUpperCase());
                                    listViewArticulos.setItems(articulosAsignados);
                                    labelPorc.setText(porc + " %");
                                });
                            }
                        } else if (!checkBoxArtNulo.isSelected()) {
                            excepcionExcelBD = "EL CÓD. ARTÍCULO [" + entry.getKey() + "] NO EXISTE.";
                            break;
                        } else {
                            Utilidades.log.info("EL CÓD. ARTÍCULO [" + entry.getKey() + "] NO EXISTE.");
                        }
                    }
                } else {
                    excepcionExcelBD = "EXCEPCIÓN [BD]";
                }
                if (ConexionPostgres.getCon() != null) {
                    ConexionPostgres.cerrarLocal();
                }
                Platform.runLater(() -> {
                    bloqueandoFecha();
                    ocultandoProgressExcel();
                });
                return true;
            }
        };
    }
    //PROGRESS TASK EXCEL**************************** -> -> -> -> -> -> -> -> ->

    public static List<JSONObject> getPromTempDetJSONObjList() {
        return promTempDetJSONObjList;
    }

    public static JSONObject getPromTempCab() {
        return promTempCab;
    }

    public static List<JSONObject> getPromTempDetObsequioJSONObjList() {
        return promTempDetObsequioJSONObjList;
    }

    public static void setPromTempDetObsequioJSONObjList(List<JSONObject> aPromTempDetObsequioJSONObjList) {
        promTempDetObsequioJSONObjList = aPromTempDetObsequioJSONObjList;
    }

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }
}
