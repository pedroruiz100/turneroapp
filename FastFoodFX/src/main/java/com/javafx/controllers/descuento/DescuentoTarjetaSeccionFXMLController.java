/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.dao.Nf1TipoDAO;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class DescuentoTarjetaSeccionFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private SplitPane splitPaneConfigFuncDesc;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelConfigDescuentoTarjeta;
    @FXML
    private AnchorPane anchorPaneListView;
    @FXML
    private AnchorPane anchorPaneDisable;
    @FXML
    private SplitPane splitPaneTreeView;
    @FXML
    private AnchorPane anchorPaneTitleTreeView;
    @FXML
    private Label labelTreeView;
    @FXML
    private AnchorPane anchorPaneTreeView;
    @FXML
    private TreeView<String> treeViewSecciones;
    @FXML
    private SplitPane splitPaneListView;
    @FXML
    private AnchorPane anchorPaneTitleListView;
    @FXML
    private Label labelListView;
    @FXML
    private ListView<HBox> listViewSeccion;
    @FXML
    private VBox vBoxButtons;
    @FXML
    private Button buttonAgregar;
    @FXML
    private Button buttonAgregarTodos;
    @FXML
    private Button buttonQuitar;
    @FXML
    private Button buttonQuitarTodos;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;

    @Autowired
    private Nf1TipoDAO nf1TipoDAO;

    private boolean alert;
    //para inserción
    HashMap<ImageView, Nf1Tipo> hashMapNf1;
    HashMap<ImageView, Nf2Sfamilia> hashMapNf2;
    HashMap<ImageView, Nf3Sseccion> hashMapNf3;
    HashMap<ImageView, Nf4Seccion1> hashMapNf4;
    HashMap<ImageView, Nf5Seccion2> hashMapNf5;
    HashMap<ImageView, Nf6Secnom6> hashMapNf6;
    HashMap<ImageView, Nf7Secnom7> hashMapNf7;
    //para inserción
    //para consulta detalle
    HashMap<Long, Nf1Tipo> hashMapDetalleNf1;
    HashMap<Long, Nf2Sfamilia> hashMapDetalleNf2;
    HashMap<Long, Nf3Sseccion> hashMapDetalleNf3;
    HashMap<Long, Nf4Seccion1> hashMapDetalleNf4;
    HashMap<Long, Nf5Seccion2> hashMapDetalleNf5;
    HashMap<Long, Nf6Secnom6> hashMapDetalleNf6;
    HashMap<Long, Nf7Secnom7> hashMapDetalleNf7;
    //para consulta detalle
    //para asignación
    HashMap<Nf1Tipo, HBox> hashMapAsignacionNf1;
    HashMap<Nf2Sfamilia, HBox> hashMapAsignacionNf2;
    HashMap<Nf3Sseccion, HBox> hashMapAsignacionNf3;
    HashMap<Nf4Seccion1, HBox> hashMapAsignacionNf4;
    HashMap<Nf5Seccion2, HBox> hashMapAsignacionNf5;
    HashMap<Nf6Secnom6, HBox> hashMapAsignacionNf6;
    HashMap<Nf7Secnom7, HBox> hashMapAsignacionNf7;
    //para asignación
    //para inserción en dto. tarjeta
    private static HashMap<Long, String> hashMapNf1Id;
    private static HashMap<Long, String> hashMapNf2Id;
    private static HashMap<Long, String> hashMapNf3Id;
    private static HashMap<Long, String> hashMapNf4Id;
    private static HashMap<Long, String> hashMapNf5Id;
    private static HashMap<Long, String> hashMapNf6Id;
    private static HashMap<Long, String> hashMapNf7Id;
    //para inserción en dto. tarjeta
    private ObservableList<HBox> seccionesAsignadas;
    @FXML
    private AnchorPane anchorPane;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonAgregarAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void buttonAgregarTodosAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void buttonQuitarAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void buttonQuitarTodosAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        generarNivelFamiliaLocal();
        alert = false;
        //list view secciones con descuento...
        ObservableList<HBox> list = FXCollections.observableArrayList();
        seccionesAsignadas = FXCollections.observableArrayList(list);
        listViewSeccion.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //list view secciones con descuento...
        hashMapAsignacionNf1 = new HashMap<>();
        hashMapAsignacionNf2 = new HashMap<>();
        hashMapAsignacionNf3 = new HashMap<>();
        hashMapAsignacionNf4 = new HashMap<>();
        hashMapAsignacionNf5 = new HashMap<>();
        hashMapAsignacionNf6 = new HashMap<>();
        hashMapAsignacionNf7 = new HashMap<>();
        hashMapNf1Id = new HashMap<>();
        hashMapNf2Id = new HashMap<>();
        hashMapNf3Id = new HashMap<>();
        hashMapNf4Id = new HashMap<>();
        hashMapNf5Id = new HashMap<>();
        hashMapNf6Id = new HashMap<>();
        hashMapNf7Id = new HashMap<>();
        DescuentoTarjetaFXMLController.setScreenScopedTarjSec(false);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/descuento/DescuentoTarjetaFXML.fxml", 1268, 806, "/vista/descuento/DescuentoTarjetaSeccionFXML.fxml", 982, 677, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == KeyCode.ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void asignando() {
        Integer nf = 0;
        String descripcionNivel = "";
        Long idNf = 0l;
        if (treeViewSecciones.getSelectionModel().getSelectedItem() != null) {
            if (hashMapNf1.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 1;
                descripcionNivel = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf1Tipo();
            } else if (hashMapNf2.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 2;
                descripcionNivel = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf2Sfamilia();
            } else if (hashMapNf3.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 3;
                descripcionNivel = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf3Sseccion();
            } else if (hashMapNf4.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 4;
                descripcionNivel = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf4Seccion1();
            } else if (hashMapNf5.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 5;
                descripcionNivel = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf5Seccion2();
            } else if (hashMapNf6.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 6;
                descripcionNivel = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf6Secnom6();
            } else if (hashMapNf7.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 7;
                descripcionNivel = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf7Secnom7();
            }
        }
        if (nf != 0) {
            boolean asignado = false;
            switch (nf) {
                case 1:
                    if (hashMapAsignacionNf1.containsKey(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
                        Label labelDescripcionNf1 = new Label(descripcionNivel);
                        labelDescripcionNf1.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf1);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf1.put(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf1Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 2:
                    if (hashMapAsignacionNf2.containsKey(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png"))));
                        Label labelDescripcionNf2 = new Label(descripcionNivel);
                        labelDescripcionNf2.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf2);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf2.put(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf2Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 3:
                    if (hashMapAsignacionNf3.containsKey(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png"))));
                        Label labelDescripcionNf3 = new Label(descripcionNivel);
                        labelDescripcionNf3.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf3);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf3.put(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf3Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 4:
                    if (hashMapAsignacionNf4.containsKey(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png"))));
                        Label labelDescripcionNf4 = new Label(descripcionNivel);
                        labelDescripcionNf4.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf4);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf4.put(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf4Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 5:
                    if (hashMapAsignacionNf5.containsKey(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png"))));
                        Label labelDescripcionNf5 = new Label(descripcionNivel);
                        labelDescripcionNf5.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf5);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf5.put(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf5Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 6:
                    if (hashMapAsignacionNf6.containsKey(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png"))));
                        Label labelDescripcionNf6 = new Label(descripcionNivel);
                        labelDescripcionNf6.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf6);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf6.put(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf6Id.put(idNf, descripcionNivel);
                    }
                    break;
                case 7:
                    if (hashMapAsignacionNf7.containsKey(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                        asignado = true;
                    } else {
                        HBox hBox = new HBox();
                        hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png"))));
                        Label labelDescripcionNf7 = new Label(descripcionNivel);
                        labelDescripcionNf7.setStyle("-fx-text-fill: black; "
                                + "-fx-font-size: 12px; "
                                + "-fx-font-weight: bold;");
                        hBox.getChildren().add(labelDescripcionNf7);
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                        hBox.getChildren().add(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                        hBox.setSpacing(6);
                        seccionesAsignadas.addAll(hBox);
                        listViewSeccion.setItems(seccionesAsignadas);
                        hashMapAsignacionNf7.put(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                        hashMapNf7Id.put(idNf, descripcionNivel);
                    }
                    break;
                default:
                    asignado = false;
                    break;
            }
            if (asignado) {
                mensajeAdv("LA SECCIÓN [ " + descripcionNivel.toUpperCase() + " ] DEL NIVEL [ " + nf + "]\n"
                        + "YA HA SIDO ASIGNADA.");
            }
        } else {
            mensajeAdv("ASEGURESE DE SELECCIONAR UNA SECCIÓN ANTES DE AGREGAR.");
        }

    }

    private void asignandoTodos() {
        seccionesAsignadas.removeAll(seccionesAsignadas);
        listViewSeccion.setItems(seccionesAsignadas);
        hashMapNf1Id = new HashMap<>();
        hashMapNf2Id = new HashMap<>();
        hashMapNf3Id = new HashMap<>();
        hashMapNf4Id = new HashMap<>();
        hashMapNf5Id = new HashMap<>();
        hashMapNf6Id = new HashMap<>();
        hashMapNf7Id = new HashMap<>();
        for (Map.Entry<ImageView, Nf1Tipo> hmJsonNf1 : hashMapNf1.entrySet()) {
            HBox hBox = new HBox();
            hBox.alignmentProperty().set(Pos.CENTER_LEFT);
            hBox.getChildren().add(new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
            Label labelDescripcionNf1 = new Label(hmJsonNf1.getValue().getDescripcion());
            labelDescripcionNf1.setStyle("-fx-text-fill: black; "
                    + "-fx-font-size: 12px; "
                    + "-fx-font-weight: bold;");
            hBox.getChildren().add(labelDescripcionNf1);
            hBox.getChildren().add(new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
            hBox.getChildren().add(new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
            hBox.setSpacing(6);
            seccionesAsignadas.addAll(hBox);
            hashMapAsignacionNf1.put(hmJsonNf1.getValue(), hBox);
            hashMapNf1Id.put(hmJsonNf1.getValue().getIdNf1Tipo(), hmJsonNf1.getValue().getDescripcion());
        }
        listViewSeccion.setItems(seccionesAsignadas);
    }

    private void quitando() {
        boolean verificado = false;
        int cantSeleccionados = listViewSeccion.getSelectionModel().getSelectedItems().size();
        if (cantSeleccionados != 0) {
            for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                for (Map.Entry<Nf1Tipo, HBox> hmAsigNf1 : hashMapAsignacionNf1.entrySet()) {
                    //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                    //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                    if (hmAsigNf1.getValue() == selectedItem) {
                        hashMapNf1Id.remove(hmAsigNf1.getKey().getIdNf1Tipo());
                        hashMapAsignacionNf1.remove(hmAsigNf1.getKey());
                        if (--cantSeleccionados == 0) {
                            verificado = true;
                            break;
                        }
                    }
                }
                if (cantSeleccionados == 0) {
                    break;
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf2Sfamilia, HBox> hmAsigNf2 : hashMapAsignacionNf2.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf2.getValue() == selectedItem) {
                            hashMapNf2Id.remove(hmAsigNf2.getKey().getIdNf2Sfamilia());
                            hashMapAsignacionNf2.remove(hmAsigNf2.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf3Sseccion, HBox> hmAsigNf3 : hashMapAsignacionNf3.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf3.getValue() == selectedItem) {
                            hashMapNf3Id.remove(hmAsigNf3.getKey().getIdNf3Sseccion());
                            hashMapAsignacionNf3.remove(hmAsigNf3.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf4Seccion1, HBox> hmAsigNf4 : hashMapAsignacionNf4.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf4.getValue() == selectedItem) {
                            hashMapNf4Id.remove(hmAsigNf4.getKey().getIdNf4Seccion1());
                            hashMapAsignacionNf4.remove(hmAsigNf4.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf5Seccion2, HBox> hmAsigNf5 : hashMapAsignacionNf5.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf5.getValue() == selectedItem) {
                            hashMapNf5Id.remove(hmAsigNf5.getKey().getIdNf5Seccion2());
                            hashMapAsignacionNf5.remove(hmAsigNf5.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf6Secnom6, HBox> hmAsigNf6 : hashMapAsignacionNf6.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf6.getValue() == selectedItem) {
                            hashMapNf6Id.remove(hmAsigNf6.getKey().getIdNf6Secnom6());
                            hashMapAsignacionNf6.remove(hmAsigNf6.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf7Secnom7, HBox> hmAsigNf7 : hashMapAsignacionNf7.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf7.getValue() == selectedItem) {
                            hashMapNf7Id.remove(hmAsigNf7.getKey().getIdNf7Secnom7());
                            hashMapAsignacionNf7.remove(hmAsigNf7.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
        }
        if (!listViewSeccion.getSelectionModel().getSelectedItems().isEmpty()) {
            seccionesAsignadas.removeAll(listViewSeccion.getSelectionModel().getSelectedItems());
            listViewSeccion.setItems(seccionesAsignadas);
        }
    }

    private void quitandoTodos() {
        seccionesAsignadas.removeAll(seccionesAsignadas);
        listViewSeccion.setItems(seccionesAsignadas);
        hashMapAsignacionNf1.clear();
        hashMapAsignacionNf2.clear();
        hashMapAsignacionNf3.clear();
        hashMapAsignacionNf4.clear();
        hashMapAsignacionNf5.clear();
        hashMapAsignacionNf6.clear();
        hashMapAsignacionNf7.clear();
        hashMapNf1Id = new HashMap<>();
        hashMapNf2Id = new HashMap<>();
        hashMapNf3Id = new HashMap<>();
        hashMapNf4Id = new HashMap<>();
        hashMapNf5Id = new HashMap<>();
        hashMapNf6Id = new HashMap<>();
        hashMapNf7Id = new HashMap<>();
    }

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, NF
    private void generarNivelFamiliaLocal() {
        TreeItem<String> treeItemRoot = new TreeItem<>("ÁRBOL DE SECCIONES");
        treeItemRoot.setExpanded(true);
        //y sí... mucho recurso...
        hashMapNf1 = new HashMap<>();
        hashMapNf2 = new HashMap<>();
        hashMapNf3 = new HashMap<>();
        hashMapNf4 = new HashMap<>();
        hashMapNf5 = new HashMap<>();
        hashMapNf6 = new HashMap<>();
        hashMapNf7 = new HashMap<>();
        hashMapDetalleNf1 = new HashMap<>();
        hashMapDetalleNf2 = new HashMap<>();
        hashMapDetalleNf3 = new HashMap<>();
        hashMapDetalleNf4 = new HashMap<>();
        hashMapDetalleNf5 = new HashMap<>();
        hashMapDetalleNf6 = new HashMap<>();
        hashMapDetalleNf7 = new HashMap<>();
        //la descripción e id, pueden coincidir en niveles distintos a la hora de mapear... 
        //si se instancia una vez y se setea a todos los items del treeview con el mismo objeto del tipo ImageView
        //no funcionará correctamente
        //se puede rastrear también con una concatenación id + descripción, como nombre del item, pero estéticamente no queda muy bien
        for (Nf1Tipo nf1Tipo : nf1TipoDAO.listar()) {
            ImageView imageViewNf1 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
            hashMapNf1.put(imageViewNf1, nf1Tipo);//rastreo para inserción
            hashMapDetalleNf1.put(nf1Tipo.getIdNf1Tipo(), nf1Tipo);
            TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), imageViewNf1);
            for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
                ImageView imageViewNf2 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
                hashMapNf2.put(imageViewNf2, nf2Sfamilia);
                hashMapDetalleNf2.put(nf2Sfamilia.getIdNf2Sfamilia(), nf2Sfamilia);
                TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), imageViewNf2);
                for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                    ImageView imageViewNf3 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                    hashMapNf3.put(imageViewNf3, nf3Sseccion);
                    hashMapDetalleNf3.put(nf3Sseccion.getIdNf3Sseccion(), nf3Sseccion);
                    TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), imageViewNf3);
                    for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                        ImageView imageViewNf4 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                        hashMapNf4.put(imageViewNf4, nf4Seccion1);
                        hashMapDetalleNf4.put(nf4Seccion1.getIdNf4Seccion1(), nf4Seccion1);
                        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), imageViewNf4);
                        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                            ImageView imageViewNf5 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                            hashMapNf5.put(imageViewNf5, nf5Seccion2);
                            hashMapDetalleNf5.put(nf5Seccion2.getIdNf5Seccion2(), nf5Seccion2);
                            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), imageViewNf5);
                            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                                ImageView imageViewNf6 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                                hashMapNf6.put(imageViewNf6, nf6Secnom6);
                                hashMapDetalleNf6.put(nf6Secnom6.getIdNf6Secnom6(), nf6Secnom6);
                                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), imageViewNf6);
                                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                    ImageView imageViewNf7 = new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                    hashMapNf7.put(imageViewNf7, nf7Secnom7);
                                    hashMapDetalleNf7.put(nf7Secnom7.getIdNf7Secnom7(), nf7Secnom7);
                                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), imageViewNf7);
                                    treeItemNf6.getChildren().add(treeItemNf7);
                                }
                                treeItemNf5.getChildren().add(treeItemNf6);
                            }
                            treeItemNf4.getChildren().add(treeItemNf5);
                        }
                        treeItemNf3.getChildren().add(treeItemNf4);
                    }
                    treeItemNf2.getChildren().add(treeItemNf3);
                }
                treeItemNf1.getChildren().add(treeItemNf2);
            }
            treeItemRoot.getChildren().add(treeItemNf1);
        }
        treeViewSecciones.setRoot(treeItemRoot);
    }
    //////READ, NF
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    public static HashMap<Long, String> getHashMapNf1Id() {
        return hashMapNf1Id;
    }

    public static void setHashMapNf1Id(HashMap<Long, String> aHashMapNfId) {
        hashMapNf1Id = aHashMapNfId;
    }

    public static HashMap<Long, String> getHashMapNf2Id() {
        return hashMapNf2Id;
    }

    public static void setHashMapNf2Id(HashMap<Long, String> aHashMapNf2Id) {
        hashMapNf2Id = aHashMapNf2Id;
    }

    public static HashMap<Long, String> getHashMapNf3Id() {
        return hashMapNf3Id;
    }

    public static void setHashMapNf3Id(HashMap<Long, String> aHashMapNf3Id) {
        hashMapNf3Id = aHashMapNf3Id;
    }

    public static HashMap<Long, String> getHashMapNf4Id() {
        return hashMapNf4Id;
    }

    public static void setHashMapNf4Id(HashMap<Long, String> aHashMapNf4Id) {
        hashMapNf4Id = aHashMapNf4Id;
    }

    public static HashMap<Long, String> getHashMapNf5Id() {
        return hashMapNf5Id;
    }

    public static void setHashMapNf5Id(HashMap<Long, String> aHashMapNf5Id) {
        hashMapNf5Id = aHashMapNf5Id;
    }

    public static HashMap<Long, String> getHashMapNf6Id() {
        return hashMapNf6Id;
    }

    public static void setHashMapNf6Id(HashMap<Long, String> aHashMapNf6Id) {
        hashMapNf6Id = aHashMapNf6Id;
    }

    public static HashMap<Long, String> getHashMapNf7Id() {
        return hashMapNf7Id;
    }

    public static void setHashMapNf7Id(HashMap<Long, String> aHashMapNf7Id) {
        hashMapNf7Id = aHashMapNf7Id;
    }

}
