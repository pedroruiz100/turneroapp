package com.javafx.controllers.descuento;

import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.PromoTemporada;
import com.peluqueria.core.domain.PromoTemporadaNf1;
import com.peluqueria.core.domain.PromoTemporadaNf2;
import com.peluqueria.core.domain.PromoTemporadaNf3;
import com.peluqueria.core.domain.PromoTemporadaNf4;
import com.peluqueria.core.domain.PromoTemporadaNf5;
import com.peluqueria.core.domain.PromoTemporadaNf6;
import com.peluqueria.core.domain.PromoTemporadaNf7;
import com.peluqueria.dao.Nf1TipoDAO;
import com.peluqueria.dto.PromoTemporadaDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Pagination;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.PromoTemporadaDAO;
import com.peluqueria.dao.PromoTemporadaNf1DAO;
import com.peluqueria.dao.PromoTemporadaNf2DAO;
import com.peluqueria.dao.PromoTemporadaNf3DAO;
import com.peluqueria.dao.PromoTemporadaNf4DAO;
import com.peluqueria.dao.PromoTemporadaNf5DAO;
import com.peluqueria.dao.PromoTemporadaNf6DAO;
import com.peluqueria.dao.PromoTemporadaNf7DAO;
import com.peluqueria.dao.RangoPromoTempDAO;
import com.peluqueria.dto.PromoTemporadaNf1DTO;
import com.peluqueria.dto.PromoTemporadaNf2DTO;
import com.peluqueria.dto.PromoTemporadaNf3DTO;
import com.peluqueria.dto.PromoTemporadaNf4DTO;
import com.peluqueria.dto.PromoTemporadaNf5DTO;
import com.peluqueria.dto.PromoTemporadaNf6DTO;
import com.peluqueria.dto.PromoTemporadaNf7DTO;
import com.javafx.util.Descuento;
import com.javafx.util.RemindTask;
import java.util.LinkedHashMap;
import java.util.Timer;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
public class PromocionTemporadaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private PromoTemporadaDAO promoTemporadaDAO;
    @Autowired
    private PromoTemporadaNf1DAO promoTemporadaNf1DAO;
    @Autowired
    private PromoTemporadaNf2DAO promoTemporadaNf2DAO;
    @Autowired
    private PromoTemporadaNf3DAO promoTemporadaNf3DAO;
    @Autowired
    private PromoTemporadaNf4DAO promoTemporadaNf4DAO;
    @Autowired
    private PromoTemporadaNf5DAO promoTemporadaNf5DAO;
    @Autowired
    private PromoTemporadaNf6DAO promoTemporadaNf6DAO;
    @Autowired
    private PromoTemporadaNf7DAO promoTemporadaNf7DAO;
    @Autowired
    private RangoPromoTempDAO rangoPromoDAO;
    @Autowired
    private Nf1TipoDAO nf1TipoDAO;

    //para inserción
    HashMap<ImageView, Nf1Tipo> hashMapNf1;
    HashMap<ImageView, Nf2Sfamilia> hashMapNf2;
    HashMap<ImageView, Nf3Sseccion> hashMapNf3;
    HashMap<ImageView, Nf4Seccion1> hashMapNf4;
    HashMap<ImageView, Nf5Seccion2> hashMapNf5;
    HashMap<ImageView, Nf6Secnom6> hashMapNf6;
    HashMap<ImageView, Nf7Secnom7> hashMapNf7;
    //para inserción
    //para consulta detalle
    HashMap<Long, Nf1Tipo> hashMapDetalleNf1;
    HashMap<Long, Nf2Sfamilia> hashMapDetalleNf2;
    HashMap<Long, Nf3Sseccion> hashMapDetalleNf3;
    HashMap<Long, Nf4Seccion1> hashMapDetalleNf4;
    HashMap<Long, Nf5Seccion2> hashMapDetalleNf5;
    HashMap<Long, Nf6Secnom6> hashMapDetalleNf6;
    HashMap<Long, Nf7Secnom7> hashMapDetalleNf7;
    //para consulta detalle
    //para asignación
    HashMap<Nf1Tipo, HBox> hashMapAsignacionNf1;
    HashMap<Nf2Sfamilia, HBox> hashMapAsignacionNf2;
    HashMap<Nf3Sseccion, HBox> hashMapAsignacionNf3;
    HashMap<Nf4Seccion1, HBox> hashMapAsignacionNf4;
    HashMap<Nf5Seccion2, HBox> hashMapAsignacionNf5;
    HashMap<Nf6Secnom6, HBox> hashMapAsignacionNf6;
    HashMap<Nf7Secnom7, HBox> hashMapAsignacionNf7;
    //para asignación
    //para visualizar en el formulario promoción temporada detalle
    private static HashMap<String, Object> hashMapFormularioDetalleChilds;
    //para visualizar en el formulario promoción temporada detalle

    Timer timer;
    Task copyWorker;
    private final String patternDateReadFecha = "dd/MMM/yyyy";
    private final String patternDateFechaFiltro = "yyyy-MM-dd";
    private static List<JSONObject> promTempDetJSONObjList = new ArrayList<>();
    private static JSONObject promTempCab = new JSONObject();
    public static JSONArray jsonArrayDetalles = new JSONArray();
    private static JSONArray jsonArrayChilds = new JSONArray();
    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoBaja;
    private boolean buscarTodosPromTemp;
    private List<JSONObject> seccionList;
    private boolean cargarSeccion;
    private JSONArray seccionJSONArray;
    private ObservableList<HBox> seccionesAsignadas;
    private LinkedHashMap<String, JSONObject> hashJsonCombo;
    private HashMap<String, String> hashMapComboAux;
    private JSONArray promTempJSONArray;
    private List<JSONObject> promTempList;
    //PAGINATION
    private int filaIniPromTemp, filaCantPromTemp, filaLimitPromTemp;
    private int pageCountPromTemp, pageActualPromTemp;
    private ObservableList<JSONObject> promTempData;
    private TableView<JSONObject> tablePromTemp;
    private TableColumn<JSONObject, String> columnPromTemp;
    private TableColumn<JSONObject, String> columnPromTempFechaIni;
    private TableColumn<JSONObject, String> columnPromTempFechaFin;
    private TableColumn<JSONObject, Boolean> columnAcciones;
    //PAGINATION
    //DESCUENTOS
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPanePromoTemp;
    @FXML
    private SplitPane splitPaneConfigFuncDesc;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private Label labelConfigPromTemporada;
    @FXML
    private AnchorPane anchorPaneListView;
    @FXML
    private Button buttonInsertar;
    @FXML
    private Label labelDescuento;
    @FXML
    private ChoiceBox<String> choiceBoxDescuento;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private Label labelNomPromTem;
    @FXML
    private TextField textFieldNomPromTem;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private VBox vBoxButtons;
    @FXML
    private Button buttonAgregar;
    @FXML
    private Button buttonAgregarTodos;
    @FXML
    private Button buttonQuitar;
    @FXML
    private Button buttonQuitarTodos;
    @FXML
    private GridPane gridPaneFecha;
    @FXML
    private Pagination paginationPromTemp;
    @FXML
    private Label labelFiltro;
    @FXML
    private HBox vBoxFiltro;
    @FXML
    private Label labelPromTemp;
    @FXML
    private TextField textFielFiltroPromTemp;
    @FXML
    private Button buttonBuscarPromTemp;
    @FXML
    private Button buttonTodos;
    @FXML
    private ListView<HBox> listViewSeccion;
    @FXML
    private Label labelFechaInicioFiltro;
    @FXML
    private DatePicker datePickerFechaInicioFiltro;
    @FXML
    private Label labelFechaFinFiltro;
    @FXML
    private DatePicker datePickerFechaFinFiltro;
    @FXML
    private HBox hBoxDescuento;
    @FXML
    private AnchorPane anchorPaneDisable;
    @FXML
    private Button buttonCancelarTodas;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private Label labelProgress;
    @FXML
    private SplitPane splitPaneTreeView;
    @FXML
    private AnchorPane anchorPaneTitleTreeView;
    @FXML
    private Label labelTreeView;
    @FXML
    private AnchorPane anchorPaneTreeView;
    @FXML
    private TreeView<String> treeViewSecciones;
    @FXML
    private CheckBox checkBoxSincro;
    @FXML
    private SplitPane splitPaneListView;
    @FXML
    private AnchorPane anchorPaneTitleListView;
    @FXML
    private Label labelListView;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoPromTemp();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonAgregarAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void buttonAgregarTodosAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void buttonQuitarAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void buttonQuitarTodosAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void buttonBuscarPromTempAction(ActionEvent event) {
        buscandoPromTempFiltro();
    }

    @FXML
    private void buttonTodosAction(ActionEvent event) {
        buscandoPromTempTodo();
    }

    @FXML
    private void anchorPanePromoTempKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonCancelarTodasAction(ActionEvent event) {
        cancelandoDescuentoPromTempTodo();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        generarNivelFamiliaLocal();
        ocultandoProgress();
        cargandoChoiceDescuento();
        choiceBoxDescuento.getSelectionModel().select(0);
        cargarSeccion = true;
        alert = false;
        exitoInsertarCab = false;
        exitoBaja = false;
        buscarTodosPromTemp = true;
        //list view secciones con descuento...
        ObservableList<HBox> list = FXCollections.observableArrayList();
        seccionesAsignadas = FXCollections.observableArrayList(list);
        listViewSeccion.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        //list view secciones con descuento...
        hashMapComboAux = new HashMap<>();
        hashMapAsignacionNf1 = new HashMap<>();
        hashMapAsignacionNf2 = new HashMap<>();
        hashMapAsignacionNf3 = new HashMap<>();
        hashMapAsignacionNf4 = new HashMap<>();
        hashMapAsignacionNf5 = new HashMap<>();
        hashMapAsignacionNf6 = new HashMap<>();
        hashMapAsignacionNf7 = new HashMap<>();
        preparandoDatePicker();
        primeraPaginacion();
        cargandoImagenListView();
        RemindTask.enviandoNodoCheckBox(checkBoxSincro);
        timer = new Timer();
        timer.schedule(new RemindTask(), 1000, 5000);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        timer.cancel();
        timer.purge();
        RemindTask.enviandoNodoCheckBox(null);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/descuento/PromocionTemporadaFXML.fxml", 982, 812, false);
    }

    private void viendoDetalleSeccion() {
        this.sc.loadScreen("/vista/descuento/PromocionTemporadaDetalleFXML.fxml", 860, 804, "/vista/descuento/PromocionTemporadaFXML.fxml", 982, 812, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoPromTemp();
        }
        if (keyCode == event.getCode().F2) {
            buscandoPromTempFiltro();
        }
        if (keyCode == event.getCode().F3) {
            buscandoPromTempTodo();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().F12) {
            cancelandoDescuentoPromTempTodo();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    private void cargandoImagenListView() {
//        listViewSeccion.setCellFactory(param -> new ListCell<HBox>() {
//            @Override
//            public void updateItem(HBox hBox, boolean empty) {
//                super.updateItem(hBox, empty);
//                if (empty) {
//                    setText(null);
//                    setGraphic(null);
//                } else {
////                    if (name.equals("RUBY")) {
////                        imageView.setImage(listOfImages[0]);
////                    } else if (name.equals("APPLE")) {
////                        imageView.setImage(listOfImages[1]);
////                    } else if (name.equals("VISTA")) {
////                        imageView.setImage(listOfImages[2]);
////                    } else if (name.equals("TWITTER")) {
////                        imageView.setImage(listOfImages[3]);
////                    }
////                    setText(name);
////                    setGraphic(new ImageView(
////                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
//                    setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//                    setGraphic(hBox);
//                }
//            }
//        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoChoiceDescuento() {
        //nada de loops y mod...
        choiceBoxDescuento.getItems().add("-");
        choiceBoxDescuento.getItems().add("5 %");
        choiceBoxDescuento.getItems().add("10 %");
        choiceBoxDescuento.getItems().add("15 %");
        choiceBoxDescuento.getItems().add("20 %");
        choiceBoxDescuento.getItems().add("25 %");
        choiceBoxDescuento.getItems().add("27 %");
        choiceBoxDescuento.getItems().add("30 %");
        choiceBoxDescuento.getItems().add("35 %");
        choiceBoxDescuento.getItems().add("40 %");
        choiceBoxDescuento.getItems().add("45 %");
        choiceBoxDescuento.getItems().add("50 %");
        choiceBoxDescuento.getItems().add("55 %");
        choiceBoxDescuento.getItems().add("60 %");
        choiceBoxDescuento.getItems().add("65 %");
        choiceBoxDescuento.getItems().add("70 %");
        choiceBoxDescuento.getSelectionModel().select(0);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaFin.setEditable(false);
        datePickerFechaInicioFiltro.setEditable(false);
        datePickerFechaFinFiltro.setEditable(false);
        datePickerFechaInicio.setOnAction(event -> {
            resetComboBoxListView();
        });
        datePickerFechaFin.setOnAction(event -> {
            resetComboBoxListView();
        });
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        long p = ChronoUnit.DAYS.between(
                                datePickerFechaInicio.getValue(), item
                        );
                        setTooltip(new Tooltip(
                                "Descuento Promoción Temporada por " + p + " días.")
                        );
                    }
                };
            }
        };
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private boolean verificacionFechaSeccion() {
        if (datePickerFechaInicio.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA INICIO, PARA OBTENER SECCIONES DISPONIBLES.");
            resetComboBoxListView();
            return false;
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA FIN, PARA OBTENER SECCIONES DISPONIBLES.");
            resetComboBoxListView();
            return false;
        } else if (datePickerFechaFin.getValue().isBefore(datePickerFechaInicio.getValue())) {
            mensajeError("LA FECHA FIN NO DEBE SER MENOR A FECHA INICIO.");
            resetComboBoxListView();
            return false;
        } else {
            return true;
        }
    }

    private String fechaParam(LocalDate fechaDesdeLD) {
        String filterFecha = "null";
        if (fechaDesdeLD != null) {
            Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
            filterFecha = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
        }
        return filterFecha;
    }

    private void insertandoPromTemp() {
        if (textFieldNomPromTem.getText().contentEquals("")) {
            mensajeError("DEBE ASIGNAR UN NOMBRE A PROMOCIÓN TEMPORADA.");
        } else if (listViewSeccion.getItems().isEmpty()) {
            mensajeError("DEBE AGREGAR UNA SECCIÓN COMO MÍNIMO.");
        } else if (datePickerFechaInicio.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA INICIO.");
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeError("DEBE ASIGNAR UNA FECHA FIN.");
        } else if (datePickerFechaFin.getValue().isBefore(datePickerFechaInicio.getValue())) {
            mensajeError("LA FECHA FIN NO DEBE SER MENOR A FECHA INICIO.");
        } else {
            procesandoInsercion();
        }
    }

    private void procesandoInsercion() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR PROMOCIÓN TEMPORADA "
                + textFieldNomPromTem.getText() + "?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
//            if ("guardar_prom_temp")) {
            creandoCabPromTemp();
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/descuento/PromocionTemporadaFXML.fxml", 982, 812, false);
//            }
        } else if (alert.getResult() == cancel) {
            alert.close();
        }
    }

    private void asignando() {
        Integer nf = 0;
        String descripcionNivel = "";
        Long idNf = 0l;
        if (treeViewSecciones.getSelectionModel().getSelectedItem() != null) {
            if (hashMapNf1.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 1;
                descripcionNivel = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf1Tipo();
            } else if (hashMapNf2.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 2;
                descripcionNivel = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf2Sfamilia();
            } else if (hashMapNf3.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 3;
                descripcionNivel = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf3Sseccion();
            } else if (hashMapNf4.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 4;
                descripcionNivel = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf4Seccion1();
            } else if (hashMapNf5.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 5;
                descripcionNivel = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf5Seccion2();
            } else if (hashMapNf6.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 6;
                descripcionNivel = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf6Secnom6();
            } else if (hashMapNf7.containsKey(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic())) {
                nf = 7;
                descripcionNivel = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getDescripcion();
                idNf = hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()).getIdNf7Secnom7();
            }
        }
        if (choiceBoxDescuento.getSelectionModel().isSelected(0)) {
            mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
        } else {
            if (datePickerFechaFin.getValue() == null) {
                mensajeError("DEBE ASIGNAR UN RANGO DE FECHAS.");
            } else {
                if (nf != 0) {
                    if (validacionInsercion(nf, idNf)) {
                        mensajeError("LA SECCIÓN [ " + descripcionNivel.toUpperCase() + " ] DEL NIVEL [ " + nf + "]\n"
                                + "ACTUALMENTE SE ENCUENTRA EN USO POR UNA PROMOCIÓN TEMPORADA, VERIFIQUE.");
                    } else {
                        boolean asignado = false;
                        switch (nf) {
                            case 1:
                                if (hashMapAsignacionNf1.containsKey(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
                                    Label labelDescripcionNf1 = new Label(descripcionNivel);
                                    labelDescripcionNf1.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf1 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf1.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf1.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf1);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf1);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf1.put(hashMapNf1.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 2:
                                if (hashMapAsignacionNf2.containsKey(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png"))));
                                    Label labelDescripcionNf2 = new Label(descripcionNivel);
                                    labelDescripcionNf2.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf2 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf2.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf2.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf2);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf2);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf2.put(hashMapNf2.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 3:
                                if (hashMapAsignacionNf3.containsKey(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png"))));
                                    Label labelDescripcionNf3 = new Label(descripcionNivel);
                                    labelDescripcionNf3.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf3 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf3.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf3.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf3);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf3);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf3.put(hashMapNf3.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 4:
                                if (hashMapAsignacionNf4.containsKey(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png"))));
                                    Label labelDescripcionNf4 = new Label(descripcionNivel);
                                    labelDescripcionNf4.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf4 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf4.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf4.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf4);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf4);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf4.put(hashMapNf4.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 5:
                                if (hashMapAsignacionNf5.containsKey(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png"))));
                                    Label labelDescripcionNf5 = new Label(descripcionNivel);
                                    labelDescripcionNf5.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf5 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf5.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf5.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf5);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf5);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf5.put(hashMapNf5.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 6:
                                if (hashMapAsignacionNf6.containsKey(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png"))));
                                    Label labelDescripcionNf6 = new Label(descripcionNivel);
                                    labelDescripcionNf6.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf6 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf6.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf6.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf6);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf6);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf6.put(hashMapNf6.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            case 7:
                                if (hashMapAsignacionNf7.containsKey(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()))) {
                                    asignado = true;
                                } else {
                                    HBox hBox = new HBox();
                                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png"))));
                                    Label labelDescripcionNf7 = new Label(descripcionNivel);
                                    labelDescripcionNf7.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                                    Label labelPorcentajeNf7 = new Label(arrayPorcentaje[0]);
                                    labelPorcentajeNf7.setStyle("-fx-text-fill: black; "
                                            + "-fx-font-size: 12px; "
                                            + "-fx-font-weight: bold;");
                                    labelPorcentajeNf7.setEffect(new DropShadow(5, Color.GOLD));
                                    hBox.getChildren().add(labelDescripcionNf7);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                                    hBox.getChildren().add(labelPorcentajeNf7);
                                    hBox.getChildren().add(new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                                    hBox.setSpacing(6);
                                    seccionesAsignadas.addAll(hBox);
                                    listViewSeccion.setItems(seccionesAsignadas);
                                    hashMapAsignacionNf7.put(hashMapNf7.get(treeViewSecciones.getSelectionModel().getSelectedItem().getGraphic()), hBox);
                                }
                                break;
                            default:
                                asignado = false;
                                break;
                        }
                        if (!asignado) {
                            if (!listViewSeccion.getItems().isEmpty()) {
                                datePickerFechaFin.setDisable(true);
                                datePickerFechaInicio.setDisable(true);
                            }
                        } else {
                            mensajeError("LA SECCIÓN [ " + descripcionNivel.toUpperCase() + " ] DEL NIVEL [ " + nf + "]\n"
                                    + "YA HA SIDO ASIGNADA.");
                        }
                    }
                } else {
                    mensajeError("ASEGURESE DE SELECCIONAR UNA SECCIÓN ANTES DE AGREGAR.");
                }
            }
        }
    }

    private void asignandoTodos() {
        if (choiceBoxDescuento.getSelectionModel().isSelected(0)) {
            mensajeError("DEBE SELECCIONAR UN PORCENTAJE DE DESCUENTO.");
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeError("DEBE ASIGNAR UN RANGO DE FECHAS.");
        } else {
            boolean existe = false;
            for (Map.Entry<ImageView, Nf1Tipo> hmJsonNf1 : hashMapNf1.entrySet()) {
                if (validacionInsercion(1, hmJsonNf1.getValue().getIdNf1Tipo())) {
                    existe = true;
                }
            }
            if (existe) {
                mensajeError("EL NIVEL UNO DEL ÁRBOL DE SECCIONES, NO ESTÁ TOTALMENTE DISPONIBLE\n"
                        + "VERIFIQUE QUE NO ESTÉ EN USO, ALGUNA SECCIÓN DEL NIVEL UNO POR UNA PROMOCIÓN TEMPORADA.");
            } else {
                seccionesAsignadas.removeAll(seccionesAsignadas);
                listViewSeccion.setItems(seccionesAsignadas);
                for (Map.Entry<ImageView, Nf1Tipo> hmJsonNf1 : hashMapNf1.entrySet()) {
                    HBox hBox = new HBox();
                    hBox.alignmentProperty().set(Pos.CENTER_LEFT);
                    hBox.getChildren().add(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png"))));
                    Label labelDescripcionNf1 = new Label(hmJsonNf1.getValue().getDescripcion());
                    labelDescripcionNf1.setStyle("-fx-text-fill: black; "
                            + "-fx-font-size: 12px; "
                            + "-fx-font-weight: bold;");
                    String arrayPorcentaje[] = choiceBoxDescuento.getSelectionModel().getSelectedItem().split(" %");
                    Label labelPorcentajeNf1 = new Label(arrayPorcentaje[0]);
                    labelPorcentajeNf1.setStyle("-fx-text-fill: black; "
                            + "-fx-font-size: 12px; "
                            + "-fx-font-weight: bold;");
                    labelPorcentajeNf1.setEffect(new DropShadow(5, Color.GOLD));
                    hBox.getChildren().add(labelDescripcionNf1);
                    hBox.getChildren().add(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))));
                    hBox.getChildren().add(labelPorcentajeNf1);
                    hBox.getChildren().add(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Ecommerce-Sale-icon.png"))));
                    hBox.setSpacing(6);
                    seccionesAsignadas.addAll(hBox);
                    hashMapAsignacionNf1.put(hmJsonNf1.getValue(), hBox);
                }
                listViewSeccion.setItems(seccionesAsignadas);
            }
            if (!listViewSeccion.getItems().isEmpty()) {
                datePickerFechaFin.setDisable(true);
                datePickerFechaInicio.setDisable(true);
            }
        }
    }

    private void quitando() {
        boolean verificado = false;
        int cantSeleccionados = listViewSeccion.getSelectionModel().getSelectedItems().size();
        if (cantSeleccionados != 0) {
            for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                for (Map.Entry<Nf1Tipo, HBox> hmAsigNf1 : hashMapAsignacionNf1.entrySet()) {
                    //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                    //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                    if (hmAsigNf1.getValue() == selectedItem) {
                        hashMapAsignacionNf1.remove(hmAsigNf1.getKey());
                        if (--cantSeleccionados == 0) {
                            verificado = true;
                            break;
                        }
                    }
                }
                if (cantSeleccionados == 0) {
                    break;
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf2Sfamilia, HBox> hmAsigNf2 : hashMapAsignacionNf2.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf2.getValue() == selectedItem) {
                            hashMapAsignacionNf2.remove(hmAsigNf2.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf3Sseccion, HBox> hmAsigNf3 : hashMapAsignacionNf3.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf3.getValue() == selectedItem) {
                            hashMapAsignacionNf3.remove(hmAsigNf3.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf4Seccion1, HBox> hmAsigNf4 : hashMapAsignacionNf4.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf4.getValue() == selectedItem) {
                            hashMapAsignacionNf4.remove(hmAsigNf4.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf5Seccion2, HBox> hmAsigNf5 : hashMapAsignacionNf5.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf5.getValue() == selectedItem) {
                            hashMapAsignacionNf5.remove(hmAsigNf5.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf6Secnom6, HBox> hmAsigNf6 : hashMapAsignacionNf6.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf6.getValue() == selectedItem) {
                            hashMapAsignacionNf6.remove(hmAsigNf6.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
            if (!verificado) {
                for (HBox selectedItem : listViewSeccion.getSelectionModel().getSelectedItems()) {
                    for (Map.Entry<Nf7Secnom7, HBox> hmAsigNf7 : hashMapAsignacionNf7.entrySet()) {
                        //Map -> HBox@282d947b Selected -> [HBox@6663f816, HBox@282d947b] 
                        //al ser multiplechoice, puede quitar de asignados más de uno, por ende un bucle más...
                        if (hmAsigNf7.getValue() == selectedItem) {
                            hashMapAsignacionNf7.remove(hmAsigNf7.getKey());
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
            }
        }
        if (listViewSeccion.getSelectionModel().getSelectedItems().size() != 0) {
            seccionesAsignadas.removeAll(listViewSeccion.getSelectionModel().getSelectedItems());
            listViewSeccion.setItems(seccionesAsignadas);
        }
        if (listViewSeccion.getItems().isEmpty()) {
            datePickerFechaFin.setDisable(false);
            datePickerFechaInicio.setDisable(false);
        }
    }

    private void quitandoTodos() {
        seccionesAsignadas.removeAll(seccionesAsignadas);
        listViewSeccion.setItems(seccionesAsignadas);
        hashMapAsignacionNf1.clear();
        hashMapAsignacionNf2.clear();
        hashMapAsignacionNf3.clear();
        hashMapAsignacionNf4.clear();
        hashMapAsignacionNf5.clear();
        hashMapAsignacionNf6.clear();
        hashMapAsignacionNf7.clear();
        if (listViewSeccion.getItems().isEmpty()) {
            datePickerFechaFin.setDisable(false);
            datePickerFechaInicio.setDisable(false);
        }
    }

    private void limpiandoCampos() {
        textFieldNomPromTem.setText("");
        quitandoTodos();
        datePickerFechaInicio.setValue(LocalDate.now());
        datePickerFechaFin.setValue(null);
        choiceBoxDescuento.getSelectionModel().select(0);
    }

    private void resetComboBoxListView() {
        cargarSeccion = true;
        ObservableList<String> list = FXCollections.observableArrayList();
//        comboBoxSeccion.valueProperty().set(null);
//        comboBoxSeccion.setItems(list);
//        comboBoxSeccion.getSelectionModel().clearSelection();
//        comboBoxSeccion.getProperties().clear();
//        comboBoxSeccion.getEditor().appendText("");
//        comboBoxSeccion.setPromptText("Filtro Sección...");
        hashMapComboAux = new HashMap<>();
//        new AutoCompleteComboBoxListener<>(comboBoxSeccion);
    }

    private void resetPromTempBaja() {
        exitoBaja = false;
        primeraPaginacion();
        quitandoTodos();
    }

    private void limpiandoBusqueda() {
        textFielFiltroPromTemp.setText("");
        datePickerFechaInicioFiltro.setValue(null);
        datePickerFechaFinFiltro.setValue(null);
    }

    private void cancelandoDescuentoPromTempTodo() {
//        if ("baja_prom_temp")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR TODOS LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA?", ok, cancel);
        alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            if (bajaDescPromTempTodo()) {
                exitoBaja = false;
            }
            alert2.close();
        } else if (alert2.getResult() == cancel) {
            alert2.close();
        }
//        } else {
//            mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//        }
    }

    public void viendoProgress() {
        anchorPaneListView.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        paginationPromTemp.setDisable(true);
        anchorPaneBottom.setDisable(true);
        buttonLimpiar.setDisable(true);
        vBoxFiltro.setDisable(true);
        buttonCancelarTodas.setDisable(true);
        anchorPaneListView.setDisable(true);
        progressIndicator.setVisible(true);
        labelProgress.setVisible(true);
    }

    public void ocultandoProgress() {
        anchorPaneListView.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        paginationPromTemp.setDisable(false);
        anchorPaneBottom.setDisable(false);
        buttonLimpiar.setDisable(false);
        vBoxFiltro.setDisable(false);
        buttonCancelarTodas.setDisable(false);
        anchorPaneListView.setDisable(false);
        progressIndicator.setVisible(false);
        labelProgress.setVisible(false);
    }

    private Integer jsonRowCount() {
        Integer rowCount = 0;
        rowCount = generarCountPromoTemp();
        return rowCount;
    }

    private Integer jsonRowCountFilter(String promTempFilter, String fechaDesde, String fechaHasta) {
        Integer rowCountFilter = 0;
        rowCountFilter = generarCountFilterPromoTempLocal(promTempFilter, fechaDesde, fechaHasta);
        return rowCountFilter;
    }

//    private void jsonCargandoSeccion() {
//        if (cargarSeccion) {
//            seccionJSONArray = null;
//            String fechaIniParam = fechaParam(datePickerFechaInicio.getValue());
//            String fechaFinParam = fechaParam(datePickerFechaFin.getValue());
//            seccionJSONArray = generarSeccionLocal(fechaIniParam, fechaFinParam);
//            cargarSeccion = false;
//            seccionList = new ArrayList<>();
//            hashJsonCombo = new LinkedHashMap<>();
//            for (Object obj : seccionJSONArray) {
//                JSONObject seccion = (JSONObject) obj;
//                seccionList.add(seccion);
////                comboBoxSeccion.getItems().add(seccion.get("descripcion").toString());
//                hashJsonCombo.put(seccion.get("descripcion").toString(), seccion);
//            }
////            new AutoCompleteComboBoxListener<>(comboBoxSeccion);
//        }
//    }
    private List<JSONObject> jsonArrayPromTempFetch(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> promTempJSONObjList = new ArrayList<>();
        promTempJSONArray = generarFetchPromoTemp(limRowS, offSetS);
        for (Object promTempObj : promTempJSONArray) {
            JSONObject promTempJSONObj = (JSONObject) promTempObj;
            promTempJSONObjList.add(promTempJSONObj);
        }
        return promTempJSONObjList;
    }

    private List<JSONObject> jsonArrayPromTempFetchFiltro(int limRow, int offSet, String promTempFiltro, String fechaDesde, String fechaHasta) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        promTempJSONArray = generarFetchFiltroPromoTempLocal(limRowS, offSetS, promTempFiltro, fechaDesde, fechaHasta);
        for (Object cabFielObj : promTempJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private Map recuperarFechaLong(Long id) {
        Map mapeo = new HashMap();
        PromoTemporada desc = promoTemporadaDAO.getById(id);
        mapeo.put("fechaAlta", desc.getFechaAlta().getTime());
        mapeo.put("fechaMod", desc.getFechaMod().getTime());
        mapeo.put("fechaInicio", desc.getFechaInicio().getTime());
        mapeo.put("fechaFin", desc.getFechaFin().getTime());
        return mapeo;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, PROMO TEMP. -> POST
    private boolean creandoCabPromTemp() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject cabPromTemp = new JSONObject();
        try {
            cabPromTemp = creandoJsonPromTemp();
            CajaDatos.setIdPromoTemporada(rangoPromoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
            cabPromTemp.put("idTemporada", CajaDatos.getIdPromoTemporada());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporada");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(cabPromTemp.toString());
                System.out.println("PROMO TEMPORADA ->> " + cabPromTemp.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        cabPromTemp = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setDescuentoPromo(true);
                        exitoInsertarCab = true;
                    }
                    br.close();
                } else {
                    cabPromTemp = registrarPromoTemporadaLocal(cabPromTemp);
                }
            } else {
                cabPromTemp = registrarPromoTemporadaLocal(cabPromTemp);
            }
        } catch (IOException | ParseException ex) {
            cabPromTemp = registrarPromoTemporadaLocal(cabPromTemp);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoInsertarCab) {
            //INSERTAR EN food_local HABIENDO O NO CONEXION
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            long longInicio = Long.parseLong(cabPromTemp.get("fechaInicio").toString());
            long longFin = Long.parseLong(cabPromTemp.get("fechaFin").toString());
            cabPromTemp.put("fechaAlta", null);
            cabPromTemp.put("fechaMod", null);
            cabPromTemp.put("fechaInicio", null);
            cabPromTemp.put("fechaFin", null);
            PromoTemporadaDTO promoDTO = gson.fromJson(cabPromTemp.toString(), PromoTemporadaDTO.class);
            promoDTO.setFechaAlta(timestamp);
            promoDTO.setFechaMod(timestamp);
            promoDTO.setFechaInicio(new Timestamp(longInicio));
            promoDTO.setFechaFin(new Timestamp(longFin));
            if (promoTemporadaDAO.insertarObtenerObjeto(PromoTemporada.fromPromoTemporadaDTO(promoDTO)) != null) {
                System.out.println("DATOS PERSISTIDOS EN POSTGRES LOCAL PROMO TEMPORADA CABECERA");
            } else {
                System.out.println("DATOS NO...!! PERSISTIDOS EN POSTGRESQL LOCAL PROMO TEMPORADA CABECERA");
            } //CIERRE INSERCION food_local
            JSONArray jsonArrayPromTempDet = new JSONArray();
            int cantSeleccionados = seccionesAsignadas.size();
            boolean verificado = false;
            for (int i = 0; i < seccionesAsignadas.size(); i++) {
                for (Map.Entry<Nf1Tipo, HBox> hmAsigNf1 : hashMapAsignacionNf1.entrySet()) {
                    if (hmAsigNf1.getValue() == seccionesAsignadas.get(i)) {
                        try {
                            Label labelPorcentaje = (Label) hmAsigNf1.getValue().getChildren().get(3);
                            JSONObject jsonNf1 = (JSONObject) parser.parse(gson.toJson(hmAsigNf1.getKey().toNf1TipoDTOEntitiesNull()));
                            jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf1, Integer.valueOf(labelPorcentaje.getText()), 1));
                            if (--cantSeleccionados == 0) {
                                verificado = true;
                                break;
                            }
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
                    }
                }
                if (cantSeleccionados == 0) {
                    break;
                }
                if (!verificado) {
                    for (Map.Entry<Nf2Sfamilia, HBox> hmAsigNf2 : hashMapAsignacionNf2.entrySet()) {
                        if (hmAsigNf2.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf2.getValue().getChildren().get(3);
                                JSONObject jsonNf2 = (JSONObject) parser.parse(gson.toJson(hmAsigNf2.getKey().toNf2SfamiliaDTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf2, Integer.valueOf(labelPorcentaje.getText()), 2));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                if (!verificado) {
                    for (Map.Entry<Nf3Sseccion, HBox> hmAsigNf3 : hashMapAsignacionNf3.entrySet()) {
                        if (hmAsigNf3.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf3.getValue().getChildren().get(3);
                                JSONObject jsonNf3 = (JSONObject) parser.parse(gson.toJson(hmAsigNf3.getKey().toNf3SseccionDTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf3, Integer.valueOf(labelPorcentaje.getText()), 3));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                if (!verificado) {
                    for (Map.Entry<Nf4Seccion1, HBox> hmAsigNf4 : hashMapAsignacionNf4.entrySet()) {
                        if (hmAsigNf4.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf4.getValue().getChildren().get(3);
                                JSONObject jsonNf4 = (JSONObject) parser.parse(gson.toJson(hmAsigNf4.getKey().toNf4Seccion1DTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf4, Integer.valueOf(labelPorcentaje.getText()), 4));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                if (!verificado) {
                    for (Map.Entry<Nf5Seccion2, HBox> hmAsigNf5 : hashMapAsignacionNf5.entrySet()) {
                        if (hmAsigNf5.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf5.getValue().getChildren().get(3);
                                JSONObject jsonNf5 = (JSONObject) parser.parse(gson.toJson(hmAsigNf5.getKey().toNf5Seccion2DTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf5, Integer.valueOf(labelPorcentaje.getText()), 5));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                if (!verificado) {
                    for (Map.Entry<Nf6Secnom6, HBox> hmAsigNf6 : hashMapAsignacionNf6.entrySet()) {
                        if (hmAsigNf6.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf6.getValue().getChildren().get(3);
                                JSONObject jsonNf6 = (JSONObject) parser.parse(gson.toJson(hmAsigNf6.getKey().toNf6Secnom6DTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf6, Integer.valueOf(labelPorcentaje.getText()), 6));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                if (!verificado) {
                    for (Map.Entry<Nf7Secnom7, HBox> hmAsigNf7 : hashMapAsignacionNf7.entrySet()) {
                        if (hmAsigNf7.getValue() == seccionesAsignadas.get(i)) {
                            try {
                                Label labelPorcentaje = (Label) hmAsigNf7.getValue().getChildren().get(3);
                                JSONObject jsonNf7 = (JSONObject) parser.parse(gson.toJson(hmAsigNf7.getKey().toNf7Secnom7DTOEntitiesNull()));
                                jsonArrayPromTempDet.add(creandoJsonDetPromTemp(cabPromTemp, jsonNf7, Integer.valueOf(labelPorcentaje.getText()), 7));
                                if (--cantSeleccionados == 0) {
                                    verificado = true;
                                    break;
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        }
                    }
                    if (cantSeleccionados == 0) {
                        break;
                    }
                }
                verificado = false;
            }
            copyWorker = createWorker();
            progressIndicator.progressProperty().unbind();
            progressIndicator.progressProperty().bind(copyWorker.progressProperty());
            new Thread(copyWorker).start();
            jsonArrayDetalles = jsonArrayPromTempDet;
        }
        return exitoInsertarCab;
    }
    //////CREATE, PROMO TEMP. -> POST

    //////CREATE, PROMO TEMP. Nf1 -> POST
    private boolean creandoDetPromTempNf1(JSONObject detPromTempNf1) {
        String inputLine;
        JSONParser parser = new JSONParser();
        System.out.println("#####\n" + detPromTempNf1.toJSONString() + "\n#####");
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf1");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf1 = detPromTempNf1;
                JSONObject jsonNf1Tipo = (JSONObject) jsonPromoTempNf1.get("nf1Tipo");
                jsonNf1Tipo.put("fechaAlta", null);
                jsonNf1Tipo.put("fechaMod", null);
                jsonPromoTempNf1.put("nf1Tipo", jsonNf1Tipo);
                wr.write(jsonPromoTempNf1.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf1, 1);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf1, 1);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf1, 1);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf1DTO promoTemporadaNf1DTO = gson.fromJson(detPromTempNf1.toString(), PromoTemporadaNf1DTO.class);
        PromoTemporadaNf1 promoTemporadaNf1 = PromoTemporadaNf1.fromPromoTemporadaNf1DTOEntitiesFull(promoTemporadaNf1DTO);
        if (promoTemporadaNf1DAO.insercionMasivaEstado(promoTemporadaNf1)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf1");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf1");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf1 -> POST

    //////CREATE, PROMO TEMP. Nf2 -> POST
    private boolean creandoDetPromTempNf2(JSONObject detPromTempNf2) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf2");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf2 = detPromTempNf2;
                JSONObject jsonNf2Sfamilia = (JSONObject) jsonPromoTempNf2.get("nf2Sfamilia");
                jsonNf2Sfamilia.put("fechaAlta", null);
                jsonNf2Sfamilia.put("fechaMod", null);
                jsonPromoTempNf2.put("nf2Sfamilia", jsonNf2Sfamilia);
                wr.write(jsonPromoTempNf2.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf2, 2);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf2, 2);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf2, 2);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf2DTO promoTemporadaNf2DTO = gson.fromJson(detPromTempNf2.toString(), PromoTemporadaNf2DTO.class);
        PromoTemporadaNf2 promoTemporadaNf2 = PromoTemporadaNf2.fromPromoTemporadaNf2DTOEntitiesFull(promoTemporadaNf2DTO);
        if (promoTemporadaNf2DAO.insercionMasivaEstado(promoTemporadaNf2)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf2");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf2");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf2 -> POST

    //////CREATE, PROMO TEMP. Nf3 -> POST
    private boolean creandoDetPromTempNf3(JSONObject detPromTempNf3) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf3");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf3 = detPromTempNf3;
                JSONObject jsonNf3Sseccion = (JSONObject) jsonPromoTempNf3.get("nf3Sseccion");
                jsonNf3Sseccion.put("fechaAlta", null);
                jsonNf3Sseccion.put("fechaMod", null);
                jsonPromoTempNf3.put("nf3Sseccion", jsonNf3Sseccion);
                wr.write(jsonPromoTempNf3.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf3, 3);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf3, 3);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf3, 3);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf3DTO promoTemporadaNf3DTO = gson.fromJson(detPromTempNf3.toString(), PromoTemporadaNf3DTO.class);
        PromoTemporadaNf3 promoTemporadaNf3 = PromoTemporadaNf3.fromPromoTemporadaNf3DTOEntitiesFull(promoTemporadaNf3DTO);
        if (promoTemporadaNf3DAO.insercionMasivaEstado(promoTemporadaNf3)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf3");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf3");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf3 -> POST

    //////CREATE, PROMO TEMP. Nf4 -> POST
    private boolean creandoDetPromTempNf4(JSONObject detPromTempNf4) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf4");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf4 = detPromTempNf4;
                JSONObject jsonNf4Seccion1 = (JSONObject) jsonPromoTempNf4.get("nf4Seccion1");
                jsonNf4Seccion1.put("fechaAlta", null);
                jsonNf4Seccion1.put("fechaMod", null);
                jsonPromoTempNf4.put("nf4Seccion1", jsonNf4Seccion1);
                wr.write(jsonPromoTempNf4.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf4, 4);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf4, 4);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf4, 4);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf4DTO promoTemporadaNf4DTO = gson.fromJson(detPromTempNf4.toString(), PromoTemporadaNf4DTO.class);
        PromoTemporadaNf4 promoTemporadaNf4 = PromoTemporadaNf4.fromPromoTemporadaNf4DTOEntitiesFull(promoTemporadaNf4DTO);
        if (promoTemporadaNf4DAO.insercionMasivaEstado(promoTemporadaNf4)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf4");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf4");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf4 -> POST

    //////CREATE, PROMO TEMP. Nf5 -> POST
    private boolean creandoDetPromTempNf5(JSONObject detPromTempNf5) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf5");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf5 = detPromTempNf5;
                JSONObject jsonNf5Seccion2 = (JSONObject) jsonPromoTempNf5.get("nf5Seccion2");
                jsonNf5Seccion2.put("fechaAlta", null);
                jsonNf5Seccion2.put("fechaMod", null);
                jsonPromoTempNf5.put("nf5Seccion2", jsonNf5Seccion2);
                wr.write(jsonPromoTempNf5.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf5, 5);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf5, 5);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf5, 5);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf5DTO promoTemporadaNf5DTO = gson.fromJson(detPromTempNf5.toString(), PromoTemporadaNf5DTO.class);
        PromoTemporadaNf5 promoTemporadaNf5 = PromoTemporadaNf5.fromPromoTemporadaNf5DTOEntitiesFull(promoTemporadaNf5DTO);
        if (promoTemporadaNf5DAO.insercionMasivaEstado(promoTemporadaNf5)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf5");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf5");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf5 -> POST

    //////CREATE, PROMO TEMP. Nf6 -> POST
    private boolean creandoDetPromTempNf6(JSONObject detPromTempNf6) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf6");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf6 = detPromTempNf6;
                JSONObject jsonNf6Secnom6 = (JSONObject) jsonPromoTempNf6.get("nf6Secnom6");
                jsonNf6Secnom6.put("fechaAlta", null);
                jsonNf6Secnom6.put("fechaMod", null);
                jsonPromoTempNf6.put("nf6Secnom6", jsonNf6Secnom6);
                wr.write(jsonPromoTempNf6.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf6, 6);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf6, 6);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf6, 6);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf6DTO promoTemporadaNf6DTO = gson.fromJson(detPromTempNf6.toString(), PromoTemporadaNf6DTO.class);
        PromoTemporadaNf6 promoTemporadaNf6 = PromoTemporadaNf6.fromPromoTemporadaNf6DTOEntitiesFull(promoTemporadaNf6DTO);
        if (promoTemporadaNf6DAO.insercionMasivaEstado(promoTemporadaNf6)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf6");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf6");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf6 -> POST

    //////CREATE, PROMO TEMP. Nf7 -> POST
    private boolean creandoDetPromTempNf7(JSONObject detPromTempNf7) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && CajaDatos.isDescuentoPromo() && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporadaNf7");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                JSONObject jsonPromoTempNf7 = detPromTempNf7;
                JSONObject jsonNf7Secnom7 = (JSONObject) jsonPromoTempNf7.get("nf7Secnom7");
                jsonNf7Secnom7.put("fechaAlta", null);
                jsonNf7Secnom7.put("fechaMod", null);
                jsonPromoTempNf7.put("nf7Secnom7", jsonNf7Secnom7);
                wr.write(jsonPromoTempNf7.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoInsertarDet = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoInsertarDet = registrarPromoTempLocal(detPromTempNf7, 7);
                }
            } else {
                exitoInsertarDet = registrarPromoTempLocal(detPromTempNf7, 7);
            }
        } catch (IOException | ParseException ex) {
            exitoInsertarDet = registrarPromoTempLocal(detPromTempNf7, 7);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //Para registrarlo de manera local ya que tanto en el servidor como de manera local debe existir este dato
        PromoTemporadaNf7DTO promoTemporadaNf7DTO = gson.fromJson(detPromTempNf7.toString(), PromoTemporadaNf7DTO.class);
        PromoTemporadaNf7 promoTemporadaNf7 = PromoTemporadaNf7.fromPromoTemporadaNf7DTOEntitiesFull(promoTemporadaNf7DTO);
        if (promoTemporadaNf7DAO.insercionMasivaEstado(promoTemporadaNf7)) {
            System.out.println("DATOS PERSISTIDOS EN PROMO TEMPORADA Nf7");
        } else {
            System.out.println("DATOS NO!.... PERSISTIDOS EN PROMO TEMPORADA Nf7");
        }
        return exitoInsertarDet;
    }
    //////CREATE, PROMO TEMP. Nf7 -> POST

    //////READ, PROMO TEMP. ID -> GET
    private JSONObject recuperarDatoPromoTemp(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject descuento = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporada/getById/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    descuento = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            }
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR IOException | ParseException: ", ex.fillInStackTrace());
            return descuento;
        }
        return descuento;
    }
    //////READ, PROMO TEMP. ID -> GET

    //////UPDATE, BAJA -> PUT
    private boolean bajaPromTemp(JSONObject promTemp) {
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            boolean status = false;
            if (recuperarDatoPromoTemp(Long.parseLong(promTemp.get("idTemporada").toString())) != null) {
                status = true;
            }
            Map mapeo = recuperarFechaLong(Long.valueOf(promTemp.get("idTemporada").toString()));
            promTemp.put("fechaInicio", mapeo.get("fechaInicio"));
            promTemp.put("fechaFin", mapeo.get("fechaFin"));
            promTemp.put("fechaAlta", mapeo.get("fechaAlta"));
            promTemp.put("fechaMod", mapeo.get("fechaMod"));
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && status && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporada");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(promTemp.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    br.close();
                } else {
                    exitoBaja = actualizarPromo(promTemp);
                }
            } else {
                exitoBaja = actualizarPromo(promTemp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = actualizarPromo(promTemp);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            java.sql.Timestamp fechaInicio = null;
            java.sql.Timestamp fechaFin = null;
            if (promTemp.get("fechaAlta") != null) {
                fechaAlta = new Timestamp(Long.parseLong(promTemp.get("fechaAlta").toString()));
                promTemp.put("fechaAlta", null);
            }
            if (promTemp.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(promTemp.get("fechaMod").toString()));
                promTemp.put("fechaMod", null);
            }
            if (promTemp.get("fechaInicio") != null) {
                fechaInicio = new Timestamp(Long.parseLong(promTemp.get("fechaInicio").toString()));
                promTemp.put("fechaInicio", null);
            }
            if (promTemp.get("fechaFin") != null) {
                fechaFin = new Timestamp(Long.parseLong(promTemp.get("fechaFin").toString()));
                promTemp.put("fechaFin", null);
            }
            PromoTemporadaDTO promoDTO = gson.fromJson(promTemp.toString(), PromoTemporadaDTO.class);
            promoDTO.setFechaAlta(fechaAlta);
            promoDTO.setFechaMod(fechaMod);
            promoDTO.setFechaInicio(fechaInicio);
            promoDTO.setFechaFin(fechaFin);
            promoDTO.setEstadoPromo(false);
            if (promoTemporadaDAO.actualizarObtenerEstado(PromoTemporada.fromPromoTemporadaDTOEntitiesNull(promoDTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡PROMOCIÓN POR TEMPORADA CANCELADA!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetPromTempBaja();
            } else {
                alert2.close();
                resetPromTempBaja();
            }
        } else {
            mensajeError("NO SE CANCELÓ LA PROMOCIÓN TEMPORADA.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA -> PUT

    //////UPDATE, BAJA MASIVA -> GET
    private boolean bajaDescPromTempTodo() {
        JSONParser parser = new JSONParser();
        String inputLine;
        String u = UtilLoaderBase.msjIda(Identity.getNomFun());
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        String ts = UtilLoaderBase.msjIda(Utilidades.getTSFormat().format(timestamp));
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/bajaMasiva/baja");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");
                conn.setRequestMethod("PUT");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(creandoJsonBajaDescMasivo(u, ts).toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        exitoBaja = (boolean) parser.parse(inputLine);
                    }
                    if (exitoBaja) {
                        if (promoTemporadaDAO.bajasLocal(Identity.getNomFun(), timestamp)) {
                            System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- ");
                        }
                    }
                    br.close();
                } else {
                    exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
                }
            } else {
                exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            }
        } catch (IOException | ParseException ex) {
            exitoBaja = recuperarBajavLocal(Identity.getNomFun(), timestamp);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (exitoBaja) {
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡TODOS LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA HAN SIDO CANCELADOS!", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetPromTempBaja();
            } else {
                alert2.close();
                resetPromTempBaja();
            }
        } else {
            mensajeError("LOS DESCUENTOS EN PROMOCIÓN POR TEMPORADA NO SE CANCELARON.");
        }
        return exitoBaja;
    }
    //////UPDATE, BAJA MASIVA -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, VALIDACIÓN INSERCIÓN
    private boolean validacionInsercion(int nf, long idNf) {
        return promoTemporadaDAO.validacionInsercion(nf, idNf);
    }
    //////READ, VALIDACIÓN INSERCIÓN

    //////READ, ROW COUNT PROMO TEMP.
    private Integer generarCountPromoTemp() {
        return (int) promoTemporadaDAO.rowCount();
    }
    //////READ, ROW COUNT PROMO TEMP.

    //////READ, ROW COUNT FILTER PROMO TEMP.
    private Integer generarCountFilterPromoTempLocal(String promTempFilter, String fechaDesde, String fechaHasta) {
        return (int) promoTemporadaDAO.rowCountFilter(promTempFilter, fechaDesde, fechaHasta);
    }
    //////READ, ROW COUNT FILTER PROMO TEMP.

    //////READ, FETCH PROMO TEMP.
    private JSONArray generarFetchPromoTemp(String limRowS, String offSetS) {
        JSONArray listaObj = new JSONArray();
        JSONParser parser = new JSONParser();
        for (PromoTemporada promoTemporada : promoTemporadaDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(promoTemporada.toPromoTemporadaDTOEntitiesFullFactNull()));
                listaObj.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return listaObj;
    }
    //////READ, FETCH PROMO TEMP.

    //////READ, FETCH FILTRO PROMO TEMP.
    private JSONArray generarFetchFiltroPromoTempLocal(String limRowS, String offSetS, String promTempFiltro, String fechaDesde, String fechaHasta) {
        JSONArray listaJSON = new JSONArray();
        JSONParser parser = new JSONParser();
        for (PromoTemporada promoTemporada : promoTemporadaDAO.listarFETCHFilter(Long.parseLong(limRowS), Long.parseLong(offSetS), promTempFiltro, fechaDesde, fechaHasta)) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(promoTemporada.toPromoTemporadaDTOEntitiesFullFactNull()));
                listaJSON.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return listaJSON;
    }
    //////READ, FETCH FILTRO PROMO TEMP.

    //////CREATE, PENDIENTES PROM TEMP.
    private JSONObject registrarPromoTemporadaLocal(JSONObject cabPromTemp) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada");
        j.put("data", cabPromTemp.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                obj = cabPromTemp;
                exitoInsertarCab = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////CREATE, PENDIENTES PROM TEMP. 

    //////UPDATE, PENDIENTES - PROMO TEMP. MASIVO
    private boolean recuperarBajavLocal(String u, Timestamp ts) {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        boolean valor = false;
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada_masivo");
        j.put("data", "{\"estadoPromo\" : false, \"usuMod\" : \"" + u + "\", \"fechaMod\" : \"" + ts + "\"}");
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U', '" + x + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro en HANDLER PARANA BD ********");
                if (promoTemporadaDAO.bajasLocal(u, ts)) {
                    valor = true;
                    System.out.println("-->> DATOS PERSISTIDOS PARANA LOCAL BD <<-- ");
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return valor;
    }
    //////UPDATE, PENDIENTES - PROMO TEMP. MASIVO

    //////UPDATE, PENDIENTES - PROMO TEMP.
    private boolean actualizarPromo(JSONObject promTemp) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "promo_temporada");
        j.put("data", promTemp.toString());
        String x = Utilidades.setToJson(j.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','U','" + x + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - PROMO TEMP.

    //////INSERT, PENDIENTES - PROMO TEMP NF1.
    private boolean registrarPromoTempLocal(JSONObject jsonObjectNf, int nf) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        switch (nf) {
            case 1:
                j.put("table", "promo_temporada_nf1");
                break;
            case 2:
                j.put("table", "promo_temporada_nf2");
                break;
            case 3:
                j.put("table", "promo_temporada_nf3");
                break;
            case 4:
                j.put("table", "promo_temporada_nf4");
                break;
            case 5:
                j.put("table", "promo_temporada_nf5");
                break;
            case 6:
                j.put("table", "promo_temporada_nf6");
                break;
            case 7:
                j.put("table", "promo_temporada_nf7");
                break;
            default:
                break;
        }
        j.put("data", jsonObjectNf.toJSONString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'" + Utilidades.host + "','I','" + Utilidades.setToJson(j.toString()) + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES handler_food BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////INSERT, PENDIENTES - PROMO TEMP NF1.

    //////READ, NF
    private void generarNivelFamiliaLocal() {
        TreeItem<String> treeItemRoot = new TreeItem<>("ÁRBOL DE SECCIONES");
        treeItemRoot.setExpanded(true);
        //y sí... mucho recurso...
        hashMapNf1 = new HashMap<>();
        hashMapNf2 = new HashMap<>();
        hashMapNf3 = new HashMap<>();
        hashMapNf4 = new HashMap<>();
        hashMapNf5 = new HashMap<>();
        hashMapNf6 = new HashMap<>();
        hashMapNf7 = new HashMap<>();
        hashMapDetalleNf1 = new HashMap<>();
        hashMapDetalleNf2 = new HashMap<>();
        hashMapDetalleNf3 = new HashMap<>();
        hashMapDetalleNf4 = new HashMap<>();
        hashMapDetalleNf5 = new HashMap<>();
        hashMapDetalleNf6 = new HashMap<>();
        hashMapDetalleNf7 = new HashMap<>();
        //la descripción e id, pueden coincidir en niveles distintos a la hora de mapear... 
        //si se instancia una vez y se setea a todos los items del treeview con el mismo objeto del tipo ImageView
        //no funcionará correctamente
        //se puede rastrear también con una concatenación id + descripción, como nombre del item, pero estéticamente no queda muy bien
        for (Nf1Tipo nf1Tipo : nf1TipoDAO.listar()) {
            ImageView imageViewNf1 = new ImageView(
                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-1-icon.png")));
            hashMapNf1.put(imageViewNf1, nf1Tipo);//rastreo para inserción
            hashMapDetalleNf1.put(nf1Tipo.getIdNf1Tipo(), nf1Tipo);
            TreeItem<String> treeItemNf1 = new TreeItem<>(nf1Tipo.getDescripcion(), imageViewNf1);
            for (Nf2Sfamilia nf2Sfamilia : nf1Tipo.getNf2Sfamilias()) {
                ImageView imageViewNf2 = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-2-icon.png")));
                hashMapNf2.put(imageViewNf2, nf2Sfamilia);
                hashMapDetalleNf2.put(nf2Sfamilia.getIdNf2Sfamilia(), nf2Sfamilia);
                TreeItem<String> treeItemNf2 = new TreeItem<>(nf2Sfamilia.getDescripcion(), imageViewNf2);
                for (Nf3Sseccion nf3Sseccion : nf2Sfamilia.getNf3Sseccions()) {
                    ImageView imageViewNf3 = new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-3-icon.png")));
                    hashMapNf3.put(imageViewNf3, nf3Sseccion);
                    hashMapDetalleNf3.put(nf3Sseccion.getIdNf3Sseccion(), nf3Sseccion);
                    TreeItem<String> treeItemNf3 = new TreeItem<>(nf3Sseccion.getDescripcion(), imageViewNf3);
                    for (Nf4Seccion1 nf4Seccion1 : nf3Sseccion.getNf4Seccion1s()) {
                        ImageView imageViewNf4 = new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/Numbers-4-icon.png")));
                        hashMapNf4.put(imageViewNf4, nf4Seccion1);
                        hashMapDetalleNf4.put(nf4Seccion1.getIdNf4Seccion1(), nf4Seccion1);
                        TreeItem<String> treeItemNf4 = new TreeItem<>(nf4Seccion1.getDescripcion(), imageViewNf4);
                        for (Nf5Seccion2 nf5Seccion2 : nf4Seccion1.getNf5Seccion2s()) {
                            ImageView imageViewNf5 = new ImageView(
                                    new Image(getClass().getResourceAsStream("/vista/img/Numbers-5-icon.png")));
                            hashMapNf5.put(imageViewNf5, nf5Seccion2);
                            hashMapDetalleNf5.put(nf5Seccion2.getIdNf5Seccion2(), nf5Seccion2);
                            TreeItem<String> treeItemNf5 = new TreeItem<>(nf5Seccion2.getDescripcion(), imageViewNf5);
                            for (Nf6Secnom6 nf6Secnom6 : nf5Seccion2.getNf6Secnom6s()) {
                                ImageView imageViewNf6 = new ImageView(
                                        new Image(getClass().getResourceAsStream("/vista/img/Numbers-6-icon.png")));
                                hashMapNf6.put(imageViewNf6, nf6Secnom6);
                                hashMapDetalleNf6.put(nf6Secnom6.getIdNf6Secnom6(), nf6Secnom6);
                                TreeItem<String> treeItemNf6 = new TreeItem<>(nf6Secnom6.getDescripcion(), imageViewNf6);
                                for (Nf7Secnom7 nf7Secnom7 : nf6Secnom6.getNf7Secnom7s()) {
                                    ImageView imageViewNf7 = new ImageView(
                                            new Image(getClass().getResourceAsStream("/vista/img/Numbers-7-icon.png")));
                                    hashMapNf7.put(imageViewNf7, nf7Secnom7);
                                    hashMapDetalleNf7.put(nf7Secnom7.getIdNf7Secnom7(), nf7Secnom7);
                                    TreeItem<String> treeItemNf7 = new TreeItem<>(nf7Secnom7.getDescripcion(), imageViewNf7);
                                    treeItemNf6.getChildren().add(treeItemNf7);
                                }
                                treeItemNf5.getChildren().add(treeItemNf6);
                            }
                            treeItemNf4.getChildren().add(treeItemNf5);
                        }
                        treeItemNf3.getChildren().add(treeItemNf4);
                    }
                    treeItemNf2.getChildren().add(treeItemNf3);
                }
                treeItemNf1.getChildren().add(treeItemNf2);
            }
            treeItemRoot.getChildren().add(treeItemNf1);
        }
        treeViewSecciones.setRoot(treeItemRoot);
    }
    //////READ, NF
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON PROMO TEMP.
    private JSONObject creandoJsonPromTemp() {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject promTemp = new JSONObject();
        //**********************************************************************
        promTemp.put("descripcionTemporada", textFieldNomPromTem.getText());
        promTemp.put("estadoPromo", true);
        //************** Fecha Inicio / Fecha Fin
        LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
        LocalDate fechaFinLD = datePickerFechaFin.getValue();
        Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
        Timestamp timestampIni = new Timestamp(dateIni.getTime());
        Long timestampIniJSON = timestampIni.getTime();
        Date dateFin = java.sql.Date.valueOf(fechaFinLD);
        Timestamp timestampFin = new Timestamp(dateFin.getTime());
        Long timestampFinJSON = timestampFin.getTime();
        //************** Fecha Inicio / Fecha Fin
        promTemp.put("fechaInicio", timestampIniJSON);
        promTemp.put("fechaFin", timestampFinJSON);
        //**********************************************************************
        promTemp.put("usuAlta", Identity.getNomFun());
        promTemp.put("fechaAlta", timestampJSON);
        promTemp.put("usuMod", Identity.getNomFun());
        promTemp.put("fechaMod", timestampJSON);
        //**********************************************************************
        return promTemp;
    }
    //JSON PROMO TEMP.

    //JSON PROMO DET.
    private JSONObject creandoJsonDetPromTemp(JSONObject promTempCab, JSONObject seccionNf, int porcentajeDesc, int nf) {
        JSONObject promTempDet = new JSONObject();
        //**********************************************************************
        promTempDet.put("promoTemporada", promTempCab);
        switch (nf) {
            case 1://nf1_tipo
                promTempDet.put("nf1Tipo", seccionNf);
                break;
            case 2:
                promTempDet.put("nf2Sfamilia", seccionNf);
                break;
            case 3:
                promTempDet.put("nf3Sseccion", seccionNf);
                break;
            case 4:
                promTempDet.put("nf4Seccion1", seccionNf);
                break;
            case 5:
                promTempDet.put("nf5Seccion2", seccionNf);
                break;
            case 6:
                promTempDet.put("nf6Secnom6", seccionNf);
                break;
            case 7:
                promTempDet.put("nf7Secnom7", seccionNf);
                break;
            default:
                break;
        }
        promTempDet.put("descriSeccion", seccionNf.get("descripcion"));
        promTempDet.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return promTempDet;
    }
    //JSON PROMO DET.

    //JSON PROMO BAJA
    private JSONObject bajaJsonCabPromTemp(JSONObject promTempCab) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        promTempCab.put("usuMod", Identity.getNomFun());
        promTempCab.put("fechaMod", timestampJSON);
        promTempCab.put("estadoPromo", false);
        //**********************************************************************
        return promTempCab;
    }
    //JSON PROMO BAJA

    //JSON BAJA DESCUENTO FIEL MASIVO
    private JSONObject creandoJsonBajaDescMasivo(String u, String ts) {
        JSONObject jsonBajaMasiva = new JSONObject();
        jsonBajaMasiva.put("usuMod", u);
        jsonBajaMasiva.put("fechaMod", ts);
        jsonBajaMasiva.put("descripcionDescuento", UtilLoaderBase.msjIda(Descuento.f3));
        return jsonBajaMasiva;
    }
    //JSON BAJA DESCUENTO FIEL MASIVO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantPromTemp = 5;
        filaIniPromTemp = 0;
        pageActualPromTemp = 0;
        buscarTodosPromTemp = true;
        buscandoPromTempTodo();
    }

    private void buscandoPromTempTodo() {
        filaLimitPromTemp = jsonRowCount();
        buscarTodosPromTemp = true;
        paginandoPromTemp();
    }

    private void buscandoPromTempFiltro() {
        String filterParam = "null";
        String filterFechaDesde = "null";
        String filterFechaHasta = "null";
        LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
        LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
        if (fechaDesdeLD != null) {
            Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
            filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
        }
        if (fechaHastaLD != null) {
            Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
            filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
        }
        if (!textFielFiltroPromTemp.getText().contentEquals("")) {
            filterParam = textFielFiltroPromTemp.getText();
        }
        filaLimitPromTemp = jsonRowCountFilter(filterParam, filterFechaDesde, filterFechaHasta);
        buscarTodosPromTemp = false;
        paginandoPromTemp();
    }

    private void paginandoPromTemp() {
        paginationPromTemp.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabFiel(pageIndex);
                //**************************************************************
            }
        });
        paginationPromTemp.setCurrentPageIndex(this.pageActualPromTemp);//index inicial paginación
        paginationPromTemp.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountPromTemp = filaLimitPromTemp / filaCantPromTemp;//cantidad index paginación
        if (filaLimitPromTemp % filaCantPromTemp > 0) {//index EXTRA (resto de filas)***********
            pageCountPromTemp++;
        }
        paginationPromTemp.setPageCount(pageCountPromTemp);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabFiel(int pageIndex) {
        filaIniPromTemp = pageIndex * filaCantPromTemp;
        if (buscarTodosPromTemp) {
            promTempList = jsonArrayPromTempFetch(filaCantPromTemp, filaIniPromTemp);
        } else {
            String filterParam = "null";
            String filterFechaDesde = "null";
            String filterFechaHasta = "null";
            LocalDate fechaDesdeLD = datePickerFechaInicioFiltro.getValue();
            LocalDate fechaHastaLD = datePickerFechaFinFiltro.getValue();
            if (fechaDesdeLD != null) {
                Date dateDesde = java.sql.Date.valueOf(fechaDesdeLD);
                filterFechaDesde = new SimpleDateFormat(patternDateFechaFiltro).format(dateDesde);
            }
            if (fechaHastaLD != null) {
                Date dateHasta = java.sql.Date.valueOf(fechaHastaLD);
                filterFechaHasta = new SimpleDateFormat(patternDateFechaFiltro).format(dateHasta);
            }
            if (!textFielFiltroPromTemp.getText().contentEquals("")) {
                filterParam = textFielFiltroPromTemp.getText();
            }
            promTempList = jsonArrayPromTempFetchFiltro(filaCantPromTemp, filaIniPromTemp, filterParam, filterFechaDesde, filterFechaHasta);
        }
        actualizandoTablaPromTemp();
        return tablePromTemp;
    }

    private void actualizandoTablaPromTemp() {
        tablePromTemp = new TableView<JSONObject>();
        promTempData = FXCollections.observableArrayList(promTempList);
        //columna Sección ....................................................
        columnPromTemp = new TableColumn("Promoción temporada");
        columnPromTemp.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPromTemp.setMinWidth(540);
        columnPromTemp.setEditable(false);
        columnPromTemp.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descripcionTemporada").toString());
            }
        });
        //columna Sección ....................................................
        //columna Fecha Inicio..................................................
        columnPromTempFechaIni = new TableColumn("Fecha inicio");
        columnPromTempFechaIni.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPromTempFechaIni.setMinWidth(120);
        columnPromTempFechaIni.setEditable(false);
        columnPromTempFechaIni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaInicio"))));
            }
        });
        //columna Fecha Inicio..................................................
        //columna Fecha Fin.....................................................
        columnPromTempFechaFin = new TableColumn("Fecha fin");
        columnPromTempFechaFin.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPromTempFechaFin.setMinWidth(120);
        columnPromTempFechaFin.setEditable(false);
        columnPromTempFechaFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaFin"))));
            }
        });
        //columna Fecha Fin.....................................................
        //columna Cancelar .....................................................
        columnAcciones = new TableColumn("Acciones");
        columnAcciones.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new PromocionTemporadaFXMLController.AddObjectsCell(tablePromTemp);
            }
        });
        //columna Cancelar .....................................................
        tablePromTemp.getColumns().addAll(columnPromTemp, columnPromTempFechaIni, columnPromTempFechaFin, columnAcciones);
        tablePromTemp.setItems(promTempData);
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button("cancelar");
        Button verButton = new Button("ver secciones");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    promTempCab = new JSONObject();
                    jsonArrayChilds = new JSONArray();
                    hashMapFormularioDetalleChilds = new HashMap<>();
                    promTempCab = tablePromTemp.getItems().get(index);
                    if (promTempCab.containsKey("promoTemporadaNf1s")) {
                        JSONArray jsonArrayPromoTemporadaNf1 = (JSONArray) promTempCab.get("promoTemporadaNf1s");
                        for (Object objectPromoTemporadaNf1 : jsonArrayPromoTemporadaNf1) {
                            JSONObject jsonPromoTemporadaNf1 = (JSONObject) objectPromoTemporadaNf1;
                            JSONObject jsonNf1Tipo = (JSONObject) jsonPromoTemporadaNf1.get("nf1Tipo");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("1->" + jsonNf1Tipo.get("idNf1Tipo").toString(),
                                    hashMapDetalleNf1.get(Long.valueOf(jsonNf1Tipo.get("idNf1Tipo").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf2s")) {
                        JSONArray jsonArrayPromoTemporadaNf2 = (JSONArray) promTempCab.get("promoTemporadaNf2s");
                        for (Object objectPromoTemporadaNf2 : jsonArrayPromoTemporadaNf2) {
                            JSONObject jsonPromoTemporadaNf2 = (JSONObject) objectPromoTemporadaNf2;
                            JSONObject jsonNf2Sfamilia = (JSONObject) jsonPromoTemporadaNf2.get("nf2Sfamilia");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("2->" + jsonNf2Sfamilia.get("idNf2Sfamilia").toString(),
                                    hashMapDetalleNf2.get(Long.valueOf(jsonNf2Sfamilia.get("idNf2Sfamilia").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf3s")) {
                        JSONArray jsonArrayPromoTemporadaNf3 = (JSONArray) promTempCab.get("promoTemporadaNf3s");
                        for (Object objectPromoTemporadaNf3 : jsonArrayPromoTemporadaNf3) {
                            JSONObject jsonPromoTemporadaNf3 = (JSONObject) objectPromoTemporadaNf3;
                            JSONObject jsonNf3Sseccion = (JSONObject) jsonPromoTemporadaNf3.get("nf3Sseccion");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("3->" + jsonNf3Sseccion.get("idNf3Sseccion").toString(),
                                    hashMapDetalleNf3.get(Long.valueOf(jsonNf3Sseccion.get("idNf3Sseccion").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf4s")) {
                        JSONArray jsonArrayPromoTemporadaNf4 = (JSONArray) promTempCab.get("promoTemporadaNf4s");
                        for (Object objectPromoTemporadaNf4 : jsonArrayPromoTemporadaNf4) {
                            JSONObject jsonPromoTemporadaNf4 = (JSONObject) objectPromoTemporadaNf4;
                            JSONObject jsonNf4Seccion1 = (JSONObject) jsonPromoTemporadaNf4.get("nf4Seccion1");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("4->" + jsonNf4Seccion1.get("idNf4Seccion1").toString(),
                                    hashMapDetalleNf4.get(Long.valueOf(jsonNf4Seccion1.get("idNf4Seccion1").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf5s")) {
                        JSONArray jsonArrayPromoTemporadaNf5 = (JSONArray) promTempCab.get("promoTemporadaNf5s");
                        for (Object objectPromoTemporadaNf5 : jsonArrayPromoTemporadaNf5) {
                            JSONObject jsonPromoTemporadaNf5 = (JSONObject) objectPromoTemporadaNf5;
                            JSONObject jsonNf5Seccion2 = (JSONObject) jsonPromoTemporadaNf5.get("nf5Seccion2");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("5->" + jsonNf5Seccion2.get("idNf5Seccion2").toString(),
                                    hashMapDetalleNf5.get(Long.valueOf(jsonNf5Seccion2.get("idNf5Seccion2").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf6s")) {
                        JSONArray jsonArrayPromoTemporadaNf6 = (JSONArray) promTempCab.get("promoTemporadaNf6s");
                        for (Object objectPromoTemporadaNf6 : jsonArrayPromoTemporadaNf6) {
                            JSONObject jsonPromoTemporadaNf6 = (JSONObject) objectPromoTemporadaNf6;
                            JSONObject jsonNf6Secnom6 = (JSONObject) jsonPromoTemporadaNf6.get("nf6Secnom6");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("6->" + jsonNf6Secnom6.get("idNf6Secnom6").toString(),
                                    hashMapDetalleNf6.get(Long.valueOf(jsonNf6Secnom6.get("idNf6Secnom6").toString())));
                        }
                    }
                    if (promTempCab.containsKey("promoTemporadaNf7s")) {
                        JSONArray jsonArrayPromoTemporadaNf7 = (JSONArray) promTempCab.get("promoTemporadaNf7s");
                        for (Object objectPromoTemporadaNf7 : jsonArrayPromoTemporadaNf7) {
                            JSONObject jsonPromoTemporadaNf7 = (JSONObject) objectPromoTemporadaNf7;
                            JSONObject jsonNf7Secnom7 = (JSONObject) jsonPromoTemporadaNf7.get("nf7Secnom7");
                            //para pasar las hojas o hijos, parte del treeview
                            hashMapFormularioDetalleChilds.put("7->" + jsonNf7Secnom7.get("idNf7Secnom7").toString(),
                                    hashMapDetalleNf7.get(Long.valueOf(jsonNf7Secnom7.get("idNf7Secnom7").toString())));
                        }
                    }
                    viendoDetalleSeccion();
                }
            });
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_prom_temp")) {
                    JSONObject descPromTemp = tablePromTemp.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR PROMOCIÓN TEMPORADA "
                            + descPromTemp.get("descripcionTemporada").toString().toUpperCase() + "?", ok, cancel);
                    alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ok) {
                        descPromTemp = bajaJsonCabPromTemp(descPromTemp);
                        bajaPromTemp(descPromTemp);
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeError("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker() {
        return new Task() {
            @Override
            protected Object call() {
                JSONParser parser = new JSONParser();
                int i = 1;
                int size = jsonArrayDetalles.size();
                Platform.runLater(() -> {
                    viendoProgress();
                });
                for (int j = 0; j < size; j++) {
                    try {
                        JSONObject jsonDetalle = (JSONObject) parser.parse(jsonArrayDetalles.get(j).toString());
                        System.out.println("-->> " + jsonDetalle);
                        try {
                            if (jsonDetalle.containsKey("nf1Tipo")) {
                                creandoDetPromTempNf1(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf2Sfamilia")) {
                                creandoDetPromTempNf2(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf3Sseccion")) {
                                creandoDetPromTempNf3(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf4Seccion1")) {
                                creandoDetPromTempNf4(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf5Seccion2")) {
                                creandoDetPromTempNf5(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf6Secnom6")) {
                                creandoDetPromTempNf6(jsonDetalle);
                            } else if (jsonDetalle.containsKey("nf7Secnom7")) {
                                creandoDetPromTempNf7(jsonDetalle);
                            }
                        } catch (Exception ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        } finally {
                            Utilidades.log.error("finally createWorker PromoTemp");
                        }
                        updateProgress((j + 1), size);
                        if (i == size) {
                            exitoInsertarCab = false;
                            Platform.runLater(() -> {
                                buscandoPromTempTodo();
                                limpiandoCampos();
                                datePickerFechaInicio.setValue(LocalDate.now());
                            });
                        } else {
                            i++;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                Platform.runLater(() -> {
                    ocultandoProgress();
                });
                CajaDatos.setDescuentoPromo(false);
                return true;
            }
        };
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    public static List<JSONObject> getPromTempDetJSONObjList() {
        return promTempDetJSONObjList;
    }

    public static JSONObject getPromTempCab() {
        return promTempCab;
    }

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public static HashMap<String, Object> getHashMapFormularioDetalleChilds() {
        return hashMapFormularioDetalleChilds;
    }
}
