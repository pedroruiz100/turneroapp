/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.descuento;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class PromocionTemporadaDetalleArtFXMLController extends BaseScreenController implements Initializable {

    //TABLE VIEW
    private List<JSONObject> promTempDetList;
    private ObservableList<JSONObject> detalleData;
    //TABLE VIEW

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private SplitPane splitPane;
    @FXML
    private AnchorPane anchorPaneHeader;
    @FXML
    private AnchorPane anchorPaneBody;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescuento;
    @FXML
    private Label labelPromTempDetArt;
    @FXML
    private TableView<JSONObject> tableViewArticuloTemp;
    @FXML
    private TableColumn<JSONObject, String> tableColumnArticulo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodArt;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantObsequio;
    @FXML
    private TableColumn<JSONObject, String> tableColumnMinReq;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        labelPromTempDetArt.setText(String.valueOf(PromocionTemporadaArtFXMLController.getPromTempCab().get("descripcionTemporadaArt")).toUpperCase());
        promTempDetList = new ArrayList<>();
        for (int i = 0; i < PromocionTemporadaArtFXMLController.getPromTempDetJSONObjList().size(); i++) {
            promTempDetList.add(PromocionTemporadaArtFXMLController.getPromTempDetJSONObjList().get(i));
        }
        for (int i = 0; i < PromocionTemporadaArtFXMLController.getPromTempDetObsequioJSONObjList().size(); i++) {
            promTempDetList.add(PromocionTemporadaArtFXMLController.getPromTempDetObsequioJSONObjList().get(i));
        }
        vistaJSONObjectPromTemp();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/descuento/PromocionTemporadaArtFXML.fxml", 982, 875, "/vista/descuento/PromocionTemporadaDetalleArtFXML.fxml", 802, 743, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
    private void vistaJSONObjectPromTemp() {
        //......................................................................
        detalleData = FXCollections.observableArrayList(promTempDetList);
        //columna código..............................................
        tableColumnCodArt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodArt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(jsonArticulo.get("codArticulo").toString());
            }
        });
        //columna código......................................................
        //columna artículo..............................................
        tableColumnArticulo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnArticulo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descriArticulo").toString());
            }
        });
        //columna artículo......................................................
        //columna Porcentaje..............................................
        tableColumnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        tableColumnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().containsKey("porcentajeDesc")) {
                    String[] parts = data.getValue().get("porcentajeDesc").toString().split("\\.");
                    return new SimpleStringProperty(parts[0] + " %");
                } else {
                    return new SimpleStringProperty("N/A");
                }
            }
        });
        //columna Cant. Obsequio................................................
        tableColumnCantObsequio.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        tableColumnCantObsequio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().containsKey("cantObsequio")) {
                    return new SimpleStringProperty(data.getValue().get("cantObsequio").toString());
                } else {
                    return new SimpleStringProperty("N/A");
                }
            }
        });
        //columna Cant. Obsequio................................................
        //columna Cant. Min.....................................................
        tableColumnMinReq.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        tableColumnMinReq.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().containsKey("minReq")) {
                    return new SimpleStringProperty(data.getValue().get("minReq").toString());
                } else {
                    return new SimpleStringProperty("N/A");
                }
            }
        });
        //columna Cant. Min.....................................................
        tableViewArticuloTemp.setItems(detalleData);
    }
    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
}
