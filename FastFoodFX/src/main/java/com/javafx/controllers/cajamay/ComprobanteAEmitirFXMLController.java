/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ComprobanteAEmitirFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private VBox vBox;
    @FXML
    private Button buttonFacturaContado;
    @FXML
    private Button buttonFacturaCredito;
    @FXML
    private Button buttonNotaDeRemision;
    @FXML
    private Button buttonFraCtdoCanjeOcAc;
    @FXML
    private Button buttonFraCredCanjeOcAc;
    @FXML
    private Button buttonFraCtdoMerma;
    @FXML
    private Button buttonFraCredMerma;
    @FXML
    private Button buttonFraCtdoDonacion;
    @FXML
    private Button buttonFraCredDonacion;
    @FXML
    private Button buttonRegistroOcAComercial;
    @FXML
    private Button buttonNotaDeCredito;
    @FXML
    private Button buttonNotaDeDebito;
    @FXML
    private Button buttonPresupuesto;
    @FXML
    private Label labelTipoDeComprobante;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonFacturaContadoAction(ActionEvent event) {
    }

    @FXML
    private void buttonFacturaCreditoAction(ActionEvent event) {
    }

    @FXML
    private void buttonNotaDeRemisionAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCtdoCanjeOcAcAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCredCanjeOcAcAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCtdoMermaAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCredMermaAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCtdoDonacionAction(ActionEvent event) {
    }

    @FXML
    private void buttonFraCredDonacionAction(ActionEvent event) {
    }

    @FXML
    private void buttonRegistroOcAComercialAction(ActionEvent event) {
    }

    @FXML
    private void buttonNotaDeCreditoAction(ActionEvent event) {
    }

    @FXML
    private void buttonNotaDeDebitoAction(ActionEvent event) {
    }

    @FXML
    private void buttonPresupuestoAction(ActionEvent event) {
    }

}
