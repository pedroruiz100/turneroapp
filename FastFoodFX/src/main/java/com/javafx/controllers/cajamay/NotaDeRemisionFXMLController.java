/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class NotaDeRemisionFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Label labelTitulo;
    @FXML
    private AnchorPane anchorPane1;
    @FXML
    private VBox vBoxTituloAnchorPane1;
    @FXML
    private HBox hBoxCliente;
    @FXML
    private Label labelCliente;
    @FXML
    private Button buttonCliente;
    @FXML
    private Label labelTipoMov1;
    @FXML
    private Label labelTipoMov2;
    @FXML
    private Label labelNroFra;
    @FXML
    private VBox vBoxTextFieldAnchorPane1;
    @FXML
    private TextField textFieldCliente;
    @FXML
    private TextField textFieldRem;
    @FXML
    private TextField textFieldSal;
    @FXML
    private TextField textFieldNroFra;
    @FXML
    private Label labelTipoMov11;
    @FXML
    private Label labelTipoMov12;
    @FXML
    private AnchorPane anchorPane2;
    @FXML
    private VBox vBoxAnchorPane2Buttons;
    @FXML
    private Button buttonCargarDatos;
    @FXML
    private Button buttonSalir;
    @FXML
    private GridPane gridPaneAuto;
    @FXML
    private Label labelNroChapa;
    @FXML
    private Label labelMarca;
    @FXML
    private ComboBox<?> comboBoxNroChapa;
    @FXML
    private AnchorPane anchorPane3;
    @FXML
    private GridPane gridPaneDireccion;
    @FXML
    private ComboBox<?> comboBoxDireccPartida;
    @FXML
    private ComboBox<?> comboBoxDireccLllegada;
    @FXML
    private Label labelDireccionPartida;
    @FXML
    private Label labelDireccionLlegada;
    @FXML
    private AnchorPane anchorPane3Sub;
    @FXML
    private Label labelDatosDelTransportista;
    @FXML
    private TextField textFieldMarca;
    @FXML
    private Label labelRucTransportista;
    @FXML
    private Label labelRazonTransportista;
    @FXML
    private Label lableCiConductor;
    @FXML
    private Label labelNomConductor;
    @FXML
    private Label labelDirConductor;
    @FXML
    private ComboBox<?> comboBoxRucTransp;
    @FXML
    private ComboBox<?> comboBoxCiConductor;
    @FXML
    private TextField textFieldRazonTransportista;
    @FXML
    private TextField textFieldNomDelConductor;
    @FXML
    private TextField textFieldDirDelConductor;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonClienteAction(ActionEvent event) {
    }

    @FXML
    private void buttonCargarDatosAction(ActionEvent event) {
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void comboBoxDireccPartidaAction(ActionEvent event) {
    }

    @FXML
    private void comboBoxDireccLllegadaAction(ActionEvent event) {
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/NotaDeRemisionFXML.fxml", 944, 484, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

}
