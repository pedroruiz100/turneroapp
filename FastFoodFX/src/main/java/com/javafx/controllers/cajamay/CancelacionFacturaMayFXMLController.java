/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.MotivoCancelacionFactura;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.MotivoCancelacionFacturaDAO;
import com.peluqueria.dao.RangoCancelFactDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.caja.CancelacionFacturaFXMLController;
import com.javafx.controllers.caja.CancelacionProductoFXMLController;
import com.javafx.controllers.caja.ClienteFXMLController;
import com.javafx.controllers.caja.FacturaDeVentaFXMLController;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
public class CancelacionFacturaMayFXMLController extends BaseScreenController implements Initializable {

    public static void setFactAnt(String aFactAnt) {
        factAnt = aFactAnt;
    }

    static void setTotalFacturado(String total, TextField txtCod) {
        totalFacturado = total;
        txtCodigo = txtCod;
    }
    private boolean alert;
    private boolean primerLogueo;
    private JSONObject supervisor;
    private static TextField txtCodigo;
    private NumberValidator numValidator;

    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    @Autowired
    private SupervisorDAO superDAO;
    @Autowired
    private MotivoCancelacionFacturaDAO motivoDAO;
    @Autowired
    private RangoCancelFactDAO rangoCancelFactDAO;

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private Map motivos;
    String selectMotivoInicial = "-- Seleccione un motivo --";
    private Date date;
    private Timestamp timestamp;
    private static String factAnt;
    private static String totalFacturado;
    boolean valorEstado = false;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private HBox hBox;
    @FXML
    private Label labelCodSupervisor;
    @FXML
    private PasswordField passwordFieldCodSupervisor;
    @FXML
    private AnchorPane anchorPaneDatosRetiro;
    @FXML
    private VBox vBoxLabel;
    @FXML
    private Label labelSupervisor;
    @FXML
    private Label labelMotivo;
    @FXML
    private VBox vBoxText;
    @FXML
    private TextField textFieldSupervisor;
    @FXML
    private ChoiceBox<String> choiceBoxMotivos;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneCancelacionFactura;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        if (!valorEstado) {
            procesar();
        }
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneCancelacionFacturaReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void choiceBoxMotivosReleased(KeyEvent event) {
        keyPressChoiceBox(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        numValidator = new NumberValidator();
        valorEstado = false;
        ocultandoParametros();
        asignandoVariables();
        choiceBoxMotivos.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            choiceBoxMotivos.show();
        });
    }
    //INICIAL INICIAL INICIAL **************************************************

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == KeyCode.ENTER) {
            if (this.alert) {
                verificandoSupervisor();
            }
        }
        if (keyCode == KeyCode.ESCAPE) {
            volviendo();
        }
    }

    private void keyPressChoiceBox(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == KeyCode.ENTER) {
            if (!valorEstado) {
                procesar();
            }
        } else if (keyCode == KeyCode.ESCAPE) {
            volviendo();
        }

    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        FacturaDeVentaMayFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/CancelacionFacturaMayFXML.fxml", 499, 234, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    private void ocultandoParametros() {
        vBoxLabel.setVisible(false);
        vBoxText.setVisible(false);
        btnProcesar.setVisible(false);
    }

    private void mostrandoParametros() {
        vBoxLabel.setVisible(true);
        vBoxText.setVisible(true);
        btnProcesar.setVisible(true);
    }

    private void asignandoVariables() {
        JSONParser parser = new JSONParser();
        this.alert = true;
        this.primerLogueo = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = DatosEnCaja.getFacturados();
                JSONObject jsonFactCab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                FacturaDeVentaFXMLController.setCabFactura(jsonFactCab);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
    }

    private void verificandoSupervisor() {
        if (!passwordFieldCodSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(passwordFieldCodSupervisor.getText()));
            if (supervisor != null) {
                mostrandoParametros();
                passwordFieldCodSupervisor.setEditable(false);
                JSONObject usuario = (JSONObject) supervisor.get("usuario");
                textFieldSupervisor.setText(usuario.get("nomUsuario").toString());
                if (primerLogueo) {
                    jsonMotivoCancelaciones();
                    primerLogueo = false;
                }
                this.alert = false;
                choiceBoxMotivos.requestFocus();
            } else {
                ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alerta = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR INCORRECTA.", cancel);
                alerta.showAndWait();
                ocultandoParametros();
                passwordFieldCodSupervisor.setText("");
                if (alerta.getResult() == cancel) {
                    alerta.close();
                } else {
                    alerta.close();
                }
            }
        } else {
            ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alerta = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR VACÍA.", cancel);
            this.alert = true;
            alerta.showAndWait();
            if (alerta.getResult() == cancel) {
                alerta.close();
            } else {
                alerta.close();
            }
        }
    }

    private void procesar() {
        if (seleccionMotivo()) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            Alert alerts = new Alert(Alert.AlertType.WARNING, "DEBE SELECCIONAR UN MOTIVO.", ok);
            this.alert = true;
            alerts.showAndWait();
            if (alerts.getResult() == ok) {
                alerts.close();
            }
        } else if (procesandoCancelacion()) {
            eliminarAuxiliarCancelProd();
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            JSONParser parser = new JSONParser();
            if (!jsonDatos.isNull("idDato")) {
                if (jsonDatos.getLong("idDato") != 0) {
                    eliminarCabecera(jsonDatos.getLong("idDato"));
                }
            }
            if (jsonDatos.isNull("fecha_arqueo")) {
                ZoneId zonedId = ZoneId.of("America/Montreal");
                LocalDate today = LocalDate.now(zonedId);
                datos.put("fecha_arqueo", today.toString());
            }
            if (!jsonDatos.isNull("energiaElectrica")) {
                JSONObject talona;
                try {
                    talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
                    tal.setNroActual(tal.getNroActual() - 1);
                    taloDAO.actualizarNroActual(tal);
                } catch (ParseException ex) {
                    Logger.getLogger(CancelacionFacturaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            CancelacionProductoFXMLController.detalleArtList = new ArrayList<>();
            actualizarDatos();
            new Toaster().mensajeGenerico("Mensaje del Sistema", "LA CANCELACIÓN FACTURA SE REALIZÓ CON ÉXITO.", "", 2);
            this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/CancelacionFacturaMayFXML.fxml", 499, 234, true);
        }
        valorEstado = true;
    }

    private void mensajeInformacion(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private JSONObject jsonClaveSupervisor(String c) {
        JSONObject superv = null;
        superv = buscarCodSupervisorLocal(c);
        return superv;
    }

    public JSONObject buscarCodSupervisorLocal(String c) {
        try {
            JSONParser parser = new JSONParser();
            Supervisor sup = superDAO.buscarCodSup(c);
            return (JSONObject) parser.parse(gson.toJson(sup.toSupervisorDTO()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private void jsonMotivoCancelaciones() {
        motivos = new HashMap();
        generarChoiceBoxMotivo();
    }

    public void generarChoiceBoxMotivo() {
        JSONParser parser = new JSONParser();
        List<MotivoCancelacionFactura> listMotivos = motivoDAO.listar();
        for (MotivoCancelacionFactura moti : listMotivos) {
            try {
                JSONObject mot = (JSONObject) parser.parse(gson.toJson(moti.toBDMotivoCancelacionFacturaDTO()));
                choiceBoxMotivos.getItems().add(mot.get("descripcionMotivoCancelFact").toString());
                motivos.put(mot.get("descripcionMotivoCancelFact"), mot.get("idMotivoCancelFact"));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        if (!choiceBoxMotivos.getItems().isEmpty()) {
            choiceBoxMotivos.getSelectionModel().select(0);
        }
    }

    private boolean seleccionMotivo() {
        return choiceBoxMotivos.getSelectionModel().getSelectedItem().equalsIgnoreCase(selectMotivoInicial);
    }

    private boolean procesandoCancelacion() {
//        try {
        boolean exito = false;
        JSONParser parser = new JSONParser();
        try {
            org.json.JSONObject json = new org.json.JSONObject(datos);
            if (!json.isNull("caida")) {
                if (datos.get("caida").toString().equalsIgnoreCase("forma_pago")) {
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());

                    org.json.JSONObject jsonFact = new org.json.JSONObject(objFact);
                    org.json.JSONObject jsonFormaPago = new org.json.JSONObject();
                    JSONObject obj = new JSONObject();
                    if (!jsonFact.isNull("forma_pago")) {
                        obj = (JSONObject) parser.parse(objFact.get("forma_pago").toString());
                        jsonFormaPago = new org.json.JSONObject(obj.toString());
                    }
                    int monEfectivo = 0;
                    if (!jsonFormaPago.isNull("monEfectivo")) {
                        monEfectivo = Integer.parseInt(obj.get("monEfectivo").toString());
                    }
                    int monNotaCred = 0;
                    if (!jsonFormaPago.isNull("monNotaCred")) {
                        monNotaCred = Integer.parseInt(obj.get("monNotaCred").toString());
                    }
                    int monAsoc = 0;
                    if (!jsonFormaPago.isNull("monAsoc")) {
                        monAsoc = Integer.parseInt(obj.get("monAsoc").toString());
                    }
                    int monVale = 0;
                    if (!jsonFormaPago.isNull("monVale")) {
                        monVale = Integer.parseInt(obj.get("monVale").toString());
                    }
                    int monDolar = 0;
                    if (!jsonFormaPago.isNull("monDolar")) {
                        monDolar = Integer.parseInt(obj.get("monDolar").toString());
                    }
                    int monReal = 0;
                    if (!jsonFormaPago.isNull("monReal")) {
                        monReal = Integer.parseInt(obj.get("monReal").toString());
                    }
                    int monPeso = 0;
                    if (!jsonFormaPago.isNull("monPeso")) {
                        monPeso = Integer.parseInt(obj.get("monPeso").toString());
                    }
                    int monRetencion = 0;
                    if (!jsonFormaPago.isNull("monRetencion")) {
                        monRetencion = Integer.parseInt(obj.get("monRetencion").toString());
                    }
                    int monDesc = 0;
                    if (!jsonFormaPago.isNull("monDesc")) {
                        monDesc = Integer.parseInt(obj.get("monDesc").toString());
                    }
                    int monTarj = 0;
                    if (!jsonFormaPago.isNull("monTarj")) {
                        monTarj = Integer.parseInt(obj.get("monTarj").toString());
                    }
                    int monCheque = 0;
                    if (!jsonFormaPago.isNull("monCheque")) {
                        monCheque = Integer.parseInt(obj.get("monCheque").toString());
                    }
                    if (monEfectivo != 0) {
                        int contEfectivo = Integer.parseInt(datos.get("contEfectivo").toString());
                        int resultadoCon = contEfectivo - 1;
                        datos.put("contEfectivo", resultadoCon);
                    }
                    if (monNotaCred != 0) {
                        int sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
                        int contNotaCred = Integer.parseInt(datos.get("contNotaCred").toString());
                        int resultadoSum = sumNotaCred - monNotaCred;
                        int resultadoCon = contNotaCred - 1;
                        datos.put("sumNotaCred", resultadoSum);
                        datos.put("contNotaCred", resultadoCon);
                    }
                    if (monAsoc != 0) {
                        int sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
                        int contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
                        int resultadoSum = sumAsoc - monAsoc;
                        int resultadoCon = contAsoc - 1;
                        datos.put("sumAsoc", resultadoSum);
                        datos.put("contAsoc", resultadoCon);
                    }
                    if (monVale != 0) {
                        int sumVale = Integer.parseInt(datos.get("sumVale").toString());
                        int contVale = Integer.parseInt(datos.get("contVale").toString());
                        int resultadoSum = sumVale - monVale;
                        int resultadoCon = contVale - 1;
                        datos.put("sumVale", resultadoSum);
                        datos.put("contVale", resultadoCon);
                    }
                    if (monDolar != 0) {
                        int sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
                        int contDolar = Integer.parseInt(datos.get("contDolar").toString());
                        int resultadoSum = sumDolar - monDolar;
                        int resultadoCon = contDolar - 1;
                        datos.put("sumDolar", resultadoSum);
                        datos.put("contDolar", resultadoCon);
                    }
                    if (monReal != 0) {
                        int sumReal = Integer.parseInt(datos.get("sumReal").toString());
                        int contReal = Integer.parseInt(datos.get("contReal").toString());
                        int resultadoSum = sumReal - monReal;
                        int resultadoCon = contReal - 1;
                        datos.put("sumReal", resultadoSum);
                        datos.put("contReal", resultadoCon);
                    }
                    if (monPeso != 0) {
                        int sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
                        int contPeso = Integer.parseInt(datos.get("contPeso").toString());
                        int resultadoSum = sumPeso - monPeso;
                        int resultadoCon = contPeso - 1;
                        datos.put("sumPeso", resultadoSum);
                        datos.put("contPeso", resultadoCon);
                    }
                    if (monRetencion != 0) {
                        int sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
                        int contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
                        int resultadoSum = sumRetencion - monRetencion;
                        int resultadoCon = contRetencion - 1;
                        datos.put("sumRetencion", resultadoSum);
                        datos.put("contRetencion", resultadoCon);
                    }
                    if (monDesc != 0) {
                        int sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
                        int contDesc = Integer.parseInt(datos.get("contDesc").toString());
                        int resultadoSum = sumDesc - monDesc;
                        int resultadoCon = contDesc - 1;
                        datos.put("sumDesc", resultadoSum);
                        datos.put("contDesc", resultadoCon);
                    }
                    if (monTarj != 0) {
                        int sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
                        int contTarj = Integer.parseInt(datos.get("contTarj").toString());
                        int resultadoSum = sumTarj - monTarj;
                        int resultadoCon = contTarj - 1;
                        datos.put("sumTarj", resultadoSum);
                        datos.put("contTarj", resultadoCon);
                    }
                    if (monCheque != 0) {
                        int sumCheque = Integer.parseInt(datos.get("sumCheque").toString());
                        int contCheque = Integer.parseInt(datos.get("contCheque").toString());
                        int resultadoSum = sumCheque - monCheque;
                        int resultadoCon = contCheque - 1;
                        datos.put("sumCheque", resultadoSum);
                        datos.put("contCheque", resultadoCon);
                    }
                }
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        JSONObject jsonCancelacion = creandoJsonCancelacion();
        try {
            if (DatosEnCaja.getDatos() != null) {
                datos = DatosEnCaja.getDatos();
            }
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            }
            org.json.JSONObject json = new org.json.JSONObject(datos);
            exito = registarCancelacionFactLocal(jsonCancelacion);
            json = new org.json.JSONObject(datos);
            if (!json.isNull("caida")) {
                if (!datos.get("caida").toString().equalsIgnoreCase("forma_pago")) {
                }
            } else {
            }
            //SeteandoDatos de FacturaClienteCab
            datos.put("uuidCassandraActual", "");
            datos.put("idRangoFacturaActual", 0);
            datos.put("idFactClienteCabServidor", 0);
            datos.put("insercionFacturaVentaCab", false);
            datos.put("actualizacionLocal", false);
            datos.put("insercionFacturaVentaCabLocal", false);
            datos.put("ventaServer", false);
            ClienteFXMLController.resetParam();
            JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            FacturaDeVentaFXMLController.setTextFieldCompAnt(factAnt);
            datos.put("compAnt", cab.get("nroFactura").toString());
            datos.put("nroFact", "");
            setCantidadArticulo();
            return exito;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }

    private boolean registarCancelacionFactLocal(JSONObject jsonCancelacion) {
        boolean estado = false;
        ConexionPostgres.conectar();
        long idRango = rangoCancelFactDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        jsonCancelacion.put("idCancelacionFactura", idRango);
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonCancelacion + "','cancelacionFactura', 'insertar');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION FACTURA ********");
                FacturaVentaDatos.resetParam();
                FacturaDeVentaMayFXMLController.iniciandoFactCab();
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        registrarCancelFactDet(idRango);
        ConexionPostgres.cerrar();
        return estado;
    }

    private JSONObject creandoJsonCancelacion() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        date = new Date();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        String motivoDescri = choiceBoxMotivos.getSelectionModel().getSelectedItem();
        JSONObject cancelacion = new JSONObject();
        JSONObject usuarioSup = (JSONObject) supervisor.get("usuario");
        JSONObject aperturaCa = null;
        try {
            aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
        usuarioCajero.put("idUsuario", usuarioCajero.get("idUsuario").toString());
        JSONObject usuarioSupervisor = new JSONObject();
        usuarioSupervisor.put("idUsuario", usuarioSup.get("idUsuario"));
        JSONObject motivoCancelacionFac = new JSONObject();
        motivoCancelacionFac.put("idMotivoCancelFact", motivos.get(motivoDescri));
        JSONObject sucu = new JSONObject();
        try {
            JSONObject jsonSucu = (JSONObject) parser.parse(jsonDatos.get("sucursal").toString());
            sucu.put("idSucursal", jsonSucu.get("idSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        cancelacion.put("sucursal", sucu);
        cancelacion.put("fechaCancelacion", timestampJSON);
        cancelacion.put("usuarioCajero", usuarioCajero);
        cancelacion.put("usuarioSupervisor", usuarioSupervisor);
        cancelacion.put("motivoCancelacionFactura", motivoCancelacionFac);
        cancelacion.put("monto", numValidator.numberValidator(totalFacturado));
        cancelacion.put("nroFactura", CancelacionFacturaMayFXMLController.factAnt);
        return cancelacion;
    }

    private static void actualizarDatos() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("cancelProducto");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("totalApagar");
        datos.remove("energiaElectrica");
        datos.remove("arrayCheque");
        datos.remove("asociacionTicket");
        datos.remove("idDato");
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(new JSONObject());
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        manejo.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
    }

    private void setCantidadArticulo() {
        JSONParser parser = new JSONParser();
        try {
            JSONArray arrayFact = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            double cantidad = 0;
            for (int i = 0; i < arrayFact.size(); i++) {
                JSONObject json = (JSONObject) parser.parse(arrayFact.get(i).toString());
                cantidad += Double.parseDouble(json.get("cantidad").toString());
            }
            double cantTotal = Double.parseDouble(datos.get("nArticulos").toString());
            datos.put("nArticulos", cantTotal - cantidad);
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
    }

    private boolean eliminarAuxiliarCancelProd() {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "DELETE FROM desarrollo.auxiliar_cancel_prod";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* DATOS ELIMINADOS DEL AUXILIAR CANCELACION PRODUCTO ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private void eliminarCabecera(long id) {
        ConexionPostgres.conectar();
        String sql = "DELETE FROM desarrollo.cabecera WHERE id_dato=" + id;
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* DATOS ELIMINADOS DEL CABECERA FACTURA ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        ConexionPostgres.cerrar();
    }

    private void registrarCancelFactDet(long idRango) {
        JSONArray jsonArrayFactDet = new JSONArray();
        JSONParser parser = new JSONParser();
        //**********************************************************************
        for (int i = 0; i < FacturaDeVentaFXMLController.getDetalleArtList().size(); i++) {
            JSONObject fra = null;
            try {
                //ACTUALIZACIÓN DETALLE / DESCUENTO
                fra = (JSONObject) parser.parse(FacturaDeVentaFXMLController.getDetalleArtList().get(i).get("articulo").toString());
                FacturaDeVentaFXMLController.getDetalleArtList().get(i).put("codArticulo", Long.parseLong(fra.get("codArticulo").toString()));
            } catch (ParseException ex) {
                Logger.getLogger(CancelacionFacturaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
            FacturaDeVentaFXMLController.getDetalleArtList().get(i).put("idCancelFact", idRango);
            FacturaDeVentaFXMLController.getDetalleArtList().get(i).remove("facturaClienteCab");
            jsonArrayFactDet.add(FacturaDeVentaFXMLController.getDetalleArtList().get(i));
        }
        for (int i = 0; i < CancelacionProductoFXMLController.getDetalleArtList().size(); i++) {
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            CancelacionProductoFXMLController.getDetalleArtList().get(i).put("idCancelFact", idRango);
            CancelacionProductoFXMLController.getDetalleArtList().get(i).remove("facturaClienteCab");
            jsonArrayFactDet.add(CancelacionProductoFXMLController.getDetalleArtList().get(i));
        }
        //**********************************************************************
        String sql = "INSERT INTO desarrollo.cancel_fact_detalle(descripcion_dato, operacion, fecha) "
                + "VALUES ('" + jsonArrayFactDet + "', 'insertar', 'now()')";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION FACTURA DETALLE ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());

            }
        }
    }

}
