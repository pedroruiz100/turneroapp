/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.MotivoCancelacionProducto;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.MotivoCancelacionProductoDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class CancelacionProductoMayFXMLController extends BaseScreenController implements Initializable {

    public static List<JSONObject> detalleArtList = new ArrayList<>();

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    private JSONObject supervisor;
    boolean estadoFact = false;
    private boolean alert;
    private Date date;
    private Timestamp timestamp;
    private static TableView<JSONObject> tabla;
    private static Label labelTotalGs;
    private static TextField txtCodigo;
    private static CheckBox chkExtranjero;
    private static Label labelCantidad;
    private static ImageView imgProducto;
    private static String cant;
    private static JSONObject productoAEliminar;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    @Autowired
    private SupervisorDAO superDAO;

    @Autowired
    private MotivoCancelacionProductoDAO motivoDAO;

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private Map motivos;
    String selectMotivoInicial = "-- Seleccione un motivo --";

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private HBox hBox;
    @FXML
    private Label labelCodSupervisor;
    @FXML
    private PasswordField passwordFieldCodSupervisor;
    @FXML
    private AnchorPane anchorPaneDatosRetiro;
    @FXML
    private VBox vBoxLabel;
    @FXML
    private Label labelSupervisor;
    @FXML
    private Label labelMotivo;
    @FXML
    private VBox vBoxText;
    @FXML
    private TextField textFieldSupervisor;
    @FXML
    private ChoiceBox<String> choiceBoxMotivos;
    @FXML
    private Button btnProcesar;
    @FXML
    private AnchorPane anchorPaneCancelacionProducto;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelCant;
    @FXML
    private TextField textFieldCantidad;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        realizarCancelacion();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        FacturaDeVentaMayFXMLController.facturaCabeceraSupr = new JSONObject();
        volviendo();
    }

    @FXML
    private void anchorPaneCancelacionProductoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void choiceBoxMotivosKeyReleased(KeyEvent event) {
        keyPressCombo(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        ocultandoParametros();
        asignandoVariables();
        estadoFact = false;
        repeatFocus(passwordFieldCodSupervisor);
        choiceBoxMotivos.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            choiceBoxMotivos.show();
        });
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        FacturaDeVentaMayFXMLController.suprimirProducto(tabla, labelTotalGs, 0, labelCantidad, imgProducto, chkExtranjero, txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/CancelacionProductoMayFXML.fxml", 519, 275, false);
    }
    //NAVEGACIÓN FORMULARIOS *************************** -> -> -> -> ->

    //AVISOS AVISOS AVISOS ***************************** -> -> -> -> ->
    private void mensajeInformacion(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == KeyCode.ENTER) {
            if (!btnProcesar.isVisible()) {
                if (this.alert) {
                    verificandoSupervisor();
                }
            }
        }
        if (keyCode == KeyCode.ESCAPE) {
            FacturaDeVentaMayFXMLController.facturaCabeceraSupr = new JSONObject();
            volviendo();
        }
    }

    private void keyPressCombo(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == KeyCode.ENTER) {
            if (!estadoFact) {
                realizarCancelacion();
                estadoFact = true;
            }
        } else if (keyCode == KeyCode.ESCAPE) {
            FacturaDeVentaMayFXMLController.facturaCabeceraSupr = new JSONObject();
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void verificandoSupervisor() {
        if (!passwordFieldCodSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(passwordFieldCodSupervisor.getText()));
            if (supervisor != null) {
                mostrandoParametros();
                passwordFieldCodSupervisor.setEditable(false);
                JSONObject usuario = (JSONObject) supervisor.get("usuario");
                textFieldSupervisor.setText(usuario.get("nomUsuario").toString());
                jsonMotivoCancelaciones();
                this.alert = false;
                choiceBoxMotivos.requestFocus();
            } else {
                ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alerta = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR INCORRECTA.", cancel);
                alerta.showAndWait();
                ocultandoParametros();
                passwordFieldCodSupervisor.setText("");
                if (alerta.getResult() == cancel) {
                    alerta.close();
                } else {
                    alerta.close();
                }
            }
        } else {
            ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alerta = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR VACÍA.", cancel);
            this.alert = true;
            alerta.showAndWait();
            if (alerta.getResult() == cancel) {
                alerta.close();
            } else {
                alerta.close();
            }
        }
    }

    private void ocultandoParametros() {
        vBoxLabel.setVisible(false);
        vBoxText.setVisible(false);
        btnProcesar.setVisible(false);
    }

    private void mostrandoParametros() {
        vBoxLabel.setVisible(true);
        vBoxText.setVisible(true);
        btnProcesar.setVisible(true);
    }

    public JSONObject buscarCodSupervisiorLocal(String c) {
        try {
            JSONParser parser = new JSONParser();
            Supervisor sup = superDAO.buscarCodSup(c);
            return (JSONObject) parser.parse(gson.toJson(sup.toSupervisorDTO()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    public void generarChoiceBoxMotivo() {
        JSONParser parser = new JSONParser();
        List<MotivoCancelacionProducto> listMotivos = motivoDAO.listar();
        for (MotivoCancelacionProducto motivo : listMotivos) {
            try {
                JSONObject mot = (JSONObject) parser.parse(gson.toJson(motivo.toBDMotivoCancelacionProductoDTO()));
                choiceBoxMotivos.getItems().add(mot.get("descripcionMotivoCancelProd").toString());
                motivos.put(mot.get("descripcionMotivoCancelProd"), mot.get("idMotivoCancelProd"));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        if (!choiceBoxMotivos.getItems().isEmpty()) {
            choiceBoxMotivos.getSelectionModel().select(0);
        }
    }

    private void procesar() {
        String datoCantidad = cant;
        if (datoCantidad.equalsIgnoreCase("")) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            Alert alerts = new Alert(Alert.AlertType.WARNING, "DEBE INGRESAR LA CANTIDAD DE ARTÍCULO/S A ELIMINAR.", ok);
            alerts.showAndWait();
            if (alerts.getResult() == ok) {
                alerts.close();
            }
        } else if (Double.parseDouble(datoCantidad) <= 0) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            Alert alerts = new Alert(Alert.AlertType.WARNING, "EL ARTÍCULO A ELIMINAR NO DEBE SER MENOR O IGUAL A CERO.", ok);
            alerts.showAndWait();
            if (alerts.getResult() == ok) {
                alerts.close();
            }
        } else {
            double declarado = Double.parseDouble(cant);
            double totalATener = Double.parseDouble(cant);
            if (totalATener >= declarado) {
                JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
                JSONObject articulo = (JSONObject) detalle.get("articulo");
                if (seleccionMotivo()) {
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    Alert alerts = new Alert(Alert.AlertType.WARNING, "DEBE SELECCIONAR UN MOTIVO.", ok);
                    this.alert = true;
                    alerts.showAndWait();
                    if (alerts.getResult() == ok) {
                        alerts.close();
                    }
                } else if (procesandoCancelacion(articulo)) {
                    FacturaDeVentaMayFXMLController.suprimirProducto(tabla, labelTotalGs, declarado, labelCantidad, imgProducto, chkExtranjero, txtCodigo);
                    new Toaster().mensajeGenerico("Mensaje del Sistema", "LA CANCELACIÓN SE REALIZÓ CON ÉXITO.", "", 2);
                    if (StageSecond.getStageData().isShowing()) {
                        StageSecond.getStageData().close();
                    }
                    this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/CancelacionProductoMayFXML.fxml", 519, 275, false);
                }
            } else {
                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                Alert alerts = new Alert(Alert.AlertType.CONFIRMATION, "LA CANTIDAD DECLARADA ES SUPERIOR A LA EXISTENTE. LA CANTIDAD DE ARTÍCULO ES: " + cant, ok);
                alerts.showAndWait();
                if (alerts.getResult() == ok) {
                    alerts.close();
                }
            }
        }
    }

    private boolean seleccionMotivo() {
        boolean estado = choiceBoxMotivos.getSelectionModel().getSelectedItem().equalsIgnoreCase(selectMotivoInicial);
        return estado;
    }

    private void asignandoVariables() {
        JSONParser parser = new JSONParser();
        alert = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = DatosEnCaja.getFacturados();
                JSONObject jsonFactCab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                FacturaDeVentaMayFXMLController.setCabFactura(jsonFactCab);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
    }

    static void obtenerTable(TableView<JSONObject> tableViewFactura, Label label, JSONObject productos, Label label2, ImageView imgArt, CheckBox chk, TextField textFieldCod, String canti) {
        tabla = tableViewFactura;
        labelTotalGs = label;
        labelCantidad = label2;
        cant = canti;
        imgProducto = imgArt;
        chkExtranjero = chk;
        productoAEliminar = productos;
        txtCodigo = textFieldCod;
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonDatos = (JSONObject) parser.parse(DatosEnCaja.getDatos().toString());
            jsonDatos.put("cancelProducto", true);
            DatosEnCaja.setDatos(jsonDatos);
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            if (DatosEnCaja.getFacturados() == null) {
                manejo.setFactura(null);
            } else {
                manejo.setFactura(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            }
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            DatosEnCaja.setDatos(DatosEnCaja.getDatos());
            DatosEnCaja.setFacturados(DatosEnCaja.getFacturados());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, MOTIVO CANCELACIÓN PRODUCTO -> GET
    private void jsonMotivoCancelaciones() {
        motivos = new HashMap();
        generarChoiceBoxMotivo();
    }
    //////READ, MOTIVO CANCELACIÓN PRODUCTO -> GET

    //////READ, CLAVE SUPERIOR -> GET
    private JSONObject jsonClaveSupervisor(String c) {
        JSONObject superv = null;
        superv = buscarCodSupervisiorLocal(c);
        return superv;
    }
    //////READ, CLAVE SUPERIOR -> GET

    //////CREATE, CANCELACIÓN PRODUCTO -> POST
    private boolean procesandoCancelacion(JSONObject jsonArticulo) {
        boolean exito = false;
        FacturaDeVentaMayFXMLController.persistiendoFact(true, (long) jsonArticulo.get("idArticulo"));
        JSONObject jsonCancelacion = creandoJsonCancelacion(jsonArticulo);
        //verifica si hay conexión y si la cabecera se agrego en el servidor en caso de que no se haya registrado lo guarda de manera local 
        //para no generar error de que no existe el id_factura_cliente_cab con xxx.....
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        exito = registarCancelacionProdLocal(jsonCancelacion);
        return exito;
    }
    //////CREATE, CANCELACIÓN PRODUCTO -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////CREATE, CANCELACIÓN PRODUCTO
    private boolean registarCancelacionProdLocal(JSONObject obj) {
        boolean estado = false;
        ConexionPostgres.conectar();
        String sql1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + obj + "','cancelacionProducto', 'insertar')";
        String sql = "INSERT INTO desarrollo.auxiliar_cancel_prod (dato) VALUES (?)";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ps.setString(1, sql1);
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION PRODUCTO ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
            return true;
        }

        ConexionPostgres.cerrar();
        return estado;
    }
    //////CREATE, CANCELACIÓN PRODUCTO
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CANCELACIÓN PRODUCTO
    private JSONObject creandoJsonCancelacion(JSONObject jsonArticulo) {
        date = new Date();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        String motivoDescri = choiceBoxMotivos.getSelectionModel().getSelectedItem();
        JSONObject cancelacion = new JSONObject();
        JSONObject articulo = new JSONObject();
        articulo.put("idArticulo", FacturaVentaDatos.getIdProducto());
        JSONObject usuarioSup = (JSONObject) supervisor.get("usuario");
        JSONObject usuarioCajero = new JSONObject();
        usuarioCajero.put("idUsuario", FacturaVentaDatos.getIdCajero());
        JSONObject usuarioSupervisor = new JSONObject();
        usuarioSupervisor.put("idUsuario", usuarioSup.get("idUsuario"));
        JSONObject motivoCancelacionProducto = new JSONObject();
        motivoCancelacionProducto.put("idMotivoCancelProd", motivos.get(motivoDescri));
        JSONObject facturaClienteCab = new JSONObject();
        facturaClienteCab.put("idFacturaClienteCab", FacturaVentaDatos.getIdFacturaClienteCab());
        cancelacion.put("fechaCancelacion", timestampJSON);
        cancelacion.put("articulo", articulo);
        cancelacion.put("usuarioCajero", usuarioCajero);
        cancelacion.put("usuarioSupervisor", usuarioSupervisor);
        cancelacion.put("motivoCancelacionProducto", motivoCancelacionProducto);
        cancelacion.put("facturaClienteCab", facturaClienteCab);
        double total = 0;
        JSONObject caj = new JSONObject();
        if (!json.isNull("caja")) {
            try {
                caj = (JSONObject) parser.parse(datos.get("caja").toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        JSONObject tipoCaja = (JSONObject) caj.get("tipoCaja");
        cancelacion.put("cantidad", Double.parseDouble(cant));
        if ((long) tipoCaja.get("idTipoCaja") == 1) {//minorista...
            total = Double.parseDouble(jsonArticulo.get("precioMin").toString()) * Double.parseDouble(cant);
            cancelacion.put("precio", Integer.valueOf(jsonArticulo.get("precioMin").toString()));
            //por si acaso se agrege otro tipo de caja, de vuelta la condición...
        } else if ((long) tipoCaja.get("idTipoCaja") == 2) {//mayorista...
            total = Double.parseDouble(jsonArticulo.get("precioMay").toString()) * Double.parseDouble(cant);
            cancelacion.put("precio", Integer.valueOf(jsonArticulo.get("precioMay").toString()));
        }
        cancelacion.put("total", Math.round(total));
        return cancelacion;
    }
    //JSON CREANDO CANCELACIÓN PRODUCTO
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    private void realizarCancelacion() {
        try {
            JSONParser parser = new JSONParser();
            procesar();
            datos = DatosEnCaja.getDatos();
            fact = DatosEnCaja.getFacturados();
            JSONObject jsonFactCab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            FacturaDeVentaMayFXMLController.setCabFactura(jsonFactCab);
            FacturaDeVentaMayFXMLController.actualizandoCabFacturaLocalmente();
            long dato = recuperarIdDato();
            if (dato != 0l) {
                datos.put("idDato", dato);
            }
            eliminarCabeceraExistente();
            asignarCancelacionFacturaDetalle();

            actualizarDatosBD();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private void eliminarCabeceraExistente() {
        int num = tabla.getItems().size();
        if (num == 0) {
            ConexionPostgres.conectar();
            String sql = "DELETE FROM desarrollo.cabecera WHERE procesado=false";
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* DATOS ELIMINADOS DEL AUXILIAR ********");
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.info(ex.getLocalizedMessage());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.info(ex1.getLocalizedMessage());
                }
            }
            ConexionPostgres.cerrar();
            DatosEnCaja.getDatos().remove("sitio");
            DatosEnCaja.setFacturados(null);
        }
    }

    public static long recuperarIdDato() {
        long valor = 0l;
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.cabecera ORDER BY id_dato DESC limit 1 ";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                System.out.println("******* DATOS ELIMINADOS DEL AUXILIAR CANCELACION PRODUCTO ********");
                valor = rs.getLong("id_dato");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private void asignarCancelacionFacturaDetalle() {
        JSONParser parser = new JSONParser();
        try {
            JSONObject art = (JSONObject) parser.parse(productoAEliminar.get("articulo").toString());
            JSONObject prod = new JSONObject();
            prod.put("descripcion", productoAEliminar.get("descripcion").toString());
            prod.put("cantidad", Double.parseDouble(productoAEliminar.get("cantidad").toString()));
            prod.put("precio", Long.parseLong(productoAEliminar.get("precio").toString()));
            prod.put("poriva", Integer.parseInt(productoAEliminar.get("poriva").toString()));
            FacturaDeVentaMayFXMLController.getHmGift().remove(Long.parseLong(art.get("codArticulo").toString()));
            prod.put("codArticulo", Long.parseLong(art.get("codArticulo").toString()));
            CancelacionProductoMayFXMLController.getDetalleArtList().add(prod);
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

}
