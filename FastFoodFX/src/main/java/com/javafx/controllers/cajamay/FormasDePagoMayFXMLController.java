/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.core.domain.TarjetaConvenio;
import com.peluqueria.core.domain.TipoMoneda;
import com.peluqueria.core.domain.Vales;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.FuncionarioDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoChequeDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoEfectivoDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoFuncDAO;
import com.peluqueria.dao.RangoMonedaExtranjeraDAO;
import com.peluqueria.dao.RangoNotaCredDAO;
import com.peluqueria.dao.RangoPromoDAO;
import com.peluqueria.dao.RangoTarjetaConvenioDAO;
import com.peluqueria.dao.RangoTarjetaDAO;
import com.peluqueria.dao.RangoTarjfielDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TarjetaConvenioDAO;
import com.peluqueria.dao.TarjetaDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.ValeDAO;
import com.peluqueria.dao.impl.ClienteDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dto.ClienteDTO;
import com.peluqueria.dto.FuncionarioDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.caja.BuscarClienteFXMLController;
import com.javafx.controllers.caja.NuevoClienteFXMLController;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionParana;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Descuento;
import com.javafx.util.Fecha;
import com.javafx.util.Identity;
import com.javafx.util.MensajeFinalVenta;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXComboBox;
import com.sun.javafx.robot.FXRobot;
import com.sun.javafx.robot.FXRobotFactory;
import static java.lang.Math.toIntExact;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.InnerShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
public class FormasDePagoMayFXMLController extends BaseScreenController implements Initializable {

    static void setTxtCodigo(TextField textFieldCod) {
        txtCodigo = textFieldCod;
    }

    private static TextField txtCodigo;
    Alert alert;
    boolean alertEscEnter;
    @Autowired
    TarjetaDAO tarDAO;
    @Autowired
    ValeDAO valeDAO;
    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    @Autowired
    FuncionarioDAO funDAO;
    @Autowired
    TarjetaConvenioDAO tarConvenioDAO;
    //rangos
    @Autowired
    private RangoFuncDAO rangoFuncionarioDAO;
    @Autowired
    private RangoTarjetaDAO rangoTarjetaDAO;
    @Autowired
    private RangoTarjfielDAO rangoTarjFielDAO;
    @Autowired
    private RangoEfectivoDAO rangoEfectivoDAO;
    @Autowired
    private RangoTarjetaConvenioDAO rangoTarjetaConvenioDAO;
    @Autowired
    private RangoMonedaExtranjeraDAO rangoMonedaExtraDAO;
    @Autowired
    private RangoChequeDAO rangoChequeDAO;
    @Autowired
    private RangoNotaCredDAO rangoNotaCredDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempArtDAO;
    //rangos
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static boolean estado = true;
    private static List<JSONObject> listJsonFormaPago;
    private HashMap<String, JSONObject> hmFormaPago;
    String dtoCargado;

    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    int countFocus = 1;
    //TABLE VIEW

    //tarjeta crédito débito
    private boolean cargarTarjeta;
    private HashMap<String, Integer> hashListTarj;
    private HashMap<String, JSONObject> hashJsonComboTarjeta;
    int montoTarjCredTotal;
    int montoTarjDebTotal;
    //tarjeta crédito débito
    //vale
    private List<JSONObject> valeList;
    private boolean cargarVale;
    private HashMap<String, JSONObject> hashJsonComboVale;
    //vale
    //tarjeta convenio
    private List<JSONObject> tarjetaConvenioList;
    private boolean cargarTarjetaConvenio;
    private HashMap<String, JSONObject> hashJsonComboTarjConvenio;
    //tarjeta convenio
    //nc
    int montoNotaTotal;
    //nc
    //cheque
    int montoChequeTotal;
    //cheque
    //campos númericos
    IntegerValidator intValidator;
    private static NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos

    //parámetros total
    long total;
    long descuento;
    long totalAPagar;
    long totalAbonado;
    long donacion;
    long vuelto;
    long montoSugerido;
    //parámetros total
    //controlador descuento
    private boolean descTarjeta, descTarjetaConvenio, descDirectivo, descValePorc, descValeMonto;
    private static boolean descTarjetaFiel, descFuncionario, descPromoTemp;
    //controlador descuento
    //sumador descuento
    double descuentoFielSum;
    double descuentoFuncSum;
    double descuentoPromoNfSum;
    double descuentoPromoArtSum;
    double descuentoTarjetaSum;
    double descuentoTarjetaConvSum;
    double descuentoValePorcSum;
    double descuentoValeParesaSum;
    double descuentoGift;
    //sumador descuento
    //control, orden de llamada descuento
    boolean entradaPromo1era;
    boolean entradaFunc1era;
    boolean entradaFiel1era;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)
    private static double montoConDes10;
    private static double montoConDes5;
    private static long exenta;
    private HashMap<JSONObject, Long> hashDetallePrecioDesc;
    private HashMap<JSONObject, Long> hashDetallePrecioDescPromoArt;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)
    //promoción temporada
    private HashMap<JSONObject, Long> hashJsonPromocionDesc;
    long descuentoPromoNf;
    boolean encontradoPromoNf;
    //promoción temporada 
    //promoción temporada art.
    private HashMap<JSONObject, Long> hashJsonPromocionArtDesc;
    //promoción temporada art.
    //calculo auxiliar para descuentos, acumulativo para factura
    HashMap<JSONObject, Long> montoMap5 = new HashMap<>();
    HashMap<JSONObject, Long> montoMap10 = new HashMap<>();
    HashMap<JSONObject, Long> montoMapExe = new HashMap<>();
    //calculo auxiliar para descuentos, acumulativo para factura

    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    static boolean caida = false;
    static JSONObject formaDePago;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    boolean tarjConvActiva;
    boolean valeActivo;
    boolean estadoGift = false;

    private static JSONObject jsonFuncionario;

    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static int peso;
    private static int real;
    private static int dolar;
    boolean toggleButton;
    boolean toggleEnTarj;
    private JSONArray arrayCheque;
    boolean recursivoListen;
    private static boolean valeMayoMsjFInal;
    boolean paresaValido;
    HashMap<Long, JSONObject> hmDetallePromoObsequio;
    HashMap<Long, JSONObject> hmObsequioUlt;
    HashMap<Long, Double> hmObsequioRestoEspecial;

    boolean enterBlock;

    Toaster toaster;
    long timeToaster;

    List<Node> nodos;

    final KeyCombination altD = new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN);
    final KeyCombination altT = new KeyCodeCombination(KeyCode.T, KeyCombination.ALT_DOWN);

    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Label labelFormaDePago;
    @FXML
    private AnchorPane anchorPaneFormasPago;
    @FXML
    private AnchorPane anchorPaneTotales;
    @FXML
    private TextField textFieldDescuento;
    @FXML
    private TextField textFieldVuelto;
    @FXML
    private TabPane jfxTabPaneFormasPago;
    @FXML
    private Tab tabEfectivo;
    @FXML
    private Tab tabTarjetaCredDeb;
    @FXML
    private Tab tabMonExtr;
    @FXML
    private Tab tabCheque;
    @FXML
    private Tab tabNcs;
    @FXML
    private Tab tabVale;
    @FXML
    private Button buttonVolver;
    @FXML
    private ComboBox<String> jfxComboBoxFormaPago;
    @FXML
    private AnchorPane anchorPaneEfectivo;
    @FXML
    private TextField textFieldEfectivo;
    @FXML
    private AnchorPane anchorPaneTarjCredDeb;
    @FXML
    private GridPane gridPaneTarjCredDeb;
    @FXML
    private Label labelMontoTarjeta;
    @FXML
    private TextField textFieldMontoTarjeta;
    @FXML
    private Label labelCodAuth;
    @FXML
    private TextField textFieldCodAuth;
    @FXML
    private ComboBox<String> comboBoxTarjetas;
    @FXML
    private AnchorPane anchorPaneMonExtr;
    @FXML
    private GridPane gridPaneMonExtr;
    @FXML
    private TextField textFieldDolarCant;
    @FXML
    private TextField textFieldRealCant;
    @FXML
    private TextField textFieldPesoCant;
    @FXML
    private TextField textFieldDolarGs;
    @FXML
    private TextField textFieldRealGs;
    @FXML
    private TextField textFieldPesoGs;
    @FXML
    private AnchorPane anchorPaneCheque;
    @FXML
    private GridPane gridPaneCheque;
    @FXML
    private Label labelEntidad;
    @FXML
    private TextField textFieldEntidad;
    @FXML
    private Label labelNroCheque;
    @FXML
    private TextField textFIeldNroCheque;
    @FXML
    private Label labelMontoCheque;
    @FXML
    private TextField textFieldMonto;
    @FXML
    private AnchorPane anchorPaneNC;
    @FXML
    private GridPane gridPaneNC;
    @FXML
    private Label labelNroNotaCred;
    @FXML
    private TextField textFieldNroNotaCred;
    @FXML
    private Label labelNotaCredMonto;
    @FXML
    private TextField textFieldMontoNotaCred;
    @FXML
    private AnchorPane anchorPaneVale;
    @FXML
    private ComboBox<String> comboBoxVale;
    @FXML
    private Label labelNroVale;
    @FXML
    private TextField textFieldNroVale;
    @FXML
    private Label labelValeCI;
    @FXML
    private TextField textFieldValeCI;
    @FXML
    private Label labelValeMonto;
    @FXML
    private TextField textFieldMontoVale;
    @FXML
    private Label labelValeMontoPorc;
    @FXML
    private TextField textFieldMontoValePorc;
    @FXML
    private AnchorPane anchorPaneTableView;
    @FXML
    private TableColumn<JSONObject, String> tableColumnFormaDePago;
    @FXML
    private TableColumn<JSONObject, String> tableColumnMonto;
    @FXML
    private TableView<JSONObject> tableViewFormaDePago;
    @FXML
    private JFXButton buttonAgregar;
    @FXML
    private Label labelCampoAlPedo;
    @FXML
    private JFXComboBox<String> jfxComboBoxTipoDto;
    @FXML
    private Label labelCiTercero;
    @FXML
    private TextField textFIeldCiTercero;
    @FXML
    private Label txtDolar;
    @FXML
    private Label txtPeso;
    @FXML
    private Label txtReal;
    @FXML
    private TextField textFieldCiFuncionario;
    @FXML
    private Label labelFuncionario;
    private TextField textFieldTotal;
    @FXML
    private Label labelTotal;
    @FXML
    private Label labelDescuento;
    @FXML
    private Label labelMontoEfectivo;
    @FXML
    private Label labelCiFuncionario;
    @FXML
    private Label labelTarjeta;
    @FXML
    private Label labelDolar;
    @FXML
    private Label labelReal;
    @FXML
    private Label labelPeso;
    @FXML
    private Label labelValeOrden;
    @FXML
    private Label labelTotal1;
    @FXML
    private HBox hBoxTarjCredDeb;
    @FXML
    private Label labelVuelto;
    @FXML
    private Label labelCensura;
    @FXML
    private Label labelTotal11;
    @FXML
    private Tab tabAsoc;
    @FXML
    private AnchorPane anchorPaneAsoc;
    @FXML
    private Label labelAsocMonto;
    @FXML
    private TextField textFieldMontoAsoc;
    @FXML
    private Tab tabValeInterno;
    @FXML
    private AnchorPane anchorPaneValeInterno;
    @FXML
    private Label labelValeInternoMonto;
    @FXML
    private TextField textFieldMontoValeInterno;
    @FXML
    private Tab tabGiftCard;
    @FXML
    private AnchorPane anchorPaneGift;
    @FXML
    private Label labelGiftCod;
    @FXML
    private Label labelVerSaldo;
    @FXML
    private Label labelGiftCod2;
    @FXML
    private TextField textFieldCodGift;
    @FXML
    private Label labelSaldoGift;
    @FXML
    private TextField textFieldTotalAPagar;
    @FXML
    private Label labelGiftCod1;
    @FXML
    private TextField textFieldMontoGift;
    @FXML
    private DatePicker datePickerVigencia;

    @FXML
    private void buttonVolverAction(ActionEvent event) {
    }

    @FXML
    private void textFieldMontoTarjetaReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldCodAuthKeyReleased(KeyEvent event) {
    }

    @FXML
    private void comboBoxTarjetasMouseClicked(MouseEvent event) {
    }

    @FXML
    private void comboBoxTarjetasKeyReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldDolarCantReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldRealCantReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldPesoCantReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldDolarGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldRealGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldPesoGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldEntidadReleased(KeyEvent event) {
    }

    @FXML
    private void textFIeldNroChequeReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldMontoReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldNroNotaCredReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldMontoNotaCredReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldNroValeReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldValeCIReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldMontoValeReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldMontoValePorcReleased(KeyEvent event) {
    }

    @FXML
    private void tableViewFormaDePagoKeyReleased(KeyEvent event) {
    }

    @FXML
    private void buttonAgregarAction(ActionEvent event) {
        agregandoFormaDePago();
    }

    @FXML
    private void textFieldCiFuncionarioKeyReleased(KeyEvent event) {
        keyPressFuncionario(event);
    }

    @FXML
    private void anchorPaneFormPagoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void jfxComboBoxTipoDtoKeyReleased(KeyEvent event) {
        keyPressDto(event);
    }

    private void jfxComboBoxFormaPagoKeyReleased(KeyEvent event) {
        keyPressFormaPago(event);
    }

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        cargandoInicialVariables();
        validandoValeMay();
        cargandoComboBoxFormaPago();
        cargandoComboBoxDTO();
        jsonCargandoTarjetas();
        listenComboFormaPago();
        listenComboVale();
        listenComboDto();
        listenCampos();
        listenFocus();
        iniciandoCotizacion();

        vistaJSONObjectFormaDePago();
        calculandoTotal();
        textFieldMontoGift.setDisable(true);
        estadoGift = false;
        if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            validandoClienteFiel();
        } else {
            seteandoTotales();
            seteandoTotalAbonado();
        }
        repeatFocus(jfxComboBoxTipoDto);
        enterBlock = false;
    }

    private void cargandoInicialVariables() {
        exitoInsertarCab = false;
        exitoInsertarDet = false;
        recursivoListen = false;
        toggleEnTarj = false;
        paresaValido = false;
        textFieldTotal = new TextField();
        hmDetallePromoObsequio = new HashMap<>();
        hmObsequioRestoEspecial = new HashMap<>();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
            /*JSONObject factura;
            try {
                factura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonFact = new org.json.JSONObject(factura);
                if (!jsonFact.isNull("nroFactura")) {
                    txtNroFactura.setText(Utilidades.patternFactura(jsonFact.getString("nroFactura")));
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: (DatosEnCaja.getFacturados() != null)", ex.fillInStackTrace());
            }*/
        }
        hashJsonComboTarjConvenio = new HashMap<>();
        tarjetaConvenioList = new ArrayList<>();
        arrayCheque = new JSONArray();
        listJsonFormaPago = new ArrayList<>();
        hmFormaPago = new HashMap();
        numValidator = new NumberValidator();
        intValidator = new IntegerValidator();
        formaDePago = new JSONObject();
        montoSugerido = 0;
        labelFuncionario.setText("N/A");
        valeMayoMsjFInal = false;
        montoTarjCredTotal = 0;
        montoTarjDebTotal = 0;
        montoNotaTotal = 0;
        montoChequeTotal = 0;
        toaster = new Toaster();
        nodos = new ArrayList<>();
        nodos.add(jfxComboBoxFormaPago);
        nodos.add(jfxComboBoxTipoDto);
        nodos.add(textFieldEfectivo);
        nodos.add(textFieldCiFuncionario);
        nodos.add(comboBoxTarjetas);
        nodos.add(textFieldMontoTarjeta);
        nodos.add(textFieldCodAuth);
        nodos.add(textFieldDolarCant);
        nodos.add(textFieldRealCant);
        nodos.add(textFieldPesoCant);
        nodos.add(textFieldEntidad);
        nodos.add(textFIeldNroCheque);
        nodos.add(textFieldMonto);
        nodos.add(textFIeldCiTercero);
        nodos.add(textFieldNroNotaCred);
        nodos.add(textFieldMontoNotaCred);
        nodos.add(comboBoxVale);
        nodos.add(textFieldNroVale);
        nodos.add(textFieldMontoVale);
        nodos.add(textFieldValeCI);
        nodos.add(textFieldMontoValePorc);
        nodos.add(textFieldMontoAsoc);
        nodos.add(textFieldMontoValeInterno);
        nodos.add(textFieldCodGift);
        nodos.add(tableViewFormaDePago);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        FacturaDeVentaMayFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/FormasDePagoMayFXML.fxml", 746, 537, false);
    }

    private void navegandoAFactura() {
        FacturaDeVentaMayFXMLController.regresar(txtCodigo);
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/FormasDePagoMayFXML.fxml", 746, 537, true);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    @SuppressWarnings("element-type-mismatch")
    private void keyPress(KeyEvent event) {

        KeyCode keyCode = event.getCode();
        if (null != keyCode) {
            if (altD.match(event) || altT.match(event)) {
                if (!jfxComboBoxTipoDto.isDisable()) {
                    jfxComboBoxTipoDto.requestFocus();
                    jfxComboBoxTipoDto.show();
                }
            } else {
                switch (keyCode) {
                    case ESCAPE:
                        volviendo();
                        break;
                    case PLUS:
                        jfxComboBoxFormaPago.requestFocus();
                        jfxComboBoxFormaPago.show();
                        break;
                    case ADD:
                        jfxComboBoxFormaPago.requestFocus();
                        jfxComboBoxFormaPago.show();
                        break;
                    case DELETE:
                        if (tableViewFormaDePago.isFocused()) {
//                        if (tableViewFormaDePago.getSelectionModel().getSelectedItem().get("formaPago").toString().equalsIgnoreCase("GIFTCARD")) {
//                            jfxComboBoxFormaPago.getItems().add("GIFTCARD");
//                        }
                            hmFormaPago.remove(tableViewFormaDePago.getSelectionModel().getSelectedItem().get("formaPago"));
                            listJsonFormaPago.remove(tableViewFormaDePago.getSelectionModel().getSelectedItem());
                            if (listJsonFormaPago.isEmpty()) {
                                jfxComboBoxTipoDto.setDisable(false);
                                paresaValido = false;
                            } else {
                                JSONObject jsonValeParHM = hmFormaPago.get("VALE");
                                if (jsonValeParHM != null) {
                                    if (jsonValeParHM.get("comboBoxVale").toString().contentEquals("PARESA")) {
                                        paresaValido = true;
                                        calculandoValeParesa();
                                    } else {
                                        paresaValido = false;
                                    }
                                } else {
                                    paresaValido = false;
                                }
                            }
                            seteandoTotalAbonado();
                            actualizandoTabla();
                            if (tableViewFormaDePago.getItems().isEmpty()) {
                                jfxComboBoxTipoDto.requestFocus();
                            }
                        }
                        break;
                    case ENTER:
                        if (tabGiftCard.isSelected() && !estadoGift) {
                            numValidator = new NumberValidator();
                            if (jfxComboBoxFormaPago.isFocused()) {
                                textFieldCodGift.requestFocus();
                            } else if (textFieldCodGift.isFocused()) {
                                if (verificarGift()) {
                                    new Toaster().mensajeGenerico("Mensaje del Sistema", "YA SE HA INGRESADO LA GIFTCARD, ES NECESARIO SUPRIMIRLA Y VOLVERLA A INGRESAR..", "", 3);
                                } else {
                                    Map mapeo = buscarTarjetaGiftCard();
                                    if (!mapeo.isEmpty()) {
                                        //SEGUN BD PARANA
                                        double monto = Double.parseDouble(mapeo.get("monto").toString());
                                        long montoGift = Long.parseLong(numValidator.formatoCantidad(monto));
                                        Date fechaVigencia = Utilidades.stringToSqlDate(mapeo.get("fecha").toString());
                                        //FECHA ACTUAL
                                        Date dateActual = java.sql.Date.valueOf(LocalDate.now());
                                        if (fechaVigencia == null) {
                                            new Toaster().mensajeGenerico("Mensaje del Sistema", "LA GIFTCARD INGRESADA NO EXISTE", "", 3);
                                        } else if (dateActual.equals(fechaVigencia) || dateActual.before(fechaVigencia)) {
                                            if (montoGift == 0) {
                                                textFieldMontoGift.setDisable(true);
                                                new Toaster().mensajeGenerico("Mensaje del Sistema", "LA GIFTCARD NO POSEE MONTO DISPONIBLE PARA REALIZAR TRANSACCIONES", "", 3);
                                            } else {
                                                long montoAPagar = Long.parseLong(numValidator.numberValidator(textFieldVuelto.getText()));
                                                if (montoGift > montoAPagar) {
                                                    textFieldMontoGift.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(montoAPagar + "")));
                                                } else if (montoGift == montoAPagar) {
                                                    textFieldMontoGift.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(montoAPagar + "")));
                                                } else {
                                                    textFieldMontoGift.setText(numValidator.numberFormat("Gs ###,###.###", monto));
                                                }
                                                textFieldMontoGift.requestFocus();
                                                estadoGift = true;
                                            }
                                        } else {
                                            textFieldMontoGift.setDisable(true);
                                            textFieldMontoGift.setText("");
                                            new Toaster().mensajeGenerico("Mensaje del Sistema", "LA GIFTCARD HA FINALIZADO EL PERIODO DE USO", "", 3);
                                        }
                                    } else {
                                        new Toaster().mensajeGenerico("Mensaje del Sistema", "GIFTCARD NO DISPONIBLE!! NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR...", "", 3);
                                    }
                                }
                            } else if (textFieldMontoGift.isFocused()) {
                                textFieldCodGift.setText("");
                                textFieldCodGift.requestFocus();
                                textFieldMontoGift.setText("");
                                textFieldMontoGift.setDisable(true);
                                labelSaldoGift.setText("Gs 0");
                                datePickerVigencia.setValue(null);
                            }
                        } else if (tabGiftCard.isSelected() && Long.parseLong(numValidator.numberValidator(textFieldMontoGift.getText())) > Long.parseLong(numValidator.numberValidator(labelSaldoGift.getText()))) {
                            new Toaster().mensajeGenerico("Mensaje del Sistema", "EL MONTO INGRESADO SUPERA LO QUE EXISTE COMO SALDO..", "", 3);
//                    } else if (tabGiftCard.isSelected() && verificarGift()) {
//                        new Toaster().mensajeGenerico("Mensaje del Sistema", "YA SE HA INGRESADO LA GIFTCARD, ES NECESARIO SUPRIMIRLA Y VOLVERLA A INGRESAR..", "", 3);
                        } else {
                            //giftCard
                            if (!jfxComboBoxFormaPago.isFocused() && !jfxComboBoxTipoDto.isFocused()) {
                                if (!enterBlock) {
                                    if (textFieldEntidad.isFocused() || textFIeldCiTercero.isFocused()
                                            || textFIeldNroCheque.isFocused() || textFieldNroNotaCred.isFocused()
                                            || textFieldNroVale.isFocused() || textFieldValeCI.isFocused()) {
                                        FXRobot robot = FXRobotFactory.createRobot(this.getRoot().getScene());
                                        robot.keyPress(javafx.scene.input.KeyCode.TAB);
                                    } else if (!agregandoFormaDePago()) {
                                        if (!textFieldCiFuncionario.getText().isEmpty()
                                                && jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                                            textFieldCiFuncionario.requestFocus();
                                        } else {
                                            FXRobot robot = FXRobotFactory.createRobot(this.getRoot().getScene());
                                            robot.keyPress(javafx.scene.input.KeyCode.TAB);
                                        }
                                    }
                                }
                            } else if (jfxComboBoxTipoDto.isFocused()) {
                                switch (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem()) {
                                    case "FUNCIONARIO":
                                        textFieldEfectivo.requestFocus();
                                        break;
                                    case "FIEL / PROMO":
                                        jfxComboBoxFormaPago.requestFocus();
//                                    textFieldEfectivo.requestFocus();
                                        break;
                                    case "SÁB. COOP.":
                                        comboBoxTarjetas.requestFocus();
                                        comboBoxTarjetas.show();
                                        break;
                                    case "DESC. FUNCIONARIOS PUBLICOS Y JUBILADOS":
                                        jfxComboBoxFormaPago.requestFocus();
                                        jfxComboBoxFormaPago.show();
                                        break;
                                    case "FEUCCA":
                                        jfxComboBoxFormaPago.requestFocus();
                                        jfxComboBoxFormaPago.show();
                                        break;
                                    case "DESC. EXA SALESIANO":
                                        jfxComboBoxFormaPago.requestFocus();
                                        jfxComboBoxFormaPago.show();
                                        break;
                                    default:
                                        break;
                                }
                            } else if (jfxComboBoxFormaPago.isFocused()) {
                                if (jfxComboBoxFormaPago != null) {
                                    switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
                                        case "EFECTIVO":
                                            textFieldEfectivo.requestFocus();
                                            break;
                                        case "CHEQUE":
                                            textFieldEntidad.requestFocus();
                                            break;
                                        case "TARJETAS":
                                            comboBoxTarjetas.requestFocus();
                                            break;
                                        case "MONEDA EXTRANJERA":
                                            textFieldDolarCant.requestFocus();
                                            break;
                                        case "NOTA DE CRÉDITO":
                                            textFieldNroNotaCred.requestFocus();
                                            break;
                                        case "VALE":
                                            comboBoxVale.requestFocus();
                                            break;
                                        case "ASOCIACIÓN":
                                            textFieldMontoAsoc.requestFocus();
                                            break;
                                        case "VALE INTERNO (FUNCIONARIO)":
                                            textFieldMontoValeInterno.requestFocus();
                                            break;
                                        case "GIFTCARD":
                                            textFieldCodGift.requestFocus();
                                            break;
                                        default:
                                            mensajePopup("¡AVISO!", "FORMA DE PAGO NO VÁLIDA", "", 2);
                                            break;
                                    }
                                } else {
                                    mensajePopup("¡AVISO!", "FORMA DE PAGO NO VÁLIDA", "", 2);
                                }
                            }
                        }
                        break;

                    default:
                        break;
                }

            }
        }
    }

    private void keyPressDto(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (null != keyCode) {
            switch (keyCode) {
                case SPACE:
                    jfxComboBoxTipoDto.show();
                    break;
            }
        }
    }

    private void keyPressFormaPago(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (null != keyCode) {
            switch (keyCode) {
                case SPACE:
                    jfxComboBoxFormaPago.show();
                    break;
            }
        }
    }

    private void keyPressFuncionario(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (null != keyCode) {
            switch (keyCode) {
                case ENTER:
                    buscandoFuncionario();
                    break;
                case DELETE:
                    labelFuncionario.setText("N/A");
                    break;
                default:
                    break;
            }
        }
    }

    private void listenCampos() {
        comboBoxTarjetas.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if (!recursivoListen) {
                    if (!tarjConvActiva && !valeActivo && !hmFormaPago.containsKey("VALE")
                            && !hmFormaPago.containsKey("ASOCIACIÓN") && !hmFormaPago.containsKey("GIFTCARD")) {
                        validandoTarjetaListen();
                    } else {
                        if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                            if (validandoTarjetaConvenioTarjCredDeb()) {
                                if (tarjConvActiva) {
                                    mensajePopup("¡AVISO TARJETA!", "LA TARJETA SELECCIONADA\nES INCOMPATIBLE CON LA TARJETA CONVENIO CARGADA\nYA QUE AMBAS DISPONEN DTO.", "", 2);
                                } else {
                                    mensajePopup("¡AVISO TARJETA!", "LA TARJETA SELECCIONADA\nES INCOMPATIBLE CON EL VALE U ORDEN DE COMPRA CARGADO.", "", 2);
                                }
                                textFieldCodAuth.setText("");
                                textFieldMontoTarjeta.setText("");
                                recursivoListen = true;
                                comboBoxTarjetas.getSelectionModel().select(0);
                            }
                        }
                    }
                } else {
                    recursivoListen = false;
                }
            }
        });

        textFIeldNroCheque.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(newValue);
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(oldValue);
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFIeldNroCheque.setText(oldValue);
                        textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                    });
                }
            }
        });

        textFieldCodAuth.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(numValidator.numberValidator(newValue));
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(numValidator.numberValidator(oldValue));
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                }
            }
        });

        textFieldNroNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    if (newValue.length() < 21) {
                        Platform.runLater(() -> {
                            textFieldNroNotaCred.setText(numValidator.numberValidator(newValue));
                            textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldNroNotaCred.setText(oldValue);
                            textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                            mensajePopup("¡AVISO!", "EXCEDIÓ LA LONGITUD DEL CAMPO", "", 2);
                        });
                    }
                } else {
                    Platform.runLater(() -> {
                        textFieldNroNotaCred.setText(numValidator.numberValidator(oldValue));
                        textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                    });
                }
            }
        });

        textFieldNroVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (newValue.length() < 31) {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(newValue);
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(oldValue);
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                        mensajePopup("¡AVISO!", "EXCEDIÓ LA LONGITUD DEL CAMPO", "", 2);
                    });
                }
            }
            long montoVale = 0;
            long montoMulti = 1;
            if (!textFieldNroVale.getText().isEmpty()) {
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-0");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-1");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-2");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-3");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-4");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-5");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-6");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-7");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-8");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-9");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/0");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/1");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/2");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/3");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/4");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/5");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/6");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/7");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/8");
                montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/9");
                if (montoMulti < 1) {
                    montoMulti = 1;
                }
            }
            switch (comboBoxVale.getSelectionModel().getSelectedItem()) {
                case "CELLULAR":
                    montoVale = 125000 * montoMulti;
                    break;
                case "ESSEN":
                    montoVale = 400000 * montoMulti;
                    break;
                case "FRIGOMERC":
                    montoVale = 100000 * montoMulti;
                    break;
                case "INSPECTORATE":
                    montoVale = 200000 * montoMulti;
                    break;
                case "PARESA":
                    montoVale = 715000 * montoMulti;
                    break;
                default:
                    montoVale = 400000 * montoMulti;
                    break;
            }
            textFieldMontoVale.setText(String.valueOf(montoVale));
        });

        textFieldEntidad.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (newValue.length() < 21) {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(newValue);
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(oldValue);
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                        mensajePopup("¡AVISO!", "EXCEDIÓ LA LONGITUD DEL CAMPO", "", 2);
                    });
                }
            }
        });

        textFIeldCiTercero.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (newValue.length() < 21) {
                    Platform.runLater(() -> {
                        textFIeldCiTercero.setText(newValue);
                        textFIeldCiTercero.positionCaret(textFIeldCiTercero.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFIeldCiTercero.setText(oldValue);
                        textFIeldCiTercero.positionCaret(textFIeldCiTercero.getLength());
                        mensajePopup("¡AVISO!", "EXCEDIÓ LA LONGITUD DEL CAMPO", "", 2);
                    });
                }
            }
        });

        textFieldCodGift.textProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> {
                if (newValue.equals("")) {
                    labelSaldoGift.setText("Gs 0");
                    datePickerVigencia.setValue(null);
                    textFieldMontoGift.setText("");
                }
            });
        });

        textFieldValeCI.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (newValue.length() < 16) {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(newValue);
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(oldValue);
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                        mensajePopup("¡AVISO!", "EXCEDIÓ LA LONGITUD DEL CAMPO", "", 2);
                    });
                }
            }
        });

        textFieldMontoValePorc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        if (Integer.valueOf(newValue) < 31 && Integer.valueOf(newValue) > 0) {
                            Platform.runLater(() -> {
                                textFieldMontoValePorc.setText(newValue);
                                textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                            });
                        } else {
                            textFieldMontoValePorc.setText(oldValue);
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoValePorc.setText(oldValue);
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldMontoValePorc.setText(oldValue);
                        textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                    });
                }
            } else {
            }
        });

        textFieldMontoVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                long montoVale = 0;
                long montoMulti = 1;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (!textFieldNroVale.getText().isEmpty()) {
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-0");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-1");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-2");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-3");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-4");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-5");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-6");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-7");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-8");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "-9");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/0");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/1");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/2");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/3");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/4");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/5");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/6");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/7");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/8");
                            montoMulti = montoMulti + StringUtils.countMatches(textFieldNroVale.getText(), "/9");
                            if (montoMulti < 1) {
                                montoMulti = 1;
                            }
                        }
                        switch (comboBoxVale.getSelectionModel().getSelectedItem()) {
                            case "CELLULAR":
                                montoVale = 125000 * montoMulti;
                                break;
                            case "ESSEN":
                                montoVale = 400000 * montoMulti;
                                break;
                            case "FRIGOMERC":
                                montoVale = 100000 * montoMulti;
                                break;
                            case "INSPECTORATE":
                                montoVale = 200000 * montoMulti;
                                break;
                            case "PARESA":
                                montoVale = 715000 * montoMulti;
                                break;
                            default:
                                montoVale = 400000 * montoMulti;
                                break;
                        }
                        if (lim > 0 && lim <= montoVale) {
                            limite = false;
                        } else {
                            mensajePopup("¡AVISO VALE!", "MONTO LÍMITE "
                                    + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(montoVale))), "", 2);
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(param);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText("");
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoVale.setText(param);
                                    textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                    calculandoValeParesa();
                                    seteandoTotalAbonado();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoVale.setText("");
                                textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                calculandoValeParesa();
                                seteandoTotalAbonado();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(oldValue);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoVale.setText(param);
                        textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                        calculandoValeParesa();
                        seteandoTotalAbonado();
                    });
                }
            }
        });

        textFieldEfectivo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(param);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText("");
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldEfectivo.setText(param);
                                    textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldEfectivo.setText("");
                                textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(oldValue);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldEfectivo.setText(param);
                        textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                    });
                }
            }
        });
        textFieldMontoGift.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoGift.setText(param);
                            textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoGift.setText("");
                            textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoGift.setText(param);
                                    textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoGift.setText("");
                                textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoGift.setText(oldValue);
                            textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoGift.setText(param);
                        textFieldMontoGift.positionCaret(textFieldMontoGift.getLength());
                    });
                }
            }
        });

        textFieldDolarCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("US$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(param);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText("");
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("US$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldDolarCant.setText(param);
                                    textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                    cotizacionMoneda();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldDolarCant.setText("");
                                textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                cotizacionMoneda();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(oldValue);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldDolarCant.setText(param);
                        textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                        cotizacionMoneda();
                    });
                }
            }
        });

        textFieldPesoCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(param);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText("");
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldPesoCant.setText(param);
                                    textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                    cotizacionMoneda();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldPesoCant.setText("");
                                textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                cotizacionMoneda();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(oldValue);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldPesoCant.setText(param);
                        textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                        cotizacionMoneda();
                    });
                }
            } else {
                if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                }
            }
        });

        textFieldRealCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("R$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(param);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText("");
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("R$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldRealCant.setText(param);
                                    textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                    cotizacionMoneda();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldRealCant.setText("");
                                textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                cotizacionMoneda();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(oldValue);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldRealCant.setText(param);
                        textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                        cotizacionMoneda();
                    });
                }
            } else {
                if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                }
            }
        });

        textFieldMontoTarjeta.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(param);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText("");
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoTarjeta.setText(param);
                                    textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoTarjeta.setText("");
                                textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(oldValue);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoTarjeta.setText(param);
                        textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                    });
                }
            }
        });

        textFieldMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMonto.setText(param);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText("");
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMonto.setText(param);
                                    textFieldMonto.positionCaret(textFieldMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMonto.setText("");
                                textFieldMonto.positionCaret(textFieldMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText(oldValue);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMonto.setText(param);
                        textFieldMonto.positionCaret(textFieldMonto.getLength());
                    });
                }
            }
        });

        textFieldMontoNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(param);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText("");
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoNotaCred.setText(param);
                                    textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoNotaCred.setText("");
                                textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(oldValue);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoNotaCred.setText(param);
                        textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                    });
                }
            }
        });

        textFieldMontoAsoc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoAsoc.setText(param);
                            textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoAsoc.setText("");
                            textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoAsoc.setText(param);
                                    textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoAsoc.setText("");
                                textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoAsoc.setText(oldValue);
                            textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoAsoc.setText(param);
                        textFieldMontoAsoc.positionCaret(textFieldMontoAsoc.getLength());
                    });
                }
            }
        });

        textFieldMontoValeInterno.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoValeInterno.setText(param);
                            textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoValeInterno.setText("");
                            textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoValeInterno.setText(param);
                                    textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoValeInterno.setText("");
                                textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoValeInterno.setText(oldValue);
                            textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoValeInterno.setText(param);
                        textFieldMontoValeInterno.positionCaret(textFieldMontoValeInterno.getLength());
                    });
                }
            }
        });
    }

    private void listenComboFormaPago() {
        jfxComboBoxFormaPago.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
                    case "EFECTIVO":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(false);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        if (!paresaValido) {
                            if (hmFormaPago.containsKey("VALE")
                                    || hmFormaPago.containsKey("ASOCIACIÓN") || hmFormaPago.containsKey("GIFTCARD")) {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")) {
                                if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                        invalidandoVale();
                                        validandoClienteFiel();
                                    } else {
                                        seteandoTotales();
                                        seteandoTotalAbonado();
                                    }
                                } else {
                                    invalidandoVale();
                                    validandoClienteFiel();
                                }
                            }
                        } else {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "CHEQUE":
                        tabCheque.setDisable(false);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabCheque);
                        if (!paresaValido) {
                            if (hmFormaPago.containsKey("VALE")
                                    || hmFormaPago.containsKey("ASOCIACIÓN") || hmFormaPago.containsKey("GIFTCARD")) {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")) {
                                if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                        invalidandoVale();
                                        validandoClienteFiel();
                                    } else {
                                        seteandoTotales();
                                        seteandoTotalAbonado();
                                    }
                                } else {
                                    invalidandoVale();
                                    validandoClienteFiel();
                                }
                            }
                        } else {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "TARJETAS":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabTarjetaCredDeb);
                        if (!paresaValido) {
                            if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("ASOCIACIÓN")) {
                            } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")) {
                                if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("ASOCIACIÓN") || hmFormaPago.containsKey("GIFTCARD")) {
                                    seteandoTotales();
                                    seteandoTotalAbonado();
                                } else {
                                    if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                        if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                            invalidandoVale();
                                            validandoClienteFiel();
                                        } else {
                                            seteandoTotales();
                                            seteandoTotalAbonado();
                                        }
                                    } else {
                                        invalidandoVale();
                                        validandoClienteFiel();
                                    }
                                }
                            }
                        } else {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "MONEDA EXTRANJERA":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(false);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabMonExtr);
                        if (!paresaValido) {
                            if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("ASOCIACIÓN")
                                    || hmFormaPago.containsKey("GIFTCARD")) {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")) {
                                if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                        invalidandoVale();
                                        validandoClienteFiel();
                                    } else {
                                        seteandoTotales();
                                        seteandoTotalAbonado();
                                    }
                                } else {
                                    invalidandoVale();
                                    validandoClienteFiel();
                                }
                            }
                        } else {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "NOTA DE CRÉDITO":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(false);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabNcs);
                        if (!paresaValido) {
                            if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("ASOCIACIÓN")
                                    || hmFormaPago.containsKey("GIFTCARD")) {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")) {
                                if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                        invalidandoVale();
                                        validandoClienteFiel();
                                    } else {
                                        seteandoTotales();
                                        seteandoTotalAbonado();
                                    }
                                } else {
                                    invalidandoVale();
                                    validandoClienteFiel();
                                }
                            }
                        } else {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "VALE":
                        if (comboBoxVale.getItems().size() < 2) {
                            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                                mensajePopupVale("¡AVISO VALE!", "REGISTRE CLIENTE PARA VALE MAYORISTA", "", 2);
                            } else {
                                mensajePopupVale("¡AVISO VALE!", "NO DISPONE DE ARTÍCULOS VÁLIDOS", "", 2);
                            }
                        }
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(false);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabVale);
                        comboBoxVale.getSelectionModel().select(0);
                        textFieldNroVale.setText("");
                        textFieldNroVale.setDisable(true);
                        textFieldValeCI.setText("");
                        textFieldValeCI.setDisable(true);
                        textFieldMontoVale.setText("");
                        textFieldMontoVale.setDisable(true);
                        textFieldMontoValePorc.setText("");
                        textFieldMontoValePorc.setDisable(true);
//                        validandoVale("VALE");
                        if (paresaValido) {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        } else {
                            seteandoTotales();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "ASOCIACIÓN":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(false);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabAsoc);
//                        validandoVale("ASOCIACIÓN");
                        if (paresaValido) {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        } else {
                            seteandoTotales();
                            seteandoTotalAbonado();
                        }
                        break;
                    case "VALE INTERNO (FUNCIONARIO)":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(false);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabValeInterno);
                        if (paresaValido) {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        } else {
                            //bajada cómo parámetro, en caso del día especial
                            if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FIEL / PROMO")
                                    && (Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                invalidandoVale();
                                validandoClienteFiel();
                            } else {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            }
                        }
                        break;
                    case "GIFTCARD":
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabGiftCard.setDisable(false);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabGiftCard);
                        if (paresaValido) {
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        } else {
                            seteandoTotales();
                            seteandoTotalAbonado();
                        }
                        break;
                    default:
                        mensajePopup("¡AVISO!", "FORMA DE PAGO NO VÁLIDA", "", 2);
                        break;
                }
            }
        });

    }

    private void listenComboVale() {
        comboBoxVale.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                visualizandoCamposVale();
            }
        });
    }

    private void listenComboDto() {
        jfxComboBoxTipoDto.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                switch (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem()) {
                    case "FIEL / PROMO":
                        jsonCargandoTarjetas();
                        invalidandoTarjetaConvenio();
                        comboBoxTarjetas.getSelectionModel().select(0);
                        tabCheque.setDisable(false);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(false);
                        tabNcs.setDisable(false);
                        tabVale.setDisable(false);
                        tabEfectivo.setDisable(false);
                        jfxComboBoxFormaPago.setDisable(false);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        labelFuncionario.setVisible(false);
                        textFieldCiFuncionario.setVisible(false);
                        labelCiFuncionario.setVisible(false);
                        validandoClienteFiel();
                        break;
                    case "FUNCIONARIO":
                        jsonCargandoTarjetas();
                        invalidandoTarjetaConvenio();
                        comboBoxTarjetas.getSelectionModel().select(0);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(true);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(false);
                        jfxComboBoxFormaPago.getSelectionModel().select("EFECTIVO");
                        jfxComboBoxFormaPago.setDisable(true);
                        labelFuncionario.setVisible(true);
                        textFieldCiFuncionario.setVisible(true);
                        labelCiFuncionario.setVisible(true);
                        validandoFuncionario();
                        break;
                    case "SÁB. COOP.":
                        jsonCargandoTarjetas();
                        invalidandoTarjetaConvenio();
                        tabCheque.setDisable(true);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(true);
                        tabNcs.setDisable(true);
                        tabVale.setDisable(true);
                        tabEfectivo.setDisable(true);
                        tabAsoc.setDisable(true);
                        tabValeInterno.setDisable(true);
                        tabGiftCard.setDisable(true);
                        jfxComboBoxFormaPago.getSelectionModel().select("TARJETAS");
                        jfxComboBoxFormaPago.setDisable(true);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabTarjetaCredDeb);
                        labelFuncionario.setVisible(false);
                        textFieldCiFuncionario.setVisible(false);
                        labelCiFuncionario.setVisible(false);
                        seteandoTotales();
                        seteandoTotalAbonado();
                        break;
                    case "DESC. FUNCIONARIOS PUBLICOS Y JUBILADOS":
                        jsonCargandoTarjetas();
                        comboBoxTarjetas.getSelectionModel().select(0);
                        tabCheque.setDisable(false);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(false);
                        tabNcs.setDisable(false);
                        tabVale.setDisable(false);
                        tabEfectivo.setDisable(false);
                        tabAsoc.setDisable(false);
                        tabValeInterno.setDisable(false);
                        tabGiftCard.setDisable(false);
                        jfxComboBoxFormaPago.getSelectionModel().select("EFECTIVO");
                        jfxComboBoxFormaPago.setDisable(false);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        labelFuncionario.setVisible(false);
                        textFieldCiFuncionario.setVisible(false);
                        labelCiFuncionario.setVisible(false);
                        validandoTarjetaConvenio();
                        break;
                    case "FEUCCA":
                        jsonCargandoTarjetas();
                        comboBoxTarjetas.getSelectionModel().select(0);
                        tabCheque.setDisable(false);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(false);
                        tabNcs.setDisable(false);
                        tabVale.setDisable(false);
                        tabEfectivo.setDisable(false);
                        tabAsoc.setDisable(false);
                        tabValeInterno.setDisable(false);
                        tabGiftCard.setDisable(false);
                        jfxComboBoxFormaPago.getSelectionModel().select("EFECTIVO");
                        jfxComboBoxFormaPago.setDisable(false);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        labelFuncionario.setVisible(false);
                        textFieldCiFuncionario.setVisible(false);
                        labelCiFuncionario.setVisible(false);
                        validandoTarjetaConvenio();
                        break;
                    case "DESC. EXA SALESIANO":
                        jsonCargandoTarjetas();
                        comboBoxTarjetas.getSelectionModel().select(0);
                        tabCheque.setDisable(false);
                        tabTarjetaCredDeb.setDisable(false);
                        tabMonExtr.setDisable(false);
                        tabNcs.setDisable(false);
                        tabVale.setDisable(false);
                        tabEfectivo.setDisable(false);
                        tabAsoc.setDisable(false);
                        tabValeInterno.setDisable(false);
                        tabGiftCard.setDisable(false);
                        jfxComboBoxFormaPago.getSelectionModel().select("EFECTIVO");
                        jfxComboBoxFormaPago.setDisable(false);
                        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
                        labelFuncionario.setVisible(false);
                        textFieldCiFuncionario.setVisible(false);
                        labelCiFuncionario.setVisible(false);
                        validandoTarjetaConvenio();
                        break;
                    default:
                        break;
                }
            }
        });
    }

    //urgente tiene que ser más práctico... hash map
    private void listenFocus() {
        jfxComboBoxFormaPago.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            jfxComboBoxFormaPago.show();
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            jfxComboBoxFormaPago.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        jfxComboBoxTipoDto.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            jfxComboBoxTipoDto.show();
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            jfxComboBoxTipoDto.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldEfectivo.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldEfectivo.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldCiFuncionario.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldCiFuncionario.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        comboBoxTarjetas.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            comboBoxTarjetas.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoTarjeta.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoTarjeta.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldCodAuth.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldCodAuth.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldDolarCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldDolarCant.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldRealCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldRealCant.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldPesoCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldPesoCant.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldEntidad.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldEntidad.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFIeldNroCheque.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFIeldNroCheque.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMonto.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMonto.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFIeldCiTercero.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFIeldCiTercero.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldNroNotaCred.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldNroNotaCred.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoNotaCred.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoNotaCred.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        comboBoxVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            comboBoxVale.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
            comboBoxVale.show();
        });

        textFieldNroVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldNroVale.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoVale.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldValeCI.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldValeCI.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoValePorc.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoValePorc.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoAsoc.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoAsoc.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldMontoValeInterno.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldMontoValeInterno.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        textFieldCodGift.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            textFieldCodGift.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });

        tableViewFormaDePago.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            for (Node nodo : nodos) {
                nodo.setEffect(null);
            }
            tableViewFormaDePago.setEffect(new InnerShadow(BlurType.THREE_PASS_BOX, Color.DARKBLUE, 15, 0, 0, 0));
        });
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            } else {
                repeatShow();
            }
        });
    }

    private void repeatShow() {
        Platform.runLater(() -> {
            jfxComboBoxTipoDto.show();
            if (!jfxComboBoxTipoDto.isShowing()) {
                jfxComboBoxTipoDto.show();
                repeatShow();
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAdv(String msj) {
        alertEscEnter = true;
        alert = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        anchorPane.setEffect(new BoxBlur(3, 3, 3));
        alert.showAndWait();
        anchorPane.setEffect(null);
        if (alert.getResult() == ButtonType.OK) {
            alert.close();
        }
    }

    private void mensajePopup(String titulo, String msj, String pathImage, int time) {
        if (timeToaster == 0) {
            toaster.mensajeGenerico(titulo, msj, pathImage, time);
            timeToaster = System.currentTimeMillis();
        } else {
            if ((System.currentTimeMillis() - timeToaster) >= 2000) {
                toaster.mensajeGenerico(titulo, msj, pathImage, time);
                timeToaster = System.currentTimeMillis();
            }
        }
    }

    private void mensajePopupVale(String titulo, String msj, String pathImage, int time) {
        if (timeToaster == 0) {
            toaster.mensajeGenericoRight(titulo, msj, pathImage, time);
            timeToaster = System.currentTimeMillis();
        } else {
            if ((System.currentTimeMillis() - timeToaster) >= 2000) {
                toaster.mensajeGenericoRight(titulo, msj, pathImage, time);
                timeToaster = System.currentTimeMillis();
            }
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //CARGA DE COMBOS, CARGA DE COMBOS ************************** -> -> -> -> ->
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void cargandoComboBoxFormaPago() {
        if (!jfxComboBoxFormaPago.getItems().isEmpty()) {
            jfxComboBoxFormaPago.getItems().clear();
        }
        jfxComboBoxFormaPago.getItems().add("EFECTIVO");
        jfxComboBoxFormaPago.getItems().add("TARJETAS");
        jfxComboBoxFormaPago.getItems().add("MONEDA EXTRANJERA");
        jfxComboBoxFormaPago.getItems().add("CHEQUE");
        jfxComboBoxFormaPago.getItems().add("NOTA DE CRÉDITO");
        jfxComboBoxFormaPago.getItems().add("VALE");
        jfxComboBoxFormaPago.getItems().add("VALE INTERNO (FUNCIONARIO)");
        jfxComboBoxFormaPago.getItems().add("ASOCIACIÓN");
        jfxComboBoxFormaPago.getItems().add("GIFTCARD");
        new FormasDePagoMayFXMLController.AutoCompleteComboBoxListenerFP<>(jfxComboBoxFormaPago);
        jfxComboBoxFormaPago.getSelectionModel().select("EFECTIVO");
    }

    private void cargandoComboBoxDTO() {
        jfxComboBoxTipoDto.getItems().add("FIEL / PROMO");
        jfxComboBoxTipoDto.getItems().add("FUNCIONARIO");
        jfxComboBoxTipoDto.getItems().add("SÁB. COOP.");
        generarComboTarjetaConvenioLocal();
        jfxComboBoxTipoDto.getSelectionModel().select(0);
        textFieldCiFuncionario.setVisible(false);
        labelFuncionario.setVisible(false);
        labelCiFuncionario.setVisible(false);
    }
    //CARGA DE COMBOS, CARGA DE COMBOS ************************** -> -> -> -> ->

    private void cotizacionMoneda() {
        if (cotizacionList != null) {
            if (!textFieldDolarCant.getText().contentEquals("")) {
                int dolarCant = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
                int dolarConversion = dolarCant * dolar;
                textFieldDolarGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(dolarConversion))));
            } else {
                textFieldDolarGs.setText("");
            }
            if (!textFieldPesoCant.getText().contentEquals("")) {
                int pesoCant = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
                int pesoConversion = pesoCant * peso;
                textFieldPesoGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(pesoConversion))));
            } else {
                textFieldPesoGs.setText("");
            }
            if (!textFieldRealCant.getText().contentEquals("")) {
                int realCant = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
                int realConversion = realCant * real;
                textFieldRealGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(realConversion))));
            } else {
                textFieldRealGs.setText("");
            }
        } else {
            textFieldDolarGs.setText("");
            textFieldPesoGs.setText("");
            textFieldRealGs.setText("");
        }
    }

    @SuppressWarnings("empty-statement")
    private boolean agregandoFormaDePago() {
        if (validandoAgregarFormaPago()) {
            JSONObject jsonFormaPago = new JSONObject();
            jsonFormaPago.put("formaPago", jfxComboBoxFormaPago.getSelectionModel().getSelectedItem());
            switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
                case "EFECTIVO":
                    if (hmFormaPago.containsKey("EFECTIVO")) {
                        int monto = Integer.valueOf(hmFormaPago.get("EFECTIVO").get("textFieldEfectivo").toString())
                                + Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
                        jsonFormaPago.put("monto", String.valueOf(monto));
                        jsonFormaPago.put("textFieldEfectivo", String.valueOf(monto));
                    } else {
                        jsonFormaPago.put("monto", numValidator.numberValidator(textFieldEfectivo.getText()));
                        jsonFormaPago.put("textFieldEfectivo", numValidator.numberValidator(textFieldEfectivo.getText()));
                    }
                    break;
                case "TARJETAS":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoTarjeta.getText()));
                    jsonFormaPago.put("comboBoxTarjetas", comboBoxTarjetas.getSelectionModel().getSelectedItem());
                    jsonFormaPago.put("textFieldMontoTarjeta", numValidator.numberValidator(textFieldMontoTarjeta.getText()));
                    jsonFormaPago.put("textFieldCodAuth", textFieldCodAuth.getText());
                    break;
                case "MONEDA EXTRANJERA":
                    if (!textFieldDolarGs.getText().isEmpty()) {
                        jsonFormaPago.put("monto", numValidator.numberValidator(textFieldDolarGs.getText()));
                        jsonFormaPago.put("textFieldDolarCant", numValidator.numberValidator(textFieldDolarCant.getText()));
                        jsonFormaPago.put("textFieldDolarGs", numValidator.numberValidator(textFieldDolarGs.getText()));
                    } else if (!textFieldRealGs.getText().isEmpty()) {
                        jsonFormaPago.put("monto", numValidator.numberValidator(textFieldRealGs.getText()));
                        jsonFormaPago.put("textFieldRealCant", numValidator.numberValidator(textFieldRealCant.getText()));
                        jsonFormaPago.put("textFieldRealGs", numValidator.numberValidator(textFieldRealGs.getText()));
                    } else if (!textFieldPesoGs.getText().isEmpty()) {
                        jsonFormaPago.put("monto", numValidator.numberValidator(textFieldPesoGs.getText()));
                        jsonFormaPago.put("textFieldPesoCant", numValidator.numberValidator(textFieldPesoCant.getText()));
                        jsonFormaPago.put("textFieldPesoGs", numValidator.numberValidator(textFieldPesoGs.getText()));
                    }
                    break;
                case "CHEQUE":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMonto.getText()));
                    jsonFormaPago.put("textFieldEntidad", textFieldEntidad.getText());
                    jsonFormaPago.put("textFIeldNroCheque", textFIeldNroCheque.getText());
                    jsonFormaPago.put("textFieldMonto", numValidator.numberValidator(textFieldMonto.getText()));
                    jsonFormaPago.put("textFIeldCiTercero", textFIeldCiTercero.getText());
                    break;
                case "NOTA DE CRÉDITO":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoNotaCred.getText()));
                    jsonFormaPago.put("textFieldNroNotaCred", textFieldNroNotaCred.getText());
                    jsonFormaPago.put("textFieldMontoNotaCred", numValidator.numberValidator(textFieldMontoNotaCred.getText()));
                    break;
                case "VALE":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoVale.getText()));
                    jsonFormaPago.put("comboBoxVale", comboBoxVale.getSelectionModel().getSelectedItem());
                    jsonFormaPago.put("textFieldNroVale", textFieldNroVale.getText());
                    jsonFormaPago.put("textFieldMontoVale", numValidator.numberValidator(textFieldMontoVale.getText()));
                    jsonFormaPago.put("textFieldValeCI", textFieldValeCI.getText());
                    jsonFormaPago.put("textFieldMontoValePorc", numValidator.numberValidator(textFieldMontoValePorc.getText()));
                    break;
                case "VALE INTERNO (FUNCIONARIO)":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoValeInterno.getText()));
                    jsonFormaPago.put("textFieldMontoValeInterno", numValidator.numberValidator(textFieldMontoValeInterno.getText()));
                    break;
                case "ASOCIACIÓN":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoAsoc.getText()));
                    jsonFormaPago.put("textFieldMontoAsoc", numValidator.numberValidator(textFieldMontoAsoc.getText()));
                    break;
                case "GIFTCARD":
                    jsonFormaPago.put("monto", numValidator.numberValidator(textFieldMontoGift.getText()));
                    jsonFormaPago.put("textFieldGiftcard", numValidator.numberValidator(textFieldMontoGift.getText()));
                    jsonFormaPago.put("codigo", textFieldCodGift.getText());
                    jsonFormaPago.put("saldo", numValidator.numberValidator(labelSaldoGift.getText()));
                    break;
                default:
                    break;
            }
            //con moneda extranjera queda solo el último, en caso de haber varios
            hmFormaPago.put(jfxComboBoxFormaPago.getSelectionModel().getSelectedItem(), jsonFormaPago);
            if (jsonFormaPago.get("formaPago").toString().contentEquals("EFECTIVO")) {
                for (JSONObject jSONObject : listJsonFormaPago) {
                    if (jsonFormaPago.get("formaPago").toString().contentEquals(jSONObject.get("formaPago").toString())) {
                        listJsonFormaPago.remove(jSONObject);
                        break;
                    }
                }
                listJsonFormaPago.add(jsonFormaPago);
            } else if (jsonFormaPago.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA")) {
                for (JSONObject jSONObject : listJsonFormaPago) {
                    if (jsonFormaPago.get("formaPago").toString().contentEquals(jSONObject.get("formaPago").toString())
                            && jSONObject.containsKey("textFieldDolarGs") && jsonFormaPago.containsKey("textFieldDolarGs")) {
                        int monto = Integer.valueOf(jSONObject.get("textFieldDolarGs").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldDolarGs").toString());
                        int cant = Integer.valueOf(jSONObject.get("textFieldDolarCant").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldDolarCant").toString());;
                        jsonFormaPago.put("textFieldDolarCant", String.valueOf(cant));
                        jsonFormaPago.put("textFieldDolarGs", String.valueOf(monto));
                        jsonFormaPago.put("monto", String.valueOf(monto));
                        listJsonFormaPago.remove(jSONObject);
                        break;
                    } else if (jsonFormaPago.get("formaPago").toString().contentEquals(jSONObject.get("formaPago").toString())
                            && jSONObject.containsKey("textFieldRealGs") && jsonFormaPago.containsKey("textFieldRealGs")) {
                        int monto = Integer.valueOf(jSONObject.get("textFieldRealGs").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldRealGs").toString());
                        int cant = Integer.valueOf(jSONObject.get("textFieldRealCant").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldRealCant").toString());;
                        jsonFormaPago.put("textFieldRealCant", String.valueOf(cant));
                        jsonFormaPago.put("textFieldRealGs", String.valueOf(monto));
                        jsonFormaPago.put("monto", String.valueOf(monto));
                        listJsonFormaPago.remove(jSONObject);
                        break;
                    } else if (jsonFormaPago.get("formaPago").toString().contentEquals(jSONObject.get("formaPago").toString())
                            && jSONObject.containsKey("textFieldPesoGs") && jsonFormaPago.containsKey("textFieldPesoGs")) {
                        int monto = Integer.valueOf(jSONObject.get("textFieldPesoGs").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldPesoGs").toString());
                        int cant = Integer.valueOf(jSONObject.get("textFieldPesoCant").toString()) + Integer.valueOf(jsonFormaPago.get("textFieldPesoCant").toString());;
                        jsonFormaPago.put("textFieldPesoCant", String.valueOf(cant));
                        jsonFormaPago.put("textFieldPesoGs", String.valueOf(monto));
                        jsonFormaPago.put("monto", String.valueOf(monto));
                        listJsonFormaPago.remove(jSONObject);
                        break;
                    }
                }
                listJsonFormaPago.add(jsonFormaPago);
            } else {
                listJsonFormaPago.add(jsonFormaPago);
            }
            actualizandoTabla();
            JSONObject jsonPotencialParesa = hmFormaPago.get("VALE");
            paresaValido = false;
            if (jsonPotencialParesa != null) {
                if (jsonPotencialParesa.get("comboBoxVale").toString().contentEquals("PARESA")) {
                    calculandoValeParesa();
                    paresaValido = true;
                }
            }
            seteandoTotalAbonado();
            if (totalAbonado >= totalAPagar) {
                finalizandoVenta();
            } else {
                jfxComboBoxTipoDto.setDisable(true);
                limpiandoCampos();
                if (!paresaValido) {
                    switch (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem()) {
                        case "FIEL / PROMO":
                            switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
                                case "VALE":
                                    seteandoTotales();
                                    seteandoTotalAbonado();
                                    break;
                                case "ASOCIACIÓN":
                                    seteandoTotales();
                                    seteandoTotalAbonado();
                                    break;
                                case "GIFTCARD":
                                    seteandoTotales();
                                    seteandoTotalAbonado();
                                    break;
                                case "VALE INTERNO (FUNCIONARIO)":
                                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                        validandoClienteFiel();
                                    }
                                    break;
                                default:
                                    if (!hmFormaPago.containsKey("VALE") && !hmFormaPago.containsKey("ASOCIACIÓN") && !hmFormaPago.containsKey("GIFTCARD")) {
                                        if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")) {
                                            if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")) {
                                                validandoClienteFiel();
                                            }
                                        } else {
                                            validandoClienteFiel();
                                        }

                                    } else {
                                        seteandoTotales();
                                        seteandoTotalAbonado();
                                    }
                                    break;
                            }
                            break;
                        case "FUNCIONARIO":
                            if (!hmFormaPago.containsKey("GIFTCARD")) {
                                validandoFuncionario();
                            } else {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            }
                            break;
                        case "SÁB. COOP.":
                            break;
                        case "DESC. FUNCIONARIOS PUBLICOS Y JUBILADOS":
                            if (!hmFormaPago.containsKey("GIFTCARD")) {
                                validandoTarjetaConvenio();
                            } else {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            }
                            break;
                        case "FEUCCA":
                            if (!hmFormaPago.containsKey("GIFTCARD")) {
                                validandoTarjetaConvenio();
                            } else {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            }
                            break;
                        case "DESC. EXA SALESIANO":
                            if (!hmFormaPago.containsKey("GIFTCARD")) {
                                validandoTarjetaConvenio();
                            } else {
                                seteandoTotales();
                                seteandoTotalAbonado();
                            }
                            break;
                    }
                }
                return true;
            }
        }
        return false;
    }

    private boolean validandoAgregarFormaPago() {
        boolean correcto = true;
        switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
            case "EFECTIVO":
                if (textFieldEfectivo.getText().isEmpty()) {
                    correcto = false;
                } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                    if (Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()))
                            < Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()))) {
                        if (timeToaster == 0) {
                            toaster.mensajeGenerico("AVISO FUNCIONARIO", "EL PAGO DEBE SER POR EL TOTAL DE LA COMPRA EN EFECTIVO.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        } else {
                            if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                                toaster.mensajeGenerico("AVISO FUNCIONARIO", "EL PAGO DEBE SER POR EL TOTAL DE LA COMPRA EN EFECTIVO.", "", 3);
                                timeToaster = System.currentTimeMillis();
                            }
                        }
                        correcto = false;
                    } else if (labelFuncionario.getText().contentEquals("N/A")) {
                        correcto = false;
                    }
                }
                break;
            case "TARJETAS":
                if (!hashJsonComboTarjeta.containsKey(comboBoxTarjetas.getSelectionModel().getSelectedItem())) {
                    if (timeToaster == 0) {
                        toaster.mensajeGenerico("AVISO TARJETA", "TARJETA NO VÁLIDA.", "", 3);
                        timeToaster = System.currentTimeMillis();
                    } else {
                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                            toaster.mensajeGenerico("AVISO TARJETA", "TARJETA NO VÁLIDA.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        }
                    }
                    correcto = false;
                } else if (textFieldMontoTarjeta.getText().isEmpty()) {
                    correcto = false;
                } else if (textFieldCodAuth.getText().isEmpty()) {
                    correcto = false;
                } else if (hmFormaPago.containsKey(jfxComboBoxFormaPago.getSelectionModel().getSelectedItem())) {
                    /*if (timeToaster == 0) {
                        toaster.mensajeGenerico("AVISO TARJETA", "SOLO SE ADMITE UNA TARJETA.", "", 3);
                        timeToaster = System.currentTimeMillis();
                    } else {
                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                            toaster.mensajeGenerico("AVISO TARJETA", "SOLO SE ADMITE UNA TARJETA.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        }
                    }
                    correcto = false;*/
                } else if (comboBoxTarjetas.getSelectionModel().getSelectedItem().contains(" || ") && descuentoTarjetaSum > 0) {
                    if (Integer.valueOf(numValidator.numberValidator(textFieldMontoTarjeta.getText()))
                            < Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()))) {
                        if (timeToaster == 0) {
                            toaster.mensajeGenerico("AVISO TARJETA", "EL MONTO A ABONAR DEBE SER POR EL TOTAL DE LA FACTURA.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        } else {
                            if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                                toaster.mensajeGenerico("AVISO TARJETA", "EL MONTO A ABONAR DEBE SER POR EL TOTAL DE LA FACTURA.", "", 3);
                                timeToaster = System.currentTimeMillis();
                            }
                        }
                        correcto = false;
                    }
                }
                break;
            case "MONEDA EXTRANJERA":
                if (textFieldDolarGs.getText().isEmpty() && textFieldRealGs.getText().isEmpty()
                        && textFieldPesoGs.getText().isEmpty()) {
                    correcto = false;
                }
                break;
            case "CHEQUE":
                if (textFieldEntidad.getText().isEmpty()) {
                    correcto = false;
                } else if (textFIeldNroCheque.getText().isEmpty()) {
                    correcto = false;
                } else if (textFieldMonto.getText().isEmpty()) {
                    correcto = false;
                } else if (hmFormaPago.containsKey(jfxComboBoxFormaPago.getSelectionModel().getSelectedItem())) {
//                    if (timeToaster == 0) {
//                        toaster.mensajeGenerico("AVISO CHEQUE", "SOLO SE ADMITE UN CHEQUE.", "", 3);
//                        timeToaster = System.currentTimeMillis();
//                    } else {
//                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
//                            toaster.mensajeGenerico("AVISO CHEQUE", "SOLO SE ADMITE UN CHEQUE.", "", 3);
//                            timeToaster = System.currentTimeMillis();
//                        }
//                    }
//                    correcto = false;
                } else if (textFIeldCiTercero.getText().isEmpty()) {
                    correcto = false;
                }
                break;
            case "NOTA DE CRÉDITO":
                if (textFieldNroNotaCred.getText().isEmpty()) {
                    correcto = false;
                } else if (textFieldMontoNotaCred.getText().isEmpty()) {
                    correcto = false;
                } else if (hmFormaPago.containsKey(jfxComboBoxFormaPago.getSelectionModel().getSelectedItem())) {
//                    if (timeToaster == 0) {
//                        toaster.mensajeGenerico("AVISO NC", "SOLO SE ADMITE UNA NC.", "", 3);
//                        timeToaster = System.currentTimeMillis();
//                    } else {
//                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
//                            toaster.mensajeGenerico("AVISO NC", "SOLO SE ADMITE UNA NC.", "", 3);
//                            timeToaster = System.currentTimeMillis();
//                        }
//                    }
//                    correcto = false;
                }
                break;
            case "VALE":
                if (!hashJsonComboVale.containsKey(comboBoxVale.getSelectionModel().getSelectedItem())) {
                    correcto = false;
                }
                if (correcto) {
                    if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                        if (textFieldMontoValePorc.getText().isEmpty()) {
                            correcto = false;
                        } else if (textFieldNroVale.getText().isEmpty()) {
                            correcto = false;
                        } else if (textFieldValeCI.getText().isEmpty()) {
                            correcto = false;
                        }
                    } else if (textFieldMontoVale.getText().isEmpty()) {
                        correcto = false;
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
                        if (textFieldNroVale.getText().isEmpty()) {
                            correcto = false;
                        } else if (textFieldValeCI.getText().isEmpty()) {
                            correcto = false;
                        }
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("CELLULAR")
                            || comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ESSEN")
                            || comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FRIGOMERC")
                            || comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("INSPECTORATE")
                            || comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
                        if (textFieldNroVale.getText().isEmpty()) {
                            correcto = false;
                        }
                    }
                    if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                            || hmFormaPago.containsKey("ASOCIACIÓN")) {
                        if (timeToaster == 0) {
                            toaster.mensajeGenerico("AVISO", "SOLO SE ADMITE UN VALE/ORDEN.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        } else {
                            if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                                toaster.mensajeGenerico("AVISO", "SOLO SE ADMITE UN VALE/ORDEN.", "", 3);
                                timeToaster = System.currentTimeMillis();
                            }
                        }
                        correcto = false;
                    }
                }
                break;
            case "VALE INTERNO (FUNCIONARIO)":
                if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                        || hmFormaPago.containsKey("ASOCIACIÓN")) {
                    if (timeToaster == 0) {
                        toaster.mensajeGenerico("AVISO VALE", "SOLO SE ADMITE UN VALE.", "", 3);
                        timeToaster = System.currentTimeMillis();
                    } else {
                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                            toaster.mensajeGenerico("AVISO VALE", "SOLO SE ADMITE UN VALE.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        }
                    }
                    correcto = false;
                } else if (textFieldMontoValeInterno.getText().isEmpty()) {
                    correcto = false;
                }
                break;
            case "ASOCIACIÓN":
                if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                        || hmFormaPago.containsKey("ASOCIACIÓN")) {
                    if (timeToaster == 0) {
                        toaster.mensajeGenerico("AVISO ASOC.", "SOLO SE ADMITE UNA ASOCIACIÓN.", "", 3);
                        timeToaster = System.currentTimeMillis();
                    } else {
                        if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                            toaster.mensajeGenerico("AVISO ASOC.", "SOLO SE ADMITE UNA ASOCIACIÓN.", "", 3);
                            timeToaster = System.currentTimeMillis();
                        }
                    }
                    correcto = false;
                } else if (textFieldMontoAsoc.getText().isEmpty()) {
                    correcto = false;
                }
                break;
            case "GIFTCARD":
                if (textFieldMontoGift.getText().isEmpty()) {
                    correcto = false;
                }
                estadoGift = false;
                break;
            default:
                mensajePopup("¡AVISO!", "FORMA DE PAGO NO VÁLIDA", "", 2);
                correcto = false;
                break;
        }
        return correcto;
    }

    private void visualizandoCamposVale() {//validaciones para el vale
        if (comboBoxVale.getSelectionModel().isSelected(0)) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
            invalidandoVale();
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ORDEN DE COMPRA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(false);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80003400-7");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FRIGOMERC")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80019708-9");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("INSPECTORATE")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80037706-0");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("CELLULAR")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80008968-5");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ESSEN")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80080809-6");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        }
        if (!comboBoxVale.getSelectionModel().isSelected(0) && !comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
//            validandoVale(comboBoxVale.getSelectionModel().getSelectedItem());
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
//            panelBusquedaTipoTarjConv.setDisable(true);
            valeActivo = true;
        }
    }

    private void iniciandoCotizacion() {
        real = 0;
        peso = 0;
        dolar = 0;
        jsonTipoMoneda();
        if (cotizacionList != null) {
            for (JSONObject jsonCotizacion : cotizacionList) {
                switch ((Integer.valueOf(jsonCotizacion.get("idTipoMoneda").toString()))) {
                    case 2://peso argentino
                        peso = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtPeso.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                    case 3://real brasilero
                        real = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtReal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                    case 4://dólar américano
                        dolar = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtDolar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                }
            }
        }
    }

    private void buscandoFuncionario() {
        String ci;
        if (textFieldCiFuncionario.getText().contentEquals("")) {
            ci = "null";
        } else {
            ci = textFieldCiFuncionario.getText();
        }
        jsonFuncionario = generarListaFuncionariosLocal(ci);
        if (jsonFuncionario != null) {
            if (!jsonFuncionario.isEmpty()) {
                String nom = "";
                String ape = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nom = jsonFuncionario.get("nombre").toString().toUpperCase();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    ape = jsonFuncionario.get("apellido").toString().toUpperCase();
                }
                labelFuncionario.setText(nom + " " + ape);
//                textFieldEfectivo.requestFocus();
            } else {
                labelFuncionario.setText("N/A");
                if (timeToaster == 0) {
                    toaster.mensajeGenerico("AVISO", "NO EXISTE EL FUNCIONARIO.", "", 3);
                    timeToaster = System.currentTimeMillis();
                } else {
                    if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                        toaster.mensajeGenerico("AVISO", "NO EXISTE EL FUNCIONARIO.", "", 3);
                        timeToaster = System.currentTimeMillis();
                    }
                }
            }
        } else {
            labelFuncionario.setText("N/A");
            if (timeToaster == 0) {
                toaster.mensajeGenerico("AVISO", "NO EXISTE EL FUNCIONARIO.", "", 3);
                timeToaster = System.currentTimeMillis();
            } else {
                if ((System.currentTimeMillis() - timeToaster) >= 3000) {
                    toaster.mensajeGenerico("AVISO", "NO EXISTE EL FUNCIONARIO.", "", 3);
                    timeToaster = System.currentTimeMillis();
                }
            }
        }
    }

    private void calculandoTotal() {
        total = FacturaDeVentaMayFXMLController.getPrecioTotal();
        textFieldTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(total))));
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(total))));
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void limpiandoCampos() {
        if (tableViewFormaDePago.getItems().isEmpty()) {
            jfxComboBoxTipoDto.setDisable(false);
        }
        cargandoComboBoxFormaPago();
        jfxTabPaneFormasPago.getSelectionModel().select(tabEfectivo);
        comboBoxVale.getSelectionModel().select(0);
        comboBoxTarjetas.getSelectionModel().select(0);
        comboBoxTarjetas.getEditor().setText("");
        textFieldCiFuncionario.setText("");
        labelCiFuncionario.setVisible(false);
        labelFuncionario.setText("");
        textFieldEfectivo.setText("");
        textFieldMontoTarjeta.setText("");
        textFieldCodAuth.setText("");
        textFieldDolarCant.setText("");
        textFieldRealCant.setText("");
        textFieldPesoCant.setText("");
        textFieldDolarGs.setText("");
        textFieldRealGs.setText("");
        textFieldPesoGs.setText("");
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFieldMonto.setText("");
        textFIeldCiTercero.setText("");
        textFieldNroNotaCred.setText("");
        textFieldMontoNotaCred.setText("");
        textFieldNroVale.setText("");
        textFieldValeCI.setText("");
        textFieldMontoVale.setText("");
        textFieldMontoValePorc.setText("");
        textFieldMontoValeInterno.setText("");
        textFieldMontoAsoc.setText("");
    }

    //SETEOS GENERALES Y DTO.***************************************************
    private void seteandoTotalAbonado() {
        donacion = 0;
        vuelto = 0;
        totalAbonado = 0;
        //cheque
        //sin mapeo, para más adelante varios cheques...
        for (JSONObject jsonFormaPago : listJsonFormaPago) {
            //cheque
            if (jsonFormaPago.get("formaPago").toString().contentEquals("CHEQUE")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMonto").toString());
            }
            //tarjeta crédito/débito
            if (jsonFormaPago.get("formaPago").toString().contentEquals("TARJETAS")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMontoTarjeta").toString());
            }
            //nota crédito
            if (jsonFormaPago.get("formaPago").toString().contentEquals("NOTA DE CRÉDITO")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMontoNotaCred").toString());
            }
            //moneda local
            if (jsonFormaPago.get("formaPago").toString().contentEquals("EFECTIVO")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldEfectivo").toString());
            }
            //moneda extranjera
            if (jsonFormaPago.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA") && jsonFormaPago.containsKey("textFieldDolarGs")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldDolarGs").toString());
            }
            if (jsonFormaPago.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA") && jsonFormaPago.containsKey("textFieldRealGs")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldRealGs").toString());
            }
            if (jsonFormaPago.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA") && jsonFormaPago.containsKey("textFieldPesoGs")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldPesoGs").toString());
            }
            //vale
            if (jsonFormaPago.get("formaPago").toString().contentEquals("VALE") && jsonFormaPago.containsKey("textFieldMontoVale")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMontoVale").toString());
            } else if (jsonFormaPago.containsKey("VALE") && jsonFormaPago.containsKey("textFieldMontoValePorc")) {
                //porc.
            }
            //asoc.
            if (jsonFormaPago.get("formaPago").toString().contentEquals("ASOCIACIÓN") && jsonFormaPago.containsKey("textFieldMontoAsoc")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMontoAsoc").toString());
            }
            //vale interno
            if (jsonFormaPago.get("formaPago").toString().contentEquals("VALE INTERNO (FUNCIONARIO)") && jsonFormaPago.containsKey("textFieldMontoValeInterno")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldMontoValeInterno").toString());
            }
            //GIFTCARD
            if (jsonFormaPago.get("formaPago").toString().contentEquals("GIFTCARD")) {
                totalAbonado = totalAbonado + Long.valueOf(jsonFormaPago.get("textFieldGiftcard").toString());
            }
        }
        if (totalAbonado >= totalAPagar) {
            labelVuelto.setText("Vuelto");
            textFieldVuelto.setStyle("-fx-background-color: #3CB371; -fx-opacity:  1;");
            vuelto = totalAbonado - totalAPagar;
            if (vuelto > 0) {
                textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(vuelto))));
            } else if (vuelto == 0) {
                textFieldVuelto.setText("Gs 0");
            }
        } else {
            labelVuelto.setText("Faltante");
            long dif = (totalAbonado - totalAPagar) * -1;
            textFieldVuelto.setStyle("-fx-background-color: #F08080; -fx-opacity:  1;");
            textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
            switch (jfxComboBoxFormaPago.getSelectionModel().getSelectedItem()) {
                case "EFECTIVO":
                    textFieldEfectivo.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
                    break;
                case "TARJETAS":
                    textFieldMontoTarjeta.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
                    break;
                case "MONEDA EXTRANJERA":
                    break;
                case "CHEQUE":
                    textFieldMonto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
                    break;
                case "NOTA DE CRÉDITO":
                    textFieldMontoNotaCred.setText(/*numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif)))*/String.valueOf(dif));
                    break;
                case "VALE":
                    if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
                    }
                    break;
                case "VALE INTERNO (FUNCIONARIO)":
                    break;
                case "ASOCIACIÓN":
                    break;
                case "GIFTCARD":
//                    if (!textFieldMontoGift.isDisable()) {
//                        jfxComboBoxFormaPago.getItems().remove("GIFTCARD");
//                    }
                    textFieldCodGift.setText("");
                    textFieldCodGift.requestFocus();
                    textFieldMontoGift.setText("");
                    textFieldMontoGift.setDisable(true);
                    labelSaldoGift.setText("Gs 0");
                    datePickerVigencia.setValue(null);
                    //"en gestión"a
                    break;
                default:
                    break;
            }
        }
//        if (totalAbonado > 0) {
//            textFieldTotalAbonado.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAbonado))));
//        } else {
//            textFieldTotalAbonado.setText("Gs 0");
//        }
    }

    private void seteandoControlDesc() {
        descDirectivo = false;
        descFuncionario = false;
        descPromoTemp = false;
        descTarjeta = false;
        descTarjetaConvenio = false;
        descTarjetaFiel = false;
        descValePorc = false;
        descValeMonto = false;
    }

    private void seteandoTotales() {
        descuento = 0;
//        totalAPagar = 0;
        totalAPagar = Long.valueOf(numValidator.numberValidator(FacturaDeVentaMayFXMLController.getPrecioTotal().toString()));
        textFieldDescuento.setText("Gs 0");
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(FacturaDeVentaMayFXMLController.getPrecioTotal().toString())));
        seteandoControlDesc();
        montoConDes10 = 0;
        montoConDes5 = 0;
        exenta = 0;
        hashDetallePrecioDesc = new HashMap<>();
        hashDetallePrecioDescPromoArt = new HashMap<>();
        //sum
        descuentoPromoArtSum = 0;
        descuentoPromoNfSum = 0;
        descuentoFielSum = 0;
        descuentoFuncSum = 0;
        descuentoTarjetaSum = 0;
        descuentoTarjetaConvSum = 0;
        descuentoValePorcSum = 0;
        descuentoValeParesaSum = 0;
        descuentoGift = 0;
        //sum
    }

    private boolean seteandoDescTarjeta() {
        if (!hmFormaPago.containsKey("CHEQUE") && !hmFormaPago.containsKey("NOTA DE CRÉDITO")
                && !hmFormaPago.containsKey("VALE") && !hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                && !hmFormaPago.containsKey("ASOCIACIÓN") && !hmFormaPago.containsKey("EFECTIVO")
                && !hmFormaPago.containsKey("MONEDA EXTRANJERA") && !hmFormaPago.containsKey("GIFTCARD")) {
            calculandoTarjeta(true);
        }
        return descTarjeta;
    }
    //SETEOS GENERALES Y DTO.***************************************************

    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->
    //VALIDACIÓN MAYORISTA ************************************** -> -> -> -> ->
    private void validandoValeMay() {
        int count = 0;
        //podría haber prefijos repetidos...
        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            count = count + StringUtils.countMatches(jsonArticulo.get("descripcion").toString().toUpperCase(), "LIB.");
            count = count + StringUtils.countMatches(jsonArticulo.get("descripcion").toString().toUpperCase(), ".LIB.");
            count = count + StringUtils.countMatches(jsonArticulo.get("descripcion").toString().toUpperCase(), "LIBRERIA");
            count = count + StringUtils.countMatches(jsonArticulo.get("descripcion").toString().toUpperCase(), "+LIB.");
            if (count == 0) {
                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                    recursivoValeMayo(7, jsonNf);
                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                    recursivoValeMayo(6, jsonNf);
                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                    recursivoValeMayo(5, jsonNf);
                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                    recursivoValeMayo(4, jsonNf);
                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                    recursivoValeMayo(3, jsonNf);
                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                    if (jsonNf.get("descripcion").toString().toUpperCase().contentEquals("LIBRERIA")) {
                        count++;
                    }
                } else {
                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                }
            }
        }
        if (FacturaDeVentaMayFXMLController.getDetalleArtList().size() <= count && BuscarClienteFXMLController.getJsonCliente() != null) {
            jsonCargandoVales();
        } else {
            comboBoxVale.getItems().add("Seleccione un tipo de vale...");
            comboBoxVale.getSelectionModel().select(0);
            hashJsonComboVale = new HashMap<>();
        }
    }

    @SuppressWarnings("element-type-mismatch")
    private void recursivoValeMayo(int nf, JSONObject jsonNf) {
        switch (nf) {
            case 3:
                JSONObject jsonRaiz2 = (JSONObject) jsonNf.get("nf2Sfamilia");
                recursivoValeMayo(2, jsonRaiz2);
                break;
            case 4:
                JSONObject jsonRaiz3 = (JSONObject) jsonNf.get("nf3Sseccion");
                recursivoValeMayo(3, jsonRaiz3);
                break;
            case 5:
                JSONObject jsonRaiz4 = (JSONObject) jsonNf.get("nf4Seccion1");
                recursivoValeMayo(4, jsonRaiz4);
                break;
            case 6:
                JSONObject jsonRaiz5 = (JSONObject) jsonNf.get("nf5Seccion2");
                recursivoValeMayo(5, jsonRaiz5);
                break;
            case 7:
                JSONObject jsonRaiz6 = (JSONObject) jsonNf.get("nf6Secnom6");
                recursivoValeMayo(6, jsonRaiz6);
                break;
            default:
                break;
        }
    }
    //VALIDACIÓN MAYORISTA ************************************** -> -> -> -> ->

    //FUNCIONARIO***************************************************************
    private void validandoFuncionario() {
        tabTarjetaCredDeb.setDisable(true);
        comboBoxTarjetas.getSelectionModel().select(0);
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
        tabCheque.setDisable(true);
        jfxComboBoxTipoDto.getSelectionModel().select("FUNCIONARIO");
        tabVale.setDisable(true);
        if (!hmFormaPago.containsKey("NOTA DE CRÉDITO")) {
            entradaFunc1era = true;
            calculandoFuncionario();
//            checkBoxPromoTempButton();
        }
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void validandoClienteFiel() {
//        if (textFieldMontoPorc.getText().contentEquals("")
//                && textFieldMontoRetencion.getText().contentEquals("")) {
        if (BuscarClienteFXMLController.getJsonClienteFiel() != null && !hmFormaPago.containsKey("GIFTCARD")) {
            if (!hmFormaPago.containsKey("TARJETAS") && !validandoTarjetaConvenioTarjCredDeb()) {
                entradaFiel1era = true;
                calculandoClienteFiel();
                checkBoxPromoTempButton();
            } else {
                if (!hmFormaPago.containsKey("MONEDA EXTRANJERA")
                        && !hmFormaPago.containsKey("CHEQUE") && !hmFormaPago.containsKey("NOTA DE CRÉDITO")
                        && !hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem())) {
                    calculandoTarjeta(true);
                    if (!descTarjeta) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                        checkBoxPromoTempButton();
                    }
                } else {
                    calculandoTarjeta(false);
                    calculandoClienteFiel();
                    checkBoxPromoTempButton();
                }
            }
//            validandoGiftFielDto();
        } else {
            seteandoTotales();
            seteandoTotalAbonado();
        }
//        }
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
    private void validandoTarjeta() {
        if (/*textFieldMontoPorc.getText().contentEquals("") && */!hmFormaPago.containsKey("NOTA DE CRÉDITO") /*&& textFieldMontoRetencion.getText().contentEquals("")*/) {
            calculandoTarjeta(true);
            seteandoTotalAbonado();
        }
    }

    private void bloqueandoPanelesTarj() {
        //TABs
        tabMonExtr.setDisable(true);
        tabEfectivo.setDisable(true);
        tabCheque.setDisable(true);
        tabNcs.setDisable(true);
        tabVale.setDisable(true);
        tabAsoc.setDisable(true);
        tabValeInterno.setDisable(true);
        tabGiftCard.setDisable(true);
        //TABs
        textFieldEfectivo.setText("");
        textFieldDolarCant.setText("");
        textFieldRealCant.setText("");
        textFieldPesoCant.setText("");
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFIeldCiTercero.setText("");
        textFieldMonto.setText("");
        textFieldNroNotaCred.setText("");
        textFieldMontoNotaCred.setText("");
        comboBoxVale.getSelectionModel().select(0);
        textFieldNroVale.setText("");
        textFieldValeCI.setText("");
        textFieldMontoVale.setText("");
        textFieldMontoValePorc.setText("");
//        if (!listViewCheques.getItems().isEmpty()) {
//            listViewCheques.getSelectionModel().select(0);
//            String[] parts = listViewCheques.getSelectionModel().getSelectedItem().split(" - Gs ");
//            hashListCheque.remove(parts[0]);
//            chequesAsignados.remove(listViewCheques.getSelectionModel().getSelectedItem());
//            arrayCheque.remove(listViewCheques.getSelectionModel().getSelectedItem());
//            listViewCheques.setItems(chequesAsignados);
//        }
//        if (!listViewNotasCred.getItems().isEmpty()) {
//            listViewNotasCred.getSelectionModel().select(0);
//            String[] parts = listViewNotasCred.getSelectionModel().getSelectedItem().split(" - Gs ");
//            hashListNotaCred.remove(parts[0]);
//            notaCredAsignadas.remove(listViewNotasCred.getSelectionModel().getSelectedItem());
//            listViewNotasCred.setItems(notaCredAsignadas);
//        }
//        if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
//            listViewTarjetasConvAgregadas.getSelectionModel().select(0);
//            tarjConvAsignadas.remove(listViewTarjetasConvAgregadas.getSelectionModel().getSelectedItem());
//            listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
//        }
        toggleEnTarj = false;
    }

    private void desbloqueandoPanelesTarj() {
        //TABs
        tabMonExtr.setDisable(false);
        tabEfectivo.setDisable(false);
        tabCheque.setDisable(false);
        tabNcs.setDisable(false);
        tabVale.setDisable(false);
        tabAsoc.setDisable(false);
        tabValeInterno.setDisable(false);
        tabGiftCard.setDisable(false);
        //TABs
//        toggleEnTarj = true;
        if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO") || BuscarClienteFXMLController.getJsonClienteFiel() != null) {
//            if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO") || BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            toggleButton = true;
        }
//        if (jsonFuncionario == null) {
//            panelBusquedaTipoTarjConv.setDisable(false);
//            panelVales.setDisable(false);
//            panelBusquedaCheque.setDisable(false);
//        }
//        panelTipoDePagoExtranjera.setDisable(false);
//        panelTipoDePago.setDisable(false);
//        panelNotaCredito.setDisable(false);
//        toggleEnTarj = false;
    }

    private void validandoTarjetaListen() {
        if (comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
            desbloqueandoPanelesTarj();
        }
        comboBoxTarjeta();
    }

    private void invalidandoTarjeta() {
//        panelBusquedaClienteFiel.setDisable(false);
//        panelBusquedaFuncionario.setDisable(false);
    }

    private void comboBoxTarjeta() {
        //para habilitar algún descuento con tarjeta crédito, débito, no debe tener otra forma de pago, y solo una tarjeta...
        if (comboBoxTarjetas.getSelectionModel().getSelectedItem() == null || comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar + descuento))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
            if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                validandoFuncionario();
            } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                validandoClienteFiel();
            }
        } else {
            validandoTarjeta();
        }
    }
    //TARJETAS******************************************************************

    //TARJETAS CONVENIO*********************************************************
    private void validandoTarjetaConvenio() {
//        panelVales.setDisable(true);
        /*if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {*/
//        panelBusquedaFuncionario.setDisable(true);
        tarjConvActiva = true;
        comboBoxTarjetas.getSelectionModel().select(0);
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
        tabVale.setDisable(true);
        tabAsoc.setDisable(true);
        tabValeInterno.setDisable(true);
        comboBoxVale.getSelectionModel().select(0);
        textFieldNroVale.setText("");
        textFieldValeCI.setText("");
        textFieldMontoVale.setText("");
        textFieldMontoValePorc.setText("");
        calculandoTarjetaConvenio();
//        }
    }

    private void invalidandoTarjetaConvenio() {
//        panelBusquedaFuncionario.setDisable(false);
        tabVale.setDisable(false);
        tarjConvActiva = false;
//        if (textFieldMontoPorc.getText().contentEquals("")
//                && textFieldMontoRetencion.getText().contentEquals("")) {
        tabTarjetaCredDeb.setDisable(false);
//            panelBusquedaFuncionario.setDisable(false);
        if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            validandoFuncionario();
        } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            validandoClienteFiel();
        } else {
            seteandoTotales();
            seteandoTotalAbonado();
        }
//        }
    }

    private boolean validandoTarjetaConvenioTarjCredDeb() {
        boolean descTarjDetectado = false;
        String descripcionTarj = "N/A";
        if (hmFormaPago.containsKey("TARJETAS")) {//solo 1
            for (JSONObject jSONObject : listJsonFormaPago) {//pueden ser varios
                if (jSONObject.containsKey("TARJETAS")) {
                    descripcionTarj = jSONObject.get("comboBoxTarjetas").toString();
                    break;
                }
            }
        } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
            descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
        }
        if (Descuento.getHashDescTarjeta() != null) {
            if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
                JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
                if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) == -1
                        || (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) <= Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())))) {
                    JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
                    Calendar calendar = Calendar.getInstance();
                    JSONArray jsonArrayDiaEsp = new JSONArray();
                    int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                    for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
                        JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                        JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                        int dia = toIntExact((long) jsonDia.get("idDia"));
                        if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
                            String fechaIni = (String) jsonTarjCab.get("horaInicio");
                            String fechaFin = (String) jsonTarjCab.get("horaFin");
                            if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                Time fechaIniT = Utilidades.objectToTime(fechaIni);
                                Time fechaFinT = Utilidades.objectToTime(fechaFin);
                                Date date = new Date();
                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                Time t = Utilidades.objectToTime(hora);
                                if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                            descTarjDetectado = true;
                                            break;
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                    //se omite la bajada por el tema del convenio...
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                } else {
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        descTarjDetectado = true;
                                        break;
                                    }
                                }
                            } else {
                                JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                    if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                        descTarjDetectado = true;
                                        break;
                                    } else {
                                        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                            if ((Boolean) jsonTarjCab.get("extracto")) {
                                                //se omite la bajada por el tema del convenio...
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            } else {
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    descTarjDetectado = true;
                                    break;
                                }
                            }
                        } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
                            jsonArrayDiaEsp.add(jsonTarjDet);
                        }
                    }
                    if (!jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
                        HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
                        if (hashDiaEspecial != null) {
                            for (Object tarjDetObj : jsonArrayDiaEsp) {
                                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                                if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
                                    if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
                                        String fechaIni = (String) jsonTarjCab.get("horaInicio");
                                        String fechaFin = (String) jsonTarjCab.get("horaFin");
                                        if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                            Time fechaIniT = Time.valueOf(fechaIni);
                                            Time fechaFinT = Time.valueOf(fechaFin);
                                            Date date = new Date();
                                            String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                            Time t = Time.valueOf(hora);
                                            if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        } else {
                                            JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                            if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                    descTarjDetectado = true;
                                                    break;
                                                } else {
                                                    for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                        JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                        if ((Boolean) jsonTarjCab.get("extracto")) {
                                                            //se omite la bajada por el tema del convenio...
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        } else {
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                    //tool tip
                }
            }
        }
        return descTarjDetectado;
    }
    //TARJETAS CONVENIO*********************************************************

    //VALE*******************************************************
    private void validandoVale(String tipoVale) {
//        panelBusquedaTipoTarjConv.setDisable(true);
        valeActivo = true;
//        if (textFieldMontoPorc.getText().contentEquals("")
//                && textFieldMontoRetencion.getText().contentEquals("")) {
        calculandoVale(tipoVale);
//        }
    }

    private void invalidandoVale() {
//        panelBusquedaTipoTarjConv.setDisable(false);
        valeActivo = false;
        if (/*textFieldMontoPorc.getText().contentEquals("") &&*/textFieldMontoNotaCred.getText().contentEquals("") /*&& textFieldMontoRetencion.getText().contentEquals("")*/) {
            seteandoTotales();
        }
    }

    private void calculandoValeParesa() {
        if (comboBoxVale.getSelectionModel().getSelectedIndex() == 5) {
            seteandoTotales();
            seteandoControlDesc();
        }
        if (comboBoxVale.getSelectionModel().getSelectedIndex() == 5 && !textFieldValeCI.getText().isEmpty()
                && !textFieldMontoVale.getText().isEmpty() && !textFieldNroVale.getText().isEmpty()) {
            double diferencia = total - Long.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
            if (diferencia > 0) {
                descuentoValeParesaSum = diferencia * 0.1;
                descuento += descuentoValeParesaSum;
                totalAPagar = total - descuento;
                textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
                textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
                if (descuento > 0) {
                    descValeMonto = true;
                }
            }
        }
    }

    private void calculandoValePorc() {
        if (!textFieldMontoValePorc.getText().isEmpty() && descuentoValePorcSum <= 0) {
            descuentoValePorcSum = (Long.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            descuento += descuentoValePorcSum;
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
        }
    }
    //VALE*******************************************************

    //DIRECTIVO*****************************************************************
    private void validandoDirectivo() {
        calculandoDirectivo();
    }

    private void invalidandoDirectivo() {
        seteandoTotales();
    }
    //DIRECTIVO*****************************************************************

    //NOTA DE CRÉDITO***********************************************************
    private void validandoNotaCredito() {
        textFieldNroNotaCred.setDisable(true);
        calculandoNotaCredito();
    }

    private void invalidandoNotaCredito() {
        textFieldNroNotaCred.setDisable(false);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //NOTA DE CRÉDITO***********************************************************

    //MONTO DE RETENCIÓN***********************************************************
    private void validandoMontoRetencion() {
        jfxComboBoxTipoDto.setDisable(false);
        calculandoMontoRetencion();
    }

    private void invalidandoMontoRetencion() {
        jfxComboBoxTipoDto.getSelectionModel().select(0);
        jfxComboBoxTipoDto.setDisable(true);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //MONTO DE RETENCIÓN***********************************************************

    //cambio de formas de pago....
    private void validandoCambiandoFormaPago() {
        if (descTarjetaConvenio || hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem()) || descuentoTarjetaConvSum > 0) {
            calculandoTarjetaConvenioValidacionTar();
        } else if (!descTarjeta && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || hmFormaPago.containsKey("TARJETAS"))
                && !hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem())) {
            seteandoDescTarjeta();
        } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            validandoFuncionario();
        } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            validandoClienteFiel();
        } else if (hmFormaPago.containsKey("VALE") || hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                || hmFormaPago.containsKey("ASOCIACIÓN")) {
            if (hmFormaPago.containsKey("ASOCIACIÓN")) {
                validandoVale("ASOCIACIÓN");
            } else {
                validandoVale("VALE");
            }
        }/* else if (toggleButton.isSelected()) {
            checkBoxPromoTemp();
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            validandoTarjetaConvenio();
        } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            validandoClientFiel();
        } else if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            validandoFuncionario();
        } else {
            seteandoDescTarjeta();
        }*/
    }
    //cambio de formas de pago....
    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->

    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->
    //FUNCIONARIO***************************************************************
    private void calculandoFuncionario() {
        if (entradaFunc1era) {
            seteandoTotales();
            montoMap5 = new HashMap<>();
            montoMap10 = new HashMap<>();
            montoMapExe = new HashMap<>();
        }
        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            if (!FacturaDeVentaMayFXMLController.getHmGift().containsKey(Long.valueOf(jsonArticulo.get("codArticulo").toString()))) {
                JSONArray jsonArrayArticuloNf;
                JSONObject jsonNf;
                JSONObject jsonArticuloNf;
                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                    calculandoFuncionarioNf(7, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                    calculandoFuncionarioNf(6, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                    calculandoFuncionarioNf(5, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                    calculandoFuncionarioNf(4, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                    calculandoFuncionarioNf(3, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                    calculandoFuncionarioNf(2, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                    calculandoFuncionarioNf(1, jsonNf, detalleArtJson);
                } else {
                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                }
            }
        }

        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVenta
        if (descFuncionario) {
            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        if (entradaFunc1era) {
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFuncSum);
            totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            seteandoTotalAbonado();
        }
    }

    @SuppressWarnings("element-type-mismatch")
    private double calculandoFuncionarioNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        double descFunc = 0;
        switch (nf) {
            case 1:
                if (Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")) != null) {
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                }
                break;
            case 2:
                if (Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")) != null) {
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoFuncionarioNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoFuncionarioNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoFuncionarioNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoFuncionarioNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoFuncionarioNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoFuncionarioNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
        return descFunc;
    }

    private double calculandoFuncionarioDet(JSONObject jsonFuncionarioDesc, JSONObject detalleArtJson) {
        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonFuncionarioDesc.get("porcDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        double descFunc = 0;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            if (montoMap10.containsKey(detalleArtJson)) {
                if (montoMap10.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - Math.round((montoMap10.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMap10.put(detalleArtJson, montoMap10.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            if (montoMap5.containsKey(detalleArtJson)) {
                if (montoMap5.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - Math.round((montoMap5.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMap5.put(detalleArtJson, montoMap5.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else {
            if (montoMapExe.containsKey(detalleArtJson)) {
                if (montoMapExe.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - Math.round((montoMapExe.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    exenta = Math.round(exenta + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMapExe.put(detalleArtJson, montoMapExe.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        }
//        descuentoFuncSum = Math.round(descFunc);
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
        return descFunc;
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void calculandoClienteFiel() {
        if (entradaFiel1era) {
            seteandoTotales();
            montoMap5 = new HashMap<>();
            montoMap10 = new HashMap<>();
            montoMapExe = new HashMap<>();
        }
        int size = FacturaDeVentaMayFXMLController.getDetalleArtList().size();
        int actual = 1;
        hmObsequioUlt = new HashMap<>();
        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            boolean ultDetalle = (size == actual);
            //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
            if (!(boolean) jsonArticulo.get("bajada")) {
                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                    calculandoClienteFielNf(7, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                    calculandoClienteFielNf(6, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                    calculandoClienteFielNf(5, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                    calculandoClienteFielNf(4, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                    calculandoClienteFielNf(3, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                    calculandoClienteFielNf(2, jsonNf, detalleArtJson, ultDetalle);
                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                    calculandoClienteFielNf(1, jsonNf, detalleArtJson, ultDetalle);
                } else {
                    if (FacturaDeVentaMayFXMLController.getHmGift().containsKey(Long.valueOf(jsonArticulo.get("codArticulo").toString()))) {
                        validandoGiftFielDto(jsonArticulo, detalleArtJson);
                    } else {
                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                    }
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                        calculandoClienteFielNf(7, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                        calculandoClienteFielNf(6, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                        calculandoClienteFielNf(5, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                        calculandoClienteFielNf(4, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                        calculandoClienteFielNf(3, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                        calculandoClienteFielNf(2, jsonNf, detalleArtJson, ultDetalle);
                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                        calculandoClienteFielNf(1, jsonNf, detalleArtJson, ultDetalle);
                    } else {
                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                    }
                }
            }
            actual++;
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVenta
        if (descTarjetaFiel) {
            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        if (entradaFiel1era) {
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFielSum) + Math.round(descuentoTarjetaSum) + Math.round(descuentoGift);
            totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            seteandoTotalAbonado();
        }
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoClienteFielNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson, boolean ultDetalle) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                }
                break;
            case 2:
                if (Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoClienteFielNf(1, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            case 3:
                if (Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoClienteFielNf(2, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            case 4:
                if (Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoClienteFielNf(3, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            case 5:
                if (Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoClienteFielNf(4, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            case 6:
                if (Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoClienteFielNf(5, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            case 7:
                if (Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")), detalleArtJson, ultDetalle);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoClienteFielNf(6, jsonRaiz, detalleArtJson, ultDetalle);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoClienteFielDet(JSONObject jsonFielCab, JSONObject detalleArtJson, boolean ultDetalle) {
        JSONArray jsonArrayFielDet = (JSONArray) jsonFielCab.get("descuentoFielDets");
        Calendar calendar = Calendar.getInstance();
        int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
        for (Object fielDetObj : jsonArrayFielDet) {//verificación, día de descuento...
            JSONObject jsonFielDet = (JSONObject) fielDetObj;
            JSONObject jsonDia = (JSONObject) jsonFielDet.get("dia");
            int dia = toIntExact((long) jsonDia.get("idDia"));
            double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
            double porc = Double.valueOf(jsonFielCab.get("porcentajeDesc").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            //alambrado 3 x 2
            if (!hmDetallePromoObsequio.containsKey(Long.valueOf(jsonArticulo.get("codArticulo").toString()))) {
                if (!FacturaDeVentaMayFXMLController.getHmGift().containsKey(Long.valueOf(jsonArticulo.get("codArticulo").toString()))) {
                    if (dia == diaSemana) {
                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                            //previamente cargado con algún descuento, se da prioridad al anterior...
                            if (montoMap10.containsKey(detalleArtJson)) {
                                if (montoMap10.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                                    long porcAnterior = 100 - Math.round((montoMap10.get(detalleArtJson) * 100) / (precio * cantidad));
                                    descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                                    descTarjetaFiel = true;
                                    montoConDes10 = montoConDes10 + (((precio * cantidad) * (porc - porcAnterior)) / 100);
                                    montoMap10.put(detalleArtJson, montoMap10.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                                    hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                                }
                            } else {
                                descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                                descTarjetaFiel = true;
                                montoConDes10 = montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100);
                                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                                //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                            }
                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                            if (montoMap5.containsKey(detalleArtJson)) {
                                if (montoMap5.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                                    long porcAnterior = 100 - Math.round((montoMap5.get(detalleArtJson) * 100) / (precio * cantidad));
                                    descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                                    descTarjetaFiel = true;
                                    montoConDes5 = montoConDes5 + (((precio * cantidad) * (porc - porcAnterior)) / 100);
                                    montoMap5.put(detalleArtJson, montoMap5.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                                    hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                                }
                            } else {
                                descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                                descTarjetaFiel = true;
                                montoConDes5 = montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100);
                                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                                //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                            }
                        } else {
                            if (montoMapExe.containsKey(detalleArtJson)) {
                                if (montoMapExe.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                                    long porcAnterior = 100 - Math.round((montoMapExe.get(detalleArtJson) * 100) / (precio * cantidad));
                                    descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                                    descTarjetaFiel = true;
                                    exenta = Math.round(exenta + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                                    montoMapExe.put(detalleArtJson, montoMapExe.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                                    hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                                }
                            } else {
                                descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                                descTarjetaFiel = true;
                                exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
                                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                                //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                            }
                        }
                    }
                }
            } else {
                //mapping por mismo artículo, varios detalles, agrupado en un registro
                hmObsequioUlt.put(Long.valueOf(jsonArticulo.get("codArticulo").toString()), jsonFielCab);
            }
            if (ultDetalle) {
                //promocion obsequio
                //mapping por mismo artículo, varios detalles, agrupado en un registro
                for (Map.Entry<Long, JSONObject> entry : hmDetallePromoObsequio.entrySet()) {
                    JSONArray jsonArrayFielDetObs = (JSONArray) hmObsequioUlt.get(entry.getKey()).get("descuentoFielDets");
                    Calendar calendarObs = Calendar.getInstance();
                    int diaSemanaObs = calendarObs.get(Calendar.DAY_OF_WEEK);
                    for (Object fielDetObjObs : jsonArrayFielDetObs) {//verificación, día de descuento...
                        JSONObject jsonFielDetObs = (JSONObject) fielDetObjObs;
                        JSONObject jsonDiaObs = (JSONObject) jsonFielDetObs.get("dia");
                        int diaObs = toIntExact((long) jsonDiaObs.get("idDia"));
                        double cantidadObs = Double.parseDouble(entry.getValue().get("cantidad").toString());
                        double porcObs = Double.valueOf(jsonFielCab.get("porcentajeDesc").toString());
                        long precioObs = Long.valueOf(entry.getValue().get("precio").toString());
                        if (diaObs == diaSemanaObs) {
                            if (Long.valueOf(entry.getValue().get("poriva").toString()) == 10) {
                                //previamente cargado con algún descuento, se da prioridad al anterior...
                                if (montoMap10.containsKey(entry.getValue())) {
                                    if (hmObsequioRestoEspecial.containsKey(entry.getKey())) {
                                        descuentoFielSum += Double.parseDouble((((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * porcObs) / 100) + "");
                                        descTarjetaFiel = true;
                                        montoConDes10 = montoConDes10 + (((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100);
                                        montoMap10.put(entry.getValue(), Math.round(((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100));
                                        hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    } else {
                                        if (montoMap10.get(entry.getValue()) > Math.round(Double.parseDouble((((precioObs * cantidadObs) * (100 - porcObs)) / 100) + ""))) {
                                            long porcObsAnterior = 100 - Math.round((montoMap10.get(entry.getValue()) * 100) / (precioObs * cantidadObs));
                                            descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100) + "");
                                            descTarjetaFiel = true;
                                            montoConDes10 = montoConDes10 + (((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100);
                                            montoMap10.put(entry.getValue(), montoMap10.get(entry.getValue()) + Math.round(((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100));
                                            hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (porcObs - porcObsAnterior)) / 100));
                                        }
                                    }
                                } else {
                                    descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * porcObs) / 100) + "");
                                    descTarjetaFiel = true;
                                    montoConDes10 = montoConDes10 + (((precioObs * cantidadObs) * (100 - porcObs)) / 100);
                                    montoMap10.put(entry.getValue(), Math.round(((precioObs * cantidadObs) * (100 - porcObs)) / 100));
                                    hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    //precioObs unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                                }
                            } else if (Long.valueOf(entry.getValue().get("poriva").toString()) == 5) {
                                if (montoMap5.containsKey(entry.getValue())) {
                                    if (hmObsequioRestoEspecial.containsKey(entry.getKey())) {
                                        descuentoFielSum += Double.parseDouble((((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * porcObs) / 100) + "");
                                        descTarjetaFiel = true;
                                        montoConDes5 = montoConDes5 + (((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100);
                                        montoMap5.put(entry.getValue(), Math.round(((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100));
                                        hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    } else {
                                        if (montoMap5.get(entry.getValue()) > Math.round(Double.parseDouble((((precioObs * cantidadObs) * (100 - porcObs)) / 100) + ""))) {
                                            long porcObsAnterior = 100 - Math.round((montoMap5.get(entry.getValue()) * 100) / (precioObs * cantidadObs));
                                            descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100) + "");
                                            descTarjetaFiel = true;
                                            montoConDes5 = montoConDes5 + (((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100);
                                            montoMap5.put(entry.getValue(), montoMap5.get(entry.getValue()) + Math.round(((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100));
                                            hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (porcObs - porcObsAnterior)) / 100));
                                        }
                                    }
                                } else {
                                    descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * porcObs) / 100) + "");
                                    descTarjetaFiel = true;
                                    montoConDes5 = montoConDes5 + (((precioObs * cantidadObs) * (100 - porcObs)) / 100);
                                    montoMap5.put(entry.getValue(), Math.round(((precioObs * cantidadObs) * (100 - porcObs)) / 100));
                                    hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    //precioObs unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                                }
                            } else {
                                if (montoMapExe.containsKey(entry.getValue())) {
                                    if (hmObsequioRestoEspecial.containsKey(entry.getKey())) {
                                        descuentoFielSum += Double.parseDouble((((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * porcObs) / 100) + "");
                                        descTarjetaFiel = true;
                                        exenta = Math.round(exenta + (((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100));
                                        montoMapExe.put(entry.getValue(), Math.round(((precioObs * hmObsequioRestoEspecial.get(entry.getKey())) * (100 - porcObs)) / 100));
                                        hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    } else {
                                        if (montoMapExe.get(entry.getValue()) > Math.round(Double.parseDouble((((precioObs * cantidadObs) * (100 - porcObs)) / 100) + ""))) {
                                            long porcObsAnterior = 100 - Math.round((montoMapExe.get(entry.getValue()) * 100) / (precioObs * cantidadObs));
                                            descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100) + "");
                                            descTarjetaFiel = true;
                                            exenta = Math.round(exenta + (((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100));
                                            montoMapExe.put(entry.getValue(), montoMapExe.get(entry.getValue()) + Math.round(((precioObs * cantidadObs) * (porcObs - porcObsAnterior)) / 100));
                                            hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (porcObs - porcObsAnterior)) / 100));
                                        }
                                    }
                                } else {
                                    descuentoFielSum += Double.parseDouble((((precioObs * cantidadObs) * porcObs) / 100) + "");
                                    descTarjetaFiel = true;
                                    exenta = Math.round(exenta + (((precioObs * cantidadObs) * (100 - porcObs)) / 100));
                                    montoMapExe.put(entry.getValue(), Math.round(((precioObs * cantidadObs) * (100 - porcObs)) / 100));
                                    hashDetallePrecioDesc.put(entry.getValue(), Math.round((precioObs * (100 - porcObs)) / 100));
                                    //precioObs unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
    private void calculandoTarjeta(boolean soloTarjeta) {
//        if (BuscarClienteFXMLController.getJsonClienteFiel() != null || jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        seteandoTotales();
        //validación de descuento, solo para una tarjeta, además de no permitir efectivo, cheque, vale, nota de crédito, tarjeta convenio, moneda extranjera...
        int cont = 0;
        String descripcionTarj = "N/A";
        for (JSONObject jSONObject : listJsonFormaPago) {
            if (jSONObject.containsKey("TARJETAS")) {
                if (cont == 0) {
                    descripcionTarj = jSONObject.get("comboBoxTarjetas").toString();
                }
                cont++;
            }
        }
        if (cont < 2 && soloTarjeta) {
            //verificación de descuento según tarjeta, se toma el nombre asignado en la línea de tiempo, 
            //si se modifica el nombre de la tarjeta por "a" o "b" motivo, se dejará de aplicar descuento,
            //en caso de querer aplicar descuento con el nuevo nombre tarjeta, deberá dar de baja la tarjeta con el nombre viejo en configuración descuentos...
            if (!descripcionTarj.contentEquals("N/A")) {
            } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
                descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
            }
            if (Descuento.getHashDescTarjeta() != null) {
                if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
                    JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
                    if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) == -1
                            || (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) <= Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())))) {
                        JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
                        Calendar calendar = Calendar.getInstance();
                        JSONArray jsonArrayDiaEsp = new JSONArray();
                        int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                        for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
                            JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                            JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                            int dia = toIntExact((long) jsonDia.get("idDia"));
                            if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
                                String horaIni = (String) jsonTarjCab.get("horaInicio");
                                String horaFin = (String) jsonTarjCab.get("horaFin");
                                if (horaIni != null && horaFin != null) {//si cumple, se asignó un rango de horario...
                                    Time horaIniT = Utilidades.objectToTime(horaIni);
                                    Time horaFinT = Utilidades.objectToTime(horaFin);
                                    Date date = new Date();
                                    String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                    Time t = Utilidades.objectToTime(hora);
                                    if (horaIniT.getTime() <= t.getTime() && horaFinT.getTime() >= t.getTime()) {
                                        JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                        if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                            if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                    JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                    JSONArray jsonArrayArticuloNf;
                                                    JSONObject jsonNf;
                                                    JSONObject jsonArticuloNf;
                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else {
                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                    }
                                                }
                                            } else {
                                                for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                    JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                    if ((Boolean) jsonTarjCab.get("extracto")) {
                                                        //se omite la bajada por el tema del convenio...
                                                        if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                + jsonArticulo.get("codArticulo"))) {
                                                            double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                            double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                            descTarjeta = true;
                                                            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else {
                                                                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            }
                                                        } else {
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    } else {
                                                        if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                + jsonArticulo.get("codArticulo"))) {
                                                            double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                            double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                            descTarjeta = true;
                                                            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else {
                                                                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            }
                                                        } else {
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                JSONArray jsonArrayArticuloNf;
                                                JSONObject jsonNf;
                                                JSONObject jsonArticuloNf;
                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else {
                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                JSONArray jsonArrayArticuloNf;
                                                JSONObject jsonNf;
                                                JSONObject jsonArticuloNf;
                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else {
                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                }
                                            }
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                    //se omite la bajada por el tema del convenio...
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                        double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                        descTarjeta = true;
                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else {
                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        }
                                                    } else {
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                } else {
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                        double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                        descTarjeta = true;
                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else {
                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        }
                                                    } else {
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                            JSONArray jsonArrayArticuloNf;
                                            JSONObject jsonNf;
                                            JSONObject jsonArticuloNf;
                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else {
                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                            }
                                        }
                                    }
                                }
                            } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
                                jsonArrayDiaEsp.add(jsonTarjDet);
                            }
                        }
                        if (!descTarjeta && !jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
                            HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
                            if (hashDiaEspecial != null) {
                                for (Object tarjDetObj : jsonArrayDiaEsp) {
                                    JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                                    JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                                    if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
                                        if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
                                            String horaIni = (String) jsonTarjCab.get("horaInicio");
                                            String horaFin = (String) jsonTarjCab.get("horaFin");
                                            if (horaIni != null && horaFin != null) {//si cumple, se asignó un rango de horario...
                                                Time horaIniT = Time.valueOf(horaIni);
                                                Time horaFinT = Time.valueOf(horaFin);
                                                Date date = new Date();
                                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                                Time t = Time.valueOf(hora);
                                                if (horaIniT.getTime() <= t.getTime() && horaFinT.getTime() >= t.getTime()) {
                                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                                JSONArray jsonArrayArticuloNf;
                                                                JSONObject jsonNf;
                                                                JSONObject jsonArticuloNf;
                                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else {
                                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                }
                                                            }
                                                        } else {
                                                            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                                    //se omite la bajada por el tema del convenio...
                                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                            + jsonArticulo.get("codArticulo"))) {
                                                                        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                        double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                        descTarjeta = true;
                                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else {
                                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        }
                                                                    } else {
                                                                        JSONArray jsonArrayArticuloNf;
                                                                        JSONObject jsonNf;
                                                                        JSONObject jsonArticuloNf;
                                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else {
                                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                            + jsonArticulo.get("codArticulo"))) {
                                                                        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                        double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                        descTarjeta = true;
                                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else {
                                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        }
                                                                    } else {
                                                                        JSONArray jsonArrayArticuloNf;
                                                                        JSONObject jsonNf;
                                                                        JSONObject jsonArticuloNf;
                                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else {
                                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                                if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                    if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    } else {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            if ((Boolean) jsonTarjCab.get("extracto")) {
                                                                //se omite la bajada por el tema del convenio...
                                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                        + jsonArticulo.get("codArticulo"))) {
                                                                    double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                    double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                                    descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                    descTarjeta = true;
                                                                    if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                        montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                        montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else {
                                                                        exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    }
                                                                } else {
                                                                    JSONArray jsonArrayArticuloNf;
                                                                    JSONObject jsonNf;
                                                                    JSONObject jsonArticuloNf;
                                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else {
                                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                    }
                                                                }
                                                            } else {
                                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                        + jsonArticulo.get("codArticulo"))) {
                                                                    double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                                                                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                    double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                                    descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                    descTarjeta = true;
                                                                    if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                        montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                        montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else {
                                                                        exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    }
                                                                } else {
                                                                    JSONArray jsonArrayArticuloNf;
                                                                    JSONObject jsonNf;
                                                                    JSONObject jsonArticuloNf;
                                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else {
                                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                                                        JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                        //tool tip
                    }
                }
            }
        }
        if (descTarjeta) {//se da prioridad a descuento tarjeta, no importa el monto (mayor o menor a otra posible combinación)
            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
            descuento = Math.round(descuentoTarjetaSum);
            totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            seteandoTotalAbonado();
            bloqueandoPanelesTarj();
        } else {
            desbloqueandoPanelesTarj();
            //se valida para la carga inicial del controlador
            if (BuscarClienteFXMLController.getJsonClienteFiel() != null || jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                calculandoPromocionTemporada(true);//verifica también si hay dto. func. y fiel
            }
        }
//        }
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoDescTarjCabNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson, JSONObject jsonTarjCab) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescTarjetaCabNf1().get(jsonNf.get("idNf1Tipo")) != null) {
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf1()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf1Tipo") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    }
                }
                break;
            case 2:
                if (Descuento.getHashDescTarjetaCabNf2().get(jsonNf.get("idNf2Sfamilia")) != null) {
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf2()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf2Sfamilia") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                        calculandoDescTarjCabNf(1, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoDescTarjCabNf(1, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 3:
                if (Descuento.getHashDescTarjetaCabNf3().get(jsonNf.get("idNf3Sseccion")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf3()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf3Sseccion") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                        calculandoDescTarjCabNf(2, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoDescTarjCabNf(2, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 4:
                if (Descuento.getHashDescTarjetaCabNf4().get(jsonNf.get("idNf4Seccion1")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf4()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf4Seccion1") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                        calculandoDescTarjCabNf(3, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoDescTarjCabNf(3, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 5:
                if (Descuento.getHashDescTarjetaCabNf5().get(jsonNf.get("idNf5Seccion2")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf5()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf5Seccion2") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                        calculandoDescTarjCabNf(4, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoDescTarjCabNf(4, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 6:
                if (Descuento.getHashDescTarjetaCabNf6().get(jsonNf.get("idNf6Secnom6")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf6()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf6Secnom6") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                        calculandoDescTarjCabNf(5, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoDescTarjCabNf(5, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 7:
                if (Descuento.getHashDescTarjetaCabNf7().get(jsonNf.get("idNf7Secnom7")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf7()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf7Secnom7") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                        calculandoDescTarjCabNf(6, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoDescTarjCabNf(6, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoDescTarjNf(JSONObject jsonTarjCab, JSONObject detalleArtJson) {
        if ((Boolean) jsonTarjCab.get("extracto")) {
            //se omite la bajada por el tema del convenio...
            double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            double porc = (double) jsonTarjCab.get("porcentajeParana");
            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
            descTarjeta = true;
            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else {
                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else {
            double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            double porc = (double) jsonTarjCab.get("porcentajeDesc");
            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
            descTarjeta = true;
            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else {
                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        }
    }
    //TARJETAS******************************************************************

    //TARJETAS CONVENIO*********************************************************
    private void calculandoTarjetaConvenio() {
        if (hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem())) {//validación de descuento, solo para una tarjeta convenio...
            seteandoTotales();
            String parts = jfxComboBoxTipoDto.getSelectionModel().getSelectedItem();
            if (Descuento.getHashDescTarjetaConv().get(parts) != null) {
                JSONObject jsonTarjConvCab = Descuento.getHashDescTarjetaConv().get(parts);
                JSONArray jsonArrayTarjConvDet = (JSONArray) jsonTarjConvCab.get("descuentoTarjetaConvenioDetDTO");
                Calendar calendar = Calendar.getInstance();
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                for (Object tarjDetObj : jsonArrayTarjConvDet) {//verificación, día de descuento...
                    JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                    JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                    int dia = toIntExact((long) jsonDia.get("idDia"));
                    if (dia == diaSemana) {
                        double porc = (double) jsonTarjConvCab.get("porcentajeDesc");//descuento por el total...
                        long precio = FacturaDeVentaMayFXMLController.getPrecioTotal();//desde factura venta...
                        descuentoTarjetaConvSum += Double.parseDouble((precio * porc / 100) + "");
                        descTarjetaConvenio = true;
                    }
                }
            }
            descuento = Math.round(descuentoTarjetaConvSum);
            totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            seteandoTotalAbonado();
        } else {
            mensajeAdv("CÓMO MÁXIMO UNA TARJETA CONVENIO.");
        }
    }

    private void calculandoTarjetaConvenioValidacionTar() {
        comboBoxTarjetas.getSelectionModel().select(0);
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
        seteandoTotales();
        String parts = jfxComboBoxTipoDto.getSelectionModel().getSelectedItem();
        if (Descuento.getHashDescTarjetaConv().get(parts) != null) {
            JSONObject jsonTarjConvCab = Descuento.getHashDescTarjetaConv().get(parts);
            JSONArray jsonArrayTarjConvDet = (JSONArray) jsonTarjConvCab.get("descuentoTarjetaConvenioDetDTO");
            Calendar calendar = Calendar.getInstance();
            int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
            for (Object tarjDetObj : jsonArrayTarjConvDet) {//verificación, día de descuento...
                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                int dia = toIntExact((long) jsonDia.get("idDia"));
                if (dia == diaSemana) {
                    double porc = (double) jsonTarjConvCab.get("porcentajeDesc");//descuento por el total...
                    long precio = FacturaDeVentaMayFXMLController.getPrecioTotal();//desde factura venta...
                    descuentoTarjetaConvSum += Double.parseDouble((precio * porc / 100) + "");
                    descTarjetaConvenio = true;
                }
            }
        }
        descuento = Math.round(descuentoTarjetaConvSum);
        totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
        seteandoTotalAbonado();
    }
    //TARJETAS CONVENIO*********************************************************

    //PROMOCIÓN TEMPORADA*******************************************************
    private void calculandoPromocionTemporada(boolean tarjeta) {
        seteandoTotales();
        hashJsonPromocionDesc = new HashMap<>();
        hashJsonPromocionArtDesc = new HashMap<>();
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        hmDetallePromoObsequio = new HashMap<>();
        hmObsequioRestoEspecial = new HashMap<>();
        //se toma primero artículo, luego sección, de existir en ambos, se da prioridad a artículo...
        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
            //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
            JSONObject jsonArticuloB = (JSONObject) detalleArtJson.get("articulo");
            if (!FacturaDeVentaMayFXMLController.getHmGift().containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                if (!(boolean) jsonArticuloB.get("bajada")) {
                    if (Descuento.getHashPromTempArtObsequio().containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                        if (hmDetallePromoObsequio.containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                            JSONObject detalleArtJsonC = hmDetallePromoObsequio.get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()));
                            double cantidad = Double.parseDouble(detalleArtJsonC.get("cantidad").toString()) + Double.parseDouble(detalleArtJson.get("cantidad").toString());
                            detalleArtJsonC.put("cantidad", cantidad);
                            hmDetallePromoObsequio.remove(Long.valueOf(jsonArticuloB.get("codArticulo").toString()), detalleArtJsonC);
                            hmDetallePromoObsequio.put(Long.valueOf(jsonArticuloB.get("codArticulo").toString()), detalleArtJsonC);
                        } else {
                            JSONObject detalleArtJsonC = new JSONObject();
                            detalleArtJsonC.put("descripcion", detalleArtJson.get("descripcion"));
                            detalleArtJsonC.put("facturaClienteCab", detalleArtJson.get("facturaClienteCab"));
                            detalleArtJsonC.put("peso", detalleArtJson.get("peso"));
                            detalleArtJsonC.put("gravada", detalleArtJson.get("gravada"));
                            detalleArtJsonC.put("permiteDesc", detalleArtJson.get("permiteDesc"));
                            detalleArtJsonC.put("bajada", detalleArtJson.get("bajada"));
                            detalleArtJsonC.put("poriva", detalleArtJson.get("poriva"));
                            detalleArtJsonC.put("seccion", detalleArtJson.get("seccion"));
                            detalleArtJsonC.put("precio", detalleArtJson.get("precio"));
                            detalleArtJsonC.put("articulo", detalleArtJson.get("articulo"));
                            detalleArtJsonC.put("cantidad", detalleArtJson.get("cantidad"));
                            detalleArtJsonC.put("orden", detalleArtJson.get("orden"));
                            detalleArtJsonC.put("primeraInsercion", detalleArtJson.get("primeraInsercion"));
                            detalleArtJsonC.put("seccionSub", detalleArtJson.get("seccionSub"));
                            hmDetallePromoObsequio.put(Long.valueOf(jsonArticuloB.get("codArticulo").toString()), detalleArtJsonC);
                        }
                    } else if (Descuento.getHashPromTempArt().containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                        //verificación de descuento según artículo...
                        double descuentoPromoArt = 0;
                        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                        double porc = Double.valueOf(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())).get("porcentajeDesc").toString());
                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                        descuentoPromoArt = (double) (descuentoPromoArt + ((precio * cantidad) * porc) / 100);//descuento total en esta promoción...
                        descuentoPromoArtSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                        descPromoTemp = true;
                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                            montoConDes10 = (double) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                            montoConDes5 = (double) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        } else {
                            exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        }
                        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por artículo
                        hashDetallePrecioDescPromoArt.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
                        if (hashJsonPromocionArtDesc.containsKey(
                                Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))))) {
                            hashJsonPromocionArtDesc.put(
                                    Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                    hashJsonPromocionArtDesc.get(Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())))) + Math.round(descuentoPromoArt));
                        } else {
                            hashJsonPromocionArtDesc.put(
                                    Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                    Math.round(descuentoPromoArt));
                        }
                    }
                } else {
                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                        //promoción + bajada
                    }
                }
            }
        }
        for (Map.Entry<Long, JSONObject> entry : hmDetallePromoObsequio.entrySet()) {
            JSONObject jsonArticuloC = (JSONObject) entry.getValue().get("articulo");
            double descuentoPromoArt = 0;
            double cantidad = Double.parseDouble(entry.getValue().get("cantidad").toString());
            double minReq = Double.parseDouble(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString())).get("minReq").toString());
            int cantObsequio = Integer.valueOf(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString())).get("cantObsequio").toString());
            long precio = Long.valueOf(entry.getValue().get("precio").toString());
            if (cantidad >= minReq) {
                double mod = cantidad / minReq;
                if (mod > 1) {
                    int enteroAux = (int) mod;
                    if (enteroAux < mod) {
                        hmObsequioRestoEspecial.put(Long.valueOf(jsonArticuloC.get("codArticulo").toString()), cantidad - (enteroAux * minReq));
                    }
                }
                int entero = (int) mod;
                cantObsequio = cantObsequio * entero;
                descuentoPromoArt = (double) (descuentoPromoArt + (precio * cantObsequio));//descuento total en esta promoción...
                descuentoPromoArtSum += descuentoPromoArt;
                descPromoTemp = true;
                if (Long.valueOf(entry.getValue().get("poriva").toString()) == 10) {
                    montoConDes10 = (double) (montoConDes10 + (precio * (cantidad - cantObsequio)));
                    montoMap10.put(entry.getValue(), Math.round((double) (precio * (cantidad - cantObsequio))));
                } else if (Long.valueOf(entry.getValue().get("poriva").toString()) == 5) {
                    montoConDes5 = (double) (montoConDes5 + (precio * (cantidad - cantObsequio)));
                    montoMap5.put(entry.getValue(), Math.round((double) (precio * (cantidad - cantObsequio))));
                } else {
                    exenta = (long) (exenta + (precio * (cantidad - cantObsequio)));
                    montoMapExe.put(entry.getValue(), Math.round((double) (precio * (cantidad - cantObsequio))));
                }
                //precio unitario neto, en detalle, solo para aquellos que manejan descuento por artículo
                hashDetallePrecioDescPromoArt.put(entry.getValue(), (long) (precio * (cantidad - cantObsequio)));
                if (hashJsonPromocionArtDesc.containsKey(
                        Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString()))))) {
                    hashJsonPromocionArtDesc.put(
                            Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString()))),
                            hashJsonPromocionArtDesc.get(Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString())))) + Math.round(descuentoPromoArt));
                } else {
                    hashJsonPromocionArtDesc.put(
                            Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloC.get("codArticulo").toString()))),
                            Math.round(descuentoPromoArt));
                }
            }
        }
        descuentoPromoNf = 0;
        for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
            if (!hashDetallePrecioDescPromoArt.containsKey(detalleArtJson)) {//se da prioridad a artículo, luego NF
                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                if (!FacturaDeVentaMayFXMLController.getHmGift().containsKey(Long.valueOf(jsonArticulo.get("codArticulo").toString()))) {
                    JSONArray jsonArrayArticuloNf;
                    JSONObject jsonNf;
                    JSONObject jsonArticuloNf;
                    //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
                    if (!(boolean) jsonArticulo.get("bajada")) {
                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                            calculandoPromoTempNf(7, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                            calculandoPromoTempNf(6, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                            calculandoPromoTempNf(5, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                            calculandoPromoTempNf(4, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                            calculandoPromoTempNf(3, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                            calculandoPromoTempNf(2, jsonNf, detalleArtJson);
                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                            calculandoPromoTempNf(1, jsonNf, detalleArtJson);
                        } else {
                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                        }
                    } else {
                        if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                            //promoción + bajada
                        }
                    }
                }
            }
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVenta
        if (descPromoTemp) {
            if (!tarjeta) {
                if (!valeActivo) {
                    if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                        entradaFunc1era = false;
                        calculandoFuncionario();
                    } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                    }
                } else {
                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")
                            && comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
                        if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                            entradaFunc1era = false;
                            calculandoFuncionario();
                        } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                            entradaFiel1era = false;
                            calculandoClienteFiel();
                        }
                    }
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("tarjeta")
                        && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || hmFormaPago.containsKey("TARJETAS"))) {
                    if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                        entradaFunc1era = false;
                        calculandoFuncionario();
                    } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                    }
                }
            }
            for (JSONObject detalleArtJson : FacturaDeVentaMayFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFielSum) + Math.round(descuentoFuncSum) + Math.round(descuentoGift);
            totalAPagar = FacturaDeVentaMayFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            seteandoTotalAbonado();
        } else if (!tarjeta) {
            if (!valeActivo) {
                if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                    entradaFunc1era = true;
                    calculandoFuncionario();
                } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                    entradaFiel1era = true;
                    calculandoClienteFiel();
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")
                        && comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
                    if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                        entradaFunc1era = true;
                        calculandoFuncionario();
                    } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = true;
                        calculandoClienteFiel();
                    }
                }
            }
        } else {
            Utilidades.log.info(comboBoxTarjetas.getSelectionModel().getSelectedItem());
            if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("tarjeta")
                    && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || hmFormaPago.containsKey("TARJETAS"))) {
                if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                    entradaFunc1era = true;
                    calculandoFuncionario();
                } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                    entradaFiel1era = true;
                    calculandoClienteFiel();
                }
            }
        }
//        editandoBarChart();
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoPromoTempNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        Date date = new Date();
        Timestamp tsNow = new Timestamp(date.getTime());
        switch (nf) {
            case 1:
                if (Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                }
                break;
            case 2:
                if (Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoPromoTempNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoPromoTempNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoPromoTempNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoPromoTempNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoPromoTempNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoPromoTempNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoPromoTempDet(JSONObject jsonPromoTempCab, JSONObject jsonPromoTempDet, JSONObject detalleArtJson) {
        double cantidad = Double.parseDouble(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonPromoTempDet.get("porcentajeDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        descuentoPromoNf = Math.round(Double.parseDouble((((precio * cantidad) * porc) / 100) + ""));//descuento total en esta promoción...
        descuentoPromoNfSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");//descuento global por promoción
        descPromoTemp = true;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        } else {
            exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        }
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
        if (descuentoPromoNf > 0) {
            if (hashJsonPromocionDesc.containsKey(jsonPromoTempCab)) {
                descuentoPromoNf = descuentoPromoNf + hashJsonPromocionDesc.get(jsonPromoTempCab);
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            } else {
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            }
        }
    }

    private void checkBoxPromoTempButton() {
        checkBoxPromoTemp();
    }

    private void checkBoxPromoTemp() {
        if (toggleButton) {
            if (!hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem())) {
                validandoPromocion();
            }/* else {
                mensajeAdv("PARA APLICAR PROMOCIÓN TEMPORADA\nEL PANEL TARJETAS CONVENIO DEBE ESTAR VACÍO.");
            }*/
        } else {
            invalidandoPromocion();
            seteandoTotalAbonado();
            seteandoDescTarjeta();
            if (!valeActivo) {
                validandoCambiandoFormaPago();
            }
            if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                calculandoFuncionario();
            }
        }
    }

    private void validandoPromocion() {
//        if (textFieldMontoPorc.getText().contentEquals("")
//                && textFieldMontoRetencion.getText().contentEquals("")) {
        calculandoPromocionTemporada(false);
//        }
    }

    private void invalidandoPromocion() {
//        if (textFieldMontoPorc.getText().contentEquals("")
//                && textFieldMontoRetencion.getText().contentEquals("")) {
        seteandoTotales();
//        }
    }
    //PROMOCIÓN TEMPORADA*******************************************************

    //VALE**********************************************************************
    private void calculandoVale(String tipoVale) {
        if (!descTarjeta && !descTarjetaConvenio) {
            if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                checkBoxPromoTempButton();
            } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
                checkBoxPromoTempButton();
            } else {
                seteandoTotales();
                seteandoTotalAbonado();
            }
        } else {
            validandoCambiandoFormaPago();
        }
        /*switch (tipoVale) {
            case "FUNCIONARIO":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "ORDEN DE COMPRA":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "ACUERDO COMERCIAL":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "ASOCIACIÓN":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "ARGOS":
//                if (!textFieldMontoValePorc.getText().contentEquals("")) {
                descValePorc = true;
//                }
                break;
            case "PARESA":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "FRIGOMERC":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "INSPECTORATE":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "CELLULAR":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "ESSEN":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            case "VALE":
//                if (!textFieldMontoVale.getText().contentEquals("")) {
                descValeMonto = true;
//                }
                break;
            default:
                break;
        }*/
    }

    //VALE**********************************************************************
    //DIRECTIVO*****************************************************************
    private void calculandoDirectivo() {
        seteandoTotales();
        seteandoControlDesc();
        descDirectivo = true;
        seteandoTotalAbonado();
    }

    //DIRECTIVO*****************************************************************
    //NOTA DE CRÉDITO***********************************************************
    private void calculandoNotaCredito() {
        boolean auxDirect = descDirectivo;
//        seteandoTotalesParaNotaCred();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }

    //NOTA DE CRÉDITO***********************************************************
    //MONTO RETENCIÓN***********************************************************
    private void calculandoMontoRetencion() {
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }
    //MONTO RETENCIÓN***********************************************************
    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->

    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
    private void actualizandoTabla() {
        tableViewFormaDePago.getItems().clear();
        tableViewFormaDePago.getItems().addAll(listJsonFormaPago);
        if (!tableViewFormaDePago.getItems().isEmpty()) {
            tableViewFormaDePago.getSelectionModel().selectLast();
        }
        if (!listJsonFormaPago.isEmpty()) {
            Platform.runLater(() -> tableViewFormaDePago.scrollTo(listJsonFormaPago.size() - 1));
        }
    }

    private void vistaJSONObjectFormaDePago() {
        articuloDetData = FXCollections.observableArrayList(listJsonFormaPago);
        tableColumnFormaDePago.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFormaDePago.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("formaPago").toString());
            }
        });
        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnMonto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject datas = data.getValue();
                String valor = "";
                double precioMinDouble = Long.parseLong(datas.get("monto").toString());
                numValidator = new NumberValidator();
                valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
                return new SimpleStringProperty(valor);
            }
        });
        tableViewFormaDePago.setItems(articuloDetData);
        //**********************************************************************
    }
    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->

    //BBDD ****************************** -> -> -> -> -> -> -> -> -> -> -> -> ->
    //////READ, TARJETA CONVENIO
    public void generarComboTarjetaConvenioLocal() {
        JSONParser p = new JSONParser();
        List<TarjetaConvenio> tarConv = tarConvenioDAO.listar();
        for (TarjetaConvenio tarCon : tarConv) {
            try {
                JSONObject tarjetaConvenio = (JSONObject) p.parse(gson.toJson(tarCon.toTarjetaConvenioNombreIdDTO()));
                tarjetaConvenioList.add(tarjetaConvenio);
                jfxComboBoxTipoDto.getItems().add(tarjetaConvenio.get("descripcion").toString());
                hashJsonComboTarjConvenio.put(tarjetaConvenio.get("descripcion").toString(), tarjetaConvenio);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
    }
    //////READ, TARJETA CONVENIO

    //////READ, TARJETA -> GET
    private void jsonCargandoTarjetas() {
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        generarComboTarjetaLocal();
    }
    //////READ, TARJETA -> GET

    //////READ, VALE -> GET
    private void jsonCargandoVales() {
        cargarVale = false;
        valeList = new ArrayList<>();
        hashJsonComboVale = new HashMap<>();
        generarComboValeLocal();
    }
    //////READ, VALE -> GET

    //////READ, TARJETA
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void generarComboTarjetaLocal() {
        JSONParser p = new JSONParser();
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        List<Tarjeta> listTarjeta = tarDAO.listar();
        comboBoxTarjetas.getItems().clear();
        comboBoxTarjetas.getItems().add("");
        if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("SÁB. COOP.")) {
            //tarjetas con descuento
            for (JSONObject jsonObjectDescuentoTarjeta : Descuento.getHashDescTarjeta().values()) {
                comboBoxTarjetas.getItems().add(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString());
                hashJsonComboTarjeta.put(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString(), jsonObjectDescuentoTarjeta);
            }
        } else {
            //tarjetas sin descuento
            for (Tarjeta tar : listTarjeta) {
                try {
                    JSONObject tarjeta = (JSONObject) p.parse(gson.toJson(tar.toTarjetaDTO()));
                    comboBoxTarjetas.getItems().add(tarjeta.get("descripcion").toString());
                    hashJsonComboTarjeta.put(tarjeta.get("descripcion").toString(), tarjeta);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
        }
        comboBoxTarjetas.setCellFactory(parametro -> new FormasDePagoMayFXMLController.MyListCell());
        comboBoxTarjetas.setButtonCell(new FormasDePagoMayFXMLController.MyListCell());
        new FormasDePagoMayFXMLController.AutoCompleteComboBoxListener<>(comboBoxTarjetas);
        comboBoxTarjetas.getSelectionModel().select(0);
    }
    //////READ, TARJETA

    //////READ, VALE
    public void generarComboValeLocal() {
        JSONParser p = new JSONParser();
        List<Vales> val = valeDAO.listar();
        comboBoxVale.getItems().add("Seleccione un tipo de vale...");
        for (Vales vales : val) {
            try {
                JSONObject vale = (JSONObject) p.parse(gson.toJson(vales.toValeDTO()));
                valeList.add(vale);
                comboBoxVale.getItems().add(vale.get("descripcionVale").toString());
                hashJsonComboVale.put(vale.get("descripcionVale").toString(), vale);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        comboBoxVale.getSelectionModel().select(0);
    }
    //////READ, VALE

    //////READ, TIPO MONEDA -> GET
    private void jsonTipoMoneda() {
        tipoMonedaJSONArray = null;
        tipoMonedaJSONArray = recuperarTipoMonedaLocal();
        cotizacionList = new ArrayList<>();
        for (Object obj : tipoMonedaJSONArray) {
            JSONObject tipoMoneda = (JSONObject) obj;
            cotizacionList.add(tipoMoneda);
        }
    }
    //////READ, TIPO MONEDA -> GET

    //////READ -> TIPO MONEDA
    @SuppressWarnings("empty-statement")
    private JSONArray recuperarTipoMonedaLocal() {
        JSONParser p = new JSONParser();
        JSONArray array = new JSONArray();
        for (TipoMoneda tp : tipoMonedaDAO.listar()) {
            try {
                JSONObject obj = (JSONObject) p.parse(gson.toJson(tp.toTipoMonedaDTO()));
                array.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        };
        return array;
    }
    //////READ -> TIPO MONEDA

    //////READ, FUNCIONARIO
    public JSONObject generarListaFuncionariosLocal(String ci) {
        Funcionario fun = funDAO.listarFuncionarioPorCi(ci);
        JSONObject jsonFun = null;
        try {
            FuncionarioDTO fdto = fun.toFuncionarioDTO();
            String x = gson.toJson(fdto);
            jsonFun = (JSONObject) parser.parse(x);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return jsonFun;
    }
    //////READ, FUNCIONARIO

    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT
    private boolean actualizandoCabFactura() {
        JSONParser p = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (!jsonDatos.isNull("cancelProducto")) {
            FacturaDeVentaMayFXMLController.cancelacionProd = true;
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        try {
            editandoJsonFactCab();
            if (!FacturaDeVentaMayFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                org.json.JSONObject json = new org.json.JSONObject(datos);
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                FacturaDeVentaMayFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = FacturaDeVentaMayFXMLController.getCabFactura().get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject sucursal = (JSONObject) p.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) p.parse(datos.get("caja").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroActualmente = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                datos.put("nroFact", nroActualmente);
                FacturaDeVentaMayFXMLController.getCabFactura().put("nroFactura", nroActualmente);
            }
            if (!jsonDatos.isNull("cancelProducto")) {
                JSONObject talona = (JSONObject) p.parse(datos.get("talonarioSucursal").toString());
                long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
                TalonariosSucursales tal = taloDAO.getById(idTalonario);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
            }
            FacturaDeVentaMayFXMLController.setCabFactura(registrarFacturaCabeceraLocal(FacturaDeVentaMayFXMLController.getCabFactura().toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("caida", "forma_pago");
        if (FacturaDeVentaMayFXMLController.getCabFactura() != null) {
            exitoInsertarCab = true;
        }
        //para registrar facturaIni
        org.json.JSONObject jsons = new org.json.JSONObject(datos);
        boolean estadoFa = false;
        if (!jsons.isNull("estadoFacturaInicial")) {
            estadoFa = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        boolean estadoInicial = estadoFa;
        if (!estadoInicial) {
            datos.put("facturaInicial", FacturaDeVentaMayFXMLController.getCabFactura().get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("facturaFinal", FacturaDeVentaMayFXMLController.getCabFactura().get("nroFactura").toString());
        if (FacturaDeVentaMayFXMLController.isActualizarDatosCabecera()) {
            caida = false;
        }
        //VERIFY
        if (!jsonDatos.isNull("cancelProducto")) {
            caida = false;
        }
        if (exitoInsertarCab && !caida) {
            if (FacturaDeVentaMayFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
                JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaMayFXMLController.getCabFactura());
                JSONArray arrayDetalle = new JSONArray();
                for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                    try {
                        JSONObject json = (JSONObject) p.parse(jsonArrayFactDet.get(i).toString());
                        JSONObject art = (JSONObject) p.parse(json.get("articulo").toString());
                        art.put("fechaAlta", null);
                        art.put("fechaMod", null);
                        JSONObject iva = (JSONObject) p.parse(art.get("iva").toString());
                        iva.put("fechaAlta", null);
                        iva.put("fechaMod", null);
                        art.put("iva", iva);
                        json.put("articulo", art);
                        arrayDetalle.add(json);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                DatosEnCaja.setDatos(datos);
                DatosEnCaja.setUsers(users);
                DatosEnCaja.setFacturados(fact);
                datos = DatosEnCaja.getDatos();
                users = DatosEnCaja.getUsers();
                fact = DatosEnCaja.getFacturados();
                if (creandoFactDet(arrayDetalle)) {
                    exitoInsertarDet = false;
                }
                gestionandoPago(FacturaDeVentaMayFXMLController.getCabFactura());
            } else {
                mensajeAdv("LA FACTURA NO HA SIDO PROCESADA.");
            }
        }
        return exitoInsertarCab;
    }
    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT

    ////CREATE, FACTURA DET. MASIVO -> POST
    private boolean creandoFactDet(JSONArray jsonArray) {
        JSONParser p = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) p.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) p.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
            exitoInsertarDet = registrarArrayDetalleLocal(jsonArrayFactDet.toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }
    ////CREATE, FACTURA DET. MASIVO -> POST

    ////CREATE, FACTURA CAB. EFECTIVO -> POST
    private boolean creandoCabFacturaEfectivo(JSONObject cabFactura, JSONObject formaPagoEfectivo) {
        JSONParser p = new JSONParser();
        JSONObject cabFacturaEfectivo = new JSONObject();
        boolean finalizo = false;
        long idFact = Long.parseLong(cabFactura.get("idFacturaClienteCab").toString());
        JSONObject obj = new JSONObject();
        obj.put("idFacturaClienteCab", idFact);
        cabFacturaEfectivo = creandoJsonEfectivo(obj, Integer.valueOf(numValidator.numberValidator(formaPagoEfectivo.get("textFieldEfectivo").toString())));
        long idRango = rangoEfectivoDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaEfectivo.put("idFacturaClienteCabEfectivo", idRango);
        try {
            Map mapeo = registrarFacturaEfectivoLocal(cabFacturaEfectivo);
            cabFacturaEfectivo = (JSONObject) p.parse(mapeo.get("cabFacturaEfectivo").toString());
            finalizo = (boolean) mapeo.get("finalizo");
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        datos.put("facturaEfectivo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. EFECTIVO -> POST

    //////CREATE, FACTURA CAB. CHEQUE -> POST
    private boolean creandoCabFacturaCheque(JSONArray jsonArr) {
        JSONParser p = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayCheque = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoChequeDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) p.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) p.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.put("facturaClienteCabDTO", facturaClienteCab);
                obj.put("idFacturaClienteCabCheque", idRango);
                jsonArrayCheque.add(obj);
            }
            finalizo = realizarFactChequeLocal(jsonArrayCheque);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaCheque", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CHEQUE -> POST

    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST
    private boolean creandoCabFacturaMonedaExt(JSONArray jsonArr) {
        JSONParser p = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayMonedaExt = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoMonedaExtraDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) p.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabMonedaExtranjera", idRango);
                jsonArrayMonedaExt.add(obj);
            }
            finalizo = registrarFacturaMonedaExtranjera(jsonArrayMonedaExt);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaMonedaExtranjera", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST

    //////CREATE, FACTURA CAB. TARJETA -> POST
    private boolean creandoCabFacturaTarjeta(JSONArray jsonArr) {
        JSONParser p = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayTarjeta = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoTarjetaDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) p.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabTarjeta", idRango);
                jsonArrayTarjeta.add(obj);
            }
            jsonArrayTarjeta = registrarCabTarjetaLocal(jsonArrayTarjeta);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (jsonArrayTarjeta != null) {
            finalizo = true;
            if (jsonArrayTarjeta.size() == 1) {
                JSONObject jsonTarj = (JSONObject) jsonArrayTarjeta.get(0);
                gestionandoDescuento(jsonTarj, false);
            }
        }
        datos.put("facturaTarjeta", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. TARJETA -> POST

    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjConv(JSONObject cabFactura) {
        JSONObject cabFacturaTarjConv = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjetaConvenioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaTarjConv = creandoJsonTarjetaConvenio(cabFactura);
        cabFacturaTarjConv.put("idFacturaClienteCabTarjConvenio", idRango);
        cabFacturaTarjConv = registrarTarjetaConvenioLocal(cabFacturaTarjConv);
        if (cabFacturaTarjConv != null) {
            gestionandoDescuento(cabFacturaTarjConv, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST

    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFiel(JSONObject cabFactura) {
        JSONObject cabFacturaClienteFiel = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjFielDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaClienteFiel = creandoJsonTarjClienteFiel(cabFactura);
        cabFacturaClienteFiel.put("idFacturaClienteCabTarjFiel", idRango);
        cabFacturaClienteFiel = registrarTarjetaFielLocal(cabFacturaClienteFiel);
        if (cabFacturaClienteFiel != null) {
            gestionandoDescuento(cabFacturaClienteFiel, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST

    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionario(JSONObject cabFactura) {
        JSONObject cabFacturaFuncionario = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionario = creandoJsonFuncionario(cabFactura);
        long idRango = rangoFuncionarioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaFuncionario.put("idFacturaClienteCabFuncionario", idRango);
        cabFacturaFuncionario = registrarCabFuncionarioLocal(cabFacturaFuncionario);
        if (cabFacturaFuncionario != null) {
            gestionandoDescuento(cabFacturaFuncionario, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST

    ////CREATE, VALE -> POST
    private boolean creandoCabFacturaValeMonto(JSONObject cabFactura, JSONObject jSONObject) {
        JSONObject cabFacturaValeMonto = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeMonto = creandoJsonValeMonto(cabFactura, jSONObject);
        finalizo = registrarValeLocal(cabFacturaValeMonto.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE -> POST

    ////GIFTCARD, VALE -> POST
    private boolean creandoCabFacturaGiftcard(JSONObject cabFactura, JSONObject jSONObject) {
        JSONParser p = new JSONParser();
        JSONObject cabFacturaGiftcard = new JSONObject();
        boolean finalizo = false;
        long idFact = Long.parseLong(cabFactura.get("idFacturaClienteCab").toString());
        JSONObject obj = new JSONObject();
        obj.put("idFacturaClienteCab", idFact);
        cabFacturaGiftcard = creandoJsonGiftcard(obj, Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldGiftcard").toString())), Long.parseLong(jSONObject.get("codigo").toString()));
        try {
            Map mapeo = registrarFacturaGiftcardLocal(cabFacturaGiftcard);
            cabFacturaGiftcard = (JSONObject) p.parse(mapeo.get("cabFacturaGiftcard").toString());
            finalizo = (boolean) mapeo.get("finalizo");
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        datos.put("facturaGiftcard", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, GIFTCARD -> POST

    ////CREATE, VALE PORC. -> POST
    private boolean creandoCabFacturaValePorc(JSONObject cabFactura, JSONObject jSONObject) {
        JSONObject cabFacturaValePorc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValePorc = creandoJsonValePorc(cabFactura, jSONObject);
        finalizo = registrarPorcValeLocal(cabFacturaValePorc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE PORC. -> POST

    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST
    private boolean creandoCabFacturaNotaCred(JSONArray jsonArr) {
        JSONParser p = new JSONParser();
        boolean finalizo = false;
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoNotaCredDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) p.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) p.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.remove("facturaClienteCabDTO");
                obj.put("facturaClienteCab", facturaClienteCab);
                obj.put("idFacturaClienteCabNotaCredito", idRango);
                obj.put("nroNota", obj.get("nroNota"));
                obj.put("monto", obj.get("monto"));
//                jsonArrayCheque.add(obj);
                finalizo = registrarNotaCreditoLocal(obj.toString());
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST

    //////CREATE, FACTURA CAB. RETENCIÓN -> POST
    private boolean creandoCabFacturaMontoRetencion(JSONObject cabFactura) {
        JSONObject cabFacturaMontoRetencion = new JSONObject();
        boolean finalizo = false;
        cabFacturaMontoRetencion = creandoJsonMontoRetencion(cabFactura);
        finalizo = registrarRetencion(cabFacturaMontoRetencion.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. RETENCIÓN -> POST

    ////CREATE, DESCUENTO VALE -> POST
    private boolean creandoCabFacturaValeDesc(JSONObject cabFactura, JSONObject vale) {
        JSONObject cabFacturaValeDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeDesc = creandoJsonDescVale(cabFactura, vale);
        finalizo = registrarDescValeLocal(cabFacturaValeDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO VALE -> POST

    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. ART. -> POST
    private boolean creandoCabFacturaPromTempArtDesc(JSONObject cabFacturaPromTempArtDesc) {
        boolean finalizo = false;
        JSONObject jsonPromTemp = null;
        cabFacturaPromTempArtDesc.put("idFacturaClienteCabPromoTempArt", rangoPromoTempArtDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
        jsonPromTemp = registrarPromoTempArt(cabFacturaPromTempArtDesc.toString());
        if (jsonPromTemp != null) {
            finalizo = true;
        }
        datos.put("facturaPromo", false);
        DatosEnCaja.setDatos(datos);
        return finalizo;
    }
    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. ART. -> POST

    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST
    private boolean creandoCabFacturaPromTempDesc(JSONObject cabFacturaPromTempDesc) {
        boolean finalizo = false;
        JSONObject jsonPromTemp = null;
        long idRango = rangoPromoTempDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaPromTempDesc.put("idFacturaClienteCabPromoTemp", idRango);
        jsonPromTemp = registrarPromoTemp(cabFacturaPromTempDesc.toString());
        if (jsonPromTemp != null) {
            finalizo = true;
        }
        datos.put("facturaPromo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST

    ////CREATE, DESCUENTO DIRECTIVO -> POST
    private boolean creandoCabFacturaDirectivoDesc(JSONObject cabFactura) {
        JSONObject cabFacturaDirectivoDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaDirectivoDesc = creandoJsonDescDirectivo(cabFactura);
        finalizo = registrarDescDirectivo(cabFacturaDirectivoDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO DIRECTIVO -> POST

    ////CREATE, DESCUENTO CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFielDesc(JSONObject cabFacturaFiel) {
        JSONObject cabFacturaClienteFielDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaClienteFielDesc = creandoJsonDescTarjFiel(cabFacturaFiel);
        if (Integer.parseInt(cabFacturaClienteFielDesc.get("montoDesc").toString()) != 0) {
            finalizo = registrarDescTarjFielLocal(cabFacturaClienteFielDesc.toString());
            datos.put("facturaTarjetaFiel", false);
            DatosEnCaja.setDatos(datos);
            datos = DatosEnCaja.getDatos();
        }
        return finalizo;
    }
    ////CREATE, DESCUENTO CLIENTE FIEL -> POST

    ////CREATE, DESCUENTO FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionarioDesc(JSONObject cabFacturaFuncionario) {
        JSONObject cabFacturaFuncionarioDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionarioDesc = creandoJsonDescFuncionario(cabFacturaFuncionario);
        finalizo = registrarDescFuncionario(cabFacturaFuncionarioDesc.toString());
        datos.put("facturaFuncionario", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO FUNCIONARIO -> POST

    ////CREATE, DESCUENTO TARJETA -> POST
    private boolean creandoCabFacturaTarjetaDesc(JSONObject cabFacturaTarj) {
        JSONObject cabFacturaTarjetaDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaTarjetaDesc = creandoJsonDescTarj(cabFacturaTarj);
        finalizo = registrarDescTarjetaLocal(cabFacturaTarjetaDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO TARJETA -> POST

    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjetaConvDesc(JSONObject cabFactura) {
        JSONObject cabFacturaTarjetaConvDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaTarjetaConvDesc = creandoJsonDescTarjConv(cabFactura);
        finalizo = registrarConvenioLocal(cabFacturaTarjetaConvDesc.toString());
        datos.put("facturaTarjetaConv", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST
    //BBDD ****************************** -> -> -> -> -> -> -> -> -> -> -> -> ->

    //DESARROLLO - PENDIENTE *********************** -> -> -> -> -> -> -> -> -> 
    //////INSERT || UPDATE, FACTURA CAB.
    private JSONObject registrarFacturaCabeceraLocal(String json) {
        JSONObject estados = new JSONObject();
        JSONParser p = new JSONParser();
        String sql = "";
        String operacion = "insertar";
        boolean actualizacion = false;
        JSONObject objSon = new JSONObject();
        org.json.JSONObject jsonObj = new org.json.JSONObject(datos);
        long ids = 0L;
        if (!jsonObj.isNull("idFactClienteCabServidor")) {
            ids = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
        }
        long idFact = ids;
        String xuuids = "";
        if (idFact == 0L) {
            if (FacturaDeVentaMayFXMLController.isCancelacionProd()) {
                actualizacion = true;
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    xuuids = datos.get("uuidCassandraActual").toString();
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + xuuids + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
            } else {
                try {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    //ACTUALIZAR TALONARIO SUCURSALES
                    JSONObject sucursal = (JSONObject) p.parse(datos.get("talonarioSucursal").toString());
                    long idTalonario = Long.parseLong(sucursal.get("idTalonariosSucursales").toString());
                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
                    tal.setNroActual(tal.getNroActual() + 1);
                    taloDAO.actualizarNroActual(tal);
                    //ACTUALIZACIONES EN POSTGRESQL LOCALMENTE
                    //UUID ACTUAL PARA MODIFICAR FACTURA
                    datos.put("uuidCassandraActual", uuid);
                    objSon = (JSONObject) p.parse(json);
                    FacturaDeVentaMayFXMLController.setCabFactura(objSon);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + objSon.toString() + "', 'insertar');";
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.info("ERROR sql -> " + sql + "\n");
                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                }
            }
        } else {
            try {
                actualizacion = true;
                JSONObject jsonFactura = (JSONObject) p.parse(json);
                long idFacServer = 0L;
                if (!jsonObj.isNull("idFactClienteCabServidor")) {
                    idFacServer = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
                }
                jsonFactura.put("idFacturaClienteCab", idFacServer);
                String uuids = "";
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    uuids = datos.get("uuidCassandraActual").toString();
                }
                if (uuids.equals("")) {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + jsonFactura.toString() + "', 'actualizar');";
                } else {
                    long uuid = VentasUtiles.recuperarId();
//                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuids + ";";
                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuid + ";";
                }
            } catch (ParseException ex) {
                Utilidades.log.info("ERROR json\n -> " + json + "\n");
                Utilidades.log.info("ERROR sql -> " + sql + "\n");
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.conectar();
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                if (actualizacion) {
                    estados = (JSONObject) p.parse(json);
                } else {
                    estados = objSon;
                }
                if (estados.get("cliente") == null) {
                    ClienteDAO cliDAO = new ClienteDAOImpl();
                    ClienteDTO clienteDTOs = new ClienteDTO();
                    ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                    clienteDTOs.setIdCliente(161168L);
                    clienteDTOs.setNombre(cliDTO.getNombre());
                    if (cliDTO.getApellido() != null) {
                        clienteDTOs.setApellido(cliDTO.getApellido());
                    }
                    clienteDTOs.setRuc(cliDTO.getRuc());
                    JSONObject clienteJson = (JSONObject) p.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR objSon\n -> " + objSon + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.error("SQLException | ParseException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        if (estados.isEmpty()) {
            insertarCabeceraEliminada(json, xuuids);//NUEVO
            org.json.JSONObject jsonFact = new org.json.JSONObject(json);
            String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
            setearCabecera(idFactCab);
            if (actualizacion) {
                try {
                    estados = (JSONObject) p.parse(json);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            } else {
                estados = objSon;
            }
            if (estados.get("cliente") == null) {
                ClienteDAO cliDAO = new ClienteDAOImpl();
                ClienteDTO clienteDTOs = new ClienteDTO();
                ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                clienteDTOs.setIdCliente(161168L);
                clienteDTOs.setNombre(cliDTO.getNombre());
                if (cliDTO.getApellido() != null) {
                    clienteDTOs.setApellido(cliDTO.getApellido());
                }
                clienteDTOs.setRuc(cliDTO.getRuc());
                JSONObject clienteJson;
                try {
                    clienteJson = (JSONObject) p.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR clienteDTOs\n -> " + clienteDTOs + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            }
        } else {
            if (selectCancelProdAux() != 0) {
                org.json.JSONObject jsonFact = new org.json.JSONObject(json);
                String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
                setearCabecera(idFactCab);
            }
        }
        return estados;
    }

    private void insertarCabeceraEliminada(String json, String uuids) {
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuids + ",'" + json + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION PRODUCTO ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.info("ERROR uuids -> " + uuids + "\n");
            Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
    }

    private void setearCabecera(String idFactCab) {
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                actualizarDatosCabecera(rs.getString("dato"), idFactCab, rs.getLong("id_dato"));
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
    }

    private void actualizarDatosCabecera(String sql, String idFactCab, long idDato) {
        String replace1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'";
        String replace2 = "','cancelacionProducto', 'insertar')";
        String rep = ",\"cancelacionProducto\", \"insertar\")";
        JSONParser parser = new JSONParser();
        sql = sql.replace(replace1, "");
        sql = sql.replace(replace2, "");
        try {
            JSONObject cancelProd = (JSONObject) parser.parse(sql);
            cancelProd.remove("facturaClienteCab");
            JSONObject factCab = new JSONObject();
            factCab.put("idFacturaClienteCab", idFactCab);
            cancelProd.put("facturaClienteCab", factCab);
            String sqlQUERY = "UPDATE desarrollo.auxiliar_cancel_prod SET dato=\'" + replace1.substring(0, replace1.length() - 1) + "\"" + cancelProd + "\"" + rep + "\' WHERE id_dato=" + idDato;
            ConexionPostgres.conectar();
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sqlQUERY)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    Utilidades.log.info("******* ACTUALIZADO CANCELACION_PRODUCTO_AUXILIAR ********");
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                }
            }
        } catch (ParseException ex) {
            Utilidades.log.error(sql + "\n" + idFactCab + "\n" + idDato, ex);
        }
    }

    private int selectCancelProdAux() {
        int cancel = 0;
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";

        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cancel += 1;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
        return cancel;
    }
    //////INSERT || UPDATE, FACTURA CAB.

    //////INSERT, FACTURA DET.
    private boolean registrarArrayDetalleLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, FACTURA DET.

    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA
    private boolean registrarFacturaMonedaExtranjera(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabMonedaExtranjera', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA

    //////INSERT, FACTURA CAB. CHEQUE
    private boolean realizarFactChequeLocal(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabCheque', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. CHEQUE

    //////INSERT, FACTURA CAB. TARJETA
    private JSONArray registrarCabTarjetaLocal(JSONArray json) {
        JSONArray estados = new JSONArray();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONArray) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. TARJETA

    //////INSERT, FACTURA CAB. TARJETA CONVENIO
    private JSONObject registrarTarjetaConvenioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. TARJETA CONVENIO

    //////INSERT, FACTURA CAB. CLIENTE FIEL
    private JSONObject registrarTarjetaFielLocal(JSONObject json) {
        ConexionPostgres.conectar();
        JSONObject obj = new JSONObject();
        JSONParser parser = new JSONParser();
        String sql = "INSERT INTO desarrollo.cabecera_fiel(fecha, descripcion_dato, operacion) VALUES (now(),'" + json.toString() + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                obj = (JSONObject) parser.parse(json.toString());
            } else {
                obj = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return obj;
    }
    //////INSERT, FACTURA CAB. CLIENTE FIEL

    //////INSERT, FACTURA CAB. FUNCIONARIO
    private JSONObject registrarCabFuncionarioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabFuncionario', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. FUNCIONARIO

    //////INSERT, PENDIENTES - VALE
    private boolean registrarValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - VALE

    //////INSERT, PENDIENTES - PORC. VALE
    private boolean registrarPorcValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPorcVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - PORC. VALE

    //////INSERT, FACTURA CAB. NOTA CRÉDITO
    private boolean registrarNotaCreditoLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(), '" + json + "','facturaClienteCabNotaCredito', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        return estados;
    }
    //////INSERT, FACTURA CAB. NOTA CRÉDITO

    //////INSERT, FACTURA CAB. RETENCIÓN
    private boolean registrarRetencion(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabRetencion', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. RETENCIÓN

    //////INSERT, PENDIENTES - DESCUENTO VALE
    private boolean registrarDescValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO VALE

    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP. ART.
    private JSONObject registrarPromoTempArt(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPromoTempArt', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json);
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP. ART.

    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.
    private JSONObject registrarPromoTemp(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPromoTemp', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json);
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.

    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO
    private boolean registrarDescDirectivo(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descuentoDirectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO

    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL
    private boolean registrarDescTarjFielLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaFiel', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL

    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO
    private boolean registrarDescFuncionario(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descFuncionario', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA
    private boolean registrarDescTarjetaLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO
    private boolean registrarConvenioLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO

    //////INSERT, FACTURA CAB. EFECTIVO
    private Map registrarFacturaEfectivoLocal(JSONObject json) {
        Map mapeo = new HashMap();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabEfectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                mapeo.put("finalizo", true);
                mapeo.put("cabFacturaEfectivo", json);
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return mapeo;
    }
    //////INSERT, FACTURA CAB. EFECTIVO
    //DESARROLLO - PENDIENTE *********************** -> -> -> -> -> -> -> -> -> 

    //////INSERT, FACTURA CAB. GIFTCARD
    private Map registrarFacturaGiftcardLocal(JSONObject json) {
        Map mapeo = new HashMap();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabGiftcard', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                mapeo.put("finalizo", true);
                mapeo.put("cabFacturaGiftcard", json);
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return mapeo;
    }
    //////INSERT, FACTURA CAB. GIFTCARD

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON FACTURA CABECERA
    private void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            FacturaDeVentaMayFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            FacturaDeVentaMayFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
            FacturaDeVentaMayFXMLController.getCabFactura().put("montoFactura", monto);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //JSON FACTURA CABECERA

    //JSON FACTURA DETALLE
    private JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < FacturaDeVentaMayFXMLController.getDetalleArtList().size(); i++) {
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            if (!hashDetallePrecioDesc.isEmpty()) {//algún descuento por sección, fiel; funcionario; temporada...
            } else if (descTarjeta || descTarjetaConvenio || descDirectivo) {//vale por monto y vale porcentaje no entran...
            }
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            FacturaDeVentaMayFXMLController.getDetalleArtList().get(i).put("facturaClienteCab", factCab);
            if (FacturaDeVentaMayFXMLController.getDetalleArtList().get(i).containsKey("peso")) {
                FacturaDeVentaMayFXMLController.getDetalleArtList().get(i).remove("peso");
            }
            jsonArrayFactDet.add(FacturaDeVentaMayFXMLController.getDetalleArtList().get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }
    //JSON FACTURA DETALLE

    //JSON TIPO PAGO EFECTIVO
    private JSONObject creandoJsonEfectivo(JSONObject factCab, int monto) {
        JSONObject jsonEfectivo = new JSONObject();
        //**********************************************************************
        jsonEfectivo.put("facturaClienteCab", factCab);
        jsonEfectivo.put("montoEfectivo", monto);
        //**********************************************************************
        return jsonEfectivo;
    }
    //JSON TIPO PAGO EFECTIVO

    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA
    private JSONArray creandoJsonMonedaEx(JSONObject factCab, JSONObject jSONObject) {
        JSONArray jsonArrayMonedaExt = new JSONArray();
        JSONObject jsonDolar = new JSONObject();
        JSONObject jsonReal = new JSONObject();
        JSONObject jsonPeso = new JSONObject();
        //**********************************************************************
        //List tipo_moneda ordenado por descripción, getCotizacionList
        if (jSONObject.containsKey("textFieldDolarGs") && jSONObject.containsKey("textFieldDolarCant")) {
            jsonDolar.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldDolarGs").toString()));
            int montoDolar = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldDolarCant").toString()));
            jsonDolar.put("montoEfectivo", montoDolar);
            jsonDolar.put("monto_guaranies", montoAGs);
            if (cotizacionList != null) {
                int cotiz = Double.valueOf(String.valueOf(cotizacionList.get(0).get("venta"))).intValue();
                jsonDolar.put("cotizacion", cotiz);
                jsonDolar.put("tipoMoneda", cotizacionList.get(0));
                jsonArrayMonedaExt.add(jsonDolar);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVenta.setDolar(jSONObject.get("textFieldDolarCant").toString() + " x " + cot + " = " + jSONObject.get("textFieldDolarGs").toString() + "\n");
                //para impresión
            }
        }
        if (jSONObject.containsKey("textFieldRealGs") && jSONObject.containsKey("textFieldRealCant")) {
            jsonReal.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldRealGs").toString()));
            int montoReal = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldRealCant").toString()));
            jsonReal.put("montoEfectivo", montoReal);
            jsonReal.put("monto_guaranies", montoAGs);
            if (cotizacionList != null) {
                int cotiz = Double.valueOf(String.valueOf(cotizacionList.get(3).get("venta"))).intValue();
                jsonReal.put("cotizacion", cotiz);
                jsonReal.put("tipoMoneda", cotizacionList.get(3));
                jsonArrayMonedaExt.add(jsonReal);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVenta.setReal(jSONObject.get("textFieldRealCant").toString() + " x " + cot + " = " + jSONObject.get("textFieldRealGs").toString() + "\n");
                //para impresión
            }
        }
        if (jSONObject.containsKey("textFieldPesoGs") && jSONObject.containsKey("textFieldPesoCant")) {
            jsonPeso.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldPesoGs").toString()));
            int montoPeso = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldPesoCant").toString()));
            jsonPeso.put("montoEfectivo", montoPeso);
            jsonPeso.put("monto_guaranies", montoAGs);
            if (cotizacionList != null) {
                int cotiz = Double.valueOf(String.valueOf(cotizacionList.get(2).get("venta"))).intValue();
                jsonPeso.put("cotizacion", cotiz);
                jsonPeso.put("tipoMoneda", cotizacionList.get(2));
                jsonArrayMonedaExt.add(jsonPeso);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVenta.setPeso(jSONObject.get("textFieldPesoCant").toString() + " x " + cot + " = " + jSONObject.get("textFieldPesoGs").toString() + "\n");
                //para impresión
            }
        }
        //**********************************************************************
        return jsonArrayMonedaExt;
    }
    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA

    //JSON TIPO PAGO CHEQUE
    private JSONArray creandoJsonCheque(JSONObject factCab, JSONObject jsonObject) {
        JSONArray jsonArrayCheque = new JSONArray();
        //**********************************************************************
        JSONObject jsonCheque = new JSONObject();
        jsonCheque.put("facturaClienteCabDTO", factCab);
        jsonCheque.put("descripcion", jsonObject.get("textFieldEntidad").toString());
        jsonCheque.put("nroCheque", jsonObject.get("textFIeldNroCheque").toString());
        int monto = Integer.valueOf(jsonObject.get("textFieldMonto").toString());
        jsonCheque.put("montoCheque", monto);
        //para impresión
        montoChequeTotal = montoChequeTotal + monto;
        //para impresión
        jsonArrayCheque.add(jsonCheque);
        //para impresión
        MensajeFinalVenta.setCheque(montoChequeTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayCheque;
    }
    //JSON TIPO PAGO CHEQUE

    //JSON TIPO PAGO TARJETA CONVENIO
    private JSONObject creandoJsonTarjetaConvenio(JSONObject factCab) {
        JSONObject jsonTarjConv = new JSONObject();
        //**********************************************************************
        jsonTarjConv.put("facturaClienteCab", factCab);
        jsonTarjConv.put("tarjetaConvenio", hashJsonComboTarjConvenio.get(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem()));
        jsonTarjConv.put("descripcionTarj", jfxComboBoxTipoDto.getSelectionModel().getSelectedItem());
        //todas formas de pago...
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        //todas formas de pago...
        jsonTarjConv.put("monto", monto);
        //**********************************************************************
        return jsonTarjConv;
    }
    //JSON TIPO PAGO TARJETA CONVENIO

    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO
    private JSONArray creandoJsonTarjeta(JSONObject factCab, JSONObject jsonObject) {
        JSONArray jsonArrayTarjeta = new JSONArray();
        //**********************************************************************
        JSONObject jsonTarjeta = new JSONObject();
        //debe tener descuento
        if (jsonObject.get("comboBoxTarjetas").toString().contains(" || ")) {
            String[] partUno = jsonObject.get("comboBoxTarjetas").toString().split(" \\|\\| ");
            JSONObject tarj = hashJsonComboTarjeta.get(jsonObject.get("comboBoxTarjetas").toString());
            JSONArray jsonArrayTarjetas = (JSONArray) tarj.get("tarjeta");
            JSONObject jsonTarjetaDto = (JSONObject) jsonArrayTarjetas.get(0);
            JSONObject tarjTipo = (JSONObject) jsonTarjetaDto.get("tipoTarjeta");
            jsonTarjeta.put("facturaClienteCab", factCab);
            jsonTarjeta.put("tarjeta", jsonTarjetaDto);
            jsonTarjeta.put("descripcionTarj", partUno[1]);
            jsonTarjeta.put("codAutorizacion", jsonObject.get("textFieldCodAuth").toString());
            int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoTarjeta").toString()));
            jsonTarjeta.put("monto", monto);
            if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                montoTarjCredTotal = montoTarjCredTotal + monto;
                MensajeFinalVenta.setTarjCred(montoTarjCredTotal);
            } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                montoTarjDebTotal = montoTarjDebTotal + monto;
                MensajeFinalVenta.setTarjDeb(monto);
            }
        } else {
            JSONObject tarj = hashJsonComboTarjeta.get(jsonObject.get("comboBoxTarjetas").toString());
            JSONObject tarjTipo = (JSONObject) tarj.get("tipoTarjeta");
            jsonTarjeta.put("facturaClienteCab", factCab);
            jsonTarjeta.put("tarjeta", tarj);
            jsonTarjeta.put("descripcionTarj", jsonObject.get("comboBoxTarjetas").toString());
            jsonTarjeta.put("codAutorizacion", jsonObject.get("textFieldCodAuth").toString());
            int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoTarjeta").toString()));
            jsonTarjeta.put("monto", monto);
            if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                montoTarjCredTotal = montoTarjCredTotal + monto;
                MensajeFinalVenta.setTarjCred(montoTarjCredTotal);
            } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                montoTarjDebTotal = montoTarjDebTotal + monto;
                MensajeFinalVenta.setTarjDeb(monto);
            }
        }
        jsonArrayTarjeta.add(jsonTarjeta);
        //para impresión
        MensajeFinalVenta.setTarjCred(montoTarjCredTotal);
        MensajeFinalVenta.setTarjDeb(montoTarjDebTotal);
        //para impresión        
        //**********************************************************************
        return jsonArrayTarjeta;
    }
    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO

    //JSON TIPO PAGO CLIENTE FIEL
    private JSONObject creandoJsonTarjClienteFiel(JSONObject factCab) {
        JSONObject jsonObjectClienteFiel = new JSONObject();
        //**********************************************************************
        JSONObject factura = new JSONObject();
        factura.put("idFacturaClienteCab", Long.parseLong(factCab.get("idFacturaClienteCab").toString()));
        jsonObjectClienteFiel.put("facturaClienteCab", factura);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonObjectClienteFiel.put("monto", monto);
        JSONObject jsonClienteF = BuscarClienteFXMLController.getJsonClienteFiel();
        jsonClienteF.remove("fechaAlta");
        jsonClienteF.remove("fechaMod");
        jsonObjectClienteFiel.put("tarjetaClienteFiel", jsonClienteF);
        //**********************************************************************
        return jsonObjectClienteFiel;
    }
    //JSON TIPO PAGO CLIENTE FIEL

    //JSON TIPO PAGO FUNCIONARIO
    private JSONObject creandoJsonFuncionario(JSONObject factCab) {
        JSONObject jsonObjectFuncionario = new JSONObject();
        //**********************************************************************
        jsonObjectFuncionario.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonObjectFuncionario.put("monto", monto);
        jsonObjectFuncionario.put("funcionario", jsonFuncionario);
        //**********************************************************************
        return jsonObjectFuncionario;
    }
    //JSON TIPO PAGO FUNCIONARIO

    //JSON TIPO PAGO VALE MONTO
    private JSONObject creandoJsonValeMonto(JSONObject factCab, JSONObject jSONObject) {
        JSONObject jsonValeMonto = new JSONObject();
        //**********************************************************************
        jsonValeMonto.put("facturaClienteCab", factCab);
        int monto = 0;
        switch (jSONObject.get("formaPago").toString()) {
            case "VALE":
                jsonValeMonto.put("vale", hashJsonComboVale.get(jSONObject.get("comboBoxVale").toString()));
                monto = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoVale").toString()));
                break;
            case "VALE INTERNO (FUNCIONARIO)":
                JSONObject jsonValeFuncionario = new JSONObject();
                jsonValeFuncionario.put("idVale", 1l);
                jsonValeFuncionario.put("descripcionVale", "FUNCIONARIO");
                jsonValeMonto.put("vale", jsonValeFuncionario);
                monto = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoValeInterno").toString()));
                break;
            case "ASOCIACIÓN":
                JSONObject jsonValeAsoc = new JSONObject();
                jsonValeAsoc.put("idVale", 4l);
                jsonValeAsoc.put("descripcionVale", "ASOCIACION");
                jsonValeMonto.put("vale", jsonValeAsoc);
                monto = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoAsoc").toString()));
                break;
            default:
                break;
        }
        jsonValeMonto.put("monto", monto);
        //para impresión
        if (jSONObject.get("formaPago").toString().contentEquals("ASOCIACIÓN")) {
            MensajeFinalVenta.setAsoc(monto);
        } else {
            MensajeFinalVenta.setVale(monto);
        }
        //para impresión
        /*Integer nroVale = null;
        if (!textFieldNroVale.getText().contentEquals("")) {
            nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        }
        jsonValeMonto.put("nroVale", nroVale);*/
        if (jSONObject.containsKey("textFieldNroVale")) {
            jsonValeMonto.put("nroVale", jSONObject.get("textFieldNroVale").toString());
        } else {
            jsonValeMonto.put("nroVale", "");
        }
        if (jSONObject.containsKey("textFieldValeCI")) {
            jsonValeMonto.put("ci", jSONObject.get("textFieldValeCI").toString());
        } else {
            jsonValeMonto.put("ci", "");
        }
        //**********************************************************************
        return jsonValeMonto;
    }
    //JSON TIPO PAGO VALE MONTO

    //JSON TIPO PAGO VALE PORCENTAJE
    private JSONObject creandoJsonValePorc(JSONObject factCab, JSONObject jSONObject) {
        JSONObject jsonValePorc = new JSONObject();
        //**********************************************************************
        jsonValePorc.put("facturaClienteCab", factCab);
        jsonValePorc.put("vale", jSONObject);
        int porcVale = Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoValePorc").toString()));
        jsonValePorc.put("porcVale", porcVale);
        Long precioTotal = FacturaDeVentaMayFXMLController.getPrecioTotal();
        Long descTotal = (precioTotal * porcVale) / 100;
        jsonValePorc.put("monto", descTotal);
        MensajeFinalVenta.setVale(toIntExact((long) descTotal));
//        int nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        jsonValePorc.put("nroVale", jSONObject.get("textFieldMontoVale").toString());
        jsonValePorc.put("ci", jSONObject.get("textFieldValeCI").toString());
        //**********************************************************************
        return jsonValePorc;
    }
    //JSON TIPO PAGO VALE PORCENTAJE

    private JSONObject creandoJsonGiftcard(JSONObject factCab, Integer monto, long cod) {
        JSONObject jsonGiftcard = new JSONObject();
        //**********************************************************************
        jsonGiftcard.put("facturaClienteCab", factCab);
        jsonGiftcard.put("monto", monto);
        jsonGiftcard.put("codigo", cod);
        //**********************************************************************
        return jsonGiftcard;
    }

    //JSON TIPO PAGO NOTA DE CRÉDITO
    private JSONArray creandoJsonNotaCred(JSONObject factCab, JSONObject jsonObject) {
        JSONArray jsonArrayNota = new JSONArray();
        //**********************************************************************
        JSONObject jsonNota = new JSONObject();
        jsonNota.put("facturaClienteCabDTO", factCab);
        jsonNota.put("nroNota", jsonObject.get("textFieldNroNotaCred").toString());
        int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoNotaCred").toString()));
        jsonNota.put("monto", monto);
        //para impresión
        montoNotaTotal += monto;
        //para impresión
        jsonArrayNota.add(jsonNota);
        //para impresión
        MensajeFinalVenta.setNotaCred(montoNotaTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayNota;

    }
    //JSON TIPO PAGO NOTA DE CRÉDITO

    //JSON TIPO PAGO MONTO RETENCIÓN
    private JSONObject creandoJsonMontoRetencion(JSONObject factCab) {
        JSONObject jsonMontoRetencion = new JSONObject();
        //**********************************************************************
        jsonMontoRetencion.put("facturaClienteCab", factCab);
//        int monto = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
//        jsonMontoRetencion.put("monto", monto);
        //**********************************************************************
        return jsonMontoRetencion;
    }
    //JSON TIPO PAGO MONTO RETENCIÓN

    //JSON DESCUENTO VALE
    private JSONObject creandoJsonDescVale(JSONObject facturaClienteCab, JSONObject vale) {
        JSONObject jsonDescVale = new JSONObject();
        //**********************************************************************
        jsonDescVale.put("facturaClienteCab", facturaClienteCab);
        jsonDescVale.put("vale", vale);
        int monto = 0;
        if (descValePorc) {
            monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        } else /*if (Long.valueOf(vale.get("idVale").toString()) == 5)*/ {
            monto = toIntExact(Math.round(descuentoValeParesaSum));
        }
        jsonDescVale.put("montoDesc", monto);
        //**********************************************************************
        return jsonDescVale;
    }
    //JSON DESCUENTO VALE

    //JSON DESCUENTO PROMOCIÓN TEMPORADA ART.
    private JSONObject creandoJsonDescPromoTempArt(JSONObject facturaClienteCab, JSONObject promoTemporadaArtDesc, int monto) {
        JSONObject jsonDescPromo = new JSONObject();
        //**********************************************************************
        jsonDescPromo.put("facturaClienteCab", facturaClienteCab);
        jsonDescPromo.put("promoTemporadaArt", promoTemporadaArtDesc);
        jsonDescPromo.put("descripcionTemporada", promoTemporadaArtDesc.get("descripcionTemporadaArt"));
        jsonDescPromo.put("monto", monto);
        //**********************************************************************
        return jsonDescPromo;
    }
    //JSON DESCUENTO PROMOCIÓN TEMPORADA ART.

    //JSON DESCUENTO PROMOCIÓN TEMPORADA
    private JSONObject creandoJsonDescPromoTemp(JSONObject facturaClienteCab, JSONObject promoTemporadaDesc, int monto) {
        JSONObject jsonDescPromo = new JSONObject();
        //**********************************************************************
        jsonDescPromo.put("facturaClienteCab", facturaClienteCab);
        jsonDescPromo.put("promoTemporada", promoTemporadaDesc);
        jsonDescPromo.put("descripcionTemporada", promoTemporadaDesc.get("descripcionTemporada"));
        jsonDescPromo.put("monto", monto);
        //**********************************************************************
        return jsonDescPromo;
    }
    //JSON DESCUENTO PROMOCIÓN TEMPORADA

    //JSON DESCUENTO DIRECTIVO
    private JSONObject creandoJsonDescDirectivo(JSONObject facturaClienteCab) {
        JSONObject jsonDescDirectivo = new JSONObject();
        //**********************************************************************
        jsonDescDirectivo.put("facturaClienteCab", facturaClienteCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescDirectivo.put("montoDesc", monto);
        long porcDirectivo = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescDirectivo.put("porcDesc", porcDirectivo);
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        jsonDescDirectivo.put("ciDir", "");
        jsonDescDirectivo.put("nombreDir", "");
        jsonDescDirectivo.put("motivoDesc", "");
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        //**********************************************************************
        return jsonDescDirectivo;
    }
    //JSON DESCUENTO DIRECTIVO

    //JSON DESCUENTO TARJETA CLIENTE FIEL
    private JSONObject creandoJsonDescTarjFiel(JSONObject factCabTarjFiel) {
        JSONObject jsonDescTarjFiel = new JSONObject();
        //**********************************************************************
        jsonDescTarjFiel.put("facturaClienteCabTarjFiel", factCabTarjFiel);
        int montoDesc = toIntExact(Math.round(descuentoFielSum)) + toIntExact(Math.round(descuentoGift));
        jsonDescTarjFiel.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjFiel.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjFiel;
    }
    //JSON DESCUENTO TARJETA CLIENTE FIEL

    //JSON DESCUENTO TARJETA FUNCIONARIO
    private JSONObject creandoJsonDescFuncionario(JSONObject factCabFuncionario) {
        JSONObject jsonDescFuncionario = new JSONObject();
        //**********************************************************************
        jsonDescFuncionario.put("facturaClienteCabFuncionarioDTO", factCabFuncionario);
        int montoDesc = toIntExact(Math.round(descuentoFuncSum));
        jsonDescFuncionario.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        long redondeo = Math.round(porcentajeDesc * 100.0);
        porcentajeDesc = (double) (redondeo / 100.0);
        jsonDescFuncionario.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescFuncionario;
    }
    //JSON DESCUENTO TARJETA FUNCIONARIO

    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO
    private JSONObject creandoJsonDescTarj(JSONObject factCabTarj) {//solo UNA TARJETA...
        JSONObject jsonDescTarj = new JSONObject();
        //**********************************************************************
        jsonDescTarj.put("facturaClienteCabTarjeta", factCabTarj);
        String descTarj = "";
        for (JSONObject jsonObject : listJsonFormaPago) {
            if (jsonObject.get("formaPago").toString().contentEquals("TARJETAS")) {
                descTarj = jsonObject.get("comboBoxTarjetas").toString();
                break;
            }
        }
        JSONObject jsonDescuentoTarjetaCab = new JSONObject();
        jsonDescuentoTarjetaCab.put("idDescuentoTarjetaCab",
                Long.valueOf(Descuento.getHashDescTarjeta().get(descTarj).get("idDescuentoTarjetaCab").toString()));
        jsonDescTarj.put("descuentoTarjetaCab", jsonDescuentoTarjetaCab);
        int montoDesc = toIntExact(Math.round(descuentoTarjetaSum));
        jsonDescTarj.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100.0) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarj.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarj;
    }
    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO

    //JSON DESCUENTO TARJETA CONVENIO
    private JSONObject creandoJsonDescTarjConv(JSONObject factCabTarjConv) {
        JSONObject jsonDescTarjConv = new JSONObject();
        //**********************************************************************
        jsonDescTarjConv.put("facturaClienteCabTarjConvenio", factCabTarjConv);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescTarjConv.put("montoDesc", monto);
        int porcentajeDesc = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjConv.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjConv;
    }
    //JSON DESCUENTO TARJETA CONVENIO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    private void gestionandoPago(JSONObject cabFactura) {
        //manejo especial
        if (jfxComboBoxTipoDto.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            if (creandoCabFacturaFuncionario(cabFactura)) {
            }
        } else if (BuscarClienteFXMLController.getJsonClienteFiel() != null) {
            if (creandoCabFacturaClienteFiel(cabFactura)) {
            }
        } else if (hashJsonComboTarjConvenio.containsKey(jfxComboBoxTipoDto.getSelectionModel().getSelectedItem())) {
            if (creandoCabFacturaTarjConv(cabFactura)) {
            }
        }
        //sum, para más de uno
        montoTarjCredTotal = 0;
        montoTarjDebTotal = 0;
        montoNotaTotal = 0;
        montoChequeTotal = 0;
        //sum, para más de uno
        //manejo especial
        for (JSONObject jSONObject : listJsonFormaPago) {
            switch (jSONObject.get("formaPago").toString()) {
                case "EFECTIVO":
                    if (creandoCabFacturaEfectivo(cabFactura, jSONObject)) {
                    }
                    //para impresión
                    MensajeFinalVenta.setEfectivo(Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldEfectivo").toString())));
                    //para impresión
                    break;
                case "MONEDA EXTRANJERA":
                    JSONArray jsonArrayMonedaExt = creandoJsonMonedaEx(cabFactura, jSONObject);
                    if (creandoCabFacturaMonedaExt(jsonArrayMonedaExt)) {
                    }
                    break;
                case "CHEQUE":
                    JSONArray jsonArrayCheque = creandoJsonCheque(cabFactura, jSONObject);
                    if (creandoCabFacturaCheque(jsonArrayCheque)) {
                    }
                    break;
                case "TARJETAS":
                    JSONArray jsonArrayTarjeta = creandoJsonTarjeta(cabFactura, jSONObject);
                    if (creandoCabFacturaTarjeta(jsonArrayTarjeta)) {
                    }
                    break;
                case "NOTA DE CRÉDITO":
                    JSONArray jsonArrayNota = creandoJsonNotaCred(cabFactura, jSONObject);
                    if (creandoCabFacturaNotaCred(jsonArrayNota)) {
                    }
                    break;
                case "VALE":
                    creandoCabFacturaValeMonto(cabFactura, jSONObject);
                    break;
                case "VALE INTERNO (FUNCIONARIO)":
                    creandoCabFacturaValeMonto(cabFactura, jSONObject);
                    break;
                case "ASOCIACIÓN":
                    creandoCabFacturaValeMonto(cabFactura, jSONObject);
                    break;
                case "GIFTCARD":
                    creandoCabFacturaGiftcard(cabFactura, jSONObject);
                    break;
            }
        }
//        if (hmFormaPago.containsKey("NOTA DE CRÉDITO")) {

        //para impresión
//            MensajeFinalVenta.setNotaCred(Integer.valueOf(numValidator.numberValidator(textFieldMontoNotaCred.getText())));
        //para impresión
//        }
        /*if (!textFieldMontoRetencion.getText().contentEquals("")) {
            if (creandoCabFacturaMontoRetencion(cabFactura)) {
            }
            //para impresión
            MensajeFinalVenta.setRetencion(Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText())));
            //para impresión
        }*/
        gestionandoDescuento(cabFactura, true);
    }

    private void gestionandoDescuento(JSONObject json, boolean sinCabeceraPago) {
        if (sinCabeceraPago) {
            if (descValeMonto || descValePorc) {
                if (hmFormaPago.containsKey("VALE") /*|| hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)")
                        || hmFormaPago.containsKey("ASOCIACIÓN")*/) {
                    JSONObject jsonValeParHM = hmFormaPago.get("VALE");
                    if (jsonValeParHM.get("comboBoxVale").toString().contentEquals("PARESA")) {
                        JSONObject jsonValePar = new JSONObject();
                        jsonValePar.put("idVale", 6l);
                        jsonValePar.put("descripcionVale", "PARESA");
                        creandoCabFacturaValeDesc(json, jsonValePar);
                    }
                }
            }
            if (descPromoTemp) {
                for (Object jsonObjectPromArt : Descuento.getPromTempArtJSONArray()) {
                    //verificación de promoción art. con su respectivo descuento... (cabecera)
                    JSONObject jsonPromoTempArt = (JSONObject) jsonObjectPromArt;
                    if (hashJsonPromocionArtDesc.get(jsonPromoTempArt) != null) {
                        long descEstaPromoArt = hashJsonPromocionArtDesc.get(jsonPromoTempArt);
                        int descPromoArt = toIntExact((long) descEstaPromoArt);
                        JSONObject cabFacturaPromTempArtDesc = creandoJsonDescPromoTempArt(json, jsonPromoTempArt, descPromoArt);
                        creandoCabFacturaPromTempArtDesc(cabFacturaPromTempArtDesc);
                    }
                }
                for (Object jsonObjectProm : Descuento.getPromTempJSONArray()) {//verificación de promoción con su respectivo descuento...
                    JSONObject jsonPromoTemp = (JSONObject) jsonObjectProm;
                    if (hashJsonPromocionDesc.get(jsonPromoTemp) != null) {
                        long descEstaPromo = hashJsonPromocionDesc.get(jsonPromoTemp);
                        int descPromo = toIntExact((long) descEstaPromo);
                        JSONObject cabFacturaPromTempDesc = creandoJsonDescPromoTemp(json, jsonPromoTemp, descPromo);
                        creandoCabFacturaPromTempDesc(cabFacturaPromTempDesc);
                    }
                }
            }
            if (descDirectivo) {
                creandoCabFacturaDirectivoDesc(json);
            }
        } else {
            if ((descuentoGift > 0 || descTarjetaFiel) && json.containsKey("tarjetaClienteFiel")) {
                creandoCabFacturaClienteFielDesc(json);
            }
            if (descFuncionario && json.containsKey("funcionario")) {
                creandoCabFacturaFuncionarioDesc(json);
            }
            if (descTarjeta && json.containsKey("tarjeta") && descuentoTarjetaSum != 0.0) {
                creandoCabFacturaTarjetaDesc(json);
            }
            if (descTarjetaConvenio && json.containsKey("tarjetaConvenio")) {
                creandoCabFacturaTarjetaConvDesc(json);
            }
        }
    }

    //FINAL FINAL FINAL ******************** -> -> -> -> -> -> -> -> -> -> -> ->
    private void finalizandoVenta() {
        if (!enterBlock) {
            enterBlock = true;
            JSONParser p = new JSONParser();
            org.json.JSONObject json = new org.json.JSONObject(datos);
            if (hmFormaPago.isEmpty()) {
                mensajeAdv("DEBE INGRESAR UN MONTO, EFECTIVO; CHEQUE; VALE; TARJETAS; Y/O NOTA DE CRÉDITO.");
            } else if (totalAbonado < totalAPagar) {
                mensajeAdv("EL MONTO A ABONAR NO ES SUFICIENTE.");
            } else {
//                if ("factura_venta_procesar")) {
                //validación en caso de ser Vale...
                datos.put("energiaElectrica", false);
                actualizarDatos();
                try {
                    actualizandoCabFactura();
                    long dato = CancelacionProductoMayFXMLController.recuperarIdDato();
                    if (dato != 0l) {
                        datos.put("idDato", dato);
                    }
                    actualizarDatos();
                    seteandoCamposMsjFinal();
                    seteandoDescuentos();
                    seteandoTarjetas();
                    seteandoCheques();
                    seteandoNotaCred();
                    int valor = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
                    int sumEfe = 0;
                    if (!json.isNull("sumEfeRecibido")) {
                        sumEfe = Integer.parseInt(datos.get("sumEfeRecibido").toString());
                    }
                    int sum = sumEfe;
                    datos.put("sumEfeRecibido", (sum + valor));
                    if (hmFormaPago.containsKey("EFECTIVO")) {
                        seteandoEfectivos();
                    }
                    if (hmFormaPago.containsKey("GIFTCARD")) {
                        long monto = 0l;
                        for (JSONObject jSONObject : listJsonFormaPago) {
                            if (jSONObject.get("formaPago").toString().contentEquals("GIFTCARD")) {
                                org.json.JSONObject jsonGift = new org.json.JSONObject(jSONObject.toString());
                                actualizarGiftCard(jsonGift);
                                monto += Long.parseLong(jsonGift.getString("monto"));
                            }
                        }
                        datos.put("giftcard", monto);
                    }
                    valeMayoMsjFInal = false;
                    if (hmFormaPago.containsKey("ASOCIACIÓN")) {
                        seteandoAsociacion();
                    } else if (hmFormaPago.containsKey("VALE INTERNO (FUNCIONARIO)") || hmFormaPago.containsKey("VALE")) {
                        seteandoVales();
                    }
                    if (hmFormaPago.containsKey("MONEDA EXTRANJERA")) {
                        //desagrable esto, pero no hay tiempo...
                        for (JSONObject jSONObject : listJsonFormaPago) {
                            if (jSONObject.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA")
                                    && jSONObject.containsKey("textFieldDolarCant") && jSONObject.containsKey("textFieldDolarGs")) {
                                seteandoDolares(jSONObject);
                            }
                            if (jSONObject.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA")
                                    && jSONObject.containsKey("textFieldRealCant") && jSONObject.containsKey("textFieldRealGs")) {
                                seteandoReales(jSONObject);
                            }
                            if (jSONObject.get("formaPago").toString().contentEquals("MONEDA EXTRANJERA")
                                    && jSONObject.containsKey("textFieldPesoCant") && jSONObject.containsKey("textFieldPesoGs")) {
                                seteandoPesos(jSONObject);
                            }
                        }
                    }
//                            if (!textFieldMontoRetencion.getText().equalsIgnoreCase("")) {
//                                seteandoRetencionciones();
//                            }
                    JSONObject factJson = (JSONObject) p.parse(fact.get("facturaClienteCab").toString());
                    factJson.put("forma_pago", formaDePago);
                    fact.put("facturaClienteCab", factJson);
                    seteandoFuncionario();
                    seteandoSumaTotales();
                    //reseteando campos cliente, cliente fiel, funcionario, factura venta para próxima transacción...
                    FacturaDeVentaMayFXMLController.resetParam();
                    BuscarClienteFXMLController.resetParamCliente();
                    NuevoClienteFXMLController.resetParamCliente();
                    NuevoClienteFXMLController.resetParamClienteFiel();
//                        FormasDePagoMayFXMLController.setJsonFuncionario(null);
//                    ClienteFXMLController.resetParam();
//                    ClienteFielFXMLController.resetParam();
//                    FuncionarioFXMLController.resetParam();
                    datos.remove("cancelProducto");
                    datos.remove("energiaElectrica");
                    actualizarDatos();
                    tarjConvActiva = false;
                    valeActivo = false;
                    MensajeFinalVenta mensajeFinalVenta = new MensajeFinalVenta();
                    mensajeFinalVenta.cargandoInicial();
                    finalizandoVentaGift();
                    navegandoAFactura();
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/cajamay/FormasDePagoMayFXML.fxml", 667, 501, false);
//                }
            }
        }
    }
    //FINAL FINAL FINAL ******************** -> -> -> -> -> -> -> -> -> -> -> ->

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void seteandoCamposMsjFinal() {
        MensajeFinalVenta.setTotalAbonado(toIntExact(totalAbonado));
        MensajeFinalVenta.setVuelto(Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText())));
        MensajeFinalVenta.setTotal(Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())));
        MensajeFinalVenta.setDescuento(Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())));
        MensajeFinalVenta.setTotalAPagar(Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
        MensajeFinalVenta.setDescuentoGift((int) descuentoGift);
        datos.put("totalApagar", Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
    }

    //FINAL FINAL FINAL SET SET SET ******************** -> -> -> -> -> -> -> ->
    private void seteandoDescuentos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDesc = 0;
        int sumDesc = 0;
        if (!json.isNull("contDesc")) {
            contDesc = Integer.parseInt(datos.get("contDesc").toString());
        }
        if (!json.isNull("sumDesc")) {
            sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
        }
        int descActual = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        if (descActual != 0) {
            sumDesc += descActual;
            contDesc += 1;
            datos.put("contDesc", contDesc);
            datos.put("sumDesc", sumDesc);
            formaDePago.put("monDesc", sumDesc);
        }
    }

    private void seteandoTarjetas() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contTarj = 0;
        int sumTarj = 0;
        if (!json.isNull("contTarj")) {
            contTarj = Integer.parseInt(datos.get("contTarj").toString());
        }
        if (!json.isNull("sumTarj")) {
            sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
        }
        int sumTarjeta = 0;
        for (JSONObject jSONObject : listJsonFormaPago) {
            if (jSONObject.get("formaPago").toString().contentEquals("TARJETAS")) {
                contTarj++;
                sumTarj += Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoTarjeta").toString()));
                sumTarjeta += Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMontoTarjeta").toString()));
            }
        }
        datos.put("contTarj", contTarj);
        datos.put("sumTarj", sumTarj);
        formaDePago.put("monTarj", sumTarjeta);
        setearEfectivoPorOtraFormaDePago();
    }

    private void setearEfectivoPorOtraFormaDePago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        long dif = totalAbonado - totalAPagar;
        if (!hmFormaPago.containsKey("EFECTIVO") && dif >= 0) {
            if (estado) {
                estado = false;
                int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
                //para cuando cae la corriente electrica
                int monEfec = 0;
                if (!json.isNull("monEfectivo")) {
                    monEfec = Integer.parseInt(datos.get("monEfectivo").toString());
                }
                datos.put("monEfectivo", (monEfec - vuel));
            }
        }
    }

    private void seteandoCheques() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contChq = 0;
        int sumChq = 0;
        if (!json.isNull("contCheque")) {
            contChq = Integer.parseInt(datos.get("contCheque").toString());
        }
        if (!json.isNull("sumCheque")) {
            sumChq = Integer.parseInt(datos.get("sumCheque").toString());
        }
        int sumaCheque = 0;
        if (hmFormaPago.containsKey("CHEQUE")) {
            for (JSONObject jSONObject : listJsonFormaPago) {
                if (jSONObject.get("formaPago").toString().contentEquals("CHEQUE")) {
                    contChq++;
                    sumChq += Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMonto").toString()));
                    sumaCheque += Integer.valueOf(numValidator.numberValidator(jSONObject.get("textFieldMonto").toString()));
                    arrayCheque.add(jSONObject.get("textFieldEntidad").toString() + " - " + jSONObject.get("textFIeldNroCheque").toString()
                            + " - " + jSONObject.get("textFieldMonto").toString());
                    estado = true;
                }
            }
        }
        datos.put("contCheque", contChq);
        datos.put("sumCheque", sumChq);
        if (estado) {
            datos.put("arrayCheque", arrayCheque);
        }
        formaDePago.put("monCheque", sumaCheque);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoNotaCred() {
        int notaCred = 0;
        int sumNotaCred = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("contNotaCred")) {
            notaCred = Integer.parseInt(datos.get("contNotaCred").toString());
        }
        if (!json.isNull("sumNotaCred")) {
            sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
        }
        int sumaNota = 0;
        if (hmFormaPago.containsKey("NOTA DE CRÉDITO")) {
            for (JSONObject jsonObject : listJsonFormaPago) {
                if (jsonObject.get("formaPago").toString().contentEquals("NOTA DE CRÉDITO")) {
                    notaCred += 1;
                    sumNotaCred += Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoNotaCred").toString()));
                    sumaNota += Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoNotaCred").toString()));
                }
            }
        }
        datos.put("sumNotaCred", sumNotaCred);
        datos.put("contNotaCred", notaCred);
        formaDePago.put("monNotaCred", sumaNota);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoEfectivos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contEfect = 0;
        int montoActual = 0;
        for (JSONObject jsonObject : listJsonFormaPago) {
            if (jsonObject.get("formaPago").toString().contentEquals("EFECTIVO")) {
                montoActual = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldEfectivo").toString()));
            }
        }
        if (!json.isNull("cantEfectivoRecibido")) {
            datos.put("cantEfectivoRecibido", Integer.parseInt(datos.get("cantEfectivoRecibido").toString()) + montoActual);
        } else {
            datos.put("cantEfectivoRecibido", montoActual);
        }
        if (!json.isNull("contEfectivo")) {
            contEfect = Integer.parseInt(datos.get("contEfectivo").toString());
        }
        long dif = totalAbonado - totalAPagar;
        if (dif == 0) {
            contEfect += 1;
            datos.put("contEfectivo", contEfect);
            //por si vaya la energia electrica
            formaDePago.put("monEfectivo", montoActual);
        } else {
            int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
            if (montoActual > vuel) {
                int resultado = montoActual - vuel;
                contEfect += 1;
                datos.put("contEfectivo", contEfect);
                formaDePago.put("monEfectivo", resultado);
            } else {
                datos.put("contEfectivo", 0);
                formaDePago.put("monEfectivo", 0);
            }
        }
    }

    private void seteandoAsociacion() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contAsoc = 0;
        if (!json.isNull("contAsoc")) {
            contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
        }
        datos.put("contAsoc", contAsoc + 1);
        int sumAsoc = 0;
        if (!json.isNull("sumAsoc")) {
            sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
        }
        for (JSONObject jsonObject : listJsonFormaPago) {
            if (jsonObject.get("formaPago").toString().contentEquals("ASOCIACIÓN")) {
                datos.put("sumAsoc", (sumAsoc + Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoAsoc").toString()))));
                formaDePago.put("monAsoc", Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoAsoc").toString())));
                datos.put("asociacionTicket", numValidator.numberValidator(jsonObject.get("textFieldMontoAsoc").toString()));
            }
        }
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoVales() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contVale = 0;
        if (!json.isNull("contVale")) {
            contVale = Integer.parseInt(datos.get("contVale").toString());
        }
        datos.put("contVale", contVale + 1);
        int sumVale = 0;
        if (!json.isNull("sumVale")) {
            sumVale = Integer.parseInt(datos.get("sumVale").toString());
        }
        for (JSONObject jsonObject : listJsonFormaPago) {
            if (jsonObject.get("formaPago").toString().contentEquals("VALE")) {
                datos.put("sumVale", (sumVale + Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoVale").toString()))));
                formaDePago.put("monVale", Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoVale").toString())));
                valeMayoMsjFInal = true;
            } else if (jsonObject.get("formaPago").toString().contentEquals("VALE INTERNO (FUNCIONARIO)")) {
                datos.put("sumVale", (sumVale + Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoValeInterno").toString()))));
                formaDePago.put("monVale", Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldMontoValeInterno").toString())));
            }
        }
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoDolares(JSONObject jsonObject) {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDolar = 0;
        if (!json.isNull("contDolar")) {
            contDolar = Integer.parseInt(datos.get("contDolar").toString());
        }
        int cantidad = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldDolarCant").toString()));
        datos.put("contDolar", (contDolar + cantidad));
        int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldDolarGs").toString()));
        int sumDolar = 0;
        if (!json.isNull("sumDolar")) {
            sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
        }
        datos.put("sumDolar", (sumDolar + monto));
        formaDePago.put("monDolar", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoReales(JSONObject jsonObject) {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contReal = 0;
        int sumReal = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldRealCant").toString()));
        if (!json.isNull("contReal")) {
            contReal = Integer.parseInt(datos.get("contReal").toString());
        }
        datos.put("contReal", (contReal + cantidad));
        int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldRealGs").toString()));
        if (!json.isNull("sumReal")) {
            sumReal = Integer.parseInt(datos.get("sumReal").toString());
        }
        datos.put("sumReal", (sumReal + monto));
        formaDePago.put("monReal", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoPesos(JSONObject jsonObject) {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contPeso = 0;
        int sumPeso = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldPesoCant").toString()));
        if (!json.isNull("contPeso")) {
            contPeso = Integer.parseInt(datos.get("contPeso").toString());
        }
        datos.put("contPeso", (contPeso + cantidad));
        int monto = Integer.valueOf(numValidator.numberValidator(jsonObject.get("textFieldPesoGs").toString()));
        if (!json.isNull("sumPeso")) {
            sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
        }
        datos.put("sumPeso", (sumPeso + monto));
        formaDePago.put("monPeso", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoRetencionciones() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int sumRetencion = 0;
        int contRetencion = 0;
        if (!json.isNull("sumRetencion")) {
            sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
        }
        if (!json.isNull("contRetencion")) {
            contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
        }
//        int montoRetencion = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
//        datos.put("sumRetencion", (sumRetencion + montoRetencion));
//        formaDePago.put("monRetencion", montoRetencion);
        datos.put("contRetencion", (contRetencion + 1));
    }

    private void seteandoFuncionario() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!labelFuncionario.getText().isEmpty()) {
            int nFun = 0;
            if (!json.isNull("nFuncionarios")) {
                nFun = Integer.parseInt(datos.get("nFuncionarios").toString());
            }
            datos.put("nFuncionarios", (nFun + 1));
        }
    }

    private void seteandoSumaTotales() {
        int sumTotales = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("totales")) {
            sumTotales = Integer.parseInt(datos.get("totales").toString());
        }
        int monto = sumTotales + Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
        datos.put("totales", monto);

    }
    //FINAL FINAL FINAL SET SET SET ******************** -> -> -> -> -> -> -> ->

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        @SuppressWarnings("element-type-mismatch")
        public void handle(KeyEvent event) {
            if (null != event.getCode()) {
                if (this.comboBox.getVisibleRowCount() < 2) {
                    this.comboBox.setVisibleRowCount(5);
                }
                switch (event.getCode()) {
                    case UP:
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    case DOWN:
                        if (!comboBox.isShowing()) {
                            comboBox.show();
                        }
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    case BACK_SPACE:
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                        break;
                    case DELETE:
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                        break;
                    default:
                        break;
                }
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                /*if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
                    if (hashJsonComboTarjeta.containsKey(comboBox.getEditor().getText())) {
                        textFieldMontoTarjeta.requestFocus();
                    } else if (!comboBox.getEditor().getText().contentEquals("")) {
                        mensajeAdv("NO SE RECONOCE LA TARJETA " + comboBox.getEditor().getText());
                    } else {
                        textFieldMontoTarjeta.requestFocus();
                    }
                }*/
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }
    }

    public class AutoCompleteComboBoxListenerFP<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListenerFP(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(5);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListenerFP.this);
        }

        @Override
        @SuppressWarnings("element-type-mismatch")
        public void handle(KeyEvent event) {
            if (null != event.getCode()) {
                if (this.comboBox.getVisibleRowCount() < 2) {
                    this.comboBox.setVisibleRowCount(5);
                }
                switch (event.getCode()) {
                    case UP:
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    case DOWN:
                        if (!comboBox.isShowing()) {
                            comboBox.show();
                        }
                        caretPos = -1;
                        moveCaret(comboBox.getEditor().getText().length());
                        return;
                    case BACK_SPACE:
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                        break;
                    case DELETE:
                        moveCaretToPos = true;
                        caretPos = comboBox.getEditor().getCaretPosition();
                        break;
                    default:
                        break;
                }
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                /*if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
                    if (hashJsonComboTarjeta.containsKey(comboBox.getEditor().getText())) {
                        textFieldMontoTarjeta.requestFocus();
                    } else if (!comboBox.getEditor().getText().contentEquals("")) {
                        mensajeAdv("NO SE RECONOCE LA TARJETA " + comboBox.getEditor().getText());
                    } else {
                        textFieldMontoTarjeta.requestFocus();
                    }
                }*/
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListenerFP.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }
    }

    class MyListCell extends ListCell<String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null || empty) {
                setGraphic(null);
            } else {
                if (item.contains(" || ")) {
                    HBox hBox = new HBox(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/tarjeta_desc.jpg"))), new Label(item));
                    hBox.setAlignment(Pos.CENTER_LEFT);
                    hBox.setSpacing(10);
                    setGraphic(hBox);
                } else {
                    if (item.contentEquals("")) {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    } else {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/tarjeta.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    }
                }
            }
            setText("");
        }
    }

    public static boolean isDescTarjetaFiel() {
        return descTarjetaFiel;
    }

    public static boolean isDescFuncionario() {
        return descFuncionario;
    }

    public static boolean isDescPromoTemp() {
        return descPromoTemp;
    }

    public static double getMontoConDes5() {
        return montoConDes5;
    }

    public static long getExenta() {
        return exenta;
    }

    public static double getMontoConDes10() {
        return montoConDes10;
    }

    public static JSONObject getJsonFuncionario() {
        return jsonFuncionario;
    }

    public static void setJsonFuncionario(JSONObject aJsonFuncionario) {
        jsonFuncionario = aJsonFuncionario;
    }

    public static boolean isValeMayoMsjFInal() {
        return valeMayoMsjFInal;
    }

    private Map buscarTarjetaGiftCard() {
        numValidator = new NumberValidator();
        Map mapeo = new HashMap();
        if (ConexionParana.conectarBloque3()) {
            String sql = "SELECT * FROM stk_articulos WHERE codigo=" + textFieldCodGift.getText();
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    double montoGift = rs.getDouble("saldogift");
                    labelSaldoGift.setText(numValidator.numberFormat("Gs ###,###.###", montoGift));
                    datePickerVigencia.setValue(rs.getDate("fechavtogift").toLocalDate());
                    mapeo.put("monto", montoGift);
                    mapeo.put("fecha", rs.getDate("fechavtogift"));
                    textFieldMontoGift.setDisable(false);
                } else {
                    labelSaldoGift.setText("Gs 0");
                    datePickerVigencia.setValue(null);
                    textFieldMontoGift.setDisable(true);
                    mapeo.put("monto", 0);
                    mapeo.put("fecha", "");
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
                mapeo = new HashMap();
            }
            ConexionParana.cerrarBloque3();
        }
        return mapeo;
    }

    private void actualizarGiftCard(org.json.JSONObject jsonGift) {
        ConexionParana.conectarBloque3();
        long monto = Long.parseLong(jsonGift.getString("monto"));
        double saldo = recuperarSaldo(jsonGift.get("codigo").toString());
        String sql = "UPDATE stk_articulos SET saldogift=" + (saldo - monto) + " WHERE codigo=" + jsonGift.get("codigo");
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
            }
            ps.close();
            ConexionParana.getConBloque3().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionParana.getConBloque3().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionParana.cerrarBloque3();
    }

    private double recuperarSaldo(String codigo) {
        double saldo = 0l;
        String sql = "SELECT saldogift FROM stk_articulos WHERE codigo=" + codigo;
        try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                saldo = rs.getDouble("saldogift");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        return saldo;
    }

    private boolean verificarGift() {
        boolean valor = false;
        for (JSONObject jSONObject : listJsonFormaPago) {
            switch (jSONObject.get("formaPago").toString()) {
                case "GIFTCARD":
                    if (jSONObject.get("codigo").toString().equals(textFieldCodGift.getText())) {
                        valor = true;
                    }
                    break;
            }
        }
        return valor;
    }

    private void validandoGiftFielDto(JSONObject articulo, JSONObject detalleArtJson) {
        descuentoGift += Math.round(Long.parseLong(articulo.get("precioMin").toString()) * 0.1);
        exenta += Math.round((Long.parseLong(articulo.get("precioMin").toString()) * 0.9));
        montoMapExe.put(detalleArtJson, Math.round((Long.parseLong(articulo.get("precioMin").toString()) * 0.9)));
    }

    private void finalizandoVentaGift() {
        if (!FacturaDeVentaMayFXMLController.getHmGift().isEmpty()) {
            if (ConexionParana.getConBloque3() == null) {
                if (ConexionParana.conectarBloque3()) {
                    for (Map.Entry<Long, JSONObject> entry : FacturaDeVentaMayFXMLController.getHmGift().entrySet()) {
                        actualizarGiftCardVenta(entry.getKey(), entry.getValue());
                    }
                    ConexionParana.cerrarBloque3();
                } else {
                }
            } else {
                for (Map.Entry<Long, JSONObject> entry : FacturaDeVentaMayFXMLController.getHmGift().entrySet()) {
                    actualizarGiftCardVenta(entry.getKey(), entry.getValue());
                }
                ConexionParana.cerrarBloque3();
            }
        }
        FacturaDeVentaMayFXMLController.setHmGift(new HashMap<>());
    }

    private void actualizarGiftCardVenta(long codigo, JSONObject jsonGift) {
        long saldo = Long.parseLong(jsonGift.get("saldogift").toString());
        int comprado = Integer.valueOf(jsonGift.get("comprado").toString());
        String sql = "UPDATE stk_articulos SET saldogift=" + saldo + ", "
                + "comprado=" + comprado + ", "
                + "fechavtogift='" + jsonGift.get("fechavtogift").toString() + "' "
                + "WHERE codigo=" + codigo;
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("*** Haz agregado un nuevo registro a B3 ****");
            }
            ps.close();
            ConexionParana.getConBloque3().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            insertarLocalmente(sql);
            try {
                ConexionParana.getConBloque3().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
    }

    private void insertarLocalmente(String sqlString) {
        ConexionPostgres.conectarAviso();
        String sql = "INSERT INTO pendiente.giftcard_pendientes(msj, fecha_registro) "
                + "VALUES('" + sqlString + "', now())";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getConAviso().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
            }
            ps.close();
            ConexionParana.getConBloque3().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionParana.getConBloque3().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionParana.cerrarBloque3();
    }

}
