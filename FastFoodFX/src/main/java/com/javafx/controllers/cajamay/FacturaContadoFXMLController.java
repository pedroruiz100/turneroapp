/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import com.jfoenix.controls.JFXDatePicker;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class FacturaContadoFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private AnchorPane anchorPane1;
    @FXML
    private AnchorPane anchorPane2;
    @FXML
    private Button buttonOk;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelFechaFra;
    @FXML
    private Label labelNroFra;
    @FXML
    private Label labelCodCliente;
    @FXML
    private Label labelNombreCliente;
    @FXML
    private JFXDatePicker jfxDatePickerFechaFra;
    @FXML
    private TextField textFieldNroFra;
    @FXML
    private TextField textFieldCodCliente;
    @FXML
    private Button buttonCliente;
    @FXML
    private TextArea textAreaNombreCliente;
    @FXML
    private Label labelTitulo;
    @FXML
    private GridPane gridPaneFra;
    @FXML
    private GridPane gridPaneImpManual;
    @FXML
    private Label labelRucParaImp;
    @FXML
    private Label labelNombreParaImp;
    @FXML
    private TextField textFieldRucImp;
    @FXML
    private HBox hBoxVendedor;
    @FXML
    private Label labelVendedor;
    @FXML
    private Button buttonVendedor;
    @FXML
    private TextField textFieldVendedor;
    @FXML
    private HBox hBoxDireccCliente;
    @FXML
    private Label labelDireccion;
    @FXML
    private TextField textFieldDireccion;
    @FXML
    private GridPane gridPaneFra1;
    @FXML
    private Label labelCiNro;
    @FXML
    private Label labelRUC;
    @FXML
    private TextField textFieldNroFra1;
    @FXML
    private TextField textField;
    @FXML
    private Label labelTelef;
    @FXML
    private TextField textFieldNroFra12;
    @FXML
    private TextArea textAreaNombreClienteParaImp;
    @FXML
    private Label labelTituloSub;
    @FXML
    private GridPane gridPaneFra2;
    @FXML
    private Label labelSubTotales;
    @FXML
    private Label labelDescuento;
    @FXML
    private Label labelTotales;
    @FXML
    private TextField textFieldNroFra2;
    @FXML
    private Label labelExenta;
    @FXML
    private Label labelGrav5;
    @FXML
    private Label labelGrav10;
    @FXML
    private TextField textFieldNroFra21;
    @FXML
    private TextField textFieldNroFra22;
    @FXML
    private TextField textFieldNroFra23;
    @FXML
    private TextField textFieldNroFra24;
    @FXML
    private TextField textFieldNroFra25;
    @FXML
    private TextField textFieldNroFra26;
    @FXML
    private TextField textFieldNroFra27;
    @FXML
    private TextField textFieldNroFra28;
    @FXML
    private TextField textFieldNroFra29;
    @FXML
    private TextField textFieldNroFra210;
    @FXML
    private Label labelIva5;
    @FXML
    private Label labelIva10;
    @FXML
    private VBox vBoxTotales;
    @FXML
    private Label labelTotal;
    @FXML
    private TextField textFieldRucImp1;
    @FXML
    private Label labelCheque;
    @FXML
    private Label labelTarjCred;
    @FXML
    private Label labelEfectivo;
    @FXML
    private Label labelNC;
    @FXML
    private Label labelMontoRetencion;
    @FXML
    private Label labelMontoCred;
    @FXML
    private VBox vBoxTotales1;
    @FXML
    private Label labelVuelto;
    @FXML
    private TextField textFieldRucImp11;
    @FXML
    private TextField textFieldEfectivo;
    @FXML
    private TextField textFieldCheque;
    @FXML
    private TextField textFieldTarjCred;
    @FXML
    private TextField textFieldNotaCred;
    @FXML
    private TextField textFieldMontoRetencion;
    @FXML
    private TextField textFieldMontoCred;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonOkAction(ActionEvent event) {
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonClienteAction(ActionEvent event) {
    }

    @FXML
    private void buttonVendedorAction(ActionEvent event) {
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/FacturaContadoFXML.fxml", 927, 684, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
}
