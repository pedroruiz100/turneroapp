/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cajamay;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class NotaDeCreditoFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private AnchorPane anchorPane1;
    @FXML
    private GridPane gridPaneFra;
    @FXML
    private Label labelNroFra;
    @FXML
    private Label labelCodCliente;
    @FXML
    private Label labelNombreCliente;
    @FXML
    private TextField textFieldNroFra;
    @FXML
    private TextField textFieldCodCliente;
    @FXML
    private Button buttonCliente;
    @FXML
    private TextArea textAreaNombreCliente;
    @FXML
    private GridPane gridPaneImpManual;
    @FXML
    private Label labelRucParaImp;
    @FXML
    private Label labelNombreParaImp;
    @FXML
    private TextField textFieldRucImp;
    @FXML
    private HBox hBoxVendedor;
    @FXML
    private Label labelVendedor;
    @FXML
    private TextField textFieldVendedor;
    @FXML
    private HBox hBoxDireccCliente;
    @FXML
    private Label labelDireccion;
    @FXML
    private TextField textFieldDireccion;
    @FXML
    private GridPane gridPaneFra1;
    @FXML
    private Label labelRUC;
    @FXML
    private TextField textFieldNroFra1;
    @FXML
    private Label labelTelef;
    @FXML
    private TextField textFieldNroFra12;
    @FXML
    private AnchorPane anchorPane2;
    @FXML
    private Label labelTituloSub;
    @FXML
    private GridPane gridPaneFra2;
    @FXML
    private Label labelSubTotales;
    @FXML
    private Label labelDescuento;
    @FXML
    private Label labelTotales;
    @FXML
    private TextField textFieldNroFra2;
    @FXML
    private Label labelExenta;
    @FXML
    private Label labelGrav5;
    @FXML
    private Label labelGrav10;
    @FXML
    private TextField textFieldNroFra21;
    @FXML
    private TextField textFieldNroFra22;
    @FXML
    private TextField textFieldNroFra23;
    @FXML
    private TextField textFieldNroFra24;
    @FXML
    private TextField textFieldNroFra25;
    @FXML
    private TextField textFieldNroFra26;
    @FXML
    private TextField textFieldNroFra27;
    @FXML
    private TextField textFieldNroFra28;
    @FXML
    private TextField textFieldNroFra29;
    @FXML
    private TextField textFieldNroFra210;
    @FXML
    private Label labelIva5;
    @FXML
    private Label labelIva10;
    @FXML
    private VBox vBoxTotales;
    @FXML
    private Label labelTotal;
    @FXML
    private TextField textFieldRucImp1;
    @FXML
    private Button buttonOk;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelTitulo;
    @FXML
    private Label labelRucParaImp1;
    @FXML
    private Label labelRucParaImp2;
    @FXML
    private TextArea textAreaNombreCliente1;
    @FXML
    private TextField textFieldRucImp2;
    @FXML
    private ComboBox<?> comboBoxTipo;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonClienteAction(ActionEvent event) {
    }

    @FXML
    private void buttonOkAction(ActionEvent event) {
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }
    
    
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/NotaDeCreditoFXML.fxml", 819, 524, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

}
