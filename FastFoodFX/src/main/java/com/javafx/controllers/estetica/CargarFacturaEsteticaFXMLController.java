/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.ClientePendiente;
import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ClientePendienteDAO;
import com.peluqueria.dao.FuncionarioDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.ServPendienteDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ClientePendienteDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Articulo;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.controlsfx.control.textfield.TextFields;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class CargarFacturaEsteticaFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    private JSONArray cliPenJSONArray;
    private static List<JSONObject> cliPenList;
    private boolean alert;
    final KeyCombination altA = new KeyCodeCombination(KeyCode.A, KeyCombination.ALT_DOWN);

    @Autowired
    private ClientePendienteDAO cpDAO;

    @Autowired
    private ArticuloDAO artDAO;

    @Autowired
    private FuncionarioDAO funcDAO;

    @Autowired
    private ServPendienteDAO spDAO;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.z").create();
    private Gson gson2 = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    int posicionCliPen;
    boolean escape;

    public static int indexSeleccionado;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    private ObservableList<JSONObject> srvData;

    @FXML
    private AnchorPane anchorPaneCargarFacturaEstetica;
    @FXML
    private GridPane gridPaneClienteNuevo11;
    @FXML
    private Button buttonSalir;
    @FXML
    private Button buttonCargarFactura;
    @FXML
    private ComboBox<String> comboBoxCargarFacturaEstetica;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnIva;
    @FXML
    private TableColumn<JSONObject, String> tableColumnPrecio;
    @FXML
    private TableColumn<JSONObject, String> tableColumnEstilista;
    @FXML
    private TableView<JSONObject> tableViewServicios;
    @FXML
    private TableColumn<JSONObject, Boolean> tableColumnEliminar;
    @FXML
    private Button btnActualizar;
    @FXML
    private Label labelTotal;
    @FXML
    private Label labelTotalGs;
    @FXML
    private TextField txtCiCliente;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidad;

//    private TableColumn<JSONObject, Boolean> columnEliminar;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
        saliendo();
    }

    @FXML
    private void buttonCargarFacturaAction(ActionEvent event) {
        try {
            procesar();
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

    @FXML
    private void comboBoxCargarFacturaEsteticaReleased(KeyEvent event) {
        keyPressComboBoxClientePendiente(event);
    }

    @FXML
    private void anchorPaneCargarFacturaEsteticaKeyReleased(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            try {
                if (!this.alert) {
                    procesar();
                } else {
                    this.alert = false;
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }
        }
//        if (keyCode == event.getCode().DELETE) {
//            int posicion = tableViewServicios.getSelectionModel().getSelectedIndex();
//            if (posicion >= 0) {
//                cancelar();
//            }
//        }
        if (altA.match(event)) {
            JSONParser parser = new JSONParser();
            int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
            if (index >= 0) {
                String seleccionado = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedItem();
                cargarClientePendiente();
                comboBoxCargarFacturaEstetica.getSelectionModel().select(seleccionado);
                indexSeleccionado = index;
                JSONObject jsonCliPen = cliPenList.get(index);
//            NEW
                ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(jsonCliPen.get("idClientePendiente").toString())).toEliminarPendienteDTO();
                cpDTO.setFechaAlta(null);
                cpDTO.setFechaMod(null);
                try {
                    JSONArray array = new JSONArray();
                    for (int i = 0; i < cpDTO.getServPendiente().size(); i++) {
                        JSONObject obj = new JSONObject();
                        obj.put("idServPendiente", cpDTO.getServPendiente().get(i).getIdServPendiente());
                        array.add(obj);
                    }
                    jsonCliPen.put("servPendiente", array);
                    jsonCliPen = (JSONObject) parser.parse(gson.toJson(cpDTO).toString());
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                }
                actualizandoTablaServiciosPendientes(jsonCliPen);
            } else {
                cargarClientePendiente();
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewServicios.requestFocus();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!escape) {
                saliendo();
            } else {
                escape = false;
            }
        }
    }

    private void cancelar() {
        JSONParser parser = new JSONParser();

//        tableViewServicios.getSelectionModel().select(tableViewServicios..getIndex());
        int posicion = tableViewServicios.getSelectionModel().getSelectedIndex();
        if (posicion >= 0) {
            JSONObject servicios = tableViewServicios.getItems().get(posicion);
            JSONObject art = null;
            JSONObject func = null;
            String nombreFunc = "";
            try {
                art = (JSONObject) parser.parse(servicios.get("articulo").toString());
                func = (JSONObject) parser.parse(servicios.get("funcionario").toString());
                Funcionario funci = funcDAO.getById(Long.parseLong(func.get("idFuncionario").toString()));
                nombreFunc = funci.getNombre() + " " + funci.getApellido();
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }

            int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
            JSONObject jsonCP;
            boolean valor = false;
            try {
                jsonCP = (JSONObject) parser.parse(cliPenList.get(index).toString());
                ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(jsonCP.get("idClientePendiente").toString())).toEliminarPendienteDTO();
                cpDTO.setFechaAlta(null);
                cpDTO.setFechaMod(null);
                jsonCP = (JSONObject) parser.parse(gson.toJson(cpDTO).toString());

                JSONArray jsonSrv = (JSONArray) parser.parse(jsonCP.get("servPendiente").toString());
                for (int i = 0; i < jsonSrv.size(); i++) {
                    JSONObject jsonServicio = (JSONObject) parser.parse(jsonSrv.get(i).toString());
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonServicio.get("articulo").toString());

                    long idArticuloSeleccionado = Long.parseLong(art.get("idArticulo").toString());
                    long idArt = Long.parseLong(jsonArt.get("idArticulo").toString());
                    if (idArticuloSeleccionado == idArt) {
                        valor = true;
                        break;
                    }
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }
            if (valor) {
                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR EL SERVICIO "
                        + art.get("descripcion").toString() + " REALIZADO POR " + nombreFunc + "?", ok, cancel);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ok) {
                    spDAO.eliminar(Long.parseLong(servicios.get("idServPendiente").toString()));
                    tableViewServicios.getItems().remove(posicion);
                    JSONObject jsonCliPen = cliPenList.get(indexSeleccionado);
                    try {
                        JSONArray arraySrvPend = (JSONArray) parser.parse(jsonCliPen.get("servPendiente").toString());
                        JSONArray newJsonArray = new JSONArray();
                        for (int i = 0; i < arraySrvPend.size(); i++) {
                            JSONObject json = (JSONObject) parser.parse(arraySrvPend.get(i).toString());
                            long idDetalle = Long.parseLong(json.get("idServPendiente").toString());
                            if (idDetalle != Long.parseLong(servicios.get("idServPendiente").toString())) {
                                newJsonArray.add(json);
                            }
                        }
                        jsonCliPen.put("servPendiente", newJsonArray);
                        cliPenList.remove(indexSeleccionado);
                        cliPenList.add(indexSeleccionado, jsonCliPen);
                    } catch (ParseException ex) {
                        Logger.getLogger(CargarFacturaEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else if (alert2.getResult() == cancel) {
                    escape = true;
                    alert2.close();
                }
            } else {
                mensajeError("El Servicio ya ha sido eliminado desde el módulo táctil");
                tableViewServicios.getItems().remove(posicion);
            }
        }
    }

    private void cargandoInicial() {
        cliPenJSONArray = new JSONArray();
        cliPenList = new ArrayList<>();
//        comboBoxCargarFacturaEstetica.getItems().clear();
        posicionCliPen = 0;
        indexSeleccionado = -1;
        escape = false;
        cargarClientePendiente();
        listenComboBox();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void saliendo() {
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/CargarFacturaEsteticaFXML.fxml", 837, 468, true);
    }

    //Apartado 4 - AVISOS
    private void mensajeError(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private boolean mensajeConfirmacion(String msj) {
        escape = true;
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, msj.toUpperCase(), ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            return true;
        } else {
            alert.close();
            return false;
        }
    }

    private void mensajeInfo(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj.toUpperCase(), ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    private void keyPressComboBoxClientePendiente(KeyEvent event) {
        TextFields.bindAutoCompletion(comboBoxCargarFacturaEstetica.getEditor(), comboBoxCargarFacturaEstetica.getItems());
    }

    private void listenComboBox() {
        JSONParser parser = new JSONParser();

        comboBoxCargarFacturaEstetica.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
            indexSeleccionado = index;
            JSONObject jsonCliPen = cliPenList.get(index);

//            NEW
            ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(jsonCliPen.get("idClientePendiente").toString())).toEliminarPendienteDTO();
            cpDTO.setFechaAlta(null);
            cpDTO.setFechaMod(null);
            txtCiCliente.setText(cpDTO.getCliente().getRuc());
            try {
                jsonCliPen = (JSONObject) parser.parse(gson.toJson(cpDTO).toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }
//                    FINAL NEW
            actualizandoTablaServiciosPendientes(jsonCliPen);
        });
    }

    //Apartado 6 - BACKEND
    private void insertarServPendientes(JSONArray servPendiente, long idServPendiente) {
        JSONParser parser = new JSONParser();
        String inputLine;
        boolean exitoInsert = false;
        JSONArray jsonSP = new JSONArray();
        JSONArray jsonServices = new JSONArray();
        try {
            for (int i = 0; i < servPendiente.size(); i++) {
                JSONObject jsonClientePendiente = (JSONObject) parser.parse(servPendiente.get(i).toString());

                JSONObject clientePendiente = new JSONObject();
                JSONObject articulo = new JSONObject();
                JSONObject funcionario = new JSONObject();

                clientePendiente.put("idClientePendiente", idServPendiente);

                JSONObject objArt = (JSONObject) parser.parse(jsonClientePendiente.get("articulo").toString());

                long idArt = Long.parseLong(objArt.get("idArticulo").toString());
                articulo.put("idArticulo", idArt);

                JSONObject objFun = (JSONObject) parser.parse(jsonClientePendiente.get("funcionario").toString());
                long idFun = Long.parseLong(objFun.get("idFuncionario").toString());
                funcionario.put("idFuncionario", idFun);

                jsonClientePendiente.put("articulo", articulo);
                jsonClientePendiente.put("funcionario", funcionario);
                jsonClientePendiente.put("clientePendiente", clientePendiente);

                jsonSP.add(jsonClientePendiente);
                jsonServices.add(objArt);
            }
            datos.put("arrayServicios", jsonServices);
            //            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaGenerico("id_dato", "estetica.cli_pendiente") == 0) {
            //                URL url = new URL(Utilidades.ip + "/ServerParana/servPendiente/insercionMasiva");
            //                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            //                conn.setDoOutput(true);
            //                conn.setDoInput(true);
            //                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            //                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
            //                conn.setRequestMethod("POST");
            //                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
            //                wr.write(jsonSP.toString());
            //                wr.flush();
            //                int HttpResult = conn.getResponseCode();
            //                if (HttpResult == HttpURLConnection.HTTP_OK) {
            //                    BufferedReader br = new BufferedReader(
            //                            new InputStreamReader(conn.getInputStream(), "utf-8"));
            //                    while ((inputLine = br.readLine()) != null) {
            //                        exitoInsert = (Boolean) parser.parse(inputLine);
            //                    }
            //                    br.close();
            //                } else {
            //                    exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
            //                }
            //            } else {
            exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
//            }
        } catch (ParseException ex) {
//            exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
        }
        if (exitoInsert) {
            System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR");
        }
    }

    private void insertarClientePendientes(JSONObject cliPen) {
        JSONParser parser = new JSONParser();
        String inputLine;
        JSONObject jsonRetorno = null;
        try {
            org.json.JSONObject json = new org.json.JSONObject(cliPen);
            if (!json.isNull("fechaAlta")) {
                Timestamp fecAlt = Timestamp.valueOf(cliPen.get("fechaAlta").toString());
                cliPen.remove("fechaAlta");
                cliPen.put("fechaAlta", fecAlt.getTime());
            } else {
                cliPen.remove("fechaAlta");
                cliPen.put("fechaAlta", null);
            }

            if (!json.isNull("fechaMod")) {
                Timestamp fecMod = Timestamp.valueOf(cliPen.get("fechaMod").toString());
                cliPen.remove("fechaMod");
                cliPen.put("fechaMod", fecMod.getTime());
            } else {
                cliPen.remove("fechaMod");
                cliPen.put("fechaMod", null);
            }

            JSONObject cliente = new JSONObject();

            JSONObject obj = (JSONObject) parser.parse(cliPen.get("cliente").toString());
            long idCliente = Long.parseLong(obj.get("idCliente").toString());
            cliente.put("idCliente", idCliente);

            cliPen.put("cliente", cliente);

//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/clientePendiente/");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(cliPen.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        jsonRetorno = (JSONObject) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
//                }
//            } else {
            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
//            }
        } catch (ParseException ex) {
//            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
        }
        if (jsonRetorno.get("idClientePendiente") == null) {
            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
        }
        if (jsonRetorno != null) {
            System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR");
        }
    }

    //Apartado 7 - LOCAL
    private JSONObject persistiendoClientePendientesLocal(String jsonCliPen) {
        ConexionPostgres.conectar();
        JSONParser parser = new JSONParser();
        JSONObject jsonRetorno = null;
        String sql = "INSERT INTO estetica.cli_pendiente (descripcion_dato, operacion, fecha) VALUES ('" + jsonCliPen + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                jsonRetorno = (JSONObject) parser.parse(jsonCliPen);
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            jsonRetorno = null;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                jsonRetorno = null;
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            jsonRetorno = null;
        }
        ConexionPostgres.cerrar();
        return jsonRetorno;
    }

    private boolean persistiendoSrvPendientesLocal(String arraySrvPendiente) {
//        ConexionPostgres.conectar();
        boolean valor = false;
//        String sql = "INSERT INTO estetica.serv_pendiente (descripcion_dato, operacion, fecha) VALUES ('" + arraySrvPendiente + "', 'I', now())";
//        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
//            int op = ps.executeUpdate();
//            if (op >= 1) {
        valor = true;
//                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
//            }
//            ConexionPostgres.getCon().commit();
//        } catch (SQLException ex) {
//            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
//            valor = false;
//            try {
//                ConexionPostgres.getCon().rollback();
//            } catch (SQLException ex1) {
//                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
//                valor = false;
//            }
//        }
//        ConexionPostgres.cerrar();
        return valor;
    }

    //Apartado 8 - JSON
    private void jsonCargandoClientePendiente() throws ParseException {
        comboBoxCargarFacturaEstetica.getItems().clear();
        cliPenJSONArray = recuperarDatosLocal();
        JSONParser parser = new JSONParser();
        cliPenList = new ArrayList<>();
        int pos = 0;
        for (Object obj : cliPenJSONArray) {
            JSONObject jsonCliPen = (JSONObject) obj;
            JSONObject cliente = (JSONObject) parser.parse(jsonCliPen.get("cliente").toString());
            org.json.JSONObject jsonCli = new org.json.JSONObject(cliente);

            cliPenList.add(jsonCliPen);

            String nombre = "";
            if (!jsonCli.isNull("nombre")) {
                nombre = cliente.get("nombre").toString();
            }
            if (!jsonCli.isNull("apellido")) {
                nombre += " " + cliente.get("apellido").toString();
            }

            comboBoxCargarFacturaEstetica.getItems().add(nombre);
            pos++;
        }
    }

    private JSONArray recuperarDatosLocal() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        List<ClientePendiente> listCliPen = cpDAO.listarPendiente();
        for (ClientePendiente cp : listCliPen) {
            try {
                cp.setFechaAlta(null);
                cp.setFechaMod(null);
//                JSONObject json = (JSONObject) parser.parse(gson.toJson(cp.toClientePendienteDTO()));
                JSONObject json = (JSONObject) parser.parse(gson.toJson(cp.toEliminarPendienteDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }

    //Apartado 10 - TABLE VIEW
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaServiciosPendientes(JSONObject jsonCliPen) {
        JSONParser parser = new JSONParser();
        List<JSONObject> listJSON = new ArrayList<>();
        try {
            long precio = 0;
            listJSON = (List<JSONObject>) parser.parse(jsonCliPen.get("servPendiente").toString());
            for (int i = 0; i < listJSON.size(); i++) {
//                recupera el articulo que solo contiene el idArticulo7
                JSONObject art = new JSONObject();
                try {
                    art = (JSONObject) parser.parse(listJSON.get(i).get("articulo").toString());
                } catch (Exception e) {
                    Articulo arti = artDAO.buscarCod(listJSON.get(i).get("codArticulo").toString());
                    arti.setFechaAlta(null);
                    arti.setFechaMod(null);
                    art = (JSONObject) parser.parse(gson.toJson(arti.toBDArticuloDTO()));
                } finally {
                }

                long idArt = Long.parseLong(art.get("idArticulo").toString());
                //recupera todos los datos del articulo con el iva
                ArticuloDTO articDTO = artDAO.getById(idArt).toBDWithIvaArticuloDTO();
                articDTO.setFechaAlta(null);
                articDTO.setFechaMod(null);
                String jsonArticulo = gson.toJson(articDTO);
                //implementar el id con el mismo IVA
                JSONObject newArt = (JSONObject) parser.parse(jsonArticulo);
                listJSON.get(i).remove("articulo");
                listJSON.get(i).put("articulo", newArt);

                org.json.JSONObject json = new org.json.JSONObject(newArt);
                if (!json.isNull("precioMin")) {
                    precio += Long.parseLong(newArt.get("precioMin").toString()) * Long.parseLong(listJSON.get(i).get("cantidad").toString());
                }
            }
            NumberValidator numValidator = new NumberValidator();
            labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(precio + "")));
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
            listJSON = new ArrayList<>();
        }
        if (!listJSON.isEmpty()) {

            srvData = FXCollections.observableArrayList(listJSON);
            //columna Codigo ..................................................
            tableColumnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                        org.json.JSONObject json = new org.json.JSONObject(jsonArt);
                        if (!json.isNull("codArticulo")) {
                            String codArt = jsonArt.get("codArticulo").toString();
                            return new ReadOnlyStringWrapper(codArt);
                        } else {
                            return null;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return null;
                    }
                }
            });
            tableColumnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    try {
                        return new ReadOnlyStringWrapper(data.getValue().get("cantidad").toString());
                    } catch (Exception ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return new ReadOnlyStringWrapper("0");
                    }
                }
            });

            //columna Descripción ..................................................
            tableColumnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                        org.json.JSONObject json = new org.json.JSONObject(jsonArt);
                        if (!json.isNull("descripcion")) {
                            String descri = jsonArt.get("descripcion").toString().replace("C.E. ", "");
                            return new ReadOnlyStringWrapper(descri);
                        } else {
                            return null;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return null;
                    }
                }
            });

            //columna IVA .................................................
            tableColumnIva.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                        JSONObject jsonIva = (JSONObject) parser.parse(jsonArt.get("iva").toString());
                        org.json.JSONObject json = new org.json.JSONObject(jsonIva);
                        if (!json.isNull("poriva")) {
                            String poriva = jsonIva.get("poriva").toString();
                            return new ReadOnlyStringWrapper(poriva);
                        } else {
                            return null;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return null;
                    }
                }
            });
            //columna precio .................................................
            tableColumnPrecio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnPrecio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                        org.json.JSONObject json = new org.json.JSONObject(jsonArt);
                        if (!json.isNull("precioMin")) {
                            String precioMin = jsonArt.get("precioMin").toString();
                            NumberValidator numValidator = new NumberValidator();
                            numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(precioMin));
                            return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(precioMin)));
//                            return new ReadOnlyStringWrapper(precioMin);
                        } else {
                            return null;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return null;
                    }
                }
            });
            //columna estilista .................................................
            tableColumnEstilista.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnEstilista.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
                @Override
                public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                    JSONParser parser = new JSONParser();
                    try {
                        JSONObject jsonFunc = (JSONObject) parser.parse(data.getValue().get("funcionario").toString());
                        org.json.JSONObject json = new org.json.JSONObject(jsonFunc);
                        if (!json.isNull("idFuncionario")) {
                            long idFunc = json.getLong("idFuncionario");
                            Funcionario func = funcDAO.getById(idFunc);
                            return new ReadOnlyStringWrapper(func.getNombre() + " " + func.getApellido());
                        } else {
                            return null;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        return null;
                    }
                }
            });
            //columna eliminar .................................................
            tableColumnEliminar.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
            tableColumnEliminar.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
                @Override
                public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                    return new CargarFacturaEsteticaFXMLController.AddObjectsCell(tableViewServicios);
                }
            });
            tableViewServicios.setItems(srvData);
        } else {
            tableViewServicios.setItems(null);
        }
    }

    @FXML
    private void btnActualizarAction(ActionEvent event) {

        JSONParser parser = new JSONParser();

        int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
        if (index >= 0) {
            indexSeleccionado = index;
            String seleccionado = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedItem();
            cargarClientePendiente();
            comboBoxCargarFacturaEstetica.getSelectionModel().select(seleccionado);
            JSONObject jsonCliPen = cliPenList.get(index);

//            NEW
            ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(jsonCliPen.get("idClientePendiente").toString())).toEliminarPendienteDTO();
            cpDTO.setFechaAlta(null);
            cpDTO.setFechaMod(null);
            try {
                JSONArray array = new JSONArray();
                for (int i = 0; i < cpDTO.getServPendiente().size(); i++) {
                    JSONObject obj = new JSONObject();
                    obj.put("idServPendiente", cpDTO.getServPendiente().get(i).getIdServPendiente());
                    array.add(obj);
                }
                jsonCliPen.put("servPendiente", array);

                jsonCliPen = (JSONObject) parser.parse(gson.toJson(cpDTO).toString());

            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }
//                    FINAL NEW
            actualizandoTablaServiciosPendientes(jsonCliPen);
        } else {
            cargarClientePendiente();
        }
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

//        Button cancelarButton = new Button("cancelar");
        Button cancelarButton = new Button("X");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    JSONParser parser = new JSONParser();

                    table.getSelectionModel().select(getTableRow().getIndex());
                    int posicion = tableViewServicios.getSelectionModel().getSelectedIndex();
                    if (posicion >= 0) {
                        JSONObject servicios = tableViewServicios.getItems().get(posicion);
                        JSONObject art = null;
                        JSONObject func = null;
                        String nombreFunc = "";
                        try {
                            art = (JSONObject) parser.parse(servicios.get("articulo").toString());
                            func = (JSONObject) parser.parse(servicios.get("funcionario").toString());
                            Funcionario funci = funcDAO.getById(Long.parseLong(func.get("idFuncionario").toString()));
                            nombreFunc = funci.getNombre() + " " + funci.getApellido();
                        } catch (ParseException ex) {
                            Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        }

                        int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
                        JSONObject jsonCP;
                        boolean valor = false;
                        try {
                            jsonCP = (JSONObject) parser.parse(cliPenList.get(index).toString());
                            ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(jsonCP.get("idClientePendiente").toString())).toEliminarPendienteDTO();
                            cpDTO.setFechaAlta(null);
                            cpDTO.setFechaMod(null);
                            jsonCP = (JSONObject) parser.parse(gson.toJson(cpDTO).toString());

                            JSONArray jsonSrv = (JSONArray) parser.parse(jsonCP.get("servPendiente").toString());
                            for (int i = 0; i < jsonSrv.size(); i++) {
                                JSONObject jsonServicio = (JSONObject) parser.parse(jsonSrv.get(i).toString());
                                JSONObject jsonArt = (JSONObject) parser.parse(jsonServicio.get("articulo").toString());

                                long idArticuloSeleccionado = Long.parseLong(art.get("idArticulo").toString());
                                long idArt = Long.parseLong(jsonArt.get("idArticulo").toString());
                                if (idArticuloSeleccionado == idArt) {
                                    valor = true;
                                    break;
                                }
                            }
                        } catch (ParseException ex) {
                            Utilidades.log.error("ParseException", ex.fillInStackTrace());
                        }
                        if (valor) {
                            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                            Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR EL SERVICIO "
                                    + art.get("descripcion").toString() + " REALIZADO POR " + nombreFunc + "?", ok, cancel);
                            alert = true;
                            alert2.showAndWait();
                            if (alert2.getResult() == ok) {
                                spDAO.eliminar(Long.parseLong(servicios.get("idServPendiente").toString()));
                                tableViewServicios.getItems().remove(posicion);
                                JSONObject jsonCliPen = cliPenList.get(indexSeleccionado);
                                try {
                                    JSONArray arraySrvPend = (JSONArray) parser.parse(jsonCliPen.get("servPendiente").toString());
                                    JSONArray newJsonArray = new JSONArray();
                                    for (int i = 0; i < arraySrvPend.size(); i++) {
                                        JSONObject json = (JSONObject) parser.parse(arraySrvPend.get(i).toString());
                                        long idDetalle = Long.parseLong(json.get("idServPendiente").toString());
                                        if (idDetalle != Long.parseLong(servicios.get("idServPendiente").toString())) {
                                            newJsonArray.add(json);
                                        }
                                    }
                                    jsonCliPen.put("servPendiente", newJsonArray);
                                    cliPenList.remove(indexSeleccionado);
                                    cliPenList.add(indexSeleccionado, jsonCliPen);
                                } catch (ParseException ex) {
                                    Logger.getLogger(CargarFacturaEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                                }
                            } else if (alert2.getResult() == cancel) {
                                escape = true;
                                alert2.close();
                            }
                        } else {
                            mensajeError("El Servicio ya ha sido eliminado desde el módulo táctil");
                            tableViewServicios.getItems().remove(posicion);
                        }
                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }

    //OTROS METODOS UTILIZADOS
    private void procesar() throws ParseException {
        JSONParser parser = new JSONParser();
        int index = comboBoxCargarFacturaEstetica.getSelectionModel().getSelectedIndex();
        if (index >= 0) {
            JSONObject jsonCliPen = cliPenList.get(index);
            JSONArray arraySrvPend = (JSONArray) parser.parse(jsonCliPen.get("servPendiente").toString());
            if (!arraySrvPend.isEmpty()) {
                cargarFacturaCliente(index);
            } else {
                mensajeError("El cliente seleccionado aún no ha recibido ningún servicio");
            }
        } else {
            mensajeError("No haz seleccionado ningún Cliente");
        }
    }

    private void cargarFacturaCliente(int posicionSeleccionada) {
        if (mensajeConfirmacion("Seguro que quiere cargar para factura al cliente seleccionado")) {
            JSONObject jsonCliPen = cliPenList.get(posicionSeleccionada);
            long idCliPen = Long.parseLong(jsonCliPen.get("idClientePendiente").toString());

            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            JSONObject usuarioCajero = Identity.getUsuario();
            JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
            String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
            boolean estado = cpDAO.actualizarEstado(idCliPen, timestamp, nombreCaj);
            if (estado) {
//                mensajeInfo("Los servicios han sido pasados al módulo de facturación");
                enviandoDatosServidor(idCliPen);
                datos.put("idClientePendiente", idCliPen);
                datos.put("idCliPend", idCliPen);
                DatosEnCaja.setDatos(datos);
                this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/CargarFacturaEsteticaFXML.fxml", 837, 468, true);
            } else {
                mensajeError("No se ha podido eliminar al cliente seleccionado");
            }
        }
    }

    private void enviandoDatosServidor(long idCliPen) {
        JSONParser parser = new JSONParser();
        ClientePendienteDTO cliPenDTO = cpDAO.getById(idCliPen).toEliminarPendienteDTO();
        String jsonCliPen = gson2.toJson(cliPenDTO);
        try {
            JSONObject cliPen = (JSONObject) parser.parse(jsonCliPen);
            JSONArray servPendiente = (JSONArray) parser.parse(cliPen.get("servPendiente").toString());
            insertarClientePendientes(cliPen);
            if (!servPendiente.isEmpty()) {
                insertarServPendientes(servPendiente, Long.parseLong(cliPen.get("idClientePendiente").toString()));
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

    private void cargarClientePendiente() {
        try {
            jsonCargandoClientePendiente();
//            comboBoxCargarFacturaEstetica.getSelectionModel().select(posicionCliPen);
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

}
