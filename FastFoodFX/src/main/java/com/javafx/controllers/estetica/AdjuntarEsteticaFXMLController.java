/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class AdjuntarEsteticaFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    @FXML
    private AnchorPane anchorPaneCentroEstetica;
    @FXML
    private Button buttonCargarClienteServicio;
    @FXML
    private Button buttonCargarFactura;
    @FXML
    private Button buttonVolver;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    @FXML
    private void buttonCargarClienteServicioAction(ActionEvent event) {
        cargarClienteServicio();
    }

    @FXML
    private void buttonCargarFacturaAction(ActionEvent event) {
        cargarFactura();
    }

    @FXML
    private void anchorPaneCentroEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        saliendo();
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F10) {
            cargarClienteServicio();
        }
        if (keyCode == event.getCode().F11) {
            cargarFactura();
        }
        if (keyCode == event.getCode().ESCAPE) {
            saliendo();
        }
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void saliendo() {
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/AdjuntarEsteticaFXML.fxml", 418, 224, true);
    }

    private void cargarClienteServicio() {
//        if ("cargar_cliente_servicio")) {
        this.sc.loadScreen("/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, "/vista/estetica/AdjuntarEsteticaFXML.fxml", 418, 224, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/AdjuntarEsteticaFXML.fxml", 418, 224, true);
//        }
    }

    private void cargarFactura() {
//        if ("cargar_factura_servicio")) {
        this.sc.loadScreen("/vista/estetica/CargarFacturaEsteticaFXML.fxml", 837, 468, "/vista/estetica/AdjuntarEsteticaFXML.fxml", 418, 224, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/AdjuntarEsteticaFXML.fxml", 418, 224, true);
//        }
    }

}
