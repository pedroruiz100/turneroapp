/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.springframework.stereotype.Controller;
/**
 * FXML Controller class
 */
@Controller
@ScreenScoped
public class MenuCentroEsteticaFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    @FXML
    private AnchorPane anchorPaneMenuCentroEstetica;
    @FXML
    private Button buttonCargarClienteServicio;
    @FXML
    private Button buttonCargarFactura;
    @FXML
    private ImageView imageViewLogo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void buttonCargarClienteServicioAction(ActionEvent event) {
    }

    @FXML
    private void buttonCargarFacturaAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneMenuCentroEsteticaKeyReleased(KeyEvent event) {
    }
    
}
