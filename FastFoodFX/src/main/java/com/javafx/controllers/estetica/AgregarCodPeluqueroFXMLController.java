/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dao.UsuarioDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.UsuarioDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class AgregarCodPeluqueroFXMLController extends BaseScreenController implements Initializable {

    static void enviarDatos(JSONObject jsonArticulo) {
        jsonArt = jsonArticulo;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private NumberValidator numVal;
    public static JSONObject jsonArt;
    static String cantidad = "";
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean alertEscEnter;
    @Autowired
    private UsuarioDAO usuDAO;
//    boolean crearNuevo;

    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    private List<JSONObject> clienteFielList;
    private static JSONObject jsonClienteFiel;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente = new JSONObject();
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private JSONObject clienteFiel;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private TextField textFieldCodPeluquero;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        repeatFocusData(textFieldCodPeluquero);
        textFieldCodPeluquero.setText("");
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void navegandoFacturaVenta() {

    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreenModal("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/ModCantidadMayFXML.fxml", 232, 95, false);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
//            if (!textFieldCodPeluquero.getText().equalsIgnoreCase("")) {
            if (StageSecond.getStageData().isShowing()) {
                StageSecond.getStageData().close();
            }
            validandoIngreso();
//                this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/AgregarCodPeluqueroFXML.fxml", 270, 78, true);
//            } else {
//                mensajeAlerta("CANTIDAD NO DEBE QUEDAR VACIO");
//            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }

    private void validandoIngreso() {
//        Identity identityLocal = new Identity();
//        identityLocal.usuarioLogueado(null);
        JSONObject objJSON = loginLocal(textFieldCodPeluquero.getText());
        if (!textFieldCodPeluquero.getText().contentEquals("") && objJSON != null) {
//            identityLocal.usuarioLogueado(objJSON);
            FacturaDeVentaEsteticaFXMLController.asignandoServ(jsonArt, objJSON);
        } else {
//            mensajeAlerta("No se encuentra ningún peluquero/a con la CI ingresada...");
            FacturaDeVentaEsteticaFXMLController.asignandoServ(jsonArt, null);
        }
    }

    public JSONObject loginLocal(String CI) {
        JSONParser parser = new JSONParser();
        try {
            Usuario usu = usuDAO.busUsuCI(CI);
            UsuarioDTO usuDTO = usu.toUsuarioDTO();
            JSONObject jsonDTO = (JSONObject) parser.parse(gson.toJson(usuDTO));
            return jsonDTO;
        } catch (Exception ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return null;
        }
    }

    private void mensajeAlerta(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoClienteFiel() {

    }

    private void nuevoClienteFiel() {

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
