package com.javafx.controllers.estetica;

import com.javafx.controllers.caja.*;
import com.peluqueria.core.domain.ArqueoCaja;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.NumberValidator;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ArqueoCajaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoTesoreriaDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.impl.RangoTesoreriaDAOImpl;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.login.LoginFXMLController;
import com.javafx.util.Ticket;
import com.javafx.util.Toaster;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabEfectivo;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.core.domain.FacturaClienteCabNotaCredito;
import com.peluqueria.core.domain.FacturaClienteCabTarjeta;
import com.peluqueria.dao.AperturaCajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabEfectivoDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteCabNotaCreditoDAO;
import com.peluqueria.dao.FacturaClienteCabTarjetaDAO;
import com.sun.javafx.robot.FXRobot;
import com.sun.javafx.robot.FXRobotFactory;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.AnchorPane;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
@ScreenScoped
public class ArqueoCajaWebFXMLController extends BaseScreenController implements Initializable {

    private boolean alert;
    private boolean alertCierre;
    private JSONObject supervisor;
    private boolean montoRecaudado;
    private boolean arqueo = false;

    @Autowired
    private ArqueoCajaDAO arqueoDAO;

    @Autowired
    private FacturaClienteCabDAO facturaCabDAO;

    @Autowired
    private FacturaClienteCabHistoricoDAO facturaCabHistoricoDAO;

    @Autowired
    private FacturaClienteCabEfectivoDAO facturaCabEfectivoDAO;
    @Autowired
    private FacturaClienteCabTarjetaDAO facturaCabTarjetaDAO;
    @Autowired
    private FacturaClienteCabNotaCreditoDAO facturaCabNotaCredDAO;

    //campo númerico
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    String param;
    //campos númericos
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

//    @Autowiredtesoreria_pendientes
    private RangoTesoreriaDAO rangoTesoreriaDAO = new RangoTesoreriaDAOImpl();

    @Autowired
    private SupervisorDAO superDAO;

    @Autowired
    AperturaCajaDAO aperturaCajaDAO;
    @Autowired
    ArqueoCajaDAO arqueoCajaDAO;

    private Date date;
    private Timestamp timestamp;

    Task copyWorker;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private TextField txtTarjetaSupervisor;
    @FXML
    private TextField txtMontoEfectivo;
    @FXML
    private TextField txtCantSencillo;
    @FXML
    private TextField txtMontoSencillo;
    @FXML
    private TextField txtMontoAsociaciones;
    @FXML
    private TextField txtCantAsociaciones;
    @FXML
    private TextField txtCantFactory;
    @FXML
    private TextField txtMontoFactory;
    @FXML
    private TextField txtMontoChequeAlDia;
    @FXML
    private TextField txtCantChequeAlDia;
    @FXML
    private TextField txtCantChequeAdelantado;
    @FXML
    private TextField txtMontoChequeAdelantado;
    @FXML
    private Label lblEfectivo;
    @FXML
    private Label lblSencillo;
    @FXML
    private Label lblFactory;
    @FXML
    private Label lblAsociaciones;
    @FXML
    private Label lblChequeAlDia;
    @FXML
    private Label lblChequeAdelantado;
    @FXML
    private Label lblVales;
    @FXML
    private TextField txtMontoVale;
    @FXML
    private TextField txtCantVale;
    @FXML
    private Label lblFcred;
    @FXML
    private TextField txtMontoFcred;
    @FXML
    private TextField txtCantFCred;
    @FXML
    private Label lblFco;
    @FXML
    private TextField txtMontoFco;
    @FXML
    private TextField txtCantFCo;
    @FXML
    private Label lblFcoCanjes;
    @FXML
    private TextField txtMontoFcoCanjes;
    @FXML
    private TextField txtCantFcoCanjes;
    @FXML
    private TextField txtCantFcredCanjes;
    @FXML
    private TextField txtMontoFcredCanjes;
    @FXML
    private Label lblFcredCanjes;
    @FXML
    private Label lblReal;
    @FXML
    private Label lblPeso;
    @FXML
    private Label lblDolar;
    @FXML
    private TextField txtCantReal;
    @FXML
    private TextField txtMontoReal;
    @FXML
    private TextField txtMontoPeso;
    @FXML
    private TextField txtCantPeso;
    @FXML
    private TextField txtMontoDolar;
    @FXML
    private TextField txtCantDolar;
    @FXML
    private TextField txtCantNotaCredito;
    @FXML
    private TextField txtMontoNotaCredito;
    @FXML
    private TextField txtCantGiftCard;
    @FXML
    private TextField txtMontoGiftCard;
    @FXML
    private Label lblNotaCredito;
    @FXML
    private Label lblGiftCard;
    @FXML
    private TextField txtCantInfonet;
    @FXML
    private TextField txtMontoInfonet;
    @FXML
    private TextField txtCantPronet;
    @FXML
    private TextField txtMontoPronet;
    @FXML
    private TextField txtMontoPractipago;
    @FXML
    private Label lblInfonet;
    @FXML
    private Label lblPronet;
    @FXML
    private Label lblPractipago;
    @FXML
    private TextField txtCantPractipago;
    @FXML
    private Pane PanelSupervisor;
    @FXML
    private Pane Panel1;
    @FXML
    private Pane Panel2;
    @FXML
    private Pane Panel3;
    @FXML
    private Pane Panel4;
    @FXML
    private Pane Panel5;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button btnSalir;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private AnchorPane anchorPaneFront;
    @FXML
    private Label labelProgress;
    @FXML
    private ToolBar toolBar01;
    @FXML
    private ToolBar toolBar02;
    @FXML
    private ToolBar toolBar03;
    @FXML
    private ToolBar toolBar04;
    @FXML
    private ToolBar toolBar05;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneArqueoCajaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void txtMontoEfectivoKeyPress(KeyEvent event) {
        listenMontoEfectivo(event);
    }

    @FXML
    private void txtCantSencilloKeyPress(KeyEvent event) {
        listenCantSencillo(event);
    }

    @FXML
    private void txtMontoSencilloKeyPress(KeyEvent event) {
        listenMontoSencillo(event);
    }

    @FXML
    private void txtCantAsociacionesKeyPress(KeyEvent event) {
        listenCantAsociaciones(event);
    }

    @FXML
    private void txtMontoAsociacionesKeyPress(KeyEvent event) {
        listenMontoAsociaciones(event);
    }

    @FXML
    private void txtCantFactoryKeyPress(KeyEvent event) {
        listenCantFactory(event);
    }

    @FXML
    private void txtMontoFactoryKeyPress(KeyEvent event) {
        listenMontoFactory(event);
    }

    @FXML
    private void txtCantChequeAlDiaKeyPress(KeyEvent event) {
        listenCantChequeAlDia(event);
    }

    @FXML
    private void txtMontoChequeAlDiaKeyPress(KeyEvent event) {
        listenMontoChequeAlDia(event);
    }

    @FXML
    private void txtCantChequeAdelantadoKeyPress(KeyEvent event) {
        listenCantChequeAdelantado(event);
    }

    @FXML
    private void txtMontoChequeAdelantadoKeyPress(KeyEvent event) {
        listenMontoChequeAdelantado(event);
    }

    @FXML
    private void txtCantValeKeyPress(KeyEvent event) {
        listenCantVale(event);
    }

    @FXML
    private void txtMontoValeKeyPress(KeyEvent event) {
        listenMontoVale(event);
    }

    @FXML
    private void txtCantFCredKeyPress(KeyEvent event) {
        listenCantFcred(event);
    }

    @FXML
    private void txtMontoFcredKeyPress(KeyEvent event) {
        listenMontoFcred(event);
    }

    @FXML
    private void txtCantFCoKeyPress(KeyEvent event) {
        listenCantFco(event);
    }

    @FXML
    private void txtMontoFcoKeyPress(KeyEvent event) {
        listenMontoFco(event);
    }

    @FXML
    private void txtCantFcoCanjesKeyPress(KeyEvent event) {
        listenCantFcoCanjes(event);
    }

    @FXML
    private void txtMontoFcoCanjesKeyPress(KeyEvent event) {
        listenMontoFcoCanjes(event);
    }

    @FXML
    private void txtCantFcredCanjesKeyPress(KeyEvent event) {
        listenCantFcredCanjes(event);
    }

    @FXML
    private void txtMontoFcredCanjesKeyPress(KeyEvent event) {
        listenMontoFcredCanjes(event);
    }

    @FXML
    private void txtCantDolarKeyPress(KeyEvent event) {
        listenCantDolar(event);
    }

    @FXML
    private void txtMontoDolarKeyPress(KeyEvent event) {
        listenMontoDolar(event);
    }

    @FXML
    private void txtCantPesoKeyPress(KeyEvent event) {
        listenCantPeso(event);
    }

    @FXML
    private void txtMontoPesoKeyPress(KeyEvent event) {
        listenMontoPeso(event);
    }

    @FXML
    private void txtCantRealKeyPress(KeyEvent event) {
        listenCantReal(event);
    }

    @FXML
    private void txtMontoRealKeyPress(KeyEvent event) {
        listenMontoReal(event);
    }

    @FXML
    private void txtCantNotaCreditoKeyPress(KeyEvent event) {
        listenCantNotaCredito(event);
    }

    @FXML
    private void txtMontoNotaCreditoKeyPress(KeyEvent event) {
        listenMontoNotaCredito(event);
    }

    @FXML
    private void txtCantGiftCardKeyPress(KeyEvent event) {
        listenCantGiftCard(event);
    }

    @FXML
    private void txtMontoGiftCardKeyPress(KeyEvent event) {
        listenMontoGiftCard(event);
    }

    @FXML
    private void txtCantInfonetKeyPress(KeyEvent event) {
        listenCantInfonet(event);
    }

    @FXML
    private void txtMontoInfonetKeyPress(KeyEvent event) {
        listenMontoInfonet(event);
    }

    @FXML
    private void txtCantPronetKeyPress(KeyEvent event) {
        listenCantPronet(event);
    }

    @FXML
    private void txtMontoPronetKeyPress(KeyEvent event) {
        listenMontoPronet(event);
    }

    @FXML
    private void txtCantPractipagoKeyPress(KeyEvent event) {
        listenCantPractiPago(event);
    }

    @FXML
    private void txtMontoPractipagoKeyPress(KeyEvent event) {
        listenMontoPractiPago(event);
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
//        copyWorker = createWorker();
//        progressIndicator.progressProperty().unbind();
//        progressIndicator.progressProperty().bind(copyWorker.progressProperty());
//        new Thread(copyWorker).start();
        if (btnProcesar.isVisible()) {
            procesarArqueo();
            copyWorker = createWorker();
            progressIndicator.progressProperty().unbind();
            progressIndicator.progressProperty().bind(copyWorker.progressProperty());
            new Thread(copyWorker).start();
        }
        LoginFXMLController.setLlamarTask(false);
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        salirArqueo();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        ocultandoPaneles();
        ocultandoProgress();
        asignaciones();
        ocultandoLabels();
        btnProcesar.setVisible(false);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void salirArqueo() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA SALIR DE ARQUEO CAJA?".toUpperCase(), ok, cancel);
        alert2.showAndWait();
        alert = false;
        if (alert2.getResult() == ok) {
            alert2.close();
            LoginFXMLController.setLlamarTask(false);
            this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/caja/ArqueoCajaFXML.fxml", 999, 746, true);
        } else {
            alert2.close();
            this.alertCierre = true;
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenMontoEfectivo(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoEfectivo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoEfectivo);
                        txtMontoEfectivo.setText(dato);
                        txtMontoEfectivo.positionCaret(txtMontoEfectivo.getLength());
                    } catch (Exception e) {
                        txtMontoEfectivo.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoEfectivo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoEfectivo)) {
                        txtMontoEfectivo.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenMontoSencillo(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoSencillo);
                        txtMontoSencillo.setText(dato);
                        txtMontoSencillo.positionCaret(txtMontoSencillo.getLength());
                    } catch (Exception e) {
                        txtMontoSencillo.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoSencillo)) {
                        txtMontoSencillo.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantSencillo(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantSencillo);
                        txtCantSencillo.setText(dato);
                        txtCantSencillo.positionCaret(txtCantSencillo.getLength());
                    } catch (Exception e) {
                        txtCantSencillo.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantSencillo)) {
                        txtCantSencillo.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoAsociaciones(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoAsociaciones.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoAsociaciones);
                        txtMontoAsociaciones.setText(dato);
                        txtMontoAsociaciones.positionCaret(txtMontoAsociaciones.getLength());
                    } catch (Exception e) {
                        txtMontoAsociaciones.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoAsociaciones.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoAsociaciones)) {
                        txtMontoAsociaciones.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantAsociaciones(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantAsociaciones.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantAsociaciones);
                        txtCantAsociaciones.setText(dato);
                        txtCantAsociaciones.positionCaret(txtCantAsociaciones.getLength());
                    } catch (Exception e) {
                        txtCantAsociaciones.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantAsociaciones.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantAsociaciones)) {
                        txtCantAsociaciones.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoFactory(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoFactory.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoFactory);
                        txtMontoFactory.setText(dato);
                        txtMontoFactory.positionCaret(txtMontoFactory.getLength());
                    } catch (Exception e) {
                        txtMontoFactory.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoFactory.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoFactory)) {
                        txtMontoFactory.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantFactory(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantFactory.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantFactory);
                        txtCantFactory.setText(dato);
                        txtCantFactory.positionCaret(txtCantFactory.getLength());
                    } catch (Exception e) {
                        txtCantFactory.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantFactory.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantFactory)) {
                        txtCantFactory.setText("");
                    }
                });
            });
        }
    }

    private void listenCantChequeAlDia(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantChequeAlDia.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantChequeAlDia);
                        txtCantChequeAlDia.setText(dato);
                        txtCantChequeAlDia.positionCaret(txtCantChequeAlDia.getLength());
                    } catch (Exception e) {
                        txtCantChequeAlDia.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantChequeAlDia.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantChequeAlDia)) {
                        txtCantChequeAlDia.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoChequeAlDia(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoChequeAlDia.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoChequeAlDia);
                        txtMontoChequeAlDia.setText(dato);
                        txtMontoChequeAlDia.positionCaret(txtMontoChequeAlDia.getLength());
                    } catch (Exception e) {
                        txtMontoChequeAlDia.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoChequeAlDia.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoChequeAlDia)) {
                        txtMontoChequeAlDia.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenMontoChequeAdelantado(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoChequeAdelantado.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoChequeAdelantado);
                        txtMontoChequeAdelantado.setText(dato);
                        txtMontoChequeAdelantado.positionCaret(txtMontoChequeAdelantado.getLength());
                    } catch (Exception e) {
                        txtMontoChequeAdelantado.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoChequeAdelantado.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoChequeAdelantado)) {
                        txtMontoChequeAdelantado.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantChequeAdelantado(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantChequeAdelantado.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantChequeAdelantado);
                        txtCantChequeAdelantado.setText(dato);
                        txtCantChequeAdelantado.positionCaret(txtCantChequeAdelantado.getLength());
                    } catch (Exception e) {
                        txtCantChequeAdelantado.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantChequeAdelantado.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantChequeAdelantado)) {
                        txtCantChequeAdelantado.setText("");
                    }
                });
            });
        }
    }

    private void listenCantVale(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantVale.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantVale);
                        txtCantVale.setText(dato);
                        txtCantVale.positionCaret(txtCantVale.getLength());
                    } catch (Exception e) {
                        txtCantVale.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantVale.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantVale)) {
                        txtCantVale.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoVale(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoVale.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoVale);
                        txtMontoVale.setText(dato);
                        txtMontoVale.positionCaret(txtMontoVale.getLength());
                    } catch (Exception e) {
                        txtMontoVale.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoVale.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoVale)) {
                        txtMontoVale.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenMontoFcred(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoFcred.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoFcred);
                        txtMontoFcred.setText(dato);
                        txtMontoFcred.positionCaret(txtMontoFcred.getLength());
                    } catch (Exception e) {
                        txtMontoFcred.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoFcred.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoFcred)) {
                        txtMontoFcred.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantFcred(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantFCred.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantFCred);
                        txtCantFCred.setText(dato);
                        txtCantFCred.positionCaret(txtCantFCred.getLength());
                    } catch (Exception e) {
                        txtCantFCred.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantFCred.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantFCred)) {
                        txtCantFCred.setText("");
                    }
                });
            });
        }
    }

    private void listenCantFco(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantFCo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantFCo);
                        txtCantFCo.setText(dato);
                        txtCantFCo.positionCaret(txtCantFCo.getLength());
                    } catch (Exception e) {
                        txtCantFCo.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantFCo.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantFCo)) {
                        txtCantFCo.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoFco(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoFco.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoFco);
                        txtMontoFco.setText(dato);
                        txtMontoFco.positionCaret(txtMontoFco.getLength());
                    } catch (Exception e) {
                        txtMontoFco.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoFco.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoFco)) {
                        txtMontoFco.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantFcoCanjes(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantFcoCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantFcoCanjes);
                        txtCantFcoCanjes.setText(dato);
                        txtCantFcoCanjes.positionCaret(txtCantFcoCanjes.getLength());
                    } catch (Exception e) {
                        txtCantFcoCanjes.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantFcoCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantFcoCanjes)) {
                        txtCantFcoCanjes.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoFcoCanjes(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoFcoCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoFcoCanjes);
                        txtMontoFcoCanjes.setText(dato);
                        txtMontoFcoCanjes.positionCaret(txtMontoFcoCanjes.getLength());
                    } catch (Exception e) {
                        txtMontoFcoCanjes.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoFcoCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoFcoCanjes)) {
                        txtMontoFcoCanjes.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantFcredCanjes(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantFcredCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantFcredCanjes);
                        txtCantFcredCanjes.setText(dato);
                        txtCantFcredCanjes.positionCaret(txtCantFcredCanjes.getLength());
                    } catch (Exception e) {
                        txtCantFcredCanjes.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantFcredCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantFcredCanjes)) {
                        txtCantFcredCanjes.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoFcredCanjes(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoFcredCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoFcredCanjes);
                        txtMontoFcredCanjes.setText(dato);
                        txtMontoFcredCanjes.positionCaret(txtMontoFcredCanjes.getLength());
                    } catch (Exception e) {
                        txtMontoFcredCanjes.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoFcredCanjes.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoFcredCanjes)) {
                        txtMontoFcredCanjes.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantPeso(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantPeso.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantPeso);
                        txtCantPeso.setText(dato);
                        txtCantPeso.positionCaret(txtCantPeso.getLength());
                    } catch (Exception e) {
                        txtCantPeso.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantPeso.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantPeso)) {
                        txtCantPeso.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoPeso(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoPeso.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoPeso);
                        txtMontoPeso.setText(dato);
                        txtMontoPeso.positionCaret(txtMontoPeso.getLength());
                    } catch (Exception e) {
                        txtMontoPeso.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoPeso.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoPeso)) {
                        txtMontoPeso.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantDolar(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantDolar.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantDolar);
                        txtCantDolar.setText(dato);
                        txtCantDolar.positionCaret(txtCantDolar.getLength());
                    } catch (Exception e) {
                        txtCantDolar.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantDolar.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantDolar)) {
                        txtCantDolar.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoDolar(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoDolar.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoDolar);
                        txtMontoDolar.setText(dato);
                        txtMontoDolar.positionCaret(txtMontoDolar.getLength());
                    } catch (Exception e) {
                        txtMontoDolar.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoDolar.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoDolar)) {
                        txtMontoDolar.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantReal(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantReal.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantReal);
                        txtCantReal.setText(dato);
                        txtCantReal.positionCaret(txtCantReal.getLength());
                    } catch (Exception e) {
                        txtCantReal.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantReal.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantReal)) {
                        txtCantReal.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoReal(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoReal.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoReal);
                        txtMontoReal.setText(dato);
                        txtMontoReal.positionCaret(txtMontoReal.getLength());
                    } catch (Exception e) {
                        txtMontoReal.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoReal.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoReal)) {
                        txtMontoReal.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenMontoNotaCredito(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoNotaCredito.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoNotaCredito);
                        txtMontoNotaCredito.setText(dato);
                        txtMontoNotaCredito.positionCaret(txtMontoNotaCredito.getLength());
                    } catch (Exception e) {
                        txtMontoNotaCredito.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoNotaCredito.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoNotaCredito)) {
                        txtMontoNotaCredito.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantNotaCredito(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantNotaCredito.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantNotaCredito);
                        txtCantNotaCredito.setText(dato);
                        txtCantNotaCredito.positionCaret(txtCantNotaCredito.getLength());
                    } catch (Exception e) {
                        txtCantNotaCredito.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantNotaCredito.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantNotaCredito)) {
                        txtCantNotaCredito.setText("");
                    }
                });
            });
        }
    }

    private void listenCantGiftCard(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantGiftCard.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantGiftCard);
                        txtCantGiftCard.setText(dato);
                        txtCantGiftCard.positionCaret(txtCantGiftCard.getLength());
                    } catch (Exception e) {
                        txtCantGiftCard.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantGiftCard.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantGiftCard)) {
                        txtCantGiftCard.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoGiftCard(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoGiftCard.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoGiftCard);
                        txtMontoGiftCard.setText(dato);
                        txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                    } catch (Exception e) {
                        txtMontoGiftCard.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoGiftCard.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoGiftCard)) {
                        txtMontoGiftCard.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenMontoPronet(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoPronet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoPronet);
                        txtMontoPronet.setText(dato);
                        txtMontoPronet.positionCaret(txtMontoPronet.getLength());
                    } catch (Exception e) {
                        txtMontoPronet.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoPronet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoPronet)) {
                        txtMontoPronet.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantPronet(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantPronet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantPronet);
                        txtCantPronet.setText(dato);
                        txtCantPronet.positionCaret(txtCantPronet.getLength());
                    } catch (Exception e) {
                        txtCantPronet.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantPronet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantPronet)) {
                        txtCantPronet.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoPractiPago(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoPractipago.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoPractipago);
                        txtMontoPractipago.setText(dato);
                        txtMontoPractipago.positionCaret(txtMontoPractipago.getLength());
                    } catch (Exception e) {
                        txtMontoPractipago.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoPractipago.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoPractipago)) {
                        txtMontoPractipago.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantPractiPago(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantPractipago.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantPractipago);
                        txtCantPractipago.setText(dato);
                        txtCantPractipago.positionCaret(txtCantPractipago.getLength());
                    } catch (Exception e) {
                        txtCantPractipago.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantPractipago.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantPractipago)) {
                        txtCantPractipago.setText("");
                    }
                });
            });
        }
    }

    private void listenMontoInfonet(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtMontoInfonet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariables(txtMontoInfonet);
                        txtMontoInfonet.setText(dato);
                        txtMontoInfonet.positionCaret(txtMontoInfonet.getLength());
                    } catch (Exception e) {
                        txtMontoInfonet.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtMontoInfonet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEntero(txtMontoInfonet)) {
                        txtMontoInfonet.setText("Gs 0");
                    }
                });
            });
        }
    }

    private void listenCantInfonet(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantInfonet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    try {
                        String dato = asignandoVariablesCantidad(txtCantInfonet);
                        txtCantInfonet.setText(dato);
                        txtCantInfonet.positionCaret(txtCantInfonet.getLength());
                    } catch (Exception e) {
                        txtCantInfonet.setText("0");
                    }
                });
            });
        } else if (event.getCode().isLetterKey()) {
            txtCantInfonet.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    if (!convertirEnteroCantidad(txtCantInfonet)) {
                        txtCantInfonet.setText("");
                    }
                });
            });
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            if (btnProcesar.isVisible()) {
                procesarArqueo();
                copyWorker = createWorker();
                progressIndicator.progressProperty().unbind();
                progressIndicator.progressProperty().bind(copyWorker.progressProperty());
                new Thread(copyWorker).start();
            }
            LoginFXMLController.setLlamarTask(false);
            alertCierre = true;
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (this.alertCierre) {
                alertCierre = false;
            } else {
                salirArqueo();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (!txtTarjetaSupervisor.isFocused()) {
                FXRobot robot = FXRobotFactory.createRobot(this.getRoot().getScene());
                robot.keyPress(javafx.scene.input.KeyCode.TAB);
            } else {
                if (alert) {
                    boolean arqueo = false;
                    if (aperturaCajaDAO.verificarAperturaDia()) {
                        if (!arqueoCajaDAO.verificarArqueoDia()) {
                            arqueo = true;
                        } else {
                            long cantApertura = aperturaCajaDAO.getCantidadAperturaDia();
                            long cantArqueo = arqueoCajaDAO.getCantidadArqueoDia();
                            arqueo = cantApertura > cantArqueo;
                        }
                    } else {
                        arqueo = false;
                    }
                    if (arqueo) {
                        verificandoLogueo();
                    } else {
                        mensajeAdv("ERROR: ES PROBABLE QUE AUN NO HAYA HECHO UNA APERTURA DE CAJA");
                    }
                } else {
                    alert = true;
                }
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void procesarArqueo() {
        numValidator = new NumberValidator();
        long efectivo = 0;
        long tarjeta = 0;
        long nota = 0;
        try {
            efectivo = Long.parseLong(numValidator.numberValidator(txtMontoEfectivo.getText()));
        } catch (Exception e) {
        } finally {
        }
        try {
            tarjeta = Long.parseLong(numValidator.numberValidator(txtMontoInfonet.getText()));
        } catch (Exception e) {
        } finally {
        }
        try {
            nota = Long.parseLong(numValidator.numberValidator(txtMontoNotaCredito.getText()));
        } catch (Exception e) {
        } finally {
        }
        long sumtoria = efectivo + tarjeta + nota;
        if (sumtoria <= 0) {
            mensajeAdv("NO SE PUEDE PROCESAR EL ARQUEO DE CAJA, DEBES INGRESAR EL MONTO DE ACUERDO A LAS OPERACIONES REALIZADAS.");
        } else {
            if (arqueo()) {
//                if (arqueo) {
                Ticket ticket = new Ticket();
                ticket.ticketArqueo();
                ticket.print();

                Toaster.setTray("¡HAZ REALIZADO EL ARQUEO! LOS DATOS HAN SIDO PROCESADOS SATISFACTORIAMENTE.", "N", "S", 10000, "");
                this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/estetica/ArqueoCajaWebFXML.fxml", 896, 684, true);
//                }
//                setearDatos();
//                ModuloSupervisorFXMLController.arqueoCaja = true;
//                arqueo = true;
            } else {
                mensajeAdv("LOS DATOS NO HAN SIDO REGISTRADOS, VERIFIQUELOS.");
            }
        }
    }

    private void setearDatos() {
        //GLOBAL
        datos.put("montoFacturado", 0);
        datos.put("donaciones", 0);
        datos.put("retiroDinero", 0);
        //POR CAJERO LOGUEADO
        datos.put("retiroDineroCajero", 0);
        datos.put("montoCajero", 0);
        datos.put("donacionCajero", 0);
        //
        datos.put("zeta", 0);
        datos.put("facturaInicial", "");
        datos.put("facturaFinal", "");
        datos.put("estadoFacturaInicial", false);
        datos.put("contDesc", 0);
        datos.put("contTarj", 0);
        datos.put("sumDesc", 0);
        datos.put("sumTarj", 0);
        datos.put("cantEfectivoRecibido", 0);
        //
        datos.put("contEfectivo", 0);
        datos.put("sumEfectivo", 0);
        datos.put("totales", 0);
        datos.put("nClientes", 0);
        datos.put("nFuncionarios", 0);
        datos.put("nArticulos", 0);
        //
        datos.put("gra5", 0);
        datos.put("gra10", 0);
        datos.put("exe", 0);
        datos.put("gra", 0);
        datos.put("contDonaciones", 0);
        datos.put("sumDonaciones", 0);
        //
        datos.put("contNotaCred", 0);
        datos.put("sumNotaCred", 0);
        datos.put("contFact", 0);
        datos.put("sumFact", 0);
        datos.put("contCheque", 0);
        datos.put("sumCheque", 0);
        datos.put("contVale", 0);
        datos.put("sumVale", 0);
        datos.put("contAsoc", 0);
        datos.put("sumAsoc", 0);
        datos.put("vuelto", 0);
        //
        datos.put("contDolar", 0);
        datos.put("contReal", 0);
        datos.put("contPeso", 0);
        datos.put("contRetencion", 0);
        datos.put("sumDolar", 0);
        datos.put("sumReal", 0);
        datos.put("sumPeso", 0);
        datos.put("sumReal", 0);
        datos.put("sumRetencion", 0);
        datos.put("sumEfeRecibido", 0);
        datos.remove("codSup");
        datos.remove("fecha_arqueo");
        datos.put("caja", null);
        actualizarDatos();
    }

    private void asignaciones() {
        alert = true;
        alertCierre = false;
        montoRecaudado = true;
//        if (DatosEnCaja.getDatos() != null) {
//            datos = DatosEnCaja.getDatos();
//        }
//        if (DatosEnCaja.getUsers() != null) {
//            users = DatosEnCaja.getUsers();
//        }

    }

    private void verificandoLogueo() {
        if (!txtTarjetaSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(txtTarjetaSupervisor.getText()));
            if (supervisor != null) {
                mostrandoPaneles();
                txtTarjetaSupervisor.setEditable(false);
                txtMontoEfectivo.requestFocus();
                alert = false;
            } else {
                Alert alerta = new Alert(Alert.AlertType.ERROR, "Clave de supervisor incorrecta. (ESC -> CERRAR)".toUpperCase(), ButtonType.CLOSE);
                alerta.showAndWait();
                ocultandoPaneles();
                alertCierre = true;
                if (alerta.getResult() == ButtonType.CLOSE) {
                    alerta.close();
                } else {
                    alerta.close();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Clave de supervisor vacía. (ESC -> CERRAR)".toUpperCase(), ButtonType.CLOSE);
            this.alertCierre = true;
            alert.showAndWait();
            if (alert.getResult() == ButtonType.CLOSE) {
                alert.close();
            } else {
            }
        }
    }

    private void ocultandoPaneles() {
        Panel1.setVisible(false);
        Panel2.setVisible(false);
        Panel3.setVisible(false);
        Panel4.setVisible(false);
        Panel5.setVisible(false);

        toolBar01.setVisible(false);
        toolBar02.setVisible(false);
        toolBar03.setVisible(false);
        toolBar04.setVisible(false);
        toolBar05.setVisible(false);
    }

    private void mostrandoPaneles() {
        Panel1.setVisible(true);
        Panel2.setVisible(true);
        Panel3.setVisible(true);
        Panel4.setVisible(true);
        Panel5.setVisible(true);
        btnProcesar.setVisible(true);

        toolBar01.setVisible(true);
        toolBar02.setVisible(true);
        toolBar03.setVisible(true);
        toolBar04.setVisible(true);
        toolBar05.setVisible(true);
    }

    private String asignandoVariables(TextField monto) {
        numValidator = new NumberValidator();
        String valor = numValidator.numberValidator(monto.getText());
        if (!valor.equals("")) {
            int size = valor.length();
            if (size >= 4) {
                valor = valor.replace(",", "");
                valor = valor.replace(".", "");
            }
        }
        DecimalFormat formatea = new DecimalFormat("Gs ###,###.##");
        String dato = formatea.format(Integer.parseInt(valor));
        return dato;
    }

    private boolean convertirEntero(TextField monto) {
        numValidator = new NumberValidator();
        String valor = monto.getText().replace("Gs ", "");
        valor = valor.replace(",", "");
        try {
            Integer.parseInt(valor);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean convertirEnteroCantidad(TextField monto) {
        numValidator = new NumberValidator();
        String valor = monto.getText().replace(",", "");
        try {
            Integer.parseInt(valor);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String asignandoVariablesCantidad(TextField cantidad) {
        String valor = cantidad.getText();
        if (!valor.equals("")) {
            int size = valor.length();
            if (size >= 4) {
                valor = valor.replace(",", "");
                valor = valor.replace(".", "");
            }
        }
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        String dato = formatea.format(Integer.parseInt(valor));
        return dato;
    }

    private boolean verificandoDatos() {
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoEfectivo);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoSencillo);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoAsociaciones);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoFactory);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoChequeAlDia);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoChequeAdelantado);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoVale);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoFcred);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoFco);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoFcoCanjes);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoFcredCanjes);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoDolar);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoPeso);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoReal);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoNotaCredito);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoGiftCard);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoInfonet);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoPronet);
        }
        if (montoRecaudado) {
            montoRecaudado = verMonto(txtMontoPractipago);
        }
        return montoRecaudado;
    }

    private boolean verMonto(TextField lbl) {
        return lbl.getText().equalsIgnoreCase("0") || lbl.getText().equalsIgnoreCase("Gs 0") || lbl.getText().equalsIgnoreCase("");
    }

    public static Map contandoBilletes(int total) {
        int v100000 = 0;
        int v50000 = 0;
        int v10000 = 0;
        int v5000 = 0;
        int v1000 = 0;
        int v500 = 0;
        int v100 = 0;
        int v50 = 0;
        Map mapeo = new HashMap();
        System.out.println("INICIANDO con total = " + total);
        while (total >= 50) {
            if (total >= 100000) {
                int cantidad = (int) total / (int) 100000;
                total = total % 100000;
                v100000 = cantidad;
                System.out.println("CANTIDAD DE 100.000 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 50000) {
                int cantidad = (int) total / (int) 50000;
                total = total % 50000;
                v50000 = cantidad;
                System.out.println("CANTIDAD DE 50.000 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 10000) {
                int cantidad = (int) total / (int) 10000;
                total = total % 10000;
                v10000 = cantidad;
                System.out.println("CANTIDAD DE 10.000 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 5000) {
                int cantidad = (int) total / (int) 5000;
                total = total % 5000;
                v5000 = cantidad;
                System.out.println("CANTIDAD DE 5.000 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 1000) {
                int cantidad = (int) total / (int) 1000;
                total = total % 1000;
                v1000 = cantidad;
                System.out.println("CANTIDAD DE 1.000 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 500) {
                int cantidad = (int) total / (int) 500;
                total = total % 500;
                v500 = cantidad;
                System.out.println("CANTIDAD DE 500 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 100) {
                int cantidad = (int) total / (int) 100;
                total = total % 100;
                v100 = cantidad;
                System.out.println("CANTIDAD DE 100 Gs. : " + cantidad + " MOD: " + total);
            } else if (total >= 50) {
                int cantidad = (int) total / (int) 50;
                total = total % 50;
                v50 = cantidad;
                System.out.println("CANTIDAD DE 50 Gs. : " + cantidad + " MOD: " + total);
            }
        }
        mapeo.put("v100000", v100000);
        mapeo.put("v50000", v50000);
        mapeo.put("v10000", v10000);
        mapeo.put("v5000", v5000);
        mapeo.put("v1000", v1000);
        mapeo.put("v500", v500);
        mapeo.put("v100", v100);
        mapeo.put("v50", v50);
        return mapeo;
    }

    private void ocultandoLabels() {
        lblEfectivo.setVisible(false);
        lblSencillo.setVisible(false);
        lblAsociaciones.setVisible(false);
        lblFactory.setVisible(false);
        lblChequeAlDia.setVisible(false);
        lblChequeAdelantado.setVisible(false);
        lblVales.setVisible(false);
        lblFcred.setVisible(false);
        lblFco.setVisible(false);
        lblFcoCanjes.setVisible(false);
        lblFcredCanjes.setVisible(false);
        lblDolar.setVisible(false);
        lblPeso.setVisible(false);
        lblReal.setVisible(false);
        lblNotaCredito.setVisible(false);
        lblGiftCard.setVisible(false);
        lblInfonet.setVisible(false);
        lblPronet.setVisible(false);
        lblPractipago.setVisible(false);
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(null);
        DatosEnCaja.setUsers(null);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        boolean valor = manejoDAO.eliminarObtenerEstado(idManejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR -> GET
    private JSONObject jsonClaveSupervisor(String c) {
        JSONObject supervisor = null;
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/supervisor/auth/" + c + "");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    supervisor = (JSONObject) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
        supervisor = generarLoginAccesoSupervisor(c);
        return supervisor;
    }
    //////READ, SUPERVISOR -> GET

    //////CREATE, ARQUEO -> POST
    private boolean arqueo() {
        boolean exito = false;
        JSONParser parser = new JSONParser();
        long efectivo = 0;
        long tarjeta = 0;
        long nota = 0;
        try {
            efectivo = Long.parseLong(numValidator.numberValidator(txtMontoEfectivo.getText()));
        } catch (Exception e) {
        } finally {
        }
        try {
            tarjeta = Long.parseLong(numValidator.numberValidator(txtMontoInfonet.getText()));
        } catch (Exception e) {
        } finally {
        }
        try {
            nota = Long.parseLong(numValidator.numberValidator(txtMontoNotaCredito.getText()));
        } catch (Exception e) {
        } finally {
        }

        ArqueoCaja ac = new ArqueoCaja();
        ac.setFechaEmision(new Timestamp(System.currentTimeMillis()));
        Caja caja = new Caja();
        caja.setIdCaja(1L);

        ac.setCaja(caja);

        Supervisor superv = new Supervisor();
        superv.setIdSupervisor(Long.parseLong(supervisor.get("idSupervisor").toString()));

        List<FacturaClienteCab> listFact = facturaCabDAO.filtroFechaHoy();
        String facturaIni = "";
        String facturaFin = "";
        String timbrado = "";
        List<FacturaClienteCabHistorico> fcch = new ArrayList<>();
        for (int i = 0; i < listFact.size(); i++) {
            if (i == 0) {
                facturaIni = listFact.get(0).getNroFactura();
                fcch = facturaCabHistoricoDAO.listarPorFactura(listFact.get(0).getIdFacturaClienteCab());
                timbrado = fcch.get((fcch.size() - 1)).getNroTimbrado();
            }
            if (i == (listFact.size()) - 1) {
                facturaFin = listFact.get(0).getNroFactura();
            }
        }

        ac.setSupervisor(superv);
        ac.setEfectivo(Integer.parseInt(efectivo + ""));
        ac.setCantSencillo(0);
        ac.setSencillo(0);
        ac.setCantAsociacion(0);
        ac.setAsociacion(0);
        ac.setCantFactory(0);
        ac.setFactory(0);
        ac.setCantChequeDia(0);
        ac.setChequeDia(0);
        ac.setCantChequeAdel(0);
        ac.setChequeAdel(0);
        ac.setCantDolar(0);
        ac.setDolar(0);
        ac.setCantPeso(0);
        ac.setPeso(0);
        ac.setCantReal(0);
        ac.setReal(0);
        ac.setCantVale(0);
        ac.setVale(0);
        ac.setCantFcred(0);
        ac.setFcred(0);
        ac.setCantFco(0);
        ac.setCantFcoCanje(0);
        ac.setFcoCanje(0);
        ac.setCantFcredCanje(0);
        ac.setFcredCanje(0);
        ac.setCantNotaCred(0);
        ac.setNotaCred(Integer.parseInt(nota + ""));
        ac.setCantGifcard(0);
        ac.setGifcard(0);
        ac.setCantInfonet(0);
        ac.setInfonet(Integer.parseInt(tarjeta + ""));
        ac.setCantPronet(0);
        ac.setPronet(0);
        ac.setCantPractipago(0);
        ac.setPractipago(0);
        ac.setZeta(0);
        ac.setNroTimbrado(timbrado);
        ac.setFacturaFin(facturaFin);
        ac.setFacturaIni(facturaIni);
        ac.setContDesc(0);
        ac.setSumtDesc(0);
        int contEfe = 0;
        int sumEfe = 0;
        for (FacturaClienteCabEfectivo facturaClienteCabEfectivo : facturaCabEfectivoDAO.listarPorFechaHoy()) {
            contEfe++;
            sumEfe += facturaClienteCabEfectivo.getMontoEfectivo();
        }
        int contTarj = 0;
        int sumTarj = 0;
        for (FacturaClienteCabTarjeta facturaClienteCabTarj : facturaCabTarjetaDAO.listarPorFechaHoy()) {
            contTarj++;
            sumTarj += facturaClienteCabTarj.getMonto();
        }
        int contNota = 0;
        int sumNota = 0;
        for (FacturaClienteCabNotaCredito facturaClienteCabNotaCredito : facturaCabNotaCredDAO.listarPorFechaHoy()) {
            contNota++;
            sumNota += facturaClienteCabNotaCredito.getMonto();
        }
        ac.setContTarj(contTarj);
        ac.setSumTarj(sumTarj);
        ac.setContEfectivo(contEfe);
        ac.setSumEfectivo(sumEfe);
        ac.setCantEferecibida(sumEfe);
        ac.setContDonacion(0);
        ac.setSumDonacion(0);
        ac.setTotgra((sumTarj + sumNota + sumEfe));
        ac.setTotexe(0);
        ac.setTotgra5(0);
        ac.setTotgra10(0);
        ac.setTotales((sumTarj + sumNota + sumEfe));
        ac.setNumCliente(0);
        ac.setNumArticulo(0);
        ac.setNumFuncionario(0);
        ac.setV100(0);
        ac.setV1000(0);
        ac.setV10000(0);
        ac.setV100000(0);
        ac.setV50(0);
        ac.setV500(0);
        ac.setV5000(0);
        ac.setV50000(0);
        ac.setContNotaCred(contNota);
        ac.setSumNotaCred(sumNota);
        ac.setContVale(0);
        ac.setSumVale(0);
        ac.setContFact(0);
        ac.setSumFact((sumTarj + sumNota + sumEfe));
        ac.setContCheque(0);
        ac.setSumCheque(0);
        ac.setContAsoc(0);
        ac.setSumAsoc(0);
        ac.setContDolar(0);
        ac.setSumDolar(0);
        ac.setContReal(0);
        ac.setSumReal(0);
        ac.setContPeso(0);
        ac.setSumPeso(0);
        ac.setSumRetencion(0);
        ac.setContRetencion(0);

        return arqueoCajaDAO.insertarObtenerEstado(ac);
//        JSONObject jsonArqueo = creandoJsonArqueo();
        ////        exito = registrarArqueoLocal(jsonArqueo);
        //        exito = registrarTesoreria(jsonArqueo); //  
        //        registrarCtaTarVentas(jsonArqueo.get("caja").toString());
        //        if (exito == true) {
        //            try {
        //                jsonArqueo.put("fechaEmision", null);
        //                JSONObject caja = (JSONObject) parser.parse(jsonArqueo.get("caja").toString());
        //                long idCaja = Long.parseLong(caja.get("idCaja").toString());
        //                JSONObject newBox = new JSONObject();
        //                newBox.put("idCaja", idCaja);
        //                JSONObject supervis = (JSONObject) parser.parse(jsonArqueo.get("supervisor").toString());
        //                long idSupervisor = Long.parseLong(supervis.get("idSupervisor").toString());
        //                JSONObject newSupervisor = new JSONObject();
        //                newSupervisor.put("idSupervisor", idSupervisor);
        //                jsonArqueo.put("caja", newBox);
        //                jsonArqueo.put("supervisor", newSupervisor);
        //                Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        //                ArqueoCajaDTO arqueoDTO = gson.fromJson(jsonArqueo.toString(), ArqueoCajaDTO.class);
        //                arqueoDTO.setFechaEmision(timeStamp);
        //                boolean valor = arqueoDAO.insertarObtenerEstado(ArqueoCaja.fromArqueoCajaAsociadoDTO(arqueoDTO));
        //                if (valor) {
        //                    System.out.println("DATOS PERSISTIDOS EN PARANA LOCAL *** ARQUEO ***");
        //                } else {
        //                    System.out.println("LOS DATOS NO HAN SIDO PERSISTIDOS EN PARANA LOCAL *** ARQUEO **");
        //                }
        //            } catch (ParseException ex) {
        //                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        //            }
        //        }
    }
    //////CREATE, ARQUEO -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR
    private JSONObject generarLoginAccesoSupervisor(String c) {
        JSONParser parser = new JSONParser();
        try {
            Supervisor supervisor = superDAO.buscarCodSup(c);
            return (JSONObject) parser.parse(gson.toJson(supervisor.toSupervisorDTO()));
        } catch (Exception e) {
            return null;
        }
    }
    //////READ, SUPERVISOR

    //////CREATE, ARQUEO
    private boolean registrarArqueoLocal(JSONObject jsonArqueo) {
        try {
            JSONParser parser = new JSONParser();
            boolean estado = false;
            Calendar cal = Calendar.getInstance();
            String fecha = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
                    + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
                    + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);

            ZoneId zonedId = ZoneId.of("America/Montreal");
            LocalDate today = LocalDate.now(zonedId);
            try {
                if (today.toString().equals(datos.get("fecha_arqueo").toString())) {
                    jsonArqueo.put("fechaEmision", fecha);
                } else {
                    Timestamp fechaEmi = Timestamp.valueOf(datos.get("fecha_arqueo").toString() + " 00:00:00");
                    fechaEmi.setHours(22);
                    fechaEmi.setMinutes(0);
                    fechaEmi.setSeconds(0);
                    jsonArqueo.put("fechaEmision", fechaEmi.toString());
                }
            } catch (Exception e) {
                Timestamp fechaEmi = new Timestamp(0);
                fechaEmi.setHours(20);
                fechaEmi.setMinutes(8);
                fechaEmi.setSeconds(0);
                jsonArqueo.put("fechaEmision", fechaEmi.toString());
            }

            ConexionPostgres.conectar();
            JSONObject sup = (JSONObject) parser.parse(jsonArqueo.get("supervisor").toString());
            JSONObject usu = (JSONObject) parser.parse(sup.get("usuario").toString());
            usu.remove("fechaAlta");
            usu.remove("fechaMod");
            sup.remove("usuario");
            sup.put("usuario", usu);
            jsonArqueo.put("supervisor", sup);
            String sql = "INSERT INTO pendiente.arqueo_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "' , 'I' , '" + jsonArqueo + "' , now() , 'arqueo_caja');";
            System.out.println("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
                    estado = true;
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
            ConexionPostgres.cerrar();
            return estado;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }
    //////CREATE, ARQUEO
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    private boolean registrarTesoreria(JSONObject jsonArqueo) {
        JSONObject jsonTesoreria = crearJSONTesoreria(jsonArqueo);
        boolean estado = false;
//            Calendar cal = Calendar.getInstance();
//            String fecha = cal.get(Calendar.YEAR) + "-" + (cal.get(Calendar.MONTH) + 1) + "-"
//                    + cal.get(Calendar.DAY_OF_MONTH) + " " + cal.get(Calendar.HOUR_OF_DAY) + ":"
//                    + cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
        ZoneId zonedId = ZoneId.of("America/Montreal");
        LocalDate today = LocalDate.now(zonedId);
//            if (today.toString().equals(datos.get("fecha_arqueo").toString())) {
//                jsonArqueo.put("fechaEmision", today);
//            } else {
//                Timestamp fechaEmi = Timestamp.valueOf(datos.get("fecha_arqueo").toString());
////                Timestamp fechaEmi = Timestamp.valueOf(datos.get("fecha_arqueo").toString() + " 00:00:00");
////                fechaEmi.setHours(22);
////                fechaEmi.setMinutes(0);
////                fechaEmi.setSeconds(0);
//                jsonArqueo.put("fechaEmision", fechaEmi.toString());
//            }
        jsonTesoreria.put("fecha", today.toString());
        ConexionPostgres.conectar();
//            JSONObject sup = (JSONObject) parser.parse(jsonArqueo.get("supervisor").toString());
//            JSONObject usu = (JSONObject) parser.parse(sup.get("usuario").toString());
//            usu.remove("fechaAlta");
//            usu.remove("fechaMod");
//            sup.remove("usuario");
//            sup.put("usuario", usu);
//            jsonArqueo.put("supervisor", sup);
        String sql = "INSERT INTO pendiente.tesoreria_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "' , 'I' , '" + jsonTesoreria + "' , now() , 'tesoreria');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estado;
    }

    private void registrarCtaTarVentas(String c) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject caja = (JSONObject) parser.parse(c);
            ZoneId zonedId = ZoneId.of("America/Montreal");
            LocalDate today = LocalDate.now(zonedId);
            ConexionPostgres.conectar();
            String sql = "INSERT INTO pendiente.ctatar_ventas (nro_caja, fecha) VALUES ('" + caja.get("nroCaja").toString() + "' , '" + java.sql.Date.valueOf(today.toString()) + "')";
            System.out.println("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
            ConexionPostgres.cerrar();
        } catch (ParseException ex) {
            Logger.getLogger(ArqueoCajaWebFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private JSONObject crearJSONTesoreria(JSONObject jsonArqueo) {
        JSONParser parser = new JSONParser();
        JSONObject tesoreria = new JSONObject();

//        long cintas = (Long.parseLong(jsonArqueo.get("sumTarj").toString())) + (Long.parseLong(jsonArqueo.get("sumEfectivo").toString())) + (Long.parseLong(jsonArqueo.get("sumNotaCred").toString()));
        long cintas = (Long.parseLong(jsonArqueo.get("totales").toString()));

        tesoreria.put("deposito", jsonArqueo.get("efectivo"));
        tesoreria.put("chqade", jsonArqueo.get("chequeAdel"));
        tesoreria.put("chqdia", jsonArqueo.get("chequeDia"));
        tesoreria.put("monto_gift", jsonArqueo.get("gifcard"));
        tesoreria.put("monext1", "DOLAR");
        tesoreria.put("cmonext1", jsonArqueo.get("cantDolar"));
        tesoreria.put("gsmonext1", jsonArqueo.get("dolar"));
        tesoreria.put("monext2", "PESO");
        tesoreria.put("cmonext2", jsonArqueo.get("cantPeso"));
        tesoreria.put("gsmonext2", jsonArqueo.get("peso"));
        tesoreria.put("monext3", "REAL");
        tesoreria.put("cmonext3", jsonArqueo.get("cantReal"));
        tesoreria.put("gsmonext3", jsonArqueo.get("real"));
        tesoreria.put("salvarias", jsonArqueo.get("sumNotaCred"));
        tesoreria.put("asociac", jsonArqueo.get("asociacion"));
        tesoreria.put("intereses", jsonArqueo.get("factory"));
        tesoreria.put("cintas", cintas);
        tesoreria.put("tcaja", jsonArqueo.get("sumaArqueo"));
//        tesoreria.put("indice", rangoTesoreriaDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal()));

        try {
            JSONObject caja = (JSONObject) parser.parse(jsonArqueo.get("caja").toString());
            tesoreria.put("caja", Integer.parseInt(caja.get("nroCaja").toString()));
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }

        return tesoreria;
    }

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO ARQUEO CAJA
    private JSONObject creandoJsonArqueo() {
        try {
            numValidator = new NumberValidator();
            org.json.JSONObject json = new org.json.JSONObject(datos);
            date = new Date();
            timestamp = new Timestamp(date.getTime());
            Long timestampJSON = timestamp.getTime();
            JSONParser parser = new JSONParser();
            JSONObject retiros = new JSONObject();
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            JSONObject supervisorData = new JSONObject();
            JSONObject usuario = (JSONObject) supervisor.get("usuario");
            JSONObject funcionario = (JSONObject) usuario.get("funcionario");
            funcionario.put("nombre", funcionario.get("nombre"));
            funcionario.put("apellido", funcionario.get("apellido"));
            usuario.put("funcionario", funcionario);
            supervisorData.put("usuario", usuario);
            supervisorData.put("idSupervisor", supervisor.get("idSupervisor"));
            retiros.put("supervisor", supervisorData);
            retiros.put("caja", caja);

            ZoneId zonedId = ZoneId.of("America/Montreal");
            LocalDate today = LocalDate.now(zonedId);
            if (json.isNull("fecha_arqueo")) {
                retiros.put("fechaEmision", timestampJSON);
            } else if (today.toString().equals(datos.get("fecha_arqueo").toString())) {
                retiros.put("fechaEmision", timestampJSON);
            } else {
                Timestamp fechaEmi = Timestamp.valueOf(datos.get("fecha_arqueo").toString() + " 00:00:00");
                fechaEmi.setHours(22);
                fechaEmi.setMinutes(0);
                fechaEmi.setSeconds(0);
                retiros.put("fechaEmision", fechaEmi);
            }
            JSONObject timbra = (JSONObject) parser.parse(datos.get("timbrado").toString());
            retiros.put("zeta", (Integer.parseInt(datos.get("zeta") + "") + 1));
            retiros.put("nroTimbrado", timbra.get("nroTimbrado"));
            try {
                retiros.put("facturaIni", datos.get("facturaInicial").toString());
            } catch (Exception e) {
                retiros.put("facturaIni", "0");
            }

            try {
                retiros.put("facturaFin", datos.get("facturaFinal").toString());
            } catch (Exception e) {
                retiros.put("facturaFin", "0");
            }

            retiros.put("asociacion", txtMontoAsociaciones.getText().equals("") ? 0 : numValidator.numberValidator(txtMontoAsociaciones.getText()));
            retiros.put("cantAsociacion", txtCantAsociaciones.getText().equals("") ? 0 : numValidator.numberValidator(txtCantAsociaciones.getText()));
            retiros.put("cantChequeAdel", txtCantChequeAdelantado.getText().equals("") ? 0 : numValidator.numberValidator(txtCantChequeAdelantado.getText()));
            retiros.put("cantChequeDia", txtCantChequeAlDia.getText().equals("") ? 0 : numValidator.numberValidator(txtCantChequeAlDia.getText()));
            retiros.put("cantDolar", txtCantDolar.getText().equals("") ? 0 : numValidator.numberValidator(txtCantDolar.getText()));
            retiros.put("cantFactory", txtCantFactory.getText().equals("") ? 0 : numValidator.numberValidator(txtCantFactory.getText()));
            retiros.put("cantFco", txtCantFCo.getText().equals("") ? 0 : numValidator.numberValidator(txtCantFCo.getText()));
            retiros.put("cantFcoCanje", txtCantFcoCanjes.getText().equals("") ? 0 : numValidator.numberValidator(txtCantFcoCanjes.getText()));
            retiros.put("cantFcred", txtCantFCred.getText().equals("") ? 0 : numValidator.numberValidator(txtCantFCred.getText()));
            retiros.put("cantFcredCanje", txtCantFcredCanjes.getText().equals("") ? 0 : numValidator.numberValidator(txtCantFcredCanjes.getText()));
            retiros.put("cantGifcard", txtCantGiftCard.getText().equals("") ? 0 : numValidator.numberValidator(txtCantGiftCard.getText()));
            retiros.put("cantInfonet", txtCantInfonet.getText().equals("") ? 0 : numValidator.numberValidator(txtCantInfonet.getText()));
            retiros.put("cantNotaCred", txtCantNotaCredito.getText().equals("") ? 0 : numValidator.numberValidator(txtCantNotaCredito.getText()));
            retiros.put("cantPeso", txtCantPeso.getText().equals("") ? 0 : numValidator.numberValidator(txtCantPeso.getText()));
            retiros.put("cantPractipago", txtCantPractipago.getText().equals("") ? 0 : numValidator.numberValidator(txtCantPractipago.getText()));
            retiros.put("cantPronet", txtCantPronet.getText().equals("") ? 0 : numValidator.numberValidator(txtCantPronet.getText()));
            retiros.put("cantReal", txtCantReal.getText().equals("") ? 0 : numValidator.numberValidator(txtCantReal.getText()));
            retiros.put("cantSencillo", txtCantSencillo.getText().equals("") ? 0 : numValidator.numberValidator(txtCantSencillo.getText()));
            retiros.put("cantVale", txtCantVale.getText().equals("") ? 0 : numValidator.numberValidator(txtCantVale.getText()));

            retiros.put("chequeAdel", txtMontoChequeAdelantado.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoChequeAdelantado.getText())));
            retiros.put("chequeDia", txtMontoChequeAlDia.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoChequeAlDia.getText())));
            retiros.put("dolar", txtMontoDolar.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoDolar.getText())));
            retiros.put("efectivo", txtMontoEfectivo.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoEfectivo.getText())) + Integer.valueOf(numValidator.numberValidator(txtMontoSencillo.getText())));
            retiros.put("factory", txtMontoFactory.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFactory.getText())));
            retiros.put("fco", txtMontoFco.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFco.getText())));
            retiros.put("fcoCanje", txtMontoFcoCanjes.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcoCanjes.getText())));
            retiros.put("fcred", txtMontoFcred.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcred.getText())));
            retiros.put("fcredCanje", txtMontoFcredCanjes.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcredCanjes.getText())));
            retiros.put("gifcard", txtMontoGiftCard.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoGiftCard.getText())));
            retiros.put("infonet", txtMontoInfonet.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoInfonet.getText())));
            retiros.put("notaCred", txtMontoNotaCredito.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoNotaCredito.getText())));
            retiros.put("peso", txtMontoPeso.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPeso.getText())));
            retiros.put("practipago", txtMontoPractipago.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPractipago.getText())));
            retiros.put("pronet", txtMontoPronet.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPronet.getText())));
            retiros.put("real", txtMontoReal.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoReal.getText())));
            retiros.put("sencillo", txtMontoSencillo.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoSencillo.getText())));
            retiros.put("vale", txtMontoVale.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoVale.getText())));
            int contDesc = 0;
            int sumDesc = 0;
            int contTarj = 0;
            int sumTarj = 0;
            int sumEfeRecibido = 0;
            int sumEfectivo = 0;
            int contEfectivo = 0;
            int contDonaciones = 0;
            int donaciones = 0;
            int gra = 0;
            int gra5 = 0;
            int gra10 = 0;
            int exe = 0;
            int totales = 0;
            double nArticulos = 0;
            int nFuncionarios = 0;
            int tEferecibido = 0;
            if (!json.isNull("contDesc")) {
                contDesc = Integer.parseInt(datos.get("contDesc").toString());
            }
            if (!json.isNull("cantEfectivoRecibido")) {
                tEferecibido = Integer.parseInt(datos.get("cantEfectivoRecibido").toString());
            }
            if (!json.isNull("sumDesc")) {
                sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
            }
            if (!json.isNull("contTarj")) {
                contTarj = Integer.parseInt(datos.get("contTarj").toString());
            }
            if (!json.isNull("sumTarj")) {
                sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
            }
            if (!json.isNull("sumEfeRecibido")) {
                sumEfeRecibido = Integer.parseInt(datos.get("sumEfeRecibido").toString());
            }
            if (!json.isNull("sumEfectivo")) {
                sumEfectivo = tEferecibido - Integer.parseInt(datos.get("sumEfectivo").toString());
            }
            if (!json.isNull("contEfectivo")) {
                contEfectivo = Integer.parseInt(datos.get("contEfectivo").toString());
            }

            if (!json.isNull("contDonaciones")) {
                contDonaciones = Integer.parseInt(datos.get("contDonaciones").toString());
            }

            if (!json.isNull("donaciones")) {
                donaciones = Integer.parseInt(datos.get("donaciones").toString());
            }
            if (!json.isNull("gra")) {
                gra = Integer.parseInt(datos.get("gra").toString());
            }
            if (!json.isNull("gra5")) {
                gra5 = Integer.parseInt(datos.get("gra5").toString());
            }
            if (!json.isNull("gra10")) {
                gra10 = Integer.parseInt(datos.get("gra10").toString());
            }
            if (!json.isNull("exe")) {
                exe = Integer.parseInt(datos.get("exe").toString());
            }
            if (!json.isNull("totales")) {
                totales = Integer.parseInt(datos.get("totales").toString());
            }
            if (!json.isNull("nArticulos")) {
                nArticulos = Double.parseDouble(datos.get("nArticulos").toString());
            }
            if (!json.isNull("nFuncionarios")) {
                nFuncionarios = Integer.parseInt(datos.get("nFuncionarios").toString());
            }
            retiros.put("contDesc", contDesc);
            retiros.put("sumtDesc", sumDesc);
            retiros.put("contTarj", contTarj);
            retiros.put("sumTarj", sumTarj);
//            error aqui abajo
            long dif = 0;
            try {
                dif = (Long.valueOf(datos.get("facturaFinal").toString()) - Long.valueOf(datos.get("facturaInicial").toString())) + 1;
            } catch (Exception e) {
                dif = 0;
            }

//            int efectivoRec = sumEfeRecibido - sumEfectivo;
            retiros.put("contEfectivo", contEfectivo);
            retiros.put("sumEfectivo", sumEfectivo);
//            retiros.put("cantEferecibida", efectivoRec);
            retiros.put("cantEferecibida", tEferecibido);
            retiros.put("contDonacion", contDonaciones);
            retiros.put("sumDonacion", donaciones);
            retiros.put("totgra", gra);
            retiros.put("totexe", exe);
            retiros.put("totgra5", gra5);
            retiros.put("totgra10", gra10);
            retiros.put("totales", totales);
            retiros.put("numCliente", dif);
            retiros.put("numArticulo", nArticulos);
            retiros.put("numFuncionario", nFuncionarios);
            Map mapeo = contandoBilletes(sumEfectivo);
            retiros.put("v100000", Integer.parseInt(mapeo.get("v100000").toString()));
            retiros.put("v50000", Integer.parseInt(mapeo.get("v50000").toString()));
            retiros.put("v10000", Integer.parseInt(mapeo.get("v10000").toString()));
            retiros.put("v5000", Integer.parseInt(mapeo.get("v5000").toString()));
            retiros.put("v1000", Integer.parseInt(mapeo.get("v1000").toString()));
            retiros.put("v500", Integer.parseInt(mapeo.get("v500").toString()));
            retiros.put("v100", Integer.parseInt(mapeo.get("v100").toString()));
            retiros.put("v50", Integer.parseInt(mapeo.get("v50").toString()));
            int contNotaCred = 0;
            int sumNotaCred = 0;
            int contVale = 0;
            int sumVale = 0;
            int contFact = 0;
            int sumFact = 0;
            int contCheque = 0;
            int sumCheque = 0;
            int contAsoc = 0;
            int sumAsoc = 0;
            int contDolar = 0;
            int sumDolar = 0;
            int contReal = 0;
            int sumReal = 0;
            int contPeso = 0;
            int sumPeso = 0;
            int contRetencion = 0;
            int sumRetencion = 0;
            if (!json.isNull("contNotaCred")) {
                contNotaCred = Integer.parseInt(datos.get("contNotaCred").toString());
            }
            if (!json.isNull("sumNotaCred")) {
                sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
            }
            if (!json.isNull("contVale")) {
                contVale = Integer.parseInt(datos.get("contVale").toString());
            }
            if (!json.isNull("sumVale")) {
                sumVale = Integer.parseInt(datos.get("sumVale").toString());
            }
            if (!json.isNull("contFact")) {
                contFact = Integer.parseInt(datos.get("contFact").toString());
            }
            if (!json.isNull("sumFact")) {
                sumFact = Integer.parseInt(datos.get("sumFact").toString());
            }
            if (!json.isNull("contCheque")) {
                contCheque = Integer.parseInt(datos.get("contCheque").toString());
            }
            if (!json.isNull("sumCheque")) {
                sumCheque = Integer.parseInt(datos.get("sumCheque").toString());
            }
            if (!json.isNull("contAsoc")) {
                contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
            }
            if (!json.isNull("sumAsoc")) {
                sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
            }
            if (!json.isNull("contDolar")) {
                contDolar = Integer.parseInt(datos.get("contDolar").toString());
            }
            if (!json.isNull("sumDolar")) {
                sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
            }
            if (!json.isNull("contReal")) {
                contReal = Integer.parseInt(datos.get("contReal").toString());
            }
            if (!json.isNull("sumReal")) {
                sumReal = Integer.parseInt(datos.get("sumReal").toString());
            }
            if (!json.isNull("contPeso")) {
                contPeso = Integer.parseInt(datos.get("contPeso").toString());
            }
            if (!json.isNull("sumPeso")) {
                sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
            }
            if (!json.isNull("contRetencion")) {
                contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
            }
            if (!json.isNull("sumRetencion")) {
                sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
            }
            retiros.put("contNotaCred", contNotaCred);
            retiros.put("sumNotaCred", sumNotaCred);
            retiros.put("contVale", contVale);
            retiros.put("sumVale", sumVale);
            retiros.put("contFact", contFact);
            retiros.put("sumFact", sumFact);
            retiros.put("contCheque", contCheque);
            retiros.put("sumCheque", sumCheque);
            retiros.put("contAsoc", contAsoc);
            retiros.put("sumAsoc", sumAsoc);
            retiros.put("contDolar", contDolar);
            retiros.put("sumDolar", sumDolar);
            retiros.put("contReal", contReal);
            retiros.put("sumReal", sumReal);
            retiros.put("contPeso", contPeso);
            retiros.put("sumPeso", sumPeso);
            retiros.put("contRetencion", contRetencion);
            retiros.put("sumRetencion", sumRetencion);

            int sumaArqueo = (txtMontoEfectivo.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoEfectivo.getText())))
                    + (txtMontoSencillo.getText().equals("") ? 0 : Integer.parseInt(numValidator.numberValidator(txtMontoSencillo.getText())))
                    + (txtMontoAsociaciones.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoAsociaciones.getText())))
                    + (txtMontoFactory.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFactory.getText())))
                    + (txtMontoChequeAlDia.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoChequeAlDia.getText())))
                    + (txtMontoChequeAdelantado.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoChequeAdelantado.getText())))
                    + (txtMontoVale.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoVale.getText())))
                    + (txtMontoFcred.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcred.getText())))
                    + (txtMontoFco.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFco.getText())))
                    + (txtMontoFcoCanjes.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcoCanjes.getText())))
                    + (txtMontoFcredCanjes.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoFcredCanjes.getText())))
                    + (txtMontoDolar.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoDolar.getText())))
                    + (txtMontoPeso.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPeso.getText())))
                    + (txtMontoReal.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoReal.getText())))
                    + (txtMontoNotaCredito.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoNotaCredito.getText())))
                    + (txtMontoGiftCard.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoGiftCard.getText())))
                    + (txtMontoInfonet.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoInfonet.getText())))
                    + (txtMontoPronet.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPronet.getText())))
                    + (txtMontoPractipago.getText().equals("") ? 0 : Integer.valueOf(numValidator.numberValidator(txtMontoPractipago.getText())));

            retiros.put("sumaArqueo", sumaArqueo);

            System.out.println("RETIROS JSON -->> " + retiros);
            return retiros;
        } catch (ParseException ex) {
            mensajeAdv("NO SE PUEDE REALIZAR AUN EL ARQUEO DE CAJA INTENTE PRIMERAMENTE TRABAJAR COMO CAJERO");
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }
//JSON CREANDO ARQUEO CAJA
//JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

//PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker() {
        return new Task() {
            @Override
            protected Object call() throws InterruptedException {
                int i = 1;
                int size = 3;
                Platform.runLater(() -> {
                    viendoProgress();
                });
                for (int j = 0; j < size; j++) {
                    TimeUnit.SECONDS.sleep(1);
                    updateProgress((j + 1), size);
                    if (i == size) {
                        Platform.runLater(() -> {
                            imprimirArqueo();
                            System.out.println("FINALIZADO");
                        });
                    } else {
                        i++;
                    }
                }
                Platform.runLater(() -> {
                    ocultandoProgress();
                });
                return true;
            }
        };
    }

    private void imprimirArqueo() {
        if (arqueo) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.YES);
//            Alert alert2 = new Alert(Alert.AlertType.CONFIRMATION, "LOS DATOS HAN SIDO PROCESADOS SATISFACTORIAMENTE.", ok);
            Ticket ticket = new Ticket();
            ticket.ticketArqueo();
            ticket.print();
//            alert2.showAndWait();
//            if (alert2.getResult() != null) {
//                alert2.close();
            Toaster.setTray("¡HAZ REALIZADO EL ARQUEO! LOS DATOS HAN SIDO PROCESADOS SATISFACTORIAMENTE.", "N", "S", 10000, "");
            this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/ArqueoCajaFXML.fxml", 896, 684, true);
//            }
        }
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    public void viendoProgress() {
        anchorPaneFront.setDisable(true);
        btnProcesar.setDisable(true);
        btnSalir.setDisable(true);
        Panel1.setDisable(true);
        Panel2.setDisable(true);
        Panel3.setDisable(true);
        Panel4.setDisable(true);
        Panel5.setDisable(true);
        //
        progressIndicator.setVisible(true);
        labelProgress.setVisible(true);
    }

    public void ocultandoProgress() {
        anchorPaneFront.setDisable(false);
        btnProcesar.setDisable(false);
        btnSalir.setDisable(false);
        Panel1.setDisable(false);
        Panel2.setDisable(false);
        Panel3.setDisable(false);
        Panel4.setDisable(false);
        Panel5.setDisable(false);
        //
        progressIndicator.setVisible(false);
        labelProgress.setVisible(false);
    }

}
