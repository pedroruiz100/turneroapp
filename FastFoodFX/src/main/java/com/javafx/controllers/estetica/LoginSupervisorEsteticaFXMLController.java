/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.AnimationFX;
import com.javafx.util.CryptoFront;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.ManejoLocal;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 */
@Controller
@ScreenScoped
public class LoginSupervisorEsteticaFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    Image image;
    boolean alert;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private SupervisorDAO superDAO;
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();

    @Autowired
    ManejoLocalDAO manejoDAO;

    ManejoLocal manejoLocal = new ManejoLocal();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private TextField txtNumeroSupervisor;
    @FXML
    private Button btnIngresar;
    @FXML
    private Button btnSalir;
    @FXML
    private Pane panelLogueoSupervisor;
    @FXML
    private PasswordField passwordFieldClaveSup;
    private ImageView imageViewSupervisor;
    @FXML
    private AnchorPane anchorPaneSupervisor;
    @FXML
    private Label labelNroSupEstetica;
    @FXML
    private ImageView imageViewSupervisorEstetica;
    @FXML
    private Label labelClaveSupEstetica;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnIngresarAction(ActionEvent event) {
        ingresandoSupervisor();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void txtNumeroSupervisorAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneSupervisorKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
//        cargandoImagen();
        asignandoVariables();
        this.imageViewSupervisorEstetica = (ImageView) AnimationFX.rotationNodePlay(this.imageViewSupervisorEstetica, 1.5, false);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
//    private void ingresandoSupervisor() {
//        org.json.JSONObject json = new org.json.JSONObject(datos);
//        JSONParser parser = new JSONParser();
//        JSONObject caja = null;
//        if (!json.isNull("caja")) {
//            try {
//                caja = (JSONObject) parser.parse(datos.get("caja").toString());
//            } catch (ParseException ex) {
//                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//            }
//        }
//        if (caja != null && json.isNull("inicio")) {
//            JSONObject jsonSupervisor = jsonAccesoSupervisor(txtNumeroSupervisor.getText(), UtilLoaderBase.msjIda(passwordFieldClaveSup.getText()));
//            if (jsonSupervisor != null) {
//                if ("modulo_supervisor")) {
//                    this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, false);
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, true);
//                }
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, true);
//            }
//        } else {
//            mensajeAlerta("DEBE GENERAR UNA VENTA PARA PODER INGRESAR A ESTE MODULO.");
//        }
//    }
    private void ingresandoSupervisor() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        JSONObject caja = null;
        boolean estado = false;
        if (!json.isNull("caja")) {
            try {
                caja = (JSONObject) parser.parse(datos.get("caja").toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        //NEW A PEDIDO DE LA SUPERVISORA PARA QUE SE PUEDA HACER EL CIERRE IGUAL SI NO SE HA FACTURADO NADA
        if (caja != null && json.isNull("inicio")) {
            estado = true;
        } else {
            estado = !datos.containsKey("montoFacturado");
        }
        //FINISH

        CryptoFront cf = new CryptoFront();
//        if (caja != null && json.isNull("inicio")) {
//        if (estado) {
//            JSONObject jsonSupervisor = jsonAccesoSupervisor(txtNumeroSupervisor.getText(), UtilLoaderBase.msjIda(passwordFieldClaveSup.getText()));
        JSONObject jsonSupervisor = jsonAccesoSupervisor(txtNumeroSupervisor.getText(), cf.getHash(passwordFieldClaveSup.getText()));
        if (jsonSupervisor != null) {
//            if ("modulo_supervisor")) {
                this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, false);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, true);
//            }
        } else {
            if (ScreensContoller.getFxml().contentEquals("/vista/caja/facturaDeVentaFXML.fxml")) {
            } else {
                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, true);
            }
        }
//        } else {
//            mensajeAlerta("DEBE GENERAR UNA VENTA PARA PODER INGRESAR A ESTE MODULO.");
//        }
    }

    private void volviendo() {
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        if (jsonDatos.isNull("rendicion")) {
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, false);
        } else if (!Boolean.parseBoolean(datos.get("rendicion").toString())) {
            long idManejo = manejoDAO.recuperarId();
            ManejoLocal ml = manejoDAO.getById(idManejo);
            try {
                JSONObject json = (JSONObject) parser.parse(ml.getCaja());
                if (!Boolean.parseBoolean(json.get("rendicion").toString())) {
                    mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA FINALIZAR EL CIERRE DE TURNO.");
                } else {
                    this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, false);
                }
            } catch (Exception e) {
                mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA FINALIZAR EL CIERRE DE TURNO.");
            } finally {
            }
        } else {
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 314, false);
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //Apartado 4 - AVISOS
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj.toUpperCase(), ButtonType.OK);
        alert = false;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                if (txtNumeroSupervisor.isFocused()) {
                    passwordFieldClaveSup.requestFocus();
                } else if (passwordFieldClaveSup.isFocused()) {
                    if (txtNumeroSupervisor.getText().isEmpty()) {
                        txtNumeroSupervisor.requestFocus();
                    } else {
                        ingresandoSupervisor();
                    }
                }
            } else {
                alert = true;
            }

        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoImagen() {
        File file = new File(PATH.PATH_SUP);
        this.image = new Image(file.toURI().toString());
        this.imageViewSupervisor.setImage(this.image);
    }

    private void asignandoVariables() {
        alert = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        repeatFocus(txtNumeroSupervisor);
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    //Apartado 6 - BACKEND
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR -> GET
    private JSONObject jsonAccesoSupervisor(String nroSupervisor, String clave) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject supervisor = null;
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/supervisor/auth/" + nroSupervisor + "/" + clave + "");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    supervisor = (JSONObject) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
        supervisor = generarLoginSupervisorLocal(nroSupervisor, clave);
//            }
//        } catch (IOException | ParseException ex) {
//            supervisor = generarLoginSupervisorLocal(nroSupervisor, clave);
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        if (supervisor != null) {
            Identity identity = new Identity();
            identity.usuarioLogueado(null);
            identity.usuarioLogueado((JSONObject) supervisor.get("usuario"));
        }
        return supervisor;
    }

    //////READ, SUPERVISOR -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //Apartado 7 - LOCAL
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR
    private JSONObject generarLoginSupervisorLocal(String nroSupervisor, String clave) {
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;
        Supervisor supervisor = superDAO.buscarNumeroCodigoSup(Integer.parseInt(nroSupervisor), clave);
        if (supervisor != null) {
            try {
                jsonObj = (JSONObject) parser.parse(gson.toJson(supervisor.toSupervisorRolFuncionDTO()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return jsonObj;
    }
    //////READ, SUPERVISOR
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

}
