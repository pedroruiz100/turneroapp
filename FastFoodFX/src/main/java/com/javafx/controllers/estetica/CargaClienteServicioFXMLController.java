/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.ClientePendiente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ClientePendienteDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientePendienteDAO;
import com.peluqueria.dto.ClienteDTO;
import com.peluqueria.dto.ClientePendienteDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
//@ScreenScoped
public class CargaClienteServicioFXMLController extends BaseScreenController implements Initializable {

    final KeyCombination altE = new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN);
    final KeyCombination altA = new KeyCodeCombination(KeyCode.A, KeyCombination.ALT_DOWN);

    static void setUltimoCiIngresado(TextField txtRucCiCliente, String valor, Label lbl) {
        if (txtRucCiCliente != null) {
            txtRucCiCliente.setText(valor);
        }
        if (lbl != null) {
            lbl.setText("");
        }
        jsonClienteAdd = null;
    }

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    private Gson gson2 = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private Gson gsonOnly = new Gson();

    @Autowired
    private ClienteDAO clienteDAO;

    private boolean exitoCrear;

    @Autowired
    private RangoClienteDAO rangoCliDAO;
    public static JSONObject clienteJSON = null;
    public static String rucCi;
    JSONParser parser = new JSONParser();

    private List<JSONObject> clienteList;
    private ObservableList<JSONObject> clienteData;
    public static JSONObject jsonClienteAdd;

    @Autowired
    private RangoClientePendienteDAO rangoCliPenDAO;

    @Autowired
    private ClientePendienteDAO cpDAO;

    private boolean enter;
    private boolean escape;

    @FXML
    private AnchorPane anchorPaneClienteServicio;
    @FXML
    private Label labelNombreCliente;
    @FXML
    private Button buttonVolver;
    @FXML
    private TableView<JSONObject> tableViewCliente;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNombre;
    @FXML
    private TableColumn<JSONObject, String> tableColumnApellido;
    @FXML
    private TableColumn<JSONObject, String> tableColumnRucCi;
    @FXML
    private GridPane gridPaneBusquedaCliente;
    @FXML
    private Label labelRucCiCliente;
    @FXML
    private TextField textFieldRucCiCliente;
//    private Button buttonEditarCliente;
    @FXML
    private AnchorPane anchorPaneInsertarCliente;
    @FXML
    private Button buttonGuardar;
    @FXML
    private Button buttonSalir;
    @FXML
    private Label labelRucCiCliente1;
    @FXML
    private TextField textFieldRucCi;
//    private DatePicker datePickerFecNac;
    @FXML
    private TextField textFieldNombreClienteDATA;
    @FXML
    private TextField textFieldApellidoClienteDATA;
    @FXML
    private TextField textFieldTelefonoDATA;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        rucCi = "";
        cargandoInicial();
    }

    private void buttonBuscarClienteAction(ActionEvent event) {
        buscarClientePorRucCi();
    }

    private void buttonAgregarPendienteAction(ActionEvent event) {
        enviarCliente();
    }

    private void buttonEditarClienteAction(ActionEvent event) {
        editarCliente();
    }

    private void buttonEliminarPendienteAction(ActionEvent event) {
        eliminarCliente();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteServicioKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void cargandoInicial() {
        if (ScreensContoller.getFxml().contentEquals("/vista/estetica/InsertarNuevoClienteFXML.fxml")) {
            textFieldRucCiCliente.setText(CajaDatos.getCaja().get("clienteCI").toString());
        }

        exitoCrear = false;
        enter = false;
        escape = false;
        jsonClienteAdd = null;
        clienteList = new ArrayList<>();
//        buttonEditarCliente.setDisable(true);
        listenTextFieldRucCiCliente();
        cargarTablaClientePendiente();
        if (ScreensContoller.getFxml().contentEquals("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml")) {
            org.json.JSONObject json = new org.json.JSONObject(CajaDatos.getCaja());
            if (!json.isNull("clienteCI")) {
                textFieldRucCiCliente.setText(CajaDatos.getCaja().get("clienteCI").toString());
            }
        }

        textFieldNombreClienteDATA.setText("");
        textFieldApellidoClienteDATA.setText("");
        textFieldTelefonoDATA.setText("");

        habilitarPaneles();
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, false);
    }

    private void nuevoCliente() {
//        if ("nuevo_cliente_estetica")) {
        org.json.JSONObject json = new org.json.JSONObject(CajaDatos.getCaja());
        if (json.isNull("clienteCI")) {
            CajaDatos.setCaja(new JSONObject());
            CajaDatos.getCaja().put("clienteCI", textFieldRucCiCliente.getText());
        } else {
            CajaDatos.getCaja().put("clienteCI", textFieldRucCiCliente.getText());
        }
        deshabilitarPaneles();
//            InsertarClienteFXMLController.enviarComponente(textFieldRucCiCliente, labelNombreCliente);
//            if (StageSecond.getStageData().isShowing()) {
//                StageSecond.getStageData().close();
//            }
//            this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, true);

//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, true);
//        }
    }

    private void editarCliente() {
//        if (!buttonEditarCliente.isDisable()) {
//        if ("editar_cliente_estetica")) {
        this.sc.loadScreen("/vista/estetica/EditarClienteFXML.fxml", 511, 611, "/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, true);
//        }
//        }
    }

    //Apartado 4 - AVISOS
    private void mensajeError(String msj) {
        enter = true;
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private boolean mensajeConfirmacion(String msj) {
        enter = true;
        escape = true;
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, msj.toUpperCase(), ok, cancel);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            return true;
        } else {
            alert.close();
            return false;
        }
    }

    private void mensajeInfo(String msj) {
        enter = true;
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj.toUpperCase(), ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        enter = true;
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj.toUpperCase(), ok);
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
//            buscarClientePorRucCi();
        }
        if (keyCode == event.getCode().TAB) {
            if (textFieldRucCiCliente.isFocused()) {
                tableViewCliente.requestFocus();
            } else if (tableViewCliente.isFocused()) {
                buttonVolver.requestFocus();
            } else if (buttonVolver.isFocused()) {
                textFieldRucCiCliente.requestFocus();
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewCliente.requestFocus();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (anchorPaneInsertarCliente.isVisible()) {
                habilitarPaneles();
            } else {
                if (!escape) {
                    volviendo();
                } else {
                    escape = false;
                }
            }
        }
        if (altA.match(event)) {
            nuevoCliente();
        }
        if (altE.match(event)) {
            editarCliente();
        }
        if (keyCode == event.getCode().ENTER) {
            if (textFieldRucCiCliente.getText().equals("")) {
                enter = true;
            }
            if (textFieldRucCi.isFocused()) {
                enter = true;
                textFieldNombreClienteDATA.requestFocus();
            } else if (textFieldNombreClienteDATA.isFocused()) {
                textFieldApellidoClienteDATA.requestFocus();
                enter = true;
            } else if (textFieldApellidoClienteDATA.isFocused()) {
                textFieldTelefonoDATA.requestFocus();
                enter = true;
            }
            if (!enter) {
                boolean valor = false;
                if (buscarClientePorRucCi()) {
                    for (int i = 0; i < clienteList.size(); i++) {
                        try {
                            JSONObject jsonClientesSeteados = (JSONObject) parser.parse(clienteList.get(i).toString());
                            JSONObject jsonClientes = (JSONObject) parser.parse(jsonClientesSeteados.get("cliente").toString());
                            if (jsonClienteAdd.get("ruc").toString().toUpperCase().equalsIgnoreCase(jsonClientes.get("ruc").toString())) {
                                valor = true;
                            }
//                        System.out.println("MIRAR 01) -> " + jsonClienteAdd.toString());
//                        System.out.println("MIRAR 02) -> " + clienteList.get(i));
                        } catch (ParseException ex) {
                            Logger.getLogger(CargaClienteServicioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    if (!jsonClienteAdd.isEmpty()) {
                        if (valor) {
                            mensajeError("YA SE HA REGISTRADO EL CLIENTE");
                        } else {
                            enviarCliente();
                        }
                    }
                }
            } else {
                enter = false;
            }
        }
        if (keyCode == event.getCode().DELETE) {
            eliminarCliente();
        }
    }

    public void listenTextFieldRucCiCliente() {
        textFieldRucCiCliente.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.equals("")) {
                jsonClienteAdd = null;
                labelNombreCliente.setText("");
//                buttonEditarCliente.setDisable(true);
            }
        });
    }

    //Apartado 6 - BACKEND
    private void insertarClientePendientes(JSONObject cliPen) {
        JSONParser parser = new JSONParser();
        String inputLine;
        JSONObject jsonRetorno = null;
        try {

            org.json.JSONObject json = new org.json.JSONObject(cliPen);
            if (!json.isNull("fechaAlta")) {
                Timestamp fecAlt = Timestamp.valueOf(cliPen.get("fechaAlta").toString());
                cliPen.remove("fechaAlta");
                cliPen.put("fechaAlta", fecAlt.getTime());
            } else {
                cliPen.remove("fechaAlta");
                cliPen.put("fechaAlta", null);
            }

            if (!json.isNull("fechaMod")) {
                Timestamp fecMod = Timestamp.valueOf(cliPen.get("fechaMod").toString());
                cliPen.remove("fechaMod");
                cliPen.put("fechaMod", fecMod.getTime());
            } else {
                cliPen.remove("fechaMod");
                cliPen.put("fechaMod", null);
            }

            JSONObject cliente = new JSONObject();

            JSONObject obj = (JSONObject) parser.parse(cliPen.get("cliente").toString());
            long idCliente = Long.parseLong(obj.get("idCliente").toString());
            cliente.put("idCliente", idCliente);

            cliPen.put("cliente", cliente);

//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/clientePendiente/");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(cliPen.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        jsonRetorno = (JSONObject) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
//                }
//            } else {
            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
//            }
        } catch (ParseException ex) {
//            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
        }
        if (jsonRetorno.get("idClientePendiente") == null) {
            jsonRetorno = persistiendoClientePendientesLocal(cliPen.toString());
        }
        if (jsonRetorno != null) {
            System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR");
        }
    }

    private void insertarServPendientes(JSONArray servPendiente, long idServPendiente) {
        JSONParser parser = new JSONParser();
        String inputLine;
        boolean exitoInsert = false;
        JSONArray jsonSP = new JSONArray();
        try {
            for (int i = 0; i < servPendiente.size(); i++) {
                JSONObject jsonClientePendiente = (JSONObject) parser.parse(servPendiente.get(i).toString());

                JSONObject clientePendiente = new JSONObject();
                JSONObject articulo = new JSONObject();
                JSONObject funcionario = new JSONObject();

                clientePendiente.put("idClientePendiente", idServPendiente);

                JSONObject objArt = (JSONObject) parser.parse(jsonClientePendiente.get("articulo").toString());
                long idArt = Long.parseLong(objArt.get("idArticulo").toString());
                articulo.put("idArticulo", idArt);

                JSONObject objFun = (JSONObject) parser.parse(jsonClientePendiente.get("funcionario").toString());
                long idFun = Long.parseLong(objFun.get("idFuncionario").toString());
                funcionario.put("idFuncionario", idFun);

                jsonClientePendiente.put("articulo", articulo);
                jsonClientePendiente.put("funcionario", funcionario);
                jsonClientePendiente.put("clientePendiente", clientePendiente);

                jsonSP.add(jsonClientePendiente);
            }

//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaGenerico("id_dato", "estetica.cli_pendiente") == 0) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/servPendiente/insercionMasiva");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonSP.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exitoInsert = (Boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
//                }
//            } else {
            exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
//            }
        } catch (ParseException ex) {
//            exitoInsert = persistiendoSrvPendientesLocal(jsonSP.toString());
            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
        }
        if (exitoInsert) {
            System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR");
        }
    }

    //Apartado 7 - LOCAL
    private JSONObject persistiendoClientePendientesLocal(String jsonCliPen) {
        ConexionPostgres.conectar();
        JSONParser parser = new JSONParser();
        JSONObject jsonRetorno = null;
        String sql = "INSERT INTO estetica.cli_pendiente (descripcion_dato, operacion, fecha) VALUES ('" + jsonCliPen + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                jsonRetorno = (JSONObject) parser.parse(jsonCliPen);
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            jsonRetorno = null;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                jsonRetorno = null;
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            jsonRetorno = null;
        }
        ConexionPostgres.cerrar();
        return jsonRetorno;
    }

    private boolean persistiendoSrvPendientesLocal(String arraySrvPendiente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        String sql = "INSERT INTO estetica.serv_pendiente (descripcion_dato, operacion, fecha) VALUES ('" + arraySrvPendiente + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            valor = false;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                valor = false;
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private void cargarTablaClientePendiente() {
        List<ClientePendiente> listCliPen = cpDAO.listarPendiente();
        if (listCliPen.size() == 0) {
            tableViewCliente.setItems(null);
        }
        for (ClientePendiente cli : listCliPen) {
            cargarTabla(cli);
        }
    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    private boolean buscarClientePorRucCi() {
        boolean valor = true;
        if (textFieldRucCiCliente.getText().equalsIgnoreCase("")) {
            mensajeError("El campo RUC/CI no debe quedar vacío para realizar la búsqueda");
        } else {
            if (!verificarCliente()) {
                String rucCliente = "";
                JSONParser parser = new JSONParser();
                JSONObject clienteObj = new JSONObject();
                try {
                    rucCliente = textFieldRucCiCliente.getText();
                    Cliente cliente = new Cliente();

                    if (isNumeric(rucCliente)) {
                        StringTokenizer st = new StringTokenizer(rucCliente, "-");
                        rucCliente = st.nextElement().toString();
                    }

                    cliente = clienteDAO.listarPorRucExacto(rucCliente);
                    if (isNumeric(rucCliente)) {
                        cliente.setRuc(Utilidades.calculoSET(rucCliente));
                    } else {
                        cliente.setRuc(rucCliente);
                    }

                    if (cliente.getIdCliente() == null) {
                        // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
                        List<Cliente> listClien = clienteDAO.listarPorCIRevancha(rucCliente);
                        if (listClien.size() != 0) {
                            cliente = listClien.get(0);
                            StringTokenizer st00 = new StringTokenizer(cliente.getRuc(), "-");
                            String rucCi = st00.nextElement().toString();

                            cliente.setFecNac(null);
                            cliente.setFechaAlta(null);
                            cliente.setFechaMod(null);
                            cliente.setRuc(Utilidades.calculoSET(rucCi));
                        }
                    }

                    if (cliente.getIdCliente() == null) {
                        jsonClienteAdd = clienteObj;
//                        buttonEditarCliente.setDisable(true);
                        labelNombreCliente.setText("");
                        valor = false;
                        nuevoCliente();
                    } else {
                        clienteObj = (JSONObject) parser.parse(gson.toJson(cliente.toClienteDTO()));
                        if (cliente.getApellido() != null) {
                            labelNombreCliente.setText(cliente.getNombre() + " " + cliente.getApellido());
//                            buttonEditarCliente.setDisable(false);
                        } else if (cliente.getNombre() != null) {
                            labelNombreCliente.setText(cliente.getNombre());
//                            buttonEditarCliente.setDisable(false);
                        }
                        jsonClienteAdd = clienteObj;
                    }
                } catch (Exception e) {
                    System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
                    labelNombreCliente.setText("");
                }
            } else {
                mensajeError("El cliente ya ha sido agregado como pendiente");
                textFieldRucCiCliente.requestFocus();
            }
        }
        return valor;
    }

    private boolean verificarCliente() {
        JSONParser parser = new JSONParser();
        boolean valor = false;
        for (JSONObject json : clienteList) {
            try {
                JSONObject clienteJSON = (JSONObject) parser.parse(json.get("cliente").toString());
                if (textFieldRucCiCliente.getText().equalsIgnoreCase(clienteJSON.get("ruc").toString())) {
                    valor = true;
                    break;
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            }
        }
        return valor;
    }

    private void eliminarCliente() {
        int posicionSeleccionada = tableViewCliente.getSelectionModel().getSelectedIndex();

        if (posicionSeleccionada >= 0) {
            if (mensajeConfirmacion("Seguro que quiere eliminar al cliente")) {
                JSONObject jsonCliPen = clienteList.get(posicionSeleccionada);
                if (!jsonCliPen.isEmpty()) {
                    long idCliPen = Long.parseLong(jsonCliPen.get("idClientePendiente").toString());

                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject usuarioCajero = Identity.getUsuario();
                    JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                    String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                    boolean estado = cpDAO.actualizarEstado(idCliPen, timestamp, nombreCaj);
                    if (estado) {
//                        mensajeInfo("Cliente pendiente eliminado exitosamente");
                        cargandoInicial();
                        if (!textFieldRucCiCliente.getText().equals("") && !labelNombreCliente.getText().equals("")) {
                            enter = true;
                            buscarClientePorRucCi();
                        }
                        enviandoEliminacionServidor(idCliPen);
                    } else {
                        mensajeError("No se ha podido eliminar al cliente seleccionado");
                    }
                } else {
                    mensajeError("NO HAZ SELECCIONADO NINGUN CLIENTE");
                }
            }
        } else {
            mensajeError("NO HAZ SELECCIONADO NINGUN CLIENTE");
        }
    }

    private void addData() {
        //INSERCION TABLA cliente_pendiente
        JSONObject jsonClientePendiente = cargarJsonPendiente();
        long fAlta = Long.parseLong(jsonClientePendiente.get("fechaAlta").toString());
        long fMod = Long.parseLong(jsonClientePendiente.get("fechaMod").toString());
        jsonClientePendiente.put("fechaAlta", null);
        jsonClientePendiente.put("fechaMod", null);
        ClientePendiente cp = gson.fromJson(jsonClientePendiente.toString(), ClientePendiente.class);
        cp.setFechaAlta(new Timestamp(fAlta));
        cp.setFechaMod(new Timestamp(fMod));

        cp = cpDAO.insertarObtenerObj(cp);

        cargarTabla(cp);
    }

    //Apartado 8 - JSON
    private JSONObject cargarJsonPendiente() {
        JSONObject cliente = new JSONObject();

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();

        long idCliente = Long.parseLong(jsonClienteAdd.get("idCliente").toString());
        cliente.put("idCliente", idCliente);

        JSONObject jsonClientePendiente = new JSONObject();
//        long idActual = rangoCliPenDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
//        jsonClientePendiente.put("idClientePendiente", idActual);
        jsonClientePendiente.put("cliente", cliente);
        jsonClientePendiente.put("procesado", false);
        jsonClientePendiente.put("usuAlta", Identity.getNomFun());
        jsonClientePendiente.put("fechaAlta", timestampJSON);
        jsonClientePendiente.put("usuMod", Identity.getNomFun());
        jsonClientePendiente.put("fechaMod", timestampJSON);

        return jsonClientePendiente;
    }

    //Apartado 10 - TABLE VIEW
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente() {
        clienteData = FXCollections.observableArrayList(clienteList);
        //columna Nombre ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONParser parser = new JSONParser();
                JSONObject cliente;
                try {
                    cliente = (JSONObject) parser.parse(data.getValue().get("cliente").toString());
                    String nombre = String.valueOf(cliente.get("nombre"));
                    return new ReadOnlyStringWrapper(nombre.toUpperCase());
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                    return null;
                }
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnApellido.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnApellido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONParser parser = new JSONParser();
                JSONObject cliente;
                try {
                    cliente = (JSONObject) parser.parse(data.getValue().get("cliente").toString());
                    String apellido = String.valueOf(cliente.get("apellido"));
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                    return null;
                }
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnRucCi.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnRucCi.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONParser parser = new JSONParser();
                JSONObject cliente;
                try {
                    cliente = (JSONObject) parser.parse(data.getValue().get("cliente").toString());
                    String ruc = String.valueOf(cliente.get("ruc"));
                    return new ReadOnlyStringWrapper(ruc);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                    return null;
                }
            }
        });
        //columna Ruc .................................................
        tableViewCliente.setItems(clienteData);
    }

    //OTROS METODOS UTILIZADOS
    private void enviarCliente() {
        //validar que no se agregue el mismo cliente en la tabla 
        if (jsonClienteAdd != null) {
            if (!textFieldRucCiCliente.getText().equals("")) {
                addData();
            } else {
                mensajeError("No haz seleccionado ningún cliente por agregar");
            }
        } else {
            mensajeError("NINGUN CLIENTE POR ENVIAR");
        }
        jsonClienteAdd = null;
        textFieldRucCiCliente.setText("");
        labelNombreCliente.setText("");
    }

    private void cargarTabla(ClientePendiente cp) {
        JSONParser parser = new JSONParser();
        if (cp != null) {
            try {
                cp.setFechaAlta(null);
                cp.setFechaMod(null);
                String stringCliDTO = gsonOnly.toJson(cp.toClientePendienteDTO());
                JSONObject jsonNew = (JSONObject) parser.parse(stringCliDTO);
                jsonClienteAdd = jsonNew;
                clienteList.add(jsonClienteAdd);
                actualizandoTablaCliente();
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        } else {
            mensajeError("No se ha podido agregar cliente");
        }
    }

    private void enviandoEliminacionServidor(long idCliPen) {
        JSONParser parser = new JSONParser();
        ClientePendienteDTO cliPenDTO = cpDAO.getById(idCliPen).toEliminarPendienteDTO();
        String jsonCliPen = gson2.toJson(cliPenDTO);
        try {
            JSONObject cliPen = (JSONObject) parser.parse(jsonCliPen);
            JSONArray servPendiente = (JSONArray) parser.parse(cliPen.get("servPendiente").toString());
            insertarClientePendientes(cliPen);
            if (!servPendiente.isEmpty()) {
                insertarServPendientes(servPendiente, Long.parseLong(cliPen.get("idClientePendiente").toString()));
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    public void habilitarPaneles() {
//        anchorPane11.setDisable(false);
//        anchorPane11.setStyle("-fx-opacity:  1;");
//        anchorPane11.setStyle("-fx-background-color:   #CEE3F6;");
//
//        anchorPaneCliente1.setDisable(false);
//        anchorPaneCliente1.setStyle("-fx-opacity:  1;");
//        anchorPaneCliente1.setStyle("-fx-background-color:   #CEE3F6;");
//
//        anchorPaneCliente111.setDisable(false);
//        anchorPaneCliente111.setStyle("-fx-opacity:  1;");
//        anchorPaneCliente111.setStyle("-fx-background-color:   #CEE3F6;");
//
//        anchorPane2.setDisable(false);
//        anchorPane2.setStyle("-fx-opacity:  1;");
//        anchorPane2.setStyle("-fx-background-color:   #CEE3F6;");

        textFieldRucCiCliente.setDisable(false);
        tableViewCliente.setDisable(false);
        anchorPaneInsertarCliente.setVisible(false);
        repeatFocus(textFieldRucCiCliente);
    }

    public void deshabilitarPaneles() {
//        anchorPane11.setDisable(true);
//        anchorPane11.setStyle("-fx-opacity:  0.85;");
//        
//        anchorPane2.setDisable(true);
//        anchorPane2.setStyle("-fx-opacity:  0.85;");
//        
//        anchorPaneCliente1.setDisable(true);
//        anchorPaneCliente1.setStyle("-fx-opacity:  0.85;");
//        
//        anchorPaneCliente111.setDisable(true);
//        anchorPaneCliente111.setStyle("-fx-opacity:  0.85;");

//        anchorPaneBuscar.setVisible(true);
        textFieldRucCiCliente.setDisable(true);
        textFieldRucCi.setText(textFieldRucCiCliente.getText());
        tableViewCliente.setDisable(true);
        anchorPaneInsertarCliente.setVisible(true);
//        textFieldNombreClienteDATA.setText("");
//        textFieldApellidoClienteDATA.setText("");
//        textFieldTelefonoDATA.setText("");
//        datePickerFecNac.setValue(null);
        repeatFocus(textFieldRucCi);
    }

    @FXML
    private void buttonGuardarAction(ActionEvent event) {
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
    }

    @FXML
    private void textFieldTelefonoKeyReleased(KeyEvent event) {
        keyPressdatePicker(event);
    }

    private void datePickerFecNacKeyReleased(KeyEvent event) {
        keyPressdatePicker(event);
    }

    @FXML
    private void anchorPaneInsertarClienteKeyReleased(KeyEvent event) {
    }

    private void keyPressdatePicker(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            registrarDatos();
        }
    }

    private void registrarDatos() {
        boolean registrar = false;
//        if ("btn_new_cliente_estetica")) {
        System.out.println("-->> " + textFieldNombreClienteDATA.getText() + " + " + textFieldApellidoClienteDATA.getText() + " + " + textFieldRucCi.getText());
        if (textFieldNombreClienteDATA.getText().equals("") || textFieldRucCi.getText().equals("") || textFieldApellidoClienteDATA.getText().equals("")) {
            mensajeError("El campo NOMBRE, APELLIDO, TELEFONO Y RUC/CI no debe quedar vacío.");
        } //            else if (!textFieldCelular.getText().isEmpty() || !textFieldTelefono.getText().isEmpty() || !textFieldEmail.getText().equals("")) {
        //                if (textFieldCelular.getText().startsWith("-") || textFieldCelular.getText().endsWith("-")) {
        //                    mensajeError("VERIFIQUE EL FORMATO DEL CAMPO CELULAR.");
        else if (textFieldTelefonoDATA.getText().startsWith("-") || textFieldTelefonoDATA.getText().endsWith("-")) {
            mensajeError("VERIFIQUE EL FORMATO DEL CAMPO TELÉFONO.");
            //                } else if (!EmailValidator.getInstance().isValid(textFieldEmail.getText())) {
            //                    mensajeError("EL CAMPO EMAIL NO ES VÁLIDO.");
            //                } else {
            //                    registrar = true;
            //                }}
        } else {
            registrar = true;
        }
        if (registrar) {

            if (verificarCodCliente()) {
                mensajeError("YA EXISTE UN USUARIO REGISTRADO CON ESE RUC/CI");
                textFieldRucCi.requestFocus();
            } else {
                guardando();
            }
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/InsertarNuevoClienteFXML.fxml", 502, 215, false);
//        }
    }

    private boolean verificarCodCliente() {
        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;
        if (!rucCi.equals("")) {
            if (NumberValidator.letterValidator(rucCi)) {
                return !(clienteDAO.listarPorRuc(rucCi).getIdCliente() == null);
            } else {
                if (rucCi.contains("-")) {
                    StringTokenizer str = new StringTokenizer(rucCi, "-");
                    codCliente = Integer.parseInt(str.nextElement().toString());
                } else {
                    codCliente = Integer.parseInt(rucCi);
                }
                return !(clienteDAO.getByCod(codCliente) == null);
            }
        } else {
            return false;
        }
    }

    private void guardando() {
        JSONObject cliente = new JSONObject();
        cliente = jsonCliente();
        long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cliente.put("idCliente", idActual);
        exitoCrear = persistiendoPendientes(cliente);
        if (exitoCrear) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            cliente.put("fechaAlta", null);
            cliente.put("fechaMod", null);
            ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
            cliDTO.setFechaAlta(timestamp);
            cliDTO.setFechaMod(timestamp);
            Cliente cli = Cliente.fromClienteDTO(cliDTO);

            Pais pais = new Pais();
            pais.setIdPais(0L);

            Departamento dpto = new Departamento();
            dpto.setIdDepartamento(0l);

            Ciudad ciu = new Ciudad();
            ciu.setIdCiudad(0l);
//            ciu.setIdCiudad(cliDTO.getCiudad().getIdCiudad());

            Barrio barr = new Barrio();
//            barr.setIdBarrio(cliDTO.getBarrio().getIdBarrio());
            barr.setIdBarrio(0l);

            cli.setPais(pais);
            cli.setDepartamento(dpto);
            cli.setCiudad(ciu);
            cli.setBarrio(barr);
            try {
                clienteDAO.insertar(cli);
                CajaDatos.getCaja().put("clienteCI", textFieldRucCi.getText());
                System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
            } catch (Exception e) {
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
            }
            textFieldRucCiCliente.setText(textFieldRucCi.getText());
            textFieldRucCi.requestFocus();
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                habilitarPaneles();
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS Y QUE EL CODIGO DEL CLIENTE NO SEA REPETIDO.", ButtonType.CLOSE);
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.CLOSE) {
                alert2.close();
            }
        }
    }

    private JSONObject jsonCliente() {
        JSONObject obj = new JSONObject();

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();

        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;

        obj.put("nombre", textFieldNombreClienteDATA.getText());
        obj.put("apellido", textFieldApellidoClienteDATA.getText());
        obj.put("callePrincipal", "");
//        obj.put("callePrincipal", textFieldDireccion.getText());
        obj.put("ruc", rucCi);

        if (NumberValidator.letterValidator(rucCi)) {
            codCliente = Utilidades.genCodClienteParana(rucCi);
        } else {
            if (rucCi.contains("-")) {
                StringTokenizer str = new StringTokenizer(rucCi, "-");
                codCliente = Integer.parseInt(str.nextElement().toString());
            } else {
                codCliente = Integer.parseInt(rucCi);
            }
        }

        obj.put("codCliente", codCliente);
//        if (comboBoxCiudad.getSelectionModel() != null) {
//            if (hashMapCiudadJson.containsKey(comboBoxCiudad.getSelectionModel().getSelectedItem())) {
//                obj.put("ciudad", hashMapCiudadJson.get(comboBoxCiudad.getSelectionModel().getSelectedItem()));
//            } else {
//                JSONObject jsonCiudad = new JSONObject();
//                jsonCiudad.put("idCiudad", 0);
//                obj.put("ciudad", jsonCiudad);
//            }
//        } else {
//            JSONObject jsonCiudad = new JSONObject();
//            jsonCiudad.put("idCiudad", 0);
//            obj.put("ciudad", jsonCiudad);
//        }
//        if (comboBoxBarrio.getSelectionModel() != null) {
//            if (hashMapBarrioJson.containsKey(comboBoxBarrio.getSelectionModel().getSelectedItem())) {
//                obj.put("barrio", hashMapBarrioJson.get(comboBoxBarrio.getSelectionModel().getSelectedItem()));
//            } else {
//                JSONObject jsonBarrio = new JSONObject();
//                jsonBarrio.put("idBarrio", 0);
//                obj.put("barrio", jsonBarrio);
//            }
//        } else {
//            JSONObject jsonBarrio = new JSONObject();
//            jsonBarrio.put("idBarrio", 0);
//            obj.put("barrio", jsonBarrio);
//        }
//        LocalDate ld = datePickerFecNac.getValue();
//        if (ld != null) {
//            obj.put("fecNac", ld.toString());
//        } else {
        obj.put("fecNac", null);
//        }
//
        obj.put("telefono", textFieldTelefonoDATA.getText());
//        obj.put("telefono2", textFieldCelular.getText());
//        obj.put("email", textFieldEmail.getText());

        obj.put("usuAlta", Identity.getNomFun());
        obj.put("fechaAlta", timestampJSON);
        obj.put("usuMod", Identity.getNomFun());
        obj.put("fechaMod", timestampJSON);
        return obj;
    }

    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        cliente.put("pais", pais);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

}
