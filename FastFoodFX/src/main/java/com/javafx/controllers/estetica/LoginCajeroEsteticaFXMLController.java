/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Caja;
import com.peluqueria.dao.ArqueoCajaDAO;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.impl.ArqueoCajaDAOImpl;
import com.peluqueria.dao.impl.CajaDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Descuento;
import com.javafx.util.Utilidades;
import com.jfoenix.controls.JFXButton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.validator.routines.InetAddressValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class LoginCajeroEsteticaFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    //VARIABLE DECLARADA ORIGINAL
    /*private static CajaDAO cajaDAO = new CajaDAOImpl();
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static ArqueoCajaDAO arqueoDAO = new ArqueoCajaDAOImpl();*/
    //FIN VARIABLE DECLARADA ORIGINAL
    Image image;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    private static CajaDAO cajaDAO = new CajaDAOImpl();
    private static ArqueoCajaDAO arqueoDAO = new ArqueoCajaDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    public static boolean salirDatos = false;
    private boolean alert;

    @FXML
    private TextField txtNumeroCajeroEstetica;
    @FXML
    private PasswordField txtClaveCajeroEstetica;
    @FXML
    private ImageView imageViewCajeroEstetica;
    @FXML
    private AnchorPane anchorPanerCajeroEstetica;
    @FXML
    private JFXButton btnIngresarEstetica2;
    @FXML
    private JFXButton btnSalirEstetica2;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void btnIngresarEsteticaAction(ActionEvent event) {
        ingresandoAperturaCaja();
    }

    private void btnSalirEsteticaAction(ActionEvent event) {
        saliendo();
    }

    @FXML
    private void anchorPanerCajeroEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void cargandoInicial() {
        alert = false;
        salirDatos = false;
//        cargandoImagen();
        initStaticParamCajero();
//        articuloPromo = recuperarMapeoArticuloPromo();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        this.imageViewCajeroEstetica = (ImageView) AnimationFX.rotationNodePlay(this.imageViewCajeroEstetica, 1.5, false);
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void saliendo() {
        if (!salirDatos) {
//            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, false);
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, false);
        } else {
            this.sc.loadScreen("/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, "/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        }
    }

    //Apartado 5 - LISTEN
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                if (txtNumeroCajeroEstetica.isFocused()) {
                    txtClaveCajeroEstetica.requestFocus();
                } else if (txtClaveCajeroEstetica.isFocused()) {
                    if (txtNumeroCajeroEstetica.getText().isEmpty()) {
                        txtNumeroCajeroEstetica.requestFocus();
                    } else {
                        ingresandoAperturaCaja();
                    }
                }
            }

        }
        if (keyCode == event.getCode().ESCAPE) {
            saliendo();
        }
    }

    //Apartado 6 - BACKEND
    private JSONObject jsonAccesoCaja(String cajaNro, String pass) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject caja = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/caja/auth/" + cajaNro + "/" + pass + "");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    caja = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            } else {
                caja = generarAccesoLocal(cajaNro, pass);
            }
        } catch (IOException | ParseException ex) {
            caja = generarAccesoLocal(cajaNro, pass);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return caja;
    }

    private void mensajeInfo(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private void mensajeError(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    public static HashMap recuperarMapeoArticuloPromo() {
        HashMap mapeo = new HashMap();
        ConexionPostgres.conectarLocal();
        String sql = "SELECT cod_articulo FROM sincro.articulo_promo";
        try {
            ResultSet rs = ConexionPostgres.getStLocal().executeQuery(sql);
            while (rs.next()) {
                mapeo.put(rs.getLong("cod_articulo"), rs.getLong("cod_articulo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginCajeroEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ConexionPostgres.cerrarLocal();
        return mapeo;
    }

    //Apartado 7 - LOCAL
//    private void ingresandoAperturaCaja() {
//        JSONParser parser = new JSONParser();
//        CryptoFront cf = new CryptoFront();
//        CryptoBack cb = new CryptoBack();
//        if (manejoDAO.recuperarId() == 0l) {
//            JSONObject jsonCaja = jsonAccesoCaja(txtNumeroCajeroEstetica.getText(), cf.getHash(txtClaveCajeroEstetica.getText()));
//            if (jsonCaja != null) {
//                try {
//                    JSONObject jsonIpBoca = (JSONObject) jsonCaja.get("ipBoca");
//                    InetAddress IP = InetAddress.getLocalHost();
//                    String ipBoca = IP.getHostAddress();
//                    if ("apertura_caja_estetica")) {
//                        if (manejoDAO.verificarExistencia() == 0) {
//                            CajaDeDatos cajaData = new CajaDeDatos();
//                            cajaData.mapeandoCaja(jsonCaja);
//                            try {
//                                verificandoValorZetaLocal();
//                                Utilidades.setIdRangoLocal((long) jsonCaja.get("nroCaja"));
//                                this.sc.loadScreen("/vista/estetica/AperturaEsteticaFXML.fxml", 468, 253, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, false);
//                            } catch (ParseException ex) {
//                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                            } catch (Exception ex) {
//                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                            }
//                        }
//                    } else {
//                        this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                    }
//                } catch (UnknownHostException | java.text.ParseException ex) {
//                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                }
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//            }
//            DatosEnCaja.setDatos(datos);
//        } else {
//            JSONObject objCaja = null;
//            try {
//                objCaja = (JSONObject) parser.parse(datos.get("caja").toString());
//            } catch (ParseException ex) {
//                Utilidades.log.error("ParseException", ex.fillInStackTrace());
//            }
//            if (objCaja != null) {
//                String claveFront = cf.getHash(txtClaveCajeroEstetica.getText());
//                if (objCaja.get("nroCaja").toString().equalsIgnoreCase(txtNumeroCajeroEstetica.getText()) && objCaja.get("claveCaja").toString().equalsIgnoreCase(cb.getHash(claveFront))) {
//                    Utilidades.setIdRangoLocal((long) objCaja.get("nroCaja"));
//                    this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", 1248, 771, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                }
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//            }
//        }
//    }
    private void ingresandoAperturaCaja() {
        JSONParser parser = new JSONParser();
        JSONObject jsonCabecera;
//        boolean valor = false;
        boolean valor = true;
//        org.json.JSONObject json = new org.json.JSONObject(datos);
//        try {
//            if (!json.isNull("aperturaCaja")) {
//                jsonCabecera = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
//
//                long fechaHoraEmision = Long.parseLong(jsonCabecera.get("fechaApertura").toString());
//
//                String output = new SimpleDateFormat("yyyy-MM-dd").format(new Timestamp(fechaHoraEmision));
//
//                ZoneId zonedId = ZoneId.of("America/Montreal");
//                LocalDate today = LocalDate.now(zonedId);
//
//                valor = output.equals(today.toString());
//                if (fact != null && !fact.equals("{}") && fact.size() != 0) {
//                    if (!valor) {
//                        valor = true;
//                    }
//                }
//            } else {
//                valor = true;
//            }
//        } catch (ParseException ex) {
//            jsonCabecera = null;
//        }

        if (valor) {
            //almacenando datos para descuento... si se sabe, debería estar en el backend, 
            //pero esto es especial, debido a la inestabilidad en la conexión...
            CryptoFront cf = new CryptoFront();
            CryptoBack cb = new CryptoBack();
//            Descuento.resetParam(3l);
            if (manejoDAO.recuperarId() == 0l) {
                JSONObject jsonCaja = jsonAccesoCaja(txtNumeroCajeroEstetica.getText(), cf.getHash(txtClaveCajeroEstetica.getText()));
                if (jsonCaja != null) {
//                    articuloPromo = recuperarMapeoArticuloPromo();
                    try {
                        JSONObject jsonIpBoca = (JSONObject) jsonCaja.get("ipBoca");
                        boolean ingresoIp = false;
                        InetAddress IP = InetAddress.getLocalHost();
                        InetAddress[] allMyIps = InetAddress.getAllByName(IP.getCanonicalHostName());
                        InetAddressValidator inetValidator = InetAddressValidator.getInstance();
                        if (allMyIps != null && allMyIps.length > 1) {
                            for (int i = 0; i < allMyIps.length; i++) {
                                String auxSplit[] = allMyIps[i].toString().split("\\/");
                                if (inetValidator.isValidInet4Address(auxSplit[1])) {
                                    if (auxSplit[1].contentEquals(jsonIpBoca.get("ipBoca").toString())) {
                                        ingresoIp = true;
                                        break;
                                    }
                                }
                            }
                        }
                        //ELIMINAR LA LINEA DE ABAJO SOLO PARA PRUEBA
                        ingresoIp = true;
                        if (ingresoIp) {
//                            if ("apertura_caja_estetica")) {
                            //**--VERIFICANDO--**
                            //**--NUEVO EN BD--**
                            if (manejoDAO.verificarExistencia() == 0) {
                                CajaDeDatos cajaData = new CajaDeDatos();
                                cajaData.mapeandoCaja(jsonCaja);
                                try {
                                    verificandoValorZetaLocal();
                                    Utilidades.setIdRangoLocal((long) jsonCaja.get("nroCaja"));
                                    this.sc.loadScreen("/vista/estetica/AperturaEsteticaFXML.fxml", 468, 253, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, false);
                                } catch (ParseException ex) {
                                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                                } catch (Exception ex) {
                                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                                }
                            }
//                            } else {
//                                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                            }
                        } else {
                            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                        }
                    } catch (UnknownHostException | java.text.ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                }
                DatosEnCaja.setDatos(datos);
            } else {
                //**NEW EXAMPLE**//
                JSONObject objCaja = null;
                try {
                    objCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                }
                if (objCaja != null) {
                    String claveFront = cf.getHash(txtClaveCajeroEstetica.getText());
                    if (objCaja.get("nroCaja").toString().equalsIgnoreCase(txtNumeroCajeroEstetica.getText()) && objCaja.get("claveCaja").toString().equalsIgnoreCase(cb.getHash(claveFront))) {
                        Utilidades.setIdRangoLocal((long) objCaja.get("nroCaja"));
//                        if (!json.isNull("modSup")) {
//                            if (json.getBoolean("modSup")) {
//                                mensajeError("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO!");
//                            } else {
//                                this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                            }
//                        } else {
                        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
//                        }
                    } else {
                        this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                    }
                } else {
                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, true);
                }
                //**FINAL**//
                // ORIGINALMENTE SOLO TENIA ESTE
//            this.sc.loadScreen("/vista/estetica/facturaVentaFXML.fxml", 1248, 771, "/vista/estetica/loginCajeroFXML.fxml", 545, 317, true);
                // FINALMENTE 
            }
        } else {
            mensajeInfo("NECESARIAMENTE DEBE REALIZAR EL ARQUEO DE CAJA");
        }
    }

    private JSONObject generarAccesoLocal(String cajaNro, String pass) {
        JSONParser parser = new JSONParser();
        int nroCaja = 0;
        try {
            nroCaja = Integer.parseInt(cajaNro);
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
        try {
            Caja caja = cajaDAO.buscarCaja(nroCaja, pass);
            return (JSONObject) parser.parse(gson.toJson(caja.toCajaDTO()));
        } catch (Exception e) {
            return null;
        }
    }

    private void verificandoValorZetaLocal() throws ParseException, Exception {

        //**--VERIFICANDO--**
        datos = DatosEnCaja.getDatos();
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(datos.get("timbrado").toString());
        String nro = (String) obj.get("nroTimbrado");
        JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
        long idCaja = Long.valueOf(caja.get("idCaja").toString());
        long num = recuperarZeta(idCaja, nro);
        datos.put("zeta", num);
        //SETEAR LOS DATOS EN LA VARIABLE DE LA CLASE
        DatosEnCaja.setDatos(datos);

    }

    private long recuperarZeta(long idCaja, String nro) {
        long num = arqueoDAO.recuperarNumMaxZeta();
        return num;
    }

    private void initStaticParamCajero() {
        ClienteEsteticaFXMLController.resetParam();
    }

    @FXML
    private void btnIngresarEsteticaAction2(ActionEvent event) {
        ingresandoAperturaCaja();
    }

    @FXML
    private void btnSalirEsteticaAction2(ActionEvent event) {
        saliendo();
    }
}
