package com.javafx.controllers.estetica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.FacturaCabClientePendiente;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.MotivoCancelacionFactura;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.FacturaCabClientePendienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.MotivoCancelacionFacturaDAO;
import com.peluqueria.dao.RangoCancelFactDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dto.FacturaCabClientePendienteDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.caja.CancelacionFacturaFXMLController;
import com.javafx.controllers.caja.ClienteFXMLController;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.NumberValidator;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.io.IOException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
public class CancelacionFacturaEsteticaFXMLController extends BaseScreenController implements Initializable {
//Apartado 1 y 2 - FXML y VARIABLES INICIALES

    public static void setFactAnt(String aFactAnt) {
        factAnt = aFactAnt;
    }

    static void setTotalFacturado(String total) {
        totalFacturado = total;
    }
    private boolean alert;
    private boolean alertEscape;
    private boolean primerLogueo;
    private JSONObject supervisor;

    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    @Autowired
    private SupervisorDAO superDAO;

    private NumberValidator numValidator;

    @Autowired
    private MotivoCancelacionFacturaDAO motivoDAO;

    @Autowired
    private RangoCancelFactDAO rangoCancelFactDAO;

    @Autowired
    public static FacturaCabClientePendienteDAO factCabCliPendDAO;

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private Map motivos;
    String selectMotivoInicial = "-- Seleccione un motivo --";
    private Date date;
    private Timestamp timestamp;
    private static String factAnt;
    private static String totalFacturado;
    static long idFactCab;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private HBox hBox;
    @FXML
    private Label labelCodSupervisor;
    @FXML
    private PasswordField passwordFieldCodSupervisor;
    @FXML
    private AnchorPane anchorPaneDatosRetiro;
    @FXML
    private VBox vBoxLabel;
    @FXML
    private Label labelSupervisor;
    @FXML
    private Label labelMotivo;
    @FXML
    private VBox vBoxText;
    @FXML
    private TextField textFieldSupervisor;
    @FXML
    private ChoiceBox<String> choiceBoxMotivos;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button buttonVolver;

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        procesar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneCancelacionFacturaEsteticaReleased(KeyEvent event) {
        keyPress(event);
    }

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        numValidator = new NumberValidator();
        ocultandoParametros();
        asignandoVariables();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, "/vista/estetica/CancelacionFacturaEsteticaFXML.fxml", 600, 400, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //Apartado 4 - AVISOS
    private void mensajeInformacion(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj.toUpperCase(), ok);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (this.alert) {
                verificandoSupervisor();
            }
        }
        if (keyCode == event.getCode().F2) {
            procesar();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alertEscape) {
                volviendo();
            } else {
                alertEscape = true;
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //Apartado 6 - BACKEND
    private JSONObject jsonClaveSupervisor(String c) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject superv = null;
        superv = buscarCodSupervisorLocal(c);
        return superv;
    }

    private void insertarFacturaCabClientePendiente() throws ParseException, IOException {
        JSONParser parser = new JSONParser();
        String inputLine;
        JSONObject JSONFactCabCliPen = null;
        JSONObject factCabCliPen = new JSONObject();

        JSONObject factCab = new JSONObject();
        factCab.put("idFacturaClienteCab", idFactCab);

        JSONObject cliPend = new JSONObject();
        cliPend.put("idClientePendiente", Long.parseLong(datos.get("idClientePendiente").toString()));

        factCabCliPen.put("facturaClienteCab", factCab);
        factCabCliPen.put("clientePendiente", cliPend);

//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaCabPendiente/");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(factCabCliPen.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        JSONFactCabCliPen = (JSONObject) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
//                }
//            } else {
        JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
//            }
//        } catch (IOException | ParseException ex) {
//            JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
//            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
//        }
        JSONObject jsonFCCP = (JSONObject) parser.parse(JSONFactCabCliPen.toString());
        if (jsonFCCP.get("clientePendiente") == null) {
            JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
        }
        if (JSONFactCabCliPen != null) {
            ObjectMapper mapper = new ObjectMapper();
            String jsonObj = factCabCliPen.toString();
            FacturaCabClientePendienteDTO fccpDTO = mapper.readValue(jsonObj, FacturaCabClientePendienteDTO.class);
            FacturaCabClientePendiente fccp = factCabCliPendDAO.insertarObtenerObj(FacturaCabClientePendiente.fromFacturaCabClientePendienteAsociado(fccpDTO));
            if (fccp != null) {
                System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR Y DE MANERA LOCAL");
            }
        }
    }

    private void jsonMotivoCancelaciones() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray arrayMotivos = null;
        motivos = new HashMap();
        generarChoiceBoxMotivo();
    }

    private boolean procesandoCancelacion() {
        boolean exito = false;
        String inputLine;
        JSONParser parser = new JSONParser();
        try {
            org.json.JSONObject json = new org.json.JSONObject(datos);
            if (!json.isNull("caida")) {
                if (datos.get("caida").toString().equalsIgnoreCase("forma_pago_estetica")) {
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());

                    org.json.JSONObject jsonFact = new org.json.JSONObject(objFact);
                    org.json.JSONObject jsonFormaPago = new org.json.JSONObject();
                    JSONObject obj = new JSONObject();
                    if (!jsonFact.isNull("forma_pago_estetica")) {
                        obj = (JSONObject) parser.parse(objFact.get("forma_pago_estetica").toString());
                        jsonFormaPago = new org.json.JSONObject(obj.toString());
                    }
//                    JSONObject obj = (JSONObject) parser.parse(objFact.get("forma_pago").toString());
//                    org.json.JSONObject jsonFormaPago = new org.json.JSONObject(obj.toString());
                    int monEfectivo = 0;
                    if (!jsonFormaPago.isNull("monEfectivo")) {
                        monEfectivo = Integer.parseInt(obj.get("monEfectivo").toString());
                    }
                    int monNotaCred = 0;
                    if (!jsonFormaPago.isNull("monNotaCred")) {
                        monNotaCred = Integer.parseInt(obj.get("monNotaCred").toString());
                    }
                    int monAsoc = 0;
                    if (!jsonFormaPago.isNull("monAsoc")) {
                        monAsoc = Integer.parseInt(obj.get("monAsoc").toString());
                    }
                    int monVale = 0;
                    if (!jsonFormaPago.isNull("monVale")) {
                        monVale = Integer.parseInt(obj.get("monVale").toString());
                    }
                    int monDolar = 0;
                    if (!jsonFormaPago.isNull("monDolar")) {
                        monDolar = Integer.parseInt(obj.get("monDolar").toString());
                    }
                    int monReal = 0;
                    if (!jsonFormaPago.isNull("monReal")) {
                        monReal = Integer.parseInt(obj.get("monReal").toString());
                    }
                    int monPeso = 0;
                    if (!jsonFormaPago.isNull("monPeso")) {
                        monPeso = Integer.parseInt(obj.get("monPeso").toString());
                    }
                    int monRetencion = 0;
                    if (!jsonFormaPago.isNull("monRetencion")) {
                        monRetencion = Integer.parseInt(obj.get("monRetencion").toString());
                    }
                    int monDesc = 0;
                    if (!jsonFormaPago.isNull("monDesc")) {
                        monDesc = Integer.parseInt(obj.get("monDesc").toString());
                    }
                    int monTarj = 0;
                    if (!jsonFormaPago.isNull("monTarj")) {
                        monTarj = Integer.parseInt(obj.get("monTarj").toString());
                    }
                    int monCheque = 0;
                    if (!jsonFormaPago.isNull("monCheque")) {
                        monCheque = Integer.parseInt(obj.get("monCheque").toString());
                    }
                    if (monEfectivo != 0) {
//                        int sumEfectivo = Integer.parseInt(datos.get("sumEfectivo").toString());
                        int contEfectivo = Integer.parseInt(datos.get("contEfectivo").toString());
//                        int resultadoSum = sumEfectivo - monEfectivo;
                        int resultadoCon = contEfectivo - 1;
//                        datos.put("sumEfectivo", resultadoSum);
                        datos.put("contEfectivo", resultadoCon);
                    }
                    if (monNotaCred != 0) {
                        int sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
                        int contNotaCred = Integer.parseInt(datos.get("contNotaCred").toString());
                        int resultadoSum = sumNotaCred - monNotaCred;
                        int resultadoCon = contNotaCred - 1;
                        datos.put("sumNotaCred", resultadoSum);
                        datos.put("contNotaCred", resultadoCon);
                    }
                    if (monAsoc != 0) {
                        int sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
                        int contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
                        int resultadoSum = sumAsoc - monAsoc;
                        int resultadoCon = contAsoc - 1;
                        datos.put("sumAsoc", resultadoSum);
                        datos.put("contAsoc", resultadoCon);
                    }
                    if (monVale != 0) {
                        int sumVale = Integer.parseInt(datos.get("sumVale").toString());
                        int contVale = Integer.parseInt(datos.get("contVale").toString());
                        int resultadoSum = sumVale - monVale;
                        int resultadoCon = contVale - 1;
                        datos.put("sumVale", resultadoSum);
                        datos.put("contVale", resultadoCon);
                    }
                    if (monDolar != 0) {
                        int sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
                        int contDolar = Integer.parseInt(datos.get("contDolar").toString());
                        int resultadoSum = sumDolar - monDolar;
                        int resultadoCon = contDolar - 1;
                        datos.put("sumDolar", resultadoSum);
                        datos.put("contDolar", resultadoCon);
                    }
                    if (monReal != 0) {
                        int sumReal = Integer.parseInt(datos.get("sumReal").toString());
                        int contReal = Integer.parseInt(datos.get("contReal").toString());
                        int resultadoSum = sumReal - monReal;
                        int resultadoCon = contReal - 1;
                        datos.put("sumReal", resultadoSum);
                        datos.put("contReal", resultadoCon);
                    }
                    if (monPeso != 0) {
                        int sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
                        int contPeso = Integer.parseInt(datos.get("contPeso").toString());
                        int resultadoSum = sumPeso - monPeso;
                        int resultadoCon = contPeso - 1;
                        datos.put("sumPeso", resultadoSum);
                        datos.put("contPeso", resultadoCon);
                    }
                    if (monRetencion != 0) {
                        int sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
                        int contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
                        int resultadoSum = sumRetencion - monRetencion;
                        int resultadoCon = contRetencion - 1;
                        datos.put("sumRetencion", resultadoSum);
                        datos.put("contRetencion", resultadoCon);
                    }
                    if (monDesc != 0) {
                        int sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
                        int contDesc = Integer.parseInt(datos.get("contDesc").toString());
                        int resultadoSum = sumDesc - monDesc;
                        int resultadoCon = contDesc - 1;
                        datos.put("sumDesc", resultadoSum);
                        datos.put("contDesc", resultadoCon);
                    }
                    if (monTarj != 0) {
                        int sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
                        int contTarj = Integer.parseInt(datos.get("contTarj").toString());
                        int resultadoSum = sumTarj - monTarj;
                        int resultadoCon = contTarj - 1;
                        datos.put("sumTarj", resultadoSum);
                        datos.put("contTarj", resultadoCon);
                    }
                    if (monCheque != 0) {
                        int sumCheque = Integer.parseInt(datos.get("sumCheque").toString());
                        int contCheque = Integer.parseInt(datos.get("contCheque").toString());
                        int resultadoSum = sumCheque - monCheque;
                        int resultadoCon = contCheque - 1;
                        datos.put("sumCheque", resultadoSum);
                        datos.put("contCheque", resultadoCon);
                    }
                }
            }
//            FacturaVentaEsteticaFXMLController.persistiendoFact(false, 0l);

        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        JSONObject jsonCancelacion = creandoJsonCancelacion();
        try {
            if (DatosEnCaja.getDatos() != null) {
                datos = DatosEnCaja.getDatos();
            }
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            }
            org.json.JSONObject json = new org.json.JSONObject(datos);
            exito = registarCancelacionFactLocal(jsonCancelacion);
            json = new org.json.JSONObject(datos);
            if (!json.isNull("caida")) {
                if (!datos.get("caida").toString().equalsIgnoreCase("forma_pago_estetica")) {
//                    JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
//                    long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
//                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
//                    tal.setNroActual(tal.getNroActual() + 1);
//                    taloDAO.actualizarNroActual(tal);
                }
            } else {
//                JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
//                long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
//                TalonariosSucursales tal = taloDAO.getById(idTalonario);
//                tal.setNroActual(tal.getNroActual() + 1);
//                taloDAO.actualizarNroActual(tal);
            }
            //SeteandoDatos de FacturaClienteCab
//            cabeceraProcesada();
            datos.put("uuidCassandraActual", "");
//            datos.remove("uuidCassandraActual");
            datos.put("idRangoFacturaActual", 0);
            datos.put("idFactClienteCabServidor", 0);
            datos.put("insercionFacturaVentaCab", false);
            datos.put("actualizacionLocal", false);
            datos.put("insercionFacturaVentaCabLocal", false);
            datos.put("ventaServer", false);
            ClienteFXMLController.resetParam();
            JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            FacturaVentaEsteticaFXMLController.setTextFieldCompAnt(factAnt);
            datos.put("compAnt", cab.get("nroFactura").toString());
            datos.put("nroFact", "");
            setCantidadArticulo();
            return exito;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }

    //Apartado 7 - LOCAL
    private JSONObject persistiendoFactCabCliPendLocal(String jsonFactCabCliPend) {
        ConexionPostgres.conectar();
        JSONParser parser = new JSONParser();
        JSONObject jsonRetorno = null;
        String sql = "INSERT INTO estetica.fact_cab_cli_pend (descripcion_dato, operacion, fecha) VALUES ('" + jsonFactCabCliPend + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                jsonRetorno = (JSONObject) parser.parse(jsonFactCabCliPend);
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            jsonRetorno = null;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                jsonRetorno = null;
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            jsonRetorno = null;
        }
        ConexionPostgres.cerrar();
        return jsonRetorno;
    }

    private boolean registarCancelacionFactLocal(JSONObject jsonCancelacion) {
        boolean estado = false;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        ConexionPostgres.conectar();
        long idRango = rangoCancelFactDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        jsonCancelacion.put("idCancelacionFactura", idRango);
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonCancelacion + "','cancelacionFactura', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION FACTURA ********");
                FacturaVentaDatos.resetParam();
                FacturaVentaEsteticaFXMLController.iniciandoFactCab();
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        registrarCancelFactDet(idRango);
        ConexionPostgres.cerrar();
        return estado;
    }

    private void cabeceraProcesada() {
        long uuid = VentasUtiles.recuperarId();
        ConexionPostgres.conectar();
//        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + datos.get("uuidCassandraActual").toString();
        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + uuid;
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz actualizado un registro HANDLER PARANA ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
    }

    //Apartado 8 - JSON
    private JSONObject creandoJsonCancelacion() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

        date = new Date();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();

        String motivoDescri = choiceBoxMotivos.getSelectionModel().getSelectedItem();
        JSONObject cancelacion = new JSONObject();
        JSONObject usuarioSup = (JSONObject) supervisor.get("usuario");

        JSONObject aperturaCa = null;
        try {
            aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");

        usuarioCajero.put("idUsuario", usuarioCajero.get("idUsuario").toString());

        JSONObject usuarioSupervisor = new JSONObject();
        usuarioSupervisor.put("idUsuario", usuarioSup.get("idUsuario"));
        JSONObject motivoCancelacionFac = new JSONObject();
        motivoCancelacionFac.put("idMotivoCancelFact", motivos.get(motivoDescri));

        JSONObject sucu = new JSONObject();
        try {
            JSONObject jsonSucu = (JSONObject) parser.parse(jsonDatos.get("sucursal").toString());
            sucu.put("idSucursal", jsonSucu.get("idSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }

        cancelacion.put("sucursal", sucu);
        cancelacion.put("fechaCancelacion", timestampJSON);
        cancelacion.put("usuarioCajero", usuarioCajero);
        cancelacion.put("usuarioSupervisor", usuarioSupervisor);
        cancelacion.put("motivoCancelacionFactura", motivoCancelacionFac);
        cancelacion.put("monto", numValidator.numberValidator(totalFacturado));
        cancelacion.put("nroFactura", CancelacionFacturaEsteticaFXMLController.factAnt);
        return cancelacion;
    }

    //OTROS METODOS UTILIZADOS
    private void ocultandoParametros() {
        vBoxLabel.setVisible(false);
        vBoxText.setVisible(false);
        btnProcesar.setVisible(false);
    }

    private void mostrandoParametros() {
        vBoxLabel.setVisible(true);
        vBoxText.setVisible(true);
        btnProcesar.setVisible(true);
    }

    private void asignandoVariables() {
        JSONParser parser = new JSONParser();
        this.alert = true;
        this.alertEscape = true;
        this.primerLogueo = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = DatosEnCaja.getFacturados();
                JSONObject jsonFactCab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                FacturaVentaEsteticaFXMLController.setCabFactura(jsonFactCab);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
    }

    private void verificandoSupervisor() {
        if (!passwordFieldCodSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(passwordFieldCodSupervisor.getText()));
            if (supervisor != null) {
                mostrandoParametros();
                passwordFieldCodSupervisor.setEditable(false);
                JSONObject usuario = (JSONObject) supervisor.get("usuario");
                textFieldSupervisor.setText(usuario.get("nomUsuario").toString());
                if (primerLogueo) {
                    jsonMotivoCancelaciones();
                    primerLogueo = false;
                }
                this.alert = false;
                this.alertEscape = true;
                choiceBoxMotivos.requestFocus();
            } else {
                ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alerta = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR INCORRECTA.", cancel);
                alerta.showAndWait();
                ocultandoParametros();
                passwordFieldCodSupervisor.setText("");
                this.alertEscape = false;
                if (alerta.getResult() == cancel) {
                    alerta.close();
                } else {
                    alerta.close();
                }
            }
        } else {
            ButtonType cancel = new ButtonType("Cerrar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR VACÍA.", cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == cancel) {
                alert.close();
            } else {
                alert.close();
            }
        }
    }

    private void procesar() {
        if (seleccionMotivo()) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            Alert alerts = new Alert(Alert.AlertType.WARNING, "DEBE SELECCIONAR UN MOTIVO.", ok);
            this.alert = true;
            alerts.showAndWait();
            if (alerts.getResult() == ok) {
                alerts.close();
            }
        } else if (procesandoCancelacion()) {
            eliminarAuxiliarCancelProd();
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            JSONParser parser = new JSONParser();
            if (!jsonDatos.isNull("idDato")) {
                if (jsonDatos.getLong("idDato") != 0) {
                    eliminarCabecera(jsonDatos.getLong("idDato"));
                }
            }
            if (jsonDatos.isNull("fecha_arqueo")) {
                ZoneId zonedId = ZoneId.of("America/Montreal");
                LocalDate today = LocalDate.now(zonedId);
//            System.out.println("today : " + today);
                datos.put("fecha_arqueo", today.toString());
            }

            if (!jsonDatos.isNull("energiaElectrica")) {
                JSONObject talona;
                try {
                    talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
                    tal.setNroActual(tal.getNroActual() - 1);
                    taloDAO.actualizarNroActual(tal);
                } catch (ParseException ex) {
                    Logger.getLogger(CancelacionFacturaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            CancelacionProductoEsteticaFXMLController.detalleArtList = new ArrayList<>();
            actualizarDatos();
            mensajeInformacion("LA CANCELACIÓN FACTURA SE REALIZÓ CON ÉXITO.");
            this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, "/vista/estetica/CancelacionFacturaEsteticaFXML.fxml", 600, 400, true);
        }
    }

    public JSONObject buscarCodSupervisorLocal(String c) {
        try {
            JSONParser parser = new JSONParser();
            Supervisor sup = superDAO.buscarCodSup(c);
            return (JSONObject) parser.parse(gson.toJson(sup.toSupervisorDTO()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private void setCantidadArticulo() {
        JSONParser parser = new JSONParser();
        try {
            JSONArray arrayFact = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            int cantidad = 0;
            for (int i = 0; i < arrayFact.size(); i++) {
                JSONObject json = (JSONObject) parser.parse(arrayFact.get(i).toString());
                cantidad += Integer.parseInt(json.get("cantidad").toString());
            }
            int cantTotal = Integer.parseInt(datos.get("nArticulos").toString());
            datos.put("nArticulos", cantTotal - cantidad);
        } catch (ParseException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
    }

    public void generarChoiceBoxMotivo() {
        JSONParser parser = new JSONParser();
        List<MotivoCancelacionFactura> listMotivos = motivoDAO.listar();
        choiceBoxMotivos.getItems().add(selectMotivoInicial);
        for (MotivoCancelacionFactura moti : listMotivos) {
            try {
                JSONObject mot = (JSONObject) parser.parse(gson.toJson(moti.toBDMotivoCancelacionFacturaDTO()));
                choiceBoxMotivos.getItems().add(mot.get("descripcionMotivoCancelFact").toString());
                motivos.put(mot.get("descripcionMotivoCancelFact"), mot.get("idMotivoCancelFact"));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        if (!choiceBoxMotivos.getItems().isEmpty()) {
            choiceBoxMotivos.getSelectionModel().select(0);
        }
    }

    private boolean seleccionMotivo() {
        return choiceBoxMotivos.getSelectionModel().getSelectedItem().equalsIgnoreCase(selectMotivoInicial);
    }

    private static void actualizarDatos() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("cancelProducto");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("totalApagar");
        datos.remove("energiaElectrica");

        datos.remove("arrayCheque");
        datos.remove("asociacionTicket");

        datos.remove("idDato");

        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(new JSONObject());
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        manejo.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
    }

    private void registrarCancelFactDet(long idRango) {
        JSONArray jsonArrayFactDet = new JSONArray();
        JSONParser parser = new JSONParser();
        //**********************************************************************
        for (int i = 0; i < FacturaVentaEsteticaFXMLController.getDetalleArtList().size(); i++) {
            JSONObject fact = null;
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            try {
                //ACTUALIZACIÓN DETALLE / DESCUENTO
                fact = (JSONObject) parser.parse(FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i).get("articulo").toString());
                FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i).put("codArticulo", Long.parseLong(fact.get("codArticulo").toString()));
            } catch (ParseException ex) {
                Logger.getLogger(CancelacionFacturaEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }

            FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i).put("idCancelFact", idRango);
            FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i).remove("facturaClienteCab");

            jsonArrayFactDet.add(FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i));
        }
        for (int i = 0; i < CancelacionProductoEsteticaFXMLController.getDetalleArtList().size(); i++) {
            //ACTUALIZACIÓN DETALLE / DESCUENTO

            //ACTUALIZACIÓN DETALLE / DESCUENTO
            CancelacionProductoEsteticaFXMLController.getDetalleArtList().get(i).put("idCancelFact", idRango);
            CancelacionProductoEsteticaFXMLController.getDetalleArtList().get(i).remove("facturaClienteCab");
            jsonArrayFactDet.add(CancelacionProductoEsteticaFXMLController.getDetalleArtList().get(i));
        }
        //**********************************************************************
        String sql = "INSERT INTO desarrollo.cancel_fact_detalle(descripcion_dato, operacion, fecha) "
                + "VALUES ('" + jsonArrayFactDet + "', 'insertar', 'now()')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION FACTURA DETALLE ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());

            }
        }
    }

    private boolean eliminarAuxiliarCancelProd() {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "DELETE FROM desarrollo.auxiliar_cancel_prod";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* DATOS ELIMINADOS DEL AUXILIAR CANCELACION PRODUCTO ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }

        ConexionPostgres.cerrar();
        return valor;
    }

    private void eliminarCabecera(long id) {
        ConexionPostgres.conectar();
        String sql = "DELETE FROM desarrollo.cabecera WHERE id_dato=" + id;
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* DATOS ELIMINADOS DEL CABECERA FACTURA ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        ConexionPostgres.cerrar();
    }
}
