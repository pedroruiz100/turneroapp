/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoFacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.impl.ClienteDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaClienteCabHistoricoDAOImpl;
import com.javafx.controllers.caja.FuncionarioFXMLController;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Ticket;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 */
@Controller
@ScreenScoped
public class MensajeFinalEsteticaFXMLController extends BaseScreenController implements Initializable {

    public static void setTotalAbonado(int aTotalAbonado) {
        totalAbonado = aTotalAbonado;
    }

    public static void setVuelto(int aVuelto) {
        vuelto = aVuelto;
    }

    public static void setTotal(int aTotal) {
        total = aTotal;
    }

    public static void setDescuento(int aDescuento) {
        descuento = aDescuento;
    }

    public static void setTotalAPagar(int aTotalAPagar) {
        totalAPagar = aTotalAPagar;
    }

    public static void setEfectivo(int aEfectivo) {
        efectivo = aEfectivo;
    }

    public static void setTarjCred(int aTarjCred) {
        tarjCred = aTarjCred;
    }

    public static void setTarjDeb(int aTarjDeb) {
        tarjDeb = aTarjDeb;
    }

    public static void setVale(int aVale) {
        vale = aVale;
    }

    public static void setCheque(int aCheque) {
        cheque = aCheque;
    }

    public static void setAsoc(int aAsoc) {
        asoc = aAsoc;
    }

    public static void setNotaCred(int aNotaCred) {
        notaCred = aNotaCred;
    }

    public static void setRetencion(int aRetencion) {
        retencion = aRetencion;
    }

    public static void setDolar(String aDolar) {
        dolar = aDolar;
    }

    public static void setReal(String aReal) {
        real = aReal;
    }

    public static void setPeso(String aPeso) {
        peso = aPeso;
    }

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    static RangoFacturaClienteCabHistoricoDAO rangoHistoricoDAO = new RangoFacturaClienteCabHistoricoDAOImpl();
    static ClienteDAO clienteDAO = new ClienteDAOImpl();
    //
    private final String patternFechaHora = "dd/MM/yyyy HH:mm";
    private final String patternFecha = "dd/MM/yyyy";
    private final String patternFechaHoraMay = "EEEE, d MMM yyyy HH:mm:ss";
    //
    private int montoDonacion;
    private static int totalAbonado;
    private static int vuelto;
    private static int total;
    private static int descuento;
    private static int totalAPagar;
    //
    private static int efectivo;
    private static int tarjCred;
    private static int tarjDeb;
    private static int vale;
    private static int cheque;
    private static int asoc;
    private static int notaCred;
    private static String dolar;
    private static String real;
    private static String peso;
    private static int retencion;
    //
    private Ticket ticket;
    //campo númerico
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campo númerico
    JSONObject objetoJSON = null;
    boolean primeraEntrada;

    //CRUCIJUEGOS
    public static boolean estadoCrucijuegos = false;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneMsjFinal;
    @FXML
    private Label labelTitulo;
    @FXML
    private Button buttonNoRedondeo;
//    private Button buttonRedondeo;
    @FXML
    private TextField textFieldDonacion;
    @FXML
    private AnchorPane anchorPaneRedondeo;
    @FXML
    private Label labelVueltoSi;
    @FXML
    private Label labelRedondeo;
    @FXML
    private Label labelVueltoMontoSi;
    @FXML
    private Label labelVueltoNo;
    @FXML
    private Label labelVueltoMontoNo;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonNoRedondeoAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneMsjFinalKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        objetoJSON = new JSONObject();
        primeraEntrada = true;
        numValidator = new NumberValidator();
        intValidator = new IntegerValidator();
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
        if (vuelto != 0) {
            labelVueltoMontoNo.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(vuelto))));
            labelVueltoMontoSi.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(vuelto))));
        }
        listenMsjFinal();
        numValidator = new NumberValidator();
        procesandoDonacion();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (json.isNull("vueltoSi")) {
            String vueltoYes = numValidator.numberValidator(labelVueltoMontoSi.getText());
            if (!vueltoYes.isEmpty()) {
                int vueltoSi = Integer.parseInt(vueltoYes);
                datos.put("vueltoSi", vueltoSi);
            } else {
                datos.put("vueltoSi", 0);
            }
        } else {
            int vueltoSi = Integer.parseInt(datos.get("vueltoSi").toString());
            labelVueltoMontoSi.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(vueltoSi + "")));
        }
        if (json.isNull("vueltoNo")) {
            String vueltoNou = numValidator.numberValidator(labelVueltoMontoNo.getText());
            if (!vueltoNou.isEmpty()) {
                int vueltoNo = Integer.parseInt(vueltoNou);
                datos.put("vueltoNo", vueltoNo);
            } else {
                datos.put("vueltoNo", 0);
            }
        } else {
            int vueltoNo = Integer.parseInt(datos.get("vueltoNo").toString());
            labelVueltoMontoNo.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(vueltoNo + "")));
        }
        if (json.isNull("dona")) {
            String donac = numValidator.numberValidator(textFieldDonacion.getText());
            if (!donac.isEmpty()) {
                int don = Integer.parseInt(donac);
                datos.put("dona", don);
            } else {
                datos.put("dona", 0);
            }
        } else {
            int don = Integer.parseInt(datos.get("dona").toString());
            montoDonacion = don;
            textFieldDonacion.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(don + "")));
        }
        actualizarDatosBD();
        selectAuxiliarCancelProd();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAFactura() {
        this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, "/vista/estetica/MensajeFinalEsteticaFXML.fxml", 773, 497, true);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenMsjFinal() {
        textFieldDonacion.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!primeraEntrada) {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldDonacion.setText(param);
                            textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                            procesandoDonacion();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldDonacion.setText("");
                            textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                            procesandoDonacion();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldDonacion.setText(param);
                                    textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                                    procesandoDonacion();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldDonacion.setText("");
                                textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                                procesandoDonacion();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldDonacion.setText(oldValue);
                            textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                            procesandoDonacion();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldDonacion.setText(param);
                        textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                        procesandoDonacion();
                    });
                }
            }
//            }
        });
        buttonNoRedondeo.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldDonacion.requestFocus();
                }
            }
        });
//        buttonRedondeo.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
//            @Override
//            public void handle(KeyEvent event) {
//                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
//                    textFieldDonacion.requestFocus();
//                }
//            }
//        });
    }

    private void keyPress(KeyEvent event) {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            try {
                int montoFact = 0;
                if (!json.isNull("montoFacturado")) {
                    montoFact = Integer.parseInt(datos.get("montoFacturado").toString());
                }
                int montoCajero = 0;
                if (!json.isNull("montoCajero")) {
                    montoCajero = Integer.parseInt(datos.get("montoCajero").toString());
                }
                int vuel = 0;
                if (!json.isNull("vuelto")) {
                    vuel = Integer.parseInt(datos.get("vuelto").toString());
                }

                String vueltoNow = numValidator.numberValidator(labelVueltoMontoNo.getText());
                int vueltoInt = Integer.parseInt(vueltoNow);
                datos.put("vuelto", (vuel + vueltoInt));

                if (!json.isNull("totalApagar")) {
                    totalAPagar = Integer.parseInt(datos.get("totalApagar").toString());
                }
                datos.put("montoFacturado", (montoFact + totalAPagar));

                int contFa = 0;
                if (!json.isNull("contFact")) {
                    contFa = Integer.parseInt(datos.get("contFact").toString());
                }

                datos.put("contFact", (contFa + 1));
                datos.put("sumFact", (montoFact + totalAPagar));
                //POR CADA CAJERO
                datos.put("montoCajero", (montoCajero + totalAPagar));
                int sumEfect = 0;
                if (!json.isNull("sumEfectivo")) {
                    sumEfect = Integer.parseInt(datos.get("sumEfectivo").toString());
                }
                datos.put("sumEfectivo", (sumEfect + Integer.valueOf(numValidator.numberValidator(labelVueltoMontoNo.getText()))));
                //FIN
                JSONObject cajas = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject tipoCaja = (JSONObject) cajas.get("tipoCaja");
                if ((long) tipoCaja.get("idTipoCaja") == 1) {//minorista
                    imprimiendoFacturaMin(false);
                    creandoFactCabHistorico();
                } else if ((long) tipoCaja.get("idTipoCaja") == 2) {
                    imprimiendoFacturaMay(false);
                }//mayorista...

                JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
                JSONObject caj = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject jsonFuncionario = (JSONObject) Identity.getUsuario().get("funcionario");
                Timestamp today = new Timestamp(System.currentTimeMillis());

                JSONObject jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                String numFact = "";
                if (!json.isNull("nroFact") && datos.get("nroFact") != null) {
                    numFact = datos.get("nroFact").toString();
                } else {
                    numFact = jsonCabecera.get("nroFactura").toString();
                }

                String nomCajero = Utilidades.encodingAlambrado(jsonFuncionario.get("nombre").toString() + " " + jsonFuncionario.get("apellido").toString());
                if (!json.isNull("arrayCheque")) {
                    JSONArray array = (JSONArray) parser.parse(datos.get("arrayCheque").toString());

                    for (Object dato : array) {
                        StringTokenizer st = new StringTokenizer(dato.toString(), "-");
                        String entidad = st.nextElement().toString();
                        String nro = st.nextElement().toString();
                        String monto = st.nextElement().toString().replace("(Borrar)", "");
                        mensajeAlert("IMPRIMIR EL CHEQUE NRO: <<" + nro + ">> ENTIDAD: <<" + entidad.toUpperCase() + ">> MONTO: <<" + monto + ">>");
                        Ticket t = new Ticket();
                        t.slipCheque(empresa.get("descripcionEmpresa").toString(), monto, caj.get("nroCaja").toString(), nomCajero, numFact, today.toString());
                        t.print();
                    }
                }

                if (!json.isNull("asociacionTicket")) {
                    String monto = datos.get("asociacionTicket").toString();
                    mensajeAlert("IMPRIMIR ASOCIACION MONTO: <<" + monto + ">>");
                    Ticket t = new Ticket();
                    t.slipCheque(empresa.get("descripcionEmpresa").toString() + " Asoc.", monto, caj.get("nroCaja").toString(), nomCajero, numFact, today.toString());
                    t.print();
                }

                actualizarDatos();
                resetPago();
                navegandoAFactura();
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }

        }
        //SeteandoDatos de FacturaClienteCab
//        if (keyCode == event.getCode().ESCAPE || keyCode == event.getCode().ENTER) {
        if (keyCode == event.getCode().ENTER) {
            cabeceraProcesada();
            datos.put("uuidCassandraActual", "");
//            datos.remove("uuidCassandraActual");
            datos.put("idRangoFacturaActual", 0);
            datos.put("idFactClienteCabServidor", 0);
            datos.put("insercionFacturaVentaCab", false);
            datos.put("actualizacionLocal", false);
            datos.put("ventaServer", false);
            datos.put("insercionFacturaVentaCabLocal", false);
            datos.put("nroFact", "");
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void resetPago() {
        efectivo = 0;
        tarjCred = 0;
        tarjDeb = 0;
        vale = 0;
        cheque = 0;
        asoc = 0;
        notaCred = 0;
        dolar = "";
        real = "";
        peso = "";
        retencion = 0;
    }

    private void procesandoDonacion() {//redondeo en base a Gs 500...
        if (vuelto != 0) {
            int vueltoDos = 0;
            if (primeraEntrada) {
                montoDonacion = vuelto % 500;
                String dona = numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(montoDonacion)));
                textFieldDonacion.setText(dona);
                textFieldDonacion.positionCaret(textFieldDonacion.getLength());
                primeraEntrada = false;
            } else if (!textFieldDonacion.getText().contentEquals("")) {
                montoDonacion = Integer.valueOf(numValidator.numberValidator(textFieldDonacion.getText()));
            } else {
                montoDonacion = 0;
            }
            vueltoDos = vuelto - montoDonacion;
            if (vueltoDos < 0) {
                labelVueltoMontoSi.setText("Gs 0");
            } else {
                labelVueltoMontoSi.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(vueltoDos))));
            }
        }
    }

    //IMPRESIÓN MINORISTA -> -> -> *********************************************
    private void imprimiendoFacturaMin(boolean donacion) {

        try {
            numValidator = new NumberValidator();
            JSONParser parser = new JSONParser();
            String articulos = "";
            int size = FacturaVentaEsteticaFXMLController.getDetalleArtList().size();
            String cantTotalArt = String.valueOf(size);
            org.json.JSONObject json = new org.json.JSONObject(datos);
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            String cantArt = "";
            String precioUnitario = "";
            String precioDetalle = "";
            long gravada10 = 0l;
            long gravada5 = 0l;
            long exenta = 0l;
            String nombreCliente = "";
            String rucCliente = "";
//            JSONObject jsonCliente = (JSONObject) FacturaVentaEsteticaFXMLController.getCabFactura().get("cliente");
            JSONObject jsonFacturaCab = (JSONObject) parser.parse(jsonFact.get("facturaClienteCab").toString());
            JSONObject jsonCliente = (JSONObject) jsonFacturaCab.get("cliente");
            if (jsonCliente == null) {
                org.json.JSONObject jsonFC = new org.json.JSONObject(jsonFacturaCab);
                if (jsonFC.isNull("cliente")) {
                    nombreCliente = "SIN NOMBRE";
                    rucCliente = "XXX";
                } else {
                    org.json.JSONObject jsonCli = jsonFC.getJSONObject("cliente");
                    Cliente c = clienteDAO.getById(jsonCli.getLong("idCliente"));
                    nombreCliente = c.getNombre();
                    if (c.getApellido() != null) {
                        nombreCliente += " " + c.getApellido();
                    }
                    rucCliente = c.getRuc();
                }
            } else {
                org.json.JSONObject jsonCli = new org.json.JSONObject(jsonCliente);
                Cliente c = clienteDAO.getById(jsonCli.getLong("idCliente"));
//                if (!jsonCli.isNull("nombre")) {
                if (c.getNombre() != null) {
                    nombreCliente = c.getNombre();
//                    nombreCliente = (String) jsonCliente.get("nombre");
//                } else if (Long.valueOf(jsonCliente.get("idCliente").toString()) == Utilidades.idClienteSinNombre) {//sin nombre, en caso de existir cancelación de producto, para visualizar cliente "default"
                } else if (c.getIdCliente() == Utilidades.idClienteSinNombre) {//sin nombre, en caso de existir cancelación de producto, para visualizar cliente "default"
                    nombreCliente = "SIN NOMBRE";
                }
//                if (!jsonCli.isNull("apellido")) {
                if (c.getApellido() != null) {
                    nombreCliente = nombreCliente + " " + c.getApellido();
//                    nombreCliente = nombreCliente + " " + jsonCliente.get("apellido").toString();
                }
//                if (!jsonCli.isNull("ruc")) {
                if (c.getRuc() != null) {
//                    rucCliente = (String) jsonCliente.get("ruc");
                    rucCliente = c.getRuc();
//                } else if (Long.valueOf(jsonCliente.get("idCliente").toString()) == Utilidades.idClienteSinNombre) {//sin nombre, en caso de existir cancelación de producto, para visualizar cliente "default"
                } else if (c.getIdCliente() == Utilidades.idClienteSinNombre) {//sin nombre, en caso de existir cancelación de producto, para visualizar cliente "default"
                    rucCliente = "XXX";
                }
            }

            for (JSONObject jsonDetArt : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                size--;
                JSONObject jsonArticulo = null;
                JSONObject jsonIva = null;
                jsonArticulo = (JSONObject) jsonDetArt.get("articulo");
                jsonIva = (JSONObject) jsonArticulo.get("iva");
                cantArt = " " + jsonDetArt.get("cantidad").toString() + "  x";
                precioUnitario = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonDetArt.get("precio").toString()));
                //ajuste de "columnas"**********************************************
                int cantArcCha = cantArt.length();
                while (cantArcCha < 8) {
                    cantArcCha++;
                    cantArt = cantArt + " ";
                }
                int precioUnitarioCha = precioUnitario.length();
                while (precioUnitarioCha < 13) {
                    precioUnitarioCha++;
                    precioUnitario = precioUnitario + " ";
                }
                //ajuste de "columnas"**********************************************
                //gravadas y artículos**********************************************
                int netoGravExePorc = 0;
                if (descuento != 0) {
                    netoGravExePorc = 100 - ((descuento * 100) / total);
                }
                if ((long) jsonDetArt.get("poriva") == 0) {
                    precioDetalle = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonDetArt.get("exenta").toString()));
                    if (descuento == 0) {
                        exenta = exenta + (long) jsonDetArt.get("exenta");
                    } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                            && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                        exenta = exenta + (((long) jsonDetArt.get("exenta") * netoGravExePorc) / 100);
                    }
                } else {
                    precioDetalle = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonDetArt.get("gravada").toString()));
                    if ((long) jsonIva.get("idIva") == 2l) {//5
                        if (descuento == 0) {
                            gravada5 = gravada5 + (long) jsonDetArt.get("gravada");
                        } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                                && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                            gravada5 = gravada5 + (((long) jsonDetArt.get("gravada") * netoGravExePorc) / 100);
                        }
                    } else if ((long) jsonIva.get("idIva") == 3l) {//10
                        if (descuento == 0) {
                            gravada10 = gravada10 + (long) jsonDetArt.get("gravada");
                        } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                                && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                            gravada10 = gravada10 + (((long) jsonDetArt.get("gravada") * netoGravExePorc) / 100);
                        }
                    }
                }
                String porcGrav = "EXE";
                if ((long) jsonDetArt.get("poriva") != 0) {
                    porcGrav = "G" + jsonDetArt.get("poriva");
                }
                String descriArticulo = jsonArticulo.get("descripcion").toString();
                if (jsonArticulo.get("descripcion").toString().length() >= 17) {
                    descriArticulo = jsonArticulo.get("descripcion").toString().substring(0, 17);
                }
                articulos = articulos + jsonDetArt.get("orden").toString() + ". " + jsonArticulo.get("codArticulo").toString() + " " + descriArticulo + " " + porcGrav + "\n"
                        + "         " + cantArt + precioUnitario.replace("Gs ", "") + precioDetalle;
                if (size != 0) {
                    articulos = articulos + "\n";
                }
                //gravadas y artículos**********************************************
            }
            double gravadex10 = Double.parseDouble(String.valueOf(gravada10));
            double gravadex5 = Double.parseDouble(String.valueOf(gravada5));
            double exenex0 = Double.parseDouble(String.valueOf(exenta));

            String grav10 = numValidator.numberFormat("Gs ###,###.###", gravadex10);
            String grav5 = numValidator.numberFormat("Gs ###,###.###", gravadex5);
            String exe = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(exenta)));
            //en el caso de realizar algún descuento por artículo-sección, ciertos detalles y no toda la factura, para ese hipotético caso...
            if (FormaPagoEsteticaFXMLController.isDescFuncionario() || FormaPagoEsteticaFXMLController.isDescPromoTemp()
                    || FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                grav10 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes10())));
                grav5 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes5())));
                exe = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getExenta())));

                gravadex10 = Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes10()));
                gravadex5 = Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes5()));
                exenex0 = Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getExenta()));
            }

            JSONObject jsonCabFactu = FacturaVentaEsteticaFXMLController.getCabFactura();
            org.json.JSONObject jsonVerifi = new org.json.JSONObject(jsonCabFactu);
            Long timestampJSON = 0L;
            if (jsonVerifi.isNull("fechaEmision")) {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                timestampJSON = timestamp.getTime();
            } else {
                timestampJSON = Long.valueOf(FacturaVentaEsteticaFXMLController.getCabFactura().get("fechaEmision").toString());
            }
            Timestamp ts = new Timestamp(timestampJSON);
            String fH = new SimpleDateFormat(patternFechaHora).format(ts);
            String total = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(this.total)));
            String neto = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar)));
            String descu = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento)));
            String efec = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(efectivo)));
            String tarCred = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(tarjCred)));
            String tarDeb = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(tarjDeb)));
            String cheq = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cheque)));
            String val = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(vale)));
            String aso = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(asoc)));
            String notCre = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(notaCred)));
            String ret = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(retencion)));
            String dona = "Gs 0";
            String vuel = "Gs 0";
            if (donacion) {
                dona = textFieldDonacion.getText();
                vuel = labelVueltoMontoSi.getText();
            } else {
                vuel = labelVueltoMontoNo.getText();
            }
            //ajuste de "columnas"**********************************************
            int cantTotalArtCha = cantTotalArt.length();
            while (cantTotalArtCha < 11) {
                cantTotalArtCha++;
                cantTotalArt = cantTotalArt + " ";
            }
            int descCha = descu.length();
            while (descCha < 11) {
                descCha++;
                descu = descu + " ";
            }
            int montoCajero = 0;
            if (!json.isNull("montoCajero")) {
                montoCajero = Integer.parseInt(datos.get("montoCajero").toString());
            }
            int donaCajero = 0;
            if (!json.isNull("donacionCajero")) {
                donaCajero = Integer.parseInt(datos.get("donacionCajero").toString());
            }
            int retiroDineroCajero = 0;
            if (!json.isNull("retiroDineroCajero")) {
                retiroDineroCajero = Integer.parseInt(datos.get("retiroDineroCajero").toString());
            }
            //ajuste de "columnas"**********************************************
            Toaster.setTray("CLIENTE: " + nombreCliente.toUpperCase() + "\nRUC: " + rucCliente.toUpperCase(), "F", "S", 0, "ÚLTIMO VUELTO -> " + vuel + "\n     DONACIÓN -> " + dona);
            int vueltos = Integer.parseInt(datos.get("vuelto").toString());

            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

            int cantEfeRec = 0;
            if (!jsonDatos.isNull("cantEfectivoRecibido")) {
                cantEfeRec = Integer.parseInt(datos.get("cantEfectivoRecibido").toString());
            }

            int montoTotal = cantEfeRec - vueltos;
            int montoDeclarado = retiroDineroCajero;
            int diferencia = montoTotal - montoDeclarado;
            if (diferencia > 5000000) {
                Toaster.setTrayExceso();
            }
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject ciudad = (JSONObject) parser.parse(datos.get("ciudad").toString());
            JSONObject timbrado = (JSONObject) parser.parse(datos.get("timbrado").toString());
            JSONObject cajas = (JSONObject) parser.parse(datos.get("caja").toString());
            JSONObject jsonFuncionario = (JSONObject) Identity.getUsuario().get("funcionario");
            JSONObject jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            String numFact = "";
            if (!json.isNull("nroFact") && datos.get("nroFact") != null) {
                numFact = datos.get("nroFact").toString();
            } else {
                numFact = jsonCabecera.get("nroFactura").toString();
            }

            //CUPONES
//            if (Long.parseLong(numValidator.numberValidator(total)) >= 50000) {
//                if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//                    long cantidadCupones = CalculoCupones.generarCuponesPorMontoTotal(Long.parseLong(numValidator.numberValidator(total)));
//                    CalculoCupones.getMapeo().put("ci", ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
//
//                    JSONObject cliente = (JSONObject) ClienteFielFXMLController.getJsonClienteFiel().get("cliente");
//                    String nomCliente = "";
//                    if (cliente.get("apellido") != null) {
//                        nomCliente = cliente.get("nombre").toString() + " "
//                                + cliente.get("apellido").toString();
//                    } else {
//                        nomCliente = cliente.get("nombre").toString();
//                    }
//
//                    CalculoCupones.getMapeo().put("cliente", nomCliente);
//                    CalculoCupones.getMapeo().put("cantidadCupones", cantidadCupones);
//
//                    long cantidadPromoTemporada = 0l;
//                    for (JSONObject factDetalle : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
//                        try {
//                            long cantidad = Long.parseLong(factDetalle.get("cantidad").toString());
//                            JSONObject art = (JSONObject) parser.parse(factDetalle.get("articulo").toString());
//
//                            if (LoginCajeroFXMLController.articuloPromo.get(art.get("codArticulo")) != null) {
//                                cantidadPromoTemporada = cantidadPromoTemporada + cantidad;
//                            }
//                        } catch (ParseException ex) {
//                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                        }
//                    }
//                    cantidadPromoTemporada = cantidadPromoTemporada * 2;
//                    cantidadCupones = cantidadCupones + cantidadPromoTemporada;
//                    CalculoCupones.getMapeo().put("cantidadCupones", cantidadCupones);
//                }
//            } else {
//                if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//                    boolean datos = false;
//                    long cantidadPromoTemporada = 0l;
//                    for (JSONObject factDetalle : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
//                        try {
//                            long cantidad = Long.parseLong(factDetalle.get("cantidad").toString());
//                            JSONObject art = (JSONObject) parser.parse(factDetalle.get("articulo").toString());
//
//                            if (LoginCajeroFXMLController.articuloPromo.get(art.get("codArticulo")) != null) {
//                                if (!datos) {
//                                    CalculoCupones.getMapeo().put("ci", ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
//
//                                    JSONObject cliente = (JSONObject) ClienteFielFXMLController.getJsonClienteFiel().get("cliente");
//                                    String nomCliente = "";
//                                    if (cliente.get("apellido") != null) {
//                                        nomCliente = cliente.get("nombre").toString() + " "
//                                                + cliente.get("apellido").toString();
//                                    } else {
//                                        nomCliente = cliente.get("nombre").toString();
//                                    }
//
//                                    CalculoCupones.getMapeo().put("cliente", nomCliente);
//                                    datos = true;
//                                }
//                                cantidadPromoTemporada = cantidadPromoTemporada + cantidad;
//                            }
//                        } catch (ParseException ex) {
//                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                        }
//                    }
//                    if (cantidadPromoTemporada > 0) {
//                        cantidadPromoTemporada = cantidadPromoTemporada * 2;
//                        CalculoCupones.getMapeo().put("cantidadCupones", cantidadPromoTemporada);
//                    }
//                }
//            }
            //FIN_CUPONES
//            CRUCIJUEGOS
//            System.out.println("EL TOTAL NOMAS " + this.total);
//            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//                if (this.total >= 50000) {
//                    estadoCrucijuegos = true;
//                }
//            }
//            FIN CRUCIJUEGOS
            if (rucCliente.equalsIgnoreCase("XXX")) {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    if (FuncionarioFXMLController.getJsonFuncionario().get("ci") != null) {
                        rucCliente = FuncionarioFXMLController.getJsonFuncionario().get("ci").toString();
                    }
                    if (FuncionarioFXMLController.getJsonFuncionario().get("apellido") != null) {
                        nombreCliente = FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString() + " "
                                + FuncionarioFXMLController.getJsonFuncionario().get("apellido").toString();
                    } else {
                        nombreCliente = FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString();
                    }
                }
            }
            Timestamp tsInicial = Utilidades.objectToTimestamp(timbrado.get("fecInicial").toString() + " 00:00:00");
            Timestamp tsVenc = Utilidades.objectToTimestamp(timbrado.get("fecVencimiento").toString() + " 00:00:00");
            String fecInicial = new SimpleDateFormat(patternFecha).format(tsInicial);
            String fecVencimiento = new SimpleDateFormat(patternFecha).format(tsVenc);
            double redondeoGrav10 = Math.floor(gravadex10);
            double redondeoGrav5 = Math.floor(gravadex5);
            double redondeoExenta = Math.floor(exenex0);
            double datoDesc10Desc5AndExen0 = redondeoGrav10 + redondeoGrav5 + redondeoExenta;
            double descData = Double.parseDouble(String.valueOf(descuento));
            double sumaTOTAL = datoDesc10Desc5AndExen0 + descData;
            if (sumaTOTAL != Double.parseDouble(String.valueOf(this.total))) {
                if (gravadex10 > 0) {
                    redondeoGrav10 = (Double.parseDouble(String.valueOf(this.total)) - redondeoGrav5 - redondeoExenta) - descData;
                } else if (gravadex5 > 0) {
                    redondeoGrav5 = (Double.parseDouble(String.valueOf(this.total)) - redondeoExenta) - descData;
                } else {
                    redondeoExenta = Double.parseDouble(String.valueOf(this.total)) - descData;
                }
            }
            //DATOS
            long liq10 = 0l;
            long liq5 = 0l;
            if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                    && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                if (gravada10 != 0l) {
//                    double d = Double.parseDouble(gravada10 + "") / 11;
                    double d = redondeoGrav10 / 11;
                    liq10 = Math.round(d);
                }
                if (gravada5 != 0l) {
//                    double c = Double.parseDouble(gravada5 + "") / 1.05;
                    double c = redondeoGrav5 / 1.05;
                    double d = redondeoGrav5 - c;
                    liq5 = Math.round(d);
                }
            } else {//especial, para manejo de posibles descuentos por sección (algunas sí, otras no)
                if (FormaPagoEsteticaFXMLController.getMontoConDes10() != 0l) {
//                    double d = Double.parseDouble(FormaPagoEsteticaFXMLController.getMontoConDes10() + "") / 11;
                    double d = redondeoGrav10 / 11;
                    liq10 = Math.round(d);
                }
                if (FormaPagoEsteticaFXMLController.getMontoConDes5() != 0l) {
//                    double c = Double.parseDouble(FormaPagoEsteticaFXMLController.getMontoConDes5() + "") / 1.05;
//                    double d = FormaPagoEsteticaFXMLController.getMontoConDes5() - c;
                    double d = redondeoGrav5 / 21;
                    liq5 = Math.round(d);
                }
            }
            if (liq10 == 0 && liq5 == 0) {
                double d = redondeoGrav10 / 11;
                liq10 = Math.round(d);
                double e = redondeoGrav5 / 21;
                liq5 = Math.round(e);
            }
            //liquidación IVA
            String liqIva5 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(liq5).replace(".0", "")));
            String liqIva10 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(liq10).replace(".0", "")));
            //FIN DATOS
            String nomCajero = Utilidades.encodingAlambrado(jsonFuncionario.get("nombre").toString() + " " + jsonFuncionario.get("apellido").toString());
            ticket = new Ticket(empresa.get("descripcionEmpresa").toString(), sucursal.get("descripcion").toString(), empresa.get("ruc").toString(), "443150",
                    sucursal.get("callePrincipal").toString() + " " + sucursal.get("nroLocal").toString() + " " + sucursal.get("primeraLateral").toString() + " - " + ciudad.get("descripcion").toString(),
                    timbrado.get("nroTimbrado").toString(), fecInicial, fecVencimiento,
                    //                  ORIGINAL - MODIFICACION EN NUEVA VERSION ES EL NRO FACT
                    //                cajas.get("nroCaja").toString(), fH, Utilidades.patternFactura(datos.get("nroFact").toString()), nomCajero, articulos, grav10, grav5, exe,
                    cajas.get("nroCaja").toString(), fH, Utilidades.patternFactura(numFact), nomCajero, articulos, numValidator.numberFormat("Gs ###,###.###", redondeoGrav10), numValidator.numberFormat("Gs ###,###.###", redondeoGrav5), exe,
                    liqIva10, liqIva5, rucCliente, nombreCliente, cantTotalArt, total.replace("Gs ", ""), descu.replace("Gs ", "  "), neto.replace("Gs ", ""), efec, tarCred, tarDeb, cheq, val, aso, notCre,
                    dona, vuel, dolar, peso, real, ret, "ORIGINAL", "0");
            //Para el Arqueo de Caja seteamos las gravadas, exentas.
//            int gra10 = Integer.parseInt(numValidator.numberValidator(grav10));
//            int gra5 = Integer.parseInt(numValidator.numberValidator(grav5));
            long gra10 = Math.round(redondeoGrav10);
            long gra5 = Math.round(redondeoGrav5);
            long exe0 = Math.round(redondeoExenta);
            long gravad10 = 0;
            long gravad5 = 0;
            long exent = 0;
            if (!json.isNull("gra10")) {
                gravad10 = Integer.parseInt(datos.get("gra10").toString()) + gra10;
            } else {
                gravad10 = Integer.parseInt(gra10 + "");
            }
            if (!json.isNull("gra5")) {
                gravad5 = Integer.parseInt(datos.get("gra5").toString()) + gra5;
            } else {
                gravad5 = Integer.parseInt(gra5 + "");
            }
            if (!json.isNull("exe")) {
                exent = Integer.parseInt(datos.get("exe").toString()) + exe0;
            } else {
                exent = Integer.parseInt(exe0 + "");
            }
            datos.put("gra10", gravad10);
            datos.put("gra5", gravad5);
//            datos.put("gra10", Math.round(redondeoGrav10));
//            datos.put("gra5", Math.round(redondeoGrav5));
            datos.put("exe", exent);
            datos.put("gra", (gravad10 + gravad5));
            //PARA LA INSERCION EN FACTURA CLIENTE CAB HISTORICO
            objetoJSON.put("facturaClienteCab", fact.get("facturaClienteCab"));
            objetoJSON.put("empresa", empresa.get("descripcionEmpresa").toString());
            objetoJSON.put("sucursal", sucursal.get("descripcion").toString());
            objetoJSON.put("nroCaja", Integer.parseInt(cajas.get("nroCaja").toString()));
            objetoJSON.put("ruc", empresa.get("ruc").toString());
            objetoJSON.put("telef", "443150");
            objetoJSON.put("direccion", sucursal.get("callePrincipal").toString() + " " + sucursal.get("nroLocal").toString() + " " + sucursal.get("primeraLateral").toString() + " - " + ciudad.get("descripcion").toString());
            objetoJSON.put("nroTimbrado", timbrado.get("nroTimbrado").toString());
            objetoJSON.put("fecInicial", Utilidades.stringToSqlDate(timbrado.get("fecInicial").toString()).getTime());
            objetoJSON.put("fecVencimiento", Utilidades.stringToSqlDate(timbrado.get("fecVencimiento").toString()).getTime());
            objetoJSON.put("grav10", Math.round(redondeoGrav10));
            objetoJSON.put("grav5", Math.round(redondeoGrav5));
            objetoJSON.put("exenta", Integer.parseInt(numValidator.numberValidator(exe)));
            objetoJSON.put("liqui10", Integer.parseInt(liq10 + ""));
            objetoJSON.put("liqui5", Integer.parseInt(liq5 + ""));
            objetoJSON.put("rucCliente", rucCliente);
            objetoJSON.put("cliente", nombreCliente);
            objetoJSON.put("descuento", descuento);
            objetoJSON.put("total", this.total);
            objetoJSON.put("efectivo", efectivo);
            objetoJSON.put("tarjCred", tarjDeb);
            objetoJSON.put("tarjDeb", tarjCred);
//            objetoJSON.put("tarjCred", tarjCred);
//            objetoJSON.put("tarjDeb", tarjDeb);
            objetoJSON.put("cheque", cheque);
            objetoJSON.put("vale", vale);
            objetoJSON.put("asoc", asoc);
            objetoJSON.put("notCre", notaCred);
            if (dona.isEmpty()) {
                objetoJSON.put("redondeo", 0);
            } else {
                objetoJSON.put("redondeo", Integer.parseInt(numValidator.numberValidator(dona)));
            }
            if (vuel.isEmpty()) {
                objetoJSON.put("vuelto", 0);
            } else {
                objetoJSON.put("vuelto", Integer.parseInt(numValidator.numberValidator(vuel)));
            }
            int dol = 0;
            int pes = 0;
            int rea = 0;
            long cotiDol = 0l;
            long cotiPes = 0l;
            long cotiRea = 0l;
            if (dolar != null && !dolar.equalsIgnoreCase("")) {
                StringTokenizer st0 = new StringTokenizer(dolar, "=");
                String primDolar = st0.nextElement().toString();
                String dolares = st0.nextElement().toString();
                dol = Integer.parseInt(numValidator.numberValidator(dolares));
                StringTokenizer tt0 = new StringTokenizer(primDolar, "x");
                String prim = tt0.nextElement().toString();
                cotiDol = Integer.parseInt(numValidator.numberValidator(tt0.nextElement().toString()));
            }
            if (peso != null && !peso.equalsIgnoreCase("")) {
                StringTokenizer st1 = new StringTokenizer(peso, "=");
                String primPeso = st1.nextElement().toString();
                String pesos = st1.nextElement().toString();
                pes = Integer.parseInt(numValidator.numberValidator(pesos));
                StringTokenizer tt1 = new StringTokenizer(primPeso, "x");
                String prim = tt1.nextElement().toString();
                cotiPes = Integer.parseInt(numValidator.numberValidator(tt1.nextElement().toString()));
            }
            if (real != null && !real.equalsIgnoreCase("")) {
                StringTokenizer st2 = new StringTokenizer(real, "=");
                String primReal = st2.nextElement().toString();
                String reales = st2.nextElement().toString();
                rea = Integer.parseInt(numValidator.numberValidator(reales));
                StringTokenizer tt2 = new StringTokenizer(primReal, "x");
                String prim = tt2.nextElement().toString();
                cotiRea = Integer.parseInt(numValidator.numberValidator(tt2.nextElement().toString()));
            }
            objetoJSON.put("dolar", dol);
            objetoJSON.put("peso", pes);
            objetoJSON.put("realb", rea);
            objetoJSON.put("cotiDolar", cotiDol);
            objetoJSON.put("cotiPeso", cotiPes);
            objetoJSON.put("cotiReal", cotiRea);
            objetoJSON.put("retencion", retencion);
            // FIN DE FACTURA CLIENTE CAB HISTORICO
            FacturaVentaEsteticaFXMLController.setTextFieldCompAnt(numFact);
            datos.put("compAnt", numFact);
            ticket.print();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //IMPRESIÓN MINORISTA -> -> -> *********************************************

    //IMPRESIÓN MAYORISTA -> -> -> *********************************************
    private void imprimiendoFacturaMay(boolean donacion) {
        try {
            String articulos = "";
            JSONParser parser = new JSONParser();
            int size = FacturaVentaEsteticaFXMLController.getDetalleArtList().size();
            String cantTotalArt = String.valueOf(size);
            int sizeEspacio = 23 - size;
            String cantArt = "";
            String precioUnitario = "";
            String precioDetalle = "";
            long gravada10 = 0l;
            long gravada5 = 0l;
            long exenta = 0l;
            JSONObject jsonCliente = (JSONObject) FacturaVentaEsteticaFXMLController.getCabFactura().get("cliente");
            String nombreCliente = "";
            String rucCliente = "";
            if (jsonCliente.get("nombre") != null) {
                nombreCliente = (String) jsonCliente.get("nombre");
            }
            if (jsonCliente.get("apellido") != null) {
                nombreCliente = nombreCliente + " " + jsonCliente.get("apellido").toString();
            }
            if (jsonCliente.get("ruc") != null) {
                rucCliente = (String) jsonCliente.get("ruc");
            }
            for (JSONObject jsonDetArt : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                size--;
                JSONObject jsonArticulo = null;
                JSONObject jsonIva = null;
                jsonArticulo = (JSONObject) jsonDetArt.get("articulo");
                jsonIva = (JSONObject) jsonArticulo.get("iva");
                cantArt = jsonDetArt.get("cantidad").toString();
                precioUnitario = numValidator.numberFormat("###,###.###", Double.parseDouble(jsonDetArt.get("precio").toString()));
                //ajuste de "columnas"**********************************************
                int cantArcCha = cantArt.length();
                while (cantArcCha < 3) {
                    cantArcCha++;
                    cantArt = cantArt + " ";
                }
                int precioUnitarioCha = precioUnitario.length();
                while (precioUnitarioCha < 13) {
                    precioUnitarioCha++;
                    precioUnitario = precioUnitario + " ";
                }
                //ajuste de "columnas"**********************************************
                //gravadas y artículos**********************************************
                int netoGravExePorc = 0;
                if (descuento != 0) {
                    netoGravExePorc = 100 - ((descuento * 100) / total);
                }
                if ((long) jsonDetArt.get("poriva") == 0) {
                    precioDetalle = numValidator.numberFormat("###,###.###", Double.parseDouble(jsonDetArt.get("exenta").toString()));
                    if (descuento == 0) {
                        exenta = exenta + (long) jsonDetArt.get("exenta");
                    } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                            && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                        exenta = exenta + (((long) jsonDetArt.get("exenta") * netoGravExePorc) / 100);
                    }
                } else {
                    precioDetalle = numValidator.numberFormat("###,###.###", Double.parseDouble(jsonDetArt.get("gravada").toString()));
                    if ((long) jsonIva.get("idIva") == 2l) {//5
                        if (descuento == 0) {
                            gravada5 = gravada5 + (long) jsonDetArt.get("gravada");
                        } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                                && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                            gravada5 = gravada5 + (((long) jsonDetArt.get("gravada") * netoGravExePorc) / 100);
                        }
                    } else if ((long) jsonIva.get("idIva") == 3l) {//10
                        if (descuento == 0) {
                            gravada10 = gravada10 + (long) jsonDetArt.get("gravada");
                        } else if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                                && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                            gravada10 = gravada10 + (((long) jsonDetArt.get("gravada") * netoGravExePorc) / 100);
                        }
                    }
                }
                String codArt = jsonArticulo.get("codArticulo").toString();
                String descArt = jsonArticulo.get("descripcion").toString();
                if (codArt.length() < 13) {
                    codArt = codArt + " ";
                }
                if (descArt.length() > 18) {
                    descArt = jsonArticulo.get("descripcion").toString().substring(0, 18);
                }
                descArt = descArt + "  ";
                articulos = articulos + jsonArticulo.get("codArticulo").toString() + " " + cantArt + " " + descArt + " " + precioUnitario + "      \n";
                if (size != 0) {
                    articulos = articulos + "\n";
                }
                //gravadas y artículos**********************************************
            }
            while (sizeEspacio != 0) {
                sizeEspacio--;
                articulos = articulos + "\n";
            }
            String grav10 = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(gravada10)));
            String grav5 = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(gravada5)));
            String exe = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(exenta)));
            //en el caso de realizar algún descuento por artículo-sección, ciertos detalles y no toda la factura, para ese hipotético caso...
            if (FormaPagoEsteticaFXMLController.isDescFuncionario() || FormaPagoEsteticaFXMLController.isDescPromoTemp()
                    || FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                grav10 = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes10())));
                grav5 = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getMontoConDes5())));
                exe = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(FormaPagoEsteticaFXMLController.getExenta())));
            }
            //liquidación IVA
            long liq10 = 0l;
            long liq5 = 0l;
            if (!FormaPagoEsteticaFXMLController.isDescFuncionario() && !FormaPagoEsteticaFXMLController.isDescPromoTemp()
                    && !FormaPagoEsteticaFXMLController.isDescTarjetaFiel()) {
                if (gravada10 != 0l) {
                    liq10 = gravada10 / 11;
                }
                if (gravada5 != 0l) {
                    liq5 = (long) (gravada5 - (gravada5 / 1.05));
                }
            } else {//especial, para manejo de posibles descuentos por sección (algunas sí, otras no)
                if (FormaPagoEsteticaFXMLController.getMontoConDes10() != 0l) {
                    liq10 = (long) FormaPagoEsteticaFXMLController.getMontoConDes10() / 11;
                }
                if (FormaPagoEsteticaFXMLController.getMontoConDes5() != 0l) {
                    liq5 = (long) (FormaPagoEsteticaFXMLController.getMontoConDes5() - (FormaPagoEsteticaFXMLController.getMontoConDes5() / 1.05));
                }
            }
            //liquidación IVA
            Long timestampJSON = Long.valueOf(FacturaVentaEsteticaFXMLController.getCabFactura().get("fechaEmision").toString());
            Timestamp ts = new Timestamp(timestampJSON);
            String fH = new SimpleDateFormat(patternFechaHoraMay).format(ts);
            String liqIva5 = String.valueOf(liq5);
            String liqIva10 = String.valueOf(liq10);
            String total = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(this.total)));
            String neto = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(totalAPagar)));
            String desc = numValidator.numberFormat("###,###.###", Double.parseDouble(String.valueOf(descuento)));
            //ajuste de "columnas"**********************************************
            int cantTotalArtCha = cantTotalArt.length();
            while (cantTotalArtCha < 11) {
                cantTotalArtCha++;
                cantTotalArt = cantTotalArt + " ";
            }
            int descCha = desc.length();
            while (descCha < 11) {
                descCha++;
                desc = desc + " ";
            }
            //ajuste de "columnas"**********************************************
            JSONObject timbrado = (JSONObject) parser.parse(datos.get("timbrado").toString());
            ticket = new Ticket();
            ticket.ticketMayorista("CONT", "80008038-6", timbrado.get("nroTimbrado").toString(), timbrado.get("fecInicial").toString(),
                    timbrado.get("fecVencimiento").toString(), fH, FacturaVentaEsteticaFXMLController.getCabFactura().get("nroFactura").toString(),
                    articulos, grav10, grav5, exe, liqIva10, liqIva5, rucCliente,
                    nombreCliente, total, desc, neto);
            ticket.print();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //IMPRESIÓN MAYORISTA -> -> -> *********************************************

    private static void actualizarDatos() {
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("ventaServer");
        datos.remove("inicio");
        datos.remove("fcccp");
        datos.remove("fcc");
        datos.remove("totalApagar");
        datos.remove("arrayCheque");
        datos.remove("asociacionTicket");
        datos.remove("cancelProducto");
        datos.remove("idDato");
        datos.put("ultimaFactura", FacturaVentaEsteticaFXMLController.getCabFactura().get("nroFactura").toString());

        //NEW BY C.E.
        datos.remove("idClientePendiente");
        datos.remove("idCliPend");
        //FINAL
//        ClienteFielFXMLController.resetParam();
        FuncionarioFXMLController.resetParam();

        CancelacionProductoEsteticaFXMLController.detalleArtList = new ArrayList<>();

        if (jsonDatos.isNull("fecha_arqueo")) {
            ZoneId zonedId = ZoneId.of("America/Montreal");
            LocalDate today = LocalDate.now(zonedId);
//            System.out.println("today : " + today);
            datos.put("fecha_arqueo", today.toString());
        }

        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        manejo.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private static void actualizarDatosBD() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        manejo.setFactura(DatosEnCaja.getFacturados().toString());
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //Apartado 6 - BACKEND
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    ////CREATE, DONACIÓN -> POST
    private boolean creandoDonacion() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonDonacion = new JSONObject();
        jsonDonacion = creandoJsonDonacion(montoDonacion);
        boolean finalizo = false;
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/donacionCliente");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonDonacion.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        finalizo = (Boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    finalizo = registrarDonacionLocal(jsonDonacion.toString());
//                }
//            } else {
        finalizo = registrarDonacionLocal(jsonDonacion.toString());
//            }
//        } catch (IOException | ParseException ex) {
//            finalizo = registrarDonacionLocal(jsonDonacion.toString());
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        return finalizo;
    }
    ////CREATE, DONACIÓN -> POST

    ////CREATE, FACTURA CAB. HISTÓRICO -> POST
    private JSONObject creandoFactCabHistorico() {
        JSONObject factHistorico = new JSONObject();
        long idRangoHistorico = rangoHistoricoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        objetoJSON.put("idFacturaClienteCabHistorico", idRangoHistorico);
        factHistorico = registrarHistoricoLocal(objetoJSON);
        return factHistorico;
    }
    ////CREATE, FACTURA CAB. HISTÓRICO -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //Apartado 7 - LOCAL
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////INSERT, FACTURA CAB. HISTÓRICO
    private JSONObject registrarHistoricoLocal(JSONObject jsonHistorico) {
        JSONObject valor = new JSONObject();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonHistorico.toString() + "','facturaClienteCabHistorico', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a CASSANDRA BD ********");
                valor = jsonHistorico;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        return valor;
    }
    //////INSERT, FACTURA CAB. HISTÓRICO

    //////INSERT, DONACIÓN
    private boolean registrarDonacionLocal(String json) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','donacionCliente', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a CASSANDRA BD ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        return valor;
    }
    //////INSERT, DONACIÓN
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    private void cabeceraProcesada() {
        long uuid = VentasUtiles.recuperarId();
        ConexionPostgres.conectar();
//        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + datos.get("uuidCassandraActual").toString();
        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + uuid;
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz actualizado un registro HANDLER PARANA ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
    }
//Apartado 8 - JSON
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //"DONACIÓN"... ¬¬

    private JSONObject creandoJsonDonacion(int montoDonacion) {
        JSONObject jsonDonacionCliente = new JSONObject();
        //**********************************************************************
        jsonDonacionCliente.put("facturaClienteCab", FacturaVentaEsteticaFXMLController.getCabFactura());
        jsonDonacionCliente.put("montoDonacion", montoDonacion);
        return jsonDonacionCliente;
    }
    //"DONACIÓN"... ¬¬
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    private void selectAuxiliarCancelProd() {
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                insertarDatos(rs.getString("dato"));
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        eliminarDatos();
        ConexionPostgres.cerrar();
    }

    private void insertarDatos(String sql) {
        String r1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),\"";
        String r2 = "\",\"cancelacionProducto\", \"insertar\")";

        String replace1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'";
        String replace2 = "','cancelacionProducto', 'insertar')";

        sql = sql.replace(r1, replace1); //        ConexionPostgres.conectar();
        sql = sql.replace(r2, replace2); //        ConexionPostgres.conectar();
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION PRODUCTO ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
    }

    private void eliminarDatos() {
        String sql = "DELETE FROM desarrollo.auxiliar_cancel_prod";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* DATOS ELIMINADOS DEL AUXILIAR ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
    }

    private void mensajeAlert(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

}
