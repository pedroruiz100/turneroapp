/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.FacturaCabClientePendiente;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.core.domain.TarjetaConvenio;
import com.peluqueria.core.domain.Vales;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ClientePendienteDAO;
import com.peluqueria.dao.FacturaCabClientePendienteDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoChequeDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoEfectivoDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoFuncDAO;
import com.peluqueria.dao.RangoMonedaExtranjeraDAO;
import com.peluqueria.dao.RangoNotaCredDAO;
import com.peluqueria.dao.RangoPromoDAO;
import com.peluqueria.dao.RangoTarjetaConvenioDAO;
import com.peluqueria.dao.RangoTarjetaDAO;
import com.peluqueria.dao.RangoTarjfielDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TarjetaConvenioDAO;
import com.peluqueria.dao.TarjetaDAO;
import com.peluqueria.dao.ValeDAO;
import com.peluqueria.dao.impl.ClienteDAOImpl;
import com.peluqueria.dao.impl.FacturaCabClientePendienteDAOImpl;
import com.peluqueria.dao.impl.FacturaClienteCabDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dto.ClienteDTO;
import com.peluqueria.dto.ClientePendienteDTO;
import com.peluqueria.dto.FacturaCabClientePendienteDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.caja.ClienteFielFXMLController;
import com.javafx.controllers.caja.FuncionarioFXMLController;
import static com.javafx.controllers.estetica.FacturaVentaEsteticaFXMLController.parser;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Descuento;
import com.javafx.util.Fecha;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import com.jfoenix.controls.JFXDialog;
import java.io.IOException;
import static java.lang.Math.toIntExact;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 */
@Controller
public class FormaPagoEsteticaFXMLController extends BaseScreenController implements Initializable {

    private static String datoTotal;
    private static boolean estado = true;
    Tooltip toolTipPanelTarj;

    @Autowired
    TarjetaDAO tarDAO;
    @Autowired
    TarjetaConvenioDAO tarConvenioDAO;
    @Autowired
    ValeDAO valeDAO;
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();

    @Autowired
    private RangoFuncDAO rangoFuncionarioDAO;
    @Autowired
    private RangoTarjetaDAO rangoTarjetaDAO;
    @Autowired
    private RangoTarjfielDAO rangoTarjFielDAO;
    @Autowired
    private RangoEfectivoDAO rangoEfectivoDAO;
    @Autowired
    private RangoTarjetaConvenioDAO rangoTarjetaConvenioDAO;
    @Autowired
    private RangoMonedaExtranjeraDAO rangoMonedaExtraDAO;
    @Autowired
    private RangoChequeDAO rangoChequeDAO;
    @Autowired
    private RangoNotaCredDAO rangoNotaCredDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempArtDAO;
    @Autowired
    private static FacturaClienteCabDAO factDAO = new FacturaClienteCabDAOImpl();
    @Autowired
    public static FacturaCabClientePendienteDAO factCabCliPendDAO = new FacturaCabClientePendienteDAOImpl();
    @Autowired
    private ClientePendienteDAO cpDAO;
    @Autowired
    private ClienteDAO cDAO;

    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    private static JSONObject cabFactura;
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    static boolean caida = false;
    static JSONObject formaDePago;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos
    //tarjeta convenio
    private List<JSONObject> tarjetaConvenioList;
    private boolean cargarTarjetaConvenio;
    private JSONArray tarjetaConvenioJSONArray;
    private HashMap<String, JSONObject> hashJsonComboTarjConvenio;
    //tarjeta convenio
    //tarjeta crédito débito
    private boolean cargarTarjeta;
    private JSONArray tarjetaJSONArray;
    private HashMap<String, Integer> hashListTarj;
    private HashMap<String, JSONObject> hashJsonComboTarjeta;
    //tarjeta crédito débito
    //vale
    private List<JSONObject> valeList;
    private boolean cargarVale;
    private JSONArray valeJSONArray;
    private HashMap<String, JSONObject> hashJsonComboVale;
    //vale
    //cheque
    private ObservableList<String> chequesAsignados;
    private JSONArray arrayCheque;
    private HashMap<String, Integer> hashListCheque;
    private HashMap<String, Integer> hashListNotaCred;
    //cheque
    //tarjeta convenio
    private ObservableList<String> tarjConvAsignadas;
    //tarjeta convenio
    //tarjeta crédito débito
    private ObservableList<String> tarjetasAsignadas;
    //tarjeta crédito débito
    //Nota de créditos
    private ObservableList<String> notaCredAsignadas;
    //Nota de créditos
    //promoción temporada
    private HashMap<JSONObject, Long> hashJsonPromocionDesc;
    long descuentoPromoNf;
    boolean encontradoPromoNf;
    //promoción temporada
    //promoción temporada art.
    private HashMap<JSONObject, Long> hashJsonPromocionArtDesc;
    private List<JSONObject> tarjetaList;
    //promoción temporada art.
    //parámetros total
    long total;
    long descuento;
    long totalAPagar;
    long totalAbonado;
    long donacion;
    long vuelto;
    //parámetros total
    //controlador descuento
    private boolean descTarjeta, descTarjetaConvenio, descDirectivo, descValePorc, descValeMonto;
    private static boolean descTarjetaFiel, descFuncionario, descPromoTemp;
    //controlador descuento
    //sumador descuento
    double descuentoFielSum;
    double descuentoFuncSum;
    double descuentoPromoNfSum;
    double descuentoPromoArtSum;
    double descuentoTarjetaSum;
    double descuentoTarjetaConvSum;
    double descuentoValePorcSum;
    //sumador descuento
    //control, orden de llamada descuento
    boolean entradaPromo1era;
    boolean entradaFunc1era;
    boolean entradaFiel1era;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)
    private static double montoConDes10;
    private static double montoConDes5;
    private static long exenta;
    private HashMap<JSONObject, Long> hashDetallePrecioDesc;
    private HashMap<JSONObject, Long> hashDetallePrecioDescPromoArt;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)

    //calculo auxiliar para descuentos, acumulativo para factura
    HashMap<JSONObject, Long> montoMap5 = new HashMap<>();
    HashMap<JSONObject, Long> montoMap10 = new HashMap<>();
    HashMap<JSONObject, Long> montoMapExe = new HashMap<>();
    //calculo auxiliar para descuentos, acumulativo para factura

    boolean deshabilitar = false;
    boolean cargandoInicial;
    XYChart.Series series;
    long descuentoBarChart;
    boolean toggleEnTarj;
    boolean tarjConvActiva;
    boolean valeActivo;
    boolean recursivoListen;

    public static boolean isDescTarjetaFiel() {
        return descTarjetaFiel;
    }

    public static boolean isDescFuncionario() {
        return descFuncionario;
    }

    public static boolean isDescPromoTemp() {
        return descPromoTemp;
    }

    public static double getMontoConDes5() {
        return montoConDes5;
    }

    public static long getExenta() {
        return exenta;
    }

    public static double getMontoConDes10() {
        return montoConDes10;
    }

    static void totales(String totales) {
        datoTotal = totales;
    }

    //focus
    /*boolean focusMonExtr;
    boolean focusCheque;
    boolean focusNotaCred;
    boolean focusTarjCredDeb;
    boolean focusTarjConv;
    boolean focusVale;
    JFXFillTransition JFXFillTransitionTarjConv;
    JFXFillTransition JFXFillTransitionNotaCred;
    JFXFillTransition JFXFillTransitionCheque;
    JFXFillTransition JFXFillTransitionTarjCredDeb;
    JFXFillTransition JFXFillTransitionMonExtr;
    JFXFillTransition JFXFillTransitionEfectivo;
    JFXFillTransition JFXFillTransitionVale;*/
    JFXDialog jfxDialog;

    @FXML
    private Pane panelBusquedaCliente;
    @FXML
    private TextField txtCiCliente;
    @FXML
    private TextField txtNombreCliente;
    @FXML
    private Button btnBuscarCliente;
    @FXML
    private Pane panelBusquedaFuncionario;
    @FXML
    private Button btnBuscarFuncionario;
    @FXML
    private TextField txtNombreFuncionario;
    @FXML
    private TextField txtCiFuncionario;
    @FXML
    private Pane panelBusquedaClienteFiel;
    @FXML
    private Button btnBuscarClienteFiel;
    @FXML
    private TextField txtNombreClienteFiel;
    @FXML
    private TextField txtCiClienteFiel;
    @FXML
    private Pane panelTipoDePago;
    @FXML
    private Pane panelBusquedaTipoTarj;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonProcesar;
    @FXML
    private TextField textFieldEfectivo;
    @FXML
    private Label labelEfectivo;
    @FXML
    private Label labelCheque;
    @FXML
    private ListView<String> listViewTarjetasAgregadas;
    @FXML
    private Label labelTarjetasAgregadas;
    @FXML
    private Label labelFormaDePago;
    @FXML
    private Label labelTarjCreDeb;
    @FXML
    private ComboBox<String> comboBoxTarjetas;
    @FXML
    private Pane panelBusquedaTipoTarjConv;
    @FXML
    private Label labelTarjConv;
    @FXML
    private ListView<String> listViewTarjetasConvAgregadas;
    @FXML
    private Label labelTarjetasConvAgregadas;
    @FXML
    private ComboBox<String> comboBoxTarjetasConvenio;
    @FXML
    private Pane panelVales;
    @FXML
    private Label labelValeMonto;
    @FXML
    private TextField textFieldMontoVale;
    @FXML
    private TextField textFieldMontoPorc;
    @FXML
    private Pane panelBusquedaCheque;
    @FXML
    private ListView<String> listViewCheques;
    @FXML
    private Label labelCheques;
    @FXML
    private TextField textFieldEntidad;
    @FXML
    private Label labelEntidad;
    @FXML
    private Label labelNroCheque;
    @FXML
    private TextField textFIeldNroCheque;
    @FXML
    private Label labelMontoCheque;
    @FXML
    private TextField textFieldMonto;
    @FXML
    private Button buttonChequeAgregar;
    @FXML
    private Label labelMontoTarjeta;
    @FXML
    private TextField textFieldMontoTarjeta;
    @FXML
    private Label labelCodAuth;
    @FXML
    private TextField textFieldCodAuth;
    @FXML
    private Button buttonTarjetaAgregar;
    @FXML
    private Button buttonTarjetaConvAgregar;
    @FXML
    private ComboBox<String> comboBoxVale;
    @FXML
    private Label labelValeCI;
    @FXML
    private TextField textFieldValeCI;
    @FXML
    private Pane panelDesDirectiva;
    @FXML
    private Label labelDirecPorc;
    @FXML
    private Label labelValeMontoPorc;
    @FXML
    private TextField textFieldMontoValePorc;
    @FXML
    private Label labelNroVale;
    @FXML
    private TextField textFieldNroVale;
    @FXML
    private Pane paneTotales;
    @FXML
    private Label labelTotalesTitulo;
    @FXML
    private VBox vBoxTotales;
    @FXML
    private Label labelTotal;
    @FXML
    private TextField textFieldTotal;
    @FXML
    private Label labelDescuento;
    @FXML
    private TextField textFieldDescuento;
    @FXML
    private Label labelTotalAPagar;
    @FXML
    private TextField textFieldTotalAPagar;
    @FXML
    private Label labelTotalAbonado;
    @FXML
    private TextField textFieldTotalAbonado;
    @FXML
    private Label labelVuelto;
    @FXML
    private TextField textFieldVuelto;
    @FXML
    private CheckBox checkBoxPromo;
    @FXML
    private Pane panePromoTemp;
    @FXML
    private Button buttonPromTemp;
    @FXML
    private Pane panelNotaCredito;
    @FXML
    private Label labelNroNotaCred;
    @FXML
    private TextField textFieldNroNotaCred;
    @FXML
    private Label labelNotaCredMonto;
    @FXML
    private TextField textFieldMontoNotaCred;
    @FXML
    private Pane panelTipoDePagoExtranjera;
    @FXML
    private Label labelReal;
    @FXML
    private Label labelDolar;
    @FXML
    private Label labelPeso;
    @FXML
    private TextField textFieldDolarCant;
    @FXML
    private TextField textFieldRealCant;
    @FXML
    private TextField textFieldPesoCant;
    @FXML
    private TextField textFieldDolarGs;
    @FXML
    private TextField textFieldRealGs;
    @FXML
    private TextField textFieldPesoGs;
    @FXML
    private Label labelMontoRetencion;
    @FXML
    private TextField textFieldMontoRetencion;
    @FXML
    private Label labelRetencion;
    @FXML
    private Pane panelMontoRetencion;
    @FXML
    private AnchorPane anchorPaneFormPagoEstetica;
    @FXML
    private ListView<String> listViewNotasCred;
    @FXML
    private Label txtNroFactura;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void txtCiClienteAction(ActionEvent event) {
    }

    @FXML
    private void txtNombreClienteAction(ActionEvent event) {
    }

    @FXML
    private void btnBuscarClienteAction(ActionEvent event) {
        buscandoCliente();
    }

    @FXML
    private void btnBuscarFuncionarioAction(ActionEvent event) {
        mouseFuncionario();
    }

    @FXML
    private void txtNombreFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void txtCiFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void btnBuscarClienteFielAction(ActionEvent event) {
        mouseTarjetaClienteFiel();
    }

    @FXML
    private void txtNombreClienteFielAction(ActionEvent event) {
    }

    @FXML
    private void txtCiClienteFielAction(ActionEvent event) {
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonProcesarAction(ActionEvent event) {
        finalizandoVenta();
    }

    @FXML
    private void comboBoxTarjetasMouseClicked(MouseEvent event) {
        mouseEventComboBoxTarj(event);
    }

    @FXML
    private void comboBoxTarjetasKeyReleased(KeyEvent event) {
        keyPressComboBoxTarj(event);
    }

    @FXML
    private void comboBoxTarjetasConvenioMouseClicked(MouseEvent event) {
        mouseEventComboBoxTarjConv(event);
    }

    @FXML
    private void comboBoxTarjetasConvenioKeyReleased(KeyEvent event) {
//        keyPressComboBoxTarjConv(event);
    }

    @FXML
    private void buttonChequeAgregarAction(ActionEvent event) {
        agregandoCheque();
    }

    @FXML
    private void buttonTarjetaAgregarAction(ActionEvent event) {
        agregandoTarjeta();
    }

    @FXML
    private void buttonTarjetaConvAgregarAction(ActionEvent event) {
    }

    @FXML
    private void listViewChequesKeyReleased(KeyEvent event) {
        quitandoCheque(event);
    }

    @FXML
    private void listViewTarjetasConvAgregadasKeyReleased(KeyEvent event) {
        quitandoTarjConv(event);
    }

    @FXML
    private void listViewTarjetasAgregadasKeyReleased(KeyEvent event) {
        quitandoTarjeta(event);
    }

    @FXML
    private void comboBoxValeKeyReleased(KeyEvent event) {
//        keyPressComboBoxVale(event);
    }

    @FXML
    private void comboBoxValeMouseClicked(MouseEvent event) {
        mouseEventComboBoxVale(event);
    }

    @FXML
    private void checkBoxPromoAction(ActionEvent event) {
        checkBoxPromoTemp();
    }

    @FXML
    private void buttonPromTempAction(ActionEvent event) {
        checkBoxPromoTempButton();
    }

    @FXML
    private void textFieldMontoPorcKeyReleased(KeyEvent event) {
    }

    @FXML
    private void anchorPaneFormPagoEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void textFieldCodAuthKeyReleased(KeyEvent event) {
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
//        focusTransition();
        descuentoBarChart = 0;
        recursivoListen = false;
        cargandoInicial = true;
        toggleEnTarj = false;
        numValidator = new NumberValidator();
        JSONParser parser = new JSONParser();
        formaDePago = new JSONObject();
        arrayCheque = new JSONArray();
        alert = false;
        exitoInsertarCab = false;
        exitoInsertarDet = false;
        cargarTarjetaConvenio = true;
        cargarTarjeta = true;
        cargarVale = true;
        intValidator = new IntegerValidator();
        listViewCheques.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewTarjetasConvAgregadas.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewTarjetasAgregadas.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewNotasCred.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        comboBoxVale.getItems().add("Seleccione un tipo de vale...");
        comboBoxVale.getSelectionModel().select(0);
        JSONObject jsonCliente = null;
//        lineStrokeTotal.setVisible(false);
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
            JSONObject factura;
            try {
                factura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonFact = new org.json.JSONObject(factura);
                if (!jsonFact.isNull("cliente")) {
                    jsonCliente = (JSONObject) parser.parse(jsonFact.get("cliente").toString());
                }
                if (!jsonFact.isNull("nroFactura")) {
                    txtNroFactura.setText(Utilidades.patternFactura(jsonFact.getString("nroFactura")));
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: (DatosEnCaja.getFacturados() != null)", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("idClientePendiente")) {
            if (jsonCliente == null) {
                cargarClientePorServicio();
            } else {
                if (txtCiCliente.getText().equals("") && txtNombreCliente.getText().equals("")) {
                    long idCliente = Long.parseLong(jsonCliente.get("idCliente").toString());
                    cargarClienteActual(idCliente);
                }
            }
        } else {

            if (jsonCliente == null) {
                txtCiCliente.setText("");
                txtNombreCliente.setText("");
            } else {
                if (txtCiCliente.getText().equals("") && txtNombreCliente.getText().equals("")) {
                    long idCliente = Long.parseLong(jsonCliente.get("idCliente").toString());
                    cargarClienteActual(idCliente);
                }
            }
        }
//        visualizandoCamposVale();
        seteandoTotales();
        listenerCampos();
        tootTip();
        //cheque
        ObservableList<String> list = FXCollections.observableArrayList();
        chequesAsignados = FXCollections.observableArrayList(list);
        //tarjeta convenio
        tarjConvAsignadas = FXCollections.observableArrayList(list);
        //tarjeta
        tarjetasAsignadas = FXCollections.observableArrayList(list);
        //notaCred
        notaCredAsignadas = FXCollections.observableArrayList(list);
        //panel cliente - caja**************************************************
        if (ClienteEsteticaFXMLController.isClienteSi()) {
            if (ClienteEsteticaFXMLController.getJsonCliente() != null) {
                if (ClienteEsteticaFXMLController.getJsonCliente().get("ruc") != null) {
                    txtCiCliente.setText(ClienteEsteticaFXMLController.getJsonCliente().get("ruc").toString());
                } else {
                    txtCiCliente.setText("");
                }
                if (ClienteEsteticaFXMLController.getJsonCliente().get("apellido") != null) {
                    txtNombreCliente.setText(ClienteEsteticaFXMLController.getJsonCliente().get("nombre").toString() + " "
                            + ClienteEsteticaFXMLController.getJsonCliente().get("apellido").toString());
                } else {
                    txtNombreCliente.setText(ClienteEsteticaFXMLController.getJsonCliente().get("nombre").toString());
                }
            }
        }
        //panel cliente - caja**************************************************
        //panel clienteFiel - caja**************************************************
        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            if (ClienteFielFXMLController.getJsonClienteFiel().get("cipas") != null) {
                txtCiClienteFiel.setText(ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
            } else {
                txtCiClienteFiel.setText("");
            }
            JSONObject cliente = (JSONObject) ClienteFielFXMLController.getJsonClienteFiel().get("cliente");
            if (cliente.get("apellido") != null) {
                txtNombreClienteFiel.setText(cliente.get("nombre").toString() + " "
                        + cliente.get("apellido").toString());
            } else {
                txtNombreClienteFiel.setText(cliente.get("nombre").toString());
            }
//            se comenta para que queden independientes
//            if (ClienteEsteticaFXMLController.getJsonCliente() == null) {//por default, sin selección cliente factura, que carge los datos de cliente fiel
//                ClienteEsteticaFXMLController.seteandoParam(cliente);
//                if (cliente.get("apellido") != null) {
//                    txtNombreCliente.setText(cliente.get("nombre").toString() + " "
//                            + cliente.get("apellido").toString());
//                } else {
//                    txtNombreCliente.setText(cliente.get("nombre").toString());
//                }
//                if (cliente.get("ruc") != null) {
//                    txtCiCliente.setText(cliente.get("ruc").toString());
//                } else {
//                    txtCiCliente.setText("");
//                }
//            }
            validandoClientFiel();
        } else {
            invalidandoClientFiel();
            seteandoTotalAbonado();
        }
        //panel clienteFiel - caja**************************************************
        //panel funcionario - caja**************************************************
        if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            if (FuncionarioFXMLController.getJsonFuncionario().get("ci") != null) {
                txtCiFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("ci").toString());
            } else {
                txtCiFuncionario.setText("");
            }
            if (FuncionarioFXMLController.getJsonFuncionario().get("apellido") != null) {
                txtNombreFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString() + " "
                        + FuncionarioFXMLController.getJsonFuncionario().get("apellido").toString());
            } else {
                txtNombreFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString());
            }
            //no hay carga por default para funcionario...
            validandoFuncionario();
        } else if (ClienteFielFXMLController.getJsonClienteFiel() == null) {
            invalidandoFuncionario();
            seteandoTotalAbonado();
        }
        //panel funcionario - caja**************************************************
        //promoción tanto artículo como sección*********************************
        /*if (Descuento.getPromTempJSONArray() != null || Descuento.getPromTempArtJSONArray() != null) {
            if (FuncionarioFXMLController.getJsonFuncionario() == null && ClienteFielFXMLController.getJsonClienteFiel() == null && !cargandoInicial) {
                checkBoxPromoTemp();
            }
        }*/
        if (ClienteFielFXMLController.getJsonClienteFiel() == null && FuncionarioFXMLController.getJsonFuncionario() == null) {
//            editandoBarChart();
        }
        calculandoTotal();
        habilitandoCajaRapida();//efectivo, moneda extranjera, cliente, cliente fiel, promoción temporada... "caja rápida"
        cargandoInicial = false;
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void listenerCampos() {
        comboBoxTarjetas.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if (!recursivoListen) {
                    if (!tarjConvActiva && !valeActivo) {
                        validandoTarjetaListen();
                    } else {
                        if (!comboBoxTarjetas.getSelectionModel().isSelected(0)) {
                            if (validandoTarjetaConvenioTarjCredDeb()) {
                                if (tarjConvActiva) {
                                    mensajeAdv("LA TARJETA CRÉD. / DÉB. SELECCIONADA\nES INCOMPATIBLE CON LA TARJETA CONVENIO CARGADA\nYA QUE AMBAS DISPONEN DTO.");
                                } else {
                                    mensajeAdv("LA TARJETA CRÉD. / DÉB. SELECCIONADA\nES INCOMPATIBLE CON EL VALE U ORDEN DE COMPRA CARGADO.");
                                }
                                textFieldCodAuth.setText("");
                                textFieldMontoTarjeta.setText("");
                                recursivoListen = true;
                                comboBoxTarjetas.getSelectionModel().select(0);
                            }
                        }
                    }
                } else {
                    recursivoListen = false;
                }
            }
        });

//        toggleButton.selectedProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                if (!toggleEnTarj) {
//                    checkBoxPromoTemp();
//                }
//            }
//        });
        textFIeldNroCheque.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(newValue.toString());
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(oldValue.toString());
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFIeldNroCheque.setText(oldValue.toString());
                        textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                    });
                }
            }
        });
        textFieldCodAuth.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(newValue.toString());
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(oldValue.toString());
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                }
            }
        });
        textFieldNroVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(newValue.toString());
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(oldValue.toString());
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                    });
                }
            }
        });
        textFieldValeCI.textProperty().addListener((observable, oldValue, newValue) -> {//existen cédulas viejas con letras...
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(newValue.toString());
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(oldValue.toString());
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                    });
                }
            }
        });
        textFieldEntidad.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(newValue.toString());
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(oldValue.toString());
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                    });
                }
            }
        });
        textFieldMontoValePorc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        if (Integer.valueOf(newValue) < 31 && Integer.valueOf(newValue) > 0) {
                            Platform.runLater(() -> {
                                textFieldMontoValePorc.setText(newValue.toString());
                                textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                                validandoVale();
                                calculandoValePorc();
                                seteandoTotalAbonado();
                            });
                        } else {
                            textFieldMontoValePorc.setText(oldValue);
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoValePorc.setText(oldValue.toString());
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                            validandoVale();
                            calculandoValePorc();
                            seteandoTotalAbonado();
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldMontoValePorc.setText(oldValue.toString());
                        textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        validandoVale();
                        calculandoValePorc();
                        seteandoTotalAbonado();
                    });
                }
            } else {
                validandoCambiandoFormaPago();
            }
        });
        textFieldMontoPorc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        Platform.runLater(() -> {
                            if (Integer.valueOf(newValue) < 31 && Integer.valueOf(newValue) > 0) {
                                textFieldMontoPorc.setText(newValue);
                                textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                                validandoDirectivo();
                            } else {
                                textFieldMontoPorc.setText(oldValue);
                                textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                            }
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoPorc.setText(oldValue);
                            textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                            validandoDirectivo();
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldMontoPorc.setText(oldValue);
                        textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                        validandoDirectivo();
                    });
                }
            } else {
                Platform.runLater(() -> {
                    invalidandoDirectivo();
                    seteandoTotalAbonado();
                    validandoCambiandoFormaPago();
                });
            }
        });
        textFieldEfectivo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                    }
                    if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() != 0) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(param);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText("");
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldEfectivo.setText(param);
                                    textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldEfectivo.setText("");
                                textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(oldValue);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldEfectivo.setText(param);
                        textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                        seteandoTotalAbonado();
                        calculandoValePorc();
                    });
                }
            } else {
                seteandoTotalAbonado();
                validandoCambiandoFormaPago();
                calculandoValePorc();
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldDolarCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                    }
                    if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() > 0) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("US$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(param);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText("");
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("US$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldDolarCant.setText(param);
                                    textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldDolarCant.setText("");
                                textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                cotizacionMoneda();
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(oldValue);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldDolarCant.setText(param);
                        textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldPesoCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                    }
                    if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() > 0) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(param);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText("");
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldPesoCant.setText(param);
                                    textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldPesoCant.setText("");
                                textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                cotizacionMoneda();
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(oldValue);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldPesoCant.setText(param);
                        textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldRealCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                    }
                    if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() > 0) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("R$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(param);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText("");
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("R$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldRealCant.setText(param);
                                    textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldRealCant.setText("");
                                textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                cotizacionMoneda();
                                seteandoTotalAbonado();
                                calculandoValePorc();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(oldValue);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldRealCant.setText(param);
                        textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldMontoRetencion.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    validandoMontoRetencion();
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText(param);
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            validandoMontoRetencion();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText("");
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            invalidandoMontoRetencion();
                            seteandoTotalAbonado();
                            validandoCambiandoFormaPago();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoRetencion.setText(param);
                                    textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                                    validandoMontoRetencion();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoRetencion.setText("");
                                textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                                invalidandoMontoRetencion();
                                seteandoTotalAbonado();
                                validandoCambiandoFormaPago();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText(oldValue);
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            validandoMontoRetencion();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoRetencion.setText(param);
                        textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                        validandoMontoRetencion();
                    });
                }
            }
        });
        textFieldNroNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        textFieldNroNotaCred.setText(newValue.toString());
                        textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldNroNotaCred.setText(oldValue.toString());
                        textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                    });
                }
            }
        });
        textFieldMontoNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (textFieldNroNotaCred.getText().contentEquals("")) {
                Platform.runLater(() -> {
//                    mensajeError("DEBE INGRESAR EL NÚMERO DE NOTA DE CRÉDITO.");
                    textFieldMontoNotaCred.setText("");
                    textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                });
            } else if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    validandoNotaCredito();
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(param);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText("");
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoNotaCred.setText(param);
                                    textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoNotaCred.setText("");
                                textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(oldValue);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoNotaCred.setText(param);
                        textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                    });
                }
            }
        });
        textFieldMontoTarjeta.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(param);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText("");
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoTarjeta.setText(param);
                                    textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoTarjeta.setText("");
                                textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(oldValue);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoTarjeta.setText(param);
                        textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                    });
                }
            }
        });
        textFieldMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMonto.setText(param);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText("");
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMonto.setText(param);
                                    textFieldMonto.positionCaret(textFieldMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMonto.setText("");
                                textFieldMonto.positionCaret(textFieldMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText(oldValue);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMonto.setText(param);
                        textFieldMonto.positionCaret(textFieldMonto.getLength());
                    });
                }
            }
        });
        textFieldMontoVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(param);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            seteandoTotalAbonado();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText("");
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            seteandoTotalAbonado();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoVale.setText(param);
                                    textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                    seteandoTotalAbonado();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoVale.setText("");
                                textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                seteandoTotalAbonado();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(oldValue);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            seteandoTotalAbonado();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoVale.setText(param);
                        textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                        seteandoTotalAbonado();
                    });
                }
            }
        });
        comboBoxVale.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                visualizandoCamposVale();
            }
        });
        comboBoxVale.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                final ListCell<String> cell = new ListCell<String>() {
                    {
                        super.setPrefWidth(100);
                    }

                    @Override
                    public void updateItem(String item,
                            boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item);
                            if (!item.contains("ASOCIACION") && !item.contains("FUNCIONARIO") && !item.contains("Seleccione un tipo de vale...")) {
                                setDisable(true);
                                setOpacity(0.3);
                            }
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void buscandoClienteFiel() {
//        if ("cliente_fiel_caja")) {
        this.sc.loadScreen("/vista/caja/ClienteFielFXML.fxml", 880, 626, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        }
    }

    private void buscandoCliente() {
//        if ("cliente_caja")) {
        this.sc.loadScreen("/vista/estetica/ClienteEsteticaFXML.fxml", 890, 851, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        }
    }

    private void buscandoFuncionario() {
//        if ("funcionario_caja")) {
        this.sc.loadScreen("/vista/caja/FuncionarioFXML.fxml", 890, 410, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//        }
    }

    private void volviendo() {
        this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //Apartado 4 - AVISOS
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void mouseEventComboBoxTarj(MouseEvent event) {
        if (cargarTarjeta) {
            jsonCargandoTarjetas();
            if (comboBoxTarjetas.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                cargarTarjeta = true;
            }
        }
    }

    private void mouseEventComboBoxTarjConv(MouseEvent event) {
        if (cargarTarjetaConvenio) {
            jsonCargandoTarjetasConvenio();
            if (comboBoxTarjetasConvenio.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON TARJETAS CONVENIO DISPONIBLES.");
                cargarTarjetaConvenio = true;
            }
        }
    }

    private void mouseEventComboBoxVale(MouseEvent event) {
        if (cargarVale) {
            jsonCargandoVales();
            if (comboBoxVale.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON VALES DISPONIBLES.");
                cargarVale = true;
            }
        }
    }

    private void keyPressComboBoxTarj(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTarjeta) {
                jsonCargandoTarjetas();
                if (comboBoxTarjetas.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                    cargarTarjeta = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxTarjetas.show();
            } else if (keyCode == event.getCode().ENTER) {
                textFieldMontoTarjeta.requestFocus();
            }
        }
    }

    private void keyPressComboBoxTarjConv(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTarjetaConvenio) {
                jsonCargandoTarjetasConvenio();
                if (comboBoxTarjetasConvenio.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON TARJETAS CONVENIO DISPONIBLES.");
                    cargarTarjetaConvenio = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxTarjetasConvenio.show();
            } else if (keyCode == event.getCode().ENTER) {
                if (alert) {
                    alert = false;
                } else {
                    if (comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem() != null) {
                        agregandoTarjConv();
                    }
                }
                listViewTarjetasConvAgregadas.requestFocus();
                if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    listViewTarjetasConvAgregadas.getSelectionModel().select(0);
                }
            }
        }
    }

    private void keyPressComboBoxVale(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarVale) {
                jsonCargandoVales();
                if (comboBoxVale.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON VALES DISPONIBLES.");
                    cargarVale = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxVale.show();
            } else if (keyCode == event.getCode().ENTER) {
                if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
                    textFieldMontoRetencion.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
                    textFieldNroVale.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                    textFieldNroVale.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
                    textFieldNroVale.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
                    textFieldMontoVale.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ORDEN DE COMPRA")) {
                    textFieldMontoVale.requestFocus();
                } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                    textFieldMontoVale.requestFocus();
                }
            }
        }
    }

    private void mouseTarjetaClienteFiel() {
        if (!FuncionarioFXMLController.isFuncionarioSi()) {
            if (alert) {
                alert = false;
            } else {
                buscandoClienteFiel();
            }
        } else {
            mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nEL PANEL FUNCIONARIO DEBE ESTAR VACÍO.");
        }
    }

    private void mouseFuncionario() {
        if (!ClienteFielFXMLController.isClienteFielSi()) {
            if (alert) {
                alert = false;
            } else {
                buscandoFuncionario();
            }
        } else {
            mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL TARJETA CLIENTE FIEL DEBE ESTAR VACÍO.");
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            if (alert) {
                alert = false;
                if (jfxDialog != null) {
                    jfxDialog.close();
                }
            } else {
                finalizandoVenta();
            }
        }
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
                if (jfxDialog != null) {
                    jfxDialog.close();
                }
            } else {
                buscandoCliente();
            }
        }
        if (keyCode == event.getCode().F6) {
//            if (FuncionarioFXMLController.getJsonFuncionario() != null) {
//                mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nEL PANEL FUNCIONARIO DEBE ESTAR VACÍO.");
//            } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
//                mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nEL PANEL TARJETA CONVENIO DEBE ESTAR VACÍO.");
//            } else if (checkBoxPromo.isSelected()) {
//                mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nPROMOCIÓN TEMPORADA NO DEBE ESTAR SELECCIONADO.");
//            } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
//                mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nDEBE QUITAR TODA TARJETA CRÉDITO/DÉBITO.");
//            } else if (!listViewCheques.getItems().isEmpty()) {
//                mensajeError("NO ESTÁ PERMITIDO AGREGAR CHEQUE\nEN TARJETA CLIENTE FIEL.");
//            } else {
//                buscandoClienteFiel();
//            }
        }
        if (keyCode == event.getCode().F7) {
//            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//                mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL TARJETA CLIENTE FIEL DEBE ESTAR VACÍO.");
//            } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
//                mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL PANEL TARJETA CONVENIO DEBE ESTAR VACÍO.");
//            } else if (checkBoxPromo.isSelected()) {
//                mensajeError("PARA AGREGAR FUNCIONARIO\nPROMOCIÓN TEMPORADA NO DEBE ESTAR SELECCIONADO.");
//            } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
//                mensajeError("PARA AGREGAR FUNCIONARIO\nDEBE QUITAR TODA TARJETA CRÉDITO/DÉBITO.");
//            } else if (!listViewCheques.getItems().isEmpty()) {
//                mensajeError("NO ESTÁ PERMITIDO AGREGAR CHEQUE\nEN FUNCIONARIO.");
//            } else if (alert) {
//                alert = false;
//            } else {
//                buscandoFuncionario();
//            }
        }
        if (keyCode == event.getCode().F8) {
            if (alert) {
                alert = false;
            } else {
                agregandoTarjeta();
            }
        }
        if (keyCode == event.getCode().F9) {
            if (alert) {
                alert = false;
            } else {
                agregandoCheque();
            }
        }
        if (keyCode == event.getCode().F10) {
//            if (alert) {
//                alert = false;
//            } else {
//                agregandoTarjConv();
//            }
        }
        if (keyCode == event.getCode().F11) {
//            if (alert) {
//                alert = false;
//            } else if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//                mensajeError("PARA AGREGAR PROMOCIÓN TEMPORADA\nEL PANEL CLIENTE FIEL DEBE ESTAR VACÍO.");
//            } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
//                mensajeError("PARA AGREGAR PROMOCIÓN TEMPORADA\n\nEL PANEL FUNCIONARIO DEBE ESTAR VACÍO.");
//            } else if (Descuento.getPromTempJSONArray() == null) {
//                mensajeError("PARA AGREGAR PROMOCIÓN TEMPORADA\nDEBE EXISTIR COMO MÍNIMO UNA PROMOCIÓN TEMPORADA EN VIGENCIA.");
//            } else {
//                checkBoxPromoTempButton();
//            }
        }
        if (keyCode == event.getCode().F12) {
            if (alert) {
                alert = false;
            } else {
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
    }

    private void checkBoxPromoTempButton() {
        checkBoxPromoTemp();
    }

    private void checkBoxPromoTemp() {
        if (checkBoxPromo.isSelected()) {
            if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...") && listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                checkBoxPromo.setTextFill(Paint.valueOf("#3ba40e"));
                validandoPromocion();
            } else {
                checkBoxPromo.setSelected(false);
                checkBoxPromo.setTextFill(Paint.valueOf("#f10e0e"));
                mensajeError("PARA APLICAR PROMOCIÓN TEMPORADA,\nEL PANEL VALE DEBE ESTAR VACÍO,\nEL PANEL TARJETAS CONVENIO DEBE ESTAR VACÍO.");
            }
        } else {
            checkBoxPromo.setTextFill(Paint.valueOf("#f10e0e"));
            invalidandoPromocion();
            seteandoTotalAbonado();
            seteandoDescTarjeta();
            validandoCambiandoFormaPago();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //Apartado 6 - BACKEND
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT
    private boolean actualizandoCabFactura() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (!jsonDatos.isNull("cancelProducto")) {
            FacturaVentaEsteticaFXMLController.cancelacionProd = true;
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        try {
            editandoJsonFactCab();
            if (!FacturaVentaEsteticaFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                org.json.JSONObject json = new org.json.JSONObject(datos);
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                FacturaVentaEsteticaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = FacturaVentaEsteticaFXMLController.getCabFactura().get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroActualmente = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                datos.put("nroFact", nroActualmente);
                FacturaVentaEsteticaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
            }
            if (!jsonDatos.isNull("cancelProducto")) {
                JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
                TalonariosSucursales tal = taloDAO.getById(idTalonario);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
            }
            FacturaVentaEsteticaFXMLController.setCabFactura(registrarFacturaCabeceraLocal(FacturaVentaEsteticaFXMLController.getCabFactura().toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("caida", "forma_pago_estetica");
        if (FacturaVentaEsteticaFXMLController.getCabFactura() != null) {
            exitoInsertarCab = true;
        }
        //para registrar facturaIni
        org.json.JSONObject jsons = new org.json.JSONObject(datos);
        boolean estadoFa = false;
        if (!jsons.isNull("estadoFacturaInicial")) {
            estadoFa = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        boolean estadoInicial = estadoFa;
        if (!estadoInicial) {
            datos.put("facturaInicial", FacturaVentaEsteticaFXMLController.getCabFactura().get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("facturaFinal", FacturaVentaEsteticaFXMLController.getCabFactura().get("nroFactura").toString());
        if (FacturaVentaEsteticaFXMLController.isActualizarDatosCabecera()) {
            caida = false;
        }
        //ENVIAR CABECERA Y FACT_CAB_CLIENTE_PENDIENTE DE MANERA LOCAL
        org.json.JSONObject jsonF = new org.json.JSONObject(datos);
        if (jsonF.isNull("fcc")) {
            if (jsonF.isNull("cancelProducto")) {
                crearCabeceraLocalmente();
                datos.put("fcc", true);
            } else {
                actualizarCabeceraLocalmente();
            }
        }
        if (!jsonF.isNull("idCliPend")) {
            if (jsonF.isNull("fcccp")) {
                try {
                    //NEW PARA INSERTAR LOS DATOS EN LA TABLA factura_cliente.factura_cab_cliente_pendiente
                    insertarFacturaCabClientePendiente();
                    datos.put("fcccp", true);
                    DatosEnCaja.setDatos(datos);
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                } catch (IOException ex) {
                    Utilidades.log.error("IOException", ex.fillInStackTrace());
                }
                ///****** FINISH FOR C.E. *****///
            }
        }

        //VERIFY
        if (!jsonDatos.isNull("cancelProducto")) {
            caida = false;
        }
        if (exitoInsertarCab && !caida) {
            if (FacturaVentaEsteticaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
                JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaVentaEsteticaFXMLController.getCabFactura());
                JSONArray arrayDetalle = new JSONArray();
                for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                    try {
                        JSONObject json = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                        JSONObject art = (JSONObject) parser.parse(json.get("articulo").toString());
                        art.put("fechaAlta", null);
                        art.put("fechaMod", null);
                        JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                        iva.put("fechaAlta", null);
                        iva.put("fechaMod", null);
                        art.put("iva", iva);
                        json.put("articulo", art);
                        arrayDetalle.add(json);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                DatosEnCaja.setDatos(datos);
                DatosEnCaja.setUsers(users);
                DatosEnCaja.setFacturados(fact);
                datos = DatosEnCaja.getDatos();
                users = DatosEnCaja.getUsers();
                fact = DatosEnCaja.getFacturados();
                if (creandoFactDet(arrayDetalle)) {
                    exitoInsertarDet = false;
                }
                gestionandoPago(FacturaVentaEsteticaFXMLController.getCabFactura());
            } else {
                mensajeError("LA FACTURA NO HA SIDO PROCESADA.");
            }
        }
        datos.put("caida", "forma_pago_estetica");
        return exitoInsertarCab;
    }
    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT

    ////CREATE, FACTURA DET. MASIVO -> POST
    private boolean creandoFactDet(JSONArray jsonArray) {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
            exitoInsertarDet = registrarArrayDetalleLocal(jsonArrayFactDet.toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }
    ////CREATE, FACTURA DET. MASIVO -> POST

    ////CREATE, FACTURA CAB. EFECTIVO -> POST
    private boolean creandoCabFacturaEfectivo(JSONObject cabFactura) {
        JSONParser parser = new JSONParser();
        JSONObject cabFacturaEfectivo = new JSONObject();
        boolean finalizo = false;
        long idFact = Long.parseLong(cabFactura.get("idFacturaClienteCab").toString());
        JSONObject obj = new JSONObject();
        obj.put("idFacturaClienteCab", idFact);
        cabFacturaEfectivo = creandoJsonEfectivo(obj);
        long idRango = rangoEfectivoDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaEfectivo.put("idFacturaClienteCabEfectivo", idRango);
        try {
            Map mapeo = registrarFacturaEfectivoLocal(cabFacturaEfectivo);
            cabFacturaEfectivo = (JSONObject) parser.parse(mapeo.get("cabFacturaEfectivo").toString());
            finalizo = (boolean) mapeo.get("finalizo");
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        datos.put("facturaEfectivo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. EFECTIVO -> POST

    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST
    private boolean creandoCabFacturaMonedaExt(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayMonedaExt = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoMonedaExtraDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabMonedaExtranjera", idRango);
                jsonArrayMonedaExt.add(obj);
            }
            finalizo = registrarFacturaMonedaExtranjera(jsonArrayMonedaExt);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaMonedaExtranjera", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST

    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST
    private boolean creandoCabFacturaNotaCred(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoNotaCredDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) parser.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.remove("facturaClienteCabDTO");
                obj.put("facturaClienteCab", facturaClienteCab);
                obj.put("idFacturaClienteCabNotaCredito", idRango);
                obj.put("nroNota", obj.get("nroNota"));
                obj.put("monto", obj.get("monto"));
//                jsonArrayCheque.add(obj);
                finalizo = registrarNotaCreditoLocal(obj.toString());
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST

    //////CREATE, FACTURA CAB. RETENCIÓN -> POST
    private boolean creandoCabFacturaMontoRetencion(JSONObject cabFactura) {
        JSONObject cabFacturaMontoRetencion = new JSONObject();
        boolean finalizo = false;
        cabFacturaMontoRetencion = creandoJsonMontoRetencion(cabFactura);
        finalizo = registrarRetencion(cabFacturaMontoRetencion.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. RETENCIÓN -> POST

    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFiel(JSONObject cabFactura) {
        JSONObject cabFacturaClienteFiel = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjFielDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaClienteFiel = creandoJsonTarjClienteFiel(cabFactura);
        cabFacturaClienteFiel.put("idFacturaClienteCabTarjFiel", idRango);
        cabFacturaClienteFiel = registrarTarjetaFielLocal(cabFacturaClienteFiel);
        if (cabFacturaClienteFiel != null) {
            gestionandoDescuento(cabFacturaClienteFiel, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST

    //////CREATE, FACTURA CAB. CHEQUE -> POST
    private boolean creandoCabFacturaCheque(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayCheque = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoChequeDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) parser.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.put("facturaClienteCabDTO", facturaClienteCab);
                obj.put("idFacturaClienteCabCheque", idRango);
                jsonArrayCheque.add(obj);
            }
            finalizo = realizarFactChequeLocal(jsonArrayCheque);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaCheque", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CHEQUE -> POST

    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjConv(JSONObject cabFactura) {
        JSONObject cabFacturaTarjConv = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjetaConvenioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaTarjConv = creandoJsonTarjetaConvenio(cabFactura);
        cabFacturaTarjConv.put("idFacturaClienteCabTarjConvenio", idRango);
        cabFacturaTarjConv = registrarTarjetaConvenioLocal(cabFacturaTarjConv);
        if (cabFacturaTarjConv != null) {
            gestionandoDescuento(cabFacturaTarjConv, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST

    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionario(JSONObject cabFactura) {
        JSONObject cabFacturaFuncionario = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionario = creandoJsonFuncionario(cabFactura);
        long idRango = rangoFuncionarioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaFuncionario.put("idFacturaClienteCabFuncionario", idRango);
        cabFacturaFuncionario = registrarCabFuncionarioLocal(cabFacturaFuncionario);
        if (cabFacturaFuncionario != null) {
            gestionandoDescuento(cabFacturaFuncionario, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST

    //////CREATE, FACTURA CAB. TARJETA -> POST
    private boolean creandoCabFacturaTarjeta(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayTarjeta = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoTarjetaDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabTarjeta", idRango);
                jsonArrayTarjeta.add(obj);
            }
            jsonArrayTarjeta = registrarCabTarjetaLocal(jsonArrayTarjeta);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (jsonArrayTarjeta != null) {
            finalizo = true;
            if (jsonArrayTarjeta.size() == 1) {
                JSONObject jsonTarj = (JSONObject) jsonArrayTarjeta.get(0);
                gestionandoDescuento(jsonTarj, false);
            }
        }
        datos.put("facturaTarjeta", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. TARJETA -> POST

    //////READ, TARJETA CONVENIO -> GET
    private void jsonCargandoTarjetasConvenio() {
        tarjetaConvenioJSONArray = null;
        cargarTarjetaConvenio = false;
        tarjetaConvenioList = new ArrayList<>();
        hashJsonComboTarjConvenio = new HashMap<>();
        generarComboTarjetaConvenioLocal();
    }
    //////READ, TARJETA CONVENIO -> GET

    //////READ, TARJETA -> GET
    private void jsonCargandoTarjetas() {
        tarjetaJSONArray = null;
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        generarComboTarjetaLocal();
    }
    //////READ, TARJETA -> GET

    //////READ, VALE -> GET
    private void jsonCargandoVales() {
        valeJSONArray = null;
        cargarVale = false;
        valeList = new ArrayList<>();
        hashJsonComboVale = new HashMap<>();
        generarComboValeLocal();
    }
    //////READ, VALE -> GET

    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    ////CREATE, DESCUENTO FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionarioDesc(JSONObject cabFacturaFuncionario) {
        JSONObject cabFacturaFuncionarioDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionarioDesc = creandoJsonDescFuncionario(cabFacturaFuncionario);
        finalizo = registrarDescFuncionario(cabFacturaFuncionarioDesc.toString());
        datos.put("facturaFuncionario", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO FUNCIONARIO -> POST

    ////CREATE, DESCUENTO CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFielDesc(JSONObject cabFacturaFiel) {
        JSONObject cabFacturaClienteFielDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaClienteFielDesc = creandoJsonDescTarjFiel(cabFacturaFiel);
        if (Integer.parseInt(cabFacturaClienteFielDesc.get("montoDesc").toString()) != 0) {
            finalizo = registrarDescTarjFielLocal(cabFacturaClienteFielDesc.toString());
            datos.put("facturaTarjetaFiel", false);
            DatosEnCaja.setDatos(datos);
            datos = DatosEnCaja.getDatos();
        }
        return finalizo;
    }
    ////CREATE, DESCUENTO CLIENTE FIEL -> POST

    ////CREATE, DESCUENTO VALE -> POST
    private boolean creandoCabFacturaValeDesc(JSONObject cabFactura, JSONObject vale) {
        JSONObject cabFacturaValeDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeDesc = creandoJsonDescVale(cabFactura, vale);
        finalizo = registrarDescValeLocal(cabFacturaValeDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO VALE -> POST

    ////CREATE, DESCUENTO TARJETA -> POST
//    private boolean creandoCabFacturaTarjetaDesc(JSONObject cabFacturaTarj) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject cabFacturaTarjetaDesc = new JSONObject();
//        boolean finalizo = false;
//        org.json.JSONObject json = new org.json.JSONObject(datos);
//        try {
//            cabFacturaTarjetaDesc = creandoJsonDescTarj(cabFacturaTarj);
//            System.out.println("INTENTANDO CREAR DESC TARJETA");
//            boolean factTarj = false;
//            if (!json.isNull("ventaServer")) {
//                factTarj = Boolean.parseBoolean(datos.get("facturaTarjeta").toString());
//            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && factTarj && !caida) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/descTarjeta");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(cabFacturaTarjetaDesc.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        finalizo = (Boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    finalizo = registrarDescTarjetaLocal(cabFacturaTarjetaDesc.toString());
//                }
//            } else {
//                finalizo = registrarDescTarjetaLocal(cabFacturaTarjetaDesc.toString());
//            }
//        } catch (IOException | ParseException ex) {
//            finalizo = registrarDescTarjetaLocal(cabFacturaTarjetaDesc.toString());
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
//        DatosEnCaja.setDatos(datos);
//        datos = DatosEnCaja.getDatos();
//        return finalizo;
//    }
    ////CREATE, DESCUENTO TARJETA -> POST
    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjetaConvDesc(JSONObject cabFactura) {
        JSONObject cabFacturaTarjetaConvDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaTarjetaConvDesc = creandoJsonDescTarjConv(cabFactura);
        finalizo = registrarConvenioLocal(cabFacturaTarjetaConvDesc.toString());
        datos.put("facturaTarjetaConv", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST

    ////CREATE, DESCUENTO DIRECTIVO -> POST
    private boolean creandoCabFacturaDirectivoDesc(JSONObject cabFactura) {
        JSONObject cabFacturaDirectivoDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaDirectivoDesc = creandoJsonDescDirectivo(cabFactura);
        finalizo = registrarDescDirectivo(cabFacturaDirectivoDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO DIRECTIVO -> POST

    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST
    private boolean creandoCabFacturaPromTempDesc(JSONObject cabFacturaPromTempDesc) {
        boolean finalizo = false;
        JSONObject jsonPromTemp = null;
        long idRango = rangoPromoTempDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaPromTempDesc.put("idFacturaClienteCabPromoTemp", idRango);
        jsonPromTemp = registrarPromoTemp(cabFacturaPromTempDesc.toString());
        if (jsonPromTemp != null) {
            finalizo = true;
        }
        datos.put("facturaPromo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST

    ////CREATE, VALE -> POST
    private boolean creandoCabFacturaValeMonto(JSONObject cabFactura) {
        JSONObject cabFacturaValeMonto = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeMonto = creandoJsonValeMonto(cabFactura);
        finalizo = registrarValeLocal(cabFacturaValeMonto.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE -> POST

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
//AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

////CREATE, VALE PORC. -> POST
    private boolean creandoCabFacturaValePorc(JSONObject cabFactura) {
        JSONObject cabFacturaValePorc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValePorc = creandoJsonValePorc(cabFactura);
        finalizo = registrarPorcValeLocal(cabFacturaValePorc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE PORC. -> POST
    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    private static void insertarFacturaCabClientePendiente() throws ParseException, IOException {
        JSONParser parser = new JSONParser();
        String inputLine;
        JSONObject JSONFactCabCliPen = null;
        JSONObject factCabCliPen = new JSONObject();

        JSONObject factCab = new JSONObject();
        JSONObject cabFactura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
        factCab.put("idFacturaClienteCab", Long.parseLong(cabFactura.get("idFacturaClienteCab").toString()));

        JSONObject cliPend = new JSONObject();
        cliPend.put("idClientePendiente", Long.parseLong(datos.get("idCliPend").toString()));

        factCabCliPen.put("facturaClienteCab", factCab);
        factCabCliPen.put("clientePendiente", cliPend);
        JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
        JSONObject jsonFCCP = (JSONObject) parser.parse(JSONFactCabCliPen.toString());
        if (jsonFCCP.get("clientePendiente") == null) {
            JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
        }

        if (JSONFactCabCliPen != null) {
            ObjectMapper mapper = new ObjectMapper();
            String jsonObj = factCabCliPen.toString();
            FacturaCabClientePendienteDTO fccpDTO = mapper.readValue(jsonObj, FacturaCabClientePendienteDTO.class);
            FacturaCabClientePendiente fccp = factCabCliPendDAO.insertarObtenerObj(FacturaCabClientePendiente.fromFacturaCabClientePendienteAsociado(fccpDTO));
            if (fccp != null) {
                System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR Y DE MANERA LOCAL");
            }
        }
    }

    //Apartado 7 - LOCAL
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, TARJETA CONVENIO
    public void generarComboTarjetaConvenioLocal() {
        JSONParser parser = new JSONParser();
        List<TarjetaConvenio> tarConv = tarConvenioDAO.listar();
        for (TarjetaConvenio tarCon : tarConv) {
            try {
                JSONObject tarjetaConvenio = (JSONObject) parser.parse(gson.toJson(tarCon.toTarjetaConvenioNombreIdDTO()));
                tarjetaConvenioList.add(tarjetaConvenio);
                comboBoxTarjetasConvenio.getItems().add(tarjetaConvenio.get("descripcion").toString());
                hashJsonComboTarjConvenio.put(tarjetaConvenio.get("descripcion").toString(), tarjetaConvenio);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
    }
    //////READ, TARJETA CONVENIO

    //////READ, VALE
    public void generarComboValeLocal() {
        JSONParser parser = new JSONParser();
        List<Vales> val = valeDAO.listar();
        for (Vales vales : val) {
            try {
                JSONObject vale = (JSONObject) parser.parse(gson.toJson(vales.toValeDTO()));
                valeList.add(vale);
                comboBoxVale.getItems().add(vale.get("descripcionVale").toString());
                ObservableList<String> allOptions
                        = FXCollections.observableArrayList("Easy", "Normal", "Hard");
                hashJsonComboVale.put(vale.get("descripcionVale").toString(), vale);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
    }
    //////READ, VALE

    //////READ, TARJETA
    public void generarComboTarjetaLocal() {
        JSONParser parser = new JSONParser();
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        List<Tarjeta> listTarjeta = tarDAO.listar();
        comboBoxTarjetas.getItems().add("Seleccione una tarjeta...");
        //tarjetas con descuento
        for (JSONObject jsonObjectDescuentoTarjeta : Descuento.getHashDescTarjeta().values()) {
            comboBoxTarjetas.getItems().add(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString());
            hashJsonComboTarjeta.put(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString(), jsonObjectDescuentoTarjeta);
        }
        //tarjetas sin descuento
        for (Tarjeta tar : listTarjeta) {
            try {
                JSONObject tarjeta = (JSONObject) parser.parse(gson.toJson(tar.toTarjetaDTO()));
                comboBoxTarjetas.getItems().add(tarjeta.get("descripcion").toString());
                hashJsonComboTarjeta.put(tarjeta.get("descripcion").toString(), tarjeta);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        comboBoxTarjetas.setCellFactory(param -> new FormaPagoEsteticaFXMLController.MyListCell());
        comboBoxTarjetas.setButtonCell(new FormaPagoEsteticaFXMLController.MyListCell());
    }
    //////READ, TARJETA

    private void insertarCabeceraEliminada(String json, String uuids) {
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuids + ",'" + json + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION PRODUCTO ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.info("ERROR uuids -> " + uuids + "\n");
            Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
    }

    private void setearCabecera(String idFactCab) {
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";

        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                actualizarDatosCabecera(rs.getString("dato"), idFactCab, rs.getLong("id_dato"));
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
    }

    private int selectCancelProdAux() {
        int cancel = 0;
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";

        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cancel += 1;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
        return cancel;
    }

    private void actualizarDatosCabecera(String sql, String idFactCab, long idDato) {
        String replace1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'";
        String replace2 = "','cancelacionProducto', 'insertar')";
        String rep = ",\"cancelacionProducto\", \"insertar\")";
        JSONParser parser = new JSONParser();
        sql = sql.replace(replace1, "");

        sql = sql.replace(replace2, "");

        try {
            JSONObject cancelProd = (JSONObject) parser.parse(sql);
            cancelProd.remove("facturaClienteCab");

            JSONObject factCab = new JSONObject();
            factCab.put("idFacturaClienteCab", idFactCab);

            cancelProd.put("facturaClienteCab", factCab);

            String sqlQUERY = "UPDATE desarrollo.auxiliar_cancel_prod SET dato=\'" + replace1.substring(0, replace1.length() - 1) + "\"" + cancelProd + "\"" + rep + "\' WHERE id_dato=" + idDato;

            ConexionPostgres.conectar();

            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sqlQUERY)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    Utilidades.log.info("******* ACTUALIZADO CANCELACION_PRODUCTO_AUXILIAR ********");
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(FormaPagoEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void seteandoTotalAbonado() {
        donacion = 0;
        vuelto = 0;
        totalAbonado = 0;
        //cheques
        if (!listViewCheques.getItems().isEmpty()) {
            for (String cheque : listViewCheques.getItems()) {
                String[] parts = cheque.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[2]));
            }
        }
        //cheques
        //tarjeta crédito/débito
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            for (String tarjeta : listViewTarjetasAgregadas.getItems()) {
                String[] parts = tarjeta.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[1]));
            }
        }
        //tarjeta crédito/débito
        //nota crédito
        if (!listViewNotasCred.getItems().isEmpty()) {
            for (String nota : listViewNotasCred.getItems()) {
                String[] parts = nota.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[1]));
            }
        }
        //nota crédito
        //moneda local
        if (!textFieldEfectivo.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        }
        //moneda local
        //moneda extrajera
        if (FacturaVentaEsteticaFXMLController.getCotizacionList() != null) {
            if (!textFieldDolarCant.getText().contentEquals("")) {
                int dolarCant = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
                int dolarConversion = dolarCant * FacturaVentaEsteticaFXMLController.getDolar();
                textFieldDolarGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(dolarConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldDolarGs.getText()));
            } else {
                textFieldDolarGs.setText("");
            }
            if (!textFieldRealCant.getText().contentEquals("")) {
                int realCant = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
                int realConversion = realCant * FacturaVentaEsteticaFXMLController.getReal();
                textFieldRealGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(realConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldRealGs.getText()));
            } else {
                textFieldRealGs.setText("");
            }
            if (!textFieldPesoCant.getText().contentEquals("")) {
                int pesoCant = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
                int pesoConversion = pesoCant * FacturaVentaEsteticaFXMLController.getPeso();
                textFieldPesoGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(pesoConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldPesoGs.getText()));
            } else {
                textFieldPesoGs.setText("");
            }
        }
        //moneda extrajera
        //monto retención
        if (!textFieldMontoRetencion.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        }
        //monto retención
        //nota de crédito, porque ya es una lista ahora
//        if (!textFieldMontoNotaCred.getText().contentEquals("")) {
//            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoNotaCred.getText()));
//        }
        //nota de crédito
        //vale
        if (!textFieldMontoVale.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
        }/* else if (!textFieldMontoValePorc.getText().contentEquals("")) {
            descuento = (Long.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
        }*/
        //vale
        //directivo
        if (descDirectivo) {
            if (textFieldMontoPorc.getText().contentEquals("")) {
                descuento = 0;
            } else {
                descuento = (Long.valueOf(numValidator.numberValidator(textFieldMontoPorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            }
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
//            if (descuento > 0) {
//                lineStrokeTotal.setVisible(true);
//            } else {
//                lineStrokeTotal.setVisible(false);
//            }
        } else if (descValePorc) {
            totalAPagar = total - descuento;
        } else {
            totalAPagar = Long.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
        }
        //directivo
        if (totalAbonado >= totalAPagar) {
            labelVuelto.setText("Vuelto");
            textFieldVuelto.setStyle("-fx-background-color: #3CB371; -fx-opacity:  1;");
            vuelto = totalAbonado - totalAPagar;
            if (vuelto > 0) {
                textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(vuelto))));
            } else if (vuelto == 0) {
                textFieldVuelto.setText("Gs 0");
            }
        } else {
            labelVuelto.setText("Faltante");
            long dif = (totalAbonado - totalAPagar) * -1;
            textFieldVuelto.setStyle("-fx-background-color: #F08080; -fx-opacity:  1;");
            textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
        }
        if (totalAbonado > 0) {
            textFieldTotalAbonado.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAbonado))));
        } else {
            textFieldTotalAbonado.setText("Gs 0");
        }
    }

    private void calculandoTarjeta(boolean b) {
    }

    private void desbloqueandoPanelesTarj() {
        toggleEnTarj = true;
//        if (FuncionarioFXMLController.getJsonFuncionario() != null || ClienteFielFXMLController.getJsonClienteFiel() != null) {
//            toggleButton.setSelected(true);
//        }
//        if (FuncionarioFXMLController.getJsonFuncionario() == null) {
//            panelBusquedaTipoTarjConv.setDisable(false);
//            panelVales.setDisable(false);
//            panelBusquedaCheque.setDisable(false);
//        }
        panelTipoDePagoExtranjera.setDisable(false);
        panelTipoDePago.setDisable(false);
        panelNotaCredito.setDisable(false);
        toggleEnTarj = false;
    }

    private void calculandoPromocionTemporada(boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    class MyListCell extends ListCell<String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null || empty) {
                setGraphic(null);
            } else {
                if (item.contains(" || ")) {
                    HBox hBox = new HBox(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/tarjeta_desc.jpg"))), new Label(item));
                    hBox.setAlignment(Pos.CENTER_LEFT);
                    hBox.setSpacing(10);
                    setGraphic(hBox);
                } else {
                    if (item.contentEquals("Seleccione una tarjeta...")) {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    } else {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/tarjeta.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    }
                }
            }
            setText("");
        }
    }

    //////INSERT || UPDATE, FACTURA CAB.
    private JSONObject registrarFacturaCabeceraLocal(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        String sql = "";
        String operacion = "insertar";
        boolean actualizacion = false;
        JSONObject objSon = new JSONObject();
        org.json.JSONObject jsonObj = new org.json.JSONObject(datos);
        long ids = 0L;
        if (!jsonObj.isNull("idFactClienteCabServidor")) {
            ids = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
        }
        long idFact = ids;
        String xuuids = "";
        if (idFact == 0L) {
            if (FacturaVentaEsteticaFXMLController.isCancelacionProd()) {
                actualizacion = true;
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    xuuids = datos.get("uuidCassandraActual").toString();
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + xuuids + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
            } else {
                try {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    //ACTUALIZAR TALONARIO SUCURSALES
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    long idTalonario = Long.parseLong(sucursal.get("idTalonariosSucursales").toString());
                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
                    tal.setNroActual(tal.getNroActual() + 1);
                    taloDAO.actualizarNroActual(tal);
                    //ACTUALIZACIONES EN POSTGRESQL LOCALMENTE
                    //UUID ACTUAL PARA MODIFICAR FACTURA
                    datos.put("uuidCassandraActual", uuid);
                    objSon = (JSONObject) parser.parse(json);
                    FacturaVentaEsteticaFXMLController.setCabFactura(objSon);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + objSon.toString() + "', 'insertar');";
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.info("ERROR sql -> " + sql + "\n");
                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                }
            }
        } else {
            try {
                actualizacion = true;
                JSONObject jsonFactura = (JSONObject) parser.parse(json);
                long idFacServer = 0L;
                if (!jsonObj.isNull("idFactClienteCabServidor")) {
                    idFacServer = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
                }
                jsonFactura.put("idFacturaClienteCab", idFacServer);
                String uuids = "";
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    uuids = datos.get("uuidCassandraActual").toString();
                }
                if (uuids.equals("")) {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + jsonFactura.toString() + "', 'actualizar');";
                } else {
                    long uuid = VentasUtiles.recuperarId();
//                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuids + ";";
                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuid + ";";
                }
            } catch (ParseException ex) {
                Utilidades.log.info("ERROR json\n -> " + json + "\n");
                Utilidades.log.info("ERROR sql -> " + sql + "\n");
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.conectar();
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                if (actualizacion) {
                    estados = (JSONObject) parser.parse(json);
                } else {
                    estados = objSon;
                }
                if (estados.get("cliente") == null) {
                    ClienteDAO cliDAO = new ClienteDAOImpl();
                    ClienteDTO clienteDTOs = new ClienteDTO();
                    ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                    clienteDTOs.setIdCliente(161168L);
                    clienteDTOs.setNombre(cliDTO.getNombre());
                    if (cliDTO.getApellido() != null) {
                        clienteDTOs.setApellido(cliDTO.getApellido());
                    }
                    clienteDTOs.setRuc(cliDTO.getRuc());
                    JSONObject clienteJson = (JSONObject) parser.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR objSon\n -> " + objSon + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.error("SQLException | ParseException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        if (estados.isEmpty()) {
            insertarCabeceraEliminada(json, xuuids);//NUEVO
            org.json.JSONObject jsonFact = new org.json.JSONObject(json);
            String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
            setearCabecera(idFactCab);
            if (actualizacion) {
                try {
                    estados = (JSONObject) parser.parse(json);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            } else {
                estados = objSon;
            }
            if (estados.get("cliente") == null) {
                ClienteDAO cliDAO = new ClienteDAOImpl();
                ClienteDTO clienteDTOs = new ClienteDTO();
                ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                clienteDTOs.setIdCliente(161168L);
                clienteDTOs.setNombre(cliDTO.getNombre());
                if (cliDTO.getApellido() != null) {
                    clienteDTOs.setApellido(cliDTO.getApellido());
                }
                clienteDTOs.setRuc(cliDTO.getRuc());
                JSONObject clienteJson;
                try {
                    clienteJson = (JSONObject) parser.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR clienteDTOs\n -> " + clienteDTOs + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            }
        } else {
            if (selectCancelProdAux() != 0) {
                org.json.JSONObject jsonFact = new org.json.JSONObject(json);
                String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
                setearCabecera(idFactCab);
            }
        }
        return estados;
    }
    //////INSERT || UPDATE, FACTURA CAB.

    //////INSERT, FACTURA DET.
    private boolean registrarArrayDetalleLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, FACTURA DET.

    //////INSERT, FACTURA CAB. CHEQUE
    private boolean realizarFactChequeLocal(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabCheque', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. CHEQUE

    //////INSERT, FACTURA CAB. TARJETA
    private JSONArray registrarCabTarjetaLocal(JSONArray json) {
        JSONArray estados = new JSONArray();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONArray) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. TARJETA

    //////INSERT, FACTURA CAB. FUNCIONARIO
    private JSONObject registrarCabFuncionarioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabFuncionario', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. FUNCIONARIO

    //////INSERT, FACTURA CAB. TARJETA CONVENIO
    private JSONObject registrarTarjetaConvenioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. TARJETA CONVENIO

    //////INSERT, FACTURA CAB. CLIENTE FIEL
    private JSONObject registrarTarjetaFielLocal(JSONObject json) {
        ConexionPostgres.conectar();
        JSONObject obj = new JSONObject();
        JSONParser parser = new JSONParser();
        String sql = "INSERT INTO desarrollo.cabecera_fiel(fecha, descripcion_dato, operacion) VALUES (now(),'" + json.toString() + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                obj = (JSONObject) parser.parse(json.toString());
            } else {
                obj = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return obj;
    }
    //////INSERT, FACTURA CAB. CLIENTE FIEL

    //////INSERT, FACTURA CAB. RETENCIÓN
    private boolean registrarRetencion(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabRetencion', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. RETENCIÓN

    //////INSERT, FACTURA CAB. NOTA CRÉDITO
    private boolean registrarNotaCreditoLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(), '" + json + "','facturaClienteCabNotaCredito', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        return estados;
    }
    //////INSERT, FACTURA CAB. NOTA CRÉDITO

    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA
    private boolean registrarFacturaMonedaExtranjera(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabMonedaExtranjera', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA

    //////INSERT, FACTURA CAB. EFECTIVO
    private Map registrarFacturaEfectivoLocal(JSONObject json) {
        Map mapeo = new HashMap();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabEfectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                mapeo.put("finalizo", true);
                mapeo.put("cabFacturaEfectivo", json);
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return mapeo;
    }
    //////INSERT, FACTURA CAB. EFECTIVO

    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.
    private JSONObject registrarPromoTemp(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPromoTemp', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json);
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.

    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO
    private boolean registrarDescDirectivo(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descuentoDirectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO
    private boolean registrarConvenioLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA
    private boolean registrarDescTarjetaLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA

    //////INSERT, PENDIENTES - DESCUENTO VALE
    private boolean registrarDescValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO VALE

    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL
    private boolean registrarDescTarjFielLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaFiel', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL

    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO
    private boolean registrarDescFuncionario(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descFuncionario', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO

    //////INSERT, PENDIENTES - PORC. VALE
    private boolean registrarPorcValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPorcVale', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - PORC. VALE

    //////INSERT, PENDIENTES - VALE
    private boolean registrarValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabVale', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - VALE
    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //Apartado 8 - JSON
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON FACTURA CABECERA
    private void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            FacturaVentaEsteticaFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            FacturaVentaEsteticaFXMLController.getCabFactura().put("cliente", ClienteEsteticaFXMLController.getJsonCliente());//en nulo default NN
            int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
            FacturaVentaEsteticaFXMLController.getCabFactura().put("montoFactura", monto);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //JSON FACTURA CABECERA

    //JSON FACTURA DETALLE
    private JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < FacturaVentaEsteticaFXMLController.getDetalleArtList().size(); i++) {
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            if (!hashDetallePrecioDesc.isEmpty()) {//algún descuento por sección, fiel; funcionario; temporada...
            } else if (descTarjeta || descTarjetaConvenio || descDirectivo) {//vale por monto y vale porcentaje no entran...
            }
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(FacturaVentaEsteticaFXMLController.getDetalleArtList().get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }
    //JSON FACTURA DETALLE

    //JSON TIPO PAGO EFECTIVO
    private JSONObject creandoJsonEfectivo(JSONObject factCab) {
        JSONObject jsonEfectivo = new JSONObject();
        //**********************************************************************
        jsonEfectivo.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        jsonEfectivo.put("montoEfectivo", monto);
        //**********************************************************************
        return jsonEfectivo;
    }
    //JSON TIPO PAGO EFECTIVO

    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA
    private JSONArray creandoJsonMonedaEx(JSONObject factCab) {
        JSONArray jsonArrayMonedaExt = new JSONArray();
        JSONObject jsonDolar = new JSONObject();
        JSONObject jsonReal = new JSONObject();
        JSONObject jsonPeso = new JSONObject();
        //**********************************************************************
        //List tipo_moneda ordenado por descripción, getCotizacionList
        if (!textFieldDolarGs.getText().contentEquals("")) {
            jsonDolar.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldDolarGs.getText()));
            int montoDolar = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
            jsonDolar.put("montoEfectivo", montoDolar);
            jsonDolar.put("monto_guaranies", montoAGs);
            if (FacturaVentaEsteticaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaVentaEsteticaFXMLController.getCotizacionList().get(0).get("venta"))).intValue();
                jsonDolar.put("cotizacion", cotiz);
                jsonDolar.put("tipoMoneda", FacturaVentaEsteticaFXMLController.getCotizacionList().get(0));
                jsonArrayMonedaExt.add(jsonDolar);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalEsteticaFXMLController.setDolar(textFieldDolarCant.getText() + " x " + cot + " = " + textFieldDolarGs.getText() + "\n");
                //para impresión
            }
        }
        if (!textFieldRealGs.getText().contentEquals("")) {
            jsonReal.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldRealGs.getText()));
            int montoReal = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
            jsonReal.put("montoEfectivo", montoReal);
            jsonReal.put("monto_guaranies", montoAGs);
            if (FacturaVentaEsteticaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaVentaEsteticaFXMLController.getCotizacionList().get(3).get("venta"))).intValue();
                jsonReal.put("cotizacion", cotiz);
                jsonReal.put("tipoMoneda", FacturaVentaEsteticaFXMLController.getCotizacionList().get(3));
                jsonArrayMonedaExt.add(jsonReal);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalEsteticaFXMLController.setReal(textFieldRealCant.getText() + " x " + cot + " = " + textFieldRealGs.getText() + "\n");
                //para impresión
            }
        }
        if (!textFieldPesoGs.getText().contentEquals("")) {
            jsonPeso.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldPesoGs.getText()));
            int montoReal = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
            jsonPeso.put("montoEfectivo", montoReal);
            jsonPeso.put("monto_guaranies", montoAGs);
            if (FacturaVentaEsteticaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaVentaEsteticaFXMLController.getCotizacionList().get(2).get("venta"))).intValue();
                jsonPeso.put("cotizacion", cotiz);
                jsonPeso.put("tipoMoneda", FacturaVentaEsteticaFXMLController.getCotizacionList().get(2));
                jsonArrayMonedaExt.add(jsonPeso);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalEsteticaFXMLController.setPeso(textFieldPesoCant.getText() + " x " + cot + " = " + textFieldPesoGs.getText() + "\n");
                //para impresión
            }
        }
        //**********************************************************************
        return jsonArrayMonedaExt;
    }
    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA

    //JSON TIPO PAGO CHEQUE
    private JSONArray creandoJsonCheque(JSONObject factCab) {
        JSONArray jsonArrayCheque = new JSONArray();
        int montoChequeTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewCheques.getItems().size(); i++) {
            JSONObject jsonCheque = new JSONObject();
            String[] parts = listViewCheques.getItems().get(i).split(" - ");
            jsonCheque.put("facturaClienteCabDTO", factCab);
            jsonCheque.put("descripcion", parts[0]);
            jsonCheque.put("nroCheque", parts[1]);
            int monto = Integer.valueOf(numValidator.numberValidator(parts[2]));
            jsonCheque.put("montoCheque", monto);
            //para impresión
            montoChequeTotal = montoChequeTotal + monto;
            //para impresión
            jsonArrayCheque.add(jsonCheque);
        }
        //para impresión
        MensajeFinalEsteticaFXMLController.setCheque(montoChequeTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayCheque;
    }
    //JSON TIPO PAGO CHEQUE

    //JSON TIPO PAGO TARJETA CONVENIO
    private JSONObject creandoJsonTarjetaConvenio(JSONObject factCab) {//queda abierta la posibilidad de varias tarjetas convenio...
        JSONObject jsonTarjConv = new JSONObject();
        //**********************************************************************
        String parts = listViewTarjetasConvAgregadas.getItems().get(0);
        jsonTarjConv.put("facturaClienteCab", factCab);
        jsonTarjConv.put("tarjetaConvenio", hashJsonComboTarjConvenio.get(parts));
        jsonTarjConv.put("descripcionTarj", parts);
        //todas formas de pago...
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        //todas formas de pago...
        jsonTarjConv.put("monto", monto);
        //**********************************************************************
        return jsonTarjConv;
    }
    //JSON TIPO PAGO TARJETA CONVENIO

    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO
    private JSONArray creandoJsonTarjeta(JSONObject factCab) {
        JSONArray jsonArrayTarjeta = new JSONArray();
        int montoTarjCredTotal = 0;
        int montoTarjDebTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewTarjetasAgregadas.getItems().size(); i++) {
            JSONObject jsonTarjeta = new JSONObject();
            //debe tener descuento
            if (listViewTarjetasAgregadas.getItems().get(i).contains(" || ")) {
                String[] partUno = listViewTarjetasAgregadas.getItems().get(i).split(" \\|\\| ");
                String[] partsDos = partUno[1].split(" - ");
                JSONObject tarj = hashJsonComboTarjeta.get(partsDos[0]);
                JSONObject tarjTipo = (JSONObject) tarj.get("tipoTarjeta");
                jsonTarjeta.put("facturaClienteCab", factCab);
                jsonTarjeta.put("tarjeta", tarj);
                jsonTarjeta.put("descripcionTarj", partsDos[0]);
                jsonTarjeta.put("codAutorizacion", partsDos[2]);
                int monto = Integer.valueOf(numValidator.numberValidator(partsDos[1]));
                jsonTarjeta.put("monto", monto);
                if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                    montoTarjCredTotal = montoTarjCredTotal + monto;
                    MensajeFinalEsteticaFXMLController.setTarjCred(montoTarjCredTotal);
                } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                    montoTarjDebTotal = montoTarjDebTotal + monto;
                    MensajeFinalEsteticaFXMLController.setTarjDeb(monto);
                }
            } else {
                String[] parts = listViewTarjetasAgregadas.getItems().get(i).split(" - ");
                JSONObject tarj = hashJsonComboTarjeta.get(parts[0]);
                JSONObject tarjTipo = (JSONObject) tarj.get("tipoTarjeta");
                jsonTarjeta.put("facturaClienteCab", factCab);
                jsonTarjeta.put("tarjeta", tarj);
                jsonTarjeta.put("descripcionTarj", parts[0]);
                jsonTarjeta.put("codAutorizacion", parts[2]);
                int monto = Integer.valueOf(numValidator.numberValidator(parts[1]));
                jsonTarjeta.put("monto", monto);
                if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                    montoTarjCredTotal = montoTarjCredTotal + monto;
                    MensajeFinalEsteticaFXMLController.setTarjCred(montoTarjCredTotal);
                } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                    montoTarjDebTotal = montoTarjDebTotal + monto;
                    MensajeFinalEsteticaFXMLController.setTarjDeb(monto);
                }
            }
            jsonArrayTarjeta.add(jsonTarjeta);
        }
        //para impresión
        MensajeFinalEsteticaFXMLController.setTarjCred(montoTarjCredTotal);
        MensajeFinalEsteticaFXMLController.setTarjDeb(montoTarjDebTotal);
        //para impresión        
        //**********************************************************************
        return jsonArrayTarjeta;
    }
    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO

    //JSON TIPO PAGO CLIENTE FIEL
    private JSONObject creandoJsonTarjClienteFiel(JSONObject factCab) {
        JSONObject jsonClienteFiel = new JSONObject();
        //**********************************************************************
        JSONObject factura = new JSONObject();
        factura.put("idFacturaClienteCab", Long.parseLong(factCab.get("idFacturaClienteCab").toString()));
        jsonClienteFiel.put("facturaClienteCab", factura);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonClienteFiel.put("monto", monto);
        JSONObject jsonClienteF = ClienteFielFXMLController.getJsonClienteFiel();
        jsonClienteF.remove("fechaAlta");
        jsonClienteF.remove("fechaMod");
        jsonClienteFiel.put("tarjetaClienteFiel", jsonClienteF);
        //**********************************************************************
        return jsonClienteFiel;
    }
    //JSON TIPO PAGO CLIENTE FIEL

    //JSON TIPO PAGO FUNCIONARIO
    private JSONObject creandoJsonFuncionario(JSONObject factCab) {
        JSONObject jsonFuncionario = new JSONObject();
        //**********************************************************************
        jsonFuncionario.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonFuncionario.put("monto", monto);
        jsonFuncionario.put("funcionario", FuncionarioFXMLController.getJsonFuncionario());
        //**********************************************************************
        return jsonFuncionario;
    }
    //JSON TIPO PAGO FUNCIONARIO

    //JSON TIPO PAGO VALE PORCENTAJE
    private JSONObject creandoJsonValePorc(JSONObject factCab) {//acaité
        JSONObject jsonValePorc = new JSONObject();
        //**********************************************************************
        jsonValePorc.put("facturaClienteCab", factCab);
        jsonValePorc.put("vale", hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
        int porcVale = Integer.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText()));
        jsonValePorc.put("porcVale", porcVale);
        Long precioTotal = FacturaVentaEsteticaFXMLController.getPrecioTotal();
        Long descTotal = (precioTotal * porcVale) / 100;
        jsonValePorc.put("monto", descTotal);
        MensajeFinalEsteticaFXMLController.setVale(toIntExact((long) descTotal));
        int nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        jsonValePorc.put("nroVale", nroVale);
        jsonValePorc.put("ci", textFieldValeCI.getText());
        //**********************************************************************
        return jsonValePorc;
    }
    //JSON TIPO PAGO VALE PORCENTAJE

    //JSON TIPO PAGO VALE MONTO
    private JSONObject creandoJsonValeMonto(JSONObject factCab) {
        JSONObject jsonValeMonto = new JSONObject();
        //**********************************************************************
        jsonValeMonto.put("facturaClienteCab", factCab);
        jsonValeMonto.put("vale", hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
        jsonValeMonto.put("monto", monto);
        //para impresión
        if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
            MensajeFinalEsteticaFXMLController.setAsoc(monto);
        } else {
            MensajeFinalEsteticaFXMLController.setVale(monto);
        }
        //para impresión
        Integer nroVale = null;
        if (!textFieldNroVale.getText().contentEquals("")) {
            nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        }
        jsonValeMonto.put("nroVale", nroVale);
        jsonValeMonto.put("ci", textFieldValeCI.getText());
        //**********************************************************************
        return jsonValeMonto;
    }
    //JSON TIPO PAGO VALE MONTO

    //JSON TIPO PAGO NOTA DE CRÉDITO
    private JSONArray creandoJsonNotaCred(JSONObject factCab) {
        JSONArray jsonArrayNota = new JSONArray();
        int montoNotaTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewNotasCred.getItems().size(); i++) {
            JSONObject jsonNota = new JSONObject();
            String[] parts = listViewNotasCred.getItems().get(i).split(" - ");
            jsonNota.put("facturaClienteCabDTO", factCab);
            jsonNota.put("nroNota", parts[0]);
            int monto = Integer.valueOf(numValidator.numberValidator(parts[1]));
            jsonNota.put("monto", monto);
            //para impresión
            montoNotaTotal = montoNotaTotal + monto;
            //para impresión
            jsonArrayNota.add(jsonNota);
        }
        //para impresión
        MensajeFinalEsteticaFXMLController.setNotaCred(montoNotaTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayNota;
    }
    //JSON TIPO PAGO NOTA DE CRÉDITO

    //JSON TIPO PAGO MONTO RETENCIÓN
    private JSONObject creandoJsonMontoRetencion(JSONObject factCab) {
        JSONObject jsonMontoRetencion = new JSONObject();
        //**********************************************************************
        jsonMontoRetencion.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        jsonMontoRetencion.put("monto", monto);
        //**********************************************************************
        return jsonMontoRetencion;
    }
    //JSON TIPO PAGO MONTO RETENCIÓN

    //JSON DESCUENTO TARJETA FUNCIONARIO
    private JSONObject creandoJsonDescFuncionario(JSONObject factCabFuncionario) {//acaité
        JSONObject jsonDescFuncionario = new JSONObject();
        //**********************************************************************
        jsonDescFuncionario.put("facturaClienteCabFuncionarioDTO", factCabFuncionario);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescFuncionario.put("montoDesc", monto);
        int porcVale = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescFuncionario.put("porcentajeDesc", porcVale);
        //**********************************************************************
        return jsonDescFuncionario;
    }
    //JSON DESCUENTO TARJETA FUNCIONARIO

    //JSON DESCUENTO TARJETA CLIENTE FIEL
    private JSONObject creandoJsonDescTarjFiel(JSONObject factCabTarjFiel) {
        JSONObject jsonDescTarjFiel = new JSONObject();
        //**********************************************************************
        jsonDescTarjFiel.put("facturaClienteCabTarjFiel", factCabTarjFiel);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescTarjFiel.put("montoDesc", monto);
        int porcentajeDesc = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjFiel.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjFiel;
    }
    //JSON DESCUENTO TARJETA CLIENTE FIEL

    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO
    private JSONObject creandoJsonDescTarj(JSONObject factCabTarj) {
        JSONObject jsonDescTarj = new JSONObject();
        //**********************************************************************
        jsonDescTarj.put("facturaClienteCabTarjeta", factCabTarj);
        String[] arraySplit = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
        JSONObject jsonDescuentoTarjetaCab = new JSONObject();
        jsonDescuentoTarjetaCab.put("idDescuentoTarjetaCab",
                Long.valueOf(Descuento.getHashDescTarjeta().get(arraySplit[0]).get("idDescuentoTarjetaCab").toString()));
        jsonDescTarj.put("descuentoTarjetaCab", jsonDescuentoTarjetaCab);
        int montoDesc = toIntExact(Math.round(descuentoTarjetaSum));
        jsonDescTarj.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100.0) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarj.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarj;
    }
    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO

    //JSON DESCUENTO TARJETA CONVENIO
    private JSONObject creandoJsonDescTarjConv(JSONObject factCabTarjConv) {
        JSONObject jsonDescTarjConv = new JSONObject();
        //**********************************************************************
        jsonDescTarjConv.put("facturaClienteCabTarjConvenio", factCabTarjConv);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescTarjConv.put("montoDesc", monto);
        int porcentajeDesc = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjConv.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjConv;
    }
    //JSON DESCUENTO TARJETA CONVENIO

    //JSON DESCUENTO VALE
    private JSONObject creandoJsonDescVale(JSONObject facturaClienteCab, JSONObject vale) {
        JSONObject jsonDescVale = new JSONObject();
        //**********************************************************************
        jsonDescVale.put("facturaClienteCab", facturaClienteCab);
        jsonDescVale.put("vale", vale);
        int monto = 0;
        if (descValePorc) {
            monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        }
        jsonDescVale.put("montoDesc", monto);
        //**********************************************************************
        return jsonDescVale;
    }
    //JSON DESCUENTO VALE

    //JSON DESCUENTO PROMOCIÓN TEMPORADA
    private JSONObject creandoJsonDescPromoTemp(JSONObject facturaClienteCab, JSONObject promoTemporadaDesc, int monto) {//acaité
        JSONObject jsonDescPromo = new JSONObject();
        //**********************************************************************
        jsonDescPromo.put("facturaClienteCab", facturaClienteCab);
        jsonDescPromo.put("promoTemporada", promoTemporadaDesc);
        jsonDescPromo.put("descripcionTemporada", promoTemporadaDesc.get("descripcionTemporada"));
        jsonDescPromo.put("monto", monto);
        //**********************************************************************
        return jsonDescPromo;
    }
    //JSON DESCUENTO PROMOCIÓN TEMPORADA

    //JSON DESCUENTO DIRECTIVO
    private JSONObject creandoJsonDescDirectivo(JSONObject facturaClienteCab) {//acaité
        JSONObject jsonDescDirectivo = new JSONObject();
        //**********************************************************************
        jsonDescDirectivo.put("facturaClienteCab", facturaClienteCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescDirectivo.put("montoDesc", monto);
        long porcDirectivo = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescDirectivo.put("porcDesc", porcDirectivo);
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        jsonDescDirectivo.put("ciDir", "");
        jsonDescDirectivo.put("nombreDir", "");
        jsonDescDirectivo.put("motivoDesc", "");
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        //**********************************************************************
        return jsonDescDirectivo;
    }
    //JSON DESCUENTO DIRECTIVO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    private void comboBoxTarjeta() {
        //para habilitar algún descuento con tarjeta crédito, débito, no debe tener otra forma de pago, y solo una tarjeta...
        if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() == 0) {
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar + descuento))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            lineStrokeTotal.setVisible(false);
            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                validandoClientFiel();
            } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                validandoFuncionario();
            } else {
//                editandoBarChart();
            }
        } else {
            validandoTarjeta();
        }
    }

    private void finalizandoVenta() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (listViewTarjetasAgregadas.getItems().isEmpty() && textFieldEfectivo.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && listViewCheques.getItems().isEmpty() && listViewNotasCred.getItems().isEmpty() && comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")
                && textFieldPesoGs.getText().contentEquals("") && textFieldRealGs.getText().contentEquals("") && textFieldDolarGs.getText().contentEquals("")) {
            mensajeError("DEBE INGRESAR UN MONTO, EFECTIVO; CHEQUE; VALE; TARJETAS; Y/O NOTA DE CRÉDITO.");
        } else if (totalAbonado < totalAPagar) {
            mensajeError("EL MONTO A ABONAR NO ES SUFICIENTE.");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA FINALIZAR LA VENTA? ", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
                alert.close();
//                if ("factura_venta_estetica_procesar")) {
                //validación en caso de ser Vale...
                String msjVale = "";
                boolean pasa = true;
                if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
                    if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS") && textFieldMontoVale.getText().contentEquals("")) {
                        msjVale = "EL CAMPO MONTO VALE NO DEBE ESTAR VACÍO.";
                        pasa = false;
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldMontoValePorc.getText().contentEquals("")) {
                            msjVale = "EL CAMPO PORCENTAJE DESCUENTO VALE\nNO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    }
                }
                if (pasa) {
                    datos.put("energiaElectrica", false);
                    actualizarDatos();
                    try {
                        actualizandoCabFactura();
                        long dato = CancelacionProductoEsteticaFXMLController.recuperarIdDato();
                        if (dato != 0l) {
                            datos.put("idDato", dato);
                        }
                        actualizarDatos();
                        seteandoCamposMsjFinal();
                        seteandoDescuentos();
                        seteandoTarjetas();
                        seteandoCheques();
                        seteandoNotaCred();
                        int valor = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
                        int sumEfe = 0;
                        if (!json.isNull("sumEfeRecibido")) {
                            sumEfe = Integer.parseInt(datos.get("sumEfeRecibido").toString());
                        }
                        int sum = sumEfe;
                        datos.put("sumEfeRecibido", (sum + valor));
                        if (!textFieldEfectivo.getText().equalsIgnoreCase("")) {
                            seteandoEfectivos();
                        }
//                            if (!textFieldMontoNotaCred.getText().equalsIgnoreCase("")) {
//                            seteandoNotaCred();
//                            }
                        if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
                            seteandoAsociacion();
                        } else if (!textFieldMontoVale.getText().equalsIgnoreCase("")) {
                            seteandoVales();
                        }
                        if (!textFieldDolarCant.getText().equalsIgnoreCase("") && !textFieldDolarGs.getText().equalsIgnoreCase("")) {
                            seteandoDolares();
                        }
                        if (!textFieldRealCant.getText().equalsIgnoreCase("") && !textFieldRealGs.getText().equalsIgnoreCase("")) {
                            seteandoReales();
                        }
                        if (!textFieldPesoCant.getText().equalsIgnoreCase("") && !textFieldPesoGs.getText().equalsIgnoreCase("")) {
                            seteandoPesos();
                        }
                        if (!textFieldMontoRetencion.getText().equalsIgnoreCase("")) {
                            seteandoRetencionciones();
                        }
                        JSONObject factJson = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        factJson.put("forma_pago_estetica", formaDePago);
                        fact.put("facturaClienteCab", factJson);
                        int contFa = 0;
//                            if (!json.isNull("contFact")) {
//                                contFa = Integer.parseInt(datos.get("contFact").toString());
//                            }
                        int contFact = contFa;
//                            datos.put("contFact", (contFact + 1));
                        int sumFa = 0;
//                            if (!json.isNull("sumFact")) {
//                                sumFa = Integer.parseInt(datos.get("sumFact").toString());
//                            }
                        int sumFact = sumFa;
//                            datos.put("sumFact", sumFact + Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
                        seteandoFuncionario();
                        seteandoSumaTotales();
                        //reseteando campos cliente, cliente fiel, funcionario, factura venta para próxima transacción...
                        FacturaVentaEsteticaFXMLController.resetParam();
                        ClienteEsteticaFXMLController.resetParam();
//                            ClienteFielFXMLController.resetParam();
//                            FuncionarioFXMLController.resetParam();
                        datos.remove("cancelProducto");
                        datos.remove("energiaElectrica");
                        actualizarDatos();
                        tarjConvActiva = false;
                        valeActivo = false;
                        //                            limpiandoFillTransition();
                        this.sc.loadScreen("/vista/estetica/MensajeFinalEsteticaFXML.fxml", 773, 497, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, true);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    mensajeError(msjVale);
                }
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, false);
//                }
            } else if (alert.getResult() == cancel) {
                alert.close();
            }
        }
    }

    private void seteandoCamposMsjFinal() {
        MensajeFinalEsteticaFXMLController.setTotalAbonado(Integer.valueOf(numValidator.numberValidator(textFieldTotalAbonado.getText())));
        MensajeFinalEsteticaFXMLController.setVuelto(Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText())));
        MensajeFinalEsteticaFXMLController.setTotal(Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())));
        MensajeFinalEsteticaFXMLController.setDescuento(Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())));
        MensajeFinalEsteticaFXMLController.setTotalAPagar(Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
        datos.put("totalApagar", Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
    }

    private void agregandoCheque() {
        if (textFieldEntidad.getText().isEmpty()) {
            if (panelBusquedaCheque.isDisable()) {
                mensajeError("ESTA FORMA DE PAGO ESTA DESHABILITADA");
            } else {
                mensajeError("DEBE INGRESAR EL NOMBRE DE LA ENTIDAD FINANCIERA.");
            }
        } else if (textFIeldNroCheque.getText().isEmpty()) {
            mensajeError("DEBE INGRESAR EL NRO. DE CHEQUE.");
        } else if (!listViewCheques.getItems().isEmpty()) {
            mensajeError("TEMPORALMENTE DESHABILITADO PARA MÁS DE UN CHEQUE\nDEBIDO A INCOMPATIBILIDAD DE ESTRUCTURAS.");
        } else if (textFieldMonto.getText().isEmpty()) {
            mensajeError("DEBE INGRESAR EL MONTO DEL CHEQUE.");
        } else {
            String cheque = textFieldEntidad.getText() + " - " + textFIeldNroCheque.getText() + " - " + textFieldMonto.getText() + " (Borrar)";
            String chequeUnico = textFieldEntidad.getText() + " - " + textFIeldNroCheque.getText();//válido solo para la transacción actual...
            if (listViewCheques.getItems().isEmpty()) {
                chequesAsignados.add(cheque);
                hashListCheque = new HashMap<>();
                hashListCheque.put(chequeUnico, 0);
                listViewCheques.setItems(chequesAsignados);
                if (descTarjeta) {
                    calculandoTarjeta(false);
                    descTarjeta = false;
                }
                seteandoTotalAbonado();
                limpiandoCheque();
                listViewCheques.requestFocus();
                if (!listViewCheques.getItems().isEmpty()) {
                    listViewCheques.getSelectionModel().select(0);
                }
            } else if (hashListCheque.get(chequeUnico) == null) {
                chequesAsignados.add(cheque);
                listViewCheques.setItems(chequesAsignados);
                hashListCheque.put(chequeUnico, listViewCheques.getItems().indexOf(cheque));
                if (descTarjeta) {
                    calculandoTarjeta(false);
                    descTarjeta = false;
                }
                seteandoTotalAbonado();
                limpiandoCheque();
                listViewCheques.requestFocus();
                if (!listViewCheques.getItems().isEmpty()) {
                    listViewCheques.getSelectionModel().select(0);
                }
            } else {
                mensajeError("EL CHEQUE YA SE AGREGÓ.");
            }
        }
    }

    private void quitandoCheque(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            } else if (!listViewCheques.getItems().isEmpty()) {
                String[] parts = listViewCheques.getSelectionModel().getSelectedItem().split(" - Gs ");
                hashListCheque.remove(parts[0]);
                chequesAsignados.remove(listViewCheques.getSelectionModel().getSelectedItem());
                arrayCheque.remove(listViewCheques.getSelectionModel().getSelectedItem());
                listViewCheques.setItems(chequesAsignados);
                seteandoTotalAbonado();
                seteandoDescTarjeta();
                if (listViewCheques.getItems().isEmpty()) {
                    validandoCambiandoFormaPago();
                }
                calculandoValePorc();
                seteandoTotalAbonado();
            } else {
                seteandoTotalAbonado();
                seteandoDescTarjeta();
            }
        } else if (!listViewCheques.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewCheques.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            textFieldNroNotaCred.requestFocus();
        }
    }

    private void agregandoTarjeta() {
        if (comboBoxTarjetas.getSelectionModel().getSelectedItem() == null || comboBoxTarjetas.getSelectionModel().getSelectedIndex() == 0) {
            desbloqueandoPanelesTarj();
            if (panelBusquedaTipoTarj.isDisable()) {
                mensajeError("ESTA FORMA DE PAGO ESTA DESHABILITADA");
            } else {
                mensajeError("DEBE SELECCIONAR UNA TARJETA CRÉDITO o DÉBITO.");
            }
        } else if (textFieldCodAuth.getText().contentEquals("")) {
            mensajeError("DEBE CARGAR EL CÓDIGO DE AUTORIZACIÓN.");
        } else if (textFieldMontoTarjeta.getText().contentEquals("")) {
            mensajeError("DEBE INGRESAR EL MONTO DE LA TARJETA CRÉDITO o DÉBITO.");
        } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            mensajeError("TEMPORALMENTE DESHABILITADO PARA MÁS DE UNA TARJETA\nDEBIDO A INCOMPATIBILIDAD DE ESTRUCTURAS.");
        } else {
            String tarjeta = comboBoxTarjetas.getSelectionModel().getSelectedItem() + " - " + textFieldMontoTarjeta.getText() + " (Borrar)" + " - " + textFieldCodAuth.getText();
            String tarjetaUnica = comboBoxTarjetas.getSelectionModel().getSelectedItem() + " - " + textFieldCodAuth.getText();//válido solo para la transacción actual...
            if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                tarjetasAsignadas.add(tarjeta);
                hashListTarj = new HashMap<>();
                hashListTarj.put(tarjetaUnica, 0);
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                seteandoTotalAbonado();
                limpiandoTarjeta();
            } else if (hashListTarj.get(tarjetaUnica) == null) {
                tarjetasAsignadas.add(tarjeta);
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                hashListTarj.put(tarjetaUnica, listViewTarjetasAgregadas.getItems().indexOf(tarjeta));
                calculandoTarjeta(false);//solo descuento para una tarjeta
                seteandoTotalAbonado();
                limpiandoTarjeta();
//                validandoCambiandoFormaPago();
            } else {
                mensajeError("LA TARJETA YA SE AGREGÓ.");
            }
        }
    }

    private void quitandoTarjeta(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
                String[] parts = listViewTarjetasAgregadas.getSelectionModel().getSelectedItem().split(" - ");
                String itemRemove = parts[0] + " - " + parts[2];
                hashListTarj.remove(itemRemove);
                tarjetasAsignadas.remove(listViewTarjetasAgregadas.getSelectionModel().getSelectedItem());
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                validandoTarjetaListen();
                calculandoValePorc();
                seteandoTotalAbonado();
            }
        } else if (!listViewTarjetasAgregadas.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewTarjetasAgregadas.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            textFieldEntidad.requestFocus();
        }
    }

    private void agregandoTarjConv() {//queda abierta la posibilidad de varias tarjetas convenio...
        if (comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem() == null) {
            mensajeError("DEBE SELECCIONAR UNA TARJETA CONVENIO.");
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            mensajeError("ESTÁ PERMITIDO HASTA UNA TARJETA CONVENIO POR PAGO.");
        } else {
            tarjConvAsignadas.add(comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem());
            listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
            validandoTarjetaConvenio();
        }
    }

    private void quitandoTarjConv(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                tarjConvAsignadas.remove(listViewTarjetasConvAgregadas.getSelectionModel().getSelectedItem());
                listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
                if (listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    invalidandoTarjetaConvenio();
                    validandoCambiandoFormaPago();
                    seteandoTotalAbonado();
                }
            }
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewTarjetasConvAgregadas.getSelectionModel().select(0);
        }
    }

    private void visualizandoCamposVale() {//validaciones para el vale
        if (comboBoxVale.getSelectionModel().isSelected(0)) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
            invalidandoVale();
            validandoCambiandoFormaPago();
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ORDEN DE COMPRA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(false);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        }
    }

    private void gestionandoPago(JSONObject cabFactura) {
        if (!textFieldEfectivo.getText().contentEquals("")) {
            if (creandoCabFacturaEfectivo(cabFactura)) {
            }
            //para impresión
            MensajeFinalEsteticaFXMLController.setEfectivo(Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText())));
            //para impresión
        }
        if (!textFieldRealGs.getText().contentEquals("") || !textFieldPesoGs.getText().contentEquals("")
                || !textFieldDolarGs.getText().contentEquals("")) {
            JSONArray jsonArrayMonedaExt = creandoJsonMonedaEx(cabFactura);
            if (creandoCabFacturaMonedaExt(jsonArrayMonedaExt)) {
            }
        }
        if (!listViewCheques.getItems().isEmpty()) {
            JSONArray jsonArrayCheque = creandoJsonCheque(cabFactura);
            if (creandoCabFacturaCheque(jsonArrayCheque)) {
            }
        }
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            JSONArray jsonArrayTarjeta = creandoJsonTarjeta(cabFactura);
            if (creandoCabFacturaTarjeta(jsonArrayTarjeta)) {
            }
        }
        if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            if (creandoCabFacturaTarjConv(cabFactura)) {
            }
        }
//        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//            if (creandoCabFacturaClienteFiel(cabFactura)) {
//            }
//        }
//        if (FuncionarioFXMLController.getJsonFuncionario() != null) {
//            if (creandoCabFacturaFuncionario(cabFactura)) {
//            }
//        }
        if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
            if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                creandoCabFacturaValeMonto(cabFactura);
            } else {
                creandoCabFacturaValePorc(cabFactura);
            }
        }
        if (!listViewNotasCred.getItems().isEmpty()) {
            JSONArray jsonArrayNota = creandoJsonNotaCred(cabFactura);
            if (creandoCabFacturaNotaCred(jsonArrayNota)) {
            }
            //para impresión
//            MensajeFinalEsteticaFXMLController.setNotaCred(Integer.valueOf(numValidator.numberValidator(textFieldMontoNotaCred.getText())));
            //para impresión
        }
        if (!textFieldMontoRetencion.getText().contentEquals("")) {
            if (creandoCabFacturaMontoRetencion(cabFactura)) {
            }
            //para impresión
            MensajeFinalEsteticaFXMLController.setRetencion(Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText())));
            //para impresión
        }
        gestionandoDescuento(cabFactura, true);
    }

    private void gestionandoDescuento(JSONObject json, boolean sinCabeceraPago) {
        if (sinCabeceraPago) {
            if (descValeMonto || descValePorc) {
                creandoCabFacturaValeDesc(json, hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
            }
            if (isDescPromoTemp() || checkBoxPromo.isSelected()) {
                for (Object jsonObjectProm : Descuento.getPromTempJSONArray()) {//verificación de promoción con su respectivo descuento...
                    JSONObject jsonPromoTemp = (JSONObject) jsonObjectProm;
                    if (hashJsonPromocionDesc.get(jsonPromoTemp) != null) {
                        long descEstaPromo = hashJsonPromocionDesc.get(jsonPromoTemp);
                        int descPromo = toIntExact((long) descEstaPromo);
                        JSONObject cabFacturaPromTempDesc = creandoJsonDescPromoTemp(json, jsonPromoTemp, descPromo);
                        creandoCabFacturaPromTempDesc(cabFacturaPromTempDesc);
                    }
                }
            }
            if (descDirectivo) {
                creandoCabFacturaDirectivoDesc(json);
            }
        }
        //        else {
        //        if (isDescTarjetaFiel()) {
        //            creandoCabFacturaClienteFielDesc(json);
        //        }
        //        if (isDescFuncionario()) {
        //            creandoCabFacturaFuncionarioDesc(json);
        //        }
        //        if (descTarjeta) {
        //            creandoCabFacturaTarjetaDesc(json);
        //        }
        //        if (descTarjetaConvenio) {
        //            creandoCabFacturaTarjetaConvDesc(json);
        //        }
//        }
    }

    private void calculandoTotal() {
        this.total = FacturaVentaEsteticaFXMLController.getPrecioTotal();
        String total = numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(this.total)));
        textFieldTotal.setText(total);
    }

    private void seteandoControlDesc() {
        descDirectivo = false;
        descFuncionario = false;
        descPromoTemp = false;
        descTarjeta = false;
        descTarjetaConvenio = false;
        descTarjetaFiel = false;
        descValePorc = false;
        descValeMonto = false;
    }

    private void seteandoTotales() {
        descuento = 0;
        totalAPagar = 0;
        textFieldDescuento.setText("Gs 0");
//        lineStrokeTotal.setVisible(false);
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(FacturaVentaEsteticaFXMLController.getPrecioTotal().toString())));
        seteandoControlDesc();
        montoConDes10 = 0;
        montoConDes5 = 0;
        exenta = 0;
        hashDetallePrecioDesc = new HashMap<>();
        hashDetallePrecioDescPromoArt = new HashMap<>();
        //sum
        descuentoPromoArtSum = 0;
        descuentoPromoNfSum = 0;
        descuentoFielSum = 0;
        descuentoFuncSum = 0;
        descuentoTarjetaSum = 0;
        descuentoTarjetaConvSum = 0;
        descuentoValePorcSum = 0;
        //sum
    }

    private boolean seteandoDescTarjeta() {
        if (listViewCheques.getItems().isEmpty() && listViewTarjetasConvAgregadas.getItems().isEmpty()
                && comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")
                && textFieldMontoPorc.getText().isEmpty() && textFieldEfectivo.getText().isEmpty() && textFieldDolarGs.getText().isEmpty()
                && textFieldRealGs.getText().isEmpty() && textFieldPesoGs.getText().isEmpty() && textFieldMontoRetencion.getText().isEmpty()) {
            calculandoTarjeta(true);
        }
        return descTarjeta;
    }

    private void limpiandoTarjeta() {
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
    }

    private void limpiandoCheque() {
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFieldMonto.setText("");
    }

    private void limpiandoNotaCred() {
        textFieldNroNotaCred.setText("");
        textFieldNroNotaCred.setDisable(false);
        textFieldMontoNotaCred.setText("");
    }

    private void cotizacionMoneda() {
        if (FacturaVentaEsteticaFXMLController.getCotizacionList() != null) {
            if (!textFieldDolarCant.getText().contentEquals("")) {
                int dolarCant = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
                int dolarConversion = dolarCant * FacturaVentaEsteticaFXMLController.getDolar();
                textFieldDolarGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(dolarConversion))));
            } else {
                textFieldDolarGs.setText("");
            }
            if (!textFieldPesoCant.getText().contentEquals("")) {
                int pesoCant = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
                int pesoConversion = pesoCant * FacturaVentaEsteticaFXMLController.getPeso();
                textFieldPesoGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(pesoConversion))));
            } else {
                textFieldPesoGs.setText("");
            }
            if (!textFieldRealCant.getText().contentEquals("")) {
                int realCant = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
                int realConversion = realCant * FacturaVentaEsteticaFXMLController.getReal();
                textFieldRealGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(realConversion))));
            } else {
                textFieldRealGs.setText("");
            }
        } else {
            textFieldDolarGs.setText("");
            textFieldPesoGs.setText("");
            textFieldRealGs.setText("");
        }
    }

    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->
    //FUNCIONARIO***************************************************************
    private void validandoFuncionario() {
        panelBusquedaTipoTarj.setDisable(true);
        panelBusquedaClienteFiel.setDisable(true);
        panelBusquedaCheque.setDisable(true);
        panelBusquedaTipoTarjConv.setDisable(true);
        panelVales.setDisable(true);
        panePromoTemp.setDisable(true);
        panelNotaCredito.setDisable(true);
        panelMontoRetencion.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoFuncionario();
        }
    }

    private void invalidandoFuncionario() {
        panelBusquedaTipoTarj.setDisable(false);
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaCheque.setDisable(false);
        panelBusquedaTipoTarjConv.setDisable(false);
        panelVales.setDisable(false);
        panePromoTemp.setDisable(false);
        panelNotaCredito.setDisable(false);
        panelMontoRetencion.setDisable(false);
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void validandoClientFiel() {
        panelBusquedaTipoTarj.setDisable(true);
        panelBusquedaFuncionario.setDisable(true);
        panelBusquedaCheque.setDisable(true);
        panelBusquedaTipoTarjConv.setDisable(true);
        panelVales.setDisable(true);
        panePromoTemp.setDisable(true);
        panelNotaCredito.setDisable(true);
        panelMontoRetencion.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoClienteFiel();
        }
    }

    private void invalidandoClientFiel() {
        panelBusquedaTipoTarj.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        panelBusquedaCheque.setDisable(false);
        panelBusquedaTipoTarjConv.setDisable(false);
        panelVales.setDisable(false);
        panePromoTemp.setDisable(false);
        panelNotaCredito.setDisable(false);
        panelMontoRetencion.setDisable(false);
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
    private void validandoTarjeta() {
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoTarjeta(true);
            seteandoTotalAbonado();
        }
    }

    private void bloqueandoPanelesTarj() {
        toggleEnTarj = true;
//        toggleButton.setSelected(false);
        panelBusquedaTipoTarjConv.setDisable(true);
        panelTipoDePagoExtranjera.setDisable(true);
        panelTipoDePago.setDisable(true);
        panelBusquedaCheque.setDisable(true);
        panelNotaCredito.setDisable(true);
        panelVales.setDisable(true);
        textFieldEfectivo.setText("");
        textFieldDolarCant.setText("");
        textFieldRealCant.setText("");
        textFieldPesoCant.setText("");
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFieldMonto.setText("");
        textFieldNroNotaCred.setText("");
        textFieldMontoNotaCred.setText("");
        comboBoxVale.getSelectionModel().select(0);
        textFieldNroVale.setText("");
        textFieldValeCI.setText("");
        textFieldMontoVale.setText("");
        textFieldMontoValePorc.setText("");
        if (!listViewCheques.getItems().isEmpty()) {
            listViewCheques.getSelectionModel().select(0);
            String[] parts = listViewCheques.getSelectionModel().getSelectedItem().split(" - Gs ");
            hashListCheque.remove(parts[0]);
            chequesAsignados.remove(listViewCheques.getSelectionModel().getSelectedItem());
            arrayCheque.remove(listViewCheques.getSelectionModel().getSelectedItem());
            listViewCheques.setItems(chequesAsignados);
        }
        if (!listViewNotasCred.getItems().isEmpty()) {
            listViewNotasCred.getSelectionModel().select(0);
            String[] parts = listViewNotasCred.getSelectionModel().getSelectedItem().split(" - Gs ");
            hashListNotaCred.remove(parts[0]);
            notaCredAsignadas.remove(listViewNotasCred.getSelectionModel().getSelectedItem());
            listViewNotasCred.setItems(notaCredAsignadas);
        }
        if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            listViewTarjetasConvAgregadas.getSelectionModel().select(0);
            tarjConvAsignadas.remove(listViewTarjetasConvAgregadas.getSelectionModel().getSelectedItem());
            listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
        }
        toggleEnTarj = false;
    }

    private void invalidandoTarjeta() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
    }
    //TARJETAS******************************************************************

    //TARJETAS CONVENIO*********************************************************
    private void validandoTarjetaConvenio() {
        panelBusquedaClienteFiel.setDisable(true);
        panelBusquedaFuncionario.setDisable(true);
        panelVales.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoTarjetaConvenio();
        }
    }

    private void invalidandoTarjetaConvenio() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        panelVales.setDisable(false);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            seteandoTotales();
        }
    }
    //TARJETAS CONVENIO*********************************************************

    //PROMOCIÓN TEMPORADA*******************************************************
    private void validandoPromocion() {
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoPromocionTemporada(false);
        }
    }

    private void invalidandoPromocion() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        panelVales.setDisable(false);
        panelBusquedaTipoTarjConv.setDisable(false);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            seteandoTotales();
        }
    }
    //PROMOCIÓN TEMPORADA*******************************************************

    //VALE*******************************************************
    private void validandoVale() {
        panelBusquedaTipoTarjConv.setDisable(true);
        panePromoTemp.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoVale(comboBoxVale.getSelectionModel().getSelectedItem());
        }
    }

    private void invalidandoVale() {
        panelBusquedaTipoTarjConv.setDisable(false);
        panePromoTemp.setDisable(false);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            seteandoTotales();
        }
    }
    //VALE*******************************************************

    //DIRECTIVO*****************************************************************
    private void validandoDirectivo() {
        calculandoDirectivo();
    }

    private void invalidandoDirectivo() {
        seteandoTotales();
    }
    //DIRECTIVO*****************************************************************

    //NOTA DE CRÉDITO***********************************************************
    private void validandoNotaCredito() {
        textFieldNroNotaCred.setDisable(true);
        calculandoNotaCredito();
    }

    private void invalidandoNotaCredito() {
        textFieldNroNotaCred.setDisable(false);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //NOTA DE CRÉDITO***********************************************************

    //MONTO DE RETENCIÓN***********************************************************
    private void validandoMontoRetencion() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        calculandoMontoRetencion();
    }

    private void invalidandoMontoRetencion() {
        panelBusquedaClienteFiel.setDisable(true);
        panelBusquedaFuncionario.setDisable(true);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //MONTO DE RETENCIÓN***********************************************************

    //cambio de formas de pago....
    private void validandoCambiandoFormaPago() {
        if (comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
            validandoVale();
        } else if (checkBoxPromo.isSelected()) {
            checkBoxPromoTemp();
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            validandoTarjetaConvenio();
        } else if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            validandoClientFiel();
        } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            validandoFuncionario();
        } else {
            seteandoDescTarjeta();
        }
    }
    //cambio de formas de pago....
    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->

    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->
    //FUNCIONARIO***************************************************************
    private void calculandoFuncionario() {
        seteandoTotales();
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                calculandoFuncionarioNf(7, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                calculandoFuncionarioNf(6, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                calculandoFuncionarioNf(5, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                calculandoFuncionarioNf(4, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                calculandoFuncionarioNf(3, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                calculandoFuncionarioNf(2, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                calculandoFuncionarioNf(1, jsonNf, detalleArtJson);
            } else {
                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
            }
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalEsteticaFXMLController
        if (descFuncionario) {
            for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (long) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (long) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        totalAPagar = FacturaVentaEsteticaFXMLController.getPrecioTotal() - descuento;
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
        seteandoTotalAbonado();
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoFuncionarioNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")) != null) {
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                }
                break;
            case 2:
                if (Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")) != null) {
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoFuncionarioNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoFuncionarioNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoFuncionarioNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoFuncionarioNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoFuncionarioNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoFuncionarioNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoFuncionarioDet(JSONObject jsonFuncionarioDesc, JSONObject detalleArtJson) {
        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonFuncionarioDesc.get("porcDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        descuento = (long) (descuento + (((precio * cantidad) * porc) / 100));
        descFuncionario = true;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            montoConDes10 = (long) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
//            montoMap10.put(detalleArtJson, montoConDes10);
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            montoConDes5 = (long) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
//            montoMap5.put(detalleArtJson, montoConDes5);
        } else {
            exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
            montoMapExe.put(detalleArtJson, exenta);
        }
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void calculandoClienteFiel() {
        seteandoTotales();
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                calculandoClienteFielNf(7, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                calculandoClienteFielNf(6, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                calculandoClienteFielNf(5, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                calculandoClienteFielNf(4, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                calculandoClienteFielNf(3, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                calculandoClienteFielNf(2, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                calculandoClienteFielNf(1, jsonNf, detalleArtJson);
            } else {
                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
            }
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalEsteticaFXMLController
        if (descTarjetaFiel) {
            for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (long) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (long) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        totalAPagar = FacturaVentaEsteticaFXMLController.getPrecioTotal() - descuento;
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
        seteandoTotalAbonado();
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoClienteFielNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                }
                break;
            case 2:
                if (Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoClienteFielNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoClienteFielNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoClienteFielNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoClienteFielNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoClienteFielNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoClienteFielNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoClienteFielDet(JSONObject jsonFielCab, JSONObject detalleArtJson) {
        JSONArray jsonArrayFielDet = (JSONArray) jsonFielCab.get("descuentoFielDets");
        Calendar calendar = Calendar.getInstance();
        int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
        for (Object fielDetObj : jsonArrayFielDet) {//verificación, día de descuento...
            JSONObject jsonFielDet = (JSONObject) fielDetObj;
            JSONObject jsonDia = (JSONObject) jsonFielDet.get("dia");
            int dia = toIntExact((long) jsonDia.get("idDia"));
            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
            double porc = Double.valueOf(jsonFielCab.get("porcentajeDesc").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            if (dia == diaSemana) {
                descuento = (long) (descuento + (((precio * cantidad) * porc) / 100));
                descTarjetaFiel = true;
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    montoConDes10 = (long) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
//                    montoMap10.put(detalleArtJson, montoConDes10);
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    montoConDes5 = (long) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
//                    montoMap5.put(detalleArtJson, montoConDes5);
                } else {
                    exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
                    montoMapExe.put(detalleArtJson, exenta);
                }
                //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
            }
        }
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
//    private void calculandoTarjeta(boolean soloTarjeta) {
//        seteandoTotales();
//        if (listViewTarjetasAgregadas.getItems().size() < 2 && soloTarjeta) {//validación de descuento, solo para una tarjeta...
//            //verificación de descuento según tarjeta, se toma el nombre asignado en la línea de tiempo, 
//            //si se modifica el nombre de la tarjeta por "a" o "b" motivo, se dejará de aplicar descuento,
//            //en caso de querer aplicar descuento con el nuevo nombre tarjeta, deberá dar de baja la tarjeta con el nombre viejo en configuración descuentos...
//            String descripcionTarj = "N/A";
//            if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
//                String[] parts = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
//                descripcionTarj = parts[0];
//            } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
//                descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
//            }
//            if (Descuento.getHashDescTarjeta() != null) {
//                if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
//                    JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
//                    JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
//                    Calendar calendar = Calendar.getInstance();
//                    JSONArray jsonArrayDiaEsp = new JSONArray();
//                    int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
//                    for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
//                        JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
//                        JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
//                        int dia = toIntExact((long) jsonDia.get("idDia"));
//                        if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
//                            String fechaIni = (String) jsonTarjCab.get("horaInicio");
//                            String fechaFin = (String) jsonTarjCab.get("horaFin");
//                            if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
//                                Time fechaIniT = Time.valueOf(fechaIni);
//                                Time fechaFinT = Time.valueOf(fechaFin);
//                                Date date = new Date();
//                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
//                                Time t = Time.valueOf(hora);
//                                if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
//                                    double porc = (double) jsonTarjCab.get("porcentajeDesc");//descuento por el total...
//                                    long precio = FacturaVentaEsteticaFXMLController.getPrecioTotal();//desde factura venta...
//                                    descuento = (long) (descuento + ((precio * porc) / 100));
//                                    descTarjeta = true;
//                                }
//                            } else {
//                                double porc = (double) jsonTarjCab.get("porcentajeDesc");//descuento por el total...
//                                long precio = FacturaVentaEsteticaFXMLController.getPrecioTotal();//desde factura venta...
//                                descuento = (long) (descuento + ((precio * porc) / 100));
//                                seteandoControlDesc();
//                                descTarjeta = true;
//                            }
//                        } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
//                            jsonArrayDiaEsp.add(jsonTarjDet);
//                        }
//                    }
//                    if (!descTarjeta && !jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
//                        HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
//                        if (hashDiaEspecial != null) {
//                            for (Object tarjDetObj : jsonArrayDiaEsp) {
//                                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
//                                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
//                                if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
//                                    if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
//                                        String fechaIni = (String) jsonTarjCab.get("horaInicio");
//                                        String fechaFin = (String) jsonTarjCab.get("horaFin");
//                                        if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
//                                            Time fechaIniT = Time.valueOf(fechaIni);
//                                            Time fechaFinT = Time.valueOf(fechaFin);
//                                            Date date = new Date();
//                                            String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
//                                            Time t = Time.valueOf(hora);
//                                            if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
//                                                double porc = (double) jsonTarjCab.get("porcentajeDesc");//descuento por el total...
//                                                long precio = FacturaVentaEsteticaFXMLController.getPrecioTotal();//desde factura venta...
//                                                descuento = (long) (descuento + ((precio * porc) / 100));
//                                                descTarjeta = true;
//                                            }
//                                        } else {
//                                            double porc = (double) jsonTarjCab.get("porcentajeDesc");//descuento por el total...
//                                            long precio = FacturaVentaEsteticaFXMLController.getPrecioTotal();//desde factura venta...
//                                            descuento = (long) (descuento + ((precio * porc) / 100));
//                                            seteandoControlDesc();
//                                            descTarjeta = true;
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        totalAPagar = FacturaVentaEsteticaFXMLController.getPrecioTotal() - descuento;
//        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
//        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
//        seteandoTotalAbonado();
//    }
    //TARJETAS******************************************************************
    //TARJETAS CONVENIO*********************************************************
    private void calculandoTarjetaConvenio() {
        seteandoTotales();
        if (listViewTarjetasConvAgregadas.getItems().size() == 1) {//validación de descuento, solo para una tarjeta convenio...
            String parts = listViewTarjetasConvAgregadas.getItems().get(0);
            if (Descuento.getHashDescTarjetaConv().get(parts) != null) {
                JSONObject jsonTarjConvCab = Descuento.getHashDescTarjetaConv().get(parts);
                JSONArray jsonArrayTarjConvDet = (JSONArray) jsonTarjConvCab.get("descuentoTarjetaConvenioDetDTO");
                Calendar calendar = Calendar.getInstance();
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                for (Object tarjDetObj : jsonArrayTarjConvDet) {//verificación, día de descuento...
                    JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                    JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                    int dia = toIntExact((long) jsonDia.get("idDia"));
                    if (dia == diaSemana) {
                        double porc = (double) jsonTarjConvCab.get("porcentajeDesc");//descuento por el total...
                        long precio = FacturaVentaEsteticaFXMLController.getPrecioTotal();//desde factura venta...
                        descuento = (long) (descuento + ((precio * porc) / 100));
                        descTarjetaConvenio = true;
                    }
                }
            }
        }
        totalAPagar = FacturaVentaEsteticaFXMLController.getPrecioTotal() - descuento;
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
        seteandoTotalAbonado();
    }
    //TARJETAS CONVENIO*********************************************************

    //PROMOCIÓN TEMPORADA*******************************************************
//    private void calculandoPromocionTemporada() {
//        seteandoTotales();
//        Date date = new Date();
//        Timestamp tsNow = new Timestamp(date.getTime());
//        hashJsonPromocionDesc = new HashMap<>();
//        hashJsonPromocionArtDesc = new HashMap<>();
//        montoMap5 = new HashMap<>();
//        montoMap10 = new HashMap<>();
//        montoMapExe = new HashMap<>();
//        //se toma primero artículo, luego sección, de existir en ambos, se da prioridad a artículo...
//        for (Object jsonObjectPromArt : Descuento.getPromTempArtJSONArray()) {
//            JSONObject jsonPromoTempArt = (JSONObject) jsonObjectPromArt;
//            Timestamp tsIni = Utilidades.objectToTimestamp(jsonPromoTempArt.get("fechaInicio"));
//            Timestamp tsFin = Utilidades.objectToTimestamp(jsonPromoTempArt.get("fechaFin"));
//            tsFin.setHours(23);
//            tsFin.setMinutes(59);
//            tsFin.setSeconds(59);
//            long descuentoPromoArt = 0;
//            if (tsNow.after(tsIni) && tsNow.before(tsFin)) {//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
//                JSONArray jsonArrayPromoTempArtDet = (JSONArray) jsonPromoTempArt.get("artPromoTemporadaDTO");
//                for (Object jsonObjectPromArtDet : jsonArrayPromoTempArtDet) {
//                    JSONObject jsonPromoTempArtDet = (JSONObject) jsonObjectPromArtDet;
//                    JSONObject jsonArticulo = (JSONObject) jsonPromoTempArtDet.get("articulo");
//                    for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
//                        //verificación de descuento según artículo...
//                        if (detalleArtJson.get("descripcion").toString().toUpperCase().contentEquals(jsonArticulo.get("descripcion").toString().toUpperCase())) {
//                            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
//                            double porc = Double.valueOf(jsonPromoTempArtDet.get("porcentajeDesc").toString());
//                            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
//                            descuentoPromoArt = (long) (descuentoPromoArt + ((precio * cantidad) * porc) / 100);//descuento total en esta promoción...
//                            descuento = (long) (descuento + (((precio * cantidad) * porc) / 100));//descuento global por promoción
//                            descPromoTemp = true;
//                            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
//                                montoConDes10 = (long) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
//                                montoMap10.put(detalleArtJson, montoConDes10);
//                            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
//                                montoConDes5 = (long) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
//                                montoMap5.put(detalleArtJson, montoConDes5);
//                            } else {
//                                exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
//                                montoMapExe.put(detalleArtJson, exenta);
//                            }
//                            //precio unitario neto, en detalle, solo para aquellos que manejan descuento por artículo
//                            hashDetallePrecioDescPromoArt.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
//                        }
//                    }
//                    if (descuentoPromoArt > 0) {
//                        hashJsonPromocionArtDesc.put(jsonPromoTempArt, descuentoPromoArt);
//                    }
//                }
//            }
//        }
//
//        descuentoPromoNf = 0;
//        for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
//            if (!hashDetallePrecioDescPromoArt.containsKey(detalleArtJson)) {//se da prioridad a artículo, luego NF
//                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
//                JSONArray jsonArrayArticuloNf;
//                JSONObject jsonNf;
//                JSONObject jsonArticuloNf;
//                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
//                    calculandoPromoTempNf(7, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
//                    calculandoPromoTempNf(6, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
//                    calculandoPromoTempNf(5, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
//                    calculandoPromoTempNf(4, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
//                    calculandoPromoTempNf(3, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
//                    calculandoPromoTempNf(2, jsonNf, detalleArtJson);
//                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
//                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
//                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
//                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
//                    calculandoPromoTempNf(1, jsonNf, detalleArtJson);
//                } else {
//                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
//                }
//            }
//        }
//
//        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalEsteticaFXMLController
//        if (descPromoTemp) {
//            for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
//                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
//                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
//                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
//                    if (montoMap10.get(detalleArtJson) == null) {
//                        montoConDes10 = montoConDes10 + (long) (precio * cantidad);
//                    }
//                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
//                    if (montoMap5.get(detalleArtJson) == null) {
//                        montoConDes5 = montoConDes5 + (long) (precio * cantidad);
//                    }
//                } else if (montoMapExe.get(detalleArtJson) == null) {
//                    exenta = exenta + (long) (precio * cantidad);
//                }
//            }
//        }
//        totalAPagar = FacturaVentaEsteticaFXMLController.getPrecioTotal() - descuento;
//        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
//        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
//        seteandoTotalAbonado();
//    }
    @SuppressWarnings("element-type-mismatch")
    private void calculandoPromoTempNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        Date date = new Date();
        Timestamp tsNow = new Timestamp(date.getTime());
        switch (nf) {
            case 1:
                if (Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                }
                break;
            case 2:
                if (Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoPromoTempNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoPromoTempNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoPromoTempNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoPromoTempNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoPromoTempNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoPromoTempNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoPromoTempDet(JSONObject jsonPromoTempCab, JSONObject jsonPromoTempDet, JSONObject detalleArtJson) {
        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonPromoTempDet.get("porcentajeDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        descuentoPromoNf = (long) (((precio * cantidad) * porc) / 100);//descuento total en esta promoción...
        descuento = (long) (descuento + (((precio * cantidad) * porc) / 100));//descuento global por promoción
        descPromoTemp = true;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            montoConDes10 = (long) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
//            montoMap10.put(detalleArtJson, montoConDes10);
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            montoConDes5 = (long) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
//            montoMap5.put(detalleArtJson, montoConDes5);
        } else {
            exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
            montoMapExe.put(detalleArtJson, exenta);
        }
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
        if (descuentoPromoNf > 0) {
            if (hashJsonPromocionDesc.containsKey(jsonPromoTempCab)) {
                descuentoPromoNf = descuentoPromoNf + hashJsonPromocionDesc.get(jsonPromoTempCab);
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            } else {
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            }

        }
    }
    //PROMOCIÓN TEMPORADA*******************************************************

    //VALE**********************************************************************
    private void calculandoVale(String tipoVale) {
        seteandoTotales();
        switch (tipoVale) {
            case "FUNCIONARIO":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                    seteandoTotalAbonado();
                }
                break;
            case "ORDEN DE COMPRA":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                    seteandoTotalAbonado();
                }
                break;
            case "ACUERDO COMERCIAL":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                    seteandoTotalAbonado();
                }
                break;
            case "ASOCIACION":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                    seteandoTotalAbonado();
                }
                break;
            case "ARGOS":
                if (!textFieldMontoValePorc.getText().contentEquals("")) {
                    descValePorc = true;
                    seteandoTotalAbonado();
                }
                break;
            case "PARESA":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                    seteandoTotalAbonado();
                }
                break;
            default:
                break;
        }
    }

    //VALE**********************************************************************
    //DIRECTIVO*****************************************************************
    private void calculandoDirectivo() {
        seteandoTotales();
        seteandoControlDesc();
        descDirectivo = true;
        seteandoTotalAbonado();
    }

    //DIRECTIVO*****************************************************************
    //NOTA DE CRÉDITO***********************************************************
    private void calculandoNotaCredito() {
        boolean auxDirect = descDirectivo;
//        seteandoTotalesParaNotaCred();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }

    //NOTA DE CRÉDITO***********************************************************
    //MONTO RETENCIÓN***********************************************************
    private void calculandoMontoRetencion() {
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }
    //MONTO RETENCIÓN***********************************************************
    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->

    //SET SET SET SET SET SET ************************** -> -> -> -> -> -> -> ->
    private void seteandoDescuentos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDesc = 0;
        int sumDesc = 0;
        if (!json.isNull("contDesc")) {
            contDesc = Integer.parseInt(datos.get("contDesc").toString());
        }
        if (!json.isNull("sumDesc")) {
            sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
        }
        int descActual = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        if (descActual != 0) {
            sumDesc += descActual;
            contDesc += 1;
            datos.put("contDesc", contDesc);
            datos.put("sumDesc", sumDesc);
            formaDePago.put("monDesc", sumDesc);
        }
    }

    private void seteandoTarjetas() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contTarj = 0;
        int sumTarj = 0;
        if (!json.isNull("contTarj")) {
            contTarj = Integer.parseInt(datos.get("contTarj").toString());
        }
        if (!json.isNull("sumTarj")) {
            sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
        }
        ObservableList<String> datoes = listViewTarjetasAgregadas.getItems();
        int sumTarjeta = 0;
        for (String x : datoes) {
            contTarj++;
            String[] arraySplit = x.split(" - ");
            String banco = arraySplit[0];
            String monto = arraySplit[1];
            String cod = arraySplit[2];
            String mon = monto.replaceAll("", " (Borrar)");
            sumTarj += Integer.valueOf(numValidator.numberValidator(mon));
            sumTarjeta += Integer.valueOf(numValidator.numberValidator(mon));
        }
        datos.put("contTarj", contTarj);
        datos.put("sumTarj", sumTarj);
        formaDePago.put("monTarj", sumTarjeta);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoCheques() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contChq = 0;
        int sumChq = 0;
        if (!json.isNull("contCheque")) {
            contChq = Integer.parseInt(datos.get("contCheque").toString());
        }
        if (!json.isNull("sumCheque")) {
            sumChq = Integer.parseInt(datos.get("sumCheque").toString());
        }
        int sumaCheque = 0;
        ObservableList<String> datoes = listViewCheques.getItems();
        boolean estado = false;
        for (String x : datoes) {
            contChq++;
            StringTokenizer st = new StringTokenizer(x, "-");
            String banco = st.nextElement().toString();
            String cod = st.nextElement().toString();
            String monto = st.nextElement().toString();
            String mon = monto.replaceAll("", " (Borrar)");
            sumChq += Integer.valueOf(numValidator.numberValidator(mon));
            sumaCheque += Integer.valueOf(numValidator.numberValidator(mon));
            arrayCheque.add(banco + " - " + cod + " - " + monto);
            estado = true;
        }
        datos.put("contCheque", contChq);
        datos.put("sumCheque", sumChq);
        if (estado) {
            datos.put("arrayCheque", arrayCheque);
        }
        formaDePago.put("monCheque", sumaCheque);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoEfectivos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contEfect = 0;
        if (!json.isNull("cantEfectivoRecibido")) {
            datos.put("cantEfectivoRecibido", Integer.parseInt(datos.get("cantEfectivoRecibido").toString()) + Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText())));
        } else {
            datos.put("cantEfectivoRecibido", numValidator.numberValidator(textFieldEfectivo.getText()));
        }
        if (!json.isNull("contEfectivo")) {
            contEfect = Integer.parseInt(datos.get("contEfectivo").toString());
        }
        int montoActual = Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        long dif = totalAbonado - totalAPagar;
        if (dif == 0) {
            contEfect += 1;
            datos.put("contEfectivo", contEfect);
            //por si vaya la energia electrica
            formaDePago.put("monEfectivo", montoActual);
        } else {
            int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
            if (montoActual > vuel) {
                int resultado = montoActual - vuel;
                contEfect += 1;
                datos.put("contEfectivo", contEfect);
                formaDePago.put("monEfectivo", resultado);
            } else {
                datos.put("contEfectivo", 0);
                formaDePago.put("monEfectivo", 0);
            }
        }
    }

    private void seteandoFuncionario() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!txtNombreFuncionario.getText().equalsIgnoreCase("")) {
            int nFun = 0;
            if (!json.isNull("nFuncionarios")) {
                nFun = Integer.parseInt(datos.get("nFuncionarios").toString());
            }
            datos.put("nFuncionarios", (nFun + 1));
        }
    }

    private void seteandoSumaTotales() {
        int sumTotales = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("totales")) {
            sumTotales = Integer.parseInt(datos.get("totales").toString());
        }
        int monto = sumTotales + Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
        datos.put("totales", monto);
    }

    private void seteandoNotaCred() {
        int notaCred = 0;
        int sumNotaCred = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("contNotaCred")) {
            notaCred = Integer.parseInt(datos.get("contNotaCred").toString());
        }
        if (!json.isNull("sumNotaCred")) {
            sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
        }
        int sumaNota = 0;
        ObservableList<String> datoes = listViewNotasCred.getItems();
        for (String x : datoes) {
            notaCred += 1;
            StringTokenizer st = new StringTokenizer(x, "-");
            String nro = st.nextElement().toString();
            String monto = st.nextElement().toString();
            String mon = monto.replaceAll("", " (Borrar)");
            sumNotaCred += Integer.valueOf(numValidator.numberValidator(mon));
            sumaNota += Integer.valueOf(numValidator.numberValidator(mon));
        }
        datos.put("sumNotaCred", sumNotaCred);
        datos.put("contNotaCred", notaCred);
        formaDePago.put("monNotaCred", sumaNota);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoVales() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contVale = 0;
        if (!json.isNull("")) {
            contVale = Integer.parseInt(datos.get("contVale").toString());
        }
        datos.put("contVale", contVale + 1);
        int sumVale = 0;
        if (!json.isNull("sumVale")) {
            sumVale = Integer.parseInt(datos.get("sumVale").toString());
        }
        datos.put("sumVale", (sumVale + Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()))));
        formaDePago.put("monVale", Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText())));
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoAsociacion() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contAsoc = 0;
        if (!json.isNull("contAsoc")) {
            contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
        }
        datos.put("contAsoc", contAsoc + 1);
        int sumAsoc = 0;
        if (!json.isNull("sumAsoc")) {
            sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
        }
        datos.put("sumAsoc", (sumAsoc + Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()))));
        formaDePago.put("monAsoc", Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText())));
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoDolares() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDolar = 0;
        if (!json.isNull("contDolar")) {
            contDolar = Integer.parseInt(datos.get("contDolar").toString());
        }
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
        datos.put("contDolar", (contDolar + cantidad));
        String x = textFieldDolarGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        int sumDolar = 0;
        if (!json.isNull("sumDolar")) {
            sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
        }
        datos.put("sumDolar", (sumDolar + monto));
        formaDePago.put("monDolar", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoReales() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contReal = 0;
        int sumReal = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
        if (!json.isNull("contReal")) {
            contReal = Integer.parseInt(datos.get("contReal").toString());
        }
        datos.put("contReal", (contReal + cantidad));
        String x = textFieldRealGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        if (!json.isNull("sumReal")) {
            sumReal = Integer.parseInt(datos.get("sumReal").toString());
        }
        datos.put("sumReal", (sumReal + monto));
        formaDePago.put("monReal", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoPesos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contPeso = 0;
        int sumPeso = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
        if (!json.isNull("contPeso")) {
            contPeso = Integer.parseInt(datos.get("contPeso").toString());
        }
        datos.put("contPeso", (contPeso + cantidad));
        String x = textFieldPesoGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        if (!json.isNull("sumPeso")) {
            sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
        }
        datos.put("sumPeso", (sumPeso + monto));
        formaDePago.put("monPeso", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void setearEfectivoPorOtraFormaDePago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        long dif = totalAbonado - totalAPagar;
        if (textFieldEfectivo.getText().equalsIgnoreCase("") && dif >= 0) {
            if (estado) {
                estado = false;
                int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
                //para cuando cae la corriente electrica
                int monEfec = 0;
                if (!json.isNull("monEfectivo")) {
                    monEfec = Integer.parseInt(datos.get("monEfectivo").toString());
                }
                datos.put("monEfectivo", (monEfec - vuel));
            }
        }
    }

    private void seteandoRetencionciones() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int sumRetencion = 0;
        int contRetencion = 0;
        if (!json.isNull("sumRetencion")) {
            sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
        }
        if (!json.isNull("contRetencion")) {
            contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
        }
        int montoRetencion = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        datos.put("sumRetencion", (sumRetencion + montoRetencion));
        formaDePago.put("monRetencion", montoRetencion);
        datos.put("contRetencion", (contRetencion + 1));
    }
    //SET SET SET SET SET SET ************************** -> -> -> -> -> -> -> ->

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void cargarClientePorServicio() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject jsonData = new org.json.JSONObject(datos);
        long idClientePendiente = Long.parseLong(datos.get("idClientePendiente").toString());
        ClientePendienteDTO cpDTO = cpDAO.getById(idClientePendiente).toClientePendienteDTO();

        try {
            if (jsonData.isNull("idRangoFacturaActual") || Long.parseLong(datos.get("idRangoFacturaActual").toString()) == 0) {
                cabFactura = FacturaVentaEsteticaFXMLController.creandoJsonFactCab();
                creandoCabecera();
                fact.put("facturaClienteCab", cabFactura);
            }
            JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            JSONObject cliente = new JSONObject();
            cliente.put("idCliente", cpDTO.getCliente().getIdCliente());
            cab.put("cliente", cliente);
            fact.put("facturaClienteCab", cab);
            DatosEnCaja.setFacturados(fact);
            actualizarDatos();

            txtCiCliente.setText(cpDTO.getCliente().getRuc());
            txtNombreCliente.setText(cpDTO.getCliente().getNombre() + " " + cpDTO.getCliente().getApellido());
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

    private void creandoCabecera() throws ParseException {
        JSONParser parser = new JSONParser();

        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        TalonariosSucursales tal = taloDAO.getById(idTalonario);
        cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));

        datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
        //Linea para prueba por el rango actual
        long n = 0l;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("idRangoFacturaActual")) {
            n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
        }
        cabFactura.put("idFacturaClienteCab", n);
        tal.setNroActual(tal.getNroActual() + 1);
        taloDAO.actualizarNroActual(tal);
        //recuperarNroActual y otros datos para la numeracion de la FACTURA
        String nroAct = cabFactura.get("nroActual").toString();
        Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
        // primer trío
        JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
        JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
        long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
        // segundo trío
        long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
        String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
        cabFactura.put("nroFactura", nroFact);
        datos.put("nroFact", nroFact);
    }

    private void cargarClienteActual(long idCliente) {
        Cliente c = cDAO.getById(idCliente);
        txtCiCliente.setText(c.getRuc());
        String ape = c.getApellido();
        if (ape != null) {
            txtNombreCliente.setText(c.getNombre() + " " + ape);
        } else {
            txtNombreCliente.setText(c.getNombre());
        }
    }

    private static void crearCabeceraLocalmente() {
        try {
            ObjectMapper mapper = new ObjectMapper();

            JSONObject cabFactura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());

            JSONObject ef = (JSONObject) parser.parse(cabFactura.get("estadoFactura").toString());
            JSONObject s = (JSONObject) parser.parse(cabFactura.get("sucursal").toString());

            JSONObject jsonFact = new JSONObject();

            JSONObject estadoFact = new JSONObject();
            estadoFact.put("idEstadoFactura", Long.parseLong(ef.get("idEstadoFactura").toString()));

            JSONObject sucursal = new JSONObject();
            sucursal.put("idSucursal", Long.parseLong(s.get("idSucursal").toString()));

            jsonFact.put("nroFactura", cabFactura.get("nroFactura").toString());
            jsonFact.put("fechaEmision", null);
            jsonFact.put("idFacturaClienteCab", Long.parseLong(cabFactura.get("idFacturaClienteCab").toString()));
            jsonFact.put("estadoFactura", estadoFact);
            jsonFact.put("sucursal", sucursal);

            String jsonObj = jsonFact.toString();
            FacturaClienteCabDTO factDTO = mapper.readValue(jsonObj, FacturaClienteCabDTO.class
            );
            long fe = Long.parseLong(cabFactura.get("fechaEmision").toString());
            factDTO.setFechaEmision(new Timestamp(fe));

            factDTO.setCancelado(null);
            factDTO.setUsuAlta(null);
            factDTO.setUsuMod(null);
            factDTO.setFechaMod(null);
            factDTO.setMontoFactura(null);
            factDTO.setNroActual(null);
            factDTO.setNroActual(null);

            factDTO.setCaja(null);
            factDTO.setCliente(null);
            factDTO.setTipoComprobante(null);
            factDTO.setTipoMoneda(null);

            FacturaClienteCab fcc = FacturaClienteCab.fromEstadoAndSucursal(factDTO);
            FacturaClienteCab fact = factDAO.insertarObtenerObjeto(fcc);

            if (fact != null) {
                System.out.println("FACTURA CLIENTE CAB AGREGADOS EXITOSAMENTE");
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        } catch (IOException ex) {
            Utilidades.log.error("IOException", ex.fillInStackTrace());
        }

    }

    private static void actualizarCabeceraLocalmente() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            JSONObject cabFactura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());

            JSONObject ef = (JSONObject) parser.parse(cabFactura.get("estadoFactura").toString());
            JSONObject s = (JSONObject) parser.parse(cabFactura.get("sucursal").toString());

            JSONObject jsonFact = new JSONObject();

            JSONObject estadoFact = new JSONObject();
            estadoFact.put("idEstadoFactura", Long.parseLong(ef.get("idEstadoFactura").toString()));

            JSONObject sucursal = new JSONObject();
            sucursal.put("idSucursal", Long.parseLong(s.get("idSucursal").toString()));

            jsonFact.put("nroFactura", cabFactura.get("nroFactura").toString());
            jsonFact.put("fechaEmision", null);
            jsonFact.put("idFacturaClienteCab", Long.parseLong(cabFactura.get("idFacturaClienteCab").toString()));
            jsonFact.put("estadoFactura", estadoFact);
            jsonFact.put("sucursal", sucursal);

            String jsonObj = jsonFact.toString();
            FacturaClienteCabDTO factDTO = mapper.readValue(jsonObj, FacturaClienteCabDTO.class
            );
            long fe = Long.parseLong(cabFactura.get("fechaEmision").toString());
            factDTO.setFechaEmision(new Timestamp(fe));

            factDTO.setCancelado(null);
            factDTO.setUsuAlta(null);
            factDTO.setUsuMod(null);
            factDTO.setFechaMod(null);
            factDTO.setMontoFactura(null);
            factDTO.setNroActual(null);
            factDTO.setNroActual(null);

            factDTO.setCaja(null);
            factDTO.setCliente(null);
            factDTO.setTipoComprobante(null);
            factDTO.setTipoMoneda(null);

            FacturaClienteCab fact = factDAO.actualizarObtenerObjeto(FacturaClienteCab.fromEstadoAndSucursal(factDTO));

            if (fact != null) {
                System.out.println("FACTURA CLIENTE CAB ACTUALIZADOS EXITOSAMENTE");
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        } catch (IOException ex) {
            Utilidades.log.error("IOException", ex.fillInStackTrace());
        }
    }

    private static JSONObject persistiendoFactCabCliPendLocal(String jsonFactCabCliPend) {
        ConexionPostgres.conectar();
        JSONParser parser = new JSONParser();
        JSONObject jsonRetorno = null;
        String sql = "INSERT INTO estetica.fact_cab_cli_pend (descripcion_dato, operacion, fecha) VALUES ('" + jsonFactCabCliPend + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                jsonRetorno = (JSONObject) parser.parse(jsonFactCabCliPend);
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            jsonRetorno = null;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                jsonRetorno = null;
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            jsonRetorno = null;
        }
        ConexionPostgres.cerrar();
        return jsonRetorno;
    }

    @FXML
    private void listViewNotasCredKeyReleased(KeyEvent event) {
    }

    private void tootTip() {
        toolTipPanelTarj = new Tooltip();
    }

    private void habilitandoCajaRapida() {
        panelMontoRetencion.setDisable(true);
        panelMontoRetencion.setStyle("-fx-opacity:  0.85;");
        deshabilitar = true;
    }

    private void validandoTarjetaListen() {
        if (comboBoxTarjetas.getSelectionModel().getSelectedIndex() > 0) {
            if (Descuento.getHashDescTarjeta() != null) {
                if (Descuento.getHashDescTarjeta().get(comboBoxTarjetas.getSelectionModel().getSelectedItem()) != null) {
                    JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(comboBoxTarjetas.getSelectionModel().getSelectedItem());
                    if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                        Platform.runLater(() -> {
                            Point2D point2D = panelBusquedaTipoTarj.localToScreen((panelBusquedaTipoTarj.getLayoutBounds().getMaxX() / 2),
                                    (panelBusquedaTipoTarj.getLayoutBounds().getMinY() - (comboBoxTarjetas.getHeight() * 2.1)));
                            toolTipPanelTarj.setStyle("-fx-font: normal bold 16 System; "
                                    + "-fx-background-color: white; -fx-border-color: #d70f23; "
                                    + "-fx-text-fill: #d70f23;");
                            toolTipPanelTarj.setText("MÍNIMO REQUERIDO PARA\nEL DTO. TARJETA "
                                    + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonTarjCab.get("montoMin").toString())));
                            toolTipPanelTarj.show(panelBusquedaTipoTarj, point2D.getX(), point2D.getY());
                            toolTipPanelTarj = AnimationFX.fadeTooltip(toolTipPanelTarj);
                        });
                    } else {
                        toolTipPanelTarj.hide();
                    }
                } else {
                    toolTipPanelTarj.hide();
                }
            } else {
                toolTipPanelTarj.hide();
            }
        } else {
            if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                desbloqueandoPanelesTarj();
            }
            toolTipPanelTarj.hide();
        }
        comboBoxTarjeta();
    }

    private boolean validandoTarjetaConvenioTarjCredDeb() {
        boolean descTarjDetectado = false;
        String descripcionTarj = "N/A";
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            String[] parts = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
            descripcionTarj = parts[0];
        } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
            descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
        }
        if (Descuento.getHashDescTarjeta() != null) {
            if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
                JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
                if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) == -1
                        || (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) <= Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())))) {
                    JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
                    Calendar calendar = Calendar.getInstance();
                    JSONArray jsonArrayDiaEsp = new JSONArray();
                    int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                    for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
                        JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                        JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                        int dia = toIntExact((long) jsonDia.get("idDia"));
                        if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
                            String fechaIni = (String) jsonTarjCab.get("horaInicio");
                            String fechaFin = (String) jsonTarjCab.get("horaFin");
                            if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                Time fechaIniT = Utilidades.objectToTime(fechaIni);
                                Time fechaFinT = Utilidades.objectToTime(fechaFin);
                                Date date = new Date();
                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                Time t = Utilidades.objectToTime(hora);
                                if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                            descTarjDetectado = true;
                                            break;
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                    //se omite la bajada por el tema del convenio...
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                } else {
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        descTarjDetectado = true;
                                        break;
                                    }
                                }
                            } else {
                                JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                    if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                        descTarjDetectado = true;
                                        break;
                                    } else {
                                        for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                            if ((Boolean) jsonTarjCab.get("extracto")) {
                                                //se omite la bajada por el tema del convenio...
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            } else {
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    descTarjDetectado = true;
                                    break;
                                }
                            }
                        } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
                            jsonArrayDiaEsp.add(jsonTarjDet);
                        }
                    }
                    if (!jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
                        HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
                        if (hashDiaEspecial != null) {
                            for (Object tarjDetObj : jsonArrayDiaEsp) {
                                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                                if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
                                    if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
                                        String fechaIni = (String) jsonTarjCab.get("horaInicio");
                                        String fechaFin = (String) jsonTarjCab.get("horaFin");
                                        if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                            Time fechaIniT = Time.valueOf(fechaIni);
                                            Time fechaFinT = Time.valueOf(fechaFin);
                                            Date date = new Date();
                                            String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                            Time t = Time.valueOf(hora);
                                            if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        } else {
                                            JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                            if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                    descTarjDetectado = true;
                                                    break;
                                                } else {
                                                    for (JSONObject detalleArtJson : FacturaVentaEsteticaFXMLController.getDetalleArtList()) {
                                                        JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                        if ((Boolean) jsonTarjCab.get("extracto")) {
                                                            //se omite la bajada por el tema del convenio...
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        } else {
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                    //tool tip
                }
            }
        }
        return descTarjDetectado;
    }
    //TARJETAS CONVENIO*********************************************************

    private void calculandoValePorc() {
        if (!textFieldMontoValePorc.getText().isEmpty() && descuentoValePorcSum <= 0) {
            descuentoValePorcSum = (Long.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            descuento += descuentoValePorcSum;
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
//            if (descuento > 0) {
//                lineStrokeTotal.setVisible(true);
//            } else {
//                lineStrokeTotal.setVisible(false);
//            }
        }
    }
    //VALE*******************************************************
}
