/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.TarjetaClienteFiel;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.ClienteDTO;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dto.TarjetaClienteFielDTO;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.estetica.FacturaDeVentaEsteticaFXMLController.parser;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import java.util.Date;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class BuscarClienteEsteticaFXMLController extends BaseScreenController implements Initializable {

    public static void cargarCliente(Label labelRucCliente, Label labelNombreCliente, TextField txtCod) {
        lblRuc = labelRucCliente;
        lblNombre = labelNombreCliente;
        txtCodigo = txtCod;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private static TextField txtCodigo;
    private static Label lblRuc;
    private static Label lblNombre;
    private boolean exitoEditar;
    private static boolean clienteSi;
    private long idCliente;
    private int codCliente;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean alertEscEnter;
//    boolean crearNuevo;

    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    private List<JSONObject> clienteFielList;
    private static JSONObject jsonClienteFiel;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente = new JSONObject();
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private JSONObject clienteFiel;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private GridPane gridPaneClienteNuevo11;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private TextField textFieldNombreCliente;
    @FXML
    private TextField textFieldRucCliente;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private Label labelRetiroDinero;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
//        buscandoCliente(false);
    }

    private void buttonActualizarAction(ActionEvent event) {
//        editandoCliente();
    }

    private void buttonBuscarClienteEsteticaAction(ActionEvent event) {
        buscandoCliente(true);
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
//        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//            if (ClienteFielFXMLController.getJsonClienteFiel().get("cipas") != null) {
//                textFieldRucCliente.setText(ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//        }
        alertEscEnter = false;
        clienteFiel = new JSONObject();

        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        numVal = new NumberValidator();
        exitoCrear = false;
        exitoEditar = false;
        escucha = false;
        alert = false;
        clienteSi = false;
        idCliente = 0l;
        codCliente = -1;
    }
    //INICIAL INICIAL INICIAL **************************************************

    static void seteandoParamClienteFiel(JSONObject clienteFiel) {
        jsonClienteFiel = clienteFiel;
    }

    public static void resetParamClienteFiel() {
        jsonClienteFiel = null;
    }//NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    public static void seteandoParamCliente(JSONObject cliente) {
        jsonCliente = cliente;
    }

    public static void resetParamCliente() {
        jsonCliente = null;
        clienteSi = false;
    }

    private void navegandoFacturaVenta() {
        clienteSi = true;
        FacturaDeVentaEsteticaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/BuscarClienteEsteticaFXML.fxml", 602, 87, false);
    }

    private void volviendo() {
        clienteSi = false;
        FacturaDeVentaEsteticaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/BuscarClienteEsteticaFXML.fxml", 602, 87, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
//        if (altC.match(event)) {
//            if (textFieldNombreCliente.getText().equals("") || textFieldRucCliente.getText().equals("")) {
//                mensajeAlerta("El campo RUC/CI y NOMBRE no deben quedar vacíos");
//            } else {
//                try {
//                    org.json.JSONObject json = new org.json.JSONObject(clienteList.get(0));
//                    seteandoParamCliente(clienteList.get(0));
//                    lblRuc.setText("RUC/CI: " + clienteList.get(0).get("ruc").toString());
//                    if (json.isNull("apellido")) {
//                        lblNombre.setText(clienteList.get(0).get("nombre").toString());
//                    } else {
//                        lblNombre.setText(clienteList.get(0).get("nombre").toString() + " " + clienteList.get(0).get("apellido").toString());
//                    }
//                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idCliente", clienteList.get(0).get("idCliente"));
//                    cab.put("cliente", clienteList.get(0));
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
//                    buscandoClienteFiel();
//                    navegandoFacturaVenta();
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//                }
//            }
//        }

        if (keyCode == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (buscandoCliente(true)) {
                    if (textFieldNombreCliente.getText().equals("") || textFieldRucCliente.getText().equals("")) {
                        mensajeAlerta("El campo RUC/CI y NOMBRE no deben quedar vacíos");
                    } else {
                        try {
                            org.json.JSONObject json = new org.json.JSONObject(clienteList.get(0));
                            seteandoParamCliente(clienteList.get(0));
                            lblRuc.setText("RUC/CI: " + clienteList.get(0).get("ruc").toString());
                            if (json.isNull("apellido")) {
                                lblNombre.setText(clienteList.get(0).get("nombre").toString());
                            } else {
                                lblNombre.setText(clienteList.get(0).get("nombre").toString() + " " + clienteList.get(0).get("apellido").toString());
                            }

                            org.json.JSONObject jsonFactura = new org.json.JSONObject((JSONObject) parser.parse(fact.toString()));
                            if (jsonFactura.isNull("facturaClienteCab")) {
                                JSONObject jsonObj = new JSONObject();
                                JSONObject cliente = new JSONObject();
                                cliente.put("idCliente", clienteList.get(0).get("idCliente"));
                                jsonObj.put("cliente", clienteList.get(0));
                                fact.put("facturaClienteCab", jsonObj);
                            } else {
                                JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                                JSONObject cliente = new JSONObject();
                                cliente.put("idCliente", clienteList.get(0).get("idCliente"));
                                cab.put("cliente", clienteList.get(0));
                                fact.put("facturaClienteCab", cab);
                            }
//
//                            actualizarDatos();
                            buscandoClienteFiel();
                            navegandoFacturaVenta();
                        } catch (Exception ex) {
                            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                        }
                    }
                } else {
                    NuevoClienteEsteticaFXMLController.setRucCliente(textFieldRucCliente.getText(), txtCodigo);
                    NuevoClienteEsteticaFXMLController.setLabels(lblNombre, lblRuc);
                    if (StageSecond.getStageData().isShowing()) {
                        StageSecond.getStageData().close();
                    }
                    this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/BuscarClienteEsteticaFXML.fxml", 602, 87, true);
//                    this.sc.loadScreen("/vista/estetica/NuevoClienteEsteticaFXML.fxml", 519, 187, "/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
                }
            } else {
                alertEscEnter = false;
            }
        }

        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
//        if (keyCode == event.getCode().ENTER) {
//            JSONParser parser = new JSONParser();
//            if (alert) {
//                alert = false;
//            } else if (!buttonAnhadir.isDisable()) {
//                try {
//                    seteandoParam(jsonSeleccionActual);
//                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idCliente", idCliente);
//                    cab.put("cliente", cliente);
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
//                    navegandoFormaPago();
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//                }
//            }
//        }
//        if (keyCode == event.getCode().DELETE) {
//            if (alert) {
//                alert = false;
//            } else if (buttonBorrar.isVisible()) {
//                JSONParser parser = new JSONParser();
//                resetParam();
//                navegandoFormaPago();
//
//                //nuevo, se utiliza para cuando se elimina un cliente.
//                try {
//                    JSONObject jsonFactura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    jsonFactura.remove("cliente");
//                    jsonFactura.put("cliente", null);
//                    fact.put("facturaClienteCab", jsonFactura);
//                    actualizarDatos();
//                } catch (ParseException ex) {
//                    Utilidades.log.info(ex.getLocalizedMessage());
//                }
//            }
//        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoClienteFiel() {
        String nom, ape, cipas;
        nom = "null";
        if (textFieldRucCliente.getText().contains("-")) {
            StringTokenizer st = new StringTokenizer(textFieldRucCliente.getText(), "-");
            cipas = st.nextElement().toString();
        } else {
            cipas = textFieldRucCliente.getText();
        }

        ape = "null";
        clienteFielList = jsonArrayClienteFiel(nom, ape, cipas);
        if (clienteFielList.isEmpty()) {
//registrar cliente fiel y retornar en json cliente fiel
            nuevoClienteFiel();
        } else {
            JSONObject cab = new JSONObject();
//            try {
//                cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                JSONObject cliente = new JSONObject();
//                cliente.put("idTarjetaClienteFiel", clienteFielList.get(0).get("idTarjetaClienteFiel"));
//                cab.put("clienteFiel", clienteFielList.get(0));
            seteandoParamClienteFiel(clienteFielList.get(0));
//                fact.put("facturaClienteCab", cab);
//                actualizarDatos();
//            } catch (ParseException ex) {
//                Logger.getLogger(NuevoClienteEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }

    private void nuevoClienteFiel() {
        String soloCi = "";
        if (textFieldRucCliente.getText().contains("-")) {
            StringTokenizer st = new StringTokenizer(textFieldRucCliente.getText(), "-");
            soloCi = st.nextElement().toString();
        } else {
            soloCi = textFieldRucCliente.getText();
        }
//        String soloCi = st.nextElement().toString();
        List<TarjetaClienteFiel> listTarjFiel = tarDAO.filtrarPorNomApeCi("null", "null", soloCi);
        if (listTarjFiel.toString().equalsIgnoreCase("[]")) {

            if (insertarNuevoClienteEsteticaFiel()) {
//                buscarClienteFielPorId(Long.parseLong(clienteFiel.get("idTarjetaClienteFiel").toString()));
//                buscarClienteFielPorId(Long.valueOf(clienteFiel.get("idTarjetaClienteFiel").toString()));

//                JSONObject cab;
//                try {
//                    cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idTarjetaClienteFiel", clienteFiel.get("idTarjetaClienteFiel"));
//                    cab.put("clienteFiel", clienteFiel);
                seteandoParamClienteFiel(clienteFiel);
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
//                } catch (ParseException ex) {
//                    Logger.getLogger(NuevoClienteEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
        }
    }

    private boolean insertarNuevoClienteEsteticaFiel() {
        try {
            boolean exitoInsertar = false;
//            Jsonb jsonb = JsonbBuilder.create();
            JSONParser parser = new JSONParser();
            //seteando datos faltantes
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            clienteFiel.put("idTarjetaClienteFiel", rangoClifielDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal()));
            clienteFiel.put("fechaAlta", timestampEmision);
            clienteFiel.put("fechaMod", timestampEmision);
            clienteFiel.put("usuAlta", Identity.getNomFun());
            clienteFiel.put("usuMod", Identity.getNomFun());
            clienteFiel.put("empresaSuc", empresa.get("descripcionEmpresa").toString());

//            JSONObject cliente = new JSONObject();
            JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            org.json.JSONObject jsonCabecera = new org.json.JSONObject(cab);
            if (!jsonCabecera.isNull("cliente")) {
                JSONObject jsonCli = (JSONObject) parser.parse(jsonCabecera.getJSONObject("cliente").toString());
                jsonCli.remove("email");
//                cliente.put("cliente", jsonCli);
                clienteFiel.put("cliente", jsonCli);

                String soloCi = "";
                if (textFieldRucCliente.getText().contains("-")) {
                    StringTokenizer st = new StringTokenizer(textFieldRucCliente.getText(), "-");
                    soloCi = st.nextElement().toString();
                } else {
                    soloCi = textFieldRucCliente.getText();
                }
                clienteFiel.put("cipas", soloCi);
            }

            exitoInsertar = registrandoClienteFielLocal(clienteFiel.toString());
            if (exitoInsertar) {
                //                    TarjetaClienteFiel tarFiel = mapper.readValue(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                clienteFiel.remove("fechaAlta");
                clienteFiel.remove("fechaMod");
                TarjetaClienteFiel tarFiel = gson.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                tarFiel.setFechaAlta(tsNow);
                tarFiel.setFechaMod(tsNow);

//                TarjetaClienteFiel tarFiel = jsonb.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                boolean valor = tarDAO.insetarObtenerObj(tarFiel);
                if (valor) {
                    System.out.println("HAZ REGISTRADO UN CLIENTE FIEL");
                } else {
                    System.out.println("ERROR: AL INTENTAR REGISTRAR UN CLIENTE FIEL");
                }
//                textFieldCodClienteFiel.setText(clienteFiel.get("cipas").toString());
            }
            return exitoInsertar;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }

    private boolean registrandoClienteFielLocal(String json) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO pendiente.clienfiel_pendientes (fecha_registro ,msj, tabla, dml, ip) VALUES (now(),'" + json + "','tarjeta_cliente_fiel', 'I', '" + Utilidades.host + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private List<JSONObject> jsonArrayClienteFiel(String nom, String ape, String ciPass) {
        //carácter "espacio", formato...
        nom = nom.replaceAll(" ", "%20");
        ape = ape.replaceAll(" ", "%20");
        ciPass = ciPass.replaceAll(" ", "%20");
        //carácter "espacio", formato...
        List<JSONObject> clienteFielJSONObjList = new ArrayList<JSONObject>();
        clienteFielJSONObjList = generarTarjetaClienteFielLocal(nom, ape, ciPass);
        return clienteFielJSONObjList;
    }

    private List<JSONObject> generarTarjetaClienteFielLocal(String nom, String ape, String ciPass) {
        List<JSONObject> listaJSON = new ArrayList<>();
        JSONParser parser = new JSONParser();
        List<TarjetaClienteFiel> tarFiel = tarDAO.filtrarPorNomApeCi(nom, ape, ciPass);
        for (TarjetaClienteFiel tar : tarFiel) {
            try {
                TarjetaClienteFielDTO tarjetaFielDTO = tar.toTarjetaClienteFielDTO();
                String obj = gson.toJson(tarjetaFielDTO);
                listaJSON.add((JSONObject) parser.parse(obj));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaJSON;
    }

    private void borrando() {
        resetParamCliente();
        resetParamClienteFiel();
        navegandoFacturaVenta();
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ape, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ape, ruc);
        return clienteJSONObjList;
    }

    private boolean buscandoCliente(boolean efeuno) {
        String nom, ape, ruc;
        boolean valor = false;
//        if (textFieldNombreCliente.getText().contentEquals("")) {
        nom = "null";
//        } else {
//            nom = textFieldNombreCliente.getText();
//        }
        if (textFieldRucCliente.getText().contentEquals("")) {
            ruc = "null";
        } else {
            ruc = textFieldRucCliente.getText();
        }
        ape = "null";
//        if (textFieldApellidoCliente.getText().contentEquals("")) {
//            ape = "null";
//        } else {
//            ape = textFieldApellidoCliente.getText();
//        }

        if (nom.equals("null") && ape.equals("null") && !ruc.equals("null")) {
            buscarClientePorRucCi();
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
                textFieldNombreCliente.setText("");
            } else {
                org.json.JSONObject json = new org.json.JSONObject(clienteList.get(0));
                if (!json.isNull("apellido")) {
                    textFieldNombreCliente.setText(json.getString("nombre") + " " + json.getString("apellido"));
                } else {
                    textFieldNombreCliente.setText(json.getString("nombre"));
                }
                valor = true;
            }
//            actualizandoTablaCliente();
        } else {
            clienteList = jsonArrayCliente(nom, ape, ruc);
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
//            actualizandoTablaCliente();
        }
        return valor;
    }

    private void buscarClientePorRucCi() {
        String rucCliente = "";
        JSONParser parser = new JSONParser();
        try {
            rucCliente = textFieldRucCliente.getText();
            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
//            int contador = rucCliente.split("-", -1).length - 1;
            Cliente cliente = new Cliente();
//            if (contador >= 1) {
            //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
//                cliente = cliDAO.listarPorRuc(rucCliente);
//                if (cliente.getNombre() != null) {
//                    cliente.setFecNac(null);
//                    cliente.setFechaAlta(null);
//                    cliente.setFechaMod(null);
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toClienteCajaDTO()));
//                    clienteList.add(jsonCli);
//                }
//            } else if (contador == 0) {
            //Listar por CI en el caso que exista
            String ruc = "";
            if (!isNumeric(rucCliente)) {
                ruc = rucCliente;
            } else {
                StringTokenizer st = new StringTokenizer(rucCliente, "-");
                ruc = st.nextElement().toString();
            }

            cliente = cliDAO.listarPorRucExacto(ruc);
//            if (isNumeric(ruc)) {
//                cliente.setRuc(Utilidades.calculoSET(ruc));
//            } else {
//            cliente.setRuc(ruc);
//            }

            if (cliente.getIdCliente() == null) {
                // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
                List<Cliente> listClien = cliDAO.listarPorCIRevancha(ruc);
                if (listClien != null) {
                    if (listClien.isEmpty()) {
                        clienteList = new ArrayList<>();
                    } else {
                        clienteList = new ArrayList<>();
                        for (int i = 0; i < listClien.size(); i++) {
                            Cliente cli = listClien.get(i);
                            StringTokenizer st00 = new StringTokenizer(cli.getRuc(), "-");
                            String rucCi = st00.nextElement().toString();

                            cli.setFecNac(null);
                            cli.setFechaAlta(null);
                            cli.setFechaMod(null);
                            cli.setRuc(Utilidades.calculoSET(rucCi));
                            JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toClienteCajaDTO()));
                            clienteList.add(jsonCli);
                        }
                    }
                } else {
                    clienteList = new ArrayList<>();
                }
            } else {
                clienteList = new ArrayList<>();
                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toClienteCajaDTO()));
                clienteList.add(jsonCli);
            }
//            }
            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
        } catch (Exception e) {
            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
        }
    }

    private void buscarAutomatico(JSONObject cliente) {
        if (cliente != null) {
            if (cliente.get("ruc") != null) {
                textFieldRucCliente.setText(cliente.get("ruc").toString());
            } else {
                textFieldRucCliente.setText("");
            }
            textFieldNombreCliente.setText(cliente.get("nombre").toString() + " " + cliente.get("apellido").toString());
//            textFieldApellidoCliente.setText(cliente.get("apellido").toString());
            buscandoCliente(true);
        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            JSONObject cliente = new JSONObject();
//                cliente = creandoJsonCliente();
            int codCli = Integer.parseInt(cliente.get("codCliente").toString());
            Cliente clie = cliDAO.getByCod(codCli);
            if (clie == null) {
                long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                cliente.put("idCliente", idActual);
                exitoCrear = persistiendoPendientes(cliente);
                if (exitoCrear) {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    cliente.put("fechaAlta", null);
                    cliente.put("fechaMod", null);
                    ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
                    cliDTO.setFechaAlta(timestamp);
                    cliDTO.setFechaMod(timestamp);
                    Cliente cli = Cliente.fromClienteDTO(cliDTO);
                    Pais pais = new Pais();
                    pais.setIdPais(0L);
                    Departamento dpto = new Departamento();
                    dpto.setIdDepartamento(0l);
                    Ciudad ciu = new Ciudad();
                    ciu.setIdCiudad(0l);
                    Barrio barr = new Barrio();
                    barr.setIdBarrio(0l);
                    cli.setPais(pais);
                    cli.setDepartamento(dpto);
                    cli.setCiudad(ciu);
                    cli.setBarrio(barr);
                    try {
                        cliDAO.insertar(cli);
                        System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
//                            textFieldRucClienteNuevo.setText("");
//                            textFieldNombreClienteNuevo.setText("");
//                            textFieldClienteApellidoNuevo.setText("");
//                            textFieldClienteCelularNuevo.setText("");
//                            textFieldClienteTelefonoNuevo.setText("");
                    } catch (Exception e) {
                        Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
                    }
                    Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
                    this.alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ButtonType.OK) {
                        alert2.close();
                        buscarAutomatico(cliente);
                    } else {
                        alert2.close();
                        buscarAutomatico(cliente);
                    }
                } else {
                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                    this.alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ButtonType.CLOSE) {
                        alert2.close();
                    }
                }
            } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
                new Toaster().mensajeGenerico("Mensaje del Sistema", "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", "", 2);
            }
        } else {
            alert.close();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/BuscarClienteEsteticaFXML.fxml", 602, 87, false);
//        }
        return exitoCrear;
    }
    //////CREATE, CLIENTE -> POST

    //////UPDATE, CLIENTE -> PUT
//    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
//            this.alert = true;
//            alert.showAndWait();
//            if (alert.getResult() == ok) {
//                alert.close();
//                JSONObject cliente = new JSONObject();
//                cliente = editandoJsonCliente();
//                exitoEditar = actualizarPendientes(cliente);
//                if (exitoEditar) {
//                    try {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        JSONObject usuarioCajero = Identity.getUsuario();
//                        JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
//                        String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
//                        cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
//                        System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
//                    } catch (Exception e) {
//                        Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//                        System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
//                    }
//                    Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.OK) {
//                        alert2.close();
//                        buscarAutomatico(cliente);
//                    } else {
//                        alert2.close();
//                        buscarAutomatico(cliente);
//                    }
//                } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
//                }
//            } else {
//                alert.close();
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/BuscarClienteEsteticaFXML.fxml", 602, 87, false);
//        }
//        return exitoEditar;
//    }
    //////UPDATE, CLIENTE -> PUT
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
//        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//        alertEscEnter = true;
//        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            this.alert = true;
//            alert2.close();
//        }
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ape, String ruc) {
        JSONParser parser = new JSONParser();
        List<Cliente> cliente = cliDAO.listarPorNomRuc(nom, ape, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Cliente cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toClienteCajaDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    @FXML
    private void textFieldRucClienteKeyPress(KeyEvent event) {
        listenRuc(event);
    }

    private void listenRuc(KeyEvent event) {
        textFieldRucCliente.textProperty().addListener((observable, oldValue, newValue) -> {
            Platform.runLater(() -> {
                if (newValue.equals("")) {
                    textFieldNombreCliente.setText("");
                }
            });
        });
    }

    public static JSONObject getJsonClienteFiel() {
        return jsonClienteFiel;
    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
