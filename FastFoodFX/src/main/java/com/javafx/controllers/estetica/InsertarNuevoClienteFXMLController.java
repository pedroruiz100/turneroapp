/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.dao.BarrioDAO;
import com.peluqueria.dao.CiudadDAO;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dto.CiudadDTO;
import com.peluqueria.dto.ClienteDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.apache.commons.validator.GenericValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 */
@Controller
@ScreenScoped
public class InsertarNuevoClienteFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    private boolean cargarCiudad;
    private boolean cargarBarrio;

    private JSONArray ciudadJSONArray;
    private JSONArray barrioJSONArray;
    private HashMap<String, JSONObject> hashMapCiudadJson;
    private HashMap<String, JSONObject> hashMapBarrioJson;

    private List<JSONObject> ciudadList;
    private List<JSONObject> barrioList;

    @Autowired
    private CiudadDAO ciudadDAO;

    @Autowired
    private BarrioDAO barrioDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private ClienteDAO cliDAO;

    private boolean exitoCrear;

    int posicionBarrio;
    int posicionCiudad;

    long idCiudad;
    long idBarrio;

    private static TextField txtRucCiCliente;
    private static Label lbl;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.z").create();

    @FXML
    private AnchorPane anchorPaneInsertarCliente;
    @FXML
    private TextField textFieldNombreCliente;
    @FXML
    private TextField textFieldRucCi;
    @FXML
    private Button buttonSalir;
    @FXML
    private Button buttonGuardar;
    @FXML
    private AnchorPane anchorPaneEditar;
    @FXML
    private TextField textFieldApellidoCliente;
    @FXML
    private Label labelRucCiCliente;
    @FXML
    private TextField textFieldTelefono;
    @FXML
    private DatePicker datePickerFecNac;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneInsertarClienteKeyReleased(KeyEvent event) {
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
        saliendo();
    }

    @FXML
    private void buttonGuardarAction(ActionEvent event) {
        registrarDatos();
    }

    @FXML
    private void anchorPaneEditarKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void cargandoInicial() {
        org.json.JSONObject json = new org.json.JSONObject(CajaDatos.getCaja());
        if (!json.isNull("clienteCI")) {
            textFieldRucCi.setText(CajaDatos.getCaja().get("clienteCI").toString());
        }
        cargarCiudad = true;
        cargarBarrio = true;
        exitoCrear = false;
        posicionBarrio = 0;
        posicionCiudad = 0;

        idCiudad = 0l;
        idBarrio = 0l;
//        cargarDatosCiudad();
//        cargarDatosBarrio(0l);
//        listenComboCiudad();
        validando();
        if (verificarCodCliente()) {
            textFieldRucCi.setText("");
        }
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void saliendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        try {
//            TimeUnit.SECONDS.sleep(1);
//        } catch (InterruptedException ex) {
//            Logger.getLogger(InsertarNuevoClienteFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//        }
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/InsertarNuevoClienteFXML.fxml", 502, 215, true);
    }

    //Apartado 4 - AVISOS
    private void mensajeError(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ok);
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            saliendo();
        }
        if (keyCode == event.getCode().ENTER) {
            if (textFieldRucCi.isFocused()) {
                textFieldNombreCliente.requestFocus();
            } else if (textFieldNombreCliente.isFocused()) {
                textFieldApellidoCliente.requestFocus();
            } else if (textFieldApellidoCliente.isFocused()) {
                textFieldTelefono.requestFocus();
            } else if (textFieldTelefono.isFocused()) {
                datePickerFecNac.requestFocus();
            }
        }

    }

    private void validando() {
//        textFieldCelular.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!newValue.contentEquals("")) {
//                String aux = newValue.substring(newValue.length() - 1, newValue.length());
//                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
//                    Platform.runLater(() -> {
//                        textFieldCelular.setText(newValue.toString());
//                        textFieldCelular.positionCaret(textFieldCelular.getLength());
//                    });
//                } else {
//                    Platform.runLater(() -> {
//                        textFieldCelular.setText(oldValue.toString());
//                        textFieldCelular.positionCaret(textFieldCelular.getLength());
//                    });
//                }
//            }
//        });
        textFieldTelefono.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        textFieldTelefono.setText(newValue.toString());
                        textFieldTelefono.positionCaret(textFieldTelefono.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldTelefono.setText(oldValue.toString());
                        textFieldTelefono.positionCaret(textFieldTelefono.getLength());
                    });
                }
            }
        });
    }

    private void keyPressComboBoxCiudad(KeyEvent event) {
    }

    private void keyPressComboBoxBarrio(KeyEvent event) {
    }

    private void listenComboCiudad() {
//        comboBoxCiudad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    JSONObject jsonCiudad = hashMapCiudadJson.get(comboBoxCiudad.getSelectionModel().getSelectedItem().toUpperCase());
//                    resetComboBoxBarrio();
//                    jsonCargandoBarrio(Long.valueOf(jsonCiudad.get("idCiudad").toString()));
//                    comboBoxBarrio.setDisable(false);
//                } catch (Exception e) {
//                    resetComboBoxBarrio();
//                    comboBoxBarrio.setDisable(true);
//                } finally {
//                }
//            }
//        });
    }

    private void resetComboBoxBarrio() {
//        ObservableList<String> list = FXCollections.observableArrayList();
//        comboBoxBarrio.valueProperty().set(null);
//        comboBoxBarrio.setItems(list);
//        comboBoxBarrio.getSelectionModel().clearSelection();
//        comboBoxBarrio.getProperties().clear();
//        comboBoxBarrio.getEditor().appendText("");
//        comboBoxBarrio.setPromptText("Filtro Sección...");
//        GridPane.setColumnIndex(comboBoxBarrio, 1);
//        GridPane.setRowIndex(comboBoxBarrio, 5);
    }

    //Apartado 6 - BACKEND
    private void guardando() {
        JSONObject cliente = new JSONObject();
        cliente = jsonCliente();
        long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cliente.put("idCliente", idActual);
        exitoCrear = persistiendoPendientes(cliente);
        if (exitoCrear) {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            cliente.put("fechaAlta", null);
            cliente.put("fechaMod", null);
            ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
            cliDTO.setFechaAlta(timestamp);
            cliDTO.setFechaMod(timestamp);
            Cliente cli = Cliente.fromClienteDTO(cliDTO);

            Pais pais = new Pais();
            pais.setIdPais(0L);

            Departamento dpto = new Departamento();
            dpto.setIdDepartamento(0l);

            Ciudad ciu = new Ciudad();
            ciu.setIdCiudad(0l);
//            ciu.setIdCiudad(cliDTO.getCiudad().getIdCiudad());

            Barrio barr = new Barrio();
//            barr.setIdBarrio(cliDTO.getBarrio().getIdBarrio());
            barr.setIdBarrio(0l);

            cli.setPais(pais);
            cli.setDepartamento(dpto);
            cli.setCiudad(ciu);
            cli.setBarrio(barr);
            try {
                cliDAO.insertar(cli);
                CajaDatos.getCaja().put("clienteCI", textFieldRucCi.getText());
                System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
            } catch (Exception e) {
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
            }
            textFieldRucCi.requestFocus();
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                CargaClienteServicioFXMLController.setUltimoCiIngresado(txtRucCiCliente, textFieldRucCi.getText(), lbl);
                saliendo();
            }
        } else {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS Y QUE EL CODIGO DEL CLIENTE NO SEA REPETIDO.", ButtonType.CLOSE);
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.CLOSE) {
                alert2.close();
            }
        }
    }

    //Apartado 7 - LOCAL
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        cliente.put("pais", pais);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private JSONArray generarCiudadLocal() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        List<Ciudad> listCiudad = ciudadDAO.listar();
        for (Ciudad ciu : listCiudad) {
            try {
                ciu.setFechaAlta(null);
                ciu.setFechaMod(null);
                CiudadDTO ciuDTO = ciu.toOnlyCiudadDTO();
                JSONObject json = (JSONObject) parser.parse(gson.toJson(ciuDTO));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }

    private JSONArray generarBarrioLocal(long idCi) {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        List<Barrio> listBarrio = barrioDAO.getByIdCiudad(idCi);
        for (Barrio barrio : listBarrio) {
            try {
                barrio.setFechaAlta(null);
                barrio.setFechaMod(null);
                JSONObject json = (JSONObject) parser.parse(gson.toJson(barrio.toBarrioAClienteDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }

    //Apartado 8 - JSON
    private void jsonCargandoCiudad() {
//        ciudadJSONArray = generarCiudadLocal();
//        hashMapCiudadJson = new HashMap();
//        for (Object obj : ciudadJSONArray) {
//            JSONObject ciudad = (JSONObject) obj;
//            comboBoxCiudad.getItems().add(ciudad.get("descripcion").toString().toUpperCase());
//            hashMapCiudadJson.put(ciudad.get("descripcion").toString().toUpperCase(), ciudad);
//        }
//        new AutoCompleteComboBoxCiudadListener<>(comboBoxCiudad);
    }

    private void jsonCargandoBarrio(long idCi) {
//        barrioJSONArray = generarBarrioLocal(idCi);
//        hashMapBarrioJson = new HashMap<>();
//        for (Object obj : barrioJSONArray) {
//            JSONObject barrio = (JSONObject) obj;
//            hashMapBarrioJson.put(barrio.get("descripcion").toString().toUpperCase(), barrio);
//            comboBoxBarrio.getItems().add(barrio.get("descripcion").toString().toUpperCase());
//        }
//        new AutoCompleteComboBoxBarrioListener<>(comboBoxBarrio);
//        GridPane.setColumnIndex(comboBoxBarrio, 1);
//        GridPane.setRowIndex(comboBoxBarrio, 5);
    }

    private JSONObject jsonCliente() {
        JSONObject obj = new JSONObject();

        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();

        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;

        obj.put("nombre", textFieldNombreCliente.getText());
        obj.put("apellido", textFieldApellidoCliente.getText());
        obj.put("callePrincipal", "");
//        obj.put("callePrincipal", textFieldDireccion.getText());
        obj.put("ruc", rucCi);

        if (NumberValidator.letterValidator(rucCi)) {
            codCliente = Utilidades.genCodClienteParana(rucCi);
        } else {
            if (rucCi.contains("-")) {
                StringTokenizer str = new StringTokenizer(rucCi, "-");
                codCliente = Integer.parseInt(str.nextElement().toString());
            } else {
                codCliente = Integer.parseInt(rucCi);
            }
        }

        obj.put("codCliente", codCliente);
//        if (comboBoxCiudad.getSelectionModel() != null) {
//            if (hashMapCiudadJson.containsKey(comboBoxCiudad.getSelectionModel().getSelectedItem())) {
//                obj.put("ciudad", hashMapCiudadJson.get(comboBoxCiudad.getSelectionModel().getSelectedItem()));
//            } else {
//                JSONObject jsonCiudad = new JSONObject();
//                jsonCiudad.put("idCiudad", 0);
//                obj.put("ciudad", jsonCiudad);
//            }
//        } else {
//            JSONObject jsonCiudad = new JSONObject();
//            jsonCiudad.put("idCiudad", 0);
//            obj.put("ciudad", jsonCiudad);
//        }
//        if (comboBoxBarrio.getSelectionModel() != null) {
//            if (hashMapBarrioJson.containsKey(comboBoxBarrio.getSelectionModel().getSelectedItem())) {
//                obj.put("barrio", hashMapBarrioJson.get(comboBoxBarrio.getSelectionModel().getSelectedItem()));
//            } else {
//                JSONObject jsonBarrio = new JSONObject();
//                jsonBarrio.put("idBarrio", 0);
//                obj.put("barrio", jsonBarrio);
//            }
//        } else {
//            JSONObject jsonBarrio = new JSONObject();
//            jsonBarrio.put("idBarrio", 0);
//            obj.put("barrio", jsonBarrio);
//        }
        LocalDate ld = datePickerFecNac.getValue();
        if (ld != null) {
            obj.put("fecNac", ld.toString());
        } else {
            obj.put("fecNac", null);
        }
//
        obj.put("telefono", textFieldTelefono.getText());
//        obj.put("telefono2", textFieldCelular.getText());
//        obj.put("email", textFieldEmail.getText());

        obj.put("usuAlta", Identity.getNomFun());
        obj.put("fechaAlta", timestampJSON);
        obj.put("usuMod", Identity.getNomFun());
        obj.put("fechaMod", timestampJSON);
        return obj;
    }

    //OTROS METODOS UTILIZADOS
    private void registrarDatos() {
        boolean registrar = false;
//        if ("btn_new_cliente_estetica")) {
        if (textFieldNombreCliente.getText().equals("") || textFieldRucCi.getText().equals("") || textFieldApellidoCliente.getText().equals("")) {
            mensajeError("El campo NOMBRE, APELLIDO y RUC/CI no debe quedar vacío.");
        } //            else if (!textFieldCelular.getText().isEmpty() || !textFieldTelefono.getText().isEmpty() || !textFieldEmail.getText().equals("")) {
        //                if (textFieldCelular.getText().startsWith("-") || textFieldCelular.getText().endsWith("-")) {
        //                    mensajeError("VERIFIQUE EL FORMATO DEL CAMPO CELULAR.");
        else if (textFieldTelefono.getText().startsWith("-") || textFieldTelefono.getText().endsWith("-")) {
            mensajeError("VERIFIQUE EL FORMATO DEL CAMPO TELÉFONO.");
            //                } else if (!EmailValidator.getInstance().isValid(textFieldEmail.getText())) {
            //                    mensajeError("EL CAMPO EMAIL NO ES VÁLIDO.");
            //                } else {
            //                    registrar = true;
            //                }}
        } else {
            registrar = true;
        }
        if (registrar) {
            if (verificarCodCliente()) {
                mensajeError("YA EXISTE UN USUARIO REGISTRADO CON ESE RUC/CI");
                textFieldRucCi.requestFocus();
            } else {
                guardando();
            }
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/InsertarNuevoClienteFXML.fxml", 502, 215, false);
//        }
    }

    private void cargarDatosBarrio(long idCi) {
        jsonCargandoBarrio(idCi);
        if (barrioJSONArray.isEmpty()) {
            mensajeError("NO SE ENCONTRARON BARRIOS DISPONIBLES.");
        }
    }

    private void cargarDatosCiudad() {
        jsonCargandoCiudad();
        if (ciudadJSONArray.isEmpty()) {
            mensajeError("NO SE ENCONTRARON CIUDADES DISPONIBLES.");
        }
    }

    private boolean verificarCodCliente() {
        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;
        if (!rucCi.equals("")) {
            if (NumberValidator.letterValidator(rucCi)) {
                return !(cliDAO.listarPorRuc(rucCi).getIdCliente() == null);
            } else {
                if (rucCi.contains("-")) {
                    StringTokenizer str = new StringTokenizer(rucCi, "-");
                    codCliente = Integer.parseInt(str.nextElement().toString());
                } else {
                    codCliente = Integer.parseInt(rucCi);
                }
                return !(cliDAO.getByCod(codCliente) == null);
            }
        } else {
            return false;
        }
//        return !(rucCi.equals("") || cliDAO.getByCod(codCliente) == null);
    }

    static void enviarComponente(TextField textFieldRucCiCliente, Label labelNombreCliente) {
        txtRucCiCliente = textFieldRucCiCliente;
        lbl = labelNombreCliente;

    }

    @FXML
    private void textFieldTelefonoKeyReleased(KeyEvent event) {

    }

    private void keyPressTelefono(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            registrarDatos();
        }
    }

    @FXML
    private void datePickerFecNacKeyReleased(KeyEvent event) {
        keyPressTelefono(event);
    }

    public class AutoCompleteComboBoxCiudadListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxCiudadListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxCiudadListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(AutoCompleteComboBoxCiudadListener.this.comboBox
                        .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public class AutoCompleteComboBoxBarrioListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxBarrioListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxBarrioListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(AutoCompleteComboBoxBarrioListener.this.comboBox
                        .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }
    }
}
