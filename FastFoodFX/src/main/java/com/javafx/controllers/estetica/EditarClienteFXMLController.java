/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.dao.BarrioDAO;
import com.peluqueria.dao.CiudadDAO;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dto.CiudadDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class EditarClienteFXMLController extends BaseScreenController implements Initializable {

    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    private JSONArray ciudadJSONArray;
    private JSONArray barrioJSONArray;
    private HashMap<String, JSONObject> hashMapCiudadJson;
    private HashMap<String, JSONObject> hashMapBarrioJson;
    String rucCiInicial;

    private List<JSONObject> ciudadList;
    private List<JSONObject> barrioList;

    @Autowired
    private CiudadDAO ciudadDAO;

    @Autowired
    private BarrioDAO barrioDAO;

    @Autowired
    private ClienteDAO cliDAO;

    long idCiudad;
    long idBarrio;
    long idCliente;
    int posicionBarrio;
    int posicionCiudad;

    private boolean exitoEditar;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.z").create();

    @FXML
    private GridPane gridPaneSeguridad;
    @FXML
    private TextField textFieldNombreCliente;
    @FXML
    private TextField textFieldDireccion;
    @FXML
    private TextField textFieldRucCi;
    @FXML
    private TextField textFieldTelefono;
    @FXML
    private TextField textFieldCelular;
    @FXML
    private TextField textFieldEmail;
    @FXML
    private Button buttonSalir;
    @FXML
    private Button buttonGuardar;
    @FXML
    private DatePicker datePickerFecNac;
    @FXML
    private AnchorPane anchorPaneEditarCliente;
    @FXML
    private ComboBox<String> comboBoxCiudad;
    @FXML
    private ComboBox<String> comboBoxBarrio;
    @FXML
    private TextField textFieldApellidoCliente;
    @FXML
    private Label labelRucCiCliente;

    @FXML
    private void comboBoxCiudadKeyReleased(KeyEvent event) {
        keyPressComboBoxCiudad(event);
    }

    @FXML
    private void comboBoxBarrioKeyReleased(KeyEvent event) {
        keyPressComboBoxBarrio(event);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
        saliendo();
    }

    @FXML
    private void buttonGuardarAction(ActionEvent event) {
        registrarDatos();
    }

    @FXML
    private void anchorPaneEditarClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void comboBoxBarrioAction(ActionEvent event) {
    }

    private void cargandoInicial() {
        idBarrio = 0l;
        idCiudad = 0l;
        posicionBarrio = 0;
        posicionCiudad = 0;
        comboBoxCiudad.setEditable(true);
        comboBoxBarrio.setEditable(true);
        exitoEditar = false;
        this.sc.getStage().setTitle("Centro Estético");
        cargarDatosCiudad();
        try {
            setFields();
        } catch (ParseException ex) {
            System.out.println("-> " + ex.getLocalizedMessage());
        }
        validando();
        rucCiInicial = textFieldRucCi.getText();
        listenComboCiudad();
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void saliendo() {
        this.sc.loadScreen("/vista/estetica/CargaClienteServicioFXML.fxml", 506, 528, "/vista/estetica/EditarClienteFXML.fxml", 511, 611, false);
    }

    //Apartado 4 - AVISOS
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj.toUpperCase(), ButtonType.OK);
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            saliendo();
        }
        if (keyCode == event.getCode().F1) {
            registrarDatos();
        }
    }

    private void validando() {
        textFieldCelular.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        textFieldCelular.setText(newValue.toString());
                        textFieldCelular.positionCaret(textFieldCelular.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldCelular.setText(oldValue.toString());
                        textFieldCelular.positionCaret(textFieldCelular.getLength());
                    });
                }
            }
        });
        textFieldTelefono.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        textFieldTelefono.setText(newValue.toString());
                        textFieldTelefono.positionCaret(textFieldTelefono.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldTelefono.setText(oldValue.toString());
                        textFieldTelefono.positionCaret(textFieldTelefono.getLength());
                    });
                }
            }
        });
    }

    private void listenComboCiudad() {
        comboBoxCiudad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                try {
                    JSONObject jsonCiudad = hashMapCiudadJson.get(comboBoxCiudad.getSelectionModel().getSelectedItem().toUpperCase());
                    resetComboBoxBarrio();
                    jsonCargandoBarrio(Long.valueOf(jsonCiudad.get("idCiudad").toString()));
                    comboBoxBarrio.setDisable(false);
                } catch (Exception e) {
                    resetComboBoxBarrio();
                    comboBoxBarrio.setDisable(true);
                } finally {
                }
            }
        });
    }

    private void keyPressComboBoxCiudad(KeyEvent event) {

    }

    private void keyPressComboBoxBarrio(KeyEvent event) {

    }

    private boolean verificarCodCliente() {
        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;
        if (!rucCi.equals("")) {
            if (NumberValidator.letterValidator(rucCi)) {
                return (cliDAO.listarPorRuc(rucCi) == null);
            } else {
                if (rucCi.contains("-")) {
                    StringTokenizer str = new StringTokenizer(rucCi, "-");
                    codCliente = Integer.parseInt(str.nextElement().toString());
                } else {
                    codCliente = Integer.parseInt(rucCi);
                }
                return !(cliDAO.getByCod(codCliente) == null);
            }
        } else {
            return false;
        }
    }

    private void resetComboBoxBarrio() {
        ObservableList<String> list = FXCollections.observableArrayList();
        comboBoxBarrio.valueProperty().set(null);
        comboBoxBarrio.setItems(list);
        comboBoxBarrio.getSelectionModel().clearSelection();
        comboBoxBarrio.getProperties().clear();
        comboBoxBarrio.getEditor().appendText("");
        comboBoxBarrio.setPromptText("Filtro Sección...");
        comboBoxBarrio.setDisable(true);
        GridPane.setColumnIndex(comboBoxBarrio, 1);
        GridPane.setRowIndex(comboBoxBarrio, 5);
    }

    //Apartado 6 - BACKEND
    private void guardando() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            String inputLine;
            JSONParser parser = new JSONParser();
            JSONObject cliente = new JSONObject();
//            try {
            cliente = jsonCliente();
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            JSONObject usuarioCajero = Identity.getUsuario();
            JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
            String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
            cliente.put("usuMod", nombreCaj);
            cliente.put("fechaMod", timestamp.getTime());
//                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                    String ipLocal = Inet4Address.getLocalHost().getHostAddress();
//                    URL url = new URL(Utilidades.ip + "/ServerParana/cliente/" + UtilLoaderBase.msjIda(ipLocal));
//                    HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                    conn.setDoOutput(true);
//                    conn.setDoInput(true);
//                    conn.setRequestProperty("Content-Type", "application/json");
//                    conn.setRequestProperty("Accept", "application/json");
//                    conn.setRequestMethod("PUT");
//                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                    wr.write(cliente.toString());
//                    wr.flush();
//                    int HttpResult = conn.getResponseCode();
//                    if (HttpResult == HttpURLConnection.HTTP_OK) {
//                        BufferedReader br = new BufferedReader(
//                                new InputStreamReader(conn.getInputStream(), "utf-8"));
//                        while ((inputLine = br.readLine()) != null) {
//                            exitoEditar = (boolean) parser.parse(inputLine);
//                        }
//                        br.close();
//                    } else {
//                        exitoEditar = actualizarPendientes(cliente);
//                    }
//                } else {
            exitoEditar = actualizarPendientes(cliente);
//                }
//            } catch (ParseException ex) {
//                exitoEditar = actualizarPendientes(cliente);
//                Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
//            }

            if (exitoEditar) {
                try {
                    if (cliDAO.actualizarClienteEstetica(cliente)) {
                        rucCiInicial = textFieldRucCi.getText();
                        jsonClienteAddRefresh(cliente);
                        System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                    }
                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                }
                ButtonType okPress = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", okPress);
                alert2.showAndWait();
                if (alert2.getResult() == okPress) {
                    alert2.close();
                    saliendo();
                }
            } else {
                ButtonType cancelPress = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", cancelPress);
                alert2.showAndWait();
                if (alert2.getResult() == cancelPress) {
                    alert2.close();
                }
            }
        } else {
            alert.close();
        }
    }

    //Apartado 7 - LOCAL
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private void jsonCargandoCiudad() {
        ciudadJSONArray = generarCiudadLocal();
        hashMapCiudadJson = new HashMap();
        for (Object obj : ciudadJSONArray) {
            JSONObject ciudad = (JSONObject) obj;
            comboBoxCiudad.getItems().add(ciudad.get("descripcion").toString().toUpperCase());
            hashMapCiudadJson.put(ciudad.get("descripcion").toString().toUpperCase(), ciudad);
        }
        new AutoCompleteComboBoxCiudadListener<>(comboBoxCiudad);
    }

    private void jsonCargandoBarrio(long idCi) {
        barrioJSONArray = generarBarrioLocal(idCi);
        hashMapBarrioJson = new HashMap<>();
        for (Object obj : barrioJSONArray) {
            JSONObject barrio = (JSONObject) obj;
            hashMapBarrioJson.put(barrio.get("descripcion").toString().toUpperCase(), barrio);
            comboBoxBarrio.getItems().add(barrio.get("descripcion").toString().toUpperCase());
        }
        new AutoCompleteComboBoxBarrioListener<>(comboBoxBarrio);
        GridPane.setColumnIndex(comboBoxBarrio, 1);
        GridPane.setRowIndex(comboBoxBarrio, 5);
    }

    private JSONArray generarCiudadLocal() {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        List<Ciudad> listCiudad = ciudadDAO.listar();
        for (Ciudad ciu : listCiudad) {
            try {
                ciu.setFechaAlta(null);
                ciu.setFechaMod(null);
                CiudadDTO ciuDTO = ciu.toOnlyCiudadDTO();
                JSONObject json = (JSONObject) parser.parse(gson.toJson(ciuDTO));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }

    private JSONArray generarBarrioLocal(long idCi) {
        JSONArray array = new JSONArray();
        JSONParser parser = new JSONParser();
        List<Barrio> listBarrio = barrioDAO.getByIdCiudad(idCi);
        for (Barrio barrio : listBarrio) {
            try {
                barrio.setFechaAlta(null);
                barrio.setFechaMod(null);
                JSONObject json = (JSONObject) parser.parse(gson.toJson(barrio.toBarrioAClienteDTO()));
                array.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return array;
    }

    private void cargarDatosCiudad() {
        jsonCargandoCiudad();
        if (ciudadJSONArray.isEmpty()) {
            mensajeError("NO SE ENCONTRARON CIUDADES DISPONIBLES.");
        }
    }

    private void cargarDatosBarrio(long idCi) {
        jsonCargandoBarrio(idCi);
        if (barrioJSONArray.isEmpty()) {
            mensajeError("NO SE ENCONTRARON BARRIOS DISPONIBLES.");
        }
    }

    //Apartado 8 - JSON
    private JSONObject jsonCliente() {
        JSONObject obj = new JSONObject();
        String rucCi = textFieldRucCi.getText();
        int codCliente = 0;
        long idCli = Long.parseLong(CargaClienteServicioFXMLController.jsonClienteAdd.get("idCliente").toString());

        Cliente cli = cliDAO.getById(idCli);

        obj.put("idCliente", idCli);
        obj.put("nombre", textFieldNombreCliente.getText());
        obj.put("apellido", textFieldApellidoCliente.getText());
        obj.put("callePrincipal", textFieldDireccion.getText());
        obj.put("ruc", rucCi);

        if (NumberValidator.letterValidator(rucCi)) {
            codCliente = Utilidades.genCodClienteParana(rucCi);
        } else {
            if (rucCi.contains("-")) {
                StringTokenizer str = new StringTokenizer(rucCi, "-");
                codCliente = Integer.parseInt(str.nextElement().toString());
            } else {
                codCliente = Integer.parseInt(rucCi);
            }
        }

        obj.put("usuAlta", cli.getUsuAlta());
        if (cli.getFechaAlta() != null) {
            obj.put("fechaAlta", cli.getFechaAlta().getTime());
        } else {
            obj.put("fechaAlta", null);
        }

        obj.put("segundaLateral", cli.getSegundaLateral());
        obj.put("primeraLateral", cli.getPrimeraLateral());
        obj.put("nroLocal", cli.getNroLocal());

        JSONObject jsonPais = new JSONObject();
        jsonPais.put("idPais", cli.getPais().getIdPais());
        obj.put("pais", jsonPais);

        JSONObject jsonDpto = new JSONObject();
        jsonDpto.put("idDepartamento", cli.getDepartamento().getIdDepartamento());
        obj.put("departamento", jsonDpto);

        if (cli.getCompraUltFecha() != null) {
            obj.put("compraUltFecha", cli.getCompraUltFecha().getTime());
        } else {
            obj.put("compraUltFecha", null);
        }

        if (cli.getCompraIniFecha() != null) {
            obj.put("compraIniFecha", cli.getCompraIniFecha().getTime());
        } else {
            obj.put("compraIniFecha", null);
        }

        obj.put("codCliente", codCliente);
        if (comboBoxCiudad.getSelectionModel() != null) {
            if (hashMapCiudadJson.containsKey(comboBoxCiudad.getSelectionModel().getSelectedItem())) {
                obj.put("ciudad", hashMapCiudadJson.get(comboBoxCiudad.getSelectionModel().getSelectedItem()));
            } else {
                JSONObject jsonCiudad = new JSONObject();
                jsonCiudad.put("idCiudad", 0);
                obj.put("ciudad", jsonCiudad);
            }
        } else {
            JSONObject jsonCiudad = new JSONObject();
            jsonCiudad.put("idCiudad", 0);
            obj.put("ciudad", jsonCiudad);
        }
        if (comboBoxBarrio.getSelectionModel() != null) {
            if (hashMapBarrioJson.containsKey(comboBoxBarrio.getSelectionModel().getSelectedItem())) {
                obj.put("barrio", hashMapBarrioJson.get(comboBoxBarrio.getSelectionModel().getSelectedItem()));
            } else {
                JSONObject jsonBarrio = new JSONObject();
                jsonBarrio.put("idBarrio", 0);
                obj.put("barrio", jsonBarrio);
            }
        } else {
            JSONObject jsonBarrio = new JSONObject();
            jsonBarrio.put("idBarrio", 0);
            obj.put("barrio", jsonBarrio);
        }
        LocalDate ld = datePickerFecNac.getValue();
        if (ld != null) {
            obj.put("fecNac", ld.toString());
        } else {
            obj.put("fecNac", null);
        }
        obj.put("telefono", textFieldTelefono.getText());
        obj.put("telefono2", textFieldCelular.getText());
        obj.put("email", textFieldEmail.getText());
        return obj;
    }

    //OTROS METODOS UTILIZADOS
    private void registrarDatos() {
        boolean registrar = false;
//        if ("btn_edit_cliente_estetica")) {
        if (textFieldNombreCliente.getText().equals("") || textFieldRucCi.getText().equals("")) {
            mensajeError("El campo NOMBRE y RUC/CI no debe quedar vacío.");
        } else if (!textFieldCelular.getText().isEmpty() || !textFieldTelefono.getText().isEmpty() || !textFieldEmail.getText().equals("")) {
            if (textFieldCelular.getText().startsWith("-") || textFieldCelular.getText().endsWith("-")) {
                mensajeError("VERIFIQUE EL FORMATO DEL CAMPO CELULAR.");
            } else if (textFieldTelefono.getText().startsWith("-") || textFieldTelefono.getText().endsWith("-")) {
                mensajeError("VERIFIQUE EL FORMATO DEL CAMPO TELÉFONO.");
            } else if (!EmailValidator.getInstance().isValid(textFieldEmail.getText())) {
                mensajeError("EL CAMPO EMAIL NO ES VÁLIDO.");
            } else {
                registrar = true;
            }
        } else {
            registrar = true;
        }
        if (registrar) {
            if (textFieldRucCi.getText().contentEquals(rucCiInicial)) {//sin modificación ruc / ci
                guardando();
            } else {
                if (verificarCodCliente()) {
                    mensajeError("YA EXISTE UN USUARIO REGISTRADO CON ESE RUC/CI");
                    textFieldRucCi.requestFocus();
                } else {
                    guardando();
                }
            }
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/EditarClienteFXML.fxml", 511, 611, false);
//        }
    }

    private void setFields() throws ParseException {
        JSONParser parser = new JSONParser();
        org.json.JSONObject json = new org.json.JSONObject(CargaClienteServicioFXMLController.jsonClienteAdd.toString());
        textFieldNombreCliente.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("nombre").toString());
        if (!json.isNull("apellido")) {
            textFieldApellidoCliente.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("apellido").toString());
        }
        if (!json.isNull("callePrincipal")) {
            textFieldDireccion.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("callePrincipal").toString());
        }
        if (!json.isNull("ruc")) {
            textFieldRucCi.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("ruc").toString());
        }
        if (!json.isNull("telefono")) {
            textFieldTelefono.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("telefono").toString());
        }
        if (!json.isNull("telefono2")) {
            textFieldCelular.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("telefono2").toString());
        }
        if (!json.isNull("email")) {
            textFieldEmail.setText(CargaClienteServicioFXMLController.jsonClienteAdd.get("email").toString());
        }
        if (!json.isNull("fecNac")) {
            String fechaNacString = CargaClienteServicioFXMLController.jsonClienteAdd.get("fecNac").toString();
            StringTokenizer st = new StringTokenizer(fechaNacString, "-");
            int anho = Integer.parseInt(st.nextElement().toString());
            int mes = Integer.parseInt(st.nextElement().toString());
            int dia = Integer.parseInt(st.nextElement().toString());
            datePickerFecNac.setValue(LocalDate.of(anho, mes, dia));
        }
        if (!json.isNull("ciudad")) {
            JSONObject jsonObjCiudad = (JSONObject) parser.parse(CargaClienteServicioFXMLController.jsonClienteAdd.get("ciudad").toString());
            idCiudad = Long.parseLong(jsonObjCiudad.get("idCiudad").toString());
            comboBoxCiudad.getSelectionModel().select(jsonObjCiudad.get("descripcion").toString().toUpperCase());
        }
        if (!json.isNull("barrio")) {
            JSONObject jsonObjBarrio = (JSONObject) parser.parse(CargaClienteServicioFXMLController.jsonClienteAdd.get("barrio").toString());
            idBarrio = Long.parseLong(jsonObjBarrio.get("idBarrio").toString());
            cargarDatosBarrio(idCiudad);
            comboBoxBarrio.getSelectionModel().select(jsonObjBarrio.get("descripcion").toString().toUpperCase());
        }
    }

    private static void jsonClienteAddRefresh(JSONObject cliente) {
        CargaClienteServicioFXMLController.jsonClienteAdd = new JSONObject();
        CargaClienteServicioFXMLController.jsonClienteAdd = cliente;
    }

    public class AutoCompleteComboBoxCiudadListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxCiudadListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxCiudadListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(AutoCompleteComboBoxCiudadListener.this.comboBox
                        .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public class AutoCompleteComboBoxBarrioListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxBarrioListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxBarrioListener.this);
        }

        @Override
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(AutoCompleteComboBoxBarrioListener.this.comboBox
                        .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }
}
