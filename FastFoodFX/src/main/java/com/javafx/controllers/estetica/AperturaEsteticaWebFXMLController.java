/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.AperturaCaja;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.dao.AperturaCajaDAO;
import com.peluqueria.dao.ArqueoCajaDAO;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class AperturaEsteticaWebFXMLController extends BaseScreenController implements Initializable {

    private boolean alert;
    private Date date;
    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();
    @Autowired
    ManejoLocalDAO manejoDAO;
    @Autowired
    AperturaCajaDAO aperturaCajaDAO;
    @Autowired
    ArqueoCajaDAO arqueoCajaDAO;
    ManejoLocal manejoLocal = new ManejoLocal();

    @FXML
    private AnchorPane anchorPaneAperturaEstetica;
    @FXML
    private Pane panelMontoApertura;
    @FXML
    private TextField txtMontoApertura;
    @FXML
    private Button btnAceptar;
    @FXML
    private Button btnCancelar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnAceptarAction(ActionEvent event) {
        ingresandoFacturaVenta();
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAperturaEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void cargandoInicial() {

        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField();
        alert = false;
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void ingresandoFacturaVenta() {
//        if ("factura_venta_estetica")) {
        if (txtMontoApertura.getText().contentEquals("")) {
            mensajeAlerta("EL CAMPO MONTO NO DEBE ESTAR VACÍO.");
        } else if (procesandoApertura()) {
            mensajeAlerta("APERTURA DE CAJA WEB PROCESADA!");
            volviendo();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/AperturaEsteticaFXML.fxml", 468, 253, true);
//        }
    }

    private void volviendo() {
        this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/estetica/AperturaEsteticaWebFXML.fxml", 468, 253, true);
    }

    //Apartado 4 - AVISOS
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                ingresandoFacturaVenta();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenTextField() {
        txtMontoApertura.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMontoApertura.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMontoApertura.setText(param);
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMontoApertura.setText("");
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMontoApertura.setText(param);
                                    txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMontoApertura.setText("");
                                txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMontoApertura.setText(oldValue);
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMontoApertura.setText(param);
                        txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                    });
                }
            }
        });
    }

//    Apartado 6 - BACKEND
    private boolean procesandoApertura() {
        boolean apertura = false;
        if (aperturaCajaDAO.verificarAperturaDia()) {
            if (arqueoCajaDAO.verificarArqueoDia()) {
                long cantApertura = aperturaCajaDAO.getCantidadAperturaDia();
                long cantArqueo = arqueoCajaDAO.getCantidadArqueoDia();
                apertura = cantApertura == cantArqueo;
            } else {
                apertura = false;
            }
        } else {
            apertura = true;
        }
        if (apertura) {
            AperturaCaja ac = new AperturaCaja();
            Caja caja = new Caja();
            caja.setIdCaja(1L);
            ac.setCaja(caja);

            Usuario usuCajero = new Usuario();
            usuCajero.setIdUsuario(2L);
            ac.setUsuarioCajero(usuCajero);

            ac.setUsuarioSupervisor(null);
            ac.setMontoApertura(BigDecimal.valueOf(Long.parseLong(numValidator.numberValidator(txtMontoApertura.getText()))));
            ac.setFechaApertura(new Timestamp(System.currentTimeMillis()));
            ac.setMontoCaja(0);
            ac.setDiferenciaApertura(0);
            return aperturaCajaDAO.insertarRecuperarEstado(ac);
        } else {
            mensajeAlerta("ERROR: ES PROBABLE QUE YA HAYA HECHO UNA APERTURA DE CAJA");
            return false;
        }
    }
    //////CREATE, APERTURA CAJA -> POST

    //Apartado 7 - LOCAL
    private JSONObject registrarAperturaLocal(JSONObject aperturaCaja) {
        JSONObject obj = null;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + aperturaCaja + "','aperturaCaja', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL APERTURA CAJA ********");
                obj = aperturaCaja;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return obj;
    }

    //Apartado 8 - JSON
    private JSONObject creandoJsonAperturaLocal() {
        date = new Date();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        //**--VERIFICANDO--**        
        JSONObject caja = CajaDeDatos.onlyCaja();
        JSONObject user = Identity.onlyUser();
        JSONObject aperturaCaja = new JSONObject();
        aperturaCaja.put("caja", caja);
        //disable, políticas de la empresa...
        aperturaCaja.put("usuarioCajero", user);
        aperturaCaja.put("montoApertura", Integer.valueOf(numValidator.numberValidator(txtMontoApertura.getText())));
        aperturaCaja.put("fechaApertura", timestampJSON);
        aperturaCaja.put("montoCaja", 0);//default, por si se llegue a implementar en un futuro
        aperturaCaja.put("diferenciaApertura", 0);//default, por si se llegue a implementar en un futuro
        return aperturaCaja;
    }

    //OTROS METODOS UTILIZADOS
    private void registrandoCaidaElectrica() {
        manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        manejoLocal.setUsuario(DatosEnCaja.getUsers().toString());
        manejoLocal.setFactura(null);
        long idManejo = manejoDAO.recuperarId();
        if (idManejo == 0) {
            JSONObject cajas = DatosEnCaja.getDatos();
            cajas.remove("vueltoSi");
            cajas.remove("vueltoNo");
            cajas.remove("dona");
            cajas.remove("caida");
            cajas.remove("sitio");
            cajas.remove("insercionFacturaVentaCab");
            cajas.remove("insercionFacturaVentaCabLocal");
            cajas.remove("actualizacionLocal");
            cajas.remove("idRangoFacturaActual");
            manejoLocal.setCaja(cajas.toString());
            if (manejoDAO.insertarObtenerEstado(manejoLocal)) {
                System.out.println("VARIABLES MANEJANDO DE MANERA LOCAL");
            } else {
                System.out.println("ERROR: no se pudieron registrar los datos");
            }
        } else {
            actualizarDatos();
        }
    }

    private void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        manejoLocal.setUsuario(DatosEnCaja.getUsers().toString());
        manejoLocal.setFactura(DatosEnCaja.getFacturados().toString());
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }
}
