/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.estetica;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.FacturaCabClientePendiente;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.core.domain.TipoMoneda;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ClientePendienteDAO;
import com.peluqueria.dao.FacturaCabClientePendienteDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ClientePendienteDTO;
import com.peluqueria.dto.FacturaCabClientePendienteDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.ServPendienteDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.caja.LoginCajeroFXMLController;
import com.javafx.controllers.util.FXMLCalculadoraController;
import com.javafx.controllers.util.ModificarPassFXMLController;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.AnimationFX;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.apache.commons.validator.GenericValidator;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 */
@Controller
public class FacturaVentaEsteticaFXMLController extends BaseScreenController implements Initializable {

    public static void setTextFieldCompAnt(String aTextFieldCompAnt) {
        textFieldCompAnt = aTextFieldCompAnt;
    }

    public static boolean isActualizarDatosCabecera() {
        return actualizarDatosCabecera;
    }

    public static void setActualizarDatosCabecera(boolean actualizarDatosCabecera) {
        FacturaVentaEsteticaFXMLController.actualizarDatosCabecera = actualizarDatosCabecera;
    }

    public static List<JSONObject> getCotizacionList() {
        return cotizacionList;
    }

    public static int getPeso() {
        return peso;
    }

    public static int getReal() {
        return real;
    }

    public static int getDolar() {
        return dolar;
    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static Long getPrecioTotal() {
        return precioTotal;
    }

    public static JSONObject getCabFactura() {
        return cabFactura;
    }

    public static void setCabFactura(JSONObject aCabFactura) {
        cabFactura = aCabFactura;
    }

    public static boolean isCancelacionProd() {
        return cancelacionProd;
    }

    public static void resetParam() {
        precioTotal = 0l;
    }

    @Autowired
    public static FacturaCabClientePendienteDAO factCabCliPendDAO;
    @Autowired
    private static FacturaClienteCabDAO factDAO;
    @Autowired
    private ClientePendienteDAO cpDAO;

    /*
    //Apartado 1 y 2 - FXML y VARIABLES INICIALES
    static JSONObject datos = new JSONObject();
    private static boolean dato = false;
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;

    public static void resetParam() {
        precioTotal = 0l;
    }

    

    @Autowired
    private ArticuloDAO artDAO;

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    public static FacturaCabClientePendienteDAO factCabCliPendDAO = new FacturaCabClientePendienteDAOImpl();
    private static FacturaClienteCabDAO factDAO = new FacturaClienteCabDAOImpl();
    private static ClienteDAO clienteDAO = new ClienteDAOImpl();
    static JSONParser parser = new JSONParser();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss.z").create();
    public static boolean actualizarDatosCabecera;

    private static int peso;
    private static int real;
    private static int dolar;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;

    JSONObject tipoCaja;
    private int orden;
    private static String codBarra;
    private static List<JSONObject> detalleArtList;
    private ObservableList<JSONObject> articuloDetData;
    private boolean primeraInsercion;
    private static Long precioTotal;
    private boolean alert;
    private static boolean facturaVentaEstado = false;
    private static JSONObject cabFactura;
    private static JSONObject facturaCabeceraSupr = new JSONObject();
    private static String textFieldCompAnt;

    private ReentrantLock lock = new ReentrantLock();

    private static NumberValidator numValidator;
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;*/
    private static JSONObject cabFactura;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static boolean dato = false;
    private static boolean facturaVentaEstado = false;
    private static JSONObject facturaCabeceraSupr = new JSONObject();
    public static boolean actualizarDatosCabecera;
    Alert alertCerrarTurno = null;
    Alert alertArqueo = null;

    public JSONObject objArticulo;

    @Autowired
    private ArticuloDAO artDAO;

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    static JSONObject datos = new JSONObject();
    ManejoLocal manejoLocal = new ManejoLocal();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static String textFieldCompAnt;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static Long precioTotal;
    private static int peso;
    private static int real;
    private static int dolar;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    //primera inserción
    private boolean primeraInsercion;
    private int orden;
    //primera inserción
    //lector de código
    private static String codBarra;
    //lector de código
    private static NumberValidator numValidator;
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;
    JSONObject tipoCaja;
    private static List<JSONObject> detalleArtList;
    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    //TABLE VIEW
    private ReentrantLock lock = new ReentrantLock();
    Image image;
    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;
    private static int idFact;
    public static boolean valorIngreso = false;

    @FXML
    private AnchorPane anchorPaneFacturaVentaEstetica;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private Label txtEmpresa;
    @FXML
    private Label txtDireccion;
    @FXML
    private Label txtCiudad;
    @FXML
    private Label txtComercio;
    @FXML
    private Label txtTelefono;
    @FXML
    private Label txtNumRuc;
    @FXML
    private ImageView imgProducto;
    @FXML
    private HBox hBoxDetalle;
    @FXML
    private TextField textFieldCant;
    @FXML
    private TextField textFieldCod;
    @FXML
    private TextField textFieldDescripcion;
    @FXML
    private Label labelCantidad;
    @FXML
    private TableView<JSONObject> tableViewFactura;
    @FXML
    private TableColumn<JSONObject, String> columnOrden;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnCant;
    @FXML
    private TableColumn<JSONObject, String> columnMed;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnPrecio;
    @FXML
    private TableColumn<JSONObject, String> columnImp;
    @FXML
    private TableColumn<JSONObject, String> columnIva;
    @FXML
    private TableColumn<JSONObject, String> columnExenta;
    @FXML
    private TableColumn<JSONObject, String> columnGravada;
    @FXML
    private TableColumn<JSONObject, String> columnSeccion;
    @FXML
    private Button btnCargarCEServicio;
    @FXML
    private Button btnCerrar;
    @FXML
    private Button btnCalculadora;
    @FXML
    private Button btnCancelarTodos;
    @FXML
    private Button btnCambiarClave;
    @FXML
    private Label labelTotalGs;
    @FXML
    private Label labelTotal;
    @FXML
    private Label txtNumCaja;
    @FXML
    private Label labelDatosCaja;
    @FXML
    private Label labelCambioDia;
    @FXML
    private Label txtDolar;
    @FXML
    private Label txtPeso;
    @FXML
    private Label txtReal;
    @FXML
    private Label labelNroFactAnt;
    @FXML
    private TextField txtNumComprobanteAnt;
    @FXML
    private Label labelNroFact;
    @FXML
    private TextField txtNumComprobante;
    @FXML
    private Label labelCajeroTitulo;
    @FXML
    private VBox vBoxCajero;
    @FXML
    private Label labelCajeroFunc;
    @FXML
    private Label labelCajeroFunc2;
    @FXML
    private Button btnCerrarTurno;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            cargandoInicial();
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
    }

    private void cargandoInicial() throws ParseException {
        if (ScreensContoller.getFxml().contentEquals("/vista/estetica/MensajeFinalEsteticaFXML.fxml")) {
            Toaster toaster = new Toaster();
            toaster.mensajeDiario();
            toaster.mensajeDeNavidadAnhoNuevo();
        }
        imageViewLogo = (ImageView) AnimationFX.rotationNodePlay(imageViewLogo, 1.3, false);
        objArticulo = new JSONObject();
        facturaVentaEstado = false;
        setActualizarDatosCabecera(false);
//        cargandoImagen();
        facturaCabeceraSupr = new JSONObject();
        numValidator = new NumberValidator();
        boolean estado = false;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = new JSONObject();

            if (DatosEnCaja.getFacturados() == null) {
                fact = new JSONObject();
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                fact = new JSONObject();
            } else {
                fact = DatosEnCaja.getFacturados();
                estado = true;
            }

            //SETEAR CAMPOS PRIMERAMENTE
            //**--VERIFICANDO--**
            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");
            txtNumCaja.setText(caj.get("descripcion").toString());
            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }
                labelCajeroFunc.setText(nomFuncionario);
                labelCajeroFunc2.setText(apeFuncionario);
            } else {
                labelCajeroFunc.setText("N/A");
            }
            iniciandoCotizacion();
            //FIN DEL SETEO DE CAMPOS
            cargandoDatosIniciales();
            if (estado) {
                cargandoDetalleManeraLocal();
            } else {
                //Carga el detalle de los servicios
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idClientePendiente")) {
                    cargarDetalleServicios();
                    actualizandoCabFacturaLocalmente();
                    JSONArray jsonDetalle = new JSONArray();
                    try {
                        jsonDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (jsonDatos.isNull("rendicion")) {
            datos.put("rendicion", false);
        }
        try {
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            txtNumRuc.setText(empresa.get("ruc").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
//        if (hashJsonArticulo.size() != 0) {
//            if (!jsonDatos.isNull("exentaGlobal")) {
//                chkExtranjero.setSelected(true);
//            }
//            chkExtranjero.setDisable(true);
//        } else {
//            chkExtranjero.setDisable(false);
//        }
//        verificandoCaidaFormaPago();
    }

    @FXML
    private void textFieldCodKeyReleased(KeyEvent event) {
        keyPressTextCod(event);
    }

    @FXML
    private void btnCargarCEServicioAction(ActionEvent event) {
        cargarServicio();
    }

    @FXML
    private void btnCerrarAction(ActionEvent event) {
    }

    @FXML
    private void btnCalculadoraAction(ActionEvent event) {
    }

    @FXML
    private void btnCancelarTodosAction(ActionEvent event) {
    }

    @FXML
    private void btnCerrarTurnoAction(ActionEvent event) {
    }

    @FXML
    private void btnCambiarClaveAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneFacturaVentaEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //Apartado 3 - NAVEGACION DE FORMULARIOS
    private void cancelarFactura() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA CANCELAR LA FACTURA?", ok, cancel);
        alert1.showAndWait();
        if (alert1.getResult() == ok) {
            textFieldDescripcion.setText("");
            this.alert = true;
            CancelacionFacturaEsteticaFXMLController.setFactAnt(txtNumComprobante.getText());
            CancelacionFacturaEsteticaFXMLController.setTotalFacturado(labelTotalGs.getText());
            this.sc.loadScreen("/vista/estetica/CancelacionFacturaEsteticaFXML.fxml", 600, 400, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, true);
        } else if (alert1.getResult() == cancel) {
            textFieldCod.requestFocus();
            alert1.close();
        }
    }

    private void pagando() {
//        if ("factura_cerrar")) {
        org.json.JSONObject json = new org.json.JSONObject(fact);
        if (!json.isNull("facturaClienteCab")) {
            try {
                JSONObject factu = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                setCabFactura(factu);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        this.sc.loadScreen("/vista/estetica/FormaPagoEsteticaFXML.fxml", 1282, 829, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1258, 781, true);
//        }
    }

    private void cargarServicio() {
        if (detalleArtList.isEmpty()) {
//            if ("adjuntar_estetica")) {
            this.sc.loadScreen("/vista/estetica/AdjuntarEsteticaFXML.fxml", 575, 287, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
//            }
        } else {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
        }
    }

    private void cambiandoPass() {
        ModificarPassFXMLController.setModulo("estetica");
        this.sc.loadScreen("/vista/util/ModificarPassFXML.fxml", 377, 152, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
    }

    private void navegandoCalculador() {
        FXMLCalculadoraController.setModulo("estetica");
        this.sc.loadScreen("/vista/util/FXMLCalculadora.fxml", 419, 452, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
    }

    //Apartado 4 - AVISOS
    private void mensajeError(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (!verificandoCaidaFormaPago()) {
                cargarServicio();
            }
        }
        if (keyCode == event.getCode().F2) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            boolean valor = false;
            if (!jsonDatos.isNull("ultimaFactura")) {
                String nFac = txtNumComprobante.getText().replace("-", "");
                if (jsonDatos.getString("ultimaFactura").equals(String.valueOf(Long.parseLong(nFac)))) {
                    valor = true;
                }
            }
            if (valor) {
                mensajeDetalle("Se ha detectado una incidencia, contáctese con el área de IT.", "Eror 400");
            } else {
                if (!verificandoCaidaFormaPago()) {
                    if (!detalleArtList.isEmpty()) {
                        //NUEVO
                        org.json.JSONObject json = new org.json.JSONObject(datos);
                        boolean formaPago = false;
                        if (!json.isNull("caida")) {
                            String caida = datos.get("caida").toString();
                            if (caida.equalsIgnoreCase("factura_venta_estetica")) {
                                formaPago = false;
                            } else {
                                formaPago = true;
                            }
                        }
                        if (formaPago) {
                            try {
                                JSONArray detalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                                JSONObject cabecera = new JSONObject();
                                if (detalle.size() > 0) {
                                    JSONObject objDetalle = (JSONObject) parser.parse(detalle.get(0).toString());
                                    cabecera = (JSONObject) parser.parse(objDetalle.get("facturaClienteCab").toString());
                                } else {
                                    cabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                                }
                                cabFactura = cabecera;
                                this.sc.loadScreen("/vista/estetica/MensajeFinalEsteticaFXML.fxml", 562, 288, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 743, true);
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        } else {
                            if (DatosEnCaja.getDatos() != null) {
                                datos = DatosEnCaja.getDatos();
                            }
                            if (DatosEnCaja.getFacturados() != null) {
                                fact = DatosEnCaja.getFacturados();
                            }
                            if (!facturaCabeceraSupr.toString().equalsIgnoreCase("{}")) {
                                cabFactura = facturaCabeceraSupr;
                                fact.put("facturaClienteCab", facturaCabeceraSupr);
                                setActualizarDatosCabecera(true);
                                FacturaVentaEsteticaFXMLController.cancelacionProd = true;
                            }
                            //verificar que ingrese en forma de pago en el metodo PUT ya que cuando de cancela una factura va al POST de vuelta
                            //entonces impide que se actualice la CABECERA
                            pagando();
                            actualizandoCabFacturaLocalmente();
                        }
                        //FIN NUEVO
                    } else {
                        mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
                    }
                }
            }
        }

        if (keyCode == event.getCode().F3) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    String cajero = verificandoDatosCierre().get("cajero").toString();
                    if (cajero.equalsIgnoreCase("arqueo")) {
                        DatosEnCaja.setFacturados(null);
                        DatosEnCaja.setFacturados(null);
                        users = null;
                        fact = null;
                        DatosEnCaja.setUsers(null);
                        datos.put("modSup", true);
                        DatosEnCaja.setDatos(datos);
                        actualizarDatos();
                    } else if (cajero.equalsIgnoreCase("cambio")) {
                        DatosEnCaja.setFacturados(null);
                        DatosEnCaja.setFacturados(null);
                        users = null;
                        fact = null;
                        DatosEnCaja.setUsers(null);
                        datos.put("modSup", true);
                        DatosEnCaja.setDatos(datos);
                        actualizarDatosNuevo();
                    }
                }
            }
        }

        if (keyCode == event.getCode().F4) {
            if (alert) {
                alert = false;
            } else if (detalleArtList.isEmpty()) {
                mensajeAlerta("NO DISPONE DE ARTÍCULO ALGUNO PARA CANCELAR FACTURA.");
            } else {
                textFieldCant.requestFocus();
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                cancelarFactura();
                actualizarDatos();
            }
        }

        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    navegandoCalculador();
                }
            }
        }

        if (keyCode == event.getCode().F6) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    cambiandoPass();
                }
            }
            actualizarDatos();
        }

        if (keyCode == event.getCode().DELETE) {
            if (!verificandoCaidaFormaPago()) {
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta_estetica")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    mensajeError("NO SE PUEDE ELIMINAR LOS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                } else if (alert) {
                    alert = false;
                } else if (detalleArtList.isEmpty()) {
                    mensajeAlerta("DEBE DISPONER COMO MÍNIMO UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                } else {
                    JSONObject productos = tableViewFactura.getSelectionModel().getSelectedItem();
                    if (productos == null) {
                        mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                    } else {
                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA ELIMINAR EL ARTÍCULO " + productos.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
                        alert1.showAndWait();
                        if (alert1.getResult() == ok) {
                            try {
                                facturaCabeceraSupr = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                                FacturaVentaEsteticaFXMLController.setCabFactura(facturaCabeceraSupr);
                                textFieldDescripcion.setText("");
                                this.alert = true;
                                CancelacionProductoEsteticaFXMLController.obtenerTable(tableViewFactura, labelTotalGs, productos, labelCantidad, imgProducto);
                                this.sc.loadScreen("/vista/estetica/CancelacionProductoEsteticaFXML.fxml", 600, 400, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 743, true);
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        } else if (alert1.getResult() == cancel) {
                            alert1.close();
                        }
                        actualizarDatos();
                    }
                }
            }
        }
    }//LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void keyPressTextCod(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            if (alertArqueo != null) {
                if (alertArqueo.isShowing()) {
                    System.out.println("FUI A UN ARQUEO");
                }
            }
            if (alertCerrarTurno != null) {
                if (alertCerrarTurno.isShowing()) {
                    System.out.println("FUI A UN CIERRE DE TURNO");
                }
            }
            datos.remove("exentaGlobal");
            //NUEVO
            if (DatosEnCaja.getDatos() != null) {
                datos = DatosEnCaja.getDatos();
            }
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            }
            org.json.JSONObject json = new org.json.JSONObject(datos);
            boolean formaPago = false;
            if (!json.isNull("caida")) {
                String caida = datos.get("caida").toString();
                if (caida.equalsIgnoreCase("factura_venta_estetica")) {
                    formaPago = false;
                } else {
                    formaPago = true;
                }
            }
            if (formaPago) {
                if (!this.alert) {
                    mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                }
            } else {
                JSONObject jsonCabecera = new JSONObject();
                if (!json.isNull("sitio") && !facturaVentaEstado) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
//              FIN NUEVO
                if (!this.alert) {
                    if (!verificandoCaidaFormaPago()) {
                        if (!textFieldCod.getText().contentEquals("")) {
                            if (textFieldCant.getText().contentEquals("")) {
                                textFieldCant.setText("1");
                            }
                            int cant = 0;
                            cant = Integer.valueOf(numValidator.numberValidator(textFieldCant.getText()));
                            int cantidad = cant;
                            codBarra = textFieldCod.getText();
                            int nArt = 0;
                            boolean estado = false;
                            while (cant != 0) {
//                                cargandoDetallePorCodigoBarra();
//                                cant--;
                                if (cargandoDetalle()) {
                                    estado = true;
                                }
                                cant--;
                            }
                            if (estado) {
                                if (!json.isNull("nArticulos")) {
                                    nArt = Integer.parseInt(datos.get("nArticulos").toString());
                                }
                                datos.put("nArticulos", nArt + cantidad);
                                actualizarDatos();
                            }
                            codBarra = "";
                        }
                        textFieldCant.setText("1");
                        //NUEVO
                        JSONArray jsonDetalle = new JSONArray();
                        if (!json.isNull("sitio") && !facturaVentaEstado) {
                            try {
                                jsonDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                            fact.put("facturaClienteCab", jsonCabecera);
                            FacturaVentaEsteticaFXMLController.setCabFactura(jsonCabecera);
                            fact.put("facturaDetalle", jsonDetalle);
                            facturaVentaEstado = true;
                            DatosEnCaja.setFacturados(fact);
                        }
//                        System.out.println("LA FACTURA ES: " + fact.toString());
                        //FIN DEL NUEVO
                        datos.put("sitio", 1);
                        actualizandoCabFacturaLocalmente();
                    }
                } else {
                    this.alert = false;
                }
                if (!detalleArtList.isEmpty()) {
                    Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
                }
            }
        } else if (event.getCode().isDigitKey()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
        } else if (event.getCode() == KeyCode.INSERT) {
            if (textFieldCod.getText().length() < 13) {
                int cero = 13 - textFieldCod.getText().length();
                String ceroS = "";
                for (int i = 0; i < cero; i++) {
                    ceroS = ceroS + "0";
                }
                String inicioS = textFieldCod.getText().substring(0, textFieldCod.getCaretPosition());
                String finalS = textFieldCod.getText().substring(textFieldCod.getCaretPosition(), textFieldCod.getText().length());
                textFieldCod.setText(inicioS + ceroS + finalS);
                codBarra = textFieldCod.getText();
                textFieldCod.positionCaret(textFieldCod.getLength());
            }
        }
    }

    private void listenFactura() {
        //para evitar el uso del tab y control. La idea es mantener la actividad en el tableView
        tableViewFactura.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                }
            }
        });
        btnCerrar.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCargarCEServicio.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCerrarTurno.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCancelarTodos.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCalculadora.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCambiarClave.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        textFieldCant.setText("");
        textFieldCant.textProperty().addListener((observable, oldValue, newValue) -> {
            lock.lock();
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isLong(newValue)) {
                        Platform.runLater(() -> {
                            textFieldCant.setText(newValue.toString());
                            textFieldCant.positionCaret(textFieldCant.getLength());
                            if (textFieldCant.getText().length() > 3) {
                                textFieldCant.setText("1");
                                mensajeAlerta("EL MÁXIMO ES DE 999 ARTÍCULOS");
                            }
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldCant.setText(oldValue.toString());
                            textFieldCant.positionCaret(textFieldCant.getLength());
                            if (textFieldCant.getText().length() > 3) {
                                textFieldCant.setText("1");
                                mensajeAlerta("EL MÁXIMO ES DE 999 ARTÍCULOS");
                            }
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldCant.setText(oldValue.toString());
                        textFieldCant.positionCaret(textFieldCant.getLength());
                        if (textFieldCant.getText().length() > 3) {
                            textFieldCant.setText("1");
                            mensajeAlerta("EL MÁXIMO ES DE 999 ARTÍCULOS");
                        }
                    });
                }
            }
            lock.unlock();
        });
        textFieldCant.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.ENTER) {
                    textFieldCod.requestFocus();
                }
            }
        });
        textFieldDescripcion.setText("");
        textFieldCod.setText("");
        textFieldCant.setText("1");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textFieldCod.requestFocus();
            }
        });
    }

    //Apartado 6 - BACKEND
    private JSONObject jsonArtDet(String cod) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject articulo = null;
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 20)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/articulo/" + cod);
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    articulo = (JSONObject) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
        articulo = generarListaArticuloLocal(cod);
//            }
//        } catch (IOException | ParseException ex) {
//            articulo = generarListaArticuloLocal(cod);
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        return articulo;
    }

    public JSONObject generarListaArticuloLocal(String cod) {
        JSONParser parser = new JSONParser();
        try {
            Articulo art = artDAO.buscarCod(cod);
            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    private void jsonTipoMoneda() {
        String inputLine;
        JSONParser parser = new JSONParser();
        tipoMonedaJSONArray = null;
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/tipoMoneda");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    tipoMonedaJSONArray = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
        tipoMonedaJSONArray = recuperarTipoMonedaLocal();
//            }
//        } catch (IOException | ParseException ex) {
//            tipoMonedaJSONArray = recuperarTipoMonedaLocal();
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        cotizacionList = new ArrayList<>();
        for (Object obj : tipoMonedaJSONArray) {
            JSONObject tipoMoneda = (JSONObject) obj;
            getCotizacionList().add(tipoMoneda);
        }
    }

    public static Long creandoCabFactura(boolean cancelFact) throws ParseException, IOException {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean estadoCancelProd = false;
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        long idFact = 0l;
        if (cancelFact) {
            JSONObject estadoFactura = new JSONObject();
            estadoFactura.put("idEstadoFactura", 2L);//anulado
            cabFactura.put("estadoFactura", estadoFactura);
        }
        try {
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));
            // NUEVO 
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                if (cancelFact) {
                    JSONObject estadoFactura = new JSONObject();
                    estadoFactura.put("idEstadoFactura", 2L);//anulado
                    cabe.put("estadoFactura", estadoFactura);
                    fact.put("facturaClienteCab", cabe);
                }
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    cabFactura = objFact;
                    if (!jsonDatos.isNull("caida")) {
                        String caida = datos.get("caida").toString();
                        if (caida.equalsIgnoreCase("forma_pago_estetica")) {
                            cancelacionProdPrimera = false;
                        } else if (!jsonDatos.isNull("cancelProducto")) {
                            cancelacionProdPrimera = false;
                            estadoCancelProd = true;
                        } else {
                            cancelacionProdPrimera = true;
                        }
                    }
                }
            }
            // NUEVO
            if (cancelacionProdPrimera && idCab == 0L) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
                //Linea para prueba por el rango actual
                long n = 0l;
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idRangoFacturaActual")) {
                    n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
                }
                cabFactura.put("idFacturaClienteCab", n);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = cabFactura.get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                cabFactura.put("nroFactura", nroFact);
                datos.put("nroFact", nroFact);
            }
            idFact = generarFacturaCabLocal();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //para registrar facturaIni
        org.json.JSONObject jsonEstadoInicio = new org.json.JSONObject(datos);
        boolean estadoFactInicial = false;
        if (!jsonEstadoInicio.isNull("estadoFacturaInicial")) {
            estadoFactInicial = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        if (!estadoFactInicial) {
            datos.put("facturaInicial", cabFactura.get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        if (cancelFact) {
            estado = true;
        }

        datos.put("FacturaFinal", cabFactura.get("nroFactura").toString());
        if (idFact != 0l && cancelFact && estado) {//que sea del tipo cancelación factura...
            JSONArray jsonArrayFactDet = creandoJsonFactDet(cabFactura);
            String localidad = "";
            try {
                localidad = jsonDatos.getString("caida");
            } catch (Exception e) {
            }
            if (!localidad.equalsIgnoreCase("forma_pago_estetica")) {
                creandoFactDet(jsonArrayFactDet);
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (json.isNull("cancelProducto")) {
            crearCabeceraLocalmente();
            datos.put("fcc", true);
        } else {
            actualizarCabeceraLocalmente();
        }

        actualizarDatos();
        return idFact;
    }

    private static boolean creandoFactDet(JSONArray jsonArray) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
            exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }

    private static void insertarFacturaCabClientePendiente() throws ParseException, IOException {
        JSONParser parser = new JSONParser();
        String inputLine;
        JSONObject JSONFactCabCliPen = null;
        JSONObject factCabCliPen = new JSONObject();

        JSONObject factCab = new JSONObject();
        factCab.put("idFacturaClienteCab", FacturaVentaDatos.getIdFacturaClienteCab());

        JSONObject cliPend = new JSONObject();
        cliPend.put("idClientePendiente", Long.parseLong(datos.get("idClientePendiente").toString()));

        factCabCliPen.put("facturaClienteCab", factCab);
        factCabCliPen.put("clientePendiente", cliPend);

        JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
        JSONObject jsonFCCP = (JSONObject) parser.parse(JSONFactCabCliPen.toString());
        if (jsonFCCP.get("clientePendiente") == null) {
            JSONFactCabCliPen = persistiendoFactCabCliPendLocal(factCabCliPen.toString());
        }

        if (JSONFactCabCliPen != null) {
            ObjectMapper mapper = new ObjectMapper();
            String jsonObj = factCabCliPen.toString();
            FacturaCabClientePendienteDTO fccpDTO = mapper.readValue(jsonObj, FacturaCabClientePendienteDTO.class);
            FacturaCabClientePendiente fccp = factCabCliPendDAO.insertarObtenerObj(FacturaCabClientePendiente.fromFacturaCabClientePendienteAsociado(fccpDTO));
            if (fccp != null) {
                System.out.println("LOS DATOS HAN SIDO ENVIADOS AL SERVIDOR Y DE MANERA LOCAL");
            }
        }
    }

    //Apartado 7 - LOCAL
    private void cargandoDetallePorCodigoBarra() {
        JSONObject jsonArticulo = null;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                byte[] bytes = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    resetTray();
                } else {
                    if (hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo")) != null) {
                        int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
                        int cantidad = Integer.parseInt(detalleArtList.get(index).get("cantidad").toString());
                        cantidad++;
                        long precioTotalDetalle = (long) detalleArtList.get(index).get("precio") * cantidad;
                        if ((long) detalleArtList.get(index).get("poriva") == 0) {
                            detalleArtList.get(index).put("exenta", precioTotalDetalle);
                        } else {
                            detalleArtList.get(index).put("gravada", precioTotalDetalle);
                        }
                        detalleArtList.get(index).put("cantidad", cantidad);
                        cargandoCamposInterface(detalleArtList.get(index));
                    } else {
                        orden++;
                        detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                        detalleArtList.add(detalleArticulo);
                        hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                        hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                        cargandoCamposInterface(detalleArticulo);
                    }
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                    image = jsonArtDetImg(codBarra);
                }
                if (image == null) {
                    File file = new File(PATH.PATH_NO_IMG);
                    image = new Image(file.toURI().toString());
                    this.imgProducto.setImage(image);
                }
                this.imgProducto.setImage(image);
                centerImage();
            } catch (Exception e) {
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {
            textFieldCant.setText("1");
            textFieldCod.setText("");
            textFieldDescripcion.setText("NO SE CARGÓ EL ARTÍCULO");
            codBarra = "";
            mensajeAlerta("NO SE CARGÓ EL ARTÍCULO");
        }
    }

    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB.
    private static long generarFacturaCabLocal() {
        String sql = "";
        ConexionPostgres.conectar();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (cancelacionProdPrimera) {
            datos.put("insercionFacturaVentaCabLocal", true);
            //UUID ACTUAL PARA MODIFICAR FACTURA
            long uuid = VentasUtiles.recuperarId() + 1;
            datos.put("uuidCassandraActual", String.valueOf(uuid));
            sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'insertar');";
            System.out.println("-->> " + sql);
        } else {
            long idFactCab = 0L;
            if (!json.isNull("idFactClienteCabServidor")) {
                idFactCab = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
            }
            if (idFactCab != 0 && !dato) {
                //UUID ACTUAL PARA MODIFICAR FACTURA
                long uuid = VentasUtiles.recuperarId() + 1;
                cabFactura.put("idFacturaClienteCab", idFactCab);
                sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'actualizar');";
                System.out.println("-->> " + sql);
                datos.put("uuidCassandraActual", String.valueOf(uuid));
                //para que solo una ves entre aqui luego ya actualice nada mas...
                dato = true;
                datos.put("actualizacionLocal", true);
            } else {
                String operacion = "insertar";
                if (idFactCab != 0) {
                    cabFactura.put("idFacturaClienteCab", idFactCab);
                    operacion = "actualizar";
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + datos.get("uuidCassandraActual").toString() + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
                System.out.println("-->> " + sql);
                datos.put("actualizacionLocal", true);
            }
        }
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS FACTURA VENTA CAB ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        cabFactura.put("idFacturaClienteCab", datos.get("idRangoFacturaActual"));

        return Long.parseLong(datos.get("idRangoFacturaActual").toString());
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB.

    //////INSERT, PENDIENTES - FACTURA CLIENTE DET.
    private static boolean registrandoFacturaDetLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE DET.

    private static JSONObject persistiendoFactCabCliPendLocal(String jsonFactCabCliPend) {
        ConexionPostgres.conectar();
        JSONParser parser = new JSONParser();
        JSONObject jsonRetorno = null;
        String sql = "INSERT INTO estetica.fact_cab_cli_pend (descripcion_dato, operacion, fecha) VALUES ('" + jsonFactCabCliPend + "', 'I', now())";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                jsonRetorno = (JSONObject) parser.parse(jsonFactCabCliPend);
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            jsonRetorno = null;
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                jsonRetorno = null;
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            jsonRetorno = null;
        }
        ConexionPostgres.cerrar();
        return jsonRetorno;
    }

    private void cargandoDetalleManeraLocal() {
        try {
            resetMapeo();
            detalleArtList = new ArrayList<>();
            JSONParser parser = new JSONParser();
            JSONArray facturaDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            JSONObject facturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            cabFactura = new JSONObject();
            org.json.JSONObject jsonFact = new org.json.JSONObject(facturaCabecera);

            try {
                int num = tableViewFactura.getItems().size();

                if (num == 0) {
                    if (!jsonFact.isNull("montoFactura")) {
                        facturaCabecera.remove("montoFactura");
                        fact.put("facturaClienteCab", facturaCabecera);
                    } else {
                        cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                    }
                } else {
                    cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                }
                valorIngreso = true;
            } catch (Exception e) {
                mensajeError2("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO");
                valorIngreso = false;
            }

            if (valorIngreso) {
                cabFactura.put("estadoFactura", facturaCabecera.get("estadoFactura").toString());
                cabFactura.put("nroActual", facturaCabecera.get("nroActual").toString());
                cabFactura.put("idFacturaClienteCab", facturaCabecera.get("idFacturaClienteCab").toString());
                cabFactura.put("nroFactura", facturaCabecera.get("nroFactura").toString());
                primeraInsercion = false;
                for (int i = 0; i < facturaDetalle.size(); i++) {
                    JSONObject detalleArticulo = (JSONObject) parser.parse(facturaDetalle.get(i).toString());
                    JSONObject articulo = (JSONObject) parser.parse(detalleArticulo.get("articulo").toString());
                    hashJsonArticulo.put((Long.parseLong(articulo.get("codArticulo").toString())), articulo);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long.parseLong(articulo.get("idArticulo").toString())), detalleArtList.lastIndexOf(detalleArticulo));
                    vistaJSONObjectArtDet();
                    cargandoCamposInterfaceLocal(detalleArticulo);
                }
                CajaDeDatos.generandoNroComprobante();
                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                // primer trío
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                long nroActual = talos.getNroActual();
                JSONObject jsonFacturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
                if (jsonDatos.isNull("caida")) {
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                } else {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta_estetica")) {
                        txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    } else {
                        txtNumComprobante.setText(Utilidades.patternFactura(jsonFacturaCabecera.get("nroFactura").toString()));
                    }
                }
                File file = new File(PATH.PATH_NO_IMG);
                Image image = new Image(file.toURI().toString());
                this.imgProducto.setImage(image);
                centerImage();
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (!detalleArtList.isEmpty()) {
            Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
        }
    }

    private void mensajeError2(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            LoginCajeroFXMLController.salirDatos = true;
            alert2.close();
//            this.sc.loadScreen("/vista/caja/facturaVentaFXML.fxml", 1248, 743, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
        }
    }

    //Apartado 8 - JSON
    public static JSONObject creandoJsonFactCab() {
        try {
            JSONObject jsonCabFactura = new JSONObject();
            JSONObject estadoFactura = new JSONObject();
            JSONParser parser = new JSONParser();
            estadoFactura.put("idEstadoFactura", 1L);//normal
            JSONObject tipoMoneda = new JSONObject();
            tipoMoneda.put("idTipoMoneda", 1L);//guaraníes
            JSONObject tipoComprobante = new JSONObject();
            //acaité para el tema de mayorista...
            tipoComprobante.put("idTipoComprobante", 1L);//factura contado
            //**********************************************************************
            jsonCabFactura.put("cancelado", true);
            jsonCabFactura.put("caja", datos.get("caja"));
            jsonCabFactura.put("cliente", ClienteEsteticaFXMLController.getJsonCliente());//en nulo default NN
            jsonCabFactura.put("sucursal", datos.get("sucursal"));//en nulo default NN
            jsonCabFactura.put("nroFactura", null);
            jsonCabFactura.put("estadoFactura", estadoFactura);
            jsonCabFactura.put("tipoComprobante", tipoComprobante);
            jsonCabFactura.put("tipoMoneda", tipoMoneda);
            //**********************************************************************
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            jsonCabFactura.put("fechaEmision", timestampEmision);
            jsonCabFactura.put("fechaMod", timestampEmision);
            jsonCabFactura.put("usuAlta", Identity.getNomFun());
            jsonCabFactura.put("usuMod", Identity.getNomFun());
            //**********************************************************************
            JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            jsonCabFactura.put("nroActual", talona.get("nroActual") + " - " + String.valueOf(talona.get("idTalonariosSucursales")));
            //Consulta de nroActual de talonarios en la BD local....
            return jsonCabFactura;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    //JSON FACTURA DETALLE
    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        JSONObject detalleArticulo = new JSONObject();
        //**********************************************************************
        detalleArticulo.put("articulo", jsonArticulo);
        detalleArticulo.put("descripcion", jsonArticulo.get("descripcion"));
        detalleArticulo.put("cantidad", 1);
        detalleArticulo.put("orden", orden);
        JSONObject iva = (JSONObject) jsonArticulo.get("iva");
        detalleArticulo.put("poriva", iva.get("poriva"));
        JSONObject seccion = (JSONObject) jsonArticulo.get("seccion");
        if (seccion != null) {
            detalleArticulo.put("seccion", seccion.get("descripcion"));
        } else {
            detalleArticulo.put("seccion", "N/A");
        }
        JSONObject subSeccion = (JSONObject) jsonArticulo.get("seccionSub");
        if (subSeccion != null) {
            detalleArticulo.put("seccionSub", subSeccion.get("descripcion"));
        } else {
            detalleArticulo.put("seccionSub", "N/A");
        }
        detalleArticulo.put("permiteDesc", jsonArticulo.get("permiteDesc"));
        detalleArticulo.put("bajada", jsonArticulo.get("bajada"));
        //**********************************************************************
        //mapeo, no se persiste en el backend excepto "precio", solo para desplegar total por detalle en frontend...
        if ((long) tipoCaja.get("idTipoCaja") == 1 || (long) tipoCaja.get("idTipoCaja") == 3) {//minorista...
            detalleArticulo.put("precio", jsonArticulo.get("precioMin"));
            if ((long) iva.get("poriva") == 0) {

                long porIva = 0;
                if ((long) iva.get("poriva") != 0) {
                    porIva = (long) iva.get("poriva");
                    if (porIva == 5) {
                        double precioMinDouble = Long.parseLong(jsonArticulo.get("precioMin").toString()) / 1.05;
                        long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioMin);

                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioMin);
                    } else if (porIva == 10) {
                        double precioMinDouble = Long.parseLong(jsonArticulo.get("precioMin").toString()) / 1.1;
                        long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioMin);

                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioMin);
                    }
                } else {
                    detalleArticulo.put("exenta", jsonArticulo.get("precioMin"));
                }

                detalleArticulo.put("poriva", 0l);
            } else {
                detalleArticulo.put("gravada", jsonArticulo.get("precioMin"));
            }
            //por si acaso se agrege otro tipo de caja, de vuelta la condición...
        } else if ((long) tipoCaja.get("idTipoCaja") == 2) {//mayorista...
            detalleArticulo.put("precio", jsonArticulo.get("precioMay"));
            if ((long) iva.get("poriva") == 0) {

                long porIva = 0;
                if ((long) iva.get("poriva") != 0) {
                    porIva = (long) iva.get("poriva");
                    if (porIva == 5) {
                        long precio = Long.parseLong(jsonArticulo.get("precioMay").toString());
                        double precioMinDouble = precio / 1.05;
                        long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioDato);

                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioDato);
                    } else if (porIva == 10) {
                        long precio = Long.parseLong(jsonArticulo.get("precioMay").toString());
                        double precioMinDouble = precio / 1.1;
                        long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioDato);

                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioDato);
                    }
                } else {
                    detalleArticulo.put("exenta", jsonArticulo.get("precioMay"));
                }

                detalleArticulo.put("poriva", 0l);
            } else {
                detalleArticulo.put("gravada", jsonArticulo.get("precioMay"));
            }
        }
        //mapeo, no se persiste en el backend, solo para desplegar total por detalle en frontend...        
        //**********************************************************************
        return detalleArticulo;
    }
    //JSON FACTURA DETALLE

    //Apartado 10 - TABLE VIEW
    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
    private void vistaJSONObjectArtDet() {
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(jsonArticulo.get("codArticulo").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCant.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCant.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnMed.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnMed.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                JSONObject jsonUnidad = (JSONObject) jsonArticulo.get("unidad");
                return new SimpleStringProperty(jsonUnidad.get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Sección......................................................
        //columna Sección..............................................
        columnPrecio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPrecio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject datas = data.getValue();
                String valor = "";
//                if (chkExtranjero.isSelected()) {
//                    try {
//                        JSONObject jsonArt = (JSONObject) parser.parse(datas.get("articulo").toString());
//                        JSONObject jsonIva = (JSONObject) parser.parse(jsonArt.get("iva").toString());
//                        long porIva = Long.parseLong(jsonIva.get("poriva").toString());
//                        if (porIva == 5) {
//                            double precioMinDouble = Long.parseLong(datas.get("precio").toString()) / 1.05;
////                            long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
//
////                            double precioParseado = Double.parseDouble(precioMin);
//                            numValidator = new NumberValidator();
//                            valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
////                            return new SimpleStringProperty(valor);
//                        } else if (porIva == 10) {
//                            double precioMinDouble = Long.parseLong(datas.get("precio").toString()) / 1.1;
////                            long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
//                            numValidator = new NumberValidator();
//                            valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
////                            return new SimpleStringProperty(valor);
//                        } else {
                double precioMinDouble = Long.parseLong(datas.get("precio").toString());
                numValidator = new NumberValidator();
                valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
//                            return new SimpleStringProperty(valor);
//                        }
//                    } catch (ParseException ex) {
//                        System.out.println("-->> " + ex.getLocalizedMessage());
//                    }
//                } else {
//                    String precio = datas.get("precio").toString();
//                    double precioParseado = Double.parseDouble(precio);
//                    numValidator = new NumberValidator();
//                    valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioParseado));
//
//                }
                return new SimpleStringProperty(valor);
            }
        });
        //columna Sección......................................................
        //columna IMP..............................................
        columnImp.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnImp.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if ((long) data.getValue().get("poriva") == 0) {
                    return new SimpleStringProperty("EXE");
                } else {
                    return new SimpleStringProperty("GRAV");
                }
            }
        });
        //columna IMP......................................................
        //columna IVA..............................................
        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (!chkExtranjero.isSelected()) {
//                    return new SimpleStringProperty(data.getValue().get("poriva").toString() + " %");
//                } else {
                return new SimpleStringProperty("0 %");
//                }
            }
        });
        //columna IVA......................................................
        //columna Exenta..............................................
        columnExenta.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("exenta") == null) {
                    return new SimpleStringProperty("-");
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("exenta").toString())));
                }
            }
        });
        //columna Exenta......................................................
        //columna Gravada..............................................
        columnGravada.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("gravada") == null) {
                    return new SimpleStringProperty("-");
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("gravada").toString())));
                }
            }
        });
        //columna Gravada......................................................
        //columna Sección..............................................
        columnSeccion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("seccion").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje......................................................
        tableViewFactura.setItems(articuloDetData);
        //**********************************************************************
    }

    //TABLE-VIEW TABLE-VIEW TABLE-VIEW ************** -> -> -> -> -> -> -> -> ->
    private void cargandoDatosIniciales() throws ParseException {
        if (fact == null || fact.toString().equalsIgnoreCase("{}")) {
//            try {
            if (textFieldCompAnt != null) {
                if (textFieldCompAnt.contains("-")) {
                    txtNumComprobanteAnt.setText(textFieldCompAnt);
                } else {
                    txtNumComprobanteAnt.setText(Utilidades.patternFactura(textFieldCompAnt));
                }
            } else {
                txtNumComprobanteAnt.setText("");
            }
            this.alert = false;
            orden = 1;
            labelTotalGs.setText("Gs 0");
            hashJsonArtDet = new HashMap<>();
            hashJsonArticulo = new HashMap<>();
            primeraInsercion = true;
            detalleArtList = new ArrayList<>();
            codBarra = "";
            listenFactura();
        } else {
            System.out.println("ESTO DA: " + labelTotalGs.getText());
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("compAnt")) {
            txtNumComprobanteAnt.setText(Utilidades.patternFactura(datos.get("compAnt").toString()));
        }
        iniciandoFactCab();
    }

    private void cargarDetalleServicios() throws ParseException {
        JSONObject detalleArticulo = null;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int cantArt = 0;
        if (!json.isNull("nArticulos")) {
            cantArt = json.getInt("nArticulos");
        }
        ClientePendienteDTO cpDTO = cpDAO.getById(Long.parseLong(datos.get("idClientePendiente").toString())).toEliminarPendienteDTO();
        List<ServPendienteDTO> listSpDTO = cpDTO.getServPendiente();
//        List<ServPendienteDTO> newServPendiente = new ArrayList<>();
        for (ServPendienteDTO spDTO : listSpDTO) {
            long idArt = spDTO.getArticulo().getIdArticulo();
            ArticuloDTO articuDTO = artDAO.getById(idArt).toArticuloDTO();
            cantArt += 1;
            datos.put("nArticulos", cantArt);

            articuDTO.setFechaAlta(null);
            articuDTO.setFechaMod(null);
            //se convierte en articuloJSON para poder crear el detalle
            JSONObject jsonArticulo = (JSONObject) parser.parse(gson.toJson(articuDTO));
            spDTO.setArticulo(articuDTO);
            cargandoDetalle(jsonArticulo);

            vistaJSONObjectArtDet();
        }
    }

    private void cargandoDetalle(JSONObject jsonArticulo) {
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                byte[] bytes = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    //ONLY FOR C.E.
                    detalleArticulo.put("servicio", true);
                    //FINAL FOR C.E.
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    resetTray();
                } else {
                    if (hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo")) != null) {
                        int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
                        int cantidad = Integer.parseInt(detalleArtList.get(index).get("cantidad").toString());
                        cantidad++;
                        long precioTotalDetalle = (long) detalleArtList.get(index).get("precio") * cantidad;
                        if ((long) detalleArtList.get(index).get("poriva") == 0) {
                            detalleArtList.get(index).put("exenta", precioTotalDetalle);
                        } else {
                            detalleArtList.get(index).put("gravada", precioTotalDetalle);
                        }
                        detalleArtList.get(index).put("cantidad", cantidad);
                        cargandoCamposInterface(detalleArtList.get(index));
                    } else {
                        orden++;
                        detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                        //ONLY FOR C.E.
                        detalleArticulo.put("servicio", true);
                        //FINAL FOR C.E.
                        detalleArtList.add(detalleArticulo);
                        hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                        hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                        cargandoCamposInterface(detalleArticulo);
                    }
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
            } catch (Exception e) {
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {
            textFieldCant.setText("1");
            textFieldCod.setText("");
            textFieldDescripcion.setText("NO SE CARGÓ EL ARTÍCULO");
            codBarra = "";
            mensajeAlerta("NO SE CARGÓ EL ARTÍCULO");
        }
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalle() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                byte[] bytes = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    detalleArticulo.put("primeraInsercion", true);
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    resetTray();
                } else {
                    org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
                    if (hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo")) != null) {
                        int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
                        int cantidad = Integer.parseInt(detalleArtList.get(index).get("cantidad").toString());
                        cantidad++;
                        long precioTotalDetalle = (long) detalleArtList.get(index).get("precio") * cantidad;
                        if ((long) detalleArtList.get(index).get("poriva") == 0) {
                            long porIva = 0;
                            if (jsonDatos.isNull("exentaGlobal")) {
                                precioTotalDetalle = (long) detalleArtList.get(index).get("exenta") * cantidad;
                                datos.put("exentaGlobal", detalleArtList.get(index).get("exenta"));
                                actualizarDatos();
                            } else {
                                precioTotalDetalle = (long) datos.get("exentaGlobal") * cantidad;
                            }
                            if ((long) detalleArtList.get(index).get("poriva") != 0) {
                                porIva = (long) detalleArtList.get(index).get("poriva");
                                if (porIva == 5) {
                                    double precioMinDouble = precioTotalDetalle / 1.05;
                                    long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                                    detalleArtList.get(index).put("exenta", precioDato);
                                } else if (porIva == 10) {
                                    double precioMinDouble = precioTotalDetalle / 1.1;
                                    long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                                    detalleArtList.get(index).put("exenta", precioDato);
                                }
                            } else {
                                detalleArtList.get(index).put("exenta", precioTotalDetalle);
                            }
                        } else {
                            detalleArtList.get(index).put("gravada", precioTotalDetalle);
                        }
                        detalleArtList.get(index).put("cantidad", cantidad);
                        cargandoCamposInterface(detalleArtList.get(index));
                    } else {
                        orden++;
                        detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                        detalleArtList.add(detalleArticulo);
                        hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                        hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                        cargandoCamposInterface(detalleArticulo);
                    }
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                    image = jsonArtDetImg(codBarra);
                }
                if (image == null) {
                    File file = new File(PATH.PATH_NO_IMG);
                    image = new Image(file.toURI().toString());
                    this.imgProducto.setImage(image);
                }
                this.imgProducto.setImage(image);
                centerImage();
                this.imgProducto = (ImageView) AnimationFX.fadeNode(this.imgProducto);
            } catch (NumberFormatException | ParseException e) {
                estado = false;
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {
            if (!textFieldCod.getText().equalsIgnoreCase("")) {
                textFieldCant.setText("1");
                textFieldCod.setText("");
                textFieldDescripcion.setText("NO SE CARGÓ EL ARTÍCULO");
                codBarra = "";
                mensajeAlerta("NO SE CARGÓ EL ARTÍCULO");
            }
            estado = false;
        }
        return estado;
    }

    private void cargandoCamposInterface(JSONObject detalleArticulo) {
        org.json.JSONObject jsonDetalle = new org.json.JSONObject(detalleArticulo);
        labelCantidad.setText(detalleArticulo.get("cantidad").toString());
        textFieldCant.setText("1");
        textFieldCod.setText("");
        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
        long lblTotalgs = Long.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        precioTotal = (long) detalleArticulo.get("precio") + lblTotalgs;
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(precioTotal))));
        seteandoMontoFact();
    }

    private void resetTray() {
        Toaster.quitandoMsj();
    }

    private void centerImage() {
        Image img = imgProducto.getImage();
        if (img != null) {
            double w = 0;
            double h = 0;
            double ratioX = imgProducto.getFitWidth() / img.getWidth();
            double ratioY = imgProducto.getFitHeight() / img.getHeight();
            double reducCoeff = 0;
            if (ratioX >= ratioY) {
                reducCoeff = ratioY;
            } else {
                reducCoeff = ratioX;
            }
            w = img.getWidth() * reducCoeff;
            h = img.getHeight() * reducCoeff;
            imgProducto.setX((imgProducto.getFitWidth() - w) / 2);
            imgProducto.setY((imgProducto.getFitHeight() - h) / 2);
        }
    }

    static void seteandoMontoFact() {
        int monto = Integer.valueOf(String.valueOf(precioTotal));
        cabFactura.put("montoFactura", monto);
    }

    public static void actualizandoCabFacturaLocalmente() {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
        if (json.isNull("caida")) {
            FacturaVentaEsteticaFXMLController.cancelacionProd = false;
            datos.put("caida", "factura_venta_estetica");
        } else {
            String caida = json.get("caida").toString();
            if (caida.equalsIgnoreCase("factura_venta_estetica")) {
                if (!isActualizarDatosCabecera()) {
                    FacturaVentaEsteticaFXMLController.cancelacionProd = false;
                } else {
                    FacturaVentaEsteticaFXMLController.cancelacionProd = true;
                }
                datos.put("caida", "factura_venta_estetica");
            } else {
                FacturaVentaEsteticaFXMLController.cancelacionProd = true;
                datos.put("caida", "forma_pago_estetica");
            }
        }
        if (!facturaVentaEstado) {
            if (!FacturaVentaEsteticaFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                FacturaVentaEsteticaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                JSONObject jsonCabecera = new JSONObject();
                if (!jsonFact.isNull("facturaClienteCab")) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        FacturaVentaEsteticaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
                        datos.put("nroFact", FacturaVentaEsteticaFXMLController.getCabFactura().get("nroFact"));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    try {
                        editandoJsonFactCab();
                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaVentaEsteticaFXMLController.getCabFactura().get("nroActual").toString());
                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
                        datos.put("nroFact", nroActualmente);
                        FacturaVentaEsteticaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        fact.put("facturaClienteCab", FacturaVentaEsteticaFXMLController.getCabFactura().toString());
        if (FacturaVentaEsteticaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaVentaEsteticaFXMLController.getCabFactura());
            JSONArray arrayDetalle = new JSONArray();
            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
                    art.put("fechaAlta", null);
                    art.put("fechaMod", null);
                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                    iva.put("fechaAlta", null);
                    iva.put("fechaMod", null);
                    art.put("iva", iva);
                    jsonArt.put("articulo", art);
                    arrayDetalle.add(jsonArt);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            DatosEnCaja.setFacturados(fact);
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
            fact.put("facturaDetalle", arrayDetalle);
        }
        actualizarDatosBD();
        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
    }

    private static void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            FacturaVentaEsteticaFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            FacturaVentaEsteticaFXMLController.getCabFactura().put("cliente", ClienteEsteticaFXMLController.getJsonCliente());//en nulo default NN
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private static JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < detalleArtList.size(); i++) {
            detalleArtList.get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(detalleArtList.get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            if (DatosEnCaja.getFacturados() == null) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else {
                DatosEnCaja.setFacturados(fact);
            }
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
            jsonFact = jsonFact.replace("\"[", "[");
            jsonFact = jsonFact.replace("]\"", "]");
            manejo.setFactura(jsonFact);
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
        } catch (Exception ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        } finally {
        }
    }

    static void iniciandoFactCab() {
        cabFactura = new JSONObject();
        cabFactura = creandoJsonFactCab();
        cancelacionProd = false;
        cancelacionProdPrimera = true;
    }

    static void resetMapeo() {
        hashJsonArticulo = new HashMap<>();
        hashJsonArtDet = new HashMap<>();
    }

    private void cargandoCamposInterfaceLocal(JSONObject detalleArticulo) {
        String cantidad = detalleArticulo.get("cantidad").toString();
        labelCantidad.setText(cantidad);
        textFieldCant.setText("1");
        textFieldCod.setText("");
        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
        numValidator = new NumberValidator();
        long lblTotalgs = Long.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        long totalMultiplicado = Long.parseLong(cantidad) * Long.parseLong(detalleArticulo.get("precio").toString());
        precioTotal = totalMultiplicado + lblTotalgs;
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(precioTotal))));
    }

    private void iniciandoCotizacion() {
        real = 0;
        peso = 0;
        dolar = 0;
        jsonTipoMoneda();
        if (getCotizacionList() != null) {
            for (JSONObject jsonCotizacion : getCotizacionList()) {
                switch ((Integer.valueOf(jsonCotizacion.get("idTipoMoneda").toString()))) {
                    case 2://peso argentino
                        peso = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtPeso.setText(numValidator.numberFormat("$ ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                    case 3://real brasilero
                        real = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtReal.setText(numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                    case 4://dólar américano
                        dolar = Double.valueOf(String.valueOf(jsonCotizacion.get("venta"))).intValue();
                        txtDolar.setText(numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(String.valueOf(jsonCotizacion.get("venta")))));
                        break;
                }
            }
        }
    }

    //////READ -> TIPO MONEDA
    private JSONArray recuperarTipoMonedaLocal() {
        JSONParser parser = new JSONParser();
        JSONArray array = new JSONArray();
        for (TipoMoneda tp : tipoMonedaDAO.listar()) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(tp.toTipoMonedaDTO()));
                array.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        };
        return array;
    }
    //////READ -> TIPO MONEDA

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private static void updateData() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //PARA SUPRIMIR PRODUCTO / FACTURA, SE PERSISTE Y SE OBTIENEN LOS ID'S PERTINENTES...
    static void persistiendoFact(boolean cancelProd, long idArt) {
        try {
            JSONParser parser = new JSONParser();
            if (cancelProd) {
                if (cancelacionProdPrimera) {
                    try {
                        FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(false));
                    } catch (IOException ex) {
                        Utilidades.log.error("EXCEPTION", ex.fillInStackTrace());
                    }
                    cancelacionProdPrimera = false;
                    cancelacionProd = true;//permite cambiar a PUT en FormaPagoFXMLController, al finalizar venta...

                    ///****** NEW FOR C.E. *****///
                    org.json.JSONObject json = new org.json.JSONObject(datos);
                    if (!json.isNull("idClientePendiente")) {
                        if (json.isNull("cancelProducto")) {
                            try {
                                //NEW PARA INSERTAR LOS DATOS EN LA TABLA factura_cliente.factura_cab_cliente_pendiente
                                insertarFacturaCabClientePendiente();
                                datos.put("fcccp", true);
                                DatosEnCaja.setDatos(datos);
                            } catch (ParseException ex) {
                                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                            } catch (IOException ex) {
                                Utilidades.log.error("IOException", ex.fillInStackTrace());
                            }
                            ///****** FINISH FOR C.E. *****///
                        }
                    }
                }
                FacturaVentaDatos.setIdProducto(idArt);
            } else {
                try {
                    FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(true));
                } catch (IOException ex) {
                    Utilidades.log.error("EXCEPTION", ex.fillInStackTrace());
                }
                cancelacionProd = false;
            }
            JSONObject aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
            JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
            FacturaVentaDatos.setIdCajero(Long.valueOf(usuarioCajero.get("idUsuario").toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    static void suprimirProducto(TableView<JSONObject> tabla, Label labelTotalGs, int declarado,
            Label labelCantidad, ImageView imgProducto) {
        JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
        JSONObject articulos = (JSONObject) detalle.get("articulo");
        long codArticulo = (long) articulos.get("codArticulo");
        JSONObject jsonArticulo = hashJsonArticulo.get(codArticulo);
        int cantTotal = Integer.parseInt(detalle.get("cantidad").toString());
        int dif = cantTotal - declarado;
        int total = Integer.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        int neto = 0;
        int resultado = 0;
        String dato = "";
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        int nArt = Integer.parseInt(datos.get("nArticulos").toString());
//        datos.put("nArticulos", nArt - cantTotal);
        datos.put("nArticulos", nArt - declarado);
        if (dif <= 0) {
            resultado = Integer.parseInt(detalle.get("precio").toString()) * cantTotal;
            List<JSONObject> detalleAux = new ArrayList<>();
            resetMapeo();
            for (JSONObject detalleEnCuestion : detalleArtList) {
                JSONObject jsonArtAux = (JSONObject) detalleEnCuestion.get("articulo");
                if (codArticulo != (long) jsonArtAux.get("codArticulo")) {
                    detalleAux.add(detalleEnCuestion);
                    hashJsonArtDet.put((Long) jsonArtAux.get("idArticulo"), detalleAux.lastIndexOf(detalleEnCuestion));
                    hashJsonArticulo.put((Long) jsonArtAux.get("codArticulo"), jsonArtAux);
                }
            }
            detalleArtList = new ArrayList<>();
            detalleArtList = detalleAux;
        } else {
            int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
            resultado = Integer.parseInt(detalle.get("precio").toString()) * declarado;
            long resultadoActual = Integer.parseInt(detalle.get("precio").toString()) * dif;
            long iva = (long) detalle.get("poriva");
            if (iva == 0l) {
                detalleArtList.get(index).put("exenta", resultadoActual);
            } else {
                detalleArtList.get(index).put("gravada", resultadoActual);
            }
            detalleArtList.get(index).put("cantidad", dif);
        }
        neto = total - resultado;
        dato = formateador.format(neto);
        labelTotalGs.setText("Gs " + dato);
        labelCantidad.setText("0");
        imgProducto.setImage(null);
        precioTotal = Long.parseLong(String.valueOf(neto));
        seteandoMontoFact();
        try {
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                }
            }
            if (idCab == 0L) {
                creandoCabFactura(false);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        } catch (IOException ex) {
            Logger.getLogger(FacturaVentaEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        tabla.getItems().clear();
        tabla.getItems().addAll(detalleArtList);
        //NEW
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setFacturados(fact);
        actualizarDatos();

    }

    private static void crearCabeceraLocalmente() throws ParseException, IOException {
        ObjectMapper mapper = new ObjectMapper();

        JSONObject cabFacturaRegistrado = creandoJsonFactCab();

        JSONObject ef = (JSONObject) parser.parse(cabFactura.get("estadoFactura").toString());
        JSONObject s = (JSONObject) parser.parse(cabFacturaRegistrado.get("sucursal").toString());
//        JSONObject s = (JSONObject) parser.parse(datos.get("sucursal").toString());

        JSONObject jsonFact = new JSONObject();

        JSONObject estadoFact = new JSONObject();
        estadoFact.put("idEstadoFactura", Long.parseLong(ef.get("idEstadoFactura").toString()));

        JSONObject sucursal = new JSONObject();
        sucursal.put("idSucursal", Long.parseLong(s.get("idSucursal").toString()));

        jsonFact.put("nroFactura", cabFactura.get("nroFactura").toString());
        jsonFact.put("fechaEmision", null);
        jsonFact.put("idFacturaClienteCab", Long.parseLong(cabFactura.get("idFacturaClienteCab").toString()));
        jsonFact.put("estadoFactura", estadoFact);
        jsonFact.put("sucursal", sucursal);

        String jsonObj = jsonFact.toString();
        FacturaClienteCabDTO factDTO = mapper.readValue(jsonObj, FacturaClienteCabDTO.class);
        long fe = Long.parseLong(cabFacturaRegistrado.get("fechaEmision").toString());
        factDTO.setFechaEmision(new Timestamp(fe));

        factDTO.setCancelado(null);
        factDTO.setUsuAlta(null);
        factDTO.setUsuMod(null);
        factDTO.setFechaMod(null);
        factDTO.setMontoFactura(null);
        factDTO.setNroActual(null);
        factDTO.setNroActual(null);

        factDTO.setCaja(null);
        factDTO.setCliente(null);
        factDTO.setTipoComprobante(null);
        factDTO.setTipoMoneda(null);

        FacturaClienteCab fact = factDAO.insertarObtenerObjeto(FacturaClienteCab.fromEstadoAndSucursal(factDTO));

        if (fact != null) {
            System.out.println("FACTURA CLIENTE CAB AGREGADOS EXITOSAMENTE");
        }

    }

    private static void actualizarCabeceraLocalmente() throws IOException, ParseException {
        ObjectMapper mapper = new ObjectMapper();

        JSONObject ef = (JSONObject) parser.parse(cabFactura.get("estadoFactura").toString());
        JSONObject s = (JSONObject) parser.parse(cabFactura.get("sucursal").toString());

        JSONObject jsonFact = new JSONObject();

        JSONObject estadoFact = new JSONObject();
        estadoFact.put("idEstadoFactura", Long.parseLong(ef.get("idEstadoFactura").toString()));

        JSONObject sucursal = new JSONObject();
        sucursal.put("idSucursal", Long.parseLong(s.get("idSucursal").toString()));

        jsonFact.put("nroFactura", cabFactura.get("nroFactura").toString());
        jsonFact.put("fechaEmision", null);
        jsonFact.put("idFacturaClienteCab", Long.parseLong(cabFactura.get("idFacturaClienteCab").toString()));
        jsonFact.put("estadoFactura", estadoFact);
        jsonFact.put("sucursal", sucursal);

        String jsonObj = jsonFact.toString();
        FacturaClienteCabDTO factDTO = mapper.readValue(jsonObj, FacturaClienteCabDTO.class);
        long fe = Long.parseLong(cabFactura.get("fechaEmision").toString());
        factDTO.setFechaEmision(new Timestamp(fe));

        factDTO.setCancelado(null);
        factDTO.setUsuAlta(null);
        factDTO.setUsuMod(null);
        factDTO.setFechaMod(null);
        factDTO.setMontoFactura(null);
        factDTO.setNroActual(null);
        factDTO.setNroActual(null);

        factDTO.setCaja(null);
        factDTO.setCliente(null);
        factDTO.setTipoComprobante(null);
        factDTO.setTipoMoneda(null);

        FacturaClienteCab fact = factDAO.actualizarObtenerObjeto(FacturaClienteCab.fromEstadoAndSucursal(factDTO));

        if (fact != null) {
            System.out.println("FACTURA CLIENTE CAB ACTUALIZADOS EXITOSAMENTE");
        }
    }

    private HashMap verificandoDatosCierre() {
        HashMap valor = new HashMap();
        valor.put("cajero", "null");
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            alertCerrarTurno = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CERRAR TURNO COMO CAJERO?", ok, cancel);
            alertCerrarTurno.showAndWait();
            if (alertCerrarTurno.getResult() == ok) {
                ButtonType okData = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                ButtonType cancelData = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                alertArqueo = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA REALIZAR EL ARQUEO?", okData, cancelData);
                alertArqueo.showAndWait();
                if (alertArqueo.getResult() == okData) {
                    DatosEnCaja.setFacturados(null);
                    users = null;
                    fact = null;
                    DatosEnCaja.setUsers(null);
                    actualizarDatos();
                    this.sc.loadScreen("/vista/caja/loginSupervisorFXML.fxml", 515, 290, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
                    valor.put("cajero", "arqueo");
                } else {
                    DatosEnCaja.setFacturados(null);
                    users = null;
                    fact = null;
                    DatosEnCaja.setUsers(null);
                    actualizarDatos();
//                this.sc.loadScreen("/vista/caja/loginSupervisorFXML.fxml", 540, 312, "/vista/caja/facturaDeVentaFXML.fxml", 974, 558, true);
//                this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/caja/facturaDeVentaFXML.fxml", 974, 558, true);
                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, true);
                    valor.put("cajero", "cambio");
                    alertArqueo.close();
                }
            } else if (alertCerrarTurno.getResult() == cancel) {
                alertCerrarTurno.close();
            }
            alertArqueo = null;
            alertCerrarTurno = null;
        }
        return valor;
    }

    private boolean verificandoCaidaFormaPago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("energiaElectrica")) {
            mensajeAlerta("Esta Factura debe ser cancelada por problemas de caída de la Energía Eléctrica, ya que podrían contener datos corruptos");
            return true;
        } else {
            return false;
        }
    }

    private void mensajeDetalle(String msj, String title) {
        ButtonType btnAcept = new ButtonType("Salir (ESC)", ButtonBar.ButtonData.OK_DONE);
        Alert alert = new Alert(Alert.AlertType.INFORMATION, msj, btnAcept);
        alert.setTitle(title);
        alert.setHeaderText("Mensaje del Sistema!");
//        alert.setContentText("I have a great message for you!");

        DialogPane dialogPane = alert.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialogInformation");

        alert.showAndWait();
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }
}
