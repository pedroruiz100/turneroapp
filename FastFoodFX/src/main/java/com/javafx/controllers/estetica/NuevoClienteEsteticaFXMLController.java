package com.javafx.controllers.estetica;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.javafx.controllers.caja.*;
import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.TarjetaClienteFiel;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.ClienteDTO;
import com.peluqueria.dto.TarjetaClienteFielDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.estetica.FacturaDeVentaEsteticaFXMLController.parser;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class NuevoClienteEsteticaFXMLController extends BaseScreenController implements Initializable {

    public static void cargarCliente(Label labelRucCliente, Label labelNombreCliente, TextField txtCod) {
        lblRuc = labelRucCliente;
        lblNombre = labelNombreCliente;
        txtCodigo = txtCod;
    }

    static void setLabels(Label lNombre, Label lRuc) {
        lblRuc = lRuc;
        lblNombre = lNombre;
        lblRuc.setText("-");
        lblNombre.setText("-");
    }

    boolean enterEstado = false;

    private static String rucliente = "";

    public static void setRucCliente(String text, TextField txtCod) {
        rucliente = text;
        txtCodigo = txtCod;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private boolean exitoEditar;
    private static boolean clienteSi;
    private static TextField txtCodigo;
    private long idCliente;
    private static Label lblRuc = new Label();
    private static Label lblNombre = new Label();
    private int codCliente;
    private List<JSONObject> clienteFielList;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean crearNuevo;

    final KeyCombination altN = new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN);

    private static JSONObject jsonClienteFiel;
    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente;

    private JSONObject clienteFiel;
    @Autowired
    private RangoClientefielDAO rangoClifielDAO;
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private TableView<JSONObject> tableViewCliente;
    private TableColumn<JSONObject, String> tableColumnNombre;
    private TableColumn<JSONObject, String> tableColumnApellido;
    private TableColumn<JSONObject, String> tableColumnRucCi;
    @FXML
    private Label labelNombreClienteNuevo12;
    @FXML
    private Label labelApellidoClienteNuevo;
    @FXML
    private Label labelRucClienteNuevo12;
    @FXML
    private TextField textFieldClienteApellidoNuevo;

//    private Button buttonNuevo;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private AnchorPane anchorPaneNuevo;
    @FXML
    private TextField textFieldNombreClienteNuevo;
    @FXML
    private TextField textFieldRucClienteNuevo;
    @FXML
    private TextField textFieldClienteTelefonoNuevo;
    @FXML
    private Label labelRucClienteNuevo1211;
    @FXML
    private Label labelRetiroDinero;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void buttonActualizarAction(ActionEvent event) {
        editandoCliente();
    }

    private void buttonBuscarClienteEsteticaAction(ActionEvent event) {
//        buscandoCliente(true);
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {

        //SETEANDO FIEL
        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            if (ClienteFielFXMLController.getJsonClienteFiel().get("cipas") != null) {
//                textFieldRucCliente.setText(ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
            } else {
//                textFieldRucCliente.setText("");
            }
        }
        if (!rucliente.equals("")) {
            textFieldRucClienteNuevo.setText(rucliente);
        }
        //SETEANDO FIEL
        crearNuevo = false;
        clienteFiel = new JSONObject();

        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        numVal = new NumberValidator();
//        buttonAnhadir.setDisable(true);
//        buttonActualizar.setDisable(true);
//        buttonNuevo.setDisable(true);
        exitoCrear = false;
        exitoEditar = false;
        escucha = false;
        alert = false;
        clienteSi = false;
        idCliente = 0l;
        codCliente = -1;
        listenerCampos();
        enterEstado = false;
//        validando();
    }
    //INICIAL INICIAL INICIAL **************************************************

//    private void seteandoParamClienteFiel(JSONObject clienteFiel) {
//        jsonClienteFiel = clienteFiel;
//    }
    public static void resetParamClienteFiel() {
        jsonClienteFiel = null;
    }//NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

//    public static void seteandoParamCliente(JSONObject cliente) {
//        jsonCliente = cliente;
//    }
    public static void resetParamCliente() {
        jsonCliente = null;
        clienteSi = false;
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFacturaVenta() {
        clienteSi = false;
        rucliente = "";
        String ruc = "RUC/CI: " + textFieldRucClienteNuevo.getText();
        if (textFieldRucClienteNuevo.getText().contains("-")) {
            ruc = "RUC/CI: " + textFieldRucClienteNuevo.getText();
        }
//        if (isNumeric(textFieldRucClienteNuevo.getText())) {
//            if (Utilidades.calculoSET(textFieldRucClienteNuevo.getText()).equals("")) {
//                ruc = "RUC/CI: " + textFieldRucClienteNuevo.getText();
//            } else {
//                ruc = "RUC/CI: " + Utilidades.calculoSET(textFieldRucClienteNuevo.getText());
//            }
//        }

        String nombre = textFieldNombreClienteNuevo.getText() + " " + textFieldClienteApellidoNuevo.getText();
        if (lblNombre.getText().equals("-")) {
            FacturaDeVentaEsteticaFXMLController.setTearDatos(lblRuc, lblNombre, ruc, nombre);
        } else {
//            FacturaDeVentaEsteticaFXMLController.setTearDatos(lblRuc, lblNombre, ruc, nombre);
        }
        FacturaDeVentaEsteticaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        FacturaDeVentaEsteticaFXMLController.setCliente(ruc, nombre);
        textFieldRucClienteNuevo.setText("");
        textFieldNombreClienteNuevo.setText("");
        textFieldClienteApellidoNuevo.setText("");
        textFieldClienteTelefonoNuevo.setText("");
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/NuevoClienteEsteticaFXML.fxml", 519, 187, true);
    }

    private void volviendo() {
        clienteSi = false;
        rucliente = "";
        FacturaDeVentaEsteticaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/estetica/FacturaDeVentaEsteticaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/estetica/NuevoClienteEsteticaFXML.fxml", 519, 187, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {
        textFieldNombreClienteNuevo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("") && !textFieldRucClienteNuevo.getText().contentEquals("")) {
//                buttonNuevo.setDisable(false);
                crearNuevo = true;
            } else {
                crearNuevo = false;
            }
        });
        textFieldClienteApellidoNuevo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!textFieldRucClienteNuevo.getText().contentEquals("") && !textFieldNombreClienteNuevo.getText().contentEquals("")) {
                crearNuevo = true;
            } else {
                crearNuevo = false;
            }
        });
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
            JSONParser parser = new JSONParser();
            if (!enterEstado) {
                enterEstado = false;
            } else {
                if (crearNuevo) {
                    creandoCliente();
                } else {
                    mensajeAlerta("El campo RUC/CI y NOMBRE no deben quedar vacíos");
                    textFieldRucClienteNuevo.requestFocus();
                }
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            }
        }
    }

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void borrando() {
        resetParamCliente();
        resetParamClienteFiel();
//        navegandoFormaPago();
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ape, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ape, ruc);
        return clienteJSONObjList;
    }

//    private void buscandoCliente(boolean efeuno) {
//        String nom, ape, ruc;
//        if (textFieldNombreCliente.getText().contentEquals("")) {
//            nom = "null";
//        } else {
//            nom = textFieldNombreCliente.getText();
//        }
//        if (textFieldRucCliente.getText().contentEquals("")) {
//            ruc = "null";
//        } else {
//            ruc = textFieldRucCliente.getText();
//        }
//        if (textFieldApellidoCliente.getText().contentEquals("")) {
//            ape = "null";
//        } else {
//            ape = textFieldApellidoCliente.getText();
//        }
//
//        if (nom.equals("null") && ape.equals("null") && !ruc.equals("null")) {
//            buscarClientePorRucCi();
//            actualizandoTablaCliente();
//        } else {
//            clienteList = jsonArrayCliente(nom, ape, ruc);
//            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
//            }
//            actualizandoTablaCliente();
//        }
//        if (!tableViewCliente.getItems().isEmpty() && efeuno) {
//            tableViewCliente.requestFocus();
//            tableViewCliente.getSelectionModel().select(0);
//            tableViewCliente.getFocusModel().focus(0);
//        } else if (!tableViewCliente.getItems().isEmpty() && !efeuno) {
//            tableViewCliente.getSelectionModel().select(0);
//            tableViewCliente.getFocusModel().focus(0);
//        }
//    }
//    private void buscarClientePorRucCi() {
//        String rucCliente = "";
//        JSONParser parser = new JSONParser();
//        try {
//            rucCliente = textFieldRucCliente.getText();
//            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
//            int contador = rucCliente.split("-", -1).length - 1;
//            Cliente cliente = new Cliente();
//            if (contador >= 1) {
//                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
//                cliente = cliDAO.listarPorRuc(rucCliente);
//                if (cliente.getNombre() != null) {
//                    cliente.setFecNac(null);
//                    cliente.setFechaAlta(null);
//                    cliente.setFechaMod(null);
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toClienteCajaDTO()));
//                    clienteList.add(jsonCli);
//                }
//            } else if (contador == 0) {
//                //Listar por CI en el caso que exista
//                cliente = cliDAO.listarPorRuc(rucCliente);
//                if (cliente.getIdCliente() == null) {
//                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
//                    List<Cliente> listClien = cliDAO.listarPorCIRevancha(rucCliente);
//                    if (listClien != null) {
//                        if (listClien.isEmpty()) {
//                            clienteList = new ArrayList<>();
//                        } else {
//                            clienteList = new ArrayList<>();
//                            for (int i = 0; i < listClien.size(); i++) {
//                                Cliente cli = listClien.get(i);
//                                cli.setFecNac(null);
//                                cli.setFechaAlta(null);
//                                cli.setFechaMod(null);
//                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toClienteCajaDTO()));
//                                clienteList.add(jsonCli);
//                            }
//                        }
//                    } else {
//                        clienteList = new ArrayList<>();
//                    }
//                } else {
//                    clienteList = new ArrayList<>();
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toClienteCajaDTO()));
//                    clienteList.add(jsonCli);
//                }
//            }
//            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
//        }
//    }
//    private void buscarAutomatico(JSONObject cliente) {
//        if (cliente != null) {
//            if (cliente.get("ruc") != null) {
//                textFieldRucCliente.setText(cliente.get("ruc").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//            textFieldNombreCliente.setText(cliente.get("nombre").toString());
//            textFieldApellidoCliente.setText(cliente.get("apellido").toString());
//            buscandoCliente(true);
//        }
//    }
    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
        JSONObject cliente = new JSONObject();
        cliente = creandoJsonCliente();
//            if (!cliente.containsKey("estado")) {
//
//            } else {
//
//            }
        int codCli = Integer.parseInt(cliente.get("codCliente").toString());
        Cliente clie = cliDAO.getByCod(codCli);
        if (clie == null) {
            long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
            cliente.put("idCliente", idActual);
            exitoCrear = persistiendoPendientes(cliente);
            if (exitoCrear) {
                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                cliente.put("fechaAlta", null);
                cliente.put("fechaMod", null);
                ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
                cliDTO.setFechaAlta(timestamp);
                cliDTO.setFechaMod(timestamp);
                Cliente cli = Cliente.fromClienteDTO(cliDTO);
                Pais pais = new Pais();
                pais.setIdPais(0L);
                Departamento dpto = new Departamento();
                dpto.setIdDepartamento(0l);
                Ciudad ciu = new Ciudad();
                ciu.setIdCiudad(0l);
                Barrio barr = new Barrio();
                barr.setIdBarrio(0l);
                cli.setPais(pais);
                cli.setDepartamento(dpto);
                cli.setCiudad(ciu);
                cli.setBarrio(barr);
                try {
                    cliDAO.insertar(cli);
                    //CONSULTAR CLIENTE
                    org.json.JSONObject json = new org.json.JSONObject(cliente);
                    BuscarClienteEsteticaFXMLController.seteandoParamCliente(cliente);
                    lblRuc.setText("RUC/CI: " + cliente.get("ruc").toString());
                    if (json.isNull("apellido")) {
                        lblNombre.setText(cliente.get("nombre").toString());
                    } else {
                        lblNombre.setText(cliente.get("nombre").toString() + " " + cliente.get("apellido").toString());
                    }
                    org.json.JSONObject jsonFactura = new org.json.JSONObject((JSONObject) parser.parse(fact.toString()));
                    if (jsonFactura.isNull("facturaClienteCab")) {
                        JSONObject jsonObj = new JSONObject();
                        cliente.put("idCliente", cliente.get("idCliente"));
                        jsonObj.put("cliente", cliente);
                        fact.put("facturaClienteCab", jsonObj);
                    } else {

                        JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        cliente.put("idCliente", cliente.get("idCliente"));
                        cab.put("cliente", cliente);
                        fact.put("facturaClienteCab", cab);
                    }
                    buscandoClienteFiel();

                    System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
                    navegandoFacturaVenta();

                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.CLOSE) {
                    alert2.close();
                }
            }
        } else {
            if (actualizarPendientes(cliente)) {
                try {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    cliente.put("fechaAlta", null);
                    cliente.put("fechaMod", null);
                    ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
                    cliDTO.setFechaAlta(timestamp);
                    cliDTO.setFechaMod(timestamp);
                    Cliente cli = Cliente.fromClienteDTO(cliDTO);
                    Pais pais = new Pais();
                    pais.setIdPais(0L);
                    Departamento dpto = new Departamento();
                    dpto.setIdDepartamento(0l);
                    Ciudad ciu = new Ciudad();
                    ciu.setIdCiudad(0l);
                    Barrio barr = new Barrio();
                    barr.setIdBarrio(0l);
                    cli.setPais(pais);
                    cli.setDepartamento(dpto);
                    cli.setCiudad(ciu);
                    cli.setBarrio(barr);

                    cliDAO.actualizar(cli);
                    //CONSULTAR CLIENTE
                    org.json.JSONObject json = new org.json.JSONObject(cliente);
                    BuscarClienteEsteticaFXMLController.seteandoParamCliente(cliente);
                    lblRuc.setText("RUC/CI: " + cli.getRuc());
                    if (json.isNull("apellido")) {
                        lblNombre.setText(cliente.get("nombre").toString());
                    } else {
                        lblNombre.setText(cliente.get("nombre").toString() + " " + cliente.get("apellido").toString());
                    }
                    org.json.JSONObject jsonFactura = new org.json.JSONObject((JSONObject) parser.parse(fact.toString()));
                    if (jsonFactura.isNull("facturaClienteCab")) {
                        JSONObject jsonObj = new JSONObject();
                        cliente.put("idCliente", cliente.get("idCliente"));
                        jsonObj.put("cliente", cliente);
                        fact.put("facturaClienteCab", jsonObj);
                    } else {

                        JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        cliente.put("idCliente", cliente.get("idCliente"));
                        cab.put("cliente", cliente);
                        fact.put("facturaClienteCab", cab);
                    }
                    buscandoClienteFiel();

                    System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
                    navegandoFacturaVenta();

                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
                }
            }
//                new Toaster().mensajeGenerico("Mensaje del Sistema", "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", "", 2);
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/NuevoClienteEsteticaFXML.fxml", 519, 187, false);
//        }
        return exitoCrear;
    }

    private void buscandoClienteFiel() {
        String nom, ape, cipas;
        nom = "null";
        if (textFieldRucClienteNuevo.getText().contains("-")) {
            StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
            cipas = st.nextElement().toString();
        } else {
            cipas = textFieldRucClienteNuevo.getText();
        }

        ape = "null";
        clienteFielList = jsonArrayClienteFiel(nom, ape, cipas);
        if (clienteFielList.isEmpty()) {
//registrar cliente fiel y retornar en json cliente fiel
            nuevoClienteFiel();
        } else {
//            JSONObject cab = new JSONObject();
//            try {
//                cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                JSONObject cliente = new JSONObject();
//                cliente.put("idTarjetaClienteFiel", clienteFielList.get(0).get("idTarjetaClienteFiel"));
//                cab.put("clienteFiel", clienteFielList.get(0));
            BuscarClienteEsteticaFXMLController.seteandoParamClienteFiel(clienteFielList.get(0));
//                fact.put("facturaClienteCab", cab);
//                actualizarDatos();
//            } catch (ParseException ex) {
//                Logger.getLogger(NuevoClienteEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
        }
    }

    private void nuevoClienteFiel() {
        String soloCi = "";
        if (textFieldRucClienteNuevo.getText().contains("-")) {
            StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
            soloCi = st.nextElement().toString();
        } else {
            soloCi = textFieldRucClienteNuevo.getText();
        }
//        String soloCi = st.nextElement().toString();
        List<TarjetaClienteFiel> listTarjFiel = tarDAO.filtrarPorNomApeCi("null", "null", soloCi);
        if (listTarjFiel.toString().equalsIgnoreCase("[]")) {

            if (insertarNuevoClienteEsteticaFiel()) {
//                buscarClienteFielPorId(Long.parseLong(clienteFiel.get("idTarjetaClienteFiel").toString()));
//                buscarClienteFielPorId(Long.valueOf(clienteFiel.get("idTarjetaClienteFiel").toString()));

//                JSONObject cab;
//                try {
//                    cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idTarjetaClienteFiel", clienteFiel.get("idTarjetaClienteFiel"));
//                    cab.put("clienteFiel", clienteFiel);
                BuscarClienteEsteticaFXMLController.seteandoParamClienteFiel(clienteFiel);
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
//                } catch (ParseException ex) {
//                    Logger.getLogger(NuevoClienteEsteticaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
        }
    }

//    private void buscarClienteFielPorId(long idClienteFiel) {
//        try {
//            JSONParser parser = new JSONParser();
//            List<JSONObject> lista = new ArrayList<>();
//            TarjetaClienteFiel tarFiel = tarDAO.getById(idClienteFiel);
//            tarFiel.setFechaAlta(null);
//            tarFiel.setFechaMod(null);
//            Cliente clien = cliDAO.getById(tarFiel.getCliente().getIdCliente());
//            tarFiel.setCliente(null);
//            String gsonLista = gson.toJson(tarFiel);
//            JSONObject objteJSON = (JSONObject) parser.parse(gsonLista);
//            JSONObject clienteJSON = new JSONObject();
//            clienteJSON.put("idCliente", clien.getIdCliente());
//            clienteJSON.put("nombre", clien.getNombre());
//            clienteJSON.put("nombre", clien.getNombre());
//            clienteJSON.put("codCliente", clien.getCodCliente());
//            clienteJSON.put("ruc", clien.getRuc());
//            objteJSON.put("cliente", clienteJSON);
//            lista.add(objteJSON);
//            clienteFielList = lista;
//
//            
//        } catch (ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }
//    }
    private boolean insertarNuevoClienteEsteticaFiel() {
        try {
            boolean exitoInsertar = false;
//            Jsonb jsonb = JsonbBuilder.create();
            JSONParser parser = new JSONParser();
            //seteando datos faltantes
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            clienteFiel.put("idTarjetaClienteFiel", rangoClifielDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal()));
            clienteFiel.put("fechaAlta", timestampEmision);
            clienteFiel.put("fechaMod", timestampEmision);
            clienteFiel.put("usuAlta", Identity.getNomFun());
            clienteFiel.put("usuMod", Identity.getNomFun());
            clienteFiel.put("empresaSuc", empresa.get("descripcionEmpresa").toString());

//            JSONObject cliente = new JSONObject();
            JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            org.json.JSONObject jsonCabecera = new org.json.JSONObject(cab);
            if (!jsonCabecera.isNull("cliente")) {
                JSONObject jsonCli = (JSONObject) parser.parse(jsonCabecera.getJSONObject("cliente").toString());
                jsonCli.remove("email");
//                cliente.put("cliente", jsonCli);
                clienteFiel.put("cliente", jsonCli);

                String soloCi = "";
                if (textFieldRucClienteNuevo.getText().contains("-")) {
                    StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
                    soloCi = st.nextElement().toString();
                } else {
                    soloCi = textFieldRucClienteNuevo.getText();
                }
                clienteFiel.put("cipas", soloCi);
            }

            exitoInsertar = registrandoClienteFielLocal(clienteFiel.toString());
            if (exitoInsertar) {
                //                    TarjetaClienteFiel tarFiel = mapper.readValue(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                clienteFiel.remove("fechaAlta");
                clienteFiel.remove("fechaMod");
                TarjetaClienteFiel tarFiel = gson.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                tarFiel.setFechaAlta(tsNow);
                tarFiel.setFechaMod(tsNow);

//                TarjetaClienteFiel tarFiel = jsonb.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                boolean valor = tarDAO.insetarObtenerObj(tarFiel);
                if (valor) {
                    System.out.println("HAZ REGISTRADO UN CLIENTE FIEL");
                } else {
                    System.out.println("ERROR: AL INTENTAR REGISTRAR UN CLIENTE FIEL");
                }
//                textFieldCodClienteFiel.setText(clienteFiel.get("cipas").toString());
            }
            return exitoInsertar;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }

    private boolean registrandoClienteFielLocal(String json) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO pendiente.clienfiel_pendientes (fecha_registro ,msj, tabla, dml, ip) VALUES (now(),'" + json + "','tarjeta_cliente_fiel', 'I', '" + Utilidades.host + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private List<JSONObject> jsonArrayClienteFiel(String nom, String ape, String ciPass) {
        //carácter "espacio", formato...
        nom = nom.replaceAll(" ", "%20");
        ape = ape.replaceAll(" ", "%20");
        ciPass = ciPass.replaceAll(" ", "%20");
        //carácter "espacio", formato...
        List<JSONObject> clienteFielJSONObjList = new ArrayList<JSONObject>();
        clienteFielJSONObjList = generarTarjetaClienteFielLocal(nom, ape, ciPass);
        return clienteFielJSONObjList;
    }

    private List<JSONObject> generarTarjetaClienteFielLocal(String nom, String ape, String ciPass) {
        List<JSONObject> listaJSON = new ArrayList<>();
        JSONParser parser = new JSONParser();
        List<TarjetaClienteFiel> tarFiel = tarDAO.filtrarPorNomApeCi(nom, ape, ciPass);
        for (TarjetaClienteFiel tar : tarFiel) {
            try {
                TarjetaClienteFielDTO tarjetaFielDTO = tar.toTarjetaClienteFielDTO();
                String obj = gson.toJson(tarjetaFielDTO);
                listaJSON.add((JSONObject) parser.parse(obj));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaJSON;
    }
    //////CREATE, CLIENTE -> POST

    //////UPDATE, CLIENTE -> PUT
    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            JSONObject cliente = new JSONObject();
            cliente = editandoJsonCliente();
            exitoEditar = actualizarPendientes(cliente);
            if (exitoEditar) {
                try {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject usuarioCajero = Identity.getUsuario();
                    JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                    String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                    cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
                    System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                }
//                    Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.OK) {
//                        alert2.close();
//                        buscarAutomatico(cliente);
//                    } else {
//                        alert2.close();
//                        buscarAutomatico(cliente);
//                    }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.CLOSE) {
                    alert2.close();
                }
            }
        } else {
            alert.close();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/estetica/NuevoClienteEsteticaFXML.fxml", 519, 187, false);
//        }
        return exitoEditar;
    }
    //////UPDATE, CLIENTE -> PUT

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
//        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            this.alert = true;
//            alert2.close();
//        }
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ape, String ruc) {
        JSONParser parser = new JSONParser();
        List<Cliente> cliente = cliDAO.listarPorNomRuc(nom, ape, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Cliente cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toClienteCajaDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - CLIENTE
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
    private JSONObject creandoJsonCliente() {
        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject cliente = new JSONObject();
        Cliente cli = new Cliente();

        if (textFieldRucClienteNuevo.getText().contentEquals("")) {
            cliente.put("ruc", "0");
        } else {
            int count = StringUtils.countMatches(textFieldRucClienteNuevo.getText(), "-");
            switch (count) {
                case 0:
                    try {
                        cli = cliDAO.getByCod(Integer.parseInt(textFieldRucClienteNuevo.getText()));
                    } catch (Exception e) {
                        cli = null;
                    } finally {
                    }
                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
                    break;
                case 1:
                    StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
                    String cedula = st.nextElement().toString();
                    if (!isNumeric(cedula)) {
                        try {
                            cli = cliDAO.getByCod(Integer.parseInt(textFieldRucClienteNuevo.getText()));
                        } catch (Exception e) {
                            cli = null;
                        } finally {
                        }
                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
                    } else {
                        if (Utilidades.calculoSET(cedula).equals("")) {
                            try {
                                cli = cliDAO.getByCod(Integer.parseInt(cedula));
                            } catch (Exception e) {
                                cli = null;
                            } finally {
                            }
                            cliente.put("ruc", textFieldRucClienteNuevo.getText());
                        } else {
                            try {
                                cli = cliDAO.getByCod(Integer.parseInt(cedula));
                            } catch (Exception e) {
                                cli = null;
                            } finally {
                            }
                            try {
                                if (cli == null) {
                                    cli = cliDAO.listarPorRuc(Utilidades.calculoSET(cedula));
                                }
                            } catch (Exception e) {
                                cli = null;
                            } finally {
                            }
                            cliente.put("ruc", Utilidades.calculoSET(cedula));
                        }
                    }
                    break;
                default:
                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
                    break;
            }
        }

        if (cli == null) {
            //**********************************************************************
            cliente.put("nombre", textFieldNombreClienteNuevo.getText());
            cliente.put("apellido", textFieldClienteApellidoNuevo.getText());
            cliente.put("usuAlta", Identity.getNomFun());
            cliente.put("fechaAlta", timestampJSON);
            cliente.put("usuMod", Identity.getNomFun());
            cliente.put("fechaMod", timestampJSON);
            //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
            cliente.put("telefono", textFieldClienteTelefonoNuevo.getText());
            cliente.put("segundaLateral", null);
            cliente.put("primeraLateral", null);
            cliente.put("nroLocal", null);
            cliente.put("pais", null);
            cliente.put("departamento", null);
            cliente.put("ciudad", null);
            cliente.put("barrio", null);
            cliente.put("email", null);
            cliente.put("compraUltFecha", null);
            cliente.put("compraIniFecha", null);
            String codCliente = "";
            String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
            if (arrayCod.length > 0) {
                codCliente = arrayCod[0];
                if (!isNumeric(textFieldRucClienteNuevo.getText())) {
                    codCliente = Utilidades.genCodClienteParana(textFieldRucClienteNuevo.getText()) + "";
                }
            } else {
                codCliente = textFieldRucClienteNuevo.getText();
            }
            cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
            cliente.put("callePrincipal", null);
        } else {
            //**********************************************************************
            cliente.put("nombre", cli.getNombre());
            cliente.put("idCliente", cli.getIdCliente());
            cliente.put("apellido", cli.getApellido());
            cliente.put("usuAlta", Identity.getNomFun());
            cliente.put("fechaAlta", timestampJSON);
            cliente.put("usuMod", Identity.getNomFun());
            cliente.put("fechaMod", timestampJSON);
            //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
            cliente.put("telefono", cli.getTelefono());
            cliente.put("segundaLateral", null);
            cliente.put("primeraLateral", null);
            cliente.put("nroLocal", null);
            cliente.put("pais", null);
            cliente.put("departamento", null);
            cliente.put("ciudad", null);
            cliente.put("barrio", null);
            cliente.put("email", null);
            cliente.put("compraUltFecha", null);
            cliente.put("compraIniFecha", null);
            String codCliente = "";
            String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
            if (arrayCod.length > 0) {
                codCliente = arrayCod[0];
//                if (!isNumeric(textFieldRucClienteNuevo.getText())) {
//                    codCliente = Utilidades.genCodClienteParana(textFieldRucClienteNuevo.getText()) + "";
//                }
            } else {
                codCliente = textFieldRucClienteNuevo.getText();
            }
            cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
            cliente.put("callePrincipal", null);
//            cliente.put("estado", true);
        }

        return cliente;
    }
    //JSON CREANDO CLIENTE

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    //JSON EDITANDO CLIENTE
    private JSONObject editandoJsonCliente() {
        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
        Cliente cli = cliDAO.getById(idCliente);
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject cliente = new JSONObject();
        //**********************************************************************
        cliente.put("idCliente", idCliente);
//        cliente.put("nombre", textFieldNombreClienteEditar.getText());
//        cliente.put("apellido", textFieldClienteApellidoEditar.getText());
//        if (textFieldRucClienteEditar.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            cliente.put("ruc", textFieldRucClienteEditar.getText());
//        }
        cliente.put("usuMod", Identity.getNomFun());
        cliente.put("fechaMod", timestampJSON);
        //**********************************************************************
        cliente.put("usuAlta", cli.getUsuAlta());

        if (cli.getFechaAlta() != null) {
            cliente.put("fechaAlta", cli.getFechaAlta().getTime());
        } else {
            cliente.put("fechaAlta", null);
        }

//        cliente.put("telefono2", textFieldCelularClienteEditar.getText());
//        cliente.put("telefono", textFieldTelefonoClienteEditar.getText());
        cliente.put("segundaLateral", cli.getSegundaLateral());
        cliente.put("primeraLateral", cli.getPrimeraLateral());
        cliente.put("nroLocal", cli.getNroLocal());

        JSONObject jsonPais = new JSONObject();
        jsonPais.put("idPais", cli.getPais().getIdPais());
        cliente.put("pais", jsonPais);

        JSONObject jsonDpto = new JSONObject();
        jsonDpto.put("idDepartamento", cli.getDepartamento().getIdDepartamento());
        cliente.put("departamento", jsonDpto);

        JSONObject jsonCiudad = new JSONObject();
        jsonCiudad.put("idCiudad", cli.getCiudad().getIdCiudad());
        cliente.put("ciudad", jsonCiudad);

        JSONObject jsonBarrio = new JSONObject();
        jsonBarrio.put("idBarrio", cli.getBarrio().getIdBarrio());
        cliente.put("barrio", jsonBarrio);
        cliente.put("email", cli.getEmail());
        cliente.put("compraUltFecha", null);
        cliente.put("compraIniFecha", null);
        cliente.put("codCliente", this.codCliente);
        cliente.put("callePrincipal", cli.getCallePrincipal());
        return cliente;
    }
    //JSON EDITANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente() {
        clienteData = FXCollections.observableArrayList(clienteList);
        //columna Nombre ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("nombre"));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnApellido.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnApellido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String apellido = String.valueOf(data.getValue().get("apellido"));
                if (apellido.contentEquals("null")) {
                    apellido = "";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnRucCi.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnRucCi.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("ruc"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        //columna Ruc .................................................
        tableViewCliente.setItems(clienteData);
        if (!escucha) {
//            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

    @FXML
    private void textFieldRucClienteNuevoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void textFieldClienteApellidoNuevoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void textFieldClienteTelefonoNuevoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void keyPressEventos(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (textFieldRucClienteNuevo.isFocused()) {
                enterEstado = false;
                textFieldNombreClienteNuevo.requestFocus();
            } else if (textFieldNombreClienteNuevo.isFocused()) {
                enterEstado = false;
                textFieldClienteApellidoNuevo.requestFocus();
            } else if (textFieldClienteApellidoNuevo.isFocused()) {
                enterEstado = false;
                textFieldClienteTelefonoNuevo.requestFocus();
            } else if (textFieldClienteTelefonoNuevo.isFocused()) {
                enterEstado = true;
                textFieldNombreClienteNuevo.requestFocus();
            }
        }
    }

}
