/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.login;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.UsuarioDTO;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import static com.javafx.util.Log.logger;
import com.javafx.util.PATH;
import com.javafx.util.SubAppRun;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.UsuarioDAO;
import com.peluqueria.dao.impl.SupervisorDAOImpl;
import com.peluqueria.dto.SupervisorDTO;
import com.google.gson.GsonBuilder;
import com.javafx.util.ClosePort;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.EMF;
import com.javafx.util.EMF2;
import com.javafx.util.VerificandoInstancias;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import com.peluqueria.dao.EmpresaDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TimbradoDAO;
import de.jensd.fx.glyphs.fontawesome.FontAwesomeIconView;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.BoxBlur;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class LoginFXMLController extends BaseScreenController implements Initializable, Runnable {

    Image image;
    private JSONParser parser = new JSONParser();
    @Autowired
    private EmpresaDAO empresaDAO;
    @Autowired
    private TimbradoDAO timbradoDAO;
    @Autowired
    private TalonariosSucursaleDAO talSucuDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static SupervisorDAO superDAO = new SupervisorDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    private boolean enter;
    @Autowired
    private UsuarioDAO usuDAO;

    private static boolean llamarTask;

    public static boolean isLlamarTask() {
        return llamarTask;
    }

    public static void setLlamarTask(boolean llamarTask) {
        LoginFXMLController.llamarTask = llamarTask;
    }

    Task copyWorker;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private JFXTextField textFieldUsuario;
    @FXML
    private JFXPasswordField passwordFieldContra;
    @FXML
    private JFXButton buttonIngresar;
    @FXML
    private JFXButton buttonCerrar;
    @FXML
    private ImageView imageViewLogoCP;
    @FXML
    private Label labelAutenticacion;
    @FXML
    private AnchorPane anchorPaneLoginD;
    @FXML
    private ProgressIndicator progressIndicator;
    @FXML
    private FontAwesomeIconView fontKey;
    @FXML
    private MaterialDesignIconView materialDesignIconViewUser;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonIngresarAction(ActionEvent event) {
        validandoIngreso();
    }

    @FXML
    private void buttonCerrarAction(ActionEvent event) {
        saliendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        //Task worker
        String empresas = Utilidades.recuperarEmpresa();
        Utilidades.empresa = empresas;
        this.sc.getStage().setTitle("DHsoft " + Utilidades.version + " - Sistema de Gestión - " + empresas);
        if (isLlamarTask()) {
            copyWorker = createWorker();
            progressIndicator.progressProperty().unbind();
            progressIndicator.progressProperty().bind(copyWorker.progressProperty());
            new Thread(copyWorker).start();
        } else {
            buttonIngresar.requestFocus();
            cargandoImagen();
            animando();
            Platform.runLater(() -> textFieldUsuario.requestFocus());
            progressIndicator.setVisible(false);
        }
        //Task worker
        this.enter = true;
        long idManejo = recuperarId();
        if (idManejo != 0) {
            HashMap mapeo = getById(idManejo);
            try {
                Object cajaObj = mapeo.get("caja");
                Object usuarioObj = mapeo.get("usuario");
                Object facturaObj = mapeo.get("factura");
                DatosEnCaja.setDatos((JSONObject) parser.parse(cajaObj.toString()));
                if (usuarioObj != null) {
                    DatosEnCaja.setUsers((JSONObject) parser.parse(usuarioObj.toString()));
                } else {
                    DatosEnCaja.setUsers(new JSONObject());
                }
                if (facturaObj == null) {
                    DatosEnCaja.setFacturados(new JSONObject());
                } else if (facturaObj.toString().equalsIgnoreCase("{}")) {
                    DatosEnCaja.setFacturados(new JSONObject());
                } else {
                    DatosEnCaja.setFacturados((JSONObject) parser.parse(facturaObj.toString()));
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
        } else {
            datos = new JSONObject();
            users = new JSONObject();
            fact = new JSONObject();
        }
    }

    
    //INICIAL INICIAL INICIAL **************************************************

    public static HashMap getById(long id) {
        HashMap mapeo = new HashMap();
        ConexionPostgres.conectarLocal();
        String sql = "SELECT * FROM ande.manejo_local WHERE id_manejo=?";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ps.setLong(1, id);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                mapeo.put("caja", rs.getString("caja"));
                mapeo.put("usuario", rs.getString("usuario"));
                mapeo.put("factura", rs.getString("factura"));
            }
            ps.close();
            ConexionPostgres.cerrarLocal();
        } catch (Exception e) {
            Utilidades.log.info("-> " + e.getLocalizedMessage());
        }
        return mapeo;
    }

    public static long recuperarId() {
        long idManejo = 0l;
        ConexionPostgres.conectarLocal();
        String sql = "SELECT * FROM ande.manejo_local";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                idManejo = rs.getLong("id_manejo");
            }
            ps.close();
        } catch (Exception e) {
            Utilidades.log.info("-> " + e.getLocalizedMessage());
        }
        ConexionPostgres.cerrarLocal();
        return idManejo;
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void validandoIngreso() {
        CryptoFront cf = new CryptoFront();
        CryptoBack cb = new CryptoBack();
        JSONParser parsers = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (!textFieldUsuario.getText().contentEquals("") || !passwordFieldContra.getText().contentEquals("")) {
            if (users == null || users.toString().contentEquals("{}")) {
                validandoPrimerIngreso();
            } else {
                JSONObject usuObj = null;
                try {
                    usuObj = (JSONObject) parsers.parse(users.get("usuario").toString());
                } catch (ParseException ex) {
                    usuObj = null;
                }
                if (usuObj == null) {
                    validandoPrimerIngreso();
                } else {
                    Identity identityLocal = new Identity();
                    identityLocal.usuarioLogueado(null);
                    identityLocal.usuarioLogueado(usuObj);
                    if (usuObj.get("nomUsuario").toString().equalsIgnoreCase(textFieldUsuario.getText())
                            && usuObj.get("contrasenha").toString().equalsIgnoreCase(cb.getHash(cf.getHash(passwordFieldContra.getText())))) {
                        Toaster.setTray("¡HAZ VUELTO " + Identity.onlyUser().get("nomUsuario").toString().toUpperCase() + " A LA GALOPA!\n¡LAS COSAS MÁS LINDAS AL MEJOR PRECIO!", "N", "S", 10000, "");
//                        this.sc.loadScreen("/vista/login/menuFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
                        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
//                        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
                    } else {
                        if (!jsonDatos.isNull("modSup")) {
                            Supervisor sup = superDAO.buscarSupervisor(textFieldUsuario.getText(), cf.getHash(passwordFieldContra.getText()));
                            try {
                                if (sup != null) {
                                    SupervisorDTO supDTO = sup.toSupervisorDTO();
                                    UsuarioDTO usuDTO = usuDAO.getById(supDTO.getUsuario().getIdUsuario()).toUsuarioDTO();
                                    usuDTO.setFechaAlta(null);
                                    usuDTO.setFechaMod(null);
                                    JSONObject jsonUsuario = (JSONObject) parsers.parse(gson.toJson(usuDTO));
                                    identityLocal.usuarioLogueado(null);
                                    identityLocal.usuarioLogueado(jsonUsuario);

                                    DatosEnCaja.setDatos(datos);
                                    Toaster.setTray("¡BIENVENIDO " + Identity.onlyUser().get("nomUsuario").toString().toUpperCase() + " A LA GALOPA!\n¡LAS COSAS MÁS LINDAS AL MEJOR PRECIO!", "N", "S", 10000, "");
                                    if ((Boolean) Identity.getUsuario().get("generico")) {
                                        this.sc.loadScreen("/vista/util/ModificarPassFXML.fxml", 377, 160, "/vista/login/LoginFXML.fxml", 599, 245, false);
                                    } else {
//                                        this.sc.loadScreen("/vista/login/menuFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
                                        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
//                                        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
                                    }
                                } else {
                                    mensajeInfo("EXISTE UN USUARIO QUE AUN NO HA CERRADO ESTA CAJA");
                                    LoginFXMLController.setLlamarTask(false);
                                }
                            } catch (ParseException ex) {
                                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                            }
                        } else {
                            mensajeInfo("EXISTE UN USUARIO QUE AUN NO HA CERRADO ESTA CAJA");
                            LoginFXMLController.setLlamarTask(false);
                        }
//                        this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/LoginFXML.fxml", 599, 245, true);
                    }
                    actualizarDatos();
                }
            }
        } else {
            LoginFXMLController.setLlamarTask(false);
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/LoginFXML.fxml", 599, 245, true);
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (textFieldUsuario.isFocused()) {
                passwordFieldContra.requestFocus();
            } else if (passwordFieldContra.isFocused()) {
                if (textFieldUsuario.getText().isEmpty()) {
                    textFieldUsuario.requestFocus();
                } else if (this.enter) {
                    validandoIngreso();
                } else {
                    this.enter = true;
                }
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            saliendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void mensajeInfo(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ok);
        this.enter = false;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private void mensajeError(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    private void cargandoImagen() {
        this.image = new Image(getClass().getResourceAsStream("/vista/img/logosiv.png"));
        this.imageViewLogoCP.setImage(this.image);
        this.imageViewLogoCP = (ImageView) AnimationFX.rotationNodePlay(this.imageViewLogoCP, 1.5, false);
    }

    private void cargandoIco() {
//        File fileIco = new File(PATH.PATH_ICON);
//        Image imageIco = new Image(fileIco.toURI().toString());
        Image imageIco = new Image(getClass().getResourceAsStream("/vista/img/paranaPopUp.png"));
        this.sc.getStage().getIcons().add(imageIco);
    }

    private void actualizarDatos() {
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }
        if (DatosEnCaja.getFacturados() != null) {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        } else {
            manejo.setFactura(null);
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
    }

    private void saliendo() {
        if (SubAppRun.p != null) {
            if (SubAppRun.p.isAlive()) {
                SubAppRun.p.destroyForcibly();
                logger.info("Kill Sub-app");
            } else {
                logger.info("Sub-app notAlive");
            }
        } else {
            logger.info("Sub-app null");
        }
        bajarArranque();
        ClosePort.go();
        Platform.exit();
        System.exit(0);
    }

    private void validandoPrimerIngreso() {
        CryptoFront cf = new CryptoFront();
        JSONObject user = jsonAcceso(textFieldUsuario.getText(), cf.getHash(passwordFieldContra.getText()));
        if (user != null) {
            Identity identityLocal = new Identity();
            identityLocal.usuarioLogueado(null);
            identityLocal.usuarioLogueado(user);
//            datos.put("rendicion", true);
            datos.put("estadoInformeFinanciero", false);
            datos.put("inicio", true);
            DatosEnCaja.setDatos(datos);
            Toaster.setTray("¡BIENVENIDO " + Identity.onlyUser().get("nomUsuario").toString().toUpperCase() + " A LA GALOPA!\n¡LAS COSAS MÁS LINDAS AL MEJOR PRECIO!", "N", "S", 10000, "");
            if ((Boolean) Identity.getUsuario().get("generico")) {
                this.sc.loadScreen("/vista/util/ModificarPassFXML.fxml", 377, 160, "/vista/login/LoginFXML.fxml", 599, 245, false);
            } else {
//                this.sc.loadScreen("/vista/login/menuFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
                this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
//                this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, true);
            }
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/LoginFXML.fxml", 599, 245, true);
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, ACCESS -> GET
    private JSONObject jsonAcceso(String u, String c) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject user = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 20)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/usuario/auth/" + u + "/" + c + "");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    user = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            } else {
                user = loginLocal(u, c);
            }
        } catch (IOException | ParseException ex) {
            user = loginLocal(u, c);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return user;
    }
    //////READ, ACCESS -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    public JSONObject loginLocal(String u, String c) {
        JSONParser parser = new JSONParser();
        Usuario usu = new Usuario();
        try {
            usu = usuDAO.buscarUsuario(u, c);
            UsuarioDTO usuDTO = usu.toUsuarioDTO();
            return (JSONObject) parser.parse(gson.toJson(usuDTO));
        } catch (Exception ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return null;
        }
    }
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //*****************************ANIMACIONES**********************************
    private void animando() {
        labelAutenticacion = (Label) AnimationFX.translationNode(labelAutenticacion, 15, 0, 1300);
        textFieldUsuario = (JFXTextField) AnimationFX.translationNode(textFieldUsuario, 60, 0, 1100);
        passwordFieldContra = (JFXPasswordField) AnimationFX.translationNode(passwordFieldContra, -60, 0, 1100);
        buttonIngresar = (JFXButton) AnimationFX.translationNode(buttonIngresar, 30, 30, 900);
        buttonCerrar = (JFXButton) AnimationFX.translationNode(buttonCerrar, -30, 30, 900);
    }
    //***************************** ANIMACIONES **********************************

    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->
    public Task createWorker() {
        return new Task() {
            @Override
            protected Object call() throws InterruptedException {
//                int i = 1;
                int size = 10;
                Platform.runLater(() -> {
                    viendoProgress();
                });
                for (int j = 0; j < size; j++) {
                    TimeUnit.SECONDS.sleep(2);
                    updateProgress((j + 1), size);
                    if (j == 0) {
                        Platform.runLater(() -> {
                            (new Thread(new LoginFXMLController())).start();
//                            SubAppRun sar = new SubAppRun();
//                            Utilidades.log.info("Iniciando Sub-App");
//                            sar.run();
//                            if (!SubAppRun.p.isAlive()) {
//                                mensajeError("INCONVENIENTES TÉCNICOS DEL TIPO SA.");
//                                Utilidades.closeSystem();
//                            } 

//else {
//                                new EMF();
//                                if (EMF.isEmfOk()) {
//                                    new PATH();
//                                } else {
//                                    mensajeInfo("INCONVENIENTES TÉCNICOS DEL TIPO ORM.");
//                                    Utilidades.closeSystem();
//                                }
//                            }
                        });
                    }
//                    else {
//                        i++;
//                    }
                }
                Platform.runLater(() -> {
                    ocultandoProgress();
                    cargandoIco();
                    cargandoImagen();
                    buttonIngresar.requestFocus();
                    animando();
                    textFieldUsuario.requestFocus();
                });
                return true;
            }
        };
    }
    //PROGRESS TASK ********************************* -> -> -> -> -> -> -> -> ->

    public void viendoProgress() {
        materialDesignIconViewUser.setEffect(new BoxBlur(3, 3, 3));
        fontKey.setEffect(new BoxBlur(3, 3, 3));
        passwordFieldContra.setEffect(new BoxBlur(3, 3, 3));
        passwordFieldContra.setDisable(true);
        textFieldUsuario.setEffect(new BoxBlur(3, 3, 3));
        textFieldUsuario.setDisable(true);
        buttonIngresar.setEffect(new BoxBlur(3, 3, 3));
        buttonIngresar.setDisable(true);
        buttonCerrar.setEffect(new BoxBlur(3, 3, 3));
        buttonCerrar.setDisable(true);
        imageViewLogoCP.setEffect(new BoxBlur(3, 3, 3));
        imageViewLogoCP.setDisable(true);
        progressIndicator.setVisible(true);
    }

    public void ocultandoProgress() {
        materialDesignIconViewUser.setEffect(null);
        fontKey.setEffect(null);
        passwordFieldContra.setEffect(null);
        passwordFieldContra.setDisable(false);
        textFieldUsuario.setEffect(null);
        textFieldUsuario.setDisable(false);
        buttonIngresar.setEffect(null);
        buttonIngresar.setDisable(false);
        buttonCerrar.setEffect(null);
        buttonCerrar.setDisable(false);
        imageViewLogoCP.setEffect(null);
        imageViewLogoCP.setDisable(false);
        progressIndicator.setVisible(false);
    }

    @Override
    @SuppressWarnings({"CallToThreadRun", "ResultOfObjectAllocationIgnored"})
    public void run() {
        if (ConexionPostgres.conectarLocal()) {
            ipLocal();
            if (Utilidades.ipLocal.isEmpty()) {
                Utilidades.log.info("->> INCONVENIENTES TÉCNICOS DEL TIPO [IP]");
                ConexionPostgres.cerrarLocal();
                Utilidades.closeSystem();
            } else {
                //DESCOMENTAR PARA LLAMAR A LA SUB APP
//                ConexionPostgres.cerrarLocal();
//                SubAppRun sar = new SubAppRun();
//                Utilidades.log.info("Iniciando Sub-App");
//                sar.run();
//                if (!SubAppRun.p.isAlive()) {
//                    Utilidades.log.info("->> INCONVENIENTES TÉCNICOS DEL TIPO [SA]");
//                    Utilidades.closeSystem();
//                } else {
                new EMF();
                new EMF2();
                if (EMF.isEmfOk()) {
                    new PATH();
                } else {
                    Utilidades.log.info("-->> INCONVENIENTES TÉCNICOS DEL TIPO [ORM]");
                    Utilidades.closeSystem();
                }
            }
        }
    }

//        else {
//            Utilidades.log.info("->> INCONVENIENTES TÉCNICOS DEL TIPO [JDBC - IP]");
//        Utilidades.closeSystem();
//    }
//}
    private void bajarArranque() {
        VerificandoInstancias vi = new VerificandoInstancias();
        vi.onFinish();
    }

    //CONEXIÓN LOCAL IP
    private void ipLocal() {
        String sql = "SELECT ip_boca\n"
                + "FROM \"general\".ip_boca limit 1;";
        Utilidades.ipLocal = "";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                Utilidades.ipLocal = rs.getString("ip_boca");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR JDBC - IP", ex.fillInStackTrace());
        } finally {
        }
    }
    //CONEXIÓN LOCAL IP

}
