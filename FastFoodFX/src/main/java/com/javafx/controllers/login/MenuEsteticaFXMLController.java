package com.javafx.controllers.login;

import com.peluqueria.MainApp;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.util.Utilidades;
import com.jfoenix.controls.JFXButton;

@Controller
public class MenuEsteticaFXMLController extends BaseScreenController implements Initializable {

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button buttonCerrarSesion;
    @FXML
    private TabPane tabPaneSeleccionMenu;
//    private Tab tabCaja;
    @FXML
    private Tab tabSeguridad;
    @FXML
    private Tab tabCentroEstetica;
    @FXML
    private TitledPane titledPaneCentroEstetica;
    @FXML
    private AnchorPane anchorPaneCentroEstetica;
    @FXML
    private Button btnCajeroEstetica;
    @FXML
    private Button btnSupervisorEstetica;
    @FXML
    private GridPane gridPaneEstetica;
//    private Tab tabReporteria;
    @FXML
    private Tab tabReporteriaVenta;
    @FXML
    private TitledPane titledPaneCentroEstetica1;
    @FXML
    private AnchorPane anchorPaneCentroEstetica1;
    @FXML
    private GridPane gridPaneEstetica1;
    @FXML
    private JFXButton buttonUsuario;
    @FXML
    private JFXButton buttonRoles;
    @FXML
    private Tab tabStock;
    @FXML
    private TitledPane titledPaneCentroEstetica11;
    @FXML
    private AnchorPane anchorPaneCentroEstetica11;
    @FXML
    private GridPane gridPaneEstetica11;
    @FXML
    private JFXButton btnCargarArticulo;
    @FXML
    private JFXButton btnCargarProveedor;
    @FXML
    private JFXButton btnCargarSecciones;
    @FXML
    private TitledPane titledPaneCentroEstetica111;
    @FXML
    private AnchorPane anchorPaneCentroEstetica111;
    @FXML
    private GridPane gridPaneEstetica111;
    @FXML
    private JFXButton btnVentaDiaria;
    @FXML
    private JFXButton btnVentaDetalle;
    @FXML
    private JFXButton btnFacturaDetalle;
    @FXML
    private JFXButton btnVentaFormaPago;
    @FXML
    private JFXButton btnVentaPorTarjeta;
    @FXML
    private JFXButton btnRankingVentas;
    @FXML
    private JFXButton btnStockProducto;
    @FXML
    private JFXButton buttonConsulta;
    @FXML
    private JFXButton btnCargarCompras;
    @FXML
    private Tab tabReporteriaCompras;
    @FXML
    private TitledPane titledPaneCentroEstetica1111;
    @FXML
    private AnchorPane anchorPaneCentroEstetica1111;
    @FXML
    private GridPane gridPaneEstetica1111;
    @FXML
    private JFXButton btnDetalleGastos;
    @FXML
    private JFXButton btnResumenMes;
    @FXML
    private JFXButton btnCargarPreparar;
    @FXML
    private JFXButton btnGastoPreparacion;
    @FXML
    private JFXButton btnGastosVenta;
    @FXML
    private JFXButton btnListadoPreparados;
    @FXML
    private JFXButton btnPedidos;
    @FXML
    private JFXButton btnConfiguracion;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void btnCajeroAction(ActionEvent event) {
        actividadCajero();
    }

    private void btnSupervisorAction(ActionEvent event) {
        actividadSupervisor();
    }

    @FXML
    private void buttonCerrarSesionAction(ActionEvent event) {
        cerrandoSesion();
    }

    private void buttonConfigAction(ActionEvent event) {
        configuracionDes();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        consultandoFactura();
    }

    @FXML
    private void buttonUsuarioAction(ActionEvent event) {
        gestionandoUsuario();
    }

    @FXML
    private void buttonRolesAction(ActionEvent event) {
        gestionandoRoles();
    }

    private void btnInformeFielAction(ActionEvent event) {
        actividadReporteriaClienteFiel();
    }

    private void btnInformeFuncionarioAction(ActionEvent event) {
        actividadReporteriaFuncionario();
    }

    private void buttonCuponeraAction(ActionEvent event) {
        configuracionCuponera();
    }

    private void btnInformeVentaAction(ActionEvent event) {
        actividadReporteriaVentas();
    }

    @FXML
    private void btnCajeroEsteticaAction(ActionEvent event) {
        actividadCajeroEstetica();
    }

    @FXML
    private void btnSupervisorEsteticaAction(ActionEvent event) {
        actividadSupervisorEstetica();
    }

    private void btnInformeTarjetaAction(ActionEvent event) {
        actividadReporteriaTarjetaCaja();
    }

    private void btnInformePromoArtAction(ActionEvent event) {
        actividadReporteriaPromoTempArt();
    }

    private void btnInformeDetalleVentaAction(ActionEvent event) {
        actividadReporteriaDetalleVenta();
    }

    private void btnCajeroMayAction(ActionEvent event) {
        actividadCajeroMay();
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        visibilidadTABs();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void actividadCajero() {
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//        if ("login_cajero")) {
        JSONObject jsonCaja = DatosEnCaja.getDatos();
        if (jsonDatos.isNull("rendicion")) {
            this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
        } else {
            try {
                if (!Boolean.parseBoolean(jsonCaja.get("rendicion").toString()) && manejoDAO.recuperarId() == 0l) {
                    mensajeAlerta("Debe generar primeramente el Informe Financiero para finalizar el cierre de turno.");
                } else {
                    this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
                }
            } catch (Exception e) {
                this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
            }
        }
//            else if (!Boolean.parseBoolean(jsonCaja.get("rendicion").toString()) && manejoDAO.recuperarId() == 0l) {
//                mensajeAlerta("Debe generar primeramente el Informe Financiero para finalizar el cierre de turno.");
//            } else {
//                this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
//        }
    }

    private void actividadSupervisor() {
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        estado = fact == null || fact.isEmpty();
        if (!jsonDatos.isNull("modSup")) {
            estado = true;
        }

//        if (fact == null || fact.isEmpty()) {
        if (estado) {
//            if ("login_supervisor")) {
            this.sc.loadScreen("/vista/caja/loginSupervisorFXML.fxml", 540, 312, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//            }
        } else {
            mensajeAlerta("Una factura aún no ha sido procesada en su totalidad.");
        }
    }

    private void configuracionDes() {
//        if ("configuracion_descuento")) {
        Utilidades.setIdRangoLocal(1000l);
        this.sc.loadScreen("/vista/descuento/MenuConfiguracionFXML.fxml", 837, 409, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void cerrandoSesion() {
        Identity identity = new Identity();
        identity.usuarioLogueado(null);
        LoginFXMLController.setLlamarTask(false);
        this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/menuEsteticaFXML", 540, 359, true);
    }

    private void consultandoFactura() {
        if (Identity.rolIdentity("consultar_factura")) {
            this.sc.loadScreen("/vista/caja/ReimpresionFXML.fxml", 1020, 580, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void gestionandoUsuario() {
        if (Identity.rolIdentity("gestionar_usuario")) {
            this.sc.loadScreen("/vista/seguridad/UsuarioFX.fxml", 781, 615, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void gestionandoRoles() {
        if (Identity.rolIdentity("gestionar_rol")) {
            this.sc.loadScreen("/vista/seguridad/RolFX.fxml", 394, 575, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void configuracionCuponera() {
//        if ("configuracion_descuento")) {//falta aún...
        Utilidades.setIdRangoLocal(999l);
        this.sc.loadScreen("/vista/cuponera/CuponeraConfigFXML.fxml", 726, 560, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void consultandoFacturaMay() {
//        if ("consulta_factura")) {
        this.sc.loadScreen("/vista/caja/ReimpresionFXML.fxml", 1020, 580, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadCajeroMay() {
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//        if ("login_cajero")) {
        JSONObject jsonCaja = DatosEnCaja.getDatos();
        if (jsonDatos.isNull("rendicion")) {
            this.sc.loadScreen("/vista/cajamay/loginCajeroMayFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
        } else {
            try {
                if (!Boolean.parseBoolean(jsonCaja.get("rendicion").toString()) && manejoDAO.recuperarId() == 0l) {
                    mensajeAlerta("Debe generar primeramente el Informe Financiero para finalizar el cierre de turno.");
                } else {
                    this.sc.loadScreen("/vista/cajamay/loginCajeroMayFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
                }
            } catch (Exception e) {
                this.sc.loadScreen("/vista/cajamay/loginCajeroMayFXML.fxml", 545, 317, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
            }
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 600, 400, true);
//        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (tabSeguridad.isSelected()) {
                gestionandoUsuario();
            } else if (tabCentroEstetica.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadCajeroEstetica();
            } else if (tabStock.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadCargaArticulo();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadVentaDiaria();
            } else if (tabReporteriaCompras.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadDetalleGasto();
            }
//else if (tabReporteria.isSelected()) {
//                actividadReporteriaVentas();
//            } else if (tabReporteriaVenta.isSelected()) {
//                actividadReporteVentasDetalle();
//            }
            /*else if (tabCuponera.isSelected()) {
                configuracionCuponera();
            } else if (tabCajaMay.isSelected()) {
                actividadCajeroMay();
            }*/
        }
        if (keyCode == event.getCode().F2) {
            if (tabSeguridad.isSelected()) {
                gestionandoRoles();
//            } else if (tabCaja.isSelected()) {
//                actividadSupervisor();
            } else if (tabCentroEstetica.isSelected()) {
                actividadSupervisorEstetica();
            } else if (tabStock.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadCargaProveedor();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadVentaDetalle();
            } else if (tabReporteriaCompras.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                detalleGastoVenta();
            }
//else if (tabReporteria.isSelected()) {
//                actividadReporteriaDetalleVenta();
//            } else if (tabReporteriaVenta.isSelected()) {
//                actividadReporteVentasParana();
//            }
        }
        if (keyCode == event.getCode().F3) {
            if (tabReporteriaVenta.isSelected()) {
                actividadReporteVentasTarjeta();
            } else if (tabStock.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadCargarSecciones();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadFacturaDetalle();
            } else if (tabCentroEstetica.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                consultandoFactura();
            } else if (tabReporteriaCompras.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                detalleGastoPreparacion();
            }
        }
        if (keyCode == event.getCode().F4) {
            if (tabReporteriaVenta.isSelected()) {
                actividadReporteArticuloTarjeta();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadVentaFormaPago();
            } else if (tabStock.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadCargaCompras();
            } else if (tabReporteriaCompras.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                listadoPreparacion();
            } else if (tabCentroEstetica.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadConfiguracionEmpresa();
            }
            /*else if (tabCajaMay.isSelected()) {
                consultandoFacturaMay();
            }*/
        }

        if (keyCode == event.getCode().F5) {
            if (tabReporteriaVenta.isSelected()) {
                actividadReportePromociones();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadVentaPorTarjeta();
            } else if (tabStock.isSelected()) {
                actividadCargarPreparar();
            } else if (tabReporteriaCompras.isSelected()) {
                actividadResumenMes();
            }
        }
        if (keyCode == event.getCode().F6) {
            if (tabReporteriaVenta.isSelected()) {
                actividadReportePromociones();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadRankingVentas();
            }else if (tabReporteriaCompras.isSelected()) {
                actividadFacturaPendiente();
            }
        }
        if (keyCode == event.getCode().F7) {
            if (tabReporteriaVenta.isSelected()) {
                actividadReportePromociones();
            } else if (tabReporteriaVenta.isSelected()) {
//                actividadCajero();
//            } else if (tabCentroEstetica.isSelected()) {
                actividadStockProducto();
            }
        }
//        if (keyCode == event.getCode().F6) {
//            if (tabReporteria.isSelected()) {
//                actividadReporteriaDetalleVenta();
//            }
//        }
        if (keyCode == event.getCode().ESCAPE) {
//            setearDatos();
            cerrandoSesion();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void actividadCajeroEstetica() {
        if (Identity.rolIdentity("ingresar_caja")) {
            this.sc.loadScreen("/vista/estetica/LoginCajeroEsteticaFXML.fxml", 545, 318, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
//        if ("login_cajero_estetica")) {
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteriaVentas() {
        MainApp.adaptandoAnchor();
        if (Identity.rolIdentity("login_cajero_estetica")) {
            this.sc.loadScreen("/vista/reporteria/InformeVentaFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadReporteriaClienteFiel() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteria/InformeVentaFielFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteriaFuncionario() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteria/InformeVentaFuncionarioFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteriaPromoTempArt() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteria/InformeVentaPromoTempArtFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteriaTarjetaCaja() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteria/InformeVentaTarjetaFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteVentasDetalle() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteVenta/ReporteVentaFXML.fxml", 749, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteArticuloTarjeta() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteVenta/ReporteArticuloTarjetaFXML.fxml", 749, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteriaDetalleVenta() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteria/InformeDetalleVentaFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteVentasParana() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReporteVentasTarjeta() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteVenta/ReporteVentaTarjetasFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadReportePromociones() {
        MainApp.adaptandoAnchor();
//        if ("login_cajero_estetica")) {
        this.sc.loadScreen("/vista/reporteVenta/ReportePromocionesFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    private void actividadSupervisorEstetica() {
        if (fact == null || fact.isEmpty()) {
            if (Identity.rolIdentity("trabajo_supervisor")) {
                this.sc.loadScreen("/vista/estetica/LoginSupervisorEsteticaFXML.fxml", 543, 318, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
            } else {
                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
            }
        } else {
            mensajeAlerta("Una factura aún no ha sido procesada en su totalidad.");
        }
    }

    private void btnbtnReporteVentaAction(ActionEvent event) {
        actividadReporteVentasDetalle();
    }

    private void btnReporteVentaParanaAction(ActionEvent event) {
        actividadReporteVentasParana();
    }

    private void btnReporteVentaTarjetaAction(ActionEvent event) {
        actividadReporteVentasTarjeta();
    }

    private void btnReporteVentaArticuloTarjetaAction(ActionEvent event) {
        actividadReporteArticuloTarjeta();
    }

    private void btnReporteVentaPromocion(ActionEvent event) {
        actividadReportePromociones();
    }

    private void visibilidadTABs() {
//        if (!"tab_report_desc")) {
//        tabReporteria.setDisable(true);
//        }
//        if (!"tab_report_venta")) {
//        tabReporteriaVenta.setDisable(true);
//        }
//        if (!"rol_view") && !"user_view")) {
//        tabSeguridad.setDisable(true);
//        }
//        if (!"apertura_caja_estetica") && !"modulo_supervisor")) {
//        tabCentroEstetica.setDisable(true);
//        }
//        if (!"tab_caja_may")) {
//        tabCajaMay.setDisable(true);
//        tabCaja.setDisable(true);
//        }
    }

    private void setearDatos() {
        //GLOBAL
        datos.put("montoFacturado", 0);
        datos.put("donaciones", 0);
        datos.put("retiroDinero", 0);
        //POR CAJERO LOGUEADO
        datos.put("retiroDineroCajero", 0);
        datos.put("montoCajero", 0);
        datos.put("donacionCajero", 0);
        //
        datos.put("zeta", 0);
        datos.put("facturaInicial", "");
        datos.put("facturaFinal", "");
        datos.put("estadoFacturaInicial", false);
        datos.put("contDesc", 0);
        datos.put("contTarj", 0);
        datos.put("sumDesc", 0);
        datos.put("sumTarj", 0);
        datos.put("cantEfectivoRecibido", 0);
        //
        datos.put("contEfectivo", 0);
        datos.put("sumEfectivo", 0);
        datos.put("totales", 0);
        datos.put("nClientes", 0);
        datos.put("nFuncionarios", 0);
        datos.put("nArticulos", 0);
        //
        datos.put("gra5", 0);
        datos.put("gra10", 0);
        datos.put("exe", 0);
        datos.put("gra", 0);
        datos.put("contDonaciones", 0);
        datos.put("sumDonaciones", 0);
        //
        datos.put("contNotaCred", 0);
        datos.put("sumNotaCred", 0);
        datos.put("contFact", 0);
        datos.put("sumFact", 0);
        datos.put("contCheque", 0);
        datos.put("sumCheque", 0);
        datos.put("contVale", 0);
        datos.put("sumVale", 0);
        datos.put("contAsoc", 0);
        datos.put("sumAsoc", 0);
        datos.put("vuelto", 0);
        //
        datos.put("contDolar", 0);
        datos.put("contReal", 0);
        datos.put("contPeso", 0);
        datos.put("contRetencion", 0);
        datos.put("sumDolar", 0);
        datos.put("sumReal", 0);
        datos.put("sumPeso", 0);
        datos.put("sumReal", 0);
        datos.put("sumRetencion", 0);
        datos.put("sumEfeRecibido", 0);
        datos.remove("codSup");
        datos.remove("fecha_arqueo");
        datos.put("caja", null);
        actualizarDatos();
    }

    private void actualizarDatos() {
        DatosEnCaja.setDatos(null);
        DatosEnCaja.setUsers(null);
        DatosEnCaja.setFacturados(null);
        datos = new JSONObject();
        users = new JSONObject();
        fact = new JSONObject();
        long idManejo = manejoDAO.recuperarId();
        boolean valor = manejoDAO.eliminarObtenerEstado(idManejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
    }

    @FXML
    private void btnCargarArticuloAction(ActionEvent event) {
        actividadCargaArticulo();
    }

    @FXML
    private void btnCargarProveedorAction(ActionEvent event) {
        actividadCargaProveedor();
    }

    @FXML
    private void btnCargarSeccionesAction(ActionEvent event) {
        actividadCargarSecciones();
    }

    private void actividadCargarSecciones() {
        if (Identity.rolIdentity("cargar_seccion")) {
            this.sc.loadScreen("/vista/stock/SeccionesFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadCargaProveedor() {
        if (Identity.rolIdentity("cargar_proveedor")) {
            this.sc.loadScreen("/vista/stock/proveedoresFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadCargaArticulo() {
        if (Identity.rolIdentity("cargar_articulo")) {
            this.sc.loadScreen("/vista/stock/ArticulosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }

    }

    @FXML
    private void btnVentaDiariaAction(ActionEvent event) {
        actividadVentaDiaria();
    }

    @FXML
    private void btnVentaDetalleAction(ActionEvent event) {
        actividadVentaDetalle();
    }

    @FXML
    private void btnFacturaDetalleAction(ActionEvent event) {
        actividadFacturaDetalle();
    }

    @FXML
    private void btnVentaFormaPagoAction(ActionEvent event) {
        actividadVentaFormaPago();
    }

    @FXML
    private void btnVentaPorTarjetaAction(ActionEvent event) {
        actividadVentaPorTarjeta();
    }

    @FXML
    private void btnRankingVentasAction(ActionEvent event) {
        actividadRankingVentas();
    }

    @FXML
    private void btnStockProductoAction(ActionEvent event) {
        actividadStockProducto();
    }

    private void actividadVentaDiaria() {
        if (Identity.rolIdentity("venta_diaria")) {
            this.sc.loadScreen("/vista/reporteria/InformeVentaFXML.fxml", 711, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadVentaDetalle() {
        if (Identity.rolIdentity("venta_detalle_diaria")) {
            this.sc.loadScreen("/vista/reporteria/InformeDetalleVentaFXML.fxml", 711, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadFacturaDetalle() {
        if (Identity.rolIdentity("num_factura_detalle")) {
            this.sc.loadScreen("/vista/reporteVenta/ReporteVentaFXML.fxml", 749, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadVentaFormaPago() {
        if (Identity.rolIdentity("venta_forma_pago")) {
            this.sc.loadScreen("/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadVentaPorTarjeta() {
        if (Identity.rolIdentity("venta_tarjeta")) {
            this.sc.loadScreen("/vista/reporteVenta/ReporteVentaTarjetasFXML.fxml", 749, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadRankingVentas() {
        if (Identity.rolIdentity("ranking_prod")) {
            this.sc.loadScreen("/vista/reporteVenta/RankingProductoFXML.fxml", 749, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadStockProducto() {
        if (Identity.rolIdentity("stock_prod")) {
            this.sc.loadScreen("/vista/reporteVenta/StockProductoFXML.fxml", 749, 275, "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadCargaCompras() {
        if (Identity.rolIdentity("cargar_compra")) {
            this.sc.loadScreen("/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    @FXML
    private void btnCargarComprasAction(ActionEvent event) {
        actividadCargaCompras();
    }

    @FXML
    private void btnDetalleGastosAction(ActionEvent event) {
        actividadDetalleGasto();
    }

    @FXML
    private void btnResumenMesAction(ActionEvent event) {
        actividadResumenMes();
    }

    private void actividadDetalleGasto() {
        if (Identity.rolIdentity("detalle_gasto")) {
            this.sc.loadScreen("/vista/reporteria/InformeGastosFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void actividadResumenMes() {
        if (Identity.rolIdentity("resumen_mes")) {
            this.sc.loadScreen("/vista/reporteria/ResumenMensualFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    @FXML
    private void btnCargarPrepararAction(ActionEvent event) {
        actividadCargarPreparar();
    }

    private void actividadCargarPreparar() {
        if (Identity.rolIdentity("resumen_mes")) {
            this.sc.loadScreen("/vista/stock/PreparacionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    @FXML
    private void btnGastoPreparacionAction(ActionEvent event) {
        detalleGastoPreparacion();
    }

    @FXML
    private void btnGastosVentaAction(ActionEvent event) {
        detalleGastoVenta();
    }

    private void detalleGastoVenta() {
        if (Identity.rolIdentity("detalle_gasto")) {
            this.sc.loadScreen("/vista/reporteria/InformeGastosVentaFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void detalleGastoPreparacion() {
        if (Identity.rolIdentity("detalle_gasto")) {
            this.sc.loadScreen("/vista/reporteria/InformeGastosPreparacionFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    private void listadoPreparacion() {
        if (Identity.rolIdentity("detalle_gasto")) {
            this.sc.loadScreen("/vista/reporteria/ListadoPreparacionFXML.fxml", 711, 275, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    @FXML
    private void btnListadoPreparadosAction(ActionEvent event) {
        listadoPreparacion();
    }

    @FXML
    private void btnPedidosAction(ActionEvent event) {
        actividadFacturaPendiente();
    }

    private void actividadFacturaPendiente() {
        if (Identity.rolIdentity("detalle_gasto")) {
            this.sc.loadScreen("/vista/reporteria/InformePedidoFXML.fxml", 632, 210, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        } else {
            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
        }
    }

    @FXML
    private void btnConfiguracionAction(ActionEvent event) {
        actividadConfiguracionEmpresa();
    }

    private void actividadConfiguracionEmpresa() {
        if (fact == null || fact.isEmpty()) {
            if (Identity.rolIdentity("trabajo_supervisor")) {
                this.sc.loadScreen("/vista/caja/ConfiguracionEmpresaFXML.fxml", 748, 437, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
            } else {
                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
            }
        } else {
            mensajeAlerta("Una factura aún no ha sido procesada en su totalidad.");
        }
    }
}
