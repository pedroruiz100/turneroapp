/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.cuponera;

import com.peluqueria.core.domain.Cuponera;
import com.peluqueria.dao.CuponeraDAO;
import com.peluqueria.dao.RangoDescFielCabDAO;
import com.peluqueria.dto.CuponeraDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;
import com.jfoenix.controls.JFXToggleButton;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class CuponeraConfigFXMLController extends BaseScreenController implements Initializable {

    private boolean patternMonto;
    private NumberValidator numValidator;
    IntegerValidator intValidator;
    String param;

    @Autowired
    private RangoDescFielCabDAO rangoImprovisadoDAO;
    @Autowired
    private CuponeraDAO cuponeraDAO;
    private boolean exitoCuponera;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ.z").create();
    private final String patternDateReadFecha = "dd/MMM/yyyy";

    //PAGINATION
    private List<JSONObject> cuponeraList;
    private JSONArray cuponeraJSONArray;
    private int filaIniCuponera, filaCantCuponera, filaLimitCuponera;
    private int pageCountCuponera, pageActualCuponera;
    private ObservableList<JSONObject> cuponeraData;
    private TableView<JSONObject> tableCuponera;
    private TableColumn<JSONObject, String> columnNombre;
    private TableColumn<JSONObject, String> columnMontoMin;
    private TableColumn<JSONObject, CheckBox> columnClienteFiel;
    private TableColumn<JSONObject, String> columnCuponeraFechaIni;
    private TableColumn<JSONObject, String> columnCuponeraFechaFin;
    private TableColumn<JSONObject, Boolean> columnEliminar;
    //PAGINATION

    private boolean exitoBaja;

    @FXML
    private StackPane stackPane;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private SplitPane splitPane;
    @FXML
    private AnchorPane anchorPaneHead;
    @FXML
    private Label labelCuponera;
    @FXML
    private AnchorPane anchorPaneBody;
    @FXML
    private GridPane gridPaneFecha;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private JFXDatePicker datePickerFechaInicio;
    @FXML
    private JFXDatePicker datePickerFechaFin;
    @FXML
    private JFXTextField jfxTextFieldMontoMin;
    @FXML
    private JFXToggleButton toggleButtonClienteFiel;
    @FXML
    private JFXTextField jfxTextFieldNomCuponera;
    @FXML
    private AnchorPane anchorPaneFoot;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonInsertar;
    @FXML
    private Pagination paginationCuponera;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void toggleButtonClienteFielAction(ActionEvent event) {
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonInsertarAction(ActionEvent event) {
        insertandoCuponera();
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        numValidator = new NumberValidator();
        intValidator = new IntegerValidator();
        listenerCampos();
        preparandoDatePicker();
        primeraPaginacion();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/cuponera/CuponeraConfigFXML.fxml", 726, 560, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        stackPane.setEffect(new BoxBlur(3, 3, 3));
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            stackPane.setEffect(null);
            alert2.close();
        }
    }

    private void mensajeError(String msj) {
        stackPane.setEffect(new BoxBlur(3, 3, 3));
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            stackPane.setEffect(null);
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            insertandoCuponera();
        } else if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }

    private void listenerCampos() {
        jfxTextFieldMontoMin.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            jfxTextFieldMontoMin.setText(param);
                            jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            jfxTextFieldMontoMin.setText("");
                            jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    jfxTextFieldMontoMin.setText(param);
                                    jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                jfxTextFieldMontoMin.setText("");
                                jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            jfxTextFieldMontoMin.setText(oldValue);
                            jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        jfxTextFieldMontoMin.setText(param);
                        jfxTextFieldMontoMin.positionCaret(jfxTextFieldMontoMin.getLength());
                    });
                }
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    public void insertandoCuponera() {
        if (jfxTextFieldNomCuponera.getText().isEmpty()) {
            mensajeAlerta("DEBE ASIGNAR UN NOMBRE A LA CUPONERA.");
        } else if (datePickerFechaInicio.getValue() == null) {
            mensajeAlerta("DEBE ASIGNAR UNA FECHA INICIO.");
        } else if (datePickerFechaFin.getValue() == null) {
            mensajeAlerta("DEBE ASIGNAR UNA FECHA FIN.");
        } else {
            if (verificandoCuponera()) {
                int montoMin = 0;
                if (!jfxTextFieldNomCuponera.getText().isEmpty()) {
                    montoMin = Integer.valueOf(numValidator.numberValidator(jfxTextFieldMontoMin.getText()));
                }
                procesandoInsercion(montoMin);
            } else {
                mensajeAlerta("YA EXISTE UNA CUPONERA.");
            }
        }
    }

    private void procesandoInsercion(int montoMin) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GUARDAR CUPONERA "
                + jfxTextFieldNomCuponera.getText().toUpperCase() + "?", ok, cancel);
        stackPane.setEffect(new BoxBlur(3, 3, 3));
        alert.showAndWait();
        stackPane.setEffect(null);
        if (alert.getResult() == ok) {
            if (creandoCuponera(montoMin)) {
                exitoCuponera = false;
                limpiarCampos();
                primeraPaginacion();
            }
        }
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setStyle("-fx-font-size: 12px; -fx-font-weight: bold;");
        datePickerFechaFin.setStyle("-fx-font-size: 12px; -fx-font-weight: bold;");
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                        long p = ChronoUnit.DAYS.between(
                                datePickerFechaInicio.getValue(), item
                        );
                        setTooltip(new Tooltip(
                                "Cuponera por " + p + " días.")
                        );
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void limpiarCampos() {
        jfxTextFieldNomCuponera.setText("");
        jfxTextFieldMontoMin.setText("");
        toggleButtonClienteFiel.setSelected(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        datePickerFechaFin.setValue(null);
    }

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantCuponera = 5;
        filaIniCuponera = 0;
        pageActualCuponera = 0;
        buscandoDescuentoTodo();
    }

    private void buscandoDescuentoTodo() {
        filaLimitCuponera = jsonRowCount();
        paginandoCabTarj();
    }

    private Integer jsonRowCount() {
        Integer rowCount = 0;
        rowCount = generarCountCuponera();
        return rowCount;
    }

    private void paginandoCabTarj() {
        paginationCuponera.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionCabTarj(pageIndex);
                //**************************************************************
            }
        });
        paginationCuponera.setCurrentPageIndex(this.pageActualCuponera);//index inicial paginación
        paginationCuponera.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountCuponera = filaLimitCuponera / filaCantCuponera;//cantidad index paginación
        if (filaLimitCuponera % filaCantCuponera > 0) {//index EXTRA (resto de filas)***********
            pageCountCuponera++;
        }
        paginationCuponera.setPageCount(pageCountCuponera);//cantidad index paginación SET***
    }

    private TableView crearPaginacionCabTarj(int pageIndex) {
        filaIniCuponera = pageIndex * filaCantCuponera;
        cuponeraList = jsonArrayCuponeraFetch(filaCantCuponera, filaIniCuponera);
        actualizandoTablaCabTarj();
        return tableCuponera;
    }

    private List<JSONObject> jsonArrayCuponeraFetch(int limRow, int offSet) {
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> cabFielJSONObjList = new ArrayList<>();
        cuponeraJSONArray = generarFetchDescuentoTarjeta(limRowS, offSetS);
        for (Object cabFielObj : cuponeraJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            cabFielJSONObjList.add(cabFielJSONObj);
        }
        return cabFielJSONObjList;
    }

    private void resetCuponeraBaja() {
        exitoBaja = false;
        limpiarCampos();
        primeraPaginacion();
    }

    //////READ, ROW COUNT CUPONERA
    private Integer generarCountCuponera() {
        return cuponeraDAO.rowCount().intValue();
    }
    //////READ, ROW COUNT CUPONERA

    //////READ, FETCH DESCUENTO TARJETA
    private JSONArray generarFetchDescuentoTarjeta(String limRowS, String offSetS) {
        JSONArray listaObj = new JSONArray();
        JSONParser parser = new JSONParser();
        for (Cuponera cuponera : cuponeraDAO.listarFETCH(Long.parseLong(limRowS), Long.parseLong(offSetS))) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(Cuponera.toCuponeraDTO(cuponera)));
                listaObj.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaObj;
    }
    //////READ, FETCH FILTRO DESCUENTO TARJETA CONVENIO

    //////CREATE, CUPONERA -> POST
    @SuppressWarnings("SleepWhileInLoop")
    private boolean creandoCuponera(int montoMin) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonCuponera = new JSONObject();
        try {
            jsonCuponera = creandoJsonCuponera(montoMin);
            if (jsonCuponera.isEmpty()) {
                return false;
            }
            CajaDatos.setIdCuponera(rangoImprovisadoDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal()));//999
            jsonCuponera.put("idCuponera", CajaDatos.getIdCuponera());
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Utilidades.verificarExistenciaPendientes() == 0) {
                URL url = new URL(Utilidades.ip + "/ServerParana/cuponera");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(jsonCuponera.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        jsonCuponera = (JSONObject) parser.parse(inputLine);
                        CajaDatos.setCuponera(true);
                        CajaDatos.setIdCuponera(Long.parseLong(jsonCuponera.get("idCuponera").toString()));
                        exitoCuponera = true;
                    }
                    br.close();
                } else {
                    jsonCuponera = registrarDescuentoTarjetaLocal(jsonCuponera);
                }
            } else {
                jsonCuponera = registrarDescuentoTarjetaLocal(jsonCuponera);
            }
        } catch (IOException | ParseException ex) {
            jsonCuponera = registrarDescuentoTarjetaLocal(jsonCuponera);
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (exitoCuponera) {
            // PARA LA PERSISTENCIA DE MANERA LOCAL PARANA LOCAL
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            long longInicio = Long.parseLong(jsonCuponera.get("fechaInicio").toString());
            long longFin = Long.parseLong(jsonCuponera.get("fechaFin").toString());
            jsonCuponera.put("fechaAlta", null);
            jsonCuponera.put("fechaMod", null);
            jsonCuponera.put("fechaInicio", null);
            jsonCuponera.put("fechaFin", null);
            Utilidades.log.info("-->> " + jsonCuponera.toString());
            CuponeraDTO cuponeraDTO = gson.fromJson(jsonCuponera.toString(), CuponeraDTO.class);
            cuponeraDTO.setFechaAlta(timestamp);
            cuponeraDTO.setFechaMod(timestamp);
            cuponeraDTO.setFechaInicio(new Timestamp(longInicio));
            cuponeraDTO.setFechaFin(new Timestamp(longFin));
            Cuponera valor = cuponeraDAO.insertarObtenerObjeto(Cuponera.fromCuponeraDTO(cuponeraDTO));
            if (valor != null) {
                Utilidades.log.info("DATOS PERSISTIDOS EN BD LOCAL CUPONERA");
            } else {
                Utilidades.log.info("DATOS NO...!! PERSISTIDOS EN BD LOCAL CUPONERA");
            }
        }
        CajaDatos.setCuponera(false);
        return exitoCuponera;
    }
    //////CREATE, CUPONERA -> POST

    //////INSERT, PENDIENTES - CUPONERA
    private JSONObject registrarDescuentoTarjetaLocal(JSONObject cuponera) {
        JSONObject obj = null;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "cuponera");
        j.put("data", cuponera.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'"
                + Utilidades.host + "','I','"
                + Utilidades.setToJson(j.toString()) + "')";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a la BD ********");
                obj = cuponera;
                exitoCuponera = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return obj;
    }
    //////INSERT, PENDIENTES - CUPONERA

    //////UPDATE, DESCUENTO TARJETA CAB. -> PUT
    private boolean bajaCuponera(JSONObject descTarjCab) {
        java.sql.Timestamp fechaAl = null;
        java.sql.Timestamp fechaIn = null;
        java.sql.Timestamp fechaF = null;
        if (descTarjCab.get("fechaAlta") != null) {
            fechaAl = Utilidades.objectToTimestamp(descTarjCab.get("fechaAlta"));
            descTarjCab.put("fechaAlta", fechaAl.getTime());
        }
        if (descTarjCab.get("fechaInicio") != null) {
            fechaIn = Utilidades.objectToTimestamp(descTarjCab.get("fechaInicio"));
            descTarjCab.put("fechaInicio", fechaIn.getTime());
        }
        if (descTarjCab.get("fechaFin") != null) {
            fechaF = Utilidades.objectToTimestamp(descTarjCab.get("fechaFin"));
            descTarjCab.put("fechaFin", fechaF.getTime());
        }
        exitoBaja = actualizarDescuentoTarjeta(descTarjCab);
        if (exitoBaja) {
            // ACTUALIZANDO DE MANERA LOCAL
            descTarjCab.put("fechaAlta", fechaAl);
            descTarjCab.put("fechaFin", fechaF);
            descTarjCab.put("fechaInicio", fechaIn);
            java.sql.Timestamp fechaAlta = null;
            java.sql.Timestamp fechaMod = null;
            java.sql.Timestamp fechaInicio = null;
            java.sql.Timestamp fechaFin = null;
            if (descTarjCab.get("fechaAlta") != null) {
                String fecha = descTarjCab.get("fechaAlta").toString().substring(0, 19);
                fechaAlta = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaAlta", null);
            }
            if (descTarjCab.get("fechaMod") != null) {
                fechaMod = new Timestamp(Long.parseLong(descTarjCab.get("fechaMod").toString()));
                descTarjCab.put("fechaMod", null);
            }
            if (descTarjCab.get("fechaInicio") != null) {
                String fecha = descTarjCab.get("fechaInicio").toString().substring(0, 19);
                fechaInicio = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaInicio", null);
            }
            if (descTarjCab.get("fechaFin") != null) {
                String fecha = descTarjCab.get("fechaFin").toString().substring(0, 19);
                fechaFin = Timestamp.valueOf(fecha);
                descTarjCab.put("fechaFin", null);
            }
            CuponeraDTO descuentoTarjetaCabDTO = gson.fromJson(descTarjCab.toString(), CuponeraDTO.class);
            descuentoTarjetaCabDTO.setFechaAlta(fechaAlta);
            descuentoTarjetaCabDTO.setFechaMod(fechaMod);
            descuentoTarjetaCabDTO.setFechaInicio(fechaInicio);
            descuentoTarjetaCabDTO.setFechaFin(fechaFin);
            descuentoTarjetaCabDTO.setActivo(false);
            if (cuponeraDAO.actualizarObtenerEstado(Cuponera.fromCuponeraDTO(descuentoTarjetaCabDTO))) {
                System.out.println("-->> DATOS PERSISTIDOS EN PARANA LOCAL <<-- ");
            } else {
                System.out.println("-->> DATOS NO PERSISTIDOS EN PARANA LOCAL <<-- ");
            }
            Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡DESCUENTO TARJETA CANCELADO!", ButtonType.OK);
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
                resetCuponeraBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            } else {
                alert2.close();
                resetCuponeraBaja();
                datePickerFechaInicio.setValue(LocalDate.now());
            }
        } else {
            mensajeError("NO SE CANCELÓ LA CUPONERA.");
        }
        return exitoBaja;
    }
    //////UPDATE, DESCUENTO TARJETA CAB. -> PUT

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREANDO DESCUENTO TARJETA CAB.
    private JSONObject creandoJsonCuponera(int montoMin) {
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject jsonCuponera = new JSONObject();
        //**********************************************************************
        jsonCuponera.put("nombre", jfxTextFieldNomCuponera.getText().toUpperCase());
        jsonCuponera.put("clienteFiel", toggleButtonClienteFiel.isSelected());
        jsonCuponera.put("activo", true);
        jsonCuponera.put("montoMin", montoMin);
        //************** Fecha Inicio / Fecha Fin
        LocalDate fechaInicioLD = datePickerFechaInicio.getValue();
        LocalDate fechaFinLD = datePickerFechaFin.getValue();
        Date dateIni = java.sql.Date.valueOf(fechaInicioLD);
        Timestamp timestampIni = new Timestamp(dateIni.getTime());
        Long timestampIniJSON = timestampIni.getTime();
        Date dateFin = java.sql.Date.valueOf(fechaFinLD);
        Timestamp timestampFin = new Timestamp(dateFin.getTime());
        Long timestampFinJSON = timestampFin.getTime();
        //************** Fecha Inicio / Fecha Fin / Hora
        jsonCuponera.put("fechaInicio", timestampIniJSON);
        jsonCuponera.put("fechaFin", timestampFinJSON);
        //**********************************************************************
        jsonCuponera.put("usuAlta", Identity.getNomFun());
        jsonCuponera.put("fechaAlta", timestampJSON);
        jsonCuponera.put("usuMod", Identity.getNomFun());
        jsonCuponera.put("fechaMod", timestampJSON);
        //**********************************************************************
        return jsonCuponera;
    }
    //JSON CREANDO DESCUENTO TARJETA CAB.

    //JSON BAJA CUPONERA
    private JSONObject bajaJsonCuponera(JSONObject cuponera) {
        //**********************************************************************
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        cuponera.put("usuMod", Identity.getNomFun());
        cuponera.put("fechaMod", timestampJSON);
        cuponera.put("activo", false);
        //**********************************************************************
        return cuponera;
    }
    //JSON BAJA CUPONERA
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB.
    private boolean actualizarDescuentoTarjeta(JSONObject descTarjCab) {
        boolean estado = false;
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        JSONObject j = new JSONObject();
        j.put("table", "cuponera");
        j.put("data", descTarjCab.toString());
        String sql = "INSERT INTO pendiente.descuento_pendientes(fecha_registro, ip, dml, msj) VALUES (now(),'"
                + Utilidades.host + "','U','" + Utilidades.setToJson(j.toString()) + "')";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a HANDLER PARANA BD ********");
                estado = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return estado;
    }
    //////UPDATE, PENDIENTES - DESCUENTO TARJETA CAB.

    //////READ, CUPONERA
    private boolean verificandoCuponera() {
        return cuponeraDAO.verificandoExistencia();
    }
    //////READ, CUPONERA

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void actualizandoTablaCabTarj() {
        tableCuponera = new TableView<>();
        cuponeraData = FXCollections.observableArrayList(cuponeraList);
        //columna Nombre .......................................................
        columnNombre = new TableColumn("Cuponera");
        columnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnNombre.setMinWidth(270);
        columnNombre.setEditable(false);
        columnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("nombre").toString());
            }
        });
        //columna Nombre .......................................................
        //columna Monto mín ....................................................
        columnMontoMin = new TableColumn("Monto mín.");
        columnMontoMin.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnMontoMin.setMinWidth(100);
        columnMontoMin.setEditable(false);
        columnMontoMin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("montoMin").toString())));
            }
        });
        //columna Monto mín ....................................................
        //columna Cliente fiel .................................................
        columnClienteFiel = new TableColumn("Cliente fiel");
        columnClienteFiel.setMinWidth(60);
        columnClienteFiel.setEditable(false);
        columnClienteFiel.setStyle("-fx-alignment: CENTER;");
        columnClienteFiel.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                CheckBox cb = new CheckBox();
                cb.setSelected((Boolean) data.getValue().get("clienteFiel"));
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<>(cb);
            }
        });
        //columna Cliente fiel .................................................
        //columna Fecha Inicio..................................................
        columnCuponeraFechaIni = new TableColumn("Fecha inicio");
        columnCuponeraFechaIni.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCuponeraFechaIni.setMinWidth(40);
        columnCuponeraFechaIni.setEditable(false);
        columnCuponeraFechaIni.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaInicio"))));
            }
        });
        //columna Fecha Inicio..................................................
        //columna Fecha Fin.....................................................
        columnCuponeraFechaFin = new TableColumn("Fecha fin");
        columnCuponeraFechaFin.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCuponeraFechaFin.setMinWidth(40);
        columnCuponeraFechaFin.setEditable(false);
        columnCuponeraFechaFin.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(new SimpleDateFormat(patternDateReadFecha).format(Utilidades.objectToTimestamp(data.getValue().get("fechaFin"))));
            }
        });
        //columna Fecha Fin.....................................................
        //columna Cancelar .....................................................
        columnEliminar = new TableColumn("Cancelar");
        columnEliminar.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new AddObjectsCell(tableCuponera);
            }
        });
        //columna Cancelar .....................................................
        tableCuponera.getColumns().addAll(columnNombre, columnMontoMin, columnClienteFiel,
                columnCuponeraFechaIni, columnCuponeraFechaFin, columnEliminar);
        tableCuponera.setItems(cuponeraData);
    }

    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button("cancelar");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
//                    if ("baja_desc_tarj")) {
                    JSONObject cuponera = tableCuponera.getItems().get(index);
                    ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿CANCELAR CUPONERA "
                            + cuponera.get("nombre").toString() + "?", ok, cancel);
                    stackPane.setEffect(new BoxBlur(3, 3, 3));
                    alert2.showAndWait();
                    stackPane.setEffect(null);
                    if (alert2.getResult() == ok) {
                        bajaCuponera(bajaJsonCuponera(cuponera));
                    } else if (alert2.getResult() == cancel) {
                        alert2.close();
                    }
//                    } else {
//                        mensajeAlerta("NO DISPONE DE LOS\nPERMISOS NECESARIOS.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

}
