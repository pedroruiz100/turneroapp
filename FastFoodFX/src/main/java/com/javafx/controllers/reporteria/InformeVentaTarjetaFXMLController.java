package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeVentaTarjetaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private SucursalDAO sucDAO;
    

    HashMap map = new HashMap();

    boolean estado = false;

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelTarjeta;
    @FXML
    private ChoiceBox<String> cbTarjeta;
    @FXML
    private Button buttonConsultaGral;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbNroCaja.setItems(FXCollections.observableArrayList(
                /* "10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90"));

        preparandoDatePicker();
        preparandoTarjeta();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        estado = false;
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeVentaTarjetaFXML.fxml", 711, 275, false);
    }

    @FXML
    private void buttonConsultaGralAction(ActionEvent event) {
        estado = true;
        exportarGral();
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else if (cbTarjeta.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UNA TARJETA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"0\",\"montoFactura\":0,"
                        + "\"horaEmision\":\"N/A\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0, \"entidad\":\"N/A\",\"porcParana\":\"0\",\"PorConvenio\":\"0\",\"montoTarj\":0}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFuncionarioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaTarjetaFXML.fxml", 711, 275, true);
        }
    }

    private void exportarGral() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else if (cbTarjeta.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UNA TARJETA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("{\"ventas\": [{\"subRepTimestamp\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,\"montoTarj\":0,"
                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0, \"subRepNomFun\":\"N/A\",\"porcParana\":\"0\",\"retorno\":\"0\",\"descriTarjeta\":\"N/A\"}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasDetalleJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFuncionarioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaTarjetaFXML.fxml", 711, 275, true);
        }
    }

    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                String id = "0";
                System.out.println("-> " + cbTarjeta.getSelectionModel().getSelectedItem());
                if (!cbTarjeta.getSelectionModel().getSelectedItem().equalsIgnoreCase("**TODOS**")) {
                    id = map.get(cbTarjeta.getSelectionModel().getSelectedItem()).toString();
                }
                URL url = new URL(Utilidades.ip + "/ServerParana/descTarjeta/filtroFechaDescTarjeta/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem() + "/" + id);
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
            }
        } catch (IOException | ParseException ex) {
            jsonArrayDato = new JSONArray();
        }
        return jsonArrayDato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            if (estado) {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_ventasTarjetaTotal.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_ventasTarjetaTotal.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            } else {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_ventasTarjeta.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_ventasTarjeta.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
        estado = false;
    }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());

            JSONObject descuentoTarjetaCab = (JSONObject) parser.parse(json.get("descuentoTarjetaCab").toString());
            JSONObject facturaClienteCabTarjeta = (JSONObject) parser.parse(json.get("facturaClienteCabTarjeta").toString());

            JSONObject jsonCab = (JSONObject) parser.parse(facturaClienteCabTarjeta.get("facturaClienteCab").toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            JSONArray jsonHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());

            long montoHistorico = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
            }

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
            Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(dates));

            jsonFact.put("horaEmision", formatter.format(dates));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoHistorico);
            jsonFact.put("descAplicado", Long.parseLong(json.get("montoDesc").toString()));
            jsonFact.put("totalConDesc", jsonCab.get("montoFactura"));
            jsonFact.put("entidad", descuentoTarjetaCab.get("descriTarjeta").toString());
//            double porcParana = redondearDecimales(Double.parseDouble(descuentoTarjetaCab.get("porcentajeParana").toString()), 0);
            jsonFact.put("porcParana", String.valueOf(redondearDecimales(Double.parseDouble(descuentoTarjetaCab.get("porcentajeDesc").toString()), 0)).replace(".0", ""));

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date daes = new Date();
            Timestamp ts = new Timestamp(daes.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

    private JSONArray editandoFactuasDetalleJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        String nroCaja = "";
        long montoHistorico = 0;
        long montoDesc = 0;
        long montoFactura = 0;
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());

            JSONObject descuentoTarjetaCab = (JSONObject) parser.parse(json.get("descuentoTarjetaCab").toString());
            JSONObject facturaClienteCabTarjeta = (JSONObject) parser.parse(json.get("facturaClienteCabTarjeta").toString());

            JSONObject jsonCab = (JSONObject) parser.parse(facturaClienteCabTarjeta.get("facturaClienteCab").toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            JSONArray jsonHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());

            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
            }
            nroCaja = jsonCaja.get("nroCaja").toString();
            montoDesc += Long.parseLong(json.get("montoDesc").toString());
            montoFactura += Long.parseLong(jsonCab.get("montoFactura").toString());
        }
        JSONObject jsonFact = new JSONObject();
        jsonFact.put("nroCaja", nroCaja);
        jsonFact.put("montoFactura", montoHistorico);
        jsonFact.put("descAplicado", montoDesc);
        jsonFact.put("totalConDesc", montoFactura);
        jsonFact.put("montoTarj", 0);
        jsonFact.put("retorno", (montoDesc / 2));

        jsonFact.put("subRepNomFun", Identity.getNomFun());
        Date daes = new Date();
        Timestamp ts = new Timestamp(daes.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        jsonFact.put("subRepTimestamp", subRepTimestamp);
        newJSONArray.add(jsonFact);
        return newJSONArray;
    }

    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private ArrayList preparandoTarjeta() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabTarjeta/tarjetas");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
                }
                ArrayList array = new ArrayList();
                array.add("**TODOS**");
                for (int i = 0; i < jsonArrayDato.size(); i++) {
                    JSONObject obj = (JSONObject) parser.parse(jsonArrayDato.get(i).toString());
                    JSONObject tarj = (JSONObject) parser.parse(obj.get("tarjeta").toString());
                    map.put(obj.get("descripcionTarj"), tarj.get("idTarjeta"));
                    array.add(obj.get("descripcionTarj").toString());
                }
                cbTarjeta.setItems(FXCollections.observableArrayList(FXCollections.observableArrayList(array)));
                br.close();
            } else {
                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
            }
        } catch (IOException | ParseException ex) {
            jsonArrayDato = new JSONArray();
        }
        return jsonArrayDato;
    }

}
