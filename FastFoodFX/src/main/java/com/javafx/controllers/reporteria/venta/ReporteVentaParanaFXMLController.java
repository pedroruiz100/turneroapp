package com.javafx.controllers.reporteria.venta;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.ArqueoCaja;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabCheque;
import com.peluqueria.core.domain.FacturaClienteCabEfectivo;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.core.domain.FacturaClienteCabMonedaExtranjera;
import com.peluqueria.core.domain.FacturaClienteCabNotaCredito;
import com.peluqueria.core.domain.FacturaClienteCabTarjeta;
import com.peluqueria.core.domain.FacturaClienteDet;
import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.dao.AperturaCajaDAO;
import com.peluqueria.dao.ArqueoCajaDAO;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabChequeDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabEfectivoDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteCabMonedaExtranjeraDAO;
import com.peluqueria.dao.FacturaClienteCabNotaCreditoDAO;
import com.peluqueria.dao.FacturaClienteCabTarjetaDAO;
import com.peluqueria.dao.FacturaClienteDetDAO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ReporteVentaParanaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    CajaDAO cajaDAO;
    @Autowired
    private AperturaCajaDAO aperturaDAO;
    @Autowired
    private ArqueoCajaDAO arqueoDAO;
    @Autowired
    private FacturaClienteCabMonedaExtranjeraDAO fcMonedaExtraDAO;
    @Autowired
    private FacturaClienteCabTarjetaDAO fcTarjetaDAO;
    @Autowired
    private FacturaClienteCabEfectivoDAO fcEfectivoDAO;
    @Autowired
    private FacturaClienteCabChequeDAO fcChequeDAO;
    @Autowired
    private FacturaClienteCabNotaCreditoDAO fcNotaCredDAO;
    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private FacturaClienteCabHistoricoDAO fHistoricoDAO;
    @Autowired
    private FacturaClienteDetDAO fcDetDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private HBox hBoxDate;
    private TextField txtNUmFactura;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;
    @FXML
    private Label labelParana;
    @FXML
    private Button buttonConsultaFormaPago;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.setItems(FXCollections.observableArrayList(caja.getNroCaja()));
//        }
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90", "65", "66", "81"));

        for (Caja caja : cajaDAO.listar()) {
            cbNroCaja.getItems().add(caja.getNroCaja().toString());
        }

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonConsultaFormaPagoAction(ActionEvent event) {
        exportarFormaPago();
    }

    private void buttonConsultaFacturaAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, false);
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, false);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}", true);
                    System.out.println("-->> {\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void exportarFormaPago() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturasFormaPago(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            if (jsonFacturas.isEmpty()) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
            } else {
                try {
                    JSONArray array = editandoFactuasFormaPagoJSONArray(jsonFacturas);
                    if (array.isEmpty()) {
//                        mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
                        informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                                + "\"horaEmision\":\"N/A\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"descAplicado\":0,"
                                + "\"totalConDesc\":0,\"efectivo\":0,\"tarjeta\":0,\"cheque\":0, \"notaCred\":0,\"otroDesc\":0,\"neto\":0,\"convenio\":0,\"vale\":0,\"nomCajero\":\"N/A\"}}]}", false);
                    } else {
                        informandoVentas("{\"ventas\": " + array + "}", false);
                    }
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteVentaParanaFXML.fxml", 749, 275, true);
                    System.out.println("-->> {\"ventas\": " + array + "}");

                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento(fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCab facturaClienteCab : listFactura) {
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            List<FacturaClienteCabHistorico> listHistorico = facturaClienteCab.getFacturaClienteCabHistorico();
            cajaDTO.setFechaAlta(null);
            cajaDTO.setFechaMod(null);
            JSONObject jsonCaja;
            try {
                jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));
                FacturaClienteCabDTO cabDTO = facturaClienteCab.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = cabDTO.getFechaEmision();
                Timestamp fecMod = cabDTO.getFechaMod();
                cabDTO.setFechaEmision(null);
                cabDTO.setFechaMod(null);

                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }
                cabDTO.setFacturaClienteCabHistorico(null);

                JSONObject jsonFactura = (JSONObject) parser.parse(gson.toJson(cabDTO));
                jsonFactura.put("caja", jsonCaja);
                jsonFactura.put("fechaEmision", fecEmi.toString());
                jsonFactura.put("fechaMod", fecMod.toString());
                jsonFactura.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonFactura);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDesc/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    private JSONArray recuperarFacturasFormaPago(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento("null", fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCab facturaClienteCab : listFactura) {
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            List<FacturaClienteCabHistorico> listHistorico = facturaClienteCab.getFacturaClienteCabHistorico();
            cajaDTO.setFechaAlta(null);
            cajaDTO.setFechaMod(null);
            JSONObject jsonCaja;
            try {
                jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));
                FacturaClienteCabDTO cabDTO = facturaClienteCab.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = cabDTO.getFechaEmision();
                Timestamp fecMod = cabDTO.getFechaMod();
                cabDTO.setFechaEmision(null);
                cabDTO.setFechaMod(null);

                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }
                cabDTO.setFacturaClienteCabHistorico(null);

                JSONObject jsonFactura = (JSONObject) parser.parse(gson.toJson(cabDTO));
                jsonFactura.put("caja", jsonCaja);
                jsonFactura.put("fechaEmision", fecEmi.toString());
                jsonFactura.put("fechaMod", fecMod.toString());
                jsonFactura.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonFactura);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }

//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDescuento/null/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    private String recuperarApertura(String fecha, long id) {
        String dato = "";
        JSONParser parser = new JSONParser();
        JSONObject obj = new JSONObject();

        try {
            dato = aperturaDAO.recuperarPorCajaFecha(id, fecha);
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/aperturaCaja/recuperarPorCajaFecha/" + id + "/" + fecha);
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
////                    dato = (inputLine);
//                    obj = (JSONObject) parser.parse(inputLine);
//                }
//                org.json.JSONObject jsonDatos = new org.json.JSONObject(obj);
//                if (!jsonDatos.isNull("usuarioCajero")) {
//                    dato = Utilidades.objectToTimestamp(jsonDatos.getLong("fechaApertura")).toString();
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
        } catch (Exception ex) {
            dato = "SIN DEFINIR";
        } finally {
        }
        return dato;
    }

    private String recuperarArqueo(String fecha, long id) {
        String dato = "";
        JSONParser parser = new JSONParser();
        JSONObject obj = new JSONObject();

        try {
            ArqueoCaja ac = arqueoDAO.recuperarPorCajaFechaObj(id, fecha);
            if (ac != null) {
                Funcionario fun = ac.getSupervisor().getUsuario().getFuncionario();
                dato = (ac.getFechaEmision() + "||" + fun.getNombre() + "||" + fun.getApellido());
            }
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/arqueoCaja/recuperarPorCajaFecha/" + id + "/" + fecha);
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
////                    dato = (inputLine);
//                    obj = (JSONObject) parser.parse(inputLine);
//                }
//                org.json.JSONObject jsonDatos = new org.json.JSONObject(obj);
//                if (!jsonDatos.isNull("supervisor")) {
//                    org.json.JSONObject jsonSupervisor = new org.json.JSONObject(jsonDatos.getJSONObject("supervisor").toString());
//                    org.json.JSONObject jsonUsuario = new org.json.JSONObject(jsonSupervisor.getJSONObject("usuario").toString());
//                    org.json.JSONObject jsonFuncionario = new org.json.JSONObject(jsonUsuario.getJSONObject("funcionario").toString());
//                    dato = Utilidades.objectToTimestamp(jsonDatos.getLong("fechaEmision")).toString() + "||" + jsonFuncionario.getString("nombre") + "||" + jsonFuncionario.getString("apellido");
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
        } catch (Exception ex) {
            dato = "SIN DEFINIR||SIN||DEFINIR";
        }
        return dato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString, boolean datos) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");

            if (datos) {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_PARANA.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_PARANA.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            } else {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_VENTA_FORMA_PAGO.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_VENTA_FORMA_PAGO.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
    //REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        String fechaApertura = "";
        String fechaArqueo = "";
        String nombreSuper = "";
        String apellidoSuper = "";
        ArrayList array = new ArrayList();
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());
            org.json.JSONObject jsonDatos = new org.json.JSONObject(json);

            long idCaja = Long.parseLong(jsonCaja.get("idCaja").toString());
            if (fechaApertura.equals("")) {
                StringTokenizer st1 = new StringTokenizer(recuperarApertura(datePickerFechaInicio.getValue().toString(), idCaja), ".");
                String x = st1.nextElement().toString();
                if (x.length() >= 19) {
                    fechaApertura = x.substring(11, 19);
                }
            }

            if (fechaArqueo.equals("")) {
                StringTokenizer st2 = new StringTokenizer(recuperarArqueo(datePickerFechaFin.getValue().toString(), idCaja), "||");
                try {
                    String data1 = st2.nextElement().toString();
                    fechaArqueo = data1.equals("SIN DEFINIR") ? data1 : data1.substring(11, 19);
                    nombreSuper = st2.nextElement().toString();
                    apellidoSuper = st2.nextElement().toString();
                } catch (Exception e) {
                    fechaArqueo = "SIN DEFINIR";
                    nombreSuper = "SIN";
                    apellidoSuper = "DEFINIR";
                } finally {
                }
            }

            array.add(json.get("usuAlta").toString() + " - " + fechaArqueo);

            long descuentoHistorico = 0;
            long montoHistorico = 0;
            String cliente = "";
            String ruc = "";

            long liqui5 = 0;
            long liqui10 = 0;
            int gra5 = 0;
            int gra10 = 0;
            int exe = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                descuentoHistorico += Long.parseLong(obj.get("descuento").toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
                cliente = String.valueOf(obj.get("cliente"));
                ruc = String.valueOf(obj.get("rucCliente"));

                liqui5 += Long.parseLong(obj.get("liqui5").toString());
                liqui10 += Long.parseLong(obj.get("liqui10").toString());
                gra5 += Integer.parseInt(obj.get("grav5").toString());
                gra10 += Integer.parseInt(obj.get("grav10").toString());
                exe += Integer.parseInt(obj.get("exenta").toString());
            }

            long precio = 0l;
            List<FacturaClienteDet> listDet = fcDetDAO.listarPorFactura(Long.parseLong(json.get("idFacturaClienteCab").toString()));
            if (!listDet.isEmpty()) {
                for (FacturaClienteDet facturaClienteDet : listDet) {
                    precio += facturaClienteDet.getPrecio() * Double.parseDouble(facturaClienteDet.getCantidad().toString());
                }
            }
//            if (!jsonDatos.isNull("facturaClienteDets")) {
//                JSONArray jsonDetalle = (JSONArray) parser.parse(json.get("facturaClienteDets").toString());
//                for (int j = 0; j < jsonDetalle.size(); j++) {
//                    JSONObject jsonDet = (JSONObject) parser.parse(jsonDetalle.get(j).toString());
//                    precio += (Long.parseLong(jsonDet.get("precio").toString()) * Double.parseDouble(jsonDet.get("cantidad").toString()));
//
//                }
//            }

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", json.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(json.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(date));
            jsonFact.put("razon", cliente);
            jsonFact.put("ruc", ruc);

            jsonFact.put("iva5", gra5);
            jsonFact.put("iva10", gra10);
            jsonFact.put("liq5", liqui5);
            jsonFact.put("liq10", liqui10);
            jsonFact.put("exenta", exe);
            jsonFact.put("nomCajero", json.get("usuAlta").toString());
            jsonFact.put("cajeroApertura", json.get("usuAlta").toString() + " - " + fechaApertura);
            jsonFact.put("cajeroCierre", json.get("usuAlta").toString() + " - " + fechaArqueo);
            jsonFact.put("supervisor", nombreSuper + " " + apellidoSuper);

            jsonFact.put("horaEmision", formatter.format(date));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoHistorico);
            jsonFact.put("descAplicado", descuentoHistorico);
            jsonFact.put("totalConDesc", json.get("montoFactura"));
            jsonFact.put("totalDetalle", (precio - descuentoHistorico));
            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);

            newJSONArray.add(jsonFact);
            if (!newJSONArray.isEmpty()) {
                JSONObject jsonVentas = (JSONObject) parser.parse(newJSONArray.get(0).toString());
                jsonVentas.put("cajeroCierre", array.get(array.size() - 1).toString());

                newJSONArray.add(0, jsonVentas);
                newJSONArray.remove(1);
            }
        }
        return newJSONArray;
    }

    private JSONArray editandoFactuasFormaPagoJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        Map mapeo = new HashMap();
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject jsonCab = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);

            long efectivo = 0, cheque = 0, tarjeta = 0, notaCredito = 0, convenio = 0, vale = 0;

            //tarjeta
            List<FacturaClienteCabTarjeta> listTarjeta = fcTarjetaDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listTarjeta.isEmpty()) {
                for (FacturaClienteCabTarjeta tarj : listTarjeta) {
                    tarjeta += tarj.getMonto();
                }
            }
//            if (!jsonDatos.isNull("facturaClienteCabTarjetas")) {
//                JSONArray arrayTarj = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjetas").toString());
//                for (int j = 0; j < arrayTarj.size(); j++) {
//                    JSONObject obj = (JSONObject) parser.parse(arrayTarj.get(j).toString());
//                    tarjeta += Long.parseLong(obj.get("monto").toString());
//                }
//            }

            //efectivo
            List<FacturaClienteCabEfectivo> listEfectivo = fcEfectivoDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listEfectivo.isEmpty()) {
                for (FacturaClienteCabEfectivo efe : listEfectivo) {
                    efectivo += efe.getMontoEfectivo();
                }
            }
//            if (!jsonDatos.isNull("facturaClienteCabEfectivo")) {
//                JSONArray arrayEfe = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabEfectivo").toString());
//                for (int j = 0; j < arrayEfe.size(); j++) {
//                    JSONObject obj = (JSONObject) parser.parse(arrayEfe.get(j).toString());
////                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
////                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
////                        efectivo = Long.parseLong(json.get("efectivo").toString());
////                    }
//                    efectivo += Long.parseLong(obj.get("montoEfectivo").toString());
//                }
//            }

            //monedaExtranjera
            List<FacturaClienteCabMonedaExtranjera> listMonedaExtra = fcMonedaExtraDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listMonedaExtra.isEmpty()) {
                for (FacturaClienteCabMonedaExtranjera monedaExtra : listMonedaExtra) {
                    efectivo += monedaExtra.getMonto_guaranies();
                }
            }

//            if (!jsonDatos.isNull("facturaClienteCabMonedaExtranjera")) {
//                JSONArray arrayMoneda = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabMonedaExtranjera").toString());
//                for (int j = 0; j < arrayMoneda.size(); j++) {
//                    JSONObject obj = (JSONObject) parser.parse(arrayMoneda.get(j).toString());
//                    efectivo += Long.parseLong(obj.get("monto_guaranies").toString());
//                }
//            }
            //cheque
            List<FacturaClienteCabCheque> listCheque = fcChequeDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listCheque.isEmpty()) {
                for (FacturaClienteCabCheque fccCheque : listCheque) {
                    cheque += fccCheque.getMontoCheque();
                }
            }
//            if (!jsonDatos.isNull("facturaClienteCabCheque")) {
//                JSONArray arrayCheque = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabCheque").toString());
//                for (int j = 0; j < arrayCheque.size(); j++) {
//                    JSONObject obj = (JSONObject) parser.parse(arrayCheque.get(j).toString());
////                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
////                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
////                        cheque = Long.parseLong(json.get("cheque").toString());
////                    }
//                    cheque += Long.parseLong(obj.get("montoCheque").toString());
//                }
//            }

            //notaCred
            List<FacturaClienteCabNotaCredito> listNotaCred = fcNotaCredDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listNotaCred.isEmpty()) {
                for (FacturaClienteCabNotaCredito fccNotaCred : listNotaCred) {
                    notaCredito += fccNotaCred.getMonto();
                }
            }
//            if (!jsonDatos.isNull("facturaClienteCabNotaCredito")) {
//                JSONArray arrayNotaCred = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabNotaCredito").toString());
//                for (int j = 0; j < arrayNotaCred.size(); j++) {
//                    JSONObject obj = (JSONObject) parser.parse(arrayNotaCred.get(j).toString());
////                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
////                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
////                        notaCredito = Long.parseLong(json.get("notaCred").toString());
////                    }
//                    notaCredito += Long.parseLong(obj.get("monto").toString());
//                }
//            }

//            facturaClienteCabPromoTempArt
//            JSONArray jsonHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());
            long montoHistorico = 0;
            long descHistorico = 0;
            List<FacturaClienteCabHistorico> listCabHistorico = fHistoricoDAO.listarPorFactura(Long.parseLong(jsonCab.get("idFacturaClienteCab").toString()));
            if (!listCabHistorico.isEmpty()) {
                for (FacturaClienteCabHistorico cabHistorico : listCabHistorico) {
                    montoHistorico += cabHistorico.getTotal();
                    descHistorico += cabHistorico.getDescuento();
                }
            }

//            for (int j = 0; j < jsonHistorico.size(); j++) {aaaa
//                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
//                montoHistorico += Long.parseLong(obj.get("total").toString());
//                descHistorico += Long.parseLong(obj.get("descuento").toString());
//            }
//            OTROS DESCUENTOS
//                  FUNCIONARIO
//            long montoDescPromoTemp = 0;
//            if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
//                JSONArray facturaClienteCabFuncionario = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabFuncionario").toString());
//
//                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabFuncionario.get(j);
//                    JSONArray array = (JSONArray) obje.get("descFuncionario");
//                    for (int k = 0; k < array.size(); k++) {
//                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
//                        montoDescPromoTemp += Long.parseLong(obj.get("montoDesc").toString());
//                    }
//                }
//            }
//                    FIN FUNCIONARIO
//                  facturaClienteCabPromoTemp
//            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
//                JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());
//
//                for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
//                    montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
//                }
//
//            }
//                    FIN facturaClienteCabPromoTemp
//CLIENTE FIEL
//            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
//                JSONArray facturaClienteCabTarjFiel = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjFiel").toString());
//
//                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabTarjFiel.get(j);
//                    JSONArray array = (JSONArray) obje.get("descTarjetaFiels");
//                    for (int k = 0; k < array.size(); k++) {
//                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
//                        montoDescPromoTemp += Long.parseLong(obj.get("montoDesc").toString());
//                    }
//                }
//            }
            if (!jsonDatos.isNull("facturaClienteCabTarjConvenios")) {
                JSONArray facturaClienteCabTarjConv = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjConvenios").toString());
                for (int j = 0; j < facturaClienteCabTarjConv.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabTarjConv.get(j);
                    convenio += Long.parseLong(obje.get("monto").toString());
//                    JSONArray array = (JSONArray) obje.get("descTarjetaConvenios");
//                    for (int k = 0; k < array.size(); k++) {
//                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
//                        convenio += Long.parseLong(obj.get("montoDesc").toString());
//                    }
                }
            }

            if (!jsonDatos.isNull("descVales")) {
                JSONArray facturaClienteCabDescVale = (JSONArray) parser.parse(jsonCab.get("descVales").toString());
                for (int j = 0; j < facturaClienteCabDescVale.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabDescVale.get(j);
                    vale += Long.parseLong(obje.get("montoDescVale").toString());
                }
            }

            //FIN CLIENTE FIEL
//            FIN OTROS DESCUENTOS
            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
            Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(dates));

            jsonFact.put("horaEmision", formatter.format(dates));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoHistorico);

            long formaPago = tarjeta + cheque + notaCredito + convenio + vale;
//            if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                long descAplicado = Long.parseLong(json.get("descAplicado").toString());
//                jsonFact.put("descAplicado", Long.parseLong(facturaClienteCabPromoTempArt.get("monto").toString()) + descAplicado);
//            } else {
//                jsonFact.put("descAplicado", Long.parseLong(facturaClienteCabPromoTempArt.get("monto").toString()));
//            }

            jsonFact.put("totalConDesc", descHistorico);
            jsonFact.put("neto", (montoHistorico - descHistorico));
            jsonFact.put("efectivo", (Long.parseLong(jsonCab.get("montoFactura").toString()) - formaPago));
            jsonFact.put("tarjeta", tarjeta);
            jsonFact.put("cheque", cheque);
            jsonFact.put("notaCred", notaCredito);
            jsonFact.put("convenio", convenio);
            jsonFact.put("vale", vale);
            jsonFact.put("nomCajero", jsonCab.get("usuAlta").toString());

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date daes = new Date();
            Timestamp ts = new Timestamp(daes.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
//            mapeo.put(jsonCab.get("idFacturaClienteCab").toString(), jsonFact);
            newJSONArray.add(jsonFact);

        }
//        Iterator it = mapeo.entrySet().iterator();

//        while (it.hasNext()) {
//            Map.Entry e = (Map.Entry) it.next();
//            newJSONArray.add((JSONObject) parser.parse(e.getValue().toString()));
//        }
        return newJSONArray;
    }

}
