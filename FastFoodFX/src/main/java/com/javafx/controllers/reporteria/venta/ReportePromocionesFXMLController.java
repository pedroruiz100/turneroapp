package com.javafx.controllers.reporteria.venta;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.DescFuncionarioDTO;
import com.peluqueria.dto.DescTarjetaConvenioDTO;
import com.peluqueria.dto.DescTarjetaDTO;
import com.peluqueria.dto.DescTarjetaFielDTO;
import com.peluqueria.dto.DescValeDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabFuncionarioDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.FacturaClienteCabPromoTempArtDTO;
import com.peluqueria.dto.FacturaClienteCabPromoTempDTO;
import com.peluqueria.dto.FacturaClienteCabTarjConvenioDTO;
import com.peluqueria.dto.FacturaClienteCabTarjFielDTO;
import com.peluqueria.dto.FacturaClienteCabTarjetaDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ReportePromocionesFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    FacturaClienteCabDAO facturaClienteCabDAO;

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;
    @Autowired
    private SucursalDAO sucDAO;
    @FXML
    private Button buttonConsultaFactura;
    @Autowired
    private CajaDAO cajaDAO;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.setItems(FXCollections.observableArrayList(caja.getNroCaja()));
//        }
        cbNroCaja.setItems(FXCollections.observableArrayList(
                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90"));

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonConsultaFacturaAction(ActionEvent event) {
        exportarPorFactura();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReportePromocionesFXML.fxml", 749, 275, false);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void exportarPorFactura() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];

            List<FacturaClienteCabDTO> jsonFacturas = recuperarDatosPorFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                        + "\"horaEmision\":\"N/A\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"descAplicado\":0,\"neto\":0,"
                        + "\"descFiel\":0,\"descFunc\":0,\"descTarjeta\":0, \"descPromoArt\":0,\"descVale\":0,\"descConvenio\":0,\"nomCajero\":\"N/A\"}]}", false);

            } else {
                try {
                    List<FacturaClienteCabDTO> array = editandoDatosPorFactuasJSONArray(jsonFacturas);
                    if (array.size() == 0) {
                        informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                                + "\"horaEmision\":\"N/A\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"descAplicado\":0,\"neto\":0,"
                                + "\"descFiel\":0,\"descFunc\":0,\"descTarjeta\":0, \"descPromoArt\":0,\"descVale\":0,\"descConvenio\":0,\"nomCajero\":\"N/A\"}]}", false);
                    } else {
                        informandoVentas("{\"ventas\": " + array + "}", false);
                    }
                    System.out.println("-->> {\"ventas\": " + array + "}");

                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReportePromocionesFXML.fxml", 749, 275, true);
        }
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];

            List<FacturaClienteCabDTO> jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"ticket\":0,\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\","
                        + "\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"precio\":0,\"porcDesc\":0,\"neto\":0,\"descriPromo\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\"}]}", true);

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    if (array.size() == 0) {
                        informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"ticket\":0,"
                                + "\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"precio\":0,\"porcDesc\":0,\"neto\":0,\"descriPromo\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\"}]}", true);
                    } else {
                        informandoVentas("{\"ventas\": " + array + "}", true);
                    }
                    System.out.println("-->> {\"ventas\": " + array + "}");

                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReportePromocionesFXML.fxml", 749, 275, true);
        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private List<FacturaClienteCabDTO> recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
//        try {
        //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDescuento/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
        List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
        for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFechaDescuento(fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem())) {
            listFacDTO.add(fac.toDescuentoFacturaClienteCabDTO());
//            jsonArrayDato = (JSONArray) parser.parse(inputLine);
        }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return listFacDTO;
    }

    private List<FacturaClienteCabDTO> recuperarDatosPorFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
        for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFechaDescuento(fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem())) {
            listFacDTO.add(fac.toDescuentoFacturaClienteCabDTO());
        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDescuento/null/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return listFacDTO;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString, boolean estado) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            if (estado) {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_PROMO.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_PROMO.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            } else {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_VENTA_PROMOCIONES.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_VENTA_PROMOCIONES.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            }

            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            System.out.println("-> repJsonString " + jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
            pSQL.put("subRepSucursal", suc.getDescripcion());
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
    //REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private JSONArray editandoFactuasJSONArray(List<FacturaClienteCabDTO> jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        long countDescuentoFunc = 0;
        long countDescuentoFiel = 0;
        long countDescuentoPromoArt = 0;
        long countDescuentoPromoTempSec = 0;

        long montoDescFunc = 0;
        long montoDescFiel = 0;
        long montoDescPromoArt = 0;
        long montoDescPromoTempSec = 0;

        long montoNetoFunc = 0;
        long montoNetoFiel = 0;
        long montoNetoPromoArt = 0;
        long montoNetoPromoTempSec = 0;

        long montoBrutoFiel = 0;
        long montoBrutoFunc = 0;
        long montoBrutoPromoArt = 0;
        long montoBrutoPromoSec = 0;
        HashMap<Integer, String> map = new HashMap<Integer, String>();
        for (int i = 0; i < jsonFacturas.size(); i++) {
            FacturaClienteCabDTO jsonCab = jsonFacturas.get(i);
            CajaDTO jsonCaja = jsonCab.getCaja();
            List<FacturaClienteCabHistoricoDTO> facturaClienteCabHistorico = jsonCab.getFacturaClienteCabHistorico();

            long montoBruto = 0;
            for (int j = 0; j < facturaClienteCabHistorico.size(); j++) {
                FacturaClienteCabHistoricoDTO objHistori = facturaClienteCabHistorico.get(j);
                montoBruto += Long.parseLong(objHistori.getTotal() + "");
            }

            //                  FUNCIONARIO
//            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);
            if (jsonCab.getFacturaClienteCabFuncionario() != null) {
                List<FacturaClienteCabFuncionarioDTO> facturaClienteCabFuncionario = jsonCab.getFacturaClienteCabFuncionario();

                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
                    FacturaClienteCabFuncionarioDTO obje = facturaClienteCabFuncionario.get(j);
                    JSONArray array = (JSONArray) obje.getDescFuncionario();
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        montoBrutoFunc += montoBruto;
                        countDescuentoFunc = countDescuentoFunc + 1;
                        montoDescFunc += Long.parseLong(obj.get("montoDesc").toString());
                        montoNetoFunc = (montoBrutoFunc - montoDescFunc);

                        map.put(0, jsonCaja.getNroCaja() + "||" + "DESCUENTO A FUNCIONARIOS" + "||" + countDescuentoFunc + "||" + montoBrutoFunc + "||" + montoDescFunc + "||" + montoNetoFunc);
                    }
                }
            }
//                    FIN FUNCIONARIO

            //CLIENTE FIEL
//            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
            if (jsonCab.getFacturaClienteCabTarjFiel() != null) {
                List<FacturaClienteCabTarjFielDTO> facturaClienteCabTarjFiel = jsonCab.getFacturaClienteCabTarjFiel();

                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
                    FacturaClienteCabTarjFielDTO obje = facturaClienteCabTarjFiel.get(j);
                    List<DescTarjetaFielDTO> array = (JSONArray) obje.getDescTarjetaFiels();
                    for (int k = 0; k < array.size(); k++) {
                        DescTarjetaFielDTO obj = array.get(k);
                        montoBrutoFiel += montoBruto;
                        countDescuentoFiel = countDescuentoFiel + 1;
                        montoDescFiel += Long.parseLong(obj.getMontoDesc().toString());
                        montoNetoFiel = (montoBrutoFiel - montoDescFiel);

                        map.put(1, jsonCaja.getNroCaja() + "||" + "PROMOCION CLIENTE FIEL" + "||" + countDescuentoFiel + "||" + montoBrutoFiel + "||" + montoDescFiel + "||" + montoNetoFiel);
                    }
                }
            }

            //FIN CLIENTE FIEL
//                  facturaClienteCabPromoTempArt
            long montoDescPromoTempArt = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTempArt")) {
            if (jsonCab.getFacturaClienteCabPromoTempArt() != null) {
                List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTempArt = jsonCab.getFacturaClienteCabPromoTempArt();
                if (!facturaClienteCabPromoTempArt.isEmpty()) {
                    for (int j = 0; j < facturaClienteCabPromoTempArt.size(); j++) {
                        FacturaClienteCabPromoTempArtDTO obje = facturaClienteCabPromoTempArt.get(j);
                        montoDescPromoTempArt += Long.parseLong(obje.getMonto().toString());
                    }
                    montoBrutoPromoArt += montoBruto;
                    montoDescPromoArt += montoDescPromoTempArt;
                    countDescuentoPromoArt = countDescuentoPromoArt + 1;
                    montoNetoPromoArt = (montoBrutoPromoArt - montoDescPromoArt);

                    map.put(2, jsonCaja.getNroCaja() + "||" + "PROMOCION TEMPORADA ARTICULO" + "||" + countDescuentoPromoArt + "||" + montoBrutoPromoArt + "||" + montoDescPromoArt + "||" + montoNetoPromoArt);
                }
            }
//                    FIN facturaClienteCabPromoTempArt
//                  facturaClienteCabPromoTemp
            long montoDescPromoTemp = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
            if (jsonCab.getFacturaClienteCabPromoTemp() != null) {
                List<FacturaClienteCabPromoTempDTO> facturaClienteCabPromoTemp = jsonCab.getFacturaClienteCabPromoTemp();
                if (!facturaClienteCabPromoTemp.isEmpty()) {
                    for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
                        FacturaClienteCabPromoTempDTO obje = facturaClienteCabPromoTemp.get(j);
                        montoDescPromoTemp += Long.parseLong(obje.getMonto().toString());
                    }
                    montoBrutoPromoSec += montoBruto;
                    montoDescPromoTempSec += montoDescPromoTemp;
                    countDescuentoPromoTempSec = countDescuentoPromoTempSec + 1;
                    montoNetoPromoTempSec = (montoBrutoPromoSec - montoDescPromoTempSec);

                    map.put(3, jsonCaja.getNroCaja() + "||" + "PROMOCION TEMPORADA SECCION" + "||" + countDescuentoPromoTempSec + "||" + montoBrutoPromoSec + "||" + montoDescPromoTempSec + "||" + montoNetoPromoTempSec);
                }
            }
        }

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            JSONObject jsonFact = new JSONObject();

            StringTokenizer st = new StringTokenizer(entry.getValue(), "||");
            String nroCaja = st.nextElement().toString();
            String descriPromo = st.nextElement().toString();
            String ticket = st.nextElement().toString();
            String total = st.nextElement().toString();
            String desc = st.nextElement().toString();
            String neto = st.nextElement().toString();

            jsonFact.put("descriPromo", descriPromo);
            jsonFact.put("ticket", Integer.parseInt(ticket));
            jsonFact.put("precio", Integer.parseInt(total));
            jsonFact.put("porcDesc", Integer.parseInt(desc));
            jsonFact.put("neto", Integer.parseInt(neto));

            jsonFact.put("nroCaja", nroCaja);
            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);

            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }
//    private JSONArray editandoFactuasJSONArray(List<FacturaClienteCabDTO> jsonFacturas) throws ParseException {
//        JSONParser parser = new JSONParser();
//        JSONArray newJSONArray = new JSONArray();
//        long countDescuentoFunc = 0;
//        long countDescuentoFiel = 0;
//        long countDescuentoPromoArt = 0;
//        long countDescuentoPromoTempSec = 0;
//
//        long montoDescFunc = 0;
//        long montoDescFiel = 0;
//        long montoDescPromoArt = 0;
//        long montoDescPromoTempSec = 0;
//
//        long montoNetoFunc = 0;
//        long montoNetoFiel = 0;
//        long montoNetoPromoArt = 0;
//        long montoNetoPromoTempSec = 0;
//
//        long montoBrutoFiel = 0;
//        long montoBrutoFunc = 0;
//        long montoBrutoPromoArt = 0;
//        long montoBrutoPromoSec = 0;
//        HashMap<Integer, String> map = new HashMap<Integer, String>();
//        for (int i = 0; i < jsonFacturas.size(); i++) {
//            JSONObject jsonCab = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
//            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());
//            JSONArray facturaClienteCabHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());
//
//            long montoBruto = 0;
//            for (int j = 0; j < facturaClienteCabHistorico.size(); j++) {
//                JSONObject objHistori = (JSONObject) parser.parse(facturaClienteCabHistorico.get(j).toString());
//                montoBruto += Long.parseLong(objHistori.get("total").toString());
//            }
//
//            //                  FUNCIONARIO
//            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);
//            if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
//                JSONArray facturaClienteCabFuncionario = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabFuncionario").toString());
//
//                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabFuncionario.get(j);
//                    JSONArray array = (JSONArray) obje.get("descFuncionario");
//                    for (int k = 0; k < array.size(); k++) {
//                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
//                        montoBrutoFunc += montoBruto;
//                        countDescuentoFunc = countDescuentoFunc + 1;
//                        montoDescFunc += Long.parseLong(obj.get("montoDesc").toString());
//                        montoNetoFunc = (montoBrutoFunc - montoDescFunc);
//
//                        map.put(0, jsonCaja.get("nroCaja") + "||" + "DESCUENTO A FUNCIONARIOS" + "||" + countDescuentoFunc + "||" + montoBrutoFunc + "||" + montoDescFunc + "||" + montoNetoFunc);
//                    }
//                }
//            }
////                    FIN FUNCIONARIO
//
//            //CLIENTE FIEL
//            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
//                JSONArray facturaClienteCabTarjFiel = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjFiel").toString());
//
//                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabTarjFiel.get(j);
//                    JSONArray array = (JSONArray) obje.get("descTarjetaFiels");
//                    for (int k = 0; k < array.size(); k++) {
//                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
//                        montoBrutoFiel += montoBruto;
//                        countDescuentoFiel = countDescuentoFiel + 1;
//                        montoDescFiel += Long.parseLong(obj.get("montoDesc").toString());
//                        montoNetoFiel = (montoBrutoFiel - montoDescFiel);
//
//                        map.put(1, jsonCaja.get("nroCaja") + "||" + "PROMOCION CLIENTE FIEL" + "||" + countDescuentoFiel + "||" + montoBrutoFiel + "||" + montoDescFiel + "||" + montoNetoFiel);
//                    }
//                }
//            }
//
//            //FIN CLIENTE FIEL
////                  facturaClienteCabPromoTempArt
//            long montoDescPromoTempArt = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTempArt")) {
//                JSONArray facturaClienteCabPromoTempArt = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTempArt").toString());
//                if (!facturaClienteCabPromoTempArt.isEmpty()) {
//                    for (int j = 0; j < facturaClienteCabPromoTempArt.size(); j++) {
//                        JSONObject obje = (JSONObject) facturaClienteCabPromoTempArt.get(j);
//                        montoDescPromoTempArt += Long.parseLong(obje.get("monto").toString());
//                    }
//                    montoBrutoPromoArt += montoBruto;
//                    montoDescPromoArt += montoDescPromoTempArt;
//                    countDescuentoPromoArt = countDescuentoPromoArt + 1;
//                    montoNetoPromoArt = (montoBrutoPromoArt - montoDescPromoArt);
//
//                    map.put(2, jsonCaja.get("nroCaja") + "||" + "PROMOCION TEMPORADA ARTICULO" + "||" + countDescuentoPromoArt + "||" + montoBrutoPromoArt + "||" + montoDescPromoArt + "||" + montoNetoPromoArt);
//                }
//            }
////                    FIN facturaClienteCabPromoTempArt
////                  facturaClienteCabPromoTemp
//            long montoDescPromoTemp = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
//                JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());
//                if (!facturaClienteCabPromoTemp.isEmpty()) {
//                    for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
//                        JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
//                        montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
//                    }
//                    montoBrutoPromoSec += montoBruto;
//                    montoDescPromoTempSec += montoDescPromoTemp;
//                    countDescuentoPromoTempSec = countDescuentoPromoTempSec + 1;
//                    montoNetoPromoTempSec = (montoBrutoPromoSec - montoDescPromoTempSec);
//
//                    map.put(3, jsonCaja.get("nroCaja") + "||" + "PROMOCION TEMPORADA SECCION" + "||" + countDescuentoPromoTempSec + "||" + montoBrutoPromoSec + "||" + montoDescPromoTempSec + "||" + montoNetoPromoTempSec);
//                }
//            }
//        }
//
//        for (Map.Entry<Integer, String> entry : map.entrySet()) {
//            JSONObject jsonFact = new JSONObject();
//
//            StringTokenizer st = new StringTokenizer(entry.getValue(), "||");
//            String nroCaja = st.nextElement().toString();
//            String descriPromo = st.nextElement().toString();
//            String ticket = st.nextElement().toString();
//            String total = st.nextElement().toString();
//            String desc = st.nextElement().toString();
//            String neto = st.nextElement().toString();
//
//            jsonFact.put("descriPromo", descriPromo);
//            jsonFact.put("ticket", Integer.parseInt(ticket));
//            jsonFact.put("precio", Integer.parseInt(total));
//            jsonFact.put("porcDesc", Integer.parseInt(desc));
//            jsonFact.put("neto", Integer.parseInt(neto));
//
//            jsonFact.put("nroCaja", nroCaja);
//            jsonFact.put("subRepNomFun", Identity.getNomFun());
//            Date dates = new Date();
//            Timestamp ts = new Timestamp(dates.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//            jsonFact.put("subRepTimestamp", subRepTimestamp);
//
//            newJSONArray.add(jsonFact);
//        }
//        return newJSONArray;
//    }

    private JSONArray editandoDatosPorFactuasJSONArray(List<FacturaClienteCabDTO> jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        for (int i = 0; i < jsonFacturas.size(); i++) {
            FacturaClienteCabDTO jsonCab = jsonFacturas.get(i);
            CajaDTO jsonCaja = jsonCab.getCaja();

//            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);
            //CLIENTE FIEL
            long montoDescFiel = 0;
//            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
            if (jsonCab.getFacturaClienteCabTarjFiel() != null) {
                List<FacturaClienteCabTarjFielDTO> facturaClienteCabTarjFiel = jsonCab.getFacturaClienteCabTarjFiel();

                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
                    FacturaClienteCabTarjFielDTO obje = facturaClienteCabTarjFiel.get(j);
//                    org.json.JSONObject jsonDatosDescFiel = new org.json.JSONObject(obje);
                    if (obje.getDescTarjetaFiels() != null) {
                        List<DescTarjetaFielDTO> array = obje.getDescTarjetaFiels();
                        for (int k = 0; k < array.size(); k++) {
                            DescTarjetaFielDTO obj = array.get(k);
                            montoDescFiel += Long.parseLong(obj.getMontoDesc() + "");
                        }
                    }
                }
            }
            //FIN DESC_FIEL

            //                  FUNCIONARIO
            long montoDescFuncionario = 0;
//            if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
            if (jsonCab.getFacturaClienteCabFuncionario() != null) {
                List<FacturaClienteCabFuncionarioDTO> facturaClienteCabFuncionario = jsonCab.getFacturaClienteCabFuncionario();

                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
                    FacturaClienteCabFuncionarioDTO obje = facturaClienteCabFuncionario.get(j);
//                    org.json.JSONObject jsonDatosDescFunc = new org.json.JSONObject(obje);
                    if (obje.getDescFuncionario() != null) {
                        List<DescFuncionarioDTO> array = obje.getDescFuncionario();
                        for (int k = 0; k < array.size(); k++) {
                            DescFuncionarioDTO obj = array.get(k);
                            montoDescFuncionario += Long.parseLong(obj.getMontoDesc() + "");
                        }
                    }
                }
            }
//                    FIN FUNCIONARIO

            //tarjeta
            long montoDescTarjeta = 0;
//            if (!jsonDatos.isNull("facturaClienteCabTarjetas")) {
            if (jsonCab.getFacturaClienteCabTarjetas() != null) {
                List<FacturaClienteCabTarjetaDTO> arrayTarj = jsonCab.getFacturaClienteCabTarjetas();
                for (int j = 0; j < arrayTarj.size(); j++) {
                    FacturaClienteCabTarjetaDTO obj = arrayTarj.get(j);
//                    org.json.JSONObject jsonDatosDescTarj = new org.json.JSONObject(obj);
                    if (obj.getDescTarjetas() != null) {
                        List<DescTarjetaDTO> array = (JSONArray) obj.getDescTarjetas();
                        for (int k = 0; k < array.size(); k++) {
                            DescTarjetaDTO objDescTarjeta = array.get(k);
                            montoDescTarjeta += Long.parseLong(objDescTarjeta.getMontoDesc() + "");
                        }
                    }
                }
            }

//            //                  facturaClienteCabPromoTemp
//            long montoDescPromoTemp = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
//                JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());
//
//                for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
//                    JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
//                    montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
//                }
//            }
////                    FIN facturaClienteCabPromoTemp
//                  facturaClienteCabPromoTempArt
            long montoDescPromoTempArt = 0;
//            if (!jsonDatos.isNull("facturaClienteCabPromoTempArt")) {
            if (jsonCab.getFacturaClienteCabPromoTempArt() != null) {
                List<FacturaClienteCabPromoTempArtDTO> facturaClienteCabPromoTemp = jsonCab.getFacturaClienteCabPromoTempArt();
                for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
                    FacturaClienteCabPromoTempArtDTO obje = facturaClienteCabPromoTemp.get(j);
                    montoDescPromoTempArt += Long.parseLong(obje.getMonto() + "");
                }
            }
//                    FIN facturaClienteCabPromoTempArt

//            facturaClienteCabPromoTempArt
            List<FacturaClienteCabHistoricoDTO> jsonHistorico = jsonCab.getFacturaClienteCabHistorico();

            long montoHistorico = 0;
            long descHistorico = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                FacturaClienteCabHistoricoDTO obj = jsonHistorico.get(j);
                montoHistorico += Long.parseLong(obj.getTotal() + "");
                descHistorico += Long.parseLong(obj.getDescuento() + "");
            }

//            OTROS DESCUENTOS
            long montoDescConvenio = 0;
//            if (!jsonDatos.isNull("facturaClienteCabTarjConvenios")) {
            if (jsonCab.getFacturaClienteCabTarjConvenios() != null) {
                List<FacturaClienteCabTarjConvenioDTO> facturaClienteCabTarjConv = jsonCab.getFacturaClienteCabTarjConvenios();
                for (int j = 0; j < facturaClienteCabTarjConv.size(); j++) {
                    FacturaClienteCabTarjConvenioDTO obje = facturaClienteCabTarjConv.get(j);
//                    org.json.JSONObject jsonDatosDescConv = new org.json.JSONObject(obje);
                    if (obje.getDescTarjetaConvenios() != null) {
//                        JSONArray array = (JSONArray) obje.get("descTarjetaConvenios");
                        for (int k = 0; k < obje.getDescTarjetaConvenios().size(); k++) {
                            DescTarjetaConvenioDTO obj = obje.getDescTarjetaConvenios().get(k);
                            montoDescConvenio += Long.parseLong(obj.getMontoDesc() + "");
                        }
                    }
                }
            }

            long montoDescVale = 0;
//            if (!jsonDatos.isNull("descVales")) {
            if (jsonCab.getDescVales() != null) {
                List<DescValeDTO> facturaClienteCabDescVale = jsonCab.getDescVales();
                for (int j = 0; j < facturaClienteCabDescVale.size(); j++) {
                    DescValeDTO obje = facturaClienteCabDescVale.get(j);
                    montoDescVale += Long.parseLong(obje.getMontoDescVale() + "");
                }
            }

            //FIN CLIENTE FIEL
//            FIN OTROS DESCUENTOS
            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", jsonCab.getNroFactura());
            Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.getFechaEmision()).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(dates));

            jsonFact.put("horaEmision", formatter.format(dates));
            jsonFact.put("nroCaja", jsonCaja.getNroCaja());
            jsonFact.put("montoFactura", montoHistorico);
            jsonFact.put("descAplicado", descHistorico);

//            long formaPago = tarjeta + cheque + notaCredito + convenio + vale;
            jsonFact.put("neto", (montoHistorico - descHistorico));
            jsonFact.put("descFiel", montoDescFiel);
            jsonFact.put("descFunc", montoDescFuncionario);
            jsonFact.put("descTarjeta", montoDescTarjeta);
            jsonFact.put("descPromoArt", montoDescPromoTempArt);
            jsonFact.put("descVale", montoDescVale);
            jsonFact.put("descConvenio", montoDescConvenio);

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date daes = new Date();
            Timestamp ts = new Timestamp(daes.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);

            newJSONArray.add(jsonFact);

        }
        return newJSONArray;
    }
}
