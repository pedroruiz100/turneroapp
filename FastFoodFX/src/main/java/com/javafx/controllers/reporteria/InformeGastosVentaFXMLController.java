package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.DetalleGasto;
import com.peluqueria.core.domain.Gastos;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.DetalleGastoDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.GastosDAO;
import com.peluqueria.dao.SucursalDAO;
import com.peluqueria.dto.DetalleGastoDTO;
import com.peluqueria.dto.GastosDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeGastosVentaFXMLController extends BaseScreenController implements Initializable {

    LocalDate now = LocalDate.now(); //2015-11-23
    LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
    LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;
    @FXML
    private Button buttonConsulta;
    @Autowired
    private CajaDAO cajaDAO;
    @Autowired
    private FacturaClienteCabDAO facturaClienteCabDAO;
    @Autowired
    private GastosDAO gatosDAO;
    @Autowired
    private DetalleGastoDAO detalleGastoDAO;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.getItems().add(caja.getNroCaja() + "");
//        }
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"65", "68", "79", "79", "90"));

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");

        datePickerFechaInicio.setValue(firstDay);
        datePickerFechaFin.setValue(lastDay);
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeGastosFXML.fxml", 711, 275, false);
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private List<DetalleGasto> recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        List<DetalleGasto> listFacDTO = new ArrayList<DetalleGasto>();
        listFacDTO = detalleGastoDAO.listarPorFechaParaVenta(fechaInicio, fechaFin);

        return listFacDTO;
    }

    private JSONArray editandoFactuasJSONArray(List<DetalleGasto> jsonFacturas) throws ParseException {
        NumberValidator numbVal = new NumberValidator();
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        int totalMonto = 0;
        for (int i = 0; i < jsonFacturas.size(); i++) {
            Gastos json = jsonFacturas.get(i).getGastos();
            String codigo = jsonFacturas.get(i).getCodigo();
            String descripcion = jsonFacturas.get(i).getDescripcion();
            String cantidad = jsonFacturas.get(i).getCantCompra() + "";
            String importe = numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getCostoCompra() + ""));
            String enStock = jsonFacturas.get(i).getCantidad() + "";
            String fecha = json.getFecha() + "";
            String factura = json.getFactura();
            String proveedor = json.getEmpresa();
//            Date fecha = json.getFecha();
//            String monto = json.getMonto() + "";
            totalMonto += Long.parseLong(jsonFacturas.get(i).getCostoCompra()+"");
//            String monto = numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(json.getMonto() + ""));
            String montoTotal = numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(totalMonto + ""));

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("codigo", codigo);
//            Date date = new Date(Utilidades.objectToTimestamp(json.getFechaEmision()).getTime());
            jsonFact.put("descripcion", descripcion);

            jsonFact.put("cantidad", cantidad);
            jsonFact.put("importe", importe);
            jsonFact.put("enStock", enStock);
            jsonFact.put("fecha", fecha);
            jsonFact.put("factura", factura);
            jsonFact.put("empresa", proveedor);
            jsonFact.put("montoTotal", montoTotal);

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            String dir = "";

            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_gastos_venta_directa.jasper"));
            dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_gastos_venta_directa.pdf";

            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
//            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
//REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } /*else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        }*/ else {
            List<DetalleGasto> jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
//                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0}]}");
                mensajeAlerta("NO SE ENCUENTRAN RESULTADOS DE LA BUSQUEDA.!");
            } else {
                try {
                    List<DetalleGasto> array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeGastosVentaFXML.fxml", 711, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(InformeGastosVentaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }
}
