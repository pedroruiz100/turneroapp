package com.javafx.controllers.reporteria.venta;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.DescTarjeta;
import com.peluqueria.core.domain.DescuentoTarjetaCabArticulo;
import com.peluqueria.dao.DescTarjetaDAO;
import com.peluqueria.dao.DescuentoTarjetaCabArticuloDAO;
import com.peluqueria.dao.DescuentoTarjetaCabDAO;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.DescTarjetaDTO;
import com.peluqueria.dto.DescuentoTarjetaCabArticuloDTO;
import com.peluqueria.dto.DescuentoTarjetaCabDTO;
import com.peluqueria.dto.TarjetaDTO;
import java.io.File;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ReporteArticuloTarjetaFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelParana;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsultaArtTarjeta;
    @FXML
    private Button buttonVolverArtTarjeta;
    @Autowired
    private SucursalDAO sucDAO;
    @FXML
    private Label labelTarjeta;
    @FXML
    private ChoiceBox<String> cbTarjeta;

    HashMap map = new HashMap();
    HashMap mapArticulo = new HashMap();
    @Autowired
    DescTarjetaDAO descTarjetaDAO;
    @Autowired
    DescuentoTarjetaCabDAO descTarjDAO;
//    @Autowired
//    CajaDAO cajaDAO;
    @Autowired
    DescuentoTarjetaCabArticuloDAO descuentoTarjetaCabArticuloDAO;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.setItems(FXCollections.observableArrayList(caja.getNroCaja()));
//        }
        cbNroCaja.setItems(FXCollections.observableArrayList(
                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90"));

        preparandoDatePicker();
//        preparandoTarjeta();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaArtTarjetaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverArtTarjetaAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReporteArticuloTarjetaFXML.fxml", 711, 275, false);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            recuperarArticulosPromocion();
            JSONArray jsonFacturas = recuperarFacturas(cbNroCaja.getSelectionModel().getSelectedItem(), datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"total\":0,"
                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"cant\":0,\"neto\":0,\"precio\":0,\"porcDesc\":\"0\",\"descriArt\":\"N/A\",\"codArt\":\"N/A\",\"subRepNomFun\":\"N/A\",\"subRepTimestamp\":\"N/A\"}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                    System.out.println("-->> {\"ventas\": " + array + "}");

                } catch (ParseException ex) {
                    System.out.println("-->> " + ex.getLocalizedMessage());
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteArticuloTarjetaFXML.fxml", 749, 275, true);
        }
    }

    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }

    private JSONArray recuperarFacturas(String numCaja, String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();;
        //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
        String idTarjeta = "0";
        System.out.println("-> " + cbTarjeta.getSelectionModel().getSelectedItem());
        if (!cbTarjeta.getSelectionModel().getSelectedItem().equalsIgnoreCase("**TODOS**")) {
            StringTokenizer st = new StringTokenizer(map.get(cbTarjeta.getSelectionModel().getSelectedItem()).toString(), "||");
            String id = st.nextElement().toString(); //1
            idTarjeta = st.nextElement().toString();
        }
        List<DescTarjetaDTO> listFacDTO = new ArrayList<DescTarjetaDTO>();
        if (Long.parseLong(idTarjeta) == 0) {
            for (DescTarjeta fac : descTarjetaDAO.filtroFechaDescTarjeta(
                    fechaInicio, fechaFin, numCaja)) {
                listFacDTO.add(fac.toDescTarjetaConDetalleFacturaDTO());
            }
        } else {
            for (DescTarjeta fac : descTarjetaDAO.filtroFechaDescTarjeta(
                    fechaInicio, fechaFin, numCaja, idTarjeta)) {
                listFacDTO.add(fac.toDescTarjetaConDetalleFacturaDTO());
            }
        }
//                URL url = new URL(Utilidades.ip + "/ServerParana/descTarjeta/filtroFechaDescTarjetaConDetalle/" + fechaInicio + "/" + fechaFin + "/" + numCaja + "/" + idTarjeta);
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
        return jsonArrayDato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_ART_TARJ.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_ART_TARJ.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }

//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }

    private JSONArray preparandoTarjeta() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
//        try {
        //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/descTarjeta/recuperarEntidadTarjeta/" + datePickerFechaInicio.getValue().toString() + "/" + datePickerFechaFin.getValue().toString());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
        List<DescuentoTarjetaCabDTO> listFacDTO = new ArrayList<DescuentoTarjetaCabDTO>();
        for (BigInteger id : descTarjetaDAO.recuperarEntidadTarjeta(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString())) {
            listFacDTO.add(descTarjDAO.getById(Long.parseLong(id + ""))
                    .toBDDescuentoTarjetaCabDTO());
        }
        ArrayList array = new ArrayList();
        array.add("**TODOS**");
        for (int i = 0; i < listFacDTO.size(); i++) {
            DescuentoTarjetaCabDTO descuentoTarjetaCabDTO = listFacDTO.get(i);
            TarjetaDTO tarjDTO = descuentoTarjetaCabDTO.getTarjeta();
            map.put(descuentoTarjetaCabDTO.getDescriTarjeta(), descuentoTarjetaCabDTO.getIdDescuentoTarjetaCab() + "||" + tarjDTO.getIdTarjeta());
            array.add(descuentoTarjetaCabDTO.getDescriTarjeta());
        }
//        for (int i = 0; i < jsonArrayDato.size(); i++) {
//            JSONObject obj = (JSONObject) parser.parse(jsonArrayDato.get(i).toString());
//            JSONObject tarj = (JSONObject) parser.parse(obj.get("tarjeta").toString());
//            map.put(obj.get("descriTarjeta"), obj.get("idDescuentoTarjetaCab") + "||" + tarj.get("idTarjeta"));
//            array.add(obj.get("descriTarjeta").toString());
//        }
        cbTarjeta.setItems(FXCollections.observableArrayList(FXCollections.observableArrayList(array)));
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }
//    private ArrayList preparandoTarjeta() {
//        JSONParser parser = new JSONParser();
//        JSONArray jsonArrayDato = null;
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabTarjeta/tarjetas");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                ArrayList array = new ArrayList();
//                array.add("**TODOS**");
//                for (int i = 0; i < jsonArrayDato.size(); i++) {
//                    JSONObject obj = (JSONObject) parser.parse(jsonArrayDato.get(i).toString());
//                    JSONObject tarj = (JSONObject) parser.parse(obj.get("tarjeta").toString());
//                    map.put(obj.get("descripcionTarj"), tarj.get("idTarjeta"));
//                    array.add(obj.get("descripcionTarj").toString());
//                }
//                cbTarjeta.setItems(FXCollections.observableArrayList(FXCollections.observableArrayList(array)));
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
//        return jsonArrayDato;
//    }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());

//            JSONObject descuentoTarjetaCab = (JSONObject) parser.parse(json.get("descuentoTarjetaCab").toString());
            JSONObject facturaClienteCabTarjeta = (JSONObject) parser.parse(json.get("facturaClienteCabTarjeta").toString());

            JSONObject jsonCab = (JSONObject) parser.parse(facturaClienteCabTarjeta.get("facturaClienteCab").toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            JSONArray jsonDetalle = (JSONArray) parser.parse(jsonCab.get("facturaClienteDets").toString());

//            long montoHistorico = 0;
            for (int j = 0; j < jsonDetalle.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonDetalle.get(j).toString());
                String codArticulo = String.valueOf(obj.get("codArticulo").toString());
                double porcDesc = 0;
                long precio = Long.parseLong(obj.get("precio").toString());
                if (mapArticulo.containsKey(codArticulo)) {
                    porcDesc = Double.parseDouble(mapArticulo.get(codArticulo).toString());
                }

                double neto = ((100 - porcDesc) * precio) / 100;
                String descriArt = String.valueOf(obj.get("descripcion").toString());

                JSONObject jsonFact = new JSONObject();
                jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
                Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
                jsonFact.put("fechaEmision", dateFormat.format(dates) + " " + formatter.format(dates));

                jsonFact.put("horaEmision", formatter.format(dates));
                jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
                jsonFact.put("codArt", codArticulo);
                jsonFact.put("descriArt", descriArt);
                jsonFact.put("precio", precio);
                jsonFact.put("porcDesc", String.valueOf(porcDesc).replace(".0", ""));
                jsonFact.put("neto", neto);

                jsonFact.put("subRepNomFun", Identity.getNomFun());
                Date daes = new Date();
                Timestamp ts = new Timestamp(daes.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                jsonFact.put("subRepTimestamp", subRepTimestamp);
                newJSONArray.add(jsonFact);
            }
        }
        return newJSONArray;
    }

    @FXML
    private void cbTarjetaAMouseClicked(MouseEvent event) {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            if (preparandoTarjeta().isEmpty()) {
                System.out.println("NO SE HAN ENCONTRADO RESULTADO DE LA BUSQUEDA");
            }
        }
    }

    @FXML
    private void cbTarjetaKeyRelesed(KeyEvent event) {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            if (preparandoTarjeta().isEmpty()) {
                System.out.println("NO SE HAN ENCONTRADO RESULTADO DE LA BUSQUEDA");
            }
        }
    }

    private void recuperarArticulosPromocion() {
//        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
//        try {
        //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                String id = "0";
//                System.out.println("-> " + cbTarjeta.getSelectionModel().getSelectedItem());
//                if (!cbTarjeta.getSelectionModel().getSelectedItem().equalsIgnoreCase("**TODOS**")) {
        StringTokenizer st = new StringTokenizer(map.get(cbTarjeta.getSelectionModel().getSelectedItem()).toString(), "||");
        String id = st.nextElement().toString(); //1
//                String idTarjeta = st.nextElement().toString();
//                    String idTarjeta = st.nextElement().toString();
//                }
//                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaCabArticulo/" + id);
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
        List<DescuentoTarjetaCabArticuloDTO> descDTO = new ArrayList<DescuentoTarjetaCabArticuloDTO>();
        for (DescuentoTarjetaCabArticulo desc : descuentoTarjetaCabArticuloDAO.listarPorId(id)) {
            descDTO.add(desc.toDescuentoTarjetaCabArticuloDTO());
        }
        if (!jsonArrayDato.isEmpty()) {
            for (int i = 0; i < descDTO.size(); i++) {
                DescuentoTarjetaCabArticuloDTO descTarjArtDTO = descDTO.get(i);
                ArticuloDTO artDTO = descTarjArtDTO.getArticulo();
                DescuentoTarjetaCabDTO descTarj = descTarjArtDTO.getDescuentoTarjetaCab();
                mapArticulo.put(artDTO.getCodArticulo().toString(), descTarj.getPorcentajeDesc());
            }
        }
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
    }

}
