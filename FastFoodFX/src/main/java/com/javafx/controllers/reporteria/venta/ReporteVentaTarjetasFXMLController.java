package com.javafx.controllers.reporteria.venta;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabTarjeta;
import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.core.domain.TipoTarjeta;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteCabTarjetaDAO;
import com.peluqueria.dao.FacturaClienteDetDAO;
import com.peluqueria.dao.TarjetaDAO;
import com.peluqueria.dao.TipoTarjetaDAO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.TarjetaDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ReporteVentaTarjetasFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelParana;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;
    @Autowired
    private CajaDAO cajaDAO;
    @Autowired
    private SucursalDAO sucDAO;
    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private FacturaClienteCabHistoricoDAO fHistoricoDAO;
    @Autowired
    private FacturaClienteCabTarjetaDAO fccTarjetaDAO;
    @Autowired
    private FacturaClienteDetDAO fcDetDAO;
    @Autowired
    private TarjetaDAO tarjDAO;
    @Autowired
    private TipoTarjetaDAO tipoTarjDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.setItems(FXCollections.observableArrayList(caja.getNroCaja()));
//        }
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90"));
        for (Caja caja : cajaDAO.listar()) {
            cbNroCaja.getItems().add(caja.getNroCaja().toString());
        }

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReporteVentaTarjetasFXMLController.fxml", 749, 275, false);
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
//                Date dates = new Date();
//                Timestamp ts = new Timestamp(dates.getTime());
//                String fechaArray[] = ts.toString().split(" ");
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"total\":0,"
//                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"cant\":0,\"exenta\":0,\"iva10\":0,\"iva5\":0,\"entidad\":\"N/A\",\"ruc\":\"N/A\",\"montoFactura\":0, \"codigo\":\"N/A\"}]}", true);
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}", true);
                    System.out.println("-->> {\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteVentaTarjetasFXML.fxml", 749, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();;
        List<FacturaClienteCabTarjeta> listTarjeta = fccTarjetaDAO.filtroFechaTarjeta(fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCabTarjeta facturaClienteCabTarjeta : listTarjeta) {
            try {
                //            FacturaClienteCabTarjetaDTO fccTarjetaDTO = facturaClienteCabTarjeta.toFacturaClienteCabTarjetaSinDetalleDTO();
                JSONObject jsonTarjeta = (JSONObject) parser.parse(gson.toJson(facturaClienteCabTarjeta.toFacturaClienteCabTarjetaSinDetalleDTO()));

                FacturaClienteCab fcc = facturaClienteCabTarjeta.getFacturaClienteCab();
                CajaDTO cajaDTO = fcc.getCaja().toBDCajaDTO();
                cajaDTO.setFechaAlta(null);
                cajaDTO.setFechaMod(null);
                JSONObject jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));

                FacturaClienteCabDTO fccDTO = fcc.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = fccDTO.getFechaEmision();
                Timestamp fecMod = fccDTO.getFechaMod();
                fccDTO.setFechaEmision(null);
                fccDTO.setFechaMod(null);
                fccDTO.setFacturaClienteCabHistorico(null);
                JSONObject jsonCab = (JSONObject) parser.parse(gson.toJson(fccDTO));
                jsonCab.put("fechaEmision", fecEmi.toString());

                Tarjeta tarj = facturaClienteCabTarjeta.getTarjeta();
                TarjetaDTO tarjDTO = tarj.toTarjetaDescriDTO();
                tarjDTO.setFechaAlta(null);
                tarjDTO.setFechaMod(null);
                JSONObject jsonTarj = (JSONObject) parser.parse(gson.toJson(tarjDTO));

                jsonCab.put("caja", jsonCaja);
                jsonTarjeta.put("facturaClienteCab", jsonCab);
                jsonTarjeta.put("tarjeta", jsonTarj);

                jsonArrayDato.add(jsonTarjeta);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        JSONObject tarj = (JSONObject) parser.parse(jsonFacTarj.get("tarjeta").toString());
//            JSONObject json = (JSONObject) parser.parse(jsonFacTarj.get("facturaClienteCab").toString());
//            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabTarjeta/filtroFechaTarjeta/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString, boolean datos
    ) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_TARJETA.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_TARJETA.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
    //REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject jsonFacTarj = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject tarj = (JSONObject) parser.parse(jsonFacTarj.get("tarjeta").toString());
            JSONObject json = (JSONObject) parser.parse(jsonFacTarj.get("facturaClienteCab").toString());

            Tarjeta tarjeta = tarjDAO.getById(Long.parseLong(tarj.get("idTarjeta").toString()));
//            TipoTarjeta tipoTar = tipoTarjDAO.getById(tarjeta.getTipoTarjeta().getIdTipoTarjeta());

            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
//            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());
//            org.json.JSONObject jsonDatos = new org.json.JSONObject(json);

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("nroFactura", json.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(json.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(date));
            jsonFact.put("horaEmision", formatter.format(date));
            jsonFact.put("entidad", tarj.get("descripcion"));
            if (tarjeta.getTipoTarjeta().getIdTipoTarjeta() == 1) {
                jsonFact.put("montoCredito", jsonFacTarj.get("monto"));
                jsonFact.put("montoDebito", 0);
            } else {
                jsonFact.put("montoDebito", jsonFacTarj.get("monto"));
                jsonFact.put("montoCredito", 0);
            }

            jsonFact.put("codigo", jsonFacTarj.get("codAutorizacion"));
            jsonFact.put("nomCajero", json.get("usuAlta").toString());

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);

            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

}
