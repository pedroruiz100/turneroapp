package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeVentaPromoTempArtFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private SucursalDAO sucDAO;

    boolean estado = false;

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonConsultaDetalle;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbNroCaja.setItems(FXCollections.observableArrayList(
                /* "10", "20", "30", "40", "50", "60", "70", */"68", "79", "90"));

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        estado = false;
        exportar();
    }

    @FXML
    private void buttonConsultaDetalleAction(ActionEvent event) {
        estado = true;
        exportarDetalle();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeVentaPromoTempArtFXML.fxml", 711, 275, false);
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                        + "\"horaEmision\":\"N/A\",\"descAplicado\":0,\"totalConDesc\":0,\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"efectivo\":0,\"tarjeta\":0,\"cheque\":0, \"notaCred\":0,\"otroDesc\":0}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFuncionarioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaPromoTempArtFXML.fxml", 711, 275, true);
        }
    }

    private void exportarDetalle() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturasDetalle(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"descriArt\":\"N/A\",\"codArt\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                        + "\"promoArt\":\"N/A\",\"descAplicado\":0,\"totalConDesc\":0,\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"porcDescTotal\":0,\"tarjeta\":0,\"cheque\":0, \"notaCred\":0}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasDetalleJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFuncionarioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaPromoTempArtFXML.fxml", 711, 275, true);
        }
    }

    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabPromoTempArt/filtroFechaPromoTempArt/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
            }
        } catch (IOException | ParseException ex) {
            jsonArrayDato = new JSONArray();
        }
        return jsonArrayDato;
    }

    private JSONArray recuperarFacturasDetalle(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabPromoTempArt/filtroFechaPromoTempArtDet/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
            }
        } catch (IOException | ParseException ex) {
            jsonArrayDato = new JSONArray();
        }
        return jsonArrayDato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;

        jsonString = formatear(jsonString);

        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            if (!estado) {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_ventasPromoTempArt.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_ventasPromoTempArt.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            } else {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_TEMP_ART.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_TEMP_ART.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            }

//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            System.out.println("repJsonString -->> " + jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
        estado = false;
    }

    private JSONArray editandoFactuasDetalleJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        Map mapeoFactura = new HashMap();
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject facturaClienteCabPromoTempArt = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCab = (JSONObject) parser.parse(facturaClienteCabPromoTempArt.get("facturaClienteCab").toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);

            if (!mapeoFactura.containsKey(jsonCab.get("idFacturaClienteCab"))) {
                //promoTemporadaArt
                JSONArray jsonPromoTempArt = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTempArt").toString());
                Map mapPromoTemporada = new HashMap();
                for (int j = 0; j < jsonPromoTempArt.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(jsonPromoTempArt.get(j).toString());
                    JSONObject promoTemporadaArt = (JSONObject) parser.parse(obj.get("promoTemporadaArt").toString());
                    mapPromoTemporada.put(promoTemporadaArt.get("idTemporadaArt"), Utilidades.objectToTimestamp(promoTemporadaArt.get("fechaInicio")) + "||" + Utilidades.objectToTimestamp(promoTemporadaArt.get("fechaFin")));
                }
                //                  facturaClienteDets
                if (!jsonDatos.isNull("facturaClienteDets")) {
                    JSONArray facturaClienteDets = (JSONArray) parser.parse(jsonCab.get("facturaClienteDets").toString());

                    for (int j = 0; j < facturaClienteDets.size(); j++) {
                        String codigo = "";
                        String descriArt = "";
                        long precioMin = 0l;
                        String promo = "";
                        double porcDesc = 0;
                        long desAplicado = 0;

                        JSONObject jsonFact = new JSONObject();

                        JSONObject obje = (JSONObject) facturaClienteDets.get(j);
                        JSONObject objeArt = (JSONObject) obje.get("articulo");

                        descriArt = String.valueOf(obje.get("descripcion").toString());
                        codigo = obje.get("codArticulo").toString();
                        precioMin = Long.parseLong(obje.get("precio").toString()) * (long) Double.parseDouble(obje.get("cantidad").toString());

                        JSONArray objeArtPromoTemp = (JSONArray) objeArt.get("artPromoTemporada");
                        int numDias = 0;
                        for (int k = 0; k < objeArtPromoTemp.size(); k++) {
                            JSONObject artPromo = (JSONObject) objeArtPromoTemp.get(k);
                            JSONObject promoTempArt = (JSONObject) artPromo.get("promoTemporadaArt");

                            if (mapPromoTemporada.containsKey(promoTempArt.get("idTemporadaArt"))) {
                                String fechas = mapPromoTemporada.get(promoTempArt.get("idTemporadaArt")).toString();

                                StringTokenizer st = new StringTokenizer(fechas, "||");
                                Timestamp tmInicio = Timestamp.valueOf(st.nextElement().toString());
                                Timestamp tmFn = Timestamp.valueOf(st.nextElement().toString());
                                Calendar c = Calendar.getInstance();
//fecha inicio
                                Calendar fechaInicio = new GregorianCalendar();
                                fechaInicio.set(tmInicio.getYear(), tmInicio.getMonth(), tmInicio.getDate());
//fecha fin
                                Calendar fechaFin = new GregorianCalendar();
                                fechaFin.set(tmFn.getYear(), tmFn.getMonth(), tmFn.getDate());
                                c.setTimeInMillis(fechaFin.getTime().getTime() - fechaInicio.getTime().getTime());
                                if (numDias == 0 || numDias < c.get(Calendar.DAY_OF_YEAR)) {
                                    numDias = c.get(Calendar.DAY_OF_YEAR);
                                    porcDesc = Double.parseDouble(artPromo.get("porcentajeDesc").toString());
                                    desAplicado = (long) (precioMin * porcDesc) / 100;
                                    promo = promoTempArt.get("descripcionTemporadaArt").toString();
                                }
                            }
                        }
//                    artPromoTemporada

                        jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
                        Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
                        jsonFact.put("fechaEmision", dateFormat.format(dates) + " " + formatter.format(dates));

                        jsonFact.put("horaEmision", formatter.format(dates));
                        jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));

                        jsonFact.put("descAplicado", desAplicado);

                        jsonFact.put("codArt", codigo);
                        jsonFact.put("descriArt", descriArt);
                        jsonFact.put("montoFactura", precioMin);
                        jsonFact.put("promoArt", promo);
                        jsonFact.put("porcDescTotal", porcDesc);

                        jsonFact.put("subRepNomFun", Identity.getNomFun());
                        Date daes = new Date();
                        Timestamp ts = new Timestamp(daes.getTime());
                        String fechaArray[] = ts.toString().split(" ");
                        String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                        jsonFact.put("subRepTimestamp", subRepTimestamp);
                        newJSONArray.add(jsonFact);
                    }
                }
            }
            mapeoFactura.put(jsonCab.get("idFacturaClienteCab"), jsonCab.get("nroFactura"));
        }
        return newJSONArray;
    }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        Map mapeo = new HashMap();
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject facturaClienteCabPromoTempArt = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCab = (JSONObject) parser.parse(facturaClienteCabPromoTempArt.get("facturaClienteCab").toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);

            long efectivo = 0, cheque = 0, tarjeta = 0, notaCredito = 0;

            //tarjeta
            if (!jsonDatos.isNull("facturaClienteCabTarjetas")) {
                JSONArray arrayTarj = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjetas").toString());
                for (int j = 0; j < arrayTarj.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(arrayTarj.get(j).toString());
//                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                        tarjeta = Long.parseLong(json.get("tarjeta").toString());
//                    }
                    tarjeta += Long.parseLong(obj.get("monto").toString());
                }
            }

            //efectivo
            if (!jsonDatos.isNull("facturaClienteCabEfectivo")) {
                JSONArray arrayEfe = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabEfectivo").toString());
                for (int j = 0; j < arrayEfe.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(arrayEfe.get(j).toString());
//                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                        efectivo = Long.parseLong(json.get("efectivo").toString());
//                    }
                    efectivo += Long.parseLong(obj.get("montoEfectivo").toString());
                }
            }

            //monedaExtranjera
            if (!jsonDatos.isNull("facturaClienteCabMonedaExtranjera")) {
                JSONArray arrayMoneda = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabMonedaExtranjera").toString());
                for (int j = 0; j < arrayMoneda.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(arrayMoneda.get(j).toString());
//                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                        efectivo = Long.parseLong(json.get("efectivo").toString());
//                    }
                    efectivo += Long.parseLong(obj.get("monto_guaranies").toString());
                }
            }

            //cheque
            if (!jsonDatos.isNull("facturaClienteCabCheque")) {
                JSONArray arrayCheque = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabCheque").toString());
                for (int j = 0; j < arrayCheque.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(arrayCheque.get(j).toString());
//                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                        cheque = Long.parseLong(json.get("cheque").toString());
//                    }
                    cheque += Long.parseLong(obj.get("montoCheque").toString());
                }
            }

            //notaCred
            if (!jsonDatos.isNull("facturaClienteCabNotaCredito")) {
                JSONArray arrayNotaCred = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabNotaCredito").toString());
                for (int j = 0; j < arrayNotaCred.size(); j++) {
                    JSONObject obj = (JSONObject) parser.parse(arrayNotaCred.get(j).toString());
//                    if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
//                        JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
//                        notaCredito = Long.parseLong(json.get("notaCred").toString());
//                    }
                    notaCredito += Long.parseLong(obj.get("monto").toString());
                }
            }

//            facturaClienteCabPromoTempArt
            JSONArray jsonHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());

            long montoHistorico = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
            }

//            OTROS DESCUENTOS
//                  FUNCIONARIO
            long montoDescPromoTemp = 0;
            if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
                JSONArray facturaClienteCabFuncionario = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabFuncionario").toString());

                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabFuncionario.get(j);
                    JSONArray array = (JSONArray) obje.get("descFuncionario");
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        montoDescPromoTemp += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }

//                    FIN FUNCIONARIO
//                  facturaClienteCabPromoTemp
            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
                JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());

                for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
                    montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
                }

            }
//                    FIN facturaClienteCabPromoTemp
//CLIENTE FIEL
            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
                JSONArray facturaClienteCabTarjFiel = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjFiel").toString());

                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabTarjFiel.get(j);
                    JSONArray array = (JSONArray) obje.get("descTarjetaFiels");
//                    monto += Long.parseLong(obje.get("monto").toString());
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        montoDescPromoTemp += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }

            //FIN CLIENTE FIEL
//            FIN OTROS DESCUENTOS
            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
            Date dates = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(dates));

            jsonFact.put("horaEmision", formatter.format(dates));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoHistorico);

            long formaPago = tarjeta + cheque + notaCredito;
            if (mapeo.containsKey(jsonCab.get("idFacturaClienteCab").toString())) {
                JSONObject json = (JSONObject) parser.parse(mapeo.get(jsonCab.get("idFacturaClienteCab").toString()).toString());
                long descAplicado = Long.parseLong(json.get("descAplicado").toString());
                jsonFact.put("descAplicado", Long.parseLong(facturaClienteCabPromoTempArt.get("monto").toString()) + descAplicado);
            } else {
                jsonFact.put("descAplicado", Long.parseLong(facturaClienteCabPromoTempArt.get("monto").toString()));
            }

            jsonFact.put("totalConDesc", jsonCab.get("montoFactura"));
            jsonFact.put("efectivo", (Long.parseLong(jsonCab.get("montoFactura").toString()) - formaPago));
            jsonFact.put("tarjeta", tarjeta);
            jsonFact.put("cheque", cheque);
            jsonFact.put("notaCred", notaCredito);

            jsonFact.put("otroDesc", montoDescPromoTemp);

            jsonFact.put("promo", facturaClienteCabPromoTempArt.get("descripcionTemporada").toString().substring(0, 15).toUpperCase());

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date daes = new Date();
            Timestamp ts = new Timestamp(daes.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            mapeo.put(jsonCab.get("idFacturaClienteCab").toString(), jsonFact);

        }
        Iterator it = mapeo.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            newJSONArray.add((JSONObject) parser.parse(e.getValue().toString()));
        }
        return newJSONArray;
    }

    private String formatear(String jsonArrStr) {
        org.json.JSONObject ventas = new org.json.JSONObject(jsonArrStr);
        org.json.JSONArray jsonArr = ventas.getJSONArray("ventas");
        org.json.JSONArray sortedJsonArray = new org.json.JSONArray();

        List<org.json.JSONObject> jsonValues = new ArrayList<org.json.JSONObject>();
        for (int i = 0; i < jsonArr.length(); i++) {
            jsonValues.add(jsonArr.getJSONObject(i));
        }
        Collections.sort(jsonValues, new Comparator<org.json.JSONObject>() {
            //You can change "Name" with "ID" if you want to sort by ID
            private static final String KEY_NAME = "fechaEmision";

            @Override
            public int compare(org.json.JSONObject a, org.json.JSONObject b) {
                String valA = new String();
                String valB = new String();

                try {
                    valA = String.valueOf(a.get(KEY_NAME));
                    valB = String.valueOf(b.get(KEY_NAME));
                } catch (JSONException e) {
                    //do something
                }

                return valA.compareTo(valB);
                //if you want to change the sort order, simply use the following:
                //return -valA.compareTo(valB);
            }
        });

        for (int i = 0; i < jsonArr.length(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
        }
        org.json.JSONObject obJSON = new org.json.JSONObject();
        obJSON.put("ventas", sortedJsonArray);
        return obJSON.toString();
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

}
