package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeDetalleVentaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    CajaDAO cajaDAO;
    @Autowired
    private SucursalDAO sucDAO;
    @Autowired
    private FacturaClienteCabDAO fccDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private TextField txtNUmFactura;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /* "10", "20", "30", "40", "50", "60", "70",*/"68", "79", "79", "90"));
        for (Caja caja : cajaDAO.listar()) {
            cbNroCaja.getItems().add(caja.getNroCaja().toString());
        }
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeDetalleVentaFXML.fxml", 711, 275, false);
    }

    private void exportar() {
        if (txtNUmFactura.getText().equals("")) {
            mensajeAlerta("SELECCIONE NUMERO DE FACTURA PARA REALIZAR LA BUSQUEDA.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas();

            if (jsonFacturas.isEmpty()) {
//                Date dates = new Date();
//                Timestamp ts = new Timestamp(dates.getTime());
//                String fechaArray[] = ts.toString().split(" ");
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
//                        + "\"horaEmision\":\"N/A\",\"descAplicado\":0,\"porcDescTotal\":\"0\",\"tarjeta\":0,\"porcDescTarjeta\":\"0\","
//                        + "\"fiel\":0,\"porcDescFiel\":\"0\",\"func\":0,\"porcDescFunc\":\"0\","
//                        + "\"promoArt\":0,\"porcDescPromoArt\":\"0\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"promoSec\":0,\"porcDescPromoSec\":\"0\"}]}");
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeDetalleVentaFXML.fxml", 711, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(InformeDetalleVentaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private JSONArray recuperarFacturas() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<FacturaClienteCab> listFactura = fccDAO.filtroNroFact(txtNUmFactura.getText(), cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCab facturaClienteCab : listFactura) {
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            List<FacturaClienteCabHistorico> listHistorico = facturaClienteCab.getFacturaClienteCabHistorico();
            cajaDTO.setFechaAlta(null);
            cajaDTO.setFechaMod(null);
            JSONObject jsonCaja;
            try {
                jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));
                FacturaClienteCabDTO cabDTO = facturaClienteCab.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = cabDTO.getFechaEmision();
                Timestamp fecMod = cabDTO.getFechaMod();
                cabDTO.setFechaEmision(null);
                cabDTO.setFechaMod(null);

                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }
                cabDTO.setFacturaClienteCabHistorico(null);

                JSONObject jsonFactura = (JSONObject) parser.parse(gson.toJson(cabDTO));
                jsonFactura.put("caja", jsonCaja);
                jsonFactura.put("fechaEmision", fecEmi.toString());
                jsonFactura.put("fechaMod", fecMod.toString());
                jsonFactura.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonFactura);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroNroFactura/" + txtNUmFactura.getText() + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_detalle.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_detalle.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("nroFactura", txtNUmFactura);
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            Date date = new Date();
//            Timestamp ts = new Timestamp(date.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
//REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        for (int i = 0; i < jsonFacturas.size(); i++) {

//            --------------------------------------------------------------------------------
            long montoBruto = 0;
            long descuentoHistorico = 0;
            JSONObject jsonCab = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());

            JSONArray facturaClienteCabHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());
            for (int j = 0; j < facturaClienteCabHistorico.size(); j++) {
                JSONObject objHistori = (JSONObject) parser.parse(facturaClienteCabHistorico.get(j).toString());
                montoBruto += Long.parseLong(objHistori.get("total").toString());
                descuentoHistorico += Long.parseLong(objHistori.get("descuento").toString());
            }

            double porcDescFiel = 0;
            long montoDescFiel = 0;

            //CLIENTE FIEL
            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);
            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
                JSONArray facturaClienteCabTarjFiel = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjFiel").toString());

                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabTarjFiel.get(j);
                    JSONArray array = (JSONArray) obje.get("descTarjetaFiels");
//                    monto += Long.parseLong(obje.get("monto").toString());
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        porcDescFiel += (Double.parseDouble(obj.get("porcentajeDesc").toString()));
                        montoDescFiel += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }
            //FIN CLIENTE FIEL

            //                  TARJETAS
            double porcDescTarj = 0;
            long montoDescTarj = 0;
            if (!jsonDatos.isNull("facturaClienteCabTarjetas")) {
                JSONArray facturaClienteCabTarj = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjetas").toString());

                for (int j = 0; j < facturaClienteCabTarj.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabTarj.get(j);
                    JSONArray array = (JSONArray) obje.get("descTarjetas");
//                            monto += Long.parseLong(obje.get("monto").toString());
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        porcDescTarj += (Double.parseDouble(obj.get("porcentajeDesc").toString()));
                        montoDescTarj += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }
//                    FIN TARJETAS

//                  FUNCIONARIO
            double porcDescFun = 0;
            long montoDescFun = 0;
            if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
                JSONArray facturaClienteCabFuncionario = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabFuncionario").toString());

                for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabFuncionario.get(j);
                    JSONArray array = (JSONArray) obje.get("descFuncionario");
//                            monto += Long.parseLong(obje.get("monto").toString());
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        porcDescFun += (Double.parseDouble(obj.get("porcentajeDesc").toString()));
                        montoDescFun += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }
//                    FIN FUNCIONARIO
            double porcDescPromoArt = 0;
            long montoDescPromoArt = 0;
//                  facturaClienteCabPromoTempArt
            long montoDescPromoTempArt = 0;
            if (!jsonDatos.isNull("facturaClienteCabPromoTempArt")) {
                JSONArray facturaClienteCabPromoTempArt = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTempArt").toString());

                for (int j = 0; j < facturaClienteCabPromoTempArt.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabPromoTempArt.get(j);
                    montoDescPromoTempArt += Long.parseLong(obje.get("monto").toString());
                }
                montoDescPromoArt += montoDescPromoTempArt;
                porcDescPromoArt += ((double) montoDescPromoTempArt * 100) / (double) montoBruto;
            }
//                    FIN facturaClienteCabPromoTempArt
            double porcDescPromoSec = 0;
            long montoDescPromoSec = 0;
//                  facturaClienteCabPromoTemp
            long montoDescPromoTemp = 0;
            if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
                JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());

                for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
                    montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
                }
                montoDescPromoSec += montoDescPromoTemp;
                porcDescPromoSec += ((double) montoDescPromoTemp * 100) / (double) montoBruto;

            }

//                    FIN facturaClienteCabPromoTemp
//            ----------------------------------------------------------------------------------
            double descTotal = 0;
            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(date));

            jsonFact.put("horaEmision", formatter.format(date));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoBruto);
            jsonFact.put("descAplicado", descuentoHistorico);
            jsonFact.put("totalConDesc", jsonCab.get("montoFactura"));

//            descTotal += ((double) montoDescPromoTemp * 100) / (double) montoBruto;
            descTotal = ((double) descuentoHistorico * 100) / (double) montoBruto;

            jsonFact.put("porcDescTotal", String.valueOf(redondearDecimales(descTotal, 2)).replace(".0", "") + "%");
            jsonFact.put("tarjeta", montoDescTarj);
            jsonFact.put("porcDescTarjeta", String.valueOf(porcDescTarj).replace(".0", "") + "%");
            jsonFact.put("fiel", montoDescFiel);
            jsonFact.put("porcDescFiel", String.valueOf(porcDescFiel).replace(".0", "") + "%");
            jsonFact.put("func", montoDescFun);
            jsonFact.put("porcDescFunc", String.valueOf(porcDescFun).replace(".0", "") + "%");
            jsonFact.put("promoArt", montoDescPromoArt);
            jsonFact.put("porcDescPromoArt", String.valueOf(porcDescPromoArt).replace(".0", "") + "%");
            jsonFact.put("promoSec", montoDescPromoSec);
            jsonFact.put("porcDescPromoSec", String.valueOf(porcDescPromoSec).replace(".0", "") + "%");

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

}
