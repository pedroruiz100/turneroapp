package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.Gastos;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.GastosDAO;
import com.peluqueria.dao.SucursalDAO;
import com.peluqueria.dto.GastosDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ResumenMensualFXMLController extends BaseScreenController implements Initializable {

    LocalDate now = LocalDate.now(); //2015-11-23
    LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
    LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;
    @FXML
    private Button buttonConsulta;
    @Autowired
    private CajaDAO cajaDAO;
    @Autowired
    private FacturaClienteCabDAO facturaClienteCabDAO;
    @Autowired
    private GastosDAO gatosDAO;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.getItems().add(caja.getNroCaja() + "");
//        }
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"65", "68", "79", "79", "90"));

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");

        datePickerFechaInicio.setValue(firstDay);
        datePickerFechaFin.setValue(lastDay);
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeGastosFXML.fxml", 711, 275, false);
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private List<GastosDTO> recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        List<GastosDTO> listFacDTO = new ArrayList<GastosDTO>();
        for (Gastos fac : gatosDAO.filtroFecha(fechaInicio, fechaFin)) {
            listFacDTO.add(fac.toGastosDTO());
        }
//        try {
//            //verificar si existen datos en la BD handler_peluqueria pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFecha/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return listFacDTO;
    }

    private JSONArray editandoFactuasJSONArray(List<GastosDTO> jsonFacturas) throws ParseException {
        NumberValidator numbVal = new NumberValidator();
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        List<FacturaClienteCab> listFactura = facturaClienteCabDAO.filtroSolFecha(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());
        int sumaMonto = 0;
        for (FacturaClienteCab fac : listFactura) {
            sumaMonto += fac.getMontoFactura();
        }
        int totalMonto = 0;
        for (int i = 0; i < jsonFacturas.size(); i++) {
            GastosDTO json = jsonFacturas.get(i);
            String concepto = json.getDescripcion();
            String tipo = json.getTipo();
            String debito = numbVal.numberFormat("###,###.###", Double.parseDouble(json.getMonto() + ""));
            totalMonto += json.getMonto();

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("concepto", concepto.substring(0, 1).toUpperCase() + concepto.substring(1, concepto.length()).toLowerCase());
//            Date date = new Date(Utilidades.objectToTimestamp(json.getFechaEmision()).getTime());
//            jsonFact.put("empresa", empresa);
            jsonFact.put("tipo", tipo.toUpperCase());
            jsonFact.put("debito", debito);
            jsonFact.put("credito", "0");
            jsonFact.put("sumaDebito", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(totalMonto + "")));
            jsonFact.put("sumaCredito", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
            jsonFact.put("diferencia", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble((-totalMonto) + "")));
//            jsonFact.put("montoTotal", montoTotal);

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        JSONObject jsonFact = new JSONObject();
        jsonFact.put("concepto", "Facturación de ventas generales");
//            Date date = new Date(Utilidades.objectToTimestamp(json.getFechaEmision()).getTime());
//            jsonFact.put("empresa", empresa);
        jsonFact.put("tipo", "OPERACION COMERCIAL");
        jsonFact.put("debito", "0");
        jsonFact.put("credito", numbVal.numberFormat("###,###.###", Double.parseDouble(sumaMonto + "")));
        jsonFact.put("sumaDebito", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(totalMonto + "")));
        jsonFact.put("sumaCredito", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumaMonto + "")));
        long dif = sumaMonto - totalMonto;
        jsonFact.put("diferencia", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(dif + "")));
//            jsonFact.put("montoTotal", montoTotal);

        jsonFact.put("subRepNomFun", Identity.getNomFun());
        Date dates = new Date();
        Timestamp ts = new Timestamp(dates.getTime());
        String fechaArray[] = ts.toString().split(" ");
        String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
        jsonFact.put("subRepTimestamp", subRepTimestamp);
        newJSONArray.add(jsonFact);
        return newJSONArray;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_resumen_gral.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_resumen_gral.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
//            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
//REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } /*else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        }*/ else {
            List<GastosDTO> jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

//            if (jsonFacturas.isEmpty()) {
//                Date dates = new Date();
//                Timestamp ts = new Timestamp(dates.getTime());
//                String fechaArray[] = ts.toString().split(" ");
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
//                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0}]}");
//                mensajeAlerta("NO SE ENCUENTRAN RESULTADOS DE LA BUSQUEDA.!");
//            } else {
            try {
                List<GastosDTO> array = editandoFactuasJSONArray(jsonFacturas);
                if (array.isEmpty()) {
                    mensajeAlerta("NO SE ENCUENTRAN RESULTADOS DE LA BUSQUEDA.!");
                } else {
                    informandoVentas("{\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/ResumenMensualFXML.fxml", 711, 275, true);
                }
            } catch (ParseException ex) {
                Logger.getLogger(ResumenMensualFXMLController.class.getName()).log(Level.SEVERE, null, ex);
            }
//            }

        }
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }
}
