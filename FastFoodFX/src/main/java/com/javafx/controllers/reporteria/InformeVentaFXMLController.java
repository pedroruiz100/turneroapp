package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.AperturaCaja;
import com.peluqueria.core.domain.ArqueoCaja;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.dao.AperturaCajaDAO;
import com.peluqueria.dao.ArqueoCajaDAO;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.UsuarioDAO;
import com.peluqueria.dto.ArqueoCajaDTO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.SupervisorDTO;
import com.peluqueria.dto.UsuarioDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeVentaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    CajaDAO cajaDAO;
    @Autowired
    UsuarioDAO usuarioDAO;

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;
    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private ArqueoCajaDAO arqueoCajaDAO;
    @Autowired
    private SupervisorDAO supervisorDAO;
    @Autowired
    private AperturaCajaDAO aperturaCajaDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonConsultaArqueo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {

//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "79", "90"));
        for (Caja caja : cajaDAO.listar()) {
            cbNroCaja.getItems().add(caja.getNroCaja().toString());
        }
        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");

    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeVentaFielFXML.fxml", 711, 275, false);
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        List<FacturaClienteCab> listFactura = fccDAO.filtroFecha(fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCab facturaClienteCab : listFactura) {
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            List<FacturaClienteCabHistorico> listHistorico = facturaClienteCab.getFacturaClienteCabHistorico();
            cajaDTO.setFechaAlta(null);
            cajaDTO.setFechaMod(null);
            JSONObject jsonCaja;
            try {
                jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));
                FacturaClienteCabDTO cabDTO = facturaClienteCab.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = cabDTO.getFechaEmision();
                Timestamp fecMod = cabDTO.getFechaMod();
                cabDTO.setFechaEmision(null);
                cabDTO.setFechaMod(null);

                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }
                cabDTO.setFacturaClienteCabHistorico(null);

                JSONObject jsonFactura = (JSONObject) parser.parse(gson.toJson(cabDTO));
                jsonFactura.put("caja", jsonCaja);
                jsonFactura.put("fechaEmision", fecEmi.toString());
                jsonFactura.put("fechaMod", fecMod.toString());
                jsonFactura.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonFactura);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        JSONParser parser = new JSONParser();
//        try {
//            Articulo art = artDAO.buscarCod(cod);
//            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
//        } catch (Exception e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//            return null;
//        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFecha/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    private List<ArqueoCajaDTO> recuperarFacturasArqueos(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        List<ArqueoCajaDTO> jsonArrayDato = new ArrayList<>();
        List<ArqueoCaja> listFactura = arqueoCajaDAO.filtroFecha(fechaInicio, fechaFin);
        for (ArqueoCaja facturaClienteCab : listFactura) {
            ArqueoCajaDTO arqDTO = facturaClienteCab.toBDArqueoCajaDTO();
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            SupervisorDTO supervisorDTO = supervisorDAO.getById(facturaClienteCab.getSupervisor().getIdSupervisor()).toSupervisorDTO();

            arqDTO.setCaja(cajaDTO);
            arqDTO.setSupervisor(supervisorDTO);

            jsonArrayDato.add(arqDTO);
        }
        return jsonArrayDato;
    }
//        JSONParser parser = new JSONParser();
//        try {
//            Articulo art = artDAO.buscarCod(cod);
//            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
//        } catch (Exception e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//            return null;
//        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFecha/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());

            long descuentoHistorico = 0;
            long montoHistorico = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                descuentoHistorico += Long.parseLong(obj.get("descuento").toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
            }

            JSONObject jsonFact = new JSONObject();
            jsonFact.put("nroFactura", json.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(json.get("fechaEmision")).getTime());
            jsonFact.put("fechaEmision", dateFormat.format(date));

            jsonFact.put("horaEmision", formatter.format(date));
            jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
            jsonFact.put("montoFactura", montoHistorico);
            jsonFact.put("descAplicado", descuentoHistorico);
            jsonFact.put("totalConDesc", json.get("montoFactura"));

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_ventas.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_ventas.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
//REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
//                Date dates = new Date();
//                Timestamp ts = new Timestamp(dates.getTime());
//                String fechaArray[] = ts.toString().split(" ");
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
//                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaFXML.fxml", 711, 275, true);

                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void exportarArqueos() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else {
            List<ArqueoCajaDTO> jsonFacturas = recuperarFacturasArqueos(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty() || jsonFacturas == null) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
//                Date dates = new Date();
//                Timestamp ts = new Timestamp(dates.getTime());
//                String fechaArray[] = ts.toString().split(" ");
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
//                        + "\"horaEmision\":\"N/A\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0}]}");

            } else {
                try {
                    List<ArqueoCajaDTO> array = editandoFactuasJSONArrayArqueos(jsonFacturas);
                    informandoVentasArqueos("{\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaFXML.fxml", 711, 275, true);

                } catch (Exception ex) {
                    Logger.getLogger(InformeVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

    @FXML
    private void buttonConsultaArqueoAction(ActionEvent event) {
        exportarArqueos();
    }

    private List<ArqueoCajaDTO> editandoFactuasJSONArrayArqueos(List<ArqueoCajaDTO> jsonFacturas) {
        JSONParser parser = new JSONParser();
        NumberValidator numbVal = new NumberValidator();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
        int x = 0;
        Date fec = null;
        int sumEfe1 = 0;
        int sumTarj1 = 0;
        int sumNota1 = 0;

        int sumEfe2 = 0;
        int sumTarj2 = 0;
        int sumNota2 = 0;

        for (int i = 0; i < jsonFacturas.size(); i++) {
            CajaDTO cajaDTO = jsonFacturas.get(i).getCaja();
            SupervisorDTO supervisorDTO = jsonFacturas.get(i).getSupervisor();
            UsuarioDTO usuarioSupervisor = usuarioDAO.getById(supervisorDTO.getUsuario().getIdUsuario()).toBDUsuarioDTO();

            fec = new Date(Utilidades.objectToTimestamp(jsonFacturas.get(i).getFechaEmision().toString()).getTime());
            AperturaCaja ac = new AperturaCaja();
            if (fec == null) {
                x = 0;
                fec = new Date(Utilidades.objectToTimestamp(jsonFacturas.get(i).getFechaEmision().toString()).getTime());
                ac = aperturaCajaDAO.recuperarPorCaja(cajaDTO.getIdCaja(), x);
                sumEfe1 = jsonFacturas.get(i).getEfectivo();
                sumTarj1 = jsonFacturas.get(i).getSumTarj();
                sumNota1 = jsonFacturas.get(i).getSumNotaCred();

                sumEfe2 = jsonFacturas.get(i).getSumEfectivo();
                sumTarj2 = jsonFacturas.get(i).getInfonet();
                sumNota2 = jsonFacturas.get(i).getNotaCred();
            } else {
                if (fec.equals(new Date(Utilidades.objectToTimestamp(jsonFacturas.get(i).getFechaEmision().toString()).getTime()))) {
                    
                    ac = aperturaCajaDAO.recuperarPorCaja(cajaDTO.getIdCaja(), x);
                    sumEfe1 += jsonFacturas.get(i).getEfectivo();
                    sumTarj1 += jsonFacturas.get(i).getSumTarj();
                    sumNota1 += jsonFacturas.get(i).getSumNotaCred();

                    sumEfe2 += jsonFacturas.get(i).getSumEfectivo();
                    sumTarj2 += jsonFacturas.get(i).getInfonet();
                    sumNota2 += jsonFacturas.get(i).getNotaCred();
                    x++;
                } else {
                    x = 0;
                    ac = aperturaCajaDAO.recuperarPorCaja(cajaDTO.getIdCaja(), x);
                    sumEfe1 = jsonFacturas.get(i).getEfectivo();
                    sumTarj1 = jsonFacturas.get(i).getSumTarj();
                    sumNota1 = jsonFacturas.get(i).getSumNotaCred();

                    sumEfe2 = jsonFacturas.get(i).getSumEfectivo();
                    sumTarj2 = jsonFacturas.get(i).getInfonet();
                    sumNota2 = jsonFacturas.get(i).getNotaCred();
                }
            }

//            CajaDTO cajaDTOApertura = cajaDAO.getById(ac.getCaja().getIdCaja()).toBDCajaDTO();
            UsuarioDTO usuarioCajero = usuarioDAO.getById(ac.getUsuarioCajero().getIdUsuario()).toBDUsuarioDTO();

            JSONObject jsonFact = new JSONObject();
//            jsonFact.put("nroFactura", json.get("nroFactura"));
            Date dateApertura = new Date(Utilidades.objectToTimestamp(ac.getFechaApertura().toString()).getTime());
            jsonFact.put("fecApertura", dateFormat.format(dateApertura));
            jsonFact.put("horaApertura", formatter.format(dateApertura));

            jsonFact.put("caja", cajaDTO.getDescripcion());
            jsonFact.put("usuario", usuarioCajero.getNomUsuario());

            jsonFact.put("efectivo1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getEfectivo() + "")));
            jsonFact.put("tarjeta1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getInfonet() + "")));
            jsonFact.put("nota1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getNotaCred() + "")));

            jsonFact.put("efectivo2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getSumEfectivo() + "")));
            jsonFact.put("tarjeta2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getSumTarj() + "")));
            jsonFact.put("nota2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFacturas.get(i).getSumNotaCred() + "")));

            Date dateArqueo = new Date(Utilidades.objectToTimestamp(jsonFacturas.get(i).getFechaEmision() + "").getTime());
            jsonFact.put("fecArqueo", dateFormat.format(dateArqueo));
            jsonFact.put("horaArqueo", formatter.format(dateArqueo));
            jsonFact.put("supervisor", usuarioSupervisor.getNomUsuario());

            jsonFact.put("sumaEfe1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumEfe1 + "")));
            jsonFact.put("sumaTarj1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumTarj1 + "")));
            jsonFact.put("sumaNota1", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumNota1 + "")));

            jsonFact.put("sumaEfe2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumEfe2 + "")));
            jsonFact.put("sumaTarj2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumTarj2 + "")));
            jsonFact.put("sumaNota2", numbVal.numberFormat("Gs ###,###.###", Double.parseDouble(sumNota2 + "")));

            jsonFact.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact.put("subRepTimestamp", subRepTimestamp);
            newJSONArray.add(jsonFact);
        }
        return newJSONArray;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentasArqueos(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_listado_arqueo.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_listado_arqueo.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
//            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
}
