package com.javafx.controllers.reporteria;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class InformeVentaFielFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cbNroCaja.setItems(FXCollections.observableArrayList(
                /* "10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90"));

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");
    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteria/InformeVentaFielFXML.fxml", 711, 275, false);
    }

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];

            if (jsonFacturas.isEmpty()) {
                informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                        + "\"horaEmision\":\"N/A\",\"descAplicado\":0,\"totalConDesc\":0,\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"porDcto\":0,\"otroDesc\":0,\"otroPorcDesc\":0}]}");

            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    if (array.size() == 0) {
                        informandoVentas("{\"ventas\": [{\"nroFactura\":\"N/A\",\"fechaEmision\":\"N/A\",\"nroCaja\":\"" + cbNroCaja.getSelectionModel().getSelectedItem() + "\",\"montoFactura\":0,"
                                + "\"horaEmision\":\"N/A\",\"descAplicado\":0,\"totalConDesc\":0,\"porDcto\":0,\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"otroDesc\":0,\"otroPorcDesc\":0}]}");
                    } else {
                        informandoVentas("{\"ventas\": " + array + "}");
                    }
                } catch (ParseException ex) {
                    Logger.getLogger(InformeVentaFielFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteria/InformeVentaFielFXML.fxml", 711, 275, true);

        }
    }

    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }

    private JSONArray recuperarFacturas(String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = null;
        try {
            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDescuento/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
            }
        } catch (IOException | ParseException ex) {
            jsonArrayDato = new JSONArray();
        }
        return jsonArrayDato;
    }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

//        long monto = 0;
        for (int i = 0; i < jsonFacturas.size(); i++) {
            long montoBruto = 0;
            JSONObject jsonCab = (JSONObject) parser.parse(jsonFacturas.get(i).toString());

            JSONObject jsonCaja = (JSONObject) parser.parse(jsonCab.get("caja").toString());
            JSONArray facturaClienteCabHistorico = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabHistorico").toString());
            for (int j = 0; j < facturaClienteCabHistorico.size(); j++) {
                JSONObject objHistori = (JSONObject) parser.parse(facturaClienteCabHistorico.get(j).toString());
                montoBruto += Long.parseLong(objHistori.get("total").toString());
            }

            double porcDescuento = 0;
            long montoDesc = 0;

            //CLIENTE FIEL
            org.json.JSONObject jsonDatos = new org.json.JSONObject(jsonCab);
            if (!jsonDatos.isNull("facturaClienteCabTarjFiel")) {
                JSONArray facturaClienteCabTarjFiel = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabTarjFiel").toString());

                for (int j = 0; j < facturaClienteCabTarjFiel.size(); j++) {
                    JSONObject obje = (JSONObject) facturaClienteCabTarjFiel.get(j);
                    JSONArray array = (JSONArray) obje.get("descTarjetaFiels");
//                    monto += Long.parseLong(obje.get("monto").toString());
                    for (int k = 0; k < array.size(); k++) {
                        JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                        porcDescuento += (Double.parseDouble(obj.get("porcentajeDesc").toString()));
                        montoDesc += Long.parseLong(obj.get("montoDesc").toString());
                    }
                }
            }

            //FIN CLIENTE FIEL
            if (montoDesc != 0) {

//                  FUNCIONARIO
                double otroPorcDescuento = 0;
                long otroMontoDesc = 0;
                if (!jsonDatos.isNull("facturaClienteCabFuncionario")) {
                    JSONArray facturaClienteCabFuncionario = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabFuncionario").toString());

                    for (int j = 0; j < facturaClienteCabFuncionario.size(); j++) {
                        JSONObject obje = (JSONObject) facturaClienteCabFuncionario.get(j);
                        JSONArray array = (JSONArray) obje.get("descFuncionario");
//                            monto += Long.parseLong(obje.get("monto").toString());
                        for (int k = 0; k < array.size(); k++) {
                            JSONObject obj = (JSONObject) parser.parse(array.get(k).toString());
                            otroPorcDescuento += (Double.parseDouble(obj.get("porcentajeDesc").toString()));
                            otroMontoDesc += Long.parseLong(obj.get("montoDesc").toString());
                        }
                    }
                }

//                    FIN FUNCIONARIO
//                  facturaClienteCabPromoTempArt
                long montoDescPromoTempArt = 0;
                if (!jsonDatos.isNull("facturaClienteCabPromoTempArt")) {
                    JSONArray facturaClienteCabPromoTempArt = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTempArt").toString());
                    if (!facturaClienteCabPromoTempArt.isEmpty()) {
                        for (int j = 0; j < facturaClienteCabPromoTempArt.size(); j++) {
                            JSONObject obje = (JSONObject) facturaClienteCabPromoTempArt.get(j);
                            montoDescPromoTempArt += Long.parseLong(obje.get("monto").toString());
                        }
                        otroMontoDesc += montoDescPromoTempArt;
                        otroPorcDescuento += ((double) montoDescPromoTempArt * 100) / (double) montoBruto;
                    }
                }
//                    FIN facturaClienteCabPromoTempArt
//                  facturaClienteCabPromoTemp
                long montoDescPromoTemp = 0;
                if (!jsonDatos.isNull("facturaClienteCabPromoTemp")) {
                    JSONArray facturaClienteCabPromoTemp = (JSONArray) parser.parse(jsonCab.get("facturaClienteCabPromoTemp").toString());
                    if (!facturaClienteCabPromoTemp.isEmpty()) {
                        for (int j = 0; j < facturaClienteCabPromoTemp.size(); j++) {
                            JSONObject obje = (JSONObject) facturaClienteCabPromoTemp.get(j);
                            montoDescPromoTemp += Long.parseLong(obje.get("monto").toString());
                        }
                        otroMontoDesc += montoDescPromoTemp;
                        otroPorcDescuento += ((double) montoDescPromoTemp * 100) / (double) montoBruto;
                    }
                }
//                    FIN facturaClienteCabPromoTemp
                String desc = "";
                if (porcDescuento - (long) porcDescuento > 0.0) {
                    desc = porcDescuento + "";
                } else {
                    desc = (long) porcDescuento + "";
                }

                String otroDesc = "";
//                otroPorcDescuento = Double.valueOf(df.format(otroPorcDescuento));
                otroPorcDescuento = redondearDecimales(otroPorcDescuento, 2);
                if (otroPorcDescuento - (long) otroPorcDescuento > 0.0) {
                    otroDesc = otroPorcDescuento + "";
                } else {
                    otroDesc = (long) otroPorcDescuento + "";
                }

                JSONObject jsonFact = new JSONObject();
                jsonFact.put("nroFactura", jsonCab.get("nroFactura"));
                Date date = new Date(Utilidades.objectToTimestamp(jsonCab.get("fechaEmision")).getTime());
                jsonFact.put("fechaEmision", dateFormat.format(date));

                jsonFact.put("horaEmision", formatter.format(date));
                jsonFact.put("nroCaja", jsonCaja.get("nroCaja"));
                jsonFact.put("montoFactura", montoBruto);
                jsonFact.put("descAplicado", montoDesc);

//                long totalConDesc = montoBruto - (montoDesc + otroMontoDesc);
                jsonFact.put("totalConDesc", jsonCab.get("montoFactura"));

                jsonFact.put("otroDesc", otroMontoDesc);
                jsonFact.put("otroPorcDesc", otroDesc);
                jsonFact.put("porDcto", desc);

                jsonFact.put("subRepNomFun", Identity.getNomFun());
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                jsonFact.put("subRepTimestamp", subRepTimestamp);
                newJSONArray.add(jsonFact);
            }
        }
        return newJSONArray;
    }

    public static double redondearDecimales(double valorInicial, int numeroDecimales) {
        double parteEntera, resultado;
        resultado = valorInicial;
        parteEntera = Math.floor(resultado);
        resultado = (resultado - parteEntera) * Math.pow(10, numeroDecimales);
        resultado = Math.round(resultado);
        resultado = (resultado / Math.pow(10, numeroDecimales)) + parteEntera;
        return resultado;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_ventasClienteFiel.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_ventasClienteFiel.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
            pSQL.put("subRepNomFun", Identity.getNomFun());
            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
//REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }
}
