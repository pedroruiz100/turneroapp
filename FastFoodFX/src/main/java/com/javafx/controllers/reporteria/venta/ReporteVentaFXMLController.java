package com.javafx.controllers.reporteria.venta;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.dao.SucursalDAO;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Caja;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.core.domain.FacturaClienteDet;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteDetDAO;
import com.peluqueria.dto.CajaDTO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.FacturaClienteDetDTO;
import java.io.File;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ReporteVentaFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private AnchorPane anchorPaneComponent;
    @FXML
    private AnchorPane anchorPaneSeleccion;
    @FXML
    private Label labelPeluquero;
    @FXML
    private HBox hBoxDate;
    @FXML
    private TextField txtNUmFactura;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private ChoiceBox<String> cbNroCaja;
    @FXML
    private HBox hBoxButtons;
    @FXML
    private Button buttonConsulta;
    @FXML
    private Button buttonVolver;

    @Autowired
    private SucursalDAO sucDAO;
    @Autowired
    private CajaDAO cajaDAO;
    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private FacturaClienteCabHistoricoDAO fHistoricoDAO;
    @Autowired
    private FacturaClienteDetDAO fcDetDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    @FXML
    private Button buttonConsultaFactura;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
//        for (Caja caja : cajaDAO.listar()) {
//            cbNroCaja.setItems(FXCollections.observableArrayList(caja.getNroCaja()));
//        }
//        cbNroCaja.setItems(FXCollections.observableArrayList(
//                /*"10", "20", "30", "40", "50", "60", "70",*/"68", "79", "90", "65", "66", "81"));
        for (Caja caja : cajaDAO.listar()) {
            cbNroCaja.getItems().add(caja.getNroCaja().toString());
        }

        preparandoDatePicker();
        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");

    }

    @FXML
    private void buttonConsultaAction(ActionEvent event) {
        exportar();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/reporteVenta/ReporteVentaFXML.fxml", 749, 275, false);
    }

    @FXML
    private void buttonConsultaFacturaAction(ActionEvent event) {
        exportarFactura();
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        this.sc.getStage().setAlwaysOnTop(false);
        ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
            this.sc.getStage().setAlwaysOnTop(true);
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    private JSONArray recuperarFacturas(String numFactura, String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        if (numFactura.equalsIgnoreCase("")) {
            numFactura = "null";
        }
//        facturaClienteCab").toString());
//            
//            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
//            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());
//            
        List<FacturaClienteDet> listDetalle = fcDetDAO.filtroFechaDescuentoFact(numFactura, fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteDet facturaClienteDet : listDetalle) {
            try {
                FacturaClienteCab fcc = facturaClienteDet.getFacturaClienteCab();
                FacturaClienteCab fcc2 = fccDAO.getById(fcc.getIdFacturaClienteCab());
                Timestamp fecEmi = fcc2.getFechaEmision();
                Timestamp fecMod = fcc2.getFechaMod();
                fcc.setFechaEmision(null);
                fcc.setFechaMod(null);
                FacturaClienteCabDTO cabeceraDTO = fcc.toBDFacturaClienteCabDTO();
                cabeceraDTO.setFacturaClienteCabHistorico(null);
                JSONObject jsonCabecera = (JSONObject) parser.parse(gson.toJson(cabeceraDTO));
                jsonCabecera.put("fechaEmision", fecEmi.toString());
                jsonCabecera.put("fechaMod", fecMod.toString());

                Caja caja = fcc.getCaja();
                caja.setFechaAlta(null);
                caja.setFechaMod(null);
                JSONObject jsonCaja = (JSONObject) parser.parse(gson.toJson(caja.toBDCajaDTO()));
                List<FacturaClienteCabHistorico> listHistorico = fHistoricoDAO.listarPorFactura(fcc.getIdFacturaClienteCab());
//                List<FacturaClienteCabHistorico> listHistorico = fcc.getFacturaClienteCabHistorico();
                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }

                FacturaClienteDetDTO fcDetDTO = facturaClienteDet.toDescriFacturaClienteDetDTO();
                JSONObject jsonDet = (JSONObject) parser.parse(gson.toJson(fcDetDTO));
                jsonCabecera.put("caja", jsonCaja);
                jsonDet.put("facturaClienteCab", jsonCabecera);
                jsonDet.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonDet);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteDet/filtroFechaDescuento/" + numFactura + "/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    private JSONArray recuperarFacturasCabecera(String numFactura, String fechaInicio, String fechaFin) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayDato = new JSONArray();
        if (numFactura.equalsIgnoreCase("")) {
            numFactura = "null";
        }
        List<FacturaClienteCab> listFactura = fccDAO.filtroFechaDescuento(numFactura, fechaInicio, fechaFin, cbNroCaja.getSelectionModel().getSelectedItem());
        for (FacturaClienteCab facturaClienteCab : listFactura) {
            CajaDTO cajaDTO = facturaClienteCab.getCaja().toCajaSinRelacionDTO();
            List<FacturaClienteCabHistorico> listHistorico = facturaClienteCab.getFacturaClienteCabHistorico();
            cajaDTO.setFechaAlta(null);
            cajaDTO.setFechaMod(null);
            JSONObject jsonCaja;
            try {
                jsonCaja = (JSONObject) parser.parse(gson.toJson(cajaDTO));
                FacturaClienteCabDTO cabDTO = facturaClienteCab.toBDFacturaClienteCabDTO();
                Timestamp fecEmi = cabDTO.getFechaEmision();
                Timestamp fecMod = cabDTO.getFechaMod();
                cabDTO.setFechaEmision(null);
                cabDTO.setFechaMod(null);

                JSONArray arrayHistorico = new JSONArray();
                for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                    FacturaClienteCabHistoricoDTO historicoDTO = facturaClienteCabHistorico.toFacturaClienteCabDTO();
                    historicoDTO.setFacturaClienteCab(null);
                    historicoDTO.setFecInicial(null);
                    historicoDTO.setFecVencimiento(null);
                    JSONObject jsonHistorico = (JSONObject) parser.parse(gson.toJson(historicoDTO));
                    arrayHistorico.add(jsonHistorico);
                }
                cabDTO.setFacturaClienteCabHistorico(null);

                JSONObject jsonFactura = (JSONObject) parser.parse(gson.toJson(cabDTO));
                jsonFactura.put("caja", jsonCaja);
                jsonFactura.put("fechaEmision", fecEmi.toString());
                jsonFactura.put("fechaMod", fecMod.toString());
                jsonFactura.put("facturaClienteCabHistorico", arrayHistorico);

                jsonArrayDato.add(jsonFactura);
            } catch (ParseException ex) {
                System.out.println("ERR-> " + ex.getLocalizedMessage());
                System.out.println("ERR-> " + ex.getMessage());
                System.out.println("ERR-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            //verificar si existen datos en la BD handler_food pendiente.descuento_pendientes
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab/filtroFechaDescuento/" + numFactura + "/" + fechaInicio + "/" + fechaFin + "/" + cbNroCaja.getSelectionModel().getSelectedItem());
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    jsonArrayDato = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                mensajeAlerta("NO SE HA ESTABLECIDO CONEXION CON EL SERVIDOR");
//            }
//        } catch (IOException | ParseException ex) {
//            jsonArrayDato = new JSONArray();
//        }
        return jsonArrayDato;
    }

    private JSONArray editandoFactuasJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        Map mapeo = new HashMap();

        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject jsonDet = (JSONObject) parser.parse(jsonFacturas.get(i).toString());
            JSONObject json = (JSONObject) parser.parse(jsonDet.get("facturaClienteCab").toString());

            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
//            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());

            long descuentoHistorico = 0;
            long montoHistorico = 0;
            long liqui5 = 0;
            long liqui10 = 0;
            int gra5 = 0;
            int gra10 = 0;
            int exe = 0;
            long montoDesc = 0;

            List<FacturaClienteCabHistorico> listHistorico = fHistoricoDAO.listarPorFactura(Long.parseLong(json.get("idFacturaClienteCab").toString()));
            for (FacturaClienteCabHistorico facturaClienteCabHistorico : listHistorico) {
                descuentoHistorico += facturaClienteCabHistorico.getDescuento();
                montoHistorico += facturaClienteCabHistorico.getTotal();
                liqui5 += facturaClienteCabHistorico.getLiqui5();
                liqui10 += facturaClienteCabHistorico.getLiqui10();
                gra5 += facturaClienteCabHistorico.getGrav5();
                gra10 += facturaClienteCabHistorico.getGrav10();
                exe += facturaClienteCabHistorico.getExenta();
                montoDesc += facturaClienteCabHistorico.getDescuento();
            }

            JSONObject jsonFact1 = new JSONObject();

            long iva5 = 0;
            long iva10 = 0;
            long exenta = 0;
            if (jsonDet.get("poriva").toString().equalsIgnoreCase("5")) {
                iva5 = (Long.parseLong(jsonDet.get("precio").toString()) * (long) Double.parseDouble(jsonDet.get("cantidad").toString()));
            } else if (jsonDet.get("poriva").toString().equalsIgnoreCase("10")) {
                iva10 = (Long.parseLong(jsonDet.get("precio").toString()) * (long) Double.parseDouble(jsonDet.get("cantidad").toString()));
            } else {
                exenta = (Long.parseLong(jsonDet.get("precio").toString()) * (long) Double.parseDouble(jsonDet.get("cantidad").toString()));
            }

            jsonFact1.put("nroFactura", json.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(json.get("fechaEmision")).getTime());
            jsonFact1.put("fechaEmision", dateFormat.format(date) + " " + formatter.format(date));

            jsonFact1.put("horaEmision", formatter.format(date));
            jsonFact1.put("nroCaja", jsonCaja.get("nroCaja"));

            jsonFact1.put("codArt", jsonDet.get("codArticulo").toString());
            if (jsonDet.get("descripcion").toString().length() >= 15) {
                jsonFact1.put("descriArt", jsonDet.get("descripcion").toString().substring(0, 15));
            } else {
                jsonFact1.put("descriArt", jsonDet.get("descripcion").toString());
            }

            jsonFact1.put("iva5", iva5);
            jsonFact1.put("iva10", iva10);
            jsonFact1.put("liq5", liqui5);
            jsonFact1.put("liq10", liqui10);
            jsonFact1.put("exenta", exenta);
            jsonFact1.put("descAplicado", montoDesc);
            jsonFact1.put("gr5", gra5);
            jsonFact1.put("gr10", gra10);
            jsonFact1.put("exe", exe);

            if (mapeo.containsKey(json.get("nroFactura"))) {
                if (mapeo.get(json.get("nroFactura")).toString().equalsIgnoreCase(gra5 + "-" + gra10 + "-" + exe)) {
                    jsonFact1.put("g5", 0);
                    jsonFact1.put("g10", 0);
                    jsonFact1.put("ex", 0);
                } else {
                    jsonFact1.put("g5", gra5);
                    jsonFact1.put("g10", gra10);
                    jsonFact1.put("ex", exe);
                }
            } else {
                jsonFact1.put("g5", gra5);
                jsonFact1.put("g10", gra10);
                jsonFact1.put("ex", exe);
            }

//            jsonFact1.put("cant", Double.parseDouble(jsonDet.get("cantidad").toString()));
            jsonFact1.put("total", (Long.parseLong(jsonDet.get("precio").toString()) * Double.parseDouble(jsonDet.get("cantidad").toString())));

            jsonFact1.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact1.put("subRepTimestamp", subRepTimestamp);
            mapeo.put(json.get("nroFactura"), gra5 + "-" + gra10 + "-" + exe);
            newJSONArray.add(jsonFact1);
        }
        return newJSONArray;
    }

    private JSONArray editandoFactuasVentaJSONArray(JSONArray jsonFacturas) throws ParseException {
        JSONParser parser = new JSONParser();
        JSONArray newJSONArray = new JSONArray();
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");

        for (int i = 0; i < jsonFacturas.size(); i++) {
            JSONObject json = (JSONObject) parser.parse(jsonFacturas.get(i).toString());

            org.json.JSONObject jsonDatos = new org.json.JSONObject(json);

            JSONObject jsonCaja = (JSONObject) parser.parse(json.get("caja").toString());
            JSONArray jsonHistorico = (JSONArray) parser.parse(json.get("facturaClienteCabHistorico").toString());

            long descuentoHistorico = 0;
            long montoHistorico = 0;
            long liqui5 = 0;
            long liqui10 = 0;
            int gra5 = 0;
            int gra10 = 0;
            int exe = 0;
            long montoDesc = 0;
            for (int j = 0; j < jsonHistorico.size(); j++) {
                JSONObject obj = (JSONObject) parser.parse(jsonHistorico.get(j).toString());
                descuentoHistorico += Long.parseLong(obj.get("descuento").toString());
                montoHistorico += Long.parseLong(obj.get("total").toString());
                liqui5 += Long.parseLong(obj.get("liqui5").toString());
                liqui10 += Long.parseLong(obj.get("liqui10").toString());
                gra5 += Integer.parseInt(obj.get("grav5").toString());
                gra10 += Integer.parseInt(obj.get("grav10").toString());
                exe += Integer.parseInt(obj.get("exenta").toString());
                montoDesc += Long.parseLong(obj.get("descuento").toString());
            }

            JSONObject jsonFact1 = new JSONObject();

            long iva5 = 0;
            long iva10 = 0;
            long exenta = 0;
            if (!jsonDatos.isNull("facturaClienteDets")) {
                JSONArray jsonDetalle = (JSONArray) parser.parse(json.get("facturaClienteDets").toString());
                for (int j = 0; j < jsonDetalle.size(); j++) {
                    JSONObject jsonDet = (JSONObject) parser.parse(jsonDetalle.get(j).toString());
                    if (jsonDet.get("poriva").toString().equalsIgnoreCase("5")) {
                        iva5 += (Long.parseLong(jsonDet.get("precio").toString()) * Double.parseDouble(jsonDet.get("cantidad").toString()));
                    } else if (jsonDet.get("poriva").toString().equalsIgnoreCase("10")) {
                        iva10 += (Long.parseLong(jsonDet.get("precio").toString()) * Double.parseDouble(jsonDet.get("cantidad").toString()));
                    } else {
                        exenta += (Long.parseLong(jsonDet.get("precio").toString()) * Double.parseDouble(jsonDet.get("cantidad").toString()));
                    }
                }
            }

            jsonFact1.put("nroFactura", json.get("nroFactura"));
            Date date = new Date(Utilidades.objectToTimestamp(json.get("fechaEmision")).getTime());
            jsonFact1.put("fechaEmision", dateFormat.format(date) + " " + formatter.format(date));

            jsonFact1.put("horaEmision", formatter.format(date));
            jsonFact1.put("nroCaja", jsonCaja.get("nroCaja"));

            jsonFact1.put("iva5", gra5);
            jsonFact1.put("iva10", gra10);
            jsonFact1.put("exenta", exe);
            jsonFact1.put("liq5", liqui5);
            jsonFact1.put("liq10", liqui10);

            jsonFact1.put("descAplicado", montoDesc);
            jsonFact1.put("gr5", iva5);
            jsonFact1.put("gr10", iva10);
            jsonFact1.put("exe", exenta);

            jsonFact1.put("subRepNomFun", Identity.getNomFun());
            Date dates = new Date();
            Timestamp ts = new Timestamp(dates.getTime());
            String fechaArray[] = ts.toString().split(" ");
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
            jsonFact1.put("subRepTimestamp", subRepTimestamp);

            newJSONArray.add(jsonFact1);
        }
        return newJSONArray;
    }

    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    private void informandoVentas(String jsonString, boolean datos
    ) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            InputStream subImgCP = this.getClass().getResourceAsStream("/vista/img/logosiv.png");
            if (datos) {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_DETALLE.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_DETALLE.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            } else {
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REPORTE_VENTA_SIN_DETALLE.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REPORTE_VENTA_SIN_DETALLE.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
            }

//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            System.out.println("-> repJsonString " + jsonString);
            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
            pSQL.put("repFechaDesde", Utilidades.ordenandoFechaString(datePickerFechaInicio.getValue().toString()));
            pSQL.put("repFechaHasta", Utilidades.ordenandoFechaString(datePickerFechaFin.getValue().toString()));
            // CABECERA - SUB REPORTE
            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("subRepEmpresa", suc.getEmpresa().getDescripcionEmpresa());
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            Date date = new Date();
//            Timestamp ts = new Timestamp(date.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }
    //REPORTE REPORTE REPORTE ************************** -> -> -> -> -> -> -> ->

    private void exportar() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturas(txtNUmFactura.getText(), datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA");
            } else {
                try {
                    JSONArray array = editandoFactuasJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}", true);
                    System.out.println("-->> {\"ventas\": " + array + "}");
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteVentaFXML.fxml", 749, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void exportarFactura() {
        if (datePickerFechaInicio.getValue() == null || datePickerFechaFin.getValue() == null) {
            mensajeAlerta("SELECCIONE UN RANGO DE FECHAS.");
        } else if (cbNroCaja.getSelectionModel().isEmpty()) {
            mensajeAlerta("SELECCIONE UN NUMERO DE CAJA.");
        } else {
            JSONArray jsonFacturas = recuperarFacturasCabecera(txtNUmFactura.getText(), datePickerFechaInicio.getValue().toString(), datePickerFechaFin.getValue().toString());

            if (jsonFacturas.isEmpty()) {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA.");
            } else {
                try {
                    JSONArray array = editandoFactuasVentaJSONArray(jsonFacturas);
                    informandoVentas("{\"ventas\": " + array + "}", false);
                    this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/reporteVenta/ReporteVentaFXML.fxml", 749, 275, true);
                } catch (ParseException ex) {
                    Logger.getLogger(ReporteVentaFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void preparandoDatePicker() {
        datePickerFechaInicio.setEditable(false);
        datePickerFechaInicio.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (datePickerFechaFin.getValue() != null) {
                            if (item.isAfter(datePickerFechaFin.getValue())) {
                                setDisable(true);
                                setStyle("-fx-background-color: #ffc0cb;");
                            }
                        }
                    }
                };
            }
        };
        datePickerFechaInicio.setDayCellFactory(dayCellFactoryIni);
        final Callback<DatePicker, DateCell> dayCellFactoryFin
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isBefore(
                                datePickerFechaInicio.getValue())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFin.setEditable(false);
        datePickerFechaFin.setDayCellFactory(dayCellFactoryFin);
    }

}
