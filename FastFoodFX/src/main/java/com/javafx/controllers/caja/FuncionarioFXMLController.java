/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Tempes
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.dao.FuncionarioDAO;
import com.peluqueria.dto.FuncionarioDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class FuncionarioFXMLController extends BaseScreenController implements Initializable {

    public static boolean isFuncionarioSi() {
        return funcionarioSi;
    }

    public static JSONObject getJsonFuncionario() {
        return jsonFuncionario;
    }

    private JSONArray funcionarioJSONArray;
    private ObservableList<JSONObject> funcionarioData;
    private List<JSONObject> funcionarioList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    ;
    private boolean escucha;
    private boolean alert;
    private static boolean funcionarioSi;
    boolean tachar;

    @Autowired
    FuncionarioDAO funDAO;
    //json cliente, necesario en forma de pago...
    public static JSONObject jsonFuncionario;
    //json cliente, necesario en forma de pago...

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelFuncionarioBuscar;
    @FXML
    private Label labelNombreFuncionario;
    @FXML
    private Label labelCiFuncionario;
    @FXML
    private TextField textFieldCiFuncionario;
    @FXML
    private TextField textFieldNombreFuncionario;
    @FXML
    private Button buttonBuscarFuncionario;
    @FXML
    private TableView<JSONObject> tableViewFuncionario;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCIFunc;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNombre;
    @FXML
    private TableColumn<JSONObject, String> tableColumnApellido;
    @FXML
    private TableColumn<JSONObject, CheckBox> tableColumnActivo;
    @FXML
    private TableColumn<JSONObject, RadioButton> tableColumnHabilitado;
    @FXML
    private TextField textFieldCiBuscar;
    @FXML
    private TextField textFieldNombreBuscar;
    @FXML
    private Button buttonAnhadir;
    @FXML
    private Button buttonVolver;
    @FXML
    private Label labelBorrar;
    @FXML
    private Button buttonBorrar;
    @FXML
    private GridPane gridPane;
    @FXML
    private AnchorPane anchorPaneFuncionario;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonBuscarFuncionarioAction(ActionEvent event) {
        buscandoFuncionario();
    }

    @FXML
    private void buttonAnhadirAction(ActionEvent event) {
        navegandoFormaPago();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonBorrarAction(ActionEvent event) {
        resetParam();
        navegandoFormaPagoDesdeDelete();
    }

    @FXML
    private void anchorPaneFuncionarioKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        buttonAnhadir.setDisable(true);
        escucha = false;
        alert = false;
        funcionarioSi = false;
        tachar = false;
        if (getJsonFuncionario() == null) {
            labelBorrar.setVisible(false);
            buttonBorrar.setVisible(false);
        } else {
            try {
                labelBorrar.setText("BORRAR SELECCION PREVIA" + " - " + jsonFuncionario.get("nombre").toString().toUpperCase() + " " + jsonFuncionario.get("apellido").toString().toUpperCase());
            } catch (Exception e) {
                labelBorrar.setText("BORRAR SELECCION PREVIA" + " - " + jsonFuncionario.get("nombre").toString().toUpperCase());
            }
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        boolean habilitado = true;
        if (jsonFuncionario.get("habilitado") != null) {
            habilitado = (Boolean) jsonFuncionario.get("habilitado");
        }
        if (!(Boolean) jsonFuncionario.get("activo") /*|| !habilitado*/) {
            mensajeError("EL FUNCIONARIO NO ESTÁ EN CONDICIONES.");
        } else {
            funcionarioSi = true;
            this.sc.loadScreen("/vista/caja/formaPagoFXML.fxml", 1286, 720, "/vista/caja/FuncionarioFXML.fxml", 890, 410, true);
        }
    }

    private void navegandoFormaPagoDesdeDelete() {
        funcionarioSi = true;
        this.sc.loadScreen("/vista/caja/formaPagoFXML.fxml", 1286, 720, "/vista/caja/FuncionarioFXML.fxml", 890, 410, true);
    }

    private void volviendo() {
        funcionarioSi = false;
        this.sc.loadScreen("/vista/caja/formaPagoFXML.fxml", 1286, 720, "/vista/caja/FuncionarioFXML.fxml", 890, 410, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.CLOSE);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.CLOSE) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (alert) {
                alert = false;
            } else {
                buscandoFuncionario();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                if (!buttonBorrar.isVisible()) {
                    resetParam();
                }
                navegandoFormaPagoDesdeDelete();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else if (!buttonAnhadir.isDisable()) {
                navegandoFormaPago();
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            } else if (buttonBorrar.isVisible()) {
                resetParam();
                navegandoFormaPagoDesdeDelete();
            }
        }
    }

    private void escucha() {
        escucha = true;
        tableViewFuncionario.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewFuncionario.getSelectionModel().getSelectedItem() != null) {
                if (newSelection.get("nombre") != null) {
                    if (newSelection.get("apellido") != null) {
                        textFieldNombreBuscar.setText(newSelection.get("nombre").toString().toUpperCase() + " " + newSelection.get("apellido").toString().toUpperCase());
                    } else {
                        textFieldNombreBuscar.setText(newSelection.get("nombre").toString().toUpperCase());
                    }
                } else if (newSelection.get("apellido") != null) {
                    textFieldNombreBuscar.setText(newSelection.get("apellido").toString().toUpperCase());
                } else {
                    textFieldNombreBuscar.setText("");
                }
                if (newSelection.get("ci") != null) {
                    textFieldCiBuscar.setText(newSelection.get("ci").toString());
                } else {
                    textFieldCiBuscar.setText("");
                }
                if (buttonAnhadir.isDisable()) {
                    buttonAnhadir.setDisable(false);
                }
                seteandoParam(newSelection);
            } else {
                textFieldNombreBuscar.setText("");
                textFieldCiBuscar.setText("");
                buttonAnhadir.setDisable(true);
                resetParam();
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoFuncionario() {
        String nom, ape, ci;
        if (textFieldNombreFuncionario.getText().contentEquals("")) {
            nom = "null";
        } else {
            nom = textFieldNombreFuncionario.getText();
        }
        if (textFieldCiFuncionario.getText().contentEquals("")) {
            ci = "null";
        } else {
            ci = textFieldCiFuncionario.getText();
        }
        ape = "null";
        funcionarioList = jsonArrayFuncionario(nom, ape, ci);
        actualizandoTablaFuncionario();
        if (!tableViewFuncionario.getItems().isEmpty()) {
            tableViewFuncionario.requestFocus();
            tableViewFuncionario.getSelectionModel().select(0);
            tableViewFuncionario.getFocusModel().focus(0);
        }
    }

    public static void resetParam() {
        jsonFuncionario = null;
        funcionarioSi = false;
    }

    private void seteandoParam(JSONObject funcionario) {
        jsonFuncionario = funcionario;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, FUNCIONARIO -> GET
    private List<JSONObject> jsonArrayFuncionario(String nom, String ape, String ci) {
        //límite de registros en lado backend...
        //carácter "espacio", formato...
        nom = nom.replaceAll(" ", "%20");
        ape = ape.replaceAll(" ", "%20");
        ci = ci.replaceAll(" ", "%20");
        //carácter "espacio", formato...
//        JSONParser parser = new JSONParser();
        List<JSONObject> funcionarioJSONObjList = new ArrayList<JSONObject>();
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 20)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/funcionario/" + nom + "/" + ape + "/" + ci + "");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                String inputLine;
//                while ((inputLine = br.readLine()) != null) {
//                    funcionarioJSONArray = (JSONArray) parser.parse(inputLine);
//                    for (Object funcionarioObj : funcionarioJSONArray) {
//                        JSONObject clienteFielJSONObj = (JSONObject) funcionarioObj;
//                        funcionarioJSONObjList.add(clienteFielJSONObj);
//                    }
//                }
//                br.close();
//            } else {
        funcionarioJSONObjList = generarListaFuncionariosLocal(nom, ape, ci);
//            }
//        } catch (IOException | ParseException ex) {
//            funcionarioJSONObjList = generarListaFuncionariosLocal(nom, ape, ci);
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        return funcionarioJSONObjList;
    }
    //////READ, FUNCIONARIO -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, FUNCIONARIO
    public List<JSONObject> generarListaFuncionariosLocal(String nom, String ape, String ci) {
        JSONParser parser = new JSONParser();
        List<Funcionario> fun = funDAO.filtrarNomApeCi(nom, ape, ci);
        List<JSONObject> listaFuncionario = new ArrayList<>();
        for (Funcionario f : fun) {
            try {
                FuncionarioDTO fdto = f.toFuncionarioDTO();
                String x = gson.toJson(fdto);
                JSONObject jsonFuncionario = (JSONObject) parser.parse(x);
                listaFuncionario.add(jsonFuncionario);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return listaFuncionario;
    }
    //////READ, FUNCIONARIO
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaFuncionario() {
        funcionarioData = FXCollections.observableArrayList(funcionarioList);
        //columna Ci .................................................
        tableColumnCIFunc.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCIFunc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(String.valueOf(data.getValue().get("ci")));
            }
        });
        //columna Ci .................................................
        //columna Nombre ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                boolean habilitado = true;
                if (data.getValue().get("habilitado") != null) {
                    habilitado = (boolean) data.getValue().get("habilitado");
                }
                if (!(Boolean) data.getValue().get("activo") || /*!(Boolean) data.getValue().get("habilitado")*/ !habilitado) {//campo habilitado, por el momento en null (true)
                    tachar = true;
                } else {
                    tachar = false;
                }
                return new ReadOnlyStringWrapper(String.valueOf(data.getValue().get("nombre")));
            }
        });
        tableColumnNombre.setCellFactory(new Callback<TableColumn<JSONObject, String>, TableCell<JSONObject, String>>() {
            @Override
            public TableCell<JSONObject, String> call(TableColumn<JSONObject, String> param) {
                return new TableCell<JSONObject, String>() {
                    @Override
                    public void updateItem(String item, final boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            if (tachar) {
                                setEffect(new DropShadow(10, Color.RED));
                            } else {
                                setEffect(new DropShadow(10, Color.GREEN));
                            }
                            setText(item.toString());
                        }
                    }
                };
            }
        });
        //columna Nombre ..................................................
        //columna Apellido .....................................................
        tableColumnApellido.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnApellido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ape = "";
                if (data.getValue().get("apellido") != null) {
                    ape = String.valueOf(data.getValue().get("apellido"));
                } else {
                    ape = "";
                }
                return new ReadOnlyStringWrapper(ape);
            }
        });
        tableColumnApellido.setCellFactory(new Callback<TableColumn<JSONObject, String>, TableCell<JSONObject, String>>() {
            @Override
            public TableCell<JSONObject, String> call(TableColumn<JSONObject, String> param) {
                return new TableCell<JSONObject, String>() {
                    @Override
                    public void updateItem(String item, final boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            if (tachar) {
                                setEffect(new DropShadow(10, Color.RED));
                            } else {
                                setEffect(new DropShadow(10, Color.GREEN));
                            }
                            setText(item.toString());
                        }
                    }
                };
            }
        });
        //columna Apellido .....................................................
        //columna Activo .......................................................
        tableColumnActivo.setEditable(false);
        tableColumnActivo.setStyle("-fx-alignment: CENTER;");
        tableColumnActivo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                Boolean activo = (Boolean) data.getValue().get("activo");
                CheckBox cb = new CheckBox();
                cb.setSelected(activo);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<CheckBox>(cb);
            }
        });
        //columna Activo .......................................................
        //columna Habilitado ...................................................
        tableColumnHabilitado.setEditable(false);
        tableColumnHabilitado.setStyle("-fx-alignment: CENTER;");
        tableColumnHabilitado.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, RadioButton>, ObservableValue<RadioButton>>() {
            @Override
            public ObservableValue<RadioButton> call(TableColumn.CellDataFeatures<JSONObject, RadioButton> data) {
                Boolean habilitado = true;
                if (data.getValue().get("habilitado") != null) {//campo habilitado, por el momento en null (true)
                    habilitado = (Boolean) data.getValue().get("habilitado");
                }
                RadioButton rb = new RadioButton();
                rb.setSelected(habilitado);
                rb.setDisable(true);
                rb.setStyle("-fx-opacity: 1");
                if (rb.isSelected()) {
                    rb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    rb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<RadioButton>(rb);
            }
        });
        //columna Habilitado ...................................................
        tableViewFuncionario.setItems(funcionarioData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
}
