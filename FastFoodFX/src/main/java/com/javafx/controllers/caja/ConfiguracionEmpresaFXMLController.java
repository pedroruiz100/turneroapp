/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.NumberValidator;
import com.javafx.util.Ticket;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Empresa;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.core.domain.FacturaClienteDet;
import com.peluqueria.core.domain.Sucursal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.core.domain.Timbrado;
import com.peluqueria.core.domain.TipoComprobante;
import com.peluqueria.dao.EmpresaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteDetDAO;
import com.peluqueria.dao.SucursalDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TimbradoDAO;
import com.peluqueria.dao.TipoComprobanteDAO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.FacturaClienteDetDTO;
import com.peluqueria.dto.TipoComprobanteDTO;
import java.net.URL;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ConfiguracionEmpresaFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private TipoComprobanteDAO tipoComprobanteDAO;
    @Autowired
    private FacturaClienteCabHistoricoDAO fHistoricoDAO;
    @Autowired
    private FacturaClienteDetDAO fcDetDAO;
    @Autowired
    private EmpresaDAO empresaDAO;
    @Autowired
    private SucursalDAO sucursalDAO;
    @Autowired
    private TimbradoDAO timbradoDAO;
    @Autowired
    private TalonariosSucursaleDAO talSucuDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    private ToggleGroup group;
    private String nroFactFiltro;
    private String fechaFactFiltro;
    private String nroCajaFiltro;
    private String timbradoFiltro;
    private String tipoFactFiltro;
    private boolean cargarCaja;
    private boolean cargarTimbrado;
    private boolean cargarTipoFact;
    JSONObject jsonFactCabHist;
    private Ticket ticket;
    private boolean alert;
    private JSONArray cajasJSONArray;
    private JSONArray tipoFactJSONArray;
    private JSONArray timbradosJSONArray;
    private NumberValidator numValidator;
    private final String patternFechaHora = "dd/MM/yyyy HH:mm";
    private final String patternFecha = "dd/MM/yyyy";
    private HashMap<String, String> hashJsonTipoComp;
    private String contentTicket
            = "{{empresa}} \n"
            + "{{sucRuc}}\n"
            + "{{tel}}\n"
            + "{{dir}}\n"
            + "{{dup}}\n"
            + "{{dupSinVC}}"
            + "======================================\n"
            + "TIMBRADO NRO. {{timb}} \n"
            + "INICIO VIGENCIA: {{desde}}\n"
            + "FIN VIGENCIA: {{hasta}}\n"
            + "CAJA INTERNA: {{cj}} FECHA: {{fH}}\n"
            + "FACTURA CONTADO: {{factNro}}\n"
            + "CAJERO: {{usuario}}\n"
            + "======================================\n"
            + "Articulo  Cant.  Precio      Total\n"
            + "{{art}}\n"
            + "===DETALLE FISCAL========================\n"
            + "GRAVADA 10% :  {{grav10}}\n"
            + "GRAVADA 5%  :  {{grav5}}\n"
            + "EXENTA      :  {{exenta}}\n"
            + "===LIQUIDACION DE IVA=====================\n"
            + "10%:  {{grav10Liq}}\n"
            + " 5%:   {{grav5Liq}}\n"
            + "======================================\n"
            + "RUC: {{rucCliente}}\n"
            + "CLIENTE: \n"
            + "{{cliente}}\n"
            + "======================================\n"
            + "ARTICULOS: {{cant}}TOTAL: {{total}}\n"
            + "DESCUENTO: {{desc}} NETO: {{neto}}\n"
            + "======================================\n"
            + "{{efectivo}}"
            + "{{dolar}}"
            + "{{peso}}"
            + "{{real}}"
            + "{{tarjCred}}"
            + "{{tarjDeb}}"
            + "{{cheque}}"
            + "{{vale}}"
            + "{{asoc}}"
            + "{{notCre}}"
            + "{{retencion}}"
            + "{{giftcard}}"
            + "DONACION: {{redondeo}}\n"
            + "VUELTO: {{vuelto}}";
    private String empresa;
    private String sucursal;
    private String ruc;
    private String telef;
    private String direccion;
    private String nroTimbrado;
    private String fecInicial;
    private String fecVencimiento;
    private String nroCaja;
    private String fechaEmision;
    private String nroFactura;
    private String usuAlta;
    private String detalleArticulo;
    private String grav10;
    private String grav5;
    private String exe;
    private String liqIva10;
    private String liqIva5;
    private String rucCliente;
    private String cliente;
    private String cantTotalArt;
    private String total;
    private String descuento;
    private String neto;
    private String efectivo;
    private String giftcard;
    private String tarjCred;
    private String tarjDeb;
    private String cheque;
    private String vale;
    private String asoc;
    private String notCre;
    private String redondeo;
    private String vuelto;
    private String dolar;
    private String peso;
    private String realb;
    private String retencion;

    //FXML FXML FXML ****************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneCab;
    @FXML
    private Label labelReimp;
    @FXML
    private AnchorPane anchorPaneFiltro;
    @FXML
    private Label labelNroFact;
    @FXML
    private Label labelTipoFact;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private GridPane gridPaneFiltro;
    @FXML
    private TextField txtEmpresa;
    @FXML
    private TextField txtRucEmpresa;
    @FXML
    private GridPane gridPaneFiltro1;
    @FXML
    private Label labelNroFact1;
    @FXML
    private TextField txtPrimero;
    @FXML
    private TextField txtSegundo;
    @FXML
    private TextField txtFactura;
    @FXML
    private GridPane gridPaneFiltro2;
    @FXML
    private Label labelNroFact2;
    @FXML
    private TextField txtTimbrado;
    @FXML
    private CheckBox chkLogin;
    @FXML
    private Button btnProcesar;
    @FXML
    private Label labelNroFact22;
    @FXML
    private GridPane gridPaneFiltro21;
    @FXML
    private Label labelNroFact21;
    @FXML
    private Label labelTipoFact11;
    @FXML
    private GridPane gridPaneFiltro22;
    @FXML
    private Label labelNroFact222;
    @FXML
    private DatePicker dpInicio;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtDireccion;
    @FXML
    private DatePicker dpFin;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void buttonVerFactAction(ActionEvent event) {
        observandoCopia();
    }

    private void buttonImpFactAction(ActionEvent event) {
        imprimiendoCopia();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonAnteriorAction(ActionEvent event) {
        buttonAnte();
    }

    private void buttonSgteAction(ActionEvent event) {
        buttonSgte();
    }

    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoFiltros();
    }

    private void comboBoxTipoFactMouseClicked(MouseEvent event) {
        mouseEventComboBoxTipoFact(event);
    }

    private void comboBoxTimbradoMouseClicked(MouseEvent event) {
        mouseEventComboBoxTimbrado(event);
    }

    private void comboBoxCajaMouseClicked(MouseEvent event) {
        mouseEventComboBoxCaja(event);
    }

    private void comboBoxTipoFactKeyReleased(KeyEvent event) {
        keyPressComboBoxTipoFact(event);
    }

    private void comboBoxCajaKeyReleased(KeyEvent event) {
        keyPressComboBoxCaja(event);
    }

    private void comboBoxTimbradoKeyReleased(KeyEvent event) {
        keyPressComboBoxTimbrado(event);
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        animando();
        Empresa empresa = empresaDAO.getById(1L);
        txtEmpresa.setText(empresa.getDescripcionEmpresa());
        txtRucEmpresa.setText(empresa.getRuc());
        txtTelefono.setText(empresa.getTelefono());

        Timbrado timbrado = timbradoDAO.getById(1L);
        txtTimbrado.setText(timbrado.getNroTimbrado());
        dpInicio.setValue(java.sql.Date.valueOf(timbrado.getFecInicial() + "").toLocalDate());
        dpFin.setValue(java.sql.Date.valueOf(timbrado.getFecVencimiento() + "").toLocalDate());

        Sucursal sucursal = sucursalDAO.getById(1L);
        txtDireccion.setText(sucursal.getCallePrincipal());

        TalonariosSucursales talSucu = talSucuDAO.getById(1L);
        txtFactura.setText(talSucu.getNroActual() + "");
        txtPrimero.setText(talSucu.getPrimero());
        txtSegundo.setText(talSucu.getSegundo());

        boolean estado = recuperarEstadoSinLogin();
        chkLogin.setSelected(!estado);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/caja/ReimpresionFXML.fxml", 1020, 580, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            observandoCopia();
        }
        if (keyCode == event.getCode().F2) {
            procesarDatos();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoFiltros();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    private void keyPressComboBoxCaja(KeyEvent event) {
    }

    private void mouseEventComboBoxCaja(MouseEvent event) {
        if (cargarCaja) {
            String result = jsonCajas();
            if (cajasJSONArray.isEmpty()) {
                mensajeError(result);
                cargarCaja = true;
            }
        }
    }

    private void keyPressComboBoxTimbrado(KeyEvent event) {
    }

    private void mouseEventComboBoxTimbrado(MouseEvent event) {
        if (cargarTimbrado) {
            String result = jsonTimbrados();
            if (timbradosJSONArray.isEmpty()) {
                mensajeError(result);
                cargarTimbrado = true;
            }
        }
    }

    private void keyPressComboBoxTipoFact(KeyEvent event) {
    }

    private void mouseEventComboBoxTipoFact(MouseEvent event) {
        if (cargarTipoFact) {
            String result = jsonTipoComprobante();
            if (tipoFactJSONArray.isEmpty()) {
                mensajeError(result);
                cargarTipoFact = true;
            }
        }
    }

    private void buttonGroup() {
        group = new ToggleGroup();
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //NAVEGAR IMPRESIÓN
    private void buttonSgte() {
    }

    private void buttonAnte() {
    }
    //NAVEGAR IMPRESIÓN

    private void reseteandoDatosImpresion() {
        empresa = "";
        sucursal = "";
        ruc = "";
        telef = "";
        direccion = "";
        nroTimbrado = "";
        fecInicial = "";
        fecVencimiento = "";
        nroCaja = "";
        fechaEmision = "";
        nroFactura = "";
        usuAlta = "";
        detalleArticulo = "";
        grav10 = "";
        grav5 = "";
        exe = "";
        liqIva10 = "";
        liqIva5 = "";
        rucCliente = "";
        cliente = "";
        cantTotalArt = "";
        total = "";
        descuento = "";
        neto = "";
        efectivo = "";
        giftcard = "";
        tarjCred = "";
        tarjDeb = "";
        cheque = "";
        vale = "";
        asoc = "";
        notCre = "";
        redondeo = "";
        vuelto = "";
        dolar = "";
        peso = "";
        realb = "";
        retencion = "";
    }

    private void iniciandoCB() {
    }

    private void ofuscando() {
    }

    private void observandoCopia() {

    }

    private void limpiandoFiltros() {
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, FACTURA CAB. HISTÓRICO -> GET
    private JSONObject jsonFactCabHist() {
        JSONParser parser = new JSONParser();
        JSONObject factCabHist = new JSONObject();
//        JSONObject jsonFactCab = (JSONObject) jsonFactCabHist.get("facturaClienteCab");
//                    JSONArray jsonDetArray = (JSONArray) jsonFactCab.get("facturaClienteDets");
        FacturaClienteCabHistorico facHistorico = fHistoricoDAO.consultar(nroFactFiltro, tipoFactFiltro, fechaFactFiltro, nroCajaFiltro, timbradoFiltro);
        if (facHistorico != null) {
            try {
                FacturaClienteCabHistoricoDTO fccHistoricoDTO = facHistorico.toFacturaClienteCabDTO();
                FacturaClienteCab fcc = facHistorico.getFacturaClienteCab();
                FacturaClienteCabDTO fccDTO = fccDAO.getById(fcc.getIdFacturaClienteCab()).toBDFacturaClienteCabDTO();
                Timestamp fecEmi = fccDTO.getFechaEmision();
//                Timestamp fecMod = fccDTO.getFechaMod();
                fccDTO.setFechaEmision(null);
                fccDTO.setFechaMod(null);
                fccDTO.setFacturaClienteCabHistorico(null);
                JSONObject jsonCabecera = (JSONObject) parser.parse(gson.toJson(fccDTO));
                jsonCabecera.put("fechaEmision", fecEmi.toString());

                List<FacturaClienteDet> listFcDet = fcDetDAO.listarPorFactura(fcc.getIdFacturaClienteCab());
                JSONArray array = new JSONArray();
                if (!listFcDet.isEmpty()) {
                    for (FacturaClienteDet facturaClienteDet : listFcDet) {
                        FacturaClienteDetDTO fcDetDTO = facturaClienteDet.toDescriFacturaClienteDetDTO();
                        JSONObject jsonDet = (JSONObject) parser.parse(gson.toJson(fcDetDTO));
                        array.add(jsonDet);
                    }
                    jsonCabecera.put("facturaClienteDets", array);
                }

                factCabHist = (JSONObject) parser.parse(gson.toJson(fccHistoricoDTO));
                factCabHist.put("facturaClienteCab", jsonCabecera);
            } catch (ParseException ex) {
                System.out.println("ERROR->> " + ex.getLocalizedMessage());
                System.out.println("ERROR->> " + ex.getMessage());
                System.out.println("ERROR->> " + ex.fillInStackTrace());
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/consultar/"
//                    + nroFactFiltro + "/" + tipoFactFiltro + "/" + fechaFactFiltro + "/" + nroCajaFiltro + "/" + timbradoFiltro);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                factCabHist = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return factCabHist;
    }
    //////READ, FACTURA CAB. HISTÓRICO -> GET

    //////READ, FACTURA CAB. HISTÓRICO CAJAS -> GET
    private String jsonCajas() {
        cajasJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        String result = "ok";
        List<Integer> listHist = fHistoricoDAO.listarCajaDistinct();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON CAJAS DISPONIBLES.";
        } else {
            for (Integer integer : listHist) {
                cajasJSONArray.add(integer);
//                comboBoxCaja.getItems().add(integer.toString());
            }
        }
//        FacturaClienteCabHistorico facHistorico = fHistoricoDAO.consultar(nroFactFiltro, tipoFactFiltro, fechaFactFiltro, nroCajaFiltro, timbradoFiltro);
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/caja");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                cajasJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarCaja = false;
//                if (cajasJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON CAJAS DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "LA INFORMACIÓN SOLICITADA NO EXISTE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object cajaObj : cajasJSONArray) {
//            Long cajaNro = (Long) cajaObj;
//            comboBoxCaja.getItems().add(cajaNro.toString());
//        }
        return result;
    }
    //////READ, FACTURA CAB. HISTÓRICO CAJAS -> GET

    //////READ, FACTURA CAB. HISTÓRICO TIMBRADOS -> GET
    private String jsonTimbrados() {
        timbradosJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        String result = "ok";
        List<String> listHist = fHistoricoDAO.listarTimbDistinct();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON TIMBRADOS DISPONIBLES.";
        } else {
            for (String timbrado : listHist) {
                timbradosJSONArray.add(timbrado);
//                comboBoxTimbrado.getItems().add(timbrado);
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/timbrado");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                timbradosJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarTimbrado = false;
//                if (timbradosJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON TIMBRADOS DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "LA INFORMACIÓN SOLICITADA NO EXISTE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object timbradoObj : timbradosJSONArray) {
//            String timbrado = (String) timbradoObj;
//            comboBoxTimbrado.getItems().add(timbrado);
//        }
        return result;
    }
    //////READ, FACTURA CAB. HISTÓRICO TIMBRADOS -> GET

    //////READ, TIPO COMPROBANTE -> GET
    private String jsonTipoComprobante() {
        tipoFactJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        hashJsonTipoComp = new HashMap<>();
        String result = "ok";
        List<TipoComprobante> listHist = tipoComprobanteDAO.listar();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
        } else {
//            JSONObject tipoFactJSONObj = (JSONObject) tipoFactObj;
//            hashJsonTipoComp.put(tipoFactJSONObj.get("descripcion").toString(), tipoFactJSONObj.get("idTipoComprobante").toString());
//            comboBoxTipoFact.getItems().add(tipoFactJSONObj.get("descripcion").toString());
            for (TipoComprobante comprobante : listHist) {
                try {
                    TipoComprobanteDTO compDTO = comprobante.toTipoComprobanteDTO();
                    compDTO.setFechaAlta(null);
                    compDTO.setFechaMod(null);
                    JSONObject factCabHist = (JSONObject) parser.parse(gson.toJson(compDTO));
                    tipoFactJSONArray.add(factCabHist);
                    hashJsonTipoComp.put(comprobante.getDescripcion(), comprobante.getIdTipoComprobante().toString());
//                    comboBoxTipoFact.getItems().add(comprobante.getDescripcion());
                } catch (ParseException ex) {
                    System.out.println("ERROR->> " + ex.getLocalizedMessage());
                    System.out.println("ERROR->> " + ex.getMessage());
                    System.out.println("ERROR->> " + ex.fillInStackTrace());
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/tipoComprobante");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                tipoFactJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarTipoFact = false;
//                if (tipoFactJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object tipoFactObj : tipoFactJSONArray) {
//            JSONObject tipoFactJSONObj = (JSONObject) tipoFactObj;
//            hashJsonTipoComp.put(tipoFactJSONObj.get("descripcion").toString(), tipoFactJSONObj.get("idTipoComprobante").toString());
//            comboBoxTipoFact.getItems().add(tipoFactJSONObj.get("descripcion").toString());
//        }
        return result;
    }
    //////READ, TIPO COMPROBANTE -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //IMPRESIÓN MINORISTA -> -> -> *********************************************
    private void imprimiendoCopia() {
//        if (!buttonImpFact.isDisable()) {
//            usuAlta = Utilidades.encodingAlambrado(usuAlta);
//            ticket = new Ticket();
//            ticket.ticketReimpresion(empresa, sucursal, ruc, telef, direccion, nroTimbrado, fecInicial, fecVencimiento, nroCaja,
//                    fechaEmision, nroFactura, usuAlta, detalleArticulo, grav10, grav5, exe, liqIva10, liqIva5, rucCliente, cliente,
//                    cantTotalArt, total, descuento, neto, efectivo, tarjCred, tarjDeb, cheque, vale, asoc, notCre,
//                    redondeo, vuelto, dolar, peso, realb, retencion, "DUPLICADO", radioDuplicado.isSelected(), giftcard);
//            ticket.print();
//        }
    }
    //IMPRESIÓN MINORISTA -> -> -> *********************************************

    //VISUALIZACIÓN MINORISTA
    private void viendoMinorista(String empresa, String suc, String ruc, String tel, String dir, String timb, String desde, String hasta,
            String nroCaja, String fH, String factNro, String user, String articulos, String grav10, String grav5, String exenta,
            String grav10Liq, String grav5Liq, String rucCliente, String cliente, String cant, String total, String desc,
            String neto, String efectivo, String tarjCred, String tarjDeb, String cheque, String vale, String asoc, String notCre, String redondeo,
            String vuelto, String dolar, String peso, String real, String retencion, String giftcard) {
        String ticket = this.contentTicket;
        ticket = ticket.replace("{{empresa}}", StringUtils.center(empresa, 80));
        ticket = ticket.replace("{{sucRuc}}", StringUtils.center(suc + " - RUC: " + ruc, 65));
//        TEL. {{tel}}-COMERCIO RAMOS GENERALES
        ticket = ticket.replace("{{tel}}", StringUtils.center("TEL. " + tel + "-COMERCIO RAMOS GENERALES", 60));
        ticket = ticket.replace("{{dir}}", StringUtils.center(dir, 65));
        ticket = ticket.replace("{{timb}}", timb);
        ticket = ticket.replace("{{desde}}", desde);
        ticket = ticket.replace("{{hasta}}", hasta);
        ticket = ticket.replace("{{cj}}", nroCaja);
        ticket = ticket.replace("{{fH}}", fH);
        ticket = ticket.replace("{{dup}}", StringUtils.center("**DUPLICADO**", 75));
//        if (radioDuplicadoSinValorC.isSelected()) {
//            ticket = ticket.replace("{{dupSinVC}}", "                       ** SIN VALOR COMERCIAL **\n");
//        } else {
//            ticket = ticket.replace("{{dupSinVC}}", "");
//        }
        ticket = ticket.replace("{{factNro}}", factNro);
        ticket = ticket.replace("{{usuario}}", user);
        ticket = ticket.replace("{{art}}", articulos);
        ticket = ticket.replace("{{grav10}}", grav10);
        ticket = ticket.replace("{{grav5}}", grav5);
        ticket = ticket.replace("{{exenta}}", exenta);
        ticket = ticket.replace("{{grav10Liq}}", grav10Liq);
        ticket = ticket.replace("{{grav5Liq}}", grav5Liq);
        ticket = ticket.replace("{{rucCliente}}", rucCliente);
        ticket = ticket.replace("{{cliente}}", cliente);
        ticket = ticket.replace("{{cant}}", cant);
        ticket = ticket.replace("{{total}}", total);
        ticket = ticket.replace("{{desc}}", desc);
        ticket = ticket.replace("{{neto}}", neto);
        //**********************************************************************
        if (efectivo.contentEquals("Gs 0")) {
            efectivo = "";
        } else {
            efectivo = "EFECTIVO: " + efectivo + "\n";
        }
        if (giftcard.contentEquals("Gs 0")) {
            giftcard = "";
        } else {
            giftcard = "GIFTCARD: " + giftcard + "\n";
        }
        if (tarjCred.contentEquals("Gs 0")) {
            tarjCred = "";
        } else {
            tarjCred = "TARJ. CRED: " + tarjCred + "\n";
        }
        if (tarjDeb.contentEquals("Gs 0")) {
            tarjDeb = "";
        } else {
            tarjDeb = "TARJ. DEB: " + tarjDeb + "\n";
        }
        if (cheque.contentEquals("Gs 0")) {
            cheque = "";
        } else {
            cheque = "CHEQUE: " + cheque + "\n";
        }
        if (vale.contentEquals("Gs 0")) {
            vale = "";
        } else {
            vale = "VALE: " + vale + "\n";
        }
        if (asoc.contentEquals("Gs 0")) {
            asoc = "";
        } else {
            asoc = "ASOCIACION: " + asoc + "\n";
        }
        if (notCre.contentEquals("Gs 0")) {
            notCre = "";
        } else {
            notCre = "NOTA CREDITO: " + notCre + "\n";
        }
        if (retencion.contentEquals("Gs 0")) {
            retencion = "";
        } else {
            retencion = "RETENCION: " + retencion + "\n";
        }
        if (peso == null) {
            peso = "";
        }
        if (real == null) {
            real = "";
        }
        if (dolar == null) {
            dolar = "";
        }
        ticket = ticket.replace("{{efectivo}}", efectivo);
        ticket = ticket.replace("{{giftcard}}", giftcard);
        ticket = ticket.replace("{{tarjCred}}", tarjCred);
        ticket = ticket.replace("{{tarjDeb}}", tarjDeb);
        ticket = ticket.replace("{{cheque}}", cheque);
        ticket = ticket.replace("{{vale}}", vale);
        ticket = ticket.replace("{{asoc}}", asoc);
        ticket = ticket.replace("{{notCre}}", notCre);
        ticket = ticket.replace("{{dolar}}", dolar);
        ticket = ticket.replace("{{peso}}", peso);
        ticket = ticket.replace("{{real}}", real);
        ticket = ticket.replace("{{retencion}}", retencion);
        //**********************************************************************
        ticket = ticket.replace("{{redondeo}}", redondeo);
        ticket = ticket.replace("{{vuelto}}", vuelto);
    }
    //VISUALIZACIÓN MINORISTA

    //*****************************ANIMACIONES**********************************
    private void animando() {
        labelReimp = (Label) AnimationFX.translationNode(labelReimp, 60, 0, 1100);
        btnProcesar = (Button) AnimationFX.translationNode(btnProcesar, 60, 0, 1100);
        buttonVolver = (Button) AnimationFX.translationNode(buttonVolver, -60, 0, 1200);
        gridPaneFiltro = (GridPane) AnimationFX.translationNode(gridPaneFiltro, -60, 0, 1200);
        gridPaneFiltro2 = (GridPane) AnimationFX.translationNode(gridPaneFiltro2, 60, 0, 1100);
        gridPaneFiltro1 = (GridPane) AnimationFX.translationNode(gridPaneFiltro1, -60, 0, 1200);
//        hBoxButton = (HBox) AnimationFX.translationNode(hBoxButton, 60, 0, 1300);
//        hBoxButtonNav = (HBox) AnimationFX.translationNode(hBoxButtonNav, 0, 20, 1400);
//        buttonVolver = (Button) AnimationFX.translationNode(buttonVolver, -60, 0, 1200);
    }
    //*****************************ANIMACIONES**********************************

    private JSONArray recuperarArrayOrdenado(JSONArray jsonDetArray) {
        JSONParser parser = new JSONParser();
        org.json.JSONArray jsonArr = new org.json.JSONArray(jsonDetArray);
        org.json.JSONArray sortedJsonArray = new org.json.JSONArray();
        List<org.json.JSONObject> jsonValues = new ArrayList<>();
        for (int i = 0; i < jsonArr.length(); i++) {
            jsonValues.add(jsonArr.getJSONObject(i));
            Utilidades.log.info("JSON -> " + jsonArr.getJSONObject(i).toString());
        }
        Collections.sort(jsonValues, new Comparator<org.json.JSONObject>() {
            private static final String KEY_NAME = "idFacturaClienteDet";

            @Override
            public int compare(org.json.JSONObject a, org.json.JSONObject b) {
                String valA = new String();
                String valB = new String();

                try {
                    valA = String.valueOf(a.get(KEY_NAME));
                    valB = String.valueOf(b.get(KEY_NAME));
                } catch (JSONException e) {
                    Utilidades.log.error("ERROR JSONException REIMPRESION", e.fillInStackTrace());
                }

                return valA.compareTo(valB);
            }
        });
        for (int i = 0; i < jsonArr.length(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
            Utilidades.log.info("-> " + jsonValues.get(i));
        }
        JSONArray jsonArray;
        try {
            jsonArray = (JSONArray) parser.parse(sortedJsonArray.toString());
            return jsonArray;
        } catch (ParseException ex) {
            return null;
        }

    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        procesarDatos();
    }

    private boolean recuperarEstadoSinLogin() {
        ConexionPostgres.conectarServer();
        String sql = "SELECT * FROM general.modo_venta WHERE id_venta=1";
        System.out.println("-->> " + sql);
        boolean val = false;
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                val = rs.getBoolean("sin_login");

            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrarServer();
        return val;
    }

    private void actualizarEstado() {
        ConexionPostgres.conectarServer();
//        String sql = "UPDATE desarrollo.cabecera SET procesado=TRUE WHERE id_dato=" + datos.get("uuidCassandraActual").toString();
        String sql = "UPDATE general.modo_venta SET sin_login=" + !chkLogin.isSelected() + " WHERE id_venta=1";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz actualizado un registro HANDLER PARANA ********");
            }
            ps.close();
            ConexionPostgres.getConServer().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getConServer().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private void procesarDatos() {
        if (txtEmpresa.getText().equals("") || txtRucEmpresa.getText().equals("") || txtTimbrado.getText().equals("")
                || txtPrimero.getText().equals("") || txtSegundo.getText().equals("") || txtFactura.getText().equals("")) {
            mensajeError("TODOS LOS CAMPOS SON OBLIGATORIOS DE REGISTRAR");
        } else {
            Empresa empresa = empresaDAO.getById(1L);
            empresa.setDescripcionEmpresa(txtEmpresa.getText());
            empresa.setRuc(txtRucEmpresa.getText());
            empresa.setTelefono(txtTelefono.getText());
            empresaDAO.actualizar(empresa);

            Timbrado timbrado = timbradoDAO.getById(1L);
            timbrado.setNroTimbrado(txtTimbrado.getText());
            timbrado.setFecInicial(Date.valueOf(dpInicio.getValue()));
            timbrado.setFecVencimiento(Date.valueOf(dpFin.getValue()));
            timbradoDAO.actualizar(timbrado);

            Sucursal sucursal = sucursalDAO.getById(1L);
            sucursal.setCallePrincipal(txtDireccion.getText());
            sucursalDAO.actualizar(sucursal);

            TalonariosSucursales talSucu = talSucuDAO.getById(1L);
            talSucu.setNroActual(Long.parseLong(txtFactura.getText()));
            talSucu.setPrimero(txtPrimero.getText());
            talSucu.setSegundo(txtSegundo.getText());

            actualizarEstado();
            mensajeAdv("DATOS ACTUALIZADOS CORRECTAMENTE!.");
        }
    }
}