package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.caja.FacturaDeVentaFXMLController.creandoJsonFactDet;
import static com.javafx.controllers.caja.FacturaDeVentaFXMLController.fact;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.AnimationFX;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionParana;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.EstadoCheck;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.Identity;
import com.javafx.util.MensajeFinalVenta;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import digitalclock.DigitalClock;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author
 */
@Controller
public class FacturaDeVentaFXMLController extends BaseScreenController implements Initializable {

    public static void setTextFieldCompAnt(String aTextFieldCompAnt) {
        textFieldCompAnt = aTextFieldCompAnt;
    }

    public static boolean isActualizarDatosCabecera() {
        return actualizarDatosCabecera;
    }

    public static void setActualizarDatosCabecera(boolean actualizarDatosCabecera) {
        FacturaDeVentaFXMLController.actualizarDatosCabecera = actualizarDatosCabecera;
    }

    public static List<JSONObject> getCotizacionList() {
        return cotizacionList;
    }

    public static int getPeso() {
        return peso;
    }

    public static int getReal() {
        return real;
    }

    public static int getDolar() {
        return dolar;
    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static Long getPrecioTotal() {
        return precioTotal;
    }

    public static JSONObject getCabFactura() {
        return cabFactura;
    }

    public static void setCabFactura(JSONObject aCabFactura) {
        cabFactura = aCabFactura;
    }

    public static boolean isCancelacionProd() {
        return cancelacionProd;
    }

    private static JSONObject cabFactura;
//    private static Stage stageData = new Stage();

    static void setCliente(String ruc, String nombre) {
        nomCli = nombre;
        rucCli = ruc;
    }

//    public static void setSecondStage(Stage secondStage) {
//        stageData = secondStage;
//        StageSecond.setStageData(secondStage);
//    }
    Toaster toaster;
    boolean enterEstado = false;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static boolean dato = false;
    private static boolean facturaVentaEstado = false;
    public static JSONObject facturaCabeceraSupr = new JSONObject();
    public static boolean actualizarDatosCabecera;
    public static Map mapArticulosEnPromo;
    public static ArrayList arrayListadoPromo;
    public static ArrayList arrayListadoPromoConDesc;
    public static Map mapPromoConDesc;
    public static long descPromo;
    public static int cantPromo;

    Alert alertCerrarTurno = null;
    Alert alertArqueo = null;

    static HashMap<Long, JSONObject> hmGift;

    public static HashMap<Long, JSONObject> getHmGift() {
        return hmGift;
    }

    public static void setHmGift(HashMap<Long, JSONObject> hmGift) {
        FacturaDeVentaFXMLController.hmGift = hmGift;
    }

    public JSONObject objArticulo;

    @Autowired
    private ArticuloDAO artDAO;

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    static JSONObject datos = new JSONObject();
    ManejoLocal manejoLocal = new ManejoLocal();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static String textFieldCompAnt;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static Long precioTotal;
    private static int peso;
    private static int real;
    private static int dolar;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    //primera inserción
    private boolean primeraInsercion;
    private static int orden;
    //primera inserción
    //lector de código
    private static String codBarra;
    private static String codDecimal;
    //lector de código
    private static NumberValidator numValidator;
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;
    JSONObject tipoCaja;
    public static List<JSONObject> detalleArtList;
    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    //TABLE VIEW
    private ReentrantLock lock = new ReentrantLock();
    Image image;
    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;
    private static int idFact;
    public static boolean valorIngreso = false;
    private SimpleDateFormat formatador = new SimpleDateFormat("hh:mm:ss a");

    static String rucCli;
    static String nomCli;

    final KeyCombination altN = new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN);
    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);
//    final KeyCombination altI = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN);

    @FXML
    private AnchorPane anchorPaneFactura;
    @FXML
    private TableView<JSONObject> tableViewFactura;
    @FXML
    private TableColumn<JSONObject, String> columnOrden;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnCant;
    @FXML
    private TableColumn<JSONObject, String> columnMed;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnPrecio;
    @FXML
    private TableColumn<JSONObject, String> columnIva;
    @FXML
    private TableColumn<JSONObject, String> columnExenta;
    @FXML
    private TableColumn<JSONObject, String> columnGravada;
    @FXML
    private Label labelTotalGs;
    @FXML
    private Label labelTotal;
    @FXML
    private Pane paneImage1;
    @FXML
    private Label labelHora;
    @FXML
    private Label labelFecha;
    @FXML
    private Label txtNumCaja;
    @FXML
    private Label labelCajeroFunc;
    @FXML
    private Label labelCajeroFunc2;
    @FXML
    private Button btnCerrar;
    @FXML
    private CheckBox chkExtranjero /*= new CheckBox()*/;
    @FXML
    private Button btnCancelar;
    @FXML
    private Button btnRetiroDinero;
    @FXML
    private TextField textFieldCant;
    @FXML
    private TextField textFieldCod;
    @FXML
    private ImageView imgProducto;
    @FXML
    private Label labelTotal1;
    @FXML
    private Label labelTotal11;
    @FXML
    private Label txtNumComprobante;
    @FXML
    private Button btnCerrarTurno;
    @FXML
    private TableColumn<JSONObject, Boolean> columnCupon;
    @FXML
    private TableColumn<EstadoCheck, Boolean> columnExonerarImp;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private TableColumn<JSONObject, String> columnPeso;
    @FXML
    private Pane paneImage11;
    @FXML
    private Label labelRucCliente;
    @FXML
    private ImageView imageViewArgen1;
    @FXML
    private ImageView imageViewBrazil1;
    @FXML
    private Label labelNombreCliente;
    @FXML
    private Pane imgProducto000;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtCodVendedor;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    @SuppressWarnings("ConvertToStringSwitch")
    public void initialize(URL url, ResourceBundle rb) {
        toaster = new Toaster();
        if (getDetalleArtList() == null || !getDetalleArtList().isEmpty()) {
            chkExtranjero.setDisable(true);
        }
        listenCheckExtranjero();
        if (ScreensContoller.getFxml().contentEquals("/vista/caja/GiftCardFXML.fxml")) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(CajaDeDatos.getCaja());
            if (jsonDatos.isNull("estadoExt")) {
                chkExtranjero.setSelected(false);
            } else {
                if (Boolean.parseBoolean(CajaDeDatos.getCaja().get("estadoExt").toString())) {
                    chkExtranjero.setSelected(true);
                } else {
                    chkExtranjero.setSelected(false);
                }
            }
            vistaJSONObjectArtDet();
            long total = 0;
            for (JSONObject json : getDetalleArtList()) {
                total += Math.round(Long.parseLong(json.get("precio").toString()) * Double.parseDouble(json.get("cantidad").toString()));
            }
            ubicandoContenedorSecundario();
            precioTotal = total;
            resetFormulario(total);
        } else if (ScreensContoller.getFxml().contentEquals("/vista/caja/BuscarClienteFXML.fxml")) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(CajaDeDatos.getCaja());
            if (jsonDatos.isNull("estadoExt")) {
                chkExtranjero.setSelected(false);
            } else {
                if (Boolean.parseBoolean(CajaDeDatos.getCaja().get("estadoExt").toString())) {
                    chkExtranjero.setSelected(true);
                } else {
                    chkExtranjero.setSelected(false);
                }
            }
            vistaJSONObjectArtDet();
            long total = 0;
            for (JSONObject json : getDetalleArtList()) {
                total += Long.parseLong(json.get("precio").toString()) * (new Double(json.get("cantidad").toString())).longValue();
            }
            precioTotal = total;
            ubicandoContenedorSecundario();
            resetFormulario(total);
            this.sc.loadScreenModal("/vista/caja/NuevoClienteFXML.fxml", 519, 187, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else if (ScreensContoller.getFxml().contentEquals("/vista/caja/NuevoClienteFXML.fxml")) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(CajaDeDatos.getCaja());
            if (jsonDatos.isNull("estadoExt")) {
                chkExtranjero.setSelected(false);
            } else {
                if (Boolean.parseBoolean(CajaDeDatos.getCaja().get("estadoExt").toString())) {
                    chkExtranjero.setSelected(true);
                } else {
                    chkExtranjero.setSelected(false);
                }
            }
            vistaJSONObjectArtDet();
            labelRucCliente.setText(rucCli);
            labelNombreCliente.setText(nomCli);
            long total = 0;
            for (JSONObject json : getDetalleArtList()) {
                total += Long.parseLong(json.get("precio").toString()) * (new Double(json.get("cantidad").toString())).longValue();
            }
            precioTotal = total;
            ubicandoContenedorSecundario();
            resetFormulario(total);
        } else {
            try {
                //FECHA Y HORA ACTUAL
                fechaHora();
                //FIN FECHA Y HORA ACTUAL        
                cargandoInicial();
                repeatFocus(textFieldCod);
                enterEstado = false;
                ubicandoContenedorSecundario();
            } catch (ParseException ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
            } catch (IOException ex) {
                Utilidades.log.error("IOException", ex.fillInStackTrace());
            }
        }
    }

    @FXML
    private void btnCerrarAction(ActionEvent event) {
        cerrando();
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
//        cancelarFactura();
//Parent root = FXMLLoader.load(
//                    CancelacionFacturaFXMLController.class.getResource("/vista/caja/CancelacionFacturaFXML.fxml"));
        CancelacionFacturaFXMLController.setFactAnt(txtNumComprobante.getText());
        CancelacionFacturaFXMLController.setTotalFacturado(labelTotalGs.getText(), textFieldCod);
        this.sc.loadScreenModal("/vista/caja/CancelacionFacturaFXML.fxml", 499, 234, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);

    }

    @FXML
    private void btnRetiroDineroAction(ActionEvent event) {
        verificarMontoFacturado();
    }

    @FXML
    private void btnCerrarTurnoAction(ActionEvent event) {
        if (!verificandoCaidaFormaPago()) {
            resetTray();
            verificandoDatosCierre();
        }
    }

    @FXML
    private void anchorPaneFacturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void anchorPaneFacturaKeyPressed(KeyEvent event) {

    }

    @FXML
    private void textFieldCodKeyReleased(KeyEvent event) {
        keyPressTextCod(event);
    }

    private void cargandoInicial() throws ParseException, IOException {
        mapArticulosEnPromo = new HashMap();
        arrayListadoPromo = new ArrayList();
        arrayListadoPromoConDesc = new ArrayList();
        mapPromoConDesc = new HashMap();
        descPromo = 0;
        cantPromo = 0;

        hmGift = new HashMap<>();

        if (StageSecond.getStageData()
                .isShowing()) {
            StageSecond.getStageData().close();
        }

        if (ScreensContoller.getFxml()
                .contentEquals("/vista/caja/FormasDePagoFXML.fxml")) {
            toaster.mensajeDiario();
//            toaster.mensajeFactCierreDet(MensajeFinalVenta.getVueltoPopUp(), MensajeFinalVenta.getClientePopUp(), 5);
            toaster.mensajeDeNavidadAnhoNuevo();
        }
        objArticulo = new JSONObject();
        facturaVentaEstado = false;

        setActualizarDatosCabecera(
                false);
        cargandoImagen();
        facturaCabeceraSupr = new JSONObject();
        numValidator = new NumberValidator();
        boolean estado = false;

        if (DatosEnCaja.getDatos()
                != null) {
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = new JSONObject();
            if (DatosEnCaja.getFacturados() == null) {
                fact = new JSONObject();
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                fact = new JSONObject();
            } else {
                fact = DatosEnCaja.getFacturados();
                estado = true;
            }
            //SETEAR CAMPOS PRIMERAMENTE
            //**--VERIFICANDO--**
            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");
            txtNumCaja.setText("N°Caja: " + caj.get("descripcion").toString());
            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }
                labelCajeroFunc.setText("Cajero: " + nomFuncionario);
                labelCajeroFunc2.setText(apeFuncionario);
            } else {
                labelCajeroFunc.setText("Cajero: N/A");
            }
//            iniciandoCotizacion();
            //FIN DEL SETEO DE CAMPOS
            cargandoDatosIniciales();
            if (estado) {
                cargandoDetalleManeraLocal();
            }
        }
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

        if (jsonDatos.isNull(
                "rendicion")) {
            datos.put("rendicion", false);
        }
//        try {
//            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
//            txtNumRuc.setText(empresa.get("ruc").toString());
//        } catch (ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }

        if (!hashJsonArticulo.isEmpty()) {
            if (!jsonDatos.isNull("exentaGlobal")) {
                chkExtranjero.setSelected(true);
            }
            chkExtranjero.setDisable(true);
        } else {
            chkExtranjero.setDisable(false);
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    private void cargandoDatosIniciales() {
        if (fact == null || fact.toString().equalsIgnoreCase("{}")) {
            this.alert = false;
            orden = 1;
            labelTotalGs.setText("Gs 0");
            hashJsonArtDet = new HashMap<>();
            hashJsonArticulo = new HashMap<>();
            primeraInsercion = true;
            detalleArtList = new ArrayList<>();
            codBarra = "";
            codDecimal = "";
            listenFactura();
        } else {
            Utilidades.log.info("ESTO DA: " + labelTotalGs.getText());
        }
        iniciandoFactCab();
    }

    static void iniciandoFactCab() {
        cabFactura = new JSONObject();
        cabFactura = creandoJsonFactCab();
        cancelacionProd = false;
        cancelacionProdPrimera = true;
    }

    private void fechaHora() {
        DigitalClock dc = new DigitalClock();
        dc.setPrefWidth(100);
        dc.setPrefHeight(18);
        dc.setLayoutY(4);
        dc.setLayoutX(50);
        paneImage1.getChildren().add(dc);
        labelFecha.setText("Fecha  : " + getFechaActual());
    }

    private void listenFactura() {
        tableViewFactura.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                }
            }
        });
        btnCerrar.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnRetiroDinero.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCancelar.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        btnCerrarTurno.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                    textFieldCant.requestFocus();
                }
            }
        });
        textFieldCant.setText("");
        textFieldCod.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > 13) {
                Platform.runLater(() -> {
                    textFieldCod.setText(oldValue);
                });
            }
        });
//        textFieldCant.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!newValue.contentEquals("")) {
//                try {
//                    if (GenericValidator.isDouble(newValue)) {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(newValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(oldValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    }
//                } catch (NumberFormatException e) {
//                    Platform.runLater(() -> {
//                        textFieldCant.setText(oldValue);
//                        textFieldCant.positionCaret(textFieldCant.getLength());
//                        if (!textFieldCant.getText().endsWith(".")) {
//                            if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                textFieldCant.setText("1");
//                                mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                            }
//                        }
//                    });
//                }
//            }
//        });
        textFieldCod.setText("");
        textFieldCant.setText("1");
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                textFieldCod.requestFocus();
            }
        });
    }

    private void mensajeAlerta(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorArt(String msj) {
        toaster.mensajeGenericoError("Mensaje del Sistema", msj, true, 2);
    }

    private void cargandoDetalleManeraLocal() {
        try {
            resetMapeo();
            detalleArtList = new ArrayList<>();
            JSONParser parser = new JSONParser();
            JSONArray facturaDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            JSONObject facturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            cabFactura = new JSONObject();
            org.json.JSONObject jsonFact = new org.json.JSONObject(facturaCabecera);
            try {
                if (tableViewFactura.getItems().isEmpty()) {
                    if (!jsonFact.isNull("montoFactura")) {
                        facturaCabecera.remove("montoFactura");
                        fact.put("facturaClienteCab", facturaCabecera);
                    } else {
                        cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                    }
                } else {
                    cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                }
                valorIngreso = true;
            } catch (Exception e) {
                mensajeError2("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO");
                valorIngreso = false;
            }
            if (valorIngreso) {
                cabFactura.put("estadoFactura", facturaCabecera.get("estadoFactura").toString());
                cabFactura.put("nroActual", facturaCabecera.get("nroActual").toString());
                cabFactura.put("idFacturaClienteCab", facturaCabecera.get("idFacturaClienteCab").toString());
                cabFactura.put("nroFactura", facturaCabecera.get("nroFactura").toString());
                primeraInsercion = false;
                for (int i = 0; i < facturaDetalle.size(); i++) {
                    JSONObject detalleArticulo = (JSONObject) parser.parse(facturaDetalle.get(i).toString());
                    JSONObject articulo = (JSONObject) parser.parse(detalleArticulo.get("articulo").toString());
                    hashJsonArticulo.put((Long.parseLong(articulo.get("codArticulo").toString())), articulo);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long.parseLong(articulo.get("idArticulo").toString())), detalleArtList.lastIndexOf(detalleArticulo));
                    vistaJSONObjectArtDet();
                    cargandoCamposInterfaceLocal(detalleArticulo);
                    orden++;
                }
                CajaDeDatos.generandoNroComprobante();
                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                // primer trío
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                long nroActual = talos.getNroActual();
                JSONObject jsonFacturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
                if (jsonDatos.isNull("caida")) {
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                } else {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    } else {
                        txtNumComprobante.setText(Utilidades.patternFactura(jsonFacturaCabecera.get("nroFactura").toString()));
                    }
                }
                File file = new File(PATH.PATH_NO_IMG);
                Image image = new Image(file.toURI().toString());
                this.imgProducto.setImage(image);
                centerImage();
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (!detalleArtList.isEmpty()) {
            Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
        }
    }

    private static void resetMapeo() {
        hashJsonArticulo = new HashMap<>();
        hashJsonArtDet = new HashMap<>();
    }

    private void mensajeError2(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void centerImage() {
        Image img = imgProducto.getImage();
        if (img != null) {
            double w = 0;
            double h = 0;
            double ratioX = imgProducto.getFitWidth() / img.getWidth();
            double ratioY = imgProducto.getFitHeight() / img.getHeight();
            double reducCoeff = 0;
            if (ratioX >= ratioY) {
                reducCoeff = ratioY;
            } else {
                reducCoeff = ratioX;
            }
            w = img.getWidth() * reducCoeff;
            h = img.getHeight() * reducCoeff;
            imgProducto.setX((imgProducto.getFitWidth() - w) / 2);
            imgProducto.setY((imgProducto.getFitHeight() - h) / 2);
        }
    }

    public void vistaJSONObjectArtDet() {
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(jsonArticulo.get("codArticulo").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCant.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCant.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("peso").toString().contentEquals("N/A")) {
                    String sEntero[] = data.getValue().get("cantidad").toString().split("\\.");
                    int entero;
                    if (sEntero.length == 0) {
                        return new SimpleStringProperty(data.getValue().get("cantidad").toString());
                    } else {
                        entero = Integer.valueOf(sEntero[0]);
                        double doble = Double.parseDouble(data.getValue().get("cantidad").toString());
                        if ((doble - entero) > 0) {
                            return new SimpleStringProperty(data.getValue().get("cantidad").toString());
                        } else {
                            return new SimpleStringProperty(sEntero[0]);
                        }
                    }
                } else {
                    return new SimpleStringProperty(data.getValue().get("peso").toString());
                }
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnMed.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnMed.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("peso").toString().contentEquals("N/A")) {
                    String sEntero[] = data.getValue().get("cantidad").toString().split("\\.");
                    int entero;
                    if (sEntero.length == 0) {
                        return new SimpleStringProperty("U");
                    } else {
                        entero = Integer.valueOf(sEntero[0]);
                        double doble = Double.parseDouble(data.getValue().get("cantidad").toString());
                        if ((doble - entero) > 0) {
                            return new SimpleStringProperty("P");
                        } else {
                            return new SimpleStringProperty("U");
                        }
                    }
                } else {
                    return new SimpleStringProperty("P");
                }
            }
        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("peso").toString());
            }
        });
        //columna Peso......................................................
        //columna Sección..............................................
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Sección......................................................
        //columna Sección..............................................
        columnPrecio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPrecio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject datas = data.getValue();
                String valor = "";
//                if (chkExtranjero.isSelected()) {
//                    try {
//                        JSONObject jsonArt = (JSONObject) parser.parse(datas.get("articulo").toString());
//                        JSONObject jsonIva = (JSONObject) parser.parse(jsonArt.get("iva").toString());
//                        long porIva = Long.parseLong(jsonIva.get("poriva").toString());
//                        if (porIva == 5) {
//                            double precioMinDouble = Long.parseLong(datas.get("precio").toString()) / 1.05;
////                            long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
//
////                            double precioParseado = Double.parseDouble(precioMin);
//                            numValidator = new NumberValidator();
//                            valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
////                            return new SimpleStringProperty(valor);
//                        } else if (porIva == 10) {
//                            double precioMinDouble = Long.parseLong(datas.get("precio").toString()) / 1.1;
////                            long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
//                            numValidator = new NumberValidator();
//                            valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
////                            return new SimpleStringProperty(valor);
//                        } else {
                double precioMinDouble = Long.parseLong(datas.get("precio").toString());
                numValidator = new NumberValidator();
                valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioMinDouble));
//                            return new SimpleStringProperty(valor);
//                        }
//                    } catch (ParseException ex) {
//                        Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//                    }
//                } else {
//                    String precio = datas.get("precio").toString();
//                    double precioParseado = Double.parseDouble(precio);
//                    numValidator = new NumberValidator();
//                    valor = numValidator.numberFormat("Gs ###,###.###", Math.rint(precioParseado));
//
//                }
                return new SimpleStringProperty(valor);
            }
        });
        //columna Sección......................................................
        //columna IMP..............................................
//        columnImp.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnImp.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if ((long) data.getValue().get("poriva") == 0 || chkExtranjero.isSelected()) {
//                    return new SimpleStringProperty("EXE");
//                } else {
//                    return new SimpleStringProperty("GRAV");
//                }
//            }
//        });
        //columna IMP......................................................
        //columna IVA..............................................
        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    if (!chkExtranjero.isSelected()) {
                        return new SimpleStringProperty(data.getValue().get("poriva").toString() + " %");
                    } else {
                        return new SimpleStringProperty("0 %");
                    }
                } catch (Exception e) {
                    return new SimpleStringProperty("0 %");
                }
            }
        });
        //columna IVA......................................................
        //columna Exenta..............................................
        columnExenta.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("exenta") == null) {
                    return new SimpleStringProperty("-");
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("exenta").toString())));
                }
            }
        });
        //columna Exenta......................................................
        //columna Gravada..............................................
        columnGravada.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (data.getValue().get("gravada") == null) {
                    return new SimpleStringProperty("-");
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("gravada").toString())));
                }
            }
        });
        columnCupon.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCupon.setCellValueFactory(new PropertyValueFactory<JSONObject, Boolean>("checked"));
        columnCupon.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> param) {
                return new CheckBoxTableCell<JSONObject, Boolean>();
            }
        });

//            public TableCell<TestObject, Boolean> call(TableColumn<TestObject, Boolean> p) {
//                return new CheckBoxTableCell<TestObject, Boolean>();
//            }
//        });
//        columnCupon.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                if (data.getValue().get("gravada") == null) {
//                return new SimpleStringProperty("-");
////                } else {
////                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("gravada").toString())));
////                }
//            }
//        });
//        columnExonerarImp.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnExonerarImp.setCellValueFactory(new PropertyValueFactory<JSONObject, Boolean>("checked"));
//        columnExonerarImp.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
//            @Override
//            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> param) {
//                return new CheckBoxTableCell<JSONObject, Boolean>();
//            }
//        });
        columnExonerarImp.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnExonerarImp.setCellValueFactory(new PropertyValueFactory<EstadoCheck, Boolean>("checked"));
        columnExonerarImp.setCellFactory(p -> {
            CheckBox checkBox = new CheckBox();
            TableCell<EstadoCheck, Boolean> cell = new TableCell<EstadoCheck, Boolean>() {
                @Override
                public void updateItem(Boolean item, boolean empty) {
                    if (empty) {
                        setGraphic(null);
                    } else {
                        if (chkExtranjero.isSelected()) {
                            checkBox.setSelected(true);
                            checkBox.setDisable(true);
                            setGraphic(checkBox);
                        } else {
                            checkBox.setSelected(false);
                            checkBox.setDisable(true);
                            setGraphic(checkBox);
                        }
                    }
                }
            };
//            checkBox.selectedProperty().addListener((obs, wasSelected, isSelected)
//                    -> ((EstadoCheck) cell.getTableRow().getItem()).setChecked(true));
//            cell.setAlignment(Pos.CENTER);
            return cell;
        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewFactura.setItems(articuloDetData);
        //**********************************************************************
    }

    private void cargandoCamposInterfaceLocal(JSONObject detalleArticulo) {
        String cantidad = detalleArticulo.get("cantidad").toString();
//        labelCantidad.setText(cantidad);
        textFieldCant.setText("1");
        textFieldCod.setText("");
//        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
        numValidator = new NumberValidator();
        long lblTotalgs = Long.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        long totalMultiplicado = Math.round(Double.parseDouble(cantidad) * Double.parseDouble(detalleArticulo.get("precio").toString()));
        precioTotal = totalMultiplicado + lblTotalgs;
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(precioTotal))));
    }

    private void cerrando() {
        if (!detalleArtList.isEmpty()) {
            pagando();
        } else {
            mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
        }
    }

    private void pagando() {
//        if ("factura_cerrar")) {
        org.json.JSONObject json = new org.json.JSONObject(fact);
        if (!json.isNull("facturaClienteCab")) {
            try {
                JSONObject factu = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                setCabFactura(factu);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        FormasDePagoFXMLController.setTxtCodigo(textFieldCod);
        this.sc.loadScreenModal("/vista/caja/FormasDePagoFXML.fxml", 667, 501, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void mensajeError(String msj) {
//        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorAlertModal(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.ERROR, msj, ok);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
        }
    }

    private void verificandoDatosCierre() {
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
        } else {
            CerrarTurnoFXMLController.setTxtCodigo(textFieldCod);
            this.sc.loadScreenModal("/vista/caja/CierreTurnoFXML.fxml", 425, 201, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        }
    }
//    private HashMap () {
//        HashMap valor = new HashMap();
//        valor.put("cajero", "nverificandoDatosCierreull");
//        int items = tableViewFactura.getItems().size();
//        if (items > 0) {
//            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
//        } else {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            alertCerrarTurno = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CERRAR TURNO COMO CAJERO?", ok, cancel);
//            alertCerrarTurno.showAndWait();
//            LoginFXMLController.setLlamarTask(false);
//            if (alertCerrarTurno.getResult() == ok) {
//                ButtonType okData = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                ButtonType cancelData = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                alertArqueo = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA REALIZAR EL ARQUEO?", okData, cancelData);
//                alertArqueo.showAndWait();
//                if (alertArqueo.getResult() == okData) {
//                    users = null;
//                    fact = null;
//                    DatosEnCaja.setUsers(null);
//                    actualizarDatos();
//                    LoginFXMLController.setLlamarTask(false);
//                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                    valor.put("cajero", "cambio");
//                    alertArqueo.close();
//                }
//            } else if (alertCerrarTurno.getResult() == cancel) {
//                alertCerrarTurno.close();
//            }
//            alertArqueo = null;
//            alertCerrarTurno = null;
//        }
//        return valor;
//    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void cancelarFactura() {
        CancelacionFacturaFXMLController.setFactAnt(txtNumComprobante.getText());
        CancelacionFacturaFXMLController.setTotalFacturado(labelTotalGs.getText(), textFieldCod);
        this.sc.loadScreenModal("/vista/caja/CancelacionFacturaFXML.fxml", 499, 234, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void verificarMontoFacturado() {
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE NO HA SIDO CERRADA!");
        } else {
            if (datos.containsKey("montoFacturado")) {
                int montoFacturado = Integer.parseInt(datos.get("montoFacturado").toString());
                if (montoFacturado > 0) {
                    retirandoDinero();
                } else {
                    mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
                }
            } else {
                mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
            }
        }
    }

    private void retirandoDinero() {
//        if ("retiro_dinero")) {
        RetiroDineroFXMLController.setTxtCodigo(textFieldCod);
        this.sc.loadScreenModal("/vista/caja/retiroDineroFXML.fxml", 529, 266, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            boolean valor = false;
            if (!jsonDatos.isNull("ultimaFactura")) {
                String nFac = txtNumComprobante.getText().replace("-", "");
                if (jsonDatos.getString("ultimaFactura").equals(String.valueOf(Long.parseLong(nFac)))) {
                    valor = true;
                }
            }
            if (valor) {
                mensajeDetalle("Se ha detectado una incidencia, contáctese con el área de IT.", "Error 400");
            } else {
//                ClienteFielFXMLController.setJsonClienteFiel(null);
                if (!verificandoCaidaFormaPago()) {
                    if (!detalleArtList.isEmpty()) {
//                        VERIFICAR PROMO MAS COMPRAS MAS  AHORRAS
                        cargarArticulosOrdenar();
//                        FIN VERIFICAR PROMO MAS COMPRAS MAS AHORRAS //NUEVO
                        org.json.JSONObject json = new org.json.JSONObject(datos);
                        boolean formaPago = false;
                        if (!json.isNull("caida")) {
                            String caida = datos.get("caida").toString();
                            if (caida.equalsIgnoreCase("factura_venta")) {
                                formaPago = false;
                            } else {
                                formaPago = true;
                            }
                        }
                        if (formaPago) {
                            try {
                                JSONArray detalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                                JSONObject cabecera = new JSONObject();
                                if (detalle.size() > 0) {
                                    JSONObject objDetalle = (JSONObject) parser.parse(detalle.get(0).toString());
                                    cabecera = (JSONObject) parser.parse(objDetalle.get("facturaClienteCab").toString());
                                } else {
                                    cabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                                }
                                cabFactura = cabecera;
                                new MensajeFinalVenta().cargandoInicial();
//                                this.sc.loadScreen("/vista/caja/mensajeFinalVentaFXML.fxml", 562, 288, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        } else {
                            if (DatosEnCaja.getDatos() != null) {
                                datos = DatosEnCaja.getDatos();
                            }
                            if (DatosEnCaja.getFacturados() != null) {
                                fact = DatosEnCaja.getFacturados();
                            }
                            if (!facturaCabeceraSupr.toString().equalsIgnoreCase("{}")) {
                                cabFactura = facturaCabeceraSupr;
                                fact.put("facturaClienteCab", facturaCabeceraSupr);
                                setActualizarDatosCabecera(true);
                                FacturaDeVentaFXMLController.cancelacionProd = true;
                            }
                            //verificar que ingrese en forma de pago en el metodo PUT ya que cuando de cancela una factura va al POST de vuelta
                            //entonces impide que se actualice la CABECERA
                            pagando();
                            actualizandoCabFacturaLocalmente();
                        }
                        //FIN NUEVO
                    } else {
                        mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
                    }
                }
            }
        }
        if (keyCode == event.getCode().F4) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificarMontoFacturado();
                    actualizarDatos();
                }
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewFactura.requestFocus();
        }
        if (keyCode == event.getCode().F3) {
            if (alert) {
                alert = false;
            } else if (detalleArtList.isEmpty()) {
                mensajeAlerta("NO DISPONE DE ARTÍCULO ALGUNO PARA CANCELAR FACTURA.");
            } else {
                textFieldCant.requestFocus();
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                cancelarFactura();
                actualizarDatos();
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (!verificandoCaidaFormaPago()) {
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    mensajeError("NO SE PUEDE ELIMINAR LOS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                } else if (alert) {
                    alert = false;
                } else if (detalleArtList.isEmpty()) {
                    mensajeAlerta("DEBE DISPONER COMO MÍNIMO UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                } else {
                    JSONObject productos = tableViewFactura.getSelectionModel().getSelectedItem();
                    if (productos == null) {
                        mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                    } else {
//                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA ELIMINAR EL ARTÍCULO " + productos.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
//                        alert1.showAndWait();
//                        if (alert1.getResult() == ok) {
                        try {
                            facturaCabeceraSupr = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                            FacturaDeVentaFXMLController.setCabFactura(facturaCabeceraSupr);
//                                textFieldDescripcion.setText("");
                            this.alert = false;
                            Label labelCantidad = new Label();
                            CancelacionProductoFXMLController.obtenerTable(tableViewFactura, labelTotalGs, productos, labelCantidad, imgProducto, chkExtranjero, textFieldCod, productos.get("cantidad").toString());
                            this.sc.loadScreenModal("/vista/caja/CancelacionProductoFXML.fxml", 519, 275, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
//                        } else if (alert1.getResult() == cancel) {
//                            alert1.close();
//                        }
                        actualizarDatos();
                    }
                }
            }
        }
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificandoDatosCierre();
//                    String cajero = verificandoDatosCierre().get("cajero").toString();
//                    if (cajero.equalsIgnoreCase("arqueo")) {
//                        users = null;
//                        fact = null;
//                        DatosEnCaja.setUsers(null);
//                        datos.put("modSup", true);
//                        DatosEnCaja.setDatos(datos);
//                        actualizarDatosNuevo();
//                    }
                }
            }
        }
        if (altN.match(event)) {
            registrandoCliente();
        }
        if (altC.match(event)) {
            buscandoCliente();
        }
        if (keyCode == event.getCode().F7) {
            if (tableViewFactura.getItems().isEmpty()) {
                if (chkExtranjero.isSelected()) {
                    chkExtranjero.setSelected(false);
                    CajaDeDatos.getCaja().put("estadoExt", false);
                } else {
                    chkExtranjero.setSelected(true);
                    chkExtranjero.setDisable(true);
                    CajaDeDatos.getCaja().put("estadoExt", true);
                }
            } else {
                mensajeAlerta("DETALLE DEBE ESTAR VACÍO, PARA CAMBIAR ESTADO EXONERAR IVA.");
            }
        }
//        if (keyCode == event.getCode().F10) {
//            mensajeDialog();
//        }
        if (event.getCode().isDigitKey() || event.getCode().getName().contentEquals("Enter")) {
//            if (alert) {
//                alert = false;
//            } else {
//                lecturaCodBarra(event);
//            }
        }
    }

    private void cargarArticulosOrdenar() {
        mapArticulosEnPromo = new HashMap();
        try {
//            int valor = 0;
            cantPromo = 0;
            int orden = 0;
            for (JSONObject jsonDetalle : detalleArtList) {
                orden++;
                long cantidad = Long.parseLong(String.valueOf(jsonDetalle.get("cantidad").toString()).replace(".0", ""));
                JSONObject jsonArt = (JSONObject) parser.parse(jsonDetalle.get("articulo").toString());
                if (LoginCajeroFXMLController.articuloPromo.containsKey(jsonArt.get("codArticulo"))) {
                    mapArticulosEnPromo.put(jsonArt.get("codArticulo") + "-" + cantidad + "-" + orden, Long.parseLong(String.valueOf(jsonDetalle.get("precio").toString())));
//                    valor++;
                    cantPromo += cantidad;
                }
            }
            if (cantPromo >= 4) {
                ordenarMapeo();

            }
        } catch (ParseException ex) {
            Logger.getLogger(FacturaDeVentaFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ordenarMapeo() {
        Set<Map.Entry<String, Long>> set = mapArticulosEnPromo.entrySet();
        List<Map.Entry<String, Long>> list = new ArrayList<Map.Entry<String, Long>>(
                set);
        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
            public int compare(Map.Entry<String, Long> o2,
                    Map.Entry<String, Long> o1) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        int orden = 0;
        arrayListadoPromo = new ArrayList();
        for (Map.Entry<String, Long> entry : list) {

            StringTokenizer st = new StringTokenizer(entry.getKey(), "-");
            String cod = st.nextElement().toString();
            String cant = st.nextElement().toString();
            for (int i = 0; i < Integer.parseInt(cant); i++) {
                orden++;
                arrayListadoPromo.add(orden + "-" + cod + "-" + entry.getValue());
            }
        }

//        for (int i = 0; i < arrayListadoPromo.size(); i++) {
//            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
//            String ord = st.nextElement().toString();
//            String cod = st.nextElement().toString();
//            String precio = st.nextElement().toString();
//
//            System.out.println(ord + ") -->> " + cod + " - " + precio);
//        }
        System.out.println("------------------SIGUIENTE--------------------");
        int vueltaEnCiclo = 0;
        int cantVueltaEnCiclo = 1;

        double cantDesc = orden / 4;
        String str = String.valueOf(cantDesc);
        int cantVueltaTotalDesc = Integer.parseInt(str.substring(0, str.indexOf('.')));
        descPromo = 0;
        arrayListadoPromoConDesc = new ArrayList();
        mapPromoConDesc = new HashMap();

        for (int i = 0; i < arrayListadoPromo.size(); i++) {
            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
            String ord = st.nextElement().toString();
            String cod = st.nextElement().toString();
            String precio = st.nextElement().toString();

            vueltaEnCiclo++;

            if (cantVueltaTotalDesc >= cantVueltaEnCiclo) {
                switch (vueltaEnCiclo) {
                    case 1:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 50%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.5) + "-50");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.5);
                        break;
                    case 2:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 40%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.4) + "-40");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.4);
                        break;
                    case 3:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 30%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.3) + "-30");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.3);
                        break;
                    case 4:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 20%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.2) + "-20");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.2);
                        vueltaEnCiclo = 0;
                        cantVueltaEnCiclo++;
                        break;
//                default:
//                    break;
                }
            } else {
                System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 0%");
            }
        }
    }

    private void buscandoCliente() {
//        if ("cliente_caja")) {
        BuscarClienteFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
        this.sc.loadScreenModal("/vista/caja/BuscarClienteFXML.fxml", 602, 87, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    private void registrandoCliente() {
//        if ("cliente_caja")) {
        NuevoClienteFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
        this.sc.loadScreenModal("/vista/caja/NuevoClienteFXML.fxml", 519, 187, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    public static void resetParam() {
        precioTotal = 0l;
    }

    private void resetTray() {
        Toaster.quitandoMsj();
    }

    private void keyPressTextCod(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            keyPressEventos(event);
            if (!enterEstado) {
                enterEstado = false;
            } else {
                if (alertArqueo != null) {
                    if (alertArqueo.isShowing()) {
                        Utilidades.log.info("FUI A UN ARQUEO");
                    }
                }
                if (alertCerrarTurno != null) {
                    if (alertCerrarTurno.isShowing()) {
                        Utilidades.log.info("FUI A UN CIERRE DE TURNO");
                    }
                }
                datos.remove("exentaGlobal");
                //NUEVO
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    if (!this.alert) {
                        mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                    }
                } else {
                    JSONObject jsonCabecera = new JSONObject();
                    if (!json.isNull("sitio") && !facturaVentaEstado) {
                        try {
                            jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
                    }
//              FIN NUEVO
                    if (!this.alert) {
                        if (!verificandoCaidaFormaPago()) {
                            if (!textFieldCant.getText().isEmpty() && !textFieldCant.getText().endsWith(".")) {
                                if (Double.parseDouble(textFieldCant.getText()) > 0) {
                                    if (textFieldCod.getText().length() == 11 && textFieldCod.getText().startsWith("0")) {
                                        codBarra = textFieldCod.getText().substring(0, 6).replaceFirst("^0+(?!$)", "");
                                        codDecimal = textFieldCod.getText().substring(6, 11).replaceFirst("^0+(?!$)", "");
                                        double nArt = 0;
                                        boolean estado = false;
                                        if (cargandoDetalleDecimal()) {
                                            estado = true;
                                        }
                                        if (estado) {
                                            if (!json.isNull("nArticulos")) {
                                                nArt = Double.parseDouble(datos.get("nArticulos").toString());
                                            }
                                            datos.put("nArticulos", nArt + Double.parseDouble(codDecimal));
                                            actualizarDatos();
                                        }
                                        codBarra = "";
                                        codDecimal = "";
                                    } else {
                                        if (textFieldCant.getText().contentEquals("")) {
                                            textFieldCant.setText("1");
                                        }
                                        double cant = Double.parseDouble(numValidator.numberValidator(textFieldCant.getText()));
                                        double cantidad = cant;
                                        codBarra = textFieldCod.getText();
                                        double nArt = 0;
                                        boolean estado = false;
//                            while (cant != 0) {
                                        if (cargandoDetalleUnidad()) {
                                            estado = true;
                                        }
//                                cant--;
//                            }
                                        if (estado) {
                                            if (!json.isNull("nArticulos")) {
                                                nArt = Double.parseDouble(datos.get("nArticulos").toString());
                                            }
                                            datos.put("nArticulos", nArt + cantidad);
                                            actualizarDatos();
                                        }
                                        codBarra = "";
                                        codDecimal = "";
                                    }
                                } else {
                                    textFieldCod.setText("");
                                    mensajeAlerta("CANTIDAD ARTÍCULO DEBE SER MAYOR A CERO (0).");
                                }
                            } else {
                                textFieldCod.setText("");
                                if (textFieldCant.getText().isEmpty()) {
                                    mensajeAlerta("CANTIDAD ARTÍCULO ESTÁ VACÍO.");
                                } else if (textFieldCant.getText().endsWith(".")) {
                                    mensajeAlerta("CANTIDAD ARTÍCULO NO VÁLIDO.");
                                }
                            }
                            textFieldCant.setText("1");
                            txtCodVendedor.setText("12");
                            //NUEVO
                            JSONArray jsonDetalle = new JSONArray();
                            if (!json.isNull("sitio") && !facturaVentaEstado) {
                                try {
                                    jsonDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                                } catch (ParseException ex) {
                                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                                }
                                fact.put("facturaClienteCab", jsonCabecera);
                                FacturaDeVentaFXMLController.setCabFactura(jsonCabecera);
                                fact.put("facturaDetalle", jsonDetalle);
                                facturaVentaEstado = true;
                                DatosEnCaja.setFacturados(fact);
                            }
//                        Utilidades.log.info("LA FACTURA ES: " + fact.toString());
                            //FIN DEL NUEVO
                            datos.put("sitio", 1);
                            actualizandoCabFacturaLocalmente();
                        }
                    } else {
                        this.alert = false;
                    }
                    if (!detalleArtList.isEmpty()) {
                        Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
                    }
                }
                enterEstado = false;
            }
        } else if (event.getCode().isDigitKey()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
        } else if (event.getCode() == KeyCode.INSERT) {
            if (textFieldCod.getCaretPosition() == textFieldCod.getText().length()
                    && textFieldCod.getText().length() > 1 && textFieldCod.getText().length() < 13) {
                if (textFieldCod.getText().startsWith("2") || textFieldCod.getText().startsWith("29")) {
                    int cero = 13 - textFieldCod.getText().length();
                    String ceroS = "";
                    for (int i = 0; i < cero; i++) {
                        ceroS = ceroS + "0";
                    }
                    String inicioS;
                    String finalS;
                    if (textFieldCod.getText().startsWith("29")) {
                        inicioS = textFieldCod.getText().substring(0, 2);
                        finalS = textFieldCod.getText().substring(2, textFieldCod.getText().length());
                    } else {
                        inicioS = textFieldCod.getText().substring(0, 1);
                        finalS = textFieldCod.getText().substring(1, textFieldCod.getText().length());
                    }
                    textFieldCod.setText(inicioS + ceroS + finalS);
                    codBarra = textFieldCod.getText();
                    textFieldCod.positionCaret(textFieldCod.getLength());
                }
            } else if (textFieldCod.getText().length() < 13) {
                int cero = 13 - textFieldCod.getText().length();
                String ceroS = "";
                for (int i = 0; i < cero; i++) {
                    ceroS = ceroS + "0";
                }
                String inicioS = textFieldCod.getText().substring(0, textFieldCod.getCaretPosition());
                String finalS = textFieldCod.getText().substring(textFieldCod.getCaretPosition(), textFieldCod.getText().length());
                textFieldCod.setText(inicioS + ceroS + finalS);
                codBarra = textFieldCod.getText();
                textFieldCod.positionCaret(textFieldCod.getLength());
            }
        }
    }

    private void lecturaCodBarra(KeyEvent event) {
        lock.lock();
        if (event.getCode().isDigitKey() && !textFieldCant.focusedProperty().getValue() && !textFieldCod.focusedProperty().getValue()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
            textFieldCod.setText(codBarra);
        } else if (event.getCode().isLetterKey()) {
            mensajeAlerta("EL CAMPO CÓDIGO ARTÍCULO DEBE SER NÚMERICO.");
        }
        if (event.getCode().getName().contentEquals("Enter") && !textFieldCant.focusedProperty().getValue() && !textFieldCod.focusedProperty().getValue()) {
            if (textFieldCod.getText().length() == 11 && textFieldCod.getText().startsWith("0")) {
                codBarra = textFieldCod.getText().substring(0, 6).replaceFirst("^0+(?!$)", "");
                codDecimal = textFieldCod.getText().substring(5, 5).replaceFirst("^0+(?!$)", "");
                cargandoDetalleDecimal();
                codBarra = "";
                codDecimal = "";
            } else {
                if (!textFieldCant.getText().isEmpty()) {
                    int cant = Integer.valueOf(numValidator.numberValidator(textFieldCant.getText()));
                    while (cant != 0) {
                        cargandoDetalleUnidad();
                        cant--;
                    }
                    codBarra = "";
                    codDecimal = "";
                } else {
                    cargandoDetalleUnidad();
                    codBarra = "";
                    codDecimal = "";
                }
            }
//NUEVO
            if (alertArqueo != null) {
                if (alertArqueo.isShowing()) {
                    Utilidades.log.info("FUI A UN ARQUEO");
                }
            }
            if (alertCerrarTurno != null) {
                if (alertCerrarTurno.isShowing()) {
                    Utilidades.log.info("FUI A UN CIERRE DE TURNO");
                }
            }
            datos.remove("exentaGlobal");
            //NUEVO
            if (DatosEnCaja.getDatos() != null) {
                datos = DatosEnCaja.getDatos();
            }
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            }
            org.json.JSONObject json = new org.json.JSONObject(datos);
            boolean formaPago = false;
            if (!json.isNull("caida")) {
                String caida = datos.get("caida").toString();
                if (caida.equalsIgnoreCase("factura_venta")) {
                    formaPago = false;
                } else {
                    formaPago = true;
                }
            }
            if (formaPago) {
                if (!this.alert) {
                    mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                }
            } else {
                JSONObject jsonCabecera = new JSONObject();
                if (!json.isNull("sitio") && !facturaVentaEstado) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
//              FIN NUEVO
                if (!this.alert) {
                    if (!verificandoCaidaFormaPago()) {
                        if (!textFieldCod.getText().isEmpty()) {
                            /*if (textFieldCod.getText().length() == 11 && textFieldCod.getText().startsWith("0")) {
                                codBarra = textFieldCod.getText().substring(0, 6).replaceFirst("^0+(?!$)", "");
                                codDecimal = textFieldCod.getText().substring(6, 11).replaceFirst("^0+(?!$)", "");
                                int nArt = 0;
                                boolean estado = false;
                                if (cargandoDetalleDecimal()) {
                                    estado = true;
                                }
                                if (estado) {
                                    if (!json.isNull("nArticulos")) {
                                        nArt = Integer.parseInt(datos.get("nArticulos").toString());
                                    }
                                    datos.put("nArticulos", nArt + 1);
                                    actualizarDatos();
                                }
                                codBarra = "";
                                codDecimal = "";
                            } else {*/
                            if (textFieldCant.getText().contentEquals("")) {
                                textFieldCant.setText("1");
                            }
                            double cant = Double.parseDouble(numValidator.numberValidator(textFieldCant.getText()));
                            double cantidad = cant;
                            codBarra = textFieldCod.getText();
                            double nArt = 0;
                            boolean estado = false;
//                            while (cant != 0) {
                            if (cargandoDetalleUnidad()) {
                                estado = true;
                            }
//                                cant--;
//                            }
                            if (estado) {
                                if (!json.isNull("nArticulos")) {
                                    nArt = Double.parseDouble(datos.get("nArticulos").toString());
                                }
                                datos.put("nArticulos", nArt + cantidad);
                                actualizarDatos();
                            }
                            codBarra = "";
                            codDecimal = "";
//                            }
                        }
                        textFieldCant.setText("1");
                        //NUEVO
                        JSONArray jsonDetalle = new JSONArray();
                        if (!json.isNull("sitio") && !facturaVentaEstado) {
                            try {
                                jsonDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                            fact.put("facturaClienteCab", jsonCabecera);
                            FacturaDeVentaFXMLController.setCabFactura(jsonCabecera);
                            fact.put("facturaDetalle", jsonDetalle);
                            facturaVentaEstado = true;
                            DatosEnCaja.setFacturados(fact);
                        }
//                        Utilidades.log.info("LA FACTURA ES: " + fact.toString());
                        //FIN DEL NUEVO
                        datos.put("sitio", 1);
                        actualizandoCabFacturaLocalmente();
                    }
                } else {
                    this.alert = false;
                }
                if (!detalleArtList.isEmpty()) {
                    Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
                }
            }
        }
        lock.unlock();
    }

    private void mensajeDetalle(String msj, String title) {
        ButtonType btnAcept = new ButtonType("Salir (ESC)", ButtonBar.ButtonData.OK_DONE);
        Alert alerta = new Alert(Alert.AlertType.INFORMATION, msj, btnAcept);
        alerta.setTitle(title);
        alerta.setHeaderText("Mensaje del Sistema!");
        DialogPane dialogPane = alerta.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialogInformation");
        alerta.showAndWait();
    }

    private boolean verificandoCaidaFormaPago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("energiaElectrica")) {
            mensajeAlerta("Esta Factura debe ser cancelada por problemas de caída de la Energía Eléctrica, ya que podrían contener datos corruptos");
            return true;
        } else {
            return false;
        }
    }

//    public static void actualizandoCabFacturaLocalmente() {
//        JSONParser parser = new JSONParser();
//        if (DatosEnCaja.getFacturados() != null) {
//            try {
//                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
//            } catch (ParseException ex) {
//                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//            }
//        }
//        org.json.JSONObject json = new org.json.JSONObject(datos)
//                datos.put("caida", "forma_pago");
//            }
//        }
//        if (!facturaVentaEstado) {
//            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
//                //OBTENER ID RANGO ACTUAL DE LA FACTURA
//                long idRangoFact = 0;
//                if (!json.isNull("idRangoFacturaActual")) {
//                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
//                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                        datos.put("idRangoFacturaActual", idRangoFact);
//                    } else {
//                        String rango = datos.get("idRangoFacturaActual").toString();
//                        idRangoFact = Long.parseLong(rango);
//                    }
//                } else {
//                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    datos.put("idRangoFacturaActual", idRangoFact);
//                }
//                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
//                JSONObject jsonCabecera = new JSONObject();
//                if (!jsonFact.isNull("facturaClienteCab")) {
//                    try {
//                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
//                        datos.put("nroFact", FacturaDeVentaFXMLController.getCabFactura().get("nroFact"));
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                } else {
//                    try {
//                        editandoJsonFactCab();
//                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString());
//                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
//                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
//                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
//                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
//                        datos.put("nroFact", nroActualmente);
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                }
//            }
//        }
//        fact.put("facturaClienteCab", FacturaDeVentaFXMLController.getCabFactura().toString());
//        if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
//            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
//            JSONArray arrayDetalle = new JSONArray();
//            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
//                try {
//                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
//                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
//                    art.put("fechaAlta", null);
//                    art.put("fechaMod", null);
//                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
//                    iva.put("fechaAlta", null);
//                    iva.put("fechaMod", null);
//                    art.put("iva", iva);
//                    jsonArt.put("articulo", art);
//                    arrayDetalle.add(jsonArt);
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                }
//            }
//            DatosEnCaja.setDatos(datos);
//            DatosEnCaja.setUsers(users);
//            DatosEnCaja.setFacturados(fact);
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = DatosEnCaja.getFacturados();
//            fact.put("facturaDetalle", arrayDetalle);
//        }
//        actualizarDatosBD();
//        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
//    }
    private void mensajeDialog() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Consulta de Precio");
        dialog.setHeaderText("Mensaje del Sistema!");
        dialog.setContentText("INGRESE CODIGO DEL PRODUCTO:");
        ButtonType btnAcept = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().addAll(btnAcept, btnCancel);
        dialog.getDialogPane().getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialog.getDialogPane().getStyleClass().add("myDialogInformation");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Articulo art = artDAO.buscarCod(result.get());
            art.setFechaAlta(null);
            art.setFechaMod(null);
            String mensaje = "";
            mensaje = art.getDescripcion() + "\nPRECIO: " + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(art.getPrecioMin())));
            mensaje += "\n\nLOS DESCUENTOS QUEDAN SUJETO A VARIACIONES DE ACUERDO A LA FORMA DE PAGO.";
            mensajeDetalle(mensaje, "Detalle del Artículo");
        } else {
            Utilidades.log.info("No haz seleccionado nada");
        }
    }

    public static void actualizandoCabFacturaLocalmente() {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
        if (json.isNull("caida")) {
            FacturaDeVentaFXMLController.cancelacionProd = false;
            datos.put("caida", "factura_venta");
        } else {
            String caida = json.get("caida").toString();
            if (caida.equalsIgnoreCase("factura_venta")) {
                if (!isActualizarDatosCabecera()) {
                    FacturaDeVentaFXMLController.cancelacionProd = false;
                } else {
                    FacturaDeVentaFXMLController.cancelacionProd = true;
                }
                datos.put("caida", "factura_venta");
            } else {
                FacturaDeVentaFXMLController.cancelacionProd = true;
                datos.put("caida", "forma_pago");
            }
        }
        if (!facturaVentaEstado) {
            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                JSONObject jsonCabecera = new JSONObject();
                if (!jsonFact.isNull("facturaClienteCab")) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
                        datos.put("nroFact", FacturaDeVentaFXMLController.getCabFactura().get("nroFact"));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    try {
                        editandoJsonFactCab();
                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString());
                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
                        datos.put("nroFact", nroActualmente);
                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        FacturaDeVentaFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());
        fact.put("facturaClienteCab", FacturaDeVentaFXMLController.getCabFactura().toString());
        if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
            JSONArray arrayDetalle = new JSONArray();
            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
                    art.put("fechaAlta", null);
                    art.put("fechaMod", null);
                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                    iva.put("fechaAlta", null);
                    iva.put("fechaMod", null);
                    art.put("iva", iva);
                    jsonArt.put("articulo", art);
                    arrayDetalle.add(jsonArt);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            DatosEnCaja.setFacturados(fact);
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
            fact.put("facturaDetalle", arrayDetalle);
        }
        actualizarDatosBD();
        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
    }

    private static void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            FacturaDeVentaFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                FacturaDeVentaFXMLController.getCabFactura().put("cliente", NuevoClienteFXMLController.getJsonCliente());//en nulo default NN
            } else {
                FacturaDeVentaFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            }

        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            if (DatosEnCaja.getFacturados() == null) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else {
                DatosEnCaja.setFacturados(fact);
            }
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
            jsonFact = jsonFact.replace("\"[", "[");
            jsonFact = jsonFact.replace("]\"", "]");
            manejo.setFactura(jsonFact);
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();

            fact = DatosEnCaja.getFacturados();
        } catch (Exception ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        } finally {
        }
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleUnidad() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        Map mapeo = recuperarGiftSinUso(codBarra);
        boolean estadoGift = recuperarGiftEnUso(codBarra);
        if (estadoGift) {
            toaster.mensajeGenerico("Mensaje del Sistema", "GIFTCARD YA ESTA EN USO", "", 2);
        } else if (Boolean.parseBoolean(mapeo.get("comprado").toString())) {
            if (getHmGift().containsKey(Long.valueOf(codBarra))) {
                toaster.mensajeGenerico("Mensaje del Sistema", "YA SE HA CARGADO LA GIFTCARD", "", 2);
            } else {
//                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!getDetalleArtList().isEmpty()) {
                    orden++;
                }
                GiftCardFXMLController.setCodigo(codBarra, tableViewFactura, mapeo, orden);
                this.sc.loadScreenModal("/vista/caja/GiftCardFXML.fxml", 425, 206, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            }
        } else if (!estadoGift) {
            if (!codBarra.contentEquals("")) {
                if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                    jsonArticulo = jsonArtDet(codBarra);
                } else {
                    jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
                }
            }
            JSONArray jsonArrayArticulo = new JSONArray();
            try {
                org.json.JSONObject jsonArticu = new org.json.JSONObject(jsonArticulo);
                if (jsonArticu.isNull("articuloNf3Sseccion")) {
                    jsonArrayArticulo = new JSONArray();
                } else {
                    jsonArrayArticulo = (JSONArray) parser.parse(jsonArticulo.get("articuloNf3Sseccion").toString());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
            }
            boolean pasa = false;
            if (jsonArrayArticulo.isEmpty()) {
                pasa = true;
            } else {
                JSONObject jsonObj = (JSONObject) jsonArrayArticulo.get(0);
                JSONObject nf3Sseccion = (JSONObject) jsonObj.get("nf3Sseccion");
                pasa = !nf3Sseccion.get("descripcion").toString().equalsIgnoreCase("GIFT CARD");
            }
            if (!pasa) {
                toaster.mensajeGenerico("Mensaje del Sistema", "NO SE HA ESTABLECIDO CONEXION CON LA BASE GIFTCARD", "", 2);
            } else {
                boolean data = false;
                if (jsonArticulo != null) {
                    org.json.JSONObject jsonArti = new org.json.JSONObject(jsonArticulo);
                    if (jsonArti.get("precioMin").toString().equals("0")) {
                        mensajeDetalle("EL ARTICULO NO TIENE PRECIO, CONTACTESE CON STOCK", "Mensaje del Sistema");
                        textFieldCod.setText("");
                        data = true;
                    }
                }
                if (!data) {
                    if (jsonArticulo != null) {
                        try {
                            JSONObject detalleArticulo = null;
                            if (primeraInsercion) {
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                                detalleArtList.add(detalleArticulo);
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                                vistaJSONObjectArtDet();
                                primeraInsercion = false;
                                detalleArticulo.put("primeraInsercion", true);
                                cargandoCamposInterface(detalleArticulo);
                                CajaDeDatos.generandoNroComprobante();
                                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                                // primer trío
                                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                                // segundo trío
                                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                                long nroActual = talos.getNroActual();
                                txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                                resetTray();
                                chkExtranjero.setDisable(true);
                            } else {
                                orden++;
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);//si
                                detalleArtList.add(detalleArticulo);//si
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));//si
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);//si
                                vistaJSONObjectArtDet();//no
                                cargandoCamposInterface(detalleArticulo);
                            }
                            tableViewFactura.getItems().clear();
                            tableViewFactura.getItems().addAll(detalleArtList);
//                }
                            if (!tableViewFactura.getItems().isEmpty()) {
                                tableViewFactura.getSelectionModel().selectLast();
                            }
                            Image image = null;
                            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                                image = jsonArtDetImg(codBarra);
                            }
                            if (image == null) {
                                File file = new File(PATH.PATH_NO_IMG);
                                image = new Image(file.toURI().toString());
                                this.imgProducto.setImage(image);
                            }
                            this.imgProducto.setImage(image);
                            centerImage();
                            this.imgProducto = (ImageView) AnimationFX.fadeNode(this.imgProducto);
                        } catch (ParseException e) {
                            estado = false;
                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        }
                    } else {
                        if (!textFieldCod.getText().equalsIgnoreCase("")) {
                            textFieldCant.setText("1");
                            textFieldCod.setText("");
                            codBarra = "";
                            codDecimal = "";
                            mensajeErrorArt("NO SE ENCUENTRA EL ARTÍCULO");
                        }
                        estado = false;
                    }
                }
            }
        }
        txtCodVendedor.setText("");

        return estado;
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleDecimal() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    detalleArticulo.put("primeraInsercion", true);
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();
                    txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
                    resetTray();
                    chkExtranjero.setDisable(true);
                } else {
                    orden++;
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    cargandoCamposInterface(detalleArticulo);
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                    //en caso de cancelación artículo y vuelva desde 0 a cargar
                    if (tableViewFactura.getItems().size() == 1) {
                        chkExtranjero.setDisable(true);
                    }
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                    image = jsonArtDetImg(codBarra);
                }
                if (image == null) {
                    File file = new File(PATH.PATH_NO_IMG);
                    image = new Image(file.toURI().toString());
                    this.imgProducto.setImage(image);
                }
                this.imgProducto.setImage(image);
                centerImage();
                this.imgProducto = (ImageView) AnimationFX.fadeNode(this.imgProducto);
            } catch (NumberFormatException | ParseException e) {
                estado = false;
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {
            if (!textFieldCod.getText().equalsIgnoreCase("")) {
                textFieldCant.setText("1");
                textFieldCod.setText("");
                codBarra = "";
                codDecimal = "";
                mensajeErrorArt("NO SE ENCUENTRA EL ARTÍCULO");
            }
            estado = false;
        }
        return estado;
    }

    private JSONObject jsonArtDet(String cod) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject articulo = null;
        articulo = generarListaArticuloLocal(cod);
        return articulo;
    }

    private JSONObject generarListaArticuloLocal(String cod) {
        JSONParser parser = new JSONParser();
        try {
            Articulo art = artDAO.buscarCod(cod);
            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
    }

    private static JSONObject creandoJsonFactCab() {
        try {
            JSONObject jsonCabFactura = new JSONObject();
            JSONObject estadoFactura = new JSONObject();
            JSONParser parser = new JSONParser();
            estadoFactura.put("idEstadoFactura", 1L);//normal
            JSONObject tipoMoneda = new JSONObject();
            tipoMoneda.put("idTipoMoneda", 1L);//guaraníes
            JSONObject tipoComprobante = new JSONObject();
            //acaité para el tema de mayorista...
            tipoComprobante.put("idTipoComprobante", 1L);//factura contado
            //**********************************************************************
            jsonCabFactura.put("cancelado", true);
            jsonCabFactura.put("caja", datos.get("caja"));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                jsonCabFactura.put("cliente", NuevoClienteFXMLController.getJsonCliente());
            } else {
                jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());
            }
//            jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            jsonCabFactura.put("sucursal", datos.get("sucursal"));//en nulo default NN
            jsonCabFactura.put("nroFactura", null);
            jsonCabFactura.put("estadoFactura", estadoFactura);
            jsonCabFactura.put("tipoComprobante", tipoComprobante);
            jsonCabFactura.put("tipoMoneda", tipoMoneda);
            //**********************************************************************
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            jsonCabFactura.put("fechaEmision", timestampEmision);
            jsonCabFactura.put("fechaMod", timestampEmision);
            jsonCabFactura.put("usuAlta", Identity.getNomFun());
            jsonCabFactura.put("usuMod", Identity.getNomFun());
            //**********************************************************************
            JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            jsonCabFactura.put("nroActual", talona.get("nroActual") + " - " + String.valueOf(talona.get("idTalonariosSucursales")));
            //Consulta de nroActual de talonarios en la BD local....
            return jsonCabFactura;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        JSONObject detalleArticulo = new JSONObject();
        //**********************************************************************
        detalleArticulo.put("articulo", jsonArticulo);
        detalleArticulo.put("descripcion", jsonArticulo.get("descripcion"));
        detalleArticulo.put("cantidad", Double.parseDouble(textFieldCant.getText()));
        detalleArticulo.put("orden", orden);
        JSONObject iva = (JSONObject) jsonArticulo.get("iva");
        detalleArticulo.put("poriva", iva.get("poriva"));
        if (txtCodVendedor.getText().equals("")) {
            detalleArticulo.put("codVendedor", null);
        } else {
            detalleArticulo.put("codVendedor", txtCodVendedor.getText());
        }

        JSONObject seccion = (JSONObject) jsonArticulo.get("seccion");
        if (seccion != null) {
            detalleArticulo.put("seccion", seccion.get("descripcion"));
        } else {
            detalleArticulo.put("seccion", "N/A");
        }
        JSONObject subSeccion = (JSONObject) jsonArticulo.get("seccionSub");
        if (subSeccion != null) {
            detalleArticulo.put("seccionSub", subSeccion.get("descripcion"));
        } else {
            detalleArticulo.put("seccionSub", "N/A");
        }
        detalleArticulo.put("permiteDesc", jsonArticulo.get("permiteDesc"));
        detalleArticulo.put("bajada", jsonArticulo.get("bajada"));
        //**********************************************************************
        //mapeo, no se persiste en el backend excepto "precio", solo para desplegar total por detalle en frontend...
        if ((long) tipoCaja.get("idTipoCaja") == 1 || (long) tipoCaja.get("idTipoCaja") == 3) {//minorista...
            detalleArticulo.put("precio", jsonArticulo.get("precioMin"));
            if ((long) iva.get("poriva") == 0 || chkExtranjero.isSelected()) {
                long porIva = 0;
                if ((long) iva.get("poriva") != 0) {
                    porIva = (long) iva.get("poriva");
                    if (porIva == 5) {
                        double precioMinDouble = Long.parseLong(jsonArticulo.get("precioMin").toString()) / 1.05;
                        long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        if (codDecimal == null) {
                            codDecimal = "";
                        }
                        if (!codDecimal.isEmpty()) {
                            if (codDecimal.length() < 4) {
                                codDecimal = "0." + codDecimal;
                            } else {
                                codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                            }
                            double dec = Double.parseDouble(codDecimal) * Double.parseDouble(String.valueOf(precioMin));
                            detalleArticulo.put("exenta", Math.round(dec));
                            detalleArticulo.put("peso", codDecimal);
                            detalleArticulo.put("cantidad", codDecimal);
                        } else {
                            detalleArticulo.put("exenta", Math.round(precioMin * Double.parseDouble(textFieldCant.getText())));
                            detalleArticulo.remove("precio");
                            detalleArticulo.put("precio", precioMin);
                            detalleArticulo.put("peso", "N/A");
                        }
//                        detalleArticulo.put("exenta", precioMin);
//                        detalleArticulo.remove("precio");
//                        detalleArticulo.put("precio", precioMin);
                    } else if (porIva == 10) {
                        double precioMinDouble = Long.parseLong(jsonArticulo.get("precioMin").toString()) / 1.1;
                        long precioMin = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        if (codDecimal == null) {
                            codDecimal = "";
                        }
                        if (!codDecimal.isEmpty()) {
                            if (codDecimal.length() < 4) {
                                codDecimal = "0." + codDecimal;
                            } else {
                                codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                            }
                            double dec = Double.parseDouble(codDecimal) * Double.parseDouble(String.valueOf(precioMin));
                            detalleArticulo.put("exenta", Math.round(dec));
                            detalleArticulo.put("peso", codDecimal);
                            detalleArticulo.put("cantidad", codDecimal);
                        } else {
                            detalleArticulo.put("exenta", Math.round(precioMin * Double.parseDouble(textFieldCant.getText())));
                            detalleArticulo.remove("precio");
                            detalleArticulo.put("precio", precioMin);
                            detalleArticulo.put("peso", "N/A");
                        }
//                        detalleArticulo.put("exenta", precioMin);
//                        detalleArticulo.remove("precio");
//                        detalleArticulo.put("precio", precioMin);
                    }
                } else {
                    if (codDecimal == null) {
                        codDecimal = "";
                    }
                    if (!codDecimal.isEmpty()) {
                        if (codDecimal.length() < 4) {
                            codDecimal = "0." + codDecimal;
                        } else {
                            codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                        }
                        double dec = Double.parseDouble(codDecimal) * Double.parseDouble(jsonArticulo.get("precioMin").toString());
                        dec *= Double.parseDouble(textFieldCant.getText());
                        detalleArticulo.put("exenta", Math.round(dec));
                        detalleArticulo.put("peso", codDecimal);
                        detalleArticulo.put("cantidad", codDecimal);
                    } else {
                        detalleArticulo.put("exenta", Math.round((Double.parseDouble(jsonArticulo.get("precioMin").toString()) * Double.parseDouble(textFieldCant.getText()))));
                        detalleArticulo.put("peso", "N/A");
                    }
//                    detalleArticulo.put("exenta", jsonArticulo.get("precioMin"));
                }
                detalleArticulo.put("poriva", 0l);
            } else {
                if (codDecimal == null) {
                    codDecimal = "";
                }
                if (!codDecimal.isEmpty()) {
                    if (codDecimal.length() < 4) {
                        codDecimal = "0." + codDecimal;
                    } else {
                        codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                    }
                    double dec = Double.parseDouble(codDecimal) * Double.parseDouble(jsonArticulo.get("precioMin").toString());
                    dec *= Double.parseDouble(textFieldCant.getText());
                    detalleArticulo.put("gravada", Math.round(dec));
                    detalleArticulo.put("peso", codDecimal);
                    detalleArticulo.put("cantidad", codDecimal);
                } else {
                    detalleArticulo.put("gravada", Math.round((Double.parseDouble(jsonArticulo.get("precioMin").toString()) * Double.parseDouble(textFieldCant.getText()))));
                    detalleArticulo.put("peso", "N/A");
                }
//                detalleArticulo.put("gravada", jsonArticulo.get("precioMin"));
            }
            //por si acaso se agrege otro tipo de caja, de vuelta la condición...
        } else if ((long) tipoCaja.get("idTipoCaja") == 2) {//mayorista...
            detalleArticulo.put("precio", jsonArticulo.get("precioMay"));
            if ((long) iva.get("poriva") == 0 || chkExtranjero.isSelected()) {
                long porIva = 0;
                if ((long) iva.get("poriva") != 0) {
                    porIva = (long) iva.get("poriva");
                    if (porIva == 5) {
                        long precio = Long.parseLong(jsonArticulo.get("precioMay").toString());
                        double precioMinDouble = precio / 1.05;
                        precioMinDouble *= Double.parseDouble(textFieldCant.getText());
                        long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioDato);
                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioDato);
                    } else if (porIva == 10) {
                        long precio = Long.parseLong(jsonArticulo.get("precioMay").toString());
                        double precioMinDouble = precio / 1.1;
                        precioMinDouble *= Double.parseDouble(textFieldCant.getText());
                        long precioDato = Long.parseLong(String.valueOf(Math.rint(precioMinDouble)).replace(".0", ""));
                        detalleArticulo.put("exenta", precioDato);
                        detalleArticulo.remove("precio");
                        detalleArticulo.put("precio", precioDato);
                    }
                } else {
                    if (codDecimal == null) {
                        codDecimal = "";
                    }
                    if (!codDecimal.isEmpty()) {
                        if (codDecimal.length() < 4) {
                            codDecimal = "0." + codDecimal;
                        } else {
                            codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                        }
                        double dec = Double.parseDouble(codDecimal) * Double.parseDouble(jsonArticulo.get("precioMay").toString());
                        detalleArticulo.put("exenta", Math.round(dec));
                        detalleArticulo.put("peso", codDecimal);
                        detalleArticulo.put("cantidad", codDecimal);
                    } else {
                        detalleArticulo.put("exenta", jsonArticulo.get("precioMay"));
                        detalleArticulo.put("peso", "N/A");
                    }
//                    detalleArticulo.put("exenta", jsonArticulo.get("precioMay"));
                }
                detalleArticulo.put("poriva", 0l);
            } else {
                if (codDecimal == null) {
                    codDecimal = "";
                }
                if (!codDecimal.isEmpty()) {
                    if (codDecimal.length() < 4) {
                        codDecimal = "0." + codDecimal;
                    } else {
                        codDecimal = numValidator.numberFormat("###,###", Double.parseDouble(codDecimal));
                    }
                    double dec = Double.parseDouble(codDecimal) * Double.parseDouble(jsonArticulo.get("precioMay").toString());
                    detalleArticulo.put("gravada", Math.round(dec));
                    detalleArticulo.put("peso", codDecimal);
                    detalleArticulo.put("cantidad", codDecimal);
                } else {
                    detalleArticulo.put("gravada", jsonArticulo.get("precioMay"));
                    detalleArticulo.put("peso", "N/A");
                }
//                detalleArticulo.put("gravada", jsonArticulo.get("precioMay"));
            }
        }
        //mapeo, no se persiste en el backend, solo para desplegar total por detalle en frontend...        
        //**********************************************************************
        return detalleArticulo;
    }

    private void cargandoCamposInterface(JSONObject detalleArticulo) {
        org.json.JSONObject jsonDetalle = new org.json.JSONObject(detalleArticulo);
//        labelCantidad.setText(detalleArticulo.get("cantidad").toString());
        textFieldCant.setText("1");
        textFieldCod.setText("");
//        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
        long lblTotalgs = Long.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
//        if (jsonDetalle.isNull("exenta")) {
        if (codDecimal == null) {
            codDecimal = "";
        }
        if (!codDecimal.isEmpty()) {
            if (detalleArticulo.get("gravada") != null) {
                precioTotal = (long) detalleArticulo.get("gravada") + lblTotalgs;
            } else if (detalleArticulo.get("exenta") != null) {
                precioTotal = (long) detalleArticulo.get("exenta") + lblTotalgs;
            }
        } else {
            precioTotal = Math.round((Double.parseDouble(detalleArticulo.get("precio").toString()) * Double.parseDouble(detalleArticulo.get("cantidad").toString())) + Double.parseDouble(String.valueOf(lblTotalgs)));
        }
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(precioTotal))));
        seteandoMontoFact();
    }

    private static void seteandoMontoFact() {
        int monto = Integer.valueOf(String.valueOf(precioTotal));
        cabFactura.put("montoFactura", monto);
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    static void persistiendoFact(boolean cancelProd, long idArt) {
        try {
            JSONParser parser = new JSONParser();
            if (cancelProd) {
                if (cancelacionProdPrimera) {
                    FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(false));
                    cancelacionProdPrimera = false;
                    cancelacionProd = true;//permite cambiar a PUT en FormaPagoFXMLController, al finalizar venta...
                }
                FacturaVentaDatos.setIdProducto(idArt);
            } else {
                FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(true));
                cancelacionProd = false;
            }
            JSONObject aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
            JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
            FacturaVentaDatos.setIdCajero(Long.valueOf(usuarioCajero.get("idUsuario").toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    private static long creandoCabFactura(boolean cancelFact) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean estadoCancelProd = false;
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        long idFact = 0l;
        if (cancelFact) {
            JSONObject estadoFactura = new JSONObject();
            estadoFactura.put("idEstadoFactura", 2L);//anulado
            cabFactura.put("estadoFactura", estadoFactura);
        }
        try {
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));
            // NUEVO 
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                if (cancelFact) {
                    JSONObject estadoFactura = new JSONObject();
                    estadoFactura.put("idEstadoFactura", 2L);//anulado
                    cabe.put("estadoFactura", estadoFactura);
                    fact.put("facturaClienteCab", cabe);
                }
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    cabFactura = objFact;
                    if (!jsonDatos.isNull("caida")) {
                        String caida = datos.get("caida").toString();
                        if (caida.equalsIgnoreCase("forma_pago")) {
                            cancelacionProdPrimera = false;
                        } else if (!jsonDatos.isNull("cancelProducto")) {
                            cancelacionProdPrimera = false;
                            estadoCancelProd = true;
                        } else {
                            cancelacionProdPrimera = true;
                        }
                    }
                }
            }
            // NUEVO
            if (cancelacionProdPrimera && idCab == 0L) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
                //Linea para prueba por el rango actual
                long n = 0l;
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idRangoFacturaActual")) {
                    n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
                }
                cabFactura.put("idFacturaClienteCab", n);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = cabFactura.get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                cabFactura.put("nroFactura", nroFact);
                datos.put("nroFact", nroFact);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                int actulizacion = 0;
//                boolean insertVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    insertVenta = Boolean.parseBoolean(datos.get("insercionFacturaVentaCab").toString());
//                }
//                boolean actualizacionVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    if (jsonDatos.isNull("actualizacionLocal")) {
//                        actualizacionVenta = false;
//                    } else {
//                        actualizacionVenta = Boolean.parseBoolean(datos.get("actualizacionLocal").toString());
//                    }
//                }
//                if (!jsonDatos.isNull("cancelProd")) {
//                    cancelacionProdPrimera = false;
//                }
//                if (cancelacionProdPrimera) {
//                    conn.setRequestMethod("POST");//primera vez, sin importar factura cancelación o artículo...
//                    estado = true;
//                    actulizacion = 1;
//                } else if (insertVenta && !actualizacionVenta) {//Operacion realizada para saber si se persistio datos en el Servidor, sino lo realiza de manera local(ACTUALIZACION DE FACTURA CABECERA)
//                    conn.setRequestMethod("PUT");
//                    actulizacion = 1;
//                    if (estadoCancelProd) {
//                        estado = true;
//                    } else {
//                        estado = false;
//                    }
//                }
//                if (actulizacion == 1) {
//                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                    wr.write(cabFactura.toString());
//                    fact.put("facturaClienteCab", cabFactura.toString());
//                    wr.flush();
//                    int HttpResult = conn.getResponseCode();
//                    if (HttpResult == HttpURLConnection.HTTP_OK) {
//                        BufferedReader br = new BufferedReader(
//                                new InputStreamReader(conn.getInputStream(), "utf-8"));
//                        while ((inputLine = br.readLine()) != null) {
//                            cabFactura = (JSONObject) parser.parse(inputLine);
//                            datos.put("ventaServer", true);
//                            if (cabFactura.get("idFacturaClienteCab") != null) {
//                                idFact = (long) cabFactura.get("idFacturaClienteCab");
//                                //Setear Datos para saber que id se persistió en facturaClienteCab del servidor
//                                //en el caso que persista primeramente con conexion, y para la actualizacion, la conexión se vaya al maso...
//                                if (cancelacionProdPrimera) {
//                                    datos.put("insercionFacturaVentaCab", true);
//                                    datos.put("insercionIdFactClienteCabServidor", idFact);
//                                    datos.put("nroFact", cabFactura.get("nroFactura").toString());
//                                }
//                            }
//
//                        }
//                        br.close();
//                    } else {
//                        idFact = generarFacturaCabLocal();
//                    }
//                } else {
//                    idFact = generarFacturaCabLocal();
//                }
//            } else {
            idFact = generarFacturaCabLocal();
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //para registrar facturaIni
        org.json.JSONObject jsonEstadoInicio = new org.json.JSONObject(datos);
        boolean estadoFactInicial = false;
        if (!jsonEstadoInicio.isNull("estadoFacturaInicial")) {
            estadoFactInicial = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        if (!estadoFactInicial) {
            datos.put("facturaInicial", cabFactura.get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("FacturaFinal", cabFactura.get("nroFactura").toString());
        if (idFact != 0l && cancelFact && estado) {//que sea del tipo cancelación factura...
            JSONArray jsonArrayFactDet = creandoJsonFactDet(cabFactura);
            if (creandoFactDet(jsonArrayFactDet)) {
            }
        }
        actualizarDatos();
        return idFact;
    }

    public static JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < detalleArtList.size(); i++) {
            detalleArtList.get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(detalleArtList.get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }

    private static boolean creandoFactDet(JSONArray jsonArray) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Boolean.parseBoolean(datos.get("ventaServer").toString())) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteDet/insercionMasiva");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonArrayFactDet.toString());
//                fact.put("facturaDetalle", jsonArrayFactDet.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exitoInsertarDet = (boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//                }
//            } else {
            exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }

    private static boolean registrandoFacturaDetLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private static long generarFacturaCabLocal() {
        String sql = "";
        ConexionPostgres.conectar();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (cancelacionProdPrimera) {
            datos.put("insercionFacturaVentaCabLocal", true);
            //UUID ACTUAL PARA MODIFICAR FACTURA
            long uuid = VentasUtiles.recuperarId() + 1;
            datos.put("uuidCassandraActual", String.valueOf(uuid));
            sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'insertar');";
            Utilidades.log.info("-->> " + sql);
        } else {
            long idFactCab = 0L;
            if (!json.isNull("idFactClienteCabServidor")) {
                idFactCab = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
            }
            if (idFactCab != 0 && !dato) {
                //UUID ACTUAL PARA MODIFICAR FACTURA
                long uuid = VentasUtiles.recuperarId() + 1;
                cabFactura.put("idFacturaClienteCab", idFactCab);
                sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'actualizar');";
                Utilidades.log.info("-->> " + sql);
                datos.put("uuidCassandraActual", String.valueOf(uuid));
                //para que solo una ves entre aqui luego ya actualice nada mas...
                dato = true;
                datos.put("actualizacionLocal", true);
            } else {
                String operacion = "insertar";
                if (idFactCab != 0) {
                    cabFactura.put("idFacturaClienteCab", idFactCab);
                    operacion = "actualizar";
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + datos.get("uuidCassandraActual").toString() + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
                Utilidades.log.info("-->> " + sql);
                datos.put("actualizacionLocal", true);
            }
        }
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS FACTURA VENTA CAB ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        cabFactura.put("idFacturaClienteCab", datos.get("idRangoFacturaActual"));

        return Long.parseLong(datos.get("idRangoFacturaActual").toString());
    }

    static void suprimirProducto(TableView<JSONObject> tabla, Label labelTotalGs, double declarado,
            Label labelCantidad, ImageView imgProducto, CheckBox chkExtranj, TextField txtCod) {
        JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
        JSONObject articulos = (JSONObject) detalle.get("articulo");
        long codArticulo = (long) articulos.get("codArticulo");
        JSONObject jsonArticulo = hashJsonArticulo.get(codArticulo);
        double cantTotal = Double.parseDouble(detalle.get("cantidad").toString());
        double dif = cantTotal - declarado;
        int total = Integer.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        double neto = 0;
        double resultado = 0;
        String dato = "";
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        double nArt = Double.parseDouble(datos.get("nArticulos").toString());
//        datos.put("nArticulos", nArt - cantTotal);
        datos.put("nArticulos", nArt - declarado);
        if (dif <= 0) {
            resultado = Double.parseDouble(detalle.get("precio").toString()) * cantTotal;
            List<JSONObject> detalleAux = new ArrayList<>();
            resetMapeo();
            int ord = 0;
            for (JSONObject detalleEnCuestion : detalleArtList) {
                JSONObject jsonArtAux = (JSONObject) detalleEnCuestion.get("articulo");
                if (!(codArticulo + "|" + detalle.get("orden").toString()).equalsIgnoreCase(jsonArtAux.get("codArticulo") + "|" + detalleEnCuestion.get("orden"))) {
                    ord++;
                    detalleEnCuestion.put("orden", ord);
                    detalleAux.add(detalleEnCuestion);
                    hashJsonArtDet.put((Long) jsonArtAux.get("idArticulo"), detalleAux.lastIndexOf(detalleEnCuestion));
                    hashJsonArticulo.put((Long) jsonArtAux.get("codArticulo"), jsonArtAux);
                }
            }
            orden = ord;
            detalleArtList = new ArrayList<>();
            detalleArtList = detalleAux;
//            if (ord == 0 && detalleAux.size() == 0) {
//                BuscarClienteFXMLController.resetParamCliente();
//                BuscarClienteFXMLController.resetParamClienteFiel();
//            }
        } else {
            int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
            resultado = Double.parseDouble(detalle.get("precio").toString()) * declarado;
            long resultadoActual = Math.round(Double.parseDouble(detalle.get("precio").toString()) * dif);
            long iva = (long) detalle.get("poriva");
            if (iva == 0l || chkExtranj.isSelected()) {
                detalleArtList.get(index).put("exenta", resultadoActual);
            } else {
                detalleArtList.get(index).put("gravada", resultadoActual);
            }
            detalleArtList.get(index).put("cantidad", dif);
        }
        neto = total - resultado;
        dato = formateador.format(neto);
        labelTotalGs.setText("Gs " + dato);
        labelCantidad.setText("0");
        imgProducto.setImage(null);
        precioTotal = Math.round(neto);
        seteandoMontoFact();
        try {
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                }
            }
            if (idCab == 0L) {
                creandoCabFactura(false);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        tabla.getItems().clear();
        tabla.getItems().addAll(detalleArtList);
        //NEW
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setFacturados(fact);
        actualizarDatos();
        if (tabla.getItems().isEmpty()) {
            chkExtranj.setDisable(false);
            chkExtranj.setSelected(false);
        }
        repeatFocusData(txtCod);
    }

    static void setTearDatos(Label lblRuc, Label lblNombre, String ruc, String nombre) {
        lblRuc.setText(ruc);
        lblNombre.setText(nombre);
    }

    static void regresar(TextField txtCod) {
        repeatFocusData(txtCod);
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }

    @FXML
    private void textFieldCantKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void tableViewFacturaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void keyPressEventos(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (textFieldCant.isFocused()) {
                textFieldCant.positionCaret(textFieldCant.getLength());
                if (!textFieldCant.getText().endsWith(".")) {
                    if (Double.parseDouble(textFieldCant.getText()) > 999) {
                        textFieldCant.setText("1");
                        mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
                    } else {
                        enterEstado = false;
                        textFieldCod.requestFocus();
                    }
                } else {
                    enterEstado = false;
                    textFieldCod.requestFocus();
                }
            } else if (textFieldCod.isFocused()) {
                if (textFieldCod.getText().equals("")) {
                    enterEstado = false;
                    textFieldCant.requestFocus();
                } else {
                    enterEstado = true;
                }
            } else if (tableViewFactura.isFocused()) {
                enterEstado = false;
                textFieldCant.requestFocus();
            } else if (txtCodVendedor.isFocused()) {
                textFieldCod.requestFocus();
            }
        }
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneFactura.getChildren()
                .get(anchorPaneFactura.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

//    PARA NUESTRO ENTORNO
//    private boolean recuperarGiftEnUso(String codigo) {
//        boolean comprado = false;
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT comprado FROM stock.articulo WHERE comprado=true AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    comprado = true;
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        }
//        return comprado;
//    }
    private boolean recuperarGiftEnUso(String codigo) {
        boolean comprado = false;
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT comprado FROM stk_articulos WHERE comprado=1 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    comprado = true;
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        }
        return comprado;
    }
//    PARA NUESTRO ENTORNO
//    private Map recuperarGiftSinUso(String codigo) {
//        Map mapeo = new HashMap();
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT * FROM stock.articulo WHERE comprado=false AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    mapeo.put("comprado", true);
//                    mapeo.put("codigo", rs.getLong("cod_articulo"));
//                    mapeo.put("saldogift", rs.getDouble("saldogift"));
//                    mapeo.put("fecha", rs.getDate("fechavtogift"));
//                    mapeo.put("descripcion", rs.getString("descripcion"));
//                } else {
//                    mapeo.put("comprado", false);
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        } else {
//            mapeo.put("comprado", false);
//        }
//        return mapeo;
//    }

    private Map recuperarGiftSinUso(String codigo) {
        Map mapeo = new HashMap();
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT * FROM stk_articulos WHERE comprado=0 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    mapeo.put("comprado", true);
                    mapeo.put("codigo", rs.getLong("codigo"));
                    mapeo.put("saldogift", rs.getDouble("saldogift"));
                    mapeo.put("fecha", rs.getDate("fechavtogift"));
                    mapeo.put("descripcion", rs.getString("descripcion"));
                } else {
                    mapeo.put("comprado", false);
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        } else {
            mapeo.put("comprado", false);
        }
        return mapeo;
    }

    private void resetFormulario(long total) {
        JSONObject talonarioSucursal;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
            // primer trío
            long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
            // segundo trío
            long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
            long nroActual = talos.getNroActual();
            txtNumComprobante.setText(Utilidades.patternFactura(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual)));
//                datos.put("nroFact", Long.parseLong(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual).replace("-", "")));

            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");
            txtNumCaja.setText("N°Caja: " + caj.get("descripcion").toString());
            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }
                labelCajeroFunc.setText("Cajero: " + nomFuncionario);
                labelCajeroFunc2.setText(apeFuncionario);
            } else {
                labelCajeroFunc.setText("Cajero: N/A");
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
        primeraInsercion = false;
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", total));
        labelFecha.setText("Fecha  : " + getFechaActual());
        textFieldCant.setText("1");
        cargandoImagen();
        repeatFocus(textFieldCod);
    }

    private void listenCheckExtranjero() {
        chkExtranjero.selectedProperty().addListener(new ChangeListener<Boolean>() {
            @Override
            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                CajaDeDatos.getCaja().put("estadoExt", newValue);
            }
        });
    }
}
