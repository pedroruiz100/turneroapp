/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.MotivoRetiroDinero;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Ticket;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.MotivoRetiroDineroDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.google.gson.GsonBuilder;
import com.javafx.util.StageSecond;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class RetiroDineroFXMLController extends BaseScreenController implements Initializable {

    public static void setTxtCodigo(TextField textFieldCod) {
        txtCodigo = textFieldCod;
    }

    Ticket ticket;
    private JSONObject supervisor;
    String selectMotivoInicial = "-- Seleccione un motivo --";
    private boolean estadoLogueo;
    //para evitar inconvenientes con el message alert enter y escape
    private boolean alert;
    private static TextField txtCodigo;
    private boolean alertEscape;
    private Date date;
    private Timestamp timestamp;
    private Map motivos;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    private MotivoRetiroDineroDAO motivoDAO;

    @Autowired
    private SupervisorDAO superDAO;
    //campos númericos

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneRetiroDinero;
    @FXML
    private AnchorPane anchorPaneCodSup;
    @FXML
    private HBox hBox;
    @FXML
    private Label labelCodSupervisor;
    @FXML
    private PasswordField passwordFieldCodSupervisor;
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private AnchorPane anchorPaneDatosRetiro;
    @FXML
    private Label labelSupervisor;
    @FXML
    private Label labelMonto;
    @FXML
    private Label labelMotivo;
    @FXML
    private TextField textFieldSupervisor;
    @FXML
    private TextField textFieldMonto;
    @FXML
    private ChoiceBox<String> choiceBoxRetiroDinero;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button buttonVolver;
    @FXML
    private VBox vBoxLabel;
    @FXML
    private VBox vBoxText;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        retirarDinero();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneRetiroDineroKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void choiceBoxRetiroDineroKeyReleased(KeyEvent event) {
        keyPressRetiroDinero(event);
    }

    @FXML
    private void textFieldMontoKeyReleased(KeyEvent event) {
        keyPressTextFieldMonto(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        ocultandoParam2();
        asignandoValores();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        FacturaDeVentaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/retiroDineroFXML.fxml", 529, 266, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (!alert) {
                validandoIngreso();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alertEscape) {
                alertEscape = false;
            } else {
                volviendo();
            }
        }
//        if (keyCode == event.getCode().F2) {
//            retirarDinero();
//        }
    }

    private void keyPressTextFieldMonto(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (textFieldMonto.isFocused()) {
                choiceBoxRetiroDinero.requestFocus();
                choiceBoxRetiroDinero.show();
                alert = true;
            }
        }
    }

    private void keyPressRetiroDinero(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            retirarDinero();
        } else if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void asignandoValores() {
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        //CAMPO NUMERICO
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenRetiro();
        //CAMPO NUMERICO
        alert = false;
        alertEscape = false;
        estadoLogueo = false;
    }

    private void ocultandoParam2() {
        vBoxLabel.setVisible(false);
        vBoxText.setVisible(false);
        btnProcesar.setVisible(false);
    }

    private void descubriendoParam2() {
        vBoxLabel.setVisible(true);
        vBoxText.setVisible(true);
        btnProcesar.setVisible(true);
    }

    private void listenRetiro() {
        textFieldMonto.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        textFieldMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMonto.setText(param);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText("");
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMonto.setText(param);
                                    textFieldMonto.positionCaret(textFieldMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMonto.setText("");
                                textFieldMonto.positionCaret(textFieldMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText(oldValue);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldMonto.setText(param);
                        textFieldMonto.positionCaret(textFieldMonto.getLength());
                    });
                }
            }
        });
    }

    private void validandoIngreso() {
        if (!passwordFieldCodSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(passwordFieldCodSupervisor.getText()));
            if (supervisor != null) {
                if (!estadoLogueo) {
                    jsonMotivoRetiroDinero();
                    estadoLogueo = true;
                }
                descubriendoParam2();
                JSONObject usuario = (JSONObject) supervisor.get("usuario");
                textFieldSupervisor.setText(usuario.get("nomUsuario").toString());
                textFieldMonto.requestFocus();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR INCORRECTA.", ButtonType.CLOSE);
                this.alert = false;
                this.alertEscape = true;
                alert.showAndWait();
                if (alert.getResult() == ButtonType.CLOSE) {
                    alert.close();
                } else {
                    alert.close();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "CLAVE DE SUPERVISOR VACÍA.", ButtonType.CLOSE);
            this.alert = true;
            this.alertEscape = true;
            alert.showAndWait();
            if (alert.getResult() == ButtonType.CLOSE) {
                alert.close();
            } else {
                alert.close();
            }
        }
    }

    private void procesandoRetiro() {
//        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿REALIZAR RETIRO DE DINERO?", ok, cancel);
//        this.alert = true;
//        this.alertEscape = true;
//        alert.showAndWait();
//        if (alert.getResult() == ok) {
//            alert.close();
        if (btnProcesar.isVisible()) {
            boolean estado = retiroDinero();
            if (estado) {
                FacturaDeVentaFXMLController.regresar(txtCodigo);
                if (StageSecond.getStageData().isShowing()) {
                    StageSecond.getStageData().close();
                }
                this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/retiroDineroFXML.fxml", 572, 452, true);
            }
        }
//        } else {
//            alert.close();
//        }
    }

    private boolean seleccionMotivo() {
        return choiceBoxRetiroDinero.getSelectionModel().getSelectedItem().equalsIgnoreCase(selectMotivoInicial);
    }

    private String campoMonto() {
        if (!textFieldMonto.getText().contentEquals("")) {
            if (Long.valueOf(numValidator.numberValidator(textFieldMonto.getText())) <= 0) {
                return "EL MONTO DEBE SER MAYOR A CERO.";
            } else {
                return "true";
            }
        } else {
            return "EL MONTO NO DEBE ESTAR VACÍO.";
        }
    }

    private void retirarDinero() {
        String campo = campoMonto();
        if (seleccionMotivo()) {
            Alert alerts = new Alert(Alert.AlertType.WARNING, "DEBES SELECCIONAR UN MOTIVO DE RETIRO.", ButtonType.OK);
            this.alert = true;
            alerts.showAndWait();
            if (alerts.getResult() == ButtonType.OK) {
                alerts.close();
            }
        } else if (campo.contentEquals("true")) {
            procesandoRetiro();
        } else {
            Alert alerts = new Alert(Alert.AlertType.WARNING, campo, ButtonType.OK);
            this.alert = true;
            alerts.showAndWait();
            if (alerts.getResult() == ButtonType.OK) {
                alerts.close();
            }
        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //IMPRESIÓN TICKET RETIRO
    private void imprimirTicket(JSONObject jsonRetiroDinero) {
        try {
            JSONParser parser = new JSONParser();
            Date ahora = new Date();
            SimpleDateFormat fecha = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat hora = new SimpleDateFormat("HH:mm:ss");
            DecimalFormat formateador = new DecimalFormat("###,###.##");
            JSONObject usuarioSup = (JSONObject) supervisor.get("usuario");
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            JSONObject usuarioCajero = Identity.getUsuario();
            JSONObject funcionarioSup = (JSONObject) usuarioSup.get("funcionario");
            JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
            String nombreSup = funcionarioSup.get("nombre").toString() + " " + funcionarioSup.get("apellido").toString();
            String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
            nombreSup = Utilidades.encodingAlambrado(nombreSup.toUpperCase());
            nombreCaj = Utilidades.encodingAlambrado(nombreCaj.toUpperCase());
            ticket = new Ticket();
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject ciudad = (JSONObject) parser.parse(datos.get("ciudad").toString());
            ticket.ticketRetiro(empresa.get("descripcionEmpresa").toString(), sucursal.get("descripcion").toString(),
                    empresa.get("ruc").toString(), sucursal.get("telefono").toString(), sucursal.get("callePrincipal").toString() + " "
                    + sucursal.get("nroLocal").toString() + " "
                    + sucursal.get("primeraLateral").toString() + " - "
                    + ciudad.get("descripcion").toString(), fecha.format(ahora),
                    hora.format(ahora), nombreSup, caja.get("nroCaja").toString(),
                    nombreCaj, "Gs " + formateador.format(jsonRetiroDinero.get("montoRetiro")));
            ticket.print();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //IMPRESIÓN TICKET RETIRO

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, MOTIVO RETIRO DE DINERO -> GET
    private void jsonMotivoRetiroDinero() {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray arrayMotRetDin = null;
        motivos = new HashMap();
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 20)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/motivoRetiroDinero");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    arrayMotRetDin = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//                choiceBoxRetiroDinero.getItems().add(selectMotivoInicial);
//                for (Object obj : arrayMotRetDin) {
//                    JSONObject motRetDin = (JSONObject) obj;
//                    choiceBoxRetiroDinero.getItems().add(motRetDin.get("descripcionMotivoRetiro").toString());
//                    motivos.put(motRetDin.get("descripcionMotivoRetiro"), motRetDin.get("idMotivoRetiro"));
//                }
//                if (!choiceBoxRetiroDinero.getItems().isEmpty()) {
//                    choiceBoxRetiroDinero.getSelectionModel().select(0);
//                }
//            } else {
        generarMotivoRetiroLocal();
//            }
//        } catch (IOException | ParseException ex) {
//            generarMotivoRetiroLocal();
//            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
    }
    //////READ, MOTIVO RETIRO DE DINERO -> GET

    //////READ, RETIRO DE DINERO -> GET
    private boolean retiroDinero() {
        boolean exito = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject retiro = null;
        JSONObject jsonRetiroDinero = creandoJsonRetiro();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/retiroDinero");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoOutput(true);
                conn.setDoInput(true);
                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
                conn.setRequestMethod("POST");
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                wr.write(jsonRetiroDinero.toString());
                wr.flush();
                int HttpResult = conn.getResponseCode();
                if (HttpResult == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream(), "utf-8"));
                    while ((inputLine = br.readLine()) != null) {
                        retiro = (JSONObject) parser.parse(inputLine);
                        int montoRetiro = 0;
                        if (!jsonDatos.isNull("retiroDinero")) {
                            montoRetiro = Integer.parseInt(datos.get("retiroDinero").toString());
                        }
                        int montoCajero = 0;
                        if (!jsonDatos.isNull("retiroDineroCajero")) {
                            montoCajero = Integer.parseInt(datos.get("retiroDineroCajero").toString());
                        }
                        montoRetiro += Integer.parseInt(retiro.get("montoRetiro").toString());
                        montoCajero += Integer.parseInt(retiro.get("montoRetiro").toString());
                        datos.put("retiroDinero", montoRetiro);
                        datos.put("retiroDineroCajero", montoCajero);
                        imprimirTicket(jsonRetiroDinero);
                        imprimirTicket(jsonRetiroDinero);
                        exito = true;
                    }
                    br.close();
                } else {
                    exito = registrarRetiroDineroLocal(jsonRetiroDinero);
                }
            } else {
                exito = registrarRetiroDineroLocal(jsonRetiroDinero);
            }
        } catch (IOException | ParseException ex) {
            exito = registrarRetiroDineroLocal(jsonRetiroDinero);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exito;
    }
    //////READ, RETIRO DE DINERO -> GET

    //////READ, SUPERVISOR -> GET
    private JSONObject jsonClaveSupervisor(String c) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject supervisor = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 20)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/supervisor/auth/" + c + "");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    supervisor = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            } else {
                supervisor = buscarCodSupervisiorLocal(c);
            }
        } catch (IOException | ParseException ex) {
            supervisor = buscarCodSupervisiorLocal(c);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return supervisor;
    }
    //////READ, SUPERVISOR -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, MOTIVO RETIRO DE DINERO
    public void generarMotivoRetiroLocal() {
        JSONParser parser = new JSONParser();
        List<MotivoRetiroDinero> retiro = motivoDAO.listar();
//        choiceBoxRetiroDinero.getItems().add(selectMotivoInicial);
        for (MotivoRetiroDinero re : retiro) {
            try {
                JSONObject motRetDin = (JSONObject) parser.parse(gson.toJson(re.toMotivoRetiroDineroDTO()));
                choiceBoxRetiroDinero.getItems().add(motRetDin.get("descripcionMotivoRetiro").toString());
                motivos.put(motRetDin.get("descripcionMotivoRetiro"), motRetDin.get("idMotivoRetiro"));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        if (!choiceBoxRetiroDinero.getItems().isEmpty()) {
            choiceBoxRetiroDinero.getSelectionModel().select(0);
        }
    }
    //////READ, MOTIVO RETIRO DE DINERO

    //////READ, SUPERVISOR
    public JSONObject buscarCodSupervisiorLocal(String c) {
        try {
            JSONParser parser = new JSONParser();
            Supervisor sup = superDAO.buscarCodSup(c);
            return (JSONObject) parser.parse(gson.toJson(sup.toSupervisorDTO()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return null;
        }
    }
    //////READ, SUPERVISOR

    //////INSERT, RETIRO DE DINERO
    private boolean registrarRetiroDineroLocal(JSONObject jsonRetiroDinero) {
        try {
            JSONObject retiro = null;
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            JSONParser parser = new JSONParser();
            JSONObject supervisor = (JSONObject) parser.parse(jsonRetiroDinero.get("usuarioSupervisor").toString());
            supervisor.remove("fechaAlta");
            supervisor.remove("fechaMod");
            jsonRetiroDinero.remove("usuarioSupervisor");
            jsonRetiroDinero.put("usuarioSupervisor", supervisor);
            boolean estado = false;
            ConexionPostgres.conectar();
            String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonRetiroDinero + "','retiroDinero', 'insertar');";
            System.out.println("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    estado = true;
                    System.out.println("******* Haz agregado un nuevo registro a POSTGRESQL BD ********");
                    retiro = jsonRetiroDinero;
                    int montoRetiro = 0;
                    if (!jsonDatos.isNull("retiroDinero")) {
                        montoRetiro = Integer.parseInt(datos.get("retiroDinero").toString());
                    }
                    int montoCajero = 0;
                    if (!jsonDatos.isNull("retiroDineroCajero")) {
                        montoRetiro = Integer.parseInt(datos.get("retiroDineroCajero").toString());
                    }
                    montoRetiro += Integer.parseInt(retiro.get("montoRetiro").toString());
                    montoCajero += Integer.parseInt(retiro.get("montoRetiro").toString());
                    datos.put("retiroDinero", montoRetiro);
                    datos.put("retiroDineroCajero", montoCajero);
                    imprimirTicket(jsonRetiroDinero);
                    imprimirTicket(jsonRetiroDinero);
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
            return estado;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return false;
        }
    }
    //////INSERT, RETIRO DE DINERO
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON CREANDO RETIRO DE DINERO
    private JSONObject creandoJsonRetiro() {
        date = new Date();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        String motivoDescri = choiceBoxRetiroDinero.getSelectionModel().getSelectedItem();
        JSONObject retiros = new JSONObject();
        JSONObject caja = CajaDeDatos.onlyCaja();
        JSONObject userCajero = Identity.onlyUser();
        JSONObject userSupervisor = (JSONObject) supervisor.get("usuario");
        JSONObject retiro = new JSONObject();
        retiro.put("idMotivoRetiro", motivos.get(motivoDescri));
        retiros.put("fechaRetiro", timestampJSON);
        retiros.put("usuarioCajero", userCajero);
        retiros.put("usuarioSupervisor", userSupervisor);
        retiros.put("montoRetiro", Integer.valueOf(numValidator.numberValidator(textFieldMonto.getText())));
        retiros.put("motivoRetiroDinero", retiro);
        retiros.put("caja", caja);
        return retiros;
    }
    //JSON CREANDO RETIRO DE DINERO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
}
