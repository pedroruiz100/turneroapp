package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.Iva;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.caja.FacturaDeVentaFXMLController.detalleArtList;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class GiftCardFXMLController extends BaseScreenController implements Initializable {

    public static void setCodigo(String codBarra, TableView<JSONObject> tvFactura, Map mapeo, int ord) {
        cod = codBarra;
        table = tvFactura;
        map = mapeo;
        orden = ord;
    }

    static String cod;
    static int orden;
    static Map map;
    static TableView<JSONObject> table;
    private boolean alert;
    private Date date;
    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @FXML
    private AnchorPane anchorPaneApertura;
    @FXML
    private Pane panelMontoApertura;
    @FXML
    private TextField txtMontoGiftCard;
    @FXML
    private Button btnAceptar;
    @FXML
    private Button btnCancelar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void txtMontoGiftCardAction(ActionEvent event) {
    }

    @FXML
    private void btnAceptarAction(ActionEvent event) {
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAperturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void cargandoInicial() {
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField();
    }

    private void listenTextField() {
        txtMontoGiftCard.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMontoGiftCard.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMontoGiftCard.setText(param);
                            txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMontoGiftCard.setText("");
                            txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMontoGiftCard.setText(param);
                                    txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMontoGiftCard.setText("");
                                txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMontoGiftCard.setText(oldValue);
                            txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMontoGiftCard.setText(param);
                        txtMontoGiftCard.positionCaret(txtMontoGiftCard.getLength());
                    });
                }
            }
        });
    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/GiftCardFXML.fxml", 425, 206, true);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
        if (keyCode == event.getCode().ENTER) {
            procesar();
        }
    }

    private void procesar() {
        JSONParser parser = new JSONParser();
        Articulo art = buscarCod(cod);
        if (art.getIdArticulo() == null) {
            art = new Articulo();
            art.setDescripcion(map.get("descripcion").toString());

            Iva iva = new Iva();
            iva.setIdIva(1l);
            art.setIva(iva);

            double val = Long.parseLong(numValidator.numberValidator(txtMontoGiftCard.getText()));
            art.setPrecioMin(Math.round(val));
            art.setPrecioMay(0l);
            art.setCodArticulo(map.get("codigo").toString());
            art.setPermiteDesc(false);

            insertarServidor(art);
        }
        try {
            double val = Long.parseLong(numValidator.numberValidator(txtMontoGiftCard.getText()));
            art.setPrecioMin(Math.round(val));
            JSONObject JSONObj = (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
            JSONObject obj = creandoJsonDetalleArt(JSONObj, orden);

//            JSONObject iva = new JSONObject();
//            iva.put("poriva", 0);
//            obj.put("iva", iva);
            obj.put("seccion", "N/A");
            obj.put("seccionSub", "N/A");
            obj.put("peso", "N/A");
            obj.put("precio", art.getPrecioMin());
            obj.put("poriva", 0l);
            obj.put("codArticulo", cod);

            obj.put("permiteDesc", false);
            obj.put("bajada", false);

            FacturaDeVentaFXMLController.getDetalleArtList().add(obj);
            table.getItems().clear();
            table.getItems().addAll(detalleArtList);

            HashMap map = new HashMap();
            JSONObject jsonOBJ = new JSONObject();
            jsonOBJ.put("codigo", art.getCodArticulo());
            jsonOBJ.put("comprado", 1);
            jsonOBJ.put("saldogift", Long.parseLong(numValidator.numberValidator(txtMontoGiftCard.getText())));
            jsonOBJ.put("fechavtogift", recuperarFecha());
            map.put(art.getCodArticulo(), jsonOBJ);
            FacturaDeVentaFXMLController.setHmGift(map);
            volviendo();
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        JSONObject detalleArticulo = new JSONObject();
        //**********************************************************************
        jsonArticulo.put("bajada", false);
        detalleArticulo.put("articulo", jsonArticulo);
        detalleArticulo.put("descripcion", jsonArticulo.get("descripcion"));
        detalleArticulo.put("cantidad", Double.parseDouble("1"));
        detalleArticulo.put("orden", orden);
        JSONObject iva = (JSONObject) jsonArticulo.get("iva");
        detalleArticulo.put("poriva", iva.get("poriva"));
        detalleArticulo.put("permiteDesc", jsonArticulo.get("permiteDesc"));
        detalleArticulo.put("bajada", jsonArticulo.get("bajada"));
        detalleArticulo.put("exenta", Long.parseLong(jsonArticulo.get("precioMin").toString()));
        //********************************************************************** 
        //mapeo, no se persiste en el backend, solo para desplegar total por detalle en frontend...        
        //**********************************************************************
        return detalleArticulo;
    }

    private boolean insertarServidor(Articulo art) {
        boolean valor = false;
        ConexionPostgres.conectarServer();
        String sql = "INSERT INTO stock.articulo (descripcion, precio_min, precio_may, id_iva, permite_desc, cod_articulo) VALUES "
                + "('" + art.getDescripcion() + "', " + art.getPrecioMin() + ", " + art.getPrecioMay() + " "
                + ", 1, false, " + art.getCodArticulo() + ");";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getConServer().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getConServer().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrarServer();
        return valor;
    }

    private Articulo buscarCod(String cod) {
        Articulo art = new Articulo();
        if (ConexionPostgres.conectarServer()) {
            String sql = "SELECT * FROM stock.articulo WHERE cod_articulo=" + cod;
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    art.setIdArticulo(rs.getLong("id_articulo"));
                    art.setDescripcion(rs.getString("descripcion"));
                    art.setPrecioMin(rs.getLong("precio_min"));
                    art.setPrecioMay(rs.getLong("precio_may"));

                    Iva i = new Iva();
                    i.setIdIva(rs.getLong("id_iva"));
                    art.setIva(i);
                    art.setPermiteDesc(rs.getBoolean("permite_desc"));
                    art.setCodArticulo(rs.getString("cod_articulo"));
                } else {
                    art = new Articulo();
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionPostgres.cerrarServer();
        }
        return art;
    }

    public static Date recuperarFecha() {
        Date date = new Date();
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return Utilidades.stringToSqlDate((year + 1) + "-" + month + "-" + day);
    }

}
