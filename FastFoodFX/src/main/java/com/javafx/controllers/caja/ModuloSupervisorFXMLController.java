package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.ManejoLocal;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Ticket;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.controllers.login.LoginFXMLController;
import javafx.scene.control.ButtonBar;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
@ScreenScoped
public class ModuloSupervisorFXMLController extends BaseScreenController implements Initializable {

    public static boolean isArqueoCaja() {
        return arqueoCaja;
    }

    Ticket ticket;
    boolean alertEscape;
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();

    @Autowired
    ManejoLocalDAO manejoDAO;
    ManejoLocal manejoLocal = new ManejoLocal();
    public static boolean arqueoCaja;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private Button btnInfoFInancieroCaja;
    @FXML
    private Button btnArqueoCaja;
    @FXML
    private Button btnConfiguracionInterna;
    @FXML
    private Button btnCambiarClave;
    @FXML
    private Button btnSalirModulo;
    @FXML
    private Button btnAperturaWeb;
    @FXML
    private Button btnArqueoWeb;

    /**
     * Initializes the controller class.
     *
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnInfoFInancieroCajaAction(ActionEvent event) {
        rendiciones();
    }

    @FXML
    private void btnArqueoCajaAction(ActionEvent event) {
        arquearCaja();
    }

    @FXML
    private void btnConfiguracionInternaAction(ActionEvent event) {
    }

    @FXML
    private void btnCambiarClaveAction(ActionEvent event) {
        cambiandoPass();
    }

    @FXML
    private void btnSalirModuloAction(ActionEvent event) {
        cerrandoSupervisor();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        asignandoVariables();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("rendicion")) {
            boolean rendicion = Boolean.parseBoolean(datos.get("rendicion").toString());
            datos.put("rendicion", rendicion);
            actualizarDatosBD2();
        } else {
            datos.put("rendicion", false);
        }
        arqueoCaja = false;
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void cambiandoPass() {
        this.sc.loadScreen("/vista/util/ModificarPassFXML.fxml", 377, 152, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, true);
    }

    private void cerrandoSupervisor() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        boolean estadoFinan = false;
        boolean rendi = false;
        if (!json.isNull("estadoInformeFinanciero")) {
            estadoFinan = Boolean.parseBoolean(datos.get("estadoInformeFinanciero").toString());
        }
        if (!json.isNull("rendicion")) {
            rendi = Boolean.parseBoolean(datos.get("rendicion").toString());
        }
//        if (estadoFinan) {
//            if (!rendi) {
//                mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA FINALIZAR EL CIERRE DE TURNO.");
//            } else {
        ButtonType btnAceptar = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancelar = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, "¿DESEA CERRAR SESIÓN?".toUpperCase(), btnAceptar, btnCancelar);
        alert2.showAndWait();
        this.alertEscape = false;
        if (alert2.getResult() == btnAceptar) {
            if (!arqueoCaja) {
                actualizarDatos();
            }
            LoginFXMLController.setLlamarTask(false);
            this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, false);
        } else {
            alert2.close();
        }
//            }
//        } else if (!rendi) {
//            mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA FINALIZAR EL CIERRE DE TURNO.");
//        } else {
//            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 289, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, false);
//        }
    }

    private void arquearCaja() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        JSONObject caja = null;
        boolean estado = false;
        if (!json.isNull("caja")) {
            try {
                caja = (JSONObject) parser.parse(datos.get("caja").toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }

        //NEW A PEDIDO DE LA SUPERVISORA PARA QUE SE PUEDA HACER EL CIERRE IGUAL SI NO SE HA FACTURADO NADA
//        if (caja != null && json.isNull("inicio")) {
//            estado = true;
//        } else {
//            if (!datos.containsKey("montoFacturado")) {
//                estado = true;
//                datos.put("rendicion", true);
//            } else {
//                estado = false;
//            }
//        }
        //FINISH
//        if (caja != null && json.isNull("inicio")) {
//        if (estado) {
//            if (Boolean.parseBoolean(datos.get("rendicion").toString())) {
        this.sc.loadScreen("/vista/caja/ArqueoCajaFXML.fxml", 896, 684, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, true);
//            } else {
//                mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA PODER REALIZAR EL ARQUEO DE CAJA.");
//            }
//        } else {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            Alert alerta = new Alert(Alert.AlertType.INFORMATION, "NO SE PUEDE GENERAR EL ARQUEO,\nYA QUE AÚN NO SE HA TRABAJADO COMO CAJERO.", ok);
//            alerta.showAndWait();
//            if (alerta.getResult() == ok) {
//                alerta.close();
//            } else {
//                alerta.close();
//            }
//        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj.toUpperCase(), ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            rendiciones();
        }
        if (keyCode == event.getCode().F2) {
            arquearCaja();
        }
        if (keyCode == event.getCode().F3) {
            realizarAperturaWeb();
        }
        if (keyCode == event.getCode().F4) {
            realizarArqueoWeb();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alertEscape) {
                cerrandoSupervisor();
            } else {
                this.alertEscape = true;
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void rendiciones() {
//        if ("arqueo_caja")) {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd-MM-yyyy");
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        JSONObject caja = null;
        if (!json.isNull("caja")) {
            try {
                caja = (JSONObject) parser.parse(datos.get("caja").toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        try {
//                int montoTotal = Integer.parseInt(datos.get("montoCajero") + "") + Integer.parseInt(datos.get("donacionCajero") + "");
//                int montoDeclarado = Integer.parseInt(datos.get("retiroDineroCajero") + "");
//                int diferencia = montoTotal - montoDeclarado;
            int diferencia = Integer.parseInt(datos.get("totales").toString());
            ticket = new Ticket();
            datos.put("rendicion", true);
            datos.put("estadoInformeFinanciero", true);
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject ciudad = (JSONObject) parser.parse(datos.get("ciudad").toString());

            //PRIMERA IMPRESION
            ticket.ticketCierre(empresa.get("descripcionEmpresa").toString(),
                    sucursal.get("descripcion").toString(), empresa.get("ruc").toString(), sucursal.get("telefono").toString(),
                    sucursal.get("callePrincipal").toString() + " "
                    + sucursal.get("nroLocal").toString() + " "
                    + sucursal.get("primeraLateral").toString() + " - " + ciudad.get("descripcion").toString(), formateador.format(ahora), diferencia + "");
            ticket.print();

            //SEGUNDA IMPRESION
            ticket.ticketCierre(empresa.get("descripcionEmpresa").toString(),
                    sucursal.get("descripcion").toString(), empresa.get("ruc").toString(), sucursal.get("telefono").toString(),
                    sucursal.get("callePrincipal").toString() + " "
                    + sucursal.get("nroLocal").toString() + " "
                    + sucursal.get("primeraLateral").toString() + " - " + ciudad.get("descripcion").toString(), formateador.format(ahora), diferencia + "");
            ticket.print();

            actualizarDatosBD();

        } catch (ParseException ex) {
            mensajeAlerta("SIN INFORME FINANCIERO QUE MOSTRAR.");
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        } catch (Exception ex) {
            mensajeAlerta("SIN INFORME FINANCIERO QUE MOSTRAR.");
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
//            } else {
//                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                Alert alerta = new Alert(Alert.AlertType.INFORMATION, "NO SE PUEDE GENERAR EL INFORME FINANCIERO,\nYA QUE AÚN NO SE HA TRABAJADO COMO CAJERO.", ok);
//                alerta.showAndWait();
//                if (alerta.getResult() == ok) {
//                    alerta.close();
//                } else {
//                    alerta.close();
//                }
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, true);
//        }
    }

    private void asignandoVariables() {
        this.alertEscape = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
    }

    private void actualizarDatos() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
//        datos.put("rendicion", false);
        datos.remove("rendicion");
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(null);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void actualizarDatosBD() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        manejoLocal.setUsuario(DatosEnCaja.getUsers().toString());
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void actualizarDatosBD2() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        manejoLocal.setUsuario(DatosEnCaja.getUsers().toString());
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    @FXML
    private void btnAperturaWebAction(ActionEvent event) {
        realizarAperturaWeb();
    }

    @FXML
    private void btnArqueoWebAction(ActionEvent event) {
        realizarArqueoWeb();
    }

    private void realizarAperturaWeb() {
        this.sc.loadScreen("/vista/estetica/AperturaEsteticaWebFXML.fxml", 468, 253, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, true);
    }

    private void realizarArqueoWeb() {
        this.sc.loadScreen("/vista/estetica/ArqueoCajaWebFXML.fxml", 896, 684, "/vista/caja/moduloSupervisorFXML.fxml", 903, 368, true);
    }
}
