package com.javafx.controllers.caja;

import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.core.domain.Tarjeta;
import com.peluqueria.core.domain.TarjetaConvenio;
import com.peluqueria.core.domain.Vales;
import com.peluqueria.dao.impl.ClienteDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dao.TarjetaConvenioDAO;
import com.peluqueria.dao.TarjetaDAO;
import com.peluqueria.dao.ValeDAO;
import com.peluqueria.dto.ClienteDTO;
import com.google.gson.Gson;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Descuento;
import com.javafx.util.Fecha;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import static java.lang.Math.toIntExact;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoChequeDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoEfectivoDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoFuncDAO;
import com.peluqueria.dao.RangoMonedaExtranjeraDAO;
import com.peluqueria.dao.RangoNotaCredDAO;
import com.peluqueria.dao.RangoPromoDAO;
import com.peluqueria.dao.RangoTarjetaConvenioDAO;
import com.peluqueria.dao.RangoTarjetaDAO;
import com.peluqueria.dao.RangoTarjfielDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.google.gson.GsonBuilder;
import com.javafx.util.AnimationFX;
import com.javafx.util.Toaster;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXToggleButton;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.EventHandler;
import javafx.geometry.Point2D;
import javafx.geometry.Pos;
import javafx.scene.chart.XYChart;
import javafx.scene.control.ListCell;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.Line;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
public class FormaPagoFXMLController extends BaseScreenController implements Initializable {

    private static String datoTotal;
    private static boolean estado = true;
    Tooltip toolTipPanelTarj;
    Alert alert;
    boolean alertEscEnter;

    @Autowired
    TarjetaDAO tarDAO;
    @Autowired
    TarjetaConvenioDAO tarConvenioDAO;
    @Autowired
    ValeDAO valeDAO;
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();

    @Autowired
    private RangoFuncDAO rangoFuncionarioDAO;
    @Autowired
    private RangoTarjetaDAO rangoTarjetaDAO;
    @Autowired
    private RangoTarjfielDAO rangoTarjFielDAO;
    @Autowired
    private RangoEfectivoDAO rangoEfectivoDAO;
    @Autowired
    private RangoTarjetaConvenioDAO rangoTarjetaConvenioDAO;
    @Autowired
    private RangoMonedaExtranjeraDAO rangoMonedaExtraDAO;
    @Autowired
    private RangoChequeDAO rangoChequeDAO;
    @Autowired
    private RangoNotaCredDAO rangoNotaCredDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempDAO;
    @Autowired
    private RangoPromoDAO rangoPromoTempArtDAO;

    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    static boolean caida = false;
    static JSONObject formaDePago;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean exitoInsertarCab;
    private boolean exitoInsertarDet;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos
    //tarjeta convenio
    private List<JSONObject> tarjetaConvenioList;
    private boolean cargarTarjetaConvenio;
    private HashMap<String, JSONObject> hashJsonComboTarjConvenio;
    //tarjeta convenio
    //tarjeta crédito débito
    private boolean cargarTarjeta;
    private HashMap<String, Integer> hashListTarj;
    private HashMap<String, JSONObject> hashJsonComboTarjeta;
    //tarjeta crédito débito
    //vale
    private List<JSONObject> valeList;
    private boolean cargarVale;
    private HashMap<String, JSONObject> hashJsonComboVale;
    //vale
    //cheque
    private ObservableList<String> chequesAsignados;
    private JSONArray arrayCheque;
    private HashMap<String, Integer> hashListCheque;
    private HashMap<String, Integer> hashListNotaCred;
    //cheque
    //tarjeta convenio
    private ObservableList<String> tarjConvAsignadas;
    //tarjeta convenio
    //tarjeta crédito débito
    private ObservableList<String> tarjetasAsignadas;
    //tarjeta crédito débito
    //Nota de créditos
    private ObservableList<String> notaCredAsignadas;
    //Nota de créditos
    //promoción temporada
    private HashMap<JSONObject, Long> hashJsonPromocionDesc;
    long descuentoPromoNf;
    boolean encontradoPromoNf;
    //promoción temporada
    //promoción temporada art.
    private HashMap<JSONObject, Long> hashJsonPromocionArtDesc;
    //promoción temporada art.
    //parámetros total
    long total;
    long descuento;
    long totalAPagar;
    long totalAbonado;
    long donacion;
    long vuelto;
    //parámetros total
    //controlador descuento
    private boolean descTarjeta, descTarjetaConvenio, descDirectivo, descValePorc, descValeMonto;
    private static boolean descTarjetaFiel, descFuncionario, descPromoTemp;
    //controlador descuento
    //sumador descuento
    double descuentoFielSum;
    double descuentoFuncSum;
    double descuentoPromoNfSum;
    double descuentoPromoArtSum;
    double descuentoTarjetaSum;
    double descuentoTarjetaConvSum;
    double descuentoValePorcSum;
    double descuentoValeParesaSum;
    //sumador descuento
    //control, orden de llamada descuento
    boolean entradaPromo1era;
    boolean entradaFunc1era;
    boolean entradaFiel1era;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)
    private static double montoConDes10;
    private static double montoConDes5;
    private static long exenta;
    private HashMap<JSONObject, Long> hashDetallePrecioDesc;
    private HashMap<JSONObject, Long> hashDetallePrecioDescPromoArt;
    //descuentos que se manejan por sección (temporada; funcionario; cliente fiel) no todos los artículos aplica... (IVA)

    //calculo auxiliar para descuentos, acumulativo para factura
    HashMap<JSONObject, Long> montoMap5 = new HashMap<>();
    HashMap<JSONObject, Long> montoMap10 = new HashMap<>();
    HashMap<JSONObject, Long> montoMapExe = new HashMap<>();
    //calculo auxiliar para descuentos, acumulativo para factura

    Toaster toaster;
    long timeToasterTarj;

    boolean deshabilitar = false;
    boolean cargandoInicial;
    XYChart.Series series;
    long descuentoBarChart;
    boolean toggleEnTarj;
    boolean tarjConvActiva;
    boolean valeActivo;
    boolean recursivoListen;

    //focus
    /*boolean focusMonExtr;
    boolean focusCheque;
    boolean focusNotaCred;
    boolean focusTarjCredDeb;
    boolean focusTarjConv;
    boolean focusVale;
    JFXFillTransition JFXFillTransitionTarjConv;
    JFXFillTransition JFXFillTransitionNotaCred;
    JFXFillTransition JFXFillTransitionCheque;
    JFXFillTransition JFXFillTransitionTarjCredDeb;
    JFXFillTransition JFXFillTransitionMonExtr;
    JFXFillTransition JFXFillTransitionEfectivo;
    JFXFillTransition JFXFillTransitionVale;*/
    JFXDialog jfxDialog;

    private Pane panelBusquedaCliente;
    @FXML
    private TextField txtCiCliente;
    @FXML
    private TextField txtNombreCliente;
    @FXML
    private Button btnBuscarCliente;
    @FXML
    private Pane panelBusquedaFuncionario;
    @FXML
    private Button btnBuscarFuncionario;
    @FXML
    private TextField txtNombreFuncionario;
    @FXML
    private TextField txtCiFuncionario;
    @FXML
    private Pane panelBusquedaClienteFiel;
    @FXML
    private Button btnBuscarClienteFiel;
    @FXML
    private TextField txtNombreClienteFiel;
    @FXML
    private TextField txtCiClienteFiel;
    @FXML
    private Pane panelTipoDePago;
    @FXML
    private AnchorPane anchorPaneFormPago;
    @FXML
    private Pane panelBusquedaTipoTarj;
    @FXML
    private Button buttonVolver;
    @FXML
    private Button buttonProcesar;
    @FXML
    private TextField textFieldEfectivo;
    @FXML
    private Label labelEfectivo;
    @FXML
    private Label labelCheque;
    @FXML
    private ListView<String> listViewTarjetasAgregadas;
    @FXML
    private Label labelTarjetasAgregadas;
    @FXML
    private Label labelFormaDePago;
    @FXML
    private Label labelTarjCreDeb;
    @FXML
    private ComboBox<String> comboBoxTarjetas;
    @FXML
    private Pane panelBusquedaTipoTarjConv;
    @FXML
    private Label labelTarjConv;
    @FXML
    private ListView<String> listViewTarjetasConvAgregadas;
    @FXML
    private Label labelTarjetasConvAgregadas;
    @FXML
    private ComboBox<String> comboBoxTarjetasConvenio;
    @FXML
    private Pane panelVales;
    @FXML
    private Label labelValeMonto;
    @FXML
    private TextField textFieldMontoVale;
    @FXML
    private TextField textFieldMontoPorc;
    @FXML
    private Pane panelBusquedaCheque;
    @FXML
    private ListView<String> listViewCheques;
    @FXML
    private Label labelCheques;
    @FXML
    private TextField textFieldEntidad;
    @FXML
    private Label labelEntidad;
    @FXML
    private Label labelNroCheque;
    @FXML
    private TextField textFIeldNroCheque;
    @FXML
    private Label labelMontoCheque;
    @FXML
    private TextField textFieldMonto;
    @FXML
    private Button buttonChequeAgregar;
    @FXML
    private Label labelMontoTarjeta;
    @FXML
    private TextField textFieldMontoTarjeta;
    @FXML
    private Label labelCodAuth;
    @FXML
    private TextField textFieldCodAuth;
    @FXML
    private Button buttonTarjetaAgregar;
    @FXML
    private Button buttonTarjetaConvAgregar;
    @FXML
    private ComboBox<String> comboBoxVale;
    @FXML
    private Label labelValeCI;
    @FXML
    private TextField textFieldValeCI;
    @FXML
    private Pane panelDesDirectiva;
    @FXML
    private Label labelDirecPorc;
    @FXML
    private Label labelValeMontoPorc;
    @FXML
    private TextField textFieldMontoValePorc;
    @FXML
    private Label labelNroVale;
    @FXML
    private TextField textFieldNroVale;
    @FXML
    private Pane paneTotales;
    @FXML
    private Label labelTotalesTitulo;
    @FXML
    private VBox vBoxTotales;
    @FXML
    private Label labelTotal;
    @FXML
    private TextField textFieldTotal;
    @FXML
    private Label labelDescuento;
    @FXML
    private TextField textFieldDescuento;
    @FXML
    private Label labelTotalAPagar;
    @FXML
    private TextField textFieldTotalAPagar;
    @FXML
    private Label labelTotalAbonado;
    @FXML
    private TextField textFieldTotalAbonado;
    @FXML
    private Label labelVuelto;
    @FXML
    private TextField textFieldVuelto;
    @FXML
    private Pane panePromoTemp;
    @FXML
    private Pane panelNotaCredito;
    @FXML
    private Label labelNroNotaCred;
    @FXML
    private TextField textFieldNroNotaCred;
    @FXML
    private Label labelNotaCredMonto;
    @FXML
    private TextField textFieldMontoNotaCred;
    @FXML
    private Pane panelTipoDePagoExtranjera;
    @FXML
    private Label labelReal;
    @FXML
    private Label labelDolar;
    @FXML
    private Label labelPeso;
    @FXML
    private TextField textFieldDolarCant;
    @FXML
    private TextField textFieldRealCant;
    @FXML
    private TextField textFieldPesoCant;
    @FXML
    private TextField textFieldDolarGs;
    @FXML
    private TextField textFieldRealGs;
    @FXML
    private TextField textFieldPesoGs;
    @FXML
    private Label labelMontoRetencion;
    @FXML
    private TextField textFieldMontoRetencion;
    @FXML
    private Label labelRetencion;
    @FXML
    private Pane panelMontoRetencion;
    @FXML
    private Button buttonNotaCredAgregar;
    @FXML
    private ListView<String> listViewNotasCred;
    @FXML
    private Label txtNroFactura;
    @FXML
    private Line lineStrokeTotal;
    @FXML
    private AnchorPane anchorPanelBusquedaCliente;
    @FXML
    private JFXToggleButton toggleButton;
    @FXML
    private StackPane stackPane;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void txtCiClienteAction(ActionEvent event) {
    }

    @FXML
    private void txtNombreClienteAction(ActionEvent event) {
    }

    @FXML
    private void btnBuscarClienteAction(ActionEvent event) {
        buscandoCliente();
    }

    @FXML
    private void btnBuscarFuncionarioAction(ActionEvent event) {
        mouseFuncionario();
    }

    @FXML
    private void txtNombreFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void txtCiFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void btnBuscarClienteFielAction(ActionEvent event) {
        mouseTarjetaClienteFiel();
    }

    @FXML
    private void txtNombreClienteFielAction(ActionEvent event) {
    }

    @FXML
    private void txtCiClienteFielAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneFormPagoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonProcesarAction(ActionEvent event) {
        finalizandoVenta();
    }

    @FXML
    private void comboBoxTarjetasMouseClicked(MouseEvent event) {
        mouseEventComboBoxTarj(event);
    }

    @FXML
    private void comboBoxTarjetasKeyReleased(KeyEvent event) {
        keyPressComboBoxTarj(event);
    }

    @FXML
    private void comboBoxTarjetasConvenioMouseClicked(MouseEvent event) {
        mouseEventComboBoxTarjConv(event);
    }

    @FXML
    private void comboBoxTarjetasConvenioKeyReleased(KeyEvent event) {
        keyPressComboBoxTarjConv(event);
    }

    @FXML
    private void buttonChequeAgregarAction(ActionEvent event) {
        agregandoCheque();
    }

    @FXML
    private void buttonTarjetaAgregarAction(ActionEvent event) {
        agregandoTarjeta();
    }

    @FXML
    private void buttonTarjetaConvAgregarAction(ActionEvent event) {
    }

    @FXML
    private void buttonNotaCredAgregarAction(ActionEvent event) {
        agregandoNotaCred();
    }

    @FXML
    private void listViewChequesKeyReleased(KeyEvent event) {
        quitandoCheque(event);
    }

    @FXML
    private void listViewNotasCredKeyReleased(KeyEvent event) {
        quitandoNotaCred(event);
    }

    @FXML
    private void listViewTarjetasConvAgregadasKeyReleased(KeyEvent event) {
        quitandoTarjConv(event);
    }

    @FXML
    private void listViewTarjetasAgregadasKeyReleased(KeyEvent event) {
        quitandoTarjeta(event);
    }

    @FXML
    private void comboBoxValeKeyReleased(KeyEvent event) {
        keyPressComboBoxVale(event);
    }

    @FXML
    private void comboBoxValeMouseClicked(MouseEvent event) {
        mouseEventComboBoxVale(event);
    }

    @FXML
    private void checkBoxPromoAction(ActionEvent event) {
        checkBoxPromoTemp();
    }

    private void buttonPromTempAction(ActionEvent event) {
        checkBoxPromoTempButton();
    }

    @FXML
    private void textFieldMontoPorcKeyReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldEfectivo.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldCodAuthKeyReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (textFieldMontoTarjeta.getText().isEmpty()) {
                    listViewTarjetasAgregadas.requestFocus();
                } else {
                    if (!alert.isShowing()) {
                        agregandoTarjeta();
                    }
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldMontoTarjetaReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldCodAuth.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldEfectivoReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldDolarCant.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldDolarCantReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldRealCant.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldRealCantReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldPesoCant.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldPesoCantReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (panelBusquedaTipoTarj.isDisable()) {
                    textFieldEfectivo.requestFocus();
                } else {
                    if (cargarTarjeta) {
                        jsonCargandoTarjetas();
                        if (comboBoxTarjetas.getItems().isEmpty()) {
                            mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                            cargarTarjeta = true;
                        }
                    }
                    comboBoxTarjetas.requestFocus();
                    comboBoxTarjetas.show();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldDolarGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldRealGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldPesoGsReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldEntidadReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFIeldNroCheque.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFIeldNroChequeReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldMonto.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldMontoReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (!textFieldMonto.getText().isEmpty()) {
                    if (listViewCheques.getSelectionModel().getSelectedItems().size() == 0 && !alert.isShowing()) {
                        agregandoCheque();
                    } else {
                        mensajeError("SOLO ES ADMITIDO UN CHEQUE POR FACTURA.");
                    }
                } else {
                    listViewCheques.requestFocus();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldNroNotaCredReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldMontoNotaCred.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldMontoNotaCredReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (!textFieldMontoNotaCred.getText().isEmpty()) {
                    if (listViewNotasCred.getSelectionModel().getSelectedItems().size() == 0 && !alert.isShowing()) {
                        agregandoNotaCred();
                    } else {
                        mensajeError("SOLO ES ADMITIDA UNA NOTA DE CRÉDITO POR FACTURA.");
                    }
                } else {
                    listViewNotasCred.requestFocus();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldNroValeReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (!textFieldValeCI.isDisable()) {
                    textFieldValeCI.requestFocus();
                } else if (!textFieldMontoVale.isDisable()) {
                    textFieldMontoVale.requestFocus();
                } else if (!textFieldMontoValePorc.isDisable()) {
                    textFieldMontoValePorc.requestFocus();
                } else {
                    textFieldMontoRetencion.requestFocus();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldValeCIReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (!textFieldMontoVale.isDisable()) {
                    textFieldMontoVale.requestFocus();
                } else if (!textFieldMontoValePorc.isDisable()) {
                    textFieldMontoValePorc.requestFocus();
                } else {
                    textFieldMontoRetencion.requestFocus();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldMontoValeReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (!textFieldMontoValePorc.isDisable()) {
                    textFieldMontoValePorc.requestFocus();
                } else {
                    textFieldMontoRetencion.requestFocus();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void textFieldMontoValePorcReleased(KeyEvent event) {
        if (event.getCode() == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldMontoRetencion.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    @FXML
    private void panelTipoDePagoOnKeyPressed(KeyEvent event) {
    }

    @FXML
    private void panelTipoDePagoOnMouseClicked(MouseEvent event) {
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
//        focusTransition();
        jsonCargandoTarjetas();
        if (comboBoxTarjetas.getItems().isEmpty()) {
            mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
            cargarTarjeta = true;
        }
        alertEscEnter = false;
        timeToasterTarj = 0;
        toaster = new Toaster();
        alert = new Alert(AlertType.NONE);
        descuentoBarChart = 0;
        recursivoListen = false;
        cargandoInicial = true;
        toggleEnTarj = false;
        numValidator = new NumberValidator();
        JSONParser parser = new JSONParser();
        formaDePago = new JSONObject();
        arrayCheque = new JSONArray();
        exitoInsertarCab = false;
        exitoInsertarDet = false;
        cargarTarjetaConvenio = true;
        cargarTarjeta = true;
        cargarVale = true;
        intValidator = new IntegerValidator();
        listViewCheques.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewTarjetasConvAgregadas.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewTarjetasAgregadas.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        listViewNotasCred.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        comboBoxVale.getItems().add("Seleccione un tipo de vale...");
        comboBoxVale.getSelectionModel().select(0);
        lineStrokeTotal.setVisible(false);
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
            JSONObject factura;
            try {
                factura = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonFact = new org.json.JSONObject(factura);
                if (!jsonFact.isNull("nroFactura")) {
                    txtNroFactura.setText(Utilidades.patternFactura(jsonFact.getString("nroFactura")));
                }
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: (DatosEnCaja.getFacturados() != null)", ex.fillInStackTrace());
            }
        }
        visualizandoCamposVale();
        seteandoTotales();
        listenerCampos();
        tootTip();
        //cheque
        ObservableList<String> list = FXCollections.observableArrayList();
        chequesAsignados = FXCollections.observableArrayList(list);
        //tarjeta convenio
        tarjConvAsignadas = FXCollections.observableArrayList(list);
        //tarjeta
        tarjetasAsignadas = FXCollections.observableArrayList(list);
        //notaCred
        notaCredAsignadas = FXCollections.observableArrayList(list);
        //panel cliente - caja**************************************************
        if (ClienteFXMLController.isClienteSi()) {
            if (ClienteFXMLController.getJsonCliente() != null) {
                if (ClienteFXMLController.getJsonCliente().get("ruc") != null) {
                    txtCiCliente.setText(ClienteFXMLController.getJsonCliente().get("ruc").toString());
                } else {
                    txtCiCliente.setText("");
                }
                if (ClienteFXMLController.getJsonCliente().get("apellido") != null) {
                    txtNombreCliente.setText(ClienteFXMLController.getJsonCliente().get("nombre").toString() + " "
                            + ClienteFXMLController.getJsonCliente().get("apellido").toString());
                } else {
                    txtNombreCliente.setText(ClienteFXMLController.getJsonCliente().get("nombre").toString());
                }
            }
        }
        //panel cliente - caja**************************************************
        //panel clienteFiel - caja**************************************************
        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            if (ClienteFielFXMLController.getJsonClienteFiel().get("cipas") != null) {
                txtCiClienteFiel.setText(ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
            } else {
                txtCiClienteFiel.setText("");
            }
            JSONObject cliente = (JSONObject) ClienteFielFXMLController.getJsonClienteFiel().get("cliente");
            if (cliente.get("apellido") != null) {
                txtNombreClienteFiel.setText(cliente.get("nombre").toString() + " "
                        + cliente.get("apellido").toString());
            } else {
                txtNombreClienteFiel.setText(cliente.get("nombre").toString());
            }
//            se comenta para que queden independientes
//            if (ClienteFXMLController.getJsonCliente() == null) {//por default, sin selección cliente factura, que carge los datos de cliente fiel
//                ClienteFXMLController.seteandoParam(cliente);
//                if (cliente.get("apellido") != null) {
//                    txtNombreCliente.setText(cliente.get("nombre").toString() + " "
//                            + cliente.get("apellido").toString());
//                } else {
//                    txtNombreCliente.setText(cliente.get("nombre").toString());
//                }
//                if (cliente.get("ruc") != null) {
//                    txtCiCliente.setText(cliente.get("ruc").toString());
//                } else {
//                    txtCiCliente.setText("");
//                }
//            }
            validandoClientFiel();
        } else {
            invalidandoClientFiel();
            seteandoTotalAbonado();
        }
        //panel clienteFiel - caja**************************************************
        //panel funcionario - caja**************************************************
        if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            if (FuncionarioFXMLController.getJsonFuncionario().get("ci") != null) {
                txtCiFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("ci").toString());
            } else {
                txtCiFuncionario.setText("");
            }
            if (FuncionarioFXMLController.getJsonFuncionario().get("apellido") != null) {
                txtNombreFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString() + " "
                        + FuncionarioFXMLController.getJsonFuncionario().get("apellido").toString());
            } else {
                txtNombreFuncionario.setText(FuncionarioFXMLController.getJsonFuncionario().get("nombre").toString());
            }
            //no hay carga por default para funcionario...
            validandoFuncionario();
        } else if (ClienteFielFXMLController.getJsonClienteFiel() == null) {
            invalidandoFuncionario();
            seteandoTotalAbonado();
        }
        //panel funcionario - caja**************************************************
        //promoción tanto artículo como sección*********************************
        /*if (Descuento.getPromTempJSONArray() != null || Descuento.getPromTempArtJSONArray() != null) {
            if (FuncionarioFXMLController.getJsonFuncionario() == null && ClienteFielFXMLController.getJsonClienteFiel() == null && !cargandoInicial) {
                checkBoxPromoTemp();
            }
        }*/
        if (ClienteFielFXMLController.getJsonClienteFiel() == null && FuncionarioFXMLController.getJsonFuncionario() == null) {
//            editandoBarChart();
        }
        calculandoTotal();
        habilitandoCajaRapida();//efectivo, moneda extranjera, cliente, cliente fiel, promoción temporada... "caja rápida"
        cargandoInicial = false;
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void buscandoClienteFiel() {
//        if ("cliente_fiel_caja")) {
        this.sc.loadScreen("/vista/caja/ClienteFielFXML.fxml", 880, 626, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        }
    }

    private void buscandoCliente() {
//        if ("cliente_caja")) {
        this.sc.loadScreen("/vista/caja/ClienteFXML.fxml", 890, 748, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        }
    }

    private void buscandoFuncionario() {
//        if ("funcionario_caja") {
//            
//        }
//        
//            ) {
        this.sc.loadScreen("/vista/caja/FuncionarioFXML.fxml", 890, 410, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        }else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//        }
    }

    private void volviendo() {
        this.sc.loadScreen("/vista/caja/facturaVentaFXML.fxml", 1248, 743, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        /*this.alert = true;
        JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();
        JFXButton jfxButton = new JFXButton("Aceptar");
        jfxButton.getStyleClass().add("button-fx-dialog");
        jfxDialog = new JFXDialog(stackPane, jfxDialogLayout, JFXDialog.DialogTransition.TOP);
        VBox vBox = new VBox(15);
        ImageView imageView = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/errorR.png")));
        imageView.setEffect(new DropShadow(10, Color.RED));
        vBox.getChildren().add(imageView);
        vBox.getChildren().add(new Label(msj));
        jfxDialogLayout.setHeading(vBox);
        jfxDialogLayout.setActions(jfxButton);
        jfxDialog.show();
        jfxButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent mouseEvent) -> {
            this.alert = false;
            jfxDialog.close();
        });
        jfxDialog.setOnDialogClosed((JFXDialogEvent event) -> {
            anchorPaneFormPago.setEffect(null);
        });
        anchorPaneFormPago.setEffect(new BoxBlur(3, 3, 3));*/
        alertEscEnter = true;
        alert = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            alert.close();
        }
    }

    private void mensajeAdv(String msj) {
        /*his.alert = true;
        JFXDialogLayout jfxDialogLayout = new JFXDialogLayout();
        JFXButton jfxButton = new JFXButton("Aceptar");
        jfxButton.getStyleClass().add("button-fx-dialog");
        jfxDialog = new JFXDialog(stackPane, jfxDialogLayout, JFXDialog.DialogTransition.TOP);
        VBox vBox = new VBox(15);
        ImageView imageView = new ImageView(
                new Image(getClass().getResourceAsStream("/vista/img/image_thumb.png")));
        imageView.setEffect(new DropShadow(10, Color.YELLOW));
        vBox.getChildren().add(imageView);
        vBox.getChildren().add(new Label(msj));
        jfxDialogLayout.setHeading(vBox);
        jfxDialogLayout.setActions(jfxButton);
        jfxDialog.show();
        jfxButton.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent mouseEvent) -> {
            this.alert = false;
            jfxDialog.close();
        });
        jfxDialog.setOnDialogClosed((JFXDialogEvent event) -> {
            anchorPaneFormPago.setEffect(null);
        });
        anchorPaneFormPago.setEffect(new BoxBlur(3, 3, 3));*/
        alertEscEnter = true;
        alert = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        alert.showAndWait();
        if (alert.getResult() == ButtonType.OK) {
            alert.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {
        comboBoxTarjetas.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if (!recursivoListen) {
                    if (!tarjConvActiva && !valeActivo) {
                        validandoTarjetaListen();
                    } else {
                        if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                            if (validandoTarjetaConvenioTarjCredDeb()) {
                                if (tarjConvActiva) {
                                    mensajeAdv("LA TARJETA CRÉD. / DÉB. SELECCIONADA\nES INCOMPATIBLE CON LA TARJETA CONVENIO CARGADA\nYA QUE AMBAS DISPONEN DTO.");
                                } else {
                                    mensajeAdv("LA TARJETA CRÉD. / DÉB. SELECCIONADA\nES INCOMPATIBLE CON EL VALE U ORDEN DE COMPRA CARGADO.");
                                }
                                textFieldCodAuth.setText("");
                                textFieldMontoTarjeta.setText("");
                                recursivoListen = true;
                                comboBoxTarjetas.getSelectionModel().select(0);
                            }
                        }
                    }
                } else {
                    recursivoListen = false;
                }
            }
        });

        toggleButton.selectedProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if (!toggleEnTarj) {
                    checkBoxPromoTemp();
                }
            }
        });

        textFIeldNroCheque.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(newValue.toString());
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFIeldNroCheque.setText(oldValue.toString());
                            textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFIeldNroCheque.setText(oldValue.toString());
                        textFIeldNroCheque.positionCaret(textFIeldNroCheque.getLength());
                    });
                }
            }
        });
        textFieldCodAuth.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(newValue.toString());
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldCodAuth.setText(oldValue.toString());
                        textFieldCodAuth.positionCaret(textFieldCodAuth.getLength());
                    });
                }
            }
        });
        textFieldNroVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
//                if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                if (newValue.length() < 21) {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(newValue.toString());
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldNroVale.setText(oldValue.toString());
                        textFieldNroVale.positionCaret(textFieldNroVale.getLength());
                    });
                }
            }
        });
        textFieldValeCI.textProperty().addListener((observable, oldValue, newValue) -> {//existen cédulas viejas con letras...
            if (!newValue.contentEquals("")) {
                /*if (GenericValidator.isInt(newValue) || StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(newValue.toString());
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldValeCI.setText(oldValue.toString());
                        textFieldValeCI.positionCaret(textFieldValeCI.getLength());
                    });
                }*/
            }
        });
        textFieldEntidad.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                if (StringUtils.isAlphanumeric(newValue)) {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(newValue.toString());
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldEntidad.setText(oldValue.toString());
                        textFieldEntidad.positionCaret(textFieldEntidad.getLength());
                    });
                }
            }
        });
        textFieldMontoValePorc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        if (Integer.valueOf(newValue) < 31 && Integer.valueOf(newValue) > 0) {
                            Platform.runLater(() -> {
                                textFieldMontoValePorc.setText(newValue.toString());
                                textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                                validandoVale();
                                calculandoValePorc();
                                seteandoTotalAbonado();
                            });
                        } else {
                            textFieldMontoValePorc.setText(oldValue);
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoValePorc.setText(oldValue.toString());
                            textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                            validandoVale();
                            calculandoValePorc();
                            seteandoTotalAbonado();
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldMontoValePorc.setText(oldValue.toString());
                        textFieldMontoValePorc.positionCaret(textFieldMontoValePorc.getLength());
                        validandoVale();
                        calculandoValePorc();
                        seteandoTotalAbonado();
                    });
                }
            } else {
                validandoCambiandoFormaPago();
            }
        });
        textFieldMontoPorc.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isInt(newValue)) {
                        Platform.runLater(() -> {
                            if (Integer.valueOf(newValue) < 31 && Integer.valueOf(newValue) > 0) {
                                textFieldMontoPorc.setText(newValue);
                                textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                                validandoDirectivo();
                            } else {
                                textFieldMontoPorc.setText(oldValue);
                                textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                            }
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoPorc.setText(oldValue);
                            textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                            validandoDirectivo();
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldMontoPorc.setText(oldValue);
                        textFieldMontoPorc.positionCaret(textFieldMontoPorc.getLength());
                        validandoDirectivo();
                    });
                }
            } else {
                Platform.runLater(() -> {
                    invalidandoDirectivo();
                    seteandoTotalAbonado();
                    validandoCambiandoFormaPago();
                });
            }
        });
        textFieldEfectivo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                        calculandoValeParesa();
                    }
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(param);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText("");
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldEfectivo.setText(param);
                                    textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                    calculandoValeParesa();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldEfectivo.setText("");
                                textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                                calculandoValeParesa();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldEfectivo.setText(oldValue);
                            textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldEfectivo.setText(param);
                        textFieldEfectivo.positionCaret(textFieldEfectivo.getLength());
                        seteandoTotalAbonado();
                        calculandoValePorc();
                        calculandoValeParesa();
                    });
                }
            } else {
                seteandoTotalAbonado();
                validandoCambiandoFormaPago();
                calculandoValePorc();
                calculandoValeParesa();
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldDolarCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                        calculandoValeParesa();
                    }
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("US$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(param);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText("");
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("US$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldDolarCant.setText(param);
                                    textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                    calculandoValeParesa();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldDolarCant.setText("");
                                textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                                cotizacionMoneda();
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                                calculandoValeParesa();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldDolarCant.setText(oldValue);
                            textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldDolarCant.setText(param);
                        textFieldDolarCant.positionCaret(textFieldDolarCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                        calculandoValeParesa();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldPesoCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                        calculandoValeParesa();
                    }
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(param);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText("");
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldPesoCant.setText(param);
                                    textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                    calculandoValeParesa();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldPesoCant.setText("");
                                textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                                cotizacionMoneda();
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                seteandoTotalAbonado();
                                calculandoValePorc();
                                calculandoValeParesa();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldPesoCant.setText(oldValue);
                            textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldPesoCant.setText(param);
                        textFieldPesoCant.positionCaret(textFieldPesoCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                        calculandoValeParesa();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldRealCant.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                        seteandoTotales();
                        validandoCambiandoFormaPago();
                        calculandoValePorc();
                        calculandoValeParesa();
                    }
                    if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
                        comboBoxTarjetas.getSelectionModel().select(0);
                    }
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("R$ ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(param);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText("");
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            if (!descTarjeta) {
                                seteandoDescTarjeta();
                            }
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("R$ ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldRealCant.setText(param);
                                    textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                    cotizacionMoneda();
                                    seteandoTotalAbonado();
                                    calculandoValePorc();
                                    calculandoValeParesa();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldRealCant.setText("");
                                textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                                if (!descTarjeta) {
                                    seteandoDescTarjeta();
                                }
                                cotizacionMoneda();
                                seteandoTotalAbonado();
                                calculandoValePorc();
                                calculandoValeParesa();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldRealCant.setText(oldValue);
                            textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                            cotizacionMoneda();
                            seteandoTotalAbonado();
                            calculandoValePorc();
                            calculandoValeParesa();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldRealCant.setText(param);
                        textFieldRealCant.positionCaret(textFieldRealCant.getLength());
                        cotizacionMoneda();
                        seteandoTotalAbonado();
                        calculandoValePorc();
                        calculandoValeParesa();
                    });
                }
            } else {
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    panelBusquedaTipoTarjConv.setDisable(true);
                    panelVales.setDisable(true);
                    panelBusquedaCheque.setDisable(true);
                }
            }
        });
        textFieldMontoRetencion.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    validandoMontoRetencion();
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText(param);
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            validandoMontoRetencion();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText("");
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            invalidandoMontoRetencion();
                            seteandoTotalAbonado();
                            validandoCambiandoFormaPago();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoRetencion.setText(param);
                                    textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                                    validandoMontoRetencion();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoRetencion.setText("");
                                textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                                invalidandoMontoRetencion();
                                seteandoTotalAbonado();
                                validandoCambiandoFormaPago();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoRetencion.setText(oldValue);
                            textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                            validandoMontoRetencion();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoRetencion.setText(param);
                        textFieldMontoRetencion.positionCaret(textFieldMontoRetencion.getLength());
                        validandoMontoRetencion();
                    });
                }
            }
        });
        textFieldNroNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        textFieldNroNotaCred.setText(newValue.toString());
                        textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        textFieldNroNotaCred.setText(oldValue.toString());
                        textFieldNroNotaCred.positionCaret(textFieldNroNotaCred.getLength());
                    });
                }
            }
        });
        textFieldMontoNotaCred.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (textFieldNroNotaCred.getText().contentEquals("")) {
                Platform.runLater(() -> {
//                    mensajeError("DEBE INGRESAR EL NÚMERO DE NOTA DE CRÉDITO.");
                    textFieldMontoNotaCred.setText("");
                    textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                });
            } else if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    validandoNotaCredito();
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(param);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText("");
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoNotaCred.setText(param);
                                    textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoNotaCred.setText("");
                                textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoNotaCred.setText(oldValue);
                            textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoNotaCred.setText(param);
                        textFieldMontoNotaCred.positionCaret(textFieldMontoNotaCred.getLength());
                    });
                }
            }
        });
        textFieldMontoTarjeta.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(param);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText("");
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoTarjeta.setText(param);
                                    textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoTarjeta.setText("");
                                textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoTarjeta.setText(oldValue);
                            textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoTarjeta.setText(param);
                        textFieldMontoTarjeta.positionCaret(textFieldMontoTarjeta.getLength());
                    });
                }
            }
        });
        textFieldMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMonto.setText(param);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText("");
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMonto.setText(param);
                                    textFieldMonto.positionCaret(textFieldMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMonto.setText("");
                                textFieldMonto.positionCaret(textFieldMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText(oldValue);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMonto.setText(param);
                        textFieldMonto.positionCaret(textFieldMonto.getLength());
                    });
                }
            }
        });
        textFieldMontoVale.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(param);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText("");
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMontoVale.setText(param);
                                    textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                    calculandoValeParesa();
                                    seteandoTotalAbonado();
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMontoVale.setText("");
                                textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                                calculandoValeParesa();
                                seteandoTotalAbonado();
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMontoVale.setText(oldValue);
                            textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                            calculandoValeParesa();
                            seteandoTotalAbonado();
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        textFieldMontoVale.setText(param);
                        textFieldMontoVale.positionCaret(textFieldMontoVale.getLength());
                        calculandoValeParesa();
                        seteandoTotalAbonado();
                    });
                }
            }
        });
        comboBoxVale.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                visualizandoCamposVale();
            }
        });
        /*comboBoxVale.setCellFactory(
                new Callback<ListView<String>, ListCell<String>>() {
            @Override
            public ListCell<String> call(ListView<String> param) {
                final ListCell<String> cell = new ListCell<String>() {
                    {
                        super.setPrefWidth(100);
                    }

                    @Override
                    public void updateItem(String item,
                            boolean empty) {
                        super.updateItem(item, empty);
                        if (item != null) {
                            setText(item);
                            if (!item.contains("ASOCIACION") && !item.contains("FUNCIONARIO") && !item.contains("Seleccione un tipo de vale...")) {
                                setDisable(true);
                                setOpacity(0.3);
                            }
                        } else {
                            setText(null);
                        }
                    }
                };
                return cell;
            }
        });*/
    }

    private void mouseEventComboBoxTarj(MouseEvent event) {
        if (cargarTarjeta) {
            jsonCargandoTarjetas();
            if (comboBoxTarjetas.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                cargarTarjeta = true;
            }
        }
    }

    private void mouseEventComboBoxTarjConv(MouseEvent event) {
        if (cargarTarjetaConvenio) {
            jsonCargandoTarjetasConvenio();
            if (comboBoxTarjetasConvenio.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON TARJETAS CONVENIO DISPONIBLES.");
                cargarTarjetaConvenio = true;
            }
        }
    }

    private void mouseEventComboBoxVale(MouseEvent event) {
        if (cargarVale) {
            jsonCargandoVales();
            if (comboBoxVale.getItems().isEmpty()) {
                mensajeError("NO SE ENCONTRARON VALES DISPONIBLES.");
                cargarVale = true;
            }
        }
    }

    private void keyPressComboBoxTarj(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTarjeta) {
                jsonCargandoTarjetas();
                if (comboBoxTarjetas.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON TARJETAS DISPONIBLES.");
                    cargarTarjeta = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxTarjetas.show();
            } else if (keyCode == event.getCode().ENTER) {
                if (alertEscEnter) {
                    alertEscEnter = false;
                } else {
                    textFieldMontoTarjeta.requestFocus();
                }
            }
        }
    }

    private void keyPressComboBoxTarjConv(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTarjetaConvenio) {
                jsonCargandoTarjetasConvenio();
                if (comboBoxTarjetasConvenio.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON TARJETAS CONVENIO DISPONIBLES.");
                    cargarTarjetaConvenio = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxTarjetasConvenio.show();
            } else if (keyCode == event.getCode().ENTER) {
                if (alertEscEnter) {
                    alertEscEnter = false;
                } else {
                    if (comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem() != null) {
                        agregandoTarjConv();
                    }
                    listViewTarjetasConvAgregadas.requestFocus();
                    if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                        listViewTarjetasConvAgregadas.getSelectionModel().select(0);
                    }
                }
            }
        }
    }

    private void keyPressComboBoxVale(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarVale) {
                jsonCargandoVales();
                if (comboBoxVale.getItems().isEmpty()) {
                    mensajeError("NO SE ENCONTRARON VALES DISPONIBLES.");
                    cargarVale = true;
                }
            }
            if (!event.getCode().isArrowKey() && keyCode != event.getCode().ENTER) {
                comboBoxVale.show();
            } else if (keyCode == event.getCode().ENTER) {
                if (!alertEscEnter) {
                    if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
                        textFieldMontoRetencion.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
                        textFieldNroVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                        textFieldNroVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
                        textFieldNroVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ORDEN DE COMPRA")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FRIGOMERC")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("INSPECTORATE")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("CELLULAR")) {
                        textFieldMontoVale.requestFocus();
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ESSEN")) {
                        textFieldMontoVale.requestFocus();
                    }
                } else {
                    alertEscEnter = false;
                }
            }
        }
    }

    private void mouseTarjetaClienteFiel() {
        if (!FuncionarioFXMLController.isFuncionarioSi()) {
            if (!alert.isShowing()) {
                buscandoClienteFiel();
            }
        } else {
            mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nEL PANEL FUNCIONARIO DEBE ESTAR VACÍO.");
        }
    }

    private void mouseFuncionario() {
        if (!ClienteFielFXMLController.isClienteFielSi()) {
            if (!alert.isShowing()) {
                buscandoFuncionario();
            }
        } else {
            mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL TARJETA CLIENTE FIEL DEBE ESTAR VACÍO.");
        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            if (!alert.isShowing()) {
                alertEscEnter = false;
                finalizandoVenta();
            }
        } else if (keyCode == event.getCode().F5) {
            if (!alert.isShowing()) {
                alertEscEnter = false;
                buscandoCliente();
            }
        } else if (keyCode == event.getCode().F6) {
            if (!alert.isShowing()) {
                alertEscEnter = false;
                if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    mensajeError("PARA AGREGAR TARJETA CLIENTE FIEL\nEL PANEL FUNCIONARIO DEBE ESTAR VACÍO.");
                } else {
                    buscandoClienteFiel();
                }
            }
        } else if (keyCode == event.getCode().F7) {
            if (!alert.isShowing()) {
                alertEscEnter = false;
                if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                    mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL TARJETA CLIENTE FIEL DEBE ESTAR VACÍO.");
                } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    mensajeError("PARA AGREGAR FUNCIONARIO\nEL PANEL PANEL TARJETA CONVENIO DEBE ESTAR VACÍO.");
                } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
                    mensajeError("PARA AGREGAR FUNCIONARIO\nDEBE QUITAR TODA TARJETA CRÉDITO/DÉBITO.");
                } else if (!listViewCheques.getItems().isEmpty()) {
                    mensajeError("NO ESTÁ PERMITIDO AGREGAR CHEQUE\nEN FUNCIONARIO.");
                } else if (!alert.isShowing()) {
                    buscandoFuncionario();
                }
            }
        } else if (keyCode == event.getCode().ESCAPE) {
            if (!alertEscEnter) {
                if (!alert.isShowing()) {
                    volviendo();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    private void checkBoxPromoTempButton() {
        checkBoxPromoTemp();
    }

    private void checkBoxPromoTemp() {
        if (toggleButton.isSelected()) {
            if (listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                toggleButton.setTextFill(Paint.valueOf("#3ba40e"));
                validandoPromocion();
            } else {
                toggleButton.setSelected(false);
                toggleButton.setTextFill(Paint.valueOf("#f10e0e"));
                mensajeError("PARA APLICAR PROMOCIÓN TEMPORADA\nEL PANEL TARJETAS CONVENIO DEBE ESTAR VACÍO.");
            }
        } else {
            toggleButton.setTextFill(Paint.valueOf("#f10e0e"));
            invalidandoPromocion();
            seteandoTotalAbonado();
            seteandoDescTarjeta();
            if (!valeActivo) {
                validandoCambiandoFormaPago();
            }
            if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                calculandoFuncionario();
            }
        }
    }

    private void tootTip() {
        toolTipPanelTarj = new Tooltip();
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void habilitandoCajaRapida() {
        panelMontoRetencion.setDisable(true);
        panelMontoRetencion.setStyle("-fx-opacity:  0.85;");
        deshabilitar = true;
    }

    private void comboBoxTarjeta() {
        //para habilitar algún descuento con tarjeta crédito, débito, no debe tener otra forma de pago, y solo una tarjeta...
        if (comboBoxTarjetas.getSelectionModel().getSelectedItem() == null || comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar + descuento))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
            lineStrokeTotal.setVisible(false);
            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                validandoClientFiel();
            } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                validandoFuncionario();
            } else {
//                editandoBarChart();
            }
        } else {
            validandoTarjeta();
        }
    }

    private void finalizandoVenta() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (listViewTarjetasAgregadas.getItems().isEmpty() && textFieldEfectivo.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && listViewCheques.getItems().isEmpty() && listViewNotasCred.getItems().isEmpty() && comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")
                && textFieldPesoGs.getText().contentEquals("") && textFieldRealGs.getText().contentEquals("") && textFieldDolarGs.getText().contentEquals("")) {
            mensajeError("DEBE INGRESAR UN MONTO, EFECTIVO; CHEQUE; VALE; TARJETAS; Y/O NOTA DE CRÉDITO.");
        } else if (totalAbonado < totalAPagar) {
            mensajeError("EL MONTO A ABONAR NO ES SUFICIENTE.");
        } else {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(AlertType.CONFIRMATION, "¿DESEA FINALIZAR LA VENTA? ", ok, cancel);
            alert.showAndWait();
            if (alert.getResult() == ok) {
                alert.close();
//                if ("factura_venta_procesar")) {
                //validación en caso de ser Vale...
                String msjVale = "";
                boolean pasa = true;
                if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
                    if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS") && textFieldMontoVale.getText().contentEquals("")) {
                        msjVale = "EL CAMPO MONTO VALE NO DEBE ESTAR VACÍO.";
                        pasa = false;
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldMontoValePorc.getText().contentEquals("")) {
                            msjVale = "EL CAMPO PORCENTAJE DESCUENTO VALE\nNO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                        if (textFieldValeCI.getText().contentEquals("")) {
                            msjVale = "EL CAMPO C.I. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    } else {
                        if (textFieldNroVale.getText().contentEquals("")) {
                            msjVale = "EL CAMPO NRO. VALE NO DEBE ESTAR VACÍO.";
                            pasa = false;
                        }
                    }
                }
                if (pasa) {
                    datos.put("energiaElectrica", false);
                    actualizarDatos();
                    try {
                        actualizandoCabFactura();
                        long dato = CancelacionProductoFXMLController.recuperarIdDato();
                        if (dato != 0l) {
                            datos.put("idDato", dato);
                        }
                        actualizarDatos();
                        seteandoCamposMsjFinal();
                        seteandoDescuentos();
                        seteandoTarjetas();
                        seteandoCheques();
                        seteandoNotaCred();
                        int valor = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
                        int sumEfe = 0;
                        if (!json.isNull("sumEfeRecibido")) {
                            sumEfe = Integer.parseInt(datos.get("sumEfeRecibido").toString());
                        }
                        int sum = sumEfe;
                        datos.put("sumEfeRecibido", (sum + valor));
                        if (!textFieldEfectivo.getText().equalsIgnoreCase("")) {
                            seteandoEfectivos();
                        }
//                            if (!textFieldMontoNotaCred.getText().equalsIgnoreCase("")) {
//                            seteandoNotaCred();
//                            }
                        if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
                            seteandoAsociacion();
                        } else if (!textFieldMontoVale.getText().equalsIgnoreCase("")) {
                            seteandoVales();
                        }
                        if (!textFieldDolarCant.getText().equalsIgnoreCase("") && !textFieldDolarGs.getText().equalsIgnoreCase("")) {
                            seteandoDolares();
                        }
                        if (!textFieldRealCant.getText().equalsIgnoreCase("") && !textFieldRealGs.getText().equalsIgnoreCase("")) {
                            seteandoReales();
                        }
                        if (!textFieldPesoCant.getText().equalsIgnoreCase("") && !textFieldPesoGs.getText().equalsIgnoreCase("")) {
                            seteandoPesos();
                        }
                        if (!textFieldMontoRetencion.getText().equalsIgnoreCase("")) {
                            seteandoRetencionciones();
                        }
                        JSONObject factJson = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        factJson.put("forma_pago", formaDePago);
                        fact.put("facturaClienteCab", factJson);
                        int contFa = 0;
//                            if (!json.isNull("contFact")) {
//                                contFa = Integer.parseInt(datos.get("contFact").toString());
//                            }
                        int contFact = contFa;
//                            datos.put("contFact", (contFact + 1));
                        int sumFa = 0;
//                            if (!json.isNull("sumFact")) {
//                                sumFa = Integer.parseInt(datos.get("sumFact").toString());
//                            }
                        int sumFact = sumFa;
//                            datos.put("sumFact", sumFact + Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
                        seteandoFuncionario();
                        seteandoSumaTotales();
                        //reseteando campos cliente, cliente fiel, funcionario, factura venta para próxima transacción...
                        FacturaDeVentaFXMLController.resetParam();
                        ClienteFXMLController.resetParam();
//                            ClienteFielFXMLController.resetParam();
//                            FuncionarioFXMLController.resetParam();
                        datos.remove("cancelProducto");
                        datos.remove("energiaElectrica");
                        actualizarDatos();
                        tarjConvActiva = false;
                        valeActivo = false;
//                            limpiandoFillTransition();
                        this.sc.loadScreen("/vista/caja/mensajeFinalVentaFXML.fxml", 562, 288, "/vista/caja/formaPagoFXML.fxml", 1286, 720, true);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    mensajeError(msjVale);
                }
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/formaPagoFXML.fxml", 1286, 720, false);
//                }
            } else if (alert.getResult() == cancel) {
                alert.close();
            }
        }
    }

    private void seteandoCamposMsjFinal() {
        MensajeFinalVentaFXMLController.setTotalAbonado(Integer.valueOf(numValidator.numberValidator(textFieldTotalAbonado.getText())));
        MensajeFinalVentaFXMLController.setVuelto(Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText())));
        MensajeFinalVentaFXMLController.setTotal(Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())));
        MensajeFinalVentaFXMLController.setDescuento(Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())));
        MensajeFinalVentaFXMLController.setTotalAPagar(Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
        datos.put("totalApagar", Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText())));
    }

    private void agregandoCheque() {
        if (textFieldEntidad.getText().isEmpty()) {
            if (panelBusquedaCheque.isDisable()) {
                mensajeError("ESTA FORMA DE PAGO ESTA DESHABILITADA");
            } else {
                mensajeError("DEBE INGRESAR EL NOMBRE DE LA ENTIDAD FINANCIERA.");
            }
        } else if (textFIeldNroCheque.getText().isEmpty()) {
            mensajeError("DEBE INGRESAR EL NRO. DE CHEQUE.");
        } else if (!listViewCheques.getItems().isEmpty()) {
            mensajeError("TEMPORALMENTE DESHABILITADO PARA MÁS DE UN CHEQUE\nDEBIDO A INCOMPATIBILIDAD DE ESTRUCTURAS.");
        } else if (textFieldMonto.getText().isEmpty()) {
            mensajeError("DEBE INGRESAR EL MONTO DEL CHEQUE.");
        } else {
            String cheque = textFieldEntidad.getText() + " - " + textFIeldNroCheque.getText() + " - " + textFieldMonto.getText() + " (Borrar)";
            String chequeUnico = textFieldEntidad.getText() + " - " + textFIeldNroCheque.getText();//válido solo para la transacción actual...
            if (listViewCheques.getItems().isEmpty()) {
                chequesAsignados.add(cheque);
                hashListCheque = new HashMap<>();
                hashListCheque.put(chequeUnico, 0);
                listViewCheques.setItems(chequesAsignados);
                if (descTarjeta) {
                    calculandoTarjeta(false);
                    descTarjeta = false;
                }
                seteandoTotalAbonado();
                limpiandoCheque();
                listViewCheques.requestFocus();
                if (!listViewCheques.getItems().isEmpty()) {
                    listViewCheques.getSelectionModel().select(0);
                }
            } else if (hashListCheque.get(chequeUnico) == null) {
                chequesAsignados.add(cheque);
                listViewCheques.setItems(chequesAsignados);
                hashListCheque.put(chequeUnico, listViewCheques.getItems().indexOf(cheque));
                if (descTarjeta) {
                    calculandoTarjeta(false);
                    descTarjeta = false;
                }
                seteandoTotalAbonado();
                limpiandoCheque();
                listViewCheques.requestFocus();
                if (!listViewCheques.getItems().isEmpty()) {
                    listViewCheques.getSelectionModel().select(0);
                }
            } else {
                mensajeError("EL CHEQUE YA SE AGREGÓ.");
            }
        }
    }

    private void agregandoNotaCred() {
        if (textFieldNroNotaCred.getText().equalsIgnoreCase("")) {
            mensajeError("DEBES INGRESAR NÚMERO DE LA NOTA DE CRÉDITO.");
        } else if (textFieldMontoNotaCred.getText().equalsIgnoreCase("")) {
            mensajeError("DEBES INGRESAR EL MONTO DE LA NOTA DE CRÉDITO.");
        } else if (!listViewNotasCred.getItems().isEmpty()) {
            mensajeError("TEMPORALMENTE DESHABILITADO PARA MÁS DE UNA NOTA DE CRÉDITO\nDEBIDO A INCOMPATIBILIDAD DE ESTRUCTURAS.");
        } else {
            String nota = textFieldNroNotaCred.getText() + " - " + textFieldMontoNotaCred.getText() + " (Borrar)";
            if (listViewNotasCred.getItems().isEmpty()) {
                notaCredAsignadas.add(nota);
                hashListNotaCred = new HashMap<>();
                hashListNotaCred.put(textFieldNroNotaCred.getText(), 0);
                listViewNotasCred.setItems(notaCredAsignadas);
                seteandoTotalAbonado();
                limpiandoNotaCred();
                listViewNotasCred.requestFocus();
                if (!listViewNotasCred.getItems().isEmpty()) {
                    listViewNotasCred.getSelectionModel().select(0);
                }
            } else if (hashListNotaCred.get(textFieldNroNotaCred.getText()) == null) {
                notaCredAsignadas.add(nota);
                listViewNotasCred.setItems(notaCredAsignadas);
                hashListNotaCred.put(textFieldNroNotaCred.getText(), listViewNotasCred.getItems().indexOf(nota));
                seteandoTotalAbonado();
                limpiandoNotaCred();
                listViewNotasCred.requestFocus();
                if (!listViewNotasCred.getItems().isEmpty()) {
                    listViewNotasCred.getSelectionModel().select(0);
                }
            } else {
                mensajeError("LA NOTA DE CRÉDITO YA SE AGREGÓ.");
            }
        }
    }

    private void quitandoCheque(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (!listViewCheques.getItems().isEmpty() && !alert.isShowing()) {
                String[] parts = listViewCheques.getSelectionModel().getSelectedItem().split(" - Gs ");
                hashListCheque.remove(parts[0]);
                chequesAsignados.remove(listViewCheques.getSelectionModel().getSelectedItem());
                arrayCheque.remove(listViewCheques.getSelectionModel().getSelectedItem());
                listViewCheques.setItems(chequesAsignados);
                seteandoTotalAbonado();
                seteandoDescTarjeta();
                if (listViewCheques.getItems().isEmpty()) {
                    validandoCambiandoFormaPago();
                }
                calculandoValePorc();
                calculandoValeParesa();
                seteandoTotalAbonado();
            } else {
                seteandoTotalAbonado();
                seteandoDescTarjeta();
            }
        } else if (!listViewCheques.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewCheques.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldNroNotaCred.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    private void quitandoNotaCred(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (!listViewNotasCred.getItems().isEmpty() && !alert.isShowing()) {
                String[] parts = listViewNotasCred.getSelectionModel().getSelectedItem().split(" - Gs ");
                hashListNotaCred.remove(parts[0]);
                notaCredAsignadas.remove(listViewNotasCred.getSelectionModel().getSelectedItem());
                listViewNotasCred.setItems(notaCredAsignadas);
                seteandoTotalAbonado();
//                seteandoDescTarjeta();
//                if (listViewNotasCred.getItems().isEmpty()) {
//                    validandoCambiandoFormaPago();
//                }
            } else {
                seteandoTotalAbonado();
                seteandoDescTarjeta();
            }
        } else if (!listViewNotasCred.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewNotasCred.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (cargarTarjetaConvenio) {
                    jsonCargandoTarjetasConvenio();
                    if (comboBoxTarjetasConvenio.getItems().isEmpty()) {
                        mensajeError("NO SE ENCONTRARON TARJETAS CONVENIO DISPONIBLES.");
                        cargarTarjetaConvenio = true;
                    }
                }
                comboBoxTarjetasConvenio.requestFocus();
                comboBoxTarjetasConvenio.show();
            } else {
                alertEscEnter = false;
            }
        }
    }

    private void agregandoTarjeta() {
        if (comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")
                || !hashJsonComboTarjeta.containsKey(comboBoxTarjetas.getSelectionModel().getSelectedItem())) {
            desbloqueandoPanelesTarj();
            if (panelBusquedaTipoTarj.isDisable()) {
                mensajeError("ESTA FORMA DE PAGO ESTA DESHABILITADA");
            } else {
                mensajeError("DEBE SELECCIONAR UNA TARJETA CRÉDITO o DÉBITO.");
            }
        } else if (textFieldCodAuth.getText().contentEquals("")) {
            mensajeError("DEBE CARGAR EL CÓDIGO DE AUTORIZACIÓN.");
        } else if (textFieldMontoTarjeta.getText().contentEquals("")) {
            mensajeError("DEBE INGRESAR EL MONTO DE LA TARJETA CRÉDITO o DÉBITO.");
        } else if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            mensajeError("TEMPORALMENTE DESHABILITADO PARA MÁS DE UNA TARJETA\nDEBIDO A INCOMPATIBILIDAD DE ESTRUCTURAS.");
        } else {
            String tarjeta = comboBoxTarjetas.getSelectionModel().getSelectedItem() + " - " + textFieldMontoTarjeta.getText() + " (Borrar)" + " - " + textFieldCodAuth.getText();
            String tarjetaUnica = comboBoxTarjetas.getSelectionModel().getSelectedItem() + " - " + textFieldCodAuth.getText();//válido solo para la transacción actual...
            if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                tarjetasAsignadas.add(tarjeta);
                hashListTarj = new HashMap<>();
                hashListTarj.put(tarjetaUnica, 0);
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                seteandoTotalAbonado();
                limpiandoTarjeta();
            } else if (hashListTarj.get(tarjetaUnica) == null) {
                tarjetasAsignadas.add(tarjeta);
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                hashListTarj.put(tarjetaUnica, listViewTarjetasAgregadas.getItems().indexOf(tarjeta));
                calculandoTarjeta(false);//solo descuento para una tarjeta
                seteandoTotalAbonado();
                limpiandoTarjeta();
//                validandoCambiandoFormaPago();
            } else {
                mensajeError("LA TARJETA YA SE AGREGÓ.");
            }
        }
    }

    private void quitandoTarjeta(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (!listViewTarjetasAgregadas.getItems().isEmpty() && !alert.isShowing()) {
                String[] parts = listViewTarjetasAgregadas.getSelectionModel().getSelectedItem().split(" - ");
                String itemRemove = parts[0] + " - " + parts[2];
                hashListTarj.remove(itemRemove);
                tarjetasAsignadas.remove(listViewTarjetasAgregadas.getSelectionModel().getSelectedItem());
                listViewTarjetasAgregadas.setItems(tarjetasAsignadas);
                validandoTarjetaListen();
                calculandoValePorc();
                calculandoValeParesa();
                seteandoTotalAbonado();
            }
        } else if (!listViewTarjetasAgregadas.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewTarjetasAgregadas.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            if (!alertEscEnter) {
                textFieldEntidad.requestFocus();
            } else {
                alertEscEnter = false;
            }
        }
    }

    private void agregandoTarjConv() {//queda abierta la posibilidad de varias tarjetas convenio...
        if (comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem() == null) {
            if (panelBusquedaTipoTarjConv.isDisable()) {
                mensajeError("ESTA FORMA DE PAGO ESTA DESHABILITADA");
            } else {
                mensajeError("DEBE SELECCIONAR UNA TARJETA CONVENIO.");
            }
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            mensajeError("ESTÁ PERMITIDO HASTA UNA TARJETA CONVENIO POR PAGO.");
        } else if (!listViewTarjetasAgregadas.getItems().isEmpty() && descTarjeta && descuentoTarjetaSum > 0) {
            mensajeAdv("NO DEBE DISPONER DESCUENTO ALGUNO\nEN TARJETA CRÉDITO / DÉBITO.");
        } else {
            tarjConvAsignadas.add(comboBoxTarjetasConvenio.getSelectionModel().getSelectedItem());
            listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
            validandoTarjetaConvenio();
        }
    }

    private void quitandoTarjConv(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().DELETE) {
            if (!listViewTarjetasConvAgregadas.getItems().isEmpty() && !alert.isShowing()) {
                tarjConvAsignadas.remove(listViewTarjetasConvAgregadas.getSelectionModel().getSelectedItem());
                listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
                if (listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    invalidandoTarjetaConvenio();
                }
            }
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty() && keyCode == event.getCode().TAB) {
            listViewTarjetasConvAgregadas.getSelectionModel().select(0);
        } else if (keyCode == event.getCode().ENTER) {
            if (!alertEscEnter) {
                if (panelVales.isDisable()) {
                    textFieldMontoRetencion.requestFocus();
                } else {
                    if (cargarVale) {
                        jsonCargandoVales();
                        if (comboBoxVale.getItems().isEmpty()) {
                            mensajeError("NO SE ENCONTRARON VALES DISPONIBLES.");
                            cargarVale = true;
                        }
                    }
                    comboBoxVale.requestFocus();
                    comboBoxVale.show();
                }
            } else {
                alertEscEnter = false;
            }
        }
    }

    private void visualizandoCamposVale() {//validaciones para el vale
        if (comboBoxVale.getSelectionModel().isSelected(0)) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
            invalidandoVale();
            if (!cargandoInicial) {
                validandoCambiandoFormaPago();
            }
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FUNCIONARIO")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ORDEN DE COMPRA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ACUERDO COMERCIAL")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(true);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(true);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setText("");
            textFieldValeCI.setDisable(false);
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(true);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(false);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("PARESA")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80003400-7");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("FRIGOMERC")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80019708-9");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("INSPECTORATE")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80037706-0");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("CELLULAR")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80008968-5");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ESSEN")) {
            textFieldNroVale.setText("");
            textFieldNroVale.setDisable(false);
            textFieldValeCI.setDisable(true);
            textFieldValeCI.setText("80080809-6");
            textFieldMontoVale.setText("");
            textFieldMontoVale.setDisable(false);
            textFieldMontoValePorc.setText("");
            textFieldMontoValePorc.setDisable(true);
        }
        if (!comboBoxVale.getSelectionModel().isSelected(0) && !comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
            validandoVale();
        } else if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
            panelBusquedaTipoTarjConv.setDisable(true);
            valeActivo = true;
        }
    }

    private void gestionandoPago(JSONObject cabFactura) {
        if (!textFieldEfectivo.getText().contentEquals("")) {
            if (creandoCabFacturaEfectivo(cabFactura)) {
            }
            //para impresión
            MensajeFinalVentaFXMLController.setEfectivo(Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText())));
            //para impresión
        }
        if (!textFieldRealGs.getText().contentEquals("") || !textFieldPesoGs.getText().contentEquals("")
                || !textFieldDolarGs.getText().contentEquals("")) {
            JSONArray jsonArrayMonedaExt = creandoJsonMonedaEx(cabFactura);
            if (creandoCabFacturaMonedaExt(jsonArrayMonedaExt)) {
            }
        }
        if (!listViewCheques.getItems().isEmpty()) {
            JSONArray jsonArrayCheque = creandoJsonCheque(cabFactura);
            if (creandoCabFacturaCheque(jsonArrayCheque)) {
            }
        }
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            JSONArray jsonArrayTarjeta = creandoJsonTarjeta(cabFactura);
            if (creandoCabFacturaTarjeta(jsonArrayTarjeta)) {
            }
        }
        if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            if (creandoCabFacturaTarjConv(cabFactura)) {
            }
        }
        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            if (creandoCabFacturaClienteFiel(cabFactura)) {
            }
        }
        if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            if (creandoCabFacturaFuncionario(cabFactura)) {
            }
        }
        if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")) {
            if (!comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ARGOS")) {
                creandoCabFacturaValeMonto(cabFactura);
            } else {
                creandoCabFacturaValePorc(cabFactura);
            }
        }
        if (!listViewNotasCred.getItems().isEmpty()) {
            JSONArray jsonArrayNota = creandoJsonNotaCred(cabFactura);
            if (creandoCabFacturaNotaCred(jsonArrayNota)) {
            }
            //para impresión
//            MensajeFinalVentaFXMLController.setNotaCred(Integer.valueOf(numValidator.numberValidator(textFieldMontoNotaCred.getText())));
            //para impresión
        }
        if (!textFieldMontoRetencion.getText().contentEquals("")) {
            if (creandoCabFacturaMontoRetencion(cabFactura)) {
            }
            //para impresión
            MensajeFinalVentaFXMLController.setRetencion(Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText())));
            //para impresión
        }
        gestionandoDescuento(cabFactura, true);
    }

    private void gestionandoDescuento(JSONObject json, boolean sinCabeceraPago) {
        if (sinCabeceraPago) {
            if (descValeMonto || descValePorc) {
                creandoCabFacturaValeDesc(json, hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
            }
            if (descPromoTemp || toggleButton.isSelected()) {
                for (Object jsonObjectPromArt : Descuento.getPromTempArtJSONArray()) {
                    //verificación de promoción art. con su respectivo descuento... (cabecera)
                    JSONObject jsonPromoTempArt = (JSONObject) jsonObjectPromArt;
                    if (hashJsonPromocionArtDesc.get(jsonPromoTempArt) != null) {
                        long descEstaPromoArt = hashJsonPromocionArtDesc.get(jsonPromoTempArt);
                        int descPromoArt = toIntExact((long) descEstaPromoArt);
                        JSONObject cabFacturaPromTempArtDesc = creandoJsonDescPromoTempArt(json, jsonPromoTempArt, descPromoArt);
                        creandoCabFacturaPromTempArtDesc(cabFacturaPromTempArtDesc);
                    }
                }
                for (Object jsonObjectProm : Descuento.getPromTempJSONArray()) {//verificación de promoción con su respectivo descuento...
                    JSONObject jsonPromoTemp = (JSONObject) jsonObjectProm;
                    if (hashJsonPromocionDesc.get(jsonPromoTemp) != null) {
                        long descEstaPromo = hashJsonPromocionDesc.get(jsonPromoTemp);
                        int descPromo = toIntExact((long) descEstaPromo);
                        JSONObject cabFacturaPromTempDesc = creandoJsonDescPromoTemp(json, jsonPromoTemp, descPromo);
                        creandoCabFacturaPromTempDesc(cabFacturaPromTempDesc);
                    }
                }
            }
            if (descDirectivo) {
                creandoCabFacturaDirectivoDesc(json);
            }
        } else {
            if (descTarjetaFiel && json.containsKey("tarjetaClienteFiel")) {
                creandoCabFacturaClienteFielDesc(json);
            }
            if (descFuncionario && json.containsKey("funcionario")) {
                creandoCabFacturaFuncionarioDesc(json);
            }
            if (descTarjeta && json.containsKey("tarjeta") && descuentoTarjetaSum != 0.0) {
                creandoCabFacturaTarjetaDesc(json);
            }
            if (descTarjetaConvenio && json.containsKey("tarjetaConvenio")) {
                creandoCabFacturaTarjetaConvDesc(json);
            }
        }
    }

    private void calculandoTotal() {
        this.total = FacturaDeVentaFXMLController.getPrecioTotal();
        String total = numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(this.total)));
        textFieldTotal.setText(total);
    }

    private void seteandoTotalAbonado() {
        donacion = 0;
        vuelto = 0;
        totalAbonado = 0;
        //cheques
        if (!listViewCheques.getItems().isEmpty()) {
            for (String cheque : listViewCheques.getItems()) {
                String[] parts = cheque.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[2]));
            }
        }
        //cheques
        //tarjeta crédito/débito
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            for (String tarjeta : listViewTarjetasAgregadas.getItems()) {
                String[] parts = tarjeta.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[1]));
            }
        }
        //tarjeta crédito/débito
        //nota crédito
        if (!listViewNotasCred.getItems().isEmpty()) {
            for (String nota : listViewNotasCred.getItems()) {
                String[] parts = nota.split(" - ");
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(parts[1]));
            }
        }
        //nota crédito
        //moneda local
        if (!textFieldEfectivo.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        }
        //moneda local
        //moneda extrajera
        if (FacturaDeVentaFXMLController.getCotizacionList() != null) {
            if (!textFieldDolarCant.getText().contentEquals("")) {
                int dolarCant = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
                int dolarConversion = dolarCant * FacturaDeVentaFXMLController.getDolar();
                textFieldDolarGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(dolarConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldDolarGs.getText()));
            } else {
                textFieldDolarGs.setText("");
            }
            if (!textFieldRealCant.getText().contentEquals("")) {
                int realCant = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
                int realConversion = realCant * FacturaDeVentaFXMLController.getReal();
                textFieldRealGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(realConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldRealGs.getText()));
            } else {
                textFieldRealGs.setText("");
            }
            if (!textFieldPesoCant.getText().contentEquals("")) {
                int pesoCant = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
                int pesoConversion = pesoCant * FacturaDeVentaFXMLController.getPeso();
                textFieldPesoGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(pesoConversion))));
                totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldPesoGs.getText()));
            } else {
                textFieldPesoGs.setText("");
            }
        }
        //moneda extrajera
        //monto retención
        if (!textFieldMontoRetencion.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        }
        //monto retención
        //nota de crédito, porque ya es una lista ahora
//        if (!textFieldMontoNotaCred.getText().contentEquals("")) {
//            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoNotaCred.getText()));
//        }
        //nota de crédito
        //vale
        if (!textFieldMontoVale.getText().isEmpty()) {
            totalAbonado = totalAbonado + Long.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
        }/* else if (!textFieldMontoValePorc.getText().contentEquals("")) {
            descuento = (Long.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
        }*/
        //vale
        //directivo
        if (descDirectivo) {
            if (textFieldMontoPorc.getText().contentEquals("")) {
                descuento = 0;
            } else {
                descuento = (Long.valueOf(numValidator.numberValidator(textFieldMontoPorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            }
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
        } else if (descValePorc) {
            totalAPagar = total - descuento;
        } else {
            totalAPagar = Long.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
        }
        //directivo
        if (totalAbonado >= totalAPagar) {
            labelVuelto.setText("Vuelto");
            textFieldVuelto.setStyle("-fx-background-color: #3CB371; -fx-opacity:  1;");
            vuelto = totalAbonado - totalAPagar;
            if (vuelto > 0) {
                textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(vuelto))));
            } else if (vuelto == 0) {
                textFieldVuelto.setText("Gs 0");
            }
        } else {
            labelVuelto.setText("Faltante");
            long dif = (totalAbonado - totalAPagar) * -1;
            textFieldVuelto.setStyle("-fx-background-color: #F08080; -fx-opacity:  1;");
            textFieldVuelto.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(dif))));
        }
        if (totalAbonado > 0) {
            textFieldTotalAbonado.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAbonado))));
        } else {
            textFieldTotalAbonado.setText("Gs 0");
        }
    }

    private void seteandoControlDesc() {
        descDirectivo = false;
        descFuncionario = false;
        descPromoTemp = false;
        descTarjeta = false;
        descTarjetaConvenio = false;
        descTarjetaFiel = false;
        descValePorc = false;
        descValeMonto = false;
    }

    private void seteandoTotales() {
        descuento = 0;
        totalAPagar = 0;
        textFieldDescuento.setText("Gs 0");
        lineStrokeTotal.setVisible(false);
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(FacturaDeVentaFXMLController.getPrecioTotal().toString())));
        seteandoControlDesc();
        montoConDes10 = 0;
        montoConDes5 = 0;
        exenta = 0;
        hashDetallePrecioDesc = new HashMap<>();
        hashDetallePrecioDescPromoArt = new HashMap<>();
        //sum
        descuentoPromoArtSum = 0;
        descuentoPromoNfSum = 0;
        descuentoFielSum = 0;
        descuentoFuncSum = 0;
        descuentoTarjetaSum = 0;
        descuentoTarjetaConvSum = 0;
        descuentoValePorcSum = 0;
        descuentoValeParesaSum = 0;
        //sum
    }

    private boolean seteandoDescTarjeta() {
        if (listViewCheques.getItems().isEmpty() && listViewTarjetasConvAgregadas.getItems().isEmpty()
                && comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("Seleccione un tipo de vale...")
                && textFieldMontoPorc.getText().isEmpty() && textFieldEfectivo.getText().isEmpty() && textFieldDolarGs.getText().isEmpty()
                && textFieldRealGs.getText().isEmpty() && textFieldPesoGs.getText().isEmpty() && textFieldMontoRetencion.getText().isEmpty()) {
            calculandoTarjeta(true);
        }
        return descTarjeta;
    }

    private void limpiandoTarjeta() {
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
    }

    private void limpiandoCheque() {
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFieldMonto.setText("");
    }

    private void limpiandoNotaCred() {
        textFieldNroNotaCred.setText("");
        textFieldNroNotaCred.setDisable(false);
        textFieldMontoNotaCred.setText("");
    }

    private void cotizacionMoneda() {
        if (FacturaDeVentaFXMLController.getCotizacionList() != null) {
            if (!textFieldDolarCant.getText().contentEquals("")) {
                int dolarCant = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
                int dolarConversion = dolarCant * FacturaDeVentaFXMLController.getDolar();
                textFieldDolarGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(dolarConversion))));
            } else {
                textFieldDolarGs.setText("");
            }
            if (!textFieldPesoCant.getText().contentEquals("")) {
                int pesoCant = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
                int pesoConversion = pesoCant * FacturaDeVentaFXMLController.getPeso();
                textFieldPesoGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(pesoConversion))));
            } else {
                textFieldPesoGs.setText("");
            }
            if (!textFieldRealCant.getText().contentEquals("")) {
                int realCant = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
                int realConversion = realCant * FacturaDeVentaFXMLController.getReal();
                textFieldRealGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(realConversion))));
            } else {
                textFieldRealGs.setText("");
            }
        } else {
            textFieldDolarGs.setText("");
            textFieldPesoGs.setText("");
            textFieldRealGs.setText("");
        }
    }

    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->
    //FUNCIONARIO***************************************************************
    private void validandoFuncionario() {
        panelBusquedaTipoTarj.setDisable(true);
        comboBoxTarjetas.getSelectionModel().select(0);
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
        panelBusquedaClienteFiel.setDisable(true);
        panelBusquedaCheque.setDisable(true);
        panelBusquedaTipoTarjConv.setDisable(true);
        panelVales.setDisable(true);
        panelMontoRetencion.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            /*if (listViewTarjetasAgregadas.getItems().isEmpty()) {*/
            entradaFunc1era = true;
            calculandoFuncionario();
            checkBoxPromoTempButton();
            /*} else {
                if (textFieldDolarGs.getText().isEmpty() && textFieldRealGs.getText().isEmpty() && textFieldPesoGs.getText().isEmpty()
                        && listViewCheques.getItems().isEmpty() && listViewNotasCred.getItems().isEmpty() && listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    calculandoTarjeta(true);
                    if (!descTarjeta) {
                        entradaFunc1era = false;
                        calculandoFuncionario();
                        checkBoxPromoTempButton();
                    }
                } else {
                    calculandoTarjeta(false);
                    calculandoFuncionario();
                    checkBoxPromoTempButton();
                }
            }*/
        }
    }

    private void invalidandoFuncionario() {
        panelBusquedaTipoTarj.setDisable(false);
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaCheque.setDisable(false);
        panelBusquedaTipoTarjConv.setDisable(false);
        panelVales.setDisable(false);
        panelMontoRetencion.setDisable(false);
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void validandoClientFiel() {
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            if (listViewTarjetasAgregadas.getItems().isEmpty() && !validandoTarjetaConvenioTarjCredDeb()) {
                entradaFiel1era = true;
                calculandoClienteFiel();
                checkBoxPromoTempButton();
            } else {
                if (textFieldDolarGs.getText().isEmpty() && textFieldRealGs.getText().isEmpty() && textFieldPesoGs.getText().isEmpty()
                        && listViewCheques.getItems().isEmpty() && listViewNotasCred.getItems().isEmpty() && listViewTarjetasConvAgregadas.getItems().isEmpty()) {
                    calculandoTarjeta(true);
                    if (!descTarjeta) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                        checkBoxPromoTempButton();
                    }
                } else {
                    calculandoTarjeta(false);
                    calculandoClienteFiel();
                    checkBoxPromoTempButton();
                }
            }
        }
    }

    private void invalidandoClientFiel() {
        panelNotaCredito.setDisable(false);
        /*panelBusquedaTipoTarj.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        panelBusquedaCheque.setDisable(false);
        panelBusquedaTipoTarjConv.setDisable(false);
        panelVales.setDisable(false);
        panePromoTemp.setDisable(false);
        panelNotaCredito.setDisable(false);
        panelMontoRetencion.setDisable(false);*/
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
    private void validandoTarjeta() {
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoTarjeta(true);
            seteandoTotalAbonado();
        }
    }

    private void bloqueandoPanelesTarj() {
        toggleEnTarj = true;
        toggleButton.setSelected(false);
        panelBusquedaTipoTarjConv.setDisable(true);
        panelTipoDePagoExtranjera.setDisable(true);
        panelTipoDePago.setDisable(true);
        panelBusquedaCheque.setDisable(true);
        panelNotaCredito.setDisable(true);
        panelVales.setDisable(true);
        textFieldEfectivo.setText("");
        textFieldDolarCant.setText("");
        textFieldRealCant.setText("");
        textFieldPesoCant.setText("");
        textFieldEntidad.setText("");
        textFIeldNroCheque.setText("");
        textFieldMonto.setText("");
        textFieldNroNotaCred.setText("");
        textFieldMontoNotaCred.setText("");
        comboBoxVale.getSelectionModel().select(0);
        textFieldNroVale.setText("");
        textFieldValeCI.setText("");
        textFieldMontoVale.setText("");
        textFieldMontoValePorc.setText("");
        if (!listViewCheques.getItems().isEmpty()) {
            listViewCheques.getSelectionModel().select(0);
            String[] parts = listViewCheques.getSelectionModel().getSelectedItem().split(" - Gs ");
            hashListCheque.remove(parts[0]);
            chequesAsignados.remove(listViewCheques.getSelectionModel().getSelectedItem());
            arrayCheque.remove(listViewCheques.getSelectionModel().getSelectedItem());
            listViewCheques.setItems(chequesAsignados);
        }
        if (!listViewNotasCred.getItems().isEmpty()) {
            listViewNotasCred.getSelectionModel().select(0);
            String[] parts = listViewNotasCred.getSelectionModel().getSelectedItem().split(" - Gs ");
            hashListNotaCred.remove(parts[0]);
            notaCredAsignadas.remove(listViewNotasCred.getSelectionModel().getSelectedItem());
            listViewNotasCred.setItems(notaCredAsignadas);
        }
        if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            listViewTarjetasConvAgregadas.getSelectionModel().select(0);
            tarjConvAsignadas.remove(listViewTarjetasConvAgregadas.getSelectionModel().getSelectedItem());
            listViewTarjetasConvAgregadas.setItems(tarjConvAsignadas);
        }
        toggleEnTarj = false;
    }

    private void desbloqueandoPanelesTarj() {
        toggleEnTarj = true;
        if (FuncionarioFXMLController.getJsonFuncionario() != null || ClienteFielFXMLController.getJsonClienteFiel() != null) {
            toggleButton.setSelected(true);
        }
        if (FuncionarioFXMLController.getJsonFuncionario() == null) {
            panelBusquedaTipoTarjConv.setDisable(false);
            panelVales.setDisable(false);
            panelBusquedaCheque.setDisable(false);
        }
        panelTipoDePagoExtranjera.setDisable(false);
        panelTipoDePago.setDisable(false);
        panelNotaCredito.setDisable(false);
        toggleEnTarj = false;
    }

    private void validandoTarjetaListen() {
        if (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("")) {
            if (Descuento.getHashDescTarjeta() != null) {
                if (Descuento.getHashDescTarjeta().get(comboBoxTarjetas.getSelectionModel().getSelectedItem()) != null) {
                    JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(comboBoxTarjetas.getSelectionModel().getSelectedItem());
                    if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                        Platform.runLater(() -> {
                            Point2D point2D = panelBusquedaTipoTarj.localToScreen((panelBusquedaTipoTarj.getLayoutBounds().getMaxX() / 2),
                                    (panelBusquedaTipoTarj.getLayoutBounds().getMinY() - (comboBoxTarjetas.getHeight() * 2.1)));
                            toolTipPanelTarj.setStyle("-fx-font: normal bold 16 System; "
                                    + "-fx-background-color: white; -fx-border-color: #d70f23; "
                                    + "-fx-text-fill: #d70f23;");
                            toolTipPanelTarj.setText("MÍNIMO REQUERIDO PARA\nEL DTO. TARJETA "
                                    + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonTarjCab.get("montoMin").toString())));
                            toolTipPanelTarj.show(panelBusquedaTipoTarj, point2D.getX(), point2D.getY());
                            toolTipPanelTarj = AnimationFX.fadeTooltip(toolTipPanelTarj);
                        });
                    } else {
                        toolTipPanelTarj.hide();
                    }
                    if (timeToasterTarj == 0) {
                        toaster.mensajeTarjetaDto(jsonTarjCab.get("descriTarjeta").toString(), 5);
                        timeToasterTarj = System.currentTimeMillis();
                    } else {
                        if ((System.currentTimeMillis() - timeToasterTarj) >= 5000) {
                            toaster.mensajeTarjetaDto(jsonTarjCab.get("descriTarjeta").toString(), 5);
                            timeToasterTarj = System.currentTimeMillis();
                        }
                    }
                } else {
                    toolTipPanelTarj.hide();
                }
            } else {
                toolTipPanelTarj.hide();
            }
        } else {
            if (listViewTarjetasAgregadas.getItems().isEmpty()) {
                desbloqueandoPanelesTarj();
            }
            toolTipPanelTarj.hide();
        }
        comboBoxTarjeta();
    }

    private void invalidandoTarjeta() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
    }
    //TARJETAS******************************************************************

    //TARJETAS CONVENIO*********************************************************
    private void validandoTarjetaConvenio() {
//        panelVales.setDisable(true);
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            panelBusquedaFuncionario.setDisable(true);
            toggleEnTarj = true;
            toggleButton.setSelected(false);
            tarjConvActiva = true;
            comboBoxTarjetas.getSelectionModel().select(0);
            textFieldCodAuth.setText("");
            textFieldMontoTarjeta.setText("");
            toggleEnTarj = false;
            panelVales.setDisable(true);
            comboBoxVale.getSelectionModel().select(0);
            textFieldNroVale.setText("");
            textFieldValeCI.setText("");
            textFieldMontoVale.setText("");
            textFieldMontoValePorc.setText("");
            calculandoTarjetaConvenio();
        }
    }

    private void invalidandoTarjetaConvenio() {
        panelBusquedaFuncionario.setDisable(false);
        panelVales.setDisable(false);
        tarjConvActiva = false;
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            panelBusquedaTipoTarj.setDisable(false);
            panelBusquedaFuncionario.setDisable(false);
            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                validandoClientFiel();
            } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                validandoFuncionario();
            } else {
                seteandoTotales();
                seteandoTotalAbonado();
//                editandoBarChart();
            }
        }
    }

    private boolean validandoTarjetaConvenioTarjCredDeb() {
        boolean descTarjDetectado = false;
        String descripcionTarj = "N/A";
        if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
            String[] parts = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
            descripcionTarj = parts[0];
        } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
            descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
        }
        if (Descuento.getHashDescTarjeta() != null) {
            if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
                JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
                if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) == -1
                        || (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) <= Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())))) {
                    JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
                    Calendar calendar = Calendar.getInstance();
                    JSONArray jsonArrayDiaEsp = new JSONArray();
                    int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                    for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
                        JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                        JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                        int dia = toIntExact((long) jsonDia.get("idDia"));
                        if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
                            String fechaIni = (String) jsonTarjCab.get("horaInicio");
                            String fechaFin = (String) jsonTarjCab.get("horaFin");
                            if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                Time fechaIniT = Utilidades.objectToTime(fechaIni);
                                Time fechaFinT = Utilidades.objectToTime(fechaFin);
                                Date date = new Date();
                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                Time t = Utilidades.objectToTime(hora);
                                if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                            descTarjDetectado = true;
                                            break;
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                    //se omite la bajada por el tema del convenio...
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                } else {
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        descTarjDetectado = true;
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        descTarjDetectado = true;
                                        break;
                                    }
                                }
                            } else {
                                JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                    if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                        descTarjDetectado = true;
                                        break;
                                    } else {
                                        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                            if ((Boolean) jsonTarjCab.get("extracto")) {
                                                //se omite la bajada por el tema del convenio...
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            } else {
                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                        + jsonArticulo.get("codArticulo"))) {
                                                    descTarjDetectado = true;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    descTarjDetectado = true;
                                    break;
                                }
                            }
                        } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
                            jsonArrayDiaEsp.add(jsonTarjDet);
                        }
                    }
                    if (!jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
                        HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
                        if (hashDiaEspecial != null) {
                            for (Object tarjDetObj : jsonArrayDiaEsp) {
                                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                                if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
                                    if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
                                        String fechaIni = (String) jsonTarjCab.get("horaInicio");
                                        String fechaFin = (String) jsonTarjCab.get("horaFin");
                                        if (fechaIni != null && fechaFin != null) {//si cumple, se asignó un rango de horario...
                                            Time fechaIniT = Time.valueOf(fechaIni);
                                            Time fechaFinT = Time.valueOf(fechaFin);
                                            Date date = new Date();
                                            String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                            Time t = Time.valueOf(hora);
                                            if (fechaIniT.getTime() <= t.getTime() && fechaFinT.getTime() >= t.getTime()) {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        } else {
                                            JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                            if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                    descTarjDetectado = true;
                                                    break;
                                                } else {
                                                    for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                        JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                        if ((Boolean) jsonTarjCab.get("extracto")) {
                                                            //se omite la bajada por el tema del convenio...
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        } else {
                                                            if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                    + jsonArticulo.get("codArticulo"))) {
                                                                descTarjDetectado = true;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                descTarjDetectado = true;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } else if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                    //tool tip
                }
            }
        }
        return descTarjDetectado;
    }
    //TARJETAS CONVENIO*********************************************************

    //PROMOCIÓN TEMPORADA*******************************************************
    private void validandoPromocion() {
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoPromocionTemporada(false);
        }
    }

    private void invalidandoPromocion() {
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            seteandoTotales();
        }
    }
    //PROMOCIÓN TEMPORADA*******************************************************

    //VALE*******************************************************
    private void validandoVale() {
        panelBusquedaTipoTarjConv.setDisable(true);
        valeActivo = true;
        if (textFieldMontoPorc.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            calculandoVale(comboBoxVale.getSelectionModel().getSelectedItem());
        }
    }

    private void invalidandoVale() {
        panelBusquedaTipoTarjConv.setDisable(false);
        valeActivo = false;
        if (textFieldMontoPorc.getText().contentEquals("") && textFieldMontoNotaCred.getText().contentEquals("")
                && textFieldMontoRetencion.getText().contentEquals("")) {
            seteandoTotales();
        }
    }

    private void calculandoValeParesa() {
        if (comboBoxVale.getSelectionModel().getSelectedIndex() == 10) {
            seteandoTotales();
            seteandoControlDesc();
        }
        if (comboBoxVale.getSelectionModel().getSelectedIndex() == 10 && !textFieldValeCI.getText().isEmpty()
                && !textFieldMontoVale.getText().isEmpty() && !textFieldNroVale.getText().isEmpty()) {
            double diferencia = total - Long.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
            if (diferencia > 0) {
                descuentoValeParesaSum = diferencia * 0.1;
                descuento += descuentoValeParesaSum;
                totalAPagar = total - descuento;
                textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
                textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
                if (descuento > 0) {
                    descValeMonto = true;
                    lineStrokeTotal.setVisible(true);
                } else {
                    lineStrokeTotal.setVisible(false);
                }
            }
        }
    }

    private void calculandoValePorc() {
        if (!textFieldMontoValePorc.getText().isEmpty() && descuentoValePorcSum <= 0) {
            descuentoValePorcSum = (Long.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText())) * Long.valueOf(numValidator.numberValidator(textFieldTotal.getText()))) / 100;
            descuento += descuentoValePorcSum;
            totalAPagar = total - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.valueOf(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
        }
    }
    //VALE*******************************************************

    //DIRECTIVO*****************************************************************
    private void validandoDirectivo() {
        calculandoDirectivo();
    }

    private void invalidandoDirectivo() {
        seteandoTotales();
    }
    //DIRECTIVO*****************************************************************

    //NOTA DE CRÉDITO***********************************************************
    private void validandoNotaCredito() {
        textFieldNroNotaCred.setDisable(true);
        calculandoNotaCredito();
    }

    private void invalidandoNotaCredito() {
        textFieldNroNotaCred.setDisable(false);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //NOTA DE CRÉDITO***********************************************************

    //MONTO DE RETENCIÓN***********************************************************
    private void validandoMontoRetencion() {
        panelBusquedaClienteFiel.setDisable(false);
        panelBusquedaFuncionario.setDisable(false);
        calculandoMontoRetencion();
    }

    private void invalidandoMontoRetencion() {
        panelBusquedaClienteFiel.setDisable(true);
        panelBusquedaFuncionario.setDisable(true);
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
    }
    //MONTO DE RETENCIÓN***********************************************************

    //cambio de formas de pago....
    private void validandoCambiandoFormaPago() {
        if (descTarjetaConvenio || !listViewTarjetasConvAgregadas.getItems().isEmpty() || descuentoTarjetaConvSum > 0) {
            calculandoTarjetaConvenioValidacionTar();
        } else if (!descTarjeta && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || !listViewTarjetasAgregadas.getItems().isEmpty())
                && listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            seteandoDescTarjeta();
        } else if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            validandoClientFiel();
        } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            validandoFuncionario();
        } else if (comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
            validandoVale();
        }/* else if (toggleButton.isSelected()) {
            checkBoxPromoTemp();
        } else if (!listViewTarjetasConvAgregadas.getItems().isEmpty()) {
            validandoTarjetaConvenio();
        } else if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
            validandoClientFiel();
        } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
            validandoFuncionario();
        } else {
            seteandoDescTarjeta();
        }*/
    }
    //cambio de formas de pago....
    //VALIDACIONES VALIDACIONES VALIDACIONES *********** -> -> -> -> -> -> -> ->

    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->
    //FUNCIONARIO***************************************************************
    private void calculandoFuncionario() {
        if (entradaFunc1era) {
            seteandoTotales();
            montoMap5 = new HashMap<>();
            montoMap10 = new HashMap<>();
            montoMapExe = new HashMap<>();
        }
        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                calculandoFuncionarioNf(7, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                calculandoFuncionarioNf(6, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                calculandoFuncionarioNf(5, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                calculandoFuncionarioNf(4, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                calculandoFuncionarioNf(3, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                calculandoFuncionarioNf(2, jsonNf, detalleArtJson);
            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                calculandoFuncionarioNf(1, jsonNf, detalleArtJson);
            } else {
                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
            }
        }

        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVentaFXMLController
        if (descFuncionario) {
            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        if (entradaFunc1era) {
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFuncSum);
            totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
            seteandoTotalAbonado();
        }
    }

    @SuppressWarnings("element-type-mismatch")
    private double calculandoFuncionarioNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        double descFunc = 0;
        switch (nf) {
            case 1:
                if (Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")) != null) {
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                }
                break;
            case 2:
                if (Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")) != null) {
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoFuncionarioNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoFuncionarioNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoFuncionarioNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoFuncionarioNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoFuncionarioNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    descFunc += calculandoFuncionarioDet(Descuento.getHashDescFuncionarioNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoFuncionarioNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
        return descFunc;
    }

    private double calculandoFuncionarioDet(JSONObject jsonFuncionarioDesc, JSONObject detalleArtJson) {
        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonFuncionarioDesc.get("porcDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        double descFunc = 0;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            if (montoMap10.containsKey(detalleArtJson)) {
                if (montoMap10.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - ((montoMap10.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMap10.put(detalleArtJson, montoMap10.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            if (montoMap5.containsKey(detalleArtJson)) {
                if (montoMap5.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - ((montoMap5.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMap5.put(detalleArtJson, montoMap5.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else {
            if (montoMapExe.containsKey(detalleArtJson)) {
                if (montoMapExe.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                    long porcAnterior = 100 - ((montoMapExe.get(detalleArtJson) * 100) / (precio * cantidad));
                    descuentoFuncSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                    descFuncionario = true;
                    exenta = Math.round(exenta + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                    montoMapExe.put(detalleArtJson, montoMapExe.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                }
            } else {
                descuentoFuncSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                descFuncionario = true;
                exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        }
//        descuentoFuncSum = Math.round(descFunc);
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
        return descFunc;
    }
    //FUNCIONARIO***************************************************************

    //TARJETA CLIENTE FIEL******************************************************
    private void calculandoClienteFiel() {
        if (entradaFiel1era) {
            seteandoTotales();
            montoMap5 = new HashMap<>();
            montoMap10 = new HashMap<>();
            montoMapExe = new HashMap<>();
        }
        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {//desde factura venta...
            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
            JSONArray jsonArrayArticuloNf;
            JSONObject jsonNf;
            JSONObject jsonArticuloNf;
            //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
            if (!(boolean) jsonArticulo.get("bajada")) {
                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                    calculandoClienteFielNf(7, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                    calculandoClienteFielNf(6, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                    calculandoClienteFielNf(5, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                    calculandoClienteFielNf(4, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                    calculandoClienteFielNf(3, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                    calculandoClienteFielNf(2, jsonNf, detalleArtJson);
                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                    calculandoClienteFielNf(1, jsonNf, detalleArtJson);
                } else {
                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                        calculandoClienteFielNf(7, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                        calculandoClienteFielNf(6, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                        calculandoClienteFielNf(5, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                        calculandoClienteFielNf(4, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                        calculandoClienteFielNf(3, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                        calculandoClienteFielNf(2, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                        calculandoClienteFielNf(1, jsonNf, detalleArtJson);
                    } else {
                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                    }
                }
            }
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVentaFXMLController
        if (descTarjetaFiel) {
            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
        }
        if (entradaFiel1era) {
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFielSum) + Math.round(descuentoTarjetaSum);
            totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
            seteandoTotalAbonado();
        }
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoClienteFielNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                }
                break;
            case 2:
                if (Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")) != null) {
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoClienteFielNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoClienteFielNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoClienteFielNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoClienteFielNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoClienteFielNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    calculandoClienteFielDet(Descuento.getHashDescTarjetaFielNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoClienteFielNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoClienteFielDet(JSONObject jsonFielCab, JSONObject detalleArtJson) {
        JSONArray jsonArrayFielDet = (JSONArray) jsonFielCab.get("descuentoFielDets");
        Calendar calendar = Calendar.getInstance();
        int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
        for (Object fielDetObj : jsonArrayFielDet) {//verificación, día de descuento...
            JSONObject jsonFielDet = (JSONObject) fielDetObj;
            JSONObject jsonDia = (JSONObject) jsonFielDet.get("dia");
            int dia = toIntExact((long) jsonDia.get("idDia"));
            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
            double porc = Double.valueOf(jsonFielCab.get("porcentajeDesc").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            if (dia == diaSemana) {
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    //previamente cargado con algún descuento, se da prioridad al anterior...
                    if (montoMap10.containsKey(detalleArtJson)) {
                        if (montoMap10.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                            long porcAnterior = 100 - ((montoMap10.get(detalleArtJson) * 100) / (precio * cantidad));
                            descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                            descTarjetaFiel = true;
                            montoConDes10 = montoConDes10 + (((precio * cantidad) * (porc - porcAnterior)) / 100);
                            montoMap10.put(detalleArtJson, montoMap10.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                            hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                        }
                    } else {
                        descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                        descTarjetaFiel = true;
                        montoConDes10 = montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100);
                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.containsKey(detalleArtJson)) {
                        if (montoMap5.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                            long porcAnterior = 100 - ((montoMap5.get(detalleArtJson) * 100) / (precio * cantidad));
                            descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                            descTarjetaFiel = true;
                            montoConDes5 = montoConDes5 + (((precio * cantidad) * (porc - porcAnterior)) / 100);
                            montoMap5.put(detalleArtJson, montoMap5.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                            hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                        }
                    } else {
                        descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                        descTarjetaFiel = true;
                        montoConDes5 = montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100);
                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                    }
                } else {
                    if (montoMapExe.containsKey(detalleArtJson)) {
                        if (montoMapExe.get(detalleArtJson) > Math.round(Double.parseDouble((((precio * cantidad) * (100 - porc)) / 100) + ""))) {
                            long porcAnterior = 100 - ((montoMapExe.get(detalleArtJson) * 100) / (precio * cantidad));
                            descuentoFielSum += Double.parseDouble((((precio * cantidad) * (porc - porcAnterior)) / 100) + "");
                            descTarjetaFiel = true;
                            exenta = Math.round(exenta + (((precio * cantidad) * (porc - porcAnterior)) / 100));
                            montoMapExe.put(detalleArtJson, montoMapExe.get(detalleArtJson) + Math.round(((precio * cantidad) * (porc - porcAnterior)) / 100));
                            hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (porc - porcAnterior)) / 100));
                        }
                    } else {
                        descuentoFielSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                        descTarjetaFiel = true;
                        exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                        hashDetallePrecioDesc.put(detalleArtJson, Math.round((precio * (100 - porc)) / 100));
                        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
                    }
                }
            }
        }
    }
    //TARJETA CLIENTE FIEL******************************************************

    //TARJETAS******************************************************************
    private void calculandoTarjeta(boolean soloTarjeta) {
//        if (ClienteFielFXMLController.getJsonClienteFiel() != null || FuncionarioFXMLController.getJsonFuncionario() != null) {
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        seteandoTotales();
        //validación de descuento, solo para una tarjeta, además de no permitir efectivo, cheque, vale, nota de crédito, tarjeta convenio, moneda extranjera...
        if (listViewTarjetasAgregadas.getItems().size() < 2 && soloTarjeta) {
            //verificación de descuento según tarjeta, se toma el nombre asignado en la línea de tiempo, 
            //si se modifica el nombre de la tarjeta por "a" o "b" motivo, se dejará de aplicar descuento,
            //en caso de querer aplicar descuento con el nuevo nombre tarjeta, deberá dar de baja la tarjeta con el nombre viejo en configuración descuentos...
            String descripcionTarj = "N/A";
            if (!listViewTarjetasAgregadas.getItems().isEmpty()) {
                String[] parts = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
                descripcionTarj = parts[0];
            } else if (!comboBoxTarjetas.getSelectionModel().isEmpty()) {
                descripcionTarj = comboBoxTarjetas.getSelectionModel().getSelectedItem();
            }
            if (Descuento.getHashDescTarjeta() != null) {
                if (Descuento.getHashDescTarjeta().get(descripcionTarj) != null) {
                    JSONObject jsonTarjCab = Descuento.getHashDescTarjeta().get(descripcionTarj);
                    if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) == -1
                            || (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) <= Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText())))) {
                        JSONArray jsonArrayTarjDet = (JSONArray) jsonTarjCab.get("descuentoTarjetaDets");
                        Calendar calendar = Calendar.getInstance();
                        JSONArray jsonArrayDiaEsp = new JSONArray();
                        int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                        for (Object tarjDetObj : jsonArrayTarjDet) {//verificación, día de descuento...
                            JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                            JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                            int dia = toIntExact((long) jsonDia.get("idDia"));
                            if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") == null) {//NO primer día y/o último...
                                String horaIni = (String) jsonTarjCab.get("horaInicio");
                                String horaFin = (String) jsonTarjCab.get("horaFin");
                                if (horaIni != null && horaFin != null) {//si cumple, se asignó un rango de horario...
                                    Time horaIniT = Utilidades.objectToTime(horaIni);
                                    Time horaFinT = Utilidades.objectToTime(horaFin);
                                    Date date = new Date();
                                    String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                    Time t = Utilidades.objectToTime(hora);
                                    if (horaIniT.getTime() <= t.getTime() && horaFinT.getTime() >= t.getTime()) {
                                        JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                        if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                            if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                    JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                    JSONArray jsonArrayArticuloNf;
                                                    JSONObject jsonNf;
                                                    JSONObject jsonArticuloNf;
                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                    } else {
                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                    }
                                                }
                                            } else {
                                                for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                    JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                    if ((Boolean) jsonTarjCab.get("extracto")) {
                                                        //se omite la bajada por el tema del convenio...
                                                        if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                + jsonArticulo.get("codArticulo"))) {
                                                            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                            double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                            descTarjeta = true;
                                                            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else {
                                                                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            }
                                                        } else {
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    } else {
                                                        if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                + jsonArticulo.get("codArticulo"))) {
                                                            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                            double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                            descTarjeta = true;
                                                            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            } else {
                                                                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                            }
                                                        } else {
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                JSONArray jsonArrayArticuloNf;
                                                JSONObject jsonNf;
                                                JSONObject jsonArticuloNf;
                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else {
                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                JSONArray jsonArrayArticuloNf;
                                                JSONObject jsonNf;
                                                JSONObject jsonArticuloNf;
                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                } else {
                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                }
                                            }
                                        } else {
                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                    //se omite la bajada por el tema del convenio...
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                        double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                        descTarjeta = true;
                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else {
                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        }
                                                    } else {
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                } else {
                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                            + jsonArticulo.get("codArticulo"))) {
                                                        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                        double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                        descTarjeta = true;
                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        } else {
                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                        }
                                                    } else {
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                            JSONArray jsonArrayArticuloNf;
                                            JSONObject jsonNf;
                                            JSONObject jsonArticuloNf;
                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                            } else {
                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                            }
                                        }
                                    }
                                }
                            } else if (dia == diaSemana && (Boolean) jsonTarjDet.get("diaEspecial") != null) {//capturar día especial...
                                jsonArrayDiaEsp.add(jsonTarjDet);
                            }
                        }
                        if (!descTarjeta && !jsonArrayDiaEsp.isEmpty()) {//primer y último día, por el momento solo BNF...
                            HashMap<Long, Boolean> hashDiaEspecial = Fecha.hashDia();
                            if (hashDiaEspecial != null) {
                                for (Object tarjDetObj : jsonArrayDiaEsp) {
                                    JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                                    JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                                    if (hashDiaEspecial.get((long) jsonDia.get("idDia")) != null) {
                                        if ((Boolean) jsonTarjDet.get("diaEspecial").equals(hashDiaEspecial.get((long) jsonDia.get("idDia")))) {
                                            String horaIni = (String) jsonTarjCab.get("horaInicio");
                                            String horaFin = (String) jsonTarjCab.get("horaFin");
                                            if (horaIni != null && horaFin != null) {//si cumple, se asignó un rango de horario...
                                                Time horaIniT = Time.valueOf(horaIni);
                                                Time horaFinT = Time.valueOf(horaFin);
                                                Date date = new Date();
                                                String hora = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
                                                Time t = Time.valueOf(hora);
                                                if (horaIniT.getTime() <= t.getTime() && horaFinT.getTime() >= t.getTime()) {
                                                    JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                                    if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                        if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                                JSONArray jsonArrayArticuloNf;
                                                                JSONObject jsonNf;
                                                                JSONObject jsonArticuloNf;
                                                                if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                    calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                    calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                    calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                    calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                    calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                    calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                    jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                    jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                    jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                    calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                } else {
                                                                    Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                }
                                                            }
                                                        } else {
                                                            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                                if ((Boolean) jsonTarjCab.get("extracto")) {
                                                                    //se omite la bajada por el tema del convenio...
                                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                            + jsonArticulo.get("codArticulo"))) {
                                                                        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                        double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                        descTarjeta = true;
                                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else {
                                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        }
                                                                    } else {
                                                                        JSONArray jsonArrayArticuloNf;
                                                                        JSONObject jsonNf;
                                                                        JSONObject jsonArticuloNf;
                                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else {
                                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                        }
                                                                    }
                                                                } else {
                                                                    if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                            + jsonArticulo.get("codArticulo"))) {
                                                                        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                                        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                        double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                                        descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                        descTarjeta = true;
                                                                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                            montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                            montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        } else {
                                                                            exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                        }
                                                                    } else {
                                                                        JSONArray jsonArrayArticuloNf;
                                                                        JSONObject jsonNf;
                                                                        JSONObject jsonArticuloNf;
                                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                        } else {
                                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                JSONArray jsonArrayDescuentoTarjetaCabArticulos = (JSONArray) jsonTarjCab.get("descuentoTarjetaCabArticulos");
                                                if (jsonArrayDescuentoTarjetaCabArticulos != null) {
                                                    if (jsonArrayDescuentoTarjetaCabArticulos.isEmpty()) {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            JSONArray jsonArrayArticuloNf;
                                                            JSONObject jsonNf;
                                                            JSONObject jsonArticuloNf;
                                                            if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                            } else {
                                                                Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                            }
                                                        }
                                                    } else {
                                                        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                            JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                            if ((Boolean) jsonTarjCab.get("extracto")) {
                                                                //se omite la bajada por el tema del convenio...
                                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                        + jsonArticulo.get("codArticulo"))) {
                                                                    int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                    double porc = (double) jsonTarjCab.get("porcentajeParana");
                                                                    descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                    descTarjeta = true;
                                                                    if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                        montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                        montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else {
                                                                        exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    }
                                                                } else {
                                                                    JSONArray jsonArrayArticuloNf;
                                                                    JSONObject jsonNf;
                                                                    JSONObject jsonArticuloNf;
                                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else {
                                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                    }
                                                                }
                                                            } else {
                                                                if (Descuento.getHashDescTarjetaArt().containsKey(jsonTarjCab.get("descriTarjeta") + " (y) "
                                                                        + jsonArticulo.get("codArticulo"))) {
                                                                    int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                                                                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                                                                    double porc = (double) jsonTarjCab.get("porcentajeDesc");
                                                                    descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
                                                                    descTarjeta = true;
                                                                    if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                                                                        montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                                                                        montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    } else {
                                                                        exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                                                                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                                                                    }
                                                                } else {
                                                                    JSONArray jsonArrayArticuloNf;
                                                                    JSONObject jsonNf;
                                                                    JSONObject jsonArticuloNf;
                                                                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                                        calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                                        calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                                        calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                                        calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                                        calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                                        calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                                        calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                                    } else {
                                                                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                                                        JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                                                        JSONArray jsonArrayArticuloNf;
                                                        JSONObject jsonNf;
                                                        JSONObject jsonArticuloNf;
                                                        if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                                                            calculandoDescTarjCabNf(7, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                                                            calculandoDescTarjCabNf(6, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                                                            calculandoDescTarjCabNf(5, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                                                            calculandoDescTarjCabNf(4, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                                                            calculandoDescTarjCabNf(3, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                                                            calculandoDescTarjCabNf(2, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                                                            jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                                                            jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                                                            jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                                                            calculandoDescTarjCabNf(1, jsonNf, detalleArtJson, jsonTarjCab);
                                                        } else {
                                                            Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF. en TARJETA");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else if (Integer.valueOf(jsonTarjCab.get("montoMin").toString()) != -1) {
                        //tool tip
                    }
                }
            }
        }
        if (descTarjeta) {//se da prioridad a descuento tarjeta, no importa el monto (mayor o menor a otra posible combinación)
            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
            descuento = Math.round(descuentoTarjetaSum);
            totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
            seteandoTotalAbonado();
            bloqueandoPanelesTarj();
//            editandoBarChart();
        } else {
            desbloqueandoPanelesTarj();
            //se valida para la carga inicial del controlador
            if (ClienteFielFXMLController.getJsonClienteFiel() != null || FuncionarioFXMLController.getJsonFuncionario() != null) {
                calculandoPromocionTemporada(true);//verifica también si hay dto. func. y fiel
            } else {
//                editandoBarChart();
            }
        }
//        }
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoDescTarjCabNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson, JSONObject jsonTarjCab) {
        switch (nf) {
            case 1:
                if (Descuento.getHashDescTarjetaCabNf1().get(jsonNf.get("idNf1Tipo")) != null) {
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf1()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf1Tipo") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    }
                }
                break;
            case 2:
                if (Descuento.getHashDescTarjetaCabNf2().get(jsonNf.get("idNf2Sfamilia")) != null) {
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf2()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf2Sfamilia") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                        calculandoDescTarjCabNf(1, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoDescTarjCabNf(1, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 3:
                if (Descuento.getHashDescTarjetaCabNf3().get(jsonNf.get("idNf3Sseccion")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf3()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf3Sseccion") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                        calculandoDescTarjCabNf(2, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoDescTarjCabNf(2, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 4:
                if (Descuento.getHashDescTarjetaCabNf4().get(jsonNf.get("idNf4Seccion1")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf4()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf4Seccion1") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                        calculandoDescTarjCabNf(3, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoDescTarjCabNf(3, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 5:
                if (Descuento.getHashDescTarjetaCabNf5().get(jsonNf.get("idNf5Seccion2")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf5()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf5Seccion2") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                        calculandoDescTarjCabNf(4, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoDescTarjCabNf(4, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 6:
                if (Descuento.getHashDescTarjetaCabNf6().get(jsonNf.get("idNf6Secnom6")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf6()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf6Secnom6") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                        calculandoDescTarjCabNf(5, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoDescTarjCabNf(5, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            case 7:
                if (Descuento.getHashDescTarjetaCabNf7().get(jsonNf.get("idNf7Secnom7")) != null) {//verificación de descuento según sección...
                    if (jsonTarjCab == Descuento.getHashDescTarjetaCabVerifNf7()
                            .get("[" + jsonTarjCab.get("idDescuentoTarjetaCab") + "] [" + jsonNf.get("idNf7Secnom7") + "]")) {
                        calculandoDescTarjNf(jsonTarjCab, detalleArtJson);
                    } else {
                        JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                        calculandoDescTarjCabNf(6, jsonRaiz, detalleArtJson, jsonTarjCab);
                    }
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoDescTarjCabNf(6, jsonRaiz, detalleArtJson, jsonTarjCab);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoDescTarjNf(JSONObject jsonTarjCab, JSONObject detalleArtJson) {
        if ((Boolean) jsonTarjCab.get("extracto")) {
            //se omite la bajada por el tema del convenio...
            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            double porc = (double) jsonTarjCab.get("porcentajeParana");
            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
            descTarjeta = true;
            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else {
                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        } else {
            int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
            long precio = Long.valueOf(detalleArtJson.get("precio").toString());
            double porc = (double) jsonTarjCab.get("porcentajeDesc");
            descuentoTarjetaSum += (double) ((precio * cantidad) * porc) / 100;
            descTarjeta = true;
            if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                montoConDes10 = (double) (montoConDes10 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                montoConDes5 = (double) (montoConDes5 + ((double) ((precio * cantidad) * porc) / 100));
                montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            } else {
                exenta = (long) (exenta + ((double) ((precio * cantidad) * porc) / 100));
                montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
            }
        }
    }
    //TARJETAS******************************************************************

    //TARJETAS CONVENIO*********************************************************
    private void calculandoTarjetaConvenio() {
        if (listViewTarjetasConvAgregadas.getItems().size() == 1) {//validación de descuento, solo para una tarjeta convenio...
            seteandoTotales();
            String parts = listViewTarjetasConvAgregadas.getItems().get(0);
            if (Descuento.getHashDescTarjetaConv().get(parts) != null) {
                JSONObject jsonTarjConvCab = Descuento.getHashDescTarjetaConv().get(parts);
                JSONArray jsonArrayTarjConvDet = (JSONArray) jsonTarjConvCab.get("descuentoTarjetaConvenioDetDTO");
                Calendar calendar = Calendar.getInstance();
                int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
                for (Object tarjDetObj : jsonArrayTarjConvDet) {//verificación, día de descuento...
                    JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                    JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                    int dia = toIntExact((long) jsonDia.get("idDia"));
                    if (dia == diaSemana) {
                        double porc = (double) jsonTarjConvCab.get("porcentajeDesc");//descuento por el total...
                        long precio = FacturaDeVentaFXMLController.getPrecioTotal();//desde factura venta...
                        descuentoTarjetaConvSum += Double.parseDouble((precio * porc / 100) + "");
                        descTarjetaConvenio = true;
                    }
                }
            }
            descuento = Math.round(descuentoTarjetaConvSum);
            totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
            seteandoTotalAbonado();
//            editandoBarChart();
        } else {
            mensajeAdv("CÓMO MÁXIMO UNA TARJETA CONVENIO.");
        }
    }

    private void calculandoTarjetaConvenioValidacionTar() {
        comboBoxTarjetas.getSelectionModel().select(0);
        textFieldCodAuth.setText("");
        textFieldMontoTarjeta.setText("");
        seteandoTotales();
        String parts = listViewTarjetasConvAgregadas.getItems().get(0);
        if (Descuento.getHashDescTarjetaConv().get(parts) != null) {
            JSONObject jsonTarjConvCab = Descuento.getHashDescTarjetaConv().get(parts);
            JSONArray jsonArrayTarjConvDet = (JSONArray) jsonTarjConvCab.get("descuentoTarjetaConvenioDetDTO");
            Calendar calendar = Calendar.getInstance();
            int diaSemana = calendar.get(Calendar.DAY_OF_WEEK);
            for (Object tarjDetObj : jsonArrayTarjConvDet) {//verificación, día de descuento...
                JSONObject jsonTarjDet = (JSONObject) tarjDetObj;
                JSONObject jsonDia = (JSONObject) jsonTarjDet.get("dia");
                int dia = toIntExact((long) jsonDia.get("idDia"));
                if (dia == diaSemana) {
                    double porc = (double) jsonTarjConvCab.get("porcentajeDesc");//descuento por el total...
                    long precio = FacturaDeVentaFXMLController.getPrecioTotal();//desde factura venta...
                    descuentoTarjetaConvSum += Double.parseDouble((precio * porc / 100) + "");
                    descTarjetaConvenio = true;
                }
            }
        }
        descuento = Math.round(descuentoTarjetaConvSum);
        totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
        textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
        textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
        if (descuento > 0) {
            lineStrokeTotal.setVisible(true);
        } else {
            lineStrokeTotal.setVisible(false);
        }
        seteandoTotalAbonado();
    }
    //TARJETAS CONVENIO*********************************************************

    //PROMOCIÓN TEMPORADA*******************************************************
    private void calculandoPromocionTemporada(boolean tarjeta) {
        seteandoTotales();
        hashJsonPromocionDesc = new HashMap<>();
        hashJsonPromocionArtDesc = new HashMap<>();
        montoMap5 = new HashMap<>();
        montoMap10 = new HashMap<>();
        montoMapExe = new HashMap<>();
        //se toma primero artículo, luego sección, de existir en ambos, se da prioridad a artículo...
        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
            //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
            JSONObject jsonArticuloB = (JSONObject) detalleArtJson.get("articulo");
            if (!(boolean) jsonArticuloB.get("bajada")) {
                if (Descuento.getHashPromTempArtObsequio().containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                    double descuentoPromoArt = 0;
                    int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                    int minReq = Integer.valueOf(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())).get("minReq").toString());
                    int cantObsequio = Integer.valueOf(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())).get("cantObsequio").toString());
                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                    if (cantidad >= minReq) {
                        double mod = (double) cantidad / (double) minReq;
                        int entero = (int) mod;
                        cantObsequio = cantObsequio * entero;
                        descuentoPromoArt = (double) (descuentoPromoArt + (precio * cantObsequio));//descuento total en esta promoción...
                        descuentoPromoArtSum += descuentoPromoArt;
                        descPromoTemp = true;
                        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                            montoConDes10 = (double) (montoConDes10 + (precio * (cantidad - cantObsequio)));
                            montoMap10.put(detalleArtJson, Math.round((double) (precio * (cantidad - cantObsequio))));
                        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                            montoConDes5 = (double) (montoConDes5 + (precio * (cantidad - cantObsequio)));
                            montoMap5.put(detalleArtJson, Math.round((double) (precio * (cantidad - cantObsequio))));
                        } else {
                            exenta = (long) (exenta + (precio * (cantidad - cantObsequio)));
                            montoMapExe.put(detalleArtJson, Math.round((double) (precio * (cantidad - cantObsequio))));
                        }
                        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por artículo
                        hashDetallePrecioDescPromoArt.put(detalleArtJson, (long) (precio * (cantidad - cantObsequio)));
                        if (hashJsonPromocionArtDesc.containsKey(
                                Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))))) {
                            hashJsonPromocionArtDesc.put(
                                    Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                    hashJsonPromocionArtDesc.get(Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())))) + Math.round(descuentoPromoArt));
                        } else {
                            hashJsonPromocionArtDesc.put(
                                    Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArtObsequio().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                    Math.round(descuentoPromoArt));
                        }
                    }
                } else if (Descuento.getHashPromTempArt().containsKey(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))) {
                    //verificación de descuento según artículo...
                    double descuentoPromoArt = 0;
                    int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                    double porc = Double.valueOf(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())).get("porcentajeDesc").toString());
                    long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                    descuentoPromoArt = (double) (descuentoPromoArt + ((precio * cantidad) * porc) / 100);//descuento total en esta promoción...
                    descuentoPromoArtSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");
                    descPromoTemp = true;
                    if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                        montoConDes10 = (double) (montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
                        montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                    } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                        montoConDes5 = (double) (montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
                        montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                    } else {
                        exenta = (long) (exenta + (((precio * cantidad) * (100 - porc)) / 100));
                        montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
                    }
                    //precio unitario neto, en detalle, solo para aquellos que manejan descuento por artículo
                    hashDetallePrecioDescPromoArt.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
                    if (hashJsonPromocionArtDesc.containsKey(
                            Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))))) {
                        hashJsonPromocionArtDesc.put(
                                Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                hashJsonPromocionArtDesc.get(Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString())))) + Math.round(descuentoPromoArt));
                    } else {
                        hashJsonPromocionArtDesc.put(
                                Descuento.getHashPromTempArtCab().get(Descuento.getHashPromTempArt().get(Long.valueOf(jsonArticuloB.get("codArticulo").toString()))),
                                Math.round(descuentoPromoArt));
                    }
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                    //promoción + bajada
                }
            }
        }
        descuentoPromoNf = 0;
        for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
            if (!hashDetallePrecioDescPromoArt.containsKey(detalleArtJson)) {//se da prioridad a artículo, luego NF
                JSONObject jsonArticulo = (JSONObject) detalleArtJson.get("articulo");
                JSONArray jsonArrayArticuloNf;
                JSONObject jsonNf;
                JSONObject jsonArticuloNf;
                //el artículo no aplica descuento en caso de estar en bajada desde el bloque 3
                if (!(boolean) jsonArticulo.get("bajada")) {
                    if (jsonArticulo.get("articuloNf7Secnom7") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf7Secnom7");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf7Secnom7");
                        calculandoPromoTempNf(7, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf6Secnom6") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf6Secnom6");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf6Secnom6");
                        calculandoPromoTempNf(6, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf5Seccion2") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf5Seccion2");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf5Seccion2");
                        calculandoPromoTempNf(5, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf4Seccion1") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf4Seccion1");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf4Seccion1");
                        calculandoPromoTempNf(4, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf3Sseccion") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf3Sseccion");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf3Sseccion");
                        calculandoPromoTempNf(3, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf2Sfamilia") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf2Sfamilia");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf2Sfamilia");
                        calculandoPromoTempNf(2, jsonNf, detalleArtJson);
                    } else if (jsonArticulo.get("articuloNf1Tipo") != null) {
                        jsonArrayArticuloNf = (JSONArray) jsonArticulo.get("articuloNf1Tipo");
                        jsonArticuloNf = (JSONObject) jsonArrayArticuloNf.get(0);
                        jsonNf = (JSONObject) jsonArticuloNf.get("nf1Tipo");
                        calculandoPromoTempNf(1, jsonNf, detalleArtJson);
                    } else {
                        Utilidades.log.info("El ARTÍCULO [" + jsonArticulo.get("codArticulo") + "] (" + jsonArticulo.get("descripcion") + ") no dispone de NF.");
                    }
                } else {
                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("bajada")) {
                        //promoción + bajada
                    }
                }
            }
        }
        //verificación de productos sin descuento, sumatoria para Grav y Exe, formulario MensajeFinalVentaFXMLController
        if (descPromoTemp) {
            if (!tarjeta) {
                if (!valeActivo) {
                    if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                    } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                        entradaFunc1era = false;
                        calculandoFuncionario();
                    }
                } else {
                    if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")
                            && comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
                        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                            entradaFiel1era = false;
                            calculandoClienteFiel();
                        } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                            entradaFunc1era = false;
                            calculandoFuncionario();
                        }
                    }
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("tarjeta")
                        && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || !listViewTarjetasAgregadas.getItems().isEmpty())) {
                    if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = false;
                        calculandoClienteFiel();
                    } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                        entradaFunc1era = false;
                        calculandoFuncionario();
                    }
                }
            }
            for (JSONObject detalleArtJson : FacturaDeVentaFXMLController.getDetalleArtList()) {
                long precio = Long.valueOf(detalleArtJson.get("precio").toString());
                int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
                if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
                    if (montoMap10.get(detalleArtJson) == null) {
                        montoConDes10 = montoConDes10 + (double) (precio * cantidad);
                    }
                } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
                    if (montoMap5.get(detalleArtJson) == null) {
                        montoConDes5 = montoConDes5 + (double) (precio * cantidad);
                    }
                } else if (montoMapExe.get(detalleArtJson) == null) {
                    exenta = exenta + (long) (precio * cantidad);
                }
            }
            descuento = Math.round(descuentoPromoArtSum) + Math.round(descuentoPromoNfSum) + Math.round(descuentoFielSum) + Math.round(descuentoFuncSum);
            totalAPagar = FacturaDeVentaFXMLController.getPrecioTotal() - descuento;
            textFieldTotalAPagar.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totalAPagar))));
            textFieldDescuento.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(descuento))));
            if (descuento > 0) {
                lineStrokeTotal.setVisible(true);
            } else {
                lineStrokeTotal.setVisible(false);
            }
            seteandoTotalAbonado();
        } else if (!tarjeta) {
            if (!valeActivo) {
                if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                    entradaFiel1era = true;
                    calculandoClienteFiel();
                } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    entradaFunc1era = true;
                    calculandoFuncionario();
                }
            } else {
                if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("vale")
                        && comboBoxVale.getSelectionModel().getSelectedIndex() != 0) {
                    if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                        entradaFiel1era = true;
                        calculandoClienteFiel();
                    } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                        entradaFunc1era = true;
                        calculandoFuncionario();
                    }
                }
            }
        } else {
            System.out.println(comboBoxTarjetas.getSelectionModel().getSelectedItem());
            if ((Boolean) Descuento.getJsonDescuentoFielFormasDePago().get("tarjeta")
                    && (!comboBoxTarjetas.getSelectionModel().getSelectedItem().contentEquals("") || !listViewTarjetasAgregadas.getItems().isEmpty())) {
                if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                    entradaFiel1era = true;
                    calculandoClienteFiel();
                } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                    entradaFunc1era = true;
                    calculandoFuncionario();
                }
            }
        }
//        editandoBarChart();
    }

    @SuppressWarnings("element-type-mismatch")
    private void calculandoPromoTempNf(int nf, JSONObject jsonNf, JSONObject detalleArtJson) {
        Date date = new Date();
        Timestamp tsNow = new Timestamp(date.getTime());
        switch (nf) {
            case 1:
                if (Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf1().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf1().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                }
                break;
            case 2:
                if (Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")) != null) {
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf2().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf2().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf1Tipo");
                    calculandoPromoTempNf(1, jsonRaiz, detalleArtJson);
                }
                break;
            case 3:
                if (Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf3().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf3().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf2Sfamilia");
                    calculandoPromoTempNf(2, jsonRaiz, detalleArtJson);
                }
                break;
            case 4:
                if (Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf4().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf4().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf3Sseccion");
                    calculandoPromoTempNf(3, jsonRaiz, detalleArtJson);
                }
                break;
            case 5:
                if (Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf5().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf5().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf4Seccion1");
                    calculandoPromoTempNf(4, jsonRaiz, detalleArtJson);
                }
                break;
            case 6:
                if (Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf6().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf6().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf5Seccion2");
                    calculandoPromoTempNf(5, jsonRaiz, detalleArtJson);
                }
                break;
            case 7:
                if (Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")) != null) {//verificación de descuento según sección...
                    Timestamp tsIni = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaInicio"));
                    Timestamp tsFin = Utilidades.objectToTimestamp(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")).get("fechaFin"));
                    tsFin.setHours(23);
                    tsFin.setMinutes(59);
                    tsFin.setSeconds(59);
                    if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                        calculandoPromoTempDet(Descuento.getHashPromoTempCabNf7().get(jsonNf.get("descripcion")),
                                Descuento.getHashPromoTempDetNf7().get(jsonNf.get("descripcion")), detalleArtJson);
                    }//en el rango de fechas; de hecho ya se validó en configuración, y login caja...
                } else {
                    JSONObject jsonRaiz = (JSONObject) jsonNf.get("nf6Secnom6");
                    calculandoPromoTempNf(6, jsonRaiz, detalleArtJson);
                }
                break;
            default:
                break;
        }
    }

    private void calculandoPromoTempDet(JSONObject jsonPromoTempCab, JSONObject jsonPromoTempDet, JSONObject detalleArtJson) {
        int cantidad = Integer.valueOf(detalleArtJson.get("cantidad").toString());
        double porc = Double.valueOf(jsonPromoTempDet.get("porcentajeDesc").toString());
        long precio = Long.valueOf(detalleArtJson.get("precio").toString());
        descuentoPromoNf = Math.round(Double.parseDouble((((precio * cantidad) * porc) / 100) + ""));//descuento total en esta promoción...
        descuentoPromoNfSum += Double.parseDouble((((precio * cantidad) * porc) / 100) + "");//descuento global por promoción
        descPromoTemp = true;
        if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 10) {
            montoConDes10 = Math.round(montoConDes10 + (((precio * cantidad) * (100 - porc)) / 100));
            montoMap10.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        } else if (Long.valueOf(detalleArtJson.get("poriva").toString()) == 5) {
            montoConDes5 = Math.round(montoConDes5 + (((precio * cantidad) * (100 - porc)) / 100));
            montoMap5.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        } else {
            exenta = Math.round(exenta + (((precio * cantidad) * (100 - porc)) / 100));
            montoMapExe.put(detalleArtJson, Math.round(((precio * cantidad) * (100 - porc)) / 100));
        }
        //precio unitario neto, en detalle, solo para aquellos que manejan descuento por sección
        hashDetallePrecioDesc.put(detalleArtJson, (long) ((precio * (100 - porc)) / 100));
        if (descuentoPromoNf > 0) {
            if (hashJsonPromocionDesc.containsKey(jsonPromoTempCab)) {
                descuentoPromoNf = descuentoPromoNf + hashJsonPromocionDesc.get(jsonPromoTempCab);
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            } else {
                hashJsonPromocionDesc.put(jsonPromoTempCab, descuentoPromoNf);
            }
        }
    }
    //PROMOCIÓN TEMPORADA*******************************************************

    //VALE**********************************************************************
    private void calculandoVale(String tipoVale) {
        if (!descTarjeta && !descTarjetaConvenio) {
            if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
                checkBoxPromoTempButton();
            } else if (FuncionarioFXMLController.getJsonFuncionario() != null) {
                checkBoxPromoTempButton();
            } else {
                seteandoTotales();
                seteandoTotalAbonado();
            }
        } else {
            validandoCambiandoFormaPago();
        }
        switch (tipoVale) {
            case "FUNCIONARIO":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "ORDEN DE COMPRA":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "ACUERDO COMERCIAL":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "ASOCIACION":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "ARGOS":
                if (!textFieldMontoValePorc.getText().contentEquals("")) {
                    descValePorc = true;
                }
                break;
            case "PARESA":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "FRIGOMERC":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "INSPECTORATE":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "CELLULAR":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            case "ESSEN":
                if (!textFieldMontoVale.getText().contentEquals("")) {
                    descValeMonto = true;
                }
                break;
            default:
                break;
        }
    }

    //VALE**********************************************************************
    //DIRECTIVO*****************************************************************
    private void calculandoDirectivo() {
        seteandoTotales();
        seteandoControlDesc();
        descDirectivo = true;
        seteandoTotalAbonado();
    }

    //DIRECTIVO*****************************************************************
    //NOTA DE CRÉDITO***********************************************************
    private void calculandoNotaCredito() {
        boolean auxDirect = descDirectivo;
//        seteandoTotalesParaNotaCred();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }

    //NOTA DE CRÉDITO***********************************************************
    //MONTO RETENCIÓN***********************************************************
    private void calculandoMontoRetencion() {
        boolean auxDirect = descDirectivo;
        seteandoTotales();
        if (auxDirect) {
            descDirectivo = auxDirect;//el que pisotea todo...
        }
        seteandoTotalAbonado();
    }
    //MONTO RETENCIÓN***********************************************************
    //DESCUENTOS DESCUENTOS DESCUENTOS ***************** -> -> -> -> -> -> -> ->

    //SET SET SET SET SET SET ************************** -> -> -> -> -> -> -> ->
    private void seteandoDescuentos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDesc = 0;
        int sumDesc = 0;
        if (!json.isNull("contDesc")) {
            contDesc = Integer.parseInt(datos.get("contDesc").toString());
        }
        if (!json.isNull("sumDesc")) {
            sumDesc = Integer.parseInt(datos.get("sumDesc").toString());
        }
        int descActual = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        if (descActual != 0) {
            sumDesc += descActual;
            contDesc += 1;
            datos.put("contDesc", contDesc);
            datos.put("sumDesc", sumDesc);
            formaDePago.put("monDesc", sumDesc);
        }
    }

    private void seteandoTarjetas() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contTarj = 0;
        int sumTarj = 0;
        if (!json.isNull("contTarj")) {
            contTarj = Integer.parseInt(datos.get("contTarj").toString());
        }
        if (!json.isNull("sumTarj")) {
            sumTarj = Integer.parseInt(datos.get("sumTarj").toString());
        }
        ObservableList<String> datoes = listViewTarjetasAgregadas.getItems();
        int sumTarjeta = 0;
        for (String x : datoes) {
            contTarj++;
            String[] arraySplit = x.split(" - ");
            String banco = arraySplit[0];
            String monto = arraySplit[1];
            String cod = arraySplit[2];
            String mon = monto.replaceAll("", " (Borrar)");
            sumTarj += Integer.valueOf(numValidator.numberValidator(mon));
            sumTarjeta += Integer.valueOf(numValidator.numberValidator(mon));
        }
        datos.put("contTarj", contTarj);
        datos.put("sumTarj", sumTarj);
        formaDePago.put("monTarj", sumTarjeta);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoCheques() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contChq = 0;
        int sumChq = 0;
        if (!json.isNull("contCheque")) {
            contChq = Integer.parseInt(datos.get("contCheque").toString());
        }
        if (!json.isNull("sumCheque")) {
            sumChq = Integer.parseInt(datos.get("sumCheque").toString());
        }
        int sumaCheque = 0;
        ObservableList<String> datoes = listViewCheques.getItems();
        boolean estado = false;
        for (String x : datoes) {
            contChq++;
            StringTokenizer st = new StringTokenizer(x, "-");
            String banco = st.nextElement().toString();
            String cod = st.nextElement().toString();
            String monto = st.nextElement().toString();
            String mon = monto.replaceAll("", " (Borrar)");
            sumChq += Integer.valueOf(numValidator.numberValidator(mon));
            sumaCheque += Integer.valueOf(numValidator.numberValidator(mon));
            arrayCheque.add(banco + " - " + cod + " - " + monto);
            estado = true;
        }
        datos.put("contCheque", contChq);
        datos.put("sumCheque", sumChq);
        if (estado) {
            datos.put("arrayCheque", arrayCheque);
        }
        formaDePago.put("monCheque", sumaCheque);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoNotaCred() {
        int notaCred = 0;
        int sumNotaCred = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("contNotaCred")) {
            notaCred = Integer.parseInt(datos.get("contNotaCred").toString());
        }
        if (!json.isNull("sumNotaCred")) {
            sumNotaCred = Integer.parseInt(datos.get("sumNotaCred").toString());
        }
        int sumaNota = 0;
        ObservableList<String> datoes = listViewNotasCred.getItems();
        for (String x : datoes) {
            notaCred += 1;
            StringTokenizer st = new StringTokenizer(x, "-");
            String nro = st.nextElement().toString();
            String monto = st.nextElement().toString();
            String mon = monto.replaceAll("", " (Borrar)");
            sumNotaCred += Integer.valueOf(numValidator.numberValidator(mon));
            sumaNota += Integer.valueOf(numValidator.numberValidator(mon));
        }
        datos.put("sumNotaCred", sumNotaCred);
        datos.put("contNotaCred", notaCred);
        formaDePago.put("monNotaCred", sumaNota);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoEfectivos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contEfect = 0;
        if (!json.isNull("cantEfectivoRecibido")) {
            datos.put("cantEfectivoRecibido", Integer.parseInt(datos.get("cantEfectivoRecibido").toString()) + Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText())));
        } else {
            datos.put("cantEfectivoRecibido", numValidator.numberValidator(textFieldEfectivo.getText()));
        }
        if (!json.isNull("contEfectivo")) {
            contEfect = Integer.parseInt(datos.get("contEfectivo").toString());
        }
        int montoActual = Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        long dif = totalAbonado - totalAPagar;
        if (dif == 0) {
            contEfect += 1;
            datos.put("contEfectivo", contEfect);
            //por si vaya la energia electrica
            formaDePago.put("monEfectivo", montoActual);
        } else {
            int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
            if (montoActual > vuel) {
                int resultado = montoActual - vuel;
                contEfect += 1;
                datos.put("contEfectivo", contEfect);
                formaDePago.put("monEfectivo", resultado);
            } else {
                datos.put("contEfectivo", 0);
                formaDePago.put("monEfectivo", 0);
            }
        }
    }

    private void seteandoFuncionario() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!txtNombreFuncionario.getText().equalsIgnoreCase("")) {
            int nFun = 0;
            if (!json.isNull("nFuncionarios")) {
                nFun = Integer.parseInt(datos.get("nFuncionarios").toString());
            }
            datos.put("nFuncionarios", (nFun + 1));
        }
    }

    private void seteandoSumaTotales() {
        int sumTotales = 0;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("totales")) {
            sumTotales = Integer.parseInt(datos.get("totales").toString());
        }
        int monto = sumTotales + Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
        datos.put("totales", monto);
    }

    private void seteandoVales() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contVale = 0;
        if (!json.isNull("contVale")) {
            contVale = Integer.parseInt(datos.get("contVale").toString());
        }
        datos.put("contVale", contVale + 1);
        int sumVale = 0;
        if (!json.isNull("sumVale")) {
            sumVale = Integer.parseInt(datos.get("sumVale").toString());
        }
        datos.put("sumVale", (sumVale + Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()))));
        formaDePago.put("monVale", Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText())));
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoAsociacion() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contAsoc = 0;
        if (!json.isNull("contAsoc")) {
            contAsoc = Integer.parseInt(datos.get("contAsoc").toString());
        }
        datos.put("contAsoc", contAsoc + 1);
        int sumAsoc = 0;
        if (!json.isNull("sumAsoc")) {
            sumAsoc = Integer.parseInt(datos.get("sumAsoc").toString());
        }
        datos.put("sumAsoc", (sumAsoc + Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()))));
        formaDePago.put("monAsoc", Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText())));
        datos.put("asociacionTicket", textFieldMontoVale.getText());
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoDolares() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contDolar = 0;
        if (!json.isNull("contDolar")) {
            contDolar = Integer.parseInt(datos.get("contDolar").toString());
        }
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
        datos.put("contDolar", (contDolar + cantidad));
        String x = textFieldDolarGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        int sumDolar = 0;
        if (!json.isNull("sumDolar")) {
            sumDolar = Integer.parseInt(datos.get("sumDolar").toString());
        }
        datos.put("sumDolar", (sumDolar + monto));
        formaDePago.put("monDolar", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoReales() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contReal = 0;
        int sumReal = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
        if (!json.isNull("contReal")) {
            contReal = Integer.parseInt(datos.get("contReal").toString());
        }
        datos.put("contReal", (contReal + cantidad));
        String x = textFieldRealGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        if (!json.isNull("sumReal")) {
            sumReal = Integer.parseInt(datos.get("sumReal").toString());
        }
        datos.put("sumReal", (sumReal + monto));
        formaDePago.put("monReal", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void seteandoPesos() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int contPeso = 0;
        int sumPeso = 0;
        int cantidad = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
        if (!json.isNull("contPeso")) {
            contPeso = Integer.parseInt(datos.get("contPeso").toString());
        }
        datos.put("contPeso", (contPeso + cantidad));
        String x = textFieldPesoGs.getText().replace("Gs ", "");
        int monto = Integer.valueOf(numValidator.numberValidator(x));
        if (!json.isNull("sumPeso")) {
            sumPeso = Integer.parseInt(datos.get("sumPeso").toString());
        }
        datos.put("sumPeso", (sumPeso + monto));
        formaDePago.put("monPeso", monto);
        setearEfectivoPorOtraFormaDePago();
    }

    private void setearEfectivoPorOtraFormaDePago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        long dif = totalAbonado - totalAPagar;
        if (textFieldEfectivo.getText().equalsIgnoreCase("") && dif >= 0) {
            if (estado) {
                estado = false;
                int vuel = Integer.valueOf(numValidator.numberValidator(textFieldVuelto.getText()));
                //para cuando cae la corriente electrica
                int monEfec = 0;
                if (!json.isNull("monEfectivo")) {
                    monEfec = Integer.parseInt(datos.get("monEfectivo").toString());
                }
                datos.put("monEfectivo", (monEfec - vuel));
            }
        }
    }

    private void seteandoRetencionciones() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        int sumRetencion = 0;
        int contRetencion = 0;
        if (!json.isNull("sumRetencion")) {
            sumRetencion = Integer.parseInt(datos.get("sumRetencion").toString());
        }
        if (!json.isNull("contRetencion")) {
            contRetencion = Integer.parseInt(datos.get("contRetencion").toString());
        }
        int montoRetencion = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        datos.put("sumRetencion", (sumRetencion + montoRetencion));
        formaDePago.put("monRetencion", montoRetencion);
        datos.put("contRetencion", (contRetencion + 1));
    }
    //SET SET SET SET SET SET ************************** -> -> -> -> -> -> -> ->

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT
    private boolean actualizandoCabFactura() {
        JSONParser parser = new JSONParser();
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (!jsonDatos.isNull("cancelProducto")) {
            FacturaDeVentaFXMLController.cancelacionProd = true;
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }

        try {
            editandoJsonFactCab();
            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                org.json.JSONObject json = new org.json.JSONObject(datos);
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroActualmente = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                datos.put("nroFact", nroActualmente);
                FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
            }
            if (!jsonDatos.isNull("cancelProducto")) {
                JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                long idTalonario = Long.parseLong(talona.get("idTalonariosSucursales").toString());
                TalonariosSucursales tal = taloDAO.getById(idTalonario);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
            }
            FacturaDeVentaFXMLController.setCabFactura(registrarFacturaCabeceraLocal(FacturaDeVentaFXMLController.getCabFactura().toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("caida", "forma_pago");
        if (FacturaDeVentaFXMLController.getCabFactura() != null) {
            exitoInsertarCab = true;
        }
        //para registrar facturaIni
        org.json.JSONObject jsons = new org.json.JSONObject(datos);
        boolean estadoFa = false;
        if (!jsons.isNull("estadoFacturaInicial")) {
            estadoFa = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        boolean estadoInicial = estadoFa;
        if (!estadoInicial) {
            datos.put("facturaInicial", FacturaDeVentaFXMLController.getCabFactura().get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("facturaFinal", FacturaDeVentaFXMLController.getCabFactura().get("nroFactura").toString());
        if (FacturaDeVentaFXMLController.isActualizarDatosCabecera()) {
            caida = false;
        }
        //VERIFY
        if (!jsonDatos.isNull("cancelProducto")) {
            caida = false;
        }
        if (exitoInsertarCab && !caida) {
            if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
                JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
                JSONArray arrayDetalle = new JSONArray();
                for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                    try {
                        JSONObject json = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                        JSONObject art = (JSONObject) parser.parse(json.get("articulo").toString());
                        art.put("fechaAlta", null);
                        art.put("fechaMod", null);
                        JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                        iva.put("fechaAlta", null);
                        iva.put("fechaMod", null);
                        art.put("iva", iva);
                        json.put("articulo", art);
                        arrayDetalle.add(json);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
                DatosEnCaja.setDatos(datos);
                DatosEnCaja.setUsers(users);
                DatosEnCaja.setFacturados(fact);
                datos = DatosEnCaja.getDatos();
                users = DatosEnCaja.getUsers();
                fact = DatosEnCaja.getFacturados();
                if (creandoFactDet(arrayDetalle)) {
                    exitoInsertarDet = false;
                }
                gestionandoPago(FacturaDeVentaFXMLController.getCabFactura());
            } else {
                mensajeError("LA FACTURA NO HA SIDO PROCESADA.");
            }
        }
        return exitoInsertarCab;
    }
    //////CREATE || UPDATE, FACTURA CAB. -> POST || PUT

    ////CREATE, FACTURA DET. MASIVO -> POST
    private boolean creandoFactDet(JSONArray jsonArray) {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
            exitoInsertarDet = registrarArrayDetalleLocal(jsonArrayFactDet.toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }
    ////CREATE, FACTURA DET. MASIVO -> POST

    ////CREATE, FACTURA CAB. EFECTIVO -> POST
    private boolean creandoCabFacturaEfectivo(JSONObject cabFactura) {
        JSONParser parser = new JSONParser();
        JSONObject cabFacturaEfectivo = new JSONObject();
        boolean finalizo = false;
        long idFact = Long.parseLong(cabFactura.get("idFacturaClienteCab").toString());
        JSONObject obj = new JSONObject();
        obj.put("idFacturaClienteCab", idFact);
        cabFacturaEfectivo = creandoJsonEfectivo(obj);
        long idRango = rangoEfectivoDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaEfectivo.put("idFacturaClienteCabEfectivo", idRango);
        try {
            Map mapeo = registrarFacturaEfectivoLocal(cabFacturaEfectivo);
            cabFacturaEfectivo = (JSONObject) parser.parse(mapeo.get("cabFacturaEfectivo").toString());
            finalizo = (boolean) mapeo.get("finalizo");
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        datos.put("facturaEfectivo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. EFECTIVO -> POST

    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST
    private boolean creandoCabFacturaMonedaExt(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayMonedaExt = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoMonedaExtraDAO.actualizarRecuperarRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabMonedaExtranjera", idRango);
                jsonArrayMonedaExt.add(obj);
            }
            finalizo = registrarFacturaMonedaExtranjera(jsonArrayMonedaExt);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaMonedaExtranjera", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. MONEDA EXTRANJERA -> POST

    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST
    private boolean creandoCabFacturaNotaCred(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoNotaCredDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) parser.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.remove("facturaClienteCabDTO");
                obj.put("facturaClienteCab", facturaClienteCab);
                obj.put("idFacturaClienteCabNotaCredito", idRango);
                obj.put("nroNota", obj.get("nroNota"));
                obj.put("monto", obj.get("monto"));
//                jsonArrayCheque.add(obj);
                finalizo = registrarNotaCreditoLocal(obj.toString());
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. NOTA CRÉDITO -> POST

    //////CREATE, FACTURA CAB. RETENCIÓN -> POST
    private boolean creandoCabFacturaMontoRetencion(JSONObject cabFactura) {
        JSONObject cabFacturaMontoRetencion = new JSONObject();
        boolean finalizo = false;
        cabFacturaMontoRetencion = creandoJsonMontoRetencion(cabFactura);
        finalizo = registrarRetencion(cabFacturaMontoRetencion.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. RETENCIÓN -> POST

    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFiel(JSONObject cabFactura) {
        JSONObject cabFacturaClienteFiel = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjFielDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaClienteFiel = creandoJsonTarjClienteFiel(cabFactura);
        cabFacturaClienteFiel.put("idFacturaClienteCabTarjFiel", idRango);
        cabFacturaClienteFiel = registrarTarjetaFielLocal(cabFacturaClienteFiel);
        if (cabFacturaClienteFiel != null) {
            gestionandoDescuento(cabFacturaClienteFiel, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CLIENTE FIEL -> POST

    //////CREATE, FACTURA CAB. CHEQUE -> POST
    private boolean creandoCabFacturaCheque(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayCheque = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoChequeDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                JSONObject fact = (JSONObject) parser.parse(obj.get("facturaClienteCabDTO").toString());
                long idFact = Long.parseLong(fact.get("idFacturaClienteCab").toString());
                JSONObject facturaClienteCab = new JSONObject();
                facturaClienteCab.put("idFacturaClienteCab", idFact);
                obj.put("facturaClienteCabDTO", facturaClienteCab);
                obj.put("idFacturaClienteCabCheque", idRango);
                jsonArrayCheque.add(obj);
            }
            finalizo = realizarFactChequeLocal(jsonArrayCheque);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        datos.put("facturaCheque", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. CHEQUE -> POST

    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjConv(JSONObject cabFactura) {
        JSONObject cabFacturaTarjConv = new JSONObject();
        boolean finalizo = false;
        long idRango = rangoTarjetaConvenioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaTarjConv = creandoJsonTarjetaConvenio(cabFactura);
        cabFacturaTarjConv.put("idFacturaClienteCabTarjConvenio", idRango);
        cabFacturaTarjConv = registrarTarjetaConvenioLocal(cabFacturaTarjConv);
        if (cabFacturaTarjConv != null) {
            gestionandoDescuento(cabFacturaTarjConv, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CAB. TARJETA CONVENIO -> POST

    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionario(JSONObject cabFactura) {
        JSONObject cabFacturaFuncionario = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionario = creandoJsonFuncionario(cabFactura);
        long idRango = rangoFuncionarioDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaFuncionario.put("idFacturaClienteCabFuncionario", idRango);
        cabFacturaFuncionario = registrarCabFuncionarioLocal(cabFacturaFuncionario);
        if (cabFacturaFuncionario != null) {
            gestionandoDescuento(cabFacturaFuncionario, false);
            finalizo = true;
        }
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. FUNCIONARIO -> POST

    //////CREATE, FACTURA CAB. TARJETA -> POST
    private boolean creandoCabFacturaTarjeta(JSONArray jsonArr) {
        JSONParser parser = new JSONParser();
        boolean finalizo = false;
        JSONArray jsonArrayTarjeta = new JSONArray();
        try {
            for (int i = 0; i < jsonArr.size(); i++) {
                long idRango = rangoTarjetaDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                JSONObject obj = (JSONObject) parser.parse(jsonArr.get(i).toString());
                obj.put("idFacturaClienteCabTarjeta", idRango);
                jsonArrayTarjeta.add(obj);
            }
            jsonArrayTarjeta = registrarCabTarjetaLocal(jsonArrayTarjeta);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (jsonArrayTarjeta != null) {
            finalizo = true;
            if (jsonArrayTarjeta.size() == 1) {
                JSONObject jsonTarj = (JSONObject) jsonArrayTarjeta.get(0);
                gestionandoDescuento(jsonTarj, false);
            }
        }
        datos.put("facturaTarjeta", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    //////CREATE, FACTURA CAB. TARJETA -> POST

    //////READ, TARJETA CONVENIO -> GET
    private void jsonCargandoTarjetasConvenio() {
        cargarTarjetaConvenio = false;
        tarjetaConvenioList = new ArrayList<>();
        hashJsonComboTarjConvenio = new HashMap<>();
        generarComboTarjetaConvenioLocal();
    }
    //////READ, TARJETA CONVENIO -> GET

    //////READ, TARJETA -> GET
    private void jsonCargandoTarjetas() {
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        generarComboTarjetaLocal();
    }
    //////READ, TARJETA -> GET

    //////READ, VALE -> GET
    private void jsonCargandoVales() {
        cargarVale = false;
        valeList = new ArrayList<>();
        hashJsonComboVale = new HashMap<>();
        generarComboValeLocal();
    }
    //////READ, VALE -> GET

    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    ////CREATE, DESCUENTO FUNCIONARIO -> POST
    private boolean creandoCabFacturaFuncionarioDesc(JSONObject cabFacturaFuncionario) {
        JSONObject cabFacturaFuncionarioDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaFuncionarioDesc = creandoJsonDescFuncionario(cabFacturaFuncionario);
        finalizo = registrarDescFuncionario(cabFacturaFuncionarioDesc.toString());
        datos.put("facturaFuncionario", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO FUNCIONARIO -> POST

    ////CREATE, DESCUENTO CLIENTE FIEL -> POST
    private boolean creandoCabFacturaClienteFielDesc(JSONObject cabFacturaFiel) {
        JSONObject cabFacturaClienteFielDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaClienteFielDesc = creandoJsonDescTarjFiel(cabFacturaFiel);
        if (Integer.parseInt(cabFacturaClienteFielDesc.get("montoDesc").toString()) != 0) {
            finalizo = registrarDescTarjFielLocal(cabFacturaClienteFielDesc.toString());
            datos.put("facturaTarjetaFiel", false);
            DatosEnCaja.setDatos(datos);
            datos = DatosEnCaja.getDatos();
        }
        return finalizo;
    }
    ////CREATE, DESCUENTO CLIENTE FIEL -> POST

    ////CREATE, DESCUENTO VALE -> POST
    private boolean creandoCabFacturaValeDesc(JSONObject cabFactura, JSONObject vale) {
        JSONObject cabFacturaValeDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeDesc = creandoJsonDescVale(cabFactura, vale);
        finalizo = registrarDescValeLocal(cabFacturaValeDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO VALE -> POST

    ////CREATE, DESCUENTO TARJETA -> POST
    private boolean creandoCabFacturaTarjetaDesc(JSONObject cabFacturaTarj) {
        JSONObject cabFacturaTarjetaDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaTarjetaDesc = creandoJsonDescTarj(cabFacturaTarj);
        finalizo = registrarDescTarjetaLocal(cabFacturaTarjetaDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO TARJETA -> POST

    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST
    private boolean creandoCabFacturaTarjetaConvDesc(JSONObject cabFactura) {
        JSONObject cabFacturaTarjetaConvDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaTarjetaConvDesc = creandoJsonDescTarjConv(cabFactura);
        finalizo = registrarConvenioLocal(cabFacturaTarjetaConvDesc.toString());
        datos.put("facturaTarjetaConv", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO TARJETA CONVENIO -> POST

    ////CREATE, DESCUENTO DIRECTIVO -> POST
    private boolean creandoCabFacturaDirectivoDesc(JSONObject cabFactura) {
        JSONObject cabFacturaDirectivoDesc = new JSONObject();
        boolean finalizo = false;
        cabFacturaDirectivoDesc = creandoJsonDescDirectivo(cabFactura);
        finalizo = registrarDescDirectivo(cabFacturaDirectivoDesc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, DESCUENTO DIRECTIVO -> POST

    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST
    private boolean creandoCabFacturaPromTempDesc(JSONObject cabFacturaPromTempDesc) {
        boolean finalizo = false;
        JSONObject jsonPromTemp = null;
        long idRango = rangoPromoTempDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
        cabFacturaPromTempDesc.put("idFacturaClienteCabPromoTemp", idRango);
        jsonPromTemp = registrarPromoTemp(cabFacturaPromTempDesc.toString());
        if (jsonPromTemp != null) {
            finalizo = true;
        }
        datos.put("facturaPromo", false);
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. -> POST

    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. ART. -> POST
    private boolean creandoCabFacturaPromTempArtDesc(JSONObject cabFacturaPromTempArtDesc) {
        boolean finalizo = false;
        JSONObject jsonPromTemp = null;
        cabFacturaPromTempArtDesc.put("idFacturaClienteCabPromoTempArt", rangoPromoTempArtDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
        jsonPromTemp = registrarPromoTempArt(cabFacturaPromTempArtDesc.toString());
        if (jsonPromTemp != null) {
            finalizo = true;
        }
        datos.put("facturaPromo", false);
        DatosEnCaja.setDatos(datos);
        return finalizo;
    }
    ////CREATE, FACTURA CLIENTE CAB. PROMO TEMP. ART. -> POST

    ////CREATE, VALE -> POST
    private boolean creandoCabFacturaValeMonto(JSONObject cabFactura) {
        JSONObject cabFacturaValeMonto = new JSONObject();
        boolean finalizo = false;
        cabFacturaValeMonto = creandoJsonValeMonto(cabFactura);
        finalizo = registrarValeLocal(cabFacturaValeMonto.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE -> POST

    ////CREATE, VALE PORC. -> POST
    private boolean creandoCabFacturaValePorc(JSONObject cabFactura) {
        JSONObject cabFacturaValePorc = new JSONObject();
        boolean finalizo = false;
        cabFacturaValePorc = creandoJsonValePorc(cabFactura);
        finalizo = registrarPorcValeLocal(cabFacturaValePorc.toString());
        DatosEnCaja.setDatos(datos);
        datos = DatosEnCaja.getDatos();
        return finalizo;
    }
    ////CREATE, VALE PORC. -> POST
    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, TARJETA CONVENIO
    public void generarComboTarjetaConvenioLocal() {
        JSONParser parser = new JSONParser();
        List<TarjetaConvenio> tarConv = tarConvenioDAO.listar();
        for (TarjetaConvenio tarCon : tarConv) {
            try {
                JSONObject tarjetaConvenio = (JSONObject) parser.parse(gson.toJson(tarCon.toTarjetaConvenioNombreIdDTO()));
                tarjetaConvenioList.add(tarjetaConvenio);
                comboBoxTarjetasConvenio.getItems().add(tarjetaConvenio.get("descripcion").toString());
                hashJsonComboTarjConvenio.put(tarjetaConvenio.get("descripcion").toString(), tarjetaConvenio);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
    }
    //////READ, TARJETA CONVENIO

    //////READ, VALE
    public void generarComboValeLocal() {
        JSONParser parser = new JSONParser();
        List<Vales> val = valeDAO.listar();
        for (Vales vales : val) {
            try {
                JSONObject vale = (JSONObject) parser.parse(gson.toJson(vales.toValeDTO()));
                valeList.add(vale);
                comboBoxVale.getItems().add(vale.get("descripcionVale").toString());
                ObservableList<String> allOptions
                        = FXCollections.observableArrayList("Easy", "Normal", "Hard");
                hashJsonComboVale.put(vale.get("descripcionVale").toString(), vale);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
    }
    //////READ, VALE

    //////READ, TARJETA
    @SuppressWarnings("ResultOfObjectAllocationIgnored")
    public void generarComboTarjetaLocal() {
        JSONParser parser = new JSONParser();
        cargarTarjeta = false;
        hashJsonComboTarjeta = new HashMap<>();
        List<Tarjeta> listTarjeta = tarDAO.listar();
        comboBoxTarjetas.getItems().add("");
        //tarjetas con descuento
        for (JSONObject jsonObjectDescuentoTarjeta : Descuento.getHashDescTarjeta().values()) {
            comboBoxTarjetas.getItems().add(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString());
            hashJsonComboTarjeta.put(jsonObjectDescuentoTarjeta.get("descriTarjeta").toString(), jsonObjectDescuentoTarjeta);
        }
        //tarjetas sin descuento
        for (Tarjeta tar : listTarjeta) {
            try {
                JSONObject tarjeta = (JSONObject) parser.parse(gson.toJson(tar.toTarjetaDTO()));
                comboBoxTarjetas.getItems().add(tarjeta.get("descripcion").toString());
                hashJsonComboTarjeta.put(tarjeta.get("descripcion").toString(), tarjeta);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        comboBoxTarjetas.setCellFactory(param -> new MyListCell());
        comboBoxTarjetas.setButtonCell(new MyListCell());
        new AutoCompleteComboBoxListener<>(comboBoxTarjetas);
        comboBoxTarjetas.getSelectionModel().select(0);
    }
    //////READ, TARJETA

    //////INSERT || UPDATE, FACTURA CAB.
    private JSONObject registrarFacturaCabeceraLocal(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        String sql = "";
        String operacion = "insertar";
        boolean actualizacion = false;
        JSONObject objSon = new JSONObject();
        org.json.JSONObject jsonObj = new org.json.JSONObject(datos);
        long ids = 0L;
        if (!jsonObj.isNull("idFactClienteCabServidor")) {
            ids = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
        }
        long idFact = ids;
        String xuuids = "";
        if (idFact == 0L) {
            if (FacturaDeVentaFXMLController.isCancelacionProd()) {
                actualizacion = true;
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    xuuids = datos.get("uuidCassandraActual").toString();
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + xuuids + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + json + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
            } else {
                try {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    //ACTUALIZAR TALONARIO SUCURSALES
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    long idTalonario = Long.parseLong(sucursal.get("idTalonariosSucursales").toString());
                    TalonariosSucursales tal = taloDAO.getById(idTalonario);
                    tal.setNroActual(tal.getNroActual() + 1);
                    taloDAO.actualizarNroActual(tal);
                    //ACTUALIZACIONES EN POSTGRESQL LOCALMENTE
                    //UUID ACTUAL PARA MODIFICAR FACTURA
                    datos.put("uuidCassandraActual", uuid);
                    objSon = (JSONObject) parser.parse(json);
                    FacturaDeVentaFXMLController.setCabFactura(objSon);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + objSon.toString() + "', 'insertar');";
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.info("ERROR sql -> " + sql + "\n");
                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                }
            }
        } else {
            try {
                actualizacion = true;
                JSONObject jsonFactura = (JSONObject) parser.parse(json);
                long idFacServer = 0L;
                if (!jsonObj.isNull("idFactClienteCabServidor")) {
                    idFacServer = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
                }
                jsonFactura.put("idFacturaClienteCab", idFacServer);
                String uuids = "";
                if (!jsonObj.isNull("uuidCassandraActual")) {
                    uuids = datos.get("uuidCassandraActual").toString();
                }
                if (uuids.equals("")) {
                    String uuid = String.valueOf(VentasUtiles.recuperarId() + 1);
                    sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + jsonFactura.toString() + "', 'actualizar');";
                } else {
                    long uuid = VentasUtiles.recuperarId();
//                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuids + ";";
                    sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + jsonFactura.toString() + "', fecha=dateOf(now()), operacion ='actualizar' WHERE id_dato=" + uuid + ";";
                }
            } catch (ParseException ex) {
                Utilidades.log.info("ERROR json\n -> " + json + "\n");
                Utilidades.log.info("ERROR sql -> " + sql + "\n");
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.conectar();
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                if (actualizacion) {
                    estados = (JSONObject) parser.parse(json);
                } else {
                    estados = objSon;
                }
                if (estados.get("cliente") == null) {
                    ClienteDAO cliDAO = new ClienteDAOImpl();
                    ClienteDTO clienteDTOs = new ClienteDTO();
                    ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                    clienteDTOs.setIdCliente(161168L);
                    clienteDTOs.setNombre(cliDTO.getNombre());
                    if (cliDTO.getApellido() != null) {
                        clienteDTOs.setApellido(cliDTO.getApellido());
                    }
                    clienteDTOs.setRuc(cliDTO.getRuc());
                    JSONObject clienteJson = (JSONObject) parser.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                }
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR objSon\n -> " + objSon + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.error("SQLException | ParseException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        if (estados.isEmpty()) {
            insertarCabeceraEliminada(json, xuuids);//NUEVO
            org.json.JSONObject jsonFact = new org.json.JSONObject(json);
            String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
            setearCabecera(idFactCab);
            if (actualizacion) {
                try {
                    estados = (JSONObject) parser.parse(json);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR json\n -> " + json + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            } else {
                estados = objSon;
            }
            if (estados.get("cliente") == null) {
                ClienteDAO cliDAO = new ClienteDAOImpl();
                ClienteDTO clienteDTOs = new ClienteDTO();
                ClienteDTO cliDTO = Cliente.toClienteDTO(cliDAO.getById(161168L));
                clienteDTOs.setIdCliente(161168L);
                clienteDTOs.setNombre(cliDTO.getNombre());
                if (cliDTO.getApellido() != null) {
                    clienteDTOs.setApellido(cliDTO.getApellido());
                }
                clienteDTOs.setRuc(cliDTO.getRuc());
                JSONObject clienteJson;
                try {
                    clienteJson = (JSONObject) parser.parse(gson.toJson(clienteDTOs));
                    estados.put("cliente", clienteJson);
                } catch (ParseException ex) {
                    Utilidades.log.info("ERROR clienteDTOs\n -> " + clienteDTOs + "\n");
                    Utilidades.log.error("ParseException ex: ", ex.fillInStackTrace());
                }
            }
        } else {
            if (selectCancelProdAux() != 0) {
                org.json.JSONObject jsonFact = new org.json.JSONObject(json);
                String idFactCab = jsonFact.get("idFacturaClienteCab").toString();
                setearCabecera(idFactCab);
            }
        }
        return estados;
    }

    private int selectCancelProdAux() {
        int cancel = 0;
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";

        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                cancel += 1;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
        return cancel;
    }

    private void setearCabecera(String idFactCab) {
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.auxiliar_cancel_prod";

        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                actualizarDatosCabecera(rs.getString("dato"), idFactCab, rs.getLong("id_dato"));
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info("-->> " + ex.getLocalizedMessage());
        }
        ConexionPostgres.cerrar();
    }

    private void actualizarDatosCabecera(String sql, String idFactCab, long idDato) {
        String replace1 = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'";
        String replace2 = "','cancelacionProducto', 'insertar')";
        String rep = ",\"cancelacionProducto\", \"insertar\")";
        JSONParser parser = new JSONParser();
        sql = sql.replace(replace1, "");

        sql = sql.replace(replace2, "");

        try {
            JSONObject cancelProd = (JSONObject) parser.parse(sql);
            cancelProd.remove("facturaClienteCab");

            JSONObject factCab = new JSONObject();
            factCab.put("idFacturaClienteCab", idFactCab);

            cancelProd.put("facturaClienteCab", factCab);

            String sqlQUERY = "UPDATE desarrollo.auxiliar_cancel_prod SET dato=\'" + replace1.substring(0, replace1.length() - 1) + "\"" + cancelProd + "\"" + rep + "\' WHERE id_dato=" + idDato;

            ConexionPostgres.conectar();

            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sqlQUERY)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    Utilidades.log.info("******* ACTUALIZADO CANCELACION_PRODUCTO_AUXILIAR ********");
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                }
            }

        } catch (ParseException ex) {
            Logger.getLogger(FormaPagoFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void insertarCabeceraEliminada(String json, String uuids) {
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuids + ",'" + json + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL CANCELACION PRODUCTO ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.info("ERROR json\n -> " + json + "\n");
            Utilidades.log.info("ERROR sql -> " + sql + "\n");
            Utilidades.log.info("ERROR uuids -> " + uuids + "\n");
            Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info("-->> " + ex1.getLocalizedMessage());
                Utilidades.log.error("SQLException ex: ", ex.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
    }
    //////INSERT || UPDATE, FACTURA CAB.

    //////INSERT, FACTURA DET.
    private boolean registrarArrayDetalleLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, FACTURA DET.

    //////INSERT, FACTURA CAB. CHEQUE
    private boolean realizarFactChequeLocal(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabCheque', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. CHEQUE

    //////INSERT, FACTURA CAB. TARJETA
    private JSONArray registrarCabTarjetaLocal(JSONArray json) {
        JSONArray estados = new JSONArray();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONArray) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. TARJETA

    //////INSERT, FACTURA CAB. FUNCIONARIO
    private JSONObject registrarCabFuncionarioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabFuncionario', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. FUNCIONARIO

    //////INSERT, FACTURA CAB. TARJETA CONVENIO
    private JSONObject registrarTarjetaConvenioLocal(JSONObject json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabTarjConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json.toString());
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }

    //////INSERT, FACTURA CAB. TARJETA CONVENIO
    //////INSERT, FACTURA CAB. CLIENTE FIEL
    private JSONObject registrarTarjetaFielLocal(JSONObject json) {
        ConexionPostgres.conectar();
        JSONObject obj = new JSONObject();
        JSONParser parser = new JSONParser();
        String sql = "INSERT INTO desarrollo.cabecera_fiel(fecha, descripcion_dato, operacion) VALUES (now(),'" + json.toString() + "', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                obj = (JSONObject) parser.parse(json.toString());
            } else {
                obj = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return obj;
    }
    //////INSERT, FACTURA CAB. CLIENTE FIEL

    //////INSERT, FACTURA CAB. RETENCIÓN
    private boolean registrarRetencion(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabRetencion', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. RETENCIÓN

    //////INSERT, FACTURA CAB. NOTA CRÉDITO
    private boolean registrarNotaCreditoLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(), '" + json + "','facturaClienteCabNotaCredito', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        return estados;
    }
    //////INSERT, FACTURA CAB. NOTA CRÉDITO

    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA
    private boolean registrarFacturaMonedaExtranjera(JSONArray json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabMonedaExtranjera', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, FACTURA CAB. MONEDA EXTRANJERA

    //////INSERT, FACTURA CAB. EFECTIVO
    private Map registrarFacturaEfectivoLocal(JSONObject json) {
        Map mapeo = new HashMap();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json.toString() + "','facturaClienteCabEfectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                mapeo.put("finalizo", true);
                mapeo.put("cabFacturaEfectivo", json);
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return mapeo;
    }
    //////INSERT, FACTURA CAB. EFECTIVO

    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.
    private JSONObject registrarPromoTemp(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPromoTemp', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json);
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP.

    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP. ART.
    private JSONObject registrarPromoTempArt(String json) {
        JSONObject estados = new JSONObject();
        JSONParser parser = new JSONParser();
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPromoTempArt', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = (JSONObject) parser.parse(json);
            } else {
                estados = null;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - FACTURA CLIENTE CAB. PROMO TEMP. ART.

    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO
    private boolean registrarDescDirectivo(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descuentoDirectivo', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO DIRECTIVO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO
    private boolean registrarConvenioLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaConvenio', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA CONVENIO

    //////INSERT, PENDIENTES - DESCUENTO TARJETA
    private boolean registrarDescTarjetaLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjeta', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO TARJETA

    //////INSERT, PENDIENTES - DESCUENTO VALE
    private boolean registrarDescValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO VALE

    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL
    private boolean registrarDescTarjFielLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descTarjetaFiel', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO CLIENTE FIEL

    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO
    private boolean registrarDescFuncionario(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','descFuncionario', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - DESCUENTO FUNCIONARIO

    //////INSERT, PENDIENTES - PORC. VALE
    private boolean registrarPorcValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabPorcVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - PORC. VALE

    //////INSERT, PENDIENTES - VALE
    private boolean registrarValeLocal(String json) {
        boolean estados = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha, descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + json + "','facturaClienteCabVale', 'insertar');";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* Haz agregado un nuevo registro a POSTGRES BD LOCAL ********");
                estados = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return estados;
    }
    //////INSERT, PENDIENTES - VALE
    //DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS DESCUENTOS ########
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->
    //JSON FACTURA CABECERA
    private void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            FacturaDeVentaFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            FacturaDeVentaFXMLController.getCabFactura().put("cliente", ClienteFXMLController.getJsonCliente());//en nulo default NN
            int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotalAPagar.getText()));
            FacturaDeVentaFXMLController.getCabFactura().put("montoFactura", monto);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //JSON FACTURA CABECERA

    //JSON FACTURA DETALLE
    private JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < FacturaDeVentaFXMLController.getDetalleArtList().size(); i++) {
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            if (!hashDetallePrecioDesc.isEmpty()) {//algún descuento por sección, fiel; funcionario; temporada...
            } else if (descTarjeta || descTarjetaConvenio || descDirectivo) {//vale por monto y vale porcentaje no entran...
            }
            //ACTUALIZACIÓN DETALLE / DESCUENTO
            FacturaDeVentaFXMLController.getDetalleArtList().get(i).put("facturaClienteCab", factCab);
            if (FacturaDeVentaFXMLController.getDetalleArtList().get(i).containsKey("peso")) {
                FacturaDeVentaFXMLController.getDetalleArtList().get(i).remove("peso");
            }
            jsonArrayFactDet.add(FacturaDeVentaFXMLController.getDetalleArtList().get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }
    //JSON FACTURA DETALLE

    //JSON TIPO PAGO EFECTIVO
    private JSONObject creandoJsonEfectivo(JSONObject factCab) {
        JSONObject jsonEfectivo = new JSONObject();
        //**********************************************************************
        jsonEfectivo.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldEfectivo.getText()));
        jsonEfectivo.put("montoEfectivo", monto);
        //**********************************************************************
        return jsonEfectivo;
    }
    //JSON TIPO PAGO EFECTIVO

    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA
    private JSONArray creandoJsonMonedaEx(JSONObject factCab) {
        JSONArray jsonArrayMonedaExt = new JSONArray();
        JSONObject jsonDolar = new JSONObject();
        JSONObject jsonReal = new JSONObject();
        JSONObject jsonPeso = new JSONObject();
        //**********************************************************************
        //List tipo_moneda ordenado por descripción, getCotizacionList
        if (!textFieldDolarGs.getText().contentEquals("")) {
            jsonDolar.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldDolarGs.getText()));
            int montoDolar = Integer.valueOf(numValidator.numberValidator(textFieldDolarCant.getText()));
            jsonDolar.put("montoEfectivo", montoDolar);
            jsonDolar.put("monto_guaranies", montoAGs);
            if (FacturaDeVentaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaDeVentaFXMLController.getCotizacionList().get(0).get("venta"))).intValue();
                jsonDolar.put("cotizacion", cotiz);
                jsonDolar.put("tipoMoneda", FacturaDeVentaFXMLController.getCotizacionList().get(0));
                jsonArrayMonedaExt.add(jsonDolar);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVentaFXMLController.setDolar(textFieldDolarCant.getText() + " x " + cot + " = " + textFieldDolarGs.getText() + "\n");
                //para impresión
            }
        }
        if (!textFieldRealGs.getText().contentEquals("")) {
            jsonReal.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldRealGs.getText()));
            int montoReal = Integer.valueOf(numValidator.numberValidator(textFieldRealCant.getText()));
            jsonReal.put("montoEfectivo", montoReal);
            jsonReal.put("monto_guaranies", montoAGs);
            if (FacturaDeVentaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaDeVentaFXMLController.getCotizacionList().get(3).get("venta"))).intValue();
                jsonReal.put("cotizacion", cotiz);
                jsonReal.put("tipoMoneda", FacturaDeVentaFXMLController.getCotizacionList().get(3));
                jsonArrayMonedaExt.add(jsonReal);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVentaFXMLController.setReal(textFieldRealCant.getText() + " x " + cot + " = " + textFieldRealGs.getText() + "\n");
                //para impresión
            }
        }
        if (!textFieldPesoGs.getText().contentEquals("")) {
            jsonPeso.put("facturaClienteCab", factCab);
            int montoAGs = Integer.valueOf(numValidator.numberValidator(textFieldPesoGs.getText()));
            int montoReal = Integer.valueOf(numValidator.numberValidator(textFieldPesoCant.getText()));
            jsonPeso.put("montoEfectivo", montoReal);
            jsonPeso.put("monto_guaranies", montoAGs);
            if (FacturaDeVentaFXMLController.getCotizacionList() != null) {
                int cotiz = Double.valueOf(String.valueOf(FacturaDeVentaFXMLController.getCotizacionList().get(2).get("venta"))).intValue();
                jsonPeso.put("cotizacion", cotiz);
                jsonPeso.put("tipoMoneda", FacturaDeVentaFXMLController.getCotizacionList().get(2));
                jsonArrayMonedaExt.add(jsonPeso);
                //para impresión
                String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(cotiz)));
                MensajeFinalVentaFXMLController.setPeso(textFieldPesoCant.getText() + " x " + cot + " = " + textFieldPesoGs.getText() + "\n");
                //para impresión
            }
        }
        //**********************************************************************
        return jsonArrayMonedaExt;
    }
    //JSON TIPO PAGO EFECTIVO MONEDA EXTRANJERA

    //JSON TIPO PAGO CHEQUE
    private JSONArray creandoJsonCheque(JSONObject factCab) {
        JSONArray jsonArrayCheque = new JSONArray();
        int montoChequeTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewCheques.getItems().size(); i++) {
            JSONObject jsonCheque = new JSONObject();
            String[] parts = listViewCheques.getItems().get(i).split(" - ");
            jsonCheque.put("facturaClienteCabDTO", factCab);
            jsonCheque.put("descripcion", parts[0]);
            jsonCheque.put("nroCheque", parts[1]);
            int monto = Integer.valueOf(numValidator.numberValidator(parts[2]));
            jsonCheque.put("montoCheque", monto);
            //para impresión
            montoChequeTotal = montoChequeTotal + monto;
            //para impresión
            jsonArrayCheque.add(jsonCheque);
        }
        //para impresión
        MensajeFinalVentaFXMLController.setCheque(montoChequeTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayCheque;
    }
    //JSON TIPO PAGO CHEQUE

    //JSON TIPO PAGO TARJETA CONVENIO
    private JSONObject creandoJsonTarjetaConvenio(JSONObject factCab) {//queda abierta la posibilidad de varias tarjetas convenio...
        JSONObject jsonTarjConv = new JSONObject();
        //**********************************************************************
        String parts = listViewTarjetasConvAgregadas.getItems().get(0);
        jsonTarjConv.put("facturaClienteCab", factCab);
        jsonTarjConv.put("tarjetaConvenio", hashJsonComboTarjConvenio.get(parts));
        jsonTarjConv.put("descripcionTarj", parts);
        //todas formas de pago...
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        //todas formas de pago...
        jsonTarjConv.put("monto", monto);
        //**********************************************************************
        return jsonTarjConv;
    }
    //JSON TIPO PAGO TARJETA CONVENIO

    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO
    private JSONArray creandoJsonTarjeta(JSONObject factCab) {
        JSONArray jsonArrayTarjeta = new JSONArray();
        int montoTarjCredTotal = 0;
        int montoTarjDebTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewTarjetasAgregadas.getItems().size(); i++) {
            JSONObject jsonTarjeta = new JSONObject();
            //debe tener descuento
            if (listViewTarjetasAgregadas.getItems().get(i).contains(" || ")) {
                String[] partUno = listViewTarjetasAgregadas.getItems().get(i).split(" \\|\\| ");
                String[] partsDos = partUno[1].split(" - ");
                JSONObject tarj = hashJsonComboTarjeta.get(partsDos[0]);
                JSONObject tarjTipo = (JSONObject) tarj.get("tipoTarjeta");
                jsonTarjeta.put("facturaClienteCab", factCab);
                jsonTarjeta.put("tarjeta", tarj);
                jsonTarjeta.put("descripcionTarj", partsDos[0]);
                jsonTarjeta.put("codAutorizacion", partsDos[2]);
                int monto = Integer.valueOf(numValidator.numberValidator(partsDos[1]));
                jsonTarjeta.put("monto", monto);
                if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                    montoTarjCredTotal = montoTarjCredTotal + monto;
                    MensajeFinalVentaFXMLController.setTarjCred(montoTarjCredTotal);
                } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                    montoTarjDebTotal = montoTarjDebTotal + monto;
                    MensajeFinalVentaFXMLController.setTarjDeb(monto);
                }
            } else {
                String[] parts = listViewTarjetasAgregadas.getItems().get(i).split(" - ");
                JSONObject tarj = hashJsonComboTarjeta.get(parts[0]);
                JSONObject tarjTipo = (JSONObject) tarj.get("tipoTarjeta");
                jsonTarjeta.put("facturaClienteCab", factCab);
                jsonTarjeta.put("tarjeta", tarj);
                jsonTarjeta.put("descripcionTarj", parts[0]);
                jsonTarjeta.put("codAutorizacion", parts[2]);
                int monto = Integer.valueOf(numValidator.numberValidator(parts[1]));
                jsonTarjeta.put("monto", monto);
                if (tarjTipo.get("descripcion").toString().contentEquals("CREDITO")) {
                    montoTarjCredTotal = montoTarjCredTotal + monto;
                    MensajeFinalVentaFXMLController.setTarjCred(montoTarjCredTotal);
                } else if (tarjTipo.get("descripcion").toString().contentEquals("DEBITO")) {
                    montoTarjDebTotal = montoTarjDebTotal + monto;
                    MensajeFinalVentaFXMLController.setTarjDeb(monto);
                }
            }
            jsonArrayTarjeta.add(jsonTarjeta);
        }
        //para impresión
        MensajeFinalVentaFXMLController.setTarjCred(montoTarjCredTotal);
        MensajeFinalVentaFXMLController.setTarjDeb(montoTarjDebTotal);
        //para impresión        
        //**********************************************************************
        return jsonArrayTarjeta;
    }
    //JSON TIPO PAGO TARJETA CRÉDITO / DÉBITO

    //JSON TIPO PAGO CLIENTE FIEL
    private JSONObject creandoJsonTarjClienteFiel(JSONObject factCab) {
        JSONObject jsonClienteFiel = new JSONObject();
        //**********************************************************************
        JSONObject factura = new JSONObject();
        factura.put("idFacturaClienteCab", Long.parseLong(factCab.get("idFacturaClienteCab").toString()));
        jsonClienteFiel.put("facturaClienteCab", factura);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonClienteFiel.put("monto", monto);
        JSONObject jsonClienteF = ClienteFielFXMLController.getJsonClienteFiel();
        jsonClienteF.remove("fechaAlta");
        jsonClienteF.remove("fechaMod");
        jsonClienteFiel.put("tarjetaClienteFiel", jsonClienteF);
        //**********************************************************************
        return jsonClienteFiel;
    }
    //JSON TIPO PAGO CLIENTE FIEL

    //JSON TIPO PAGO FUNCIONARIO
    private JSONObject creandoJsonFuncionario(JSONObject factCab) {
        JSONObject jsonFuncionario = new JSONObject();
        //**********************************************************************
        jsonFuncionario.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonFuncionario.put("monto", monto);
        jsonFuncionario.put("funcionario", FuncionarioFXMLController.getJsonFuncionario());
        //**********************************************************************
        return jsonFuncionario;
    }
    //JSON TIPO PAGO FUNCIONARIO

    //JSON TIPO PAGO VALE PORCENTAJE
    private JSONObject creandoJsonValePorc(JSONObject factCab) {
        JSONObject jsonValePorc = new JSONObject();
        //**********************************************************************
        jsonValePorc.put("facturaClienteCab", factCab);
        jsonValePorc.put("vale", hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
        int porcVale = Integer.valueOf(numValidator.numberValidator(textFieldMontoValePorc.getText()));
        jsonValePorc.put("porcVale", porcVale);
        Long precioTotal = FacturaDeVentaFXMLController.getPrecioTotal();
        Long descTotal = (precioTotal * porcVale) / 100;
        jsonValePorc.put("monto", descTotal);
        MensajeFinalVentaFXMLController.setVale(toIntExact((long) descTotal));
//        int nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        jsonValePorc.put("nroVale", textFieldNroVale.getText());
        jsonValePorc.put("ci", textFieldValeCI.getText());
        //**********************************************************************
        return jsonValePorc;
    }
    //JSON TIPO PAGO VALE PORCENTAJE

    //JSON TIPO PAGO VALE MONTO
    private JSONObject creandoJsonValeMonto(JSONObject factCab) {
        JSONObject jsonValeMonto = new JSONObject();
        //**********************************************************************
        jsonValeMonto.put("facturaClienteCab", factCab);
        jsonValeMonto.put("vale", hashJsonComboVale.get(comboBoxVale.getSelectionModel().getSelectedItem()));
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldMontoVale.getText()));
        jsonValeMonto.put("monto", monto);
        //para impresión
        if (comboBoxVale.getSelectionModel().getSelectedItem().contentEquals("ASOCIACION")) {
            MensajeFinalVentaFXMLController.setAsoc(monto);
        } else {
            MensajeFinalVentaFXMLController.setVale(monto);
        }
        //para impresión
        /*Integer nroVale = null;
        if (!textFieldNroVale.getText().contentEquals("")) {
            nroVale = Integer.valueOf(numValidator.numberValidator(textFieldNroVale.getText()));
        }
        jsonValeMonto.put("nroVale", nroVale);*/
        jsonValeMonto.put("nroVale", textFieldNroVale.getText());
        jsonValeMonto.put("ci", textFieldValeCI.getText());
        //**********************************************************************
        return jsonValeMonto;
    }
    //JSON TIPO PAGO VALE MONTO

    //JSON TIPO PAGO NOTA DE CRÉDITO
    private JSONArray creandoJsonNotaCred(JSONObject factCab) {
        JSONArray jsonArrayNota = new JSONArray();
        int montoNotaTotal = 0;
        //**********************************************************************
        for (int i = 0; i < listViewNotasCred.getItems().size(); i++) {
            JSONObject jsonNota = new JSONObject();
            String[] parts = listViewNotasCred.getItems().get(i).split(" - ");
            jsonNota.put("facturaClienteCabDTO", factCab);
            jsonNota.put("nroNota", parts[0]);
            int monto = Integer.valueOf(numValidator.numberValidator(parts[1]));
            jsonNota.put("monto", monto);
            //para impresión
            montoNotaTotal = montoNotaTotal + monto;
            //para impresión
            jsonArrayNota.add(jsonNota);
        }
        //para impresión
        MensajeFinalVentaFXMLController.setNotaCred(montoNotaTotal);
        //para impresión
        //**********************************************************************
        return jsonArrayNota;

    }
    //JSON TIPO PAGO NOTA DE CRÉDITO

    //JSON TIPO PAGO MONTO RETENCIÓN
    private JSONObject creandoJsonMontoRetencion(JSONObject factCab) {
        JSONObject jsonMontoRetencion = new JSONObject();
        //**********************************************************************
        jsonMontoRetencion.put("facturaClienteCab", factCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldMontoRetencion.getText()));
        jsonMontoRetencion.put("monto", monto);
        //**********************************************************************
        return jsonMontoRetencion;
    }
    //JSON TIPO PAGO MONTO RETENCIÓN

    //JSON DESCUENTO TARJETA FUNCIONARIO
    private JSONObject creandoJsonDescFuncionario(JSONObject factCabFuncionario) {
        JSONObject jsonDescFuncionario = new JSONObject();
        //**********************************************************************
        jsonDescFuncionario.put("facturaClienteCabFuncionarioDTO", factCabFuncionario);
        int montoDesc = toIntExact(Math.round(descuentoFuncSum));
        jsonDescFuncionario.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        long redondeo = Math.round(porcentajeDesc * 100.0);
        porcentajeDesc = (double) (redondeo / 100.0);
        jsonDescFuncionario.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescFuncionario;
    }
    //JSON DESCUENTO TARJETA FUNCIONARIO

    //JSON DESCUENTO TARJETA CLIENTE FIEL
    private JSONObject creandoJsonDescTarjFiel(JSONObject factCabTarjFiel) {
        JSONObject jsonDescTarjFiel = new JSONObject();
        //**********************************************************************
        jsonDescTarjFiel.put("facturaClienteCabTarjFiel", factCabTarjFiel);
        int montoDesc = toIntExact(Math.round(descuentoFielSum));
        jsonDescTarjFiel.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjFiel.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjFiel;
    }
    //JSON DESCUENTO TARJETA CLIENTE FIEL

    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO
    private JSONObject creandoJsonDescTarj(JSONObject factCabTarj) {//solo UNA TARJETA...
        JSONObject jsonDescTarj = new JSONObject();
        //**********************************************************************
        jsonDescTarj.put("facturaClienteCabTarjeta", factCabTarj);
        String[] arraySplit = listViewTarjetasAgregadas.getItems().get(0).split(" - ");
        JSONObject jsonDescuentoTarjetaCab = new JSONObject();
        jsonDescuentoTarjetaCab.put("idDescuentoTarjetaCab",
                Long.valueOf(Descuento.getHashDescTarjeta().get(arraySplit[0]).get("idDescuentoTarjetaCab").toString()));
        jsonDescTarj.put("descuentoTarjetaCab", jsonDescuentoTarjetaCab);
        int montoDesc = toIntExact(Math.round(descuentoTarjetaSum));
        jsonDescTarj.put("montoDesc", montoDesc);
        double porcentajeDesc = (double) (montoDesc * 100.0) / Double.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarj.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarj;
    }
    //JSON DESCUENTO TARJETA CRÉDITO / DÉBITO

    //JSON DESCUENTO TARJETA CONVENIO
    private JSONObject creandoJsonDescTarjConv(JSONObject factCabTarjConv) {
        JSONObject jsonDescTarjConv = new JSONObject();
        //**********************************************************************
        jsonDescTarjConv.put("facturaClienteCabTarjConvenio", factCabTarjConv);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescTarjConv.put("montoDesc", monto);
        int porcentajeDesc = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescTarjConv.put("porcentajeDesc", porcentajeDesc);
        //**********************************************************************
        return jsonDescTarjConv;
    }
    //JSON DESCUENTO TARJETA CONVENIO

    //JSON DESCUENTO VALE
    private JSONObject creandoJsonDescVale(JSONObject facturaClienteCab, JSONObject vale) {
        JSONObject jsonDescVale = new JSONObject();
        //**********************************************************************
        jsonDescVale.put("facturaClienteCab", facturaClienteCab);
        jsonDescVale.put("vale", vale);
        int monto = 0;
        if (descValePorc) {
            monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        } else if (Long.valueOf(vale.get("idVale").toString()) == 6) {
            monto = toIntExact(Math.round(descuentoValeParesaSum));
        }
        jsonDescVale.put("montoDesc", monto);
        //**********************************************************************
        return jsonDescVale;
    }
    //JSON DESCUENTO VALE

    //JSON DESCUENTO PROMOCIÓN TEMPORADA
    private JSONObject creandoJsonDescPromoTemp(JSONObject facturaClienteCab, JSONObject promoTemporadaDesc, int monto) {
        JSONObject jsonDescPromo = new JSONObject();
        //**********************************************************************
        jsonDescPromo.put("facturaClienteCab", facturaClienteCab);
        jsonDescPromo.put("promoTemporada", promoTemporadaDesc);
        jsonDescPromo.put("descripcionTemporada", promoTemporadaDesc.get("descripcionTemporada"));
        jsonDescPromo.put("monto", monto);
        //**********************************************************************
        return jsonDescPromo;
    }
    //JSON DESCUENTO PROMOCIÓN TEMPORADA

    //JSON DESCUENTO PROMOCIÓN TEMPORADA ART.
    private JSONObject creandoJsonDescPromoTempArt(JSONObject facturaClienteCab, JSONObject promoTemporadaArtDesc, int monto) {
        JSONObject jsonDescPromo = new JSONObject();
        //**********************************************************************
        jsonDescPromo.put("facturaClienteCab", facturaClienteCab);
        jsonDescPromo.put("promoTemporadaArt", promoTemporadaArtDesc);
        jsonDescPromo.put("descripcionTemporada", promoTemporadaArtDesc.get("descripcionTemporadaArt"));
        jsonDescPromo.put("monto", monto);
        //**********************************************************************
        return jsonDescPromo;
    }
    //JSON DESCUENTO PROMOCIÓN TEMPORADA ART.

    //JSON DESCUENTO DIRECTIVO
    private JSONObject creandoJsonDescDirectivo(JSONObject facturaClienteCab) {
        JSONObject jsonDescDirectivo = new JSONObject();
        //**********************************************************************
        jsonDescDirectivo.put("facturaClienteCab", facturaClienteCab);
        int monto = Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText()));
        jsonDescDirectivo.put("montoDesc", monto);
        long porcDirectivo = (Integer.valueOf(numValidator.numberValidator(textFieldDescuento.getText())) * 100) / Integer.valueOf(numValidator.numberValidator(textFieldTotal.getText()));
        jsonDescDirectivo.put("porcDesc", porcDirectivo);
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        jsonDescDirectivo.put("ciDir", "");
        jsonDescDirectivo.put("nombreDir", "");
        jsonDescDirectivo.put("motivoDesc", "");
        //*********** POSIBILIDAD DE REGISTRAR MÁS DATOS ADELANTE...
        //**********************************************************************
        return jsonDescDirectivo;
    }
    //JSON DESCUENTO DIRECTIVO
    //JSON JSON JSON JSON ****************************** -> -> -> -> -> -> -> ->

    //BAR CHART
    private void editandoBarChart() {
        /*if ((descuentoBarChart != descuento) || cargandoInicial) {
            if (!barChart.getData().isEmpty()) {
                barChart.getData().removeAll(series);
            }
            series = new XYChart.Series<>();
            Data data1 = new XYChart.Data("BRUTO", this.total);
            series.getData().add(data1);
            Data data2 = new XYChart.Data("DTO.", this.descuento);
            series.getData().add(data2);
            Data data3 = new XYChart.Data("NETO", this.total - this.descuento);
            series.getData().add(data3);
            barChart.getData().addAll(series);
            if (cargandoInicial) {
                barChart.setStyle("-fx-font: normal bold 16 System;");
            }
            if (this.descuento > 0) {
                double porc = (this.descuento * 100) / this.total;
                jfxTextFieldPorc.setText(porc + " % ahorro");
            } else {
                jfxTextFieldPorc.setText("");
            }
            descuentoBarChart = descuento;
        }*/
    }
    //BAR CHART

    //FILL TRANSITION
    /*private void focusTransition() {
        fillTransition();
        //panelTipoDePago
        textFieldEfectivo.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            JFXFillTransitionEfectivo.playFromStart();
            JFXFillTransitionTarjConv.stop();
            JFXFillTransitionNotaCred.stop();
            JFXFillTransitionCheque.stop();
            JFXFillTransitionTarjCredDeb.stop();
            JFXFillTransitionMonExtr.stop();
            JFXFillTransitionVale.stop();
            focusMonExtr = false;
            focusCheque = false;
            focusNotaCred = false;
            focusTarjCredDeb = false;
            focusTarjConv = false;
            focusVale = false;
            panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
            panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
            panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
            panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
            panelVales.setStyle("-fx-background-color: #D3D3D3;");
        });
        //panelTipoDePagoExtranjera
        textFieldDolarCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusMonExtr) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.playFromStart();
                JFXFillTransitionVale.stop();
                focusMonExtr = true;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldRealCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusMonExtr) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.playFromStart();
                JFXFillTransitionVale.stop();
                focusMonExtr = true;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldPesoCant.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusMonExtr) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.playFromStart();
                JFXFillTransitionVale.stop();
                focusMonExtr = true;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        //panelBusquedaTipoTarj
        comboBoxTarjetas.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjCredDeb) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.playFromStart();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = true;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldCodAuth.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjCredDeb) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.playFromStart();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = true;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldMontoTarjeta.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjCredDeb) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.playFromStart();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = true;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        listViewTarjetasAgregadas.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjCredDeb) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.playFromStart();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = true;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        //panelBusquedaCheque
        textFieldEntidad.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusCheque) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.playFromStart();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = true;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFIeldNroCheque.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusCheque) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.playFromStart();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = true;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldMonto.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusCheque) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.playFromStart();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = true;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        listViewCheques.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusCheque) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.playFromStart();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = true;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        //panelNotaCredito
        textFieldNroNotaCred.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusNotaCred) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.playFromStart();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = true;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldMontoNotaCred.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusNotaCred) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.playFromStart();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = true;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        listViewNotasCred.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusNotaCred) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.stop();
                JFXFillTransitionNotaCred.playFromStart();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = true;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarjConv.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        //panelBusquedaTipoTarjConv
        comboBoxTarjetasConvenio.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjConv) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.playFromStart();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = true;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        listViewTarjetasConvAgregadas.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusTarjConv) {
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionTarjConv.playFromStart();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionVale.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = true;
                focusVale = false;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
                panelVales.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        //panelVales
        comboBoxVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusVale) {
                JFXFillTransitionVale.playFromStart();
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionTarjConv.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = true;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldNroVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusVale) {
                JFXFillTransitionVale.playFromStart();
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionTarjConv.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = true;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldValeCI.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusVale) {
                JFXFillTransitionVale.playFromStart();
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionTarjConv.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = true;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldMontoVale.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusVale) {
                JFXFillTransitionVale.playFromStart();
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionTarjConv.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = true;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
        textFieldMontoValePorc.focusedProperty().addListener((ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) -> {
            if (!focusVale) {
                JFXFillTransitionVale.playFromStart();
                JFXFillTransitionEfectivo.stop();
                JFXFillTransitionNotaCred.stop();
                JFXFillTransitionCheque.stop();
                JFXFillTransitionTarjCredDeb.stop();
                JFXFillTransitionMonExtr.stop();
                JFXFillTransitionTarjConv.stop();
                focusMonExtr = false;
                focusCheque = false;
                focusNotaCred = false;
                focusTarjCredDeb = false;
                focusTarjConv = false;
                focusVale = true;
                panelTipoDePago.setStyle("-fx-background-color: #D3D3D3;");
                panelTipoDePagoExtranjera.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaTipoTarj.setStyle("-fx-background-color: #D3D3D3;");
                panelBusquedaCheque.setStyle("-fx-background-color: #D3D3D3;");
                panelNotaCredito.setStyle("-fx-background-color: #D3D3D3;");
            }
        });
    }*/

 /*private void limpiandoFillTransition() {
        JFXFillTransitionEfectivo.stop();
        JFXFillTransitionTarjConv.stop();
        JFXFillTransitionNotaCred.stop();
        JFXFillTransitionCheque.stop();
        JFXFillTransitionTarjCredDeb.stop();
        JFXFillTransitionMonExtr.stop();
        JFXFillTransitionVale.stop();
        JFXFillTransitionEfectivo = null;
        JFXFillTransitionTarjConv = null;
        JFXFillTransitionNotaCred = null;
        JFXFillTransitionCheque = null;
        JFXFillTransitionTarjCredDeb = null;
        JFXFillTransitionMonExtr = null;
        JFXFillTransitionVale = null;
    }*/

 /*private void fillTransition() {
        JFXFillTransitionEfectivo = new JFXFillTransition();
        JFXFillTransitionEfectivo.setAutoReverse(true);
        JFXFillTransitionEfectivo.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionEfectivo.setDuration(Duration.millis(1000));
        JFXFillTransitionEfectivo.setRegion(panelTipoDePago);
        JFXFillTransitionEfectivo.setFromValue(Color.LIGHTGRAY);
        JFXFillTransitionEfectivo.setToValue(Color.DARKGRAY);
        JFXFillTransitionMonExtr = new JFXFillTransition();
        JFXFillTransitionMonExtr.setAutoReverse(true);
        JFXFillTransitionMonExtr.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionMonExtr.setDuration(Duration.millis(1000));
        JFXFillTransitionMonExtr.setRegion(panelTipoDePagoExtranjera);
        JFXFillTransitionMonExtr.setFromValue(Color.LIGHTGREY);
        JFXFillTransitionMonExtr.setToValue(Color.DARKGRAY);
        JFXFillTransitionTarjCredDeb = new JFXFillTransition();
        JFXFillTransitionTarjCredDeb.setAutoReverse(true);
        JFXFillTransitionTarjCredDeb.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionTarjCredDeb.setDuration(Duration.millis(1000));
        JFXFillTransitionTarjCredDeb.setRegion(panelBusquedaTipoTarj);
        JFXFillTransitionTarjCredDeb.setFromValue(Color.LIGHTGREY);
        JFXFillTransitionTarjCredDeb.setToValue(Color.DARKGRAY);
        JFXFillTransitionCheque = new JFXFillTransition();
        JFXFillTransitionCheque.setAutoReverse(true);
        JFXFillTransitionCheque.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionCheque.setDuration(Duration.millis(1000));
        JFXFillTransitionCheque.setRegion(panelBusquedaCheque);
        JFXFillTransitionCheque.setFromValue(Color.LIGHTGREY);
        JFXFillTransitionCheque.setToValue(Color.DARKGRAY);
        JFXFillTransitionNotaCred = new JFXFillTransition();
        JFXFillTransitionNotaCred.setAutoReverse(true);
        JFXFillTransitionNotaCred.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionNotaCred.setDuration(Duration.millis(1000));
        JFXFillTransitionNotaCred.setRegion(panelNotaCredito);
        JFXFillTransitionNotaCred.setFromValue(Color.LIGHTGREY);
        JFXFillTransitionNotaCred.setToValue(Color.DARKGRAY);
        JFXFillTransitionTarjConv = new JFXFillTransition();
        JFXFillTransitionTarjConv.setAutoReverse(true);
        JFXFillTransitionTarjConv.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionTarjConv.setDuration(Duration.millis(1000));
        JFXFillTransitionTarjConv.setRegion(panelBusquedaTipoTarjConv);
        JFXFillTransitionTarjConv.setFromValue(Color.LIGHTGREY);
        JFXFillTransitionTarjConv.setToValue(Color.DARKGRAY);
        JFXFillTransitionVale = new JFXFillTransition();
        JFXFillTransitionVale.setAutoReverse(true);
        JFXFillTransitionVale.setCycleCount(Animation.INDEFINITE);
        JFXFillTransitionVale.setDuration(Duration.millis(1000));
        JFXFillTransitionVale.setRegion(panelVales);
        JFXFillTransitionVale.setFromValue(Color.LIGHTGRAY);
        JFXFillTransitionVale.setToValue(Color.DARKGRAY);
    }*/
    //FILL TRANSITION
    class MyListCell extends ListCell<String> {

        @Override
        protected void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            if (item == null || empty) {
                setGraphic(null);
            } else {
                if (item.contains(" || ")) {
                    HBox hBox = new HBox(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/tarjeta_desc.jpg"))), new Label(item));
                    hBox.setAlignment(Pos.CENTER_LEFT);
                    hBox.setSpacing(10);
                    setGraphic(hBox);
                } else {
                    if (item.contentEquals("")) {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/arrow-redo-icon.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    } else {
                        HBox hBox = new HBox(new ImageView(
                                new Image(getClass().getResourceAsStream("/vista/img/tarjeta.png"))), new Label(item));
                        hBox.setAlignment(Pos.CENTER_LEFT);
                        hBox.setSpacing(10);
                        setGraphic(hBox);
                    }
                }
            }
            setText("");
        }
    }

    public class AutoCompleteComboBoxListener<T> implements EventHandler<KeyEvent> {

        private ComboBox comboBox;
        private StringBuilder sb;
        private ObservableList<T> data;
        private boolean moveCaretToPos = false;
        private int caretPos;

        public AutoCompleteComboBoxListener(ComboBox comboBox) {
            this.comboBox = new ComboBox<>();
            this.comboBox = comboBox;
            this.comboBox.setVisibleRowCount(10);
            sb = new StringBuilder();
            data = comboBox.getItems();
            this.comboBox.setEditable(true);
            this.comboBox.setOnKeyPressed(new EventHandler<KeyEvent>() {
                @Override
                public void handle(KeyEvent t) {
                    comboBox.hide();
                }
            });
            this.comboBox.setOnKeyReleased(AutoCompleteComboBoxListener.this);
        }

        @Override
        @SuppressWarnings("element-type-mismatch")
        public void handle(KeyEvent event) {
            if (event.getCode() == KeyCode.UP) {
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.DOWN) {
                if (!comboBox.isShowing()) {
                    comboBox.show();
                }
                caretPos = -1;
                moveCaret(comboBox.getEditor().getText().length());
                return;
            } else if (event.getCode() == KeyCode.BACK_SPACE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            } else if (event.getCode() == KeyCode.DELETE) {
                moveCaretToPos = true;
                caretPos = comboBox.getEditor().getCaretPosition();
            }
            if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT
                    || event.isControlDown() || event.getCode() == KeyCode.HOME
                    || event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB
                    || event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.ESCAPE) {
                if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
                    if (hashJsonComboTarjeta.containsKey(comboBox.getEditor().getText())) {
                        textFieldMontoTarjeta.requestFocus();
                    } else if (!comboBox.getEditor().getText().contentEquals("")) {
                        mensajeAdv("NO SE RECONOCE LA TARJETA " + comboBox.getEditor().getText());
                    } else {
                        textFieldMontoTarjeta.requestFocus();
                    }
                }
                return;
            }
            ObservableList list = FXCollections.observableArrayList();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).toString().toLowerCase().startsWith(
                        AutoCompleteComboBoxListener.this.comboBox
                                .getEditor().getText().toLowerCase())) {
                    list.add(data.get(i));
                }
            }
            String t = comboBox.getEditor().getText();
            comboBox.setItems(list);
            if (list.size() < 10) {
                this.comboBox.setVisibleRowCount(list.size());
            } else {
                this.comboBox.setVisibleRowCount(10);
            }
            comboBox.getEditor().setText(t);
            if (!moveCaretToPos) {
                caretPos = -1;
            }
            moveCaret(t.length());
            if (!list.isEmpty()) {
                comboBox.show();
            }
        }

        private void moveCaret(int textLength) {
            if (caretPos == -1) {
                comboBox.getEditor().positionCaret(textLength);
            } else {
                comboBox.getEditor().positionCaret(caretPos);
            }
            moveCaretToPos = false;
        }

    }

    public static boolean isDescTarjetaFiel() {
        return descTarjetaFiel;
    }

    public static boolean isDescFuncionario() {
        return descFuncionario;
    }

    public static boolean isDescPromoTemp() {
        return descPromoTemp;
    }

    public static double getMontoConDes5() {
        return montoConDes5;
    }

    public static long getExenta() {
        return exenta;
    }

    public static double getMontoConDes10() {
        return montoConDes10;
    }

    static void totales(String totales) {
        datoTotal = totales;
    }

}
