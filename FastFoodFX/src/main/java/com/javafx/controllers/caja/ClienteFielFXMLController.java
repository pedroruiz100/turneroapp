/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Cliente;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.TarjetaClienteFiel;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.ClienteDTO;
import com.peluqueria.dto.TarjetaClienteFielDTO;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.google.gson.GsonBuilder;
import javafx.application.Platform;
import org.apache.commons.validator.GenericValidator;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ClienteFielFXMLController extends BaseScreenController implements Initializable {

    public static boolean isClienteFielSi() {
        return clienteFielSi;
    }

    public static JSONObject getJsonClienteFiel() {
        return jsonClienteFiel;
    }

    public static void setJsonClienteFiel(JSONObject jsonClienteFiel) {
        ClienteFielFXMLController.jsonClienteFiel = jsonClienteFiel;
    }

    private JSONArray clienteFielJSONArray;
    private ObservableList<JSONObject> clienteFielData;
    private List<JSONObject> clienteFielList;
    private boolean escucha;
    private boolean alert;
    private static boolean clienteFielSi;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;
    //json cliente, necesario en forma de pago..
    private static JSONObject jsonClienteFiel;
    //json cliente, necesario en forma de pago..
    @Autowired
    private ClienteDAO cliDAO;
    private JSONObject clienteFiel;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneClienteFiel;
    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelClienteFielBuscar;
    @FXML
    private GridPane gridPaneClienteNuevo11;
    @FXML
    private Label labelNombreClienteFiel;
    @FXML
    private Label labelRucClienteFiel;
    @FXML
    private TextField textFieldNombreClienteFiel;
    @FXML
    private Button buttonBuscarClienteFiel;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCIPas;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNombre;
    @FXML
    private TextField textFieldCiPasFielBuscar;
    @FXML
    private TextField textFieldNombreFielBuscar;
    @FXML
    private Button buttonAnhadir;
    @FXML
    private Button buttonVolver;
    @FXML
    private TextField textFieldCodClienteFiel;
    @FXML
    private TableView<JSONObject> tableViewClienteFiel;
    @FXML
    private Label labelBorrar;
    @FXML
    private Button buttonBorrar;
    @FXML
    private AnchorPane anchorPaneEditar;
    @FXML
    private Label labelClienteEditar;
    @FXML
    private GridPane gridPaneClienteNuevo1;
    @FXML
    private Label labelNombreClienteNuevo1;
    @FXML
    private Label labelClienteApellidoEdit;
    @FXML
    private Label labelRucClienteNuevo1;
    @FXML
    private TextField txtNuevoRucClienteFiel;
    @FXML
    private TextField txtNuevoNombreClienteFiel;
    @FXML
    private HBox hBoxClienteNuevo1;
    @FXML
    private TextField txtNuevoApellidoClienteFiel;
    @FXML
    private Button btnNuevoNewClienteFiel;

    /**
     * Initializes the controller class.
     */
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();
    ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    ManejoLocal manejoLocal = new ManejoLocal();
    ObjectMapper mapper;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private TextField txtNuevoTelefonoClienteFiel;
    @FXML
    private TextField txtNuevoCelularClienteFiel;
    @FXML
    private Label labelRucClienteNuevo111;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
        buscandoClienteFiel(false);
    }

    @FXML
    private void buttonBuscarClienteFielAction(ActionEvent event) {
        buscandoClienteFiel(true);
    }

    @FXML
    private void buttonAnhadirAction(ActionEvent event) {
        navegandoFormaPago();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteFielKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonBorrarAction(ActionEvent event) {
        borrandoAccion();
    }

    @FXML
    private void btnNuevoNewClienteFielAction(ActionEvent event) {
        buscandoNuevoClienteFiel();
    }

    private void buttonNuevoAction(ActionEvent event) {
        creandoCliente();
    }

    //INICIAL INICIAL INICIAL ***************************************
    private void cargandoInicial() {
        mapper = new ObjectMapper();
        clienteFiel = new JSONObject();
        buttonAnhadir.setDisable(true);
        escucha = false;
        alert = false;
        clienteFielSi = false;
//        btnNuevoBuscarClienteFiel.setDisable(true);
//        btnNuevoNewClienteFiel.setDisable(true);
        if (jsonClienteFiel == null) {
            labelBorrar.setVisible(false);
            buttonBorrar.setVisible(false);
        } else {
            JSONObject jsonCliente = (JSONObject) jsonClienteFiel.get("cliente");
            try {
                labelBorrar.setText("BORRAR SELECCION PREVIA" + " - " + jsonCliente.get("nombre").toString().toUpperCase() + " " + jsonCliente.get("apellido").toString().toUpperCase());
            } catch (Exception e) {
                labelBorrar.setText("BORRAR SELECCION PREVIA" + " - " + jsonCliente.get("nombre").toString().toUpperCase());
            }

        }
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        validando();
        listenerCampos();

        //SETEANDO CLIENTE
        if (ClienteFXMLController.isClienteSi()) {
            if (ClienteFXMLController.getJsonCliente() != null) {
                if (ClienteFXMLController.getJsonCliente().get("ruc") != null) {
                    textFieldCodClienteFiel.setText(ClienteFXMLController.getJsonCliente().get("ruc").toString());
                } else {
                    textFieldCodClienteFiel.setText("");
                }
            }
        }
        //SETEANDO CLIENTE
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        clienteFielSi = true;
        this.sc.loadScreen("/vista/caja/formaPagoFXML.fxml", 1286, 720, "/vista/caja/ClienteFielFXML.fxml", 880, 626, true);
    }

    private void volviendo() {
        clienteFielSi = false;
        this.sc.loadScreen("/vista/caja/formaPagoFXML.fxml", 1286, 720, "/vista/caja/ClienteFielFXML.fxml", 880, 626, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            buscandoClienteFiel(true);
        }
//        if (keyCode == event.getCode().F2) {
//            buscarCliente();
//        }
        if (keyCode == event.getCode().F3) {
            if (txtNuevoNombreClienteFiel.getText().equals("")) {
                mensajeAlerta("El campo Nombre no debe quedar vacío.");
            } else {
                if (!txtNuevoNombreClienteFiel.isDisable()) {
                    creandoCliente();
                }
                nuevoClienteFiel();
            }
        }
        if (keyCode == event.getCode().F4) {
            creandoCliente();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                if (!buttonBorrar.isVisible()) {
                    resetParam();
                }
                navegandoFormaPago();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else if (!buttonAnhadir.isDisable()) {
                navegandoFormaPago();
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (alert) {
                alert = false;
            } else if (buttonBorrar.isVisible()) {
                resetParam();
                navegandoFormaPago();
            }
        }
    }

    private void listenerCampos() {
        txtNuevoRucClienteFiel.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
//                btnNuevoBuscarClienteFiel.setDisable(false);
            } else {
                resetNuevoClienteFiel();
            }
        });
    }

    private void escucha() {
        escucha = true;
        tableViewClienteFiel.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewClienteFiel.getSelectionModel().getSelectedItem() != null) {
                JSONObject cliente = (JSONObject) newSelection.get("cliente");
                if (cliente != null) {
                    textFieldNombreFielBuscar.setText(cliente.get("nombre").toString().toUpperCase());
                } else {
                    textFieldNombreFielBuscar.setText("");
                }
                if (newSelection.get("cipas") != null) {
                    textFieldCiPasFielBuscar.setText(newSelection.get("cipas").toString());
                } else {
                    textFieldCiPasFielBuscar.setText("");
                }
                if (buttonAnhadir.isDisable()) {
                    buttonAnhadir.setDisable(false);
                }
                seteandoParam(newSelection);
            } else {
                textFieldNombreFielBuscar.setText("");
                textFieldCiPasFielBuscar.setText("");
                buttonAnhadir.setDisable(true);
                resetParam();
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void borrandoAccion() {
        resetParam();
        navegandoFormaPago();
    }

    private void buscandoClienteFiel(boolean efeuno) {
        String nom, ape, cipas;
        if (textFieldNombreClienteFiel.getText().contentEquals("")) {
            nom = "null";
        } else {
            nom = textFieldNombreClienteFiel.getText();
        }
        if (textFieldCodClienteFiel.getText().contentEquals("")) {
            cipas = "null";
        } else {
            cipas = textFieldCodClienteFiel.getText();
        }
        if (!txtNuevoNombreClienteFiel.getText().equalsIgnoreCase("")) {
            nom = txtNuevoNombreClienteFiel.getText();
        }
        ape = "null";
        clienteFielList = jsonArrayClienteFiel(nom, ape, cipas);
        if (clienteFielList.isEmpty()) {
            mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            txtNuevoRucClienteFiel.setText(textFieldCodClienteFiel.getText());
            txtNuevoRucClienteFiel.requestFocus();

            txtNuevoNombreClienteFiel.setText("");
            txtNuevoApellidoClienteFiel.setText("");
            txtNuevoTelefonoClienteFiel.setText("");
            txtNuevoCelularClienteFiel.setText("");
            txtNuevoNombreClienteFiel.setDisable(false);
            txtNuevoApellidoClienteFiel.setDisable(false);
            txtNuevoTelefonoClienteFiel.setDisable(false);
            txtNuevoCelularClienteFiel.setDisable(false);
            txtNuevoNombreClienteFiel.requestFocus();

            buscarCliente();
        }
        actualizandoTablaClienteFiel();
        if (!tableViewClienteFiel.getItems().isEmpty() && efeuno) {
            tableViewClienteFiel.requestFocus();
            tableViewClienteFiel.getSelectionModel().select(0);
            tableViewClienteFiel.getFocusModel().focus(0);
        } else if (!tableViewClienteFiel.getItems().isEmpty() && !efeuno) {
            tableViewClienteFiel.getSelectionModel().select(0);
            tableViewClienteFiel.getFocusModel().focus(0);
        }
    }

    private void buscarCliente() {
        String ruc = txtNuevoRucClienteFiel.getText();
        resetNuevoClienteFiel();
        txtNuevoRucClienteFiel.setText(ruc);
        if (ruc.equalsIgnoreCase("")) {
            mensajeAlerta("El campo RUC/CI no debe quedar vacío para poder realizar la búsqueda.");
        } else {
//            buscandoClientePorRuc();
            buscarClientePorRucCi();
//            btnNuevoNewClienteFiel.setDisable(false);
        }
    }

    private void buscandoClientePorRuc() {
        boolean valor = false;
        Cliente cli = cliDAO.listarPorRuc(txtNuevoRucClienteFiel.getText());
        if (cli.getNombre() != null) {
            valor = true;
            JSONObject clien = new JSONObject();
            clien.put("idCliente", cli.getIdCliente());
            String[] datos = txtNuevoRucClienteFiel.getText().split("-");
            clienteFiel.put("cliente", clien);
            clienteFiel.put("cipas", datos[0]);
            txtNuevoNombreClienteFiel.setText(cli.getNombre());
        }
        if (cli.getApellido() != null) {
            txtNuevoApellidoClienteFiel.setText(cli.getApellido());
        }
        if (!valor) {
//            mensajeAlerta("No se encuentra resultado de la búsqueda realizada.");
            txtNuevoNombreClienteFiel.setText("");
            txtNuevoApellidoClienteFiel.setText("");
            txtNuevoNombreClienteFiel.setDisable(false);
            txtNuevoApellidoClienteFiel.setDisable(false);
            txtNuevoTelefonoClienteFiel.requestFocus();
        }
    }

    private void buscarClientePorRucCi() {
        boolean valor = false;
        String rucCliente = "";
        try {
            rucCliente = txtNuevoRucClienteFiel.getText();
            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
            int contador = rucCliente.split("-", -1).length - 1;
            Cliente cliente = new Cliente();
            if (contador >= 1) {
                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
                cliente = cliDAO.listarPorRuc(rucCliente);
//                cliente.setFecNac(null);
//                cliente.setFechaAlta(null);
//                cliente.setFechaMod(null);
//                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toClienteCajaDTO()));
//                clienteList.add(jsonCli);
            } else if (contador == 0) {
                //Listar por CI en el caso que exista
                cliente = cliDAO.listarPorRuc(rucCliente);
                if (cliente.getIdCliente() == null) {
                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
                    List<Cliente> listClien = cliDAO.listarPorCIRevancha(rucCliente);
                    if (!listClien.isEmpty()) {
//                        if (listClien.isEmpty()) {
//                            clienteList = new ArrayList<>();
//                        } else {
//                            clienteList = new ArrayList<>();
//                            for (int i = 0; i < listClien.size(); i++) {
                        cliente = listClien.get(0);
//                                cli.setFecNac(null);
//                                cli.setFechaAlta(null);
//                                cli.setFechaMod(null);
//                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toClienteCajaDTO()));
//                                clienteList.add(jsonCli);
//                            }
//                        }
//                    } else {
//                        clienteList = new ArrayList<>();
                    }
                }
            }

            if (cliente.getNombre() != null) {
                valor = true;
                JSONObject clien = new JSONObject();
                clien.put("idCliente", cliente.getIdCliente());
                String[] datos = txtNuevoRucClienteFiel.getText().split("-");
                clienteFiel.put("cliente", clien);
                clienteFiel.put("cipas", datos[0]);
                txtNuevoNombreClienteFiel.setText(cliente.getNombre());
            }
            if (cliente.getApellido() != null) {
                txtNuevoApellidoClienteFiel.setText(cliente.getApellido());
            }
            if (cliente.getTelefono() != null) {
                if (cliente.getTelefono().equals("''")) {
                    txtNuevoTelefonoClienteFiel.setText("");
                } else {
                    txtNuevoTelefonoClienteFiel.setText(cliente.getTelefono());
                }
            }
            if (cliente.getTelefono2() != null) {
                if (cliente.getTelefono2().equals("''")) {
                    txtNuevoCelularClienteFiel.setText("");
                } else {
                    txtNuevoCelularClienteFiel.setText(cliente.getTelefono2());
                }
            }
            if (!valor) {
//                mensajeAlerta("No se encuentra resultado de la búsqueda realizada.");
                txtNuevoNombreClienteFiel.setText("");
                txtNuevoApellidoClienteFiel.setText("");
                txtNuevoTelefonoClienteFiel.setText("");
                txtNuevoCelularClienteFiel.setText("");
                txtNuevoNombreClienteFiel.setDisable(false);
                txtNuevoApellidoClienteFiel.setDisable(false);
                txtNuevoTelefonoClienteFiel.setDisable(false);
                txtNuevoCelularClienteFiel.setDisable(false);
                txtNuevoNombreClienteFiel.requestFocus();
            }
        } catch (Exception e) {
            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
        }
    }

    private void buscandoNuevoClienteFiel() {
        if (txtNuevoNombreClienteFiel.getText().equals("")) {
            mensajeAlerta("El campo Nombre no debe quedar vacío.");
        } else {
            if (!txtNuevoNombreClienteFiel.isDisable()) {
                creandoCliente();
            }
            nuevoClienteFiel();
        }
    }

    private void resetNuevoClienteFiel() {
        txtNuevoRucClienteFiel.setText("");
        txtNuevoNombreClienteFiel.setText("");
        txtNuevoApellidoClienteFiel.setText("");
        txtNuevoTelefonoClienteFiel.setText("");
        txtNuevoCelularClienteFiel.setText("");
        txtNuevoNombreClienteFiel.setDisable(true);
        txtNuevoApellidoClienteFiel.setDisable(true);
        txtNuevoTelefonoClienteFiel.setDisable(true);
        txtNuevoCelularClienteFiel.setDisable(true);
//        btnNuevoBuscarClienteFiel.setDisable(true);
//        btnNuevoNewClienteFiel.setDisable(true);
        clienteFiel = new JSONObject();
    }

    private void nuevoClienteFiel() {
        String soloCi = clienteFiel.get("cipas").toString();
        List<TarjetaClienteFiel> listTarjFiel = tarDAO.filtrarPorNomApeCi("null", "null", soloCi);
        if (listTarjFiel.toString().equalsIgnoreCase("[]")) {
            if (!txtNuevoNombreClienteFiel.isDisable()) {
                if (insertarNuevoClienteFiel()) {
                    buscarClienteFielPorId(Long.parseLong(clienteFiel.get("idTarjetaClienteFiel").toString()));
                    resetNuevoClienteFiel();
                }
            } else {
//                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE FIEL? (ENTER -> SÍ || ESC -> NO)", ButtonType.OK, ButtonType.CANCEL);
//                this.alert = true;
//                alert.showAndWait();
//                if (alert.getResult() == ButtonType.OK) {
//                    alert.close();
                if (insertarNuevoClienteFiel()) {
                    buscarClienteFielPorId(Long.parseLong(clienteFiel.get("idTarjetaClienteFiel").toString()));
                    resetNuevoClienteFiel();
                    txtNuevoNombreClienteFiel.setDisable(true);
                    txtNuevoApellidoClienteFiel.setDisable(true);
                }
//                } else {
//                    alert.close();
//                }
            }
        } else {
            mensajeAlerta("El cliente ya ha sido registrado como cliente fiel.");
            textFieldCodClienteFiel.setText(soloCi);
            resetNuevoClienteFiel();
        }
    }

    private void seteandoParam(JSONObject clienteFiel) {
        jsonClienteFiel = clienteFiel;
    }

    public static void resetParam() {
        jsonClienteFiel = null;
        clienteFielSi = false;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE FIEL -> GET
    private List<JSONObject> jsonArrayClienteFiel(String nom, String ape, String ciPass) {
        //carácter "espacio", formato...
        nom = nom.replaceAll(" ", "%20");
        ape = ape.replaceAll(" ", "%20");
        ciPass = ciPass.replaceAll(" ", "%20");
        //carácter "espacio", formato...
        List<JSONObject> clienteFielJSONObjList = new ArrayList<JSONObject>();
        clienteFielJSONObjList = generarTarjetaClienteFielLocal(nom, ape, ciPass);
        return clienteFielJSONObjList;
    }
    //////READ, CLIENTE FIEL -> GET

    //////CREATE, CLIENTE FIEL -> POST
    private boolean insertarNuevoClienteFiel() {
        try {
            boolean exitoInsertar = false;
//            Jsonb jsonb = JsonbBuilder.create();
            JSONParser parser = new JSONParser();
            //seteando datos faltantes
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
            clienteFiel.put("idTarjetaClienteFiel", rangoClifielDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal()));
            clienteFiel.put("fechaAlta", timestampEmision);
            clienteFiel.put("fechaMod", timestampEmision);
            clienteFiel.put("usuAlta", Identity.getNomFun());
            clienteFiel.put("usuMod", Identity.getNomFun());
            clienteFiel.put("empresaSuc", empresa.get("descripcionEmpresa").toString());
            exitoInsertar = registrandoClienteFielLocal(clienteFiel.toString());
            if (exitoInsertar) {
                //                    TarjetaClienteFiel tarFiel = mapper.readValue(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                clienteFiel.remove("fechaAlta");
                clienteFiel.remove("fechaMod");
                TarjetaClienteFiel tarFiel = gson.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                tarFiel.setFechaAlta(tsNow);
                tarFiel.setFechaMod(tsNow);

//                TarjetaClienteFiel tarFiel = jsonb.fromJson(String.valueOf(clienteFiel), TarjetaClienteFiel.class);
                boolean valor = tarDAO.insetarObtenerObj(tarFiel);
                if (valor) {
                    System.out.println("HAZ REGISTRADO UN CLIENTE FIEL");
                } else {
                    System.out.println("ERROR: AL INTENTAR REGISTRAR UN CLIENTE FIEL");
                }
                textFieldCodClienteFiel.setText(clienteFiel.get("cipas").toString());
            }
            return exitoInsertar;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }
    //////CREATE, CLIENTE FIEL -> POST

    //////CREATE, CLIENTE -> POST
    private boolean creandoCliente() {
        boolean exitoCrear = false;
//        if ("nuevo_cliente_caja")) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿SEGURO QUE DESEA GENERAR NUEVO CLIENTE FIEL? (ENTER -> SÍ || ESC -> NO)", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
                alert.close();
                JSONObject cliente = new JSONObject();
                cliente = creandoJsonCliente();
                long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                cliente.put("idCliente", idActual);
                exitoCrear = persistiendoPendientes(cliente);
                if (exitoCrear) {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    cliente.put("fechaAlta", null);
                    cliente.put("fechaMod", null);
                    ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
                    cliDTO.setFechaAlta(timestamp);
                    cliDTO.setFechaMod(timestamp);
                    Cliente cli = Cliente.fromClienteDTO(cliDTO);
                    Pais pais = new Pais();
                    pais.setIdPais(0L);
                    Departamento dpto = new Departamento();
                    dpto.setIdDepartamento(0l);
                    Ciudad ciu = new Ciudad();
                    ciu.setIdCiudad(0l);
                    Barrio barr = new Barrio();
                    barr.setIdBarrio(0l);
                    cli.setPais(pais);
                    cli.setDepartamento(dpto);
                    cli.setCiudad(ciu);
                    cli.setBarrio(barr);
                    try {
                        cliDAO.insertar(cli);
                        System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
                    } catch (Exception e) {
                        Utilidades.log.error("ERROR ParseException: ", e.fillInStackTrace());
                        System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
                    }
                    buscarCliente();
//                    Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.OK) {
//                        buscarCliente();
//                        alert2.close();
//                    } else {
//                        alert2.close();
//                    }
                } else {
                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                    this.alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ButtonType.CLOSE) {
                        alert2.close();
                    }
                }
            } else {
                alert.close();
            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFielFXML.fxml", 880, 626, false);
//        }
        return exitoCrear;
    }
    //////CREATE, CLIENTE -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE FIEL
    private List<JSONObject> generarTarjetaClienteFielLocal(String nom, String ape, String ciPass) {
        List<JSONObject> listaJSON = new ArrayList<>();
        JSONParser parser = new JSONParser();
        List<TarjetaClienteFiel> tarFiel = tarDAO.filtrarPorNomApeCi(nom, ape, ciPass);
        for (TarjetaClienteFiel tar : tarFiel) {
            try {
                TarjetaClienteFielDTO tarjetaFielDTO = tar.toTarjetaClienteFielDTO();
                String obj = gson.toJson(tarjetaFielDTO);
                listaJSON.add((JSONObject) parser.parse(obj));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaJSON;
    }
    //////READ, CLIENTE FIEL

    //////READ, CLIENTE FIEL ID
    private void buscarClienteFielPorId(long idClienteFiel) {
        try {
            boolean efeuno = true;
            JSONParser parser = new JSONParser();
            List<JSONObject> lista = new ArrayList<>();
            TarjetaClienteFiel tarFiel = tarDAO.getById(idClienteFiel);
            tarFiel.setFechaAlta(null);
            tarFiel.setFechaMod(null);
            Cliente clien = cliDAO.getById(tarFiel.getCliente().getIdCliente());
            tarFiel.setCliente(null);
            String gsonLista = gson.toJson(tarFiel);
            JSONObject objteJSON = (JSONObject) parser.parse(gsonLista);
            JSONObject clienteJSON = new JSONObject();
            clienteJSON.put("idCliente", clien.getIdCliente());
            clienteJSON.put("nombre", clien.getNombre());
            clienteJSON.put("nombre", clien.getNombre());
            clienteJSON.put("codCliente", clien.getCodCliente());
            clienteJSON.put("ruc", clien.getRuc());
            objteJSON.put("cliente", clienteJSON);
            lista.add(objteJSON);
            clienteFielList = lista;
            actualizandoTablaClienteFiel();
            if (!tableViewClienteFiel.getItems().isEmpty() && efeuno) {
                tableViewClienteFiel.requestFocus();
                tableViewClienteFiel.getSelectionModel().select(0);
                tableViewClienteFiel.getFocusModel().focus(0);
            } else if (!tableViewClienteFiel.getItems().isEmpty() && !efeuno) {
                tableViewClienteFiel.getSelectionModel().select(0);
                tableViewClienteFiel.getFocusModel().focus(0);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }
    //////READ, CLIENTE FIEL ID

    //////CREATE, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////CREATE, PENDIENTES - CLIENTE

    //////CREATE, PENDIENTES - CLIENTE FIEL
    private boolean registrandoClienteFielLocal(String json) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO pendiente.clienfiel_pendientes (fecha_registro ,msj, tabla, dml, ip) VALUES (now(),'" + json + "','tarjeta_cliente_fiel', 'I', '" + Utilidades.host + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////CREATE, PENDIENTES - CLIENTE FIEL
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
    private JSONObject creandoJsonCliente() {
        NumberValidator numVal = new NumberValidator();
        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
        Date date = new Date();
        Timestamp timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject cliente = new JSONObject();
        //**********************************************************************
        cliente.put("nombre", txtNuevoNombreClienteFiel.getText());
        cliente.put("apellido", txtNuevoApellidoClienteFiel.getText());
        if (txtNuevoRucClienteFiel.getText().contentEquals("")) {
            cliente.put("ruc", "0");
        } else {
            cliente.put("ruc", txtNuevoRucClienteFiel.getText());
        }
        cliente.put("usuAlta", Identity.getNomFun());
        cliente.put("fechaAlta", timestampJSON);
        cliente.put("usuMod", Identity.getNomFun());
        cliente.put("fechaMod", timestampJSON);
        //**********************************************************************
        cliente.put("telefono2", txtNuevoCelularClienteFiel.getText());
        cliente.put("telefono", txtNuevoTelefonoClienteFiel.getText());
        cliente.put("segundaLateral", null);
        cliente.put("primeraLateral", null);
        cliente.put("nroLocal", null);
        cliente.put("pais", null);
        cliente.put("departamento", null);
        cliente.put("ciudad", null);
        cliente.put("barrio", null);
        cliente.put("email", null);
        cliente.put("compraUltFecha", null);
        cliente.put("compraIniFecha", null);
        String codCliente = "";
        String arrayCod[] = txtNuevoRucClienteFiel.getText().split("-");
        if (arrayCod.length > 0) {
            codCliente = arrayCod[0];
        } else {
            codCliente = txtNuevoRucClienteFiel.getText();
        }
        cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
        cliente.put("callePrincipal", null);
        return cliente;
    }
    //JSON CREANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaClienteFiel() {
        clienteFielData = FXCollections.observableArrayList(clienteFielList);
        //columna CiPass .................................................
        tableColumnCIPas.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCIPas.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(String.valueOf(data.getValue().get("cipas")));
            }
        });
        //columna CiPass .................................................
        //columna Nombre ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject cliente = (JSONObject) data.getValue().get("cliente");
                String nombre = String.valueOf(cliente.get("nombre"));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        tableViewClienteFiel.setItems(clienteFielData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

    private void validando() {
        txtNuevoCelularClienteFiel.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        txtNuevoCelularClienteFiel.setText(newValue.toString());
                        txtNuevoCelularClienteFiel.positionCaret(txtNuevoCelularClienteFiel.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        txtNuevoCelularClienteFiel.setText(oldValue.toString());
                        txtNuevoCelularClienteFiel.positionCaret(txtNuevoCelularClienteFiel.getLength());
                    });
                }
            }
        });
        txtNuevoTelefonoClienteFiel.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                String aux = newValue.substring(newValue.length() - 1, newValue.length());
                if (GenericValidator.isInt(aux) || aux.contentEquals("-")) {
                    Platform.runLater(() -> {
                        txtNuevoTelefonoClienteFiel.setText(newValue.toString());
                        txtNuevoTelefonoClienteFiel.positionCaret(txtNuevoTelefonoClienteFiel.getLength());
                    });
                } else {
                    Platform.runLater(() -> {
                        txtNuevoTelefonoClienteFiel.setText(oldValue.toString());
                        txtNuevoTelefonoClienteFiel.positionCaret(txtNuevoTelefonoClienteFiel.getLength());
                    });
                }
            }
        });
    }
}
