package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.Caja;
import com.peluqueria.dao.impl.ArqueoCajaDAOImpl;
import com.peluqueria.dao.impl.CajaDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.CryptoFront;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Descuento;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ArqueoCajaDAO;
import com.peluqueria.dao.CajaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.CryptoBack;
import com.javafx.util.Toaster;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
@ScreenScoped
public class LoginCajeroFXMLController extends BaseScreenController implements Initializable {

    Image image;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    private static CajaDAO cajaDAO = new CajaDAOImpl();
    private static ArqueoCajaDAO arqueoDAO = new ArqueoCajaDAOImpl();
    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    public static boolean salirDatos = false;
    public static Map articuloPromo = new HashMap();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private TextField txtNumeroCajero;
    @FXML
    private PasswordField txtClaveCajero;
    @FXML
    private Button btnIngresar;
    @FXML
    private Button btnSalir;
    @FXML
    private ImageView imageViewCajero;
    @FXML
    private AnchorPane anchorPaneCaja;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void txtNumeroCajeroDragEntered(MouseDragEvent event) {
    }

    @FXML
    private void btnIngresarAction(ActionEvent event) {
        ingresandoAperturaCaja();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneCajaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //FXML FXML FXML ******************************************** -> -> -> -> ->`
    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        salirDatos = false;
        cargandoImagen();
        initStaticParamCajero();
        articuloPromo = recuperarMapeoArticuloPromo();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void ingresandoAperturaCaja() {
        JSONParser parser = new JSONParser();
        JSONObject jsonCabecera;
        boolean valor = false;
        org.json.JSONObject json = new org.json.JSONObject(datos);
        try {
            if (!json.isNull("aperturaCaja")) {
                jsonCabecera = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
                long fechaHoraEmision = Long.parseLong(jsonCabecera.get("fechaApertura").toString());
                String output = new SimpleDateFormat("yyyy-MM-dd").format(new Timestamp(fechaHoraEmision));
                ZoneId zonedId = ZoneId.of("America/Montreal");
                LocalDate today = LocalDate.now(zonedId);
                valor = output.equals(today.toString());
                if (fact != null && !fact.equals("{}") && fact.size() != 0) {
                    if (!valor) {
                        valor = true;
                    }
                }
            } else {
                valor = true;
            }
        } catch (ParseException ex) {
            jsonCabecera = null;
        }

        if (valor) {
            //almacenando datos para descuento... si se sabe, debería estar en el backend, 
            //pero esto es especial, debido a la inestabilidad en la conexión...
            CryptoFront cf = new CryptoFront();
            CryptoBack cb = new CryptoBack();
            articuloPromo = recuperarMapeoArticuloPromo();
//            Descuento.resetParam(1l);
            if (manejoDAO.recuperarId() == 0l) {
                JSONObject jsonCaja = jsonAccesoCaja(txtNumeroCajero.getText(), cf.getHash(txtClaveCajero.getText()));
                if (jsonCaja != null) {
//                    articuloPromo = recuperarMapeoArticuloPromo();
                    boolean ingresoIp = true;
                    try {
//                        JSONObject jsonIpBoca = (JSONObject) jsonCaja.get("ipBoca");
//                        boolean ingresoIp = false;
//                        InetAddress IP = InetAddress.getLocalHost();
//                        InetAddress[] allMyIps = InetAddress.getAllByName(IP.getCanonicalHostName());
//                        InetAddressValidator inetValidator = InetAddressValidator.getInstance();
//                        if (allMyIps != null && allMyIps.length > 1) {
//                            for (int i = 0; i < allMyIps.length; i++) {
//                                String auxSplit[] = allMyIps[i].toString().split("\\/");
//                                if (inetValidator.isValidInet4Address(auxSplit[1])) {
//                                    if (auxSplit[1].contentEquals(jsonIpBoca.get("ipBoca").toString())) {
//                                        ingresoIp = true;
//                                        break;
//                                    }
//                                }
//                            }
//                        }
                        //ELIMINAR LA LINEA DE ABAJO SOLO PARA PRUEBA
//                        ingresoIp = true;
                        if (ingresoIp) {
//                            if ("apertura_caja")) {
                            //**--VERIFICANDO--**
                            //**--NUEVO EN BD--**
                            if (manejoDAO.verificarExistencia() == 0) {
                                CajaDeDatos cajaData = new CajaDeDatos();
                                cajaData.mapeandoCaja(jsonCaja);
                                try {
                                    verificandoValorZetaLocal();
                                    Utilidades.setIdRangoLocal((long) jsonCaja.get("nroCaja"));
                                    this.sc.loadScreen("/vista/caja/aperturaCajaFXML.fxml", 420, 245, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                                } catch (ParseException ex) {
                                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                                } catch (Exception ex) {
                                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                                }
                            }
//                            } else {
//                                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
//                            }
                        } else {
                            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                        }
                    } catch (java.text.ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                }
                DatosEnCaja.setDatos(datos);
            } else {
                //**NEW EXAMPLE**//
                JSONObject objCaja = null;
                try {
                    objCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                } catch (ParseException ex) {
                    Utilidades.log.error("ParseException", ex.fillInStackTrace());
                }
                if (objCaja != null) {
                    String claveFront = cf.getHash(txtClaveCajero.getText());
                    if (objCaja.get("nroCaja").toString().equalsIgnoreCase(txtNumeroCajero.getText()) && objCaja.get("claveCaja").toString().equalsIgnoreCase(cb.getHash(claveFront))) {
                        Utilidades.setIdRangoLocal((long) objCaja.get("nroCaja"));
                        if (!json.isNull("modSup")) {
                            if (json.getBoolean("modSup")) {
                                mensajeError("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO!");
                            } else {
                                this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                            }
                        } else {
                            this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                        }
                    } else {
                        this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                    }
                } else {
                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                }
                //**FINAL**//
                // ORIGINALMENTE SOLO TENIA ESTE
//            this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
                // FINALMENTE 
            }
        } else {
            mensajeInfo("NECESARIAMENTE DEBE REALIZAR EL ARQUEO DE CAJA");
        }
    }

    public static HashMap recuperarMapeoArticuloPromo() {
        HashMap mapeo = new HashMap();
        ConexionPostgres.conectarLocal();
        String sql = "SELECT cod_articulo FROM sincro.articulo_promo";
        try {
            ResultSet rs = ConexionPostgres.getStLocal().executeQuery(sql);
            while (rs.next()) {
                mapeo.put(rs.getLong("cod_articulo"), rs.getLong("cod_articulo"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(LoginCajeroFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        ConexionPostgres.cerrarLocal();
        return mapeo;
    }

    private void mensajeInfo(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeError(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void volviendo() {
        if (!salirDatos) {
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
        } else {
//            this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/caja/loginCajeroFXML.fxml", 545, 317, true);
            this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    @SuppressWarnings("static-access")
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtNumeroCajero.isFocused()) {
                txtClaveCajero.requestFocus();
            } else if (txtClaveCajero.isFocused()) {
                if (txtNumeroCajero.getText().isEmpty()) {
                    txtNumeroCajero.requestFocus();
                } else {
                    ingresandoAperturaCaja();
                }
            }
        } else if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoImagen() {
        File file = new File(PATH.PATH_CAJ);
        this.image = new Image(file.toURI().toString());
        this.imageViewCajero.setImage(this.image);
    }

    private void initStaticParamCajero() {
        ClienteFXMLController.resetParam();
    }

    private void verificandoValorZetaLocal() throws ParseException, Exception {

        //**--VERIFICANDO--**
        datos = DatosEnCaja.getDatos();
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject) parser.parse(datos.get("timbrado").toString());
        String nro = (String) obj.get("nroTimbrado");
        JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
        long idCaja = Long.valueOf(caja.get("idCaja").toString());
        long num = recuperarZeta(idCaja, nro);
        datos.put("zeta", num);
        //SETEAR LOS DATOS EN LA VARIABLE DE LA CLASE
        DatosEnCaja.setDatos(datos);

    }

    private static long recuperarZeta(long valor, String nro) {
        long num = arqueoDAO.recuperarNumMaxZeta();
        return num;
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, CAJA -> GET
    private JSONObject jsonAccesoCaja(String cajaNro, String pass) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
        JSONObject caja = null;
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/caja/auth/" + cajaNro + "/" + pass + "");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    caja = (JSONObject) parser.parse(inputLine);
//                }
//                br.close();
//            } else {
//                caja = generarAccesoLocal(cajaNro, pass);
//            }
//        } catch (IOException | ParseException ex) {
        caja = generarAccesoLocal(cajaNro, pass);
//        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//        }
        return caja;
    }
    //////READ, CAJA -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CAJA
    private JSONObject generarAccesoLocal(String cajaNro, String pass) {
        JSONParser parser = new JSONParser();
        int nroCaja = 0;
        try {
            nroCaja = Integer.parseInt(cajaNro);
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
        try {
            Caja caja = cajaDAO.buscarCaja(nroCaja, pass);
            return (JSONObject) parser.parse(gson.toJson(caja.toCajaDTO()));
        } catch (Exception e) {
            return null;
        }
    }
    //////READ, CAJA
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

}
