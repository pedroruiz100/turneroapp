/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class CerrarTurnoFXMLController extends BaseScreenController implements Initializable {

    public static void setTxtCodigo(TextField textFieldCod) {
        txtCodigo = textFieldCod;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean primerEnter;
    private JSONObject supervisor;
    private Date date;
    private static TextField txtCodigo;
    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    //campos númericos

    //FXML FXML FXML ******************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private AnchorPane anchorPaneCodSup;
    @FXML
    private HBox hBox;
    @FXML
    private Label labelCodSupervisor;
    @FXML
    private PasswordField passwordFieldCodSupervisor;
    @FXML
    private Label labelCierreTurno;
    @FXML
    private AnchorPane anchorPaneDatosRetiro;
    @FXML
    private Label labelSupervisor;
    @FXML
    private TextField textFieldMonto;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button buttonVolver;
    @FXML
    private VBox vBoxMonto;
    @FXML
    private VBox vBoxMontoText;
    @FXML
    private Label labelContrasenha;
    @FXML
    private TextField textFieldContrasenha;
    @FXML
    private Label labelUsuario;
    @FXML
    private TextField textFieldSupervisorControl;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        procesandoCierreTurno();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        asignandoValores();
        ocultandoParam2();
        ocultandoParam1();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        FacturaDeVentaFXMLController.regresar(txtCodigo);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/cerrarTurnoFXML.fxml", 600, 439, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (!alert) {
                if (primerEnter) {
                    validandoIngreso();
                } else {
                    validandoIngreso2();
                }
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
//            if (alert) {
//                alert = false;
//            } else {
                volviendo();
//            }
        }
        if (keyCode == event.getCode().F1) {
            if (textFieldMonto.getText().isEmpty()) {
                Alert alerta = new Alert(Alert.AlertType.ERROR, "Debe ingresar un monto para cerrar la Caja. ( ESC -> SALIR)", ButtonType.CLOSE);
                this.alert = true;
                alerta.showAndWait();
            } else {
                procesandoCierreTurno();
            }
        }
    }

    private void asignandoValores() {
        //CAMPO NUMERICO
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        textFieldMonto.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        textFieldMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            textFieldMonto.setText(param);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText("");
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    textFieldMonto.setText(param);
                                    textFieldMonto.positionCaret(textFieldMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                textFieldMonto.setText("");
                                textFieldMonto.positionCaret(textFieldMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            textFieldMonto.setText(oldValue);
                            textFieldMonto.positionCaret(textFieldMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = newValue;
                    }
                    Platform.runLater(() -> {
                        textFieldMonto.setText(param);
                        textFieldMonto.positionCaret(textFieldMonto.getLength());
                    });
                }
            }
        });
        //CAMPO NUMERICO
        primerEnter = true;
        alert = false;
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void ocultandoParam1() {
        labelUsuario.setVisible(false);
        textFieldSupervisorControl.setVisible(false);
        textFieldSupervisorControl.setText("");
        labelContrasenha.setVisible(false);
        textFieldContrasenha.setVisible(false);
        textFieldContrasenha.setText("");
        primerEnter = true;
    }

    private void ocultandoParam2() {
        vBoxMonto.setVisible(false);
        vBoxMontoText.setVisible(false);
        btnProcesar.setVisible(false);
    }

    private void descubriendoParam1() {
        labelUsuario.setVisible(true);
        textFieldSupervisorControl.setVisible(true);
        labelContrasenha.setVisible(true);
        textFieldContrasenha.setVisible(true);
        primerEnter = false;
    }

    private void descubriendoParam2() {
        vBoxMonto.setVisible(true);
        vBoxMontoText.setVisible(true);
        btnProcesar.setVisible(true);
    }

    private void validandoIngreso() {
        if (!passwordFieldCodSupervisor.getText().contentEquals("")) {
            supervisor = jsonClaveSupervisor(UtilLoaderBase.msjIda(passwordFieldCodSupervisor.getText()));
            if (supervisor != null) {
                descubriendoParam1();
                textFieldSupervisorControl.requestFocus();
            } else {
                Alert alert = new Alert(Alert.AlertType.ERROR, "Clave de supervisor incorrecta.", ButtonType.CLOSE);
                this.alert = true;
                alert.showAndWait();
                if (alert.getResult() == ButtonType.CLOSE) {
                    alert.close();
                } else {
                    alert.close();
                }
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Clave de supervisor vacía.", ButtonType.CLOSE);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ButtonType.CLOSE) {
                alert.close();
            } else {
                alert.close();
            }
        }
    }

    private void validandoIngreso2() {
        JSONObject usuario = (JSONObject) supervisor.get("usuario");
        CryptoFront cf = new CryptoFront();
        CryptoBack cb = new CryptoBack();
        if (usuario.get("nomUsuario").toString().toUpperCase().contentEquals(textFieldSupervisorControl.getText().toUpperCase())
                && usuario.get("contrasenha").toString().contentEquals(cb.getHash(cf.getHash(textFieldContrasenha.getText())))) {
            descubriendoParam2();
            textFieldMonto.requestFocus();
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR, "Lo sentimos, no reconocemos el usuario\ny/o la contraseña.", ButtonType.CLOSE);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ButtonType.CLOSE) {
                alert.close();
            } else {
                alert.close();
            }
            textFieldMonto.setText("");
            ocultandoParam1();
        }
    }

    private void procesandoCierreTurno() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿Realizar cierre de turno? (ENTER -> SÍ || ESC -> NO)", ButtonType.YES, ButtonType.NO);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult().toString().equalsIgnoreCase(ButtonType.YES.toString())) {
            if (btnProcesar.isVisible()) {
                boolean estado = cierreTurno();
                if (estado) {
                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/cerrarTurnoFXML.fxml", 572, 473, true);
                }
            }
            alert.close();
        } else {
            alert.close();
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, CLAVE SUPERIOR -> GET
    private JSONObject jsonClaveSupervisor(String c) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject supervisor = null;
        try {
            URL url = new URL(Utilidades.ip + "/ServerParana/supervisor/auth/" + c + "");
            URLConnection uc = url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
            while ((inputLine = br.readLine()) != null) {
                supervisor = (JSONObject) parser.parse(inputLine);
            }
            br.close();
        } catch (FileNotFoundException ex) {
            Utilidades.log.error("ERROR FileNotFoundException: ", ex.fillInStackTrace());
        } catch (IOException | ParseException ex) {
            Utilidades.log.error("ERROR ParseException/IOException: ", ex.fillInStackTrace());
        }
        return supervisor;
    }
    //////READ, CLAVE SUPERIOR -> GET

    //////CREATE, CIERRE CAJA -> POST
    private boolean cierreTurno() {
        boolean exito = false;
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonCierre = creandoJsonCierre();
        try {
            URL url = new URL(Utilidades.ip + "/ServerParana/cierreCaja");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accept", "application/json");
            conn.setRequestMethod("POST");
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(jsonCierre.toString());
            wr.flush();
            int HttpResult = conn.getResponseCode();
            if (HttpResult == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(
                        new InputStreamReader(conn.getInputStream(), "utf-8"));
                while ((inputLine = br.readLine()) != null) {
                    exito = (boolean) parser.parse(inputLine);
                }
                br.close();
            } else {
            }
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileNotFoundException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        return exito;
    }
    //////CREATE, CIERRE CAJA -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CIERRE TURNO
    private JSONObject creandoJsonCierre() {
        date = new Date();
        timestamp = new Timestamp(date.getTime());
        Long timestampJSON = timestamp.getTime();
        JSONObject cierreCaja = new JSONObject();
        JSONObject userCajero = Identity.onlyUser();
        cierreCaja.put("fechaCierre", timestampJSON);
        cierreCaja.put("usuarioCajero", userCajero);
        cierreCaja.put("montoCierre", (Integer.valueOf(numValidator.numberValidator(textFieldMonto.getText()))));
        cierreCaja.put("tipoCierre", "CIERRE DE CAJA");
        return cierreCaja;
    }
    //JSON CREANDO CIERRE TURNO
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
}
