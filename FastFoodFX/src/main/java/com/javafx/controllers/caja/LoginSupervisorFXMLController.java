package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.Supervisor;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.PATH;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseDragEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.google.gson.GsonBuilder;
import com.javafx.controllers.login.LoginFXMLController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.CryptoFront;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
@ScreenScoped
public class LoginSupervisorFXMLController extends BaseScreenController implements Initializable {

    Image image;
    boolean alert;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    @Autowired
    private SupervisorDAO superDAO;
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();

    @Autowired
    ManejoLocalDAO manejoDAO;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private TextField txtNumeroSupervisor;
    @FXML
    private Button btnIngresar;
    @FXML
    private Button btnSalir;
    @FXML
    private Pane panelLogueoSupervisor;
    @FXML
    private Label labelNroSup;
    @FXML
    private Label labelClaveSup;
    @FXML
    private PasswordField passwordFieldClaveSup;
    @FXML
    private ImageView imageViewSupervisor;
    @FXML
    private AnchorPane anchorPaneSupervisor;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void txtNumeroSupervisorDragEntered(MouseDragEvent event) {
    }

    @FXML
    private void btnIngresarAction(ActionEvent event) {
        ingresandoSupervisor();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void txtNumeroSupervisorAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneSupervisorKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        cargandoImagen();
        if (ScreensContoller.getFxml().contentEquals("/vista/caja/FacturaDeVentaFXML.fxml")) {
            btnSalir.setVisible(false);
        } else {
            btnSalir.setVisible(true);
        }
        asignandoVariables();
    }
    //INICIAL INICIAL INICIAL ***********************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void ingresandoSupervisor() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        JSONParser parser = new JSONParser();
        JSONObject caja = null;
        boolean estado = false;
        if (!json.isNull("caja")) {
            try {
                caja = (JSONObject) parser.parse(datos.get("caja").toString());
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        //NEW A PEDIDO DE LA SUPERVISORA PARA QUE SE PUEDA HACER EL CIERRE IGUAL SI NO SE HA FACTURADO NADA
        if (caja != null && json.isNull("inicio")) {
            estado = true;
        } else {
            estado = !datos.containsKey("montoFacturado");
        }
        //FINISH

        CryptoFront cf = new CryptoFront();
//        if (caja != null && json.isNull("inicio")) {
//        if (estado) {
//            JSONObject jsonSupervisor = jsonAccesoSupervisor(txtNumeroSupervisor.getText(), UtilLoaderBase.msjIda(passwordFieldClaveSup.getText()));
        JSONObject jsonSupervisor = jsonAccesoSupervisor(txtNumeroSupervisor.getText(), cf.getHash(passwordFieldClaveSup.getText()));
        if (jsonSupervisor != null) {
//            if ("modulo_supervisor")) {
                this.sc.loadScreen("/vista/caja/moduloSupervisorFXML.fxml", 903, 368, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, false);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, true);
//            }
        } else {
            if (ScreensContoller.getFxml().contentEquals("/vista/caja/facturaVentaFXML.fxml")) {
            } else {
                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, true);
            }
        }
//        } else {
//            mensajeAlerta("DEBE GENERAR UNA VENTA PARA PODER INGRESAR A ESTE MODULO.");
//        }
    }

    private void volviendo() {
//        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//        if (jsonDatos.isNull("rendicion")) {
//            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, false);
//        } else if (!Boolean.parseBoolean(datos.get("rendicion").toString())) {
//            mensajeAlerta("DEBE GENERAR PRIMERAMENTE EL INFORME FINANCIERO PARA FINALIZAR EL CIERRE DE TURNO.");
//        } else {
        LoginFXMLController.setLlamarTask(false);
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/caja/loginSupervisorFXML.fxml", 540, 312, false);
//        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        alert = false;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    @SuppressWarnings("static-access")
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtNumeroSupervisor.isFocused()) {
                passwordFieldClaveSup.requestFocus();
            } else if (passwordFieldClaveSup.isFocused()) {
                if (txtNumeroSupervisor.getText().isEmpty()) {
                    txtNumeroSupervisor.requestFocus();
                } else {
                    ingresandoSupervisor();
                }
            }
        } else if (keyCode == event.getCode().ESCAPE) {
            if (btnSalir.isVisible()) {
                volviendo();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void cargandoImagen() {
        File file = new File(PATH.PATH_SUP);
        this.image = new Image(file.toURI().toString());
        this.imageViewSupervisor.setImage(this.image);
    }

    private void asignandoVariables() {
        alert = true;
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR -> GET
    private JSONObject jsonAccesoSupervisor(String nroSupervisor, String clave) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject supervisor = null;
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/supervisor/auth/" + nroSupervisor + "/" + clave + "");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                while ((inputLine = br.readLine()) != null) {
                    supervisor = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            } else {
                supervisor = generarLoginSupervisorLocal(nroSupervisor, clave);
            }
        } catch (IOException | ParseException ex) {
            supervisor = generarLoginSupervisorLocal(nroSupervisor, clave);
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (supervisor != null) {
            Identity identity = new Identity();
            identity.usuarioLogueado(null);
            identity.usuarioLogueado((JSONObject) supervisor.get("usuario"));
        }
        return supervisor;
    }
    //////READ, SUPERVISOR -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, SUPERVISOR
    private JSONObject generarLoginSupervisorLocal(String nroSupervisor, String clave) {
        JSONParser parser = new JSONParser();
        JSONObject jsonObj = null;
        if (!nroSupervisor.equals("") && !clave.equals("")) {
            Supervisor supervisor = superDAO.buscarNumeroCodigoSup(Integer.parseInt(nroSupervisor), clave);
            if (supervisor != null) {
                try {
                    jsonObj = (JSONObject) parser.parse(gson.toJson(supervisor.toSupervisorRolFuncionDTO()));
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                }
            }
        }
        return jsonObj;
    }
    //////READ, SUPERVISOR
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

}
