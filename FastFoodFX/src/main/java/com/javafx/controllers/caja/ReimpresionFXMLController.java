/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.caja;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.NumberValidator;
import com.javafx.util.Ticket;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.FacturaClienteCab;
import com.peluqueria.core.domain.FacturaClienteCabHistorico;
import com.peluqueria.core.domain.FacturaClienteDet;
import com.peluqueria.core.domain.TipoComprobante;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaClienteCabHistoricoDAO;
import com.peluqueria.dao.FacturaClienteDetDAO;
import com.peluqueria.dao.TipoComprobanteDAO;
import com.peluqueria.dto.FacturaClienteCabDTO;
import com.peluqueria.dto.FacturaClienteCabHistoricoDTO;
import com.peluqueria.dto.FacturaClienteDetDTO;
import com.peluqueria.dto.TipoComprobanteDTO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.GenericValidator;
import org.controlsfx.control.textfield.TextFields;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ReimpresionFXMLController extends BaseScreenController implements Initializable {

    @Autowired
    private FacturaClienteCabDAO fccDAO;
    @Autowired
    private TipoComprobanteDAO tipoComprobanteDAO;
    @Autowired
    private FacturaClienteCabHistoricoDAO fHistoricoDAO;
    @Autowired
    private FacturaClienteDetDAO fcDetDAO;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    private ToggleGroup group;
    private String nroFactFiltro;
    private String fechaFactFiltro;
    private String nroCajaFiltro;
    private String timbradoFiltro;
    private String tipoFactFiltro;
    private boolean cargarCaja;
    private boolean cargarTimbrado;
    private boolean cargarTipoFact;
    JSONObject jsonFactCabHist;
    private Ticket ticket;
    private boolean alert;
    private JSONArray cajasJSONArray;
    private JSONArray tipoFactJSONArray;
    private JSONArray timbradosJSONArray;
    private NumberValidator numValidator;
    private final String patternFechaHora = "dd/MM/yyyy HH:mm";
    private final String patternFecha = "dd/MM/yyyy";
    private HashMap<String, String> hashJsonTipoComp;
    private String contentTicket
            = "{{empresa}} \n"
            + "{{sucRuc}}\n"
            + "{{tel}}\n"
            + "{{dir}}\n"
            + "{{dup}}\n"
            + "{{dupSinVC}}"
            + "======================================\n"
            + "TIMBRADO NRO. {{timb}} \n"
            + "INICIO VIGENCIA: {{desde}}\n"
            + "FIN VIGENCIA: {{hasta}}\n"
            + "CAJA INTERNA: {{cj}} FECHA: {{fH}}\n"
            + "FACTURA CONTADO: {{factNro}}\n"
            + "CAJERO: {{usuario}}\n"
            + "======================================\n"
            + "Articulo  Cant.  Precio      Total\n"
            + "{{art}}\n"
            + "===DETALLE FISCAL========================\n"
            + "GRAVADA 10% :  {{grav10}}\n"
            + "GRAVADA 5%  :  {{grav5}}\n"
            + "EXENTA      :  {{exenta}}\n"
            + "===LIQUIDACION DE IVA=====================\n"
            + "10%:  {{grav10Liq}}\n"
            + " 5%:   {{grav5Liq}}\n"
            + "======================================\n"
            + "RUC: {{rucCliente}}\n"
            + "CLIENTE: \n"
            + "{{cliente}}\n"
            + "======================================\n"
            + "ARTICULOS: {{cant}}TOTAL: {{total}}\n"
            + "DESCUENTO: {{desc}} NETO: {{neto}}\n"
            + "======================================\n"
            + "{{efectivo}}"
            + "{{dolar}}"
            + "{{peso}}"
            + "{{real}}"
            + "{{tarjCred}}"
            + "{{tarjDeb}}"
            + "{{cheque}}"
            + "{{vale}}"
            + "{{asoc}}"
            + "{{notCre}}"
            + "{{retencion}}"
            + "{{giftcard}}"
            + "DONACION: {{redondeo}}\n"
            + "VUELTO: {{vuelto}}";
    private String empresa;
    private String sucursal;
    private String ruc;
    private String telef;
    private String direccion;
    private String nroTimbrado;
    private String fecInicial;
    private String fecVencimiento;
    private String nroCaja;
    private String fechaEmision;
    private String nroFactura;
    private String usuAlta;
    private String detalleArticulo;
    private String grav10;
    private String grav5;
    private String exe;
    private String liqIva10;
    private String liqIva5;
    private String rucCliente;
    private String cliente;
    private String cantTotalArt;
    private String total;
    private String descuento;
    private String neto;
    private String efectivo;
    private String giftcard;
    private String tarjCred;
    private String tarjDeb;
    private String cheque;
    private String vale;
    private String asoc;
    private String notCre;
    private String redondeo;
    private String vuelto;
    private String dolar;
    private String peso;
    private String realb;
    private String retencion;

    //FXML FXML FXML ****************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneCab;
    @FXML
    private Label labelReimp;
    @FXML
    private AnchorPane anchorPaneFiltro;
    @FXML
    private Label labelNroFact;
    @FXML
    private Label labelTimbrado;
    @FXML
    private Label labelTipoFact;
    @FXML
    private Label labelCajaNro;
    @FXML
    private TextField textFieldNroFact;
    @FXML
    private ComboBox<String> comboBoxTipoFact;
    @FXML
    private ComboBox<String> comboBoxCaja;
    @FXML
    private HBox hBoxButton;
    @FXML
    private Button buttonVerFact;
    @FXML
    private Button buttonImpFact;
    @FXML
    private AnchorPane anchorPaneContent;
    @FXML
    private AnchorPane anchorPaneBottom;
    @FXML
    private Button buttonVolver;
    @FXML
    private HBox hBoxButtonNav;
    @FXML
    private Button buttonAnterior;
    @FXML
    private Button buttonSgte;
    @FXML
    private Label labelFechaFact;
    @FXML
    private DatePicker datePickerFechaFact;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private TextArea textAreaFact;
    @FXML
    private ComboBox<String> comboBoxTimbrado;
    @FXML
    private GridPane gridPaneFiltro;
    @FXML
    private VBox vBoxCheckDuplicado;
    @FXML
    private RadioButton radioDuplicado;
    @FXML
    private RadioButton radioDuplicadoSinValorC;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonVerFactAction(ActionEvent event) {
        observandoCopia();
    }

    @FXML
    private void buttonImpFactAction(ActionEvent event) {
        imprimiendoCopia();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void buttonAnteriorAction(ActionEvent event) {
        buttonAnte();
    }

    @FXML
    private void buttonSgteAction(ActionEvent event) {
        buttonSgte();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoFiltros();
    }

    @FXML
    private void comboBoxTipoFactMouseClicked(MouseEvent event) {
        mouseEventComboBoxTipoFact(event);
    }

    @FXML
    private void comboBoxTimbradoMouseClicked(MouseEvent event) {
        mouseEventComboBoxTimbrado(event);
    }

    @FXML
    private void comboBoxCajaMouseClicked(MouseEvent event) {
        mouseEventComboBoxCaja(event);
    }

    @FXML
    private void comboBoxTipoFactKeyReleased(KeyEvent event) {
        keyPressComboBoxTipoFact(event);
    }

    @FXML
    private void comboBoxCajaKeyReleased(KeyEvent event) {
        keyPressComboBoxCaja(event);
    }

    @FXML
    private void comboBoxTimbradoKeyReleased(KeyEvent event) {
        keyPressComboBoxTimbrado(event);
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        animando();
        nroFactFiltro = "";
        fechaFactFiltro = "";
        nroCajaFiltro = "";
        timbradoFiltro = "";
        tipoFactFiltro = "";
        buttonImpFact.setDisable(true);
        buttonAnterior.setDisable(true);
        buttonSgte.setDisable(true);
        cargarTimbrado = true;
        cargarTipoFact = true;
        cargarCaja = true;
        datePickerFechaFact.setValue(LocalDate.now());
        final Callback<DatePicker, DateCell> dayCellFactoryIni
                = new Callback<DatePicker, DateCell>() {
            @Override
            public DateCell call(final DatePicker datePicker) {
                return new DateCell() {
                    @Override
                    public void updateItem(LocalDate item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item.isAfter(LocalDate.now())) {
                            setDisable(true);
                            setStyle("-fx-background-color: #ffc0cb;");
                        }
                    }
                };
            }
        };
        datePickerFechaFact.setDayCellFactory(dayCellFactoryIni);
        iniciandoCB();
        reseteandoDatosImpresion();
        numValidator = new NumberValidator();
        textFieldNroFact.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isLong(newValue)) {
                        Platform.runLater(() -> {
                            textFieldNroFact.setText(newValue);
                            textFieldNroFact.positionCaret(textFieldNroFact.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldNroFact.setText(oldValue);
                            textFieldNroFact.positionCaret(textFieldNroFact.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldNroFact.setText(oldValue);
                        textFieldNroFact.positionCaret(textFieldNroFact.getLength());
                    });
                }
            }
        });
        buttonGroup();
        group.selectedToggleProperty().addListener(new ChangeListener<Toggle>() {
            @Override
            public void changed(ObservableValue<? extends Toggle> ov, Toggle old_toggle, Toggle new_toggle) {
                if (group.getSelectedToggle() != null) {
                    viendoMinorista(empresa, sucursal, ruc, telef, direccion, nroTimbrado, fecInicial, fecVencimiento, nroCaja,
                            fechaEmision, nroFactura, usuAlta, detalleArticulo, grav10, grav5, exe, liqIva10, liqIva5, rucCliente, cliente,
                            cantTotalArt, total, descuento, neto, efectivo, tarjCred, tarjDeb, cheque, vale, asoc, notCre,
                            redondeo, vuelto, dolar, peso, realb, retencion, giftcard);
                }
            }
        });
        /*comboBoxTipoFact.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                if (comboBoxTipoFact.getSelectionModel().getSelectedItem().contentEquals("FACTURA CONTADO")) {
                    textAreaFact.resizeRelocate(200, 7, 100, 497);
                } else {
                    textAreaFact.resizeRelocate(8, 7, 989, 497);
                }
            }
        });*/
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/caja/ReimpresionFXML.fxml", 1020, 580, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            observandoCopia();
        }
        if (keyCode == event.getCode().F2) {
            imprimiendoCopia();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoFiltros();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }

    private void keyPressComboBoxCaja(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarCaja) {
                if (keyCode != event.getCode().TAB) {
                    String result = jsonCajas();
                    if (cajasJSONArray.isEmpty()) {
                        mensajeError(result);
                        cargarCaja = true;
                    }
                }
            }
            TextFields.bindAutoCompletion(comboBoxCaja.getEditor(), comboBoxCaja.getItems());
        }
    }

    private void mouseEventComboBoxCaja(MouseEvent event) {
        if (cargarCaja) {
            String result = jsonCajas();
            if (cajasJSONArray.isEmpty()) {
                mensajeError(result);
                cargarCaja = true;
            } 
        }
    }

    private void keyPressComboBoxTimbrado(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTimbrado) {
                if (keyCode != event.getCode().TAB) {
                    String result = jsonTimbrados();
                    if (timbradosJSONArray.isEmpty()) {
                        mensajeError(result);
                        cargarTimbrado = true;
                    }
                }
            }
            TextFields.bindAutoCompletion(comboBoxTimbrado.getEditor(), comboBoxTimbrado.getItems());
        }
    }

    private void mouseEventComboBoxTimbrado(MouseEvent event) {
        if (cargarTimbrado) {
            String result = jsonTimbrados();
            if (timbradosJSONArray.isEmpty()) {
                mensajeError(result);
                cargarTimbrado = true;
            } 
        }
    }

    private void keyPressComboBoxTipoFact(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().ESCAPE) {
            if (cargarTipoFact) {
                if (keyCode != event.getCode().TAB) {
                    String result = jsonTipoComprobante();
                    if (tipoFactJSONArray.isEmpty()) {
                        mensajeError(result);
                        cargarTipoFact = true;
                    }
                }
            }
            TextFields.bindAutoCompletion(comboBoxTipoFact.getEditor(), comboBoxTipoFact.getItems());
        }
    }

    private void mouseEventComboBoxTipoFact(MouseEvent event) {
        if (cargarTipoFact) {
            String result = jsonTipoComprobante();
            if (tipoFactJSONArray.isEmpty()) {
                mensajeError(result);
                cargarTipoFact = true;
            } 
        }
    }

    private void buttonGroup() {
        group = new ToggleGroup();
        radioDuplicado.setSelected(true);
        radioDuplicado.setToggleGroup(group);
        radioDuplicadoSinValorC.setToggleGroup(group);
//        radioDuplicado.setDisable(true);
//        radioDuplicadoSinValorC.setDisable(true);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //NAVEGAR IMPRESIÓN
    private void buttonSgte() {
        if (!textFieldNroFact.getText().contentEquals("")
                && comboBoxTipoFact.getSelectionModel().getSelectedIndex() != 0
                && comboBoxCaja.getSelectionModel().getSelectedIndex() != 0
                && comboBoxTimbrado.getSelectionModel().getSelectedIndex() != 0) {
            long numFact = Long.valueOf(textFieldNroFact.getText());
            numFact++;
            textFieldNroFact.setText(String.valueOf(numFact));
            observandoCopia();
        } else {
            mensajeAdv("NO OLVIDES COMPLETAR LOS CAMPOS OBLIGATORIOS.");
        }
    }

    private void buttonAnte() {
        if (!textFieldNroFact.getText().contentEquals("")
                && comboBoxTipoFact.getSelectionModel().getSelectedIndex() != 0
                && comboBoxCaja.getSelectionModel().getSelectedIndex() != 0
                && comboBoxTimbrado.getSelectionModel().getSelectedIndex() != 0) {
            long numFact = Long.valueOf(textFieldNroFact.getText());
            numFact--;
            textFieldNroFact.setText(String.valueOf(numFact));
            observandoCopia();
        } else {
            mensajeAdv("NO OLVIDES COMPLETAR LOS CAMPOS OBLIGATORIOS.");
        }
    }
    //NAVEGAR IMPRESIÓN

    private void reseteandoDatosImpresion() {
        buttonImpFact.setDisable(true);
        buttonAnterior.setDisable(true);
        buttonSgte.setDisable(true);
        empresa = "";
        sucursal = "";
        ruc = "";
        telef = "";
        direccion = "";
        nroTimbrado = "";
        fecInicial = "";
        fecVencimiento = "";
        nroCaja = "";
        fechaEmision = "";
        nroFactura = "";
        usuAlta = "";
        detalleArticulo = "";
        grav10 = "";
        grav5 = "";
        exe = "";
        liqIva10 = "";
        liqIva5 = "";
        rucCliente = "";
        cliente = "";
        cantTotalArt = "";
        total = "";
        descuento = "";
        neto = "";
        efectivo = "";
        giftcard = "";
        tarjCred = "";
        tarjDeb = "";
        cheque = "";
        vale = "";
        asoc = "";
        notCre = "";
        redondeo = "";
        vuelto = "";
        dolar = "";
        peso = "";
        realb = "";
        retencion = "";
        textAreaFact.clear();
        textAreaFact.setScrollTop(Double.MIN_VALUE);
        textAreaFact.setText("\n\n\n\t\t\tSIN FACTURA.\n\t\t\t***************");
    }

    private void iniciandoCB() {
        comboBoxTipoFact.getItems().add("-");
        comboBoxTipoFact.getSelectionModel().select(0);//por el momento...
        comboBoxCaja.getItems().add("-");
        comboBoxCaja.getSelectionModel().select(0);
        comboBoxTimbrado.getItems().add("-");
        comboBoxTimbrado.getSelectionModel().select(0);
    }

    private void ofuscando() {
//        nroFactFiltro = UtilLoaderBase.msjIda(textFieldNroFact.getText());
//        LocalDate fechaFact = datePickerFechaFact.getValue();
//        fechaFactFiltro = UtilLoaderBase.msjIda(fechaFact.toString());
//        nroCajaFiltro = UtilLoaderBase.msjIda(comboBoxCaja.getSelectionModel().getSelectedItem());
//        timbradoFiltro = UtilLoaderBase.msjIda(comboBoxTimbrado.getSelectionModel().getSelectedItem());
//        tipoFactFiltro = UtilLoaderBase.msjIda(hashJsonTipoComp.get(comboBoxTipoFact.getSelectionModel().getSelectedItem()));
        nroFactFiltro = textFieldNroFact.getText();
        LocalDate fechaFact = datePickerFechaFact.getValue();
        fechaFactFiltro = fechaFact.toString();
        nroCajaFiltro = comboBoxCaja.getSelectionModel().getSelectedItem();
        timbradoFiltro = comboBoxTimbrado.getSelectionModel().getSelectedItem();
        tipoFactFiltro = hashJsonTipoComp.get(comboBoxTipoFact.getSelectionModel().getSelectedItem());
    }

    private void observandoCopia() {
        reseteandoDatosImpresion();
        boolean sgte = true;
        if (textFieldNroFact.getText().contentEquals("")) {
            sgte = false;
        } else if (comboBoxTipoFact.getSelectionModel().getSelectedItem().contentEquals("-")) {
            sgte = false;
        } else if (comboBoxCaja.getSelectionModel().getSelectedItem().contentEquals("-")) {
            sgte = false;
        } else if (comboBoxTimbrado.getSelectionModel().getSelectedItem().contentEquals("-")) {
            sgte = false;
        }
        if (!sgte) {
            nroFactFiltro = "";
            fechaFactFiltro = "";
            nroCajaFiltro = "";
            timbradoFiltro = "";
            tipoFactFiltro = "";
            mensajeAdv("NO OLVIDES COMPLETAR LOS CAMPOS OBLIGATORIOS.");
        } else {
            ofuscando();
            jsonFactCabHist = jsonFactCabHist();
            if (jsonFactCabHist != null) {
                if (!jsonFactCabHist.isEmpty()) {
                    JSONObject jsonFactCab = (JSONObject) jsonFactCabHist.get("facturaClienteCab");
                    JSONArray jsonDetArray = (JSONArray) jsonFactCab.get("facturaClienteDets");
                    jsonDetArray = recuperarArrayOrdenado(jsonDetArray);
                    detalleArticulo = "";
                    int size = jsonDetArray.size();
                    cantTotalArt = String.valueOf(size);
                    String cantArt = "";
                    String precioUnitario = "";
                    String precioDetalle = "";
                    for (Object jsonObjectDetArt : jsonDetArray) {
                        size--;
                        JSONObject jsonDetArt = (JSONObject) jsonObjectDetArt;
                        cantArt = " " + jsonDetArt.get("cantidad").toString() + "  x";
                        precioUnitario = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonDetArt.get("precio").toString()));
                        //ajuste de "columnas"**********************************************
                        int cantArcCha = cantArt.length();
                        while (cantArcCha < 8) {
                            cantArcCha++;
                            cantArt = cantArt + " ";
                        }
                        int precioUnitarioCha = precioUnitario.length();
                        while (precioUnitarioCha < 13) {
                            precioUnitarioCha++;
                            precioUnitario = precioUnitario + " ";
                        }
                        //ajuste de "columnas"**********************************************
                        //gravadas y artículos**********************************************
                        double totalDetalle = Double.parseDouble(jsonDetArt.get("cantidad").toString()) * Double.parseDouble(jsonDetArt.get("precio").toString());
                        if ((long) jsonDetArt.get("poriva") == 0) {
                            precioDetalle = numValidator.numberFormat("Gs ###,###.###", totalDetalle);
                        } else {
                            precioDetalle = numValidator.numberFormat("Gs ###,###.###", totalDetalle);
                        }
                        String descriArticulo = jsonDetArt.get("descripcion").toString();
                        if (descriArticulo.length() >= 17) {
                            descriArticulo = descriArticulo.substring(0, 17);
                        }
                        detalleArticulo = detalleArticulo + jsonDetArt.get("orden").toString() + ". " + jsonDetArt.get("codArticulo").toString() + " " + descriArticulo + "\n"
                                + "         " + cantArt + precioUnitario + precioDetalle;
                        if (size != 0) {
                            detalleArticulo = detalleArticulo + "\n";
                        }
                        //gravadas y artículos**********************************************
                    }
                    //MINORISTA 37 VARIABLES                
                    //DATOS CON FORMATO MONETARIO
                    grav10 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("grav10").toString()));
                    grav5 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("grav5").toString()));
                    exe = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("exenta").toString()));
                    liqIva5 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("liqui5").toString()));
                    liqIva10 = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("liqui10").toString()));
                    total = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("total").toString()));
                    neto = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCab.get("montoFactura").toString()));
                    descuento = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("descuento").toString()));
                    efectivo = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("efectivo").toString()));
                    if (jsonFactCabHist.get("giftcard") == null) {
                        jsonFactCabHist.put("giftcard", "");
                    }
                    giftcard = numValidator.numberFormat("Gs ###,###.###", jsonFactCabHist.get("giftcard").toString().equals("") ? Double.parseDouble(0 + "") : Double.parseDouble(jsonFactCabHist.get("giftcard").toString()));
                    tarjCred = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("tarjCred").toString()));
                    tarjDeb = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("tarjDeb").toString()));
                    cheque = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("cheque").toString()));
                    vale = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("vale").toString()));
                    asoc = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("asoc").toString()));
                    notCre = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("notCre").toString()));
                    retencion = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("retencion").toString()));
                    redondeo = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("redondeo").toString()));
                    vuelto = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("vuelto").toString()));
                    dolar = null;
                    peso = null;
                    realb = null;
                    if ((long) jsonFactCabHist.get("dolar") != 0) {
                        long cotDolar = Long.valueOf(jsonFactCabHist.get("cotiDolar").toString());
                        long monDolar = Long.valueOf(jsonFactCabHist.get("dolar").toString());
                        long cantDolar = monDolar / cotDolar;
                        String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("cotiDolar").toString()));
                        String cant = numValidator.numberFormat("US$ ###,###.###", Double.parseDouble(String.valueOf(cantDolar)));
                        String mon = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("dolar").toString()));
                        dolar = cant + " x " + cot + " = " + mon + "\n";
                    }
                    if ((long) jsonFactCabHist.get("peso") != 0) {
                        long cotPeso = Long.valueOf(jsonFactCabHist.get("cotiPeso").toString());
                        long monPeso = Long.valueOf(jsonFactCabHist.get("peso").toString());
                        long cantPeso = monPeso / cotPeso;
                        String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("cotiPeso").toString()));
                        String cant = numValidator.numberFormat("$ ###,###.###", Double.parseDouble(String.valueOf(cantPeso)));
                        String mon = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("peso").toString()));
                        peso = cant + " x " + cot + " = " + mon + "\n";
                    }
                    if ((long) jsonFactCabHist.get("realb") != 0) {
                        long cotReal = Long.valueOf(jsonFactCabHist.get("cotiReal").toString());
                        long monReal = Long.valueOf(jsonFactCabHist.get("realb").toString());
                        long cantReal = monReal / cotReal;
                        String cot = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("cotiReal").toString()));
                        String cant = numValidator.numberFormat("R$ ###,###.###", Double.parseDouble(String.valueOf(cantReal)));
                        String mon = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(jsonFactCabHist.get("realb").toString()));
                        realb = cant + " x " + cot + " = " + mon + "\n";
                    }
                    //DATOS CON FORMATO MONETARIO
                    //DATOS ALFANUMÉRICOS, FECHA
//                    Long timestampJSON = Long.valueOf(jsonFactCab.get("fechaEmision").toString());
                    Timestamp ts = Timestamp.valueOf(jsonFactCab.get("fechaEmision").toString());
//                    Timestamp ts = new Timestamp(timestampJSON);
                    fechaEmision = new SimpleDateFormat(patternFechaHora).format(ts);
                    Timestamp tsInicial = Utilidades.objectToTimestamp(jsonFactCabHist.get("fecInicial").toString() + " 00:00:00");
                    Timestamp tsVenc = Utilidades.objectToTimestamp(jsonFactCabHist.get("fecVencimiento").toString() + " 00:00:00");
                    fecInicial = new SimpleDateFormat(patternFecha).format(tsInicial);
                    fecVencimiento = new SimpleDateFormat(patternFecha).format(tsVenc);
                    empresa = jsonFactCabHist.get("empresa").toString();
                    sucursal = jsonFactCabHist.get("sucursal").toString();
                    ruc = jsonFactCabHist.get("ruc").toString();
                    telef = jsonFactCabHist.get("telef").toString();
                    direccion = jsonFactCabHist.get("direccion").toString();
                    nroTimbrado = jsonFactCabHist.get("nroTimbrado").toString();
                    nroCaja = jsonFactCabHist.get("nroCaja").toString();
                    nroFactura = Utilidades.patternFactura(jsonFactCab.get("nroFactura").toString());
                    usuAlta = jsonFactCab.get("usuAlta").toString();
                    rucCliente = jsonFactCabHist.get("rucCliente").toString();
                    cliente = jsonFactCabHist.get("cliente").toString();
                    //DATOS ALFANUMÉRICOS
                    int cantTotalArtCha = cantTotalArt.length();
                    while (cantTotalArtCha < 11) {
                        cantTotalArtCha++;
                        cantTotalArt = cantTotalArt + " ";
                    }
                    int descCha = descuento.length();
                    while (descCha < 11) {
                        descCha++;
                        descuento = descuento + " ";
                    }
                    viendoMinorista(empresa, sucursal, ruc, telef, direccion, nroTimbrado, fecInicial, fecVencimiento, nroCaja,
                            fechaEmision, nroFactura, usuAlta, detalleArticulo, grav10, grav5, exe, liqIva10, liqIva5, rucCliente, cliente,
                            cantTotalArt, total, descuento, neto, efectivo, tarjCred, tarjDeb, cheque, vale, asoc, notCre,
                            redondeo, vuelto, dolar, peso, realb, retencion, giftcard);
                    buttonImpFact.setDisable(false);
                    buttonAnterior.setDisable(false);
                    buttonSgte.setDisable(false);
                    radioDuplicado.setDisable(false);
                    radioDuplicadoSinValorC.setDisable(false);
                } else {
                    buttonImpFact.setDisable(true);
                    buttonAnterior.setDisable(true);
                    buttonSgte.setDisable(true);
//                    radioDuplicado.setDisable(true);
//                    radioDuplicadoSinValorC.setDisable(true);
                    nroFactFiltro = "";
                    fechaFactFiltro = "";
                    nroCajaFiltro = "";
                    timbradoFiltro = "";
                    tipoFactFiltro = "";
                    mensajeAdv("LA BÚSQUEDA NO GENERÓ NINGÚN RESULTADO.");
                }
            }
        }
    }

    private void limpiandoFiltros() {
        comboBoxTipoFact.getSelectionModel().select(0);//por el momento...
        comboBoxCaja.getSelectionModel().select(0);
        comboBoxTimbrado.getSelectionModel().select(0);
        textFieldNroFact.setText("");
        datePickerFechaFact.setValue(LocalDate.now());
        radioDuplicado.setSelected(true);
//        radioDuplicadoSinValorC.setDisable(true);
//        radioDuplicado.setDisable(true);
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, FACTURA CAB. HISTÓRICO -> GET
    private JSONObject jsonFactCabHist() {
        JSONParser parser = new JSONParser();
        JSONObject factCabHist = new JSONObject();
//        JSONObject jsonFactCab = (JSONObject) jsonFactCabHist.get("facturaClienteCab");
//                    JSONArray jsonDetArray = (JSONArray) jsonFactCab.get("facturaClienteDets");
        FacturaClienteCabHistorico facHistorico = fHistoricoDAO.consultar(nroFactFiltro, tipoFactFiltro, fechaFactFiltro, nroCajaFiltro, timbradoFiltro);
        if (facHistorico != null) {
            try {
                FacturaClienteCabHistoricoDTO fccHistoricoDTO = facHistorico.toFacturaClienteCabDTO();
                FacturaClienteCab fcc = facHistorico.getFacturaClienteCab();
                FacturaClienteCabDTO fccDTO = fccDAO.getById(fcc.getIdFacturaClienteCab()).toBDFacturaClienteCabDTO();
                Timestamp fecEmi = fccDTO.getFechaEmision();
//                Timestamp fecMod = fccDTO.getFechaMod();
                fccDTO.setFechaEmision(null);
                fccDTO.setFechaMod(null);
                fccDTO.setFacturaClienteCabHistorico(null);
                JSONObject jsonCabecera = (JSONObject) parser.parse(gson.toJson(fccDTO));
                jsonCabecera.put("fechaEmision", fecEmi.toString());

                List<FacturaClienteDet> listFcDet = fcDetDAO.listarPorFactura(fcc.getIdFacturaClienteCab());
                JSONArray array = new JSONArray();
                if (!listFcDet.isEmpty()) {
                    for (FacturaClienteDet facturaClienteDet : listFcDet) {
                        FacturaClienteDetDTO fcDetDTO = facturaClienteDet.toDescriFacturaClienteDetDTO();
                        JSONObject jsonDet = (JSONObject) parser.parse(gson.toJson(fcDetDTO));
                        array.add(jsonDet);
                    }
                    jsonCabecera.put("facturaClienteDets", array);
                }

                factCabHist = (JSONObject) parser.parse(gson.toJson(fccHistoricoDTO));
                factCabHist.put("facturaClienteCab", jsonCabecera);
            } catch (ParseException ex) {
                System.out.println("ERROR->> " + ex.getLocalizedMessage());
                System.out.println("ERROR->> " + ex.getMessage());
                System.out.println("ERROR->> " + ex.fillInStackTrace());
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/consultar/"
//                    + nroFactFiltro + "/" + tipoFactFiltro + "/" + fechaFactFiltro + "/" + nroCajaFiltro + "/" + timbradoFiltro);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                factCabHist = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return factCabHist;
    }
    //////READ, FACTURA CAB. HISTÓRICO -> GET

    //////READ, FACTURA CAB. HISTÓRICO CAJAS -> GET
    private String jsonCajas() {
        cajasJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        String result = "ok";
        List<Integer> listHist = fHistoricoDAO.listarCajaDistinct();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON CAJAS DISPONIBLES.";
        } else {
            for (Integer integer : listHist) {
                cajasJSONArray.add(integer);
                comboBoxCaja.getItems().add(integer.toString());
            }
        }
//        FacturaClienteCabHistorico facHistorico = fHistoricoDAO.consultar(nroFactFiltro, tipoFactFiltro, fechaFactFiltro, nroCajaFiltro, timbradoFiltro);
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/caja");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                cajasJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarCaja = false;
//                if (cajasJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON CAJAS DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "LA INFORMACIÓN SOLICITADA NO EXISTE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object cajaObj : cajasJSONArray) {
//            Long cajaNro = (Long) cajaObj;
//            comboBoxCaja.getItems().add(cajaNro.toString());
//        }
        return result;
    }
    //////READ, FACTURA CAB. HISTÓRICO CAJAS -> GET

    //////READ, FACTURA CAB. HISTÓRICO TIMBRADOS -> GET
    private String jsonTimbrados() {
        timbradosJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        String result = "ok";
        List<String> listHist = fHistoricoDAO.listarTimbDistinct();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON TIMBRADOS DISPONIBLES.";
        } else {
            for (String timbrado : listHist) {
                timbradosJSONArray.add(timbrado);
                comboBoxTimbrado.getItems().add(timbrado);
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCabHistorico/timbrado");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                timbradosJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarTimbrado = false;
//                if (timbradosJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON TIMBRADOS DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "LA INFORMACIÓN SOLICITADA NO EXISTE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object timbradoObj : timbradosJSONArray) {
//            String timbrado = (String) timbradoObj;
//            comboBoxTimbrado.getItems().add(timbrado);
//        }
        return result;
    }
    //////READ, FACTURA CAB. HISTÓRICO TIMBRADOS -> GET

    //////READ, TIPO COMPROBANTE -> GET
    private String jsonTipoComprobante() {
        tipoFactJSONArray = new JSONArray();
        JSONParser parser = new JSONParser();
        hashJsonTipoComp = new HashMap<>();
        String result = "ok";
        List<TipoComprobante> listHist = tipoComprobanteDAO.listar();
        if (listHist.isEmpty()) {
            result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
        } else {
//            JSONObject tipoFactJSONObj = (JSONObject) tipoFactObj;
//            hashJsonTipoComp.put(tipoFactJSONObj.get("descripcion").toString(), tipoFactJSONObj.get("idTipoComprobante").toString());
//            comboBoxTipoFact.getItems().add(tipoFactJSONObj.get("descripcion").toString());
            for (TipoComprobante comprobante : listHist) {
                try {
                    TipoComprobanteDTO compDTO = comprobante.toTipoComprobanteDTO();
                    compDTO.setFechaAlta(null);
                    compDTO.setFechaMod(null);
                    JSONObject factCabHist = (JSONObject) parser.parse(gson.toJson(compDTO));
                    tipoFactJSONArray.add(factCabHist);
                    hashJsonTipoComp.put(comprobante.getDescripcion(), comprobante.getIdTipoComprobante().toString());
                    comboBoxTipoFact.getItems().add(comprobante.getDescripcion());
                } catch (ParseException ex) {
                    System.out.println("ERROR->> " + ex.getLocalizedMessage());
                    System.out.println("ERROR->> " + ex.getMessage());
                    System.out.println("ERROR->> " + ex.fillInStackTrace());
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/tipoComprobante");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                tipoFactJSONArray = (JSONArray) parser.parse(inputLine);
//                cargarTipoFact = false;
//                if (tipoFactJSONArray.isEmpty()) {
//                    result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            result = "NO SE ENCONTRARON TIPOS DE COMPROBANTES DISPONIBLES.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            result = "NO SE ESTABLECIÓ CONEXIÓN CON EL SERVIDOR,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            result = "PARSE EXCEPTION,\n INTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
//        for (Object tipoFactObj : tipoFactJSONArray) {
//            JSONObject tipoFactJSONObj = (JSONObject) tipoFactObj;
//            hashJsonTipoComp.put(tipoFactJSONObj.get("descripcion").toString(), tipoFactJSONObj.get("idTipoComprobante").toString());
//            comboBoxTipoFact.getItems().add(tipoFactJSONObj.get("descripcion").toString());
//        }
        return result;
    }
    //////READ, TIPO COMPROBANTE -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //IMPRESIÓN MINORISTA -> -> -> *********************************************
    private void imprimiendoCopia() {
        if (!buttonImpFact.isDisable()) {
            usuAlta = Utilidades.encodingAlambrado(usuAlta);
            ticket = new Ticket();
            ticket.ticketReimpresion(empresa, sucursal, ruc, telef, direccion, nroTimbrado, fecInicial, fecVencimiento, nroCaja,
                    fechaEmision, nroFactura, usuAlta, detalleArticulo, grav10, grav5, exe, liqIva10, liqIva5, rucCliente, cliente,
                    cantTotalArt, total, descuento, neto, efectivo, tarjCred, tarjDeb, cheque, vale, asoc, notCre,
                    redondeo, vuelto, dolar, peso, realb, retencion, "DUPLICADO", radioDuplicado.isSelected(), giftcard);
            ticket.print();
        }
    }
    //IMPRESIÓN MINORISTA -> -> -> *********************************************

    //VISUALIZACIÓN MINORISTA
    private void viendoMinorista(String empresa, String suc, String ruc, String tel, String dir, String timb, String desde, String hasta,
            String nroCaja, String fH, String factNro, String user, String articulos, String grav10, String grav5, String exenta,
            String grav10Liq, String grav5Liq, String rucCliente, String cliente, String cant, String total, String desc,
            String neto, String efectivo, String tarjCred, String tarjDeb, String cheque, String vale, String asoc, String notCre, String redondeo,
            String vuelto, String dolar, String peso, String real, String retencion, String giftcard) {
        String ticket = this.contentTicket;
        ticket = ticket.replace("{{empresa}}", StringUtils.center(empresa, 80));
        ticket = ticket.replace("{{sucRuc}}", StringUtils.center(suc + " - RUC: " + ruc, 65));
//        TEL. {{tel}}-COMERCIO RAMOS GENERALES
        ticket = ticket.replace("{{tel}}", StringUtils.center("TEL. " + tel + "-COMERCIO RAMOS GENERALES", 60));
        ticket = ticket.replace("{{dir}}", StringUtils.center(dir, 65));
        ticket = ticket.replace("{{timb}}", timb);
        ticket = ticket.replace("{{desde}}", desde);
        ticket = ticket.replace("{{hasta}}", hasta);
        ticket = ticket.replace("{{cj}}", nroCaja);
        ticket = ticket.replace("{{fH}}", fH);
        ticket = ticket.replace("{{dup}}", StringUtils.center("**DUPLICADO**", 75));
        if (radioDuplicadoSinValorC.isSelected()) {
            ticket = ticket.replace("{{dupSinVC}}", "                       ** SIN VALOR COMERCIAL **\n");
        } else {
            ticket = ticket.replace("{{dupSinVC}}", "");
        }
        ticket = ticket.replace("{{factNro}}", factNro);
        ticket = ticket.replace("{{usuario}}", user);
        ticket = ticket.replace("{{art}}", articulos);
        ticket = ticket.replace("{{grav10}}", grav10);
        ticket = ticket.replace("{{grav5}}", grav5);
        ticket = ticket.replace("{{exenta}}", exenta);
        ticket = ticket.replace("{{grav10Liq}}", grav10Liq);
        ticket = ticket.replace("{{grav5Liq}}", grav5Liq);
        ticket = ticket.replace("{{rucCliente}}", rucCliente);
        ticket = ticket.replace("{{cliente}}", cliente);
        ticket = ticket.replace("{{cant}}", cant);
        ticket = ticket.replace("{{total}}", total);
        ticket = ticket.replace("{{desc}}", desc);
        ticket = ticket.replace("{{neto}}", neto);
        //**********************************************************************
        if (efectivo.contentEquals("Gs 0")) {
            efectivo = "";
        } else {
            efectivo = "EFECTIVO: " + efectivo + "\n";
        }
        if (giftcard.contentEquals("Gs 0")) {
            giftcard = "";
        } else {
            giftcard = "GIFTCARD: " + giftcard + "\n";
        }
        if (tarjCred.contentEquals("Gs 0")) {
            tarjCred = "";
        } else {
            tarjCred = "TARJ. CRED: " + tarjCred + "\n";
        }
        if (tarjDeb.contentEquals("Gs 0")) {
            tarjDeb = "";
        } else {
            tarjDeb = "TARJ. DEB: " + tarjDeb + "\n";
        }
        if (cheque.contentEquals("Gs 0")) {
            cheque = "";
        } else {
            cheque = "CHEQUE: " + cheque + "\n";
        }
        if (vale.contentEquals("Gs 0")) {
            vale = "";
        } else {
            vale = "VALE: " + vale + "\n";
        }
        if (asoc.contentEquals("Gs 0")) {
            asoc = "";
        } else {
            asoc = "ASOCIACION: " + asoc + "\n";
        }
        if (notCre.contentEquals("Gs 0")) {
            notCre = "";
        } else {
            notCre = "NOTA CREDITO: " + notCre + "\n";
        }
        if (retencion.contentEquals("Gs 0")) {
            retencion = "";
        } else {
            retencion = "RETENCION: " + retencion + "\n";
        }
        if (peso == null) {
            peso = "";
        }
        if (real == null) {
            real = "";
        }
        if (dolar == null) {
            dolar = "";
        }
        ticket = ticket.replace("{{efectivo}}", efectivo);
        ticket = ticket.replace("{{giftcard}}", giftcard);
        ticket = ticket.replace("{{tarjCred}}", tarjCred);
        ticket = ticket.replace("{{tarjDeb}}", tarjDeb);
        ticket = ticket.replace("{{cheque}}", cheque);
        ticket = ticket.replace("{{vale}}", vale);
        ticket = ticket.replace("{{asoc}}", asoc);
        ticket = ticket.replace("{{notCre}}", notCre);
        ticket = ticket.replace("{{dolar}}", dolar);
        ticket = ticket.replace("{{peso}}", peso);
        ticket = ticket.replace("{{real}}", real);
        ticket = ticket.replace("{{retencion}}", retencion);
        //**********************************************************************
        ticket = ticket.replace("{{redondeo}}", redondeo);
        ticket = ticket.replace("{{vuelto}}", vuelto);
        textAreaFact.clear();
        textAreaFact.setScrollTop(Double.MIN_VALUE);
        textAreaFact.setText(ticket);
        textAreaFact = (TextArea) AnimationFX.fadeNode(textAreaFact);
    }
    //VISUALIZACIÓN MINORISTA

    //*****************************ANIMACIONES**********************************
    private void animando() {
        labelReimp = (Label) AnimationFX.translationNode(labelReimp, 60, 0, 1100);
        gridPaneFiltro = (GridPane) AnimationFX.translationNode(gridPaneFiltro, -60, 0, 1200);
        hBoxButton = (HBox) AnimationFX.translationNode(hBoxButton, 60, 0, 1300);
        hBoxButtonNav = (HBox) AnimationFX.translationNode(hBoxButtonNav, 0, 20, 1400);
        buttonVolver = (Button) AnimationFX.translationNode(buttonVolver, -60, 0, 1500);
    }
    //*****************************ANIMACIONES**********************************

    private JSONArray recuperarArrayOrdenado(JSONArray jsonDetArray) {
        JSONParser parser = new JSONParser();
        org.json.JSONArray jsonArr = new org.json.JSONArray(jsonDetArray);
        org.json.JSONArray sortedJsonArray = new org.json.JSONArray();
        List<org.json.JSONObject> jsonValues = new ArrayList<>();
        for (int i = 0; i < jsonArr.length(); i++) {
            jsonValues.add(jsonArr.getJSONObject(i));
            Utilidades.log.info("JSON -> " + jsonArr.getJSONObject(i).toString());
        }
        Collections.sort(jsonValues, new Comparator<org.json.JSONObject>() {
            private static final String KEY_NAME = "idFacturaClienteDet";

            @Override
            public int compare(org.json.JSONObject a, org.json.JSONObject b) {
                String valA = new String();
                String valB = new String();

                try {
                    valA = String.valueOf(a.get(KEY_NAME));
                    valB = String.valueOf(b.get(KEY_NAME));
                } catch (JSONException e) {
                    Utilidades.log.error("ERROR JSONException REIMPRESION", e.fillInStackTrace());
                }

                return valA.compareTo(valB);
            }
        });
        for (int i = 0; i < jsonArr.length(); i++) {
            sortedJsonArray.put(jsonValues.get(i));
            Utilidades.log.info("-> " + jsonValues.get(i));
        }
        JSONArray jsonArray;
        try {
            jsonArray = (JSONArray) parser.parse(sortedJsonArray.toString());
            return jsonArray;
        } catch (ParseException ex) {
            return null;
        }

    }
}
