package com.javafx.controllers.caja;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.javafx.controllers.login.LoginFXMLController;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class CierreTurnoFXMLController extends BaseScreenController implements Initializable {

    private boolean patternMonto;
    String param;
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static JSONObject datos = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    ManejoLocal manejoLocal = new ManejoLocal();
    @FXML
    private AnchorPane anchorPaneApertura;
    @FXML
    private Pane panelMontoApertura;
    @FXML
    private Button btnAceptar;
    @FXML
    private Button btnCancelar;
    @FXML
    private Label labelAutenticacion1;
    @FXML
    private Label labelAutenticacion11;
    @FXML
    private TextField txtMontoCierre;
    @FXML
    private Button buttonVolver;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnAceptarAction(ActionEvent event) {
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
    }

    @FXML
    private void anchorPaneAperturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void cargandoInicial() {
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
//        if (DatosEnCaja.getDatos() != null) {
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            if (DatosEnCaja.getFacturados() != null) {
//                fact = DatosEnCaja.getFacturados();
//            } else {
//                fact = new JSONObject();
//            }
//        }
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField();
    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/caja/CierreTurnoFXML.fxml", 425, 201, false);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
        if (keyCode == event.getCode().F1) {
            if (!txtMontoCierre.getText().equals("")) {
                cerrarTurno();
                DatosEnCaja.setFacturados(null);
                DatosEnCaja.setFacturados(null);
                users = null;
                fact = null;
                DatosEnCaja.setUsers(null);
                datos.put("modSup", true);
                DatosEnCaja.setDatos(datos);
                actualizarDatosNuevo();
            } else {
                mensajeAlerta("El campo Dinero no debe quedar vacío.");
            }
        }
        if (keyCode == event.getCode().F2) {
            if (!txtMontoCierre.getText().equals("")) {
                arquear();
                DatosEnCaja.setFacturados(null);
                DatosEnCaja.setFacturados(null);
                users = null;
                fact = null;
                DatosEnCaja.setUsers(null);
                datos.put("modSup", true);
                DatosEnCaja.setDatos(datos);
                actualizarDatos();
            } else {
                mensajeAlerta("El campo Dinero no debe quedar vacío.");
            }
        }
    }

    private void mensajeAlerta(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void listenTextField() {
        txtMontoCierre.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMontoCierre.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMontoCierre.setText(param);
                            txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMontoCierre.setText("");
                            txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMontoCierre.setText(param);
                                    txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMontoCierre.setText("");
                                txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMontoCierre.setText(oldValue);
                            txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMontoCierre.setText(param);
                        txtMontoCierre.positionCaret(txtMontoCierre.getLength());
                    });
                }
            }
        });
    }

    private void cerrarTurno() {
        DatosEnCaja.setFacturados(null);
        users = null;
        fact = null;
        DatosEnCaja.setUsers(null);
        actualizarDatos();
        LoginFXMLController.setLlamarTask(false);
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void arquear() {
        DatosEnCaja.setFacturados(null);
        users = null;
        fact = null;
        DatosEnCaja.setUsers(null);
        actualizarDatos();
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
        this.sc.loadScreen("/vista/caja/loginSupervisorFXML.fxml", 540, 312, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

}
