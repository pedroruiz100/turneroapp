/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.core.domain.Empresa;
import com.peluqueria.core.domain.Iva;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.Unidad;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloProveedorDAO;
import com.peluqueria.dao.EmpresaDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ArticuloNf1TipoDTO;
import com.peluqueria.dto.ArticuloNf2SfamiliaDTO;
import com.peluqueria.dto.ArticuloNf3SseccionDTO;
import com.peluqueria.dto.ArticuloNf4Seccion1DTO;
import com.peluqueria.dto.ArticuloNf5Seccion2DTO;
import com.peluqueria.dto.ArticuloNf6Secnom6DTO;
import com.peluqueria.dto.ArticuloNf7Secnom7DTO;
import com.peluqueria.dto.IvaDTO;
import com.peluqueria.dto.MarcaDTO;
import com.peluqueria.dto.Nf1TipoDTO;
import com.peluqueria.dto.Nf2SfamiliaDTO;
import com.peluqueria.dto.Nf3SseccionDTO;
import com.peluqueria.dto.Nf4Seccion1DTO;
import com.peluqueria.dto.Nf5Seccion2DTO;
import com.peluqueria.dto.Nf6Secnom6DTO;
import com.peluqueria.dto.Nf7Secnom7DTO;
import com.peluqueria.dto.SeccionDTO;
import com.peluqueria.dto.SeccionSubDTO;
import com.peluqueria.dto.UnidadDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.estetica.FacturaDeVentaEsteticaFXMLController.toaster;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf1TipoDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf2SfamiliaDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf3SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf4SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf5SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf6SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.anf7SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf2SfamiliaDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf3SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf4SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf5SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf6SeccionDAO;
import static com.javafx.controllers.stock.VerificarPrecioFXMLController.nf7SeccionDAO;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.peluqueria.core.domain.ArticuloProveedor;
import com.peluqueria.core.domain.Seccion;
import com.peluqueria.dao.SeccionDAO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Random;
import javafx.beans.property.SimpleObjectProperty;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class ArticulosFXMLController extends BaseScreenController implements Initializable {

    private boolean alert;
    public static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private Date date = new Date();
    long precioMinAnt = 0l;
    long precioMayAnt = 0l;

    boolean cambioCantidad = false;

    private List<JSONObject> articuloModalList;
    private HashMap<String, Image> mapeoArticulo;
    private ObservableList<JSONObject> articuloModalData;
//    String precioMayAnt = "";
    @Autowired
    ArticuloDAO artDAO;
    @Autowired
    EmpresaDAO empDAO;
    @Autowired
    private ExistenciaDAO existenciaDAO;
    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
//    boolean precMin = false;
//    boolean precMay = false;
//    boolean increMin = false;
//    boolean increMay = false;
    private boolean patternMonto;
    String param;
    JSONParser parser = new JSONParser();
    JSONObject artBuscado = new JSONObject();
    //campos númericos
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();
    @Autowired
    ManejoLocalDAO manejoDAO;
    @Autowired
    ArticuloProveedorDAO articuloProveedorDAO;
    @Autowired
    ProveedorDAO proveedorDAO;
    @Autowired
    SeccionDAO seccionDAO;
    ManejoLocal manejoLocal = new ManejoLocal();
    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";
    final KeyCombination altN = new KeyCodeCombination(KeyCode.N, KeyCombination.ALT_DOWN);
    final KeyCombination altG = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN);

    private List<JSONObject> depositoList = new ArrayList<>();
    private ObservableList<JSONObject> depositoData;

    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtAreaDesc;
    @FXML
    private TextField txtPack;
    @FXML
    private ComboBox<String> cbEmpresa;
    @FXML
    private Button btnProveedor;
    @FXML
    private TextField txtProveedor;
    @FXML
    private TextField txtMarca;
//    private ComboBox<String> cbFamilia;
    private HashMap<String, JSONObject> hashFamilia;
    @FXML
    private ComboBox<String> cbSubFamilia;
    private HashMap<String, JSONObject> hashSubFamilia;
    @FXML
    private ComboBox<String> cbCategoria;
    private HashMap<String, JSONObject> hashCategoria;
    @FXML
    private ComboBox<String> cbSubCategoria;
    private HashMap<String, JSONObject> hashSubCategoria;
    @FXML
    private ComboBox<String> cbClase;
    private HashMap<String, JSONObject> hashClase;
    @FXML
    private ComboBox<String> cbSubClase;
    private HashMap<String, JSONObject> hashSubClase;
    @FXML
    private ComboBox<String> cbSeccion;
    private HashMap<String, JSONObject> hashSeccion;
//    private TextField txtCostoMonedaExt;
    @FXML
    private TextField txtCostoLocales;
//    private TextField txtIva;
//    private TextField txtPorcDescProve;
//    private TextField txtSensor;
//    private TextField txtPorcDesc;
//    private TextField txtOtrosGastos;
    @FXML
    private TextField txtPorcIncremParana01;
//    private TextField txtPorcIncremParana02;
//    private TextField PocIncremMayorista;
//    private TextField txtPorcIncremVarios;
//    private TextField txtMonExt;
//    private TextField txtCostoOrigen;
//    private TextField txtPorcentajeDesc;
//    private TextField txtPorcAduanero;
//    private TextField txtCodProv01;
//    private TextField txtCodProv02;
    @FXML
    private TextField txtProv01;
    @FXML
    private TextField txtProv02;
//    private TextField txtCodProv03;
    @FXML
    private TextField txtProv03;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnDescartar;
    @FXML
    private Button btnBuscar;
    @FXML
    private Button btnTransferirLambare;
    @FXML
    private Button btnTransferirYeruti;
//    private TextField txtFabricado;
//    private TextField txtCantFab01;
//    private TextField txtMonFab01;
//    private TextField txtCantFab02;
//    private TextField txtMonFab02;
//    private TextField txtMonFab03;
//    private TextField txtCantFab03;
//    private TextField txtCantFab04;
//    private TextField txtMonFab04;
//    private TextField txtCantFab05;
//    private TextField txtMonFab05;
//    private TextField txtMonFab06;
//    private TextField txtCantFab06;
//    private TextField txtMonFab07;
//    private TextField txtCantFab07;
    @FXML
    private TextField txtCotizacion;
    @FXML
    private ComboBox<String> chkMonedaExt;
//    private ComboBox<String> chkPromo;
//    private ComboBox<String> chkPromoOutlet;
    @FXML
    private CheckBox chkProdImportado;
    @FXML
    private CheckBox chkProdVigente;
    @FXML
    private CheckBox chkNoStockeable;
    @FXML
    private CheckBox chkArtOculto;
    @FXML
    private CheckBox chkArtCompra;
    @FXML
    private CheckBox chkLeerBalanza;
    @FXML
    private Button btnSalir;
    @FXML
    private AnchorPane anchorPaneArticulos;
    @FXML
    private Pane secondPane;
    @FXML
    private TabPane tabPaneSeleccionMenu;
    @FXML
    private Tab tabArticulos;
    @FXML
    private TitledPane titledPaneCaja;
    @FXML
    private AnchorPane anchorPaneCaja;
    @FXML
    private Tab tabCuponera;
    @FXML
    private TitledPane titledPaneCuponera;
    @FXML
    private AnchorPane anchorPaneCuponera;
    @FXML
    private TextField txtCotizacion1;
    @FXML
    private TextField txtCotizacion11;
    @FXML
    private TextField txtCodigo1;
    @FXML
    private TextField txtAreaDesc1;
    @FXML
    private ComboBox<String> cbEmpresa11;
    @FXML
    private ComboBox<String> cbEmpresa111;
    @FXML
    private ComboBox<String> cbEmpresa1111;
    private TextField txtCotizacion12;
    @FXML
    private TextField txtCotizacion1211;
    @FXML
    private TextField txtCotizacion12111;
    @FXML
    private TextField txtCotizacion121111;
    @FXML
    private TextField txtCotizacion1211111;
    @FXML
    private TextField txtCotizacion12111111;
    @FXML
    private TextField txtCotizacion13;
    @FXML
    private TableView<JSONObject> tableViewDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo;
    @FXML
    private Button btnProveedor1;
    @FXML
    private TextField txtCostoOrigen11;
    @FXML
    private TextField txtCostoOrigen111;
    @FXML
    private TextField txtCostoOrigen12;
    @FXML
    private TextField txtCostoOrigen112;
    @FXML
    private TextField txtCotizacion2;
    @FXML
    private TextField txtCotizacion21;
    @FXML
    private ComboBox<String> cbImp;
    @FXML
    private TextField txtPrecioMin;
    @FXML
    private TextField txtPrecioMay;
    @FXML
    private ComboBox<String> cbTipoFiscal;
    @FXML
    private Button btnProveedorEstandar;
    @FXML
    private Button btnProveedor2;
    @FXML
    private TextField txtCod01;
    @FXML
    private TextField txtCod02;
    @FXML
    private TextField txtCod03;
    @FXML
    private CheckBox chkProdImportado1;
    @FXML
    private CheckBox chkArtOculto1;
    @FXML
    private CheckBox chkArtCompra1;
    @FXML
    private CheckBox chkLeerBalanza2;
    @FXML
    private CheckBox chkLeerBalanza1;
    @FXML
    private TextField txtCotizacion121111111;
    @FXML
    private TextField txtImagenSeleccionada;
    @FXML
    private Button btnImagenSeleccionada;

    public static Stage parentStage;
    @FXML
    private TextField txtPrec01;
    @FXML
    private TextField txtPrec02;

    boolean min = false;
    boolean may = false;
    boolean local = false;
    Image image;
    String datoGuardar;

    @FXML
    private CheckBox chkServiicio;
    @FXML
    private CheckBox chkParaVenta;
    @FXML
    private ComboBox<String> cbSeccionData;
    @FXML
    private TextField txtStockMin;
    @FXML
    private TextField txtStockMax;
    @FXML
    private TextField txtStockActual;
    @FXML
    private TextField txtDescProveedor;
    @FXML
    private Button btnGenerarCod;
    @FXML
    private ImageView imageProducto;
    @FXML
    private AnchorPane anchorPaneBuscarProducto;
    @FXML
    private Label labelClienteBuscar;
    @FXML
    private TableView<JSONObject> tableViewProducto;
    @FXML
    private TableColumn<JSONObject, ImageView> tableColumnArticuloModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigoModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcionModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnSeccioModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnPrecioModal;
    @FXML
    private Label labelClienteBuscar1;
    @FXML
    private TextField txtCantidadModal;
    @FXML
    private Button buttonAnhadirProducto;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private TextField txtBuscarCodProducto;
    @FXML
    private Label lableApellido;
    @FXML
    private TextField txtBuscarDesriProducto;
    @FXML
    private ComboBox<String> cbSeccionData1;
    @FXML
    private Button btnBuscarProducto;
    @FXML
    private TextField txtObservacion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidadModal;
    @FXML
    private Pane panel01;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
//        cargarComponentes();
        limpiar();
        desHabilitarBotones();
//        habilitarTodo();
        txtCodigo.requestFocus();
//        btnGuardar.setDisable(false);
        btnDescartar.setDisable(false);
        chkServiicio.setSelected(true);
        txtStockMin.setText("1");
        txtStockMax.setText("1");
//        btnBuscar.setDisable(false);
        btnGenerarCod.setDisable(false);
    }

    @FXML
    private void btnEditarAction(ActionEvent event) {
        actualizarArticulo();
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        registrarArticulo();
    }

    @FXML
    private void btnDescartarAction(ActionEvent event) {
        limpiar();
        desHabilitarBotones();
//        deshabilitarTodo();
        btnNuevo.setDisable(false);
        btnDescartar.setDisable(false);
//        btnBuscar.setDisable(true);
    }

    @FXML
    private void btnBuscarAction(ActionEvent event) {
        desHabilitarBotones();
        txtCodigo.requestFocus();
//        btnEditar.setDisable(false);
        btnDescartar.setDisable(false);
        txtCodigo.requestFocus();
    }

    @FXML

    private void btnTransferirLambareAction(ActionEvent event) {
    }

    @FXML
    private void btnTransferirYerutiAction(ActionEvent event) {
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAperturaEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneArticulos.getChildren()
                .get(anchorPaneArticulos.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void cargandoInicial() {
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            } else {
                fact = new JSONObject();
            }
        }

        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
//        listenTextField();
        alert = false;
//        ubicandoContenedorSecundario();
//        listenerCampos();
        cargarComponentes();
        limpiar();

        cargarSecciones();
        cargarSeccionesData();
//        listenTextField(txtPrec01);
//        listenTextField(txtPrec02);
        desHabilitarBotones();
        ubicandoContenedorSecundario();
        btnNuevo.setDisable(false);
        btnDescartar.setDisable(false);
//        btnBuscar.setDisable(false);
        depositoList = new ArrayList<>();

        cbEmpresa11.getSelectionModel().select(0);
        cbEmpresa111.getSelectionModel().select(0);
        cbEmpresa1111.getSelectionModel().select(0);
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();

        tableViewProducto.setEditable(true);
        tableColumnCantidadModal.setCellFactory(TextFieldTableCell.forTableColumn());
        tableColumnCantidadModal.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {

                int row = t.getTablePosition().getRow();

//                ((Lin) t.getTableView().getItems().get(row)).nameProperty().setValue(t.getNewValue());
                tableViewProducto.getSelectionModel().select(row + 1);
//                tv.edit(row + 1, name);  // <--- here not work edition next line
            }
        });
        cambioCantidad = false;
//        deshabilitarTodo();
//        ImageView iv = (ImageView) findViewById(R.id.left);
    }

    public void cargarSeccionesData() {
        cbSeccionData1.getItems().clear();
        cbSeccionData1.getItems().add("*** NINGUNO ***");
        List<Seccion> listSeccion = seccionDAO.listarTodosTRUE();
        if (listSeccion != null) {
            for (Seccion obj : listSeccion) {
                //                    json = (JSONObject) parser.parse(obj.toString());listSeccion
                cbSeccionData1.getItems().add(obj.getDescripcion().toUpperCase());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
            }
        }
        cbSeccionData1.getSelectionModel().select(0);
    }

    public void cargarSecciones() {
        cbSeccion.getItems().add("SELECCIONE SECCION");
        List<Seccion> listSeccion = seccionDAO.listarTodosTRUE();
        if (listSeccion != null) {
            for (Seccion obj : listSeccion) {
                //                    json = (JSONObject) parser.parse(obj.toString());listSeccion
                cbSeccionData.getItems().add(obj.getDescripcion().toUpperCase());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
            }
        }
    }

    private void listenTextFieldMay(String num) {
        Platform.runLater(() -> {
            try {
                numValidator = new NumberValidator();
                String monto = numValidator.numberValidator(txtPrecioMay.getText());
                txtPrecioMay.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
                txtPrecioMay.positionCaret(txtPrecioMay.getLength());
            } catch (Exception e) {
            } finally {
            }
        });
//        txtPrecioMay.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtPrecioMay.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
//            System.out.println("MAY: " + newValue);
//            String newV = "";
//            String oldV = "";
//            if (!newValue.contentEquals("")) {
//                newV = numValidator.numberValidator(newValue);
//                oldV = numValidator.numberValidator(oldValue);
//                long lim = -1l;
//                boolean limite = true;
//                if (!newV.contentEquals("")) {//límite del monto...
//                    if (newV.length() < 19) {
//                        lim = Long.valueOf(newV);
//                        if (lim > 0 && lim < 2147483647) {
//                            limite = false;
//                        }
//                    }
//                }
//                if (limite) {
//                    if (!newValue.contentEquals("") && !newV.contentEquals("")) {
//                        if (patternMonto) {
//                            if (oldV.length() != 0) {
//                                param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                            } else {
//                                param = oldValue;
//                            }
//                            patternMonto = false;
//                        } else {
//                            patternMonto = true;
//                            param = oldValue;
//                        }
//                        Platform.runLater(() -> {
//                            txtPrecioMay.setText(param);
//                            txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            txtPrecioMay.setText("1");
//                            txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                        });
//                    }
//                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                    if (!oldValue.contentEquals("")) {
//                        if (!newValue.contentEquals("")) {
//                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                                if (patternMonto) {
//                                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                                    patternMonto = false;
//                                } else {
//                                    patternMonto = true;
//                                    param = oldValue;
//                                }
//                                Platform.runLater(() -> {
//                                    txtPrecioMay.setText(param);
//                                    txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                                });
//                            }
//                        } else {
//                            Platform.runLater(() -> {
//                                txtPrecioMay.setText("1");
//                                txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtPrecioMay.setText(oldValue);
//                            txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                        });
//                    }
//                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                    if (patternMonto) {
//                        param = numValidator.numberFormat("###,###.###", Double.parseDouble(newV));
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                    }
//                    Platform.runLater(() -> {
//                        txtPrecioMay.setText(param);
//                        txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                    });
//                }
//            }
//        });
    }

    private void listenTextFieldMin() {
//        txtPrecioMin.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//        if (oldValue.length() == 0) {
//            patternMonto = true;
//        }
//        System.out.println("MIN: " + newValue);
//        String newV = "";
//        String oldV = "";
//        if (!newValue.contentEquals("")) {
//            newV = numValidator.numberValidator(newValue);
//            oldV = numValidator.numberValidator(oldValue);
//            long lim = -1l;
//            boolean limite = true;
//            if (!newV.contentEquals("")) {//límite del monto...
//                if (newV.length() < 19) {
//                    lim = Long.valueOf(newV);
//                    if (lim > 0 && lim < 2147483647) {
//                        limite = false;
//                    }
//                }
//            }
//            if (limite) {
//                if (!newValue.contentEquals("") && !newV.contentEquals("")) {
//                    if (patternMonto) {
//                        if (oldV.length() != 0) {
//                            param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                        } else {
//                            param = oldValue;
//                        }
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = oldValue;
//                    }
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(param);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText("");
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                if (!oldValue.contentEquals("")) {
//                    if (!newValue.contentEquals("")) {
//                        if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                            if (patternMonto) {
//                                param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                                patternMonto = false;
//                            } else {
//                                patternMonto = true;
//                                param = oldValue;
//                            }
//                            Platform.runLater(() -> {
//                                txtPrecioMin.setText(param);
//                                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtPrecioMin.setText("");
//                            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                        });
//                    }
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(oldValue);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                if (patternMonto) {
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(newV));
//                    patternMonto = false;
//                } else {
//                    patternMonto = true;
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                }
//                Platform.runLater(() -> {
//                    txtPrecioMin.setText(param);
//                    txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                });
//            }
//        }
//        try {
//            numValidator = new NumberValidator();
//            String monto = numValidator.numberValidator(txtPrecioMin.getText());
//            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
//            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//        } catch (Exception e) {
//        } finally {
//        }

        Platform.runLater(() -> {
            try {
                numValidator = new NumberValidator();
                String monto = numValidator.numberValidator(txtPrecioMin.getText());
                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
            } catch (Exception e) {
            } finally {
            }
        });

//        });
    }

    private void listenTextFieldLocal() {
//        try {
//            numValidator = new NumberValidator();
//            String monto = numValidator.numberValidator(txtCostoLocales.getText());
//            txtCostoLocales.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
//            txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//        } catch (Exception e) {
//        } finally {
//        }

        Platform.runLater(() -> {
            try {
                numValidator = new NumberValidator();
                String monto = numValidator.numberValidator(txtCostoLocales.getText());
                txtCostoLocales.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
                txtCostoLocales.positionCaret(txtCostoLocales.getLength());
            } catch (Exception e) {
            } finally {
            }
        });

//        txtCostoLocales.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtCostoLocales.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
//            System.out.println("LOCAL: " + newValue);
//            String newV = "";
//            String oldV = "";
//            if (!newValue.contentEquals("")) {
//                newV = numValidator.numberValidator(newValue);
//                oldV = numValidator.numberValidator(oldValue);
//                long lim = -1l;
//                boolean limite = true;
//                if (!newV.contentEquals("")) {//límite del monto...
//                    if (newV.length() < 19) {
//                        lim = Long.valueOf(newV);
//                        if (lim > 0 && lim < 2147483647) {
//                            limite = false;
//                        }
//                    }
//                }
//                if (limite) {
//                    if (!newValue.contentEquals("") && !newV.contentEquals("")) {
//                        if (patternMonto) {
//                            if (oldV.length() != 0) {
//                                param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                            } else {
//                                param = oldValue;
//                            }
//                            patternMonto = false;
//                        } else {
//                            patternMonto = true;
//                            param = oldValue;
//                        }
//                        Platform.runLater(() -> {
//                            txtCostoLocales.setText(param);
//                            txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            txtCostoLocales.setText("");
//                            txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                        });
//                    }
//                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                    if (!oldValue.contentEquals("")) {
//                        if (!newValue.contentEquals("")) {
//                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                                if (patternMonto) {
//                                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                                    patternMonto = false;
//                                } else {
//                                    patternMonto = true;
//                                    param = oldValue;
//                                }
//                                Platform.runLater(() -> {
//                                    txtCostoLocales.setText(param);
//                                    txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                                });
//                            }
//                        } else {
//                            Platform.runLater(() -> {
//                                txtCostoLocales.setText("");
//                                txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtCostoLocales.setText(oldValue);
//                            txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                        });
//                    }
//                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                    if (patternMonto) {
//                        param = numValidator.numberFormat("###,###.###", Double.parseDouble(newV));
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                    }
//                    Platform.runLater(() -> {
//                        txtCostoLocales.setText(param);
//                        txtCostoLocales.positionCaret(txtCostoLocales.getLength());
//                    });
//                }
//            }
//        });
    }

//    private void listenIncrementoMin(KeyEvent event) {
//        numValidator = new NumberValidator();
//        if (event.getCode().isDigitKey()) {
////            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
//            try {
////                        String dato = newValue;
//                long c = (Long.parseLong(numValidator.numberValidator(txtPorcIncremParana01.getText())) + 100) * Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText()));
//                long result = Math.round((c / 100));
//                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(result + "")));
//            } catch (Exception e) {
//                txtPrecioMin.setText("0");
//            }
////                });
////            });
////        } else if (event.getCode().isLetterKey()) {
////            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
////                    if (!convertirEntero(txtPorcIncremParana01)) {
////                        txtPorcIncremParana01.setText("0");
////                    }
////                });
////            });
//        }
//    }
    private void calcularPrecioMay(String value) {
        numValidator = new NumberValidator();
        try {
            long c = (Long.parseLong(numValidator.numberValidator(value)) + 100) * Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText()));
            long result = Math.round((c / 100));
            txtPrecioMay.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(result + "")));
//            precMay = true;
        } catch (Exception e) {
            txtPrecioMay.setText("1");
        }
    }

    private void listenIncrementoMay(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtCostoOrigen12.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!increMay) {
        calcularPrecioMay(txtCostoOrigen12.getText());
//            txtCostoOrigen12.positionCaret(txtCostoOrigen12.getLength());
//                    } else {
//                        increMay = false;
//                    }
//                });
//            });
//        }
    }

    private void calcularPrecioMin(String value) {
        numValidator = new NumberValidator();
        try {
            long c = (Long.parseLong(numValidator.numberValidator(value)) + 100) * Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText()));
            long result = Math.round((c / 100));
            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(result + "")));
//            precMin = true;
        } catch (Exception e) {
            txtPrecioMin.setText("0");
        }
    }

    private void listenIncrementoMin(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!increMin) {
        calcularPrecioMin(txtPorcIncremParana01.getText());
//                        txtPorcIncremParana01.positionCaret(txtPorcIncremParana01.getLength());
//                    } else {
//                        increMin = false;
//                    }
//
//                });
//            });
//        }
    }

    private boolean convertirEntero(TextField monto) {
        numValidator = new NumberValidator();
        String valor = monto.getText().replace("Gs ", "");
        valor = valor.replace(",", "");
        try {
            Integer.parseInt(valor);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String asignandoVariables(TextField monto) {
        numValidator = new NumberValidator();
        String valor = numValidator.numberValidator(monto.getText());
        if (!valor.equals("")) {
            int size = valor.length();
            if (size >= 4) {
                valor = valor.replace(",", "");
                valor = valor.replace(".", "");
            }
        }
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        String dato = formatea.format(Integer.parseInt(valor));
        return dato;
    }

//    private void listenPrecioMin(KeyEvent event) {
//        numValidator = new NumberValidator();
////        if (event.getCode().isDigitKey()) {
////            txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
//        try {
//            String dato = numValidator.numberValidator(txtPrecioMin.getText());
////                        txtPrecioMin.setText(dato);
////            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//
//            long c = Long.parseLong(dato) * 100;
//            long result = Math.round((c / Long.parseLong(txtCostoLocales.getText())));
//            txtPorcIncremParana01.setText(result + "");
//        } catch (Exception e) {
//            txtPrecioMin.setText("0");
//        }
////                });
////            });
////        } else if (event.getCode().isLetterKey()) {
////            txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
////                    if (!convertirEntero(txtPrecioMin)) {
////                        txtPrecioMin.setText("0");
////                    }
////                });
////            });
////        }
//    }
//    private void listenPrecioMay(KeyEvent event) {
//        numValidator = new NumberValidator();
////        if (event.getCode().isDigitKey()) {
////            txtPrecioMay.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
//        try {
//            String dato = numValidator.numberValidator(txtPrecioMay.getText());
////                        txtPrecioMay.setText(dato);
////            txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//
//            long c = Long.parseLong(dato) * 100;
//            long result = Math.round((c / Long.parseLong(txtCostoLocales.getText())));
//            txtCostoOrigen12.setText(result + "");
//        } catch (Exception e) {
//            txtPrecioMay.setText("1");
//        }
////                });
////            });
////        } else if (event.getCode().isLetterKey()) {
////            txtPrecioMay.textProperty().addListener((observable, oldValue, newValue) -> {
////                Platform.runLater(() -> {
////                    if (!convertirEntero(txtPrecioMay)) {
////                        txtPrecioMay.setText("1");
////                    }
////                });
////            });
////        }
//    }
    private void volviendo() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/login/LoginFXML.fxml", 599, 245, false);
    }

    //Apartado 4 - AVISOS
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtAreaDesc.isFocused()) {
                System.out.println("NADA DE MOMENTO");
            } else if (anchorPaneBuscarProducto.isVisible()) {
                if (tableViewProducto.getSelectionModel().getSelectedIndex() >= 0) {
                    if (!cambioCantidad) {
                        seleccionarProd();
                    } else {
                        cambioCantidad = false;
                    }
                }
            } else if (txtCodigo.isFocused()) {
                busquedaArticulo();
            } else if (txtPrecioMin.isFocused()) {
                listenPrecioMin(event);
            } else if (txtPrecioMay.isFocused()) {
                listenPrecioMay(event);
            } else if (txtPorcIncremParana01.isFocused()) {
                listenIncrementoMin(event);
            } else if (txtCostoOrigen12.isFocused()) {
                listenIncrementoMay(event);
            } else if (txtAreaDesc.isFocused()) {
                txtCodigo.requestFocus();
            }
        }
        if (altN.match(event)) {
            if (!btnNuevo.isDisabled()) {
                limpiar();
                desHabilitarBotones();
//                habilitarTodo();
                txtCodigo.requestFocus();
//                btnGuardar.setDisable(false);
                btnDescartar.setDisable(false);
                chkServiicio.setSelected(true);
                txtStockMin.setText("1");
                txtStockMax.setText("1");
//                btnBuscar.setDisable(false);
                btnGenerarCod.setDisable(false);
            }
        }
        if (altG.match(event)) {
//            limpiar();
//            desHabilitarBotones();
//            btnNuevo.setDisable(true);
//            btnGuardar.setDisable(false);
//            btnDescartar.setDisable(false);
//            btnBuscar.setDisable(false);
            txtCodigo.setText(getSaltString());
            txtCodigo.requestFocus();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (anchorPaneBuscarProducto.isVisible()) {
                cerrarBusquedaProducto();
                txtCodigo.requestFocus();
            } else {
                volviendo();
            }
        }
        if (keyCode == event.getCode().F1) {
            if (anchorPaneBuscarProducto.isVisible()) {
                buscarProductoModal();
            } else {
                if (!btnBuscar.isDisabled()) {
                    desHabilitarBotones();
                    txtCodigo.requestFocus();
//                    btnEditar.setDisable(false);
                    btnDescartar.setDisable(false);
                    txtCodigo.requestFocus();
                }
            }
        }
        if (keyCode == event.getCode().F5) {
            if (!btnEditar.isDisabled()) {
                actualizarArticulo();
            }
        }
        if (keyCode == event.getCode().F2) {
            if (!btnGuardar.isDisabled()) {
                registrarArticulo();
            }
        }
        if (keyCode == event.getCode().F8) {
            limpiar();
            desHabilitarBotones();
//            deshabilitarTodo();
            btnNuevo.setDisable(false);
            btnDescartar.setDisable(false);
//            btnBuscar.setDisable(true);
//            txtCodigo.setText(getSaltString());
        }
    }

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
//    private void listenTextField() {
//        txtMontoApertura.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtMontoApertura.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
//            String newV = "";
//            String oldV = "";
//            if (!newValue.contentEquals("")) {
//                newV = numValidator.numberValidator(newValue);
//                oldV = numValidator.numberValidator(oldValue);
//                long lim = -1l;
//                boolean limite = true;
//                if (!newV.contentEquals("")) {//límite del monto...
//                    if (newV.length() < 19) {
//                        lim = Long.valueOf(newV);
//                        if (lim > 0 && lim < 2147483647) {
//                            limite = false;
//                        }
//                    }
//                }
//                if (limite) {
//                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
//                        if (patternMonto) {
//                            if (oldV.length() != 0) {
//                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                            } else {
//                                param = oldValue;
//                            }
//                            patternMonto = false;
//                        } else {
//                            patternMonto = true;
//                            param = oldValue;
//                        }
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(param);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText("");
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                    if (!oldValue.contentEquals("")) {
//                        if (!newValue.contentEquals("Gs ")) {
//                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                                if (patternMonto) {
//                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                                    patternMonto = false;
//                                } else {
//                                    patternMonto = true;
//                                    param = oldValue;
//                                }
//                                Platform.runLater(() -> {
//                                    txtMontoApertura.setText(param);
//                                    txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                                });
//                            }
//                        } else {
//                            Platform.runLater(() -> {
//                                txtMontoApertura.setText("");
//                                txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(oldValue);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                    if (patternMonto) {
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                    }
//                    Platform.runLater(() -> {
//                        txtMontoApertura.setText(param);
//                        txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                    });
//                }
//            }
//        });
//    }
//    Apartado 6 - BACKEND
    private void cargarComponentes() {
        cargarEmpresa();
        cbEmpresa11.getItems().add("UNIDAD");
        cbEmpresa111.getItems().add("LOCAL");
        cbEmpresa1111.getItems().add("MERCADERIA");
//        cargarFlia1();
        cargarImpuesto();
//        cargarFlia2();
//        cargarFlia3();
//        cargarFlia4();
//        cargarFlia5();
//        cargarFlia6();
//        cargarFlia7();
    }

    private void cargarEmpresa() {
//        String inputLine;
//        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Empresa> listEmp = empDAO.listar();
        if (listEmp != null) {
            for (Empresa emp : listEmp) {
                cbEmpresa.getItems().add(emp.getDescripcionEmpresa());
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/empresa");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//            if (!valor.isEmpty()) {
//                for (Object obj : valor) {
//                    JSONObject json = (JSONObject) parser.parse(obj.toString());
//                    cbEmpresa.getItems().add(json.get("descripcionEmpresa").toString());
//                }
//            }
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        }
    }
    //    private void cargarFlia1() {ORIGINAL
    //        String inputLine;
    //        JSONParser parser = new JSONParser();
    //        JSONArray valor = new JSONArray();
    //        hashFamilia = new HashMap<>();
    //        try {
    //            URL url = new URL(Utilidades.ip + "/ServerParana/nf1Tipo");
    //            URLConnection uc = url.openConnection();
    //            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
    //            while ((inputLine = br.readLine()) != null) {
    //                valor = (JSONArray) parser.parse(inputLine);
    //            }
    //            br.close();
    //            cbFamilia.getItems().add("*** SIN SELECCION ***");
    //            limpiarNiveles(1);
    //            hashFamilia.put("SIN SELECCION", new JSONObject());
    //            if (!valor.isEmpty()) {
    //                for (Object obj : valor) {
    //                    JSONObject json = (JSONObject) parser.parse(obj.toString());
    //                    cbFamilia.getItems().add(json.get("descripcion").toString());
    //                    hashFamilia.put(json.get("descripcion").toString(), json);
    //                }
    //            }
    //        } catch (FileNotFoundException e) {
    //            System.out.println(e.getLocalizedMessage());
    //        } catch (IOException e) {
    //            System.out.println(e.getLocalizedMessage());
    //        } catch (org.json.simple.parser.ParseException e) {
    //            e.printStackTrace();
    //        }
    //    }

    private void cargarFlia1() {
//        hashFamilia = new HashMap<>();
//        List<Nf1Tipo> listNF1 = nf1TipoDAO.listar();// anf1TipoDAO.listar();
//        cbFamilia.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(1);
//        hashFamilia.put("SIN SELECCION", new JSONObject());
//        if (listNF1.size() > 0) {
//            for (Nf1Tipo obj : listNF1) {
//                cbFamilia.getItems().add(obj.getDescripcion());
//                try {
//                    obj.setFechaAlta(null);
//                    obj.setFechaMod(null);
//                    hashFamilia.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf1TipoDTOEntitiesNull())));
//
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class
//                            .getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

//    private void cargarFlia2(long idNf1) { ORIGINAL
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashSubFamilia = new HashMap<>();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf2Sfamilia/" + idNf1);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubFamilia.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(2);
//        hashSubFamilia.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubFamilia.getItems().add(json.get("descripcion").toString());
//                    hashSubFamilia.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia2(long idNf1) {
        hashSubFamilia = new HashMap<>();
        List<Nf2Sfamilia> listNF2 = nf2SfamiliaDAO.listarPorNf(idNf1);// anf1TipoDAO.listar();
        cbSubFamilia.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(2);
        hashSubFamilia.put("SIN SELECCION", new JSONObject());
        if (listNF2.size() > 0) {
            for (Nf2Sfamilia obj : listNF2) {
                cbSubFamilia.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubFamilia.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf2SfamiliaDTOEntitiesNullNf1())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private void cargarFlia3(long id) { ORIGINAL
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashCategoria = new HashMap<>();
//        cbCategoria.getItems().clear();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf3Sseccion/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbCategoria.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(3);
//        hashCategoria.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbCategoria.getItems().add(json.get("descripcion").toString());
//                    hashCategoria.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia3(long id) {
        hashCategoria = new HashMap<>();
        cbCategoria.getItems().clear();
        List<Nf3Sseccion> listNF3 = nf3SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbCategoria.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(3);
        hashCategoria.put("SIN SELECCION", new JSONObject());
        if (listNF3.size() > 0) {
            for (Nf3Sseccion obj : listNF3) {
                cbCategoria.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashCategoria.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf3SseccionDTOEntitiesNull())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private void cargarFlia4(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashSubCategoria = new HashMap<>();
//        cbSubCategoria.getItems().clear();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf4Seccion1/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubCategoria.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(4);
//        hashSubCategoria.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubCategoria.getItems().add(json.get("descripcion").toString());
//                    hashSubCategoria.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia4(long id) {
        hashSubCategoria = new HashMap<>();
        cbSubCategoria.getItems().clear();
        List<Nf4Seccion1> listNF4 = nf4SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSubCategoria.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(4);
        hashSubCategoria.put("SIN SELECCION", new JSONObject());
        if (listNF4.size() > 0) {
            for (Nf4Seccion1 obj : listNF4) {
                cbSubCategoria.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubCategoria.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf4Seccion1DTOEntitiesNull())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private void cargarFlia5(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashClase = new HashMap<>();
//        cbClase.getItems().clear();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf5Seccion2/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbClase.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(5);
//        hashClase.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbClase.getItems().add(json.get("descripcion").toString());
//                    hashClase.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia5(long id) {
        hashClase = new HashMap<>();
        cbClase.getItems().clear();
        List<Nf5Seccion2> listNF5 = nf5SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbClase.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(5);
        hashClase.put("SIN SELECCION", new JSONObject());
        if (listNF5.size() > 0) {
            for (Nf5Seccion2 obj : listNF5) {
                cbClase.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashClase.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf5Seccion2DTOEntitiesNull())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private void cargarFlia6(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashSubClase = new HashMap<>();
//        cbSubClase.getItems().clear();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf6Secnom6/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubClase.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(6);
//        hashSubClase.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubClase.getItems().add(json.get("descripcion").toString());
//                    hashSubClase.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia6(long id) {
        hashSubClase = new HashMap<>();
        cbSubClase.getItems().clear();
        List<Nf6Secnom6> listNF6 = nf6SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSubClase.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(6);
        hashSubClase.put("SIN SELECCION", new JSONObject());
        if (listNF6.size() > 0) {
            for (Nf6Secnom6 obj : listNF6) {
                cbSubClase.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubClase.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf6Secnom6DTOEntitiesNull())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

//    private void cargarFlia7(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        hashSeccion = new HashMap<>();
//        cbSeccion.getItems().clear();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf7Secnom7/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSeccion.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(7);
//        hashSeccion.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSeccion.getItems().add(json.get("descripcion").toString());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//    }
    private void cargarFlia7(long id) {
        hashSeccion = new HashMap<>();
        cbSeccion.getItems().clear();
        List<Nf7Secnom7> listNF7 = nf7SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSeccion.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(7);
        hashSeccion.put("SIN SELECCION", new JSONObject());
        if (listNF7.size() > 0) {
            for (Nf7Secnom7 obj : listNF7) {
                cbSeccion.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSeccion.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf7Secnom7DTOEntitiesNull())));

                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    private void busquedaArticulo() {
        if (txtCodigo.getText().equalsIgnoreCase("")) {
            abrirBusquedaProducto();
        } else {
            String inputLine;
            JSONParser parser = new JSONParser();
            JSONObject valor = new JSONObject();
            Articulo art = artDAO.buscarCod(txtCodigo.getText());
            if (art != null) {
                depositoList = new ArrayList<>();
                txtAreaDesc.setText(art.getDescripcion());
                txtCodigo1.setText(art.getCodArticulo() + "");
                txtAreaDesc1.setText(art.getDescripcion());
                try {
                    if (art.getObservacion().equalsIgnoreCase("")) {
                        txtObservacion.setText("-");
                    } else {
                        txtObservacion.setText(art.getObservacion());
                    }
                } catch (Exception e) {
                    txtObservacion.setText("-");
                } finally {
                }
//            Platform.runLater(() -> {
//            txtPrecioMin.setText(art.getPrecioMin() + "");
//            txtPrecioMay.setText(art.getPrecioMay() + "");
                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(art.getPrecioMin() + "")));
                txtPrecioMay.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(art.getPrecioMay() + "")));
//            txtCostoOrigen112.setText(art.getDescMaxMay() + "");
//            txtCostoOrigen11.setText(art.getDescMaxMin() + "");
//            txtCostoOrigen11.setText(art.getDescProveedor() + "");
                if (txtCostoOrigen112.getText().equals("")) {
                    txtCostoOrigen112.setText("0");
                } else {
                    txtCostoOrigen112.setText(art.getDescMaxMay() + "");
                }
                if (txtCostoOrigen11.getText().equals("")) {
                    txtCostoOrigen11.setText("0");
                } else {
                    txtCostoOrigen11.setText(art.getDescMaxMin() + "");
                }
                if (txtDescProveedor.getText().equals("")) {
                    txtDescProveedor.setText("0");
                } else {
                    txtDescProveedor.setText(art.getDescProveedor() + "");
                }
                if (art.getSeccion().getIdSeccion() > 0) {
                    cbSeccionData.setValue(art.getSeccion().getDescripcion().toUpperCase());
                }
                if (art.getServicio()) {
                    chkServiicio.setSelected(true);
                }
                if (art.getStockeable()) {
                    chkParaVenta.setSelected(true);
                }

                try {
                    byte[] getImageInBytes = art.getImagen();  // image convert in byte form
                    Image img = new Image(new ByteArrayInputStream(getImageInBytes));
                    imageProducto.setImage(img);

//                File imageFile = File("myImage.jpg"); // we can put any name of file (just name of new file created).
//                FileOutputStream outputStream = new FileOutputStream(imageFile); // it will create new file (same location of class)
////                imageProducto.set
//                outputStream.write(getImageInBytes); // image write in "myImage.jpg" file
//                outputStream.close(); // close the output stream
//
//                Image image = new Image(imageFile.toURI().toString());
//                imageProducto.setImage(image);
                } catch (Exception e) {
                    System.out.println("->>>>> " + e.getLocalizedMessage());
                    System.out.println("->>>>> " + e.getMessage());
                    System.out.println("->>>>> " + e.fillInStackTrace());
                } finally {
                }
//            });
//
//            System.out.println("ESTO -->> " + txtPrecioMin.getText() + " - " + txtPrecioMay.getText());
//
                cbEmpresa11.getSelectionModel().select(0);
                cbEmpresa111.getSelectionModel().select(0);
                cbEmpresa1111.getSelectionModel().select(0);
                precioMinAnt = art.getPrecioMin();
                precioMayAnt = art.getPrecioMay();
//            txtCostoMonedaExt.setText("0");
                txtCostoLocales.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(art.getCosto().toString())));
                calcularPorcentaje(art.getPrecioMin() + ""); //            a
                calcularPorcentajeMay(art.getPrecioMay() + ""); //            a
//            txtIva.setText(art.getIva().getPoriva() + "");
//            txtPrec01.setText(art.getPrecioMin() + "");
//            txtPrec02.setText(art.getPrecioMay() + "");
                txtStockMin.setText(art.getStockMin());
                txtStockMax.setText(art.getStockMax());
                txtStockActual.setText(art.getStockActual());

                if (!art.getUrlImagen().equalsIgnoreCase("")) {
                    txtImagenSeleccionada.setText(art.getUrlImagen());
                }

                if (art.getServicio()) {
                    chkServiicio.setSelected(true);
                } else {
                    chkServiicio.setSelected(false);
                }
                if (art.getStockeable()) {
                    chkParaVenta.setSelected(true);
                } else {
                    chkParaVenta.setSelected(false);
                }

                if (art.getSeccion().getIdSeccion() > 0) {
                    cbSeccionData.setValue(art.getSeccion().getDescripcion().toUpperCase());
                }

                if (art.getIva().getPoriva() == 0) {
                    cbTipoFiscal.setValue("EXE");
                    cbImp.setValue("0");
                    Iva iva = new Iva();
                    iva.setIdIva(0l);
                    art.setIva(iva);
                } else {
                    cbTipoFiscal.setValue("GRA");
                    Iva iva = new Iva();
                    if (art.getIva().getPoriva() == 5) {
                        cbImp.setValue("5");
                        iva.setIdIva(5l);
                        art.setIva(iva);
                    } else {
                        cbImp.setValue("10");
                        iva.setIdIva(10l);
                        art.setIva(iva);
                    }
                }
//
                int num = 0;
                for (ArticuloProveedor artPro : articuloProveedorDAO.listarPorArticulo(art.getIdArticulo())) {
                    switch (num) {
                        case 0:
                            txtProv01.setText(artPro.getProveedor().getDescripcion());
                            txtCod01.setText(artPro.getProveedor().getRuc());
                            break;
                        case 1:
                            txtProv02.setText(artPro.getProveedor().getDescripcion());
                            txtCod02.setText(artPro.getProveedor().getRuc());
                            break;
                        case 2:
                            txtProv03.setText(artPro.getProveedor().getDescripcion());
                            txtCod03.setText(artPro.getProveedor().getRuc());
                            break;
                        default:
                            break;
                    }
                    num++;
                }

                switch (num) {
                    case 0:
                        txtProv01.setText("SIN NOMBRE");
                        txtCod01.setText("");
                        txtProv02.setText("SIN NOMBRE");
                        txtCod02.setText("");
                        txtProv03.setText("SIN NOMBRE");
                        txtCod03.setText("");
                        break;
                    case 1:
                        txtProv02.setText("SIN NOMBRE");
                        txtCod02.setText("");
                        txtProv03.setText("SIN NOMBRE");
                        txtCod03.setText("");
                        break;
                    case 2:
                        txtProv03.setText("SIN NOMBRE");
                        txtCod03.setText("");
                        break;
                    default:
                        break;
                }
                try {
                    if (Boolean.parseBoolean(art.getImportado().toString())) {
                        chkProdImportado.setSelected(true);
                    } else {
                        chkProdImportado.setSelected(false);
                    }
                } catch (Exception e) {
                    chkProdImportado.setSelected(false);
                } finally {
                }
                try {
                    if (Boolean.parseBoolean(art.getStockeable().toString())) {
                        chkNoStockeable.setSelected(true);
                    } else {
                        chkNoStockeable.setSelected(false);
                    }
                } catch (Exception e) {
                    chkNoStockeable.setSelected(false);
                } finally {
                }

                try {
                    art.setFechaAlta(null);
                    art.setFechaMod(null);
                    artBuscado = (JSONObject) parser.parse(gson.toJson(art.toBDArticuloDTO()));

                } catch (ParseException ex) {
                    Logger.getLogger(ArticulosFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
//            List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
//            for (Existencia ex : listExis) {
//                Articulo articu = new Articulo();
//                articu.setIdArticulo(art.getIdArticulo());
//
//                ex.setArticulo(articu);
//                JSONObject objJSON = new JSONObject();
//                JSONObject objART = new JSONObject();
//                objART.put("idArticulo", art.getIdArticulo());
//
//                JSONObject objDEPO = new JSONObject();
//                objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
//                objDEPO.put("descripcion", ex.getDeposito().getDescripcion());
//
//                objJSON.put("idExistencia", ex.getIdExistencia());
//                objJSON.put("cantidad", ex.getCantidad());
//                objJSON.put("articulo", objART);
//                objJSON.put("deposito", objDEPO);
//
//                depositoList.add(objJSON);
//
//                actualizandoTablaDeposito();
//            }
//            System.out.println("ESTO II -->> " + txtPrecioMin.getText() + " - " + txtPrecioMay.getText());
            } else {
                txtAreaDesc.requestFocus();
                mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
            }
        }
    }

    public void abrirBusquedaProducto() {
        anchorPaneBuscarProducto.setVisible(true);
//        tableViewFactura.setDisable(true);
//        imgProducto000.setDisable(true);
//        btnBuscarArticulo.setDisable(true);
//        textFieldCod.setDisable(true);
//        textFieldCant.setDisable(true);
//        btnCargarFactura.setDisable(true);
//        btnCargarServicio.setDisable(true);
//        btnCerrarTurno.setDisable(true);
//        btnCancelar.setDisable(true);
//        btnCerrar.setDisable(true);
//        labelTotalGs.setDisable(true);
//        paneImage1.setDisable(true);
//        paneImage11.setDisable(true);
//        txtNumComprobante.setDisable(true);

        txtBuscarCodProducto.setText("");
        txtBuscarDesriProducto.setText("");
        tableViewProducto.getItems().clear();
        txtCantidadModal.setText("1");
//        articuloModalList = new ArrayList<>();
//        mapeoArticulo = new HashMap();
        txtBuscarCodProducto.requestFocus();
        cbSeccionData1.getSelectionModel().select(0);
        buscarProductoModal();
    }

    public void cerrarBusquedaProducto() {
        anchorPaneBuscarProducto.setVisible(false);
//        tableViewFactura.setDisable(false);
//        imgProducto000.setDisable(false);
//        btnBuscarArticulo.setDisable(false);
//        textFieldCod.setDisable(false);
//        textFieldCant.setDisable(false);
//        btnCargarFactura.setDisable(false);
//        btnCargarServicio.setDisable(false);
//        btnCerrarTurno.setDisable(false);
//        btnCancelar.setDisable(false);
//        btnCerrar.setDisable(false);
//        labelTotalGs.setDisable(false);
//        paneImage1.setDisable(false);
//        paneImage11.setDisable(false);
//        txtNumComprobante.setDisable(false);
    }

    private void buscarProductoModal() {
        if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && txtBuscarDesriProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
            listarTodos();
        } else {
            if (txtBuscarDesriProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
                listarPorCodigo();
            } else if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
                listarPorDescripcion();
            } else if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && txtBuscarDesriProducto.getText().equalsIgnoreCase("")) {
                listarPorSeccion();
            } else {
                listarPorCombinaciones();
            }
//            if (txtBuscarCodProducto.getText().equalsIgnoreCase("")) {
//                listarPorDescripcion();
//            } else if (txtBuscarDesriProducto.getText().equalsIgnoreCase("")) {
//                listarPorCodigo();
//            } else {
//                listarPorSeccion();
//            }
        }
    }

    private void listarTodos() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarSoloDiez();
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("cantidad", art.getStockActual());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
//            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void mensajeError(String msj) {
//        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void listarPorDescripcion() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorDescripcion(txtBuscarDesriProducto.getText());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("cantidad", art.getStockActual());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
//            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorCodigo() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorCodigoAprox(txtBuscarCodProducto.getText());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("cantidad", art.getStockActual());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
//            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorSeccion() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorSeccion(cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("cantidad", art.getStockActual());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
//            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorCombinaciones() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        int id = cbSeccionData1.getSelectionModel().getSelectedIndex();
        String seccion = "";
        if (id != 0) {
            seccion = cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase();
        }
        List<Articulo> listArt = artDAO.listarPorCombinaciones(txtBuscarCodProducto.getText(), txtBuscarDesriProducto.getText(), seccion);
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("cantidad", art.getStockActual());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
//            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void actualizandoTablaArticulos() {
        articuloModalData = FXCollections.observableArrayList(articuloModalList);
        //columna Nombre ..................................................
        tableColumnArticuloModal.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        tableColumnArticuloModal.setCellFactory();
//        tableColumnArticuloModal.setCellValueFactory(c -> new SimpleObjectProperty<ImageView>(new ImageView(mapeoArticulo.get(c.getValue().get("idArticulo").toString()))));
        tableColumnArticuloModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, ImageView>, ObservableValue<ImageView>>() {
            @Override
            public ObservableValue<ImageView> call(TableColumn.CellDataFeatures<JSONObject, ImageView> param) {
                Image image = mapeoArticulo.get(param.getValue().get("idArticulo").toString());
                ImageView iv = new ImageView(image);
                iv.setFitHeight(60);
                iv.setFitWidth(60);
                return new SimpleObjectProperty<ImageView>(iv);
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnCodigoModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigoModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("codArticulo").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnDescripcionModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcionModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("descripcion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnSeccioModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnSeccioModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("seccion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnPrecioModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnPrecioModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("precioMin").toString())));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnCantidadModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidadModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = data.getValue().get("cantidad").toString();
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnCantidadModal.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
                t.getTableView().getItems().get(t.getTablePosition().getRow()).put("cantidad", t.getNewValue());
//                int num = tableViewProducto.getSelectionModel().getSelectedIndex() + 1;
                int num = tableViewProducto.getSelectionModel().getSelectedIndex();
                JSONObject json = articuloModalList.get(num);
//                System.out.println("ID ART: " + json.get("idArticulo").toString());
//                System.out.println("COD ART: " + json.get("codArticulo").toString());
//                System.out.println("DESCRI ART: " + json.get("descripcion").toString());
//                System.out.println("CAANTI ART: " + json.get("cantidad").toString());
                Articulo art = artDAO.getById(Long.parseLong(json.get("idArticulo").toString()));
                art.setStockActual(json.get("cantidad").toString());
                artDAO.actualizar(art);
                cambioCantidad = true;
// selection for single cells instead of single 
//                tableViewProducto.getSelectionModel().setCellSelectionEnabled(true);
// select third cell in first (possibly nested) column
//                tableViewProducto.getSelectionModel().clearAndSelect(num, tableViewProducto.getVisibleLeafColumn(3));
// focus the same cell
                tableViewProducto.getFocusModel().focus(num, tableViewProducto.getVisibleLeafColumn(3));
                tableViewProducto.scrollTo((num - 5));
//                }
            }
        });
//        tableViewProducto.getColumns().add(tableColumnArticuloModal);
        //columna Ruc .................................................
        tableViewProducto.setItems(articuloModalData);
    }

//    private void calcularPorcentaje(String value) {
//        DecimalFormat df = new DecimalFormat("#.0");
//        try {
//            if (!value.equals("null")) {
//                double result = (Long.parseLong(value) * 100) / Long.parseLong(txtCostoLocales.getText());
//                double reult2 = result - 100;
//                txtPorcIncremParana01.setText(df.format(reult2));
//            }
//        } catch (Exception e) {
//            txtPorcIncremParana01.setText("0");
//        } finally {
//        }
//
////        DecimalFormat df = new DecimalFormat("#.0");
////        double result = (Long.parseLong(value) * 100) / Long.parseLong(txtCostoLocales.getText());
////        double reult2 = result - 100;
////        txtPorcIncremParana01.setText(df.format(reult2));
//    }
//    private void verificarNivel7(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf7Secnom7/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf7Secnom7").toString());
//                nivel07 = json.get("descripcion").toString();
//                cbSeccion.getSelectionModel().select(json.get("descripcion").toString());
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel7(long id) {
        ArticuloNf7Secnom7 anf7 = anf7SeccionDAO.getByIdArticulo(id);
        if (anf7 != null) {
            nivel07 = anf7.getNf7Secnom7().getDescripcion();
            cbSeccion.getSelectionModel().select(nivel07);
        }
    }

//    private void verificarNivel6(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf6Secnom6/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf6Secnom6").toString());
//                nivel06 = json.get("descripcion").toString();
//                cbSubClase.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia7(Long.parseLong(json.get("idNf6Secnom6").toString()));
//                verificarNivel7(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel6(long id) {
        ArticuloNf6Secnom6 anf6 = anf6SeccionDAO.getByIdArticulo(id);
        if (anf6 != null) {
            nivel06 = anf6.getNf6Secnom6().getDescripcion();
            cbSubClase.getSelectionModel().select(nivel06);
            cargarFlia7(anf6.getNf6Secnom6().getIdNf6Secnom6());
            verificarNivel7(id);
        }
    }

//    private void verificarNivel5(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf5Seccion2/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf5Seccion2").toString());
//                nivel05 = json.get("descripcion").toString();
//                cbClase.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia6(Long.parseLong(json.get("idNf5Seccion2").toString()));
//                verificarNivel6(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel5(long id) {
        ArticuloNf5Seccion2 anf5 = anf5SeccionDAO.getByIdArticulo(id);
        if (anf5 != null) {
            nivel05 = anf5.getNf5Seccion2().getDescripcion();
            cbClase.getSelectionModel().select(nivel05);
            cargarFlia6(anf5.getNf5Seccion2().getIdNf5Seccion2());
            verificarNivel6(id);
        }
    }

//    private void verificarNivel4(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf4Seccion1/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf4Seccion1").toString());
//                nivel04 = json.get("descripcion").toString();
//                cbSubCategoria.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
//                verificarNivel5(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel4(long id) {
        ArticuloNf4Seccion1 anf4 = anf4SeccionDAO.getByIdArticulo(id);
        if (anf4 != null) {
            nivel04 = anf4.getNf4Seccion1().getDescripcion();
            cbSubCategoria.getSelectionModel().select(nivel04);
            cargarFlia5(anf4.getNf4Seccion1().getIdNf4Seccion1());
            verificarNivel5(id);
        }
    }

//    private void verificarNivel3(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf3Sseccion/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf3Sseccion").toString());
//                nivel03 = json.get("descripcion").toString();
//                cbCategoria.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia4(Long.parseLong(json.get("idNf3Sseccion").toString()));
//                verificarNivel4(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel3(long id) {
        ArticuloNf3Sseccion anf3 = anf3SeccionDAO.getByIdArticulo(id);
        if (anf3 != null) {
            nivel03 = anf3.getNf3Sseccion().getDescripcion();
            cbCategoria.getSelectionModel().select(nivel03);
            cargarFlia4(anf3.getNf3Sseccion().getIdNf3Sseccion());
            verificarNivel4(id);
        }
    }

//    private void verificarNivel2(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf2Sfamilia/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf2Sfamilia").toString());
//                nivel02 = json.get("descripcion").toString();
//                cbSubFamilia.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia3(Long.parseLong(json.get("idNf2Sfamilia").toString()));
//                verificarNivel3(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel2(long id) {
        ArticuloNf2Sfamilia anf2 = anf2SfamiliaDAO.getByIdArticulo(id);
        if (anf2 != null) {
            nivel02 = anf2.getNf2Sfamilia().getDescripcion();
            cbSubFamilia.getSelectionModel().select(nivel02);
            cargarFlia3(anf2.getNf2Sfamilia().getIdNf2Sfamilia());
            verificarNivel3(id);
        }
    }

//    private void verificarNivel1(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf1Tipo/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf1Tipo").toString());
//                nivel01 = json.get("descripcion").toString();
//                cbFamilia.getSelectionModel().select(json.get("descripcion").toString());
//                cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
//
//                verificarNivel2(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(ArticulosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
//    }
    private void verificarNivel1(long id) {
        ArticuloNf1Tipo anf1 = anf1TipoDAO.getByIdArticulo(id);
        if (anf1 != null) {
            nivel01 = anf1.getNf1Tipo().getDescripcion();
//            cbFamilia.getSelectionModel().select(nivel01);
            cargarFlia2(Long.parseLong(anf1.getNf1Tipo().getIdNf1Tipo().toString()));
            verificarNivel2(id);
        }
    }

    @FXML
    private void cbFamiliaMouseClicked(MouseEvent event) {
//        mouseEventCbFamilia(event);
    }

    @FXML
    private void cbFamiliaMouseKeyReleased(KeyEvent event) {
        mouseEventCbFamilia();
    }

    private void mouseEventCbFamilia() {
    }

    private void listenerCampos() {
//        cbFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    cbSubFamilia.getItems().clear();
//                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                    cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
//                } catch (Exception e) {
//                } finally {
//                }
//            }
//        });
//        cbSubFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    JSONObject json = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
//                    cargarFlia3(Long.parseLong(json.get("idNf2Sfamilia").toString()));
//                } catch (Exception e) {
//                } finally {
//                }
//            }
//        });
//        cbCategoria.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    JSONObject json = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
//                    cargarFlia4(Long.parseLong(json.get("idNf3Sseccion").toString()));
//                } catch (Exception e) {
//                } finally {
//                }
//            }
//        });
//        cbSubCategoria.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    JSONObject json = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
//                    cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
//                } catch (Exception e) {
//                } finally {
//                }
//            }
//        });
//        cbClase.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    if (cbClase.getSelectionModel().getSelectedIndex() >= 0) {
//                        JSONObject json = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
//                        cargarFlia6(Long.parseLong(json.get("idNf5Seccion2").toString()));
//                    }
//                } catch (Exception e) {
//                } finally {
//                }
//            }
//        });
//        cbSubClase.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                try {
//                    if (cbSubClase.getSelectionModel().getSelectedIndex() >= 0) {
//                        JSONObject json = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
//                        cargarFlia7(Long.parseLong(json.get("idNf6Secnom6").toString()));
//                    }
//                } catch (Exception ex) {
//                } finally {
//                }
//            }
//        });
//        txtCodigo.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (newValue.equalsIgnoreCase("")) {
//                cbFamilia.getSelectionModel().select(0);
//                cbSubFamilia.getItems().clear();
//                cbCategoria.getItems().clear();
//                cbSubCategoria.getItems().clear();
//                cbClase.getItems().clear();
//                cbSubClase.getItems().clear();
//                cbSeccion.getItems().clear();
//            }
//        });
    }

    private void limpiarNiveles(int val) {
        switch (val) {
            case 1:
                cbSubFamilia.getItems().clear();
                cbCategoria.getItems().clear();
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 2:
                cbCategoria.getItems().clear();
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 3:
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 4:
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 5:
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 6:
                cbSeccion.getItems().clear();
                break;
            case 7:
                break;
            default:
                break;
        }
    }

    private void registrarArticulo() {
        numValidator = new NumberValidator();
        if (txtPrecioMin.getText().equalsIgnoreCase("") || txtPrecioMin.getText().equalsIgnoreCase("0") || txtPrecioMay.getText().equalsIgnoreCase("") || txtPrecioMay.getText().equalsIgnoreCase("0") || txtCostoLocales.getText().equalsIgnoreCase("") || txtCostoLocales.getText().equalsIgnoreCase("0")) {
            mensajeAlerta("LOS CAMPOS COSTO LOCAL, PRECIO MIN Y PRECIO MAY NO DEBES QUEDAR VACIO, NI DEBEN SER IGUAL A CERO.");
        } else if (txtPorcIncremParana01.getText().equalsIgnoreCase("") || txtPorcIncremParana01.getText().equalsIgnoreCase("0") || txtCostoOrigen12.getText().equalsIgnoreCase("") || txtCostoOrigen12.getText().equalsIgnoreCase("0")) {
            mensajeAlerta("EL CAMPO INCREMENTO MINORISTA Y MAYORISTA NO DEBE QUEDAR VACIO, NI DEBE SER IGUAL A CERO");
        } else if (cbSeccionData.getSelectionModel().getSelectedIndex() < 0) {
            mensajeAlerta("DEBES SELECCIONAR UNA SECCION PARA REGISTRAR EL PRODUCTO");
        } else {
            Articulo articu = artDAO.buscarCod(txtCodigo.getText());
            if (articu != null) {
                mensajeAlerta("EL PRODUCTO YA HA SIDO REGISTRADO!");
            } else {
                JSONObject jsonDatos = cargarArticulo();
                Timestamp timestamp = new Timestamp(date.getTime());

                jsonDatos.put("fechaAlta", null);
                jsonDatos.put("fechaMod", null);

                Articulo arti = gson.fromJson(jsonDatos.toString(), Articulo.class);
                arti.setFechaAlta(timestamp);
                arti.setFechaMod(timestamp);
                arti.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
                arti.setPrecioMay(Long.parseLong(numValidator.numberValidator(txtPrecioMay.getText())));
                arti.setCosto(Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText())));
                arti.setUrlImagen(txtImagenSeleccionada.getText());
                arti.setObservacion(txtObservacion.getText());
                if (txtCostoOrigen112.getText().equals("")) {
                    arti.setDescMaxMay(Long.parseLong("0"));
                } else {
                    arti.setDescMaxMay(Long.parseLong(txtCostoOrigen112.getText()));
                }
                if (txtCostoOrigen11.getText().equals("")) {
                    arti.setDescMaxMin(Long.parseLong("0"));
                } else {
                    arti.setDescMaxMin(Long.parseLong(txtCostoOrigen11.getText()));
                }
                if (txtDescProveedor.getText().equals("")) {
                    arti.setDescProveedor(Long.parseLong("0"));
                } else {
                    arti.setDescProveedor(Long.parseLong(txtDescProveedor.getText()));
                }
                if (cbSeccionData.getSelectionModel().getSelectedIndex() >= 0) {
                    Seccion sec = seccionDAO.listarPorNombre(cbSeccionData.getSelectionModel().getSelectedItem().toUpperCase());
                    arti.setSeccion(sec);
                }

                arti.setServicio(false);
                arti.setStockeable(false);

                arti.setStockActual(txtStockActual.getText());
                arti.setStockMax(txtStockMax.getText());
                arti.setStockMin(txtStockMin.getText());
                if (chkServiicio.isSelected()) {
                    arti.setServicio(true);
                }
                if (chkParaVenta.isSelected()) {
                    arti.setStockeable(true);
                }

                Unidad unidad = new Unidad();
                unidad.setIdUnidad(1L);
                arti.setUnidad(unidad);

                Iva iva = new Iva();
                if (Long.parseLong(cbImp.getSelectionModel().getSelectedItem()) == 0) {
                    iva.setIdIva(1l);
                    arti.setIva(iva);
                } else {
                    if (Long.parseLong(cbImp.getSelectionModel().getSelectedItem()) == 5) {
                        iva.setIdIva(2l);
                        arti.setIva(iva);
                    } else {
                        iva.setIdIva(3l);
                        arti.setIva(iva);
                    }
                }

                try {
//                saveToFile(image, datoGuardar);

                    File imagePath = new File(txtImagenSeleccionada.getText()); //here we given fully specified image path.

                    byte[] imageInBytes = new byte[(int) imagePath.length()]; //image convert in byte form
                    FileInputStream inputStream = new FileInputStream(imagePath);  //input stream object create to read the file
                    inputStream.read(imageInBytes); // here inputstream object read the file
                    inputStream.close();
                    arti.setImagen(imageInBytes);
                } catch (Exception e) {
                } finally {
                }

                arti = artDAO.insertarRecuperarDato(arti);
                if (arti != null) {
                    if (!txtCod01.getText().equals("")) {
                        Proveedor p1 = proveedorDAO.listarPorRuc(txtCod01.getText());
                        Articulo art1 = new Articulo();
                        art1.setIdArticulo(arti.getIdArticulo());

                        ArticuloProveedor ap1 = new ArticuloProveedor();
                        ap1.setArticulo(art1);
                        ap1.setProveedor(p1);
                        ap1.setFechaAlta(timestamp);
                        ap1.setFechaMod(timestamp);

                        articuloProveedorDAO.insertar(ap1);
                    }
                    if (!txtCod02.getText().equals("")) {
                        Proveedor p2 = proveedorDAO.listarPorRuc(txtCod02.getText());
                        Articulo art2 = new Articulo();
                        art2.setIdArticulo(arti.getIdArticulo());

                        ArticuloProveedor ap2 = new ArticuloProveedor();
                        ap2.setArticulo(art2);
                        ap2.setProveedor(p2);
                        ap2.setFechaAlta(timestamp);
                        ap2.setFechaMod(timestamp);

                        articuloProveedorDAO.insertar(ap2);
                    }
                    if (!txtCod03.getText().equals("")) {
                        Proveedor p3 = proveedorDAO.listarPorRuc(txtCod03.getText());
                        Articulo art3 = new Articulo();
                        art3.setIdArticulo(arti.getIdArticulo());

                        ArticuloProveedor ap3 = new ArticuloProveedor();
                        ap3.setArticulo(art3);
                        ap3.setProveedor(p3);
                        ap3.setFechaAlta(timestamp);
                        ap3.setFechaMod(timestamp);

                        articuloProveedorDAO.insertar(ap3);
                    }
                    mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE!!");
                    limpiar();
                    desHabilitarBotones();
                    btnNuevo.setDisable(false);
                    btnDescartar.setDisable(false);
//                    btnBuscar.setDisable(false);
//                if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
//                    registrarNiveles(arti.getIdArticulo(), true);
//                    limpiar();
//                    desHabilitarBotones();
//                    btnNuevo.setDisable(false);
//                    btnDescartar.setDisable(false);
//                    btnBuscar.setDisable(false);
//                }
                } else {
                    mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS, VERIFIQUELOS");
                }
            }
        }
    }

    private JSONObject cargarArticulo() {
        numValidator = new NumberValidator();
        ArticuloDTO art = new ArticuloDTO();
        art.setUsuAlta(Identity.getNomFun());
        art.setUsuMod(Identity.getNomFun());
        art.setDescripcion(txtAreaDesc.getText());
        UnidadDTO uniDTO = new UnidadDTO();
        uniDTO.setIdUnidad(1l);
        art.setUnidad(uniDTO);
        SeccionSubDTO ssDTO = new SeccionSubDTO();
        ssDTO.setIdSeccionSub(0l);
        art.setSeccionSub(ssDTO);
        MarcaDTO marcaDTO = new MarcaDTO();
        marcaDTO.setIdMarca(0l);
        art.setMarca(marcaDTO);

        art.setCosto(Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText())));

        art.setPermiteDesc(false);
        art.setCodArticulo((txtCodigo.getText()));
        art.setServicio(false);
        art.setBajada(false);
        if (chkProdImportado.isSelected()) {
            art.setImportado(true);
        } else {
            art.setImportado(false);
        }
        if (chkNoStockeable.isSelected()) {
            art.setStockeable(true);
        } else {
            art.setStockeable(false);
        }

        IvaDTO ivaDTO = new IvaDTO();
        ivaDTO.setIdIva(1l);

        SeccionDTO sDTO = new SeccionDTO();
        sDTO.setIdSeccion(0l);

        try {
            return (JSONObject) parser.parse(gson.toJson(art));
        } catch (ParseException ex) {
            return null;
        }
    }

    public void limpiar() {
        txtCodigo.setText("");
        txtAreaDesc.setText("");
        txtPack.setText("");
        txtProveedor.setText("");
        txtMarca.setText("");
        txtPrecioMin.setText("");
        txtPrecioMay.setText("1");
        txtCodigo1.setText("");
        txtAreaDesc1.setText("");
        txtCostoOrigen12.setText("1");
        txtObservacion.setText("");

        chkParaVenta.setSelected(false);
        chkServiicio.setSelected(false);
        image = null;
        datoGuardar = null;

        imageProducto.setImage(null);

//        cbFamilia.getSelectionModel().select(0);
//        cbSubFamilia.getItems().clear();
//        cbCategoria.getItems().clear();
//        cbSubCategoria.getItems().clear();
//        cbClase.getItems().clear();
//        cbSubClase.getItems().clear();
//        cbSeccionData.getItems().clear();
        cbSeccionData.getSelectionModel().select(0);
        cbEmpresa.getSelectionModel().select(0);
        cbTipoFiscal.getSelectionModel().select(0);
        cbImp.getSelectionModel().select(0);
        depositoList = new ArrayList<>();
        tableViewDeposito.getItems().clear();

//        txtCostoMonedaExt.setText("");
        txtCostoLocales.setText("");
//        txtPorcDescProve.setText("");
//        txtPorcDesc.setText("");
        txtPorcIncremParana01.setText("");
//        txtPorcIncremParana02.setText("");
//        PocIncremMayorista.setText("");
//        txtPorcIncremVarios.setText("");

//        txtIva.setText("");
//        txtSensor.setText("");
//        txtOtrosGastos.setText("");
//        txtMonExt.setText("");
//        txtCostoOrigen.setText("");
//        txtPorcentajeDesc.setText("");
//        txtPorcAduanero.setText("");
//
//        txtCodProv01.setText("");
//        txtCodProv02.setText("");
//        txtCodProv03.setText("");
        txtProv01.setText("");
        txtProv02.setText("");
        txtProv03.setText("");

        txtCod01.setText("");
        txtCod02.setText("");
        txtCod03.setText("");

        chkProdImportado.setSelected(false);
        chkProdVigente.setSelected(false);
        chkNoStockeable.setSelected(false);
        chkArtOculto.setSelected(false);
        chkArtCompra.setSelected(false);
        chkLeerBalanza.setSelected(false);

//        txtFabricado.setText("");
//        txtCantFab01.setText("");
//        txtCantFab02.setText("");
//        txtCantFab03.setText("");
//        txtCantFab04.setText("");
//        txtCantFab05.setText("");
//        txtCantFab06.setText("");
//        txtCantFab07.setText("");
//        txtMonFab01.setText("");
//        txtMonFab02.setText("");
//        txtMonFab03.setText("");
//        txtMonFab04.setText("");
//        txtMonFab05.setText("");
//        txtMonFab06.setText("");
//        txtMonFab07.setText("");
        chkMonedaExt.getSelectionModel().select(0);
//        chkPromo.getSelectionModel().select(0);
//        chkPromoOutlet.getSelectionModel().select(0);

        txtCotizacion.setText("");

        txtDescProveedor.setText("0");
        txtCostoOrigen11.setText("0");
        txtCostoOrigen112.setText("0");
        txtStockActual.setText("");
        txtStockMin.setText("");
        txtStockMax.setText("");
        txtImagenSeleccionada.setText("");

    }

    private void registrarNiveles(long idArt, boolean dato) {
//        if (dato) {
        mensajeAlerta("DATOS INSERTADOS EXITOSAMENTE");
//        }
//        if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
//            setearNivelParametrizado(1, idArt);
//        }
        if (cbSubFamilia.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(2, idArt);
        }
        if (cbCategoria.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(3, idArt);
        }
        if (cbSubCategoria.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(4, idArt);
        }
        if (cbClase.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(5, idArt);
        }
        if (cbSubClase.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(6, idArt);
        }
        if (cbSeccion.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(7, idArt);
        }
        limpiar();
    }

    private void setearNivelParametrizado(int val, long idArt) {
        JSONObject objInsert = new JSONObject();
        ArticuloDTO artDTO = new ArticuloDTO();
        String nivel = "";
        try {
            switch (val) {
                case 1:
                    ArticuloNf1TipoDTO aNF1DTO = new ArticuloNf1TipoDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF1DTO.setArticulo(artDTO);

                    Nf1TipoDTO n1 = new Nf1TipoDTO();
//                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                    n1.setIdNf1Tipo(Long.parseLong(json.get("idNf1Tipo").toString()));
                    aNF1DTO.setNf1Tipo(n1);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF1DTO));
                    nivel = "articuloNf1Tipo";
                    break;
                case 2:
                    ArticuloNf2SfamiliaDTO aNF2DTO = new ArticuloNf2SfamiliaDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF2DTO.setArticulo(artDTO);

                    Nf2SfamiliaDTO n2 = new Nf2SfamiliaDTO();
                    JSONObject jsonSubFlia = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
                    n2.setIdNf2Sfamilia(Long.parseLong(jsonSubFlia.get("idNf2Sfamilia").toString()));
                    aNF2DTO.setNf2Sfamilia(n2);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF2DTO));
                    nivel = "articuloNf2Sfamilia";
                    break;
                case 3:
                    ArticuloNf3SseccionDTO aNF3DTO = new ArticuloNf3SseccionDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF3DTO.setArticulo(artDTO);

                    Nf3SseccionDTO n3 = new Nf3SseccionDTO();
                    JSONObject jsonCategoria = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
                    n3.setIdNf3Sseccion(Long.parseLong(jsonCategoria.get("idNf3Sseccion").toString()));
                    aNF3DTO.setNf3Sseccion(n3);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF3DTO));
                    nivel = "articuloNf3Sseccion";
                    break;
                case 4:
                    ArticuloNf4Seccion1DTO aNF4DTO = new ArticuloNf4Seccion1DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF4DTO.setArticulo(artDTO);

                    Nf4Seccion1DTO n4 = new Nf4Seccion1DTO();
                    JSONObject jsonSubCategoria = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
                    n4.setIdNf4Seccion1(Long.parseLong(jsonSubCategoria.get("idNf4Seccion1").toString()));
                    aNF4DTO.setNf4Seccion1(n4);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF4DTO));
                    nivel = "articuloNf4Seccion1";
                    break;
                case 5:
                    ArticuloNf5Seccion2DTO aNF5DTO = new ArticuloNf5Seccion2DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF5DTO.setArticulo(artDTO);

                    Nf5Seccion2DTO n5 = new Nf5Seccion2DTO();
                    JSONObject jsonClase = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
                    n5.setIdNf5Seccion2(Long.parseLong(jsonClase.get("idNf5Seccion2").toString()));
                    aNF5DTO.setNf5Seccion2(n5);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF5DTO));
                    nivel = "articuloNf5Seccion2";
                    break;
                case 6:
                    ArticuloNf6Secnom6DTO aNF6DTO = new ArticuloNf6Secnom6DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF6DTO.setArticulo(artDTO);

                    Nf6Secnom6DTO n6 = new Nf6Secnom6DTO();
                    JSONObject jsonSubClase = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
                    n6.setIdNf6Secnom6(Long.parseLong(jsonSubClase.get("idNf6Secnom6").toString()));
                    aNF6DTO.setNf6Secnom6(n6);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF6DTO));
                    nivel = "articuloNf6Secnom6";
                    break;
                case 7:
                    ArticuloNf7Secnom7DTO aNF7DTO = new ArticuloNf7Secnom7DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF7DTO.setArticulo(artDTO);

                    Nf7Secnom7DTO n7 = new Nf7Secnom7DTO();
                    JSONObject jsonSeccion = hashSeccion.get(cbSeccion.getSelectionModel().getSelectedItem());
                    n7.setIdNf7Secnom7(Long.parseLong(jsonSeccion.get("idNf7Secnom7").toString()));
                    aNF7DTO.setNf7Secnom7(n7);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF7DTO));
                    nivel = "articuloNf7Secnom7";
                    break;
                default:
                    break;

            }
        } catch (ParseException ex) {
            Logger.getLogger(ArticulosFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        if (!objInsert.toString().equals("{}")) {
            registrandoNiveles(objInsert, nivel);
        }
    }

    private void registrandoNiveles(JSONObject objInsert, String nivel) {
//        String inputLine;
        JSONParser parser = new JSONParser();

        if (nivel.equalsIgnoreCase("articuloNf7Secnom7")) {
            anf7SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf7Secnom7.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf6Secnom6")) {
            anf6SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf6Secnom6.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf5Seccion2")) {
            anf5SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf5Seccion2.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf4Seccion1")) {
            anf4SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf4Seccion1.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf3Sseccion")) {
            anf3SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf3Sseccion.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf2Sfamilia")) {
            anf2SfamiliaDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf2Sfamilia.class
            ));

        } else if (nivel.equalsIgnoreCase("articuloNf1Tipo")) {
            anf1TipoDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf1Tipo.class
            ));
        }
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(objInsert.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        }
    }

    private void actualizarArticulo() {
//        if (!artBuscado.toString().equalsIgnoreCase("{}")) {
//            artBuscado.put("descripcion", txtAreaDesc.getText());
//            artBuscado.put("costo", Long.parseLong(txtCostoLocales.getText()));
//            artBuscado.put("precioMin", Long.parseLong(txtPrecioMin.getText()));
//            artBuscado.put("precioMay", Long.parseLong(txtPrecioMay.getText()));
//            artBuscado.put("UsuMod", Identity.getNomFun());
//            if (chkProdImportado.isSelected()) {
//                artBuscado.put("importado", true);
//            } else {
//                artBuscado.put("importado", false);
//            }
//            if (chkNoStockeable.isSelected()) {
//                artBuscado.put("stockeable", true);
//            } else {
//                artBuscado.put("stockeable", true);
//            }
//            modificarArticulo(artBuscado);
//        }
        numValidator = new NumberValidator();
        if (txtPrecioMin.getText().equalsIgnoreCase("") || txtPrecioMin.getText().equalsIgnoreCase("0") || txtPrecioMay.getText().equalsIgnoreCase("") || txtPrecioMay.getText().equalsIgnoreCase("0") || txtCostoLocales.getText().equalsIgnoreCase("") || txtCostoLocales.getText().equalsIgnoreCase("0")) {
            mensajeAlerta("LOS CAMPOS COSTO LOCAL, PRECIO MIN Y PRECIO MAY NO DEBEN QUEDAR VACIO, NI DEBEN SER IGUAL A CERO.");
        } else if (txtPorcIncremParana01.getText().equalsIgnoreCase("") || txtPorcIncremParana01.getText().equalsIgnoreCase("0") || txtCostoOrigen12.getText().equalsIgnoreCase("") || txtCostoOrigen12.getText().equalsIgnoreCase("0")) {
            mensajeAlerta("EL CAMPO INCREMENTO MINORISTA Y MAYORISTA NO DEBEN QUEDAR VACIO, NI DEBE SER IGUAL A CERO");
        } else if (cbSeccionData.getSelectionModel().getSelectedIndex() < 0) {
            mensajeAlerta("DEBES SELECCIONAR UNA SECCION PARA ACTUALIZAR EL PRODUCTO");
        } else {
            Articulo arti = artDAO.buscarCod(txtCodigo.getText());
            try {
                if (!arti.getUrlImagen().toUpperCase().equalsIgnoreCase(txtImagenSeleccionada.getText().toUpperCase())) {
                    try {
//                    saveToFile(image, datoGuardar);

                        File imagePath = new File(txtImagenSeleccionada.getText()); //here we given fully specified image path.

                        byte[] imageInBytes = new byte[(int) imagePath.length()]; //image convert in byte form
                        FileInputStream inputStream = new FileInputStream(imagePath);  //input stream object create to read the file
                        inputStream.read(imageInBytes); // here inputstream object read the file
                        inputStream.close();
                        arti.setImagen(imageInBytes);

//                    MyImageBean object1 = new MyImageBean();
//                    object1.setImageName("Fruits Image");
//                    object1.setImage(imageInBytes);
                    } catch (Exception e) {
                    } finally {
                    }
                }
            } catch (Exception e) {
            } finally {
            }

            try {
                arti.setFechaAlta(timestamp);
                arti.setFechaMod(timestamp);
                arti.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
                arti.setPrecioMay(Long.parseLong(numValidator.numberValidator(txtPrecioMay.getText())));
                arti.setCosto(Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText())));
                arti.setUrlImagen(txtImagenSeleccionada.getText());
                arti.setObservacion(txtObservacion.getText());
//            arti.setDescMaxMay(Long.parseLong(txtCostoOrigen112.getText()));
//            arti.setDescMaxMin(Long.parseLong(txtCostoOrigen11.getText()));
//            arti.setDescProveedor(Long.parseLong(txtCostoOrigen11.getText()));
                if (txtCostoOrigen112.getText().equals("")) {
                    arti.setDescMaxMay(Long.parseLong("0"));
                } else {
                    arti.setDescMaxMay(Long.parseLong(txtCostoOrigen112.getText()));
                }
                if (txtCostoOrigen11.getText().equals("")) {
                    arti.setDescMaxMin(Long.parseLong("0"));
                } else {
                    arti.setDescMaxMin(Long.parseLong(txtCostoOrigen11.getText()));
                }
                if (txtDescProveedor.getText().equals("")) {
                    arti.setDescProveedor(Long.parseLong("0"));
                } else {
                    arti.setDescProveedor(Long.parseLong(txtDescProveedor.getText()));
                }

                arti.setServicio(false);
                arti.setStockeable(false);
                if (chkServiicio.isSelected()) {
                    arti.setServicio(true);
                }
                if (chkParaVenta.isSelected()) {
                    arti.setStockeable(true);
                }
                arti.setDescripcion(txtAreaDesc.getText());

                arti.setStockActual(txtStockActual.getText());
                arti.setStockMax(txtStockMax.getText());
                arti.setStockMin(txtStockMin.getText());

                Unidad unidad = new Unidad();
                unidad.setIdUnidad(1L);
                arti.setUnidad(unidad);

                Iva iva = new Iva();
                if (Long.parseLong(cbImp.getSelectionModel().getSelectedItem()) == 0) {
                    iva.setIdIva(1l);
                    arti.setIva(iva);
                } else {
                    if (Long.parseLong(cbImp.getSelectionModel().getSelectedItem()) == 5) {
                        iva.setIdIva(2l);
                        arti.setIva(iva);
                    } else {
                        iva.setIdIva(3l);
                        arti.setIva(iva);
                    }
                }
                if (cbSeccionData.getSelectionModel().getSelectedIndex() >= 0) {
                    Seccion sec = seccionDAO.listarPorNombre(cbSeccionData.getSelectionModel().getSelectedItem().toUpperCase());
                    arti.setSeccion(sec);
                }

                artDAO.actualizar(arti);
                if (arti != null) {
                    if (!txtCod01.getText().equals("") && !txtCod01.getText().equals("")) {
                        Proveedor p1 = proveedorDAO.listarPorRuc(txtCod01.getText());
                        if (p1.getIdProveedor() != null) {
                            if (!txtCod01.getText().equalsIgnoreCase(p1.getRuc())) {
                                Articulo art1 = new Articulo();
                                art1.setIdArticulo(arti.getIdArticulo());

                                ArticuloProveedor ap1 = new ArticuloProveedor();
                                ap1.setArticulo(art1);
                                ap1.setProveedor(p1);
                                ap1.setFechaAlta(timestamp);
                                ap1.setFechaMod(timestamp);
                                articuloProveedorDAO.insertar(ap1);
                            }
                        }
                    }
                    if (!txtCod02.getText().equals("")) {
                        Proveedor p2 = proveedorDAO.listarPorRuc(txtCod02.getText());
                        if (p2.getIdProveedor() != null) {
                            if (!txtCod02.getText().equalsIgnoreCase(p2.getRuc())) {
                                Articulo art2 = new Articulo();
                                art2.setIdArticulo(arti.getIdArticulo());

                                ArticuloProveedor ap2 = new ArticuloProveedor();
                                ap2.setArticulo(art2);
                                ap2.setProveedor(p2);
                                ap2.setFechaAlta(timestamp);
                                ap2.setFechaMod(timestamp);

                                articuloProveedorDAO.insertar(ap2);
                            }
                        }
                    }
                    if (!txtCod03.getText().equals("")) {
                        Proveedor p3 = proveedorDAO.listarPorRuc(txtCod03.getText());
                        if (p3.getIdProveedor() != null) {
                            if (!txtCod03.getText().equalsIgnoreCase(p3.getRuc())) {
                                Articulo art3 = new Articulo();
                                art3.setIdArticulo(arti.getIdArticulo());

                                ArticuloProveedor ap3 = new ArticuloProveedor();
                                ap3.setArticulo(art3);
                                ap3.setProveedor(p3);
                                ap3.setFechaAlta(timestamp);
                                ap3.setFechaMod(timestamp);

                                articuloProveedorDAO.insertar(ap3);
                            }
                        }
                    }
                    mensajeAlerta("LOS DATOS HAN SIDO ACTUALIZADOS CORRECTAMENTE!!");
                    limpiar();
                    desHabilitarBotones();
                    btnNuevo.setDisable(false);
                    btnDescartar.setDisable(false);
//                btnBuscar.setDisable(false);
                } else {
                    mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS, VERIFIQUELOS");
                }
            } catch (Exception e) {
                mensajeAlerta("EL ARTICULO NO EXISTE, POR ESE MOTIVO NO ES POSIBLE REGISTRARLO");
            } finally {
            }
        }
    }
//    private void actualizarArticulo() {
//        if (!artBuscado.toString().equalsIgnoreCase("{}")) {
//            artBuscado.put("descripcion", txtAreaDesc.getText());
//            artBuscado.put("costo", Long.parseLong(txtCostoLocales.getText()));
//            artBuscado.put("precioMin", Long.parseLong(txtPrecioMin.getText()));
//            artBuscado.put("precioMay", Long.parseLong(txtPrecioMay.getText()));
////            artBuscado.put("precioMay", Long.parseLong(txtCostoLocales.getText()));
//            artBuscado.put("UsuMod", Identity.getNomFun());
//            if (chkProdImportado.isSelected()) {
//                artBuscado.put("importado", true);
//            } else {
//                artBuscado.put("importado", false);
//            }
//            if (chkNoStockeable.isSelected()) {
//                artBuscado.put("stockeable", true);
//            } else {
//                artBuscado.put("stockeable", true);
//            }
////            if (Long.parseLong(txtPrecioMin.getText()) > precioMinAnt || Long.parseLong(txtPrecioMay.getText()) > precioMayAnt) {
////                actualizarMañana(artBuscado);
////                artBuscado = new JSONObject();
////                mensajeAlerta("DATOS ACTUALIZADOS EXITOSAMENTE");
////                limpiar();
////                desHabilitarBotones();
////                btnNuevo.setDisable(false);
////                btnDescartar.setDisable(false);
////                btnBuscar.setDisable(false);
////            } else {
//            modificarArticulo(artBuscado);
////            }
//        }
//}

    private void modificarArticulo(JSONObject art) {
        boolean exito = false;
        JSONParser parser = new JSONParser();
        JSONObject jsonDatos = art;
        Timestamp timestamp = new Timestamp(date.getTime());
        jsonDatos.put("fechaAlta", null);
        jsonDatos.put("fechaMod", null);

        Articulo arti = gson.fromJson(jsonDatos.toString(), Articulo.class
        );
        arti.setFechaAlta(timestamp);
        arti.setFechaMod(timestamp);

        articuloProveedorDAO.eliminarTodos(arti.getIdArticulo());
        if (!txtCod01.getText().equals("")) {
            Proveedor p1 = proveedorDAO.listarPorRuc(txtCod01.getText());
            Articulo art1 = new Articulo();
            art1.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap1 = new ArticuloProveedor();
            ap1.setArticulo(art1);
            ap1.setProveedor(p1);
            ap1.setFechaAlta(timestamp);
            ap1.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap1);
        }
        if (!txtCod02.getText().equals("")) {
            Proveedor p2 = proveedorDAO.listarPorRuc(txtCod02.getText());
            Articulo art2 = new Articulo();
            art2.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap2 = new ArticuloProveedor();
            ap2.setArticulo(art2);
            ap2.setProveedor(p2);
            ap2.setFechaAlta(timestamp);
            ap2.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap2);
        }
        if (!txtCod03.getText().equals("")) {
            Proveedor p3 = proveedorDAO.listarPorRuc(txtCod03.getText());
            Articulo art3 = new Articulo();
            art3.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap3 = new ArticuloProveedor();
            ap3.setArticulo(art3);
            ap3.setProveedor(p3);
            ap3.setFechaAlta(timestamp);
            ap3.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap3);
        }
        try {
            arti.setCosto(Long.parseLong(txtCostoLocales.getText()));
            Iva iva = new Iva();
            artDAO.actualizar(arti);
            exito = true;
        } catch (Exception e) {
            exito = false;
        }

        if (exito) {
            artBuscado = new JSONObject();
            mensajeAlerta("DATOS ACTUALIZADOS EXITOSAMENTE");
            actualizarNiveles(Long.parseLong(jsonDatos.get("idArticulo").toString()));
            limpiar();
            desHabilitarBotones();
            btnNuevo.setDisable(false);
            btnDescartar.setDisable(false);
//            btnBuscar.setDisable(false);
        } else {
            mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS, VERIFIQUELOS");
        }
    }

    private void actualizarNiveles(long idArt) {
        JSONObject objInsert = new JSONObject();
        ArticuloDTO artDTO = new ArticuloDTO();
        String nivel = "";
        try {
//            if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel01)) {
//                    eliminarNiveles(1, idArt);
//
//                    ArticuloNf1TipoDTO aNF1DTO = new ArticuloNf1TipoDTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF1DTO.setArticulo(artDTO);
//
//                    Nf1TipoDTO n1 = new Nf1TipoDTO();
//                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                    n1.setIdNf1Tipo(Long.parseLong(json.get("idNf1Tipo").toString()));
//                    aNF1DTO.setNf1Tipo(n1);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF1DTO));
//                    nivel = "articuloNf1Tipo";
//
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(1, idArt);
//            }
            if (cbSubFamilia.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel02)) {
                    eliminarNiveles(2, idArt);

                    ArticuloNf2SfamiliaDTO aNF2DTO = new ArticuloNf2SfamiliaDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF2DTO.setArticulo(artDTO);

                    Nf2SfamiliaDTO n2 = new Nf2SfamiliaDTO();
                    JSONObject jsonSubFlia = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
                    n2.setIdNf2Sfamilia(Long.parseLong(jsonSubFlia.get("idNf2Sfamilia").toString()));
                    aNF2DTO.setNf2Sfamilia(n2);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF2DTO));
                    nivel = "articuloNf2Sfamilia";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(2, idArt);
            }
            if (cbCategoria.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel03)) {
                    eliminarNiveles(3, idArt);

                    ArticuloNf3SseccionDTO aNF3DTO = new ArticuloNf3SseccionDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF3DTO.setArticulo(artDTO);

                    Nf3SseccionDTO n3 = new Nf3SseccionDTO();
                    JSONObject jsonCategoria = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
                    n3.setIdNf3Sseccion(Long.parseLong(jsonCategoria.get("idNf3Sseccion").toString()));
                    aNF3DTO.setNf3Sseccion(n3);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF3DTO));
                    nivel = "articuloNf3Sseccion";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }

                }
            } else {
                eliminarNiveles(3, idArt);
            }
            if (cbSubCategoria.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel04)) {
                    eliminarNiveles(4, idArt);

                    ArticuloNf4Seccion1DTO aNF4DTO = new ArticuloNf4Seccion1DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF4DTO.setArticulo(artDTO);

                    Nf4Seccion1DTO n4 = new Nf4Seccion1DTO();
                    JSONObject jsonSubCategoria = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
                    n4.setIdNf4Seccion1(Long.parseLong(jsonSubCategoria.get("idNf4Seccion1").toString()));
                    aNF4DTO.setNf4Seccion1(n4);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF4DTO));
                    nivel = "articuloNf4Seccion1";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(4, idArt);
            }
            if (cbClase.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel05)) {
                    eliminarNiveles(5, idArt);

                    ArticuloNf5Seccion2DTO aNF5DTO = new ArticuloNf5Seccion2DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF5DTO.setArticulo(artDTO);

                    Nf5Seccion2DTO n5 = new Nf5Seccion2DTO();
                    JSONObject jsonClase = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
                    n5.setIdNf5Seccion2(Long.parseLong(jsonClase.get("idNf5Seccion2").toString()));
                    aNF5DTO.setNf5Seccion2(n5);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF5DTO));
                    nivel = "articuloNf5Seccion2";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(5, idArt);
            }
            if (cbSubClase.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel06)) {
                    eliminarNiveles(6, idArt);

                    ArticuloNf6Secnom6DTO aNF6DTO = new ArticuloNf6Secnom6DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF6DTO.setArticulo(artDTO);

                    Nf6Secnom6DTO n6 = new Nf6Secnom6DTO();
                    JSONObject jsonSubClase = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
                    n6.setIdNf6Secnom6(Long.parseLong(jsonSubClase.get("idNf6Secnom6").toString()));
                    aNF6DTO.setNf6Secnom6(n6);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF6DTO));
                    nivel = "articuloNf6Secnom6";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(6, idArt);
            }
            if (cbSeccion.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSeccion.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel07)) {
                    eliminarNiveles(7, idArt);

                    ArticuloNf7Secnom7DTO aNF7DTO = new ArticuloNf7Secnom7DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF7DTO.setArticulo(artDTO);

                    Nf7Secnom7DTO n7 = new Nf7Secnom7DTO();
                    JSONObject jsonSeccion = hashSeccion.get(cbSeccion.getSelectionModel().getSelectedItem());
                    n7.setIdNf7Secnom7(Long.parseLong(jsonSeccion.get("idNf7Secnom7").toString()));
                    aNF7DTO.setNf7Secnom7(n7);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF7DTO));
                    nivel = "articuloNf7Secnom7";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(7, idArt);

            }
        } catch (ParseException ex) {
            Logger.getLogger(ArticulosFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void eliminarNiveles(int numNivel, long idArt) {
        String nivel = "";
        switch (numNivel) {
            case 1:
                try {
                    nivel = "articuloNf1Tipo";
                    ArticuloNf1Tipo artNF1 = anf1TipoDAO.listarArticulo(idArt);
                    anf1TipoDAO.eliminar(artNF1.getIdNf1TipoArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 2:
                try {
                    nivel = "articuloNf2Sfamilia";
                    ArticuloNf2Sfamilia artNF2 = anf2SfamiliaDAO.listarArticulo(idArt);
                    anf2SfamiliaDAO.eliminar(artNF2.getIdNf2SfamiliaArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 3:
                try {
                    nivel = "articuloNf3Sseccion";
                    ArticuloNf3Sseccion artNF3 = anf3SeccionDAO.listarArticulo(idArt);
                    anf3SeccionDAO.eliminar(artNF3.getIdNf3SseccionArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 4:
                try {
                    nivel = "articuloNf4Seccion1";
                    ArticuloNf4Seccion1 artNF4 = anf4SeccionDAO.listarArticulo(idArt);
                    anf4SeccionDAO.eliminar(artNF4.getIdNf4Seccion1Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 5:
                try {
                    nivel = "articuloNf5Seccion2";
                    ArticuloNf5Seccion2 artNF5 = anf5SeccionDAO.listarArticulo(idArt);
                    anf5SeccionDAO.eliminar(artNF5.getIdNf5Seccion2Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 6:
                try {
                    nivel = "articuloNf6Secnom6";
                    ArticuloNf6Secnom6 artNF6 = anf6SeccionDAO.listarArticulo(idArt);
                    anf6SeccionDAO.eliminar(artNF6.getIdNf6Secnom6Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 7:
                try {
                    nivel = "articuloNf7Secnom7";
                    ArticuloNf7Secnom7 artNF7 = anf7SeccionDAO.listarArticulo(idArt);
                    anf7SeccionDAO.eliminar(artNF7.getIdNf7Secnom7Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            default:
                break;
        }

//        String inputLine;
//        JSONParser parser = new JSONParser();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel + "/delete/" + idArt);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BuffbtnAeredReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                boolean dato = (Boolean) parser.parse(inputLine);
//                System.out.println("-->> " + dato);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
    }

    public void habilitarBotones() {
        btnNuevo.setDisable(false);
//        btnEditar.setDisable(false);
//        btnGuardar.setDisable(false);
        btnDescartar.setDisable(false);
        btnTransferirLambare.setDisable(false);
        btnTransferirYeruti.setDisable(false);
    }

    public void desHabilitarBotones() {
        btnNuevo.setDisable(true);
//        btnEditar.setDisable(true);
//        btnGuardar.setDisable(true);
        btnDescartar.setDisable(true);
        btnTransferirLambare.setDisable(true);
        btnTransferirYeruti.setDisable(true);
    }

    private void cargarImpuesto() {
        cbTipoFiscal.getItems().add("GRA");
        cbTipoFiscal.getItems().add("EXE");
        cbImp.getItems().add("0");
        cbImp.getItems().add("5");
        cbImp.getItems().add("10");
    }

    @FXML
    private void txtCodigoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void actualizandoTablaDeposito() {
        depositoData = FXCollections.observableArrayList(depositoList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                    String apellido = String.valueOf(jsonDepo.get("descripcion"));
                    if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                        apellido = "-";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());

                } catch (ParseException ex) {
                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnCantDepo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantDepo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                //                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                String apellido = String.valueOf(data.getValue().get("cantidad"));
                if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                    apellido = "-";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Ruc .................................................
        tableViewDeposito.setItems(depositoData);
//        if (!escucha) {
//            escucha();
//        }
    }

    @FXML
    private void txtPrecioMinKeyPress(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPorcIncremParana01.setText("0");
            listenTextFieldMin();
        }
        keyPress(event);
//        listenPrecioMin(event);
    }

    private void listenPrecioMin(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!oldValue.equalsIgnoreCase(newValue)) {
        calcularPorcentaje(txtPrecioMin.getText());
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    }
//                });
//            });
//        }
    }

    private void calcularPorcentaje(String value) {
        numValidator = new NumberValidator();
//        DecimalFormat df = new DecimalFormat("#.0");
        try {
            if (!value.equals("null")) {
                double result = (Long.parseLong(numValidator.numberValidator(value)) * 100) / Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText()));
                double reult2 = result - 100;
                long res = Math.round(reult2);
                txtPorcIncremParana01.setText(res + "");
            }
        } catch (Exception e) {
            txtPorcIncremParana01.setText("0");
        } finally {
        }
    }

    private void listenPrecioMay(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtPrecioMay.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!oldValue.equalsIgnoreCase(newValue)) {
        calcularPorcentajeMay(txtPrecioMay.getText());
//                        txtPrecioMay.positionCaret(txtPrecioMay.getLength());
//                    }
//                });
//            });
//        }
    }

    private void calcularPorcentajeMay(String value) {
//        DecimalFormat df = new DecimalFormat("#.0");
        try {
            if (!value.equals("null")) {
                double result = (Long.parseLong(numValidator.numberValidator(value)) * 100) / Long.parseLong(numValidator.numberValidator(txtCostoLocales.getText()));
                double reult2 = result - 100;
                long res = Math.round(reult2);
                txtCostoOrigen12.setText(res + "");
            }
        } catch (Exception e) {
            txtCostoOrigen12.setText("1");
        } finally {
        }
    }

    @FXML
    private void btnProveedorEstandarAction(ActionEvent event) {
        BuscarProveedorFXMLController.cargarRucRazon(txtCod01, txtProv01);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/ArticulosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    @FXML
    private void btnProveedor1Action(ActionEvent event) {
        BuscarProveedorFXMLController.cargarRucRazon(txtCod02, txtProv02);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/ArticulosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    @FXML
    private void btnProveedor2Action(ActionEvent event) {
        BuscarProveedorFXMLController.cargarRucRazon(txtCod03, txtProv03);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/ArticulosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);

    }

    private void actualizarMañana(JSONObject arti) {
        ConexionPostgres.conectarLocal();
//            JSONObject sup = (JSONObject) parser.parse(jsonArqueo.get("supervisor").toString());
//            JSONObject usu = (JSONObject) parser.parse(sup.get("usuario").toString());
//            usu.remove("fechaAlta");
//            usu.remove("fechaMod");
//            sup.remove("usuario");
//            sup.put("usuario", usu);
//            jsonArqueo.put("supervisor", sup);
        String sql = "INSERT INTO general.articulo_suba(fecha, tabla) VALUES (now() , '" + arti + "');";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                System.out.println("******* Haz agregado un nuevo registro a POSTGRES BD HANDLER PARANA ********");
//                estado = true;
            }
            ps.close();
            ConexionPostgres.getConLocal().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getConLocal().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrarLocal();
    }

    @FXML
    private void btnImagenSeleccionadaAction(ActionEvent event) {
//        System.out.println("--->> " + );
        String dato = guardandoDatos();
        File file = new File(txtImagenSeleccionada.getText());
        image = new Image(file.toURI().toString());

        try {
//            File imagePath = new File(txtImagenSeleccionada.getText()); //here we given fully specified image path.
//            image.getScaledInstance //            byte[] imageInBytes = new byte[(int) imagePath.length()]; //image convert in byte form
            //            Image img = new Image(new ByteArrayInputStream(imageInBytes));
            imageProducto.setImage(image);
//            FileInputStream inputStream = new FileInputStream(imagePath);  //input stream object create to read the file
//            inputStream.read(imageInBytes); // here inputstream object read the file
//            inputStream.close();
//            arti.setImagen(imageInBytes);
        } catch (Exception e) {
            System.out.println("->>>>> " + e.getLocalizedMessage());
            System.out.println("->>>>> " + e.getMessage());
            System.out.println("->>>>> " + e.fillInStackTrace());
        } finally {
        }
    }

    public static void saveToFile(Image image, String dato) {
        File outputFile = new File(Utilidades.URL_IMG_SRV + dato);
        BufferedImage bImage = SwingFXUtils.fromFXImage(image, null);
        try {
            ImageIO.write(bImage, "jpg", outputFile);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private String guardandoDatos() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Seleccione Imagen");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Document", Arrays.asList("*.jpg", "*.jpeg", "*.png")));
        File file = fileChooser.showOpenDialog(parentStage);
        txtImagenSeleccionada.setText(file.getAbsolutePath());
//        System.out.println("NAME: " + file.getName());
        return file.getName();
    }

    @FXML
    private void txtPorcIncremParana01KeyPress(KeyEvent event) {
//        listenIncrementoMin(event);
//        if (event.getCode().isDigitKey()) {
        KeyCode keyCode = event.getCode();
        System.out.println("->> " + keyCode + " " + event.getCode().F5 + " " + event.getCode().F8);
        if (keyCode != event.getCode().F5 || keyCode != event.getCode().F2) {
            txtPrecioMin.setText("0");
        }
        keyPress(event);
    }

    @FXML
    private void txtPrecioMayKeyPressed(KeyEvent event) {
//        listenPrecioMay(event);
        if (event.getCode().isDigitKey()) {
            txtCostoOrigen12.setText("1");
//            System.out.println("INGRESO: " + event.getCode());
            listenTextFieldMay(event.getCode().toString().replace("NUMPAD", ""));
        }
        keyPress(event);
    }

    @FXML
    private void txtCostoOrigen12KeyPressed(KeyEvent event) {
//        listenIncrementoMay(event);
//        if (event.getCode().isDigitKey()) {
        KeyCode keyCode = event.getCode();
        if (keyCode != event.getCode().F5 || keyCode != event.getCode().F2) {
            txtPrecioMay.setText("1");
        }
//        }txtPorcIncremParana01
        keyPress(event);
    }

    @FXML
    private void txtPrecioLOcalKeyPress(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            //            listenTextFieldMay();
            listenTextFieldLocal();
        }
    }

    public static String getSaltString() {
        String SALTCHARS = "1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 13) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;
    }

    @FXML
    private void btnGenerarCodAction(ActionEvent event) {
        txtCodigo.setText(getSaltString());
        txtCodigo.requestFocus();
    }

    private File File(String myImagejpg) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @FXML
    private void buttonAnhadirProductoAction(ActionEvent event) {
        seleccionarProd();
    }

    @FXML
    private void buttonBuscarClienteAction(ActionEvent event) {
        buscarProductoModal();
    }

    @FXML
    private void anchorPaneBuscarProductoKeyReleased(KeyEvent event) {
    }

    private void seleccionarProd() {
        if (tableViewProducto.getSelectionModel().getSelectedIndex() >= 0) {
            cerrarBusquedaProducto();
            JSONObject json = tableViewProducto.getSelectionModel().getSelectedItem();
            txtCodigo.requestFocus();
            txtCodigo.setText(json.get("codArticulo").toString());
        }
    }

//    public void deshabilitarTodo() {
//        tabPaneSeleccionMenu.setDisable(true);
//        panel01.setDisable(true);
//        btnGenerarCod.setDisable(true);
//        btnBuscar.setDisable(true);
//    }
//
//    public void habilitarTodo() {
//        tabPaneSeleccionMenu.setDisable(false);
//        panel01.setDisable(false);
//        btnGenerarCod.setDisable(false);
//        btnBuscar.setDisable(false);
//    }
    @FXML
    private void txtBuscarCodProductoKeyReleased(KeyEvent event) {
        buscarProductoModal();
    }

    @FXML
    private void txtBuscarDesriProductoKeyReleased(KeyEvent event) {
        buscarProductoModal();
    }

    @FXML
    private void cbSeccionData1KeyReleased(KeyEvent event) {
        buscarProductoModal();
    }
}
