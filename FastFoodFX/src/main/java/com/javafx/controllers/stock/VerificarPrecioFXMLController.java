/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.core.domain.Existencia;
import com.peluqueria.core.domain.Iva;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Nf1Tipo;
import com.peluqueria.core.domain.Nf2Sfamilia;
import com.peluqueria.core.domain.Nf3Sseccion;
import com.peluqueria.core.domain.Nf4Seccion1;
import com.peluqueria.core.domain.Nf5Seccion2;
import com.peluqueria.core.domain.Nf6Secnom6;
import com.peluqueria.core.domain.Nf7Secnom7;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import com.peluqueria.dao.ArticuloProveedorDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.Nf1TipoDAO;
import com.peluqueria.dao.Nf2SfamiliaDAO;
import com.peluqueria.dao.Nf3SseccionDAO;
import com.peluqueria.dao.Nf4Seccion1DAO;
import com.peluqueria.dao.Nf5Seccion2DAO;
import com.peluqueria.dao.Nf6Secnom6DAO;
import com.peluqueria.dao.Nf7Secnom7DAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.impl.ArticuloNf1TipoDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf3SseccionDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf7Secnom7DAOImpl;
import com.peluqueria.dao.impl.FacturaCompraDetDAOImpl;
import com.peluqueria.dao.impl.Nf1TipoDAOImpl;
import com.peluqueria.dao.impl.Nf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.Nf3SseccionDAOImpl;
import com.peluqueria.dao.impl.Nf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.Nf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.Nf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.Nf7Secnom7DAOImpl;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.ArticuloNf1TipoDTO;
import com.peluqueria.dto.ArticuloNf2SfamiliaDTO;
import com.peluqueria.dto.ArticuloNf3SseccionDTO;
import com.peluqueria.dto.ArticuloNf4Seccion1DTO;
import com.peluqueria.dto.ArticuloNf5Seccion2DTO;
import com.peluqueria.dto.ArticuloNf6Secnom6DTO;
import com.peluqueria.dto.ArticuloNf7Secnom7DTO;
import com.peluqueria.dto.IvaDTO;
import com.peluqueria.dto.MarcaDTO;
import com.peluqueria.dto.Nf1TipoDTO;
import com.peluqueria.dto.Nf2SfamiliaDTO;
import com.peluqueria.dto.Nf3SseccionDTO;
import com.peluqueria.dto.Nf4Seccion1DTO;
import com.peluqueria.dto.Nf5Seccion2DTO;
import com.peluqueria.dto.Nf6Secnom6DTO;
import com.peluqueria.dto.Nf7Secnom7DTO;
import com.peluqueria.dto.SeccionDTO;
import com.peluqueria.dto.SeccionSubDTO;
import com.peluqueria.dto.UnidadDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.stock.BuscarProveedorFXMLController.seteandoParam;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.StageSecond;
import com.javafx.util.Utilidades;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.peluqueria.core.domain.ArticuloProveedor;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class VerificarPrecioFXMLController extends BaseScreenController implements Initializable {

    static void setCostoLocal(String dato) {
        costoLocal = dato;
    }

    static void setCodArt(String text) {
        cod = text;
    }

    int nume = 0;

    private boolean alert;
    public static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private Date date = new Date();

    @Autowired
    ArticuloDAO artDAO;
    @Autowired
    ArticuloProveedorDAO articuloProveedorDAO;
    @Autowired
    ProveedorDAO proveedorDAO;

    Image image;

    public static String costoLocal = "";
    public static String cod = "";

    public static ArticuloNf1TipoDAO anf1TipoDAO = new ArticuloNf1TipoDAOImpl();
    public static ArticuloNf2SfamiliaDAO anf2SfamiliaDAO = new ArticuloNf2SfamiliaDAOImpl();
    public static ArticuloNf3SseccionDAO anf3SeccionDAO = new ArticuloNf3SseccionDAOImpl();
    public static ArticuloNf4Seccion1DAO anf4SeccionDAO = new ArticuloNf4Seccion1DAOImpl();
    public static ArticuloNf5Seccion2DAO anf5SeccionDAO = new ArticuloNf5Seccion2DAOImpl();
    public static ArticuloNf6Secnom6DAO anf6SeccionDAO = new ArticuloNf6Secnom6DAOImpl();
    public static ArticuloNf7Secnom7DAO anf7SeccionDAO = new ArticuloNf7Secnom7DAOImpl();

    public static Nf1TipoDAO nf1TipoDAO = new Nf1TipoDAOImpl();
    public static Nf2SfamiliaDAO nf2SfamiliaDAO = new Nf2SfamiliaDAOImpl();
    public static Nf3SseccionDAO nf3SeccionDAO = new Nf3SseccionDAOImpl();
    public static Nf4Seccion1DAO nf4SeccionDAO = new Nf4Seccion1DAOImpl();
    public static Nf5Seccion2DAO nf5SeccionDAO = new Nf5Seccion2DAOImpl();
    public static Nf6Secnom6DAO nf6SeccionDAO = new Nf6Secnom6DAOImpl();
    public static Nf7Secnom7DAO nf7SeccionDAO = new Nf7Secnom7DAOImpl();
    public static FacturaCompraDetDAO fcdDAO = new FacturaCompraDetDAOImpl();

    private List<JSONObject> depositoList = new ArrayList<>();
    private ObservableList<JSONObject> depositoData;

    @Autowired
    private ExistenciaDAO existenciaDAO;
    @Autowired
    private ArticuloProveedorDAO artProDAO;

    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    JSONParser parser = new JSONParser();
    JSONObject artBuscado = new JSONObject();
    //campos númericos
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();
    @Autowired
    ManejoLocalDAO manejoDAO;
    ManejoLocal manejoLocal = new ManejoLocal();
    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";

    private ObservableList<JSONObject> clienteData;
    private boolean escucha;
    private JSONObject jsonSeleccionActual;

    @FXML
    private TextArea txtAreaDesc;
    @FXML
    private ComboBox<String> cbFamilia;
    private HashMap<String, JSONObject> hashFamilia;
    @FXML
    private ComboBox<String> cbSubFamilia;
    private HashMap<String, JSONObject> hashSubFamilia;
    @FXML
    private ComboBox<String> cbCategoria;
    private HashMap<String, JSONObject> hashCategoria;
    @FXML
    private ComboBox<String> cbSubCategoria;
    private HashMap<String, JSONObject> hashSubCategoria;
    @FXML
    private ComboBox<String> cbClase;
    private HashMap<String, JSONObject> hashClase;
    @FXML
    private ComboBox<String> cbSubClase;
    private HashMap<String, JSONObject> hashSubClase;
    @FXML
    private ComboBox<String> cbSeccion;
    private HashMap<String, JSONObject> hashSeccion;
    @FXML
    private TextField txtCostoMonedaExt;
    @FXML
    private TextField txtCostoLocal;
    @FXML
    private ComboBox<String> cbImpuesto;
    @FXML
    private TextField txtIva;
    @FXML
    private TextField txtPorcDescProve;
    @FXML
    private TextField txtSensor;
    @FXML
    private TextField txtPorcDesc;
    @FXML
    private TextField txtOtrosGastos;
    @FXML
    private Label lala;
    @FXML
    private TextField txtPorcIncremParana01;
    @FXML
    private Label ldld;
    @FXML
    private TextField txtPorcIncremParana02;
    @FXML
    private Label cccc;
    @FXML
    private TextField PocIncremMayorista;
    @FXML
    private TextField txtPorcIncremVarios;
    @FXML
    private TextField txtCostoOrigen;
    @FXML
    private TextField txtPorcentajeDesc;
    @FXML
    private TextField txtPorcAduanero;
    @FXML
    private TextField txtCodProv01;
    @FXML
    private TextField txtCodProv02;
    @FXML
    private TextField txtProv01;
    @FXML
    private TextField txtProv02;
    @FXML
    private TextField txtCodProv03;
    @FXML
    private TextField txtProv03;
    @FXML
    private Button btnGuardar;
    @FXML
    private TextField txtCotizacion;
    @FXML
    private ComboBox<String> chkPromo;
    @FXML
    private ComboBox<String> chkPromoOutlet;
    @FXML
    private CheckBox chkProdImportado;
//    private CheckBox chkProdVigente;
//    private CheckBox chkNoStockeable;
    @FXML
    private CheckBox chkArtOculto;
    @FXML
    private CheckBox chkArtCompra;
    @FXML
    private CheckBox chkLeerBalanza;
    @FXML
    private Button btnSalir;
    @FXML
    private AnchorPane anchorPaneArticulos;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtCostoRealParana;
    @FXML
    private TextField txtRealProveedor;
    @FXML
    private TextField txtPorcDescParana;
    @FXML
    private TextField txtPorcDescProveedor;
    @FXML
    private TextField txtPrecioMin1;
    @FXML
    private TextField txtPrecioMin2;
    @FXML
    private ComboBox<String> cbPermiteDesc;
    @FXML
    private TextField txtDescVenta;
    @FXML
    private TextField txtPorcMD1;
    @FXML
    private TextField txtPorcMD2;
    @FXML
    private TextField txtDescMaxMay;
    @FXML
    private TextField txtPrecioMayNuevo1;
    @FXML
    private TextField txtPrecioMayNuevo2;
    @FXML
    private TextField txtPrecioMinOutlet1;
    @FXML
    private TextField txtPrecioMinOutlet2;
    @FXML
    private ComboBox<String> cbMonedaExt;
    @FXML
    private TextField txtCostoMonExt;
    @FXML
    private TextField txtMarca;
    @FXML
    private Button btnMarca;
    @FXML
    private Button btnProveedorEstandar;
    @FXML
    private Button btnProveedor1;
    @FXML
    private Button btnProveedor2;
    @FXML
    private Pane imgProducto000;
    @FXML
    private ImageView imgProducto;
    @FXML
    private TableView<JSONObject> tableViewDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo;
    @FXML
    private TextField txtPrecioMinCacique1;
    @FXML
    private TextField txtPrecioMinCacique2;
    @FXML
    private TextField txtPrecioMinSanlo1;
    @FXML
    private TextField txtPrecioMinSanlo2;
    @FXML
    private Label lala1;
    @FXML
    private TextField txtPorcIncremParana011;
    @FXML
    private TextField txtOtrosGastos1;
    @FXML
    private TextField txtPorcMD11;
    @FXML
    private TextField txtPorcMD111;
    @FXML
    private TextField txtPorcMD1111;
    @FXML
    private CheckBox chkLeerBalanza1;
    @FXML
    private Label txtCod;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelClienteBuscar;
    @FXML
    private TableView<JSONObject> tableViewCliente;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNombre;
    @FXML
    private TableColumn<JSONObject, String> tableColumnRucCi;
    @FXML
    private TextField textFieldRucCiBuscar;
    @FXML
    private TextField textFieldNombreBuscar;
    @FXML
    private Button buttonAnhadir;
    @FXML
    private Button buttonBuscarCliente;
    @FXML
    private TextField textFieldNombreCliente;
    @FXML
    private TextField textFieldRucCliente;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private Pane panePrincipal;

    private List<JSONObject> clienteList;
    @FXML
    private TextField txtCostoFinalImportado;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void btnEditarAction(ActionEvent event) {
        actualizarArticulo();
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        registrarArticulo();
    }

    private void btnBuscarAction(ActionEvent event) {
        busquedaArticulo();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAperturaEsteticaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneArticulos.getChildren()
                .get(anchorPaneArticulos.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void cargandoInicial() {
        anchorPaneBuscar.setVisible(false);
        panePrincipal.setDisable(false);
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            if (DatosEnCaja.getFacturados() != null) {
                fact = DatosEnCaja.getFacturados();
            } else {
                fact = new JSONObject();
            }
        }
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        depositoList = new ArrayList<>();
//        listenTextField();
        alert = false;
//        ubicandoContenedorSecundario();
        listenerCampos();
        cargarComponentes();
        cargarImagen();
        busquedaArticulo();
        cbPermiteDesc.getItems().add("SI");
        cbPermiteDesc.getItems().add("NO");
        cbImpuesto.getItems().add("GRA");
        cbImpuesto.getItems().add("EXE");
        cbMonedaExt.getItems().addAll("DOLAR", "PESO", "REAL");
        if (!costoLocal.equalsIgnoreCase("")) {
            txtCostoLocal.setText(costoLocal);
            if (txtPorcIncremParana01.getText().equals("")) {
                if (!txtPrecioMin1.getText().equals("")) {
                    calcularPorcentaje(txtPrecioMin1.getText());
                }
            } else {
                calcularPorcentaje(txtPrecioMin1.getText());
            }
        }
        txtCod.setText(FacturaCompraFXMLController.getCodArt());
        cargandoImagen();
        try {
            cbMonedaExt.setValue(FacturaCompraFXMLController.moneda);
            txtCotizacion.setText(FacturaCompraFXMLController.cotizacion);
        } catch (Exception e) {
        } finally {
        }
    }

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    private void volviendo() {
//        if (!FacturaCompraFXMLController.insertar) {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        } else {
//            FacturaCompraFXMLController.insertar = false;
//            this.sc.loadScreen("/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, true);
//        }

    }

    //Apartado 4 - AVISOS
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            alert2.close();
        }
    }

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtCostoOrigen.isFocused()) {
                calcularCostoOrigen();
            } else if (txtCostoFinalImportado.isFocused()) {
                calcularCostoFinalImportado();
            } else if (anchorPaneBuscar.isVisible()) {
                addProveedor();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
        if (keyCode == event.getCode().F2) {
            registrarArticulo();
        }
        if (keyCode == event.getCode().F1) {
            buscandoCliente(true);
        }
    }

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
//    private void listenTextField() {
//        txtMontoApertura.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtMontoApertura.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
//            String newV = "";
//            String oldV = "";
//            if (!newValue.contentEquals("")) {
//                newV = numValidator.numberValidator(newValue);
//                oldV = numValidator.numberValidator(oldValue);
//                long lim = -1l;
//                boolean limite = true;
//                if (!newV.contentEquals("")) {//límite del monto...
//                    if (newV.length() < 19) {
//                        lim = Long.valueOf(newV);
//                        if (lim > 0 && lim < 2147483647) {
//                            limite = false;
//                        }
//                    }
//                }
//                if (limite) {
//                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
//                        if (patternMonto) {
//                            if (oldV.length() != 0) {
//                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                            } else {
//                                param = oldValue;
//                            }
//                            patternMonto = false;
//                        } else {
//                            patternMonto = true;
//                            param = oldValue;
//                        }
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(param);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText("");
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                    if (!oldValue.contentEquals("")) {
//                        if (!newValue.contentEquals("Gs ")) {
//                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                                if (patternMonto) {
//                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                                    patternMonto = false;
//                                } else {
//                                    patternMonto = true;
//                                    param = oldValue;
//                                }
//                                Platform.runLater(() -> {
//                                    txtMontoApertura.setText(param);
//                                    txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                                });
//                            }
//                        } else {
//                            Platform.runLater(() -> {
//                                txtMontoApertura.setText("");
//                                txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(oldValue);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                    if (patternMonto) {
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                    }
//                    Platform.runLater(() -> {
//                        txtMontoApertura.setText(param);
//                        txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                    });
//                }
//            }
//        });
//    }
//    Apartado 6 - BACKEND
    private void cargarComponentes() {
//        cargarEmpresa();
        cargarFlia1();
//        cargarFlia2();
//        cargarFlia3();
//        cargarFlia4();
//        cargarFlia5();
//        cargarFlia6();
//        cargarFlia7();
    }

//    private void cargarEmpresa() {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONArray valor = new JSONArray();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/empresa");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//            if (!valor.isEmpty()) {
//                for (Object obj : valor) {
//                    JSONObject json = (JSONObject) parser.parse(obj.toString());
//                    cbEmpresa.getItems().add(json.get("descripcionEmpresa").toString());
//                }
//            }
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        }
//    }
    private void cargarFlia1() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashFamilia = new HashMap<>();
        List<Nf1Tipo> listNF1 = nf1TipoDAO.listar();// anf1TipoDAO.listar();
        cbFamilia.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(1);
        hashFamilia.put("SIN SELECCION", new JSONObject());
        if (listNF1.size() > 0) {
            for (Nf1Tipo obj : listNF1) {
                cbFamilia.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashFamilia.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf1TipoDTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf1Tipo");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//            cbFamilia.getItems().add("*** SIN SELECCION ***");
//            limpiarNiveles(1);
//            hashFamilia.put("SIN SELECCION", new JSONObject());
//            if (!valor.isEmpty()) {
//                for (Object obj : valor) {
//                    JSONObject json = (JSONObject) parser.parse(obj.toString());
//                    cbFamilia.getItems().add(json.get("descripcion").toString());
//                    hashFamilia.put(json.get("descripcion").toString(), json);
//                }
//            }
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        }
    }

    private void cargarFlia2(long idNf1) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashSubFamilia = new HashMap<>();
        List<Nf2Sfamilia> listNF2 = nf2SfamiliaDAO.listarPorNf(idNf1);// anf1TipoDAO.listar();
        cbSubFamilia.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(2);
        hashSubFamilia.put("SIN SELECCION", new JSONObject());
        if (listNF2.size() > 0) {
            for (Nf2Sfamilia obj : listNF2) {
                cbSubFamilia.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubFamilia.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf2SfamiliaDTOEntitiesNullNf1())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf2Sfamilia/" + idNf1);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubFamilia.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(2);
//        hashSubFamilia.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubFamilia.getItems().add(json.get("descripcion").toString());
//                    hashSubFamilia.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void cargarFlia3(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashCategoria = new HashMap<>();
        cbCategoria.getItems().clear();
        List<Nf3Sseccion> listNF3 = nf3SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbCategoria.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(3);
        hashCategoria.put("SIN SELECCION", new JSONObject());
        if (listNF3.size() > 0) {
            for (Nf3Sseccion obj : listNF3) {
                cbCategoria.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashCategoria.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf3SseccionDTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf3Sseccion/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbCategoria.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(3);
//        hashCategoria.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbCategoria.getItems().add(json.get("descripcion").toString());
//                    hashCategoria.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void cargarFlia4(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashSubCategoria = new HashMap<>();
        cbSubCategoria.getItems().clear();
        List<Nf4Seccion1> listNF4 = nf4SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSubCategoria.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(4);
        hashSubCategoria.put("SIN SELECCION", new JSONObject());
        if (listNF4.size() > 0) {
            for (Nf4Seccion1 obj : listNF4) {
                cbSubCategoria.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubCategoria.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf4Seccion1DTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf4Seccion1/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubCategoria.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(4);
//        hashSubCategoria.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubCategoria.getItems().add(json.get("descripcion").toString());
//                    hashSubCategoria.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void cargarFlia5(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashClase = new HashMap<>();
        cbClase.getItems().clear();
        List<Nf5Seccion2> listNF5 = nf5SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbClase.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(5);
        hashClase.put("SIN SELECCION", new JSONObject());
        if (listNF5.size() > 0) {
            for (Nf5Seccion2 obj : listNF5) {
                cbClase.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashClase.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf5Seccion2DTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf5Seccion2/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbClase.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(5);
//        hashClase.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbClase.getItems().add(json.get("descripcion").toString());
//                    hashClase.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void cargarFlia6(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashSubClase = new HashMap<>();
        cbSubClase.getItems().clear();
        List<Nf6Secnom6> listNF6 = nf6SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSubClase.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(6);
        hashSubClase.put("SIN SELECCION", new JSONObject());
        if (listNF6.size() > 0) {
            for (Nf6Secnom6 obj : listNF6) {
                cbSubClase.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSubClase.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf6Secnom6DTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf6Secnom6/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSubClase.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(6);
//        hashSubClase.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSubClase.getItems().add(json.get("descripcion").toString());
//                    hashSubClase.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void cargarFlia7(long id) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        hashSeccion = new HashMap<>();
        cbSeccion.getItems().clear();
        List<Nf7Secnom7> listNF7 = nf7SeccionDAO.listarPorNf(id);// anf1TipoDAO.listar();
        cbSeccion.getItems().add("*** SIN SELECCION ***");
        limpiarNiveles(7);
        hashSeccion.put("SIN SELECCION", new JSONObject());
        if (listNF7.size() > 0) {
            for (Nf7Secnom7 obj : listNF7) {
                cbSeccion.getItems().add(obj.getDescripcion());
                try {
                    obj.setFechaAlta(null);
                    obj.setFechaMod(null);
                    hashSeccion.put(obj.getDescripcion(), (JSONObject) parser.parse(gson.toJson(obj.toNf7Secnom7DTOEntitiesNull())));
                } catch (ParseException ex) {
                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/nf7Secnom7/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        cbSeccion.getItems().add("*** SIN SELECCION ***");
//        limpiarNiveles(7);
//        hashSeccion.put("SIN SELECCION", new JSONObject());
//        if (!valor.isEmpty()) {
//            for (Object obj : valor) {
//                JSONObject json;
//                try {
//                    json = (JSONObject) parser.parse(obj.toString());
//                    cbSeccion.getItems().add(json.get("descripcion").toString());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
//                } catch (ParseException ex) {
//                    Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
    }

    private void busquedaArticulo() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articulo/" + txtCodigo.getText());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//                artBuscado = valor;
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        Articulo art = artDAO.buscarCod(FacturaCompraFXMLController.getCodArt());
        if (art != null) {
            txtCod.setText(FacturaCompraFXMLController.getCodArt());
            txtAreaDesc.setText(art.getDescripcion());
            txtCostoMonedaExt.setText("0");
            txtCostoLocal.setText(art.getCosto().toString());
            txtPorcMD2.setText(art.getMd());
            txtPorcDescProve.setText("0");
            txtPorcDesc.setText("0");
//            txtPorcIncremParana01 calculo;
            txtPorcIncremParana02.setText("0");
            PocIncremMayorista.setText("0");
            txtPorcIncremVarios.setText("0");

            txtIva.setText(art.getIva().getPoriva() + "");
            txtSensor.setText("0");
            txtOtrosGastos.setText("0");

            txtCostoRealParana.setText("0");
            txtPorcDescParana.setText("0");
            txtPorcDescProveedor.setText("0");
            txtRealProveedor.setText(art.getCosto().toString());

            txtCostoMonExt.setText("0");
            txtCostoOrigen.setText("0");
            txtPorcentajeDesc.setText("0");
            txtPorcAduanero.setText("0");

            try {
                txtPrecioMin1.setText(art.getPrecioMin().toString());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtPrecioMin2.setText(art.getPrecioMin().toString());
            } catch (Exception e) {
            } finally {
            }

            txtDescVenta.setText("0");
            txtDescMaxMay.setText("0");
            try {
                txtPrecioMayNuevo1.setText(art.getPrecioMay().toString());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtPrecioMayNuevo2.setText(art.getPrecioMay().toString());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtPrecioMinOutlet1.setText(art.getPrecioMin().toString());
            } catch (Exception e) {
            } finally {
            }
            try {
                txtPrecioMinOutlet2.setText(art.getPrecioMin().toString());
            } catch (Exception e) {
            } finally {
            }

            //            txtAreaDesc.setText(art.getDescripcion());
            //            txtCostoMonedaExt.setText("0");
            try {
                if (art.getPermiteDesc()) {
                    cbPermiteDesc.setValue("SI");
                } else {
                    cbPermiteDesc.setValue("NO");
                }
            } catch (Exception e) {
                cbPermiteDesc.setValue("NO");
            } finally {
            }
            try {
                txtPrecioMinOutlet1.setText(art.getOutlet() + "");
                txtPrecioMinOutlet2.setText(art.getOutlet() + "");
            } catch (Exception e) {
                txtPrecioMinOutlet1.setText("0");
                txtPrecioMinOutlet2.setText("0");
            } finally {
            }
            try {
                txtPrecioMinCacique1.setText(art.getCacique() + "");
                txtPrecioMinCacique2.setText(art.getCacique() + "");
            } catch (Exception e) {
                txtPrecioMinCacique1.setText("0");
                txtPrecioMinCacique2.setText("0");
            } finally {
            }
            try {
                txtPrecioMinSanlo1.setText(art.getSanlo() + "");
                txtPrecioMinSanlo2.setText(art.getSanlo() + "");
            } catch (Exception e) {
                txtPrecioMinSanlo1.setText("0");
                txtPrecioMinSanlo2.setText("0");
            } finally {
            }
            try {
                txtDescMaxMay.setText(art.getDescMaxMay() + "");
                calcularDescuento();
            } catch (Exception e) {
                txtDescMaxMay.setText("0");
                calcularDescuento();
            } finally {
            }
            try {
                txtPrecioMinCacique1.setText(art.getCacique() + "");
            } catch (Exception e) {
                txtPrecioMinCacique1.setText("0");
            } finally {
            }
            try {
                if (art.getIva().getPoriva().toString().equals("0")) {
                    cbImpuesto.setValue("EXE");
                } else {
                    cbImpuesto.setValue("GRA");
                }
            } catch (Exception e) {
                cbImpuesto.setValue("EXE");
            } finally {
            }

            try {
                if (Boolean.parseBoolean(valor.get("importado").toString())) {
                    chkProdImportado.setSelected(true);
                } else {
                    chkProdImportado.setSelected(false);
                }
            } catch (Exception e) {
                chkProdImportado.setSelected(false);
            } finally {
            }

//            try {
//                if (Boolean.parseBoolean(valor.get("stockeable").toString())) {
//                    chkNoStockeable.setSelected(true);
//                } else {
//                    chkNoStockeable.setSelected(false);
//                }
//            } catch (Exception e) {
//                chkNoStockeable.setSelected(false);
//            } finally {
//            }
            List<ArticuloProveedor> listArtPro = artProDAO.listarPorArticulo(art.getIdArticulo());
            int ord = 0;
            for (ArticuloProveedor articuloProveedor : listArtPro) {
                switch (ord) {
                    case 0:
                        txtCodProv01.setText(articuloProveedor.getProveedor().getRuc());
                        txtProv01.setText(articuloProveedor.getProveedor().getDescripcion());
                        break;
                    case 1:
                        txtCodProv02.setText(articuloProveedor.getProveedor().getRuc());
                        txtProv02.setText(articuloProveedor.getProveedor().getDescripcion());
                        break;
                    case 2:
                        txtCodProv03.setText(articuloProveedor.getProveedor().getRuc());
                        txtProv03.setText(articuloProveedor.getProveedor().getDescripcion());
                        break;
                    default:
                        break;
                }
                ord++;
            }

            List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
            for (Existencia ex : listExis) {
//                try {
                Articulo articu = new Articulo();
                articu.setIdArticulo(art.getIdArticulo());

                ex.setArticulo(articu);
//                    JSONObject objJSON = (JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objJSON = new JSONObject();//(JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objART = new JSONObject();
                objART.put("idArticulo", art.getIdArticulo());

                JSONObject objDEPO = new JSONObject();
                objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
                objDEPO.put("descripcion", ex.getDeposito().getDescripcion());

                objJSON.put("idExistencia", ex.getIdExistencia());
                objJSON.put("cantidad", ex.getCantidad());
                objJSON.put("articulo", objART);
                objJSON.put("deposito", objDEPO);

                depositoList.add(objJSON);

                actualizandoTablaDeposito();
//                } catch (ParseException ex1) {
//                    Logger.getLogger(FacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
            }

            calcularPorcentaje(art.getPrecioMin().toString());
            verificarNivel1(art.getIdArticulo());
        } else {
//            mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
        }
    }

    private void verificarNivel7(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf7Secnom7/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf7Secnom7 anf7 = anf7SeccionDAO.getByIdArticulo(id);
            if (anf7 != null) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf6Secnom6").toString());
                nivel07 = anf7.getNf7Secnom7().getDescripcion();
                cbSeccion.getSelectionModel().select(nivel07);
//            cargarFlia7(anf6.getNf6Secnom6().getIdNf6Secnom6());
//            verificarNivel7(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }

//        if (!valor.isEmpty()) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf7Secnom7").toString());
//                nivel07 = json.get("descripcion").toString();
//                cbSeccion.getSelectionModel().select(json.get("descripcion").toString());
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    private void verificarNivel6(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf6Secnom6/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf6Secnom6 anf6 = anf6SeccionDAO.getByIdArticulo(id);
            if (anf6 != null) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf6Secnom6").toString());
                nivel06 = anf6.getNf6Secnom6().getDescripcion();
                cbSubClase.getSelectionModel().select(nivel06);
                cargarFlia7(anf6.getNf6Secnom6().getIdNf6Secnom6());
                verificarNivel7(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }
    }

    private void verificarNivel5(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf5Seccion2/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf5Seccion2 anf5 = anf5SeccionDAO.getByIdArticulo(id);
            if (anf5 != null) {
//            try {

                nivel05 = anf5.getNf5Seccion2().getDescripcion();
                cbClase.getSelectionModel().select(nivel05);
                cargarFlia6(anf5.getNf5Seccion2().getIdNf5Seccion2());
                verificarNivel6(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }

    }

    private void verificarNivel4(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf4Seccion1/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf4Seccion1 anf4 = anf4SeccionDAO.getByIdArticulo(id);
//        if (anf4 != null) {
//            try {
            nivel04 = anf4.getNf4Seccion1().getDescripcion();
            cbSubCategoria.getSelectionModel().select(nivel04);
            cargarFlia5(anf4.getNf4Seccion1().getIdNf4Seccion1());
            verificarNivel5(id);
        } catch (Exception e) {
        } finally {
        }

//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }
    }

    private void verificarNivel3(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf3Sseccion/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf3Sseccion anf3 = anf3SeccionDAO.getByIdArticulo(id);
            if (anf3 != null) {
//            try {
                nivel03 = anf3.getNf3Sseccion().getDescripcion();
                cbCategoria.getSelectionModel().select(nivel03);
                cargarFlia4(anf3.getNf3Sseccion().getIdNf3Sseccion());
                verificarNivel4(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }

    }

    private void verificarNivel2(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf2Sfamilia/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        try {
            ArticuloNf2Sfamilia anf2 = anf2SfamiliaDAO.getByIdArticulo(id);
            if (anf2 != null) {
//            try {
                nivel02 = anf2.getNf2Sfamilia().getDescripcion();
                cbSubFamilia.getSelectionModel().select(nivel02);
                cargarFlia3(anf2.getNf2Sfamilia().getIdNf2Sfamilia());
                verificarNivel3(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }

    }

    private void verificarNivel1(long id) {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articuloNf1Tipo/" + id);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }

        try {
            ArticuloNf1Tipo anf1 = anf1TipoDAO.getByIdArticulo(id);
            if (anf1 != null) {
//            try {
//                JSONObject json = (JSONObject) parser.parse(valor.get("nf1Tipo").toString());
                nivel01 = anf1.getNf1Tipo().getDescripcion();
                cbFamilia.getSelectionModel().select(nivel01);
                cargarFlia2(Long.parseLong(anf1.getNf1Tipo().getIdNf1Tipo().toString()));

                verificarNivel2(id);
//            } catch (ParseException ex) {
//                Logger.getLogger(VerificarPrecioFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//            }
            }
        } catch (Exception e) {
        } finally {
        }
    }

    @FXML
    private void cbFamiliaMouseClicked(MouseEvent event) {
//        mouseEventCbFamilia(event);
    }

    @FXML
    private void cbFamiliaMouseKeyReleased(KeyEvent event) {
        mouseEventCbFamilia();
    }

    private void mouseEventCbFamilia() {
    }

    private void listenerCampos() {
        cbFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                cbSubFamilia.getItems().clear();
                JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
                cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
            }
        });
        cbSubFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                JSONObject json = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
                cargarFlia3(Long.parseLong(json.get("idNf2Sfamilia").toString()));
            }
        });
        cbCategoria.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                JSONObject json = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
                cargarFlia4(Long.parseLong(json.get("idNf3Sseccion").toString()));
            }
        });
        cbSubCategoria.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                JSONObject json = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
                cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
            }
        });
        cbClase.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                JSONObject json = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
                cargarFlia6(Long.parseLong(json.get("idNf5Seccion2").toString()));
            }
        });
        cbSubClase.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                JSONObject json = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
                cargarFlia7(Long.parseLong(json.get("idNf6Secnom6").toString()));
            }
        });
    }

    private void limpiarNiveles(int val) {
        switch (val) {
            case 1:
                cbSubFamilia.getItems().clear();
                cbCategoria.getItems().clear();
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 2:
                cbCategoria.getItems().clear();
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 3:
                cbSubCategoria.getItems().clear();
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 4:
                cbClase.getItems().clear();
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 5:
                cbSubClase.getItems().clear();
                cbSeccion.getItems().clear();
                break;
            case 6:
                cbSeccion.getItems().clear();
                break;
            case 7:
                break;
            default:
                break;
        }
    }

    private void registrarArticulo() {
        if (FacturaCompraFXMLController.insertar && txtIva.getText().equals("")) {
            mensajeAlerta("EL CAMPO IVA NO DEBE QUEDAR VACIO");
            txtIva.requestFocus();
        } else {
            Articulo art = new Articulo();
            if (!FacturaCompraFXMLController.insertar) {
                art = artDAO.buscarCod(FacturaCompraFXMLController.getCodArt());
            }
            art.setCostoExtranjero(txtCostoMonExt.getText());
            art.setCostoOrigenImportado(txtCostoOrigen.getText());
            art.setDescImportado(txtPorcentajeDesc.getText());
            art.setIncreImportado(txtPorcAduanero.getText());
            art.setIncreParana(txtPorcIncremParana01.getText());
            art.setIncreParana2(txtPorcIncremParana011.getText());
            art.setIncreParana3(txtPorcIncremVarios.getText());
            art.setIncreMay(PocIncremMayorista.getText());
            art.setCotizacion(txtCotizacion.getText());
            if (!txtCostoFinalImportado.getText().equals("")) {
                art.setImportado(true);
            }

            try {
                art.setPrecioMin(Long.parseLong(txtPrecioMin1.getText()));
            } catch (Exception e) {
            } finally {
            }
            try {
                art.setPrecioMay(Long.parseLong(txtPrecioMayNuevo1.getText()));
            } catch (Exception e) {
            } finally {
            }
            try {
                if (!txtCostoLocal.getText().equals("")) {
                    art.setCosto(Long.parseLong(txtCostoLocal.getText()));
                } else {
                    art.setCosto(Long.parseLong(txtCostoFinalImportado.getText()));
                }

            } catch (Exception e) {
            } finally {
            }
            try {
                art.setDescMaxMay(Long.parseLong(txtDescMaxMay.getText()));
            } catch (Exception e) {
            } finally {
            }
            try {
                art.setOutlet(Long.parseLong(txtPrecioMinOutlet1.getText()));
            } catch (Exception e) {
            } finally {
            }
            try {
                art.setCacique(Long.parseLong(txtPrecioMinCacique1.getText()));
            } catch (Exception e) {
            } finally {
            }
            try {
                art.setSanlo(Long.parseLong(txtPrecioMinSanlo1.getText()));
            } catch (Exception e) {
            } finally {
            }
            art.setDescripcion(txtAreaDesc.getText());
            art.setCodArticulo((txtCod.getText()));
            try {
                if (FacturaCompraFXMLController.insertar) {
//                    if (!txtIva.getText().equals("")) {
                    Iva iva = new Iva();
                    if (txtIva.getText().equals("5")) {
                        iva.setIdIva(2L);
                    } else if (txtIva.getText().equals("10")) {
                        iva.setIdIva(3L);
                    } else {
                        iva.setIdIva(1L);
                    }
                    art.setIva(iva);
                    artDAO.insertar(art);
//                    }
                }
                actualizarNiveles(art.getIdArticulo());
            } catch (Exception e) {
            } finally {
            }
            try {
                try {
                    long numerador = art.getPrecioMin() - art.getCosto();
                    long denominador = art.getPrecioMin();
//                int desc = 100 - Integer.parseInt(txtDescMaxMay.getText());
                    float porc = (float) numerador / denominador;
//                double pM = Long.parseLong(txtPrecioMin1.getText()) * porc;
//                long precioMay = (long) pM;
                    art.setMd((porc * 100) + "");
                } catch (Exception e) {
                } finally {
                }
            } catch (Exception e) {
            } finally {
            }

            if (!FacturaCompraFXMLController.insertar) {
                artDAO.actualizar(art);
            } else {
//            art.setDescripcion(txtAreaDesc.getText());
//            art.setCodArticulo(Long.parseLong(txtCod.getText()));
//            artDAO.insertar(art);
            }

            if (!txtCodProv01.getText().equals("")) {
                Proveedor p1 = proveedorDAO.listarPorRuc(txtCodProv01.getText());
                Articulo art1 = new Articulo();
                art1.setIdArticulo(art.getIdArticulo());

                ArticuloProveedor ap1 = new ArticuloProveedor();
                ap1.setArticulo(art1);
                ap1.setProveedor(p1);
                ap1.setFechaAlta(timestamp);
                ap1.setFechaMod(timestamp);

                articuloProveedorDAO.insertar(ap1);
            }
            if (!txtCodProv02.getText().equals("")) {
                Proveedor p2 = proveedorDAO.listarPorRuc(txtCodProv02.getText());
                Articulo art2 = new Articulo();
                art2.setIdArticulo(art.getIdArticulo());

                ArticuloProveedor ap2 = new ArticuloProveedor();
                ap2.setArticulo(art2);
                ap2.setProveedor(p2);
                ap2.setFechaAlta(timestamp);
                ap2.setFechaMod(timestamp);

                articuloProveedorDAO.insertar(ap2);
            }
            if (!txtCodProv03.getText().equals("")) {
                Proveedor p3 = proveedorDAO.listarPorRuc(txtCodProv03.getText());
                Articulo art3 = new Articulo();
                art3.setIdArticulo(art.getIdArticulo());

                ArticuloProveedor ap3 = new ArticuloProveedor();
                ap3.setArticulo(art3);
                ap3.setProveedor(p3);
                ap3.setFechaAlta(timestamp);
                ap3.setFechaMod(timestamp);

                articuloProveedorDAO.insertar(ap3);
            }

            mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE...");
            FacturaCompraFXMLController.insertar = false;
            volviendo();
//        long exito = 0l;
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonDatos = cargarArticulo();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        jsonDatos.put("fechaAlta", timestampJSON);
//        jsonDatos.put("fechaMod", timestampJSON);
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/articulo/insertar");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(jsonDatos.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exito = (long) parser.parse(inputLine);
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        } catch (ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }
//        if (exito
//                > 0) {
//            if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
//                registrarNiveles(exito, true);
//            }
//        } else {
//            mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS, VERIFIQUELOS");
        }
    }

    private JSONObject cargarArticulo() {

//        JSONObject jsonArticulo = new JSONObject();
        ArticuloDTO art = new ArticuloDTO();
        art.setDescripcion(txtAreaDesc.getText());
        art.setPrecioMay(Long.parseLong(txtCostoMonedaExt.getText()));
        art.setPrecioMin(Long.parseLong(txtCostoMonedaExt.getText()));

//        Timestamp tsNow = new Timestamp(date.getTime());
//        Long timestampNOW = tsNow.getTime();
//        art.setFechaAlta(timestampNOW);
//        art.setFechaAlta(timestampNOW);
// jsonCabFactura.put("fechaMod", timestampEmision);
//        jsonCabFactura.put("usuAlta", );
//        jsonCabFactura.put("usuMod", Identity.getNomFun());
        art.setUsuAlta(Identity.getNomFun());
        art.setUsuMod(Identity.getNomFun());
        art.setPermiteDesc(false);
//        art.setCodArticulo(Long.parseLong(txtCodigo.getText()));
        art.setServicio(false);
        art.setBajada(false);
        if (chkProdImportado.isSelected()) {
            art.setImportado(true);
        } else {
            art.setImportado(false);
        }
//        if (chkNoStockeable.isSelected()) {
//            art.setStockeable(true);
//        } else {
//            art.setStockeable(false);
//        }

        IvaDTO ivaDTO = new IvaDTO();
        ivaDTO.setIdIva(1l);

        MarcaDTO marcaDTO = new MarcaDTO();
        marcaDTO.setIdMarca(0l);

        UnidadDTO uDTO = new UnidadDTO();
        uDTO.setIdUnidad(1l);

        SeccionSubDTO ssDTO = new SeccionSubDTO();
        ssDTO.setIdSeccionSub(0l);

        SeccionDTO sDTO = new SeccionDTO();
        sDTO.setIdSeccion(0l);

        art.setIva(ivaDTO);
        art.setMarca(marcaDTO);
        art.setUnidad(uDTO);
        art.setSeccionSub(ssDTO);
        art.setSeccion(sDTO);

        try {
            return (JSONObject) parser.parse(gson.toJson(art));
        } catch (ParseException ex) {
            return null;
        }
    }

    public void limpiar() {
//        txtCodigo.setText("");
        txtAreaDesc.setText("");

        cbFamilia.getSelectionModel().select(0);
        cbSubFamilia.getItems().clear();
        cbCategoria.getItems().clear();
        cbSubCategoria.getItems().clear();
        cbClase.getItems().clear();
        cbSubClase.getItems().clear();
        cbSeccion.getItems().clear();

        txtCostoMonedaExt.setText("");
        txtCostoLocal.setText("");
        txtPorcDescProve.setText("");
        txtPorcDesc.setText("");
        txtPorcIncremParana01.setText("");
        txtPorcIncremParana02.setText("");
        PocIncremMayorista.setText("");
        txtPorcIncremVarios.setText("");

        txtIva.setText("");
        txtSensor.setText("");
        txtOtrosGastos.setText("");

        txtCostoOrigen.setText("");
        txtPorcentajeDesc.setText("");
        txtPorcAduanero.setText("");

        txtCodProv01.setText("");
        txtCodProv02.setText("");
        txtCodProv03.setText("");
        txtProv01.setText("");
        txtProv02.setText("");
        txtProv03.setText("");

        chkProdImportado.setSelected(false);
//        chkProdVigente.setSelected(false);
//        chkNoStockeable.setSelected(false);
        chkArtOculto.setSelected(false);
        chkArtCompra.setSelected(false);
        chkLeerBalanza.setSelected(false);

        cbMonedaExt.getSelectionModel().select(0);
        chkPromo.getSelectionModel().select(0);
        chkPromoOutlet.getSelectionModel().select(0);

        txtCotizacion.setText("");
    }

    private void registrarNiveles(long idArt, boolean dato) {
//        if (dato) {
        mensajeAlerta("DATOS INSERTADOS EXITOSAMENTE");
//        }
        if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(1, idArt);
        }
        if (cbSubFamilia.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(2, idArt);
        }
        if (cbCategoria.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(3, idArt);
        }
        if (cbSubCategoria.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(4, idArt);
        }
        if (cbClase.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(5, idArt);
        }
        if (cbSubClase.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(6, idArt);
        }
        if (cbSeccion.getSelectionModel().getSelectedIndex() > 0) {
            setearNivelParametrizado(7, idArt);
        }
        limpiar();
    }

    private void setearNivelParametrizado(int val, long idArt) {
        JSONObject objInsert = new JSONObject();
        ArticuloDTO artDTO = new ArticuloDTO();
        String nivel = "";
        try {
            switch (val) {
                case 1:
                    ArticuloNf1TipoDTO aNF1DTO = new ArticuloNf1TipoDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF1DTO.setArticulo(artDTO);

                    Nf1TipoDTO n1 = new Nf1TipoDTO();
                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
                    n1.setIdNf1Tipo(Long.parseLong(json.get("idNf1Tipo").toString()));
                    aNF1DTO.setNf1Tipo(n1);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF1DTO));
                    nivel = "articuloNf1Tipo";
                    break;
                case 2:
                    ArticuloNf2SfamiliaDTO aNF2DTO = new ArticuloNf2SfamiliaDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF2DTO.setArticulo(artDTO);

                    Nf2SfamiliaDTO n2 = new Nf2SfamiliaDTO();
                    JSONObject jsonSubFlia = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
                    n2.setIdNf2Sfamilia(Long.parseLong(jsonSubFlia.get("idNf2Sfamilia").toString()));
                    aNF2DTO.setNf2Sfamilia(n2);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF2DTO));
                    nivel = "articuloNf2Sfamilia";
                    break;
                case 3:
                    ArticuloNf3SseccionDTO aNF3DTO = new ArticuloNf3SseccionDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF3DTO.setArticulo(artDTO);

                    Nf3SseccionDTO n3 = new Nf3SseccionDTO();
                    JSONObject jsonCategoria = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
                    n3.setIdNf3Sseccion(Long.parseLong(jsonCategoria.get("idNf3Sseccion").toString()));
                    aNF3DTO.setNf3Sseccion(n3);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF3DTO));
                    nivel = "articuloNf3Sseccion";
                    break;
                case 4:
                    ArticuloNf4Seccion1DTO aNF4DTO = new ArticuloNf4Seccion1DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF4DTO.setArticulo(artDTO);

                    Nf4Seccion1DTO n4 = new Nf4Seccion1DTO();
                    JSONObject jsonSubCategoria = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
                    n4.setIdNf4Seccion1(Long.parseLong(jsonSubCategoria.get("idNf4Seccion1").toString()));
                    aNF4DTO.setNf4Seccion1(n4);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF4DTO));
                    nivel = "articuloNf4Seccion1";
                    break;
                case 5:
                    ArticuloNf5Seccion2DTO aNF5DTO = new ArticuloNf5Seccion2DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF5DTO.setArticulo(artDTO);

                    Nf5Seccion2DTO n5 = new Nf5Seccion2DTO();
                    JSONObject jsonClase = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
                    n5.setIdNf5Seccion2(Long.parseLong(jsonClase.get("idNf5Seccion2").toString()));
                    aNF5DTO.setNf5Seccion2(n5);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF5DTO));
                    nivel = "articuloNf5Seccion2";
                    break;
                case 6:
                    ArticuloNf6Secnom6DTO aNF6DTO = new ArticuloNf6Secnom6DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF6DTO.setArticulo(artDTO);

                    Nf6Secnom6DTO n6 = new Nf6Secnom6DTO();
                    JSONObject jsonSubClase = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
                    n6.setIdNf6Secnom6(Long.parseLong(jsonSubClase.get("idNf6Secnom6").toString()));
                    aNF6DTO.setNf6Secnom6(n6);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF6DTO));
                    nivel = "articuloNf6Secnom6";
                    break;
                case 7:
                    ArticuloNf7Secnom7DTO aNF7DTO = new ArticuloNf7Secnom7DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF7DTO.setArticulo(artDTO);

                    Nf7Secnom7DTO n7 = new Nf7Secnom7DTO();
                    JSONObject jsonSeccion = hashSeccion.get(cbSeccion.getSelectionModel().getSelectedItem());
                    n7.setIdNf7Secnom7(Long.parseLong(jsonSeccion.get("idNf7Secnom7").toString()));
                    aNF7DTO.setNf7Secnom7(n7);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF7DTO));
                    nivel = "articuloNf7Secnom7";
                    break;
                default:
                    break;

            }
        } catch (ParseException ex) {
            Logger.getLogger(VerificarPrecioFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        if (!objInsert.toString().equals("{}")) {
            registrandoNiveles(objInsert, nivel);
        }
    }

    private void registrandoNiveles(JSONObject objInsert, String nivel) {

//        String inputLine;
        JSONParser parser = new JSONParser();
        if (nivel.equalsIgnoreCase("articuloNf7Secnom7")) {
            anf7SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf7Secnom7.class));
        } else if (nivel.equalsIgnoreCase("articuloNf6Secnom6")) {
            anf6SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf6Secnom6.class));
        } else if (nivel.equalsIgnoreCase("articuloNf5Seccion2")) {
            anf5SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf5Seccion2.class));
        } else if (nivel.equalsIgnoreCase("articuloNf4Seccion1")) {
            anf4SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf4Seccion1.class));
        } else if (nivel.equalsIgnoreCase("articuloNf3Sseccion")) {
            anf3SeccionDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf3Sseccion.class));
        } else if (nivel.equalsIgnoreCase("articuloNf2Sfamilia")) {
            anf2SfamiliaDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf2Sfamilia.class));
        } else if (nivel.equalsIgnoreCase("articuloNf1Tipo")) {
            anf1TipoDAO.insertar(gson.fromJson(objInsert.toString(), ArticuloNf1Tipo.class));
        }
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(objInsert.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        }
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel);
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(objInsert.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        }
    }

    private void actualizarArticulo() {
        if (!artBuscado.toString().equalsIgnoreCase("{}")) {
            artBuscado.put("descripcion", txtAreaDesc.getText());
            artBuscado.put("precioMin", Long.parseLong(txtCostoMonedaExt.getText()));
            artBuscado.put("precioMay", Long.parseLong(txtCostoLocal.getText()));
            artBuscado.put("UsuMod", Identity.getNomFun());
            if (chkProdImportado.isSelected()) {
                artBuscado.put("importado", true);
            } else {
                artBuscado.put("importado", false);
            }
//            if (chkNoStockeable.isSelected()) {
//                artBuscado.put("stockeable", true);
//            } else {
//                artBuscado.put("stockeable", true);
//            }
            modificarArticulo(artBuscado);
        }
    }

    private void modificarArticulo(JSONObject art) {
        boolean exito = false;
//        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonDatos = art;
        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
        jsonDatos.put("fechaAlta", null);
        jsonDatos.put("fechaMod", null);
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/articulo");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("PUT");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(jsonDatos.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exito = (Boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        } catch (ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }

        Articulo arti = gson.fromJson(jsonDatos.toString(), Articulo.class);
        arti.setFechaAlta(timestamp);
        arti.setFechaMod(timestamp);

//        mensajeAlerta("-->> 1) " + txtCod01.getText() + " 2) " + txtCod02.getText() + " 3) " + txtCod03.getText());
        articuloProveedorDAO.eliminarTodos(arti.getIdArticulo());
        if (!txtCodProv01.getText().equals("")) {
            Proveedor p1 = proveedorDAO.listarPorRuc(txtCodProv01.getText());
            Articulo art1 = new Articulo();
            art1.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap1 = new ArticuloProveedor();
            ap1.setArticulo(art1);
            ap1.setProveedor(p1);
            ap1.setFechaAlta(timestamp);
            ap1.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap1);
        }
        if (!txtCodProv02.getText().equals("")) {
            Proveedor p2 = proveedorDAO.listarPorRuc(txtCodProv02.getText());
            Articulo art2 = new Articulo();
            art2.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap2 = new ArticuloProveedor();
            ap2.setArticulo(art2);
            ap2.setProveedor(p2);
            ap2.setFechaAlta(timestamp);
            ap2.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap2);
        }
        if (!txtCodProv03.getText().equals("")) {
            Proveedor p3 = proveedorDAO.listarPorRuc(txtCodProv03.getText());
            Articulo art3 = new Articulo();
            art3.setIdArticulo(arti.getIdArticulo());

            ArticuloProveedor ap3 = new ArticuloProveedor();
            ap3.setArticulo(art3);
            ap3.setProveedor(p3);
            ap3.setFechaAlta(timestamp);
            ap3.setFechaMod(timestamp);

            articuloProveedorDAO.insertar(ap3);
        }
//        id_iva falta
//        jsonDatos.put("fechaAlta", timestampJSON);
//        jsonDatos.put("fechaMod", timestampJSON);
        try {
            arti.setCosto(Long.parseLong(txtCostoLocal.getText()));
            Iva iva = new Iva();
            if (Long.parseLong(txtIva.getText()) == 0) {
                iva.setIdIva(1l);
                arti.setIva(iva);
            } else {
                if (Long.parseLong(txtIva.getText()) == 5) {
                    iva.setIdIva(2l);
                    arti.setIva(iva);
                } else {
                    iva.setIdIva(3l);
                    arti.setIva(iva);
                }
            }

            artDAO.actualizar(arti);
            exito = true;
        } catch (Exception e) {
            exito = false;
        }

        if (exito) {
            artBuscado = new JSONObject();
            mensajeAlerta("DATOS ACTUALIZADOS EXITOSAMENTE");
            actualizarNiveles(Long.parseLong(jsonDatos.get("idArticulo").toString()));
//            limpiar();
//            desHabilitarBotones();
//            btnNuevo.setDisable(false);
//            btnDescartar.setDisable(false);
//            btnBuscar.setDisable(false);
        } else {
            mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS, VERIFIQUELOS");
        }
//        boolean exito = false;
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject jsonDatos = art;
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        jsonDatos.put("fechaAlta", timestampJSON);
//        jsonDatos.put("fechaMod", timestampJSON);
//        try {
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/articulo");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json");
//                conn.setRequestProperty("Accept", "application/json");
//                conn.setRequestMethod("PUT");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//                wr.write(jsonDatos.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exito = (Boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                }
//            }
//        } catch (IOException e) {
//            System.out.println("-> " + e.getLocalizedMessage());
//        } catch (ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }
//        if (exito) {
//            artBuscado = new JSONObject();
//            mensajeAlerta("DATOS ACTUALIZADOS EXITOSAMENTE");
//            actualizarNiveles(Long.parseLong(jsonDatos.get("idArticulo").toString()));
//        } else {
//            mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS, VERIFIQUELOS");
//        }
    }

    private void actualizarNiveles(long idArt) {
        JSONObject objInsert = new JSONObject();
        ArticuloDTO artDTO = new ArticuloDTO();
        String nivel = "";
        try {
            if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel01)) {
                    eliminarNiveles(1, idArt);

                    ArticuloNf1TipoDTO aNF1DTO = new ArticuloNf1TipoDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF1DTO.setArticulo(artDTO);

                    Nf1TipoDTO n1 = new Nf1TipoDTO();
                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
                    n1.setIdNf1Tipo(Long.parseLong(json.get("idNf1Tipo").toString()));
                    aNF1DTO.setNf1Tipo(n1);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF1DTO));
                    nivel = "articuloNf1Tipo";

                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(1, idArt);
            }
            if (cbSubFamilia.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel02)) {
                    eliminarNiveles(2, idArt);

                    ArticuloNf2SfamiliaDTO aNF2DTO = new ArticuloNf2SfamiliaDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF2DTO.setArticulo(artDTO);

                    Nf2SfamiliaDTO n2 = new Nf2SfamiliaDTO();
                    JSONObject jsonSubFlia = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
                    n2.setIdNf2Sfamilia(Long.parseLong(jsonSubFlia.get("idNf2Sfamilia").toString()));
                    aNF2DTO.setNf2Sfamilia(n2);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF2DTO));
                    nivel = "articuloNf2Sfamilia";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(2, idArt);
            }
            if (cbCategoria.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel03)) {
                    eliminarNiveles(3, idArt);

                    ArticuloNf3SseccionDTO aNF3DTO = new ArticuloNf3SseccionDTO();
                    artDTO.setIdArticulo(idArt);
                    aNF3DTO.setArticulo(artDTO);

                    Nf3SseccionDTO n3 = new Nf3SseccionDTO();
                    JSONObject jsonCategoria = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
                    n3.setIdNf3Sseccion(Long.parseLong(jsonCategoria.get("idNf3Sseccion").toString()));
                    aNF3DTO.setNf3Sseccion(n3);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF3DTO));
                    nivel = "articuloNf3Sseccion";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }

                }
            } else {
                eliminarNiveles(3, idArt);
            }
            if (cbSubCategoria.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel04)) {
                    eliminarNiveles(4, idArt);

                    ArticuloNf4Seccion1DTO aNF4DTO = new ArticuloNf4Seccion1DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF4DTO.setArticulo(artDTO);

                    Nf4Seccion1DTO n4 = new Nf4Seccion1DTO();
                    JSONObject jsonSubCategoria = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
                    n4.setIdNf4Seccion1(Long.parseLong(jsonSubCategoria.get("idNf4Seccion1").toString()));
                    aNF4DTO.setNf4Seccion1(n4);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF4DTO));
                    nivel = "articuloNf4Seccion1";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(4, idArt);
            }
            if (cbClase.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel05)) {
                    eliminarNiveles(5, idArt);

                    ArticuloNf5Seccion2DTO aNF5DTO = new ArticuloNf5Seccion2DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF5DTO.setArticulo(artDTO);

                    Nf5Seccion2DTO n5 = new Nf5Seccion2DTO();
                    JSONObject jsonClase = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
                    n5.setIdNf5Seccion2(Long.parseLong(jsonClase.get("idNf5Seccion2").toString()));
                    aNF5DTO.setNf5Seccion2(n5);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF5DTO));
                    nivel = "articuloNf5Seccion2";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(5, idArt);
            }
            if (cbSubClase.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSubClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel06)) {
                    eliminarNiveles(6, idArt);

                    ArticuloNf6Secnom6DTO aNF6DTO = new ArticuloNf6Secnom6DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF6DTO.setArticulo(artDTO);

                    Nf6Secnom6DTO n6 = new Nf6Secnom6DTO();
                    JSONObject jsonSubClase = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
                    n6.setIdNf6Secnom6(Long.parseLong(jsonSubClase.get("idNf6Secnom6").toString()));
                    aNF6DTO.setNf6Secnom6(n6);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF6DTO));
                    nivel = "articuloNf6Secnom6";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(6, idArt);
            }
            if (cbSeccion.getSelectionModel().getSelectedIndex() > 0) {
                if (!cbSeccion.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel07)) {
                    eliminarNiveles(7, idArt);

                    ArticuloNf7Secnom7DTO aNF7DTO = new ArticuloNf7Secnom7DTO();
                    artDTO.setIdArticulo(idArt);
                    aNF7DTO.setArticulo(artDTO);

                    Nf7Secnom7DTO n7 = new Nf7Secnom7DTO();
                    JSONObject jsonSeccion = hashSeccion.get(cbSeccion.getSelectionModel().getSelectedItem());
                    n7.setIdNf7Secnom7(Long.parseLong(jsonSeccion.get("idNf7Secnom7").toString()));
                    aNF7DTO.setNf7Secnom7(n7);

                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF7DTO));
                    nivel = "articuloNf7Secnom7";
                    if (!objInsert.toString().equals("{}")) {
                        registrandoNiveles(objInsert, nivel);
                    }
                }
            } else {
                eliminarNiveles(7, idArt);
            }
        } catch (ParseException ex) {
            Logger.getLogger(ArticulosFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
//        JSONObject objInsert = new JSONObject();
//        ArticuloDTO artDTO = new ArticuloDTO();
//        String nivel = "";
//        try {
//            if (cbFamilia.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel01)) {
//                    eliminarNiveles(1, idArt);
//
//                    ArticuloNf1TipoDTO aNF1DTO = new ArticuloNf1TipoDTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF1DTO.setArticulo(artDTO);
//
//                    Nf1TipoDTO n1 = new Nf1TipoDTO();
//                    JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                    n1.setIdNf1Tipo(Long.parseLong(json.get("idNf1Tipo").toString()));
//                    aNF1DTO.setNf1Tipo(n1);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF1DTO));
//                    nivel = "articuloNf1Tipo";
//
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(1, idArt);
//            }
//            if (cbSubFamilia.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbSubFamilia.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel02)) {
//                    eliminarNiveles(2, idArt);
//
//                    ArticuloNf2SfamiliaDTO aNF2DTO = new ArticuloNf2SfamiliaDTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF2DTO.setArticulo(artDTO);
//
//                    Nf2SfamiliaDTO n2 = new Nf2SfamiliaDTO();
//                    JSONObject jsonSubFlia = hashSubFamilia.get(cbSubFamilia.getSelectionModel().getSelectedItem());
//                    n2.setIdNf2Sfamilia(Long.parseLong(jsonSubFlia.get("idNf2Sfamilia").toString()));
//                    aNF2DTO.setNf2Sfamilia(n2);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF2DTO));
//                    nivel = "articuloNf2Sfamilia";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(2, idArt);
//            }
//            if (cbCategoria.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel03)) {
//                    eliminarNiveles(3, idArt);
//
//                    ArticuloNf3SseccionDTO aNF3DTO = new ArticuloNf3SseccionDTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF3DTO.setArticulo(artDTO);
//
//                    Nf3SseccionDTO n3 = new Nf3SseccionDTO();
//                    JSONObject jsonCategoria = hashCategoria.get(cbCategoria.getSelectionModel().getSelectedItem());
//                    n3.setIdNf3Sseccion(Long.parseLong(jsonCategoria.get("idNf3Sseccion").toString()));
//                    aNF3DTO.setNf3Sseccion(n3);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF3DTO));
//                    nivel = "articuloNf3Sseccion";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//
//                }
//            } else {
//                eliminarNiveles(3, idArt);
//            }
//            if (cbSubCategoria.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbSubCategoria.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel04)) {
//                    eliminarNiveles(4, idArt);
//
//                    ArticuloNf4Seccion1DTO aNF4DTO = new ArticuloNf4Seccion1DTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF4DTO.setArticulo(artDTO);
//
//                    Nf4Seccion1DTO n4 = new Nf4Seccion1DTO();
//                    JSONObject jsonSubCategoria = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
//                    n4.setIdNf4Seccion1(Long.parseLong(jsonSubCategoria.get("idNf4Seccion1").toString()));
//                    aNF4DTO.setNf4Seccion1(n4);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF4DTO));
//                    nivel = "articuloNf4Seccion1";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(4, idArt);
//            }
//            if (cbClase.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel05)) {
//                    eliminarNiveles(5, idArt);
//
//                    ArticuloNf5Seccion2DTO aNF5DTO = new ArticuloNf5Seccion2DTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF5DTO.setArticulo(artDTO);
//
//                    Nf5Seccion2DTO n5 = new Nf5Seccion2DTO();
//                    JSONObject jsonClase = hashClase.get(cbClase.getSelectionModel().getSelectedItem());
//                    n5.setIdNf5Seccion2(Long.parseLong(jsonClase.get("idNf5Seccion2").toString()));
//                    aNF5DTO.setNf5Seccion2(n5);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF5DTO));
//                    nivel = "articuloNf5Seccion2";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(5, idArt);
//            }
//            if (cbSubClase.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbSubClase.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel06)) {
//                    eliminarNiveles(6, idArt);
//
//                    ArticuloNf6Secnom6DTO aNF6DTO = new ArticuloNf6Secnom6DTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF6DTO.setArticulo(artDTO);
//
//                    Nf6Secnom6DTO n6 = new Nf6Secnom6DTO();
//                    JSONObject jsonSubClase = hashSubClase.get(cbSubClase.getSelectionModel().getSelectedItem());
//                    n6.setIdNf6Secnom6(Long.parseLong(jsonSubClase.get("idNf6Secnom6").toString()));
//                    aNF6DTO.setNf6Secnom6(n6);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF6DTO));
//                    nivel = "articuloNf6Secnom6";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(6, idArt);
//            }
//            if (cbSeccion.getSelectionModel().getSelectedIndex() > 0) {
//                if (!cbSeccion.getSelectionModel().getSelectedItem().equalsIgnoreCase(nivel07)) {
//                    eliminarNiveles(7, idArt);
//
//                    ArticuloNf7Secnom7DTO aNF7DTO = new ArticuloNf7Secnom7DTO();
//                    artDTO.setIdArticulo(idArt);
//                    aNF7DTO.setArticulo(artDTO);
//
//                    Nf7Secnom7DTO n7 = new Nf7Secnom7DTO();
//                    JSONObject jsonSeccion = hashSeccion.get(cbSeccion.getSelectionModel().getSelectedItem());
//                    n7.setIdNf7Secnom7(Long.parseLong(jsonSeccion.get("idNf7Secnom7").toString()));
//                    aNF7DTO.setNf7Secnom7(n7);
//
//                    objInsert = (JSONObject) parser.parse(gson.toJson(aNF7DTO));
//                    nivel = "articuloNf7Secnom7";
//                    if (!objInsert.toString().equals("{}")) {
//                        registrandoNiveles(objInsert, nivel);
//                    }
//                }
//            } else {
//                eliminarNiveles(7, idArt);
//            }
//        } catch (ParseException ex) {
//            Logger.getLogger(VerificarPrecioFXMLController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
    }

    private void eliminarNiveles(int numNivel, long idArt) {
        String nivel = "";
        switch (numNivel) {
            case 1:
                try {
                    nivel = "articuloNf1Tipo";
                    ArticuloNf1Tipo artNF1 = anf1TipoDAO.listarArticulo(idArt);
                    anf1TipoDAO.eliminar(artNF1.getIdNf1TipoArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 2:
                try {
                    nivel = "articuloNf2Sfamilia";
                    ArticuloNf2Sfamilia artNF2 = anf2SfamiliaDAO.listarArticulo(idArt);
                    anf2SfamiliaDAO.eliminar(artNF2.getIdNf2SfamiliaArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 3:
                try {
                    nivel = "articuloNf3Sseccion";
                    ArticuloNf3Sseccion artNF3 = anf3SeccionDAO.listarArticulo(idArt);
                    anf3SeccionDAO.eliminar(artNF3.getIdNf3SseccionArticulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 4:
                try {
                    nivel = "articuloNf4Seccion1";
                    ArticuloNf4Seccion1 artNF4 = anf4SeccionDAO.listarArticulo(idArt);
                    anf4SeccionDAO.eliminar(artNF4.getIdNf4Seccion1Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 5:
                try {
                    nivel = "articuloNf5Seccion2";
                    ArticuloNf5Seccion2 artNF5 = anf5SeccionDAO.listarArticulo(idArt);
                    anf5SeccionDAO.eliminar(artNF5.getIdNf5Seccion2Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 6:
                try {
                    nivel = "articuloNf6Secnom6";
                    ArticuloNf6Secnom6 artNF6 = anf6SeccionDAO.listarArticulo(idArt);
                    anf6SeccionDAO.eliminar(artNF6.getIdNf6Secnom6Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            case 7:
                try {
                    nivel = "articuloNf7Secnom7";
                    ArticuloNf7Secnom7 artNF7 = anf7SeccionDAO.listarArticulo(idArt);
                    anf7SeccionDAO.eliminar(artNF7.getIdNf7Secnom7Articulo());
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                }
                break;
            default:
                break;
        }

//        String inputLine;
//        JSONParser parser = new JSONParser();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel + "/delete/" + idArt);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BuffbtnAeredReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                boolean dato = (Boolean) parser.parse(inputLine);
//                System.out.println("-->> " + dato);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
//        String nivel = "";
//        switch (numNivel) {
//            case 1:
//                nivel = "articuloNf1Tipo";
//                break;
//            case 2:
//                nivel = "articuloNf2Sfamilia";
//                break;
//            case 3:
//                nivel = "articuloNf3Sseccion";
//                break;
//            case 4:
//                nivel = "articuloNf4Seccion1";
//                break;
//            case 5:
//                nivel = "articuloNf5Seccion2";
//                break;
//            case 6:
//                nivel = "articuloNf6Secnom6";
//                break;
//            case 7:
//                nivel = "articuloNf7Secnom7";
//                break;
//            default:
//                break;
//        }
//
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/" + nivel + "/delete/" + idArt);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                boolean dato = (Boolean) parser.parse(inputLine);
//                System.out.println("-->> " + dato);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
    }

    @FXML
    private void txtPrecioMin1KeyPress(KeyEvent event) {
        listenPrecioMin(event);
    }

    private void listenPrecioMin(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPrecioMin1.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularPorcentaje(newValue);
                });
            });
        }
//        else if (event.getCode().isLetterKey()) {
//            txtMontoSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!convertirEntero(txtMontoSencillo)) {
//                        txtMontoSencillo.setText("Gs 0");
//                    }
//                });
//            });
//        }
    }

    private void listenPrecioMay(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPrecioMayNuevo1.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularPorcentajeMay(newValue);
                });
            });
        }
//        else if (event.getCode().isLetterKey()) {
//            txtMontoSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!convertirEntero(txtMontoSencillo)) {
//                        txtMontoSencillo.setText("Gs 0");
//                    }
//                });
//            });
//        }
    }

    private void listenPorcMin(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularMinorista(newValue, 1);
                });
            });
        }
    }

    private void listenPorcMin2(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPorcIncremParana011.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularMinorista(newValue, 2);
                });
            });
        }
    }

    private void listenPorcMay(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            PocIncremMayorista.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularMayorista(newValue);
                });
            });
        }
    }

    private void listenPorcMin3(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPorcIncremVarios.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularMinorista(newValue, 3);
                });
            });
        }
    }

    private void calcularPorcentaje(String value) {
        if (!txtCostoLocal.equals("")) {
            DecimalFormat df = new DecimalFormat("#.0");
            double result = (Long.parseLong(value) * 100);
            result = result / Double.parseDouble(txtCostoLocal.getText());
            double reult2 = result - 100;
            txtPorcIncremParana01.setText(df.format(reult2));
        } else if (!txtCostoFinalImportado.equals("")) {
            DecimalFormat df = new DecimalFormat("#.0");
            double result = (Long.parseLong(value) * 100);
            result = result / Double.parseDouble(txtCostoFinalImportado.getText());
            double reult2 = result - 100;
            txtPorcIncremParana01.setText(df.format(reult2));
        }
    }

    private void calcularPorcentajeMay(String value) {
        DecimalFormat df = new DecimalFormat("#.0");
        double result = (Long.parseLong(value) * 100);
        result = result / Double.parseDouble(txtCostoLocal.getText());
        double reult2 = result - 100;
        txtPorcIncremParana01.setText(df.format(reult2));
    }

    @FXML
    private void btnMarcaAction(ActionEvent event) {
    }

    @FXML
    private void btnProveedorEstandarAction(ActionEvent event) {
        nume = 1;
        anchorPaneBuscar.setVisible(true);
        panePrincipal.setDisable(true);
//        BuscarProveedorFXMLController.cargarRucRazon(txtCodProv01, txtProv01);
//        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, true);
    }

    @FXML
    private void btnProveedor1Action(ActionEvent event) {
        nume = 2;
//        BuscarProveedorFXMLController.cargarRucRazon(txtCodProv02, txtProv02);
//        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, true);
        anchorPaneBuscar.setVisible(true);
        panePrincipal.setDisable(true);
    }

    @FXML
    private void btnProveedor2Action(ActionEvent event) {
        nume = 3;
//        BuscarProveedorFXMLController.cargarRucRazon(txtCodProv03, txtProv03);
//        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, true);
        anchorPaneBuscar.setVisible(true);
        panePrincipal.setDisable(true);
    }

    private void cargarImagen() {
        Image image = null;
        if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
            image = jsonArtDetImg(FacturaCompraFXMLController.getCodArt());
        }
        if (image == null) {
            File file = new File(PATH.PATH_NO_IMG);
            image = new Image(file.toURI().toString());
            this.imgProducto.setImage(image);
        }
        this.imgProducto.setImage(image);
        centerImage();
        this.imgProducto = (ImageView) AnimationFX.fadeNode(this.imgProducto);
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    public void centerImage() {
        Image img = imgProducto.getImage();
        if (img != null) {
            double w = 0;
            double h = 0;
            double ratioX = imgProducto.getFitWidth() / img.getWidth();
            double ratioY = imgProducto.getFitHeight() / img.getHeight();
            double reducCoeff = 0;
            if (ratioX >= ratioY) {
                reducCoeff = ratioY;
            } else {
                reducCoeff = ratioX;
            }
            w = img.getWidth() * reducCoeff;
            h = img.getHeight() * reducCoeff;
            imgProducto.setX((imgProducto.getFitWidth() - w) / 2);
            imgProducto.setY((imgProducto.getFitHeight() - h) / 2);
        }
    }

    private void actualizandoTablaDeposito() {
        depositoData = FXCollections.observableArrayList(depositoList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                    String apellido = String.valueOf(jsonDepo.get("descripcion"));
                    if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                        apellido = "-";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());
                } catch (ParseException ex) {
                    Logger.getLogger(FacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnCantDepo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantDepo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                //                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                String apellido = String.valueOf(data.getValue().get("cantidad"));
                if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                    apellido = "-";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Ruc .................................................
        tableViewDeposito.setItems(depositoData);
//        if (!escucha) {
//            escucha();
//        }
    }

    @FXML
    private void txtDescMaxMayKeyPress(KeyEvent event) {
        calcularDescuento();
    }

    private void calcularDescuento() {
        try {
            int desc = 100 - Integer.parseInt(txtDescMaxMay.getText());
            float porc = (float) desc / 100;
            double pM = Long.parseLong(txtPrecioMin1.getText()) * porc;
            long precioMay = (long) pM;
            txtPrecioMayNuevo1.setText(precioMay + "");
        } catch (Exception e) {
        } finally {
        }
    }

    @FXML
    private void txtDescMaxMayKeyTyped(KeyEvent event) {
        calcularDescuento();
    }

    @FXML
    private void buttonAnhadirAction(ActionEvent event) {
        addProveedor();
    }

    private void escucha() {
        escucha = true;
        tableViewCliente.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewCliente.getSelectionModel().getSelectedItem() != null) {

                if (buttonAnhadir.isDisable()) {
                    buttonAnhadir.setDisable(false);
                }
                jsonSeleccionActual = newSelection;
                textFieldRucCiBuscar.setText(jsonSeleccionActual.get("ruc").toString());
                textFieldNombreBuscar.setText(jsonSeleccionActual.get("descripcion").toString());
                //editar cliente, por si acaso...
            } else {
                buttonAnhadir.setDisable(true);
                textFieldRucCiBuscar.setText("");
                textFieldNombreBuscar.setText("");
            }
        });
    }

    @FXML
    private void buttonBuscarClienteAction(ActionEvent event) {
        buscandoCliente(true);
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
    }

    private void buscandoCliente(boolean efeuno) {
        String nom, ape, ruc;
        if (textFieldNombreCliente.getText().contentEquals("")) {
            nom = "null";
        } else {
            nom = textFieldNombreCliente.getText();
        }
        if (textFieldRucCliente.getText().contentEquals("")) {
            ruc = "null";
        } else {
            ruc = textFieldRucCliente.getText();
        }

        if (nom.equals("null") && !ruc.equals("null")) {
            buscarClientePorRucCi();
            actualizandoTablaCliente();
        } else {
            clienteList = jsonArrayCliente(nom, ruc);
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
            actualizandoTablaCliente();
        }
        if (!tableViewCliente.getItems().isEmpty() && efeuno) {
            tableViewCliente.requestFocus();
            tableViewCliente.getSelectionModel().select(0);
            tableViewCliente.getFocusModel().focus(0);
        } else if (!tableViewCliente.getItems().isEmpty() && !efeuno) {
            tableViewCliente.getSelectionModel().select(0);
            tableViewCliente.getFocusModel().focus(0);
        }
    }

    private List<JSONObject> jsonArrayCliente(String nom, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ruc);
        return clienteJSONObjList;
    }

    private void actualizandoTablaCliente() {
        clienteData = FXCollections.observableArrayList(clienteList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String apellido = String.valueOf(data.getValue().get("descripcion"));
                if (apellido.contentEquals("null")) {
                    apellido = "";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnRucCi.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnRucCi.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("ruc"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        //columna Ruc .................................................
        tableViewCliente.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }

    public List<JSONObject> generarListaCliente(String nom, String ruc) {
        JSONParser parser = new JSONParser();
        List<Proveedor> cliente = proveedorDAO.listarPorNomRuc(nom, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Proveedor cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }

    private void buscarClientePorRucCi() {
        String rucCliente = "";
        JSONParser parser = new JSONParser();
        try {
            rucCliente = textFieldRucCliente.getText();
            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
            int contador = rucCliente.split("-", -1).length - 1;
            Proveedor cliente = new Proveedor();
            if (contador >= 1) {
                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
                clienteList = new ArrayList<>();
//                cliente = cliDAO.listarPorRuc(rucCliente);
                cliente = proveedorDAO.listarPorRuc(rucCliente);
                if (cliente.getDescripcion() != null) {
//                    cliente.setFecNac(null);
                    cliente.setFechaAlta(null);
                    cliente.setFechaMod(null);
                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
                    clienteList.add(jsonCli);
                }
            } else if (contador == 0) {
                //Listar por CI en el caso que exista
                cliente = proveedorDAO.listarPorRuc(rucCliente);
                if (cliente.getIdProveedor() == null) {
                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
                    List<Proveedor> listClien = proveedorDAO.listarPorCIRevancha(rucCliente);
                    if (listClien != null) {
                        if (listClien.isEmpty()) {
                            clienteList = new ArrayList<>();
                        } else {
                            clienteList = new ArrayList<>();
                            for (int i = 0; i < listClien.size(); i++) {
                                Proveedor cli = listClien.get(i);
//                                cli.setFecNac(null);
                                cli.setFechaAlta(null);
                                cli.setFechaMod(null);
                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                                clienteList.add(jsonCli);
                            }
                        }
                    } else {
                        clienteList = new ArrayList<>();
                    }
                } else {
                    clienteList = new ArrayList<>();
                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
                    clienteList.add(jsonCli);
                }
            }
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
        } catch (Exception e) {
            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
        }
    }

    private void addProveedor() {
        if (!buttonAnhadir.isDisable()) {
//                try {

            seteandoParam(jsonSeleccionActual);
            //            txtCodProv01, txtProv01
            if (nume == 1) {
                txtCodProv01.setText(jsonSeleccionActual.get("ruc").toString());
                txtProv01.setText(jsonSeleccionActual.get("descripcion").toString());
            } else if (nume == 2) {
                txtCodProv02.setText(jsonSeleccionActual.get("ruc").toString());
                txtProv02.setText(jsonSeleccionActual.get("descripcion").toString());
            } else if (nume == 3) {
                txtCodProv03.setText(jsonSeleccionActual.get("ruc").toString());
                txtProv03.setText(jsonSeleccionActual.get("descripcion").toString());
            }

//            if (ScreensContoller.getFxml().contentEquals("/vista/stock/FacturaCompraFXML.fxml")) {
//                txtTimbrado.setText(jsonSeleccionActual.get("timbrado").toString());
//            }
//                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idCliente", idCliente);
//                    cab.put("cliente", cliente);
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
            anchorPaneBuscar.setVisible(false);
            panePrincipal.setDisable(false);
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//                }
        }
    }

    @FXML
    private void txtPorcIncremParana01KeyPressed(KeyEvent event) {
        listenPorcMin(event);
    }

    private void calcularMinorista(String newValue, int ord) {
        double incre2 = 0;
        double incre3 = 0;
        double incre1 = 0;
        if (ord == 2) {
            incre2 = Double.parseDouble(newValue);
        } else if (!txtPorcIncremParana011.getText().equals("")) {
            incre2 = Double.parseDouble(txtPorcIncremParana011.getText());
        }
        if (ord == 3) {
            incre3 = Double.parseDouble(newValue);
        } else if (!txtPorcIncremVarios.getText().equals("")) {
            incre3 = Double.parseDouble(txtPorcIncremVarios.getText());
        }
        if (ord == 1) {
            incre1 = Double.parseDouble(newValue);
        } else if (!txtPorcIncremParana01.getText().equals("")) {
            incre1 = Double.parseDouble(txtPorcIncremParana01.getText());
        }
        incre1 = incre1 + incre2 + incre3; //        DecimalFormat df = new DecimalFormat("#.0");
        if (!txtCostoLocal.getText().equals("")) {
            double result = incre1 * Double.parseDouble(txtCostoLocal.getText());
            result = result / 100;
            long reult2 = Math.round(result) + Long.parseLong(txtCostoLocal.getText());
            txtPrecioMin1.setText(reult2 + "");
        } else {
            double result = incre1 * Double.parseDouble(txtCostoFinalImportado.getText());
            result = result / 100;
            long reult2 = Math.round(result) + Long.parseLong(txtCostoFinalImportado.getText());
            txtPrecioMin1.setText(reult2 + "");
        }

    }

    private void calcularMayorista(String newValue) {
//        DecimalFormat df = new DecimalFormat("#.0");
        if (!txtCostoLocal.getText().equals("")) {
            double result = Double.parseDouble(newValue) * Double.parseDouble(txtCostoLocal.getText());
            result = result / 100;
            long reult2 = Math.round(result) + Long.parseLong(txtCostoLocal.getText());
            txtPrecioMayNuevo1.setText(reult2 + "");
        } else {
            double result = Double.parseDouble(newValue) * Double.parseDouble(txtCostoFinalImportado.getText());
            result = result / 100;
            long reult2 = Math.round(result) + Long.parseLong(txtCostoFinalImportado.getText());
            txtPrecioMayNuevo1.setText(reult2 + "");
        }

    }

    @FXML
    private void txtCostoOrigenKeyPressed(KeyEvent event) {
        keyPress(event);
    }

    private void calcularCostoOrigen() {
        double calculo = Double.parseDouble(txtCostoMonExt.getText()) * Double.parseDouble(txtCotizacion.getText());
        txtCostoOrigen.setText(Math.round(calculo) + "");
    }

    private void calcularCostoFinalImportado() {
        long descImportado = 0;
        long increImportado = 0;
        if (!txtPorcentajeDesc.getText().equals("")) {
            descImportado = Long.parseLong(txtPorcentajeDesc.getText());
        }
        if (!txtPorcAduanero.getText().equals("")) {
            increImportado = Long.parseLong(txtPorcAduanero.getText());
        }

        float numDesc = (float) (100 - descImportado) / 100;
        float descu = (float) Long.parseLong(txtCostoOrigen.getText()) * numDesc;
        descImportado = Math.round(descu);

        float numIncre = (float) (100 + increImportado) / 100;
        float incre = (float) descImportado * numIncre;
        increImportado = Math.round(incre);
        chkArtOculto.setSelected(true);
//        costoFinal = descImportado + increImportado;
        txtCostoFinalImportado.setText(increImportado + "");
    }

    @FXML
    private void txtCostoFinalImportadoKeyPressed(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void txtPorcIncremVariosKeyPressed(KeyEvent event) {
        listenPorcMin3(event);
    }

    @FXML
    private void txtPorcIncremParana011KeyPressed(KeyEvent event) {
        listenPorcMin2(event);
    }

    @FXML
    private void PocIncremMayoristaKeyPressed(KeyEvent event) {
        listenPorcMay(event);
    }

    @FXML
    private void txtPrecioMayNuevo1KeyPressed(KeyEvent event) {
        listenPrecioMay(event);
    }
}
