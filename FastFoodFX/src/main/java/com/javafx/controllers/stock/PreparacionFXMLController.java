package com.javafx.controllers.stock;

import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.NumberValidator;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.DetalleGasto;
import com.peluqueria.core.domain.Gastos;
import com.peluqueria.core.domain.IpCaja;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.PreparacionCabecera;
import com.peluqueria.core.domain.PreparacionDetalle;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.Seccion;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.BarrioDAO;
import com.peluqueria.dao.CiudadDAO;
import com.peluqueria.dao.DepartamentoDAO;
import com.peluqueria.dao.DetalleGastoDAO;
import com.peluqueria.dao.GastosDAO;
import com.peluqueria.dao.IpCajaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PaisDAO;
import com.peluqueria.dao.PreparacionCabeceraDAO;
import com.peluqueria.dao.PreparacionDetalleDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.SeccionDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.BarrioDTO;
import com.peluqueria.dto.CiudadDTO;
import com.peluqueria.dto.DepartamentoDTO;
import com.peluqueria.dto.PaisDTO;
import java.io.ByteArrayInputStream;
import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
public class PreparacionFXMLController extends BaseScreenController implements Initializable {

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    @Autowired
    ArticuloDAO artDAO;
    @Autowired
    PreparacionCabeceraDAO pcDAO;
    @Autowired
    PreparacionDetalleDAO pdDAO;

    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> proveedorList;
    private ObservableList<JSONObject> detalleData;
    private List<JSONObject> detalleList;
    private ObservableList<JSONObject> preparacionData;
    private List<JSONObject> preparacionList;
    private boolean escucha;
    LocalDate now = LocalDate.now(); //2015-11-23
    LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
    LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30
    final KeyCombination altN = new KeyCodeCombination(KeyCode.N, KeyCombination.ALT_DOWN);

    private Date date = new Date();

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    SeccionDAO seccionDAO;
    @Autowired
    DetalleGastoDAO detalleGastoDAO;
    @Autowired
    ProveedorDAO proveedorDAO;
    @Autowired
    GastosDAO gastosDAO;
    @Autowired
    IpCajaDAO ipCajaDAO;
    @Autowired
    PaisDAO paisDAO;
    @Autowired
    CiudadDAO ciudadDAO;
    @Autowired
    BarrioDAO barrioDAO;
    @Autowired
    DepartamentoDAO dptoDAO;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private Button buttonCerrar;
    private TableView<JSONObject> tableViewProveedor;
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    private RadioButton radioBtnNombreCta;
    private RadioButton radioBtnCodigoCta;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnGuardar;
    @FXML
    private TextField txtCodigo;
    private TextField txtNombre;
    @FXML
    private TextField txtVtoDia;
    private ComboBox<String> cbFamilia;
    private HashMap<String, JSONObject> hashFamilia;
    private ComboBox<String> cbPais;
    private HashMap<String, JSONObject> hashPais;
    private ComboBox<String> cbDpto;
    private HashMap<String, JSONObject> hashDpto;
    private ComboBox<String> cbCiudad;
    private HashMap<String, JSONObject> hashCiudad;
    private ComboBox<String> cbBarrio;
    private HashMap<String, JSONObject> hashBarrio;
    private TextField txtRuc;
    @FXML
    private Button btnActualizar;
    @FXML
    private Button btnCancelar;
    private TextField txtIdProveedor;
    private ComboBox<String> cbPersona;
    @FXML
    private Pane secondPane;
    private TableColumn<JSONObject, String> tableColumnFactura;
    private TableColumn<JSONObject, String> tableColumnEmpresa;
    @FXML
    private TableColumn<JSONObject, String> tableColumnFecha;
    private TableColumn<JSONObject, String> tableColumnMonto;
    private TextField txtNumFactura;
    private TextField txtEmpresa;
    private TextField txtObservacion;
//    private ComboBox<String> cbTipo;
    @FXML
    private DatePicker dpFecha;
    @FXML
    private TextField txtMonto;
    public static Toaster toaster;
    private List<JSONObject> articuloModalList;
    private HashMap<String, Image> mapeoArticulo;
    private ObservableList<JSONObject> articuloModalData;
    private List<JSONObject> gastoModalList;
    private ObservableList<JSONObject> gastoModalData;

    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    @FXML
    private Label labelTotal;
    @FXML
    private Label labelTotalGs;
    @FXML
    private Label lblTipo;
    private TableColumn<JSONObject, String> tableColumnTipo;
    private Tab tabDetalleGasto;
    private TableColumn<JSONObject, String> tableColumnCodigoDetalle;
    private TableColumn<JSONObject, String> tableColumnDescripcionDetalle;
    private TableColumn<JSONObject, String> tableColumnCantidadDetalle;
    private TableColumn<JSONObject, String> tableColumnMontoDetalle;
    private Label labelTotalGsDetalle;
//    private TextField txtCodProductoDetalle;
    private TextField txtDescripcionDetalle;
    private TextField txtPrecioDetalle;
    private TextField txtCantidadDetalle;
    private Pane panelDetalleGastos;
    private Pane panelPrecioVenta;
//    private TextField txtPrecioMin;
//    private TextField txtPorcIncremParana01;
//    private ComboBox<String> cbSeccionData1;
    private TableView<JSONObject> tableViewDetalle;
    @FXML
    private TextField txtIdGasto;
    private Button btnProveedorEstandar;
    @FXML
    private TextField txtRucProveedor;
    @FXML
    private TextField txtProducto;
    @FXML
    private TextField txtCantidad;
    @FXML
    private Button btnBuscarProducto;
    @FXML
    private TextField txtDescriGasto;
    @FXML
    private Button btnBuscarCompra;
    @FXML
    private Button btnAgregar;
    @FXML
    private TextField txtIdProducto;
    @FXML
    private TextField txtIdDetalleGasto;
    @FXML
    private TableView<JSONObject> tableViewPreparacion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidad;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCosto;
    @FXML
    private AnchorPane anchorPaneBuscarProducto;
    @FXML
    private Label labelClienteBuscar;
    @FXML
    private TableView<JSONObject> tableViewProducto;
    @FXML
    private TableColumn<JSONObject, ImageView> tableColumnArticuloModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigoModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcionModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnSeccioModal;
    @FXML
    private TableColumn<JSONObject, String> tableColumnPrecioModal;
    @FXML
    private Label labelClienteBuscar1;
    @FXML
    private TextField txtCantidadModal;
    @FXML
    private Button buttonAnhadirProducto;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private TextField txtBuscarCodProducto;
    @FXML
    private Label lableApellido;
    @FXML
    private TextField txtBuscarDesriProducto;
    @FXML
    private ComboBox<String> cbSeccionData1;
    @FXML
    private Button btnBuscarProducto1;
    @FXML
    private TableView<JSONObject> tableViewGasto;
    @FXML
    private TableColumn<JSONObject, String> columnProveedorGasto;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcionGasto;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadGasto;
    @FXML
    private TableColumn<JSONObject, String> columnCostoGasto;
    @FXML
    private Label labelClienteBuscar11;
    @FXML
    private TextField txtCantidadModal1;
    @FXML
    private Button buttonAnhadirGasto;
    @FXML
    private Label lableApellido1;
    @FXML
    private ComboBox<String> cbProveedorGasto;
    @FXML
    private Button btnBuscarGasto;
    @FXML
    private AnchorPane anchorPaneBuscarGasto;
    @FXML
    private Pane panel03;
    @FXML
    private Pane panel01;
    @FXML
    private Pane panel02;
    @FXML
    private Pane panel04;
    @FXML
    private Button btnActualizar1;
    @FXML
    private TextField txtCantidadPreparado;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {

        toaster = new Toaster();
        articuloModalList = new ArrayList<>();
        gastoModalList = new ArrayList<>();
        preparacionList = new ArrayList<>();

        mapeoArticulo = new HashMap();
        escucha = false;
        hashPais = new HashMap<>();
        hashCiudad = new HashMap<>();
        hashBarrio = new HashMap<>();
        hashDpto = new HashMap<>();
        hashFamilia = new HashMap<>();

        dpFecha.setValue(LocalDate.now());
//        cargarFamilia();
//        cargarSecciones();
        cargarProveedores();
//        cargarPais();
//        listenerCampos();
//        listenerCamposCombos();
//        repeatFocus(txtBuscar);
//        listenerCamposCombos();

//        cbPais.setValue("PARAGUAY");
//        cbPais.setDisable(true);
        ubicandoContenedorSecundario();
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField();
        listenTextFieldPrecioDetalle();
        listenTextFieldSoloNumero();
//        listarporFecha();
        cargarSecciones();
        Image imgBuscar = new Image(getClass().getResourceAsStream("/vista/img/buscar.png"));
        btnBuscarProducto.setGraphic(new ImageView(imgBuscar));
        btnBuscarCompra.setGraphic(new ImageView(imgBuscar));

//        btnBuscarProducto.setDisable(true);
//        btnBuscarCompra.setDisable(true);

//        limpiarNiveles(1);
//        JSONObject json = hashPais.get(cbPais.getSelectionModel().getSelectedItem());
//        cargarDepartamento(Long.parseLong(json.get("idPais").toString()));
    }

    public void cargarSecciones() {
        cbSeccionData1.getItems().add("SELECCIONE SECCION");
        List<Seccion> listSeccion = seccionDAO.listarTodosTRUE();
        if (listSeccion != null) {
            for (Seccion obj : listSeccion) {
                //                    json = (JSONObject) parser.parse(obj.toString());listSeccion
                cbSeccionData1.getItems().add(obj.getDescripcion().toUpperCase());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
            }
        }
    }

    public void cargarProveedores() {
        cbProveedorGasto.getItems().add("SELECCIONE PROVEEDOR");
        List<String> listSeccion = gastosDAO.listarProveedorDistinct();
        if (listSeccion != null) {
            for (String obj : listSeccion) {
                //                    json = (JSONObject) parser.parse(obj.toString());listSeccion
                cbProveedorGasto.getItems().add(obj.toUpperCase());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
            }
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void listenTextField() {
        txtMonto.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMonto.setText(param);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText("");
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMonto.setText(param);
                                    txtMonto.positionCaret(txtMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMonto.setText("");
                                txtMonto.positionCaret(txtMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText(oldValue);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMonto.setText(param);
                        txtMonto.positionCaret(txtMonto.getLength());
                    });
                }
            }
        });
    }

    private void listenTextFieldPrecioDetalle() {
        txtMonto.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMonto.setText(param);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText("");
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMonto.setText(param);
                                    txtMonto.positionCaret(txtMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMonto.setText("");
                                txtMonto.positionCaret(txtMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText(oldValue);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMonto.setText(param);
                        txtMonto.positionCaret(txtMonto.getLength());
                    });
                }
            }
        });
    }

    private void listenTextFieldSoloNumero() {
        txtCantidad.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtCantidad.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {

                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        Platform.runLater(() -> {
                            txtCantidad.setText("");
                            txtCantidad.positionCaret(txtCantidad.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtCantidad.setText(oldValue);
                            txtCantidad.positionCaret(txtCantidad.getLength());
                        });
                    }
                } else if (newV.substring((newV.length() - 1), newV.length()).equalsIgnoreCase(",")) {
                    Platform.runLater(() -> {
//                        txtCantidad.setText(param);
                        txtCantidad.getText().replace(",", ".");
                        txtCantidad.positionCaret(txtCantidad.getLength());
                    });
                }
            }
        });
    }

    private void listenerCampos() {
        cbFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                System.out.println("--->> " + cbFamilia.getSelectionModel().getSelectedItem());
//                cbSubFamilia.getItems().clear();
//                JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
            }
        });
    }

    @FXML
    private void buttonCerrarAction(ActionEvent event) {
        cerrandoSesion();
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
        limpiar();
//        btnBuscarProducto.setDisable(false);
//        btnBuscarCompra.setDisable(false);
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
//        if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
//            mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
//        } else {
        if (txtProducto.getText().equals("") || txtIdProducto.getText().equals("") || preparacionList.size() == 0) {
            mensajeError("SE DEBE SELECCIONAR UN PRODUCTO COMO PREPARACION Y DEBE TENER DETALLE PARA REGISTRARLO");
        } else {
            panel01.setDisable(true);
            panel02.setDisable(true);
            panel03.setDisable(true);
            panel04.setVisible(true);
        }
//        guardarProveedor();
//        }
    }

    private void cerrandoSesion() {
        if (preparacionList.size() > 0) {

            ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿SEGURO QUE DESEA SALIR?", ok, cancel);
            alert.showAndWait();
            if (alert.getResult() == ok) {
//                btnBuscarProducto.setDisable(true);
//                btnBuscarCompra.setDisable(true);
                limpiar();
//                btnGuardar.setDisable(true);
                btnActualizar.setDisable(false);
                alert.close();
                this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//                    mensajeAlerta("DATO ELIMINADO CORRECTAMENTE!!");
            } else {
                alert.close();
            }
        } else {
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        }
    }

    private void configuracionCuponera() {
//        if ("configuracion_descuento")) {//falta aún...
        Utilidades.setIdRangoLocal(999l);
        this.sc.loadScreen("/vista/cuponera/CuponeraConfigFXML.fxml", 726, 560, "/vista/login/menuFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuFXML.fxml", 540, 359, true);
//    }
    }

//AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("ACEPTAR (ENTER)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
//            if (tabArticulos.isSelected()) {
//            } else if (tabCuponera.isSelected()) {
//                configuracionCuponera();
//            }
            if (anchorPaneBuscarProducto.isVisible()) {
                buscarProductoModal();
            } else if (anchorPaneBuscarGasto.isFocused()) {
                buscarGastos();
            }
        }
        if (altN.match(event)) {
            limpiar();
//            btnBuscarProducto.setDisable(false);
//            btnBuscarCompra.setDisable(false);
        }
        if (keyCode == event.getCode().F5) {
            if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
                mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
            } else {
                actualizarProveedor();
            }
        }
        if (keyCode == event.getCode().F2) {
//            if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
//                mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
//            } else {
            if (txtProducto.getText().equals("") || txtIdProducto.getText().equals("") || preparacionList.size() == 0) {
                mensajeError("SE DEBE SELECCIONAR UN PRODUCTO COMO PREPARACION Y DEBE TENER DETALLE PARA REGISTRARLO");
            } else {
                if (!btnGuardar.isDisable()) {
                    panel01.setDisable(true);
                    panel02.setDisable(true);
                    panel03.setDisable(true);
                    panel04.setVisible(true);
                } else {
                    mensajeAlerta("DEBES PRESIONAR EL BOTON NUEVO (ALT+N) PARA LUEGO COMPLETAR LOS CAMPOS");
                }
            }
//            }
        }
        if (keyCode == event.getCode().F8) {
//            btnBuscarProducto.setDisable(true);
//            btnBuscarCompra.setDisable(true);
            limpiar();
//            btnGuardar.setDisable(true);
            btnActualizar.setDisable(false);
        }
        if (keyCode == event.getCode().F4) {
        }
        if (keyCode == event.getCode().F3) {
//            if (panelDetalleGastos.isVisible()) {
//            if (panel04.isVisible()) {
            agregarDetallePrecios();
//            }

//            }
        }
        if (keyCode == event.getCode().F7) {
            if (panelDetalleGastos.isVisible()) {
                verificarTipo();
            }
        }

        if (keyCode == event.getCode().ENTER) {
            if (anchorPaneBuscarProducto.isVisible()) {
                if (tableViewProducto.getSelectionModel().getSelectedIndex() >= 0) {
                    seleccionarProd();
                }
            } else if (anchorPaneBuscarGasto.isVisible()) {
                if (tableViewGasto.getSelectionModel().getSelectedIndex() >= 0) {
                    seleccionarGasto();
                }
            } else if (txtCantidadPreparado.isFocused()) {
                try {
                    if (Long.parseLong(txtCantidadPreparado.getText()) > 0) {
                        panel01.setDisable(false);
                        panel02.setDisable(false);
                        panel03.setDisable(false);
                        panel04.setVisible(false);
                        guardarProveedor();
                    }
                } catch (Exception e) {
                    mensajeError("EL CAMPO CANTIDAD NO DEBE QUEDAR VACIO, NI DEBE CONTENER LETRAS");
                } finally {
                }
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (tableViewPreparacion.getSelectionModel().getSelectedIndex() >= 0) {
                ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
                ButtonType cancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿SEGURO QUE DESEA ELIMINAR LA SELECCION?", ok, cancel);
                alert.showAndWait();
                if (alert.getResult() == ok) {
                    eliminarDetalle();
                    alert.close();
//                    mensajeAlerta("DATO ELIMINADO CORRECTAMENTE!!");
                } else {
                    alert.close();
                }
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (anchorPaneBuscarProducto.isVisible()) {
                cerrarBusquedaProducto();
            } else if (anchorPaneBuscarGasto.isVisible()) {
                cerrarBusquedaGasto();
            } else {
                cerrandoSesion();
            }
        }
    }

    private void busquedaArticulo() {
        abrirBusquedaProducto();
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

//    private void btnCargarArticuloAction(ActionEvent event) {
//        if (tab.isSelected()) {
//        } else if (tabCuponera.isSelected()) {
//            configuracionCuponera();
//        }
//    }
    private void listenPrecioMin() {
//        if (event.getCode().isDigitKey()) {
//            txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!oldValue.equalsIgnoreCase(newValue)) {
//        calcularPorcentaje(txtPrecioMin.getText());
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    }
//                });
//            });
//        }
    }

    private void listenIncrementoMin(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!increMin) {
//        calcularPrecioMin(txtPorcIncremParana01.getText());
//                        txtPorcIncremParana01.positionCaret(txtPorcIncremParana01.getLength());
//                    } else {
//                        increMin = false;
//                    }
//
//                });
//            });
//        }
    }

    private void calcularPrecioMin(String value) {
        numValidator = new NumberValidator();
        try {
            long c = (Long.parseLong(numValidator.numberValidator(value)) + 100) * Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
            long result = Math.round((c / 100));
//            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(result + "")));
//            precMin = true;
        } catch (Exception e) {
//            txtPrecioMin.setText("0");
        }
    }

    private void calcularPorcentaje(String value) {
//        numValidator = new NumberValidator();
////        DecimalFormat df = new DecimalFormat("#.0");
//        try {
//            if (!value.equals("null")) {
//                double result = (Long.parseLong(numValidator.numberValidator(value)) * 100) / Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
//                double reult2 = result - 100;
//                long res = Math.round(reult2);
//                txtPorcIncremParana01.setText(res + "");
//            }
//        } catch (Exception e) {
//            txtPorcIncremParana01.setText("0");
//        } finally {
//        }
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void buscarCliente() {
//        proveedorList = new ArrayList<>();
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
//        if (txtBuscar.getText().equalsIgnoreCase("")) {
//            mensajeAlerta("Debes ingresar una descripción para realizar la búsqueda");
//        } else if (!radioBtnNombreCta.isSelected() && !radioBtnCodigoCta.isSelected()) {
//            mensajeAlerta("Debes selecionar un rango de búsqueda");
//        } else {
//            String filtro = txtBuscar.getText();
////            String urlServer = "";
//            if (radioBtnNombreCta.isSelected()) {
////                urlServer = "null/" + filtro;
//                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc(filtro, "null");
//                if (listProveedor != null) {
//                    tableViewProveedor.getItems().clear();
//                    proveedorList = new ArrayList<>();
//                    for (Proveedor pro : listProveedor) {
//                        ProveedorDTO proDTO = pro.toProveedorDTO();
//                        proDTO.setFechaAlta(null);
//                        proDTO.setFechaMod(null);
//                        JSONObject json;
//                        try {
//                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
//                            proveedorList.add(json);
//                            actualizandoTabla();
//
//                        } catch (ParseException ex) {
//                            Logger.getLogger(GastosFXMLController.class
//                                    .getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                } else {
//                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
//                }
//            } else {
////                urlServer = filtro + "/null";
//                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc("null", filtro);
//                if (listProveedor != null) {
//                    tableViewProveedor.getItems().clear();
//                    proveedorList = new ArrayList<>();
//                    for (Proveedor pro : listProveedor) {
//                        ProveedorDTO proDTO = pro.toProveedorDTO();
//                        proDTO.setFechaAlta(null);
//                        proDTO.setFechaMod(null);
//                        JSONObject json;
//                        try {
//                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
//                            proveedorList.add(json);
//                            actualizandoTabla();
//
//                        } catch (ParseException ex) {
//                            Logger.getLogger(GastosFXMLController.class
//                                    .getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                } else {
//                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
//                }
//            }
////            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {a
////                try {
////                    URL url = new URL(Utilidades.ip + "/ServerParana/proveedor/" + urlServer);
////                    URLConnection uc = url.openConnection();
////                    BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
////                    while ((inputLine = br.readLine()) != null) {
////                        valor = (JSONArray) parser.parse(inputLine);
////                    }
////                    br.close();
////                    if (!valor.isEmpty()) {
////                        proveedorList = new ArrayList<>();
////                        for (Object obj : valor) {
////                            JSONObject json = (JSONObject) parser.parse(obj.toString());
////                            proveedorList.add(json);
////                        }
////                        actualizandoTabla();
////                    } else {
////                        mensajeAlerta("NO SE ENCONTRARON DATOS");
////                    }
////                } catch (FileNotFoundException e) {
////                    System.out.println(e.getLocalizedMessage());
////                } catch (IOException e) {
////                    System.out.println(e.getLocalizedMessage());
////                } catch (org.json.simple.parser.ParseException e) {
////                    e.printStackTrace();
////                }
////            } else {
//                mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//            }

//        }
    }

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTabla() {
        NumberValidator numVal = new NumberValidator();
        clienteData = FXCollections.observableArrayList(proveedorList);
        //columna Nombre ..................................................
//        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnFactura.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String cod = String.valueOf(data.getValue().get("numFactura"));
                return new ReadOnlyStringWrapper(cod.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
//        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnEmpresa.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("empresa"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("tipo"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnFecha.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFecha.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnFecha.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("fecha"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnMonto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("monto"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(numVal.numberValidator(nombre))));
            }
        });
        //columna Ruc .................................................
        tableViewProveedor.setItems(clienteData);
        if (!escucha) {
//            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

//    private void escucha() {
//        JSONParser parser = new JSONParser();
//        escucha = true;
//        tableViewProveedor.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//            if (tableViewProveedor.getSelectionModel().getSelectedItem() != null) {
////                mensajeAlerta("-> " + newSelection.get("codigo").toString().toUpperCase() + " ) " + newSelection.get("nombre").toString().toUpperCase());
//                txtNumFactura.setText(newSelection.get("numFactura").toString());
//                txtEmpresa.setText(newSelection.get("empresa").toString());
//                txtObservacion.setText(newSelection.get("observacion").toString());
//                txtIdProveedor.setText(newSelection.get("idGasto").toString().toUpperCase());
//                txtMonto.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newSelection.get("monto").toString())));
//                dpFecha.setValue(LocalDate.parse(newSelection.get("fecha").toString()));
////                int val = 0;
////                if (newSelection.get("tipo").toString().toUpperCase().equalsIgnoreCase("OTRO")) {
////                    val = 1;
////                }
//                Proveedor provee = proveedorDAO.getById(Long.parseLong(newSelection.get("idProveedor").toString()));
//                txtRucProveedor.setText(provee.getRuc());
//                txtIdGasto.setText(newSelection.get("idGasto").toString().toUpperCase());
//                cbTipo.getSelectionModel().select(newSelection.get("tipo").toString().toUpperCase());
//                cargarDatos();
//            }
//        });
//    }
//                try {
////                    pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
//                    txtNumeroTimbrado.setText(newSelection.get("timbrado").toString());
//                } catch (Exception ex) {
//                } finally {
//                }
//                try {
//                    String[] result = newSelection.get("inicioVigencia").toString().split(" ");
//                    java.sql.Date desde = java.sql.Date.valueOf(result[0]);
////                    pro.setInicioVigencia(desde);
//                    dpInicioVigencia.setValue(desde.toLocalDate());
//                } catch (Exception ex) {
//                    dpInicioVigencia.setValue(null);
//                } finally {
//                }
//                try {
//                    String[] result = newSelection.get("finVigencia").toString().split(" ");
//                    java.sql.Date hasta = java.sql.Date.valueOf(result[0]);
//                    dpFinVigencia.setValue(hasta.toLocalDate());
//                } catch (Exception ex) {
//                    dpFinVigencia.setValue(null);
//                } finally {
//                }
//
//                cbPersona.setValue("JURIDICA");
//                if (newSelection.containsKey("callePrincipal")) {
//                    txtDireccion.setText(newSelection.get("callePrincipal").toString());
//                }
//                if (newSelection.containsKey("telefono")) {
//                    txtTelefono.setText(newSelection.get("telefono").toString());
//                }
//                if (newSelection.containsKey("telefono2")) {
//                    txtCelular.setText(newSelection.get("telefono2").toString());
//                }
//                if (newSelection.containsKey("email")) {
//                    txtMail.setText(newSelection.get("email").toString());
//                }
//                if (newSelection.containsKey("timbrado")) {
//                    txtTimbrado.setText(newSelection.get("timbrado").toString());
//                }
//                if (newSelection.containsKey("usuAlta")) {
//                    txtUsuAlta.setText(newSelection.get("usuAlta").toString());
//                }
//                if (newSelection.containsKey("usuMod")) {
//                    txtUsuMod.setText(newSelection.get("usuMod").toString());
//                }
//                if (newSelection.containsKey("fechaAlta")) {
//                    txtFechaAlta.setText(newSelection.get("fechaAlta").toString());
//                }
//                if (newSelection.containsKey("fechaMod")) {
//                    txtFechaMod.setText(newSelection.get("fechaMod").toString());
//                }
//
////                txtVtoDia.setText(newSelection.get("condven").toString());
//                txtNroAcuerdo.setText("");
//                txtContacto.setText("");
////                txtSaldo.setText(newSelection.get("saldo").toString());
//                txtSIDI.setText("");
//
//                if (newSelection.containsKey("familiaProv")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("familiaProv").toString());
//                        cbFamilia.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("pais")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("pais").toString());
//                        cbPais.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("departamento")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("departamento").toString());
//                        cbDpto.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("ciudad")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("ciudad").toString());
//                        cbCiudad.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("barrio")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("barrio").toString());
//                        cbBarrio.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//
//            } else {
//            }
//            });
//            }
    private void cargarFamilia() {
//        cbTipo.getItems().add("COMPRAS P/ PREPARACION");
//        cbTipo.getItems().add("COMPRAS P/ VENTA");
//        cbTipo.getItems().add("INSUMOS");
//        cbTipo.getItems().add("SERVICIOS PERSONALES");
//        cbTipo.getItems().add("SERVICIOS NO PERSONALES");
//        cbTipo.getItems().add("SERVICIOS FINANCIEROS"); //        cbCondicion.getItems().add("10");
        //        cbCondicion.getItems().add("15");
        //        cbCondicion.getItems().add("30");
        //        cbCondicion.getItems().add("45");
        //
        //        cbFamilia.getItems().add("COMERCIALES");
        //        cbFamilia.getItems().add("DISTRIBUIDORES");
        //
        //        JSONObject obj1 = new JSONObject();
        //        obj1.put("idFamiliaProv", 1);
        //        obj1.put("descripcion", "COMERCIALES");
        //
        //        JSONObject obj2 = new JSONObject();
        //        obj2.put("idFamiliaProv", 2);
        //        obj2.put("descripcion", "DISTRIBUIDORES");
        //
        //        hashFamilia.put("COMERCIALES", obj1);
        //        hashFamilia.put("DISTRIBUIDORES", obj2);
    }

//    private void listenerCamposCombos() {
//        cbTipo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                String dato = cbTipo.getSelectionModel().getSelectedItem();
//                if (dato.equalsIgnoreCase("SERVICIOS PERSONALES")) {
//                    lblTipo.setText("Salario, Servicio de limpieza y otros.");
//                    deshabilitarDetalleGastos();
//                } else if (dato.equalsIgnoreCase("SERVICIOS NO PERSONALES")) {
//                    lblTipo.setText("Alquiler, Essap, Ande y otros.");
//                    deshabilitarDetalleGastos();
//                } else if (dato.equalsIgnoreCase("COMPRAS P/ VENTA")) {
//                    lblTipo.setText("Compras para venta directa de mercadería.");
//                    habilitarDetalleGastos();
//                    habilitarDetalleGastosVentaDirecta();
//                } else if (dato.equalsIgnoreCase("SERVICIOS FINANCIEROS")) {
//                    lblTipo.setText("Contabilidad, Iva, comisión Bancaria y otros.");
//                    deshabilitarDetalleGastos();
//                } else if (dato.equalsIgnoreCase("INSUMOS")) {
//                    lblTipo.setText("Compras para uso del local.");
//                    deshabilitarDetalleGastos();
//                } else if (dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
//                    lblTipo.setText("Compras para preparación de productos para la venta.");
//                    habilitarDetalleGastos();
//                    habilitarDetalleGastosVentaPreparacion();
//                }
//            }
//        });
//    }
    public void deshabilitarDetalleGastos() {
        tabDetalleGasto.setDisable(true);
    }

    public void habilitarDetalleGastos() {
        tabDetalleGasto.setDisable(false);
    }

    public void habilitarDetalleGastosVentaDirecta() {
//        txtCodProductoDetalle.setDisable(false);
    }

    public void habilitarDetalleGastosVentaPreparacion() {
//        txtCodProductoDetalle.setDisable(true);
    }
//        cbDpto.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                limpiarNiveles(2);
//                JSONObject json = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem());
//                cargarCiudad(Long.parseLong(json.get("idDepartamento").toString()));
//            }
//        });
//        cbCiudad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                limpiarNiveles(3);
//                JSONObject json = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem());
//                cargarBarrio(Long.parseLong(json.get("idCiudad").toString()));
//            }
//        });
//        cbBarrio.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
////                JSONObject json = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
////                cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
//            }
//        });

    private void cargarPais() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Pais> listPais = paisDAO.listar();
        if (listPais != null) {
            for (Pais objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                PaisDTO paisDTO = objPais.toPaisDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbPais.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashPais.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(PreparacionFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
//            actualizandoTabla();
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE PAIS");
        }
//        if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {
//            try {
//                URL url = new URL(Utilidades.ip + "/ServerParana/pais");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    valor = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//                if (!valor.isEmpty()) {
//                    for (Object obj : valor) {
//                        JSONObject json = (JSONObject) parser.parse(obj.toString());
//                        proveedorList.add(json);a
//
//                        cbPais.getItems().add(json.get("descripcion").toString());
//                        hashPais.put(json.get("descripcion").toString(), json);
//                    }
//                    actualizandoTabla();
//                } else {
//                    mensajeAlerta("NO SE ENCONTRARON DATOS");
//                }
//            } catch (FileNotFoundException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (IOException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (org.json.simple.parser.ParseException e) {
//                e.printStackTrace();
//            }
//        } else {
//            mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//        }
    }

    private void cargarCiudad(long idDpto) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Ciudad> listPais = ciudadDAO.listarPorCiudad(idDpto);
        if (listPais != null) {
            for (Ciudad objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                CiudadDTO paisDTO = objPais.toOnlyCiudadDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbCiudad.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashCiudad.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(PreparacionFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE CIUDAD");
        }
    }

    private void cargarDepartamento(long idPais) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Departamento> listPais = dptoDAO.listarPorPais(idPais);
        if (listPais != null) {
            for (Departamento objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                DepartamentoDTO paisDTO = objPais.toSinOtrosDatosDepartamentoDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbDpto.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashDpto.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(PreparacionFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void cargarBarrio(long idCiudad) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Barrio> listPais = barrioDAO.listarPorCiudad(idCiudad);
        if (listPais != null) {
            for (Barrio objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                BarrioDTO paisDTO = objPais.toBarrioAClienteDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbBarrio.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashBarrio.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(PreparacionFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void limpiar() {
        numValidator = new NumberValidator();
        if (preparacionList.size() > 0) {
            for (JSONObject jSONObject : preparacionList) {
                DetalleGasto dg = detalleGastoDAO.getById(Long.parseLong(jSONObject.get("idDetalleGasto").toString()));
                BigDecimal bd = new BigDecimal(jSONObject.get("cantidad").toString());
                BigDecimal result = dg.getCantidad().add(bd);
                dg.setCantidad(result);
                detalleGastoDAO.actualizar(dg);
            }
        }

        txtProducto.setText("");
        txtIdProducto.setText("");
        txtDescriGasto.setText("");
        txtIdDetalleGasto.setText("");

        txtMonto.setText("");
        txtCantidad.setText("");
        txtCantidadPreparado.setText("");

//        btnGuardar.setDisable(false);
        btnActualizar.setDisable(true);
        labelTotalGs.setText("Gs 0");
        txtIdGasto.setText("");
        txtIdDetalleGasto.setText("");
        detalleList = new ArrayList<>();
        tableViewGasto.getItems().clear();
        tableViewProducto.getItems().clear();
//        tableViewProveedor.getItems().clear();
        detalleList = new ArrayList<>();
        tableViewPreparacion.getItems().clear();

        articuloModalList = new ArrayList<>();
        gastoModalList = new ArrayList<>();
        preparacionList = new ArrayList<>();
    }

    @FXML
    private void btnActualizarAction(ActionEvent event) {
        if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
            mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
        } else {
            actualizarProveedor();
        }
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
//        btnBuscarProducto.setDisable(true);
//        btnBuscarCompra.setDisable(true);
        limpiar();
//        btnGuardar.setDisable(true);
        btnActualizar.setDisable(false);
    }

    private void guardarProveedor() {
        numValidator = new NumberValidator();
        PreparacionCabecera pc = new PreparacionCabecera();
        Articulo art = artDAO.getById(Long.parseLong(txtIdProducto.getText()));
        long resp = Long.parseLong(art.getStockActual()) + Long.parseLong(txtCantidadPreparado.getText());
        art.setStockActual(resp + "");
        artDAO.actualizar(art);

        Articulo articu = new Articulo();
        articu.setIdArticulo(art.getIdArticulo());
        pc.setArticulo(articu);
        pc.setCantidad(txtCantidadPreparado.getText());
        pc.setDescripcion(txtProducto.getText());
        java.sql.Date desde = java.sql.Date.valueOf(dpFecha.getValue());
        pc.setFecha(desde);
        pc.setTotal(numValidator.numberValidator(labelTotalGs.getText()));

        pc = pcDAO.insertarObtenerObj(pc);

        int num = 0;
        for (JSONObject json : preparacionList) {
            PreparacionDetalle pd = new PreparacionDetalle();
            java.sql.Date fec = java.sql.Date.valueOf(json.get("fecha").toString());
            BigDecimal bd = new BigDecimal(json.get("cantidad").toString());
            DetalleGasto dg = new DetalleGasto();
            dg.setIdDetalleGasto(Long.parseLong(json.get("idDetalleGasto").toString()));

            pd.setDetalleGasto(dg);
            pd.setCantidad(bd);
            pd.setCosto(Long.parseLong(json.get("costo").toString()));
            pd.setDescripcion(json.get("descripcion").toString());
            pd.setFechaCompra(fec);
            pd.setPreparacionCabecera(pc);
            pdDAO.insercionMasiva(pd, num);
            num++;
        }

        mensajeAlerta("DATOS AGREGADOS CORRECTAMENTE.");
//        btnBuscarProducto.setDisable(true);
//        btnBuscarCompra.setDisable(true);
        preparacionList = new ArrayList<>();
        tableViewPreparacion.getItems().clear();
        limpiar();
//        btnGuardar.setDisable(true);
        btnActualizar.setDisable(false);
//        if (cbTipo.getSelectionModel().getSelectedIndex() >= 0 || !txtObservacion.getText().equals("") || !txtMonto.getText().equals("")) {
//            Timestamp timestamp = new Timestamp(date.getTime());
//            Gastos pro = new Gastos();
//            numValidator = new NumberValidator();
//            pro.setEmpresa(txtEmpresa.getText());
//            pro.setDescripcion(txtObservacion.getText());
//            pro.setFactura(txtNumFactura.getText());
//            pro.setTipo(cbTipo.getSelectionModel().getSelectedItem());
//            pro.setMonto(Integer.parseInt(numValidator.numberValidator(txtMonto.getText())));
//            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
//            pro.setFecha(date);
//            if (!txtRucProveedor.getText().equals("")) {
//                Proveedor p1 = proveedorDAO.listarPorRuc(txtRucProveedor.getText());
//                pro.setProveedor(p1);
//            }
//        if (txtNumeroTimbrado.getText().equals("") || txtNumeroTimbrado.getText().equals("0")) {
//            pro.setTimbrado(0);
//        } else {
//            pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
//        }
//        try {
//            java.sql.Date desde = java.sql.Date.valueOf(dpInicioVigencia.getValue());
//            pro.setInicioVigencia(desde);
//        } catch (Exception ex) {
//        } finally {
//        }
//        try {
//            java.sql.Date hasta = java.sql.Date.valueOf(dpFinVigencia.getValue());
//            pro.setFinVigencia(hasta);
//        } catch (Exception ex) {
//        } finally {
//        }
////        pro.setNroLocal(Integer.parseInt(txtNroAcuerdo.getText()));
//
//        if (!cbFamilia.getValue().toString().equals("")) {
//            JSONObject jsonFLIA = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem().toUpperCase());
//            FamiliaProv flia = new FamiliaProv();
//            flia.setIdFamiliaProv(Long.parseLong(jsonFLIA.get("idFamiliaProv").toString()));
//            pro.setFamiliaProv(flia);
//        }
//        if (!cbPais.getValue().toString().equals("")) {
//            JSONObject jsonPAIS = hashPais.get(cbPais.getSelectionModel().getSelectedItem().toUpperCase());
//            Pais pais = new Pais();
//            pais.setIdPais(Long.parseLong(jsonPAIS.get("idPais").toString()));
//            pro.setPais(pais);
//        }
//        try {
//            if (!cbDpto.getValue().toString().equals("")) {
//                JSONObject jsonDPTO = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem().toUpperCase());
//                Departamento pais = new Departamento();
//                pais.setIdDepartamento(Long.parseLong(jsonDPTO.get("idDepartamento").toString()));
//                pro.setDepartamento(pais);
//            }
//        } catch (Exception e) {
//            Departamento pais = new Departamento();
//            pais.setIdDepartamento(0L);
//            pro.setDepartamento(pais);
//        } finally {
//        }
//        try {
//            if (!cbCiudad.getValue().toString().equals("")) {
//                JSONObject jsonCIU = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem().toUpperCase());
//                Ciudad pais = new Ciudad();
//                pais.setIdCiudad(Long.parseLong(jsonCIU.get("idCiudad").toString()));
//                pro.setCiudad(pais);
//            }
//        } catch (Exception e) {
//            Ciudad pais = new Ciudad();
//            pais.setIdCiudad(0l);
//            pro.setCiudad(pais);
//        } finally {
//        }
//        try {
//            if (!cbBarrio.getValue().toString().equals("")) {
//                JSONObject jsonBARRIO = hashBarrio.get(cbBarrio.getSelectionModel().getSelectedItem().toUpperCase());
//                Barrio pais = new Barrio();
//                pais.setIdBarrio(Long.parseLong(jsonBARRIO.get("idBarrio").toString()));
//                pro.setBarrio(pais);
//            }
//        } catch (Exception e) {
//            Barrio pais = new Barrio();
//            pais.setIdBarrio(0l);
//            pro.setBarrio(pais);
//        } finally {
//        }
//            if (gastosDAO.insertarEstado(pro)) {
////            registrarProveedorSincro(pro);
//                pro = gastosDAO.recuperarUltimo();
//                txtIdGasto.setText(pro.getIdGasto() + "");
//                mensajeAlerta("DATOS REGISTRADOS CORRECTAMENTE");
//                tableViewProveedor.getSelectionModel().clearSelection();
//                String dato = cbTipo.getSelectionModel().getSelectedItem();
//
//                if (dato.equalsIgnoreCase("COMPRAS P/ VENTA") || dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
//                } else {
//                    limpiar();
//                }
//                listarporFecha();
//                btnGuardar.setDisable(true);
//                btnActualizar.setDisable(false);
//            } else {
//                mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS VERIFIQUELOS.");
//            }
//        } else {
//            mensajeAlerta("SE DEBEN AGREGAR TIPO DE GASTOS, OBSERVACION Y MONTO.");
//        }
    }

    private void actualizarProveedor() {
        Timestamp timestamp = new Timestamp(date.getTime());
        Gastos pro = new Gastos();
        pro.setIdGasto(Long.parseLong(txtIdProveedor.getText()));
        pro.setEmpresa(txtEmpresa.getText());
        pro.setDescripcion(txtObservacion.getText());
//        pro.setDescripcion(txtNombre.getText());
        pro.setFactura(txtNumFactura.getText());
        pro.setMonto(Integer.parseInt(numValidator.numberValidator(txtMonto.getText())));
//        pro.setTipo(cbTipo.getSelectionModel().getSelectedItem());
        if (!txtRucProveedor.getText().equals("")) {
            Proveedor p1 = proveedorDAO.listarPorRuc(txtRucProveedor.getText());
            pro.setProveedor(p1);
        }
//        a
//        java.sql.Date date = new java.sql.Date(dpFecha.getValue());
        pro.setFecha(java.sql.Date.valueOf(dpFecha.getValue()));

        if (gastosDAO.actualizarEstado(pro)) {
//            actualizarProveedorSincro(pro);
            mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE");
//            tableViewProveedor.getSelectionModel().clearSelection();
//            String dato = cbTipo.getSelectionModel().getSelectedItem();
//            if (dato.equalsIgnoreCase("COMPRAS P/ VENTA") || dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
//            } else {
//                limpiar();
//            }
            listarporFecha();
//            btnGuardar.setDisable(true);
            btnActualizar.setDisable(false);
        } else {
            mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS VERIFIQUELOS");
        }
    }

    private void limpiarNiveles(int val) {
        switch (val) {
            case 1:
//                cbPais.getItems().clear();
                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbDpto.setValue("");
                cbCiudad.setValue("");
                cbBarrio.setValue("");
                break;
            case 2:
//                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbCiudad.setValue("");
                cbBarrio.setValue("");

                break;
            case 3:
//                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbBarrio.setValue("");
                break;
            default:
                break;
        }
    }

    private void registrarProveedorSincro(Proveedor arti) {
        Timestamp timestamp = new Timestamp(date.getTime());
        String query = "INSERT INTO cuenta.proveedor(id_familia_prov, id_pais, id_departamento, id_ciudad, "
                + "id_barrio, calle_principal, ruc,"
                + "telefono, telefono2, timbrado, usu_alta, fecha_alta, usu_mod, fecha_mod, "
                + "saldo, cond_pago, descripcion, fin_vigencia, inicio_vigencia) "
                + "VALUES (1, 100, " + arti.getDepartamento().getIdDepartamento() + ", " + arti.getCiudad().getIdCiudad() + ", "
                + arti.getBarrio().getIdBarrio() + ",\"" + arti.getCallePrincipal() + "\", \"" + arti.getRuc() + "\", \""
                + arti.getTelefono() + "\", \"" + arti.getTelefono2() + "\", \"" + arti.getTimbrado() + "\", "
                + "\"" + Identity.getNomFun() + "\",\"" + timestamp + "\",\"" + Identity.getNomFun() + "\",\"" + timestamp + "\", "
                + arti.getSaldo() + ", \"" + arti.getCondPago() + "\", \"" + arti.getDescripcion() + "\", \"" + arti.getFinVigencia() + "\", \"" + arti.getInicioVigencia()
                + "\")";
        ConexionPostgres.conectarServer();
        for (IpCaja ip : ipCajaDAO.listar()) {
            String sql = "INSERT INTO sincro.proveedores(operacion, ip, fecha, descripcion_dato) VALUES ('I', '" + ip.getIpCaja() + "', 'now()', '" + query + "')";
            System.out.println("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* ALMACENANDO DATOS ARTICULOS SINCRO ********");
                }
                ps.close();
                ConexionPostgres.getConServer().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getConServer().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private void actualizarProveedorSincro(Proveedor arti) {
        Timestamp timestamp = new Timestamp(date.getTime());
        String sql = "UPDATE cuenta.proveedor SET id_familia_prov=1, id_pais=100, id_departamento=" + arti.getDepartamento().getIdDepartamento() + ", "
                + "id_ciudad=" + arti.getCiudad().getIdCiudad() + ", id_barrio=" + arti.getBarrio().getIdBarrio() + ", calle_principal=\"" + arti.getCallePrincipal() + "\", "
                + "telefono=\"" + arti.getTelefono() + "\", telefono2=\"" + arti.getTelefono2() + "\", timbrado=\"" + arti.getTimbrado() + "\", usu_mod=\"" + Identity.getNomFun() + "\", fecha_mod=\"" + timestamp + "\", "
                + "saldo=" + arti.getSaldo() + ", cond_pago=\"" + arti.getCondPago() + "\", descripcion=\"" + arti.getDescripcion() + "\", fin_vigencia=\"" + arti.getFinVigencia() + "\", inicio_vigencia=\"" + arti.getInicioVigencia() + "\" "
                + "WHERE ruc=\"" + arti.getRuc() + "\"";
//        String sql = "INSERT INTO cuenta.proveedor(id_familia_prov, id_pais, id_departamento, id_ciudad, "
//                + "id_barrio, calle_principal, ruc,"
//                + "telefono, telefono2, timbrado, email, "
//                + "saldo, cond_pago, descripcion, fin_vigencia, inicio_vigencia) "
//                + "VALUES (1, 100, " + arti.getDepartamento().getIdDepartamento() + ", " + arti.getCiudad().getIdCiudad() + ", "
//                + arti.getBarrio().getIdBarrio() + ",'" + arti.getCallePrincipal() + "', '" + arti.getRuc() + "', '"
//                + arti.getTelefono() + "', '" + arti.getTelefono2() + "', '" + arti.getTimbrado() + "', '" + arti.getEmail() + "', '"
//                + arti.getSaldo() + "', '" + arti.getCondPago() + "', '" + arti.getDescripcion() + "', '" + arti.getFinVigencia() + "', '" + arti.getInicioVigencia()
//                + "')";
        sql = sql.replaceAll("\"\"", "null");
        ConexionPostgres.conectarServer();
        for (IpCaja ip : ipCajaDAO.listar()) {
            String query = "INSERT INTO sincro.proveedores(operacion, ip, fecha, descripcion_dato) VALUES ('U', '" + ip.getIpCaja() + "', 'now()', '" + sql + "')";
            System.out.println("-->> " + query);
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(query)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* ALMACENANDO DATOS ARTICULOS SINCRO ********");
                }
                ps.close();
                ConexionPostgres.getConServer().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getConServer().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private void listarporFecha() {
        List<Gastos> listGastos = gastosDAO.listarPorFechas(firstDay, lastDay);
        proveedorList = new ArrayList<>();
        numValidator = new NumberValidator();
        int monto = 0;
        for (Gastos listGasto : listGastos) {
            JSONObject json = new JSONObject();
            json.put("numFactura", listGasto.getFactura());
            json.put("empresa", listGasto.getEmpresa());
            json.put("observacion", listGasto.getDescripcion());
            json.put("fecha", listGasto.getFecha() + "");
            json.put("monto", listGasto.getMonto() + "");
            json.put("idGasto", listGasto.getIdGasto());
            json.put("tipo", listGasto.getTipo());
            try {
                json.put("idProveedor", listGasto.getProveedor().getIdProveedor());
            } catch (Exception e) {
                json.put("idProveedor", 0);
            } finally {
            }

            monto += listGasto.getMonto();

            proveedorList.add(json);
        }
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(monto + "")));
        actualizandoTabla();
    }

    private void btnAgregarDetalleAction(ActionEvent event) {
//        agregarDetallePrecios();
    }

    private void txtCodProductoDetalleReleased(KeyEvent event) {
//        KeyCode keyCode = event.getCode();
//        if (keyCode == event.getCode().ENTER) {
//            if (txtCodProductoDetalle.isFocused()) {
//                busquedaArticulo();
//            }
//        }
        keyPress(event);
    }

    private void txtPrecioMinKeyPress(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
//            txtPorcIncremParana01.setText("0");
            listenTextFieldMin();
        }
        keyPress(event);
    }

    private void listenTextFieldMin() {
//        txtPrecioMin.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//        if (oldValue.length() == 0) {
//            patternMonto = true;
//        }
//        System.out.println("MIN: " + newValue);
//        String newV = "";
//        String oldV = "";
//        if (!newValue.contentEquals("")) {
//            newV = numValidator.numberValidator(newValue);
//            oldV = numValidator.numberValidator(oldValue);
//            long lim = -1l;
//            boolean limite = true;
//            if (!newV.contentEquals("")) {//límite del monto...
//                if (newV.length() < 19) {
//                    lim = Long.valueOf(newV);
//                    if (lim > 0 && lim < 2147483647) {
//                        limite = false;
//                    }
//                }
//            }
//            if (limite) {
//                if (!newValue.contentEquals("") && !newV.contentEquals("")) {
//                    if (patternMonto) {
//                        if (oldV.length() != 0) {
//                            param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                        } else {
//                            param = oldValue;
//                        }
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = oldValue;
//                    }
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(param);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText("");
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                if (!oldValue.contentEquals("")) {
//                    if (!newValue.contentEquals("")) {
//                        if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                            if (patternMonto) {
//                                param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                                patternMonto = false;
//                            } else {
//                                patternMonto = true;
//                                param = oldValue;
//                            }
//                            Platform.runLater(() -> {
//                                txtPrecioMin.setText(param);
//                                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtPrecioMin.setText("");
//                            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                        });
//                    }
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(oldValue);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                if (patternMonto) {
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(newV));
//                    patternMonto = false;
//                } else {
//                    patternMonto = true;
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                }
//                Platform.runLater(() -> {
//                    txtPrecioMin.setText(param);
//                    txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                });
//            }
//        }
//        try {
//            numValidator = new NumberValidator();
//            String monto = numValidator.numberValidator(txtPrecioMin.getText());
//            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
//            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//        } catch (Exception e) {
//        } finally {
//        }

        Platform.runLater(() -> {
            try {
                numValidator = new NumberValidator();
//                String monto = numValidator.numberValidator(txtPrecioMin.getText());
//                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
//                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
            } catch (Exception e) {
            } finally {
            }
        });

//        });
    }

    private void txtPorcIncremParana01KeyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        System.out.println("->> " + keyCode + " " + event.getCode().F5 + " " + event.getCode().F8);
        if (keyCode != event.getCode().F5 || keyCode != event.getCode().F2) {
//            txtPrecioMin.setText("0");
        }
        keyPress(event);
    }

    private void btnConfirmarVentaAction(ActionEvent event) {
        confirmarDetallePrecios();
    }

    private void verificarTipo() {
//        if (!txtCodProductoDetalle.isDisabled()) {
//            if (txtCodProductoDetalle.getText().equals("") || txtDescripcionDetalle.getText().equals("") || txtPrecioDetalle.getText().equals("") || txtCantidadDetalle.getText().equals("")) {
//                mensajeAlerta("LOS CAMPOS CODIGO, DESCRIPCION, PRECIO Y CANTIDAD NO DEBEN QUEDAR VACIOS");
//            } else {
//                cbSeccionData1.getSelectionModel().select(0);
//                panelPrecioVenta.setVisible(true);
//                panelDetalleGastos.setDisable(true);
//            }
//        } else {
//            DetalleGasto dg = new DetalleGasto();
//            Gastos gas = new Gastos();
//            gas.setIdGasto(Long.parseLong(txtIdGasto.getText()));
//
//            BigDecimal bigDecimal = new BigDecimal(txtCantidadDetalle.getText().replace(",", "."));
//            dg.setCantidad(bigDecimal);
//            dg.setCodigo(txtCodProductoDetalle.getText());
//            dg.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
//            dg.setDescripcion(txtDescripcionDetalle.getText());
//            dg.setGastos(gas);
//            detalleGastoDAO.insertar(dg);
//            mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE.");
//            cargarDatos();
//            limpiarDatos();
//        }
    }

    private void confirmarDetallePrecios() {
        if (txtIdGasto.getText().equals("")) {
            mensajeAlerta("DEBES SELECCIONAR UN GASTO O CREAR UNO NUEVO PARA PODER AGREGAR DETALLES");
        } else {
            numValidator = new NumberValidator();
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            DetalleGasto dg = new DetalleGasto();
            Gastos gas = new Gastos();
            gas.setIdGasto(Long.parseLong(txtIdDetalleGasto.getText()));

            BigDecimal bigDecimal = new BigDecimal(txtCantidadDetalle.getText().replace(",", "."));
            dg.setCantidad(bigDecimal);
//            dg.setCodigo(txtCodProductoDetalle.getText());
            dg.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
            dg.setDescripcion(txtDescripcionDetalle.getText());
            dg.setGastos(gas);

//            if (txtPrecioMin.getText().equalsIgnoreCase("") || txtPorcIncremParana01.getText().equalsIgnoreCase("") || txtPrecioMin.getText().equalsIgnoreCase("0") || txtPorcIncremParana01.getText().equalsIgnoreCase("0") || cbSeccionData1.getSelectionModel().getSelectedItem().equalsIgnoreCase("SELECCIONE SECCION")) {
//                mensajeAlerta("LOS CAMPOS NO DEBEN QUEDAR VACIOS NI SER IGUAL A CERO");
//            } else {
//
//                detalleGastoDAO.insertar(dg);
//                mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE.");
//                cargarDatos();
//
//                panelPrecioVenta.setVisible(false);
//                panelDetalleGastos.setDisable(false);
//                Articulo art = artDAO.buscarCod(txtCodProductoDetalle.getText());
//                if (art != null) {
//                    long stockActual = Long.parseLong(art.getStockActual());
//                    long result = stockActual + Long.parseLong(txtCantidadDetalle.getText());
//                    art.setStockActual(result + "");
//                    art.setDescripcion(txtDescripcionDetalle.getText());
//                    art.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
//                    art.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
//
//                    artDAO.actualizar(art);
//                } else {
//                    Articulo arti = new Articulo();
//                    arti.setFechaAlta(timestamp);
//                    arti.setFechaMod(timestamp);
//                    arti.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
//                    arti.setPrecioMay(0L);
//                    arti.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
//                    arti.setUrlImagen(null);
//                    arti.setDescMaxMay(null);
//                    arti.setDescMaxMin(null);
////                if (txtDescProveedor.getText().equals("")) {
////                    arti.setDescProveedor(Long.parseLong("0"));
////                } else {
////                    arti.setDescProveedor(Long.parseLong(txtDescProveedor.getText()));
//                    arti.setDescProveedor(null);
////                }
//                    arti.setDescMaxMay(null);
//                    if (cbSeccionData1.getSelectionModel().getSelectedIndex() >= 0) {
//                        Seccion sec = seccionDAO.listarPorNombre(cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase());
//                        arti.setSeccion(sec);
//                    }
//
//                    arti.setServicio(false);
//                    arti.setStockeable(false);
//                    arti.setStockActual(txtCantidadDetalle.getText());
//                    arti.setStockMax("1");
//                    arti.setStockMin("1");
//                    arti.setServicio(true);
//                    arti.setStockeable(false);
//
//                    Unidad unidad = new Unidad();
//                    unidad.setIdUnidad(1L);
//                    arti.setUnidad(unidad);
//
//                    Iva iva = new Iva();
//                    iva.setIdIva(3l);
//                    arti.setIva(iva);
//
//                    arti = artDAO.insertarRecuperarDato(arti);
//                }
//                long val = Long.parseLong(numValidator.numberValidator(labelTotalGsDetalle.getText()));
//                long val2 = Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
//                long result = val + val2;
////                labelTotalGsDetalle.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(result + ""))));
//                limpiarDatos();
//            }
        }
    }

    private void limpiarDatos() {
//        txtCodProductoDetalle.setText("");
        txtDescripcionDetalle.setText("");
        txtPrecioDetalle.setText("");
        txtCantidadDetalle.setText("");
        cbSeccionData1.getSelectionModel().select(0);
//        txtPrecioMin.setText("");
//        txtPorcIncremParana01.setText("");
    }

    public void cargarDatos() {
        List<DetalleGasto> listGastos = detalleGastoDAO.listarPorFactura(Long.parseLong(txtIdDetalleGasto.getText()));
        detalleList = new ArrayList<>();
        numValidator = new NumberValidator();
        int monto = 0;
        for (DetalleGasto listGasto : listGastos) {
            JSONObject json = new JSONObject();
            json.put("codigo", listGasto.getCodigo());
            json.put("descripcion", listGasto.getDescripcion());
            json.put("cantidad", listGasto.getCantidad());
            json.put("costo", listGasto.getCosto());
//            json.put("monto", listGasto.get);
//            json.put("idGasto", listGasto.getIdGasto());
//            json.put("tipo", listGasto.getTipo());
            monto += listGasto.getCosto();

            detalleList.add(json);
        }
        labelTotalGsDetalle.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(monto + "")));
        actualizandoTablaDetalle();
    }

    private void actualizandoTablaDetalle() {
        NumberValidator numVal = new NumberValidator();
        detalleData = FXCollections.observableArrayList(detalleList);
        //columna Nombre ..................................................
//        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigoDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnCodigoDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String cod = String.valueOf(data.getValue().get("codigo"));
                return new ReadOnlyStringWrapper(cod.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
//        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcionDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnDescripcionDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("descripcion"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidadDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnCantidadDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("cantidad"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnMontoDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnMontoDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("costo"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(numVal.numberValidator(nombre))));
            }
        });
        //columna Ruc .................................................
        tableViewDetalle.setItems(detalleData);
//        if (!escucha) {
//            escucha();
//        }
    }

    private void btnProveedorEstandarAction(ActionEvent event) {
        BuscarProveedorFXMLController.cargarRucRazon(txtRucProveedor, txtEmpresa);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    @FXML
    private void btnBuscarProductoAction(ActionEvent event) {
        busquedaArticulo();
    }

    @FXML
    private void btnBuscarCompraAction(ActionEvent event) {
        abrirBusquedaGasto();
    }

    @FXML
    private void btnAgregarAction(ActionEvent event) {
        agregarDetallePrecios();
    }

    private void abrirBusquedaProducto() {
        anchorPaneBuscarProducto.setVisible(true);

        txtBuscarCodProducto.setText("");
        txtBuscarDesriProducto.setText("");
        tableViewProducto.getItems().clear();
        txtCantidadModal.setText("1");
        txtBuscarCodProducto.requestFocus();
        cbSeccionData1.getSelectionModel().select(0);
        buscarProductoModal();
    }

    public void cerrarBusquedaProducto() {
        anchorPaneBuscarProducto.setVisible(false);
    }

    private void abrirBusquedaGasto() {
        anchorPaneBuscarGasto.setVisible(true);

        cbProveedorGasto.getSelectionModel().select(0);
        buscarGastos();
    }

    public void cerrarBusquedaGasto() {
        anchorPaneBuscarGasto.setVisible(false);
    }

    private void buscarProductoModal() {
        if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && txtBuscarDesriProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
            listarTodos();
        } else {
            if (txtBuscarDesriProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
                listarPorCodigo();
            } else if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && cbSeccionData1.getSelectionModel().getSelectedIndex() == 0) {
                listarPorDescripcion();
            } else if (txtBuscarCodProducto.getText().equalsIgnoreCase("") && txtBuscarDesriProducto.getText().equalsIgnoreCase("")) {
                listarPorSeccion();
            } else {
                listarPorCombinaciones();
            }
        }
    }

    private void listarTodos() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarSoloDiez();
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorDescripcion() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorDescripcion(txtBuscarDesriProducto.getText());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorCodigo() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorCodigo(txtBuscarCodProducto.getText());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorSeccion() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        List<Articulo> listArt = artDAO.listarPorSeccion(cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase());
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void listarPorCombinaciones() {
        JSONParser parser = new JSONParser();
        articuloModalList = new ArrayList<>();
        mapeoArticulo = new HashMap();
        int id = cbSeccionData1.getSelectionModel().getSelectedIndex();
        String seccion = "";
        if (id != 0) {
            seccion = cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase();
        }
        List<Articulo> listArt = artDAO.listarPorCombinaciones(txtBuscarCodProducto.getText(), txtBuscarDesriProducto.getText(), seccion);
        for (Articulo art : listArt) {
            try {
                JSONObject json = new JSONObject();
                json.put("idArticulo", art.getIdArticulo());
                json.put("descripcion", art.getDescripcion());
                json.put("codArticulo", art.getCodArticulo());
                json.put("precioMin", art.getPrecioMin());
                json.put("seccion", art.getSeccion().getDescripcion());
                try {
                    byte[] artic = art.getImagen();
                    Image img = (new Image(new ByteArrayInputStream(artic)));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
//                    System.out.println(art.getIdArticulo() + ") ->> " + art.getCodArticulo() + " - " + art.getDescripcion() + " - " + art.getPrecioMin());
                } catch (Exception e) {
//                    System.out.println("-->> " + e.getLocalizedMessage());
//                    System.out.println("-->> " + e.getMessage());
//                    System.out.println("-->> " + e.fillInStackTrace());
                    Image img = (new Image(getClass().getResourceAsStream("/vista/img/sin_imagen.png")));
                    mapeoArticulo.put(art.getIdArticulo().toString(), img);
                } finally {
                }
//                jsonClienteAdd = jsonNew;
                articuloModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listArt.isEmpty()) {
            actualizandoTablaArticulos();
            txtBuscarCodProducto.requestFocus();
//            tableViewProducto.requestFocus();
//            tableViewProducto.getSelectionModel().select(0);
        }
    }

    private void actualizandoTablaArticulos() {
        articuloModalData = FXCollections.observableArrayList(articuloModalList);
        //columna Nombre ..................................................
        tableColumnArticuloModal.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        tableColumnArticuloModal.setCellFactory();
//        tableColumnArticuloModal.setCellValueFactory(c -> new SimpleObjectProperty<ImageView>(new ImageView(mapeoArticulo.get(c.getValue().get("idArticulo").toString()))));
        tableColumnArticuloModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, ImageView>, ObservableValue<ImageView>>() {
            @Override
            public ObservableValue<ImageView> call(TableColumn.CellDataFeatures<JSONObject, ImageView> param) {
                Image image = mapeoArticulo.get(param.getValue().get("idArticulo").toString());
                ImageView iv = new ImageView(image);
                iv.setFitHeight(60);
                iv.setFitWidth(60);
                return new SimpleObjectProperty<ImageView>(iv);
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnCodigoModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigoModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("codArticulo").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnDescripcionModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcionModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("descripcion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnSeccioModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnSeccioModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("seccion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnPrecioModal.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnPrecioModal.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("precioMin").toString())));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableViewProducto.getColumns().add(tableColumnArticuloModal);
        //columna Ruc .................................................
        tableViewProducto.setItems(articuloModalData);
    }

    private void mensajeError(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    @FXML
    private void buttonAnhadirProductoAction(ActionEvent event) {
        seleccionarProd();
    }

    private void seleccionarProd() {
        if (tableViewProducto.getSelectionModel().getSelectedIndex() >= 0) {
            cerrarBusquedaProducto();
            JSONObject json = tableViewProducto.getSelectionModel().getSelectedItem();
            txtIdProducto.requestFocus();
            txtIdProducto.setText(json.get("idArticulo").toString());
            txtProducto.setText(json.get("descripcion").toString().toUpperCase());
        }
    }

    private void seleccionarGasto() {
        numValidator = new NumberValidator();
        if (tableViewGasto.getSelectionModel().getSelectedIndex() >= 0) {
            cerrarBusquedaGasto();
            JSONObject json = tableViewGasto.getSelectionModel().getSelectedItem();
//            txtIdProducto.requestFocus();
            txtIdDetalleGasto.setText(json.get("idDetalleGasto").toString());
            txtDescriGasto.setText(json.get("descripcion").toString().toUpperCase());
            txtMonto.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(json.get("costo").toString())));
            txtCantidad.setText(json.get("cantidad").toString());
        }
    }


    @FXML
    private void buttonBuscarClienteAction(ActionEvent event) {
        buscarProductoModal();
    }

    @FXML
    private void anchorPaneBuscarProductoKeyReleased(KeyEvent event) {
    }

    @FXML
    private void buttonAnhadirGastoAction(ActionEvent event) {
        seleccionarGasto();
    }

    @FXML
    private void btnBuscarGastoAction(ActionEvent event) {
        buscarGastos();
    }

    private void buscarGastos() {
        List<DetalleGasto> listDetalle = new ArrayList<>();
        if (cbProveedorGasto.getSelectionModel().getSelectedItem().toUpperCase().equalsIgnoreCase("SELECCIONE PROVEEDOR")) {
            listDetalle = detalleGastoDAO.listarTodosMayoresACero();
        } else {
            listDetalle = detalleGastoDAO.listarPorProveedorMayorAcero(cbProveedorGasto.getSelectionModel().getSelectedItem().toUpperCase());
        }
        JSONParser parser = new JSONParser();
        gastoModalList = new ArrayList<>();
//        List<Articulo> listArt = artDAO.listarPorSeccion(cbSeccionData1.getSelectionModel().getSelectedItem().toUpperCase());
        for (DetalleGasto art : listDetalle) {
            try {
                JSONObject json = new JSONObject();
                json.put("idDetalleGasto", art.getIdDetalleGasto());
                json.put("proveedor", art.getGastos().getEmpresa().toUpperCase());
                json.put("descripcion", art.getDescripcion());
                json.put("cantidad", art.getCantidad());
                json.put("costo", art.getCosto());
//                jsonClienteAdd = jsonNew;
                gastoModalList.add(json);
            } catch (Exception ex) {
                Utilidades.log.error("ParseException", ex.fillInStackTrace());
                mensajeError("No se ha podido agregar cliente");
            }
        }
        if (!listDetalle.isEmpty()) {
            actualizandoTablaGasto();
//            txtBuscarCodProducto.requestFocus();
        } else {
            gastoModalList = new ArrayList<>();
            tableViewGasto.getItems().clear();
        }
    }

    private void actualizandoTablaGasto() {
        gastoModalData = FXCollections.observableArrayList(gastoModalList);
        //columna Nombre ..................................................
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        columnProveedorGasto.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnProveedorGasto.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("proveedor").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        columnDescripcionGasto.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcionGasto.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("descripcion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        columnCantidadGasto.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadGasto.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("cantidad").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        columnCostoGasto.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCostoGasto.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("costo").toString())));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableViewProducto.getColumns().add(tableColumnArticuloModal);
        //columna Ruc .................................................
        tableViewGasto.setItems(gastoModalData);
    }

    private void agregarDetallePrecios() {
        numValidator = new NumberValidator();
        if (!txtIdDetalleGasto.getText().equals("") && !txtMonto.getText().equals("") && !txtCantidad.getText().equals("")) {
            DetalleGasto dg = detalleGastoDAO.getById(Long.parseLong(txtIdDetalleGasto.getText()));
            Gastos g = gastosDAO.getById(dg.getGastos().getIdGasto());
            try {
                BigDecimal bd = new BigDecimal(txtCantidad.getText());
                int res;
                res = bd.compareTo(dg.getCantidad());
                if (res == 1) {
                    mensajeAlerta("EL CAMPO CANTIDAD NO DEBE SER MAYOR A LA CANTIDAD DEL DETALLE COMPRADO");
                } else {
                    JSONObject json = new JSONObject();
                    json.put("fecha", g.getFecha().toString());
                    json.put("idDetalleGasto", dg.getIdDetalleGasto());
                    json.put("descripcion", dg.getDescripcion());
                    json.put("cantidad", bd.toString());
                    double costo = (float) Long.parseLong(numValidator.numberValidator(txtMonto.getText())) / dg.getCantidad().doubleValue();
//                    long cost = Math.round(costo);
                    double costTotal = Math.round(costo) * bd.doubleValue();

                    json.put("costo", numValidator.numberValidator(Math.round(costTotal) + ""));
                    preparacionList.add(json);
                    actualizandoTablaPreparacion();
                    DetalleGasto dgasto = detalleGastoDAO.getById(Long.parseLong(txtIdDetalleGasto.getText()));
                    BigDecimal result = dgasto.getCantidad().subtract(bd);
                    dgasto.setCantidad(result);
                    double dato = Math.round(costo) * result.doubleValue();
                    dgasto.setCosto(Math.round(dato));
                    detalleGastoDAO.actualizar(dgasto);

                    long monto = 0;
                    for (JSONObject jSONObject : preparacionList) {
                        monto += Long.parseLong(jSONObject.get("costo").toString());
                    }
                    labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(monto + "")));
                    txtDescriGasto.setText("");
                    txtIdDetalleGasto.setText("");
                    txtMonto.setText("");
                    txtCantidad.setText("");
                }
            } catch (Exception e) {
                mensajeAlerta("EL CAMPO CANTIDAD NO DEBE SER MAYOR A LA CANTIDAD DEL DETALLE COMPRADO");
            } finally {
            }
        } else {
            mensajeAlerta("LOS CAMPOS COMPRA, PRECIO Y CANTIDAD NO DEBE QUEDAR VACIO");
        }
    }

    private void actualizandoTablaPreparacion() {
//json.put("fecha", g.getFecha().toString());
//                    json.put("idDetalleGasto", dg.getIdDetalleGasto());
//                    json.put("descripcion", dg.getDescripcion());
//                    json.put("cantidad", bd.toString());
//                    json.put("costo", numValidator.numberValidator(txtMonto.getText()));
        preparacionData = FXCollections.observableArrayList(preparacionList);
        //columna Nombre ..................................................
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnFecha.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFecha.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("fecha").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnDescripcion.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcion.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("descripcion").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnCantidad.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidad.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(data.getValue().get("cantidad").toString());
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        tableColumnCosto.setStyle(
                "-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCosto.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data
            ) {
                String nombre = String.valueOf(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("costo").toString())));
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableViewProducto.getColumns().add(tableColumnArticuloModal);
        //columna Ruc .................................................
        tableViewPreparacion.setItems(preparacionData);
    }

    private void eliminarDetalle() {
//        json.put("fecha", g.getFecha().toString());
//        json.put("idDetalleGasto", dg.getIdDetalleGasto());
//        json.put("descripcion", dg.getDescripcion());
//        json.put("cantidad", bd.toString());
//        json.put("costo", numValidator.numberValidator(txtMonto.getText()));
        numValidator = new NumberValidator();
        int num = tableViewPreparacion.getSelectionModel().getSelectedIndex();
        JSONObject json = preparacionList.get(num);
        DetalleGasto dg = detalleGastoDAO.getById(Long.parseLong(json.get("idDetalleGasto").toString()));
        BigDecimal bd = new BigDecimal(json.get("cantidad").toString());
        BigDecimal result = dg.getCantidad().add(bd);
        dg.setCantidad(result);
        detalleGastoDAO.actualizar(dg);
        long resultado = Long.parseLong(numValidator.numberValidator(labelTotalGs.getText())) - Long.parseLong(numValidator.numberValidator(json.get("costo").toString()));
        labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(resultado + "")));

        preparacionList.remove(num);
        tableViewPreparacion.getItems().remove(num);
    }

    @FXML
    private void txtBuscarCodProductoKeyReleased(KeyEvent event) {
        buscarProductoModal();
    }

    @FXML
    private void txtBuscarDesriProductoKeyReleased(KeyEvent event) {
        buscarProductoModal();
    }

}
