package com.javafx.controllers.stock;

import com.javafx.controllers.login.*;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.javafx.util.Utilidades;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
public class MenuStockFXMLController extends BaseScreenController implements Initializable {

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TabPane tabPaneSeleccionMenu;
    @FXML
    private TitledPane titledPaneCaja;
    @FXML
    private AnchorPane anchorPaneCaja;
    @FXML
    private GridPane gridPaneCaja;
    private Tab tabCuponera;
    @FXML
    private TitledPane titledPaneCuponera;
    @FXML
    private AnchorPane anchorPaneCuponera;
    @FXML
    private GridPane gridPaneCuponera;
    @FXML
    private Button buttonCerrar;
    @FXML
    private Button btnCargarArticulo;
    private Tab tabArticulos;
    @FXML
    private Button btnCargarProveedor;
    @FXML
    private Button btnCargarTransferir;
    @FXML
    private Button buttonMatriz;
    @FXML
    private Label labelCuponera1;
    @FXML
    private Button buttonPedidoCompra;
    @FXML
    private Label labelCuponera11;
    @FXML
    private Button buttonOrdenCompra;
    @FXML
    private Label labelCuponera111;
    @FXML
    private Tab tabMantenimiento;
    @FXML
    private Tab tabSolcitudCompra;
    @FXML
    private Tab tabRecepcion;
    @FXML
    private TitledPane titledPaneCuponera1;
    @FXML
    private AnchorPane anchorPaneCuponera1;
    @FXML
    private GridPane gridPaneCuponera1;
    @FXML
    private Button buttonCuponera1;
    @FXML
    private Tab tabCompra;
    @FXML
    private TitledPane titledPaneCuponera11;
    @FXML
    private AnchorPane anchorPaneCuponera11;
    @FXML
    private GridPane gridPaneCuponera11;
    @FXML
    private Button btnCargarCompras1;
    @FXML
    private Tab tabDeposito;
    @FXML
    private TitledPane titledPaneCuponera111;
    @FXML
    private AnchorPane anchorPaneCuponera111;
    @FXML
    private GridPane gridPaneCuponera111;
    @FXML
    private Button btnConteo;
    @FXML
    private Label labelCuponera12;
    @FXML
    private Button btnCruce;
    @FXML
    private Label labelCuponera121;
    @FXML
    private Button btnNotaCred;
    @FXML
    private Label labelCuponera1211;
    @FXML
    private Button btnCargarSecciones;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void buttonCuponeraAction(ActionEvent event) {
        actividadRecepcion();
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (DatosEnCaja.getUsers() != null) {
            users = DatosEnCaja.getUsers();
        }
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void actividadCargaArticulo() {
        this.sc.loadScreen("/vista/stock/ArticulosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadCargarCruce() {
        this.sc.loadScreen("/vista/stock/CruceFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadCargarSecciones() {
        this.sc.loadScreen("/vista/stock/SeccionesFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadCargaProveedor() {
        this.sc.loadScreen("/vista/stock/proveedoresFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadCargaCompras() {
        this.sc.loadScreen("/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadMatriz() {
        this.sc.loadScreen("/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadPedidoCompra() {
        this.sc.loadScreen("/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadBuscarOrden() {
        this.sc.loadScreen("/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadRealizarConteo() {
        this.sc.loadScreen("/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void actividadCargaTransferencia() {
        this.sc.loadScreen("/vista/stock/TransferenciaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, true);
    }

    private void cerrandoSesion() {
        Identity identity = new Identity();
        identity.usuarioLogueado(null);
        LoginFXMLController.setLlamarTask(false);
        this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/menuEsteticaFXML", 540, 359, true);
    }

    private void actividadRecepcion() {
//        if ("configuracion_descuento")) {//falta aún...
            Utilidades.setIdRangoLocal(999l);
            this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/menuStockFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (tabMantenimiento.isSelected()) {
                actividadCargaArticulo();
            } else if (tabSolcitudCompra.isSelected()) {
                actividadMatriz();
            } else if (tabRecepcion.isSelected()) {
                actividadRecepcion();
            } else if (tabCompra.isSelected()) {
                actividadCargaCompras();
            } else if (tabDeposito.isSelected()) {
                actividadCargaTransferencia();
            }
        }
        if (keyCode == event.getCode().F2) {
            if (tabMantenimiento.isSelected()) {
                actividadCargaProveedor();
            } else if (tabSolcitudCompra.isSelected()) {
                actividadPedidoCompra();
            } else if (tabRecepcion.isSelected()) {
                actividadRealizarConteo();
            }
        }
        if (keyCode == event.getCode().F3) {
            if (tabMantenimiento.isSelected()) {
                actividadCargarSecciones();
            } else if (tabSolcitudCompra.isSelected()) {
                actividadBuscarOrden();
            } else if (tabRecepcion.isSelected()) {
                actividadCargarCruce();
            }
        }
//        if (keyCode == event.getCode().F3) {
//        }
        if (keyCode == event.getCode().F4) {
//            if (tabArticulos.isSelected()) {
//                actividadCargaTransferencia();
//            } else 
            if (tabRecepcion.isSelected()) {
//                actividadCargarNotaCredito();
            }
        }
        if (keyCode == event.getCode().F5) {
        }
        if (keyCode == event.getCode().F6) {
        }
        if (keyCode == event.getCode().ESCAPE) {
//            setearDatos();
            cerrandoSesion();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    @FXML
    private void buttonCerrarAction(ActionEvent event) {
        cerrandoSesion();
    }

    @FXML
    private void btnCargarArticuloAction(ActionEvent event) {
//        if (tabArticulos.isSelected()) {
        actividadCargaArticulo();
//        } else if (tabCuponera.isSelected()) {
//            actividadRecepcion();
//        }
    }

    @FXML
    private void btnCargarProveedorAction(ActionEvent event) {
//        if (tabArticulos.isSelected()) {
        actividadCargaProveedor();
//        }
    }

    @FXML
    private void btnCargarComprasAction(ActionEvent event) {
        actividadCargaCompras();
    }

    @FXML
    private void btnCargarTransferirAction(ActionEvent event) {
//        if (tabArticulos.isSelected()) {
        actividadCargaTransferencia();
//        }
    }

    @FXML
    private void buttonMatrizAction(ActionEvent event) {
        actividadMatriz();
    }

    @FXML
    private void buttonPedidoCompraAction(ActionEvent event) {
        actividadPedidoCompra();
    }

    @FXML
    private void buttonOrdenCompraAction(ActionEvent event) {
        actividadBuscarOrden();
    }

    @FXML
    private void btnConteoAction(ActionEvent event) {
        actividadRealizarConteo();
    }

    @FXML
    private void btnCruceAction(ActionEvent event) {
        actividadCargarCruce();
    }

    @FXML
    private void btnNotaCredAction(ActionEvent event) {
//        actividadCargarNotaCredito();
    }

    @FXML
    private void btnCargarSeccionesAction(ActionEvent event) {
        actividadCargarSecciones();
    }

}
