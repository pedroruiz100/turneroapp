/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.Deposito;
import com.peluqueria.core.domain.Existencia;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.TransferenciaCab;
import com.peluqueria.core.domain.TransferenciaDet;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import com.peluqueria.dao.DepositoDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.Nf1TipoDAO;
import com.peluqueria.dao.Nf2SfamiliaDAO;
import com.peluqueria.dao.Nf3SseccionDAO;
import com.peluqueria.dao.Nf4Seccion1DAO;
import com.peluqueria.dao.Nf5Seccion2DAO;
import com.peluqueria.dao.Nf6Secnom6DAO;
import com.peluqueria.dao.Nf7Secnom7DAO;
import com.peluqueria.dao.TransferenciaCabDAO;
import com.peluqueria.dao.TransferenciaDetDAO;
import com.peluqueria.dao.impl.ArticuloNf1TipoDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf3SseccionDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf7Secnom7DAOImpl;
import com.peluqueria.dao.impl.FacturaCompraDetDAOImpl;
import com.peluqueria.dao.impl.Nf1TipoDAOImpl;
import com.peluqueria.dao.impl.Nf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.Nf3SseccionDAOImpl;
import com.peluqueria.dao.impl.Nf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.Nf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.Nf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.Nf7Secnom7DAOImpl;
import com.peluqueria.dto.ArticuloDTO;
import com.peluqueria.dto.IvaDTO;
import com.peluqueria.dto.MarcaDTO;
import com.peluqueria.dto.SeccionDTO;
import com.peluqueria.dto.SeccionSubDTO;
import com.peluqueria.dto.UnidadDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
@ScreenScoped
public class TransferenciaFXMLController extends BaseScreenController implements Initializable {

    private boolean alert;
    public static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private Date date = new Date();

    @Autowired
    ArticuloDAO artDAO;
    @Autowired
    DepositoDAO depositoDAO;
    @Autowired
    ExistenciaDAO existenciaDAO;
    @Autowired
    TransferenciaCabDAO transCabDAO;
    @Autowired
    TransferenciaDetDAO transDetDAO;

    Toaster toaster;

    public static ArticuloNf1TipoDAO anf1TipoDAO = new ArticuloNf1TipoDAOImpl();
    public static ArticuloNf2SfamiliaDAO anf2SfamiliaDAO = new ArticuloNf2SfamiliaDAOImpl();
    public static ArticuloNf3SseccionDAO anf3SeccionDAO = new ArticuloNf3SseccionDAOImpl();
    public static ArticuloNf4Seccion1DAO anf4SeccionDAO = new ArticuloNf4Seccion1DAOImpl();
    public static ArticuloNf5Seccion2DAO anf5SeccionDAO = new ArticuloNf5Seccion2DAOImpl();
    public static ArticuloNf6Secnom6DAO anf6SeccionDAO = new ArticuloNf6Secnom6DAOImpl();
    public static ArticuloNf7Secnom7DAO anf7SeccionDAO = new ArticuloNf7Secnom7DAOImpl();

    public static Nf1TipoDAO nf1TipoDAO = new Nf1TipoDAOImpl();
    public static Nf2SfamiliaDAO nf2SfamiliaDAO = new Nf2SfamiliaDAOImpl();
    public static Nf3SseccionDAO nf3SeccionDAO = new Nf3SseccionDAOImpl();
    public static Nf4Seccion1DAO nf4SeccionDAO = new Nf4Seccion1DAOImpl();
    public static Nf5Seccion2DAO nf5SeccionDAO = new Nf5Seccion2DAOImpl();
    public static Nf6Secnom6DAO nf6SeccionDAO = new Nf6Secnom6DAOImpl();
    public static Nf7Secnom7DAO nf7SeccionDAO = new Nf7Secnom7DAOImpl();
    public static FacturaCompraDetDAO fcdDAO = new FacturaCompraDetDAOImpl();

    private List<JSONObject> articuloList = new ArrayList<>();
    private ObservableList<JSONObject> artDATA;

    private Timestamp timestamp;
    //campos númericos
    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    JSONParser parser = new JSONParser();
    JSONObject artBuscado = new JSONObject();
    //campos númericos
    JSONObject datos = new JSONObject();
    JSONObject users = new JSONObject();
    JSONObject fact = new JSONObject();
    @Autowired
    ManejoLocalDAO manejoDAO;
    ManejoLocal manejoLocal = new ManejoLocal();
    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";

    @FXML
    private Button btnSalir;
    @FXML
    private AnchorPane anchorPaneArticulos;
    @FXML
    private Pane secondPane;
    @FXML
    private Button buttonAnhadir;
    @FXML
    private TextField txtCodigo;
    @FXML
    private Label labelSupervisor111111111;
    @FXML
    private TextField txtDescripcion;
    @FXML
    private Label labelSupervisor1111111111;
    @FXML
    private Label labelSupervisor111221111;
    @FXML
    private TextField txtExistencia;
    @FXML
    private Label labelSupervisor11122111;
    @FXML
    private TextField txtCantidad;
    @FXML
    private Label labelSupervisor111221112;
    @FXML
    private TableView<JSONObject> tableViewCliente;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidad;
    @FXML
    private TableColumn<JSONObject, String> tableColumnObservacion;
    @FXML
    private Button btnAgregar;
    @FXML
    private ComboBox<String> chkOrigen;
    @FXML
    private ComboBox<String> chkDestino;
    @FXML
    private TextField txtObservacion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnExistencia;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private void btnEditarAction(ActionEvent event) {
//        actualizarArticulo();
    }

    private void btnGuardarAction(ActionEvent event) {
        registrarArticulo();
    }

    private void btnBuscarAction(ActionEvent event) {
//         busquedaArticulo();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAperturaEsteticaKeyReleased(KeyEvent event) {
//        keyPress(event);
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneArticulos.getChildren()
                .get(anchorPaneArticulos.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void cargandoInicial() {
        toaster = new Toaster();
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
//        listenTextField();
        alert = false;
        ubicandoContenedorSecundario();
        listenerCampos();
        cargarComponentes();
        articuloList = new ArrayList<>();
        btnAgregar.setDisable(true);
        chkOrigen.setValue("-SELECCIONE-");//();
        chkDestino.setValue("-SELECCIONE-");
//        busquedaArticulo();

    }

    private void volviendo() {
//        if (StageSecond.getStageData().isShowing()) {
//            StageSecond.getStageData().close();
//        }
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/TransferenciaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
    }

    //Apartado 4 - AVISOS
    private void mensajeAlerta(String msj) {
//        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    //Apartado 5 - LISTEN
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtCodigo.isFocused()) {
                if (!chkOrigen.getSelectionModel().getSelectedItem().toUpperCase().equals(chkDestino.getSelectionModel().getSelectedItem().toUpperCase())) {
                    busquedaArticulo();
                } else {
                    mensajeAlerta("DEPOSITO DE ORIGEN Y DESTINO NO DEBEN SER IGUALES...");
                }

            }
        }
        if (keyCode == event.getCode().ESCAPE) {

        }
    }

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
//    private void listenTextField() {
//        txtMontoApertura.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtMontoApertura.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
//            String newV = "";
//            String oldV = "";
//            if (!newValue.contentEquals("")) {
//                newV = numValidator.numberValidator(newValue);
//                oldV = numValidator.numberValidator(oldValue);
//                long lim = -1l;
//                boolean limite = true;
//                if (!newV.contentEquals("")) {//límite del monto...
//                    if (newV.length() < 19) {
//                        lim = Long.valueOf(newV);
//                        if (lim > 0 && lim < 2147483647) {
//                            limite = false;
//                        }
//                    }
//                }
//                if (limite) {
//                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
//                        if (patternMonto) {
//                            if (oldV.length() != 0) {
//                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                            } else {
//                                param = oldValue;
//                            }
//                            patternMonto = false;
//                        } else {
//                            patternMonto = true;
//                            param = oldValue;
//                        }
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(param);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText("");
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                    if (!oldValue.contentEquals("")) {
//                        if (!newValue.contentEquals("Gs ")) {
//                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                                if (patternMonto) {
//                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
//                                    patternMonto = false;
//                                } else {
//                                    patternMonto = true;
//                                    param = oldValue;
//                                }
//                                Platform.runLater(() -> {
//                                    txtMontoApertura.setText(param);
//                                    txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                                });
//                            }
//                        } else {
//                            Platform.runLater(() -> {
//                                txtMontoApertura.setText("");
//                                txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtMontoApertura.setText(oldValue);
//                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                        });
//                    }
//                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                    if (patternMonto) {
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                    }
//                    Platform.runLater(() -> {
//                        txtMontoApertura.setText(param);
//                        txtMontoApertura.positionCaret(txtMontoApertura.getLength());
//                    });
//                }
//            }
//        });
//    }
//    Apartado 6 - BACKEND
    private void cargarComponentes() {
        cargarDepositos();
//        cargarEmpresa();
//        cargarFlia1();
//        cargarFlia2();
//        cargarFlia3();
//        cargarFlia4();
//        cargarFlia5();
//        cargarFlia6();
//        cargarFlia7();
    }

    private void busquedaArticulo() {
//        String inputLine;
//        JSONParser parser = new JSONParser();
//        JSONObject valor = new JSONObject();
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/articulo/" + txtCodigo.getText());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                valor = (JSONObject) parser.parse(inputLine);
//                artBuscado = valor;
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (IOException e) {
//            System.out.println(e.getLocalizedMessage());
//        } catch (org.json.simple.parser.ParseException e) {
//            e.printStackTrace();
//        } finally {
//        }
        Articulo art = artDAO.buscarCod(txtCodigo.getText());
        if (art != null) {
            txtDescripcion.setText(art.getDescripcion());
            long id = depositoDAO.getByDescripcion(chkOrigen.getSelectionModel().getSelectedItem());
            Existencia ex = existenciaDAO.listarPorDepositoArticulo(id, art.getIdArticulo());
            if (ex != null) {
                btnAgregar.setDisable(false);
                txtExistencia.setText(ex.getCantidad() + "");
                repeatFocus(txtCantidad);
            } else {
                mensajeAlerta("NO SE ENCUENTRA EXISTENCIAS DEL PRODUCTO... ");
                btnAgregar.setDisable(true);
            }
//            txtCostoMonedaExt.setText("0");
//            txtCostoLocal.setText(art.getCosto().toString());
//            txtPorcDescProve.setText("0");
//            txtPorcDesc.setText("0");
//            txtPorcIncremParana01 calculo;
//            txtPorcIncremParana02.setText("0");
//            PocIncremMayorista.setText("0");
//            txtPorcIncremVarios.setText("0");

//            txtIva.setText(art.getIva().getPoriva() + "");
//            txtSensor.setText("0");
//            txtOtrosGastos.setText("0");
//            txtCostoRealParana.setText("0");
//            txtPorcDescParana.setText("0");
//            txtPorcDescProveedor.setText("0");
//            txtRealProveedor.setText(art.getCosto().toString());
//
//            txtCostoMonExt.setText("0");
//            txtCostoOrigen.setText("0");
//            txtPorcentajeDesc.setText("0");
//            txtPorcAduanero.setText("0");
//
//            txtPrecioMin1.setText(art.getPrecioMin().toString());
//            txtPrecioMin2.setText(art.getPrecioMin().toString());
//            txtDescVenta.setText("0");
//            txtDescMaxMay.setText("0");
//            txtPrecioMayNuevo1.setText(art.getPrecioMay().toString());
//            txtPrecioMayNuevo2.setText(art.getPrecioMay().toString());
//            txtPrecioMinOutlet1.setText(art.getPrecioMin().toString());
//            txtPrecioMinOutlet2.setText(art.getPrecioMin().toString());
            //            txtAreaDesc.setText(art.getDescripcion());
            //            txtCostoMonedaExt.setText("0");
//            try {
//                if (art.getPermiteDesc()) {
//                    cbPermiteDesc.setValue("SI");
//                } else {
//                    cbPermiteDesc.setValue("NO");
//                }
//            } catch (Exception e) {
//                cbPermiteDesc.setValue("NO");
//            } finally {
//            }
//            try {
//                if (Boolean.parseBoolean(valor.get("importado").toString())) {
//                    chkProdImportado.setSelected(true);
//                } else {
//                    chkProdImportado.setSelected(false);
//                }
//            } catch (Exception e) {
//                chkProdImportado.setSelected(false);
//            } finally {
//            }
//            try {
//                if (Boolean.parseBoolean(valor.get("stockeable").toString())) {
//                    chkNoStockeable.setSelected(true);
//                } else {
//                    chkNoStockeable.setSelected(false);
//                }
//            } catch (Exception e) {
//                chkNoStockeable.setSelected(false);
//            } finally {
//            }
            calcularPorcentaje(art.getPrecioMin().toString());
//            verificarNivel1(art.getIdArticulo());
        } else {
            mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void cbFamiliaMouseKeyReleased(KeyEvent event) {
        mouseEventCbFamilia();
    }

    private void mouseEventCbFamilia() {
    }

    private void listenerCampos() {

    }

    private void limpiarNiveles(int val) {

    }

    private void registrarArticulo() {
        Articulo art = artDAO.buscarCod(ReplicaFacturaCompraFXMLController.getCodArt());
//        art.setPrecioMin(Long.parseLong(txtPrecioMin1.getText()));
        artDAO.actualizar(art);
        mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE...");
        volviendo();
    }

    private JSONObject cargarArticulo() {

//        JSONObject jsonArticulo = new JSONObject();
//        ArticuloDTO art = new ArticuloDTO();
//        art.setDescripcion(txtAreaDesc.getText());
//        art.setPrecioMay(Long.parseLong(txtCostoMonedaExt.getText()));
//        art.setPrecioMin(Long.parseLong(txtCostoMonedaExt.getText()));
//        Timestamp tsNow = new Timestamp(date.getTime());
//        Long timestampNOW = tsNow.getTime();
//        art.setFechaAlta(timestampNOW);
//        art.setFechaAlta(timestampNOW);
// jsonCabFactura.put("fechaMod", timestampEmision);
//        jsonCabFactura.put("usuAlta", );
//        jsonCabFactura.put("usuMod", Identity.getNomFun());
//        art.setUsuAlta(Identity.getNomFun());
//        art.setUsuMod(Identity.getNomFun());
//        art.setPermiteDesc(false);
////        art.setCodArticulo(Long.parseLong(txtCodigo.getText()));
//        art.setServicio(false);
//        art.setBajada(false);
//        if (chkProdImportado.isSelected()) {
//            art.setImportado(true);
//        } else {
//            art.setImportado(false);
//        }
//        if (chkNoStockeable.isSelected()) {
//            art.setStockeable(true);
//        } else {
//            art.setStockeable(false);
//        }
        IvaDTO ivaDTO = new IvaDTO();
        ivaDTO.setIdIva(1l);

        MarcaDTO marcaDTO = new MarcaDTO();
        marcaDTO.setIdMarca(0l);

        UnidadDTO uDTO = new UnidadDTO();
        uDTO.setIdUnidad(1l);

        SeccionSubDTO ssDTO = new SeccionSubDTO();
        ssDTO.setIdSeccionSub(0l);

        SeccionDTO sDTO = new SeccionDTO();
        sDTO.setIdSeccion(0l);

//        art.setIva(ivaDTO);
//        art.setMarca(marcaDTO);
//        art.setUnidad(uDTO);
//        art.setSeccionSub(ssDTO);
//        art.setSeccion(sDTO);
//        try {
//            return (JSONObject) parser.parse(gson.toJson(art));
//        } catch (ParseException ex) {
        return null;
//        }
    }

    public void limpiar() {
//        txtCodigo.setText("");
        chkOrigen.setValue("-SELECCIONE-");
        chkDestino.setValue("-SELECCIONE-");
        txtCodigo.setText("");
        txtDescripcion.setText("");
        txtExistencia.setText("");
        txtCantidad.setText("");
        txtObservacion.setText("");
        articuloList = new ArrayList<>();
        tableViewCliente.getItems().clear();
    }

    private void txtPrecioMin1KeyPress(KeyEvent event) {
        listenPrecioMin(event);
    }

    private void listenPrecioMin(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
//            txtPrecioMin1.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    calcularPorcentaje(newValue);
//                });
//            });
        }
//        else if (event.getCode().isLetterKey()) {
//            txtMontoSencillo.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!convertirEntero(txtMontoSencillo)) {
//                        txtMontoSencillo.setText("Gs 0");
//                    }
//                });
//            });
//        }
    }

    private void calcularPorcentaje(String value) {
        DecimalFormat df = new DecimalFormat("#.0");
//        double result = (Long.parseLong(value) * 100) / Long.parseLong(txtCostoLocal.getText());
//        double reult2 = result - 100;
//        txtPorcIncremParana01.setText(df.format(reult2));
    }

    @FXML
    private void buttonAnhadirAction(ActionEvent event) {
        procesarDatos();
    }

    @FXML
    private void txtCodigoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void txtCantidadKeyPress(KeyEvent event) {
    }

    @FXML
    private void btnAgregarAction(ActionEvent event) {
        agregarDetalle();
    }

    private void cargarDepositos() {
        chkOrigen.getItems().add("-SELECCIONE-");//();
        chkDestino.getItems().add("-SELECCIONE-");
        for (Deposito dep : depositoDAO.listar()) {
            chkOrigen.getItems().add(dep.getDescripcion());
            chkDestino.getItems().add(dep.getDescripcion());
        }

    }

    private void agregarDetalle() {
        if (!txtExistencia.getText().equals("") || !txtCantidad.getText().equals("") || !txtCantidad.getText().equals("0")) {
            if (!chkOrigen.getSelectionModel().getSelectedItem().toUpperCase().equals(chkDestino.getSelectionModel().getSelectedItem().toUpperCase())) {
                if (Long.parseLong(txtExistencia.getText()) >= Long.parseLong(txtCantidad.getText())) {
                    guardarCabecera();
//                    guardarDetalle();
                } else {
                    mensajeAlerta("LA CANTIDAD DEBE SER MENOR O IGUAL A LA EXISTENCIA...");
                }
            } else {
                mensajeAlerta("DEPOSITO DE ORIGEN Y DESTINO NO DEBEN SER IGUALES...");
            }
        } else {
            mensajeAlerta("EL CAMPO CANTIDAD NO DEBE QUEDAR VACIO");
        }
    }

    private void guardarCabecera() {
        Timestamp timeStamp = new Timestamp(System.currentTimeMillis());
        TransferenciaCab tc = new TransferenciaCab();
        tc.setFecha(timeStamp);
        tc.setUsuario(Identity.getNomFun());

        Deposito origen = new Deposito();
        long idDepoOrigen = depositoDAO.getByDescripcion(chkOrigen.getSelectionModel().getSelectedItem());
//        long idDepoOrigen = chkOrigen.getSelectionModel().getSelectedIndex();
        origen.setIdDeposito(idDepoOrigen);

        Deposito destino = new Deposito();
        long idDepoDestino = depositoDAO.getByDescripcion(chkDestino.getSelectionModel().getSelectedItem());
        destino.setIdDeposito(idDepoDestino);

        tc.setDepositoDestino(destino);
        tc.setDepositoOrigen(origen);

        TransferenciaCab transCab = transCabDAO.insertarObtenerObjeto(tc);

        guardarDetalle(transCab);
    }

//    private void guardarDetalle(TransferenciaCab transCab) {
//        for (Object object : col) {
//            a
//        }
//    }
    private void guardarDetalle(TransferenciaCab transCab) {
        TransferenciaDet td = new TransferenciaDet();
        Articulo art = artDAO.buscarCod(txtCodigo.getText());
        art.setIva(null);
        art.setMarca(null);
        art.setUnidad(null);
        art.setSeccionSub(null);
        art.setSeccion(null);
        art.setCancelacionProductos(null);
        art.setComboDets(null);
        art.setFacturaClienteDets(null);
        art.setNotaCreditoDets(null);
        art.setNotaRemisionDets(null);
        art.setServPendiente(null);
        art.setArticuloSrvComision(null);
        art.setArtPromoTemporada(null);
        art.setArticuloAuxCodNf(null);
        art.setArticuloNf1Tipo(null);
        art.setArticuloNf2Sfamilia(null);
        art.setArticuloNf3Sseccion(null);
        art.setArticuloNf4Seccion1(null);
        art.setArticuloNf5Seccion2(null);
        art.setArticuloNf6Secnom6(null);
        art.setArticuloNf7Secnom7(null);
        art.setDescuentoTarjetaCabArticulos(null);
        art.setArtPromoTemporadaObsequio(null);
        art.setExistencia(null);
        art.setFechaAlta(null);
        art.setFechaMod(null);

        td.setCantidad(txtCantidad.getText());
        td.setArticulo(art);
        td.setDescripcion(txtDescripcion.getText());
        td.setExistencia(txtExistencia.getText());
        td.setObservacion(txtObservacion.getText());
        transCab.setFecha(null);
        td.setTransferenciaCab(transCab);

        JSONObject detalle;
        try {
            detalle = (JSONObject) parser.parse(gson.toJson(td).toString());
            articuloList.add(detalle);
            actualizandoTablaArt();

            txtCodigo.setText("");
            txtDescripcion.setText("");
            txtExistencia.setText("");
            txtCantidad.setText("");
            txtObservacion.setText("");

            repeatFocus(txtCodigo);
        } catch (ParseException ex) {
            Utilidades.log.info("ex " + ex.getLocalizedMessage());
        }

    }

    private void actualizandoTablaArt() {
        artDATA = FXCollections.observableArrayList(articuloList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                    String apellido = String.valueOf(jsonArt.get("codArticulo"));
                    if (apellido.contentEquals("null")) {
                        apellido = "";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());
                } catch (ParseException ex) {
                    Logger.getLogger(TransferenciaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }

            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(data.getValue().get("articulo").toString());
                    String apellido = String.valueOf(jsonArt.get("descripcion"));
                    if (apellido.contentEquals("null")) {
                        apellido = "";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());
                } catch (ParseException ex) {
                    Logger.getLogger(TransferenciaFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        });
        tableColumnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("cantidad"));
                if (ruc.contentEquals("null") || ruc.contentEquals("")) {
                    ruc = "0";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnObservacion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnObservacion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("observacion"));
                if (ruc.contentEquals("null") || ruc.contentEquals("")) {
                    ruc = "-";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnExistencia.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnExistencia.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("existencia"));
                if (ruc.contentEquals("null") || ruc.contentEquals("")) {
                    ruc = "-";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        //columna Ruc .................................................
        tableViewCliente.setItems(artDATA);
//        if (!escucha) {
//            escucha();
//        }
    }

    private void procesarDatos() {
        int var = 0;
        for (JSONObject json : articuloList) {
            var++;
            TransferenciaDet transDet = gson.fromJson(json.toString(), TransferenciaDet.class);
            transDetDAO.insercionMasiva(transDet, var);

            Existencia exis = existenciaDAO.listarPorDepositoArticulo(transDet.getTransferenciaCab().getDepositoDestino().getIdDeposito(), transDet.getArticulo().getIdArticulo());
            if (exis == null) {
                Existencia existencia = new Existencia();

                Articulo art = transDet.getArticulo();
                art.setIva(null);
                art.setMarca(null);
                art.setUnidad(null);
                art.setSeccionSub(null);
                art.setSeccion(null);
                art.setCancelacionProductos(null);
                art.setComboDets(null);
                art.setFacturaClienteDets(null);
                art.setNotaCreditoDets(null);
                art.setNotaRemisionDets(null);
                art.setServPendiente(null);
                art.setArticuloSrvComision(null);
                art.setArtPromoTemporada(null);
                art.setArticuloAuxCodNf(null);
                art.setArticuloNf1Tipo(null);
                art.setArticuloNf2Sfamilia(null);
                art.setArticuloNf3Sseccion(null);
                art.setArticuloNf4Seccion1(null);
                art.setArticuloNf5Seccion2(null);
                art.setArticuloNf6Secnom6(null);
                art.setArticuloNf7Secnom7(null);
                art.setDescuentoTarjetaCabArticulos(null);
                art.setArtPromoTemporadaObsequio(null);
                art.setExistencia(null);
                art.setFechaAlta(null);
                art.setFechaMod(null);

                existencia.setArticulo(art);
                existencia.setCantidad(Long.parseLong(transDet.getCantidad()));
                Deposito dep = transDet.getTransferenciaCab().getDepositoDestino();
                existencia.setDeposito(dep);
                existenciaDAO.insertar(existencia);
            } else {
                int cantidad = Integer.parseInt(exis.getCantidad() + "") + Integer.parseInt(transDet.getCantidad());
                exis.setCantidad(Long.parseLong(cantidad + ""));
                existenciaDAO.actualizar(exis);
            }
            Existencia exisOrigen = existenciaDAO.listarPorDepositoArticulo(transDet.getTransferenciaCab().getDepositoOrigen().getIdDeposito(), transDet.getArticulo().getIdArticulo());
            int canti = Integer.parseInt(exisOrigen.getCantidad() + "") - Integer.parseInt(transDet.getCantidad());
            exisOrigen.setCantidad(Long.parseLong(canti + ""));
            existenciaDAO.actualizar(exisOrigen);
        }
        if (var == articuloList.size()) {
            mensajeAlerta("DATOS PROCESADOS CORRECTAMENTE");
            limpiar();
        }
    }
}
