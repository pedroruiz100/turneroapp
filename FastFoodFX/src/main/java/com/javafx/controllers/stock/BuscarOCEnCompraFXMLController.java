/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.MainApp;
import com.peluqueria.core.domain.FacturaCompraDet;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoCompraDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.stock.ReplicaFacturaCompraFXMLController.detalleArtList;
import static com.javafx.controllers.stock.ReplicaFacturaCompraFXMLController.getDetalleArtList;
import static com.javafx.controllers.stock.ReplicaFacturaCompraFXMLController.mapeoCheck;
import static com.javafx.controllers.stock.MatrizFXMLController.categoriasList;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.util.PATH;
import com.javafx.util.StageSecond;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.Pane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class BuscarOCEnCompraFXMLController extends BaseScreenController implements Initializable {

    static void cargarRucRazon(TextField txtProveedorDoc, TextField txtProveedor) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
    }

    static void cargarOc(String oc) {
        ordenCompra = oc;
    }

    static void cargarDatos(TextField txtProveedorDoc, TextField txtProveedor, TextField txtTimbrados) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
        txtTimbrado = txtTimbrados;
    }

    private static NumberValidator numValidator;

    //para evitar inconvenientes con el message alert enter y escape...
    public static String ordenCompra;
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private boolean exitoEditar;
    private static TextField txtRucCli;
    private static TextField txtRazonCli;
    private static TextField txtTimbrado;
    private static boolean clienteSi;
    public static String proveedor;
    private long idCliente;
    private int codCliente;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    private ObservableList<JSONObject> articuloDetData;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private ProveedorDAO proveedorDAO;

    @Autowired
    private PedidoCabDAO pedidoCabDAO;

    @Autowired
    private PedidoCompraDAO pedidoCompraDAO;

    @Autowired
    private PedidoDetDAO pedidoDetDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private FacturaCompraDetDAO factCompraDetDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente;
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    boolean estadoCheck = false;

    final KeyCombination altE = new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN);
    final KeyCombination altD = new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN);

    @FXML
    private Label labelClienteBuscar;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private TableView<JSONObject> tableViewCompra;
    private TableColumn<JSONObject, String> tableColumnFecha;
    private TableColumn<JSONObject, String> tableColumnOc;
    private TableColumn<JSONObject, String> tableColumnTipo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnTotal;
//    private TableColumn<JSONObject, String> tableColumnComprador;
    @FXML
    private TextField txtProveedor;
    @FXML
    private TextField txtNumOrden;
    @FXML
    private TextField txtOrdenCompra;
    @FXML
    private Button btnProveedor;
    private ChoiceBox<String> chkEmpresa;
    @FXML
    private TextField txtNomProveedor;
    @FXML
    private TextField txtRucProveedor;
    private TableColumn<JSONObject, String> tableColumnPlazo;
    @FXML
    private AnchorPane secondPane;
    @FXML
    private TableColumn<JSONObject, Boolean> tableColumnOpc;
    @FXML
    private Button buttonBuscar;
    @FXML
    private Button btnProcesar;
    @FXML
    private CheckBox chkCentral;
    @FXML
    private CheckBox chkCacique;
    @FXML
    private CheckBox chkSanLorenzo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnOrden;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidad;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCosto;
    @FXML
    private Pane paneImage121;
    @FXML
    private Label labelCajeroFunc211;
    @FXML
    private Pane paneImage1211;
    @FXML
    private Label labelCajeroFunc2111;
    @FXML
    private Button buttonBuscarArt;
    @FXML
    private TextField txtCodigo;
    @FXML
    private Label labelNombreClienteNuevo111;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
//        buscandoCliente(false);
    }

    private void buttonActualizarAction(ActionEvent event) {
        editandoCliente();
    }

    private void buttonBuscarClienteAction(ActionEvent event) {
        buscandoCliente();
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
//        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha();
        txtOrdenCompra.setText(ordenCompra);
//        chkEmpresa.getItems().addAll("-- SELECCIONE --", "CASA CENTRAL", "SAN LORENZO", "CACIQUE");
//        chkEmpresa.setValue("-- SELECCIONE --");
//        ubicandoContenedorSecundario();
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneCliente.getChildren()
                .get(anchorPaneCliente.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        clienteSi = true;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, true);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
    }

    private void volviendo() {
//        clienteSi = false;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {

    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            buscandoOC();
        }

        if (keyCode == event.getCode().F3) {
//            if (!buttonNuevo.isDisable()) {
//                creandoCliente();
//            }
        }
        if (keyCode == event.getCode().F2) {
            buscarArt();
        }
        if (keyCode == event.getCode().SPACE) {
            if (tableViewCompra.isFocused()) {
//                int posicion = tableViewCompra.getSelectionModel().getSelectedIndex();
//                if (posicion >= 0) {
////                        String ord = tableViewCompra.getSelectionModel()
////                                .getSelectedItem().get("orden") + "-" + tableViewCompra.getSelectionModel()
////                                .getSelectedItem().get("codigo");
//                    String ord = tableViewCompra.getItems().get(posicion)
//                            .get("orden") + "-" + tableViewCompra.getItems().get(posicion)
//                            .get("codArticulo");
//                    if (checkBox.isSelected()) {
//                        mapeoCheck.remove(ord);
//                        mapeoCheck.put(ord, true);
////                            System.out.println(ord + " | " + true);
//                        tableViewCompra.getSelectionModel().getSelectedItem().put("check", true);
//                    } else {
//                        mapeoCheck.remove(ord);
//                        mapeoCheck.put(ord, false);
////                            System.out.println(ord + " | " + false);
//                        tableViewCompra.getSelectionModel().getSelectedItem().put("check", false);
//                    }
//                }
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewCompra.requestFocus();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            }
        }
        if (altE.match(event)) {
            estadoCheck = true;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), true);
            }
            vistaJSONObjectArtDet();
        }
        if (altD.match(event)) {
            estadoCheck = true;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), false);
            }
            vistaJSONObjectArtDet();
        }
    }

    private void escucha() {
        escucha = true;
        tableViewCompra.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewCompra.getSelectionModel().getSelectedItem() != null) {

//                if (buttonAnhadir.isDisable()) {
//                    buttonAnhadir.setDisable(false);
//                }
                jsonSeleccionActual = newSelection;
                txtNomProveedor.setText(jsonSeleccionActual.get("proveedor").toString());
                txtNumOrden.setText(jsonSeleccionActual.get("oc").toString());
                //editar cliente, por si acaso...
            }
        });
    }
//LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void borrando() {
        resetParam();
        navegandoFormaPago();
    }

    public static void seteandoParam(JSONObject cliente) {
        jsonCliente = cliente;
    }

    public static void resetParam() {
        jsonCliente = null;
        clienteSi = false;
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ruc);
        return clienteJSONObjList;
    }

    private void buscandoCliente() {
        clienteList = new ArrayList<>();
        tableViewCompra.getItems().clear();
        String proveedor = "";
        String sucursal = "";
        String oc = "";
        if (!txtProveedor.getText().equals("")) {
            proveedor = BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString();
        }
        try {
            if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("-- SELECCIONE --")) {
                sucursal = chkEmpresa.getSelectionModel().getSelectedItem();
            }
        } catch (Exception e) {
        } finally {
        }
//        if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("")) {

//        }
        if (!txtOrdenCompra.getText().equals("")) {
            oc = txtOrdenCompra.getText();
        }
//        List<PedidoCab> lisPedido = pedidoCabDAO.filtrarPorProveedorSucursalOc(proveedor, sucursal, oc);
        List<PedidoCab> lisPedido = pedidoCabDAO.filtrarPorProveedorOc(proveedor, oc);
//        List<PedidoCab> lisPedido = pedidoCompra.filtrarPorProveedorOc(proveedor, oc);
//        PedidoCab lisPedido = pedidoCabDAO.listarPorOrden(oc);
        for (PedidoCab pedidoCab : lisPedido) {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("proveedor", pedidoCab.getProveedor().getDescripcion());
            jsonObj.put("idProveedor", pedidoCab.getProveedor().getIdProveedor());
            jsonObj.put("docProveedor", pedidoCab.getProveedor().getRuc());
            String date = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date(pedidoCab.getFecha().getTime()));
//                txtFechaPedido.setText(date);
            jsonObj.put("fecha", date);
            jsonObj.put("oc", pedidoCab.getOc());
            jsonObj.put("tipo", pedidoCab.getTipo());
            jsonObj.put("plazo", pedidoCab.getPlazo());
//            long total = pedidoDetDAO.recuperarTotal(pedidoCab.getIdPedidoCab());
            jsonObj.put("total", pedidoCab.getTotal());
            jsonObj.put("sucursal", sucursal);
            jsonObj.put("comprador", pedidoCab.getUsuario());
            jsonObj.put("idPedidoCab", pedidoCab.getIdPedidoCab());

            clienteList.add(jsonObj);
        }
        actualizandoTablaCliente();
    }

    private void buscarClientePorRucCi() {
//        String rucCliente = "";
//        JSONParser parser = new JSONParser();
//        try {
//            rucCliente = textFieldRucCliente.getText();
//            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
//            int contador = rucCliente.split("-", -1).length - 1;
//            Proveedor cliente = new Proveedor();
//            if (contador >= 1) {
//                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
////                cliente = cliDAO.listarPorRuc(rucCliente);
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getDescripcion() != null) {
////                    cliente.setFecNac(null);
//                    cliente.setFechaAlta(null);
//                    cliente.setFechaMod(null);
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            } else if (contador == 0) {
//                //Listar por CI en el caso que exista
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getIdProveedor() == null) {
//                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
//                    List<Proveedor> listClien = proveedorDAO.listarPorCIRevancha(rucCliente);
//                    if (listClien != null) {
//                        if (listClien.isEmpty()) {
//                            clienteList = new ArrayList<>();
//                        } else {
//                            clienteList = new ArrayList<>();
//                            for (int i = 0; i < listClien.size(); i++) {
//                                Proveedor cli = listClien.get(i);
////                                cli.setFecNac(null);
//                                cli.setFechaAlta(null);
//                                cli.setFechaMod(null);
//                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
//                                clienteList.add(jsonCli);
//                            }
//                        }
//                    } else {
//                        clienteList = new ArrayList<>();
//                    }
//                } else {
//                    clienteList = new ArrayList<>();
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            }
//            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
//        }
    }

    private void buscarAutomatico(JSONObject cliente) {
//        if (cliente != null) {
//            if (cliente.get("ruc") != null) {
//                textFieldRucCliente.setText(cliente.get("ruc").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//            textFieldNombreCliente.setText(cliente.get("nombre").toString());
//            buscandoCliente(true);
//        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
//    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE?", ok, cancel);
//            this.alert = true;
//            alert.showAndWait();
//            if (alert.getResult() == ok) {
//                alert.close();
//                JSONObject cliente = new JSONObject();
//                cliente = creandoJsonCliente();
//                int codCli = Integer.parseInt(cliente.get("codCliente").toString());
//                Cliente clie = cliDAO.getByCod(codCli);
//                if (clie == null) {
//                    long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    cliente.put("idCliente", idActual);
//                    exitoCrear = persistiendoPendientes(cliente);
//                    if (exitoCrear) {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        cliente.put("fechaAlta", null);
//                        cliente.put("fechaMod", null);
//                        ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
//                        cliDTO.setFechaAlta(timestamp);
//                        cliDTO.setFechaMod(timestamp);
//                        Cliente cli = Cliente.fromClienteDTO(cliDTO);
//                        Pais pais = new Pais();
//                        pais.setIdPais(0L);
//                        Departamento dpto = new Departamento();
//                        dpto.setIdDepartamento(0l);
//                        Ciudad ciu = new Ciudad();
//                        ciu.setIdCiudad(0l);
//                        Barrio barr = new Barrio();
//                        barr.setIdBarrio(0l);
//                        cli.setPais(pais);
//                        cli.setDepartamento(dpto);
//                        cli.setCiudad(ciu);
//                        cli.setBarrio(barr);
//                        try {
//                            cliDAO.insertar(cli);
//                            System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
//                        } catch (Exception e) {
//                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//                            System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
//                        }
//                        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.OK) {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        } else {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        }
//                    } else {
//                        Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.CLOSE) {
//                            alert2.close();
//                        }
//                    }
//                } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
//                }
//            } else {
//                alert.close();
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
//        return exitoCrear;
//    }
    //////CREATE, CLIENTE -> POST
    //////UPDATE, CLIENTE -> PUT
    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
            this.alert = true;
            alert.showAndWait();
            if (alert.getResult() == ok) {
                alert.close();
                JSONObject cliente = new JSONObject();
                cliente = editandoJsonCliente();
                exitoEditar = actualizarPendientes(cliente);
                if (exitoEditar) {
                    try {
                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                        JSONObject usuarioCajero = Identity.getUsuario();
                        JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                        String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                        cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
                        System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                    } catch (Exception e) {
                        Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                    }
                    Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
                    this.alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ButtonType.OK) {
                        alert2.close();
                        buscarAutomatico(cliente);
                    } else {
                        alert2.close();
                        buscarAutomatico(cliente);
                    }
                } else {
                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                    this.alert = true;
                    alert2.showAndWait();
                    if (alert2.getResult() == ButtonType.CLOSE) {
                        alert2.close();
                    }
                }
            } else {
                alert.close();
            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
        return exitoEditar;
    }
    //////UPDATE, CLIENTE -> PUT

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            this.alert = true;
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ruc) {
        JSONParser parser = new JSONParser();
        List<Proveedor> cliente = proveedorDAO.listarPorNomRuc(nom, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Proveedor cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - CLIENTE
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
//    private JSONObject creandoJsonCliente() {
//        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("nombre", textFieldNombreClienteNuevo.getText());
//        cliente.put("apellido", textFieldClienteApellidoNuevo.getText());
//        if (textFieldRucClienteNuevo.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            int count = StringUtils.countMatches(textFieldRucClienteNuevo.getText(), "-");
//            if (count == 0) {
//                if (textFieldRucClienteNuevo.getText().length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(textFieldRucClienteNuevo.getText()).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(textFieldRucClienteNuevo.getText()));
//                    }
//                }
//            } else if (count == 1) {
//                StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
//                String cedula = st.nextElement().toString();
//
//                if (cedula.length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(cedula).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(cedula));
//                    }
//                }
//            } else {
//                cliente.put("ruc", textFieldRucClienteNuevo.getText());
//            }
//        }
//        cliente.put("usuAlta", Identity.getNomFun());
//        cliente.put("fechaAlta", timestampJSON);
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
//        cliente.put("telefono", textFieldClienteTelefonoNuevo.getText());
//        cliente.put("segundaLateral", null);
//        cliente.put("primeraLateral", null);
//        cliente.put("nroLocal", null);
//        cliente.put("pais", null);
//        cliente.put("departamento", null);
//        cliente.put("ciudad", null);
//        cliente.put("barrio", null);
//        cliente.put("email", null);
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        String codCliente = "";
//        String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
//        if (arrayCod.length > 0) {
//            codCliente = arrayCod[0];
//        } else {
//            codCliente = textFieldRucClienteNuevo.getText();
//        }
//        cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
//        cliente.put("callePrincipal", null);
//        return cliente;
//    }
    //JSON CREANDO CLIENTE
    //JSON EDITANDO CLIENTE
    private JSONObject editandoJsonCliente() {
        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Cliente cli = cliDAO.getById(idCliente);
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("idCliente", idCliente);
//        cliente.put("nombre", textFieldNombreClienteEditar.getText());
//        cliente.put("apellido", textFieldClienteApellidoEditar.getText());
//        if (textFieldRucClienteEditar.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            cliente.put("ruc", textFieldRucClienteEditar.getText());
//        }
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("usuAlta", cli.getUsuAlta());
//
//        if (cli.getFechaAlta() != null) {
//            cliente.put("fechaAlta", cli.getFechaAlta().getTime());
//        } else {
//            cliente.put("fechaAlta", null);
//        }
//
//        cliente.put("telefono2", textFieldCelularClienteEditar.getText());
//        cliente.put("telefono", textFieldTelefonoClienteEditar.getText());
//        cliente.put("segundaLateral", cli.getSegundaLateral());
//        cliente.put("primeraLateral", cli.getPrimeraLateral());
//        cliente.put("nroLocal", cli.getNroLocal());
//
//        JSONObject jsonPais = new JSONObject();
//        jsonPais.put("idPais", cli.getPais().getIdPais());
//        cliente.put("pais", jsonPais);
//
//        JSONObject jsonDpto = new JSONObject();
//        jsonDpto.put("idDepartamento", cli.getDepartamento().getIdDepartamento());
//        cliente.put("departamento", jsonDpto);
//
//        JSONObject jsonCiudad = new JSONObject();
//        jsonCiudad.put("idCiudad", cli.getCiudad().getIdCiudad());
//        cliente.put("ciudad", jsonCiudad);
//
//        JSONObject jsonBarrio = new JSONObject();
//        jsonBarrio.put("idBarrio", cli.getBarrio().getIdBarrio());
//        cliente.put("barrio", jsonBarrio);
//        cliente.put("email", cli.getEmail());
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        cliente.put("codCliente", this.codCliente);
//        cliente.put("callePrincipal", cli.getCallePrincipal());
//        return cliente;
        return null;
    }
    //JSON EDITANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente() {
        clienteData = FXCollections.observableArrayList(clienteList);
        numVal = new NumberValidator();
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
//        tableColumnProveedor.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnProveedor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String apellido = String.valueOf(data.getValue().get("proveedor"));
//                if (apellido.contentEquals("null")) {
//                    apellido = "";
//                }
//                return new ReadOnlyStringWrapper(apellido.toUpperCase());
//            }
//        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnFecha.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFecha.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("fecha"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnOc.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnOc.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("oc"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("tipo"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnPlazo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnPlazo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("plazo"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        tableColumnTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("total"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
//                return new ReadOnlyStringWrapper(ruc);
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(numVal.numberValidator(ruc + ""))));
            }
        });
//        tableColumnComprador.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnComprador.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String ruc = String.valueOf(data.getValue().get("comprador"));
//                if (ruc.contentEquals("null")) {
//                    ruc = "";
//                }
//                return new ReadOnlyStringWrapper(ruc);
//            }
//        });
        //columna Ruc .................................................
        tableViewCompra.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

    @FXML
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    private void buttonNroEntradaAction(ActionEvent event) {
        if (tableViewCompra.getSelectionModel().getSelectedIndex() >= 0) {
            JSONObject jsonDatos = new JSONObject();
            jsonDatos.put("idProveedor", tableViewCompra.getSelectionModel().getSelectedItem().get("idProveedor").toString());
            BuscarProveedorFXMLController.seteandoParam(jsonDatos);
            RecepcionFXMLController.cargarRecepcion(tableViewCompra.getSelectionModel().getSelectedItem().get("docProveedor").toString(), tableViewCompra.getSelectionModel().getSelectedItem().get("proveedor").toString(),
                    tableViewCompra.getSelectionModel().getSelectedItem().get("sucursal").toString(), tableViewCompra.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()
            );
            this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA AGREGAR NUMERO DE ENTRADA...");
        }
    }

    private void buttonImprimirAction(ActionEvent event) {
        exportar();
    }

    private void buscarProveedor() {
        BuscarProveedorFXMLController.cargarRucRazon(txtRucProveedor, txtProveedor);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void exportar() {
        if (chkEmpresa.getSelectionModel().getSelectedItem().equalsIgnoreCase("-- SELECCIONE --")) {
            mensajeAlerta("DEBE SELECCIONAR UNA SUCURSAL");
        } else {
            if (tableViewCompra.getSelectionModel().getSelectedIndex() >= 0) {
                JSONArray jsonFacturas = recuperarDatos();

                if (jsonFacturas.isEmpty()) {
                    Date dates = new Date();
                    Timestamp ts = new Timestamp(dates.getTime());
                    String fechaArray[] = ts.toString().split(" ");
                    String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                    informandoVentas("{\"ventas\": [{\"orden\":\"N/A\",\"codigo\":\"N/A\",\"montoFactura\":0,"
                            + "\"descripcion\":\"N/A\",\"recepcion\":\"\",\"conteo\":\"\",\"observacion\":\"\",\"porcDescTarjeta\":\"0\","
                            + "\"fiel\":0,\"porcDescFiel\":\"0\",\"func\":0,\"porcDescFunc\":\"0\","
                            + "\"promoArt\":0,\"porcDescPromoArt\":\"0\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"promoSec\":0,\"porcDescPromoSec\":\"0\"}]}");

                } else {
                    JSONArray array = jsonFacturas;
                    informandoVentas("{\"ventas\": " + array + "}");
                }
                this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            } else {
                mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA IMPRIMIR LA ORDEN...");
            }
        }
    }

    private JSONArray recuperarDatos() {
//        , "CASA CENTRAL", "SAN LORENZO", "CACIQUE"
        JSONArray array = new JSONArray();

        List<PedidoCompra> listPedidoCompra = pedidoCompraDAO.listarPorCabecera(Long.parseLong(tableViewCompra.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()));
        if (listPedidoCompra.size() > 0) {
            int ord = 0;
            for (PedidoCompra pedidoCompra : listPedidoCompra) {
                long cantidad = 0;
                if (chkEmpresa.getSelectionModel().getSelectedItem().equalsIgnoreCase("CASA CENTRAL")) {
                    cantidad = cantidad = pedidoCompra.getCantCc();
                } else if (chkEmpresa.getSelectionModel().getSelectedItem().equalsIgnoreCase("SAN LORENZO")) {
                    cantidad = cantidad = pedidoCompra.getCantSl();
                } else {
                    cantidad = cantidad = pedidoCompra.getCantSc();
                }
                if (cantidad != 0) {
                    ord++;
                    JSONObject json = new JSONObject();
                    json.put("orden", ord);
                    json.put("codigo", pedidoCompra.getArticulo().getCodArticulo());
                    json.put("descripcion", pedidoCompra.getDescripcion());
                    json.put("recepcion", "");
                    json.put("conteo", "");
                    json.put("observacion", "");
                    json.put("oc", tableViewCompra.getSelectionModel().getSelectedItem().get("oc").toString());

                    proveedor = pedidoCabDAO.getById(pedidoCompra.getPedidoCab().getIdPedidoCab()).getProveedor().getDescripcion();
                    Date date = new Date();
                    Timestamp ts = new Timestamp(date.getTime());
                    String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
                    StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
                    String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
                    json.put("subRepNomFun", Identity.getNomFun());
                    json.put("subRepTimestamp", subRepTimestamp);
                    json.put("sucursal", chkEmpresa.getSelectionModel().getSelectedItem());
                    array.add(json);
                }
            }
        }
        return array;
    }

    private void informandoVentas(String jsonString) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_CONTEO.jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_CONTEO.pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
//            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
//            pSQL.put("nroFactura", txtNUmFactura);
            // CABECERA - SUB REPORTE
//            Sucursal suc = sucDAO.consultandoSuc(1L);
            pSQL.put("proveedor", proveedor);
            pSQL.put("empresa", "PARANA FUNCIONAL S.A.");
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
//            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            Date date = new Date();
//            Timestamp ts = new Timestamp(date.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String horaArray[] = ts.toString().split(".");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + horaArray[0];
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            pSQL.put("subRepTimestamp", subRepTimestamp);
            pSQL.put("subRepPathLogo", subImg);
            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }

    @FXML
    private void buttonBuscarAction(ActionEvent event) {
        buscandoOC();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        mantenerSeleccionados();
    }

    private void buscandoOC() {
        int dato = 0;
        if (chkCentral.isSelected()) {
            dato++;
        }
        if (chkCacique.isSelected()) {
            dato++;
        }
        if (chkSanLorenzo.isSelected()) {
            dato++;
        }
        if (dato == 1) {
            int orden = 0;
            PedidoCab pc = pedidoCabDAO.listarPorOrden(txtOrdenCompra.getText());
            List<PedidoCompra> listPedido = pedidoCompraDAO.listarPorCabecera(pc.getIdPedidoCab());

//            List<JSONObject> array = getDetalleArtList();
            detalleArtList = new ArrayList<>();
            tableViewCompra.getItems().clear();
            mapeoCheck = new HashMap();
            orden = 0;

            for (PedidoCompra pd : listPedido) {
                long cantidad = 0;
                if (chkCentral.isSelected()) {
                    cantidad = pd.getCantCc();
                } else if (chkCacique.isSelected()) {
                    cantidad = pd.getCantSc();
                } else if (chkSanLorenzo.isSelected()) {
                    cantidad = pd.getCantSl();
                }

                if (cantidad != 0) {
                    JSONObject jsonDetalle = new JSONObject();
                    JSONObject jsonRecep = new JSONObject();
                    jsonRecep.put("idPedidoCab", pc.getIdPedidoCab());

                    JSONObject jsonArt = new JSONObject();
                    jsonArt.put("idArticulo", pd.getArticulo().getIdArticulo());

                    jsonDetalle.put("pedidoCab", jsonRecep);
                    jsonDetalle.put("articulo", jsonArt);

                    jsonDetalle.put("cantidad", cantidad);
                    jsonDetalle.put("precio", pd.getPrecio());
                    jsonDetalle.put("total", (pd.getPrecio() * cantidad));
                    jsonDetalle.put("descripcion", pd.getDescripcion());

                    orden++;

                    FacturaCompraDet factCompra = factCompraDetDAO.getByLastCodigo(Long.parseLong(pd.getArticulo().getCodArticulo()));

                    jsonDetalle.put("orden", orden);
                    jsonDetalle.put("sec1", factCompra.getSec1());
                    jsonDetalle.put("sec2", factCompra.getSec2());
                    jsonDetalle.put("codArticulo", pd.getArticulo().getCodArticulo());
                    jsonDetalle.put("deposito", pd.getPedidoCab().getSucursal());
                    jsonDetalle.put("tipo", "MER");
                    jsonDetalle.put("medida", "UNIDAD");
                    jsonDetalle.put("contenido", "1");
                    jsonDetalle.put("existencia", "0");
                    jsonDetalle.put("descuento", pd.getDescuento());

                    detalleArtList.add(jsonDetalle);
                    vistaJSONObjectArtDet();
                }
            }
        } else if (dato == 0) {
            mensajeAlerta("DEBE SELECCIONAR UNA SUCURSAL PARA REALIZAR EL FILTRO DE BUSQUEDA...");
        } else if (dato > 1) {
            mensajeAlerta("DEBE SELECCIONAR UNA SOLA SUCURSAL PARA REALIZAR EL FILTRO DE BUSQUEDA...");
        }
    }

    public void vistaJSONObjectArtDet() {
        numValidator = new NumberValidator();
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        tableColumnOpc.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
                return new BooleanCell(tableViewCompra);
            }
        };
        tableColumnOpc.setCellValueFactory((data) -> {
            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
            try {
                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getBigInteger("codArticulo")).toString())) {
                    return new SimpleBooleanProperty(true);
                } else {
                    return new SimpleBooleanProperty(false);
                }
            } catch (Exception e) {
                return new SimpleBooleanProperty(false);
            } finally {
            }
        });
        tableColumnOpc.setCellFactory(booleanCellFactory);
        //columna Sección..............................................
        tableColumnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
//                return new SimpleStringProperty(data.getValue().get("tipo").toString());
//            }
//        });
        tableColumnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codArticulo").toString());
            }
        });
        tableColumnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        tableColumnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        tableColumnTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("total"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
//                return new ReadOnlyStringWrapper(ruc);
                return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(ruc + ""))));
            }
        });
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> (t.getTableView().getItems().get(
//                        t.getTablePosition().getRow())).put("cantidad", t.getNewValue())
//        );
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", t.getNewValue())
//        );
//        columnCantidad.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "cantidad";
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("cantidad", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", numValidator.numberValidator(t.getNewValue()));
//            }
//        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnMedida.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("medida").toString());
//            }
//        });
        tableColumnCosto.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        tableColumnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                long prec = (Long.parseLong(data.getValue().get("precio").toString()) * Long.parseLong(data.getValue().get("cantidad").toString()));
//                tot = prec + "";
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(prec + "")));
            }
        });
//        columnTotal.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "total";
//                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("total", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("total", numValidator.numberValidator(t.getNewValue()));
//                if (!numValidator.numberValidator(tot).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
//                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
//                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 745, 600, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                }
////                ReplicaFacturaCompraFXMLController.setCodArt(getDetalleArtList().get(t.getTablePosition().getRow()).get("codArticulo").toString());
////                sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 745, 600, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//            }
//        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
//        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("medida").toString());
//            }
//        });
//        columnContenido.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("contenido").toString());
//            }
//        });
//        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("descuento").toString());
//            }
//        });
        tableColumnCosto.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        tableColumnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                prec = data.getValue().get("precio").toString();
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("precio").toString()))));
//                return new SimpleStringProperty(data.getValue().get("precio").toString());
            }
        });
//        columnCosto.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "precio";
//                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("precio", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("precio", numValidator.numberValidator(t.getNewValue()));
//                if (!numValidator.numberValidator(prec).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
//                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
//                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 745, 600, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                }
//            }
//        });
//        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty("0");
//                } else {
//                    return new SimpleStringProperty(data.getValue().get("poriva").toString());
//                }
//            }
//        });
//        columnExenta.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("iva").toString()))));
////                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf("0"))));
////                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnGravada.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) != 0) {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("iva").toString())));
////                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
////                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnDeposito.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("deposito").toString());
//            }
//        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewCompra.setItems(articuloDetData);
        //**********************************************************************
    }

    @FXML
    private void buttonBuscarArtAction(ActionEvent event) {
        buscarArt();
    }

    private void buscarArt() {
        for (int i = 0; i < tableViewCompra.getItems().size(); i++) {
            JSONObject json = (JSONObject) tableViewCompra.getItems().get(i);
            if (json.get("codArticulo").toString().equalsIgnoreCase(txtCodigo.getText())) {
                tableViewCompra.requestFocus();
                tableViewCompra.getSelectionModel().select(i);
                break;
            }
        }
    }

    class BooleanCell extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCell(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
//                    table.getSelectionModel().select(getTableRow().getIndex());
//                    int posicion = tableViewCompra.getSelectionModel().getSelectedIndex();
                    int posicion = getTableRow().getIndex();
                    if (posicion >= 0) {
//                        String ord = tableViewCompra.getSelectionModel()
//                                .getSelectedItem().get("orden") + "-" + tableViewCompra.getSelectionModel()
//                                .getSelectedItem().get("codigo");
                        String ord = tableViewCompra.getItems().get(posicion)
                                .get("orden") + "-" + tableViewCompra.getItems().get(posicion)
                                .get("codArticulo");
                        if (checkBox.isSelected()) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
//                            System.out.println(ord + " | " + true);
                            tableViewCompra.getSelectionModel().getSelectedItem().put("check", true);
                        } else {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
//                            System.out.println(ord + " | " + false);
                            tableViewCompra.getSelectionModel().getSelectedItem().put("check", false);
                        }
                    }
                }
            });
//            this.setGraphic(checkBox);
//            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            this.setEditable(true);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    @FXML
    private void tableViewCompraKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void mantenerSeleccionados() {
//        if (!estadoCheck) {
//            long total = 0;
        suprimirSelecciones();
        volviendo();
//        ReplicaFacturaCompraFXMLController.
//        this.sc.loadScreenModal("/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/BuscarOCEnCompra.fxml", 888, 625, true);
//        Map mapping = new HashMap();
//        List<JSONObject> list = new ArrayList<>();
//        long exenta = 0;
//        long gravada10 = 0;
//        long gravada5 = 0;
//        for (JSONObject json : getDetalleArtList()) {
//            org.json.JSONObject jsonDat = new org.json.JSONObject(json);
//            mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), true);
//            switch (json.get("poriva").toString()) {
//                case "0":
//                    exenta += Long.parseLong(json.get("total").toString());
//                    break;
//                case "5":
//                    gravada5 += Long.parseLong(json.get("total").toString());
//                    break;
//                case "10":
//                    gravada10 += Long.parseLong(json.get("total").toString());
//                    break;
//                default:
//                    break;
//            }
//        }
//        try {
//            vistaJSONObjectArtDet();
//            long iva5 = Math.round(gravada5 / 21);
//            long grav5 = gravada5 - iva5;
//            long iva10 = Math.round(gravada10 / 11);
//            long grav10 = gravada10 - iva10;
//            long total = exenta + iva5 + grav5 + iva10 + grav10;
//
//            txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
//            txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
//            txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
//            txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
//            txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
//            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));
//
//        } catch (Exception e) {
//            System.out.println("-->> " + e.getLocalizedMessage());
//            System.out.println("-->> " + e.getMessage());
////            mensajeAlerta("DATOS CARGADOS INCORRECTAMENTE...");
////            detalleArtList = array;
////            vistaJSONObjectArtDet();
//        } finally {
//        }
////        int ord = 1;
////        for (JSONObject jSONObject : list) {
////            jSONObject.put("orden", ord);
////            categoriasList.add(jSONObject);
////            ord++;
////        }
//        tableViewCompra.getItems().clear();
//        vistaJSONObjectArtDet();
////        actualizandoTablaMatriz();
////        actualizarTablaCategoria();
////        chkLocal.setSelected(true);
////        cbSucursal.getSelectionModel().select("CASA CENTRAL");
////        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
////        } else {
////            suprimirSelecciones();
////        }
//        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    private void suprimirSelecciones() {
        JSONParser parser = new JSONParser();
//        int val = 0;
//        long tam = tableViewCompra.getItems().size();
        List aEliminar = new ArrayList();
        categoriasList = new ArrayList<>();
//        ObservableList<JSONObject> ol = tableViewCompra.getItems();
        for (JSONObject item : tableViewCompra.getItems()) {
//            org.json.JSONObject json = new org.json.JSONObject(item);
            JSONObject jsonArti = new JSONObject();
            try {
//                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
                boolean estado = Boolean.parseBoolean(mapeoCheck.get(item.get("orden").toString() + "-" + item.get("codArticulo").toString()).toString());
                if (estado == false) {
//                    System.out.println("-STATUS : " + tableViewCompra.getItems().remove(item));
//                    hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                    hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                    aEliminar.add(item);
                }
            } catch (Exception e) {
//                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                aEliminar.add(item);
            } finally {
            }
//            columnOpcion.setVisible(false);
        }

        for (int i = 0; i < aEliminar.size(); i++) {
            try {
                System.out.println("-> " + aEliminar.get(i));
                ReplicaFacturaCompraFXMLController.getDetalleArtList().remove(aEliminar.get(i));
//                matrizList.remove(aEliminar.get(i));a
                System.out.println("-STATUS : " + tableViewCompra.getItems().remove(aEliminar.get(i)));
            } catch (Exception e) {
            } finally {
            }
        }
//        actualizarTotales(false);
    }
}
