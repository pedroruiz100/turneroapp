/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.google.gson.GsonBuilder;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.StageSecond;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class BuscarProveedorFXMLController extends BaseScreenController implements Initializable {

    static void cargarRucRazon(TextField txtProveedorDoc, TextField txtProveedor) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
    }

    static void cargarDatos(TextField txtProveedorDoc, TextField txtProveedor, TextField txtTimbrados) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
        txtTimbrado = txtTimbrados;
    }

    static void cargarDatos2(TextField txtProveedorDoc, TextField txtProveedor, TextField txtTimbrados, TextField idProv) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
        txtTimbrado = txtTimbrados;
        idProveedor = idProv;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private boolean exitoEditar;
    private static TextField txtRucCli;
    private static TextField txtRazonCli;
    private static TextField txtTimbrado;
    private static TextField idProveedor;
    private static boolean clienteSi;
    private long idCliente;
    private int codCliente;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private ProveedorDAO proveedorDAO;
    @Autowired
    private RangoClienteDAO rangoCliDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente;
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelClienteBuscar;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private TextField textFieldNombreCliente;
    @FXML
    private TextField textFieldRucCliente;
    @FXML
    private Button buttonBuscarCliente;
    @FXML
    private TableView<JSONObject> tableViewCliente;
    @FXML
    private TableColumn<JSONObject, String> tableColumnRucCi;
    @FXML
    private Button buttonAnhadir;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private TextField textFieldNombreBuscar;
    @FXML
    private TextField textFieldRucCiBuscar;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNombre;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
//        buscandoCliente(false);
    }

    private void buttonActualizarAction(ActionEvent event) {
        editandoCliente();
    }

    @FXML
    private void buttonBuscarClienteAction(ActionEvent event) {
        buscandoCliente(true);
    }

    @FXML
    private void buttonAnhadirAction(ActionEvent event) {
        seteandoParam(jsonSeleccionActual);
        txtRucCli.setText(jsonSeleccionActual.get("ruc").toString());
        txtRazonCli.setText(jsonSeleccionActual.get("descripcion").toString());
        if (ScreensContoller.getFxml().contentEquals("/vista/stock/FacturaCompraFXML.fxml")) {
            txtTimbrado.setText(jsonSeleccionActual.get("timbrado").toString());
        } else if (ScreensContoller.getFxml().contentEquals("/vista/stock/MatrizFXML.fxml")) {
            volviendo();
        }
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
//        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha();
        //SETEANDO FIEL
//        if (ClienteFielFXMLController.getJsonClienteFiel() != null) {
//            if (ClienteFielFXMLController.getJsonClienteFiel().get("cipas") != null) {
//                textFieldRucCliente.setText(ClienteFielFXMLController.getJsonClienteFiel().get("cipas").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//        }
//        //SETEANDO FIEL
//
//        if (DatosEnCaja.getDatos() != null) {
//            datos = DatosEnCaja.getDatos();
//        }
//        if (DatosEnCaja.getUsers() != null) {
//            users = DatosEnCaja.getUsers();
//        }
//        if (DatosEnCaja.getFacturados() != null) {
//            fact = DatosEnCaja.getFacturados();
//        }
//        numVal = new NumberValidator();
//        buttonAnhadir.setDisable(true);
//        exitoCrear = false;
//        exitoEditar = false;
//        escucha = false;
//        alert = false;
//        clienteSi = false;
//        idCliente = 0l;
//        codCliente = -1;
//        listenerCampos();
//        validando();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        clienteSi = true;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, true);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
    }

    private void volviendo() {
        clienteSi = false;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {

    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            buscandoCliente(true);
        }

        if (keyCode == event.getCode().F3) {
//            if (!buttonNuevo.isDisable()) {
//                creandoCliente();
//            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;

            }
        }
        if (keyCode == event.getCode().ENTER) {
//            JSONParser parser = new JSONParser();
            if (alert) {
                alert = false;
            } else if (!buttonAnhadir.isDisable()) {
//                try {

                seteandoParam(jsonSeleccionActual);
                txtRucCli.setText(jsonSeleccionActual.get("ruc").toString());
                txtRazonCli.setText(jsonSeleccionActual.get("descripcion").toString());
                if (ScreensContoller.getFxml().contentEquals("/vista/stock/FacturaCompraFXML.fxml")) {
                    txtTimbrado.setText(jsonSeleccionActual.get("timbrado").toString());
                    volviendo();
                } else if (ScreensContoller.getFxml().contentEquals("/vista/stock/PedidoCompraFXML.fxml")) {
                    idProveedor.setText(jsonSeleccionActual.get("idProveedor").toString());
                    volviendo();
                } else /*(ScreensContoller.getFxml().contentEquals("/vista/stock/MatrizFXML.fxml"))*/ {
                    volviendo();
                }
//                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                    JSONObject cliente = new JSONObject();
//                    cliente.put("idCliente", idCliente);
//                    cab.put("cliente", cliente);
//                    fact.put("facturaClienteCab", cab);
//                    actualizarDatos();
//                navegandoFormaPago();
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//                }
            }
        }
    }

    private void escucha() {
        escucha = true;
        tableViewCliente.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewCliente.getSelectionModel().getSelectedItem() != null) {

                if (buttonAnhadir.isDisable()) {
                    buttonAnhadir.setDisable(false);
                }
                jsonSeleccionActual = newSelection;
                textFieldRucCiBuscar.setText(jsonSeleccionActual.get("ruc").toString());
                textFieldNombreBuscar.setText(jsonSeleccionActual.get("descripcion").toString());
                //editar cliente, por si acaso...
            } else {
                buttonAnhadir.setDisable(true);
                textFieldRucCiBuscar.setText("");
                textFieldNombreBuscar.setText("");
            }
        });
    }
//LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void borrando() {
        resetParam();
        navegandoFormaPago();
    }

    public static void seteandoParam(JSONObject cliente) {
        jsonCliente = cliente;
    }

    public static void resetParam() {
        jsonCliente = null;
        clienteSi = false;
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ruc);
        return clienteJSONObjList;
    }

    private void buscandoCliente(boolean efeuno) {
        String nom, ape, ruc;
        if (textFieldNombreCliente.getText().contentEquals("")) {
            nom = "null";
        } else {
            nom = textFieldNombreCliente.getText();
        }
        if (textFieldRucCliente.getText().contentEquals("")) {
            ruc = "null";
        } else {
            ruc = textFieldRucCliente.getText();
        }

        if (nom.equals("null") && !ruc.equals("null")) {
            buscarClientePorRucCi();
            actualizandoTablaCliente();
        } else {
            clienteList = jsonArrayCliente(nom, ruc);
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
            actualizandoTablaCliente();
        }
        if (!tableViewCliente.getItems().isEmpty() && efeuno) {
            tableViewCliente.requestFocus();
            tableViewCliente.getSelectionModel().select(0);
            tableViewCliente.getFocusModel().focus(0);
        } else if (!tableViewCliente.getItems().isEmpty() && !efeuno) {
            tableViewCliente.getSelectionModel().select(0);
            tableViewCliente.getFocusModel().focus(0);
        }
    }

    private void buscarClientePorRucCi() {
        String rucCliente = "";
        JSONParser parser = new JSONParser();
        try {
            rucCliente = textFieldRucCliente.getText();
            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
            int contador = rucCliente.split("-", -1).length - 1;
            Proveedor cliente = new Proveedor();
            if (contador >= 1) {
                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
                clienteList = new ArrayList<>();
//                cliente = cliDAO.listarPorRuc(rucCliente);
                cliente = proveedorDAO.listarPorRuc(rucCliente);
                if (cliente.getDescripcion() != null) {
//                    cliente.setFecNac(null);
                    cliente.setFechaAlta(null);
                    cliente.setFechaMod(null);
                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
                    clienteList.add(jsonCli);
                }
            } else if (contador == 0) {
                //Listar por CI en el caso que exista
                cliente = proveedorDAO.listarPorRuc(rucCliente);
                if (cliente.getIdProveedor() == null) {
                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
                    List<Proveedor> listClien = proveedorDAO.listarPorCIRevancha(rucCliente);
                    if (listClien != null) {
                        if (listClien.isEmpty()) {
                            clienteList = new ArrayList<>();
                        } else {
                            clienteList = new ArrayList<>();
                            for (int i = 0; i < listClien.size(); i++) {
                                Proveedor cli = listClien.get(i);
//                                cli.setFecNac(null);
                                cli.setFechaAlta(null);
                                cli.setFechaMod(null);
                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                                clienteList.add(jsonCli);
                            }
                        }
                    } else {
                        clienteList = new ArrayList<>();
                    }
                } else {
                    clienteList = new ArrayList<>();
                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
                    clienteList.add(jsonCli);
                }
            }
            if (clienteList.isEmpty()) {
                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
            }
        } catch (Exception e) {
            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
        }
    }

    private void buscarAutomatico(JSONObject cliente) {
        if (cliente != null) {
            if (cliente.get("ruc") != null) {
                textFieldRucCliente.setText(cliente.get("ruc").toString());
            } else {
                textFieldRucCliente.setText("");
            }
            textFieldNombreCliente.setText(cliente.get("nombre").toString());
            buscandoCliente(true);
        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
//    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE?", ok, cancel);
//            this.alert = true;
//            alert.showAndWait();
//            if (alert.getResult() == ok) {
//                alert.close();
//                JSONObject cliente = new JSONObject();
//                cliente = creandoJsonCliente();
//                int codCli = Integer.parseInt(cliente.get("codCliente").toString());
//                Cliente clie = cliDAO.getByCod(codCli);
//                if (clie == null) {
//                    long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    cliente.put("idCliente", idActual);
//                    exitoCrear = persistiendoPendientes(cliente);
//                    if (exitoCrear) {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        cliente.put("fechaAlta", null);
//                        cliente.put("fechaMod", null);
//                        ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
//                        cliDTO.setFechaAlta(timestamp);
//                        cliDTO.setFechaMod(timestamp);
//                        Cliente cli = Cliente.fromClienteDTO(cliDTO);
//                        Pais pais = new Pais();
//                        pais.setIdPais(0L);
//                        Departamento dpto = new Departamento();
//                        dpto.setIdDepartamento(0l);
//                        Ciudad ciu = new Ciudad();
//                        ciu.setIdCiudad(0l);
//                        Barrio barr = new Barrio();
//                        barr.setIdBarrio(0l);
//                        cli.setPais(pais);
//                        cli.setDepartamento(dpto);
//                        cli.setCiudad(ciu);
//                        cli.setBarrio(barr);
//                        try {
//                            cliDAO.insertar(cli);
//                            System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
//                        } catch (Exception e) {
//                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//                            System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
//                        }
//                        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.OK) {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        } else {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        }
//                    } else {
//                        Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.CLOSE) {
//                            alert2.close();
//                        }
//                    }
//                } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
//                }
//            } else {
//                alert.close();
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
//        return exitoCrear;
//    }
    //////CREATE, CLIENTE -> POST
    //////UPDATE, CLIENTE -> PUT
    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            JSONObject cliente = new JSONObject();
            cliente = editandoJsonCliente();
            exitoEditar = actualizarPendientes(cliente);
            if (exitoEditar) {
                try {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject usuarioCajero = Identity.getUsuario();
                    JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                    String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                    cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
                    System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                }
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.OK) {
                    alert2.close();
                    buscarAutomatico(cliente);
                } else {
                    alert2.close();
                    buscarAutomatico(cliente);
                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.CLOSE) {
                    alert2.close();
                }
            }
        } else {
            alert.close();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
        return exitoEditar;
    }
    //////UPDATE, CLIENTE -> PUT

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            this.alert = true;
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ruc) {
        JSONParser parser = new JSONParser();
        List<Proveedor> cliente = proveedorDAO.listarPorNomRuc(nom, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Proveedor cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - CLIENTE
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
//    private JSONObject creandoJsonCliente() {
//        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("nombre", textFieldNombreClienteNuevo.getText());
//        cliente.put("apellido", textFieldClienteApellidoNuevo.getText());
//        if (textFieldRucClienteNuevo.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            int count = StringUtils.countMatches(textFieldRucClienteNuevo.getText(), "-");
//            if (count == 0) {
//                if (textFieldRucClienteNuevo.getText().length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(textFieldRucClienteNuevo.getText()).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(textFieldRucClienteNuevo.getText()));
//                    }
//                }
//            } else if (count == 1) {
//                StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
//                String cedula = st.nextElement().toString();
//
//                if (cedula.length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(cedula).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(cedula));
//                    }
//                }
//            } else {
//                cliente.put("ruc", textFieldRucClienteNuevo.getText());
//            }
//        }
//        cliente.put("usuAlta", Identity.getNomFun());
//        cliente.put("fechaAlta", timestampJSON);
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
//        cliente.put("telefono", textFieldClienteTelefonoNuevo.getText());
//        cliente.put("segundaLateral", null);
//        cliente.put("primeraLateral", null);
//        cliente.put("nroLocal", null);
//        cliente.put("pais", null);
//        cliente.put("departamento", null);
//        cliente.put("ciudad", null);
//        cliente.put("barrio", null);
//        cliente.put("email", null);
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        String codCliente = "";
//        String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
//        if (arrayCod.length > 0) {
//            codCliente = arrayCod[0];
//        } else {
//            codCliente = textFieldRucClienteNuevo.getText();
//        }
//        cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
//        cliente.put("callePrincipal", null);
//        return cliente;
//    }
    //JSON CREANDO CLIENTE
    //JSON EDITANDO CLIENTE
    private JSONObject editandoJsonCliente() {
        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Cliente cli = cliDAO.getById(idCliente);
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("idCliente", idCliente);
//        cliente.put("nombre", textFieldNombreClienteEditar.getText());
//        cliente.put("apellido", textFieldClienteApellidoEditar.getText());
//        if (textFieldRucClienteEditar.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            cliente.put("ruc", textFieldRucClienteEditar.getText());
//        }
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("usuAlta", cli.getUsuAlta());
//
//        if (cli.getFechaAlta() != null) {
//            cliente.put("fechaAlta", cli.getFechaAlta().getTime());
//        } else {
//            cliente.put("fechaAlta", null);
//        }
//
//        cliente.put("telefono2", textFieldCelularClienteEditar.getText());
//        cliente.put("telefono", textFieldTelefonoClienteEditar.getText());
//        cliente.put("segundaLateral", cli.getSegundaLateral());
//        cliente.put("primeraLateral", cli.getPrimeraLateral());
//        cliente.put("nroLocal", cli.getNroLocal());
//
//        JSONObject jsonPais = new JSONObject();
//        jsonPais.put("idPais", cli.getPais().getIdPais());
//        cliente.put("pais", jsonPais);
//
//        JSONObject jsonDpto = new JSONObject();
//        jsonDpto.put("idDepartamento", cli.getDepartamento().getIdDepartamento());
//        cliente.put("departamento", jsonDpto);
//
//        JSONObject jsonCiudad = new JSONObject();
//        jsonCiudad.put("idCiudad", cli.getCiudad().getIdCiudad());
//        cliente.put("ciudad", jsonCiudad);
//
//        JSONObject jsonBarrio = new JSONObject();
//        jsonBarrio.put("idBarrio", cli.getBarrio().getIdBarrio());
//        cliente.put("barrio", jsonBarrio);
//        cliente.put("email", cli.getEmail());
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        cliente.put("codCliente", this.codCliente);
//        cliente.put("callePrincipal", cli.getCallePrincipal());
//        return cliente;
        return null;
    }
    //JSON EDITANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente() {
        clienteData = FXCollections.observableArrayList(clienteList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String apellido = String.valueOf(data.getValue().get("descripcion"));
                if (apellido.contentEquals("null")) {
                    apellido = "";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnRucCi.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnRucCi.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("ruc"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        //columna Ruc .................................................
        tableViewCliente.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

}
