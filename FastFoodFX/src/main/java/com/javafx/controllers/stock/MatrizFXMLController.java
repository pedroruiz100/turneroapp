package com.javafx.controllers.stock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.FileOutputStream;

import com.peluqueria.MainApp;
import com.javafx.controllers.caja.*;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.core.domain.Existencia;
import com.peluqueria.core.domain.FacturaCompraDet;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoDet;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.ArticuloCompraMatrizDAO;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import com.peluqueria.dao.ArticuloProveedorDAO;
import com.peluqueria.dao.ArticuloVentaMatrizDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.FacturaClienteCabDAO;
import com.peluqueria.dao.FacturaCompraCabDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoPedidoDAO;
import com.peluqueria.dao.RecepcionDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.impl.ArticuloNf1TipoDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf3SseccionDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf7Secnom7DAOImpl;
import com.peluqueria.dao.impl.FacturaCompraDetDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.RangoPedidoDAOImpl;
import com.peluqueria.dao.impl.RecepcionDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionParana;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.Identity;
import com.javafx.util.MensajeFinalVenta;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.peluqueria.core.domain.ArticuloProveedor;

/**
 * FXML Controller class
 *
 * @author
 */
@Controller
public class MatrizFXMLController extends BaseScreenController implements Initializable {

    public static void setTextFieldCompAnt(String aTextFieldCompAnt) {
        textFieldCompAnt = aTextFieldCompAnt;
    }

    public static boolean isActualizarDatosCabecera() {
        return actualizarDatosCabecera;
    }

    public static void setActualizarDatosCabecera(boolean actualizarDatosCabecera) {
        MatrizFXMLController.actualizarDatosCabecera = actualizarDatosCabecera;
    }

    public static List<JSONObject> getCotizacionList() {
        return cotizacionList;
    }

    public static int getPeso() {
        return peso;
    }

    public static int getReal() {
        return real;
    }

    public static int getDolar() {
        return dolar;
    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static Long getPrecioTotal() {
        return precioTotal;
    }

    public static JSONObject getCabFactura() {
        return cabFactura;
    }

    public static void setCabFactura(JSONObject aCabFactura) {
        cabFactura = aCabFactura;
    }

    public static boolean isCancelacionProd() {
        return cancelacionProd;
    }

    private static JSONObject cabFactura;
//    private static Stage stageData = new Stage();

    static void setCliente(String ruc, String nombre) {
        nomCli = nombre;
        rucCli = ruc;
    }

//    public static void setSecondStage(Stage secondStage) {
//        stageData = secondStage;
//        StageSecond.setStageData(secondStage);
//    }
    Image image;
    Toaster toaster;
    boolean enterEstado = false;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static boolean dato = false;
    private static boolean facturaVentaEstado = false;
    public static JSONObject facturaCabeceraSupr = new JSONObject();
    public static boolean actualizarDatosCabecera;
    public static Map mapArticulosEnPromo;
    public static ArrayList arrayListadoPromo;
    public static ArrayList arrayListadoPromoConDesc;
    public static Map mapPromoConDesc;
    public static long descPromo;
    public static int cantPromo;
    public static ArticuloNf1TipoDAO anf1TipoDAO = new ArticuloNf1TipoDAOImpl();
    public static ArticuloNf2SfamiliaDAO anf2SfamiliaDAO = new ArticuloNf2SfamiliaDAOImpl();
    public static ArticuloNf3SseccionDAO anf3SeccionDAO = new ArticuloNf3SseccionDAOImpl();
    public static ArticuloNf4Seccion1DAO anf4SeccionDAO = new ArticuloNf4Seccion1DAOImpl();
    public static ArticuloNf5Seccion2DAO anf5SeccionDAO = new ArticuloNf5Seccion2DAOImpl();
    public static ArticuloNf6Secnom6DAO anf6SeccionDAO = new ArticuloNf6Secnom6DAOImpl();
    public static ArticuloNf7Secnom7DAO anf7SeccionDAO = new ArticuloNf7Secnom7DAOImpl();
    public static FacturaCompraDetDAO fcdDAO = new FacturaCompraDetDAOImpl();

    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";

    Alert alertCerrarTurno = null;
    Alert alertArqueo = null;

    static HashMap<Long, JSONObject> hmGift;

    public static HashMap<Long, JSONObject> getHmGift() {
        return hmGift;
    }

    public static void setHmGift(HashMap<Long, JSONObject> hmGift) {
        MatrizFXMLController.hmGift = hmGift;
    }

    public JSONObject objArticulo;

    @Autowired
    private ArticuloDAO artDAO;
    @Autowired
    private ExistenciaDAO existenciaDAO;

    private List<JSONObject> depositoList = new ArrayList<>();

    private ObservableList<JSONObject> depositoData;

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;

    @Autowired
    private ArticuloNf4Seccion1DAO artNf4SeccionDAO;

    @Autowired
    private FacturaCompraDetDAO facCompraDetDAO;

    @Autowired
    private ArticuloProveedorDAO artProvDAO;

    @Autowired
    private ArticuloCompraMatrizDAO artCompraMatrizDAO;

    @Autowired
    private ArticuloVentaMatrizDAO artVentaMatrizDAO;

    @Autowired
    private FacturaCompraCabDAO facCompraCabDAO;

    @Autowired
    private FacturaClienteCabDAO facturaClienteCabDAO;

    @Autowired
    private PedidoCabDAO pedidoCabDAO;

    @Autowired
    private PedidoDetDAO pedidoDetDAO;

    Map mapeoNow = new HashMap();
    private static RangoPedidoDAO rangoPedidoDAO = new RangoPedidoDAOImpl();

    public static List<JSONObject> matrizList;
    private ObservableList<JSONObject> matrizData;
    private ObservableList<JSONObject> categoriaData;

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    private static RecepcionDAO recepcionDAO = new RecepcionDAOImpl();
    static JSONObject datos = new JSONObject();
    ManejoLocal manejoLocal = new ManejoLocal();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static String textFieldCompAnt;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static Long precioTotal;
    private static int peso;
    private static int real;
    private static int dolar;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    //primera inserción
    private boolean primeraInsercion;
    private static int orden;
    //primera inserción
    //lector de código
    private static String codBarra;
    private static String codDecimal;
    //lector de código
    private static NumberValidator numValidator;
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;
    JSONObject tipoCaja;
    public static List<JSONObject> detalleArtList;

    private static String[] columns = {"Orden", "Codigo", "Descripcion", "Seccion", "Ultima Fecha", "Precio", "Costo", "Md", "Compra CC",
        "Compra SL", "Compra SC", "Total", "Compra CCAN", "Compra SLAN", "Compra SCAN", "Total", "Venta CC", "Venta SL", "Venta SC", "Total",
        "Venta CCAN", "Venta SLAN", "Venta SCAN", "Total", "Exis CC", "Exis SL", "Exis SC", "Total", "Rep CC", "Rep SL", "Rep SC", "Precio", "Valorizado"};
    public static HashMap mapeoArticulos;
    public static List<JSONObject> categoriasList;
    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    //TABLE VIEW
    private ReentrantLock lock = new ReentrantLock();

    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;
    private static int idFact;
    public static boolean valorIngreso = false;
    private SimpleDateFormat formatador = new SimpleDateFormat("hh:mm:ss a");

    static String rucCli;
    static String nomCli;
    static String posicion;
    boolean estadoCheck = false;
    public static Map mapeoCheck = new HashMap();

    final KeyCombination altN = new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN);
    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);
    final KeyCombination altB = new KeyCodeCombination(KeyCode.B, KeyCombination.ALT_DOWN);
    final KeyCombination altS = new KeyCodeCombination(KeyCode.S, KeyCombination.ALT_DOWN);

    public static String codArt = "";
//    public static Map mapFecha = new HashMap();
    public static ArrayList<LocalDate> arrayFecha = new ArrayList<LocalDate>();
    public static String plazo = "";
    public static String sucursal = "";

    final KeyCombination altE = new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN);
    final KeyCombination altO = new KeyCodeCombination(KeyCode.O, KeyCombination.ALT_DOWN);
    final KeyCombination altD = new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN);
    final KeyCombination altG = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN);

    public static String getCodArt() {
        return codArt;
    }

    public static void setCodArt(String codArt) {
        MatrizFXMLController.codArt = codArt;
    }

//    final KeyCombination altI = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN);
    @FXML
    private AnchorPane anchorPaneFactura;
    @FXML
    private Label labelTotal11;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtCodVendedor;
    @FXML
    private Pane secondPane1;
    @FXML
    private TextField txtCodVendedor1;
    @FXML
    private Label labelSupervisor111111121;
    @FXML
    private Label labelSupervisor1112211;
    @FXML
    private Label labelSupervisor11122111111;
    @FXML
    private AnchorPane anchorPane3Sub11;
    @FXML
    private Button btnProveedor;
    @FXML
    private TextField txtProveedorDoc;
    @FXML
    private TextField txtProveedor;
    @FXML
    private Label labelSupervisor12;
    @FXML
    private Label labelSupervisor1112;
    @FXML
    private Label labelSupervisor111221;
    @FXML
    private Label labelSupervisor11121;
    @FXML
    private Label labelSupervisor111211;
    @FXML
    private Label labelSupervisor1221;
    @FXML
    private Label labelSupervisor12211;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtContenido;
    @FXML
    private TextField txtNroPedido;
    @FXML
    private TextField txtGasto;
    @FXML
    private TextField txtSec1;
    @FXML
    private Button btnSeccion;
    @FXML
    private TextField txtSec2;
    @FXML
    private Button btnNose;
    @FXML
    private Button btnCargarVtoProd;
    @FXML
    private Button btnBorrarDetalle;
    @FXML
    private Button btnBuscarOrdenCompra;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnBorrar;
    @FXML
    private Button btnNuevo;
    @FXML
    private CheckBox chkOtros;
    @FXML
    private TextField txtNroPedidoSis;
    @FXML
    private TextField txtEntregaInicial;
    @FXML
    private TextField txtCantCuota;
    @FXML
    private TextField txtCuota;
    @FXML
    private Button btnGenerar;
    @FXML
    private TableView<JSONObject> tableViewFactura;
    @FXML
    private TableColumn<JSONObject, String> columnOrden;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;

    public static Stage parentStage;
//    private TableColumn<JSONObject, String> columnIva;
//    private TableColumn<JSONObject, String> columnExenta;
//    private TableColumn<JSONObject, String> columnGravada;
    private TableColumn<JSONObject, String> columnPeso;
//    private TableColumn<JSONObject, String> columnTipo;
    private TableColumn<JSONObject, String> columnCantidad;
//    private TableColumn<JSONObject, String> columnMedida;
    private TableColumn<JSONObject, String> columnContenido;
//    private TableColumn<JSONObject, String> columnDescuento;
    @FXML
    private TableColumn<JSONObject, String> columnCosto;
//    private TableColumn<JSONObject, String> columnDeposito;
    @FXML
    private TextField txtProveedorDoc1;
    @FXML
    private RadioButton chkLocalEImportado;
    @FXML
    private RadioButton chkImportado;
    @FXML
    private RadioButton chkLocal;
    @FXML
    private Label labelSupervisor1112111;
    @FXML
    private DatePicker dpDesdeActual;
    @FXML
    private Label labelSupervisor11121111;
    @FXML
    private DatePicker dpHastaActual;
    @FXML
    private TableView<JSONObject> tableViewSeccion;
    @FXML
    private Button btnFiltrar;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo111;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo1111;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo1112;
    @FXML
    private CheckBox chkOtros1;
    @FXML
    private DatePicker dpDesdeAnterior;
    @FXML
    private Label labelSupervisor111211111;
    @FXML
    private DatePicker dpHastaAnterior;
    @FXML
    private CheckBox chkOtros11;
    @FXML
    private ChoiceBox<String> cbMonedaExtran;
    @FXML
    private TextField txtCotizacion;
    @FXML
    private Label labelSupervisor1112112;
    @FXML
    private TextField txtPlazo;
    @FXML
    private Label labelSupervisor11121121;
    @FXML
    private TextField txtFrecuencia;
    @FXML
    private Label labelSupervisor1112111111;
    @FXML
    private DatePicker dpEntrega;
    @FXML
    private Label labelSupervisor11121111111;
    @FXML
    private TextField txtHorario;
    @FXML
    private Label labelSupervisor11121111112;
    @FXML
    private DatePicker dpVisita;
    @FXML
    private ChoiceBox<String> cbSucursal;
    @FXML
    private Button btnCargaMatriz;
    @FXML
    private Button btnAplicar;
    @FXML
    private Button btnGenerarPedido;
    @FXML
    private Button btnBuscar;
    @FXML
    private Button btnImprimir;
    @FXML
    private TableView<JSONObject> tableViewDeposito;
    @FXML
    private Button btnSalir;
    @FXML
    private TableColumn<JSONObject, Boolean> columnOpcion;
    @FXML
    private TableColumn<JSONObject, String> columnFechaUlt;
    @FXML
    private TableColumn<JSONObject, String> columnPrecio;
    @FXML
    private TableColumn<JSONObject, String> columnMD;
    @FXML
    private TableColumn<JSONObject, String> columnCompraCentral;
    @FXML
    private TableColumn<JSONObject, String> columnCompraSanLo;
    @FXML
    private TableColumn<JSONObject, String> columnCompraCacique;
    @FXML
    private TableColumn<JSONObject, String> columnCompraTotal;
    @FXML
    private TableColumn<JSONObject, String> columnCompraCentralAnt;
    @FXML
    private TableColumn<JSONObject, String> columnCompraSanLoAnt;
    @FXML
    private TableColumn<JSONObject, String> columnCompraCaciqueAnt;
    @FXML
    private TableColumn<JSONObject, String> columnCompraTotalAnt;
    @FXML
    private TableColumn<JSONObject, String> columnVentaCentral;
    @FXML
    private TableColumn<JSONObject, String> columnVentaSanLo;
    @FXML
    private TableColumn<JSONObject, String> columnVentaCacique;
    @FXML
    private TableColumn<JSONObject, String> columnVentaTotal;
    @FXML
    private TableColumn<JSONObject, String> columnVentaCentralAnt;
    @FXML
    private TableColumn<JSONObject, String> columnVentaSanLoAnt;
    @FXML
    private TableColumn<JSONObject, String> columnVentaCaciqueAnt;
    @FXML
    private TableColumn<JSONObject, String> columnVentaTotalAnt;
    @FXML
    private TableColumn<JSONObject, String> columnExisCentral;
    @FXML
    private TableColumn<JSONObject, String> columnExisSanLo;
    @FXML
    private TableColumn<JSONObject, String> columnExisCacique;
    @FXML
    private TableColumn<JSONObject, String> columnExisTotal;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadRepoCentral;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadRepoSanLo;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadRepoCacique;
    @FXML
    private TableColumn<JSONObject, String> columnPrecioCosto;
    @FXML
    private TableColumn<JSONObject, String> columnValorizado;
    @FXML
    private TableColumn<JSONObject, String> columnSeccion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnNroSeccion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnSeccion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo;
    @FXML
    private ChoiceBox<String> cbImpresion;
    @FXML
    private Button btnSubir;
    @FXML
    private Button btnBajar;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    @SuppressWarnings("ConvertToStringSwitch")
    public void initialize(URL url, ResourceBundle rb) {
        chkLocal.setSelected(true);
        cbSucursal.getSelectionModel().select("CASA CENTRAL");
        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
        listenFactura();
        ubicandoContenedorSecundario();
        switch (ScreensContoller.getFxml()) {
            case "/vista/stock/ModCantidadReponerFXML.fxml": {
                actualizandoTablaMatriz(matrizList);
                actualizarTablaCategoria();
                cargarSucursal();
                cargarFechas();
                cargarImpresion();
                cargarMoneda();
                cbMonedaExtran.getSelectionModel().select(0);
                txtProveedor.setText(BuscarProveedorFXMLController.getJsonCliente().get("descripcion").toString());
//                arrayFecha.put("fechaDesde", dpDesdeActual.getValue());
//                mapFecha.put("fechaHasta", dpHastaActual.getValue());
//                mapFecha.put("fechaDesdeAn", dpDesdeAnterior.getValue());
//                mapFecha.put("fechaHastaAn", dpHastaAnterior.getValue());

                dpDesdeActual.setValue(arrayFecha.get(0));
                dpHastaActual.setValue(arrayFecha.get(1));
                dpDesdeAnterior.setValue(arrayFecha.get(2));
                dpHastaAnterior.setValue(arrayFecha.get(3));
                txtPlazo.setText(plazo);
                cbSucursal.setValue(sucursal);

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableViewFactura.requestFocus();
                        tableViewFactura.getSelectionModel().select(Integer.parseInt(posicion));
                        tableViewFactura.getFocusModel().focus(Integer.parseInt(posicion));
                        tableViewFactura.scrollTo(Integer.parseInt(posicion));
                    }
                });
                break;
            }
            default:
                toaster = new Toaster();
                detalleArtList = new ArrayList<>();
                depositoList = new ArrayList<>();
                orden = 0;
                ubicandoContenedorSecundario();
//                cargarTipoMovimiento();
                cargarSucursal();
                cargarDeposito();
                cargarTipo();
                cargarMedida();
                cargarMoneda();
                cbMonedaExtran.getSelectionModel().select(0);
                cargarSituacion();
                cargarImpresion();
                cargarClaseMovimiento();
                cargarFechas();
                cbImpresion.getSelectionModel().select(0);
                break;
        }
        tableViewFactura.setEditable(true);
        columnCantidadRepoCentral.setCellFactory(TextFieldTableCell.forTableColumn());
        columnCantidadRepoSanLo.setCellFactory(TextFieldTableCell.forTableColumn());
        columnCantidadRepoCacique.setCellFactory(TextFieldTableCell.forTableColumn());
        cargandoImagen();
//        cargandoInicial();
    }

    private void cargarTipoMovimiento() {
//        chkTipoMovimiento.getItems().addAll("CR", "CO");
    }

    private void cargarDeposito() {
//        chkDepDestino.getItems().addAll("DEP 01", "DEP 02", "DEP 03");
    }

    private void cargarTipo() {
//        chkTipo.getItems().addAll("MER", "SER", "DESC");
    }

    private void cargarMedida() {
//        chkMedida.getItems().addAll("UNIDAD", "KG");
    }

    private void cargarSucursal() {
//        chkEmpresa.getItems().addAll("CASA CENTRAL", "SAN LORENZO", "CACIQUE");
        cbSucursal.getItems().addAll("CASA CENTRAL", "SAN LORENZO", "CACIQUE");
    }

    private void cargarSituacion() {
//        chkSituacion.getItems().addAll("CONTRIBUYENTE", "OTROS");
    }

    private void cargarImpresion() {
        cbImpresion.getItems().addAll("--SELECCIONE IMPRESION--", "MATRIZ COMPRADOR", "MATRIZ VENDEDOR");
    }

    private void cargarClaseMovimiento() {
//        chkClaseMov.getItems().addAll("MER", "SER", "DESC");
    }

    private void btnCerrarAction(ActionEvent event) {
//        cerrandoCabecera();
    }

    private void btnRetiroDineroAction(ActionEvent event) {
        verificarMontoFacturado();
    }

    private void btnCerrarTurnoAction(ActionEvent event) {
        if (!verificandoCaidaFormaPago()) {
            resetTray();
            verificandoDatosCierre();
        }
    }

    @FXML
    private void anchorPaneFacturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void anchorPaneFacturaKeyPressed(KeyEvent event) {

    }

    private void textFieldCodKeyReleased(KeyEvent event) {
        keyPressTextCod(event);
    }

    private void cargandoInicial() {
//        switch (ScreensContoller.getFxml()) {
//            case "/vista/cajamay/ModCantidadMayFXML.fxml": {
//                actualizandoTablaMatriz();
//                break;
//            }
//            default:
//                break;
//        }

    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    static void iniciandoFactCab() {
        cabFactura = new JSONObject();
        cabFactura = creandoJsonFactCab();
        cancelacionProd = false;
        cancelacionProdPrimera = true;
    }

    private void listenFactura() {
        tableViewFactura.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                }
            }
        });

        tableViewFactura.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection != null) {
                depositoList = new ArrayList<>();
                String codigo = tableViewFactura.getSelectionModel().getSelectedItem().get("codigo").toString();
                Articulo art = artDAO.buscarCod(codigo);
                List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
                if (listExis.size() > 0) {
                    for (Existencia ex : listExis) {
//                try {
                        Articulo articu = new Articulo();
                        articu.setIdArticulo(art.getIdArticulo());

                        ex.setArticulo(articu);
//                    JSONObject objJSON = (JSONObject) parser.parse(gson.toJson(ex).toString());
                        JSONObject objJSON = new JSONObject();//(JSONObject) parser.parse(gson.toJson(ex).toString());
                        JSONObject objART = new JSONObject();
                        objART.put("idArticulo", art.getIdArticulo());

                        JSONObject objDEPO = new JSONObject();
                        objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
                        objDEPO.put("descripcion", ex.getDeposito().getDescripcion());

                        objJSON.put("idExistencia", ex.getIdExistencia());
                        objJSON.put("cantidad", ex.getCantidad());
                        objJSON.put("articulo", objART);
                        objJSON.put("deposito", objDEPO);

                        depositoList.add(objJSON);

                        actualizandoTablaDeposito();
//                } catch (ParseException ex1) {
//                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
                    }
                } else {
                    tableViewDeposito.getItems().clear();
                    depositoList = new ArrayList<>();
                }
            } else {
                tableViewDeposito.getItems().clear();
                depositoList = new ArrayList<>();
            }
        });

//        textFieldCant.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!newValue.contentEquals("")) {
//                try {
//                    if (GenericValidator.isDouble(newValue)) {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(newValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(oldValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    }
//                } catch (NumberFormatException e) {
//                    Platform.runLater(() -> {
//                        textFieldCant.setText(oldValue);
//                        textFieldCant.positionCaret(textFieldCant.getLength());
//                        if (!textFieldCant.getText().endsWith(".")) {
//                            if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                textFieldCant.setText("1");
//                                mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                            }
//                        }
//                    });
//                }
//            }
//        });
    }

    private void mensajeAlerta(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorArt(String msj) {
        toaster.mensajeGenericoError("Mensaje del Sistema", msj, true, 2);
    }

    private void cargandoDetalleManeraLocal() {
        try {
            resetMapeo();
            detalleArtList = new ArrayList<>();
            JSONParser parser = new JSONParser();
            JSONArray facturaDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            JSONObject facturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            cabFactura = new JSONObject();
            org.json.JSONObject jsonFact = new org.json.JSONObject(facturaCabecera);
            try {
                if (tableViewFactura.getItems().isEmpty()) {
                    if (!jsonFact.isNull("montoFactura")) {
                        facturaCabecera.remove("montoFactura");
                        fact.put("facturaClienteCab", facturaCabecera);
                    } else {
                        cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                    }
                } else {
                    cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                }
                valorIngreso = true;
            } catch (Exception e) {
                mensajeError2("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO");
                valorIngreso = false;
            }
            if (valorIngreso) {
                cabFactura.put("estadoFactura", facturaCabecera.get("estadoFactura").toString());
                cabFactura.put("nroActual", facturaCabecera.get("nroActual").toString());
                cabFactura.put("idFacturaClienteCab", facturaCabecera.get("idFacturaClienteCab").toString());
                cabFactura.put("nroFactura", facturaCabecera.get("nroFactura").toString());
                primeraInsercion = false;
                for (int i = 0; i < facturaDetalle.size(); i++) {
                    JSONObject detalleArticulo = (JSONObject) parser.parse(facturaDetalle.get(i).toString());
                    JSONObject articulo = (JSONObject) parser.parse(detalleArticulo.get("articulo").toString());
                    hashJsonArticulo.put((Long.parseLong(articulo.get("codArticulo").toString())), articulo);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long.parseLong(articulo.get("idArticulo").toString())), detalleArtList.lastIndexOf(detalleArticulo));
                    vistaJSONObjectArtDet();
                    cargandoCamposInterfaceLocal(detalleArticulo);
                    orden++;
                }
                CajaDeDatos.generandoNroComprobante();
                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                // primer trío
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                long nroActual = talos.getNroActual();
                JSONObject jsonFacturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

                File file = new File(PATH.PATH_NO_IMG);
                Image image = new Image(file.toURI().toString());
//                centerImage();
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    private static void resetMapeo() {
        hashJsonArticulo = new HashMap<>();
        hashJsonArtDet = new HashMap<>();
    }

    private void mensajeError2(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    public void vistaJSONObjectArtDet() {
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
//                return new SimpleStringProperty(data.getValue().get("tipo").toString());
//            }
//        });
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codArticulo").toString());
            }
        });
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnMedida.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("medida").toString());
//            }
//        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnContenido.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("contenido").toString());
            }
        });
//        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("descuento").toString());
//            }
//        });
        columnCosto.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("precio").toString());
            }
        });
//        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty("0");
//                } else {
//                    return new SimpleStringProperty(data.getValue().get("poriva").toString());
//                }
//            }
//        });
//        columnExenta.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnGravada.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) != 0) {
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnDeposito.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("deposito").toString());
//            }
//        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewFactura.setItems(articuloDetData);
        //**********************************************************************
    }

    private void cargandoCamposInterfaceLocal(JSONObject detalleArticulo) {

    }

    private void pagando() {
//        if ("factura_cerrar")) {
        org.json.JSONObject json = new org.json.JSONObject(fact);
        if (!json.isNull("facturaClienteCab")) {
            try {
                JSONObject factu = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                setCabFactura(factu);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }

        this.sc.loadScreenModal("/vista/caja/FormasDePagoFXML.fxml", 667, 501, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void mensajeError(String msj) {
//        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorAlertModal(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.ERROR, msj, ok);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
        }
    }

    private void verificandoDatosCierre() {

    }
//    private HashMap () {
//        HashMap valor = new HashMap();
//        valor.put("cajero", "nverificandoDatosCierreull");
//        int items = tableViewFactura.getItems().size();
//        if (items > 0) {
//            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
//        } else {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            alertCerrarTurno = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CERRAR TURNO COMO CAJERO?", ok, cancel);
//            alertCerrarTurno.showAndWait();
//            LoginFXMLController.setLlamarTask(false);
//            if (alertCerrarTurno.getResult() == ok) {
//                ButtonType okData = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                ButtonType cancelData = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                alertArqueo = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA REALIZAR EL ARQUEO?", okData, cancelData);
//                alertArqueo.showAndWait();
//                if (alertArqueo.getResult() == okData) {
//                    users = null;
//                    fact = null;
//                    DatosEnCaja.setUsers(null);
//                    actualizarDatos();
//                    LoginFXMLController.setLlamarTask(false);
//                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                    valor.put("cajero", "cambio");
//                    alertArqueo.close();
//                }
//            } else if (alertCerrarTurno.getResult() == cancel) {
//                alertCerrarTurno.close();
//            }
//            alertArqueo = null;
//            alertCerrarTurno = null;
//        }
//        return valor;
//    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void cancelarFactura() {

    }

    private void verificarMontoFacturado() {
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE NO HA SIDO CERRADA!");
        } else {
            if (datos.containsKey("montoFacturado")) {
                int montoFacturado = Integer.parseInt(datos.get("montoFacturado").toString());
                if (montoFacturado > 0) {
                    retirandoDinero();
                } else {
                    mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
                }
            } else {
                mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
            }
        }
    }

    private void retirandoDinero() {
//        if ("retiro_dinero")) {

        this.sc.loadScreenModal("/vista/caja/retiroDineroFXML.fxml", 529, 266, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
            boolean valor = false;
            if (!jsonDatos.isNull("ultimaFactura")) {

            }
            if (valor) {
                mensajeDetalle("Se ha detectado una incidencia, contáctese con el área de IT.", "Error 400");
            } else {
//                ClienteFielFXMLController.setJsonClienteFiel(null);
                if (!verificandoCaidaFormaPago()) {
                    if (!detalleArtList.isEmpty()) {
//                        VERIFICAR PROMO MAS COMPRAS MAS  AHORRAS
                        cargarArticulosOrdenar();
//                        FIN VERIFICAR PROMO MAS COMPRAS MAS AHORRAS //NUEVO
                        org.json.JSONObject json = new org.json.JSONObject(datos);
                        boolean formaPago = false;
                        if (!json.isNull("caida")) {
                            String caida = datos.get("caida").toString();
                            if (caida.equalsIgnoreCase("factura_venta")) {
                                formaPago = false;
                            } else {
                                formaPago = true;
                            }
                        }
                        if (formaPago) {
                            try {
                                JSONArray detalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
                                JSONObject cabecera = new JSONObject();
                                if (detalle.size() > 0) {
                                    JSONObject objDetalle = (JSONObject) parser.parse(detalle.get(0).toString());
                                    cabecera = (JSONObject) parser.parse(objDetalle.get("facturaClienteCab").toString());
                                } else {
                                    cabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                                }
                                cabFactura = cabecera;
                                new MensajeFinalVenta().cargandoInicial();
//                                this.sc.loadScreen("/vista/caja/mensajeFinalVentaFXML.fxml", 562, 288, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                            } catch (ParseException ex) {
                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                            }
                        } else {
                            if (DatosEnCaja.getDatos() != null) {
                                datos = DatosEnCaja.getDatos();
                            }
                            if (DatosEnCaja.getFacturados() != null) {
                                fact = DatosEnCaja.getFacturados();
                            }
                            if (!facturaCabeceraSupr.toString().equalsIgnoreCase("{}")) {
                                cabFactura = facturaCabeceraSupr;
                                fact.put("facturaClienteCab", facturaCabeceraSupr);
                                setActualizarDatosCabecera(true);
                                MatrizFXMLController.cancelacionProd = true;
                            }
                            //verificar que ingrese en forma de pago en el metodo PUT ya que cuando de cancela una factura va al POST de vuelta
                            //entonces impide que se actualice la CABECERA
                            pagando();
                            actualizandoCabFacturaLocalmente();
                        }
                        //FIN NUEVO
                    } else {
                        mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
                    }
                }
            }
        }
        if (keyCode == event.getCode().F4) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificarMontoFacturado();
                    actualizarDatos();
                }
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewFactura.requestFocus();
        }
        if (altE.match(event)) {
            estadoCheck = true;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codigo"), true);
            }
            actualizandoTablaMatriz(matrizList);
        }
        if (altD.match(event)) {
            estadoCheck = true;
//            long total = 0;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
//                Boolean.parseBoolean(mapeoCheck.get(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo")).toString());
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codigo"), false);
//                total += Math.round(Long.parseLong(json.get("precio").toString()) * Double.parseDouble(json.get("cantidad").toString()));
            }
            actualizandoTablaMatriz(matrizList);
        }
        if (altG.match(event)) {
            mantenerSeleccionados();
        }
        if (altO.match(event)) {
            List<JSONObject> listandoJSON = new ArrayList<>();
            if (tableViewSeccion.getSelectionModel().getSelectedIndex() >= 0) {
                List<JSONObject> listJSON = matrizList;

                detalleArtList = new ArrayList<>();
                for (JSONObject jSONObject : listJSON) {
//                String ord = jSONObject.get("orden").toString() + "-" + jSONObject.get("codigo").toString();
//                    if (jSONObject.get("seccion").toString().toUpperCase().equalsIgnoreCase(categoria.toUpperCase())) {
                    listandoJSON.add(jSONObject);
                }
            }
            actualizandoTablaMatriz(listandoJSON);
//            mantenerSeleccionados();
        }
//        if (keyCode
//                == event.getCode().F3) {
//            if (alert) {
//                alert = false;
//            } else if (detalleArtList.isEmpty()) {
//                mensajeAlerta("NO DISPONE DE ARTÍCULO ALGUNO PARA CANCELAR FACTURA.");
//            } else {
//
//                if (DatosEnCaja.getDatos() != null) {
//                    datos = DatosEnCaja.getDatos();
//                }
//                if (DatosEnCaja.getFacturados() != null) {
//                    fact = DatosEnCaja.getFacturados();
//                }
//                cancelarFactura();
//                actualizarDatos();
//            }
//        }
        if (keyCode
                == event.getCode().DELETE) {
            if (!verificandoCaidaFormaPago()) {
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    mensajeError("NO SE PUEDE ELIMINAR LOS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                } else if (alert) {
                    alert = false;
                } else if (detalleArtList.isEmpty()) {
                    mensajeAlerta("DEBE DISPONER COMO MÍNIMO UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                } else {
                    JSONObject productos = tableViewFactura.getSelectionModel().getSelectedItem();
                    if (productos == null) {
                        mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                    } else {
//                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA ELIMINAR EL ARTÍCULO " + productos.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
//                        alert1.showAndWait();
//                        if (alert1.getResult() == ok) {
                        try {
                            facturaCabeceraSupr = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                            MatrizFXMLController.setCabFactura(facturaCabeceraSupr);
//                                textFieldDescripcion.setText("");
                            this.alert = false;
                            Label labelCantidad = new Label();

                            this.sc.loadScreenModal("/vista/caja/CancelacionProductoFXML.fxml", 519, 275, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
//                        } else if (alert1.getResult() == cancel) {
//                            alert1.close();
//                        }
                        actualizarDatos();
                    }
                }
            }
        }
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificandoDatosCierre();
//                    String cajero = verificandoDatosCierre().get("cajero").toString();
//                    if (cajero.equalsIgnoreCase("arqueo")) {
//                        users = null;
//                        fact = null;
//                        DatosEnCaja.setUsers(null);
//                        datos.put("modSup", true);
//                        DatosEnCaja.setDatos(datos);
//                        actualizarDatosNuevo();
//                    }
                }
            }
        }
        if (keyCode == event.getCode().F1) {
            buscarArticuloEnTabla();
        }
        if (keyCode == event.getCode().F3) {
            imprimirNow();
        }
        if (keyCode == event.getCode().F4) {
            filtrando();
        }
        if (keyCode == event.getCode().F7) {
            cargarDetalle();
        }
        if (keyCode == event.getCode().F8) {
            mantenerSeleccionados();
        }
        if (keyCode == event.getCode().F9) {
            generarPedidos();
        }
        if (keyCode == event.getCode().ESCAPE) {
            this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        }

        if (altN.match(event)) {
            registrandoCliente();
        }

        if (altC.match(event)) {
            buscandoCliente();
        }
        if (altB.match(event)) {
            generarExcel();
        }
        if (altS.match(event)) {
            try {
                importando();
            } catch (IOException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
            } catch (InvalidFormatException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
            }
        }
        if (keyCode == event.getCode().F7) {
            cargarDetalle();
        }
//        if (keyCode == event.getCode().F10) {
//            mensajeDialog();
//        }

//        if (event.getCode()
//                .isDigitKey() || event.getCode().getName().contentEquals("Enter"btnSubirAction)) {
//            if (alert) {
//                alert = false;
//            } else {
//                lecturaCodBarra(event);
//            }
//        }
    }

    private void cargarArticulosOrdenar() {
        mapArticulosEnPromo = new HashMap();
        try {
//            int valor = 0;
            cantPromo = 0;
            int orden = 0;
            for (JSONObject jsonDetalle : detalleArtList) {
                orden++;
                long cantidad = Long.parseLong(String.valueOf(jsonDetalle.get("cantidad").toString()).replace(".0", ""));
                JSONObject jsonArt = (JSONObject) parser.parse(jsonDetalle.get("articulo").toString());
                if (LoginCajeroFXMLController.articuloPromo.containsKey(jsonArt.get("codArticulo"))) {
                    mapArticulosEnPromo.put(jsonArt.get("codArticulo") + "-" + cantidad + "-" + orden, Long.parseLong(String.valueOf(jsonDetalle.get("precio").toString())));
//                    valor++;
                    cantPromo += cantidad;
                }
            }
            if (cantPromo >= 4) {
                ordenarMapeo();

            }
        } catch (ParseException ex) {
            Logger.getLogger(MatrizFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ordenarMapeo() {
        Set<Map.Entry<String, Long>> set = mapArticulosEnPromo.entrySet();
        List<Map.Entry<String, Long>> list = new ArrayList<Map.Entry<String, Long>>(
                set);
        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
            public int compare(Map.Entry<String, Long> o2,
                    Map.Entry<String, Long> o1) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        int orden = 0;
        arrayListadoPromo = new ArrayList();
        for (Map.Entry<String, Long> entry : list) {

            StringTokenizer st = new StringTokenizer(entry.getKey(), "-");
            String cod = st.nextElement().toString();
            String cant = st.nextElement().toString();
            for (int i = 0; i < Integer.parseInt(cant); i++) {
                orden++;
                arrayListadoPromo.add(orden + "-" + cod + "-" + entry.getValue());
            }
        }

//        for (int i = 0; i < arrayListadoPromo.size(); i++) {
//            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
//            String ord = st.nextElement().toString();
//            String cod = st.nextElement().toString();
//            String precio = st.nextElement().toString();
//
//            System.out.println(ord + ") -->> " + cod + " - " + precio);
//        }
        System.out.println("------------------SIGUIENTE--------------------");
        int vueltaEnCiclo = 0;
        int cantVueltaEnCiclo = 1;

        double cantDesc = orden / 4;
        String str = String.valueOf(cantDesc);
        int cantVueltaTotalDesc = Integer.parseInt(str.substring(0, str.indexOf('.')));
        descPromo = 0;
        arrayListadoPromoConDesc = new ArrayList();
        mapPromoConDesc = new HashMap();

        for (int i = 0; i < arrayListadoPromo.size(); i++) {
            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
            String ord = st.nextElement().toString();
            String cod = st.nextElement().toString();
            String precio = st.nextElement().toString();

            vueltaEnCiclo++;

            if (cantVueltaTotalDesc >= cantVueltaEnCiclo) {
                switch (vueltaEnCiclo) {
                    case 1:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 50%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.5) + "-50");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.5);
                        break;
                    case 2:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 40%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.4) + "-40");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.4);
                        break;
                    case 3:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 30%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.3) + "-30");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.3);
                        break;
                    case 4:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 20%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.2) + "-20");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.2);
                        vueltaEnCiclo = 0;
                        cantVueltaEnCiclo++;
                        break;
//                default:
//                    break;
                }
            } else {
                System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 0%");
            }
        }
    }

    private void buscandoCliente() {
//        if ("cliente_caja")) {

        this.sc.loadScreenModal("/vista/caja/BuscarClienteFXML.fxml", 602, 87, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    private void registrandoCliente() {
//        if ("cliente_caja")) {

        this.sc.loadScreenModal("/vista/caja/NuevoClienteFXML.fxml", 519, 187, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    public static void resetParam() {
        precioTotal = 0l;
    }

    private void resetTray() {
        Toaster.quitandoMsj();
    }

    private void keyPressTextCod(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            keyPressEventos(event);
            if (!enterEstado) {
                enterEstado = false;
            } else {
                if (alertArqueo != null) {
                    if (alertArqueo.isShowing()) {
                        Utilidades.log.info("FUI A UN ARQUEO");
                    }
                }
                if (alertCerrarTurno != null) {
                    if (alertCerrarTurno.isShowing()) {
                        Utilidades.log.info("FUI A UN CIERRE DE TURNO");
                    }
                }
                datos.remove("exentaGlobal");
                //NUEVO
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    if (!this.alert) {
                        mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                    }
                } else {
                    JSONObject jsonCabecera = new JSONObject();
                    if (!json.isNull("sitio") && !facturaVentaEstado) {
                        try {
                            jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
                    }
//              FIN NUEVO
                }
                enterEstado = false;
            }
        } else if (event.getCode().isDigitKey()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
        } else if (event.getCode() == KeyCode.INSERT) {

        }
    }

    private void lecturaCodBarra(KeyEvent event) {
    }

    private void mensajeDetalle(String msj, String title) {
        ButtonType btnAcept = new ButtonType("Salir (ESC)", ButtonBar.ButtonData.OK_DONE);
        Alert alerta = new Alert(Alert.AlertType.INFORMATION, msj, btnAcept);
        alerta.setTitle(title);
        alerta.setHeaderText("Mensaje del Sistema!");
        DialogPane dialogPane = alerta.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialogInformation");
        alerta.showAndWait();
    }

    private boolean verificandoCaidaFormaPago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("energiaElectrica")) {
            mensajeAlerta("Esta Factura debe ser cancelada por problemas de caída de la Energía Eléctrica, ya que podrían contener datos corruptos");
            return true;
        } else {
            return false;
        }
    }

//    public static void actualizandoCabFacturaLocalmente() {
//        JSONParser parser = new JSONParser();
//        if (DatosEnCaja.getFacturados() != null) {
//            try {
//                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
//            } catch (ParseException ex) {
//                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//            }
//        }
//        org.json.JSONObject json = new org.json.JSONObject(datos)
//                datos.put("caida", "forma_pago");
//            }
//        }
//        if (!facturaVentaEstado) {
//            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
//                //OBTENER ID RANGO ACTUAL DE LA FACTURA
//                long idRangoFact = 0;
//                if (!json.isNull("idRangoFacturaActual")) {
//                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
//                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                        datos.put("idRangoFacturaActual", idRangoFact);
//                    } else {
//                        String rango = datos.get("idRangoFacturaActual").toString();
//                        idRangoFact = Long.parseLong(rango);
//                    }
//                } else {
//                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    datos.put("idRangoFacturaActual", idRangoFact);
//                }
//                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
//                JSONObject jsonCabecera = new JSONObject();
//                if (!jsonFact.isNull("facturaClienteCab")) {
//                    try {
//                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
//                        datos.put("nroFact", FacturaDeVentaFXMLController.getCabFactura().get("nroFact"));
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                } else {
//                    try {
//                        editandoJsonFactCab();
//                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString());
//                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
//                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
//                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
//                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
//                        datos.put("nroFact", nroActualmente);
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                }
//            }
//        }
//        fact.put("facturaClienteCab", FacturaDeVentaFXMLController.getCabFactura().toString());
//        if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
//            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
//            JSONArray arrayDetalle = new JSONArray();
//            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
//                try {
//                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
//                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
//                    art.put("fechaAlta", null);
//                    art.put("fechaMod", null);
//                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
//                    iva.put("fechaAlta", null);
//                    iva.put("fechaMod", null);
//                    art.put("iva", iva);
//                    jsonArt.put("articulo", art);
//                    arrayDetalle.add(jsonArt);
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                }
//            }
//            DatosEnCaja.setDatos(datos);
//            DatosEnCaja.setUsers(users);
//            DatosEnCaja.setFacturados(fact);
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = DatosEnCaja.getFacturados();
//            fact.put("facturaDetalle", arrayDetalle);
//        }
//        actualizarDatosBD();
//        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
//    }
    private void mensajeDialog() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Consulta de Precio");
        dialog.setHeaderText("Mensaje del Sistema!");
        dialog.setContentText("INGRESE CODIGO DEL PRODUCTO:");
        ButtonType btnAcept = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().addAll(btnAcept, btnCancel);
        dialog.getDialogPane().getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialog.getDialogPane().getStyleClass().add("myDialogInformation");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Articulo art = artDAO.buscarCod(result.get());
            art.setFechaAlta(null);
            art.setFechaMod(null);
            String mensaje = "";
            mensaje = art.getDescripcion() + "\nPRECIO: " + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(art.getCosto())));
            mensaje += "\n\nLOS DESCUENTOS QUEDAN SUJETO A VARIACIONES DE ACUERDO A LA FORMA DE PAGO.";
            mensajeDetalle(mensaje, "Detalle del Artículo");
        } else {
            Utilidades.log.info("No haz seleccionado nada");
        }
    }

    public static void actualizandoCabFacturaLocalmente() {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
        if (json.isNull("caida")) {
            MatrizFXMLController.cancelacionProd = false;
            datos.put("caida", "factura_venta");
        } else {
            String caida = json.get("caida").toString();
            if (caida.equalsIgnoreCase("factura_venta")) {
                if (!isActualizarDatosCabecera()) {
                    MatrizFXMLController.cancelacionProd = false;
                } else {
                    MatrizFXMLController.cancelacionProd = true;
                }
                datos.put("caida", "factura_venta");
            } else {
                MatrizFXMLController.cancelacionProd = true;
                datos.put("caida", "forma_pago");
            }
        }
        if (!facturaVentaEstado) {
            if (!MatrizFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                MatrizFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                JSONObject jsonCabecera = new JSONObject();
                if (!jsonFact.isNull("facturaClienteCab")) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        MatrizFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
                        datos.put("nroFact", MatrizFXMLController.getCabFactura().get("nroFact"));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    try {
                        editandoJsonFactCab();
                        Map<String, String> mapeo = Utilidades.splitNroActual(MatrizFXMLController.getCabFactura().get("nroActual").toString());
                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
                        datos.put("nroFact", nroActualmente);
                        MatrizFXMLController.getCabFactura().put("nroFactura", nroActualmente);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        MatrizFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());
        fact.put("facturaClienteCab", MatrizFXMLController.getCabFactura().toString());
        if (MatrizFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
            JSONArray jsonArrayFactDet = creandoJsonFactDet(MatrizFXMLController.getCabFactura());
            JSONArray arrayDetalle = new JSONArray();
            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
                    art.put("fechaAlta", null);
                    art.put("fechaMod", null);
                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                    iva.put("fechaAlta", null);
                    iva.put("fechaMod", null);
                    art.put("iva", iva);
                    jsonArt.put("articulo", art);
                    arrayDetalle.add(jsonArt);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            DatosEnCaja.setFacturados(fact);
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
            fact.put("facturaDetalle", arrayDetalle);
        }
        actualizarDatosBD();
        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
    }

    private static void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            MatrizFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                MatrizFXMLController.getCabFactura().put("cliente", NuevoClienteFXMLController.getJsonCliente());//en nulo default NN
            } else {
                MatrizFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            }

        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            if (DatosEnCaja.getFacturados() == null) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else {
                DatosEnCaja.setFacturados(fact);
            }
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
            jsonFact = jsonFact.replace("\"[", "[");
            jsonFact = jsonFact.replace("]\"", "]");
            manejo.setFactura(jsonFact);
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();

            fact = DatosEnCaja.getFacturados();
        } catch (Exception ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        } finally {
        }
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleUnidad() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        Map mapeo = recuperarGiftSinUso(codBarra);
        boolean estadoGift = recuperarGiftEnUso(codBarra);
        if (estadoGift) {
            toaster.mensajeGenerico("Mensaje del Sistema", "GIFTCARD YA ESTA EN USO", "", 2);
        } else if (Boolean.parseBoolean(mapeo.get("comprado").toString())) {
            if (getHmGift().containsKey(Long.valueOf(codBarra))) {
                toaster.mensajeGenerico("Mensaje del Sistema", "YA SE HA CARGADO LA GIFTCARD", "", 2);
            } else {
//                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!getDetalleArtList().isEmpty()) {
                    orden++;
                }
                GiftCardFXMLController.setCodigo(codBarra, tableViewFactura, mapeo, orden);
                this.sc.loadScreenModal("/vista/caja/GiftCardFXML.fxml", 425, 206, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            }
        } else if (!estadoGift) {
            if (!codBarra.contentEquals("")) {
                if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                    jsonArticulo = jsonArtDet(codBarra);
                } else {
                    jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
                }
            }
            JSONArray jsonArrayArticulo = new JSONArray();
            try {
                org.json.JSONObject jsonArticu = new org.json.JSONObject(jsonArticulo);
                if (jsonArticu.isNull("articuloNf3Sseccion")) {
                    jsonArrayArticulo = new JSONArray();
                } else {
                    jsonArrayArticulo = (JSONArray) parser.parse(jsonArticulo.get("articuloNf3Sseccion").toString());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
            }
            boolean pasa = false;
            if (jsonArrayArticulo.isEmpty()) {
                pasa = true;
            } else {
                JSONObject jsonObj = (JSONObject) jsonArrayArticulo.get(0);
                JSONObject nf3Sseccion = (JSONObject) jsonObj.get("nf3Sseccion");
                pasa = !nf3Sseccion.get("descripcion").toString().equalsIgnoreCase("GIFT CARD");
            }
            if (!pasa) {
                toaster.mensajeGenerico("Mensaje del Sistema", "NO SE HA ESTABLECIDO CONEXION CON LA BASE GIFTCARD", "", 2);
            } else {
                boolean data = false;
                if (jsonArticulo != null) {
                    org.json.JSONObject jsonArti = new org.json.JSONObject(jsonArticulo);
                    if (jsonArti.get("costo").toString().equals("0")) {
                        mensajeDetalle("EL ARTICULO NO TIENE PRECIO, CONTACTESE CON STOCK", "Mensaje del Sistema");

                        data = true;
                    }
                }
                if (!data) {
                    if (jsonArticulo != null) {
                        try {
                            JSONObject detalleArticulo = null;
                            if (primeraInsercion) {
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                                detalleArtList.add(detalleArticulo);
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                                vistaJSONObjectArtDet();
                                primeraInsercion = false;
                                detalleArticulo.put("primeraInsercion", true);
                                cargandoCamposInterface(detalleArticulo);
                                CajaDeDatos.generandoNroComprobante();
                                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                                // primer trío
                                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                                // segundo trío
                                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                                long nroActual = talos.getNroActual();
                            } else {
                                orden++;
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);//si
                                detalleArtList.add(detalleArticulo);//si
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));//si
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);//si
                                vistaJSONObjectArtDet();//no
                                cargandoCamposInterface(detalleArticulo);
                            }
                            tableViewFactura.getItems().clear();
                            tableViewFactura.getItems().addAll(detalleArtList);
//                }
//                            if (!tableViewFactura.getItems().isEmpty()) {
//                                tableViewFactura.getSelectionModel().selectLast();
//                            }
                            Image image = null;
                            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                                image = jsonArtDetImg(codBarra);
                            }
                            if (image == null) {
                                File file = new File(PATH.PATH_NO_IMG);
                                image = new Image(file.toURI().toString());

                            }
                        } catch (ParseException e) {
                            estado = false;
                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        }
                    } else {

                        estado = false;
                    }
                }
            }
        }
        txtCodVendedor.setText("");

        return estado;
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleDecimal() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    detalleArticulo.put("primeraInsercion", true);
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();

                } else {
                    orden++;
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    cargandoCamposInterface(detalleArticulo);
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                    //en caso de cancelación artículo y vuelva desde 0 a cargar
                }
//                if (!tableViewFactura.getItems().isEmpty()) {
//                    tableViewFactura.getSelectionModel().selectLast();
//                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                    image = jsonArtDetImg(codBarra);
                }

            } catch (NumberFormatException | ParseException e) {
                estado = false;
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {

            estado = false;
        }
        return estado;
    }

    private JSONObject jsonArtDet(String cod) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject articulo = null;
        articulo = generarListaArticuloLocal(cod);
        return articulo;
    }

    private JSONObject generarListaArticuloLocal(String cod) {
        JSONParser parser = new JSONParser();
        try {
            Articulo art = artDAO.buscarCod(cod);
            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
    }

    private static JSONObject creandoJsonFactCab() {
        try {
            JSONObject jsonCabFactura = new JSONObject();
            JSONObject estadoFactura = new JSONObject();
            JSONParser parser = new JSONParser();
            estadoFactura.put("idEstadoFactura", 1L);//normal
            JSONObject tipoMoneda = new JSONObject();
            tipoMoneda.put("idTipoMoneda", 1L);//guaraníes
            JSONObject tipoComprobante = new JSONObject();
            //acaité para el tema de mayorista...
            tipoComprobante.put("idTipoComprobante", 1L);//factura contado
            //**********************************************************************
            jsonCabFactura.put("cancelado", true);
            jsonCabFactura.put("caja", datos.get("caja"));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                jsonCabFactura.put("cliente", NuevoClienteFXMLController.getJsonCliente());
            } else {
                jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());
            }
//            jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            jsonCabFactura.put("sucursal", datos.get("sucursal"));//en nulo default NN
            jsonCabFactura.put("nroFactura", null);
            jsonCabFactura.put("estadoFactura", estadoFactura);
            jsonCabFactura.put("tipoComprobante", tipoComprobante);
            jsonCabFactura.put("tipoMoneda", tipoMoneda);
            //**********************************************************************
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            jsonCabFactura.put("fechaEmision", timestampEmision);
            jsonCabFactura.put("fechaMod", timestampEmision);
            jsonCabFactura.put("usuAlta", Identity.getNomFun());
            jsonCabFactura.put("usuMod", Identity.getNomFun());
            //**********************************************************************
            JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            jsonCabFactura.put("nroActual", talona.get("nroActual") + " - " + String.valueOf(talona.get("idTalonariosSucursales")));
            //Consulta de nroActual de talonarios en la BD local....
            return jsonCabFactura;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        return null;
    }

    private void cargandoCamposInterface(JSONObject detalleArticulo) {
        org.json.JSONObject jsonDetalle = new org.json.JSONObject(detalleArticulo);
//        labelCantidad.setText(detalleArticulo.get("cantidad").toString());

//        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
//        if (jsonDetalle.isNull("exenta")) {
        if (codDecimal == null) {
            codDecimal = "";
        }
        if (!codDecimal.isEmpty()) {

        } else {

        }

        seteandoMontoFact();
    }

    private static void seteandoMontoFact() {
        int monto = Integer.valueOf(String.valueOf(precioTotal));
        cabFactura.put("montoFactura", monto);
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    static void persistiendoFact(boolean cancelProd, long idArt) {
        try {
            JSONParser parser = new JSONParser();
            if (cancelProd) {
                if (cancelacionProdPrimera) {
                    FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(false));
                    cancelacionProdPrimera = false;
                    cancelacionProd = true;//permite cambiar a PUT en FormaPagoFXMLController, al finalizar venta...
                }
                FacturaVentaDatos.setIdProducto(idArt);
            } else {
                FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(true));
                cancelacionProd = false;
            }
            JSONObject aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
            JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
            FacturaVentaDatos.setIdCajero(Long.valueOf(usuarioCajero.get("idUsuario").toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    private static long creandoCabFactura(boolean cancelFact) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean estadoCancelProd = false;
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        long idFact = 0l;
        if (cancelFact) {
            JSONObject estadoFactura = new JSONObject();
            estadoFactura.put("idEstadoFactura", 2L);//anulado
            cabFactura.put("estadoFactura", estadoFactura);
        }
        try {
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));
            // NUEVO 
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                if (cancelFact) {
                    JSONObject estadoFactura = new JSONObject();
                    estadoFactura.put("idEstadoFactura", 2L);//anulado
                    cabe.put("estadoFactura", estadoFactura);
                    fact.put("facturaClienteCab", cabe);
                }
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    cabFactura = objFact;
                    if (!jsonDatos.isNull("caida")) {
                        String caida = datos.get("caida").toString();
                        if (caida.equalsIgnoreCase("forma_pago")) {
                            cancelacionProdPrimera = false;
                        } else if (!jsonDatos.isNull("cancelProducto")) {
                            cancelacionProdPrimera = false;
                            estadoCancelProd = true;
                        } else {
                            cancelacionProdPrimera = true;
                        }
                    }
                }
            }
            // NUEVO
            if (cancelacionProdPrimera && idCab == 0L) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
                //Linea para prueba por el rango actual
                long n = 0l;
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idRangoFacturaActual")) {
                    n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
                }
                cabFactura.put("idFacturaClienteCab", n);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = cabFactura.get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                cabFactura.put("nroFactura", nroFact);
                datos.put("nroFact", nroFact);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                int actulizacion = 0;
//                boolean insertVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    insertVenta = Boolean.parseBoolean(datos.get("insercionFacturaVentaCab").toString());
//                }
//                boolean actualizacionVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    if (jsonDatos.isNull("actualizacionLocal")) {
//                        actualizacionVenta = false;
//                    } else {
//                        actualizacionVenta = Boolean.parseBoolean(datos.get("actualizacionLocal").toString());
//                    }
//                }
//                if (!jsonDatos.isNull("cancelProd")) {
//                    cancelacionProdPrimera = false;
//                }
//                if (cancelacionProdPrimera) {
//                    conn.setRequestMethod("POST");//primera vez, sin importar factura cancelación o artículo...
//                    estado = true;
//                    actulizacion = 1;
//                } else if (insertVenta && !actualizacionVenta) {//Operacion realizada para saber si se persistio datos en el Servidor, sino lo realiza de manera local(ACTUALIZACION DE FACTURA CABECERA)
//                    conn.setRequestMethod("PUT");
//                    actulizacion = 1;
//                    if (estadoCancelProd) {
//                        estado = true;
//                    } else {
//                        estado = false;
//                    }
//                }
//                if (actulizacion == 1) {
//                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                    wr.write(cabFactura.toString());
//                    fact.put("facturaClienteCab", cabFactura.toString());
//                    wr.flush();
//                    int HttpResult = conn.getResponseCode();
//                    if (HttpResult == HttpURLConnection.HTTP_OK) {
//                        BufferedReader br = new BufferedReader(
//                                new InputStreamReader(conn.getInputStream(), "utf-8"));
//                        while ((inputLine = br.readLine()) != null) {
//                            cabFactura = (JSONObject) parser.parse(inputLine);
//                            datos.put("ventaServer", true);
//                            if (cabFactura.get("idFacturaClienteCab") != null) {
//                                idFact = (long) cabFactura.get("idFacturaClienteCab");
//                                //Setear Datos para saber que id se persistió en facturaClienteCab del servidor
//                                //en el caso que persista primeramente con conexion, y para la actualizacion, la conexión se vaya al maso...
//                                if (cancelacionProdPrimera) {
//                                    datos.put("insercionFacturaVentaCab", true);
//                                    datos.put("insercionIdFactClienteCabServidor", idFact);
//                                    datos.put("nroFact", cabFactura.get("nroFactura").toString());
//                                }
//                            }
//
//                        }
//                        br.close();
//                    } else {
//                        idFact = generarFacturaCabLocal();
//                    }
//                } else {
//                    idFact = generarFacturaCabLocal();
//                }
//            } else {
            idFact = generarFacturaCabLocal();
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //para registrar facturaIni
        org.json.JSONObject jsonEstadoInicio = new org.json.JSONObject(datos);
        boolean estadoFactInicial = false;
        if (!jsonEstadoInicio.isNull("estadoFacturaInicial")) {
            estadoFactInicial = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        if (!estadoFactInicial) {
            datos.put("facturaInicial", cabFactura.get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("FacturaFinal", cabFactura.get("nroFactura").toString());
        if (idFact != 0l && cancelFact && estado) {//que sea del tipo cancelación factura...
            JSONArray jsonArrayFactDet = creandoJsonFactDet(cabFactura);
            if (creandoFactDet(jsonArrayFactDet)) {
            }
        }
        actualizarDatos();
        return idFact;
    }

    public static JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < detalleArtList.size(); i++) {
            detalleArtList.get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(detalleArtList.get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }

    private static boolean creandoFactDet(JSONArray jsonArray) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Boolean.parseBoolean(datos.get("ventaServer").toString())) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteDet/insercionMasiva");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonArrayFactDet.toString());
//                fact.put("facturaDetalle", jsonArrayFactDet.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exitoInsertarDet = (boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//                }
//            } else {
            exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }

    private static boolean registrandoFacturaDetLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private static long generarFacturaCabLocal() {
        String sql = "";
        ConexionPostgres.conectar();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (cancelacionProdPrimera) {
            datos.put("insercionFacturaVentaCabLocal", true);
            //UUID ACTUAL PARA MODIFICAR FACTURA
            long uuid = VentasUtiles.recuperarId() + 1;
            datos.put("uuidCassandraActual", String.valueOf(uuid));
            sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'insertar');";
            Utilidades.log.info("-->> " + sql);
        } else {
            long idFactCab = 0L;
            if (!json.isNull("idFactClienteCabServidor")) {
                idFactCab = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
            }
            if (idFactCab != 0 && !dato) {
                //UUID ACTUAL PARA MODIFICAR FACTURA
                long uuid = VentasUtiles.recuperarId() + 1;
                cabFactura.put("idFacturaClienteCab", idFactCab);
                sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'actualizar');";
                Utilidades.log.info("-->> " + sql);
                datos.put("uuidCassandraActual", String.valueOf(uuid));
                //para que solo una ves entre aqui luego ya actualice nada mas...
                dato = true;
                datos.put("actualizacionLocal", true);
            } else {
                String operacion = "insertar";
                if (idFactCab != 0) {
                    cabFactura.put("idFacturaClienteCab", idFactCab);
                    operacion = "actualizar";
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + datos.get("uuidCassandraActual").toString() + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
                Utilidades.log.info("-->> " + sql);
                datos.put("actualizacionLocal", true);
            }
        }
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS FACTURA VENTA CAB ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        cabFactura.put("idFacturaClienteCab", datos.get("idRangoFacturaActual"));

        return Long.parseLong(datos.get("idRangoFacturaActual").toString());
    }

    static void suprimirProducto(TableView<JSONObject> tabla, Label labelTotalGs, double declarado,
            Label labelCantidad, ImageView imgProducto, CheckBox chkExtranj, TextField txtCod) {
        JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
        JSONObject articulos = (JSONObject) detalle.get("articulo");
        long codArticulo = (long) articulos.get("codArticulo");
        JSONObject jsonArticulo = hashJsonArticulo.get(codArticulo);
        double cantTotal = Double.parseDouble(detalle.get("cantidad").toString());
        double dif = cantTotal - declarado;
        int total = Integer.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        double neto = 0;
        double resultado = 0;
        String dato = "";
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        double nArt = Double.parseDouble(datos.get("nArticulos").toString());
//        datos.put("nArticulos", nArt - cantTotal);
        datos.put("nArticulos", nArt - declarado);
        if (dif <= 0) {
            resultado = Double.parseDouble(detalle.get("precio").toString()) * cantTotal;
            List<JSONObject> detalleAux = new ArrayList<>();
            resetMapeo();
            int ord = 0;
            for (JSONObject detalleEnCuestion : detalleArtList) {
                JSONObject jsonArtAux = (JSONObject) detalleEnCuestion.get("articulo");
                if (!(codArticulo + "|" + detalle.get("orden").toString()).equalsIgnoreCase(jsonArtAux.get("codArticulo") + "|" + detalleEnCuestion.get("orden"))) {
                    ord++;
                    detalleEnCuestion.put("orden", ord);
                    detalleAux.add(detalleEnCuestion);
                    hashJsonArtDet.put((Long) jsonArtAux.get("idArticulo"), detalleAux.lastIndexOf(detalleEnCuestion));
                    hashJsonArticulo.put((Long) jsonArtAux.get("codArticulo"), jsonArtAux);
                }
            }
            orden = ord;
            detalleArtList = new ArrayList<>();
            detalleArtList = detalleAux;
//            if (ord == 0 && detalleAux.size() == 0) {
//                BuscarClienteFXMLController.resetParamCliente();
//                BuscarClienteFXMLController.resetParamClienteFiel();
//            }
        } else {
            int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
            resultado = Double.parseDouble(detalle.get("precio").toString()) * declarado;
            long resultadoActual = Math.round(Double.parseDouble(detalle.get("precio").toString()) * dif);
            long iva = (long) detalle.get("poriva");
            if (iva == 0l || chkExtranj.isSelected()) {
                detalleArtList.get(index).put("exenta", resultadoActual);
            } else {
                detalleArtList.get(index).put("gravada", resultadoActual);
            }
            detalleArtList.get(index).put("cantidad", dif);
        }
        neto = total - resultado;
        dato = formateador.format(neto);
        labelTotalGs.setText("Gs " + dato);
        labelCantidad.setText("0");
        imgProducto.setImage(null);
        precioTotal = Math.round(neto);
        seteandoMontoFact();
        try {
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                }
            }
            if (idCab == 0L) {
                creandoCabFactura(false);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        tabla.getItems().clear();
        tabla.getItems().addAll(detalleArtList);
        //NEW
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setFacturados(fact);
        actualizarDatos();
        if (tabla.getItems().isEmpty()) {
            chkExtranj.setDisable(false);
            chkExtranj.setSelected(false);
        }
        repeatFocusData(txtCod);
    }

    static void setTearDatos(Label lblRuc, Label lblNombre, String ruc, String nombre) {
        lblRuc.setText(ruc);
        lblNombre.setText(nombre);
    }

    static void regresar(TextField txtCod) {
        repeatFocusData(txtCod);
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }

    @FXML
    private void textFieldCantKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void tableViewFacturaKeyReleased(KeyEvent event) {
//        keyPressEventos(event);   
    }

    private void keyPressEventos(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtCodigo.isFocused()) {
                buscarArticuloEnTabla();
            } else {
//                if ("cliente_caja")) {
                posicion = tableViewFactura.getSelectionModel().getSelectedIndex() + "";
//            BuscarClienteMayFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
                ModCantidadReponerFXMLController.setCantidad(tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoCentral").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoSanLorenzo").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoCacique").toString(), tableViewFactura);
                this.sc.loadScreenModal("/vista/stock/ModCantidadReponerFXML.fxml", 232, 178, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//                } else {
//                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//
//                }
            }
//            if (txtNroEntrada.isFocused()) {
//                buscarRecepcion();
//            } else if (txtCodigo.isFocused()) {
//                buscarArticulo();
//            }
        }
    }

    private void buscarArticulo() {
        Articulo art = artDAO.buscarCod(txtCodigo.getText());
        if (art != null) {
            depositoList = new ArrayList<>();

            txtContenido.setText("1");
            txtGasto.setText("0");
            if (art.getIva().getIdIva() == 1) {
//                txtDescriIva.setText("EXE");
//                txtPorcIva.setText(art.getIva().getPoriva() + "");
//                txtExenta.setText(art.getCosto() + "");
            } else {
//                txtDescriIva.setText("GRA");
//                txtPorcIva.setText(art.getIva().getPoriva() + "");
//                long iva = 0;
//                if (art.getIva().getIdIva() == 2) {
//                    iva = Math.round(art.getCosto() / 21);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                } else {
//                    iva = Math.round(art.getCosto() / 11);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                }
//                txtGravada.setText(iva + "");
            }
            verificarNivel1(art.getIdArticulo());
            List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
            for (Existencia ex : listExis) {
//                try {
                Articulo articu = new Articulo();
                articu.setIdArticulo(art.getIdArticulo());

                ex.setArticulo(articu);
//                    JSONObject objJSON = (JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objJSON = new JSONObject();//(JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objART = new JSONObject();
                objART.put("idArticulo", art.getIdArticulo());

                JSONObject objDEPO = new JSONObject();
                objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
                objDEPO.put("descripcion", ex.getDeposito().getDescripcion());

                objJSON.put("idExistencia", ex.getIdExistencia());
                objJSON.put("cantidad", ex.getCantidad());
                objJSON.put("articulo", objART);
                objJSON.put("deposito", objDEPO);

                depositoList.add(objJSON);

                actualizandoTablaDeposito();
//                } catch (ParseException ex1) {
//                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
            }
        } else {
            mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
        }
    }

    private void actualizandoTablaDeposito() {
        depositoData = FXCollections.observableArrayList(depositoList);
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                    String apellido = String.valueOf(jsonDepo.get("descripcion"));
                    if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                        apellido = "-";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());

                } catch (ParseException ex) {
                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnCantDepo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantDepo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                //                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                String apellido = String.valueOf(data.getValue().get("cantidad"));
                if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                    apellido = "-";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Ruc .................................................
        tableViewDeposito.setItems(depositoData);
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneFactura.getChildren()
                .get(anchorPaneFactura.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

//    PARA NUESTRO ENTORNO
//    private boolean recuperarGiftEnUso(String codigo) {
//        boolean comprado = false;
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT comprado FROM stock.articulo WHERE comprado=true AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    comprado = true;
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        }
//        return comprado;
//    }
    private boolean recuperarGiftEnUso(String codigo) {
        boolean comprado = false;
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT comprado FROM stk_articulos WHERE comprado=1 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    comprado = true;
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        }
        return comprado;
    }
//    PARA NUESTRO ENTORNO
//    private Map recuperarGiftSinUso(String codigo) {
//        Map mapeo = new HashMap();
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT * FROM stock.articulo WHERE comprado=false AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    mapeo.put("comprado", true);
//                    mapeo.put("codigo", rs.getLong("cod_articulo"));
//                    mapeo.put("saldogift", rs.getDouble("saldogift"));
//                    mapeo.put("fecha", rs.getDate("fechavtogift"));
//                    mapeo.put("descripcion", rs.getString("descripcion"));
//                } else {
//                    mapeo.put("comprado", false);
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        } else {
//            mapeo.put("comprado", false);
//        }
//        return mapeo;
//    }

    private Map recuperarGiftSinUso(String codigo) {
        Map mapeo = new HashMap();
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT * FROM stk_articulos WHERE comprado=0 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    mapeo.put("comprado", true);
                    mapeo.put("codigo", rs.getLong("codigo"));
                    mapeo.put("saldogift", rs.getDouble("saldogift"));
                    mapeo.put("fecha", rs.getDate("fechavtogift"));
                    mapeo.put("descripcion", rs.getString("descripcion"));
                } else {
                    mapeo.put("comprado", false);
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        } else {
            mapeo.put("comprado", false);
        }
        return mapeo;
    }

    private void resetFormulario(long total) {
        JSONObject talonarioSucursal;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
            // primer trío
            long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
            // segundo trío
            long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
            long nroActual = talos.getNroActual();

//                datos.put("nroFact", Long.parseLong(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual).replace("-", "")));
            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");

            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }

            } else {

            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
        primeraInsercion = false;

        cargandoImagen();
    }

    private void listenCheckExtranjero() {
//        chkExtranjero.selectedProperty().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                CajaDeDatos.getCaja().put("estadoExt", newValue);
//            }
//        });
    }

    @FXML
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    @FXML
    private void btnBorrarDetalleAction(ActionEvent event) {
    }

    @FXML
    private void btnSeccionAction(ActionEvent event) {
    }

    @FXML
    private void btnNoserAction(ActionEvent event) {
    }

    @FXML
    private void btnCargarVtoProdAction(ActionEvent event) {
    }

    private void btnVerificarPrecioAction(ActionEvent event) {
        verificarPrecio();
    }

    private void btnGuardarDetalleAction(ActionEvent event) {
        guardarDetalle();
    }

    @FXML
    private void btnBuscarOrdenCompraAction(ActionEvent event) {
    }

    @FXML
    private void btnEditarAction(ActionEvent event) {
    }

    private void btnGuardarAction(ActionEvent event) {
        guardarCompra();
    }

    @FXML
    private void btnBorrarAction(ActionEvent event) {
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
    }

    private void txtNroEntradaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void txtCodigoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void verificarNivel1(long id) {
        ArticuloNf1Tipo anf1Tipo = anf1TipoDAO.getByIdArticulo(id);
        if (anf1Tipo != null) {
            nivel01 = anf1Tipo.getNf1Tipo().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel01);
            txtSec1.setText(nivel01);
            verificarNivel2(id);
        }
    }

    private void verificarNivel2(long id) {
        ArticuloNf2Sfamilia anf2Familia = anf2SfamiliaDAO.getByIdArticulo(id);
        if (anf2Familia != null) {
            nivel02 = anf2Familia.getNf2Sfamilia().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel02);
            verificarNivel3(id);
        }
    }

    private void verificarNivel3(long id) {
        ArticuloNf3Sseccion anf3Seccion = anf3SeccionDAO.getByIdArticulo(id);
        if (anf3Seccion != null) {
            nivel03 = anf3Seccion.getNf3Sseccion().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel03);
            verificarNivel4(id);
        }
    }

    private void verificarNivel4(long id) {
        ArticuloNf4Seccion1 anf4Seccion = anf4SeccionDAO.getByIdArticulo(id);
        if (anf4Seccion != null) {
            nivel04 = anf4Seccion.getNf4Seccion1().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel04);
            verificarNivel5(id);
        }
    }

    private void verificarNivel5(long id) {
        ArticuloNf5Seccion2 anf5Seccion = anf5SeccionDAO.getByIdArticulo(id);
        if (anf5Seccion != null) {
            nivel05 = anf5Seccion.getNf5Seccion2().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel05);
            verificarNivel6(id);
        }
    }

    private void verificarNivel6(long id) {
        ArticuloNf6Secnom6 anf6Seccion = anf6SeccionDAO.getByIdArticulo(id);
        if (anf6Seccion != null) {
            nivel06 = anf6Seccion.getNf6Secnom6().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel06);
            verificarNivel7(id);
        }
    }

    private void verificarNivel7(long id) {
        ArticuloNf7Secnom7 anf7Seccion = anf7SeccionDAO.getByIdArticulo(id);
        if (anf7Seccion != null) {
            nivel07 = anf7Seccion.getNf7Secnom7().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel07);
        }
    }

    private void guardarDetalle() {
    }

    private void guardarCompra() {
    }

    public void limpiarCampos() {
        matrizList = new ArrayList<>();
        tableViewFactura.getItems().clear();
        categoriasList = new ArrayList<>();
        tableViewSeccion.getItems().clear();
        depositoList = new ArrayList<>();
        tableViewDeposito.getItems().clear();
        orden = 0;

        txtProveedor.setText("");
        BuscarProveedorFXMLController.resetParam();
//                arrayFecha.put("fechaDesde", dpDesdeActual.getValue());
//                mapFecha.put("fechaHasta", dpHastaActual.getValue());
//                mapFecha.put("fechaDesdeAn", dpDesdeAnterior.getValue());
//                mapFecha.put("fechaHastaAn", dpHastaAnterior.getValue());

//        dpDesdeActual.setValue(null);
//        dpHastaActual.setValue(null);
//        dpDesdeAnterior.setValue(null);
//        dpHastaAnterior.setValue(null);
        cargarFechas();
        txtPlazo.setText("");
    }

    private void buscarProveedor() {
        BuscarProveedorFXMLController.cargarRucRazon(txtProveedorDoc, txtProveedor);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void buscarOrden() {
//        BuscarRecepcionFXMLController.cargarOrden(txtNroEntrada);
//        this.sc.loadScreenModal("/vista/stock/BuscarRecepcionFXML.fxml", 454, 450, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void verificarPrecio() {
        if (!txtCodigo.getText().equals("")) {
            MatrizFXMLController.setCodArt(txtCodigo.getText());
            VerificarPrecioFXMLController.setCodArt(txtCodigo.getText());
            this.sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("NO SE HA CARGADO NINGUN CODIGO DE ARTICULO");
        }
    }

    private void txtCantidadKeyPress(KeyEvent event) {
        listenCantidad(event);
    }

    private void listenCantidad(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
//            txtCantidad.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    calcularCosto(newValue);
//                });
//            });
        }
    }

    private void calcularCosto(String newValue) {
//        if (!newValue.equals("")) {
//            if (txtPorcIva.getText().equalsIgnoreCase("0")) {
//                txtDescriIva.setText("EXE");
//                txtPorcIva.setText(0 + "");
//                long total = Long.parseLong(txtCantidad.getText()) * Long.parseLong(txtCosto.getText());
//                txtExenta.setText(total + "");
//            } else {
//                txtDescriIva.setText("GRA");
//                txtPorcIva.setText(txtPorcIva.getText());
//                long iva = 0;
//                if (txtPorcIva.equals("5")) {
//                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 21);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                    iva = iva * Long.parseLong(txtCantidad.getText());
//                } else {
//                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 11);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                    iva = iva * Long.parseLong(txtCantidad.getText());
//                }
//                txtGravada.setText(iva + "");
//            }
//        }
    }

    @FXML
    private void btnFiltrarAction(ActionEvent event) {
        filtrando();
    }

    @FXML
    private void btnCargaMatrizAction(ActionEvent event) {
        cargarDetalle();
    }

    @FXML
    private void btnAplicarAction(ActionEvent event) {
        mantenerSeleccionados();
    }

    @FXML
    private void btnGenerarPedidoAction(ActionEvent event) {
        generarPedidos();
    }

    @FXML
    private void btnBuscarAction(ActionEvent event) {
        buscarArticuloEnTabla();
    }

    @FXML
    private void btnImprimirAction(ActionEvent event) {
        imprimirNow();
    }

    private void cargarFechas() {
        LocalDate now = LocalDate.now(); //2015-11-23
        LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
        LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30
//        System.out.println("-> " + lastDay + " - " + firstDay);
        dpDesdeActual.setValue(firstDay);
        dpHastaActual.setValue(lastDay);

        LocalDate nowLast = LocalDate.now(); //2015-11-23
        LocalDate lastDayLast = nowLast.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
        lastDayLast = lastDayLast.withYear((now.getYear() - 1));

        LocalDate firstDayLast = nowLast.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30
        firstDayLast = firstDayLast.withYear((firstDayLast.getYear() - 1));
        dpDesdeAnterior.setValue(firstDayLast);
        dpHastaAnterior.setValue(lastDayLast);
//        System.out.println("-> " + lastDayLast + " - " + firstDayLast);
    }

    private void cargarDetalle() {
//CATEGORIA
        List<JSONObject> list = new ArrayList<>();
        Map mapping = new HashMap();
//CATEGORIA
        matrizList = new ArrayList<>();
        mapeoNow = new HashMap();
        mapeoCheck = new HashMap();
        categoriasList = new ArrayList<>();
        List<FacturaCompraDet> listNow = new ArrayList<>();
        List<ArticuloProveedor> listArtProv = new ArrayList<>();
        chkLocal.setSelected(true);
        cbSucursal.getSelectionModel().select("CASA CENTRAL");
        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
        boolean val = false;
        if (!txtProveedor.getText().equals("")) {
            listArtProv = artProvDAO.findByProveedor(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString());
            String fecData = facCompraDetDAO.recuperarUltimaFechaVenta(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString());
            try {
                String oc = facCompraCabDAO.recuperarOcByFecha(fecData, BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString());
                listNow = facCompraDetDAO.listarPorOC(oc);
            } catch (Exception e) {
                listNow = new ArrayList<>();
            } finally {
            }
        } else {
            mensajeAlerta("DEBE SELECIONAR UN PROVEEDOR PARA INICIAR LA BUSQUEDA");
            val = true;
        }
        arrayFecha.add(dpDesdeActual.getValue());
        arrayFecha.add(dpHastaActual.getValue());
        arrayFecha.add(dpDesdeAnterior.getValue());
        arrayFecha.add(dpHastaAnterior.getValue());
        plazo = txtPlazo.getText();
        sucursal = cbSucursal.getSelectionModel().getSelectedItem();
        int ordenes = 1;
        for (ArticuloProveedor articuloProveedor : listArtProv) {
            JSONObject jsonNow = new JSONObject();
            jsonNow.put("orden", ordenes);
            jsonNow.put("descripcion", articuloProveedor.getArticulo().getDescripcion());
            jsonNow.put("codigo", articuloProveedor.getArticulo().getCodArticulo());
            jsonNow.put("precio", articuloProveedor.getArticulo().getPrecioMin());
            jsonNow.put("costo", articuloProveedor.getArticulo().getCosto());
            try {
                long numerador = articuloProveedor.getArticulo().getPrecioMin() - articuloProveedor.getArticulo().getCosto();
                long denominador = articuloProveedor.getArticulo().getPrecioMin();
                float porc = (float) numerador / denominador;
                porc = porc * 100;
                DecimalFormat decimalFormat = new DecimalFormat("#.##");
                float prcent = Float.valueOf(decimalFormat.format(porc));
                jsonNow.put("md", prcent + "");
            } catch (Exception e) {
                jsonNow.put("md", "0");
            } finally {
            }
            jsonNow.put("ultimaCompra", listNow.get(0).getFacturaCompraCab().getFechaDoc().toString());
            try {
                jsonNow.put("proveedor", txtProveedor.getText());
            } catch (Exception e) {
            } finally {
            }
//            long compraCC = artCompraMatrizDAO.getCantidadArticuloCC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//            long compraSL = artCompraMatrizDAO.getCantidadArticuloSL(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//            long compradSC = artCompraMatrizDAO.getCantidadArticuloSC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
            long compraCC = 0;
            long compraSL = 0;
            long compradSC = 0;
            jsonNow.put("cantCompraCC", compraCC);
            jsonNow.put("cantCompraSL", compraSL);
            jsonNow.put("cantCompraSC", compradSC);
            jsonNow.put("totalCompra", (compraCC + compraSL + compradSC));

//                System.out.println("-8888-> " + artCompraMatrizDAO.getById(1966904).getCodigo());
//                long compraCCAN = facCompraDetDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//                long compraSLAN = facCompraDetDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//                long compradSCAN = facCompraDetDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//            long compraCCAN = artCompraMatrizDAO.getCantidadArticuloCC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//            long compraSLAN = artCompraMatrizDAO.getCantidadArticuloSL(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//            long compradSCAN = artCompraMatrizDAO.getCantidadArticuloSC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
            long compraCCAN = 0;
            long compraSLAN = 0;
            long compradSCAN = 0;
            jsonNow.put("cantCompraCCAN", compraCCAN);
            jsonNow.put("cantCompraSLAN", compraSLAN);
            jsonNow.put("cantCompraSCAN", compradSCAN);
            jsonNow.put("totalCompraAN", (compraCCAN + compraSLAN + compradSCAN));

//                long cantidadCC = facturaClienteCabDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//                long cantidadSL = facturaClienteCabDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//                long cantidadSC = facturaClienteCabDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//            long cantidadCC = artVentaMatrizDAO.getCantidadArticuloCC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//            long cantidadSL = artVentaMatrizDAO.getCantidadArticuloSL(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//            long cantidadSC = artVentaMatrizDAO.getCantidadArticuloSC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
            long cantidadCC = 0;
            long cantidadSL = 0;
            long cantidadSC = 0;

            jsonNow.put("ventaCC", cantidadCC);
            jsonNow.put("ventaSL", cantidadSL);
            jsonNow.put("ventaSC", cantidadSC);
            jsonNow.put("ventaTotal", (cantidadCC + cantidadSL + cantidadSC));

            long cantidadCCAN = 0;
            long cantidadSLAN = 0;
            long cantidadSCAN = 0;
//            long cantidadCCAN = artVentaMatrizDAO.getCantidadArticuloCC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//            long cantidadSLAN = artVentaMatrizDAO.getCantidadArticuloSL(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//            long cantidadSCAN = artVentaMatrizDAO.getCantidadArticuloSC(articuloProveedor.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());

            jsonNow.put("ventaCCAN", cantidadCCAN);
            jsonNow.put("ventaSLAN", cantidadSLAN);
            jsonNow.put("ventaSCAN", cantidadSCAN);
            jsonNow.put("ventaTotalAN", (cantidadCCAN + cantidadSLAN + cantidadSCAN));

            long exisCC = 0;
            long exisSL = 0;
            long exisSC = 0;
//                long exisCC = existenciaDAO.getCantidadArticuloCC(fcdNow.getArticulo().getIdArticulo());
//                long exisSL = existenciaDAO.getCantidadArticuloSL(fcdNow.getArticulo().getIdArticulo());
//                long exisSC = existenciaDAO.getCantidadArticuloSC(fcdNow.getArticulo().getIdArticulo());

            jsonNow.put("exisCC", exisCC);
            jsonNow.put("exisSL", exisSL);
            jsonNow.put("exisSC", exisSC);
//                cantRepoCacique-cantRepoSanLorenzo-cantRepoCentral
            jsonNow.put("cantRepoCentral", 0);
            jsonNow.put("cantRepoSanLorenzo", 0);
            jsonNow.put("cantRepoCacique", 0);

            jsonNow.put("exisTotal", (exisCC + exisSL + exisSC));
            jsonNow.put("precioCosto", articuloProveedor.getArticulo().getCosto());
            jsonNow.put("valorizado", articuloProveedor.getArticulo().getCosto());
//                jsonNow.put("seccion", fcdNow.getSec2());
            if (articuloProveedor.getArticulo().getSec1() == null) {
                jsonNow.put("seccion", "");
            } else {
                jsonNow.put("seccion", articuloProveedor.getArticulo().getSec1());
            }
            JSONObject jsonCategoria = new JSONObject();
            jsonCategoria.put("categoria", jsonNow.get("seccion").toString());
            if (!mapping.containsKey(jsonNow.get("seccion").toString())) {
                if (!jsonNow.get("seccion").toString().equals("")) {
                    list.add(jsonCategoria);
                }
            }
            mapping.put(jsonNow.get("seccion").toString(), jsonNow.get("seccion").toString());
            matrizList.add(jsonNow);
            ordenes++;
        }
        if (listNow.size() > 0) {
            int ord = 1;
            for (JSONObject jSONObject : list) {
                jSONObject.put("orden", ord);
                categoriasList.add(jSONObject);
                ord++;
            }
            actualizandoTablaMatriz(matrizList);
            actualizarTablaCategoria();
        } else {
            if (!val) {
                mensajeAlerta("NO SE ENCUENTRAN RESULTADOS DE LA BUSQUEDA...");
            }
        }
    }

    private void cargarDetalle2() {
        matrizList = new ArrayList<>();
        mapeoNow = new HashMap();
        mapeoCheck = new HashMap();
        categoriasList = new ArrayList<>();
        List<FacturaCompraDet> listNow = new ArrayList<>();
        chkLocal.setSelected(true);
        cbSucursal.getSelectionModel().select("CASA CENTRAL");
        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
        boolean val = false;
        if (!txtProveedor.getText().equals("")) {
            String fecData = facCompraDetDAO.recuperarUltimaFechaVenta(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString());
            try {
                String oc = facCompraCabDAO.recuperarOcByFecha(fecData, BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString());
                listNow = facCompraDetDAO.listarPorOC(oc);
            } catch (Exception e) {
                listNow = new ArrayList<>();
            } finally {
            }
        } else {
            mensajeAlerta("DEBE SELECIONAR UN PROVEEDOR PARA INICIAR LA BUSQUEDA");
            val = true;
        }
        arrayFecha.add(dpDesdeActual.getValue());
        arrayFecha.add(dpHastaActual.getValue());
        arrayFecha.add(dpDesdeAnterior.getValue());
        arrayFecha.add(dpHastaAnterior.getValue());
        plazo = txtPlazo.getText();
        sucursal = cbSucursal.getSelectionModel().getSelectedItem();

        for (FacturaCompraDet fcdNow : listNow) {
            JSONObject jsonNow = new JSONObject();
            if (mapeoNow.containsKey(fcdNow.getArticulo().getCodArticulo())) {
                try {
                    String jsonEnString = String.valueOf(mapeoNow.get(fcdNow.getArticulo().getCodArticulo()).toString());
                    JSONObject jsonObj = (JSONObject) parser.parse(jsonEnString);

                    jsonNow.put("descripcion", fcdNow.getArticulo().getDescripcion());
                    jsonNow.put("codigo", fcdNow.getArticulo().getCodArticulo());
                    jsonNow.put("precio", fcdNow.getArticulo().getPrecioMin());
                    jsonNow.put("costo", fcdNow.getArticulo().getCosto());

                    try {
                        long numerador = fcdNow.getArticulo().getPrecioMin() - fcdNow.getArticulo().getCosto();
                        long denominador = fcdNow.getArticulo().getPrecioMin();
                        float porc = (float) numerador / denominador;
                        porc = porc * 100;
                        DecimalFormat decimalFormat = new DecimalFormat("#.##");
                        float prcent = Float.valueOf(decimalFormat.format(porc));
                        jsonNow.put("md", prcent + "");
                    } catch (Exception e) {
                        jsonNow.put("md", "0");
                    } finally {
                    }
                    try {
                        jsonNow.put("proveedor", txtProveedor.getText());
                    } catch (Exception e) {
                    } finally {
                    }

                    jsonNow.put("ultimaCompra", fcdNow.getFacturaCompraCab().getFechaDoc().toString());
                    long totalCompra = 0;
                    if (fcdNow.getFacturaCompraCab().isCc()) {
                        jsonNow.put("cantCompraCC", (Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraCC").toString())));
                        totalCompra += (Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraCC").toString()));
                    } else {
                        jsonNow.put("cantCompraCC", Long.parseLong(jsonObj.get("cantCompraCC").toString()));
                        totalCompra += Long.parseLong(jsonObj.get("cantCompraCC").toString());
                    }
                    if (fcdNow.getFacturaCompraCab().isCc()) {
                        jsonNow.put("cantCompraSL", Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraSL").toString()));
                        totalCompra += Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraSL").toString());
                    } else {
                        jsonNow.put("cantCompraSL", Long.parseLong(jsonObj.get("cantCompraSL").toString()));
                        totalCompra += Long.parseLong(jsonObj.get("cantCompraSL").toString());
                    }
                    if (fcdNow.getFacturaCompraCab().isSc()) {
                        jsonNow.put("cantCompraSC", Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraSC").toString()));
                        totalCompra += Long.parseLong(fcdNow.getCantidad().intValue() + "") + Long.parseLong(jsonObj.get("cantCompraSC").toString());
                    } else {
                        jsonNow.put("cantCompraSC", Long.parseLong(jsonObj.get("cantCompraSC").toString()));
                        totalCompra += Long.parseLong(jsonObj.get("cantCompraSC").toString());
                    }
                    jsonNow.put("totalCompra", totalCompra);

                    jsonNow.put("cantCompraCCAN", Long.parseLong(jsonObj.get("cantCompraCCAN").toString()));
                    jsonNow.put("cantCompraSLAN", Long.parseLong(jsonObj.get("cantCompraSLAN").toString()));
                    jsonNow.put("cantCompraSCAN", Long.parseLong(jsonObj.get("cantCompraSCAN").toString()));
                    jsonNow.put("totalCompraAN", (Long.parseLong(jsonObj.get("cantCompraCCAN").toString()) + Long.parseLong(jsonObj.get("cantCompraSLAN").toString()) + Long.parseLong(jsonObj.get("cantCompraSCAN").toString())));

                    jsonNow.put("ventaCC", Long.parseLong(jsonObj.get("ventaCC").toString()));
                    jsonNow.put("ventaSL", Long.parseLong(jsonObj.get("ventaSL").toString()));
                    jsonNow.put("ventaSC", Long.parseLong(jsonObj.get("ventaSC").toString()));
                    jsonNow.put("ventaTotal", (Long.parseLong(jsonObj.get("ventaCC").toString()) + Long.parseLong(jsonObj.get("ventaSL").toString()) + Long.parseLong(jsonObj.get("ventaSC").toString())));

                    jsonNow.put("ventaCCAN", Long.parseLong(jsonObj.get("ventaCCAN").toString()));
                    jsonNow.put("ventaSLAN", Long.parseLong(jsonObj.get("ventaSLAN").toString()));
                    jsonNow.put("ventaSCAN", Long.parseLong(jsonObj.get("ventaSCAN").toString()));
                    jsonNow.put("ventaTotalAN", (Long.parseLong(jsonObj.get("ventaCCAN").toString()) + Long.parseLong(jsonObj.get("ventaSLAN").toString()) + Long.parseLong(jsonObj.get("ventaSCAN").toString())));

                    long exisCC = 0;
                    long exisSL = 0;
                    long exisSC = 0;
//                    long exisCC = existenciaDAO.getCantidadArticuloCC(fcdNow.getArticulo().getIdArticulo());
//                    long exisSL = existenciaDAO.getCantidadArticuloSL(fcdNow.getArticulo().getIdArticulo());
//                    long exisSC = existenciaDAO.getCantidadArticuloSC(fcdNow.getArticulo().getIdArticulo());

                    jsonNow.put("cantRepoCentral", 0);
                    jsonNow.put("cantRepoSanLorenzo", 0);
                    jsonNow.put("cantRepoCacique", 0);

                    jsonNow.put("exisCC", exisCC);
                    jsonNow.put("exisSL", exisSL);
                    jsonNow.put("exisSC", exisSC);
                    jsonNow.put("exisTotal", (exisCC + exisSL + exisSC));
                    jsonNow.put("precioCosto", fcdNow.getArticulo().getCosto());
                    jsonNow.put("valorizado", fcdNow.getArticulo().getCosto());
                    if (fcdNow.getSec2() == null) {
                        jsonNow.put("seccion", "");
                    } else if (fcdNow.getSec2().equals("null")) {
                        jsonNow.put("seccion", "");
                    } else {
                        jsonNow.put("seccion", fcdNow.getSec2());
                    }
                    mapeoNow.put(fcdNow.getArticulo().getCodArticulo(), jsonNow);

                } catch (ParseException ex) {
                    Logger.getLogger(MatrizFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                jsonNow.put("descripcion", fcdNow.getArticulo().getDescripcion());
                jsonNow.put("codigo", fcdNow.getArticulo().getCodArticulo());
                jsonNow.put("precio", fcdNow.getArticulo().getPrecioMin());
                jsonNow.put("costo", fcdNow.getArticulo().getCosto());
                try {
                    long numerador = fcdNow.getArticulo().getPrecioMin() - fcdNow.getArticulo().getCosto();
                    long denominador = fcdNow.getArticulo().getPrecioMin();
                    float porc = (float) numerador / denominador;
                    porc = porc * 100;
                    DecimalFormat decimalFormat = new DecimalFormat("#.##");
                    float prcent = Float.valueOf(decimalFormat.format(porc));
                    jsonNow.put("md", prcent + "");
                } catch (Exception e) {
                    jsonNow.put("md", "0");
                } finally {
                }
                jsonNow.put("ultimaCompra", fcdNow.getFacturaCompraCab().getFechaDoc().toString());
                try {
                    jsonNow.put("proveedor", txtProveedor.getText());
                } catch (Exception e) {
                } finally {
                }
                int totalCompra = 0;
                if (fcdNow.getFacturaCompraCab().isCc()) {
                    jsonNow.put("cantCompraCC", fcdNow.getCantidad().intValue());
                    totalCompra += fcdNow.getCantidad().intValue();
                } else {
                    jsonNow.put("cantCompraCC", 0);
                    totalCompra += 0;
                }
                if (fcdNow.getFacturaCompraCab().isSl()) {
                    jsonNow.put("cantCompraSL", fcdNow.getCantidad().intValue());
                    totalCompra += fcdNow.getCantidad().intValue();
                } else {
                    jsonNow.put("cantCompraSL", 0);
                    totalCompra += 0;
                }
                if (fcdNow.getFacturaCompraCab().isSc()) {
                    jsonNow.put("cantCompraSC", fcdNow.getCantidad().intValue());
                    totalCompra += fcdNow.getCantidad().intValue();
                } else {
                    jsonNow.put("cantCompraSC", 0);
                    totalCompra += 0;
                }
                jsonNow.put("totalCompra", totalCompra);

//                System.out.println("-8888-> " + artCompraMatrizDAO.getById(1966904).getCodigo());
//                long compraCCAN = facCompraDetDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//                long compraSLAN = facCompraDetDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//                long compradSCAN = facCompraDetDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
                long compraCCAN = artCompraMatrizDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
                long compraSLAN = artCompraMatrizDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
                long compradSCAN = artCompraMatrizDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
//                long compraCCAN = 0;
//                long compraSLAN = 0;
//                long compradSCAN = 0;
                jsonNow.put("cantCompraCCAN", compraCCAN);
                jsonNow.put("cantCompraSLAN", compraSLAN);
                jsonNow.put("cantCompraSCAN", compradSCAN);
                jsonNow.put("totalCompraAN", (compraCCAN + compraSLAN + compradSCAN));

//                long cantidadCC = facturaClienteCabDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//                long cantidadSL = facturaClienteCabDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//                long cantidadSC = facturaClienteCabDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
                long cantidadCC = artVentaMatrizDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
                long cantidadSL = artVentaMatrizDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
                long cantidadSC = artVentaMatrizDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeActual.getValue(), dpHastaActual.getValue());
//                long cantidadCC = 0;
//                long cantidadSL = 0;
//                long cantidadSC = 0;

                jsonNow.put("ventaCC", cantidadCC);
                jsonNow.put("ventaSL", cantidadSL);
                jsonNow.put("ventaSC", cantidadSC);
                jsonNow.put("ventaTotal", (cantidadCC + cantidadSL + cantidadSC));

//                long cantidadCCAN = 0;
//                long cantidadSLAN = 0;
//                long cantidadSCAN = 0;
                long cantidadCCAN = artVentaMatrizDAO.getCantidadArticuloCC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
                long cantidadSLAN = artVentaMatrizDAO.getCantidadArticuloSL(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());
                long cantidadSCAN = artVentaMatrizDAO.getCantidadArticuloSC(fcdNow.getArticulo().getCodArticulo() + "", dpDesdeAnterior.getValue(), dpHastaAnterior.getValue());

                jsonNow.put("ventaCCAN", cantidadCCAN);
                jsonNow.put("ventaSLAN", cantidadSLAN);
                jsonNow.put("ventaSCAN", cantidadSCAN);
                jsonNow.put("ventaTotalAN", (cantidadCCAN + cantidadSLAN + cantidadSCAN));

                long exisCC = 0;
                long exisSL = 0;
                long exisSC = 0;
//                long exisCC = existenciaDAO.getCantidadArticuloCC(fcdNow.getArticulo().getIdArticulo());
//                long exisSL = existenciaDAO.getCantidadArticuloSL(fcdNow.getArticulo().getIdArticulo());
//                long exisSC = existenciaDAO.getCantidadArticuloSC(fcdNow.getArticulo().getIdArticulo());

                jsonNow.put("exisCC", exisCC);
                jsonNow.put("exisSL", exisSL);
                jsonNow.put("exisSC", exisSC);
//                cantRepoCacique-cantRepoSanLorenzo-cantRepoCentral
                jsonNow.put("cantRepoCentral", 0);
                jsonNow.put("cantRepoSanLorenzo", 0);
                jsonNow.put("cantRepoCacique", 0);

                jsonNow.put("exisTotal", (exisCC + exisSL + exisSC));
                jsonNow.put("precioCosto", fcdNow.getArticulo().getCosto());
                jsonNow.put("valorizado", fcdNow.getArticulo().getCosto());
//                jsonNow.put("seccion", fcdNow.getSec2());
                if (fcdNow.getSec2() == null) {
                    jsonNow.put("seccion", "");
                } else if (fcdNow.getSec2().equals("null")) {
                    jsonNow.put("seccion", "");
                } else {
                    jsonNow.put("seccion", fcdNow.getSec2());
                }
                mapeoNow.put(fcdNow.getArticulo().getCodArticulo(), jsonNow);
            }
        }
        if (listNow.size() > 0) {
            Iterator entries = mapeoNow.entrySet().iterator();
            int orden = 0;
//            int ord = 0;
            List<JSONObject> list = new ArrayList<>();
            Map mapping = new HashMap();
            while (entries.hasNext()) {
                try {
                    Map.Entry entry = (Map.Entry) entries.next();
//            Integer key = (Integer) entry.getKey();
                    JSONObject value = (JSONObject) entry.getValue();
                    orden++;
                    value.put("orden", orden);
//            System.out.println("Key = " + key + ", Value = " + value);
                    matrizList.add(value);
                    detalleArtList.add(value);//si
                    JSONObject jsonCategoria = new JSONObject();
//                jsonCategoria.put("orden", orden);
                    jsonCategoria.put("categoria", value.get("seccion").toString());
                    if (!mapping.containsKey(value.get("seccion").toString())) {
                        if (!value.get("seccion").toString().equals("")) {
                            list.add(jsonCategoria);
                        }
                    }
                    mapping.put(value.get("seccion").toString(), value.get("seccion").toString());
                } catch (Exception e) {
                } finally {
                }
            }
            int ord = 1;
            for (JSONObject jSONObject : list) {
                jSONObject.put("orden", ord);
                categoriasList.add(jSONObject);
                ord++;
            }
            actualizandoTablaMatriz(matrizList);
            actualizarTablaCategoria();
        } else {
            if (!val) {
                mensajeAlerta("NO SE ENCUENTRAN RESULTADOS DE LA BUSQUEDA...");
            }
        }
    }

    private void actualizandoTablaMatriz(List<JSONObject> matrizList) {
        matrizData = FXCollections.observableArrayList(matrizList);
        numValidator = new NumberValidator();
        //columna Ruc .................................................
        columnOpcion.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
                return new BooleanCell(tableViewFactura);
            }
        };
        columnOpcion.setCellValueFactory((data) -> {
            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
            try {
                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getBigInteger("codigo")).toString())) {
                    return new SimpleBooleanProperty(true);
                } else {
                    return new SimpleBooleanProperty(false);
                }
            } catch (Exception e) {
                return new SimpleBooleanProperty(false);
            } finally {
            }
        });
        columnOpcion.setCellFactory(booleanCellFactory);
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("orden"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("codigo"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("descripcion"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnFechaUlt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFechaUlt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ultimaCompra"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnPrecio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPrecio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("precio"));
                //                return new ReadOnlyStringWrapper(numValidator.numberFormat("###,###.###", Double.parseDouble(orden)));
                try {
                    return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(orden + ""))));
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("Gs 0");
                } finally {
                }
//                return new ReadOnlyStringWrapper(orden);
            }
        });
        columnCosto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("costo"));
//                return new ReadOnlyStringWrapper(numValidator.numberFormat("###,###.###", Double.parseDouble(orden)));
//                return new ReadOnlyStringWrapper(orden);
                try {
                    return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(orden + ""))));
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("Gs 0");
                } finally {
                }
            }
        });
        columnMD.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnMD.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("md"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }

            }
        });
        columnCompraCentral.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraCentral.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraCC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraSanLo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraSanLo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraSL"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraCacique.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraCacique.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraSC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("totalCompra"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraCentralAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraCentralAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraCCAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraSanLoAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraSanLoAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraSLAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCompraCaciqueAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraCaciqueAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantCompraSCAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
//        columnCompraCaciqueAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        columnCompraCaciqueAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String orden = String.valueOf(data.getValue().get("totalCompraAN"));
//                return new ReadOnlyStringWrapper(orden);
//            }
//        });
        columnCompraTotalAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCompraTotalAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("totalCompraAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaCentral.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaCentral.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaCC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaSanLo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaSanLo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaSL"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaCacique.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaCacique.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaSC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaTotal"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaCentralAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaCentralAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaCCAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaSanLoAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaSanLoAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaSLAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaCaciqueAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaCaciqueAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaSCAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnVentaTotalAnt.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnVentaTotalAnt.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("ventaTotalAN"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnExisCentral.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExisCentral.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("exisCC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnExisSanLo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExisSanLo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("exisSL"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnExisCacique.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExisCacique.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("exisSC"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnExisTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExisTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("exisTotal"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnExisTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnExisTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("exisTotal"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCantidadRepoCentral.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadRepoCentral.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantRepoCentral"));
                return new ReadOnlyStringWrapper(orden);
            }
        });
        columnCantidadRepoCentral.setOnEditCommit(
                (TableColumn.CellEditEvent<JSONObject, String> t)
                -> (t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).put("cantRepoCentral", t.getNewValue())
        );
        columnCantidadRepoSanLo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadRepoSanLo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantRepoSanLorenzo"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCantidadRepoSanLo.setOnEditCommit(
                (TableColumn.CellEditEvent<JSONObject, String> t)
                -> (t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).put("cantRepoSanLorenzo", t.getNewValue())
        );
        columnCantidadRepoCacique.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadRepoCacique.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("cantRepoCacique"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("");
                } finally {
                }
            }
        });
        columnCantidadRepoCacique.setOnEditCommit(
                (TableColumn.CellEditEvent<JSONObject, String> t)
                -> (t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).put("cantRepoCacique", t.getNewValue())
        );
        columnPrecioCosto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPrecioCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("precioCosto"));
//                return new ReadOnlyStringWrapper(numValidator.numberFormat("###,###.###", Double.parseDouble(orden)));
//                return new ReadOnlyStringWrapper(orden);
                try {
                    return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(orden + ""))));
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("Gs 0");
                } finally {
                }
            }
        });
        columnValorizado.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnValorizado.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("valorizado"));
//                return new ReadOnlyStringWrapper(numValidator.numberFormat("###,###.###", Double.parseDouble(orden)));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("Gs 0");
                } finally {
                }
            }
        });
        columnSeccion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSeccion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("seccion"));
                try {
                    return new ReadOnlyStringWrapper(orden);
                } catch (Exception ex) {
                    return new ReadOnlyStringWrapper("Gs 0");
                } finally {
                }
            }
        });
        //columna Ruc .................................................
        tableViewFactura.setItems(matrizData);
    }

    @FXML
    private void tableViewFacturaMouseClicked(MouseEvent event) {
//        if (event.getClickCount() == 2) {
//            sucursal = cbSucursal.getSelectionModel().getSelectedItem();
////            System.out.println("-->> " + tableViewFactura.getSelectionModel().getSelectedItem().toString());
//            if ("cliente_caja")) {
////            BuscarClienteMayFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
//                posicion = tableViewFactura.getSelectionModel().getSelectedIndex() + "";
//                ModCantidadReponerFXMLController.setCantidad(tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoCentral").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoSanLorenzo").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantRepoCacique").toString(), tableViewFactura);
//                this.sc.loadScreenModal("/vista/stock/ModCantidadReponerFXML.fxml", 232, 178, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//
//            }
//        }
    }

    private void actualizarTablaCategoria() {
        categoriaData = FXCollections.observableArrayList(categoriasList);

        tableColumnNroSeccion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnNroSeccion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String orden = String.valueOf(ord);
//                System.out.println("ORDEN: " + orden);
                String orden = String.valueOf(data.getValue().get("orden"));
                return new ReadOnlyStringWrapper(orden);
            }
        });
        tableColumnSeccion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnSeccion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String orden = String.valueOf(data.getValue().get("categoria"));
                return new ReadOnlyStringWrapper(orden);
            }
        });
        //columna Ruc .................................................
        tableViewSeccion.setItems(categoriaData);
    }

    private void cargarMoneda() {
        cbMonedaExtran.getItems().addAll("--SELECCIONE MONEDA--", "DOLAR", "REAL", "PESO");

    }

    @FXML
    private void btnSubirAction(ActionEvent event) {
        try {
            importando();
        } catch (IOException ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
        } catch (InvalidFormatException ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
        }
    }

    @FXML
    private void btnBajarAction(ActionEvent event) {
        generarExcel();
    }

    private void imprimirNow() {
        if (cbImpresion.getSelectionModel().getSelectedIndex() > 0) {
            if (matrizList.size() >= 0) {
                if (cbImpresion.getSelectionModel().getSelectedIndex() == 1) {
                    if (!txtPlazo.getText().equals("") && !txtFrecuencia.getText().equals("")) {
                        imprimirMatrizComprador();
                    } else {
                        mensajeAlerta("EL CAMPO PLAZO Y FRECUENCIA NO DEBE QUEDAR VACIO");
                        repeatFocus(txtPlazo);
                    }
                } else {
                    if (!txtPlazo.getText().equals("") && !txtFrecuencia.getText().equals("")) {
                        imprimirMatrizVendedor();
                    } else {
                        mensajeAlerta("EL CAMPO PLAZO Y FRECUENCIA NO DEBE QUEDAR VACIO");
                        repeatFocus(txtPlazo);
                    }
                }
            } else {
                mensajeAlerta("NO HAY DETALLE EN LA MATRIZ PARA IMPRIMIR..");
            }
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN MODO DE IMPRESION..");
            repeatFocus(cbImpresion);
        }
    }

    private void filtrando() {
        if (tableViewSeccion.getSelectionModel().getSelectedIndex() >= 0) {
            String categoria = tableViewSeccion.getSelectionModel().getSelectedItem().get("categoria").toString();
            List<JSONObject> listJSON = matrizList;
            List<JSONObject> listaJSON = new ArrayList<>();
            detalleArtList = new ArrayList<>();
            for (JSONObject jSONObject : listJSON) {
//                String ord = jSONObject.get("orden").toString() + "-" + jSONObject.get("codigo").toString();
                if (jSONObject.get("seccion").toString().toUpperCase().equalsIgnoreCase(categoria.toUpperCase())) {
                    listaJSON.add(jSONObject);
                }
            }
            actualizandoTablaMatriz(listaJSON);
//            mantenerSeleccionados();
            System.out.println("-->> " + categoria);
        }
    }

    class BooleanCell extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCell(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
//                    table.getSelectionModel().select(getTableRow().getIndex());
//                    int posicion = tableViewFactura.getSelectionModel().getSelectedIndex();
                    int posicion = getTableRow().getIndex();
                    if (posicion >= 0) {
//                        String ord = tableViewFactura.getSelectionModel()
//                                .getSelectedItem().get("orden") + "-" + tableViewFactura.getSelectionModel()
//                                .getSelectedItem().get("codigo");
                        String ord = tableViewFactura.getItems().get(posicion)
                                .get("orden") + "-" + tableViewFactura.getItems().get(posicion)
                                .get("codigo");
                        if (checkBox.isSelected()) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
//                            System.out.println(ord + " | " + true);
                            tableViewFactura.getSelectionModel().getSelectedItem().put("check", true);
                        } else {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
//                            System.out.println(ord + " | " + false);
                            tableViewFactura.getSelectionModel().getSelectedItem().put("check", false);
                        }
                    }
                }
            });
//            this.setGraphic(checkBox);
//            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            this.setEditable(true);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    private void mantenerSeleccionados() {
//        if (!estadoCheck) {
//            long total = 0;
        suprimirSelecciones();
        Map mapping = new HashMap();
        List<JSONObject> list = new ArrayList<>();
        for (JSONObject json : getDetalleArtList()) {
            org.json.JSONObject jsonDat = new org.json.JSONObject(json);
            mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codigo"), true);
            JSONObject jsonCategoria = new JSONObject();
            jsonCategoria.put("orden", jsonDat.getInt("orden"));
//            jsonCategoria.put("categoria", jsonDat.getString("seccion"));
//            categoriasList.add(jsonCategoria);
//                total += Math.round(Long.parseLong(json.get("precio").toString()) * Double.parseDouble(json.get("cantidad").toString()));
            if (!mapping.containsKey(jsonDat.get("seccion").toString())) {
                if (!jsonDat.get("seccion").toString().equals("")) {
                    jsonCategoria.put("categoria", jsonDat.get("seccion").toString());
                    list.add(jsonCategoria);
                }
            }
            mapping.put(jsonDat.get("seccion").toString(), jsonDat.get("seccion").toString());
        }
        int ord = 1;
        for (JSONObject jSONObject : list) {
            jSONObject.put("orden", ord);
            categoriasList.add(jSONObject);
            ord++;
        }
        tableViewFactura.getItems().clear();
        actualizandoTablaMatriz(matrizList);
        actualizarTablaCategoria();
        chkLocal.setSelected(true);
        cbSucursal.getSelectionModel().select("CASA CENTRAL");
        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
//        } else {
//            suprimirSelecciones();
//        }
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    private void suprimirSelecciones() {
        JSONParser parser = new JSONParser();
//        int val = 0;
//        long tam = tableViewFactura.getItems().size();
        List aEliminar = new ArrayList();
        categoriasList = new ArrayList<>();
//        ObservableList<JSONObject> ol = tableViewFactura.getItems();
        for (JSONObject item : tableViewFactura.getItems()) {
//            org.json.JSONObject json = new org.json.JSONObject(item);
            JSONObject jsonArti = new JSONObject();
            try {
//                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
                boolean estado = Boolean.parseBoolean(mapeoCheck.get(item.get("orden").toString() + "-" + item.get("codigo").toString()).toString());
                if (estado == false) {
//                    System.out.println("-STATUS : " + tableViewFactura.getItems().remove(item));
//                    hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                    hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                    aEliminar.add(item);
                }
            } catch (Exception e) {
//                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                aEliminar.add(item);
            } finally {
            }
//            columnOpcion.setVisible(false);
        }

        for (int i = 0; i < aEliminar.size(); i++) {
            try {
                System.out.println("-> " + aEliminar.get(i));
                detalleArtList.remove(aEliminar.get(i));
                matrizList.remove(aEliminar.get(i));
                System.out.println("-STATUS : " + tableViewFactura.getItems().remove(aEliminar.get(i)));
            } catch (Exception e) {
            } finally {
            }
        }
//        actualizarTotales(false);
    }

    private void generarPedidos() {
        boolean valor = false;
        try {
            cbSucursal.getSelectionModel().getSelectedItem().equals("");
            valor = true;
        } catch (Exception e) {
            mensajeAlerta("EL CAMPO PLAZO y SUCURSAL NO DEBE QUEDAR VACIO...");
        }
        if (valor) {
            if (txtPlazo.getText().equals("")) {
                mensajeAlerta("EL CAMPO PLAZO y SUCURSAL NO DEBE QUEDAR VACIO...");
            } else {
                PedidoCab pedidoCab = new PedidoCab();

                Date date = new Date();
                Timestamp tsNow = new Timestamp(date.getTime());
                pedidoCab.setFecha(tsNow);
                pedidoCab.setNroPedido(rangoPedidoDAO.actualizarObtenerRangoActual(1L) + "");
                pedidoCab.setPlazo(txtPlazo.getText());
                Proveedor prov = new Proveedor();
                prov.setIdProveedor(Long.parseLong(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString()));
                pedidoCab.setProveedor(prov);
                pedidoCab.setUsuario(Identity.getNomFun());
                pedidoCab.setSucursal(cbSucursal.getSelectionModel().getSelectedItem());
                pedidoCab.setTotal(0l);
                if (cbMonedaExtran.getSelectionModel().getSelectedItem().equals("--SELECCIONE MONEDA--")) {
                    pedidoCab.setMoneda("GUARANIES");
                    pedidoCab.setCotizacion("0");
                } else {
                    pedidoCab.setMoneda(cbMonedaExtran.getSelectionModel().getSelectedItem());
                    pedidoCab.setCotizacion(txtCotizacion.getText());
                }

                PedidoCab pCab = pedidoCabDAO.insertarObtenerObj(pedidoCab);
                try {
                    if (pCab.getIdPedidoCab() != null) {
                        if (matrizList.size() > 0) {
                            long i = 0;
                            for (JSONObject detalle : matrizList) {
                                i++;
                                PedidoDet pd = new PedidoDet();
                                Articulo art = new Articulo();
                                art.setIdArticulo(artDAO.buscarCod(detalle.get("codigo").toString()).getIdArticulo());
                                pd.setArticulo(art);
                                pd.setCantCentral(detalle.get("cantRepoCentral").toString());
                                pd.setCantSanlo(detalle.get("cantRepoSanLorenzo").toString());
                                pd.setCantCacique(detalle.get("cantRepoCacique").toString());
                                pd.setDescripcion(detalle.get("descripcion").toString());
                                PedidoCab pc = new PedidoCab();
                                pc.setIdPedidoCab(pCab.getIdPedidoCab());
                                pd.setPedidoCab(pc);
                                pd.setPrecio(Long.parseLong(detalle.get("costo").toString()));
//                PedidoDe fcd = gson.fromJson(detalle.toString(), FacturaCompraDet.class);
                                try {
                                    pedidoDetDAO.insercionMasiva(pd, i);
                                } catch (Exception e) {
                                    System.out.println("-> " + e.getLocalizedMessage());
                                    System.out.println("-> " + e.getMessage());
                                    System.out.println("-> " + e.fillInStackTrace());
                                } finally {
                                }

                            }
                            limpiarCampos();
//                        secondPane1.setVisible(false);
//                        mensajeAlerta("COMPRA HA SIDO AGREGADO EXITOSAMENTE...");
                            mensajeDetalle("EL PEDIDO HA SIDO GENERADO... N° de Pedido: " + pedidoCab.getNroPedido(), "MENSAJE DEL SISTEMA");

//                        btnVerificarPrecio.setDisable(true);
                            toaster = new Toaster();
//                        btnVerificarPrecio.setDisable(true);
                            tableViewFactura.getItems().clear();
                            detalleArtList = new ArrayList<>();
                            orden = 0;
//            ubicandoContenedorSecundario();
//            cargarTipoMovimiento();
//            cargarDeposito();
//            cargarTipo();
//            cargarMedida();
//            cargarSucursal();
//            cargarSituacion();
//            cargarClaseMovimiento();
//                        repeatFocus(txtNroEntrada);
                        } else {
                            mensajeAlerta("NO SE HA CARGADO NINGUN DETALLE A LA FACTURA");
                        }
                    }
                } catch (Exception e) {
                    mensajeAlerta("NO SE HAN INSERTADO LOS DATOS DE LA CABECERA...");
                }
            }
        }

    }

    private void buscarArticuloEnTabla() {
        for (int i = 0; i < tableViewFactura.getItems().size(); i++) {
            JSONObject json = (JSONObject) tableViewFactura.getItems().get(i);
            if (json.get("codigo").toString().equalsIgnoreCase(txtCodigo.getText())) {
                tableViewFactura.requestFocus();
                tableViewFactura.getSelectionModel().select(i);
                tableViewFactura.scrollTo(json);
                break;
            }
        }
//        for (JSONObject detalle : tableViewFactura.getItems()) {
//            PedidoDet pd = new PedidoDet();
//            Articulo art = new Articulo();
//            art.setIdArticulo(artDAO.buscarCod(detalle.get("codigo").toString()).getIdArticulo());
//            pd.setArticulo(art);
//            pd.setCantCentral(detalle.get("cantRepoCentral").toString());
//            pd.setCantSanlo(detalle.get("cantRepoSanLorenzo").toString());
//            pd.setCantCacique(detalle.get("cantRepoCacique").toString());
//            pd.setDescripcion(detalle.get("descripcion").toString());
//            PedidoCab pc = new PedidoCab();
//            pc.setIdPedidoCab(pCab.getIdPedidoCab());
//            pd.setPedidoCab(pc);
//            pd.setPrecio(Long.parseLong(detalle.get("precio").toString()));
////                PedidoDe fcd = gson.fromJson(detalle.toString(), FacturaCompraDet.class);
//            pedidoDetDAO.insercionMasiva(pd, i);
//        }
    }

    private void imprimirMatrizComprador() {
        JSONArray jsonFacturas = recuperarCompras();

//        if (jsonFacturas.isEmpty()) {
//            Date dates = new Date();
//            Timestamp ts = new Timestamp(dates.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//            informandoVentas("{\"ventas\": [{\"orden\":\"N/A\",\"codigo\":\"N/A\",\"montoFactura\":0,"
//                    + "\"descripcion\":\"N/A\",\"recepcion\":\"\",\"conteo\":\"\",\"observacion\":\"\",\"porcDescTarjeta\":\"0\","
//                    + "\"fiel\":0,\"porcDescFiel\":\"0\",\"func\":0,\"porcDescFunc\":\"0\","
//                    + "\"promoArt\":0,\"porcDescPromoArt\":\"0\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"promoSec\":0,\"porcDescPromoSec\":\"0\"}]}");
//
//        } else {
//            JSONArray array = jsonFacturas;
        informandoVentas("{\"ventas\": " + jsonFacturas + "}", "REP_MATRIZ_COMPRA");
//        }
        this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void imprimirMatrizVendedor() {
        JSONArray jsonFacturas = recuperarVentas();

//        if (jsonFacturas.isEmpty()) {
//            Date dates = new Date();
//            Timestamp ts = new Timestamp(dates.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
//            informandoVentas("{\"ventas\": [{\"orden\":\"N/A\",\"codigo\":\"N/A\",\"montoFactura\":0,"
//                    + "\"descripcion\":\"N/A\",\"recepcion\":\"\",\"conteo\":\"\",\"observacion\":\"\",\"porcDescTarjeta\":\"0\","
//                    + "\"fiel\":0,\"porcDescFiel\":\"0\",\"func\":0,\"porcDescFunc\":\"0\","
//                    + "\"promoArt\":0,\"porcDescPromoArt\":\"0\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"promoSec\":0,\"porcDescPromoSec\":\"0\"}]}");
//
//        } else {
//            JSONArray array = jsonFacturas;
        informandoVentas("{\"ventas\": " + jsonFacturas + "}", "REP_MATRIZ_VENTA");
//        }
        this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private JSONArray recuperarCompras() {
        numValidator = new NumberValidator();
        JSONArray arrayCompra = new JSONArray();
        int sumValorizado = 0;

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaInicial;
        int days = 0;
        int weeks = 0;
        try {
            fechaInicial = dateFormat.parse(dpDesdeAnterior.getValue().toString());
            Date fechaFinal = dateFormat.parse(dpHastaAnterior.getValue().toString());
            days = (int) ((fechaFinal.getTime() - fechaInicial.getTime()) / 86400000);

            System.out.println("a) -> " + dpDesdeAnterior.getValue().getYear() + "-" + (fechaInicial.getMonth() + 1) + "-" + dpDesdeAnterior.getValue().getDayOfMonth());
            System.out.println("b) -> " + dpHastaAnterior.getValue().getYear() + "-" + (fechaFinal.getMonth() + 1) + "-" + dpHastaAnterior.getValue().getDayOfMonth());
            Calendar a = new GregorianCalendar(dpDesdeAnterior.getValue().getYear(), (fechaInicial.getMonth() + 1), dpDesdeAnterior.getValue().getDayOfMonth());
            Calendar b = new GregorianCalendar(dpHastaAnterior.getValue().getYear(), (fechaFinal.getMonth() + 1), dpHastaAnterior.getValue().getDayOfMonth());
            weeks = b.get(Calendar.WEEK_OF_YEAR) - a.get(Calendar.WEEK_OF_YEAR);
        } catch (java.text.ParseException ex) {
            Logger.getLogger(MatrizFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (JSONObject detalle : matrizList) {
            JSONObject json = new JSONObject();
            json.put("codigo", detalle.get("codigo").toString());
            json.put("num", rangoPedidoDAO.recuperarActual());
            json.put("descripcion", detalle.get("descripcion").toString());
            json.put("gondola", "12");
            json.put("precio", Integer.parseInt(detalle.get("precio").toString()));
            json.put("costo", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("costo").toString())));
            json.put("md", detalle.get("md").toString());
            json.put("ultcompra", detalle.get("ultimaCompra").toString());
            json.put("compracentral", detalle.get("cantCompraCC").toString());
            json.put("comprasanlo", detalle.get("cantCompraSL").toString());
            json.put("compracacique", detalle.get("cantCompraSC").toString());
            json.put("compratotal", detalle.get("totalCompra").toString());

            json.put("ventacentral", detalle.get("ventaCC").toString());
            json.put("ventasanlo", detalle.get("ventaSL").toString());
            json.put("ventacacique", detalle.get("ventaSC").toString());
            json.put("ventatotal", detalle.get("ventaTotal").toString());

            json.put("exiscentral", detalle.get("exisCC").toString());
            json.put("exisanlo", detalle.get("exisSL").toString());
            json.put("exiscacique", detalle.get("exisSC").toString());
            json.put("existotal", detalle.get("exisTotal").toString());

            json.put("repocentral", detalle.get("cantRepoCentral").toString());
            json.put("reposanlo", detalle.get("cantRepoSanLorenzo").toString());
            json.put("repocacique", detalle.get("cantRepoCacique").toString());
            json.put("repototal", "0");

            json.put("preciocosto", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("precioCosto").toString())));
            json.put("valorizad", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("valorizado").toString())));

            sumValorizado += Integer.parseInt(detalle.get("valorizado").toString());
            json.put("valCosto", numValidator.numberFormat("###,###.###", Double.parseDouble(sumValorizado + "")));
            json.put("valUltCompra", numValidator.numberFormat("###,###.###", Double.parseDouble("0")));
            json.put("valExis", numValidator.numberFormat("###,###.###", Double.parseDouble("0")));
            json.put("seccion", detalle.get("seccion").toString());
            json.put("seccion", detalle.get("seccion").toString());

            json.put("seccion", detalle.get("seccion").toString());
            json.put("difDia", days);
            json.put("difSemana", weeks);

            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
            StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
            json.put("subRepNomFun", Identity.getNomFun());
            json.put("subRepTimestamp", subRepTimestamp);
            json.put("sucursal", "CASA CENTRAL");

            json.put("fecDesde", dpDesdeActual.getValue().toString());
            json.put("fecHasta", dpHastaActual.getValue().toString());

            json.put("fecDesdeAn", dpDesdeAnterior.getValue().toString());
            json.put("fecHastaAn", dpHastaAnterior.getValue().toString());

            json.put("visita", "LUNES");
            json.put("proveedor", txtProveedor.getText());
//            json.put("proveedor", detalle.get("proveedor").toString());

            arrayCompra.add(json);
        }
        return arrayCompra;
    }

    private JSONArray recuperarVentas() {
        numValidator = new NumberValidator();
        JSONArray arrayCompra = new JSONArray();
        for (JSONObject detalle : matrizList) {
            JSONObject json = new JSONObject();
            json.put("codigo", detalle.get("codigo").toString());
            json.put("num", rangoPedidoDAO.recuperarActual());
            json.put("descripcion", detalle.get("descripcion").toString());
            json.put("gondola", "12");
            json.put("precio", Integer.parseInt(detalle.get("precio").toString()));
            json.put("costo", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("costo").toString())));
            json.put("md", detalle.get("md").toString());
            json.put("ultcompra", detalle.get("ultimaCompra").toString());
            json.put("compracentral", detalle.get("cantCompraCC").toString());
            json.put("comprasanlo", detalle.get("cantCompraSL").toString());
            json.put("compracacique", detalle.get("cantCompraSC").toString());
            json.put("compratotal", detalle.get("totalCompra").toString());

            json.put("ventacentral", detalle.get("ventaCC").toString());
            json.put("ventasanlo", detalle.get("ventaSL").toString());
            json.put("ventacacique", detalle.get("ventaSC").toString());
            json.put("ventatotal", detalle.get("ventaTotal").toString());

            json.put("exiscentral", detalle.get("exisCC").toString());
            json.put("exisanlo", detalle.get("exisSL").toString());
            json.put("exiscacique", detalle.get("exisSC").toString());
            json.put("existotal", detalle.get("exisTotal").toString());

            json.put("repocentral", detalle.get("cantRepoCentral").toString());
            json.put("reposanlo", detalle.get("cantRepoSanLorenzo").toString());
            json.put("repocacique", detalle.get("cantRepoCacique").toString());
            json.put("repototal", "0");

            json.put("preciocosto", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("precioCosto").toString())));
            json.put("valorizad", numValidator.numberFormat("###,###.###", Double.parseDouble(detalle.get("valorizado").toString())));

            json.put("seccion", detalle.get("seccion").toString());
            json.put("plazo", txtPlazo.getText());
            json.put("frecuencia", txtFrecuencia.getText());

            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
            StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
            json.put("subRepNomFun", Identity.getNomFun());
            json.put("subRepTimestamp", subRepTimestamp);
            json.put("sucursal", "CASA CENTRAL");

            json.put("fecDesde", dpDesdeActual.getValue().toString());
            json.put("fecHasta", dpHastaActual.getValue().toString());

            json.put("fecDesdeAn", dpDesdeAnterior.getValue().toString());
            json.put("fecHastaAn", dpHastaAnterior.getValue().toString());

            json.put("visita", "LUNES");
            json.put("proveedor", txtProveedor.getText());
//            json.put("proveedor", detalle.get("proveedor").toString());

            arrayCompra.add(json);
        }
        return arrayCompra;
    }

    private void informandoVentas(String jsonString, String rep) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + rep + ".jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + rep + ".pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
//            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
//            pSQL.put("repCodPeluq", codPeluq);
//            pSQL.put("nroFactura", txtNUmFactura);
            // CABECERA - SUB REPORTE
//            Sucursal suc = sucDAO.consultandoSuc(1L);
//            pSQL.put("proveedor", proveedor);
            pSQL.put("empresa", "PARANA FUNCIONAL S.A.");
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
//            pSQL.put("subRepSucursal", suc.getDescripcion());
//            pSQL.put("subRepSucursalDir", "");
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            Date date = new Date();
//            Timestamp ts = new Timestamp(date.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String horaArray[] = ts.toString().split(".");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + horaArray[0];
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            pSQL.put("subRepTimestamp", subRepTimestamp);
//            pSQL.put("subRepPathLogo", subImg);
//            pSQL.put("subRepPathLogoCP", subImgCP);

            pSQL.put("SUBREPORT_DIR", subHeader);
            // CABECERA - SUB REPORTE
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }

    public static void generarExcel() {
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat, 
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Employee");

        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());

        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        // Create Cell Style for formatting Date
        CellStyle dateCellStyle = workbook.createCellStyle();
        dateCellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd-MM-yyyy"));

        // Create Other rows and cells with employees data
        int rowNum = 1;
        for (JSONObject employee : matrizList) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0)
                    .setCellValue(employee.get("orden").toString());

            row.createCell(1)
                    .setCellValue(employee.get("codigo").toString());

//            Cell dateOfBirthCell = row.createCell(2);
//            dateOfBirthCell.setCellValue(employee.getDateOfBirth());
//            dateOfBirthCell.setCellStyle(dateCellStyle);
            row.createCell(2)
                    .setCellValue(employee.get("descripcion").toString());
            row.createCell(3)
                    .setCellValue(employee.get("seccion").toString());
            row.createCell(4)
                    .setCellValue(employee.get("ultimaCompra").toString());
            row.createCell(5)
                    .setCellValue(employee.get("precio").toString());
            row.createCell(6)
                    .setCellValue(employee.get("costo").toString());
            row.createCell(7)
                    .setCellValue(employee.get("md").toString());
            row.createCell(8)
                    .setCellValue(employee.get("cantCompraCC").toString());
            row.createCell(9)
                    .setCellValue(employee.get("cantCompraSL").toString());
            row.createCell(10)
                    .setCellValue(employee.get("cantCompraSC").toString());
            row.createCell(11)
                    .setCellValue(employee.get("totalCompra").toString());
            row.createCell(12)
                    .setCellValue(employee.get("cantCompraCCAN").toString());
            row.createCell(13)
                    .setCellValue(employee.get("cantCompraSLAN").toString());
            row.createCell(14)
                    .setCellValue(employee.get("cantCompraSCAN").toString());
            row.createCell(15)
                    .setCellValue(employee.get("totalCompraAN").toString());
            row.createCell(16)
                    .setCellValue(employee.get("ventaCC").toString());
            row.createCell(17)
                    .setCellValue(employee.get("ventaSL").toString());
            row.createCell(18)
                    .setCellValue(employee.get("ventaSC").toString());
            row.createCell(19)
                    .setCellValue(employee.get("ventaTotal").toString());
            row.createCell(20)
                    .setCellValue(employee.get("ventaCCAN").toString());
            row.createCell(21)
                    .setCellValue(employee.get("ventaSLAN").toString());
            row.createCell(22)
                    .setCellValue(employee.get("ventaSCAN").toString());
            row.createCell(23)
                    .setCellValue(employee.get("ventaTotalAN").toString());
            row.createCell(24)
                    .setCellValue(employee.get("exisCC").toString());
            row.createCell(25)
                    .setCellValue(employee.get("exisSL").toString());
            row.createCell(26)
                    .setCellValue(employee.get("exisSC").toString());
            row.createCell(27)
                    .setCellValue(employee.get("exisTotal").toString());
            row.createCell(28)
                    .setCellValue(employee.get("cantRepoCentral").toString());
            row.createCell(29)
                    .setCellValue(employee.get("cantRepoSanLorenzo").toString());
            row.createCell(30)
                    .setCellValue(employee.get("cantRepoCacique").toString());
            row.createCell(31)
                    .setCellValue(employee.get("precioCosto").toString());
            row.createCell(32)
                    .setCellValue(employee.get("valorizado").toString());
        }

        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        FileOutputStream fileOut;
        try {
//            fileOut = new FileOutputStream("C:\\Users\\sebastian\\Desktop\\BU\\poi-generated-file.xlsx");
            fileOut = new FileOutputStream(guardando());
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MatrizFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MatrizFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static String guardando() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XLSX Document", Arrays.asList("*.xlsx", "*.XLSX")));
        File file = fileChooser.showSaveDialog(parentStage);
        return file.getAbsolutePath();
    }

    private static String importar() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import File");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("XLSX Document", Arrays.asList("*.xlsx", "*.XLSX")));
        File file = fileChooser.showOpenDialog(parentStage);
        return file.getAbsolutePath();
    }

    private void importando() throws IOException, InvalidFormatException {
        String SAMPLE_XLSX_FILE_PATH = importar();
        // Creating a Workbook from an Excel file (.xls or .xlsx)
        Workbook workbook = WorkbookFactory.create(new File(SAMPLE_XLSX_FILE_PATH));

        // Retrieving the number of sheets in the Workbook
        System.out.println("Workbook has " + workbook.getNumberOfSheets() + " Sheets : ");

        /*
           =============================================================
           Iterating over all the sheets in the workbook (Multiple ways)
           =============================================================
         */
        // 1. You can obtain a sheetIterator and iterate over it
        Iterator<Sheet> sheetIterator = workbook.sheetIterator();
        System.out.println("Retrieving Sheets using Iterator");
        while (sheetIterator.hasNext()) {
            Sheet sheet = sheetIterator.next();
            System.out.println("=> " + sheet.getSheetName());
        }
//
//        // 2. Or you can use a for-each loop
        System.out.println("Retrieving Sheets using for-each loop");
        for (Sheet sheet : workbook) {
            System.out.println("=> " + sheet.getSheetName());
        }
//
//        // 3. Or you can use a Java 8 forEach with lambda
        System.out.println("Retrieving Sheets using Java 8 forEach with lambda");
        workbook.forEach(sheet -> {
            System.out.println("=> " + sheet.getSheetName());
        });

        /*
           ==================================================================
           Iterating over all the rows and columns in a Sheet (Multiple ways)
           ==================================================================
         */
        // Getting the Sheet at index zero
        Sheet sheet = workbook.getSheetAt(0);

        // Create a DataFormatter to format and get each cell's value as String
        DataFormatter dataFormatter = new DataFormatter();

        // 1. You can obtain a rowIterator and columnIterator and iterate over them
        System.out.println("\n\nIterating over Rows and Columns using Iterator\n");
        Iterator<Row> rowIterator = sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();

            // Now let's iterate over the columns of the current row
            Iterator<Cell> cellIterator = row.cellIterator();
//            int num = 0;
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String cellValue = dataFormatter.formatCellValue(cell);
                System.out.print(cellValue + "\t");
//                System.out.println("-->> " + num + ") " + cellValue);
            }
            System.out.println();
//            num++;
        }
//
//        // 2. Or you can use a for-each loop to iterate over the rows and columns
//        System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
//        for (Row row : sheet) {
//            int num = 0;
//            for (Cell cell : row) {
//                String cellValue = dataFormatter.formatCellValue(cell);
//                System.out.print(cellValue + "\t");
//                System.out.println("-->> " + num + ") " + cellValue);
//            }
//            num++;
//            System.out.println();
//        }
        // 3. Or you can use Java 8 forEach loop with lambda
        System.out.println("\n\nIterating over Rows and Columns using Java 8 forEach with lambda\n");
        Map mapeo = new HashMap();
        mapeo.put("estado", true);
        mapeo.put("num", 0);
        mapeo.put("numero", 0);
//        mapeo.put("orden", 0);
        matrizList = new ArrayList<>();
        categoriasList = new ArrayList<>();
        mapeoNow = new HashMap();
//        int num = 0;
        sheet.forEach(row -> {

            JSONObject json = new JSONObject();
            if (Integer.parseInt(mapeo.get("numero").toString()) > 0) {
                mapeo.put("estado", false);
            }
            row.forEach(cell -> {
                try {
                    String cellValue = dataFormatter.formatCellValue(cell);
                    if (Boolean.parseBoolean(mapeo.get("estado").toString())) {
                    } else {
                        mapeo.put("estado", false);
                        switch (Integer.parseInt(mapeo.get("num").toString())) {
                            case 0:
                                mapeo.put("num", 1);
                                json.put("orden", cellValue);
                                break;
                            case 1:
                                mapeo.put("num", 2);
                                json.put("codigo", cellValue);
                                break;
                            case 2:
                                mapeo.put("num", 3);
                                json.put("descripcion", cellValue);
                                break;
                            case 3:
                                mapeo.put("num", 4);
                                json.put("seccion", cellValue);
                                break;
                            case 4:
                                mapeo.put("num", 5);
                                json.put("ultimaCompra", cellValue);
                                break;
                            case 5:
                                mapeo.put("num", 6);
                                json.put("precio", cellValue);
                                break;
                            case 6:
                                mapeo.put("num", 7);
                                json.put("costo", cellValue);
                                break;
                            case 7:
                                mapeo.put("num", 8);
                                json.put("md", cellValue);
                                break;
                            case 8:
                                mapeo.put("num", 9);
                                json.put("cantCompraCC", cellValue);
                                break;
                            case 9:
                                mapeo.put("num", 10);
                                json.put("cantCompraSL", cellValue);
                                break;
                            case 10:
                                mapeo.put("num", 11);
                                json.put("cantCompraSC", cellValue);
                                break;
                            case 11:
                                mapeo.put("num", 12);
                                json.put("totalCompra", cellValue);
                                break;
                            case 12:
                                mapeo.put("num", 13);
                                json.put("cantCompraCCAN", cellValue);
                                break;
                            case 13:
                                mapeo.put("num", 14);
                                json.put("cantCompraSLAN", cellValue);
                                break;
                            case 14:
                                mapeo.put("num", 15);
                                json.put("cantCompraSCAN", cellValue);
                                break;
                            case 15:
                                mapeo.put("num", 16);
                                json.put("totalCompraAN", cellValue);
                                break;
                            case 16:
                                mapeo.put("num", 17);
                                json.put("ventaCC", cellValue);
                                break;
                            case 17:
                                mapeo.put("num", 18);
                                json.put("ventaSL", cellValue);
                                break;
                            case 18:
                                mapeo.put("num", 19);
                                json.put("ventaSC", cellValue);
                                break;
                            case 19:
                                mapeo.put("num", 20);
                                json.put("ventaTotal", cellValue);
                                break;
                            case 20:
                                mapeo.put("num", 21);
                                json.put("ventaCCAN", cellValue);
                                break;
                            case 21:
                                mapeo.put("num", 22);
                                json.put("ventaSLAN", cellValue);
                                break;
                            case 22:
                                mapeo.put("num", 23);
                                json.put("ventaSCAN", cellValue);
                                break;
                            case 23:
                                mapeo.put("num", 24);
                                json.put("ventaTotalAN", cellValue);
                                break;
                            case 24:
                                mapeo.put("num", 25);
                                json.put("exisCC", cellValue);
                                break;
                            case 25:
                                mapeo.put("num", 26);
                                json.put("exisSL", cellValue);
                                break;
                            case 26:
                                mapeo.put("num", 27);
                                json.put("exisSC", cellValue);
                                break;
                            case 27:
                                mapeo.put("num", 28);
                                json.put("exisTotal", cellValue);
                                break;
                            case 28:
                                mapeo.put("num", 29);
                                json.put("cantRepoCentral", cellValue);
                                break;
                            case 29:
                                mapeo.put("num", 30);
                                json.put("cantRepoSanLorenzo", cellValue);
                                break;
                            case 30:
                                mapeo.put("num", 31);
                                json.put("cantRepoCacique", cellValue);
                                break;
                            case 31:
                                mapeo.put("num", 32);
                                json.put("precioCosto", cellValue);
                                break;
                            case 32:
                                mapeo.put("num", 0);
                                json.put("valorizado", cellValue);
                                break;
//                        case 33:
//                            mapeo.put("num", 0);
//                            json.put("", cellValue);
//                            break;
                            default:
                                break;
                        }
                    }
                } catch (Exception e) {
                } finally {
                }
            });
            try {
//                System.out.println("JSON FORMAT -->> " + json.toString());
                if (!Boolean.parseBoolean(mapeo.get("estado").toString())) {
                    if (json.get("codigo").toString().equals("") || json.get("codigo").toString().equals("null") || json.get("codigo") == null) {

                    } else {
                        mapeoNow.put(json.get("codigo").toString(), json);
//                        int ord = Integer.parseInt(mapeo.get("orden").toString()) + 1;
//                        mapeo.put("orden", ord + "");
//                        json.remove("orden");
//                        json.put("orden", ord);
//                        System.out.println("JSON FORMATER II -->> " + json.toString());
                        matrizList.add(json);
                    }
                }
                mapeo.put("numero", (Integer.parseInt(mapeo.get("numero").toString()) + 1));
//                System.out.println();
            } catch (Exception ex) {
            } finally {
            }
        });

        if (matrizList.size() > 0) {
            Iterator entries = mapeoNow.entrySet().iterator();
            int orden = 0;
//            int ord = 0;
            List<JSONObject> list = new ArrayList<>();
            Map mapping = new HashMap();
            while (entries.hasNext()) {
                try {
                    Map.Entry entry = (Map.Entry) entries.next();
//            Integer key = (Integer) entry.getKey();
                    JSONObject value = (JSONObject) entry.getValue();
                    orden++;
                    value.put("orden", orden);
//            System.out.println("Key = " + key + ", Value = " + value);
//                    matrizList.add(value);
//                    detalleArtList.add(value);//si
                    JSONObject jsonCategoria = new JSONObject();
//                jsonCategoria.put("orden", orden);
                    jsonCategoria.put("categoria", value.get("seccion").toString());
                    if (!mapping.containsKey(value.get("seccion").toString())) {
                        if (!value.get("seccion").toString().equals("")) {
                            list.add(jsonCategoria);
                        }
                    }
                    mapping.put(value.get("seccion").toString(), value.get("seccion").toString());
                } catch (Exception e) {
                } finally {
                }
            }
            int ord = 1;
            for (JSONObject jSONObject : list) {
                jSONObject.put("orden", ord);
                categoriasList.add(jSONObject);
                ord++;
            }
//            System.out.println("*** INICIO ***");
            int orde = 1;
            List<JSONObject> array = matrizList;
            matrizList = new ArrayList<>();
            for (JSONObject jSONObject : array) {
                jSONObject.remove("orden");
                jSONObject.put("orden", orde + "");
                orde++;
                matrizList.add(jSONObject);
            }
            detalleArtList = matrizList;
            actualizandoTablaMatriz(matrizList);
            actualizarTablaCategoria();
        }

        // Closing the workbook
        workbook.close();
//        actualizandoTablaMatriz(matrizList);
//        actualizarTablaCategoria();
    }
}
