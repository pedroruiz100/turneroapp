/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class CodArticuloFXMLController extends BaseScreenController implements Initializable {

    static void setCantidad(String cant, TableView tv) {
        cantidad = cant;
        tableFact = tv;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private NumberValidator numVal;
    static String cantidad = "";
    static TableView<JSONObject> tableFact;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean alertEscEnter;
//    boolean crearNuevo;

    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    private List<JSONObject> clienteFielList;
    private static JSONObject jsonClienteFiel;

    @Autowired
    private ClienteDAO cliDAO;
    @Autowired
    private ArticuloDAO artDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente = new JSONObject();
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private JSONObject clienteFiel;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private TextField textFieldCantidad;
    @FXML
    private TextField txtCant;
    @FXML
    private Label labelRucClienteNuevo111;
    @FXML
    private Button btnConfirmar;
    @FXML
    private TextField txtSanLorenzo;
    @FXML
    private Label labelRucClienteNuevo1111;
    @FXML
    private TextField txtCacique;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        repeatFocusData(textFieldCantidad);
        textFieldCantidad.setText(cantidad);
        txtCant.setText("0");
        txtSanLorenzo.setText(cantidad);
        txtCacique.setText(cantidad);
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void navegandoFacturaVenta() {

    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreenModal("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/ModCantidadMayFXML.fxml", 232, 95, false);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (textFieldCantidad.isFocused()) {
                txtCant.requestFocus();
            } else if (txtCant.isFocused()) {
                txtSanLorenzo.requestFocus();
            } else if (txtSanLorenzo.isFocused()) {
                txtCacique.requestFocus();
            } else if (txtCacique.isFocused()) {
                textFieldCantidad.requestFocus();
                confirmacion();
            }
            if (keyCode == event.getCode().ESCAPE) {
                volviendo();
            }
        }
    }

    private void mensajeAlerta(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoClienteFiel() {

    }

    private void nuevoClienteFiel() {

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @FXML
    private void btnConfirmarAction(ActionEvent event) {
//        if (textFieldCantidad.isFocused()) {
//            txtCant.requestFocus();
//        } else if (txtCant.isFocused()) {
//            if (!textFieldCantidad.getText().equalsIgnoreCase("")) {

//                    for (int i = 0; i < tableFact.getItems().size(); i++) {
//                        JSONObject json = (JSONObject) tableFact.getItems().get(i);
//                        if (json.get("codigo").toString().equalsIgnoreCase(textFieldCantidad.getText())) {
//                            mensajeAlerta("YA EXISTE UN CODIGO");
//                            break;
//                        }
//                    }
        confirmacion();
//            } else {
//                mensajeAlerta("CANTIDAD NO DEBE QUEDAR VACIO");
//    }
//}
    }

    private void confirmacion() {
        Articulo art = artDAO.buscarCod(textFieldCantidad.getText());
        JSONObject jsonDet = new JSONObject();
        if (art != null) {

            jsonDet.put("orden", (PedidoCompraFXMLController.orden + 1));
            jsonDet.put("tipo", "MER");
            jsonDet.put("codigo", textFieldCantidad.getText());
            jsonDet.put("descripcion", art.getDescripcion());

            int cantida = Integer.parseInt(txtCant.getText()) + Integer.parseInt(txtCacique.getText()) + Integer.parseInt(txtSanLorenzo.getText());

            jsonDet.put("cantidad", cantida + "");
            jsonDet.put("cantidadCC", txtCant.getText());
            jsonDet.put("cantidadSL", txtSanLorenzo.getText());
            jsonDet.put("cantidadSC", txtCacique.getText());
            jsonDet.put("medida", "UNI");
            try {
                jsonDet.put("descuento", "0");
            } catch (Exception e) {
                jsonDet.put("descuento", "0");
            } finally {
            }
            jsonDet.put("costo", art.getCosto());
//                    long iva = 0;

            if (art.getIva().getPoriva().equals("0")) {
                long exenta = art.getCosto() * Long.parseLong(txtCant.getText());
                jsonDet.put("exenta", exenta);
                jsonDet.put("iva", "0");
            } else {
                if (art.getIva().getPoriva().equals("5")) {
                    long gravada5 = art.getCosto() * Long.parseLong(txtCant.getText());
                    gravada5 = Math.round(gravada5 / 21);
                    long iva5 = (art.getCosto() * Long.parseLong(txtCant.getText())) - gravada5;
                    jsonDet.put("gravada", iva5);
                    jsonDet.put("iva", "5");
                } else {
                    long gravada10 = art.getCosto() * Long.parseLong(txtCant.getText());
                    gravada10 = Math.round(gravada10 / 11);
                    long iva10 = (art.getCosto() * Long.parseLong(txtCant.getText())) - gravada10;
                    jsonDet.put("gravada", iva10);
                    jsonDet.put("iva", "10");
                }
            }
            jsonDet.put("deposito", "DEPOSITO");

            tableFact.getItems().add(jsonDet);
            JSONParser parser = new JSONParser();
            int val = 0;
            PedidoCompraFXMLController.detalleArtList = new ArrayList<>();
            long exenta = 0;
            long gravada5 = 0;
            long gravada10 = 0;
            long iva5 = 0;
            long iva10 = 0;
            long iva = 0;
            for (JSONObject item : tableFact.getItems()) {

                if (item.get("iva").toString().equalsIgnoreCase("0")) {
                    jsonDet.put("iva", "0");
                    item.put("exenta", (Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString())));
                } else {
                    if (item.get("iva").toString().equalsIgnoreCase("5")) {
                        jsonDet.put("iva", "5");
                        long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
                        iva5 = Math.round(costo / 21);

                        iva = costo - iva5;
                        item.put("gravada", iva);
                    } else {
                        jsonDet.put("iva", "10");
                        long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
                        iva10 = Math.round(costo / 11);

                        iva = costo - iva10;
                        item.put("gravada", iva);
                    }
                }
//
                PedidoCompraFXMLController.detalleArtList.add(item);//si
            }
            if (StageSecond.getStageData().isShowing()) {
                StageSecond.getStageData().close();
            }
            this.sc.loadScreen("/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/CodArticuloFXML.fxml", 286, 203, true);
        } else {
            mensajeAlerta("NO SE ENCUENTRA RESULTADOS DEL ARTICULO INGRESADO...");
        }
    }

}
