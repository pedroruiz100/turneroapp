/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloDevolucion;
import com.peluqueria.core.domain.FacturaCompraCab;
import com.peluqueria.core.domain.FacturaCompraDet;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.core.domain.PedidoDetConteo;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloDevolucionDAO;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.FacturaCompraCabDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoCompraDAO;
import com.peluqueria.dao.PedidoDetConteoDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RecepcionDAO;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.stock.PedidoCompraFXMLController.orden;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.PATH;
import com.javafx.util.StageSecond;
import java.io.File;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
//@ScreenScoped
public class CruceFXMLController extends BaseScreenController implements Initializable {

    static void cargarRucRazon(TextField txtProveedorDoc, TextField txtProveedor) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
    }

    static void cargarDatos(TextField txtProveedorDoc, TextField txtProveedor, TextField txtTimbrados) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
        txtTimbrado = txtTimbrados;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private boolean exitoEditar;
    private static TextField txtRucCli;
    private static TextField txtRazonCli;
    private static TextField txtTimbrado;
    private static boolean clienteSi;
    public static String proveedor;
    private long idCliente;
    private int codCliente;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    public static long idPedidoCab;
    public static Map mapeoCruce = new HashMap();
//    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    public static Map mapeoCheck = new HashMap();
    boolean estadoCheck = false;

    final KeyCombination altG = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN);
    public static List<JSONObject> detalleArtList;

    @Autowired
    private ClienteDAO cliDAO;
    @Autowired
    private ArticuloDevolucionDAO artDevoDAO;
    @Autowired
    private ArticuloDAO artDAO;

    @Autowired
    private ProveedorDAO proveedorDAO;

    @Autowired
    private PedidoCabDAO pedidoCabDAO;

    @Autowired
    private FacturaCompraCabDAO facturaCompraCabDAO;

    @Autowired
    private ArticuloDevolucionDAO articuloDevolucionDAO;

    public static JSONArray listaArray = new JSONArray();

    @Autowired
    private PedidoCompraDAO pedidoCompraDAO;

    @Autowired
    private PedidoDetDAO pedidoDetDAO;

    @Autowired
    private PedidoDetConteoDAO pedidoDetConteoDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private FacturaCompraDetDAO fcdDAO;

    @Autowired
    private RecepcionDAO recepcionDAO;

    public static String compra = "";
    public static String entrada = "";
    public static String cod = "";

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente;

    Image image;
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;

    @FXML
    private Label labelClienteBuscar;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private TextField txtNumOrden;
//    private TextField txtOrdenCompra;
    @FXML
    private Label labelSupervisor1112;
//    private ChoiceBox<String> chkEmpresa;
    @FXML
    private TextField txtNomProveedor;
    @FXML
    private TextField txtRucProveedor;
//    private TableColumn<JSONObject, String> tableColumnPlazo;
    @FXML
    private AnchorPane secondPane;
    @FXML
    private TextField txtCompra;
    @FXML
    private TextField txtNroEntrada;
    @FXML
    private Button btnFiltrar;
    @FXML
    private Button btnBuscar;
    @FXML
    private TableView<JSONObject> tableViewConteo;
    @FXML
    private Button btnSalir;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
//    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnCantidad;
//    private TableColumn<JSONObject, String> columnObservacion;
//    private TableColumn<JSONObject, String> columnPrecio;
//    private TableColumn<JSONObject, String> columnTotal;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TableColumn<JSONObject, Boolean> columnCheck;
    @FXML
    private Label labelRucClienteNuevo111;
    @FXML
    private TableColumn<JSONObject, String> columnRazon;
    private TableColumn<JSONObject, String> columnEntrada;
    private TableColumn<JSONObject, String> columnOc;
    @FXML
    private TableColumn<JSONObject, String> columnPedido;
    @FXML
    private TableColumn<JSONObject, String> columnFecha;
    @FXML
    private Button btnImprimir;
    @FXML
    private Button btnGenerarOrden;
    @FXML
    private DatePicker dpFecha;
    @FXML
    private Label labelSupervisor11121;
    @FXML
    private ChoiceBox<String> chkEmpresa;
    @FXML
    private TableColumn<JSONObject, String> columnFactura;
    @FXML
    private TableColumn<JSONObject, String> columnSucursal;
    @FXML
    private TableColumn<JSONObject, Boolean> columnDevolucion;
    @FXML
    private ImageView imageViewLogo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
//        buscandoCliente(false);
    }

    private void buttonActualizarAction(ActionEvent event) {
        editandoCliente();
    }

    private void buttonBuscarClienteAction(ActionEvent event) {
        buscandoCliente();
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
//        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha();
        ubicandoContenedorSecundario();
        switch (ScreensContoller.getFxml()) {
            case "/vista/stock/ModCantidadFXML.fxml": {
                txtCompra.setText(compra);
                txtNroEntrada.setText(entrada);
                txtCodigo.setText(cod);
                actualizandoTablaCliente(getDetalleArtList());
                columnCheck.setVisible(false);
                break;
            }
//            case "/vista/stock/CodArticuloFXML.fxml": {
//                txtPedido.setText(nroPedido);
//                buscarRecep();
//                chkDiasVisita.setValue(diaVisita);
//                chkTipoDoc.setValue(tipoDoc);
//                txtCuotas.setText(cuota);
//                txtPlazo.setText(plazo);
//                txtFrecuencia.setText(frecuencia);
//                //                cargarDetalle(pedidoCabDAO.getByNroOrden(txtPedido.getText()).getIdPedidoCab());
//                calcularTotales();
//
//                break;
//            }
            default:
//                toaster = new Toaster();
//        btnVerificarPrecio.setDisable(true);
                detalleArtList = new ArrayList<>();
                mapeoCheck = new HashMap();
//                depositoList = new ArrayList<>();
                orden = 0;
                chkEmpresa.getItems().addAll("-- SELECCIONE --", "CASA CENTRAL", "SAN LORENZO", "CACIQUE");
                chkEmpresa.setValue("-- SELECCIONE --");
//        cargarTipoMovimiento();
//                labelOrdenCompra.setText(rangoOCDAO.recuperarActual());
//                repeatFocus(txtPedido);
        }
        repeatFocus(txtCompra);
//        chkEmpresa.getItems().addAll("-- SELECCIONE --", "CASA CENTRAL", "SAN LORENZO", "CACIQUE");
        cargandoImagen();

    }
    //INICIAL INICIAL INICIAL **************************************************

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneCliente.getChildren()
                .get(anchorPaneCliente.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        clienteSi = true;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, true);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
    }

    private void volviendo() {
//        clienteSi = false;
//        if (StageSecond.getStageData().isShowing()) {
//            StageSecond.getStageData().close();
//        }
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {

    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static void setDetalleArtList(List<JSONObject> detalleArtList) {
//        FacturaAprobadaPagoFXMLController.detalleArtList = detalleArtList;
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            filtrar();
        }

        if (keyCode == event.getCode().F3) {
//            if (!buttonNuevo.isDisable()) {
            imprimirCruce();
//            }a
        }
        if (keyCode == event.getCode().F6) {
//            if (!buttonNuevo.isDisable()) {
            tableViewConteo.requestFocus();
//            }a
        }
        if (keyCode == event.getCode().F9) {
//            if (!buttonNuevo.isDisable()) {
            generarOrdenDevolucion();
//            }
        }
        if (keyCode == event.getCode().F2) {
//            if (!buttonNuevo.isDisable()) {
            seleccionar();
//            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
//            JSONParser parser = new JSONParser();
            if (alert) {
                alert = false;
            }
//            else if (!buttonAnhadir.isDisable()) {
////                try {
//
//                seteandoParam(jsonSeleccionActual);
//                txtRucCli.setText(jsonSeleccionActual.get("ruc").toString());
//                txtRazonCli.setText(jsonSeleccionActual.get("descripcion").toString());
//                if (ScreensContoller.getFxml().contentEquals("/vista/stock/FacturaCompraFXML.fxml")) {
//                    txtTimbrado.setText(jsonSeleccionActual.get("timbrado").toString());
//                }
////                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
////                    JSONObject cliente = new JSONObject();
////                    cliente.put("idCliente", idCliente);
////                    cab.put("cliente", cliente);
////                    fact.put("facturaClienteCab", cab);
////                    actualizarDatos();
//                navegandoFormaPago();
////                } catch (ParseException ex) {
////                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
////                }
//            }
        }

        if (altG.match(event)) {
            recortarDetalle();
//            recortarDetalle();
        }
    }

    private void limpiarDatos() {
//        limpiar();
        mapeoCheck = new HashMap();
        detalleArtList = new ArrayList<>();
        tableViewConteo.getItems().clear();
        idPedidoCab = 0;
        txtCodigo.setText("");
        txtNroEntrada.setText("");
        txtCompra.setText("");
        txtCompra.requestFocus();
        columnCheck.setVisible(true);
//        dpDesde.setValue(null);
//        dpHasta.setValue(null);
//        dpFecIni.setValue(null);
//        dpFecFin.setValue(null);
//        labelTotalGs.setText("Gs 0");
    }

    private void escucha() {
        escucha = true;
        tableViewConteo.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewConteo.getSelectionModel().getSelectedItem() != null) {

//                if (buttonAnhadir.isDisable()) {
//                    buttonAnhadir.setDisable(false);
//                }
                jsonSeleccionActual = newSelection;
//                txtNomProveedor.setText(jsonSeleccionActual.get("proveedor").toString());
//                txtNumOrden.setText(jsonSeleccionActual.get("oc").toString());
                //editar cliente, por si acaso...
            }
        });
    }
//LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void borrando() {
        resetParam();
        navegandoFormaPago();
    }

    public static void seteandoParam(JSONObject cliente) {
        jsonCliente = cliente;
    }

    public static void resetParam() {
        jsonCliente = null;
        clienteSi = false;
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ruc);
        return clienteJSONObjList;
    }

    private void buscandoCliente() {
////        clienteList = new ArrayList<>();
//        tableViewConteo.getItems().clear();
//        String proveedor = "";
//        String sucursal = "";
//        String oc = "";
////        if (!txtProveedor.getText().equals("")) {
////            proveedor = BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString();
////        }
//        try {
//            if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("-- SELECCIONE --")) {
//                sucursal = chkEmpresa.getSelectionModel().getSelectedItem();
//            }
//        } catch (Exception e) {
//        } finally {
//        }
////        if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("")) {
//
////        }
//        if (!txtOrdenCompra.getText().equals("")) {
//            oc = txtOrdenCompra.getText();
//        }
//        List<PedidoCab> lisPedido = pedidoCabDAO.filtrarPorProveedorSucursalOc(proveedor, sucursal, oc);
//        for (PedidoCab pedidoCab : lisPedido) {
//            JSONObject jsonObj = new JSONObject();
//            jsonObj.put("proveedor", pedidoCab.getProveedor().getDescripcion());
//            jsonObj.put("docProveedor", pedidoCab.getProveedor().getRuc());
//            String date = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date(pedidoCab.getFecha().getTime()));
////                txtFechaPedido.setText(date);
//            jsonObj.put("fecha", date);
//            jsonObj.put("oc", pedidoCab.getOc());
//            jsonObj.put("tipo", pedidoCab.getTipo());
//            jsonObj.put("plazo", pedidoCab.getPlazo());
//            long total = pedidoDetDAO.recuperarTotal(pedidoCab.getIdPedidoCab());
//            jsonObj.put("total", total);
//            jsonObj.put("sucursal", pedidoCab.getSucursal());
//            jsonObj.put("comprador", pedidoCab.getUsuario());
//            jsonObj.put("idPedidoCab", pedidoCab.getIdPedidoCab());
//
////            clienteList.add(jsonObj);
//        }
////        actualizandoTablaCliente();
    }

    private void buscarClientePorRucCi() {
//        String rucCliente = "";
//        JSONParser parser = new JSONParser();
//        try {
//            rucCliente = textFieldRucCliente.getText();
//            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
//            int contador = rucCliente.split("-", -1).length - 1;
//            Proveedor cliente = new Proveedor();
//            if (contador >= 1) {
//                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
////                cliente = cliDAO.listarPorRuc(rucCliente);
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getDescripcion() != null) {
////                    cliente.setFecNac(null);
//                    cliente.setFechaAlta(null);
//                    cliente.setFechaMod(null);
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            } else if (contador == 0) {
//                //Listar por CI en el caso que exista
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getIdProveedor() == null) {
//                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
//                    List<Proveedor> listClien = proveedorDAO.listarPorCIRevancha(rucCliente);
//                    if (listClien != null) {
//                        if (listClien.isEmpty()) {
//                            clienteList = new ArrayList<>();
//                        } else {
//                            clienteList = new ArrayList<>();
//                            for (int i = 0; i < listClien.size(); i++) {
//                                Proveedor cli = listClien.get(i);
////                                cli.setFecNac(null);
//                                cli.setFechaAlta(null);
//                                cli.setFechaMod(null);
//                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
//                                clienteList.add(jsonCli);
//                            }
//                        }
//                    } else {
//                        clienteList = new ArrayList<>();
//                    }
//                } else {
//                    clienteList = new ArrayList<>();
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            }
//            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
//        }
    }

    private void buscarAutomatico(JSONObject cliente) {
//        if (cliente != null) {
//            if (cliente.get("ruc") != null) {
//                textFieldRucCliente.setText(cliente.get("ruc").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//            textFieldNombreCliente.setText(cliente.get("nombre").toString());
//            buscandoCliente(true);
//        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
//    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE?", ok, cancel);
//            this.alert = true;
//            alert.showAndWait();
//            if (alert.getResult() == ok) {
//                alert.close();
//                JSONObject cliente = new JSONObject();
//                cliente = creandoJsonCliente();
//                int codCli = Integer.parseInt(cliente.get("codCliente").toString());
//                Cliente clie = cliDAO.getByCod(codCli);
//                if (clie == null) {
//                    long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    cliente.put("idCliente", idActual);
//                    exitoCrear = persistiendoPendientes(cliente);
//                    if (exitoCrear) {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        cliente.put("fechaAlta", null);
//                        cliente.put("fechaMod", null);
//                        ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
//                        cliDTO.setFechaAlta(timestamp);
//                        cliDTO.setFechaMod(timestamp);
//                        Cliente cli = Cliente.fromClienteDTO(cliDTO);
//                        Pais pais = new Pais();
//                        pais.setIdPais(0L);
//                        Departamento dpto = new Departamento();
//                        dpto.setIdDepartamento(0l);
//                        Ciudad ciu = new Ciudad();
//                        ciu.setIdCiudad(0l);
//                        Barrio barr = new Barrio();
//                        barr.setIdBarrio(0l);
//                        cli.setPais(pais);
//                        cli.setDepartamento(dpto);
//                        cli.setCiudad(ciu);
//                        cli.setBarrio(barr);
//                        try {
//                            cliDAO.insertar(cli);
//                            System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
//                        } catch (Exception e) {
//                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//                            System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
//                        }
//                        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.OK) {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        } else {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        }
//                    } else {
//                        Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.CLOSE) {
//                            alert2.close();
//                        }
//                    }
//                } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
//                }
//            } else {
//                alert.close();
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
//        return exitoCrear;
//    }
    //////CREATE, CLIENTE -> POST
    //////UPDATE, CLIENTE -> PUT
    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            JSONObject cliente = new JSONObject();
            cliente = editandoJsonCliente();
            exitoEditar = actualizarPendientes(cliente);
            if (exitoEditar) {
                try {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject usuarioCajero = Identity.getUsuario();
                    JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                    String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                    cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
                    System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                }
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.OK) {
                    alert2.close();
                    buscarAutomatico(cliente);
                } else {
                    alert2.close();
                    buscarAutomatico(cliente);
                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.CLOSE) {
                    alert2.close();
                }
            }
        } else {
            alert.close();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
        return exitoEditar;
    }
    //////UPDATE, CLIENTE -> PUT

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            this.alert = true;
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ruc) {
        JSONParser parser = new JSONParser();
        List<Proveedor> cliente = proveedorDAO.listarPorNomRuc(nom, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Proveedor cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - CLIENTE
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
//    private JSONObject creandoJsonCliente() {
//        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("nombre", textFieldNombreClienteNuevo.getText());
//        cliente.put("apellido", textFieldClienteApellidoNuevo.getText());
//        if (textFieldRucClienteNuevo.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            int count = StringUtils.countMatches(textFieldRucClienteNuevo.getText(), "-");
//            if (count == 0) {
//                if (textFieldRucClienteNuevo.getText().length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(textFieldRucClienteNuevo.getText()).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(textFieldRucClienteNuevo.getText()));
//                    }
//                }
//            } else if (count == 1) {
//                StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
//                String cedula = st.nextElement().toString();
//
//                if (cedula.length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(cedula).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(cedula));
//                    }
//                }
//            } else {
//                cliente.put("ruc", textFieldRucClienteNuevo.getText());
//            }
//        }
//        cliente.put("usuAlta", Identity.getNomFun());
//        cliente.put("fechaAlta", timestampJSON);
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
//        cliente.put("telefono", textFieldClienteTelefonoNuevo.getText());
//        cliente.put("segundaLateral", null);
//        cliente.put("primeraLateral", null);
//        cliente.put("nroLocal", null);
//        cliente.put("pais", null);
//        cliente.put("departamento", null);
//        cliente.put("ciudad", null);
//        cliente.put("barrio", null);
//        cliente.put("email", null);
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        String codCliente = "";
//        String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
//        if (arrayCod.length > 0) {
//            codCliente = arrayCod[0];
//        } else {
//            codCliente = textFieldRucClienteNuevo.getText();
//        }
//        cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
//        cliente.put("callePrincipal", null);
//        return cliente;
//    }
    //JSON CREANDO CLIENTE
    //JSON EDITANDO CLIENTE
    private JSONObject editandoJsonCliente() {
        return null;
    }
    //JSON EDITANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente(List<JSONObject> arrayLista) {
        numVal = new NumberValidator();
        clienteData = FXCollections.observableArrayList(arrayLista);

//        columnCheck.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
//                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
//            @Override
//            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
//                return new BooleanCell(tableViewConteo);
//            }
//        };
//        columnCheck.setCellValueFactory((data) -> {
//            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
//            try {
//                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getString("codigo")).toString())) {
//                    return new SimpleBooleanProperty(true);
//                } else {
//                    return new SimpleBooleanProperty(false);
//                }
//            } catch (Exception e) {
//                return new SimpleBooleanProperty(false);
//            } finally {
//            }
//        });
//        columnCheck.setCellFactory(booleanCellFactory);
//        columnCheck.setVisible(true);
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String apellido = String.valueOf(data.getValue().get("codigo"));
                if (apellido.contentEquals("null")) {
                    apellido = "";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        columnRazon.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnRazon.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("razon"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("cantidad"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnSucursal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnSucursal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("sucursal"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnDevolucion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDevolucion.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new CruceFXMLController.AddObjectsCell(tableViewConteo);
            }
        });
        columnFactura.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFactura.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("factura"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnPedido.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPedido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("pedido"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnFecha.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFecha.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("fecha"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
//        tableColumnComprador.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnComprador.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String ruc = String.valueOf(data.getValue().get("comprador"));
//                if (ruc.contentEquals("null")) {
//                    ruc = "";
//                }
//                return new ReadOnlyStringWrapper(ruc);
//            }
//        });
        //columna Ruc .................................................
        tableViewConteo.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    private void buttonNroEntradaAction(ActionEvent event) {
//        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
//            RecepcionFXMLController.cargarRecepcion(tableViewConteo.getSelectionModel().getSelectedItem().get("docProveedor").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("proveedor").toString(),
//                    tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString()
//            );
//            this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA AGREGAR NUMERO DE ENTRADA...");
//        }
    }

    private void buttonImprimirAction(ActionEvent event) {
        exportar();
    }

    private void buscarProveedor() {
//        BuscarProveedorFXMLController.cargarRucRazon(txtRucProveedor, txtProveedor);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void exportar() {
        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
            JSONArray jsonFacturas = recuperarDatos();

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("");

            } else {
                JSONArray array = jsonFacturas;
                informandoVentas("{\"ventas\": " + array + "}");
            }
            this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/CruceFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA IMPRIMIR LA ORDEN...");
        }
    }

    private JSONArray recuperarDatos() {
        JSONParser parser = new JSONParser();
        JSONArray array = new JSONArray();
        HashMap mapeoCodigo = new HashMap();
        HashMap mapeoFactura = new HashMap();
//        List<FacturaCompraDet> listFCD = fcdDAO.listarPorRecepcion(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idFacturaCompraCab").toString()));
        List<FacturaCompraDet> listFCD = fcdDAO.buscarPorSucursalOC(tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
        FacturaCompraCab fcc = facturaCompraCabDAO.getByIdCompra(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idFacturaCompraCab").toString()));
        if (listFCD.size() > 0) {
            int ord = 0;
            long tot = 0;
            numVal = new NumberValidator();
            long gra5 = 0;
            long tot5 = 0;
            long iva5 = 0;
            long gra10 = 0;
            long tot10 = 0;
            long iva10 = 0;
            long exe = 0;
            long importeAjustado = 0;
            long totalFactura = 0;

//            String num1 = "";
//            String num2 = "";
//            String num3 = "";
//
//            String doc1 = "";
//            String doc2 = "";
//            String doc3 = "";
//
//            String fec1 = "";
//            String fec2 = "";
//            String fec3 = "";
//
//            String mon1 = "";
//            String mon2 = "";
//            String mon3 = "";
//            int num = 0;
//            try {
//                for (ArticuloDevolucion artDevo : articuloDevolucionDAO.listarByIdFacturaCab(fcc.getIdFacturaCompraCab() + "")) {
//                    num1 = artDevo.getNroDevolucion();
//                    doc1 = fcc.getNroDoc();
//                    fec1 = fcc.getFechaDoc() + "";
//                    num += artDevo.getTotal();
//                    mon1 = num + "";
//                }
//            } catch (Exception e) {
//                System.out.println("-->> " + e.getLocalizedMessage());
//                num1 = "-";
//                doc1 = "-";
//                fec1 = "-";
//                num = 0;
//                mon1 = 0 + "";
//            } finally {
//            }
            for (FacturaCompraDet factCompraDet : listFCD) {
                ord++;
                JSONObject json = new JSONObject();
//                json.put("codigo", ord);
                json.put("codigo", factCompraDet.getArticulo().getCodArticulo() + "");
                json.put("sucursal", tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
                json.put("pedido", fcc.getPedidoCab().getNroPedido());
                json.put("descripcion", factCompraDet.getDescripcion());
                PedidoCompra pdc = pedidoCompraDAO.getByIdPedidoCabAndCodArt(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()), factCompraDet.getArticulo().getIdArticulo());

                String data = tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString();
                String suc = data.substring(0, (data.length() - 2));
                StringTokenizer sts = new StringTokenizer(suc, ".");
                String suc1 = "";
                try {
                    suc1 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }
                String suc2 = "";
                try {
                    suc2 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }
                String suc3 = "";
                try {
                    suc3 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }
                PedidoDetConteo pdconteo = pedidoDetConteoDAO.getByIdPedidoCabAndCodArt(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()), factCompraDet.getArticulo().getIdArticulo(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
//                PedidoDetConteo pdconteo = pedidoDetConteoDAO.getByIdPedidoCabAndCodArt(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()), factCompraDet.getArticulo().getIdArticulo(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());

                int cantida = 0;
                int cantidadConteo = 0;

                suc1 = suc1.trim();
                suc2 = suc2.trim();
                suc3 = suc3.trim();

                if (suc1.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc1.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc1.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (suc3.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc3.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc3.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (suc2.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc2.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc2.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (!mapeoCodigo.containsKey(factCompraDet.getArticulo().getCodArticulo())) {
                    json.put("cantidad", cantida);
                    try {
                        json.put("precio", numVal.numberFormat("###,###.###", Double.parseDouble(pdc.getPrecio() + "")));
                    } catch (Exception e) {
                        json.put("precio", numVal.numberFormat("###,###.###", Double.parseDouble("0")));
                    } finally {
                    }
                    long total = 0;
                    try {
                        total = Long.parseLong(cantida + "") * pdc.getPrecio();
                        tot += Long.parseLong(cantida + "") * pdc.getPrecio();
                    } catch (Exception e) {
                        total = 0;
                    } finally {
                    }
                    json.put("total", numVal.numberFormat("###,###.###", Double.parseDouble(total + "")));
                    json.put("recibido", cantidadConteo + "");

                    String canti = factCompraDet.getCantidad().toString().replace(".000", "");
                    json.put("factura", canti + "");
                    json.put("bonificacion", "0");
                    json.put("preciodos", numVal.numberFormat("###,###.###", Double.parseDouble(factCompraDet.getPrecio() + "")));
                    long totalDos = 0;
                    json.put("cantajuste", "0");
                    json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                    double impAjus = 0;
                    json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));
                    json.put("leyenda", "F+");
//                long totalDos = Long.parseLong(canti + "") * factCompraDet.getPrecio();
//                json.put("totaldos", totalDos + "");

                    json.put("nroOC", tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString());
                    json.put("fechaPed", tableViewConteo.getSelectionModel().getSelectedItem().get("fecha").toString());
                    json.put("proveedor", tableViewConteo.getSelectionModel().getSelectedItem().get("razon").toString());
                    try {
                        json.put("observacion", tableViewConteo.getSelectionModel().getSelectedItem().get("obs").toString());
                    } catch (Exception e) {
                        json.put("observacion", "");
                    } finally {
                    }

                    json.put("deposito", "DEP 6");
                    json.put("condicion", "CONTADO");
                    json.put("plazo", "0");

                    String leyenda = "";
                    if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("recibido").toString())) {
                        int result = Integer.parseInt(json.get("recibido").toString()) - Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "C";
                    } else if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("cantidad").toString())) {
                        int result = Integer.parseInt(json.get("cantidad").toString()) - Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "C+";
                    } else if (Integer.parseInt(json.get("factura").toString()) < Integer.parseInt(json.get("recibido").toString())) {
                        leyenda = "F-";
                    } else if (Integer.parseInt(json.get("cantidad").toString()) == 0) {
                        int result = Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "F+";
                    }
                    if (leyenda.equalsIgnoreCase("")) {
                        try {
                            if (factCompraDet.getPrecio() > pdc.getPrecio()) {
                                int result = Integer.parseInt(json.get("factura").toString());
                                json.put("cantajuste", result + "");

                                totalDos = result * factCompraDet.getPrecio();
                                json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                                impAjus = totalDos / 1.1;
                                json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                                leyenda = "P";
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                    }

                    if (leyenda.equalsIgnoreCase("")) {
                        try {
                            if (factCompraDet.getPrecio() < pdc.getPrecio()) {
                                leyenda = "PM";
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                    }
                    json.put("leyenda", leyenda);
                    mapeoCodigo.put(factCompraDet.getArticulo().getCodArticulo(), json);
                } else {
                    JSONObject jsonDetalle = new JSONObject();
                    try {
                        //                    Recuperar detalle ya aagregado
                        jsonDetalle = (JSONObject) parser.parse(mapeoCodigo.get(factCompraDet.getArticulo().getCodArticulo()).toString());
                    } catch (ParseException ex) {
                        System.out.println("ex " + ex.getLocalizedMessage());
                    }
                    json.put("cantidad", cantida);
//                    json.put("cantidad", (Integer.parseInt(jsonDetalle.get("cantidad").toString()) + cantida));
                    try {
                        json.put("precio", numVal.numberFormat("###,###.###", Double.parseDouble(pdc.getPrecio() + "")));
                    } catch (Exception e) {
                        json.put("precio", numVal.numberFormat("###,###.###", Double.parseDouble("0")));
                    } finally {
                    }
                    long total = 0;
                    try {
                        total = Long.parseLong(json.get("cantidad").toString()) * pdc.getPrecio();
//                        tot += Long.parseLong(json.get("cantidad").toString()) * pdc.getPrecio();
                    } catch (Exception e) {
                        total = 0;
                    } finally {
                    }
                    json.put("total", numVal.numberFormat("###,###.###", Double.parseDouble(total + "")));
                    json.put("recibido", cantidadConteo + "");
//                    json.put("recibido", (Integer.parseInt(jsonDetalle.get("recibido").toString()) + cantidadConteo));

                    String canti = factCompraDet.getCantidad().toString().replace(".000", "");
//                    json.put("factura", canti + "");
                    json.put("factura", (Integer.parseInt(jsonDetalle.get("factura").toString()) + Integer.parseInt(canti)) + "");
                    json.put("bonificacion", "0");
                    json.put("preciodos", numVal.numberFormat("###,###.###", Double.parseDouble(factCompraDet.getPrecio() + "")));
                    long totalDos = 0;
                    json.put("cantajuste", "0");
                    json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                    double impAjus = 0;
                    json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));
                    json.put("leyenda", "F+");
//                long totalDos = Long.parseLong(canti + "") * factCompraDet.getPrecio();
//                json.put("totaldos", totalDos + "");

                    json.put("nroOC", tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString());
                    json.put("fechaPed", tableViewConteo.getSelectionModel().getSelectedItem().get("fecha").toString());
                    json.put("proveedor", tableViewConteo.getSelectionModel().getSelectedItem().get("razon").toString());
                    try {
                        json.put("observacion", tableViewConteo.getSelectionModel().getSelectedItem().get("obs").toString());
                    } catch (Exception e) {
                        json.put("observacion", "");
                    } finally {
                    }

                    json.put("deposito", "DEP 6");
                    json.put("condicion", "CONTADO");
                    json.put("plazo", "0");

                    String leyenda = "";
                    if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("recibido").toString())) {
                        int result = Integer.parseInt(json.get("recibido").toString()) - Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "C";
                    } else if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("cantidad").toString())) {
                        int result = Integer.parseInt(json.get("cantidad").toString()) - Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "C+";
                    } else if (Integer.parseInt(json.get("factura").toString()) < Integer.parseInt(json.get("recibido").toString())) {
                        leyenda = "F-";
                    } else if (Integer.parseInt(json.get("cantidad").toString()) == 0) {
                        int result = Integer.parseInt(json.get("factura").toString());
                        json.put("cantajuste", result + "");

                        totalDos = result * factCompraDet.getPrecio();
                        json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                        impAjus = totalDos / 1.1;
                        json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                        leyenda = "F+";
                    }
                    if (leyenda.equalsIgnoreCase("")) {
                        try {
                            if (factCompraDet.getPrecio() > pdc.getPrecio()) {
                                int result = Integer.parseInt(json.get("factura").toString());
                                json.put("cantajuste", result + "");

                                totalDos = result * factCompraDet.getPrecio();
                                json.put("totaldos", numVal.numberFormat("###,###.###", Double.parseDouble(totalDos + "")));
                                impAjus = totalDos / 1.1;
                                json.put("impajuste", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(impAjus)))));

                                leyenda = "P";
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                    }

                    if (leyenda.equalsIgnoreCase("")) {
                        try {
                            if (factCompraDet.getPrecio() < pdc.getPrecio()) {
                                leyenda = "PM";
                            }
                        } catch (Exception e) {
                        } finally {
                        }
                    }
                    json.put("leyenda", leyenda);
                }

//                proveedor = pedidoCabDAO.getById(pedidoCompra.getPedidoCab().getIdPedidoCab()).getProveedor().getDescripcion();
                Date date = new Date();
                Timestamp ts = new Timestamp(date.getTime());
                String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
                StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
                json.put("subRepNomFun", Identity.getNomFun());
                json.put("subRepTimestamp", subRepTimestamp);
//                json.put("sucursal", pedidoCompra.getPedidoCab().getSucursal());

//ACA
//                JSONObject jsonFacturados = new JSONObject();
//                if (fcc.getTipoDocumento().toUpperCase().equalsIgnoreCase("CR")) {
//                    jsonFacturados.put("tipoDoc", "CREDITO");
//                } else {
//                    jsonFacturados.put("tipoDoc", "CONTADO");
//                }
//                a System
//                .out.println("TOTAL DOS ->> " + Long.parseLong(numVal.numberValidator(json.get("totaldos").toString())));
//                if (!mapeoCodigo.containsKey(factCompraDet.getArticulo().getCodArticulo())) {
//                    importeAjustado += Long.parseLong(numVal.numberValidator(json.get("totaldos").toString()));
//                } else {
//                    importeAjustado += 0l;
//                }
//
//                jsonFacturados.put("numDoc", fcc.getNroDoc());
//                jsonFacturados.put("numCuota", "0");
//                jsonFacturados.put("fecDto", fcc.getFechaDoc().toString());
//                jsonFacturados.put("fecDoc", fcc.getFechaDoc().toString());
                json.put("cantDetalle", ord);
//                jsonFacturados.put("timbrado", fcc.getProveedor().getTimbrado() + "");
//
////                Articulo art = artDAO.buscarCod(factCompraDet.getArticulo().getCodArticulo() + ""); //                total;
//                if (factCompraDet.getPoriva() == 0) {
//                    exe += factCompraDet.getIva();
//                    jsonFacturados.put("exe", String.valueOf(exe));
//                } else {
//                    exe += 0;
//                    jsonFacturados.put("exe", String.valueOf(exe));
//                }
//                if (factCompraDet.getPoriva() == 5) {
//                    String cantidadi = String.valueOf(factCompraDet.getCantidad()).replace(".000", "");
//                    long totales = Long.parseLong(cantidadi) * factCompraDet.getPrecio();
//                    tot5 += totales;
//                    gra5 += factCompraDet.getIva();
//                    iva5 = tot5 - gra5;
//                    jsonFacturados.put("gra5", String.valueOf(Math.round(gra5)));
//                    jsonFacturados.put("iva5", String.valueOf(iva5));
//                } else {
//                    tot5 += 0;
//                    gra5 += 0;
//                    iva5 = tot5 - gra5;
//                    jsonFacturados.put("gra5", String.valueOf(Math.round(gra5)));
//                    jsonFacturados.put("iva5", String.valueOf(iva5));
//                }
//                if (factCompraDet.getPoriva() == 10) {
//                    String cantidadi = String.valueOf(factCompraDet.getCantidad()).replace(".000", "");
//                    long totales = Long.parseLong(cantidadi) * factCompraDet.getPrecio();
//                    tot10 += totales;
//                    gra10 += factCompraDet.getIva();
//                    iva10 = tot10 - gra10;
//
//                    jsonFacturados.put("gra10", String.valueOf(Math.round(gra10)));
//                    jsonFacturados.put("iva10", String.valueOf(iva10));
//                } else {
//                    tot10 += 0;
//                    gra10 += 0;
//                    iva10 = tot10 - gra10;
//                    jsonFacturados.put("gra5", String.valueOf(Math.round(gra10)));
//                    jsonFacturados.put("iva5", String.valueOf(iva10));
//                }
//                String cantidadi = String.valueOf(factCompraDet.getCantidad()).replace(".000", "");
////                totalFactura += Long.parseLong(cantidadi) * factCompraDet.getPrecio();
////                System.out.println("CANTI: " + cantidadi);
////                System.out.println("PREC: " + totalFactura);
////                System.out.println("FACTURA: " + fcc.getNroDoc() + " || " + fcc.getIdFacturaCompraCab());
//                jsonFacturados.put("totFactura", fcc.getTotal());
////                jsonFacturados.put("totFactura", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(fcc.getTotal()))));
//
//                jsonFacturados.put("tot", "202");
//
//                jsonFacturados.put("num1", num1);
//                jsonFacturados.put("num2", num2);
//                jsonFacturados.put("num3", num3);
//
//                jsonFacturados.put("doc1", doc1);
//                jsonFacturados.put("doc2", doc2);
//                jsonFacturados.put("doc3", doc3);
//
//                jsonFacturados.put("fecha1", fec1);
//                jsonFacturados.put("fecha2", fec2);
//                jsonFacturados.put("fecha3", fec3);
//
//                if (!mon1.equalsIgnoreCase("")) {
//                    jsonFacturados.put("monto1", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(mon1))));
//                } else {
//                    jsonFacturados.put("monto1", "");
//                }
//                if (!mon2.equalsIgnoreCase("")) {
//                    jsonFacturados.put("monto2", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(mon2))));
//                } else {
//                    jsonFacturados.put("monto2", "");
//                }
//                if (!mon3.equalsIgnoreCase("")) {
//                    jsonFacturados.put("monto3", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(mon3))));
//                } else {
//                    jsonFacturados.put("monto3", "");
//                }
                //FINAL 

//                if (ord == 1) {
//                    listaArray.add(json);
//                }
//                if (!mapeoFactura.containsKey(factCompraDet.getFacturaCompraCab().getNroDoc())) {
//                    mapeoFactura.remove(factCompraDet.getFacturaCompraCab().getNroDoc());
//                    mapeoFactura.put(factCompraDet.getFacturaCompraCab().getNroDoc(), jsonFacturados);
//                    gra5 = 0;
//                    tot5 = 0;
//                    iva5 = 0;
//                    gra10 = 0;
//                    tot10 = 0;
//                    iva10 = 0;
//                    exe = 0;
////                    long importeAjustado = 0;
//                } else {
//                    mapeoFactura.put(factCompraDet.getFacturaCompraCab().getNroDoc(), jsonFacturados);
//                }
                if (!mapeoCodigo.containsKey(factCompraDet.getArticulo().getCodArticulo())) {
                    mapeoCodigo.put(factCompraDet.getArticulo().getCodArticulo(), json);
                } else {
                    mapeoCodigo.remove(factCompraDet.getArticulo().getCodArticulo());
                    mapeoCodigo.put(factCompraDet.getArticulo().getCodArticulo(), json);
                }
//                array.add(json);a
            }
        }
        Iterator it = mapeoCodigo.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            try {
                array.add((JSONObject) parser.parse(e.getValue().toString()));
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
            }
        }
        String valor = "";
        String cantDetalle = "";
        String impAjus = "";
        String difAjuste = "";

        long val = 0l;
        long impaj = 0l;
        long dif = 0l;
        for (int i = 0; i < array.size(); i++) {
            try {
                JSONObject jsonObj = (JSONObject) parser.parse(array.get(i).toString());
                cantDetalle = jsonObj.get("cantDetalle").toString();

                val += Long.parseLong(numVal.numberValidator(jsonObj.get("total").toString()));
                valor = numVal.numberFormat("###,###.###", Double.parseDouble(val + ""));

                impaj += Long.parseLong(numVal.numberValidator(jsonObj.get("totaldos").toString()));
                impAjus = numVal.numberFormat("###,###.###", Double.parseDouble(impaj + ""));

                dif = val + impaj;
                difAjuste = numVal.numberFormat("###,###.###", Double.parseDouble(dif + ""));

            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
            }
        }

        long g5 = 0l;
        long g10 = 0l;
        long i5 = 0l;
        long i10 = 0l;
        long ex = 0l;
        long to = 0l;
        long to5 = 0l;
        long to10 = 0l;

//        List<FacturaCompraDet> listFCD = fcdDAO.buscarPorSucursalOC(tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
        List<FacturaCompraCab> listFCC = facturaCompraCabDAO.listarCabeceras(tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
        int ord = 0;
        listaArray = new JSONArray();

        for (FacturaCompraCab facturaCompraCab : listFCC) {
            JSONObject jsonFacturados = new JSONObject();
            ord++;
            g5 = 0l;
            g10 = 0l;
            i5 = 0l;
            i10 = 0l;
            ex = 0l;
            to = 0l;
            to5 = 0l;
            to10 = 0l;

            if (facturaCompraCab.getTipoDocumento().toUpperCase().equalsIgnoreCase("CR")) {
                jsonFacturados.put("tipoDoc", "CREDITO");
            } else {
                jsonFacturados.put("tipoDoc", "CONTADO");
            }
            jsonFacturados.put("numDoc", facturaCompraCab.getNroDoc());
            jsonFacturados.put("numCuota", "0");
            jsonFacturados.put("fecDto", facturaCompraCab.getFechaDoc().toString());
            jsonFacturados.put("fecDoc", facturaCompraCab.getFechaDoc().toString());
            jsonFacturados.put("valor", valor);
            int canDet = Integer.parseInt(cantDetalle) + 1;
            jsonFacturados.put("cantDetalle", String.valueOf(canDet));
            jsonFacturados.put("impAjus", impAjus);
            jsonFacturados.put("difAjuste", difAjuste);
            jsonFacturados.put("timbrado", facturaCompraCab.getProveedor().getTimbrado() + "");
            jsonFacturados.put("totFactura", facturaCompraCab.getTotal());

            jsonFacturados.put("totFactura2", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(facturaCompraCab.getTotal()))));
//                jsonFacturados.put("totFactura", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(fcc.getTotal()))));

            jsonFacturados.put("tot", ord);

            List<ArticuloDevolucion> listArtDev = articuloDevolucionDAO.listarByIdFacturaCab(facturaCompraCab.getIdFacturaCompraCab() + "");
            if (listArtDev != null && listArtDev.size() > 0) {
                String fecha = "";
                long mon = 0;
                for (ArticuloDevolucion articuloDevolucion : listArtDev) {
                    mon += articuloDevolucion.getTotal();
                    fecha = String.valueOf(articuloDevolucion.getFecha());
                }
                jsonFacturados.put("fec", fecha);
                jsonFacturados.put("monto2", String.valueOf(mon));
                jsonFacturados.put("monto", mon);
            } else {
                jsonFacturados.put("fec", "-");
                jsonFacturados.put("monto2", "0");
                jsonFacturados.put("monto", 0l);
            }

            List<FacturaCompraDet> listFDet = fcdDAO.listarPorRecepcion(facturaCompraCab.getIdFacturaCompraCab());
            for (FacturaCompraDet facturaCompraDet : listFDet) {

//                Articulo art = artDAO.buscarCod(factCompraDet.getArticulo().getCodArticulo() + ""); //                total;
                if (facturaCompraDet.getPoriva() == 0) {
                    ex += facturaCompraDet.getIva();
                    jsonFacturados.put("exe2", String.valueOf(ex));
                    jsonFacturados.put("exe", ex);
                } else {
                    ex += 0;
                    jsonFacturados.put("exe2", String.valueOf(ex));
                    jsonFacturados.put("exe", ex);
                }
                if (facturaCompraDet.getPoriva() == 5) {
                    String cantidadi = String.valueOf(facturaCompraDet.getCantidad()).replace(".000", "");
                    long totales = Long.parseLong(cantidadi) * facturaCompraDet.getPrecio();
                    to5 += totales;
                    g5 += facturaCompraDet.getIva();
                    i5 = to5 - g5;
                    jsonFacturados.put("gra52", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(g5)))));
                    jsonFacturados.put("gra5", Math.round(g5));
                    jsonFacturados.put("iva52", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(i5))));
                    jsonFacturados.put("iva5", i5);
                } else {
                    to5 += 0;
                    g5 += 0;
                    i5 = to5 - g5;
                    jsonFacturados.put("gra52", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(g5)))));
                    jsonFacturados.put("gra5", Math.round(g5));
                    jsonFacturados.put("iva5", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(i5))));
                    jsonFacturados.put("iva52", i5);
                }
                if (facturaCompraDet.getPoriva() == 10) {
                    String cantidadi = String.valueOf(facturaCompraDet.getCantidad()).replace(".000", "");
                    long totales = Long.parseLong(cantidadi) * facturaCompraDet.getPrecio();
                    to10 += totales;
                    g10 += facturaCompraDet.getIva();
                    i10 = to10 - g10;

                    jsonFacturados.put("gra102", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(g10)))));
                    jsonFacturados.put("gra10", String.valueOf(Math.round(g10)));
                    jsonFacturados.put("iva102", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(i10))));
                    jsonFacturados.put("iva10", String.valueOf(i10));
                } else {
                    to10 += 0;
                    g10 += 0;
                    i10 = to10 - g10;
                    jsonFacturados.put("gra52", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(Math.round(g10)))));
                    jsonFacturados.put("gra5", Math.round(g10));
                    jsonFacturados.put("iva52", numVal.numberFormat("###,###.###", Double.parseDouble(String.valueOf(i10))));
                    jsonFacturados.put("iva5", i10);
                }
                String cantidadi = String.valueOf(facturaCompraDet.getCantidad()).replace(".000", "");

            }
            listaArray.add(jsonFacturados);
        }

//        Iterator it2 = mapeoFactura.entrySet().iterator();
//        while (it2.hasNext()) {
//            Map.Entry e = (Map.Entry) it2.next();
//            try {
//                JSONObject jsonFacturas = (JSONObject) parser.parse(e.getValue().toString());
//                g5 = Long.parseLong(jsonFacturas.get("gra5").toString());
//                g10 = Long.parseLong(jsonFacturas.get("gra10").toString());
//                i5 = Long.parseLong(jsonFacturas.get("iva5").toString());
//                i10 = Long.parseLong(jsonFacturas.get("iva10").toString());
//                ex = Long.parseLong(jsonFacturas.get("exe").toString());
//                to = Long.parseLong(jsonFacturas.get("totFactura").toString());
//                jsonFacturas.put("gra5", g5);
//                jsonFacturas.put("gra10", g10);
//                jsonFacturas.put("iva5", i5);
//                jsonFacturas.put("iva10", i10);
//                jsonFacturas.put("exe", ex);
//                jsonFacturas.put("totFactura", to);
//                listaArray.add(jsonFacturas);
//            } catch (ParseException ex1) {
//                System.out.println("-->> " + ex1.getLocalizedMessage());
//            }
//        }
        return array;
    }
//    private JSONArray recuperarDatos() {
//        JSONArray array = new JSONArray();
//        List<PedidoCompra> listPedidoCompra = pedidoCompraDAO.listarPorCabecera(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()));
//        if (listPedidoCompra.size() > 0) {
//            int ord = 0;
//            for (PedidoCompra pedidoCompra : listPedidoCompra) {
//                ord++;
//                JSONObject json = new JSONObject();
//                json.put("codigo", ord);
//                json.put("codigo", pedidoCompra.getArticulo().getCodArticulo());
//                json.put("descripcion", pedidoCompra.getDescripcion());
//                json.put("recepcion", "");
//                json.put("conteo", "");
//                json.put("observacion", "");
//
//                proveedor = pedidoCabDAO.getById(pedidoCompra.getPedidoCab().getIdPedidoCab()).getProveedor().getDescripcion();
//                Date date = new Date();
//                Timestamp ts = new Timestamp(date.getTime());
//                String fechaArray[] = ts.toString().split(" ");
////                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
//                StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
////                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
//                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
//                json.put("subRepNomFun", Identity.getNomFun());
//                json.put("subRepTimestamp", subRepTimestamp);
//                json.put("sucursal", pedidoCompra.getPedidoCab().getSucursal());
//                array.add(json);
//            }
//        }
//        return array;
//    }

    private void suprimirSelecciones() {
        JSONParser parser = new JSONParser();
//        int val = 0;
        long monto = 0;
//        long tam = tableViewConteo.getItems().size();
        List aEliminar = new ArrayList();
//        ObservableList<JSONObject> ol = tableViewConteo.getItems();
        for (JSONObject item : tableViewConteo.getItems()) {
            org.json.JSONObject json = new org.json.JSONObject(item);
            JSONObject jsonArti = new JSONObject();
            try {
//                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
                String x = mapeoCheck.get(json.getInt("orden") + "-" + json.getString("codigo")).toString();
                System.out.println("ESTADO: " + x);
                boolean estado = Boolean.parseBoolean(x);
                System.out.println("DATA: " + estado);

            } catch (Exception e) {
                System.out.println("-->> " + e.getLocalizedMessage());
                System.out.println("-->> " + e.getMessage());
                System.out.println("-->> " + e.fillInStackTrace());
//                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                aEliminar.add(item);
            } finally {
            }
            columnCheck.setVisible(false);
        }

        for (int i = 0; i < aEliminar.size(); i++) {
            try {
                System.out.println("-> " + aEliminar.get(i));
                detalleArtList.remove(aEliminar.get(i));
                System.out.println("-STATUS : " + tableViewConteo.getItems().remove(aEliminar.get(i)));

            } catch (Exception e) {
            } finally {
            }
        }
//        actualizarTotales(false);
    }

    private void informandoVentas(String jsonString) {
        if (jsonString.equals("")) {
            mensajeAlerta("NO SE HA REGISTRADO INFORMACION PARA IMPRIMIR EL CRUCE...");
        } else {
            JasperPrint jasperPrint = null;
            JasperReport reporte = null;
            Map pSQL = null;
            try {
//                InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
                InputStream subHeader = this.getClass().getResourceAsStream("/jasper/subreport/SUB_REPORTE.jasper");
                InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
                InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
                reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + "REP_CRUCE.jasper"));
                String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + "REP_CRUCE.pdf";
                File fichero = new File(dir);

                if (fichero.delete()) {
                    System.out.println("El fichero ha sido borrado satisfactoriamente");
                } else {
                    System.out.println("El fichero no pudó ser borrado");
                }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
                pSQL = new HashMap<String, Object>();
                // CUERPO - REPORTE
                pSQL.put("repJsonString", jsonString);
                pSQL.put("jsonStringFactura", "{\"facturas\": " + listaArray.toString() + "}");
                //            pSQL.put("numCaja", cbNroCaja.getSelectionModel().getSelectedItem().toString());
                //            pSQL.put("repCodPeluq", codPeluq);
                //            pSQL.put("nroFactura", txtNUmFactura);
                // CABECERA - SUB REPORTE
                //            Sucursal suc = sucDAO.consultandoSuc(1L);
                //                pSQL.put("proveedor", proveedor);
                pSQL.put("empresa", "PARANA FUNCIONAL S.A.");
//            pSQL.put("subRepEmpresaDir", suc.getCallePrincipal() + " e/ " + suc.getPrimeraLateral() + " y " + suc.getSegundaLateral());
                pSQL.put("subRepEmpresa", "CASA PARANA");
                pSQL.put("subRepSucursal", "CASA CENTRAL");
//            pSQL.put("subRepSucursalDir", "");
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            Date date = new Date();
//            Timestamp ts = new Timestamp(date.getTime());
//            String fechaArray[] = ts.toString().split(" ");
//            String horaArray[] = ts.toString().split(".");
//            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + horaArray[0];
//            pSQL.put("subRepNomFun", Identity.getNomFun());
//            pSQL.put("subRepTimestamp", subRepTimestamp);
//                pSQL.put("subRepPathLogo", subImg);
                pSQL.put("subRepPathLogoCP", subImgCP);

                pSQL.put("SUBREPORT_DIR", subHeader);
                // CABECERA - SUB REPORTE
                jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
                new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
            } catch (JRException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
//                System.out.println("-->> "+ex.);
                Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
            }
        }
    }

    private void recortarDetalle() {
        if (!estadoCheck) {
            long total = 0;
            List<JSONObject> lista = getDetalleArtList();
            detalleArtList = new ArrayList<>();
            tableViewConteo.getItems().clear();
            for (JSONObject json : lista) {
//                    org.json.JSONObject jsonDat = new org.json.JSONObject(json);
//                    mapeoCheck.put(jsonDat.getInt("codigo") + "-" + jsonDat.getBigInteger("factura"), true);
//                total += Math.round(Long.parseLong(json.get("pagar").toString()));
                detalleArtList.add(json);
            }
//            labelTotalGs.setText(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(total))));
//                suprimirSelecciones();

            actualizandoTablaCliente(getDetalleArtList());
//                precioTotal = total;
//                resetFormulario(total);

//                cerrando();
        } else {
            suprimirSelecciones();
        }
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    @FXML
    private void tableViewConteoMouseClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            compra = txtCompra.getText();
            entrada = txtNroEntrada.getText();
            cod = txtCodigo.getText();
//            sucursal = cbSucursal.getSelectionModel().getSelectedItem();
//            System.out.println("-->> " + tableViewFactura.getSelectionModel().getSelectedItem().toString());
//            if ("cliente_caja") {
//                
//            }
//            
//                ) {
//            BuscarClienteMayFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
            ModCantidadFXMLController.setCantidad(tableViewConteo.getSelectionModel().getSelectedItem().get("cantidad").toString(), tableViewConteo);
            this.sc.loadScreenModal("/vista/stock/ModCantidadFXML.fxml", 232, 95, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//            }else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//
//            }
        }
    }

    private void seleccionarTodo() {
        List<JSONObject> lista = getDetalleArtList();
        detalleArtList = new ArrayList<>();
        tableViewConteo.getItems().clear();
        for (JSONObject json : lista) {
            mapeoCheck.put(Integer.parseInt(json.get("orden") + "") + "-" + json.get("codigo").toString(), true);
//            System.out.println("ESTADO: " + x);
//            boolean estado = Boolean.parseBoolean(x);
//            System.out.println("DATA: " + estado);
            detalleArtList.add(json);
        }

        actualizandoTablaCliente(getDetalleArtList());

        suprimirSelecciones();
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    @FXML
    private void btnImprimirAction(ActionEvent event) {
        imprimirCruce();
    }

    @FXML
    private void btnGenerarOrdenAction(ActionEvent event) {
        generarOrdenDevolucion();
    }

    private void imprimirCruce() {
        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
            JSONArray jsonFacturas = recuperarDatos();

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("");

            } else {
                JSONArray array = jsonFacturas;
                informandoVentas("{\"ventas\": " + array + "}");
            }
            this.sc.loadScreen("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/CruceFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA IMPRIMIR LA ORDEN...");
        }
    }

    class BooleanCell extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCell(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int posicion = tableViewConteo.getSelectionModel().getSelectedIndex();
                    if (posicion >= 0) {
                        String ord = tableViewConteo.getSelectionModel()
                                .getSelectedItem().get("orden") + "-" + tableViewConteo.getSelectionModel()
                                .getSelectedItem().get("codigo");
                        if (checkBox.isSelected()) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
                            System.out.println(ord + " | " + true);
                            tableViewConteo.getSelectionModel().getSelectedItem().put("check", true);
                        } else {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
                            System.out.println(ord + " | " + false);
                            tableViewConteo.getSelectionModel().getSelectedItem().put("check", false);
                        }
                    }
                }
            });
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    @FXML
    private void btnFiltrarAction(ActionEvent event) {
        filtrar();
    }

    @FXML
    private void btnBuscarAction(ActionEvent event) {
        seleccionar();
    }

    private void btnSeleccionarTodoAction(ActionEvent event) {
        seleccionarTodo();
    }

    private void btnProcesarAction(ActionEvent event) {
        procesar();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    private void filtrar() {
        try {
            List<FacturaCompraCab> lisPedido = facturaCompraCabDAO.getByOCFiltro(txtCompra.getText());
            detalleArtList = new ArrayList<>();
            tableViewConteo.getItems().clear();
            int orden = 0;

            HashMap mapeo = new HashMap();

            if (lisPedido.size() > 0) {
                for (FacturaCompraCab pd : lisPedido) {
                    orden++;
                    JSONObject jsonObj = new JSONObject();
                    jsonObj.put("orden", orden);
                    jsonObj.put("codigo", pd.getProveedor().getRuc() + "");
                    jsonObj.put("timbrado", pd.getProveedor().getTimbrado() + "");
                    jsonObj.put("factura", pd.getNroDoc() + "");
                    jsonObj.put("idFacturaCompraCab", pd.getIdFacturaCompraCab() + "");
                    jsonObj.put("idPedidoCab", pd.getPedidoCab().getIdPedidoCab() + "");
                    jsonObj.put("razon", pd.getProveedor().getDescripcion() + "");
                    if (articuloDevolucionDAO.getByIdFacturaCab(pd.getIdFacturaCompraCab()) == null) {
                        jsonObj.put("dato", "false");
                    } else {
                        jsonObj.put("dato", "true");
                    }
                    String sucu = "";
                    if (pd.isCc()) {
                        sucu += "CENTRAL. ";
                    }
                    if (pd.isSc()) {
                        sucu += "CACIQUE. ";
                    }
                    if (pd.isSl()) {
                        sucu += "SAN LORENZO. ";
                    }
                    jsonObj.put("sucursal", sucu);
                    jsonObj.put("oc", pd.getPedidoCab().getOc());
                    jsonObj.put("pedido", pd.getPedidoCab().getNroPedido());
                    jsonObj.put("fecha", pd.getFechaDoc() + "");
                    jsonObj.put("obs", pd.getObservacion());
                    jsonObj.put("cantidad", pd.getCantidad());
                    if (mapeo.containsKey(sucu.trim())) {
                    } else {
                        mapeo.put(sucu.trim(), sucu.trim());
                        detalleArtList.add(jsonObj);
                    }
                }
                actualizandoTablaCliente(detalleArtList);
            } else {
                mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA...");
            }
        } catch (Exception e) {
            mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA...");
        } finally {
        }

    }
//    private void filtrar() {
//        try {
////            if (dpFecha.getValue() != null) {
//            if (txtCompra.getText().equals("")) {
//                mensajeAlerta("EL CAMPO OC NO DEBE QUEDAR VACIO");
//            } else {
//                List<Recepcion> listRecepcion = recepcionDAO.getByNroOrdenEntradaFecha(txtCompra.getText(), txtNroEntrada.getText(), dpFecha.getValue());
//
//                if (listRecepcion.size() <= 0) {
//                    PedidoCab pc = pedidoCabDAO.listarPorOrden(txtCompra.getText());
//                    listRecepcion.add(recepcionDAO.getByIdFecha(pc.getIdPedidoCab(), dpFecha.getValue()));
//                }
//                detalleArtList = new ArrayList<>();
//                tableViewConteo.getItems().clear();
//                int orden = 0;
//
//                if (listRecepcion.size() > 0) {
//                    for (Recepcion pd : listRecepcion) {
//                        orden++;
//                        JSONObject jsonObj = new JSONObject();
//                        jsonObj.put("orden", orden);
//                        jsonObj.put("codigo", pd.getProveedor().getRuc() + "");
//                        jsonObj.put("timbrado", pd.getProveedor().getTimbrado() + "");
//                        jsonObj.put("factura", pd.getNroDoc() + "");
//                        jsonObj.put("idRecepcion", pd.getIdRecepcion() + "");
//                        jsonObj.put("razon", pd.getProveedor().getDescripcion() + "");
//                        jsonObj.put("entrada", pd.getNroOrden());
//                        jsonObj.put("oc", pd.getPedidoCab().getOc());
//                        jsonObj.put("pedido", pd.getPedidoCab().getNroPedido());
//                        jsonObj.put("fecha", pd.getFechaDoc() + "");
//                        jsonObj.put("cantidad", pd.getCantRec());
//
//                        detalleArtList.add(jsonObj);
//                    }
//                    actualizandoTablaCliente(detalleArtList);
//                } else {
//                    mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA...");
//                }
//            }
////            } else {
////                mensajeAlerta("EL CAMPO FECHA NO DEBE QUEDAR VACIO");
////            }
//        } catch (Exception e) {
//            mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA...");
//        } finally {
//        }
//
//    }

    private void seleccionar() {
        for (int i = 0; i < tableViewConteo.getItems().size(); i++) {
            JSONObject json = (JSONObject) tableViewConteo.getItems().get(i);
            if (json.get("codigo").toString().equalsIgnoreCase(txtCodigo.getText())) {
                tableViewConteo.getSelectionModel().select(i);
                break;
            }
        }
    }

    private void procesar() {
        int i = 0;
        for (JSONObject jSONObject : getDetalleArtList()) {
            PedidoDetConteo pdc = new PedidoDetConteo();
            pdc.setDescripcion(jSONObject.get("descripcion").toString());
            pdc.setCantidad(jSONObject.get("cantidad").toString());
            pdc.setPrecio(Long.parseLong(jSONObject.get("precio").toString()));
            long x = Long.parseLong(jSONObject.get("cantidad").toString()) * Long.parseLong(jSONObject.get("precio").toString());
            pdc.setTotal(x);

            Articulo art = new Articulo();
            art.setIdArticulo(Long.parseLong(jSONObject.get("idArticulo").toString()));
            pdc.setArticulo(art);

            PedidoCab pc = new PedidoCab();
            pc.setIdPedidoCab(idPedidoCab);
            pdc.setPedidoCab(pc);

            i++;
            pedidoDetConteoDAO.insercionMasiva(pdc, i);
        }
        if (i >= 1) {
            mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE");
            limpiarDatos();
        }
    }

    private void generarOrdenDevolucion() {
        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
            DevolucionFXMLController.cargarRecepcion(tableViewConteo);
            this.sc.loadScreenModal("/vista/stock/DevolucionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/CruceFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA GENERAR ORDEN DEVOLUCIÓN...");
        }
    }

    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button cancelarButton = new Button(" X ");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            cancelarButton.contentDisplayProperty().setValue(ContentDisplay.CENTER);
            cancelarButton.setStyle("-fx-background-color: linear-gradient(#ff5400, #be1d00);\n"
                    + "    -fx-background-radius: 16;\n"
                    + "    -fx-background-insets: 0;\n"
                    + "    -fx-text-fill: white;"
                    + "-fx-font-size: 14px;");
            HBoxButtons.getChildren().add(cancelarButton);
            HBoxButtons.setSpacing(6);
            cancelarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            cancelarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    JSONParser parser = new JSONParser();

                    table.getSelectionModel().select(getTableRow().getIndex());
                    int posicion = tableViewConteo.getSelectionModel().getSelectedIndex();
                    if (posicion >= 0) {
                        boolean valo = false;
                        try {
                            valo = mapeoCruce.containsKey(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString() + "-" + tableViewConteo.getSelectionModel().getSelectedItem().get("idFacturaCompraCab").toString());
                        } catch (Exception ex) {
                        } finally {
                        }
//                        if (tableViewConteo.getSelectionModel().getSelectedItem().get("dato").equals("true")) {
                        if (valo) {
                            mensajeAlerta("YA SE HA GENERADO LA NOTA DE DEVOLUCION...");
                        } else {
                            if (recuperarLeyendas(tableViewConteo) > 0) {
                                DevolucionFXMLController.setIdFacturaCompraCab(tableViewConteo.getSelectionModel().getSelectedItem().get("idFacturaCompraCab").toString(), tableViewConteo, tableViewConteo.getSelectionModel().getSelectedIndex());
                                repeatFocusData();
                            } else {
                                mensajeAlerta("NO EXISTEN PRODUCTOS PARA DEVOLUCIÓN");
                            }
                        }
                    }
                }
            });
        }

        @Override

        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }

    private void repeatFocusData() {
        Platform.runLater(() -> {
            this.sc.loadScreenModal("/vista/stock/DevolucionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/CruceFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            filtrar();
        });
    }

    private int recuperarLeyendas(TableView<JSONObject> tableViewConteo) {
        int count = 0;
        JSONArray array = new JSONArray();
        HashMap mapeoCodigo = new HashMap();
        HashMap mapeoCodigoCantidad = new HashMap();
//        List<FacturaCompraDet> listFCD = fcdDAO.listarPorRecepcion(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idFacturaCompraCab").toString()));
        List<FacturaCompraDet> listFCD = fcdDAO.buscarPorSucursalOC(tableViewConteo.getSelectionModel().getSelectedItem().get("oc").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());
        for (FacturaCompraDet pedidoDet : listFCD) {
            if (!mapeoCodigoCantidad.containsKey(pedidoDet.getArticulo().getCodArticulo())) {
                String canti = pedidoDet.getCantidad().toString().replace(".000", "");
                mapeoCodigoCantidad.put(pedidoDet.getCodArticulo(), canti);
            } else {
                String canti = pedidoDet.getCantidad().toString().replace(".000", "");
                int data = Integer.parseInt(mapeoCodigoCantidad.get(pedidoDet.getCodArticulo()).toString()) + Integer.parseInt(canti);
                mapeoCodigoCantidad.remove(pedidoDet.getCodArticulo());
                mapeoCodigoCantidad.put(pedidoDet.getCodArticulo(), data);
            }
        }
        if (listFCD.size() > 0) {
            for (FacturaCompraDet factCompraDet : listFCD) {
                JSONObject json = new JSONObject();
                PedidoCompra pdc = pedidoCompraDAO.getByIdPedidoCabAndCodArt(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()), factCompraDet.getArticulo().getIdArticulo());
                PedidoDetConteo pdconteo = pedidoDetConteoDAO.getByIdPedidoCabAndCodArt(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()), factCompraDet.getArticulo().getIdArticulo(), tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString());

                int cantida = 0;
                int cantidadConteo = 0;

                String data = tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString();
                String suc = data.substring(0, (data.length() - 2));
                StringTokenizer sts = new StringTokenizer(suc, ".");
                String suc1 = "";
                try {
                    suc1 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }
                String suc2 = "";
                try {
                    suc2 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }
                String suc3 = "";
                try {
                    suc3 = sts.nextElement().toString(); //1   
                } catch (Exception e) {
                } finally {
                }

                suc1 = suc1.trim();
                suc2 = suc2.trim();
                suc3 = suc3.trim();

                if (suc1.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc1.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc1.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (suc3.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc3.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc3.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }

                if (suc2.equalsIgnoreCase("CENTRAL")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantCc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getCc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
//                    break;
                } else if (suc2.equalsIgnoreCase("SAN LORENZO")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSl() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSl()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                } else if (suc2.equalsIgnoreCase("CACIQUE")) {
                    try {
                        cantida += Integer.parseInt(pdc.getCantSc() + "");
                    } catch (Exception e) {
                    } finally {
                    }
                    try {
                        if (pdconteo.getSc()) {
                            cantidadConteo = Integer.parseInt(pdconteo.getCantidad());
                        }
                    } catch (Exception e) {
                    } finally {
                    }
                }
//                json.put("cantidad", canti);
                json.put("cantidad", cantida);
                json.put("recibido", cantidadConteo + "");

//                String canti = factCompraDet.getCantidad().toString().replace(".000", "");
                int facturado = Integer.parseInt(mapeoCodigoCantidad.get(factCompraDet.getCodArticulo()).toString());
                json.put("factura", facturado + "");
                json.put("bonificacion", "0");
//                int result = cantida - Integer.parseInt(canti);
                int result = Integer.parseInt(json.get("recibido").toString()) - Integer.parseInt(json.get("factura").toString());
                json.put("cantajuste", result + "");
                json.put("preciodos", factCompraDet.getPrecio() + "");
                json.put("impajuste", "0");
                json.put("leyenda", "F+");
                String leyenda = "";
                if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("recibido").toString())) {
                    leyenda = "C";
                    count++;
                }
                if (Integer.parseInt(json.get("factura").toString()) > Integer.parseInt(json.get("cantidad").toString())) {
                    leyenda = "C+";
                    count++;
                }
                if (Integer.parseInt(json.get("cantidad").toString()) == 0) {
                    leyenda = "F+";
                    count++;
                }
                try {
                    if (factCompraDet.getPrecio() > pdc.getPrecio()) {
                        leyenda = "P";
                        count++;
                    }
                } catch (Exception e) {
                } finally {
                }
            }
        }
        return count;
    }
}
