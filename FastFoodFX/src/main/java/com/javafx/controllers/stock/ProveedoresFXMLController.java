package com.javafx.controllers.stock;

import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.FamiliaProv;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.BarrioDAO;
import com.peluqueria.dao.CiudadDAO;
import com.peluqueria.dao.DepartamentoDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PaisDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dto.BarrioDTO;
import com.peluqueria.dto.CiudadDTO;
import com.peluqueria.dto.DepartamentoDTO;
import com.peluqueria.dto.PaisDTO;
import com.peluqueria.dto.ProveedorDTO;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.util.Utilidades;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
public class ProveedoresFXMLController extends BaseScreenController implements Initializable {

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> proveedorList;
    private boolean escucha;

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    ProveedorDAO proveedorDAO;
    @Autowired
    PaisDAO paisDAO;
    @Autowired
    CiudadDAO ciudadDAO;
    @Autowired
    BarrioDAO barrioDAO;
    @Autowired
    DepartamentoDAO dptoDAO;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TabPane tabPaneSeleccionMenu;
    @FXML
    private TitledPane titledPaneCaja;
    @FXML
    private AnchorPane anchorPaneCaja;
    @FXML
    private TitledPane titledPaneCuponera;
    @FXML
    private AnchorPane anchorPaneCuponera;
    @FXML
    private Button buttonCerrar;
    @FXML
    private TableView<JSONObject> tableViewProveedor;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private Tab tabTrbajarCon;
    @FXML
    private TextField txtBuscar;
    @FXML
    private RadioButton radioBtnNombreCta;
    @FXML
    private RadioButton radioBtnCodigoCta;
    @FXML
    private Label lblCoincidencia;
    @FXML
    private Tab tabDatosCuenta;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnGuardar;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtVtoDia;
    @FXML
    private ComboBox<String> cbFamilia;
    private HashMap<String, JSONObject> hashFamilia;
    @FXML
    private TextField txtDireccion;
    @FXML
    private ComboBox<String> cbPais;
    private HashMap<String, JSONObject> hashPais;
    @FXML
    private ComboBox<String> cbDpto;
    private HashMap<String, JSONObject> hashDpto;
    @FXML
    private ComboBox<String> cbCiudad;
    private HashMap<String, JSONObject> hashCiudad;
    @FXML
    private ComboBox<String> cbBarrio;
    private HashMap<String, JSONObject> hashBarrio;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtCelular;
    @FXML
    private TextField txtMail;
    @FXML
    private TextField txtRuc;
    @FXML
    private TextField txtTimbrado;
    @FXML
    private TextField txtNroAcuerdo;
    @FXML
    private TextField txtSaldo;
    @FXML
    private TextArea txtContacto;
    @FXML
    private TextField txtUsuAlta;
    @FXML
    private TextField txtUsuMod;
    @FXML
    private TextField txtFechaAlta;
    @FXML
    private TextField txtFechaMod;
    @FXML
    private TextField txtSIDI;
    @FXML
    private Button btnActualizar;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField txtIdProveedor;
    @FXML
    private TextField txtNombre1;
    @FXML
    private ComboBox<String> cbPersona;
    @FXML
    private TextField txtTelefono11;
    @FXML
    private TextField txtTelefono111;
    @FXML
    private TextField txtTelefono1111;
    @FXML
    private ComboBox<String> cbCondicion;
    @FXML
    private TextField txtTelefono1;
    @FXML
    private TextField txtTelefono2;
    @FXML
    private TextField txtCelular2;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtSaldo1;
    @FXML
    private TextArea txtContacto1;
    @FXML
    private TextField txtIdProveedor1;
    @FXML
    private TextField txtNumeroTimbrado;
    @FXML
    private DatePicker dpInicioVigencia;
    @FXML
    private DatePicker dpFinVigencia;

    final KeyCombination altN = new KeyCodeCombination(KeyCode.N, KeyCombination.ALT_DOWN);
    @FXML
    private Label lblCoincidencia1;
    @FXML
    private TextField txtCiudad;
    @FXML
    private TextField txtDpto;
    @FXML
    private TextField txtPais;
    @FXML
    private TextField txtBarrio;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha = false;
        hashPais = new HashMap<>();
        hashCiudad = new HashMap<>();
        hashBarrio = new HashMap<>();
        hashDpto = new HashMap<>();
        hashFamilia = new HashMap<>();
        cargarFamilia();
        cargarPais();
        listenerCampos();
        listenerCamposCombos();
        repeatFocus(txtBuscar);
        ubicandoContenedorSecundario();
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void listenerCampos() {
        cbFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                System.out.println("--->> " + cbFamilia.getSelectionModel().getSelectedItem());
//                cbSubFamilia.getItems().clear();
//                JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
            }
        });
    }

    @FXML
    private void buttonCerrarAction(ActionEvent event) {
        cerrandoSesion();
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
        limpiar();
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        guardarProveedor();
    }

    private void cerrandoSesion() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/stock/proveedoresFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
    }

    private void configuracionCuponera() {
//        if ("configuracion_descuento")) {//falta aún...
        Utilidades.setIdRangoLocal(999l);
        this.sc.loadScreen("/vista/cuponera/CuponeraConfigFXML.fxml", 726, 560, "/vista/login/menuEsteticaFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuEsteticaFXML.fxml", 540, 359, true);
//        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
//            if (tabArticulos.isSelected()) {
//            } else if (tabCuponera.isSelected()) {
//                configuracionCuponera();
//            }
        }
        if (keyCode == event.getCode().F2) {
            if (!btnGuardar.isDisabled()) {
                guardarProveedor();
            }
        }
        if (keyCode == event.getCode().F3) {
        }
        if (keyCode == event.getCode().F4) {
        }
        if (keyCode == event.getCode().F5) {
            if (!btnActualizar.isDisabled()) {
                actualizarProveedor();
            }
        }
        if (keyCode == event.getCode().F8) {
            if (!btnCancelar.isDisabled()) {
                limpiar();
//                btnGuardar.setDisable(true);
//                btnActualizar.setDisable(false);
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (txtBuscar.isFocused()) {
                buscarCliente();
            }
        }

        if (altN.match(event)) {
            if (!btnNuevo.isDisabled()) {
                limpiar();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            cerrandoSesion();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

//    private void btnCargarArticuloAction(ActionEvent event) {
//        if (tab.isSelected()) {
//        } else if (tabCuponera.isSelected()) {
//            configuracionCuponera();
//        }
//    }
    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void buscarCliente() {
//        proveedorList = new ArrayList<>();
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        if (txtBuscar.getText().equalsIgnoreCase("")) {
            mensajeAlerta("Debes ingresar una descripción para realizar la búsqueda");
        } else if (!radioBtnNombreCta.isSelected() && !radioBtnCodigoCta.isSelected()) {
            mensajeAlerta("Debes selecionar un rango de búsqueda");
        } else {
            String filtro = txtBuscar.getText();
//            String urlServer = "";
            if (radioBtnNombreCta.isSelected()) {
//                urlServer = "null/" + filtro;
                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc(filtro, "null");
                if (listProveedor != null) {
                    tableViewProveedor.getItems().clear();
                    proveedorList = new ArrayList<>();
                    for (Proveedor pro : listProveedor) {
                        ProveedorDTO proDTO = pro.toProveedorDTO();
                        proDTO.setFechaAlta(null);
                        proDTO.setFechaMod(null);
                        JSONObject json;
                        try {
                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
                            proveedorList.add(json);
                            actualizandoTabla();
                        } catch (ParseException ex) {
                            Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else {
                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
                }
            } else {
//                urlServer = filtro + "/null";
                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc("null", filtro);
                if (!listProveedor.isEmpty() || listProveedor != null) {
                    tableViewProveedor.getItems().clear();
                    proveedorList = new ArrayList<>();
                    for (Proveedor pro : listProveedor) {
                        ProveedorDTO proDTO = pro.toProveedorDTO();
                        proDTO.setFechaAlta(null);
                        proDTO.setFechaMod(null);
                        JSONObject json;
                        try {
                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
                            proveedorList.add(json);
                            actualizandoTabla();
                        } catch (ParseException ex) {
                            Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                } else {
                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
                }
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {a
//                try {set
//                    URL url = new URL(Utilidades.ip + "/ServerParana/proveedor/" + urlServer);
//                    URLConnection uc = url.openConnection();
//                    BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        valor = (JSONArray) parser.parse(inputLine);
//                    }
//                    br.close();
//                    if (!valor.isEmpty()) {
//                        proveedorList = new ArrayList<>();
//                        for (Object obj : valor) {
//                            JSONObject json = (JSONObject) parser.parse(obj.toString());
//                            proveedorList.add(json);
//                        }
//                        actualizandoTabla();
//                    } else {
//                        mensajeAlerta("NO SE ENCONTRARON DATOS");
//                    }
//                } catch (FileNotFoundException e) {
//                    System.out.println(e.getLocalizedMessage());
//                } catch (IOException e) {
//                    System.out.println(e.getLocalizedMessage());
//                } catch (org.json.simple.parser.ParseException e) {
//                    e.printStackTrace();
//                }
//            } else {
//                mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//            }

        }
    }

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTabla() {
        clienteData = FXCollections.observableArrayList(proveedorList);
        //columna Nombre ..................................................
        tableColumnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String cod = String.valueOf(data.getValue().get("ruc"));
                return new ReadOnlyStringWrapper(cod.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("descripcion"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
        //columna Ruc .................................................
        tableViewProveedor.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

    private void escucha() {
        JSONParser parser = new JSONParser();
        escucha = true;
        tableViewProveedor.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewProveedor.getSelectionModel().getSelectedItem() != null) {
//                mensajeAlerta("-> " + newSelection.get("codigo").toString().toUpperCase() + " ) " + newSelection.get("nombre").toString().toUpperCase());
                txtRuc.setText(newSelection.get("ruc").toString());
                StringTokenizer st = new StringTokenizer(newSelection.get("ruc").toString(), "-");
                txtCodigo.setText(st.nextElement().toString());
                txtNombre.setText(newSelection.get("descripcion").toString().toUpperCase());
                txtIdProveedor.setText(newSelection.get("idProveedor").toString().toUpperCase());
                cbCondicion.getSelectionModel().select(newSelection.get("condPago").toString());

                try {
                    txtPais.setText(newSelection.get("paisProv").toString().toUpperCase());
                } catch (Exception e) {
                    txtPais.setText("");
                } finally {
                }
                try {
                    txtDpto.setText(newSelection.get("departamentoProv").toString().toUpperCase());
                } catch (Exception e) {
                    txtDpto.setText("");
                } finally {
                }
                try {
                    txtCiudad.setText(newSelection.get("ciudadProv").toString().toUpperCase());
                } catch (Exception e) {
                    txtCiudad.setText("");
                } finally {
                }
                try {
                    txtBarrio.setText(newSelection.get("barrioProv").toString().toUpperCase());
                } catch (Exception e) {
                    txtBarrio.setText("");
                } finally {
                }

                try {
                    txtNombre1.setText(newSelection.get("descripcion2").toString().toUpperCase());
                } catch (Exception ex) {
                    txtNombre1.setText("");
                } finally {
                }
                try {
                    txtNumeroTimbrado.setText(newSelection.get("timbrado").toString().toUpperCase());
                } catch (Exception ex) {
                } finally {
                }
                try {
                    String inicio = newSelection.get("inicioVigencia").toString();
                    Timestamp tInicio = Timestamp.valueOf(inicio);
//                    java.sql.Date desde = java.sql.Date.valueOf(tInicio.getTime());
                    java.sql.Date desde = Date.valueOf(tInicio.toLocalDateTime().toLocalDate());
                    dpInicioVigencia.setValue(desde.toLocalDate());
                } catch (Exception ex) {
                } finally {
                }
                try {
                    String fin = newSelection.get("finVigencia").toString();
                    Timestamp tFin = Timestamp.valueOf(fin);
                    java.sql.Date hasta = Date.valueOf(tFin.toLocalDateTime().toLocalDate());
                    dpFinVigencia.setValue(hasta.toLocalDate());
                } catch (Exception ex) {
                } finally {
                }

                cbPersona.setValue("JURIDICA");
                if (newSelection.containsKey("callePrincipal")) {
                    txtDireccion.setText(newSelection.get("callePrincipal").toString());
                }
                if (newSelection.containsKey("telefono")) {
                    txtTelefono.setText(newSelection.get("telefono").toString());
                }
                if (newSelection.containsKey("telefono2")) {
                    txtCelular.setText(newSelection.get("telefono2").toString());
                }
                if (newSelection.containsKey("email")) {
                    txtMail.setText(newSelection.get("email").toString());
                }
                if (newSelection.containsKey("timbrado")) {
                    txtTimbrado.setText(newSelection.get("timbrado").toString());
                }
                if (newSelection.containsKey("usuAlta")) {
                    txtUsuAlta.setText(newSelection.get("usuAlta").toString());
                }
                if (newSelection.containsKey("usuMod")) {
                    txtUsuMod.setText(newSelection.get("usuMod").toString());
                }
                if (newSelection.containsKey("fechaAlta")) {
                    txtFechaAlta.setText(newSelection.get("fechaAlta").toString());
                }
                if (newSelection.containsKey("fechaMod")) {
                    txtFechaMod.setText(newSelection.get("fechaMod").toString());
                }

//                txtVtoDia.setText(newSelection.get("condven").toString());
                txtNroAcuerdo.setText("");
                txtContacto.setText("");
//                txtSaldo.setText(newSelection.get("saldo").toString());
                txtSIDI.setText("");

                if (newSelection.containsKey("familiaProv")) {
                    try {
                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("familiaProv").toString());
                        cbFamilia.setValue(objFlia.get("descripcion").toString().toUpperCase());
                    } catch (ParseException ex) {
                        Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (newSelection.containsKey("pais")) {
                    try {
                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("pais").toString());
                        cbPais.setValue(objFlia.get("descripcion").toString().toUpperCase());
                    } catch (ParseException ex) {
                        Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (newSelection.containsKey("departamento")) {
                    try {
                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("departamento").toString());
                        cbDpto.setValue(objFlia.get("descripcion").toString().toUpperCase());
                    } catch (ParseException ex) {
                        Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (newSelection.containsKey("ciudad")) {
                    try {
                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("ciudad").toString());
                        cbCiudad.setValue(objFlia.get("descripcion").toString().toUpperCase());
                    } catch (ParseException ex) {
                        Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if (newSelection.containsKey("barrio")) {
                    try {
                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("barrio").toString());
                        cbBarrio.setValue(objFlia.get("descripcion").toString().toUpperCase());
                    } catch (ParseException ex) {
                        Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            } else {
            }
        });
    }

    private void cargarFamilia() {
        cbPersona.getItems().add("JURIDICA");
        cbPersona.getItems().add("FISICA");

//        cbCondicion.getItems().add("10");
        cbCondicion.getItems().add("15");
        cbCondicion.getItems().add("30");
        cbCondicion.getItems().add("45");

        cbFamilia.getItems().add("COMERCIALES");
        cbFamilia.getItems().add("DISTRIBUIDORES");

        JSONObject obj1 = new JSONObject();
        obj1.put("idFamiliaProv", 1);
        obj1.put("descripcion", "COMERCIALES");

        JSONObject obj2 = new JSONObject();
        obj2.put("idFamiliaProv", 2);
        obj2.put("descripcion", "DISTRIBUIDORES");

        hashFamilia.put("COMERCIALES", obj1);
        hashFamilia.put("DISTRIBUIDORES", obj2);
    }

    private void listenerCamposCombos() {
        cbPais.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                limpiarNiveles(1);
                JSONObject json = hashPais.get(cbPais.getSelectionModel().getSelectedItem());
                cargarDepartamento(Long.parseLong(json.get("idPais").toString()));
            }
        });
        cbDpto.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                limpiarNiveles(2);
                JSONObject json = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem());
                cargarCiudad(Long.parseLong(json.get("idDepartamento").toString()));
            }
        });
        cbCiudad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                limpiarNiveles(3);
                JSONObject json = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem());
                cargarBarrio(Long.parseLong(json.get("idCiudad").toString()));
            }
        });
        cbBarrio.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
//                JSONObject json = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
//                cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
            }
        });
    }

    private void cargarPais() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Pais> listPais = paisDAO.listar();
        if (listPais != null) {
            for (Pais objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                PaisDTO paisDTO = objPais.toPaisDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbPais.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashPais.put(objPais.getDescripcion().toUpperCase(), json);
                } catch (ParseException ex) {
                    Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
//            actualizandoTabla();
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE PAIS");
        }
//        if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {
//            try {
//                URL url = new URL(Utilidades.ip + "/ServerParana/pais");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    valor = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//                if (!valor.isEmpty()) {
//                    for (Object obj : valor) {
//                        JSONObject json = (JSONObject) parser.parse(obj.toString());
//                        proveedorList.add(json);a
//
//                        cbPais.getItems().add(json.get("descripcion").toString());
//                        hashPais.put(json.get("descripcion").toString(), json);
//                    }
//                    actualizandoTabla();
//                } else {
//                    mensajeAlerta("NO SE ENCONTRARON DATOS");
//                }
//            } catch (FileNotFoundException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (IOException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (org.json.simple.parser.ParseException e) {
//                e.printStackTrace();
//            }
//        } else {
//            mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//        }
    }

    private void cargarCiudad(long idDpto) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Ciudad> listPais = ciudadDAO.listarPorCiudad(idDpto);
        if (listPais != null) {
            for (Ciudad objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                CiudadDTO paisDTO = objPais.toOnlyCiudadDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbCiudad.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashCiudad.put(objPais.getDescripcion().toUpperCase(), json);
                } catch (ParseException ex) {
                    Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE CIUDAD");
        }
    }

    private void cargarDepartamento(long idPais) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Departamento> listPais = dptoDAO.listarPorPais(idPais);
        if (listPais != null) {
            for (Departamento objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                DepartamentoDTO paisDTO = objPais.toSinOtrosDatosDepartamentoDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbDpto.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashDpto.put(objPais.getDescripcion().toUpperCase(), json);
                } catch (ParseException ex) {
                    Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void cargarBarrio(long idCiudad) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Barrio> listPais = barrioDAO.listarPorCiudad(idCiudad);
        if (listPais != null) {
            for (Barrio objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                BarrioDTO paisDTO = objPais.toBarrioAClienteDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbBarrio.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashBarrio.put(objPais.getDescripcion().toUpperCase(), json);
                } catch (ParseException ex) {
                    Logger.getLogger(ProveedoresFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void limpiar() {
        txtNombre.setText("");
        cbFamilia.getSelectionModel().select(0);
        cbCondicion.getSelectionModel().select(0);
        cbPais.getSelectionModel().select(0);
        cbDpto.getSelectionModel().select(0);
        cbCiudad.getSelectionModel().select(0);
        cbBarrio.getSelectionModel().select(0);
        cbPersona.getSelectionModel().select(0);

        txtDireccion.setText("");
        txtTelefono.setText("");
        txtCelular.setText("");
        txtMail.setText("");
        txtRuc.setText("");
        txtTimbrado.setText("");
        txtNroAcuerdo.setText("");
        txtContacto.setText("");
        txtSaldo.setText("");

        repeatFocus(txtRuc);
//        btnGuardar.setDisable(false);
//        btnActualizar.setDisable(true);

        txtNombre1.setText("");
        txtNumeroTimbrado.setText("");

        txtPais.setText("");
        txtDpto.setText("");
        txtCiudad.setText("");
        txtBarrio.setText("");

        dpInicioVigencia.setValue(null);
        dpFinVigencia.setValue(null);

//        tableViewProveedor.getSelectionModel().select(-1);
    }

    @FXML
    private void btnActualizarAction(ActionEvent event) {
        actualizarProveedor();
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
        limpiar();
//        btnGuardar.setDisable(true);
//        btnActualizar.setDisable(false);
    }

    private void guardarProveedor() {
        if (txtRuc.getText().equals("") || txtNombre.getText().equals("")) {
            mensajeAlerta("EL CAMPO RUC Y NOMBRE DEL PROVEEDOR NO DEBEN QUEDAR VACIOS");
        } else {
            Proveedor prov = proveedorDAO.listarPorRuc(txtRuc.getText());
            if (prov.getIdProveedor() != null) {
                mensajeAlerta("EL PROVEEDOR REGISTRADO YA EXISTE!");
            } else {
                Proveedor pro = new Proveedor();
                pro.setCallePrincipal(txtDireccion.getText());
                pro.setEmail(txtMail.getText());
                pro.setDescripcion(txtNombre.getText());
                pro.setDescripcion2(txtNombre1.getText());
                pro.setRuc(txtRuc.getText());
                pro.setTelefono(txtTelefono.getText());
                pro.setTelefono2(txtCelular.getText());
                pro.setCondPago(cbCondicion.getSelectionModel().getSelectedItem());
                pro.setPaisProv(txtPais.getText());
                pro.setDepartamentoProv(txtDpto.getText());
                pro.setCiudadProv(txtCiudad.getText());
                pro.setBarrioProv(txtBarrio.getText());
                if (txtNumeroTimbrado.getText().equals("") || txtNumeroTimbrado.getText().equals("0")) {
                    pro.setTimbrado(0);
                } else {
                    pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
                }
                try {
                    java.sql.Date desde = java.sql.Date.valueOf(dpInicioVigencia.getValue());
                    pro.setInicioVigencia(desde);
                } catch (Exception ex) {
                } finally {
                }
                try {
                    java.sql.Date hasta = java.sql.Date.valueOf(dpFinVigencia.getValue());
                    pro.setFinVigencia(hasta);
                } catch (Exception ex) {
                } finally {
                }
//        pro.setNroLocal(Integer.parseInt(txtNroAcuerdo.getText()));

//        if (!cbFamilia.getValue().toString().equals("")) {
//            JSONObject jsonFLIA = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem().toUpperCase());
                FamiliaProv flia = new FamiliaProv();
                flia.setIdFamiliaProv(1L);
                pro.setFamiliaProv(flia);
//        }
//        if (!cbPais.getValue().toString().equals("")) {
//            JSONObject jsonPAIS = hashPais.get(cbPais.getSelectionModel().getSelectedItem().toUpperCase());
                Pais pais = new Pais();
                pais.setIdPais(0L);
//            pais.setIdPais(Long.parseLong(jsonPAIS.get("idPais").toString()));
                pro.setPais(pais);
//        }
//        if (!cbDpto.getValue().toString().equals("")) {
//            JSONObject jsonDPTO = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem().toUpperCase());
                Departamento dpto = new Departamento();
//            pais.setIdDepartamento(Long.parseLong(jsonDPTO.get("idDepartamento").toString()));
                dpto.setIdDepartamento(0l);
                pro.setDepartamento(dpto);
//        }
//        if (!cbCiudad.getValue().toString().equals("")) {
//            JSONObject jsonCIU = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem().toUpperCase());
                Ciudad ciud = new Ciudad();
                ciud.setIdCiudad(0l);
                pro.setCiudad(ciud);
//        }
//        if (!cbBarrio.getValue().toString().equals("")) {
//            JSONObject jsonBARRIO = hashBarrio.get(cbBarrio.getSelectionModel().getSelectedItem().toUpperCase());
                Barrio bo = new Barrio();
//            pais.setIdBarrio(Long.parseLong(jsonBARRIO.get("idBarrio").toString()));
                bo.setIdBarrio(0l);

                pro.setBarrio(bo);
//        }

                if (proveedorDAO.insertarEstado(pro)) {
                    mensajeAlerta("DATOS REGISTRADOS CORRECTAMENTE");
                    limpiar();
//                btnGuardar.setDisable(true);
//                btnActualizar.setDisable(false);
                } else {
                    mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS VERIFIQUELOS");
                }
            }
        }
    }

    private void actualizarProveedor() {
        if (txtRuc.getText().equals("") || txtNombre.getText().equals("")) {
            mensajeAlerta("EL CAMPO RUC Y NOMBRE DEL PROVEEDOR NO DEBEN QUEDAR VACIOS");
        } else {
            try {
                Proveedor pro = new Proveedor();
                pro.setIdProveedor(Long.parseLong(txtIdProveedor.getText()));
                pro.setCallePrincipal(txtDireccion.getText());
                pro.setEmail(txtMail.getText());
                pro.setDescripcion(txtNombre.getText());
                pro.setDescripcion2(txtNombre1.getText());
                pro.setRuc(txtRuc.getText());
                pro.setTelefono(txtTelefono.getText());
                pro.setTelefono2(txtCelular.getText());
                pro.setCondPago(cbCondicion.getSelectionModel().getSelectedItem());
                pro.setPaisProv(txtPais.getText());
                pro.setDepartamentoProv(txtDpto.getText());
                pro.setCiudadProv(txtCiudad.getText());
                pro.setBarrioProv(txtBarrio.getText());
                try {
                    pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
                } catch (Exception ex) {
                } finally {
                }
                try {
                    java.sql.Date desde = java.sql.Date.valueOf(dpInicioVigencia.getValue());
                    pro.setInicioVigencia(desde);
                } catch (Exception ex) {
                } finally {
                }
                try {
                    java.sql.Date hasta = java.sql.Date.valueOf(dpFinVigencia.getValue());
                    pro.setFinVigencia(hasta);
                } catch (Exception ex) {
                } finally {
                }
//        pro.setNroLocal(Integer.parseInt(txtNroAcuerdo.getText()));
                FamiliaProv flia = new FamiliaProv();
                flia.setIdFamiliaProv(1L);
                pro.setFamiliaProv(flia);
//        }
//        if (!cbPais.getValue().toString().equals("")) {
//            JSONObject jsonPAIS = hashPais.get(cbPais.getSelectionModel().getSelectedItem().toUpperCase());
                Pais pais = new Pais();
                pais.setIdPais(0L);
//            pais.setIdPais(Long.parseLong(jsonPAIS.get("idPais").toString()));
                pro.setPais(pais);
//        }
//        if (!cbDpto.getValue().toString().equals("")) {
//            JSONObject jsonDPTO = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem().toUpperCase());
                Departamento dpto = new Departamento();
//            pais.setIdDepartamento(Long.parseLong(jsonDPTO.get("idDepartamento").toString()));
                dpto.setIdDepartamento(0l);
                pro.setDepartamento(dpto);
//        }
//        if (!cbCiudad.getValue().toString().equals("")) {
//            JSONObject jsonCIU = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem().toUpperCase());
                Ciudad ciud = new Ciudad();
                ciud.setIdCiudad(0l);
                pro.setCiudad(ciud);
//        }
//        if (!cbBarrio.getValue().toString().equals("")) {
//            JSONObject jsonBARRIO = hashBarrio.get(cbBarrio.getSelectionModel().getSelectedItem().toUpperCase());
                Barrio bo = new Barrio();
//            pais.setIdBarrio(Long.parseLong(jsonBARRIO.get("idBarrio").toString()));
                bo.setIdBarrio(0l);

                pro.setBarrio(bo);

//        if (!cbFamilia.getValue().toString().equals("")) {
//            JSONObject jsonFLIA = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem().toUpperCase());
//            FamiliaProv flia = new FamiliaProv();
//            flia.setIdFamiliaProv(Long.parseLong(jsonFLIA.get("idFamiliaProv").toString()));
//            pro.setFamiliaProv(flia);
//        }
//        if (!cbPais.getValue().toString().equals("")) {
//            JSONObject jsonPAIS = hashPais.get(cbPais.getSelectionModel().getSelectedItem().toUpperCase());
//            Pais pais = new Pais();
//            pais.setIdPais(Long.parseLong(jsonPAIS.get("idPais").toString()));
//            pro.setPais(pais);
//        }
//        if (!cbDpto.getValue().toString().equals("")) {
//            JSONObject jsonDPTO = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem().toUpperCase());
//            Departamento pais = new Departamento();
//            pais.setIdDepartamento(Long.parseLong(jsonDPTO.get("idDepartamento").toString()));
//            pro.setDepartamento(pais);
//        }
//        if (!cbCiudad.getValue().toString().equals("")) {
//            JSONObject jsonCIU = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem().toUpperCase());
//            Ciudad pais = new Ciudad();
//            pais.setIdCiudad(Long.parseLong(jsonCIU.get("idCiudad").toString()));
//            pro.setCiudad(pais);
//        }
//        if (!cbBarrio.getValue().toString().equals("")) {
//            JSONObject jsonBARRIO = hashBarrio.get(cbBarrio.getSelectionModel().getSelectedItem().toUpperCase());
//            Barrio pais = new Barrio();
//            pais.setIdBarrio(Long.parseLong(jsonBARRIO.get("idBarrio").toString()));
//            pro.setBarrio(pais);
//        }
                if (proveedorDAO.actualizarEstado(pro)) {
                    mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE");
//            limpiar();
//                btnGuardar.setDisable(true);
//                btnActualizar.setDisable(false);
                } else {
                    mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS VERIFIQUELOS");
                }
            } catch (Exception e) {
                mensajeAlerta("DEBE SELECCIONAR UN PROVEEDOR EN LA PESTAÑA BUSQUEDA, PARA PODER ACTUALIZAR SUS DATOS");
            } finally {
            }
        }
    }

    private void limpiarNiveles(int val) {
        switch (val) {
            case 1:
//                cbPais.getItems().clear();
                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbDpto.setValue("");
                cbCiudad.setValue("");
                cbBarrio.setValue("");
                break;
            case 2:
//                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbCiudad.setValue("");
                cbBarrio.setValue("");

                break;
            case 3:
//                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbBarrio.setValue("");
                break;
            default:
                break;
        }
    }

    @FXML
    private void radioBtnCodigoCtaMouseClicked(MouseEvent event) {
        if (radioBtnCodigoCta.isSelected()) {
            radioBtnNombreCta.setSelected(false);
        } else {
            radioBtnNombreCta.setSelected(true);
        }
    }

    @FXML
    private void radioBtnNombreCtaClicked(MouseEvent event) {
        if (radioBtnNombreCta.isSelected()) {
            radioBtnCodigoCta.setSelected(false);
        } else {
            radioBtnCodigoCta.setSelected(true);
        }
    }

}
