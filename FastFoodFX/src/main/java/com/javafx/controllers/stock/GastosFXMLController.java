package com.javafx.controllers.stock;

import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.Barrio;
import com.peluqueria.core.domain.Ciudad;
import com.peluqueria.core.domain.Departamento;
import com.peluqueria.core.domain.DetalleGasto;
import com.peluqueria.core.domain.Gastos;
import com.peluqueria.core.domain.IpCaja;
import com.peluqueria.core.domain.Iva;
import com.peluqueria.core.domain.Pais;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.Seccion;
import com.peluqueria.core.domain.Unidad;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.BarrioDAO;
import com.peluqueria.dao.CiudadDAO;
import com.peluqueria.dao.DepartamentoDAO;
import com.peluqueria.dao.DetalleGastoDAO;
import com.peluqueria.dao.GastosDAO;
import com.peluqueria.dao.IpCajaDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PaisDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.SeccionDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dto.BarrioDTO;
import com.peluqueria.dto.CiudadDTO;
import com.peluqueria.dto.DepartamentoDTO;
import com.peluqueria.dto.PaisDTO;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ADMIN
 */
@Controller
public class GastosFXMLController extends BaseScreenController implements Initializable {

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    @Autowired
    ArticuloDAO artDAO;

    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> proveedorList;
    private ObservableList<JSONObject> detalleData;
    private List<JSONObject> detalleList;
    private boolean escucha;
    LocalDate now = LocalDate.now(); //2015-11-23
    LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth()); //2015-11-30
    LocalDate firstDay = now.with(TemporalAdjusters.firstDayOfMonth()); //2015-11-30
    final KeyCombination altN = new KeyCodeCombination(KeyCode.N, KeyCombination.ALT_DOWN);

    private Date date = new Date();

    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @Autowired
    SeccionDAO seccionDAO;
    @Autowired
    DetalleGastoDAO detalleGastoDAO;
    @Autowired
    ProveedorDAO proveedorDAO;
    @Autowired
    GastosDAO gastosDAO;
    @Autowired
    IpCajaDAO ipCajaDAO;
    @Autowired
    PaisDAO paisDAO;
    @Autowired
    CiudadDAO ciudadDAO;
    @Autowired
    BarrioDAO barrioDAO;
    @Autowired
    DepartamentoDAO dptoDAO;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPane;
    @FXML
    private TabPane tabPaneSeleccionMenu;
    @FXML
    private TitledPane titledPaneCaja;
    @FXML
    private AnchorPane anchorPaneCaja;
    @FXML
    private TitledPane titledPaneCuponera;
    @FXML
    private AnchorPane anchorPaneCuponera;
    @FXML
    private Button buttonCerrar;
    @FXML
    private TableView<JSONObject> tableViewProveedor;
    private TableColumn<JSONObject, String> tableColumnCodigo;
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private Tab tabTrbajarCon;
    private RadioButton radioBtnNombreCta;
    private RadioButton radioBtnCodigoCta;
    @FXML
    private Tab tabDatosCuenta;
    @FXML
    private Button btnNuevo;
    @FXML
    private Button btnGuardar;
    @FXML
    private TextField txtCodigo;
    private TextField txtNombre;
    @FXML
    private TextField txtVtoDia;
    private ComboBox<String> cbFamilia;
    private HashMap<String, JSONObject> hashFamilia;
    @FXML
    private TextField txtDireccion;
    @FXML
    private ComboBox<String> cbPais;
    private HashMap<String, JSONObject> hashPais;
    @FXML
    private ComboBox<String> cbDpto;
    private HashMap<String, JSONObject> hashDpto;
    @FXML
    private ComboBox<String> cbCiudad;
    private HashMap<String, JSONObject> hashCiudad;
    @FXML
    private ComboBox<String> cbBarrio;
    private HashMap<String, JSONObject> hashBarrio;
    @FXML
    private TextField txtTelefono;
    @FXML
    private TextField txtCelular;
    @FXML
    private TextField txtMail;
    private TextField txtRuc;
    @FXML
    private TextField txtTimbrado;
    @FXML
    private TextField txtNroAcuerdo;
    @FXML
    private TextField txtSaldo;
    @FXML
    private TextArea txtContacto;
    @FXML
    private TextField txtUsuAlta;
    @FXML
    private TextField txtUsuMod;
    @FXML
    private TextField txtFechaAlta;
    @FXML
    private TextField txtFechaMod;
    @FXML
    private TextField txtSIDI;
    @FXML
    private Button btnActualizar;
    @FXML
    private Button btnCancelar;
    @FXML
    private TextField txtIdProveedor;
    private ComboBox<String> cbPersona;
    @FXML
    private TextField txtTelefono11;
    @FXML
    private TextField txtTelefono111;
    @FXML
    private TextField txtTelefono1111;
    @FXML
    private ComboBox<String> cbCondicion;
    @FXML
    private TextField txtTelefono1;
    @FXML
    private TextField txtTelefono2;
    @FXML
    private TextField txtCelular2;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtSaldo1;
    @FXML
    private TextArea txtContacto1;
    @FXML
    private TextField txtIdProveedor1;
    @FXML
    private TextField txtNumeroTimbrado;
    @FXML
    private DatePicker dpInicioVigencia;
    @FXML
    private DatePicker dpFinVigencia;
    @FXML
    private TableColumn<JSONObject, String> tableColumnFactura;
    @FXML
    private TableColumn<JSONObject, String> tableColumnEmpresa;
    @FXML
    private TableColumn<JSONObject, String> tableColumnFecha;
    @FXML
    private TableColumn<JSONObject, String> tableColumnMonto;
    @FXML
    private TextField txtNumFactura;
    @FXML
    private TextField txtEmpresa;
    @FXML
    private TextField txtObservacion;
    @FXML
    private ComboBox<String> cbTipo;
    @FXML
    private DatePicker dpFecha;
    @FXML
    private TextField txtMonto;

    IntegerValidator intValidator;
    private NumberValidator numValidator;
    private boolean patternMonto;
    String param;
    @FXML
    private Label labelTotal;
    @FXML
    private Label labelTotalGs;
    @FXML
    private Label lblTipo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnTipo;
    @FXML
    private Tab tabDetalleGasto;
    @FXML
    private TitledPane titledPaneCaja1;
    @FXML
    private AnchorPane anchorPaneCaja1;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigoDetalle;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcionDetalle;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidadDetalle;
    @FXML
    private TableColumn<JSONObject, String> tableColumnMontoDetalle;
    @FXML
    private Label labelTotal1;
    @FXML
    private Label labelTotalGsDetalle;
    @FXML
    private TextField txtCodProductoDetalle;
    @FXML
    private TextField txtDescripcionDetalle;
    @FXML
    private TextField txtPrecioDetalle;
    @FXML
    private TextField txtCantidadDetalle;
    @FXML
    private Button btnAgregarDetalle;
    @FXML
    private Pane panelDetalleGastos;
    @FXML
    private Pane panelPrecioVenta;
    @FXML
    private TextField txtPrecioMin;
    @FXML
    private TextField txtPorcIncremParana01;
    @FXML
    private Button btnConfirmarVenta;
    @FXML
    private ComboBox<String> cbSeccionData;
    @FXML
    private TableView<JSONObject> tableViewDetalle;
    @FXML
    private TextField txtIdGasto;
    @FXML
    private Button btnProveedorEstandar;
    @FXML
    private TextField txtRucProveedor;
    @FXML
    private Label labelFechaFin;
    @FXML
    private DatePicker datePickerFechaFin;
    @FXML
    private DatePicker datePickerFechaInicio;
    @FXML
    private Label labelFechaInicio;
    @FXML
    private Button btnListarGastos;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void anchorPaneKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha = false;
        hashPais = new HashMap<>();
        hashCiudad = new HashMap<>();
        hashBarrio = new HashMap<>();
        hashDpto = new HashMap<>();
        hashFamilia = new HashMap<>();

        dpFecha.setValue(LocalDate.now());
        cargarFamilia();

        datePickerFechaFin.setStyle("-fx-font-size: 20px;");
        datePickerFechaInicio.setStyle("-fx-font-size: 20px;");

        datePickerFechaInicio.setValue(firstDay);
        datePickerFechaFin.setValue(lastDay);
//        cargarPais();
//        listenerCampos();
//        listenerCamposCombos();
//        repeatFocus(txtBuscar);
        listenerCamposCombos();

//        cbPais.setValue("PARAGUAY");
//        cbPais.setDisable(true);
        ubicandoContenedorSecundario();
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField();
        listenTextFieldPrecioDetalle();
        listenTextFieldSoloNumero();
        listarporFecha();
        cargarSecciones();
        Image imgBuscar = new Image(getClass().getResourceAsStream("/vista/img/buscar.png"));
        btnProveedorEstandar.setGraphic(new ImageView(imgBuscar));
        btnListarGastos.setGraphic(new ImageView(imgBuscar));

//        limpiarNiveles(1);
//        JSONObject json = hashPais.get(cbPais.getSelectionModel().getSelectedItem());
//        cargarDepartamento(Long.parseLong(json.get("idPais").toString()));
    }

    public void cargarSecciones() {
        cbSeccionData.getItems().add("SELECCIONE SECCION");
        List<Seccion> listSeccion = seccionDAO.listarTodosTRUE();
        if (listSeccion != null) {
            for (Seccion obj : listSeccion) {
                //                    json = (JSONObject) parser.parse(obj.toString());listSeccion
                cbSeccionData.getItems().add(obj.getDescripcion().toUpperCase());
//                    hashSeccion.put(json.get("descripcion").toString(), json);
            }
        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void listenTextField() {
        txtMonto.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMonto.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMonto.setText(param);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText("");
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMonto.setText(param);
                                    txtMonto.positionCaret(txtMonto.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMonto.setText("");
                                txtMonto.positionCaret(txtMonto.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMonto.setText(oldValue);
                            txtMonto.positionCaret(txtMonto.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMonto.setText(param);
                        txtMonto.positionCaret(txtMonto.getLength());
                    });
                }
            }
        });
    }

    private void listenTextFieldPrecioDetalle() {
        txtPrecioDetalle.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtPrecioDetalle.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtPrecioDetalle.setText(param);
                            txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtPrecioDetalle.setText("");
                            txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtPrecioDetalle.setText(param);
                                    txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtPrecioDetalle.setText("");
                                txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtPrecioDetalle.setText(oldValue);
                            txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtPrecioDetalle.setText(param);
                        txtPrecioDetalle.positionCaret(txtPrecioDetalle.getLength());
                    });
                }
            }
        });
    }

    private void listenTextFieldSoloNumero() {
        txtCantidadDetalle.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtCantidadDetalle.textProperty().addListener((observable, oldValue, newValue) -> {
            if (oldValue.length() == 0) {
                patternMonto = true;
            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim > 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {

                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        Platform.runLater(() -> {
                            txtCantidadDetalle.setText("");
                            txtCantidadDetalle.positionCaret(txtCantidadDetalle.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtCantidadDetalle.setText(oldValue);
                            txtCantidadDetalle.positionCaret(txtCantidadDetalle.getLength());
                        });
                    }
                } else if (newV.substring((newV.length() - 1), newV.length()).equalsIgnoreCase(",")) {
                    Platform.runLater(() -> {
//                        txtCantidadDetalle.setText(param);
                        txtCantidadDetalle.getText().replace(",", ".");
                        txtCantidadDetalle.positionCaret(txtCantidadDetalle.getLength());
                    });
                }
            }
        });
    }

    private void listenerCampos() {
        cbFamilia.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                System.out.println("--->> " + cbFamilia.getSelectionModel().getSelectedItem());
//                cbSubFamilia.getItems().clear();
//                JSONObject json = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem());
//                cargarFlia2(Long.parseLong(json.get("idNf1Tipo").toString()));
            }
        });
    }

    @FXML
    private void buttonCerrarAction(ActionEvent event) {
        cerrandoSesion();
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
        limpiar();
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
            mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
        } else {
            guardarProveedor();
        }
    }

    private void cerrandoSesion() {
        if (panelPrecioVenta.isVisible()) {
            panelPrecioVenta.setVisible(false);
            panelDetalleGastos.setDisable(false);
        } else {
            this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        }
    }

    private void configuracionCuponera() {
//        if ("configuracion_descuento")) {//falta aún...
        Utilidades.setIdRangoLocal(999l);
        this.sc.loadScreen("/vista/cuponera/CuponeraConfigFXML.fxml", 726, 560, "/vista/login/menuFXML.fxml", 540, 359, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/login/menuFXML.fxml", 540, 359, true);
//    }
    }

//AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("ACEPTAR (ENTER)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
//        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
//            if (tabArticulos.isSelected()) {
//            } else if (tabCuponera.isSelected()) {
//                configuracionCuponera();
//            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (tableViewProveedor.isFocused()) {
                if (tableViewProveedor.getSelectionModel().getSelectedIndex() >= 0) {
                    ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿SEGURO QUE DESEA ELIMINAR LA SELECCION?", ok, cancel);
                    alert.showAndWait();
                    if (alert.getResult() == ok) {
                        eliminarGasto();
                        alert.close();
                        listarporFecha();
//                    mensajeAlerta("DATO ELIMINADO CORRECTAMENTE!!");
                    } else {
                        alert.close();
                    }
                }
            } else {
                if (tableViewDetalle.getSelectionModel().getSelectedIndex() >= 0) {
                    ButtonType ok = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
                    ButtonType cancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿SEGURO QUE DESEA ELIMINAR LA SELECCION?", ok, cancel);
                    alert.showAndWait();
                    if (alert.getResult() == ok) {
                        eliminarDetalle();
                        alert.close();
//                    mensajeAlerta("DATO ELIMINADO CORRECTAMENTE!!");
                    } else {
                        alert.close();
                    }
                }
            }
        }
        if (altN.match(event)) {
            limpiar();
        }
        if (keyCode == event.getCode().F5) {
            if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
                mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
            } else {
                if (!btnActualizar.isDisable()) {
                    actualizarProveedor();
                } else {
                    mensajeAlerta("BOTON DESHABILITADO");
                }
            }
        }
        if (keyCode == event.getCode().F2) {
            if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
                mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
            } else {
                if (!btnGuardar.isDisable()) {
                    guardarProveedor();
                } else {
                    mensajeAlerta("DEBES PRESIONAR EL BOTON NUEVO (ALT+N) PARA LUEGO COMPLETAR LOS CAMPOS");
                }
            }
        }
        if (keyCode == event.getCode().F8) {
            limpiar();
//            btnGuardar.setDisable(true);
//            btnActualizar.setDisable(false);
        }
        if (keyCode == event.getCode().F4) {
        }
        if (keyCode == event.getCode().F3) {
            if (panelDetalleGastos.isVisible()) {
                confirmarDetallePrecios();
            }
        }
        if (keyCode == event.getCode().F7) {
            if (panelDetalleGastos.isVisible()) {
                verificarTipo();
            }
        }

        if (keyCode == event.getCode().ENTER) {
            if (txtPrecioMin.isFocused()) {
                listenPrecioMin();
            } else if (txtPorcIncremParana01.isFocused()) {
                listenIncrementoMin(event);
            } else if (txtCodProductoDetalle.isFocused()) {
                busquedaArticulo();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (panelPrecioVenta.isVisible()) {
                panelPrecioVenta.setVisible(false);
                panelDetalleGastos.setDisable(false);
            } else {
                cerrandoSesion();
            }
        }
    }

    private void busquedaArticulo() {
        numValidator = new NumberValidator();
        if (txtCodProductoDetalle.getText().equalsIgnoreCase("")) {

        } else {
            Articulo art = artDAO.buscarCod(txtCodProductoDetalle.getText());
            if (art != null) {
                txtDescripcionDetalle.setText(art.getDescripcion());
                txtCodProductoDetalle.setText(art.getCodArticulo() + "");
//                txtPrecioDetalle.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(art.getCosto() + "")));
                txtPrecioDetalle.setText("");
                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(art.getPrecioMin() + "")));
                cbSeccionData.getSelectionModel().select(art.getSeccion().getDescripcion().toUpperCase());
                listenPrecioMin();
            } else {
                txtDescripcionDetalle.setText("");
                txtPrecioDetalle.setText("");
                txtPrecioMin.setText("");
                txtPorcIncremParana01.setText("");
                cbSeccionData.getSelectionModel().select(0);
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

//    private void btnCargarArticuloAction(ActionEvent event) {
//        if (tab.isSelected()) {
//        } else if (tabCuponera.isSelected()) {
//            configuracionCuponera();
//        }
//    }
    private void listenPrecioMin() {
//        if (event.getCode().isDigitKey()) {
//            txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!oldValue.equalsIgnoreCase(newValue)) {
        calcularPorcentaje(txtPrecioMin.getText());
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    }
//                });
//            });
//        }
    }

    private void listenIncrementoMin(KeyEvent event) {
//        if (event.getCode().isDigitKey()) {
//            txtPorcIncremParana01.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    if (!increMin) {
        calcularPrecioMin(txtPorcIncremParana01.getText());
//                        txtPorcIncremParana01.positionCaret(txtPorcIncremParana01.getLength());
//                    } else {
//                        increMin = false;
//                    }
//
//                });
//            });
//        }
    }

    private void calcularPrecioMin(String value) {
        numValidator = new NumberValidator();
        try {
            long c = (Long.parseLong(numValidator.numberValidator(value)) + 100) * Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
            long result = Math.round((c / 100));
            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(result + "")));
//            precMin = true;
        } catch (Exception e) {
            txtPrecioMin.setText("0");
        }
    }

    private void calcularPorcentaje(String value) {
        numValidator = new NumberValidator();
//        DecimalFormat df = new DecimalFormat("#.0");
        try {
            if (!value.equals("null")) {
                double result = (Long.parseLong(numValidator.numberValidator(value)) * 100) / Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
                double reult2 = result - 100;
                long res = Math.round(reult2);
                txtPorcIncremParana01.setText(res + "");
            }
        } catch (Exception e) {
            txtPorcIncremParana01.setText("0");
        } finally {
        }
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPane.getChildren()
                .get(anchorPane.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    private void buscarCliente() {
//        proveedorList = new ArrayList<>();
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
//        if (txtBuscar.getText().equalsIgnoreCase("")) {
//            mensajeAlerta("Debes ingresar una descripción para realizar la búsqueda");
//        } else if (!radioBtnNombreCta.isSelected() && !radioBtnCodigoCta.isSelected()) {
//            mensajeAlerta("Debes selecionar un rango de búsqueda");
//        } else {
//            String filtro = txtBuscar.getText();
////            String urlServer = "";
//            if (radioBtnNombreCta.isSelected()) {
////                urlServer = "null/" + filtro;
//                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc(filtro, "null");
//                if (listProveedor != null) {
//                    tableViewProveedor.getItems().clear();
//                    proveedorList = new ArrayList<>();
//                    for (Proveedor pro : listProveedor) {
//                        ProveedorDTO proDTO = pro.toProveedorDTO();
//                        proDTO.setFechaAlta(null);
//                        proDTO.setFechaMod(null);
//                        JSONObject json;
//                        try {
//                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
//                            proveedorList.add(json);
//                            actualizandoTabla();
//
//                        } catch (ParseException ex) {
//                            Logger.getLogger(GastosFXMLController.class
//                                    .getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                } else {
//                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
//                }
//            } else {
////                urlServer = filtro + "/null";
//                List<Proveedor> listProveedor = proveedorDAO.listarPorNomRuc("null", filtro);
//                if (listProveedor != null) {
//                    tableViewProveedor.getItems().clear();
//                    proveedorList = new ArrayList<>();
//                    for (Proveedor pro : listProveedor) {
//                        ProveedorDTO proDTO = pro.toProveedorDTO();
//                        proDTO.setFechaAlta(null);
//                        proDTO.setFechaMod(null);
//                        JSONObject json;
//                        try {
//                            json = (JSONObject) parser.parse(gson.toJson(proDTO));
//                            proveedorList.add(json);
//                            actualizandoTabla();
//
//                        } catch (ParseException ex) {
//                            Logger.getLogger(GastosFXMLController.class
//                                    .getName()).log(Level.SEVERE, null, ex);
//                        }
//                    }
//                } else {
//                    mensajeAlerta("NO SE ENCONTRAN RESULTADOS DE LA BUSQUEDA");
//                }
//            }
////            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {a
////                try {
////                    URL url = new URL(Utilidades.ip + "/ServerParana/proveedor/" + urlServer);
////                    URLConnection uc = url.openConnection();
////                    BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
////                    while ((inputLine = br.readLine()) != null) {
////                        valor = (JSONArray) parser.parse(inputLine);
////                    }
////                    br.close();
////                    if (!valor.isEmpty()) {
////                        proveedorList = new ArrayList<>();
////                        for (Object obj : valor) {
////                            JSONObject json = (JSONObject) parser.parse(obj.toString());
////                            proveedorList.add(json);
////                        }
////                        actualizandoTabla();
////                    } else {
////                        mensajeAlerta("NO SE ENCONTRARON DATOS");
////                    }
////                } catch (FileNotFoundException e) {
////                    System.out.println(e.getLocalizedMessage());
////                } catch (IOException e) {
////                    System.out.println(e.getLocalizedMessage());
////                } catch (org.json.simple.parser.ParseException e) {
////                    e.printStackTrace();
////                }
////            } else {
//                mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//            }

//        }
    }

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTabla() {
        NumberValidator numVal = new NumberValidator();
        clienteData = FXCollections.observableArrayList(proveedorList);
        //columna Nombre ..................................................
//        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnFactura.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String cod = String.valueOf(data.getValue().get("numFactura"));
                return new ReadOnlyStringWrapper(cod.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
//        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnEmpresa.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("empresa"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("tipo"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnFecha.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnFecha.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnFecha.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("fecha"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnMonto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("monto"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(numVal.numberValidator(nombre))));
            }
        });
        //columna Ruc .................................................
        tableViewProveedor.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }
    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************

    private void escucha() {
        JSONParser parser = new JSONParser();
        escucha = true;
        tableViewProveedor.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewProveedor.getSelectionModel().getSelectedItem() != null) {
//                mensajeAlerta("-> " + newSelection.get("codigo").toString().toUpperCase() + " ) " + newSelection.get("nombre").toString().toUpperCase());
                txtNumFactura.setText(newSelection.get("numFactura").toString());
                txtEmpresa.setText(newSelection.get("empresa").toString());
                txtObservacion.setText(newSelection.get("observacion").toString());
                txtIdProveedor.setText(newSelection.get("idGasto").toString().toUpperCase());
                txtMonto.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newSelection.get("monto").toString())));
                dpFecha.setValue(LocalDate.parse(newSelection.get("fecha").toString()));
//                int val = 0;
//                if (newSelection.get("tipo").toString().toUpperCase().equalsIgnoreCase("OTRO")) {
//                    val = 1;
//                }
                Proveedor provee = proveedorDAO.getById(Long.parseLong(newSelection.get("idProveedor").toString()));
                txtRucProveedor.setText(provee.getRuc());
                txtIdGasto.setText(newSelection.get("idGasto").toString().toUpperCase());
                cbTipo.getSelectionModel().select(newSelection.get("tipo").toString().toUpperCase());
                cargarDatos();
            }
        });
    }
//                try {
////                    pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
//                    txtNumeroTimbrado.setText(newSelection.get("timbrado").toString());
//                } catch (Exception ex) {
//                } finally {
//                }
//                try {
//                    String[] result = newSelection.get("inicioVigencia").toString().split(" ");
//                    java.sql.Date desde = java.sql.Date.valueOf(result[0]);
////                    pro.setInicioVigencia(desde);
//                    dpInicioVigencia.setValue(desde.toLocalDate());
//                } catch (Exception ex) {
//                    dpInicioVigencia.setValue(null);
//                } finally {
//                }
//                try {
//                    String[] result = newSelection.get("finVigencia").toString().split(" ");
//                    java.sql.Date hasta = java.sql.Date.valueOf(result[0]);
//                    dpFinVigencia.setValue(hasta.toLocalDate());
//                } catch (Exception ex) {
//                    dpFinVigencia.setValue(null);
//                } finally {
//                }
//
//                cbPersona.setValue("JURIDICA");
//                if (newSelection.containsKey("callePrincipal")) {
//                    txtDireccion.setText(newSelection.get("callePrincipal").toString());
//                }
//                if (newSelection.containsKey("telefono")) {
//                    txtTelefono.setText(newSelection.get("telefono").toString());
//                }
//                if (newSelection.containsKey("telefono2")) {
//                    txtCelular.setText(newSelection.get("telefono2").toString());
//                }
//                if (newSelection.containsKey("email")) {
//                    txtMail.setText(newSelection.get("email").toString());
//                }
//                if (newSelection.containsKey("timbrado")) {
//                    txtTimbrado.setText(newSelection.get("timbrado").toString());
//                }
//                if (newSelection.containsKey("usuAlta")) {
//                    txtUsuAlta.setText(newSelection.get("usuAlta").toString());
//                }
//                if (newSelection.containsKey("usuMod")) {
//                    txtUsuMod.setText(newSelection.get("usuMod").toString());
//                }
//                if (newSelection.containsKey("fechaAlta")) {
//                    txtFechaAlta.setText(newSelection.get("fechaAlta").toString());
//                }
//                if (newSelection.containsKey("fechaMod")) {
//                    txtFechaMod.setText(newSelection.get("fechaMod").toString());
//                }
//
////                txtVtoDia.setText(newSelection.get("condven").toString());
//                txtNroAcuerdo.setText("");
//                txtContacto.setText("");
////                txtSaldo.setText(newSelection.get("saldo").toString());
//                txtSIDI.setText("");
//
//                if (newSelection.containsKey("familiaProv")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("familiaProv").toString());
//                        cbFamilia.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("pais")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("pais").toString());
//                        cbPais.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("departamento")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("departamento").toString());
//                        cbDpto.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("ciudad")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("ciudad").toString());
//                        cbCiudad.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//                if (newSelection.containsKey("barrio")) {
//                    try {
//                        JSONObject objFlia = (JSONObject) parser.parse(newSelection.get("barrio").toString());
//                        cbBarrio.setValue(objFlia.get("descripcion").toString().toUpperCase());
//                    } catch (ParseException ex) {
//                        Logger.getLogger(GastosFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    }
//                }
//
//            } else {
//            }
//            });
//            }

    private void cargarFamilia() {
        cbTipo.getItems().add("COMPRAS P/ PREPARACION");
        cbTipo.getItems().add("COMPRAS P/ VENTA");
        cbTipo.getItems().add("INSUMOS");
        cbTipo.getItems().add("SERVICIOS PERSONALES");
        cbTipo.getItems().add("SERVICIOS NO PERSONALES");
        cbTipo.getItems().add("SERVICIOS FINANCIEROS"); //        cbCondicion.getItems().add("10");
        //        cbCondicion.getItems().add("15");
        //        cbCondicion.getItems().add("30");
        //        cbCondicion.getItems().add("45");
        //
        //        cbFamilia.getItems().add("COMERCIALES");
        //        cbFamilia.getItems().add("DISTRIBUIDORES");
        //
        //        JSONObject obj1 = new JSONObject();
        //        obj1.put("idFamiliaProv", 1);
        //        obj1.put("descripcion", "COMERCIALES");
        //
        //        JSONObject obj2 = new JSONObject();
        //        obj2.put("idFamiliaProv", 2);
        //        obj2.put("descripcion", "DISTRIBUIDORES");
        //
        //        hashFamilia.put("COMERCIALES", obj1);
        //        hashFamilia.put("DISTRIBUIDORES", obj2);
    }

    private void listenerCamposCombos() {
        cbTipo.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue ov, Object t, Object t1) {
                String dato = cbTipo.getSelectionModel().getSelectedItem();
                if (dato.equalsIgnoreCase("SERVICIOS PERSONALES")) {
                    lblTipo.setText("Salario, Servicio de limpieza y otros.");
                    deshabilitarDetalleGastos();
                } else if (dato.equalsIgnoreCase("SERVICIOS NO PERSONALES")) {
                    lblTipo.setText("Alquiler, Essap, Ande y otros.");
                    deshabilitarDetalleGastos();
                } else if (dato.equalsIgnoreCase("COMPRAS P/ VENTA")) {
                    lblTipo.setText("Compras para venta directa de mercadería.");
                    habilitarDetalleGastos();
                    habilitarDetalleGastosVentaDirecta();
                } else if (dato.equalsIgnoreCase("SERVICIOS FINANCIEROS")) {
                    lblTipo.setText("Contabilidad, Iva, comisión Bancaria y otros.");
                    deshabilitarDetalleGastos();
                } else if (dato.equalsIgnoreCase("INSUMOS")) {
                    lblTipo.setText("Compras para uso del local.");
                    deshabilitarDetalleGastos();
                } else if (dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
                    lblTipo.setText("Compras para preparación de productos para la venta.");
                    habilitarDetalleGastos();
                    habilitarDetalleGastosVentaPreparacion();
                }
            }
        });
    }

    public void deshabilitarDetalleGastos() {
        tabDetalleGasto.setDisable(true);
    }

    public void habilitarDetalleGastos() {
        tabDetalleGasto.setDisable(false);
    }

    public void habilitarDetalleGastosVentaDirecta() {
        txtCodProductoDetalle.setDisable(false);
    }

    public void habilitarDetalleGastosVentaPreparacion() {
        txtCodProductoDetalle.setDisable(true);
    }
//        cbDpto.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                limpiarNiveles(2);
//                JSONObject json = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem());
//                cargarCiudad(Long.parseLong(json.get("idDepartamento").toString()));
//            }
//        });
//        cbCiudad.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
//                limpiarNiveles(3);
//                JSONObject json = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem());
//                cargarBarrio(Long.parseLong(json.get("idCiudad").toString()));
//            }
//        });
//        cbBarrio.getSelectionModel().selectedItemProperty().addListener(new ChangeListener() {
//            @Override
//            public void changed(ObservableValue ov, Object t, Object t1) {
////                JSONObject json = hashSubCategoria.get(cbSubCategoria.getSelectionModel().getSelectedItem());
////                cargarFlia5(Long.parseLong(json.get("idNf4Seccion1").toString()));
//            }
//        });

    private void cargarPais() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Pais> listPais = paisDAO.listar();
        if (listPais != null) {
            for (Pais objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                PaisDTO paisDTO = objPais.toPaisDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbPais.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashPais.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(GastosFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
//            actualizandoTabla();
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE PAIS");
        }
//        if (Utilidades.pingHost(Utilidades.host, Utilidades.port, 2)) {
//            try {
//                URL url = new URL(Utilidades.ip + "/ServerParana/pais");
//                URLConnection uc = url.openConnection();
//                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    valor = (JSONArray) parser.parse(inputLine);
//                }
//                br.close();
//                if (!valor.isEmpty()) {
//                    for (Object obj : valor) {
//                        JSONObject json = (JSONObject) parser.parse(obj.toString());
//                        proveedorList.add(json);a
//
//                        cbPais.getItems().add(json.get("descripcion").toString());
//                        hashPais.put(json.get("descripcion").toString(), json);
//                    }
//                    actualizandoTabla();
//                } else {
//                    mensajeAlerta("NO SE ENCONTRARON DATOS");
//                }
//            } catch (FileNotFoundException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (IOException e) {
//                System.out.println(e.getLocalizedMessage());
//            } catch (org.json.simple.parser.ParseException e) {
//                e.printStackTrace();
//            }
//        } else {
//            mensajeAlerta("NO SE ESTABLECIO CONEXION CON EL SERVIDOR");
//        }
    }

    private void cargarCiudad(long idDpto) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Ciudad> listPais = ciudadDAO.listarPorCiudad(idDpto);
        if (listPais != null) {
            for (Ciudad objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                CiudadDTO paisDTO = objPais.toOnlyCiudadDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbCiudad.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashCiudad.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(GastosFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE CIUDAD");
        }
    }

    private void cargarDepartamento(long idPais) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Departamento> listPais = dptoDAO.listarPorPais(idPais);
        if (listPais != null) {
            for (Departamento objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                DepartamentoDTO paisDTO = objPais.toSinOtrosDatosDepartamentoDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbDpto.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashDpto.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(GastosFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void cargarBarrio(long idCiudad) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONArray valor = new JSONArray();
        List<Barrio> listPais = barrioDAO.listarPorCiudad(idCiudad);
        if (listPais != null) {
            for (Barrio objPais : listPais) {
//                JSONObject json = (JSONObject) parser.parse(obj.toString());
//                proveedorList.add(json);
                BarrioDTO paisDTO = objPais.toBarrioAClienteDTO();
                paisDTO.setFechaAlta(null);
                paisDTO.setFechaMod(null);
                try {
                    JSONObject json = (JSONObject) parser.parse(gson.toJson(paisDTO));
                    cbBarrio.getItems().add(objPais.getDescripcion().toUpperCase());
                    hashBarrio.put(objPais.getDescripcion().toUpperCase(), json);

                } catch (ParseException ex) {
                    Logger.getLogger(GastosFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                }

            }
        } else {
            mensajeAlerta("NO SE ENCONTRARON DATOS DE DEPARTAMENTO");
        }
    }

    private void limpiar() {
        txtNumFactura.setText("");
        cbTipo.getSelectionModel().select(0);
        lblTipo.setText("Compra para ventas, para uso del local y otros.");
        txtEmpresa.setText("");
        txtObservacion.setText("");
        txtMonto.setText("");
//        listarporFecha();
//        cbCondicion.getSelectionModel().select(0);
////        cbPais.getSelectionModel().select(0);
//        cbDpto.getSelectionModel().select(0);
//        cbCiudad.getSelectionModel().select(0);
//        cbBarrio.getSelectionModel().select(0);

//        txtCelular.setText("");
//        txtMail.setText("");
//        txtRuc.setText("");
//        txtTimbrado.setText("");
//        txtNroAcuerdo.setText("");
//        txtContacto.setText("");
//        txtSaldo.setText("");
//        txtNumeroTimbrado.setText("");
//        dpInicioVigencia.setValue(null);
//        dpFinVigencia.setValue(null);
//        repeatFocus(txtNumFactura);
        txtNumFactura.requestFocus();
//        btnGuardar.setDisable(false);
//        btnActualizar.setDisable(true);
        labelTotalGsDetalle.setText("Gs 0");
        txtIdGasto.setText("");
        detalleList = new ArrayList<>();
        tableViewDetalle.getItems().clear();
    }

    @FXML
    private void btnActualizarAction(ActionEvent event) {
        if (txtNumFactura.getText().equals("") || txtEmpresa.getText().equals("")) {
            mensajeAlerta("EL CAMPO EMPRESA Y NUMERO DE FACTURA NO DEBE QUEDAR VACIO");
        } else {
            actualizarProveedor();
        }
    }

    @FXML
    private void btnCancelarAction(ActionEvent event) {
        limpiar();
//        btnGuardar.setDisable(true);
//        btnActualizar.setDisable(false);
    }

    private void guardarProveedor() {
        if (cbTipo.getSelectionModel().getSelectedIndex() >= 0 || !txtObservacion.getText().equals("") || !txtMonto.getText().equals("")) {
            Timestamp timestamp = new Timestamp(date.getTime());
            Gastos pro = new Gastos();
            numValidator = new NumberValidator();
            pro.setEmpresa(txtEmpresa.getText());
            pro.setDescripcion(txtObservacion.getText());
            pro.setFactura(txtNumFactura.getText());
            pro.setTipo(cbTipo.getSelectionModel().getSelectedItem());
            pro.setMonto(Integer.parseInt(numValidator.numberValidator(txtMonto.getText())));
            java.sql.Date date = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            pro.setFecha(date);
            if (!txtRucProveedor.getText().equals("")) {
                Proveedor p1 = proveedorDAO.listarPorRuc(txtRucProveedor.getText());
                pro.setProveedor(p1);
            }
//        if (txtNumeroTimbrado.getText().equals("") || txtNumeroTimbrado.getText().equals("0")) {
//            pro.setTimbrado(0);
//        } else {
//            pro.setTimbrado(Integer.parseInt(txtNumeroTimbrado.getText()));
//        }
//        try {
//            java.sql.Date desde = java.sql.Date.valueOf(dpInicioVigencia.getValue());
//            pro.setInicioVigencia(desde);
//        } catch (Exception ex) {
//        } finally {
//        }
//        try {
//            java.sql.Date hasta = java.sql.Date.valueOf(dpFinVigencia.getValue());
//            pro.setFinVigencia(hasta);
//        } catch (Exception ex) {
//        } finally {
//        }
////        pro.setNroLocal(Integer.parseInt(txtNroAcuerdo.getText()));
//
//        if (!cbFamilia.getValue().toString().equals("")) {
//            JSONObject jsonFLIA = hashFamilia.get(cbFamilia.getSelectionModel().getSelectedItem().toUpperCase());
//            FamiliaProv flia = new FamiliaProv();
//            flia.setIdFamiliaProv(Long.parseLong(jsonFLIA.get("idFamiliaProv").toString()));
//            pro.setFamiliaProv(flia);
//        }
//        if (!cbPais.getValue().toString().equals("")) {
//            JSONObject jsonPAIS = hashPais.get(cbPais.getSelectionModel().getSelectedItem().toUpperCase());
//            Pais pais = new Pais();
//            pais.setIdPais(Long.parseLong(jsonPAIS.get("idPais").toString()));
//            pro.setPais(pais);
//        }
//        try {
//            if (!cbDpto.getValue().toString().equals("")) {
//                JSONObject jsonDPTO = hashDpto.get(cbDpto.getSelectionModel().getSelectedItem().toUpperCase());
//                Departamento pais = new Departamento();
//                pais.setIdDepartamento(Long.parseLong(jsonDPTO.get("idDepartamento").toString()));
//                pro.setDepartamento(pais);
//            }
//        } catch (Exception e) {
//            Departamento pais = new Departamento();
//            pais.setIdDepartamento(0L);
//            pro.setDepartamento(pais);
//        } finally {
//        }
//        try {
//            if (!cbCiudad.getValue().toString().equals("")) {
//                JSONObject jsonCIU = hashCiudad.get(cbCiudad.getSelectionModel().getSelectedItem().toUpperCase());
//                Ciudad pais = new Ciudad();
//                pais.setIdCiudad(Long.parseLong(jsonCIU.get("idCiudad").toString()));
//                pro.setCiudad(pais);
//            }
//        } catch (Exception e) {
//            Ciudad pais = new Ciudad();
//            pais.setIdCiudad(0l);
//            pro.setCiudad(pais);
//        } finally {
//        }
//        try {
//            if (!cbBarrio.getValue().toString().equals("")) {
//                JSONObject jsonBARRIO = hashBarrio.get(cbBarrio.getSelectionModel().getSelectedItem().toUpperCase());
//                Barrio pais = new Barrio();
//                pais.setIdBarrio(Long.parseLong(jsonBARRIO.get("idBarrio").toString()));
//                pro.setBarrio(pais);
//            }
//        } catch (Exception e) {
//            Barrio pais = new Barrio();
//            pais.setIdBarrio(0l);
//            pro.setBarrio(pais);
//        } finally {
//        }
            if (gastosDAO.insertarEstado(pro)) {
//            registrarProveedorSincro(pro);
                pro = gastosDAO.recuperarUltimo();
                txtIdGasto.setText(pro.getIdGasto() + "");
                mensajeAlerta("DATOS REGISTRADOS CORRECTAMENTE");
                tableViewProveedor.getSelectionModel().clearSelection();
                String dato = cbTipo.getSelectionModel().getSelectedItem();

                if (dato.equalsIgnoreCase("COMPRAS P/ VENTA") || dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
                } else {
                    limpiar();
                }
                listarporFecha();
//                btnGuardar.setDisable(true);
//                btnActualizar.setDisable(false);
            } else {
                mensajeAlerta("LOS DATOS NO HAN SIDO REGISTRADOS VERIFIQUELOS.");
            }
        } else {
            mensajeAlerta("SE DEBEN AGREGAR TIPO DE GASTOS, OBSERVACION Y MONTO.");
        }
    }

    private void actualizarProveedor() {
        try {
            Timestamp timestamp = new Timestamp(date.getTime());
            Gastos pro = new Gastos();
            pro.setIdGasto(Long.parseLong(txtIdGasto.getText()));
            pro.setEmpresa(txtEmpresa.getText());
            pro.setDescripcion(txtObservacion.getText());
//        pro.setDescripcion(txtNombre.getText());
            pro.setFactura(txtNumFactura.getText());
            pro.setMonto(Integer.parseInt(numValidator.numberValidator(txtMonto.getText())));
            pro.setTipo(cbTipo.getSelectionModel().getSelectedItem());
            if (!txtRucProveedor.getText().equals("")) {
                Proveedor p1 = proveedorDAO.listarPorRuc(txtRucProveedor.getText());
                pro.setProveedor(p1);
            }
//        a
//        java.sql.Date date = new java.sql.Date(dpFecha.getValue());
            pro.setFecha(java.sql.Date.valueOf(dpFecha.getValue()));

            if (gastosDAO.actualizarEstado(pro)) {
//            actualizarProveedorSincro(pro);
                mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE");
//            tableViewProveedor.getSelectionModel().clearSelection();
                String dato = cbTipo.getSelectionModel().getSelectedItem();
                if (dato.equalsIgnoreCase("COMPRAS P/ VENTA") || dato.equalsIgnoreCase("COMPRAS P/ PREPARACION")) {
                } else {
                    limpiar();
                }
                listarporFecha();
//            btnGuardar.setDisable(true);
//            btnActualizar.setDisable(false);
            } else {
                mensajeAlerta("LOS DATOS NO HAN SIDO ACTUALIZADOS VERIFIQUELOS");
            }
        } catch (Exception e) {
            mensajeAlerta("EL GASTO NO HA SIDO REGISTRADO, POR ESE MOTIVO NO SE PUEDE ACTUALIZAR ESTA INFORMACION");
        } finally {
        }
    }

    private void limpiarNiveles(int val) {
        switch (val) {
            case 1:
//                cbPais.getItems().clear();
                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbDpto.setValue("");
                cbCiudad.setValue("");
                cbBarrio.setValue("");
                break;
            case 2:
//                cbDpto.getItems().clear();
                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbCiudad.setValue("");
                cbBarrio.setValue("");

                break;
            case 3:
//                cbCiudad.getItems().clear();
                cbBarrio.getItems().clear();

                cbBarrio.setValue("");
                break;
            default:
                break;
        }
    }

    private void registrarProveedorSincro(Proveedor arti) {
        Timestamp timestamp = new Timestamp(date.getTime());
        String query = "INSERT INTO cuenta.proveedor(id_familia_prov, id_pais, id_departamento, id_ciudad, "
                + "id_barrio, calle_principal, ruc,"
                + "telefono, telefono2, timbrado, usu_alta, fecha_alta, usu_mod, fecha_mod, "
                + "saldo, cond_pago, descripcion, fin_vigencia, inicio_vigencia) "
                + "VALUES (1, 100, " + arti.getDepartamento().getIdDepartamento() + ", " + arti.getCiudad().getIdCiudad() + ", "
                + arti.getBarrio().getIdBarrio() + ",\"" + arti.getCallePrincipal() + "\", \"" + arti.getRuc() + "\", \""
                + arti.getTelefono() + "\", \"" + arti.getTelefono2() + "\", \"" + arti.getTimbrado() + "\", "
                + "\"" + Identity.getNomFun() + "\",\"" + timestamp + "\",\"" + Identity.getNomFun() + "\",\"" + timestamp + "\", "
                + arti.getSaldo() + ", \"" + arti.getCondPago() + "\", \"" + arti.getDescripcion() + "\", \"" + arti.getFinVigencia() + "\", \"" + arti.getInicioVigencia()
                + "\")";
        ConexionPostgres.conectarServer();
        for (IpCaja ip : ipCajaDAO.listar()) {
            String sql = "INSERT INTO sincro.proveedores(operacion, ip, fecha, descripcion_dato) VALUES ('I', '" + ip.getIpCaja() + "', 'now()', '" + query + "')";
            System.out.println("-->> " + sql);
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* ALMACENANDO DATOS ARTICULOS SINCRO ********");
                }
                ps.close();
                ConexionPostgres.getConServer().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getConServer().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private void actualizarProveedorSincro(Proveedor arti) {
        Timestamp timestamp = new Timestamp(date.getTime());
        String sql = "UPDATE cuenta.proveedor SET id_familia_prov=1, id_pais=100, id_departamento=" + arti.getDepartamento().getIdDepartamento() + ", "
                + "id_ciudad=" + arti.getCiudad().getIdCiudad() + ", id_barrio=" + arti.getBarrio().getIdBarrio() + ", calle_principal=\"" + arti.getCallePrincipal() + "\", "
                + "telefono=\"" + arti.getTelefono() + "\", telefono2=\"" + arti.getTelefono2() + "\", timbrado=\"" + arti.getTimbrado() + "\", usu_mod=\"" + Identity.getNomFun() + "\", fecha_mod=\"" + timestamp + "\", "
                + "saldo=" + arti.getSaldo() + ", cond_pago=\"" + arti.getCondPago() + "\", descripcion=\"" + arti.getDescripcion() + "\", fin_vigencia=\"" + arti.getFinVigencia() + "\", inicio_vigencia=\"" + arti.getInicioVigencia() + "\" "
                + "WHERE ruc=\"" + arti.getRuc() + "\"";
//        String sql = "INSERT INTO cuenta.proveedor(id_familia_prov, id_pais, id_departamento, id_ciudad, "
//                + "id_barrio, calle_principal, ruc,"
//                + "telefono, telefono2, timbrado, email, "
//                + "saldo, cond_pago, descripcion, fin_vigencia, inicio_vigencia) "
//                + "VALUES (1, 100, " + arti.getDepartamento().getIdDepartamento() + ", " + arti.getCiudad().getIdCiudad() + ", "
//                + arti.getBarrio().getIdBarrio() + ",'" + arti.getCallePrincipal() + "', '" + arti.getRuc() + "', '"
//                + arti.getTelefono() + "', '" + arti.getTelefono2() + "', '" + arti.getTimbrado() + "', '" + arti.getEmail() + "', '"
//                + arti.getSaldo() + "', '" + arti.getCondPago() + "', '" + arti.getDescripcion() + "', '" + arti.getFinVigencia() + "', '" + arti.getInicioVigencia()
//                + "')";
        sql = sql.replaceAll("\"\"", "null");
        ConexionPostgres.conectarServer();
        for (IpCaja ip : ipCajaDAO.listar()) {
            String query = "INSERT INTO sincro.proveedores(operacion, ip, fecha, descripcion_dato) VALUES ('U', '" + ip.getIpCaja() + "', 'now()', '" + sql + "')";
            System.out.println("-->> " + query);
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(query)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* ALMACENANDO DATOS ARTICULOS SINCRO ********");
                }
                ps.close();
                ConexionPostgres.getConServer().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getConServer().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
                }
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private void listarporFecha() {
        List<Gastos> listGastos = gastosDAO.listarPorFechas(datePickerFechaInicio.getValue(), datePickerFechaFin.getValue());
        proveedorList = new ArrayList<>();
        tableViewProveedor.getItems().clear();
        numValidator = new NumberValidator();
        int monto = 0;
        if (!listGastos.isEmpty()) {
            for (Gastos listGasto : listGastos) {
                JSONObject json = new JSONObject();
                json.put("numFactura", listGasto.getFactura());
                json.put("empresa", listGasto.getEmpresa());
                json.put("observacion", listGasto.getDescripcion());
                json.put("fecha", listGasto.getFecha() + "");
                json.put("monto", listGasto.getMonto() + "");
                json.put("idGasto", listGasto.getIdGasto());
                json.put("tipo", listGasto.getTipo());
                try {
                    json.put("idProveedor", listGasto.getProveedor().getIdProveedor());
                } catch (Exception e) {
                    json.put("idProveedor", 0);
                } finally {
                }

                monto += listGasto.getMonto();

                proveedorList.add(json);
            }
            labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(monto + "")));
            actualizandoTabla();
        }
    }

    @FXML
    private void btnAgregarDetalleAction(ActionEvent event) {
        verificarTipo();
    }

    @FXML
    private void txtCodProductoDetalleReleased(KeyEvent event) {
//        KeyCode keyCode = event.getCode();
//        if (keyCode == event.getCode().ENTER) {
//            if (txtCodProductoDetalle.isFocused()) {
//                busquedaArticulo();
//            }
//        }
        keyPress(event);
    }

    @FXML
    private void txtPrecioMinKeyPress(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtPorcIncremParana01.setText("0");
            listenTextFieldMin();
        }
        keyPress(event);
    }

    private void listenTextFieldMin() {
//        txtPrecioMin.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
//        txtPrecioMin.textProperty().addListener((observable, oldValue, newValue) -> {
//        if (oldValue.length() == 0) {
//            patternMonto = true;
//        }
//        System.out.println("MIN: " + newValue);
//        String newV = "";
//        String oldV = "";
//        if (!newValue.contentEquals("")) {
//            newV = numValidator.numberValidator(newValue);
//            oldV = numValidator.numberValidator(oldValue);
//            long lim = -1l;
//            boolean limite = true;
//            if (!newV.contentEquals("")) {//límite del monto...
//                if (newV.length() < 19) {
//                    lim = Long.valueOf(newV);
//                    if (lim > 0 && lim < 2147483647) {
//                        limite = false;
//                    }
//                }
//            }
//            if (limite) {
//                if (!newValue.contentEquals("") && !newV.contentEquals("")) {
//                    if (patternMonto) {
//                        if (oldV.length() != 0) {
//                            param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                        } else {
//                            param = oldValue;
//                        }
//                        patternMonto = false;
//                    } else {
//                        patternMonto = true;
//                        param = oldValue;
//                    }
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(param);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText("");
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
//                if (!oldValue.contentEquals("")) {
//                    if (!newValue.contentEquals("")) {
//                        if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                            if (patternMonto) {
//                                param = numValidator.numberFormat("###,###.###", Double.parseDouble(oldV));
//                                patternMonto = false;
//                            } else {
//                                patternMonto = true;
//                                param = oldValue;
//                            }
//                            Platform.runLater(() -> {
//                                txtPrecioMin.setText(param);
//                                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                            });
//                        }
//                    } else {
//                        Platform.runLater(() -> {
//                            txtPrecioMin.setText("");
//                            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                        });
//                    }
//                } else {
//                    Platform.runLater(() -> {
//                        txtPrecioMin.setText(oldValue);
//                        txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                    });
//                }
//            } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
//                if (patternMonto) {
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(newV));
//                    patternMonto = false;
//                } else {
//                    patternMonto = true;
//                    param = numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
//                }
//                Platform.runLater(() -> {
//                    txtPrecioMin.setText(param);
//                    txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//                });
//            }
//        }
//        try {
//            numValidator = new NumberValidator();
//            String monto = numValidator.numberValidator(txtPrecioMin.getText());
//            txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
//            txtPrecioMin.positionCaret(txtPrecioMin.getLength());
//        } catch (Exception e) {
//        } finally {
//        }

        Platform.runLater(() -> {
            try {
                numValidator = new NumberValidator();
                String monto = numValidator.numberValidator(txtPrecioMin.getText());
                txtPrecioMin.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(monto))));
                txtPrecioMin.positionCaret(txtPrecioMin.getLength());
            } catch (Exception e) {
            } finally {
            }
        });

//        });
    }

    @FXML
    private void txtPorcIncremParana01KeyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        System.out.println("->> " + keyCode + " " + event.getCode().F5 + " " + event.getCode().F8);
        if (keyCode != event.getCode().F5 || keyCode != event.getCode().F2) {
            txtPrecioMin.setText("0");
        }
        keyPress(event);
    }

    @FXML
    private void btnConfirmarVentaAction(ActionEvent event) {
        confirmarDetallePrecios();
    }

    private void verificarTipo() {
        if (txtIdGasto.getText().equals("")) {
            mensajeAlerta("DEBES SELECCIONAR UN GASTOS O CREAR UNO NUEVO PARA PODER AGREGAR DETALLES");
        } else {
            if (!txtCodProductoDetalle.isDisabled()) {
                if (txtCodProductoDetalle.getText().equals("") || txtDescripcionDetalle.getText().equals("") || txtPrecioDetalle.getText().equals("") || txtCantidadDetalle.getText().equals("")) {
                    mensajeAlerta("LOS CAMPOS CODIGO, DESCRIPCION, PRECIO Y CANTIDAD NO DEBEN QUEDAR VACIOS");
                } else {
//                cbSeccionData.getSelectionModel().select(0);
                    panelPrecioVenta.setVisible(true);
                    panelDetalleGastos.setDisable(true);
//                    cbSeccionData.getSelectionModel().select(0);
                }
            } else {
                if (txtIdGasto.getText().equals("")) {
                    mensajeAlerta("DEBES SELECCIONAR UN GASTO O REGISTRAR UNO NUEVO PARA CARGAR DETALLES");
                } else {
                    DetalleGasto dg = new DetalleGasto();
                    Gastos gas = new Gastos();
                    gas.setIdGasto(Long.parseLong(txtIdGasto.getText()));

                    BigDecimal bigDecimal = new BigDecimal(txtCantidadDetalle.getText().replace(",", "."));
                    dg.setCantidad(bigDecimal);
                    dg.setCantCompra(bigDecimal);
                    dg.setCodigo(txtCodProductoDetalle.getText());
                    dg.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
                    dg.setCostoCompra(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
                    dg.setDescripcion(txtDescripcionDetalle.getText());
                    dg.setGastos(gas);
                    detalleGastoDAO.insertar(dg);
                    mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE.");
                    cargarDatos();
                    limpiarDatos();
                }
            }
        }
    }

    private void confirmarDetallePrecios() {
        numValidator = new NumberValidator();
        if (txtIdGasto.getText().equals("")) {
            mensajeAlerta("DEBES SELECCIONAR UN GASTOS O CREAR UNO NUEVO PARA PODER AGREGAR DETALLES");
        } else {
            Date date = new Date();
            Timestamp timestamp = new Timestamp(date.getTime());
            DetalleGasto dg = new DetalleGasto();
            Gastos gas = new Gastos();
            gas.setIdGasto(Long.parseLong(txtIdGasto.getText()));

            BigDecimal bigDecimal = new BigDecimal(txtCantidadDetalle.getText().replace(",", "."));
            dg.setCantidad(bigDecimal);
            dg.setCantCompra(bigDecimal);
            dg.setCodigo(txtCodProductoDetalle.getText());
            dg.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
            dg.setCostoCompra(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
            dg.setDescripcion(txtDescripcionDetalle.getText());
            dg.setGastos(gas);

            if (txtPrecioMin.getText().equalsIgnoreCase("") || txtPrecioMin.getText().equalsIgnoreCase("0") || cbSeccionData.getSelectionModel().getSelectedItem().equalsIgnoreCase("SELECCIONE SECCION")) {
                mensajeAlerta("LOS CAMPOS NO DEBEN QUEDAR VACIOS NI SER IGUAL A CERO");
            } else {

                detalleGastoDAO.insertar(dg);
                mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE.");
                cargarDatos();

                panelPrecioVenta.setVisible(false);
                panelDetalleGastos.setDisable(false);
                Articulo art = artDAO.buscarCod(txtCodProductoDetalle.getText());
                if (art != null) {
                    long stockActual = Long.parseLong(art.getStockActual());
                    long result = stockActual + Long.parseLong(txtCantidadDetalle.getText());
                    art.setStockActual(result + "");
                    art.setDescripcion(txtDescripcionDetalle.getText());
                    art.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
                    long cociente = Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())) / bigDecimal.longValue();
                    art.setCosto(cociente);

                    artDAO.actualizar(art);
                } else {
                    Articulo arti = new Articulo();
                    arti.setFechaAlta(timestamp);
                    arti.setFechaMod(timestamp);
                    arti.setCodArticulo(txtCodProductoDetalle.getText());
                    arti.setDescripcion(txtDescripcionDetalle.getText());
                    arti.setPrecioMin(Long.parseLong(numValidator.numberValidator(txtPrecioMin.getText())));
                    arti.setPrecioMay(0L);
//                    long cociente = Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())) / bigDecimal.longValue();
//                    System.out.println("ACA-> " + Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
                    arti.setCosto(Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText())));
                    arti.setUrlImagen(null);
                    arti.setDescMaxMay(null);
                    arti.setDescMaxMin(null);
//                if (txtDescProveedor.getText().equals("")) {
//                    arti.setDescProveedor(Long.parseLong("0"));
//                } else {
//                    arti.setDescProveedor(Long.parseLong(txtDescProveedor.getText()));
                    arti.setDescProveedor(null);
//                }
                    arti.setDescMaxMay(null);
                    if (cbSeccionData.getSelectionModel().getSelectedIndex() >= 0) {
                        Seccion sec = seccionDAO.listarPorNombre(cbSeccionData.getSelectionModel().getSelectedItem().toUpperCase());
                        arti.setSeccion(sec);
                    }

                    arti.setServicio(false);
                    arti.setStockeable(false);
                    arti.setStockActual(txtCantidadDetalle.getText());
                    arti.setStockMax("1");
                    arti.setStockMin("1");
                    arti.setServicio(true);
                    arti.setStockeable(false);

                    Unidad unidad = new Unidad();
                    unidad.setIdUnidad(1L);
                    arti.setUnidad(unidad);

                    Iva iva = new Iva();
                    iva.setIdIva(3l);
                    arti.setIva(iva);

                    arti = artDAO.insertarRecuperarDato(arti);
                }
                long val = Long.parseLong(numValidator.numberValidator(labelTotalGsDetalle.getText()));
                long val2 = Long.parseLong(numValidator.numberValidator(txtPrecioDetalle.getText()));
                long result = val + val2;
//                labelTotalGsDetalle.setText(numValidator.numberFormat("###,###.###", Double.parseDouble(numValidator.numberValidator(result + ""))));
                limpiarDatos();
            }
        }
    }

    @FXML
    private void cbFamiliaMouseClicked(MouseEvent event) {
    }

    @FXML
    private void cbFamiliaMouseKeyReleased(KeyEvent event) {
    }

    private void limpiarDatos() {
        txtCodProductoDetalle.setText("");
        txtDescripcionDetalle.setText("");
        txtPrecioDetalle.setText("");
        txtCantidadDetalle.setText("");
        cbSeccionData.getSelectionModel().select(0);
        txtPrecioMin.setText("");
        txtPorcIncremParana01.setText("");
    }

    public void cargarDatos() {
        List<DetalleGasto> listGastos = detalleGastoDAO.listarPorFactura(Long.parseLong(txtIdGasto.getText()));
        detalleList = new ArrayList<>();
        numValidator = new NumberValidator();
        BigDecimal monto = BigDecimal.ZERO;
        for (DetalleGasto listGasto : listGastos) {
            JSONObject json = new JSONObject();
            json.put("codigo", listGasto.getCodigo());
            json.put("descripcion", listGasto.getDescripcion());
            BigDecimal dif = new BigDecimal(BigInteger.ONE);
            if (listGasto.getCantCompra().compareTo(listGasto.getCantidad()) != 0) {
                dif = listGasto.getCantCompra().subtract(listGasto.getCantidad());
            } else {
                dif = listGasto.getCantCompra();
            }
            json.put("cantidad", dif);
            json.put("costo", listGasto.getCosto());
            json.put("idDetalleGasto", listGasto.getIdDetalleGasto());
//            json.put("monto", listGasto.get);
//            json.put("idGasto", listGasto.getIdGasto());
//            json.put("tipo", listGasto.getTipo());
            BigDecimal montoData = BigDecimal.valueOf(listGasto.getCosto()).multiply(dif);
            monto = monto.add(montoData);

            detalleList.add(json);
        }
        labelTotalGsDetalle.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(monto + "")));
        actualizandoTablaDetalle();
    }

    private void actualizandoTablaDetalle() {
        NumberValidator numVal = new NumberValidator();
        detalleData = FXCollections.observableArrayList(detalleList);
        //columna Nombre ..................................................
//        tableColumnFactura.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigoDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnCodigoDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String cod = String.valueOf(data.getValue().get("codigo"));
                return new ReadOnlyStringWrapper(cod.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
//        tableColumnEmpresa.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcionDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnDescripcionDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("descripcion"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantidadDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnCantidadDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("cantidad"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(nombre.toUpperCase());
            }
        });
//        tableColumnMonto.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnMontoDetalle.setStyle("-fx-alignment: CENTER-LEFT;");
        tableColumnMontoDetalle.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String nombre = String.valueOf(data.getValue().get("costo"));
                if (nombre.contentEquals("null")) {
                    nombre = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(numVal.numberValidator(nombre))));
            }
        });
        //columna Ruc .................................................
        tableViewDetalle.setItems(detalleData);
//        if (!escucha) {
//            escucha();
//        }
    }

    @FXML
    private void btnProveedorEstandarAction(ActionEvent event) {
        BuscarProveedorFXMLController.cargarRucRazon(txtRucProveedor, txtEmpresa);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/gastosFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    @FXML
    private void btnListarGastosAction(ActionEvent event) {
        listarporFecha();
    }

    private void eliminarDetalle() {
//        json.put("fecha", g.getFecha().toString());
//        json.put("idDetalleGasto", dg.getIdDetalleGasto());
//        json.put("descripcion", dg.getDescripcion());
//        json.put("cantidad", bd.toString());
//        json.put("costo", numValidator.numberValidator(txtMonto.getText()));
        numValidator = new NumberValidator();
        int num = tableViewDetalle.getSelectionModel().getSelectedIndex();
        JSONObject json = detalleList.get(num);
//        DetalleGasto dg = detalleGastoDAO.getById(Long.parseLong(json.get("idDetalleGasto").toString()));

        if (detalleGastoDAO.eliminarObtenerEstado(Long.parseLong(json.get("idDetalleGasto").toString()))) {
            try {
                Articulo art = artDAO.buscarCod(json.get("codigo").toString());
                BigDecimal artStockAtual = new BigDecimal(art.getStockActual());
                BigDecimal artCantidad = new BigDecimal(json.get("cantidad").toString());

                BigDecimal result = artStockAtual.subtract(artCantidad);
                art.setStockActual(result.longValue() + "");
                artDAO.actualizar(art);
            } catch (Exception e) {
            } finally {
            }
            long resultado = Long.parseLong(numValidator.numberValidator(labelTotalGs.getText())) - Long.parseLong(numValidator.numberValidator(json.get("costo").toString()));
            labelTotalGs.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(resultado + "")));

            detalleList.remove(num);
            tableViewDetalle.getItems().remove(num);
        } else {
            mensajeAlerta("EL DATO SELECCIONADO NO HA SIDO ELIMINADO, VERIFIQUE QUE NO SE HAYA UTILIZADO EN PREPARACION DE PRODUCTOS.");
        }

    }

    private void eliminarGasto() {
        ConexionPostgres.conectarServer();
        boolean val = eliminarDetalleGatos();
        if (val) {
            String sql = "DELETE FROM factura_cliente.gatos WHERE id_gasto=" + txtIdGasto.getText();
            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
                int op = ps.executeUpdate();
//                if (op >= 1) {
                System.out.println("******* DATOS ELIMINADOS DE GASTOS ********");
//                }
                ps.close();
                ConexionPostgres.getConServer().commit();
            } catch (SQLException ex) {
                Utilidades.log.info(ex.getLocalizedMessage());
                try {
                    ConexionPostgres.getConServer().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.info(ex1.getLocalizedMessage());
                }
            }
        }
        ConexionPostgres.cerrarServer();
    }

    private boolean eliminarDetalleGatos() {
        actualizarCantidad();
        boolean val = false;
        String sql = "DELETE FROM factura_cliente.detalle_gasto WHERE id_gasto=" + txtIdGasto.getText();
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
            int op = ps.executeUpdate();
//            if (op >= 1) {
//                System.out.println("******* DATOS ELIMINADO DATOS DETALLE_GATOS ********");
            val = true;
//            }
            ps.close();
            ConexionPostgres.getConServer().commit();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
            try {
                ConexionPostgres.getConServer().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.info(ex1.getLocalizedMessage());
            }
        }
        return val;
    }

    private void actualizarCantidad() {
        String sql = "SELECT * FROM factura_cliente.detalle_gasto WHERE id_gasto=" + txtIdGasto.getText();
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//            int op = ps.executeUpdate();
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("codigo").equalsIgnoreCase("") || rs.getString("codigo").equalsIgnoreCase("''")) {
                } else {
                    try {
                        Articulo art = artDAO.buscarCod(rs.getString("codigo"));
                        BigDecimal artStockAtual = new BigDecimal(art.getStockActual());
                        BigDecimal artCantidad = (rs.getBigDecimal("cantidad"));

                        BigDecimal result = artStockAtual.subtract(artCantidad);
                        art.setStockActual(result.longValue() + "");
                        artDAO.actualizar(art);
                        detalleGastoDAO.eliminarObtenerEstado(rs.getLong("id_detalle_gasto"));
                    } catch (Exception e) {
                    } finally {
                    }
                }
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
    }
}
