/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.MainApp;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.core.domain.PedidoDetConteo;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.util.Callback;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoCompraDAO;
import com.peluqueria.dao.PedidoDetConteoDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.ProveedorDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RecepcionDAO;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.stock.PedidoCompraFXMLController.orden;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.PATH;
import com.javafx.util.StageSecond;
import java.awt.AWTException;
import java.awt.Robot;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.TableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import org.json.simple.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ConteoFXMLController extends BaseScreenController implements Initializable {

    static void cargarRucRazon(TextField txtProveedorDoc, TextField txtProveedor) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
    }

    static void cargarDatos(TextField txtProveedorDoc, TextField txtProveedor, TextField txtTimbrados) {
        txtRucCli = txtProveedorDoc;
        txtRazonCli = txtProveedor;
        txtTimbrado = txtTimbrados;
    }
    Image image;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    private boolean escucha;
    private boolean exitoCrear;
    private boolean exitoEditar;
    private static TextField txtRucCli;
    private static TextField txtRazonCli;
    private static TextField txtTimbrado;
    private static boolean clienteSi;
    public static String proveedor;
    private long idCliente;
    private int codCliente;
    private NumberValidator numVal;
    private ObservableList<JSONObject> clienteData;
    public static long idPedidoCab;
    public static String combo = "";
//    private List<JSONObject> clienteList;
    boolean valorEstado = false;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();

    public static Map mapeoCheck = new HashMap();
    boolean estadoCheck = false;

    final KeyCombination altG = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN);
    final KeyCombination altEnter = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.ALT_DOWN);
    public static List<JSONObject> detalleArtList;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private ProveedorDAO proveedorDAO;

    @Autowired
    private PedidoCabDAO pedidoCabDAO;

    @Autowired
    private PedidoCompraDAO pedidoCompraDAO;

    @Autowired
    private PedidoDetDAO pedidoDetDAO;

    @Autowired
    private PedidoDetConteoDAO pedidoDetConteoDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private FacturaCompraDetDAO fcdDAO;

    @Autowired
    private RecepcionDAO recepcionDAO;

    static String posicion;

    public static String compra = "";
    public static String entrada = "";
    public static String cod = "";

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente;
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;

    @FXML
    private Label labelClienteBuscar;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private TextField txtNumOrden;
    private TextField txtOrdenCompra;
    @FXML
    private Label labelSupervisor1112;
    private ChoiceBox<String> chkEmpresa;
    @FXML
    private TextField txtNomProveedor;
    @FXML
    private TextField txtRucProveedor;
    private TableColumn<JSONObject, String> tableColumnPlazo;
    @FXML
    private AnchorPane secondPane;
    @FXML
    private TextField txtCompra;
    @FXML
    private TextField txtNroEntrada;
    @FXML
    private Button btnFiltrar;
    @FXML
    private Button btnBuscar;
    @FXML
    private TableView<JSONObject> tableViewConteo;
    @FXML
    private Button btnSeleccionarTodo;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button btnSalir;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnCantidad;
    @FXML
    private TableColumn<JSONObject, String> columnObservacion;
    @FXML
    private TableColumn<JSONObject, String> columnPrecio;
    @FXML
    private TableColumn<JSONObject, String> columnTotal;
    @FXML
    private TextField txtCodigo;
    @FXML
    private TableColumn<JSONObject, Boolean> columnCheck;
    @FXML
    private Label labelSupervisor11121;
    @FXML
    private ChoiceBox<String> cbSucursal;
    @FXML
    private CheckBox chkCentral;
    @FXML
    private CheckBox chkCacique;
    @FXML
    private CheckBox chkSanLorenzo;
    @FXML
    private ImageView imageViewLogo;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
//        buscandoCliente(false);
    }

    private void buttonActualizarAction(ActionEvent event) {
        editandoCliente();
    }

    private void buttonBuscarClienteAction(ActionEvent event) {
        buscandoCliente();
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    private void buttonNuevoAction(ActionEvent event) {
//        creandoCliente();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    private void buttonBorrarAction(ActionEvent event) {
        borrando();
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        escucha();
        ubicandoContenedorSecundario();
        cargarSucursal();
        switch (ScreensContoller.getFxml()) {
            case "/vista/stock/ModCantidadFXML.fxml": {
                txtCompra.setText(compra);
                txtNroEntrada.setText(entrada);
                txtCodigo.setText(cod);
                actualizandoTablaCliente(getDetalleArtList());
                columnCheck.setVisible(false);
                cbSucursal.setValue(combo);

                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableViewConteo.requestFocus();
                        tableViewConteo.getSelectionModel().select(Integer.parseInt(posicion));
                        tableViewConteo.getFocusModel().focus(Integer.parseInt(posicion));
                        tableViewConteo.scrollTo(Integer.parseInt(posicion));
                    }
                });
                break;
            }
//            case "/vista/stock/CodArticuloFXML.fxml": {
//                txtPedido.setText(nroPedido);
//                buscarRecep();
//                chkDiasVisita.setValue(diaVisita);
//                chkTipoDoc.setValue(tipoDoc);
//                txtCuotas.setText(cuota);
//                txtPlazo.setText(plazo);
//                txtFrecuencia.setText(frecuencia);
//                //                cargarDetalle(pedidoCabDAO.getByNroOrden(txtPedido.getText()).getIdPedidoCab());
//                calcularTotales();
//
//                break;
//            }
            default:
//                toaster = new Toaster();
//        btnVerificarPrecio.setDisable(true);
                detalleArtList = new ArrayList<>();
                mapeoCheck = new HashMap();
//                depositoList = new ArrayList<>();
                orden = 0;
                repeatFocus(txtCompra);
//        cargarTipoMovimiento();
//                labelOrdenCompra.setText(rangoOCDAO.recuperarActual());
//                repeatFocus(txtPedido);
        }
        chkCentral.setSelected(true);
        chkCacique.setSelected(true);
        tableViewConteo.setEditable(true);
        columnCantidad.setCellFactory(TextFieldTableCell.forTableColumn());
        columnCantidad.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {

                int row = t.getTablePosition().getRow();

//                ((Lin) t.getTableView().getItems().get(row)).nameProperty().setValue(t.getNewValue());
                tableViewConteo.getSelectionModel().select(row + 1);
//                tv.edit(row + 1, name);  // <--- here not work edition next line
            }
        });
//        columnCantidad.setOnEditCommit(
//                new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<Lin, String> t) {
//
//                int row = t.getTablePosition().getRow();
//
//                ((Lin) t.getTableView().getItems().get(row)).nameProperty().setValue(t.getNewValue());
//
//                tv.getSelectionModel().select(row + 1);
//                tv.edit(row + 1, name);  // <--- here not work edition next line
//            }
//        }
        columnObservacion.setCellFactory(TextFieldTableCell.forTableColumn());
        cargandoImagen();
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneCliente.getChildren()
                .get(anchorPaneCliente.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoFormaPago() {
        clienteSi = true;
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, true);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
    }

    private void volviendo() {
//        clienteSi = false;
//        if (StageSecond.getStageData().isShowing()) {
//            StageSecond.getStageData().close();
//        }
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", 745, 504, "/vista/stock/BuscarProveedorFXML.fxml", 581, 450, false);
//        this.sc.loadScreen("/vista/caja/FormasDePagoFXML.fxml", 746, 537, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void listenerCampos() {

    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static void setDetalleArtList(List<JSONObject> detalleArtList) {
//        FacturaAprobadaPagoFXMLController.detalleArtList = detalleArtList;
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            filtrar();
        }

        if (keyCode == event.getCode().F4) {
//            if (!buttonNuevo.isDisable()) {
            seleccionar();
//            }
        }
        if (keyCode == event.getCode().F2) {
//            if (!buttonNuevo.isDisable()) {
            procesar();
//            }
        }
        if (keyCode == event.getCode().F6) {
//            if (!buttonNuevo.isDisable()) {
            tableViewConteo.requestFocus();
//            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (!alert) {
                volviendo();
            } else {
                alert = false;
            }
        }
        if (keyCode == event.getCode().ENTER) {
//            JSONParser parser = new JSONParser();
            if (alert) {
                alert = false;
            }
//            if (tableViewConteo.isFocused()) {
//                int num = tableViewConteo.getSelectionModel().getSelectedIndex() + 1;
//                tableViewConteo.requestFocus();
//                tableViewConteo.getSelectionModel().select(num);
//                tableViewConteo.getFocusModel().focus(num);
//                tableViewConteo.scrollTo(num);
////                compra = txtCompra.getText();
////                entrada = txtNroEntrada.getText();
////                cod = txtCodigo.getText();
//////            sucursal = cbSucursal.getSelectionModel().getSelectedItem();
//////            System.out.println("-->> " + tableViewFactura.getSelectionModel().getSelectedItem().toString());
////                if ("cliente_caja")) {
//////            BuscarClienteMayFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
////                    posicion = tableViewConteo.getSelectionModel().getSelectedIndex() + "";
////                    ModCantidadFXMLController.setCantidad(tableViewConteo.getSelectionModel().getSelectedItem().get("cantidad").toString(), tableViewConteo);
////                    this.sc.loadScreenModal("/vista/stock/ModCantidadFXML.fxml", 232, 95, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
////                } else {
////                    this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
////
////                }
//            }
//            else if (!buttonAnhadir.isDisable()) {
////                try {
//
//                seteandoParam(jsonSeleccionActual);
//                txtRucCli.setText(jsonSeleccionActual.get("ruc").toString());
//                txtRazonCli.setText(jsonSeleccionActual.get("descripcion").toString());
//                if (ScreensContoller.getFxml().contentEquals("/vista/stock/FacturaCompraFXML.fxml")) {
//                    txtTimbrado.setText(jsonSeleccionActual.get("timbrado").toString());
//                }
////                    JSONObject cab = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
////                    JSONObject cliente = new JSONObject();
////                    cliente.put("idCliente", idCliente);
////                    cab.put("cliente", cliente);
////                    fact.put("facturaClienteCab", cab);
////                    actualizarDatos();
//                navegandoFormaPago();
////                } catch (ParseException ex) {
////                    Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
////                }
//            }
        }

        if (altG.match(event)) {
            recortarDetalle();
//            recortarDetalle();
        }
//        if (altEnter.match(event)) {
////            int row = t.getTablePosition().getRow();
////                tableViewConteo.getSelectionModel().select(row + 1);
//            if (tableViewConteo.isFocused()) {
//                int num = tableViewConteo.getSelectionModel().getSelectedIndex() + 1;
////                tableViewConteo.requestFocus();
////                tableViewConteo.getSelectionModel().select(num);
////                tableViewConteo.getSelectionModel().select(num);
////                tableViewConteo.getFocusModel().focus(2);
////                System.out.println("0) " + tableViewConteo.getVisibleLeafColumn(0) + " - " + tableViewConteo.getVisibleLeafColumn(0).toString());
////                System.out.println("1) " + tableViewConteo.getVisibleLeafColumn(1) + " - " + tableViewConteo.getVisibleLeafColumn(1).toString());
////                System.out.println("2) " + tableViewConteo.getVisibleLeafColumn(2) + " - " + tableViewConteo.getVisibleLeafColumn(2).toString());
////                System.out.println("3) " + tableViewConteo.getVisibleLeafColumn(3) + " - " + tableViewConteo.getVisibleLeafColumn(3).toString());
////                System.out.println("4) " + tableViewConteo.getVisibleLeafColumn(4) + " - " + tableViewConteo.getVisibleLeafColumn(4).toString());
////                System.out.println("5) " + tableViewConteo.getVisibleLeafColumn(5) + " - " + tableViewConteo.getVisibleLeafColumn(5).toString());
////                System.out.println("6) " + tableViewConteo.getVisibleLeafColumn(6) + " - " + tableViewConteo.getVisibleLeafColumn(6).toString());
////                tableViewConteo.getFocusModel().focus(num, tableViewConteo.getVisibleLeafColumn(3));
////                tableViewConteo.scrollTo(num);
//// selection for single cells instead of single 
//                tableViewConteo.getSelectionModel().setCellSelectionEnabled(true);
//
//// select third cell in first (possibly nested) column
//                tableViewConteo.getSelectionModel().clearAndSelect(num, tableViewConteo.getVisibleLeafColumn(3));
//
//// focus the same cell
//                tableViewConteo.getFocusModel().focus(num, tableViewConteo.getVisibleLeafColumn(3));
//            }
//        }
    }

    private void limpiarDatos() {
//        limpiar();
        mapeoCheck = new HashMap();
        detalleArtList = new ArrayList<>();
        tableViewConteo.getItems().clear();
        idPedidoCab = 0;
        txtCodigo.setText("");
        txtNroEntrada.setText("");
        txtCompra.setText("");
        txtCompra.requestFocus();
        columnCheck.setVisible(true);
//        dpDesde.setValue(null);
//        dpHasta.setValue(null);
//        dpFecIni.setValue(null);
//        dpFecFin.setValue(null);
//        labelTotalGs.setText("Gs 0");
    }

    private void escucha() {
        escucha = true;
        tableViewConteo.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (tableViewConteo.getSelectionModel().getSelectedItem() != null) {

//                if (buttonAnhadir.isDisable()) {
//                    buttonAnhadir.setDisable(false);
//                }
                jsonSeleccionActual = newSelection;
//                txtNomProveedor.setText(jsonSeleccionActual.get("proveedor").toString());
//                txtNumOrden.setText(jsonSeleccionActual.get("oc").toString());
                //editar cliente, por si acaso...
            }
        });
    }
//LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void borrando() {
        resetParam();
        navegandoFormaPago();
    }

    public static void seteandoParam(JSONObject cliente) {
        jsonCliente = cliente;
    }

    public static void resetParam() {
        jsonCliente = null;
        clienteSi = false;
    }

    public static boolean isClienteSi() {
        return clienteSi;
    }

    public static JSONObject getJsonCliente() {
        return jsonCliente;
    }

    //límite de registros en lado backend...
    private List<JSONObject> jsonArrayCliente(String nom, String ruc) {
        List<JSONObject> clienteJSONObjList = new ArrayList<>();
        clienteJSONObjList = generarListaCliente(nom, ruc);
        return clienteJSONObjList;
    }

    private void buscandoCliente() {
////        clienteList = new ArrayList<>();
//        tableViewConteo.getItems().clear();
//        String proveedor = "";
//        String sucursal = "";
//        String oc = "";
////        if (!txtProveedor.getText().equals("")) {
////            proveedor = BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString();
////        }
//        try {
//            if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("-- SELECCIONE --")) {
//                sucursal = chkEmpresa.getSelectionModel().getSelectedItem();
//            }
//        } catch (Exception e) {
//        } finally {
//        }
////        if (!chkEmpresa.getSelectionModel().getSelectedItem().equals("")) {
//
////        }
//        if (!txtOrdenCompra.getText().equals("")) {
//            oc = txtOrdenCompra.getText();
//        }
//        List<PedidoCab> lisPedido = pedidoCabDAO.filtrarPorProveedorSucursalOc(proveedor, sucursal, oc);
//        for (PedidoCab pedidoCab : lisPedido) {
//            JSONObject jsonObj = new JSONObject();
//            jsonObj.put("proveedor", pedidoCab.getProveedor().getDescripcion());
//            jsonObj.put("docProveedor", pedidoCab.getProveedor().getRuc());
//            String date = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date(pedidoCab.getFecha().getTime()));
////                txtFechaPedido.setText(date);
//            jsonObj.put("fecha", date);
//            jsonObj.put("oc", pedidoCab.getOc());
//            jsonObj.put("tipo", pedidoCab.getTipo());
//            jsonObj.put("plazo", pedidoCab.getPlazo());
//            long total = pedidoDetDAO.recuperarTotal(pedidoCab.getIdPedidoCab());
//            jsonObj.put("total", total);
//            jsonObj.put("sucursal", pedidoCab.getSucursal());
//            jsonObj.put("comprador", pedidoCab.getUsuario());
//            jsonObj.put("idPedidoCab", pedidoCab.getIdPedidoCab());
//
////            clienteList.add(jsonObj);
//        }
////        actualizandoTablaCliente();
    }

    private void buscarClientePorRucCi() {
//        String rucCliente = "";
//        JSONParser parser = new JSONParser();
//        try {
//            rucCliente = textFieldRucCliente.getText();
//            //contar la cantidad de '-' que se ha introducido, a modo de saber si es un extranjero
//            int contador = rucCliente.split("-", -1).length - 1;
//            Proveedor cliente = new Proveedor();
//            if (contador >= 1) {
//                //Listar por RUC/CI o por lo que se ingresa en el caso que sea extranjero y tenga que ingresar un ci y tenga mas de un guion
//                clienteList = new ArrayList<>();
////                cliente = cliDAO.listarPorRuc(rucCliente);
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getDescripcion() != null) {
////                    cliente.setFecNac(null);
//                    cliente.setFechaAlta(null);
//                    cliente.setFechaMod(null);
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            } else if (contador == 0) {
//                //Listar por CI en el caso que exista
//                cliente = proveedorDAO.listarPorRuc(rucCliente);
//                if (cliente.getIdProveedor() == null) {
//                    // Primera Revancha: listar datos ingresando solo ci y recuperando una lista de RUC 
//                    List<Proveedor> listClien = proveedorDAO.listarPorCIRevancha(rucCliente);
//                    if (listClien != null) {
//                        if (listClien.isEmpty()) {
//                            clienteList = new ArrayList<>();
//                        } else {
//                            clienteList = new ArrayList<>();
//                            for (int i = 0; i < listClien.size(); i++) {
//                                Proveedor cli = listClien.get(i);
////                                cli.setFecNac(null);
//                                cli.setFechaAlta(null);
//                                cli.setFechaMod(null);
//                                JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
//                                clienteList.add(jsonCli);
//                            }
//                        }
//                    } else {
//                        clienteList = new ArrayList<>();
//                    }
//                } else {
//                    clienteList = new ArrayList<>();
//                    JSONObject jsonCli = (JSONObject) parser.parse(gson.toJson(cliente.toProveedorBdDTO()));
//                    clienteList.add(jsonCli);
//                }
//            }
//            if (clienteList.isEmpty()) {
//                mensajeAlerta("No se encuentra registrado el cliente seleccionado");
//            }
//        } catch (Exception e) {
//            System.out.println("ERROR AL INTENTAR PARSEAR TIPO DE DATO");
//        }
    }

    private void buscarAutomatico(JSONObject cliente) {
//        if (cliente != null) {
//            if (cliente.get("ruc") != null) {
//                textFieldRucCliente.setText(cliente.get("ruc").toString());
//            } else {
//                textFieldRucCliente.setText("");
//            }
//            textFieldNombreCliente.setText(cliente.get("nombre").toString());
//            buscandoCliente(true);
//        }
    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(fact);
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        manejo.setUsuario(DatosEnCaja.getUsers().toString());
        String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
        jsonFact = jsonFact.replace("\"[", "[");
        jsonFact = jsonFact.replace("]\"", "]");
        manejo.setFactura(jsonFact);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            System.out.println("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            System.out.println(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////CREATE, CLIENTE -> POST
//    private boolean creandoCliente() {
//        if ("nuevo_cliente_caja")) {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿GENERAR NUEVO CLIENTE?", ok, cancel);
//            this.alert = true;
//            alert.showAndWait();
//            if (alert.getResult() == ok) {
//                alert.close();
//                JSONObject cliente = new JSONObject();
//                cliente = creandoJsonCliente();
//                int codCli = Integer.parseInt(cliente.get("codCliente").toString());
//                Cliente clie = cliDAO.getByCod(codCli);
//                if (clie == null) {
//                    long idActual = rangoCliDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    cliente.put("idCliente", idActual);
//                    exitoCrear = persistiendoPendientes(cliente);
//                    if (exitoCrear) {
//                        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
//                        cliente.put("fechaAlta", null);
//                        cliente.put("fechaMod", null);
//                        ClienteDTO cliDTO = gson.fromJson(cliente.toString(), ClienteDTO.class);
//                        cliDTO.setFechaAlta(timestamp);
//                        cliDTO.setFechaMod(timestamp);
//                        Cliente cli = Cliente.fromClienteDTO(cliDTO);
//                        Pais pais = new Pais();
//                        pais.setIdPais(0L);
//                        Departamento dpto = new Departamento();
//                        dpto.setIdDepartamento(0l);
//                        Ciudad ciu = new Ciudad();
//                        ciu.setIdCiudad(0l);
//                        Barrio barr = new Barrio();
//                        barr.setIdBarrio(0l);
//                        cli.setPais(pais);
//                        cli.setDepartamento(dpto);
//                        cli.setCiudad(ciu);
//                        cli.setBarrio(barr);
//                        try {
//                            cliDAO.insertar(cli);
//                            System.out.println("-->> DATOS INSERTADOS CORRECTAMENTE");
//                        } catch (Exception e) {
//                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//                            System.out.println("-->> LOS DATOS NO HAN PODIDO SER INSERTADOS");
//                        }
//                        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡NUEVO CLIENTE REGISTRADO!", ButtonType.OK);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.OK) {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        } else {
//                            alert2.close();
//                            buscarAutomatico(cliente);
//                        }
//                    } else {
//                        Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE REGISTRÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
//                        this.alert = true;
//                        alert2.showAndWait();
//                        if (alert2.getResult() == ButtonType.CLOSE) {
//                            alert2.close();
//                        }
//                    }
//                } else {
//                    Alert alert2 = new Alert(Alert.AlertType.ERROR, "YA EXISTE EL CLIENTE CON EL MISMO CODIGO.", ButtonType.CLOSE);
//                    this.alert = true;
//                    alert2.showAndWait();
//                    if (alert2.getResult() == ButtonType.CLOSE) {
//                        alert2.close();
//                    }
//                }
//            } else {
//                alert.close();
//            }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
//        return exitoCrear;
//    }
    //////CREATE, CLIENTE -> POST
    //////UPDATE, CLIENTE -> PUT
    private boolean editandoCliente() {
//        if ("editar_cliente_caja")) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿ACTUALIZAR DATOS DEL CLIENTE?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            JSONObject cliente = new JSONObject();
            cliente = editandoJsonCliente();
            exitoEditar = actualizarPendientes(cliente);
            if (exitoEditar) {
                try {
                    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                    JSONObject usuarioCajero = Identity.getUsuario();
                    JSONObject funcionarioCaj = (JSONObject) usuarioCajero.get("funcionario");
                    String nombreCaj = funcionarioCaj.get("nombre").toString() + " " + funcionarioCaj.get("apellido").toString();
                    cliDAO.actualizarNomApeRuc(cliente.get("nombre").toString(), cliente.get("apellido").toString(), cliente.get("ruc").toString(), cliente.get("telefono").toString(), cliente.get("telefono2").toString(), Long.parseLong(cliente.get("idCliente").toString()), nombreCaj, timestamp);
                    System.out.println("-->> DATOS ACTUALIZADOS CORRECTAMENTE");
                } catch (Exception e) {
                    Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                    System.out.println("-->> LOS DATOS NO HAN PODIDO SER ACTUALIZADOS");
                }
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CLIENTE ACTUALIZADO!", ButtonType.OK);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.OK) {
                    alert2.close();
                    buscarAutomatico(cliente);
                } else {
                    alert2.close();
                    buscarAutomatico(cliente);
                }
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CLIENTE NO SE ACTUALIZÓ.\nVERIFIQUE LOS CAMPOS.", ButtonType.CLOSE);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.CLOSE) {
                    alert2.close();
                }
            }
        } else {
            alert.close();
        }
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/ClienteFXML.fxml", 890, 748, false);
//        }
        return exitoEditar;
    }
    //////UPDATE, CLIENTE -> PUT

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeAlerta(String msj) {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ok);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ok) {
            this.alert = true;
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, CLIENTE
    public List<JSONObject> generarListaCliente(String nom, String ruc) {
        JSONParser parser = new JSONParser();
        List<Proveedor> cliente = proveedorDAO.listarPorNomRuc(nom, ruc);
        List<JSONObject> listaCliente = new ArrayList<>();
        for (Proveedor cli : cliente) {
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(cli.toProveedorBdDTO()));
                listaCliente.add(obj);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            }
        }
        return listaCliente;
    }
    //////READ, CLIENTE

    //////INSERT, PENDIENTES - CLIENTE
    private boolean persistiendoPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'I', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////INSERT, PENDIENTES - CLIENTE

    //////UPDATE, PENDIENTES - CLIENTE
    private boolean actualizarPendientes(JSONObject cliente) {
        ConexionPostgres.conectar();
        boolean valor = false;
        JSONObject pais = new JSONObject();
        pais.put("idPais", 0);
        JSONObject dpto = new JSONObject();
        dpto.put("idDepartamento", 0);
        JSONObject ciudad = new JSONObject();
        ciudad.put("idCiudad", 0);
        JSONObject barrio = new JSONObject();
        barrio.put("idBarrio", 0);
        cliente.put("pais", pais);
        cliente.put("barrio", barrio);
        cliente.put("ciudad", ciudad);
        cliente.put("departamento", dpto);
        String sql = "INSERT INTO pendiente.cliente_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.host + "', 'U', '" + cliente.toString() + "', now(), 'cliente')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - CLIENTE
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO CLIENTE
//    private JSONObject creandoJsonCliente() {
//        //solo tres campos desde caja: nombre, apellido, ruc / ci; y por supuesto quién y cuándo lo hizo...
//        Date date = new Date();
//        Timestamp timestamp = new Timestamp(date.getTime());
//        Long timestampJSON = timestamp.getTime();
//        JSONObject cliente = new JSONObject();
//        //**********************************************************************
//        cliente.put("nombre", textFieldNombreClienteNuevo.getText());
//        cliente.put("apellido", textFieldClienteApellidoNuevo.getText());
//        if (textFieldRucClienteNuevo.getText().contentEquals("")) {
//            cliente.put("ruc", "0");
//        } else {
//            int count = StringUtils.countMatches(textFieldRucClienteNuevo.getText(), "-");
//            if (count == 0) {
//                if (textFieldRucClienteNuevo.getText().length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(textFieldRucClienteNuevo.getText()).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(textFieldRucClienteNuevo.getText()));
//                    }
//                }
//            } else if (count == 1) {
//                StringTokenizer st = new StringTokenizer(textFieldRucClienteNuevo.getText(), "-");
//                String cedula = st.nextElement().toString();
//
//                if (cedula.length() >= 8) {
//                    cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                } else {
//                    if (Utilidades.calculoSET(cedula).equals("")) {
//                        cliente.put("ruc", textFieldRucClienteNuevo.getText());
//                    } else {
//                        cliente.put("ruc", Utilidades.calculoSET(cedula));
//                    }
//                }
//            } else {
//                cliente.put("ruc", textFieldRucClienteNuevo.getText());
//            }
//        }
//        cliente.put("usuAlta", Identity.getNomFun());
//        cliente.put("fechaAlta", timestampJSON);
//        cliente.put("usuMod", Identity.getNomFun());
//        cliente.put("fechaMod", timestampJSON);
//        //**********************************************************************
//        cliente.put("telefono2", textFieldClienteCelularNuevo.getText());
//        cliente.put("telefono", textFieldClienteTelefonoNuevo.getText());
//        cliente.put("segundaLateral", null);
//        cliente.put("primeraLateral", null);
//        cliente.put("nroLocal", null);
//        cliente.put("pais", null);
//        cliente.put("departamento", null);
//        cliente.put("ciudad", null);
//        cliente.put("barrio", null);
//        cliente.put("email", null);
//        cliente.put("compraUltFecha", null);
//        cliente.put("compraIniFecha", null);
//        String codCliente = "";
//        String arrayCod[] = textFieldRucClienteNuevo.getText().split("-");
//        if (arrayCod.length > 0) {
//            codCliente = arrayCod[0];
//        } else {
//            codCliente = textFieldRucClienteNuevo.getText();
//        }
//        cliente.put("codCliente", Integer.valueOf(numVal.numberValidator(codCliente)));
//        cliente.put("callePrincipal", null);
//        return cliente;
//    }
    //JSON CREANDO CLIENTE
    //JSON EDITANDO CLIENTE
    private JSONObject editandoJsonCliente() {
        return null;
    }
    //JSON EDITANDO CLIENTE
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void actualizandoTablaCliente(List<JSONObject> arrayLista) {
        numVal = new NumberValidator();
        clienteData = FXCollections.observableArrayList(arrayLista);

        columnCheck.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
                return new BooleanCell(tableViewConteo);
            }
        };
        columnCheck.setCellValueFactory((data) -> {
            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
            try {
                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getString("codigo")).toString())) {
                    return new SimpleBooleanProperty(true);
                } else {
                    return new SimpleBooleanProperty(false);
                }
            } catch (Exception e) {
                return new SimpleBooleanProperty(false);
            } finally {
            }
        });
        columnCheck.setCellFactory(booleanCellFactory);
        columnCheck.setVisible(true);
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String apellido = String.valueOf(data.getValue().get("codigo"));
                if (apellido.contentEquals("null")) {
                    apellido = "";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("descripcion"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("cantidad"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> (t.getTableView().getItems().get(
//                        t.getTablePosition().getRow())).put("cantidad", t.getNewValue())
//        );
        columnCantidad.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
                t.getTableView().getItems().get(t.getTablePosition().getRow()).put("cantidad", t.getNewValue());
//                if (tableViewConteo.isFocused()) {
                int num = tableViewConteo.getSelectionModel().getSelectedIndex() + 1;
// selection for single cells instead of single 
                tableViewConteo.getSelectionModel().setCellSelectionEnabled(true);
// select third cell in first (possibly nested) column
                tableViewConteo.getSelectionModel().clearAndSelect(num, tableViewConteo.getVisibleLeafColumn(3));
// focus the same cell
                tableViewConteo.getFocusModel().focus(num, tableViewConteo.getVisibleLeafColumn(3));
                tableViewConteo.scrollTo((num - 5));
//                }
            }
        });
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> (t.getTableView().getItems().get(
//                        t.getTablePosition().getRow())).put("cantidad", t.getNewValue())
//        );
        columnObservacion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnObservacion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("observacion"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(ruc);
            }
        });
        columnObservacion.setOnEditCommit(
                (TableColumn.CellEditEvent<JSONObject, String> t)
                -> (t.getTableView().getItems().get(
                        t.getTablePosition().getRow())).put("observacion", t.getNewValue())
        );
        columnPrecio.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnPrecio.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("precio"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(ruc)));
            }
        });
        columnTotal.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("total"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
                return new ReadOnlyStringWrapper(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(ruc)));
            }
        });
//        tableColumnComprador.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnComprador.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String ruc = String.valueOf(data.getValue().get("comprador"));
//                if (ruc.contentEquals("null")) {
//                    ruc = "";
//                }
//                return new ReadOnlyStringWrapper(ruc);
//            }
//        });
        //columna Ruc .................................................
        tableViewConteo.setItems(clienteData);
        if (!escucha) {
            escucha();
        }
    }

    //TABLE VIEW, TABLE VIEW, TABLE VIEW****************************************
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    private void buttonNroEntradaAction(ActionEvent event) {
//        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
//            RecepcionFXMLController.cargarRecepcion(tableViewConteo.getSelectionModel().getSelectedItem().get("docProveedor").toString(), tableViewConteo.getSelectionModel().getSelectedItem().get("proveedor").toString(),
//                    tableViewConteo.getSelectionModel().getSelectedItem().get("sucursal").toString()
//            );
//            this.sc.loadScreen("/vista/stock/RecepcionFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA AGREGAR NUMERO DE ENTRADA...");
//        }
    }

    private void buttonImprimirAction(ActionEvent event) {
        exportar();
    }

    private void buscarProveedor() {
//        BuscarProveedorFXMLController.cargarRucRazon(txtRucProveedor, txtProveedor);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void exportar() {
        if (tableViewConteo.getSelectionModel().getSelectedIndex() >= 0) {
            JSONArray jsonFacturas = recuperarDatos();

            if (jsonFacturas.isEmpty()) {
                Date dates = new Date();
                Timestamp ts = new Timestamp(dates.getTime());
                String fechaArray[] = ts.toString().split(" ");
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + fechaArray[1];
                informandoVentas("{\"ventas\": [{\"orden\":\"N/A\",\"codigo\":\"N/A\",\"montoFactura\":0,"
                        + "\"descripcion\":\"N/A\",\"recepcion\":\"\",\"conteo\":\"\",\"observacion\":\"\",\"porcDescTarjeta\":\"0\","
                        + "\"fiel\":0,\"porcDescFiel\":\"0\",\"func\":0,\"porcDescFunc\":\"0\","
                        + "\"promoArt\":0,\"porcDescPromoArt\":\"0\",\"subRepNomFun\":\"" + Identity.getNomFun() + "\",\"subRepTimestamp\":\"" + subRepTimestamp + "\",\"promoSec\":0,\"porcDescPromoSec\":\"0\"}]}");

            } else {
                JSONArray array = jsonFacturas;
                informandoVentas("{\"ventas\": " + array + "}");
            }
            this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA IMPRIMIR LA ORDEN...");
        }
    }

    private JSONArray recuperarDatos() {
        JSONArray array = new JSONArray();
        List<PedidoCompra> listPedidoCompra = pedidoCompraDAO.listarPorCabecera(Long.parseLong(tableViewConteo.getSelectionModel().getSelectedItem().get("idPedidoCab").toString()));
        if (listPedidoCompra.size() > 0) {
            int ord = 0;
            for (PedidoCompra pedidoCompra : listPedidoCompra) {
                ord++;
                JSONObject json = new JSONObject();
                json.put("codigo", ord);
                json.put("codigo", pedidoCompra.getArticulo().getCodArticulo());
                json.put("descripcion", pedidoCompra.getDescripcion());
                json.put("recepcion", "");
                json.put("conteo", "");
                json.put("observacion", "");

                proveedor = pedidoCabDAO.getById(pedidoCompra.getPedidoCab().getIdPedidoCab()).getProveedor().getDescripcion();
                Date date = new Date();
                Timestamp ts = new Timestamp(date.getTime());
                String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
                StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
                String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
                json.put("subRepNomFun", Identity.getNomFun());
                json.put("subRepTimestamp", subRepTimestamp);
                json.put("sucursal", pedidoCompra.getPedidoCab().getSucursal());
                array.add(json);
            }
        }
        return array;
    }

    private void suprimirSelecciones() {
        JSONParser parser = new JSONParser();
//        int val = 0;
        long monto = 0;
//        long tam = tableViewConteo.getItems().size();
        List aEliminar = new ArrayList();
//        ObservableList<JSONObject> ol = tableViewConteo.getItems();
        for (JSONObject item : tableViewConteo.getItems()) {
            org.json.JSONObject json = new org.json.JSONObject(item);
            JSONObject jsonArti = new JSONObject();
            try {
//                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
                String x = mapeoCheck.get(json.getInt("orden") + "-" + json.getString("codigo")).toString();
                System.out.println("ESTADO: " + x);
                boolean estado = Boolean.parseBoolean(x);
                System.out.println("DATA: " + estado);

            } catch (Exception e) {
                System.out.println("-->> " + e.getLocalizedMessage());
                System.out.println("-->> " + e.getMessage());
                System.out.println("-->> " + e.fillInStackTrace());
//                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                aEliminar.add(item);
            } finally {
            }
            columnCheck.setVisible(false);
        }

        for (int i = 0; i < aEliminar.size(); i++) {
            try {
                System.out.println("-> " + aEliminar.get(i));
                detalleArtList.remove(aEliminar.get(i));
                System.out.println("-STATUS : " + tableViewConteo.getItems().remove(aEliminar.get(i)));

            } catch (Exception e) {
            } finally {
            }
        }
//        actualizarTotales(false);
    }

    private void informandoVentas(String jsonString) {
    }

    private void recortarDetalle() {
        if (!estadoCheck) {
            long total = 0;
            List<JSONObject> lista = getDetalleArtList();
            detalleArtList = new ArrayList<>();
            tableViewConteo.getItems().clear();
            for (JSONObject json : lista) {
//                    org.json.JSONObject jsonDat = new org.json.JSONObject(json);
//                    mapeoCheck.put(jsonDat.getInt("codigo") + "-" + jsonDat.getBigInteger("factura"), true);
//                total += Math.round(Long.parseLong(json.get("pagar").toString()));
                detalleArtList.add(json);
            }
//            labelTotalGs.setText(numVal.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(total))));
//                suprimirSelecciones();

            actualizandoTablaCliente(getDetalleArtList());
//                precioTotal = total;
//                resetFormulario(total);

//                cerrando();
        } else {
            suprimirSelecciones();
        }
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    @FXML
    private void tableViewConteoMouseClicked(MouseEvent event) {
//        if (event.getClickCount() == 2) {
//            compra = txtCompra.getText();
//            entrada = txtNroEntrada.getText();
//            cod = txtCodigo.getText();
////            sucursal = cbSucursal.getSelectionModel().getSelectedItem();
////            System.out.println("-->> " + tableViewFactura.getSelectionModel().getSelectedItem().toString());
//            if ("cliente_caja")) {
////            BuscarClienteMayFXMLController.cargarCliente(labelRucCliente, labelNombreCliente, textFieldCod);
//                posicion = tableViewConteo.getSelectionModel().getSelectedIndex() + "";
//                ModCantidadFXMLController.setCantidad(tableViewConteo.getSelectionModel().getSelectedItem().get("cantidad").toString(), tableViewConteo);
//                this.sc.loadScreenModal("/vista/stock/ModCantidadFXML.fxml", 232, 95, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//            } else {
//                this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//
//            }
//        }
    }

    private void seleccionarTodo() {
        List<JSONObject> lista = getDetalleArtList();
        detalleArtList = new ArrayList<>();
        tableViewConteo.getItems().clear();
        for (JSONObject json : lista) {
            mapeoCheck.put(Integer.parseInt(json.get("orden") + "") + "-" + json.get("codigo").toString(), true);
//            System.out.println("ESTADO: " + x);
//            boolean estado = Boolean.parseBoolean(x);
//            System.out.println("DATA: " + estado);
            detalleArtList.add(json);
        }

        actualizandoTablaCliente(getDetalleArtList());

        suprimirSelecciones();
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    @FXML
    private void tableViewConteoKeyReleased(KeyEvent event) {
//        keyPress(event);
    }

    class BooleanCell extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCell(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int posicion = tableViewConteo.getSelectionModel().getSelectedIndex();
                    if (posicion >= 0) {
                        String ord = tableViewConteo.getSelectionModel()
                                .getSelectedItem().get("orden") + "-" + tableViewConteo.getSelectionModel()
                                .getSelectedItem().get("codigo");
                        if (checkBox.isSelected()) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
                            System.out.println(ord + " | " + true);
                            tableViewConteo.getSelectionModel().getSelectedItem().put("check", true);
                        } else {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
                            System.out.println(ord + " | " + false);
                            tableViewConteo.getSelectionModel().getSelectedItem().put("check", false);
                        }
                    }
                }
            });
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    @FXML
    private void btnFiltrarAction(ActionEvent event) {
        filtrar();
    }

    @FXML
    private void btnBuscarAction(ActionEvent event) {
        seleccionar();
    }

    @FXML
    private void btnSeleccionarTodoAction(ActionEvent event) {
        seleccionarTodo();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        procesar();
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        volviendo();
    }

    private void filtrar() {
        int canti = 0;
        if (chkCentral.isSelected()) {
            canti++;
        }
        if (chkCacique.isSelected()) {
            canti++;
        }
        if (chkSanLorenzo.isSelected()) {
            canti++;
        }
//        if (canti > 1) {
//            mensajeAlerta("DEBE SELECCIONAR UNA SOLA SUCURSAL");
//        } else {
        if (chkCentral.isSelected() || chkCacique.isSelected() || chkSanLorenzo.isSelected()) {
            boolean estado = false;
            combo = cbSucursal.getSelectionModel().getSelectedItem();
            if (columnCheck.isVisible()) {
                estado = true;
            } else {
                if (tableViewConteo.getItems().size() > 0) {
                    estado = true;
                } else {
                    mensajeAlerta("YA SE HAN CONFIRMADO LOS DATOS");
                }
            }
            if (estado) {
                if (txtCompra.getText().equals("") && txtNroEntrada.getText().equals("")) {
                    mensajeAlerta("EL CAMPO OC Y ENTRADA NO DEBE QUEDAR VACIO");
                } else {
//                Map mapeo = recepcionDAO.getByNroOrdenEntrada(txtCompra.getText(), txtNroEntrada.getText());
                    List<PedidoDetConteo> pedidoDet = new ArrayList<>();
                    List<PedidoCompra> pedidoCompra = new ArrayList<>();
                    try {
                        PedidoCab pc = pedidoCabDAO.listarPorOrden(txtCompra.getText());
                        List<PedidoDetConteo> lista = pedidoDetConteoDAO.listarPorCabecera(pc.getIdPedidoCab());
                        idPedidoCab = pc.getIdPedidoCab();
                        if (lista != null && lista.size() != 0) {
                            pedidoDet = lista;
                            valorEstado = true;
                        } else {
                            pedidoCompra = pedidoCompraDAO.listarPorCabecera(pc.getIdPedidoCab());
                        }
                    } catch (Exception e) {
                        pedidoDet = new ArrayList<>();
                    } finally {
                    }
                    detalleArtList = new ArrayList<>();
                    tableViewConteo.getItems().clear();
                    int orden = 0;

                    if (pedidoDet.size() > 0) {
                        for (PedidoDetConteo pd : pedidoDet) {
//                            long cantidad = 0;
//                            if (chkCentral.isSelected()) {
//                                cantidad += pd.getCantCc();
//                            }
//                            if (chkCacique.isSelected()) {
//                                cantidad += pd.getCantSc();
//                            }
//                            if (chkSanLorenzo.isSelected()) {
//                                cantidad += pd.getCantSl();
//                            }
//                            if (!String.valueOf(cantidad).equalsIgnoreCase("0")) {
                            orden++;
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("orden", orden);
                            jsonObj.put("codigo", pd.getArticulo().getCodArticulo() + "");
                            jsonObj.put("idArticulo", pd.getArticulo().getIdArticulo() + "");
                            jsonObj.put("descripcion", pd.getArticulo().getDescripcion());

                            int cant = Integer.parseInt(pd.getCantidad());
                            long total = cant * pd.getPrecio();
//                            if (valorEstado) {
                            jsonObj.put("idPedidoDetConteo", pd.getIdPedidoDetConteo());
                            jsonObj.put("idPedidoCab", pd.getPedidoCab().getIdPedidoCab());
                            jsonObj.put("cantidad", cant);
//                            } else {
//                                jsonObj.put("cantidad", 0);
//                            }

                            jsonObj.put("observacion", "");
                            jsonObj.put("precio", pd.getPrecio());
                            jsonObj.put("sucursal", "");
                            jsonObj.put("factura", total);
                            jsonObj.put("total", total);
                            jsonObj.put("cc", chkCentral.isSelected());
                            jsonObj.put("sc", chkCacique.isSelected());
                            jsonObj.put("sl", chkSanLorenzo.isSelected());

                            detalleArtList.add(jsonObj);
//                            }
                        }
                        actualizandoTablaCliente(detalleArtList);
                    } else if (pedidoCompra.size() > 0) {
                        for (PedidoCompra pd : pedidoCompra) {
                            long cantidad = 0;
                            if (chkCentral.isSelected()) {
                                cantidad += pd.getCantCc();
                            }
                            if (chkCacique.isSelected()) {
                                cantidad += pd.getCantSc();
                            }
                            if (chkSanLorenzo.isSelected()) {
                                cantidad += pd.getCantSl();
                            }
//                            if (!String.valueOf(cantidad).equalsIgnoreCase("0")) {
                            orden++;
                            JSONObject jsonObj = new JSONObject();
                            jsonObj.put("orden", orden);
                            jsonObj.put("codigo", pd.getArticulo().getCodArticulo() + "");
                            jsonObj.put("idArticulo", pd.getArticulo().getIdArticulo() + "");
                            jsonObj.put("descripcion", pd.getArticulo().getDescripcion());

                            int cant = Integer.parseInt(pd.getCantSc() + "") + Integer.parseInt(pd.getCantCc() + "") + Integer.parseInt(pd.getCantSl() + "");
                            long total = cant * pd.getPrecio();
                            if (valorEstado) {
                                jsonObj.put("idPedidoCompra", pd.getIdPedidoCompra());
                                jsonObj.put("cantidad", cantidad);
                            } else {
                                jsonObj.put("cantidad", 0);
                            }

                            jsonObj.put("observacion", "");
                            jsonObj.put("precio", pd.getPrecio());
                            jsonObj.put("factura", total);
                            jsonObj.put("total", 0L);

                            detalleArtList.add(jsonObj);
//                            }
                        }
                        actualizandoTablaCliente(detalleArtList);
                    } else {
                        mensajeAlerta("NO SE ENCONTRARON RESULTADOS DE LA BUSQUEDA...");
                    }
                }
            }
        } else {
            mensajeAlerta("DEBE SELECCIONAR UNA SUCURSAL PARA REALIZAR EL CONTEO");
        }
//        }
    }

    private void seleccionar() {
        for (int i = 0; i < tableViewConteo.getItems().size(); i++) {
            JSONObject json = (JSONObject) tableViewConteo.getItems().get(i);
            if (json.get("codigo").toString().equalsIgnoreCase(txtCodigo.getText())) {
                tableViewConteo.requestFocus();
                tableViewConteo.getSelectionModel().select(i);
                tableViewConteo.getFocusModel().focus(i);
                tableViewConteo.scrollTo(i);
                break;
            }
        }
    }

    private void procesar() {
        int i = 0;
        String sucursal = "";
        if (chkCentral.isSelected()) {
            sucursal += "CASA CENTRAL";
        } else if (chkCacique.isSelected()) {
            sucursal += "CACIQUE";
        } else if (chkSanLorenzo.isSelected()) {
            sucursal += "SAN LORENZO";
        }
        if (!valorEstado) {
            pedidoDetConteoDAO.eliminarPorcabeceraPedidoDetConteoSucursal(idPedidoCab, sucursal);
            for (JSONObject jSONObject : getDetalleArtList()) {
                PedidoDetConteo pdc = new PedidoDetConteo();
                pdc.setDescripcion(jSONObject.get("descripcion").toString());
                pdc.setCantidad(jSONObject.get("cantidad").toString());
                pdc.setPrecio(Long.parseLong(jSONObject.get("precio").toString()));
                long x = Long.parseLong(jSONObject.get("cantidad").toString()) * Long.parseLong(jSONObject.get("precio").toString());
                pdc.setTotal(x);

                Articulo art = new Articulo();
                art.setIdArticulo(Long.parseLong(jSONObject.get("idArticulo").toString()));
                pdc.setArticulo(art);

                PedidoCab pc = new PedidoCab();
                pc.setIdPedidoCab(idPedidoCab);
                pdc.setPedidoCab(pc);
                if (chkCentral.isSelected()) {
                    pdc.setCc(true);
                } else {
                    pdc.setCc(false);
                }
                if (chkCacique.isSelected()) {
                    pdc.setSc(true);
                } else {
                    pdc.setSc(false);
                }
                if (chkSanLorenzo.isSelected()) {
                    pdc.setSl(true);
                } else {
                    pdc.setSl(false);
                }
                pdc.setSucursal(cbSucursal.getSelectionModel().getSelectedItem());

                i++;
                pedidoDetConteoDAO.insercionMasiva(pdc, i);

            }
        } else {
            for (JSONObject jSONObject : getDetalleArtList()) {
                PedidoDetConteo pdc = new PedidoDetConteo();
                pdc.setIdPedidoDetConteo(Long.parseLong(jSONObject.get("idPedidoDetConteo").toString()));
                pdc.setCantidad(jSONObject.get("cantidad").toString());
                pdc.setDescripcion(jSONObject.get("descripcion").toString());
                pdc.setPrecio(Long.parseLong(jSONObject.get("precio").toString()));
                long x = Long.parseLong(jSONObject.get("cantidad").toString()) * Long.parseLong(jSONObject.get("precio").toString());
                pdc.setTotal(x);

                Articulo art = new Articulo();
                art.setIdArticulo(Long.parseLong(jSONObject.get("idArticulo").toString()));
                pdc.setArticulo(art);

                PedidoCab pc = new PedidoCab();
                pc.setIdPedidoCab(idPedidoCab);
                pdc.setPedidoCab(pc);
                pdc.setSucursal(jSONObject.get("sucursal").toString());
                if (chkCentral.isSelected()) {
                    pdc.setCc(true);
                } else {
                    pdc.setCc(false);
                }
                if (chkCacique.isSelected()) {
                    pdc.setSc(true);
                } else {
                    pdc.setSc(false);
                }
                if (chkSanLorenzo.isSelected()) {
                    pdc.setSl(true);
                } else {
                    pdc.setSl(false);
                }
//                pdc.setSucursal(cbSucursal.getSelectionModel().getSelectedItem());

                i++;
                pedidoDetConteoDAO.actualizacionMasiva(pdc, i);

            }
        }
        if (i >= 1) {
            if (valorEstado) {
                mensajeAlerta("DATOS ACTUALIZADOS CORRECTAMENTE");
            } else {
                mensajeAlerta("DATOS INSERTADOS CORRECTAMENTE");
            }
            limpiarDatos();
        }
        valorEstado = false;
    }

    private void cargarSucursal() {
        cbSucursal.getItems().addAll("CASA CENTRAL", "SAN LORENZO", "CACIQUE");
    }

}
