/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ModCantidadReponerFXMLController extends BaseScreenController implements Initializable {

    static void setCantidad(String cantCC, String cantSL, String cantSC, TableView tv) {
        cantidadCC = cantCC;
        cantidadSL = cantSL;
        cantidadSC = cantSC;
        tableFact = tv;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private NumberValidator numVal;
    static String cantidadCC = "";
    static String cantidadSL = "";
    static String cantidadSC = "";
    static TableView<JSONObject> tableFact;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean alertEscEnter;
//    boolean crearNuevo;

    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    private List<JSONObject> clienteFielList;
    private static JSONObject jsonClienteFiel;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente = new JSONObject();
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private JSONObject clienteFiel;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private Label labelRetiroDinero;
//    private TextField textFieldCantidad;
    @FXML
    private TextField txtCentral;
    @FXML
    private Button btnConfirmar;
    @FXML
    private TextField txtSanLorenzo;
    @FXML
    private Label labelRucClienteNuevo111;
    @FXML
    private TextField txtCacique;
    @FXML
    private Label labelRucClienteNuevo1111;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        repeatFocusData(txtCentral);
        txtCentral.setText(cantidadCC);
        txtSanLorenzo.setText(cantidadSL);
        txtCacique.setText(cantidadSC);
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void navegandoFacturaVenta() {

    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreenModal("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/ModCantidadMayFXML.fxml", 232, 95, false);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtCentral.isFocused()) {
                txtSanLorenzo.requestFocus();
            } else if (txtSanLorenzo.isFocused()) {
                txtCacique.requestFocus();
            } else if (txtCacique.isFocused()) {
                txtCentral.requestFocus();
                confirmacion();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }

    private void mensajeAlerta(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoClienteFiel() {

    }

    private void nuevoClienteFiel() {

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    @FXML
    private void btnConfirmarAction(ActionEvent event) {
        confirmacion();
    }

    private void confirmacion() {
        if (!txtCentral.getText().equalsIgnoreCase("") && !txtSanLorenzo.getText().equalsIgnoreCase("") && !txtCacique.getText().equalsIgnoreCase("")) {
            if (StageSecond.getStageData().isShowing()) {
                StageSecond.getStageData().close();
            }
//                cantRepoCacique-cantRepoSanLorenzo-cantRepoCentral
//                tableFact.getSelectionModel().getSelectedItem().put("cantidad", Integer.parseInt(textFieldCantidad.getText()));
            tableFact.getSelectionModel().getSelectedItem().put("cantRepoCentral", Integer.parseInt(txtCentral.getText()));
            tableFact.getSelectionModel().getSelectedItem().put("cantRepoSanLorenzo", Integer.parseInt(txtSanLorenzo.getText()));
            tableFact.getSelectionModel().getSelectedItem().put("cantRepoCacique", Integer.parseInt(txtCacique.getText()));

//                JSONParser parser = new JSONParser();
            int val = 0;
            MatrizFXMLController.detalleArtList = new ArrayList<>();
            MatrizFXMLController.matrizList = new ArrayList<>();
            //                    FacturaDeVentaMayFXMLController.hashJsonArtDet = new HashMap<>();
//                    FacturaDeVentaMayFXMLController.hashJsonArticulo = new HashMap<>();
            for (JSONObject item : tableFact.getItems()) {
//                        org.json.JSONObject jsonDatos = new org.json.JSONObject(item);
//                        JSONObject jsonArt = (JSONObject) parser.parse(item.get("articulo").toString());
//                        System.out.println("1) -> " + detalleArtList.get(val));
//                        if (!jsonDatos.isNull("gravada")) {
//                            long precio = jsonDatos.getLong("cantidad") * jsonDatos.getLong("precio");
//                            item.put("gravada", precio);
//                        } else if (!jsonDatos.isNull("exenta")) {
//                            long precio = jsonDatos.getLong("cantidad") * jsonDatos.getLong("precio");
//                            item.put("exenta", precio);
//                        }
                System.out.println("2) -> " + item.toString());
                try {
                    boolean v = Boolean.parseBoolean(MatrizFXMLController.mapeoCheck.get(item.get("orden").toString() + "-" + item.get("codigo")).toString());
                    tableFact.getSelectionModel().getSelectedItem().put("check", v);
                } catch (Exception e) {
                    tableFact.getSelectionModel().getSelectedItem().put("check", false);
                } finally {
                }

                MatrizFXMLController.detalleArtList.add(item);//si
                MatrizFXMLController.matrizList.add(item);//si
//                        MatrizFXMLController.hashJsonArtDet.put(Long.parseLong(jsonArt.get("idArticulo").toString()), FacturaDeVentaMayFXMLController.detalleArtList.lastIndexOf(item));//si

//                        FacturaDeVentaMayFXMLController.hashJsonArticulo.put(Long.parseLong(jsonArt.get("codArticulo").toString()), jsonArt);//si
                val++;
            }

            this.sc.loadScreen("/vista/stock/MatrizFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/ModCantidadReponerFXML.fxml", 232, 178, true);
        } else {
            mensajeAlerta("LA CANTIDAD EN CENTRAL, SAN LORENZO Y CACIQUE NO DEBE QUEDAR VACIO");
        }
    }

}
