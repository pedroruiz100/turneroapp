package com.javafx.controllers.stock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.javafx.controllers.caja.*;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloCompraMatriz;
import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.core.domain.Deposito;
import com.peluqueria.core.domain.Existencia;
import com.peluqueria.core.domain.FacturaCompraCab;
import com.peluqueria.core.domain.FacturaCompraDet;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.Recepcion;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.ArticuloCompraMatrizDAO;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import com.peluqueria.dao.DepositoDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.FacturaCompraCabDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoCompraDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoOrdenCompraDAO;
import com.peluqueria.dao.RecepcionDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.impl.ArticuloNf1TipoDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf3SseccionDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf7Secnom7DAOImpl;
import com.peluqueria.dao.impl.FacturaCompraDetDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.RangoOrdenCompraDAOImpl;
import com.peluqueria.dao.impl.RecepcionDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import static com.javafx.controllers.stock.MatrizFXMLController.categoriasList;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionParana;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.DatePicker;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.IntegerValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author
 */
@Controller
public class ReplicaFacturaCompraFXMLController extends BaseScreenController implements Initializable {

    public static void setTextFieldCompAnt(String aTextFieldCompAnt) {
        textFieldCompAnt = aTextFieldCompAnt;
    }

    public static boolean isActualizarDatosCabecera() {
        return actualizarDatosCabecera;
    }

    public static void setActualizarDatosCabecera(boolean actualizarDatosCabecera) {
        ReplicaFacturaCompraFXMLController.actualizarDatosCabecera = actualizarDatosCabecera;
    }

    public static List<JSONObject> getCotizacionList() {
        return cotizacionList;
    }

    @Autowired
    DepositoDAO depositoDAO;
    @Autowired
    ExistenciaDAO exisDAO;

    public static int getPeso() {
        return peso;
    }

    public static int getReal() {
        return real;
    }

    public static int getDolar() {
        return dolar;
    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static Long getPrecioTotal() {
        return precioTotal;
    }

    public static JSONObject getCabFactura() {
        return cabFactura;
    }

    public static void setCabFactura(JSONObject aCabFactura) {
        cabFactura = aCabFactura;
    }

    public static boolean isCancelacionProd() {
        return cancelacionProd;
    }

    private static JSONObject cabFactura;
//    private static Stage stageData = new Stage();

    static void setCliente(String ruc, String nombre) {
        nomCli = nombre;
        rucCli = ruc;
    }

//    public static void setSecondStage(Stage secondStage) {
//        stageData = secondStage;
//        StageSecond.setStageData(secondStage);
//    }
    Toaster toaster;
    boolean enterEstado = false;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static boolean dato = false;
    private static boolean facturaVentaEstado = false;
    public static JSONObject facturaCabeceraSupr = new JSONObject();
    public static boolean actualizarDatosCabecera;
    public static Map mapArticulosEnPromo;
    public static ArrayList arrayListadoPromo;
    public static ArrayList arrayListadoPromoConDesc;
    public static Map mapPromoConDesc;
    public static long descPromo;
    public static int cantPromo;
    public static ArticuloNf1TipoDAO anf1TipoDAO = new ArticuloNf1TipoDAOImpl();
    public static ArticuloNf2SfamiliaDAO anf2SfamiliaDAO = new ArticuloNf2SfamiliaDAOImpl();
    public static ArticuloNf3SseccionDAO anf3SeccionDAO = new ArticuloNf3SseccionDAOImpl();
    public static ArticuloNf4Seccion1DAO anf4SeccionDAO = new ArticuloNf4Seccion1DAOImpl();
    public static ArticuloNf5Seccion2DAO anf5SeccionDAO = new ArticuloNf5Seccion2DAOImpl();
    public static ArticuloNf6Secnom6DAO anf6SeccionDAO = new ArticuloNf6Secnom6DAOImpl();
    public static ArticuloNf7Secnom7DAO anf7SeccionDAO = new ArticuloNf7Secnom7DAOImpl();
    public static FacturaCompraDetDAO fcdDAO = new FacturaCompraDetDAOImpl();

    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";

    Alert alertCerrarTurno = null;
    Alert alertArqueo = null;

    static HashMap<Long, JSONObject> hmGift;

    public static HashMap<Long, JSONObject> getHmGift() {
        return hmGift;
    }

    public static void setHmGift(HashMap<Long, JSONObject> hmGift) {
        ReplicaFacturaCompraFXMLController.hmGift = hmGift;
    }

    public JSONObject objArticulo;

    @Autowired
    private ArticuloDAO artDAO;
    @Autowired
    private ExistenciaDAO existenciaDAO;
    @Autowired
    private PedidoCabDAO pedidoCabDAO;
    @Autowired
    private FacturaCompraDetDAO factCompraDetDAO;
    @Autowired
    private FacturaCompraCabDAO facturaCompraCabDAO;
    @Autowired
    private ArticuloCompraMatrizDAO artCompraMatrizDAO;
    @Autowired
    private PedidoDetDAO pedidoDetDAO;
    @Autowired
//    private PedidoDetDAO pedidoDetDAO;
    private PedidoCompraDAO pedidoCompraDAO;

    private List<JSONObject> depositoList = new ArrayList<>();

    private ObservableList<JSONObject> depositoData;

    static String edicion = "";

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoOrdenCompraDAO rangoOcDAO = new RangoOrdenCompraDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    private static RecepcionDAO recepcionDAO = new RecepcionDAOImpl();
    static JSONObject datos = new JSONObject();
    ManejoLocal manejoLocal = new ManejoLocal();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static String textFieldCompAnt;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static Long precioTotal;
    private static int peso;
    private static int real;
    private static int dolar;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    //primera inserción
    private boolean primeraInsercion;
    private static int orden;
    //primera inserción
    //lector de código
    private static String codBarra;
    private static String codDecimal;
    //lector de código
    private static NumberValidator numValidator;
    IntegerValidator intValidator;
    static String prec = "";
    static String tot = "";
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;
    JSONObject tipoCaja;
    public static List<JSONObject> detalleArtList;
    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    //TABLE VIEW
    private ReentrantLock lock = new ReentrantLock();
    Image image;
    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;
    private static int idFact;
    public static boolean valorIngreso = false;
    private SimpleDateFormat formatador = new SimpleDateFormat("hh:mm:ss a");

    static String rucCli;
    static String nomCli;

    final KeyCombination altN = new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN);
    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);
    final KeyCombination altE = new KeyCodeCombination(KeyCode.E, KeyCombination.ALT_DOWN);
    final KeyCombination altD = new KeyCodeCombination(KeyCode.D, KeyCombination.ALT_DOWN);

    private boolean patternMonto;
    String param;

    public static String codArt = "";
    boolean estadoCheck = false;

    public static String getCodArt() {
        return codArt;
    }

    public static void setCodArt(String codArt) {
        ReplicaFacturaCompraFXMLController.codArt = codArt;
    }

    boolean estadoValor = false;
    boolean val = false;
    boolean busquedaRecep = false;

    public static Map mapeoCheck = new HashMap();
    final KeyCombination altG = new KeyCodeCombination(KeyCode.G, KeyCombination.ALT_DOWN);
//    final KeyCombination altB = new KeyCodeCombination(KeyCode.B, KeyCombination.ALT_DOWN);

//    final KeyCombination altI = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN);
    @FXML
    private AnchorPane anchorPaneFactura;
    @FXML
    private Label labelTotal11;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtCodVendedor;
    @FXML
    private Pane secondPane1;
    @FXML
    private TextField txtCodVendedor1;
    @FXML
    private Label labelSupervisor1111111;
    @FXML
    private Label labelSupervisor11111111;
    @FXML
    private Label labelSupervisor111111111;
    @FXML
    private Label labelSupervisor11111112;
    @FXML
    private Label labelSupervisor111111121;
    @FXML
    private Label labelSupervisor1112211;
    @FXML
    private Label labelSupervisor11122111;
    @FXML
    private Label labelSupervisor111221111;
    @FXML
    private Label labelSupervisor1112211111;
    @FXML
    private Label labelSupervisor11122111111;
    @FXML
    private Label labelSupervisor111221111111;
    @FXML
    private Label labelSupervisor1112211111111;
    @FXML
    private Label labelSupervisor1112211111112;
    @FXML
    private Label labelSupervisor11122111111121;
    @FXML
    private Label labelSupervisor123;
    @FXML
    private Label labelSupervisor111111122;
    @FXML
    private Label labelSupervisor1111111221;
    @FXML
    private Label labelSupervisor11111112211;
    @FXML
    private Label labelSupervisor111111122111;
    @FXML
    private Label labelSupervisor1111111221111;
    @FXML
    private Label labelSupervisor11111112211111;
    @FXML
    private AnchorPane anchorPane3Sub11;
    @FXML
    private TextField txtClaseMov;
    @FXML
    private Label labelRazonTransportista;
    @FXML
    private Label labelSupervisor11111;
    @FXML
    private TextField txtObservacion;
    @FXML
    private Label labelSupervisor111111;
    @FXML
    private Label labelSupervisor;
    @FXML
    private ComboBox<String> chkTipoMovimiento;
    @FXML
    private TextField txtTipoMovimiento;
    @FXML
    private Label labelSupervisor1;
    @FXML
    private Button btnProveedor;
    @FXML
    private TextField txtProveedorDoc;
    @FXML
    private TextField txtProveedor;
    @FXML
    private Label labelSupervisor11;
    @FXML
    private Label labelSupervisor111;
    @FXML
    private ComboBox<String> chkClaseMov;
    @FXML
    private Label labelSupervisor12;
    @FXML
    private Label labelSupervisor121;
    @FXML
    private Label labelSupervisor122;
    @FXML
    private Label labelSupervisor1211;
    @FXML
    private Label labelSupervisor1112;
    @FXML
    private Label labelSupervisor11122;
    @FXML
    private Label labelSupervisor111221;
    @FXML
    private Label labelSupervisor11121;
    @FXML
    private Label labelSupervisor111211;
    @FXML
    private Label labelSupervisor1221;
    @FXML
    private Label labelSupervisor12211;
    @FXML
    private ComboBox<String> chkDepDestino;
    @FXML
    private ComboBox<String> chkTipo;
    @FXML
    private TextField txtCodigo;
    @FXML
    private ComboBox<String> chkMedida;
    @FXML
    private TextField txtContenido;
    @FXML
    private TextField txtNroPedido;
    @FXML
    private TextField txtCantidad;
    @FXML
    private TextField txtExistencia;
    @FXML
    private TextField txtDescuento;
    @FXML
    private TextField txtGasto;
    @FXML
    private TextField txtCosto;
    @FXML
    private TextField txtDescriIva;
    @FXML
    private TextField txtPorcIva;
    @FXML
    private TextField txtExenta;
    @FXML
    private TextField txtGravada;
    @FXML
    private TextField txtSec1;
    @FXML
    private Button btnSeccion;
    @FXML
    private TextField txtSec2;
    @FXML
    private TextField txtDescripcion;
    @FXML
    private Button btnNose;
    @FXML
    private Button btnCargarVtoProd;
    @FXML
    private Button btnVerificarPrecio;
    @FXML
    private Button btnGuardarDetalle;
    @FXML
    private Button btnBorrarDetalle;
    @FXML
    private Button btnBuscarOrdenCompra;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnBorrar;
    @FXML
    private Button btnNuevo;
    @FXML
    private TextField txtMonExtranjeraExenta;
    @FXML
    private TextField txtGsExenta;
    @FXML
    private TextField txtMonExtranjeraGrav5;
    @FXML
    private TextField txtGsGrav5;
    @FXML
    private TextField txtMonExtranjeraGrav10;
    @FXML
    private TextField txtGsGrav10;
    @FXML
    private TextField txtMonExtranjeraIva5;
    @FXML
    private TextField txtGsIva5;
    @FXML
    private TextField txtMonExtranjeraIva10;
    @FXML
    private TextField txtGsIva10;
    @FXML
    private TextField txtMonExtranjeraTotal;
    @FXML
    private TextField txtGsTotal;
    @FXML
    private ComboBox<String> chkSituacion;
    @FXML
    private TextField txtNroTimbrado;
    @FXML
    private TextField txtRucProveedor;
    @FXML
    private TextField txtNroEntrada;
    @FXML
    private TextField txtNroFactura;
    @FXML
    private TextField txtFechaFactura;
    @FXML
    private TextField txtVtoDias;
    @FXML
    private TextField txtFechaRecepcion;
    @FXML
    private ChoiceBox<String> chkEmpresa;
    @FXML
    private CheckBox chkOtros;
    @FXML
    private TextField txtNroDespacho;
    @FXML
    private TextField txtNroPedidoSis;
    @FXML
    private ChoiceBox<String> chkMonedaExtran;
    @FXML
    private Button btnCargar;
    @FXML
    private TextField txtMonedaExtran;
    @FXML
    private TextField txtEntregaInicial;
    @FXML
    private TextField txtCantCuota;
    @FXML
    private TextField txtCuota;
    @FXML
    private Button btnGenerar;
    @FXML
    private Button btnSalir;
    @FXML
    private TableView<JSONObject> tableViewFactura;
    @FXML
    private TableColumn<JSONObject, String> columnOrden;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnIva;
    @FXML
    private TableColumn<JSONObject, String> columnExenta;
    @FXML
    private TableColumn<JSONObject, String> columnGravada;
    @FXML
    private TableColumn<JSONObject, String> columnPeso;
    @FXML
    private TableColumn<JSONObject, String> columnTipo;
    @FXML
    private TableColumn<JSONObject, String> columnCantidad;
    @FXML
    private TableColumn<JSONObject, String> columnMedida;
    @FXML
    private TableColumn<JSONObject, String> columnContenido;
    @FXML
    private TableColumn<JSONObject, String> columnDescuento;
    @FXML
    private TableColumn<JSONObject, String> columnCosto;
    @FXML
    private TableColumn<JSONObject, String> columnDeposito;
    @FXML
    private Label labelSupervisor1111111111;
    @FXML
    private TextField txtProveedorDoc1;
    @FXML
    private TextField txtProveedorDoc11;
    @FXML
    private TableView<JSONObject> tableViewDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDeposito;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantDepo;
    @FXML
    private Label labelSupervisor124;
    @FXML
    private TextField txtOC;
    @FXML
    private TableColumn<JSONObject, String> columnTotal;
    @FXML
    private TableColumn<JSONObject, Boolean> columnOpc;
    @FXML
    private CheckBox checkListado;
    @FXML
    private AnchorPane panelBusquedaCompra;
    @FXML
    private Label labelClienteBuscar;
    @FXML
    private TableView<JSONObject> tableViewCompra;
    @FXML
    private TableColumn<JSONObject, Boolean> tableColumnOpc;
    @FXML
    private TableColumn<JSONObject, String> tableColumnOrden;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCodigo;
    @FXML
    private TableColumn<JSONObject, String> tableColumnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCantidad;
    @FXML
    private TableColumn<JSONObject, String> tableColumnCosto;
    @FXML
    private TableColumn<JSONObject, String> tableColumnTotal;
    @FXML
    private TextField txtNomProveedor;
    @FXML
    private TextField txtNumOrden;
    @FXML
    private TextField txtProveedor1;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private Button btnProveedor1;
    @FXML
    private TextField txtRucProveedor1;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button buttonVolver;
    @FXML
    private Pane paneImage121;
    @FXML
    private Label labelCajeroFunc211;
    @FXML
    private Button buttonBuscar;
    @FXML
    private TextField txtOrdenCompra;
    @FXML
    private Label labelNombreClienteNuevo11;
    @FXML
    private CheckBox chkCentral;
    @FXML
    private CheckBox chkCacique;
    @FXML
    private CheckBox chkSanLorenzo;
    @FXML
    private Pane paneImage1211;
    @FXML
    private Label labelCajeroFunc2111;
    @FXML
    private Button buttonBuscarArt;
    @FXML
    private Label labelNombreClienteNuevo111;
    @FXML
    private TextField txtCod;
    @FXML
    private TableView<?> tableViewCompra2;
    @FXML
    private TableColumn<?, ?> tableColumnOpc2;
    @FXML
    private TableColumn<?, ?> tableColumnOrden2;
    @FXML
    private TableColumn<?, ?> tableColumnCodigo2;
    @FXML
    private TableColumn<?, ?> tableColumnDescripcion2;
    @FXML
    private TableColumn<?, ?> tableColumnCantidad2;
    @FXML
    private TableColumn<?, ?> tableColumnCosto2;
    @FXML
    private TableColumn<?, ?> tableColumnTotal2;
    @FXML
    private TableColumn<JSONObject, String> columnDiferencia;
    @FXML
    private TextField txtProveedorDoc111;
    @FXML
    private TextField txtGsExentaManual;
    @FXML
    private TextField txtGsGrav5Manual;
    @FXML
    private TextField txtGsGrav10Manual;
    @FXML
    private TextField txtGsIva5Manual;
    @FXML
    private TextField txtGsIva10Manual;
    @FXML
    private TextField txtGsTotalManual;
    @FXML
    private CheckBox chkTodo;
    @FXML
    private DatePicker dpFecha;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    @SuppressWarnings("ConvertToStringSwitch")
    public void initialize(URL url, ResourceBundle rb) {
        toaster = new Toaster();
        btnVerificarPrecio.setDisable(true);
        detalleArtList = new ArrayList<>();
        depositoList = new ArrayList<>();
        orden = 0;
        anchorPane3Sub11.setVisible(true);
        secondPane1.setVisible(true);
        panelBusquedaCompra.setVisible(false);
        ubicandoContenedorSecundario();
        cargarTipoMovimiento();
        cargarDeposito();
        cargarTipo();
        cargarMedida();
        cargarSucursal();
        cargarSituacion();
        cargarClaseMovimiento();
        repeatFocus(txtNroFactura);
        checkListado.setSelected(true);
//            } catch (ParseException ex) {
        mapeoCheck = new HashMap();
        detalleArtList = new ArrayList<>();
        tableViewFactura.getItems().clear();
//        orden = 0;
//                Utilidades.log.error("ParseException", ex.fillInStackTrace());
//        } catch (IOException ex) {
//            Utilidades.log.error("IOException", ex.fillInStackTrace());
//        }
//        }
        tableViewFactura.setEditable(true);
        columnCantidad.setCellFactory(TextFieldTableCell.forTableColumn());
        columnCosto.setCellFactory(TextFieldTableCell.forTableColumn());
        columnTotal.setCellFactory(TextFieldTableCell.forTableColumn());
//        lastNameColumn.setCellFactory(TextFieldTableCell.forTableColumn());a
        cargandoImagen();
        intValidator = new IntegerValidator();
        numValidator = new NumberValidator();
        listenTextField(txtGsIva5Manual);
        listenTextField(txtGsIva10Manual);
        listenTextField(txtGsTotalManual);
        listenTextField(txtGsExentaManual);
        chkTodo.setSelected(true);
//        tableViewFactura.getColumns().addListener(
//                new ListChangeListener() {
//
//            @Override
//            public void onChanged(Change change) {
//
//                change.next();
//
//                System.out.println("old list");
//                System.out.println(change.getRemoved());
//
//                System.out.println("new list");
//                System.out.println(change.getList());
//            }
//
//        });
//        tableViewFactura.setRowFactory(param
//                -> {
//            TableRow<JSONObject> row = new TableRow<>();
//
//            row.disableProperty().addListener((observable, oldValue, newValue)
//                    -> {
//                if (newValue) {
//                    row.setStyle("-fx-background-color: red");
//                } else {
//                    row.setStyle("");
//                }
//            });
//
////            row.disableProperty().bind(true);
//
//            return row;
//        });
//        tableViewFactura.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//            if (newSelection != null) {
//                tableview2.getSelectionModel().clearSelection();
//            }
//        });
        LocalDate now = LocalDate.now();
        dpFecha.setValue(now);
    }

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    private void cargarTipoMovimiento() {
        chkTipoMovimiento.getItems().addAll("CR", "CO");
    }

    private void cargarDeposito() {
//        chkDepDestino.getItems().addAll("CC", "SL", "SC");
        for (Deposito dep : depositoDAO.listar()) {
            chkDepDestino.getItems().add(dep.getDescripcion());
//            chkDestino.getItems().add(dep.getDescripcion());
        }
    }

    private void cargarTipo() {
        chkTipo.getItems().addAll("MER", "SER", "DESC");
    }

    private void cargarMedida() {
        chkMedida.getItems().addAll("UNIDAD", "KG");
    }

    private void cargarSucursal() {
        chkEmpresa.getItems().addAll("CASA CENTRAL", "SAN LORENZO", "CACIQUE");
    }

    private void cargarSituacion() {
        chkSituacion.getItems().addAll("CONTRIBUYENTE", "OTROS");
    }

    private void cargarClaseMovimiento() {
        chkClaseMov.getItems().addAll("MER", "SER", "DESC");
    }

    @FXML
    private void btnCerrarAction(ActionEvent event) {
//        cerrandoCabecera();
        buscandoOC();
    }

    private void btnRetiroDineroAction(ActionEvent event) {
        verificarMontoFacturado();
    }

    private void btnCerrarTurnoAction(ActionEvent event) {
        if (!verificandoCaidaFormaPago()) {
            resetTray();
            verificandoDatosCierre();
        }
    }

    @FXML
    private void anchorPaneFacturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void anchorPaneFacturaKeyPressed(KeyEvent event) {

    }

    private void textFieldCodKeyReleased(KeyEvent event) {
        keyPressTextCod(event);
    }

    private void cargandoInicial() throws ParseException, IOException {
//        mapArticulosEnPromo = new HashMap();
//        arrayListadoPromo = new ArrayList();
//        arrayListadoPromoConDesc = new ArrayList();
//        mapPromoConDesc = new HashMap();
//        descPromo = 0;
//        cantPromo = 0;
//
//        hmGift = new HashMap<>();
//
//        if (StageSecond.getStageData()
//                .isShowing()) {
//            StageSecond.getStageData().close();
//        }
//
//        if (ScreensContoller.getFxml()
//                .contentEquals("/vista/caja/FormasDePagoFXML.fxml")) {
//            toaster.mensajeDiario();
////            toaster.mensajeFactCierreDet(MensajeFinalVenta.getVueltoPopUp(), MensajeFinalVenta.getClientePopUp(), 5);
//            toaster.mensajeDeNavidadAnhoNuevo();
//        }
//        objArticulo = new JSONObject();
//        facturaVentaEstado = false;
//
//        setActualizarDatosCabecera(
//                false);
//        cargandoImagen();
//        facturaCabeceraSupr = new JSONObject();
//        numValidator = new NumberValidator();
//        boolean estado = false;
//
//        if (DatosEnCaja.getDatos()
//                != null) {
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = new JSONObject();
//            if (DatosEnCaja.getFacturados() == null) {
//                fact = new JSONObject();
//            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
//                fact = new JSONObject();
//            } else {
//                fact = DatosEnCaja.getFacturados();
//                estado = true;
//            }
//            //SETEAR CAMPOS PRIMERAMENTE
//            //**--VERIFICANDO--**
//            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
//            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
//            tipoCaja = (JSONObject) caj.get("tipoCaja");
//            txtNumCaja.setText("N°Caja: " + caj.get("descripcion").toString());
//            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
//            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
//            if (jsonFuncionario != null) {
//                String nomFuncionario = "";
//                String apeFuncionario = "";
//                if (jsonFuncionario.get("nombre") != null) {
//                    nomFuncionario = jsonFuncionario.get("nombre").toString();
//                }
//                if (jsonFuncionario.get("apellido") != null) {
//                    apeFuncionario = jsonFuncionario.get("apellido").toString();
//                }
//                labelCajeroFunc.setText("Cajero: " + nomFuncionario);
//                labelCajeroFunc2.setText(apeFuncionario);
//            } else {
//                labelCajeroFunc.setText("Cajero: N/A");
//            }
////            iniciandoCotizacion();
//            //FIN DEL SETEO DE CAMPOS
//            cargandoDatosIniciales();
//            if (estado) {
//                cargandoDetalleManeraLocal();
//            }
//        }
//        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//
//        if (jsonDatos.isNull(
//                "rendicion")) {
//            datos.put("rendicion", false);
//        }
////        try {
////            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
////            txtNumRuc.setText(empresa.get("ruc").toString());
////        } catch (ParseException ex) {
////            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
////        }
//
//        if (!hashJsonArticulo.isEmpty()) {
//            if (!jsonDatos.isNull("exentaGlobal")) {
//                chkExtranjero.setSelected(true);
//            }
//            chkExtranjero.setDisable(true);
//        } else {
//            chkExtranjero.setDisable(false);
//        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    static void iniciandoFactCab() {
        cabFactura = new JSONObject();
        cabFactura = creandoJsonFactCab();
        cancelacionProd = false;
        cancelacionProdPrimera = true;
    }

    private void listenFactura() {
        tableViewFactura.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                }
            }
        });

//        textFieldCant.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!newValue.contentEquals("")) {
//                try {
//                    if (GenericValidator.isDouble(newValue)) {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(newValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(oldValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    }
//                } catch (NumberFormatException e) {
//                    Platform.runLater(() -> {
//                        textFieldCant.setText(oldValue);
//                        textFieldCant.positionCaret(textFieldCant.getLength());
//                        if (!textFieldCant.getText().endsWith(".")) {
//                            if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                textFieldCant.setText("1");
//                                mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                            }
//                        }
//                    });
//                }
//            }
//        });
    }

    private void mensajeAlerta(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorArt(String msj) {
        toaster.mensajeGenericoError("Mensaje del Sistema", msj, true, 2);
    }

    private void cargandoDetalleManeraLocal() {
        try {
            resetMapeo();
            detalleArtList = new ArrayList<>();
            JSONParser parser = new JSONParser();
            JSONArray facturaDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            JSONObject facturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            cabFactura = new JSONObject();
            org.json.JSONObject jsonFact = new org.json.JSONObject(facturaCabecera);
            try {
                if (tableViewFactura.getItems().isEmpty()) {
                    if (!jsonFact.isNull("montoFactura")) {
                        facturaCabecera.remove("montoFactura");
                        fact.put("facturaClienteCab", facturaCabecera);
                    } else {
                        cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                    }
                } else {
                    cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                }
                valorIngreso = true;
            } catch (Exception e) {
                mensajeError2("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO");
                valorIngreso = false;
            }
            if (valorIngreso) {
                cabFactura.put("estadoFactura", facturaCabecera.get("estadoFactura").toString());
                cabFactura.put("nroActual", facturaCabecera.get("nroActual").toString());
                cabFactura.put("idFacturaClienteCab", facturaCabecera.get("idFacturaClienteCab").toString());
                cabFactura.put("nroFactura", facturaCabecera.get("nroFactura").toString());
                primeraInsercion = false;
                for (int i = 0; i < facturaDetalle.size(); i++) {
                    JSONObject detalleArticulo = (JSONObject) parser.parse(facturaDetalle.get(i).toString());
                    JSONObject articulo = (JSONObject) parser.parse(detalleArticulo.get("articulo").toString());
                    hashJsonArticulo.put((Long.parseLong(articulo.get("codArticulo").toString())), articulo);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long.parseLong(articulo.get("idArticulo").toString())), detalleArtList.lastIndexOf(detalleArticulo));
                    vistaJSONObjectArtDet();
                    cargandoCamposInterfaceLocal(detalleArticulo);
                    orden++;
                }
                CajaDeDatos.generandoNroComprobante();
                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                // primer trío
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                long nroActual = talos.getNroActual();
                JSONObject jsonFacturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

                File file = new File(PATH.PATH_NO_IMG);
                Image image = new Image(file.toURI().toString());
//                centerImage();
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (!detalleArtList.isEmpty()) {
            Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
        }
    }

    private static void resetMapeo() {
        hashJsonArticulo = new HashMap<>();
        hashJsonArtDet = new HashMap<>();
    }

    private void mensajeError2(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    public void vistaJSONObjectArtDet() {
        numValidator = new NumberValidator();
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        columnOpc.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
                return new BooleanCell(tableViewFactura);
            }
        };

        columnOpc.setCellValueFactory((data) -> {
            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
            try {
                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getBigInteger("codArticulo")).toString())) {
                    return new SimpleBooleanProperty(true);
                } else {
                    return new SimpleBooleanProperty(false);
                }
            } catch (Exception e) {
                return new SimpleBooleanProperty(false);
            } finally {
            }
        });
        columnOpc.setCellFactory(booleanCellFactory);
        //columna Sección..............................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
//        columnOpc.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, Boolean>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, Boolean> t) {
//                int num = t.getTablePosition().getRow();
//                System.out.println("ROW -> " + num);
//                System.out.println("-->> " + t.getTableView().getItems().get(num));
//                System.out.println("ESTO NOS DA --->>>> " + t.getNewValue());
//            }
//        });

        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("tipo").toString());
            }
        });
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codArticulo").toString());
            }
        });
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCantidad.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> (t.getTableView().getItems().get(
//                        t.getTablePosition().getRow())).put("cantidad", t.getNewValue())
//        );
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", t.getNewValue())
//        );
        columnCantidad.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
                edicion = "cantidad";
                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("cantidad", numValidator.numberValidator(t.getNewValue()));
                getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", numValidator.numberValidator(t.getNewValue()));
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnMedida.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnTotal.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                long prec = (Long.parseLong(data.getValue().get("precio").toString()) * Long.parseLong(data.getValue().get("cantidad").toString()));
                tot = prec + "";
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(prec + "")));
            }
        });
        columnTotal.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
                edicion = "total";
                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("total", numValidator.numberValidator(t.getNewValue()));
//(t.getTableView().getItems().get(t.getTablePosition().getRow())).put("precio", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(t.getNewValue()))));
                getDetalleArtList().get(t.getTablePosition().getRow()).put("total", numValidator.numberValidator(t.getNewValue()));
                if (!numValidator.numberValidator(tot).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
                    long precio = Long.parseLong(numValidator.numberValidator(t.getNewValue())) / Long.parseLong(getDetalleArtList().get(t.getTablePosition().getRow()).get("cantidad").toString());
                    VerificarPrecioFXMLController.setCostoLocal(precio + "");
                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                }
                recargarDatos();
//                ReplicaFacturaCompraFXMLController.setCodArt(getDetalleArtList().get(t.getTablePosition().getRow()).get("codArticulo").toString());
//                sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            }
        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
        columnPeso.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnDiferencia.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold; -fx-font-color: red;");
        columnDiferencia.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("diferencia").toString());
            }
        });
        columnContenido.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("contenido").toString());
            }
        });
        columnDescuento.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descuento").toString());
            }
        });
        columnCosto.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                prec = data.getValue().get("precio").toString();
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("precio").toString()))));
//                return new SimpleStringProperty(data.getValue().get("precio").toString());
            }
        });
        columnCosto.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
            @Override
            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
                edicion = "precio";
                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("precio", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(t.getNewValue()))));
                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("precio", numValidator.numberValidator(t.getNewValue()));
                getDetalleArtList().get(t.getTablePosition().getRow()).put("precio", numValidator.numberValidator(t.getNewValue()));
                if (!numValidator.numberValidator(prec).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
                    VerificarPrecioFXMLController.setCostoLocal(numValidator.numberValidator(t.getNewValue()));
                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                }
                recargarDatos();
            }
        });
        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
                    return new SimpleStringProperty("0");
                } else {
                    return new SimpleStringProperty(data.getValue().get("poriva").toString());
                }
            }
        });
        columnExenta.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("iva").toString()))));
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf("0"))));
//                    return new SimpleStringProperty("0");
                }
            }
        });
        columnGravada.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                if (Long.parseLong(data.getValue().get("poriva").toString()) != 0) {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("iva").toString())));
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
                } else {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//                    return new SimpleStringProperty("0");
                }
            }
        });
        columnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("deposito").toString());
            }
        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewFactura.setItems(articuloDetData);
        //**********************************************************************
    }

    private void cargandoCamposInterfaceLocal(JSONObject detalleArticulo) {

    }

    private void cerrandoCabecera() {
//        if (txtTipoMovimiento.getText().equals("")
//                || txtProveedor.getText().equals("")
//                || txtRucProveedor.getText().equals("")
//                //                || txtNroEntrada.getText().equals("")
//                || txtClaseMov.getText().equals("")
//                || txtNroFactura.getText().equals("")) {
//            mensajeAlerta("EXISTEN CAMPOS QUE REQUIEREN CARGARSE");
//        } else {
////            if (txtOC.getText().equals("")) {
////                txtOC.setText(rangoOcDAO.recuperarActual());
////            }
//            secondPane1.setVisible(true);
//            txtCodigo.requestFocus();
//            if (!txtOC.getText().equals("")) {
//                detalleArtList = new ArrayList<>();
//                tableViewFactura.getItems().clear();
//                orden = 0;
//                cargarDetalleByOC(txtOC.getText());
//            }
//        }
//        if (!detalleArtList.isEmpty()) {
//            pagando();
//        } else {
//            mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
//        }
    }

    private void pagando() {
//        if ("factura_cerrar")) {
            org.json.JSONObject json = new org.json.JSONObject(fact);
            if (!json.isNull("facturaClienteCab")) {
                try {
                    JSONObject factu = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    setCabFactura(factu);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }

            this.sc.loadScreenModal("/vista/caja/FormasDePagoFXML.fxml", 667, 501, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void mensajeError(String msj) {
//        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorAlertModal(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.ERROR, msj, ok);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
        }
    }

    private void verificandoDatosCierre() {

    }
//    private HashMap () {
//        HashMap valor = new HashMap();
//        valor.put("cajero", "nverificandoDatosCierreull");
//        int items = tableViewFactura.getItems().size();
//        if (items > 0) {
//            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
//        } else {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            alertCerrarTurno = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CERRAR TURNO COMO CAJERO?", ok, cancel);
//            alertCerrarTurno.showAndWait();
//            LoginFXMLController.setLlamarTask(false);
//            if (alertCerrarTurno.getResult() == ok) {
//                ButtonType okData = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                ButtonType cancelData = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                alertArqueo = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA REALIZAR EL ARQUEO?", okData, cancelData);
//                alertArqueo.showAndWait();
//                if (alertArqueo.getResult() == okData) {
//                    users = null;
//                    fact = null;
//                    DatosEnCaja.setUsers(null);
//                    actualizarDatos();
//                    LoginFXMLController.setLlamarTask(false);
//                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                    valor.put("cajero", "cambio");
//                    alertArqueo.close();
//                }
//            } else if (alertCerrarTurno.getResult() == cancel) {
//                alertCerrarTurno.close();
//            }
//            alertArqueo = null;
//            alertCerrarTurno = null;
//        }
//        return valor;
//    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void cancelarFactura() {

    }

    private void verificarMontoFacturado() {
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE NO HA SIDO CERRADA!");
        } else {
            if (datos.containsKey("montoFacturado")) {
                int montoFacturado = Integer.parseInt(datos.get("montoFacturado").toString());
                if (montoFacturado > 0) {
                    retirandoDinero();
                } else {
                    mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
                }
            } else {
                mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
            }
        }
    }

    private void retirandoDinero() {
//        if ("retiro_dinero")) {

            this.sc.loadScreenModal("/vista/caja/retiroDineroFXML.fxml", 529, 266, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            if (panelBusquedaCompra.isVisible()) {
                anchorPane3Sub11.setDisable(false);
                secondPane1.setDisable(false);
                panelBusquedaCompra.setVisible(false);
                mantenerSeleccionadosOC();
                tableViewFactura.getItems().clear();
                vistaJSONObjectArtDet();
                repeatFocus(txtOC);
            } else {
                if (!txtNroFactura.getText().equals("")) {
                    guardarCompra();
                } else {
                    mensajeAlerta("DEBE SELECCIONAR EL NUMERO DE FACTURA");
                }
            }
//            org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//            boolean valor = false;
//            if (!jsonDatos.isNull("ultimaFactura")) {
//
//            }
//            if (valor) {
//                mensajeDetalle("Se ha detectado una incidencia, contáctese con el área de IT.", "Error 400");
//            } else {
////                ClienteFielFXMLController.setJsonClienteFiel(null);
//                if (!verificandoCaidaFormaPago()) {
//                    if (!detalleArtList.isEmpty()) {
////                        VERIFICAR PROMO MAS COMPRAS MAS  AHORRAS
//                        cargarArticulosOrdenar();
////                        FIN VERIFICAR PROMO MAS COMPRAS MAS AHORRAS //NUEVO
//                        org.json.JSONObject json = new org.json.JSONObject(datos);
//                        boolean formaPago = false;
//                        if (!json.isNull("caida")) {
//                            String caida = datos.get("caida").toString();
//                            if (caida.equalsIgnoreCase("factura_venta")) {
//                                formaPago = false;
//                            } else {
//                                formaPago = true;
//                            }
//                        }
//                        if (formaPago) {
//                            try {
//                                JSONArray detalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
//                                JSONObject cabecera = new JSONObject();
//                                if (detalle.size() > 0) {
//                                    JSONObject objDetalle = (JSONObject) parser.parse(detalle.get(0).toString());
//                                    cabecera = (JSONObject) parser.parse(objDetalle.get("facturaClienteCab").toString());
//                                } else {
//                                    cabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                                }
//                                cabFactura = cabecera;
//                                new MensajeFinalVenta().cargandoInicial();
////                                this.sc.loadScreen("/vista/caja/mensajeFinalVentaFXML.fxml", 562, 288, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                            } catch (ParseException ex) {
//                                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                            }
//                        } else {
//                            if (DatosEnCaja.getDatos() != null) {
//                                datos = DatosEnCaja.getDatos();
//                            }
//                            if (DatosEnCaja.getFacturados() != null) {
//                                fact = DatosEnCaja.getFacturados();
//                            }
//                            if (!facturaCabeceraSupr.toString().equalsIgnoreCase("{}")) {
//                                cabFactura = facturaCabeceraSupr;
//                                fact.put("facturaClienteCab", facturaCabeceraSupr);
//                                setActualizarDatosCabecera(true);
//                                ReplicaFacturaCompraFXMLController.cancelacionProd = true;
//                            }
//                            //verificar que ingrese en forma de pago en el metodo PUT ya que cuando de cancela una factura va al POST de vuelta
//                            //entonces impide que se actualice la CABECERA
//                            pagando();
//                            actualizandoCabFacturaLocalmente();
//                        }
//                        //FIN NUEVO
//                    } else {
//                        mensajeError("DEBE DISPONER COMO MÍNIMO UN DETALLE FACTURA.");
//                    }
//                }
//            }
        }
        if (altG.match(event)) {
            mantenerSeleccionados();
        }

        if (altE.match(event)) {
            estadoCheck = true;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), true);
            }
            if (!panelBusquedaCompra.isDisabled()) {
                vistaJSONObjectArtDetOC();
            } else {
                vistaJSONObjectArtDet();
            }
        }
        if (altD.match(event)) {
            estadoCheck = true;
//            long total = 0;
            for (JSONObject json : getDetalleArtList()) {
                org.json.JSONObject jsonDat = new org.json.JSONObject(json);
//                Boolean.parseBoolean(mapeoCheck.get(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo")).toString());
                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), false);
//                total += Math.round(Long.parseLong(json.get("precio").toString()) * Double.parseDouble(json.get("cantidad").toString()));
            }
            if (!panelBusquedaCompra.isDisabled()) {
                vistaJSONObjectArtDetOC();
            } else {
                vistaJSONObjectArtDet();
            }
//            ubicandoContenedorSecundario();
//            chkPresupuesto.setSelected(true);
//            precioTotal = total;
//            resetFormulario(total);
        }
        if (keyCode == event.getCode().F4) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificarMontoFacturado();
                    actualizarDatos();
                }
            }
        }
        if (keyCode == event.getCode().F1) {
            if (panelBusquedaCompra.isVisible()) {
                buscarArt();
            } else {
                buscandoOC();
            }
        }
        if (keyCode == event.getCode().F12) {
            abrirBusquedaOC();
        }
        if (keyCode == event.getCode().F7) {
//            cerrandoCabecera();
            buscandoOC();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (panelBusquedaCompra.isVisible()) {
                anchorPane3Sub11.setDisable(false);
                secondPane1.setDisable(false);
                panelBusquedaCompra.setVisible(false);
                repeatFocus(tableViewCompra);
            } else {
                this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewFactura.requestFocus();
        }
        if (keyCode == event.getCode().F8) {
            verificarPrecio();
        }
        if (keyCode == event.getCode().F5) {
            guardarDetalle();
        }
        if (keyCode == event.getCode().F3) {
            if (alert) {
                alert = false;
            } else if (detalleArtList.isEmpty()) {
                mensajeAlerta("NO DISPONE DE ARTÍCULO ALGUNO PARA CANCELAR FACTURA.");
            } else {

                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                cancelarFactura();
                actualizarDatos();
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (!verificandoCaidaFormaPago()) {
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    mensajeError("NO SE PUEDE ELIMINAR LOS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                } else if (alert) {
                    alert = false;
                } else if (detalleArtList.isEmpty()) {
                    mensajeAlerta("DEBE DISPONER COMO MÍNIMO UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                } else {
                    JSONObject productos = tableViewFactura.getSelectionModel().getSelectedItem();
                    if (productos == null) {
                        mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                    } else {
//                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA ELIMINAR EL ARTÍCULO " + productos.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
//                        alert1.showAndWait();
//                        if (alert1.getResult() == ok) {
                        try {
                            facturaCabeceraSupr = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                            ReplicaFacturaCompraFXMLController.setCabFactura(facturaCabeceraSupr);
//                                textFieldDescripcion.setText("");
                            this.alert = false;
                            Label labelCantidad = new Label();

                            this.sc.loadScreenModal("/vista/caja/CancelacionProductoFXML.fxml", 519, 275, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
//                        } else if (alert1.getResult() == cancel) {
//                            alert1.close();
//                        }
                        actualizarDatos();
                    }
                }
            }
        }
//        if (keyCode == event.getCode().F5) {
//            if (alert) {
//                alert = false;
//            } else {
//                if (!verificandoCaidaFormaPago()) {
//                    resetTray();
//                    verificandoDatosCierre();
////                    String cajero = verificandoDatosCierre().get("cajero").toString();
////                    if (cajero.equalsIgnoreCase("arqueo")) {
////                        users = null;
////                        fact = null;
////                        DatosEnCaja.setUsers(null);
////                        datos.put("modSup", true);
////                        DatosEnCaja.setDatos(datos);
////                        actualizarDatosNuevo();
////                    }
//                }
//            }
//        }
        if (altN.match(event)) {
            registrandoCliente();
        }
        if (altC.match(event)) {
            buscandoCliente();
        }
//        if (keyCode == event.getCode().F10) {
//            mensajeDialog();
//        }
        if (event.getCode().isDigitKey() || event.getCode().getName().contentEquals("Enter")) {
//            if (alert) {
//                alert = false;
//            } else {
//                lecturaCodBarra(event);
//            }
        }
    }

//    private void cargarArticulosOrdenar() {
//        mapArticulosEnPromo = new HashMap();
//        try {
////            int valor = 0;
//            cantPromo = 0;
//            int orden = 0;
//            for (JSONObject jsonDetalle : detalleArtList) {
//                orden++;
//                long cantidad = Long.parseLong(String.valueOf(jsonDetalle.get("cantidad").toString()).replace(".0", ""));
//                JSONObject jsonArt = (JSONObject) parser.parse(jsonDetalle.get("articulo").toString());
//                if (LoginCajeroFXMLController.articuloPromo.containsKey(jsonArt.get("codArticulo"))) {
//                    mapArticulosEnPromo.put(jsonArt.get("codArticulo") + "-" + cantidad + "-" + orden, Long.parseLong(String.valueOf(jsonDetalle.get("precio").toString())));
////                    valor++;
//                    cantPromo += cantidad;
//                }
//            }
//            if (cantPromo >= 4) {
//                ordenarMapeo();
//
//            }
//        } catch (ParseException ex) {
//            Logger.getLogger(ReplicaFacturaCompraFXMLController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
//    }
//    private void ordenarMapeo() {
//        Set<Map.Entry<String, Long>> set = mapArticulosEnPromo.entrySet();
//        List<Map.Entry<String, Long>> list = new ArrayList<Map.Entry<String, Long>>(
//                set);
//        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
//            public int compare(Map.Entry<String, Long> o2,
//                    Map.Entry<String, Long> o1) {
//                return o2.getValue().compareTo(o1.getValue());
//            }
//        });
//
//        int orden = 0;
//        arrayListadoPromo = new ArrayList();
//        for (Map.Entry<String, Long> entry : list) {
//
//            StringTokenizer st = new StringTokenizer(entry.getKey(), "-");
//            String cod = st.nextElement().toString();
//            String cant = st.nextElement().toString();
//            for (int i = 0; i < Integer.parseInt(cant); i++) {
//                orden++;
//                arrayListadoPromo.add(orden + "-" + cod + "-" + entry.getValue());
//            }
//        }
//
////        for (int i = 0; i < arrayListadoPromo.size(); i++) {
////            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
////            String ord = st.nextElement().toString();
////            String cod = st.nextElement().toString();
////            String precio = st.nextElement().toString();
////
////            System.out.println(ord + ") -->> " + cod + " - " + precio);
////        }
//        System.out.println("------------------SIGUIENTE--------------------");
//        int vueltaEnCiclo = 0;
//        int cantVueltaEnCiclo = 1;
//
//        double cantDesc = orden / 4;
//        String str = String.valueOf(cantDesc);
//        int cantVueltaTotalDesc = Integer.parseInt(str.substring(0, str.indexOf('.')));
//        descPromo = 0;
//        arrayListadoPromoConDesc = new ArrayList();
//        mapPromoConDesc = new HashMap();
//
//        for (int i = 0; i < arrayListadoPromo.size(); i++) {
//            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
//            String ord = st.nextElement().toString();
//            String cod = st.nextElement().toString();
//            String precio = st.nextElement().toString();
//
//            vueltaEnCiclo++;
//
//            if (cantVueltaTotalDesc >= cantVueltaEnCiclo) {
//                switch (vueltaEnCiclo) {
//                    case 1:
////                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 50%");
//                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.5) + "-50");
//                        if (mapPromoConDesc.containsKey(cod)) {
//                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
//                            mapPromoConDesc.put(cod, (valor + 1));
//                        } else {
//                            mapPromoConDesc.put(cod, 1);
//                        }
//                        descPromo += (Long.parseLong(precio) * 0.5);
//                        break;
//                    case 2:
////                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 40%");
//                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.4) + "-40");
//                        if (mapPromoConDesc.containsKey(cod)) {
//                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
//                            mapPromoConDesc.put(cod, (valor + 1));
//                        } else {
//                            mapPromoConDesc.put(cod, 1);
//                        }
//                        descPromo += (Long.parseLong(precio) * 0.4);
//                        break;
//                    case 3:
////                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 30%");
//                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.3) + "-30");
//                        if (mapPromoConDesc.containsKey(cod)) {
//                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
//                            mapPromoConDesc.put(cod, (valor + 1));
//                        } else {
//                            mapPromoConDesc.put(cod, 1);
//                        }
//                        descPromo += (Long.parseLong(precio) * 0.3);
//                        break;
//                    case 4:
////                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 20%");
//                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.2) + "-20");
//                        if (mapPromoConDesc.containsKey(cod)) {
//                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
//                            mapPromoConDesc.put(cod, (valor + 1));
//                        } else {
//                            mapPromoConDesc.put(cod, 1);
//                        }
//                        descPromo += (Long.parseLong(precio) * 0.2);
//                        vueltaEnCiclo = 0;
//                        cantVueltaEnCiclo++;
//                        break;
////                default:
////                    break;
//                }
//            } else {
//                System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 0%");
//            }
//        }
//    }
    private void buscandoCliente() {
//        if ("cliente_caja")) {

            this.sc.loadScreenModal("/vista/caja/BuscarClienteFXML.fxml", 602, 87, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    private void registrandoCliente() {
//        if ("cliente_caja")) {

            this.sc.loadScreenModal("/vista/caja/NuevoClienteFXML.fxml", 519, 187, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    public static void resetParam() {
        precioTotal = 0l;
    }

    private void resetTray() {
        Toaster.quitandoMsj();
    }

    private void keyPressTextCod(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            keyPressEventos(event);
            if (!enterEstado) {
                enterEstado = false;
            } else {
                if (alertArqueo != null) {
                    if (alertArqueo.isShowing()) {
                        Utilidades.log.info("FUI A UN ARQUEO");
                    }
                }
                if (alertCerrarTurno != null) {
                    if (alertCerrarTurno.isShowing()) {
                        Utilidades.log.info("FUI A UN CIERRE DE TURNO");
                    }
                }
                datos.remove("exentaGlobal");
                //NUEVO
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    if (!this.alert) {
                        mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                    }
                } else {
                    JSONObject jsonCabecera = new JSONObject();
                    if (!json.isNull("sitio") && !facturaVentaEstado) {
                        try {
                            jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
                    }
//              FIN NUEVO
                }
                enterEstado = false;
            }
        } else if (event.getCode().isDigitKey()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
        } else if (event.getCode() == KeyCode.INSERT) {

        }
    }

    private void lecturaCodBarra(KeyEvent event) {
    }

    private void mensajeDetalle(String msj, String title) {
        ButtonType btnAcept = new ButtonType("Salir (ESC)", ButtonBar.ButtonData.OK_DONE);
        Alert alerta = new Alert(Alert.AlertType.INFORMATION, msj, btnAcept);
        alerta.setTitle(title);
        alerta.setHeaderText("Mensaje del Sistema!");
        DialogPane dialogPane = alerta.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialogInformation");
        alerta.showAndWait();
    }

    private boolean verificandoCaidaFormaPago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("energiaElectrica")) {
            mensajeAlerta("Esta Factura debe ser cancelada por problemas de caída de la Energía Eléctrica, ya que podrían contener datos corruptos");
            return true;
        } else {
            return false;
        }
    }

//    public static void actualizandoCabFacturaLocalmente() {
//        JSONParser parser = new JSONParser();
//        if (DatosEnCaja.getFacturados() != null) {
//            try {
//                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
//            } catch (ParseException ex) {
//                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//            }
//        }
//        org.json.JSONObject json = new org.json.JSONObject(datos)
//                datos.put("caida", "forma_pago");
//            }
//        }
//        if (!facturaVentaEstado) {
//            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
//                //OBTENER ID RANGO ACTUAL DE LA FACTURA
//                long idRangoFact = 0;
//                if (!json.isNull("idRangoFacturaActual")) {
//                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
//                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                        datos.put("idRangoFacturaActual", idRangoFact);
//                    } else {
//                        String rango = datos.get("idRangoFacturaActual").toString();
//                        idRangoFact = Long.parseLong(rango);
//                    }
//                } else {
//                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    datos.put("idRangoFacturaActual", idRangoFact);
//                }
//                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
//                JSONObject jsonCabecera = new JSONObject();
//                if (!jsonFact.isNull("facturaClienteCab")) {
//                    try {
//                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
//                        datos.put("nroFact", FacturaDeVentaFXMLController.getCabFactura().get("nroFact"));
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                } else {
//                    try {
//                        editandoJsonFactCab();
//                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString());
//                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
//                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
//                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
//                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
//                        datos.put("nroFact", nroActualmente);
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                }
//            }
//        }
//        fact.put("facturaClienteCab", FacturaDeVentaFXMLController.getCabFactura().toString());
//        if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
//            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
//            JSONArray arrayDetalle = new JSONArray();
//            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
//                try {
//                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
//                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
//                    art.put("fechaAlta", null);
//                    art.put("fechaMod", null);
//                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
//                    iva.put("fechaAlta", null);
//                    iva.put("fechaMod", null);
//                    art.put("iva", iva);
//                    jsonArt.put("articulo", art);
//                    arrayDetalle.add(jsonArt);
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                }
//            }
//            DatosEnCaja.setDatos(datos);
//            DatosEnCaja.setUsers(users);
//            DatosEnCaja.setFacturados(fact);
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = DatosEnCaja.getFacturados();
//            fact.put("facturaDetalle", arrayDetalle);
//        }
//        actualizarDatosBD();
//        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
//    }
    private void mensajeDialog() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Consulta de Precio");
        dialog.setHeaderText("Mensaje del Sistema!");
        dialog.setContentText("INGRESE CODIGO DEL PRODUCTO:");
        ButtonType btnAcept = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().addAll(btnAcept, btnCancel);
        dialog.getDialogPane().getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialog.getDialogPane().getStyleClass().add("myDialogInformation");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Articulo art = artDAO.buscarCod(result.get());
            art.setFechaAlta(null);
            art.setFechaMod(null);
            String mensaje = "";
            mensaje = art.getDescripcion() + "\nPRECIO: " + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(art.getCosto())));
            mensaje += "\n\nLOS DESCUENTOS QUEDAN SUJETO A VARIACIONES DE ACUERDO A LA FORMA DE PAGO.";
            mensajeDetalle(mensaje, "Detalle del Artículo");
        } else {
            Utilidades.log.info("No haz seleccionado nada");
        }
    }

    public static void actualizandoCabFacturaLocalmente() {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
        if (json.isNull("caida")) {
            ReplicaFacturaCompraFXMLController.cancelacionProd = false;
            datos.put("caida", "factura_venta");
        } else {
            String caida = json.get("caida").toString();
            if (caida.equalsIgnoreCase("factura_venta")) {
                if (!isActualizarDatosCabecera()) {
                    ReplicaFacturaCompraFXMLController.cancelacionProd = false;
                } else {
                    ReplicaFacturaCompraFXMLController.cancelacionProd = true;
                }
                datos.put("caida", "factura_venta");
            } else {
                ReplicaFacturaCompraFXMLController.cancelacionProd = true;
                datos.put("caida", "forma_pago");
            }
        }
        if (!facturaVentaEstado) {
            if (!ReplicaFacturaCompraFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                ReplicaFacturaCompraFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                JSONObject jsonCabecera = new JSONObject();
                if (!jsonFact.isNull("facturaClienteCab")) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        ReplicaFacturaCompraFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
                        datos.put("nroFact", ReplicaFacturaCompraFXMLController.getCabFactura().get("nroFact"));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    try {
                        editandoJsonFactCab();
                        Map<String, String> mapeo = Utilidades.splitNroActual(ReplicaFacturaCompraFXMLController.getCabFactura().get("nroActual").toString());
                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
                        datos.put("nroFact", nroActualmente);
                        ReplicaFacturaCompraFXMLController.getCabFactura().put("nroFactura", nroActualmente);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        ReplicaFacturaCompraFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());
        fact.put("facturaClienteCab", ReplicaFacturaCompraFXMLController.getCabFactura().toString());
        if (ReplicaFacturaCompraFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
            JSONArray jsonArrayFactDet = creandoJsonFactDet(ReplicaFacturaCompraFXMLController.getCabFactura());
            JSONArray arrayDetalle = new JSONArray();
            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
                    art.put("fechaAlta", null);
                    art.put("fechaMod", null);
                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                    iva.put("fechaAlta", null);
                    iva.put("fechaMod", null);
                    art.put("iva", iva);
                    jsonArt.put("articulo", art);
                    arrayDetalle.add(jsonArt);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            DatosEnCaja.setFacturados(fact);
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
            fact.put("facturaDetalle", arrayDetalle);
        }
        actualizarDatosBD();
        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
    }

    private static void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            ReplicaFacturaCompraFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                ReplicaFacturaCompraFXMLController.getCabFactura().put("cliente", NuevoClienteFXMLController.getJsonCliente());//en nulo default NN
            } else {
                ReplicaFacturaCompraFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            }

        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            if (DatosEnCaja.getFacturados() == null) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else {
                DatosEnCaja.setFacturados(fact);
            }
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
            jsonFact = jsonFact.replace("\"[", "[");
            jsonFact = jsonFact.replace("]\"", "]");
            manejo.setFactura(jsonFact);
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();

            fact = DatosEnCaja.getFacturados();
        } catch (Exception ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        } finally {
        }
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleUnidad() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        Map mapeo = recuperarGiftSinUso(codBarra);
        boolean estadoGift = recuperarGiftEnUso(codBarra);
        if (estadoGift) {
            toaster.mensajeGenerico("Mensaje del Sistema", "GIFTCARD YA ESTA EN USO", "", 2);
        } else if (Boolean.parseBoolean(mapeo.get("comprado").toString())) {
            if (getHmGift().containsKey(Long.valueOf(codBarra))) {
                toaster.mensajeGenerico("Mensaje del Sistema", "YA SE HA CARGADO LA GIFTCARD", "", 2);
            } else {
//                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!getDetalleArtList().isEmpty()) {
                    orden++;
                }
                GiftCardFXMLController.setCodigo(codBarra, tableViewFactura, mapeo, orden);
                this.sc.loadScreenModal("/vista/caja/GiftCardFXML.fxml", 425, 206, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            }
        } else if (!estadoGift) {
            if (!codBarra.contentEquals("")) {
                if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                    jsonArticulo = jsonArtDet(codBarra);
                } else {
                    jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
                }
            }
            JSONArray jsonArrayArticulo = new JSONArray();
            try {
                org.json.JSONObject jsonArticu = new org.json.JSONObject(jsonArticulo);
                if (jsonArticu.isNull("articuloNf3Sseccion")) {
                    jsonArrayArticulo = new JSONArray();
                } else {
                    jsonArrayArticulo = (JSONArray) parser.parse(jsonArticulo.get("articuloNf3Sseccion").toString());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
            }
            boolean pasa = false;
            if (jsonArrayArticulo.isEmpty()) {
                pasa = true;
            } else {
                JSONObject jsonObj = (JSONObject) jsonArrayArticulo.get(0);
                JSONObject nf3Sseccion = (JSONObject) jsonObj.get("nf3Sseccion");
                pasa = !nf3Sseccion.get("descripcion").toString().equalsIgnoreCase("GIFT CARD");
            }
            if (!pasa) {
                toaster.mensajeGenerico("Mensaje del Sistema", "NO SE HA ESTABLECIDO CONEXION CON LA BASE GIFTCARD", "", 2);
            } else {
                boolean data = false;
                if (jsonArticulo != null) {
                    org.json.JSONObject jsonArti = new org.json.JSONObject(jsonArticulo);
                    if (jsonArti.get("costo").toString().equals("0")) {
                        mensajeDetalle("EL ARTICULO NO TIENE PRECIO, CONTACTESE CON STOCK", "Mensaje del Sistema");

                        data = true;
                    }
                }
                if (!data) {
                    if (jsonArticulo != null) {
                        try {
                            JSONObject detalleArticulo = null;
                            if (primeraInsercion) {
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                                detalleArtList.add(detalleArticulo);
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                                vistaJSONObjectArtDet();
                                primeraInsercion = false;
                                detalleArticulo.put("primeraInsercion", true);
                                cargandoCamposInterface(detalleArticulo);
                                CajaDeDatos.generandoNroComprobante();
                                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                                // primer trío
                                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                                // segundo trío
                                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                                long nroActual = talos.getNroActual();
                            } else {
                                orden++;
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);//si
                                detalleArtList.add(detalleArticulo);//si
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));//si
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);//si
                                vistaJSONObjectArtDet();//no
                                cargandoCamposInterface(detalleArticulo);
                            }
                            tableViewFactura.getItems().clear();
                            tableViewFactura.getItems().addAll(detalleArtList);
//                }
                            if (!tableViewFactura.getItems().isEmpty()) {
                                tableViewFactura.getSelectionModel().selectLast();
                            }
                            Image image = null;
                            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                                image = jsonArtDetImg(codBarra);
                            }
                            if (image == null) {
                                File file = new File(PATH.PATH_NO_IMG);
                                image = new Image(file.toURI().toString());

                            }
                        } catch (ParseException e) {
                            estado = false;
                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        }
                    } else {

                        estado = false;
                    }
                }
            }
        }
        txtCodVendedor.setText("");

        return estado;
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleDecimal() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    vistaJSONObjectArtDet();
                    primeraInsercion = false;
                    detalleArticulo.put("primeraInsercion", true);
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();

                } else {
                    orden++;
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    cargandoCamposInterface(detalleArticulo);
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                    //en caso de cancelación artículo y vuelva desde 0 a cargar
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                    image = jsonArtDetImg(codBarra);
                }

            } catch (NumberFormatException | ParseException e) {
                estado = false;
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {

            estado = false;
        }
        return estado;
    }

    private JSONObject jsonArtDet(String cod) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject articulo = null;
        articulo = generarListaArticuloLocal(cod);
        return articulo;
    }

    private JSONObject generarListaArticuloLocal(String cod) {
        JSONParser parser = new JSONParser();
        try {
            Articulo art = artDAO.buscarCod(cod);
            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
    }

    private static JSONObject creandoJsonFactCab() {
        try {
            JSONObject jsonCabFactura = new JSONObject();
            JSONObject estadoFactura = new JSONObject();
            JSONParser parser = new JSONParser();
            estadoFactura.put("idEstadoFactura", 1L);//normal
            JSONObject tipoMoneda = new JSONObject();
            tipoMoneda.put("idTipoMoneda", 1L);//guaraníes
            JSONObject tipoComprobante = new JSONObject();
            //acaité para el tema de mayorista...
            tipoComprobante.put("idTipoComprobante", 1L);//factura contado
            //**********************************************************************
            jsonCabFactura.put("cancelado", true);
            jsonCabFactura.put("caja", datos.get("caja"));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                jsonCabFactura.put("cliente", NuevoClienteFXMLController.getJsonCliente());
            } else {
                jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());
            }
//            jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            jsonCabFactura.put("sucursal", datos.get("sucursal"));//en nulo default NN
            jsonCabFactura.put("nroFactura", null);
            jsonCabFactura.put("estadoFactura", estadoFactura);
            jsonCabFactura.put("tipoComprobante", tipoComprobante);
            jsonCabFactura.put("tipoMoneda", tipoMoneda);
            //**********************************************************************
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            jsonCabFactura.put("fechaEmision", timestampEmision);
            jsonCabFactura.put("fechaMod", timestampEmision);
            jsonCabFactura.put("usuAlta", Identity.getNomFun());
            jsonCabFactura.put("usuMod", Identity.getNomFun());
            //**********************************************************************
            JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            jsonCabFactura.put("nroActual", talona.get("nroActual") + " - " + String.valueOf(talona.get("idTalonariosSucursales")));
            //Consulta de nroActual de talonarios en la BD local....
            return jsonCabFactura;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        return null;
    }

    private void cargandoCamposInterface(JSONObject detalleArticulo) {
        org.json.JSONObject jsonDetalle = new org.json.JSONObject(detalleArticulo);
//        labelCantidad.setText(detalleArticulo.get("cantidad").toString());

//        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
//        if (jsonDetalle.isNull("exenta")) {
        if (codDecimal == null) {
            codDecimal = "";
        }
        if (!codDecimal.isEmpty()) {

        } else {

        }

        seteandoMontoFact();
    }

    private static void seteandoMontoFact() {
        int monto = Integer.valueOf(String.valueOf(precioTotal));
        cabFactura.put("montoFactura", monto);
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    static void persistiendoFact(boolean cancelProd, long idArt) {
        try {
            JSONParser parser = new JSONParser();
            if (cancelProd) {
                if (cancelacionProdPrimera) {
                    FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(false));
                    cancelacionProdPrimera = false;
                    cancelacionProd = true;//permite cambiar a PUT en FormaPagoFXMLController, al finalizar venta...
                }
                FacturaVentaDatos.setIdProducto(idArt);
            } else {
                FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(true));
                cancelacionProd = false;
            }
            JSONObject aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
            JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
            FacturaVentaDatos.setIdCajero(Long.valueOf(usuarioCajero.get("idUsuario").toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    private static long creandoCabFactura(boolean cancelFact) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean estadoCancelProd = false;
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        long idFact = 0l;
        if (cancelFact) {
            JSONObject estadoFactura = new JSONObject();
            estadoFactura.put("idEstadoFactura", 2L);//anulado
            cabFactura.put("estadoFactura", estadoFactura);
        }
        try {
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));
            // NUEVO 
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                if (cancelFact) {
                    JSONObject estadoFactura = new JSONObject();
                    estadoFactura.put("idEstadoFactura", 2L);//anulado
                    cabe.put("estadoFactura", estadoFactura);
                    fact.put("facturaClienteCab", cabe);
                }
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    cabFactura = objFact;
                    if (!jsonDatos.isNull("caida")) {
                        String caida = datos.get("caida").toString();
                        if (caida.equalsIgnoreCase("forma_pago")) {
                            cancelacionProdPrimera = false;
                        } else if (!jsonDatos.isNull("cancelProducto")) {
                            cancelacionProdPrimera = false;
                            estadoCancelProd = true;
                        } else {
                            cancelacionProdPrimera = true;
                        }
                    }
                }
            }
            // NUEVO
            if (cancelacionProdPrimera && idCab == 0L) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
                //Linea para prueba por el rango actual
                long n = 0l;
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idRangoFacturaActual")) {
                    n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
                }
                cabFactura.put("idFacturaClienteCab", n);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = cabFactura.get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                cabFactura.put("nroFactura", nroFact);
                datos.put("nroFact", nroFact);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                int actulizacion = 0;
//                boolean insertVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    insertVenta = Boolean.parseBoolean(datos.get("insercionFacturaVentaCab").toString());
//                }
//                boolean actualizacionVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    if (jsonDatos.isNull("actualizacionLocal")) {
//                        actualizacionVenta = false;
//                    } else {
//                        actualizacionVenta = Boolean.parseBoolean(datos.get("actualizacionLocal").toString());
//                    }
//                }
//                if (!jsonDatos.isNull("cancelProd")) {
//                    cancelacionProdPrimera = false;
//                }
//                if (cancelacionProdPrimera) {
//                    conn.setRequestMethod("POST");//primera vez, sin importar factura cancelación o artículo...
//                    estado = true;
//                    actulizacion = 1;
//                } else if (insertVenta && !actualizacionVenta) {//Operacion realizada para saber si se persistio datos en el Servidor, sino lo realiza de manera local(ACTUALIZACION DE FACTURA CABECERA)
//                    conn.setRequestMethod("PUT");
//                    actulizacion = 1;
//                    if (estadoCancelProd) {
//                        estado = true;
//                    } else {
//                        estado = false;
//                    }
//                }
//                if (actulizacion == 1) {
//                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                    wr.write(cabFactura.toString());
//                    fact.put("facturaClienteCab", cabFactura.toString());
//                    wr.flush();
//                    int HttpResult = conn.getResponseCode();
//                    if (HttpResult == HttpURLConnection.HTTP_OK) {
//                        BufferedReader br = new BufferedReader(
//                                new InputStreamReader(conn.getInputStream(), "utf-8"));
//                        while ((inputLine = br.readLine()) != null) {
//                            cabFactura = (JSONObject) parser.parse(inputLine);
//                            datos.put("ventaServer", true);
//                            if (cabFactura.get("idFacturaClienteCab") != null) {
//                                idFact = (long) cabFactura.get("idFacturaClienteCab");
//                                //Setear Datos para saber que id se persistió en facturaClienteCab del servidor
//                                //en el caso que persista primeramente con conexion, y para la actualizacion, la conexión se vaya al maso...
//                                if (cancelacionProdPrimera) {
//                                    datos.put("insercionFacturaVentaCab", true);
//                                    datos.put("insercionIdFactClienteCabServidor", idFact);
//                                    datos.put("nroFact", cabFactura.get("nroFactura").toString());
//                                }
//                            }
//
//                        }
//                        br.close();
//                    } else {
//                        idFact = generarFacturaCabLocal();
//                    }
//                } else {
//                    idFact = generarFacturaCabLocal();
//                }
//            } else {
            idFact = generarFacturaCabLocal();
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //para registrar facturaIni
        org.json.JSONObject jsonEstadoInicio = new org.json.JSONObject(datos);
        boolean estadoFactInicial = false;
        if (!jsonEstadoInicio.isNull("estadoFacturaInicial")) {
            estadoFactInicial = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        if (!estadoFactInicial) {
            datos.put("facturaInicial", cabFactura.get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("FacturaFinal", cabFactura.get("nroFactura").toString());
        if (idFact != 0l && cancelFact && estado) {//que sea del tipo cancelación factura...
            JSONArray jsonArrayFactDet = creandoJsonFactDet(cabFactura);
            if (creandoFactDet(jsonArrayFactDet)) {
            }
        }
        actualizarDatos();
        return idFact;
    }

    public static JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < detalleArtList.size(); i++) {
            detalleArtList.get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(detalleArtList.get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }

    private static boolean creandoFactDet(JSONArray jsonArray) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Boolean.parseBoolean(datos.get("ventaServer").toString())) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteDet/insercionMasiva");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonArrayFactDet.toString());
//                fact.put("facturaDetalle", jsonArrayFactDet.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exitoInsertarDet = (boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//                }
//            } else {
            exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }

    private static boolean registrandoFacturaDetLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private static long generarFacturaCabLocal() {
        String sql = "";
        ConexionPostgres.conectar();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (cancelacionProdPrimera) {
            datos.put("insercionFacturaVentaCabLocal", true);
            //UUID ACTUAL PARA MODIFICAR FACTURA
            long uuid = VentasUtiles.recuperarId() + 1;
            datos.put("uuidCassandraActual", String.valueOf(uuid));
            sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'insertar');";
            Utilidades.log.info("-->> " + sql);
        } else {
            long idFactCab = 0L;
            if (!json.isNull("idFactClienteCabServidor")) {
                idFactCab = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
            }
            if (idFactCab != 0 && !dato) {
                //UUID ACTUAL PARA MODIFICAR FACTURA
                long uuid = VentasUtiles.recuperarId() + 1;
                cabFactura.put("idFacturaClienteCab", idFactCab);
                sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'actualizar');";
                Utilidades.log.info("-->> " + sql);
                datos.put("uuidCassandraActual", String.valueOf(uuid));
                //para que solo una ves entre aqui luego ya actualice nada mas...
                dato = true;
                datos.put("actualizacionLocal", true);
            } else {
                String operacion = "insertar";
                if (idFactCab != 0) {
                    cabFactura.put("idFacturaClienteCab", idFactCab);
                    operacion = "actualizar";
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + datos.get("uuidCassandraActual").toString() + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
                Utilidades.log.info("-->> " + sql);
                datos.put("actualizacionLocal", true);
            }
        }
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS FACTURA VENTA CAB ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        cabFactura.put("idFacturaClienteCab", datos.get("idRangoFacturaActual"));

        return Long.parseLong(datos.get("idRangoFacturaActual").toString());
    }

    static void suprimirProducto(TableView<JSONObject> tabla, Label labelTotalGs, double declarado,
            Label labelCantidad, ImageView imgProducto, CheckBox chkExtranj, TextField txtCod) {
        JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
        JSONObject articulos = (JSONObject) detalle.get("articulo");
        long codArticulo = (long) articulos.get("codArticulo");
        JSONObject jsonArticulo = hashJsonArticulo.get(codArticulo);
        double cantTotal = Double.parseDouble(detalle.get("cantidad").toString());
        double dif = cantTotal - declarado;
        int total = Integer.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        double neto = 0;
        double resultado = 0;
        String dato = "";
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        double nArt = Double.parseDouble(datos.get("nArticulos").toString());
//        datos.put("nArticulos", nArt - cantTotal);
        datos.put("nArticulos", nArt - declarado);
        if (dif <= 0) {
            resultado = Double.parseDouble(detalle.get("precio").toString()) * cantTotal;
            List<JSONObject> detalleAux = new ArrayList<>();
            resetMapeo();
            int ord = 0;
            for (JSONObject detalleEnCuestion : detalleArtList) {
                JSONObject jsonArtAux = (JSONObject) detalleEnCuestion.get("articulo");
                if (!(codArticulo + "|" + detalle.get("orden").toString()).equalsIgnoreCase(jsonArtAux.get("codArticulo") + "|" + detalleEnCuestion.get("orden"))) {
                    ord++;
                    detalleEnCuestion.put("orden", ord);
                    detalleAux.add(detalleEnCuestion);
                    hashJsonArtDet.put((Long) jsonArtAux.get("idArticulo"), detalleAux.lastIndexOf(detalleEnCuestion));
                    hashJsonArticulo.put((Long) jsonArtAux.get("codArticulo"), jsonArtAux);
                }
            }
            orden = ord;
            detalleArtList = new ArrayList<>();
            detalleArtList = detalleAux;
//            if (ord == 0 && detalleAux.size() == 0) {
//                BuscarClienteFXMLController.resetParamCliente();
//                BuscarClienteFXMLController.resetParamClienteFiel();
//            }
        } else {
            int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
            resultado = Double.parseDouble(detalle.get("precio").toString()) * declarado;
            long resultadoActual = Math.round(Double.parseDouble(detalle.get("precio").toString()) * dif);
            long iva = (long) detalle.get("poriva");
            if (iva == 0l || chkExtranj.isSelected()) {
                detalleArtList.get(index).put("exenta", resultadoActual);
            } else {
                detalleArtList.get(index).put("gravada", resultadoActual);
            }
            detalleArtList.get(index).put("cantidad", dif);
        }
        neto = total - resultado;
        dato = formateador.format(neto);
        labelTotalGs.setText("Gs " + dato);
        labelCantidad.setText("0");
        imgProducto.setImage(null);
        precioTotal = Math.round(neto);
        seteandoMontoFact();
        try {
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                }
            }
            if (idCab == 0L) {
                creandoCabFactura(false);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        tabla.getItems().clear();
        tabla.getItems().addAll(detalleArtList);
        //NEW
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setFacturados(fact);
        actualizarDatos();
        if (tabla.getItems().isEmpty()) {
            chkExtranj.setDisable(false);
            chkExtranj.setSelected(false);
        }
        repeatFocusData(txtCod);
    }

    static void setTearDatos(Label lblRuc, Label lblNombre, String ruc, String nombre) {
        lblRuc.setText(ruc);
        lblNombre.setText(nombre);
    }

    static void regresar(TextField txtCod) {
        repeatFocusData(txtCod);
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }

    @FXML
    private void textFieldCantKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void tableViewFacturaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void keyPressEventos(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtNroEntrada.isFocused()) {
                buscarRecepcion();
            } else if (txtCodigo.isFocused()) {
                buscarArticulo();
            } else if (txtOC.isFocused()) {
//                PedidoCab pc = pedidoCabDAO.getByOC(txtOC.getText());
                if (txtNroFactura.getText().equals("")) {
                    mensajeAlerta("EL CAMPO NRO DE FACTURA NO DEBE QUEDAR VACIO");
                } else {
//                    mapeoCheck = new HashMap();
                    Recepcion recep = recepcionDAO.getByNroOrdenPedido(txtOC.getText());
                    if (recep != null) {
                        txtNroEntrada.setText(recep.getNroOrden());
                        buscarRecepcion();
                    } else {
//                        detalleArtList = new ArrayList<>();
//                        tableViewFactura.getItems().clear();
//                        orden = 0;

                        PedidoCab pc = pedidoCabDAO.getByOC(txtOC.getText());
                        if (pc.getTipo().toUpperCase().equals("CO")) {
                            txtTipoMovimiento.setText("CONTADO");
                        } else {
                            txtTipoMovimiento.setText("CREDITO");
                        }
                        JSONObject objProveedor = new JSONObject();
                        objProveedor.put("idProveedor", pc.getProveedor().getIdProveedor());
                        objProveedor.put("descripcion", pc.getProveedor().getDescripcion());
                        BuscarProveedorFXMLController.seteandoParam(objProveedor);
                        txtProveedor.setText(pc.getProveedor().getDescripcion());
                        txtNroTimbrado.setText(pc.getProveedor().getTimbrado() + "");
                        txtRucProveedor.setText(pc.getProveedor().getRuc());
//                    txtClaseMov.setText(pc.get);
                        txtObservacion.setText(pc.getObservacion());
//                    txtNroFactura.setText(pc.get);
                        Date d = new Date(pc.getFecha().getTime() * 1000);
                        DateFormat f = new SimpleDateFormat("yyyy-MM-dd");
//                    DateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.mmm'Z'");
//                    System.out.println(f.format(d));
//                    txtFechaRecepcion.setText(f.format(d) + "");
                        txtFechaFactura.setText(LocalDate.now() + "");
                        txtFechaRecepcion.setText(LocalDate.now() + "");
                        chkEmpresa.setValue(pc.getSucursal().toUpperCase());
                        chkTipoMovimiento.setValue(pc.getTipo());
                        chkClaseMov.setValue("MER");
                        txtClaseMov.setText("MERCADERIA");
                        chkSituacion.setValue("CONTRIBUYENTE");
                    }
//                    BuscarOCEnCompraFXMLController.cargarOc(txtOC.getText());
//                    abrirBusquedaOC();
//                    this.sc.loadScreenModal("/vista/stock/BuscarOCEnCompra.fxml", 888, 625, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                    ESTO ESTABA ANTES
//                    cargarDetalleByOC(txtOC.getText());
                }
                busquedaRecep = true;
            } else if (tableViewFactura.isFocused()) {
//                cerrandoCabecera();
                recargarDatos();
            } else if (txtGsTotalManual.isFocused()) {
                if (txtGsTotalManual.getText().equalsIgnoreCase("")) {
                    mensajeAlerta("EL CAMPO TOTAL NO DEBE QUEDAR VACIO PARA REALIZAR LOS CALCULOS");
                } else {
                    long iva5 = 0l;
                    long iva10 = 0l;
                    long grav5 = 0l;
                    long grav10 = 0l;
                    long total = Long.parseLong(numValidator.numberValidator(txtGsTotalManual.getText()));
                    long exe = 0;
                    if (!txtGsExentaManual.getText().equalsIgnoreCase("")) {
                        exe = Long.parseLong(numValidator.numberValidator(txtGsExentaManual.getText()));
                    }
                    if (!txtGsIva5Manual.getText().equalsIgnoreCase("")) {
                        iva5 = Long.parseLong(numValidator.numberValidator(txtGsIva5Manual.getText()));
//                        grav5 = (iva5 * 100) / 5;
                        grav5 = (iva5 * 21) - iva5;
                    }
                    if (!txtGsIva10Manual.getText().equalsIgnoreCase("")) {
                        iva10 = Long.parseLong(numValidator.numberValidator(txtGsIva10Manual.getText()));
                        grav10 = (iva10 * 11) - iva10;
                    }
                    long dif = total - (grav5 + grav10 + iva5 + iva10);

                    if (grav10 > 0) {
                        grav10 += dif;
                    } else if (grav5 > 0) {
                        grav5 += dif;
                    }

                    txtGsExentaManual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exe + "")));
                    txtGsGrav5Manual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
                    txtGsGrav10Manual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
                    txtGsTotalManual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));
                    txtGsIva5Manual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
                    txtGsIva10Manual.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));

                    Platform.runLater(() -> {
                        if (txtGsIva5Manual.getText().equalsIgnoreCase("")) {
                            txtGsIva5Manual.setText("Gs 0");
                        }
                        if (txtGsIva10Manual.getText().equalsIgnoreCase("")) {
                            txtGsIva10Manual.setText("Gs 0");
                        }
                        if (txtGsExentaManual.getText().equalsIgnoreCase("")) {
                            txtGsExentaManual.setText("Gs 0");
                        }
                    });
                }
            }
        }
    }

    private void buscarArticulo() {
        Articulo art = artDAO.buscarCod(txtCodigo.getText());
        if (art != null) {
            depositoList = new ArrayList<>();
            tableViewDeposito.getItems().clear();

            btnVerificarPrecio.setDisable(false);
            chkMedida.setValue("UNIDAD");
            txtContenido.setText("1");
            txtExistencia.setText("0");
            txtDescuento.setText("0");
            txtGasto.setText("0");
            txtCantidad.setText("1");

            txtDescripcion.setText(art.getDescripcion());
            txtCosto.setText(art.getCosto() + "");
            if (art.getIva().getIdIva() == 1) {
                txtDescriIva.setText("EXE");
                txtPorcIva.setText(art.getIva().getPoriva() + "");
                txtExenta.setText(art.getCosto() + "");
            } else {
                txtDescriIva.setText("GRA");
                txtPorcIva.setText(art.getIva().getPoriva() + "");
                long iva = 0;
                if (art.getIva().getIdIva() == 2) {
                    iva = Math.round(art.getCosto() / 21);
                    iva = Long.parseLong(txtCosto.getText()) - iva;
                } else {
                    iva = Math.round(art.getCosto() / 11);
                    iva = Long.parseLong(txtCosto.getText()) - iva;
                }
                txtGravada.setText(iva + "");
            }
            verificarNivel1(art.getIdArticulo());
            List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
            for (Existencia ex : listExis) {
//                try {
                Articulo articu = new Articulo();
                articu.setIdArticulo(art.getIdArticulo());

                ex.setArticulo(articu);
//                    JSONObject objJSON = (JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objJSON = new JSONObject();//(JSONObject) parser.parse(gson.toJson(ex).toString());
                JSONObject objART = new JSONObject();
                objART.put("idArticulo", art.getIdArticulo());

                JSONObject objDEPO = new JSONObject();
                objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
                objDEPO.put("descripcion", ex.getDeposito().getDescripcion());

                objJSON.put("idExistencia", ex.getIdExistencia());
                objJSON.put("cantidad", ex.getCantidad());
                objJSON.put("articulo", objART);
                objJSON.put("deposito", objDEPO);

                depositoList.add(objJSON);

                actualizandoTablaDeposito();
//                } catch (ParseException ex1) {
//                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
//                }
            }
        } else {
            mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
        }
    }

    private void actualizandoTablaDeposito() {
        depositoData = FXCollections.observableArrayList(depositoList);
        //columna Nombre ..................................................
//        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                String nombre = String.valueOf(data.getValue().get("nombre"));
//                return new ReadOnlyStringWrapper(nombre.toUpperCase());
//            }
//        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        tableColumnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                    String apellido = String.valueOf(jsonDepo.get("descripcion"));
                    if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                        apellido = "-";
                    }
                    return new ReadOnlyStringWrapper(apellido.toUpperCase());

                } catch (ParseException ex) {
                    Logger.getLogger(ReplicaFacturaCompraFXMLController.class
                            .getName()).log(Level.SEVERE, null, ex);
                    return null;
                }
            }
        });
        //columna Apellido ..................................................
        //columna Ruc .................................................
        tableColumnCantDepo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCantDepo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                //                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
                String apellido = String.valueOf(data.getValue().get("cantidad"));
                if (apellido.contentEquals("null") || apellido.contentEquals("")) {
                    apellido = "-";
                }
                return new ReadOnlyStringWrapper(apellido.toUpperCase());
            }
        });
        //columna Ruc .................................................
        tableViewDeposito.setItems(depositoData);
//        if (!escucha) {
//            escucha();
//        }
    }

    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneFactura.getChildren()
                .get(anchorPaneFactura.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

//    PARA NUESTRO ENTORNO
//    private boolean recuperarGiftEnUso(String codigo) {
//        boolean comprado = false;
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT comprado FROM stock.articulo WHERE comprado=true AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    comprado = true;
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        }
//        return comprado;
//    }
    private boolean recuperarGiftEnUso(String codigo) {
        boolean comprado = false;
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT comprado FROM stk_articulos WHERE comprado=1 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    comprado = true;
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        }
        return comprado;
    }
//    PARA NUESTRO ENTORNO
//    private Map recuperarGiftSinUso(String codigo) {
//        Map mapeo = new HashMap();
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT * FROM stock.articulo WHERE comprado=false AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    mapeo.put("comprado", true);
//                    mapeo.put("codigo", rs.getLong("cod_articulo"));
//                    mapeo.put("saldogift", rs.getDouble("saldogift"));
//                    mapeo.put("fecha", rs.getDate("fechavtogift"));
//                    mapeo.put("descripcion", rs.getString("descripcion"));
//                } else {
//                    mapeo.put("comprado", false);
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        } else {
//            mapeo.put("comprado", false);
//        }
//        return mapeo;
//    }

    private Map recuperarGiftSinUso(String codigo) {
        Map mapeo = new HashMap();
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT * FROM stk_articulos WHERE comprado=0 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    mapeo.put("comprado", true);
                    mapeo.put("codigo", rs.getLong("codigo"));
                    mapeo.put("saldogift", rs.getDouble("saldogift"));
                    mapeo.put("fecha", rs.getDate("fechavtogift"));
                    mapeo.put("descripcion", rs.getString("descripcion"));
                } else {
                    mapeo.put("comprado", false);
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        } else {
            mapeo.put("comprado", false);
        }
        return mapeo;
    }

    private void resetFormulario(long total) {
        JSONObject talonarioSucursal;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
            // primer trío
            long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
            // segundo trío
            long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
            long nroActual = talos.getNroActual();

//                datos.put("nroFact", Long.parseLong(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual).replace("-", "")));
            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");

            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }

            } else {

            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
        primeraInsercion = false;

        cargandoImagen();
    }

    private void listenCheckExtranjero() {
//        chkExtranjero.selectedProperty().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                CajaDeDatos.getCaja().put("estadoExt", newValue);
//            }
//        });
    }

    @FXML
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    @FXML
    private void btnBorrarDetalleAction(ActionEvent event) {
    }

    @FXML
    private void btnSeccionAction(ActionEvent event) {
    }

    @FXML
    private void btnNoserAction(ActionEvent event) {
    }

    @FXML
    private void btnCargarVtoProdAction(ActionEvent event) {
    }

    @FXML
    private void btnVerificarPrecioAction(ActionEvent event) {
        verificarPrecio();
    }

    @FXML
    private void btnGuardarDetalleAction(ActionEvent event) {
        guardarDetalle();
    }

    @FXML
    private void btnBuscarOrdenCompraAction(ActionEvent event) {
    }

    @FXML
    private void btnEditarAction(ActionEvent event) {
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        if (!txtNroFactura.getText().equals("")) {
            guardarCompra();
        } else {
            mensajeAlerta("DEBE SELECCIONAR EL NUMERO DE FACTURA");
        }
    }

    @FXML
    private void btnBorrarAction(ActionEvent event) {
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
    }

    @FXML
    private void txtNroEntradaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void buscarRecepcion() {
        Recepcion recep = recepcionDAO.getByNroOrden(txtNroEntrada.getText());
        if (recep != null) {
            txtTipoMovimiento.setText(recep.getTipoMovimiento());
            txtProveedor.setText(recep.getProveedor().getDescripcion());
            txtNroTimbrado.setText(recep.getProveedor().getTimbrado() + "");
            txtRucProveedor.setText(recep.getProveedor().getRuc());
            txtClaseMov.setText(recep.getClaseDocumento());
            txtObservacion.setText(recep.getObservacion());
            txtNroFactura.setText(recep.getNroDoc());
            txtFechaRecepcion.setText(recep.getFechaDoc() + "");
            txtFechaFactura.setText(LocalDate.now() + "");
            chkEmpresa.setValue(recep.getSucursal().toUpperCase());
            chkTipoMovimiento.setValue("CO");
            chkClaseMov.setValue("MER");
            chkSituacion.setValue("CONTRIBUYENTE");
            txtOC.setText(recep.getPedidoCab().getOc());
        } else {
            if (BuscarProveedorFXMLController.getJsonCliente() == null) {
                mensajeAlerta("DEBE SELECCIONAR UN PROVEEDOR...");
            } else {
                buscarOrden();
            }

        }
    }

    @FXML
    private void txtCodigoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void verificarNivel1(long id) {
        ArticuloNf1Tipo anf1Tipo = anf1TipoDAO.getByIdArticulo(id);
        if (anf1Tipo != null) {
            nivel01 = anf1Tipo.getNf1Tipo().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel01);
            txtSec1.setText(nivel01);
            verificarNivel2(id);
        }
    }

    private void verificarNivel2(long id) {
        ArticuloNf2Sfamilia anf2Familia = anf2SfamiliaDAO.getByIdArticulo(id);
        if (anf2Familia != null) {
            nivel02 = anf2Familia.getNf2Sfamilia().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel02);
            verificarNivel3(id);
        }
    }

    private void verificarNivel3(long id) {
        ArticuloNf3Sseccion anf3Seccion = anf3SeccionDAO.getByIdArticulo(id);
        if (anf3Seccion != null) {
            nivel03 = anf3Seccion.getNf3Sseccion().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel03);
            verificarNivel4(id);
        }
    }

    private void verificarNivel4(long id) {
        ArticuloNf4Seccion1 anf4Seccion = anf4SeccionDAO.getByIdArticulo(id);
        if (anf4Seccion != null) {
            nivel04 = anf4Seccion.getNf4Seccion1().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel04);
            verificarNivel5(id);
        }
    }

    private void verificarNivel5(long id) {
        ArticuloNf5Seccion2 anf5Seccion = anf5SeccionDAO.getByIdArticulo(id);
        if (anf5Seccion != null) {
            nivel05 = anf5Seccion.getNf5Seccion2().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel05);
            verificarNivel6(id);
        }
    }

    private void verificarNivel6(long id) {
        ArticuloNf6Secnom6 anf6Seccion = anf6SeccionDAO.getByIdArticulo(id);
        if (anf6Seccion != null) {
            nivel06 = anf6Seccion.getNf6Secnom6().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel06);
            verificarNivel7(id);
        }
    }

    private void verificarNivel7(long id) {
        ArticuloNf7Secnom7 anf7Seccion = anf7SeccionDAO.getByIdArticulo(id);
        if (anf7Seccion != null) {
            nivel07 = anf7Seccion.getNf7Secnom7().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel07);
        }
    }

    private void guardarDetalle() {
        numValidator = new NumberValidator();
        if (chkDepDestino.getSelectionModel().getSelectedIndex() >= 0 && chkTipo.getSelectionModel().getSelectedIndex() >= 0 && !txtCantidad.getText().equals("") && !txtCantidad.getText().equals("0")) {
            orden++;
            switch (txtPorcIva.getText()) {
                case "0":
                    if (numValidator.numberValidator(txtGsExenta.getText()).equals("")) {
                        long tExe = Long.parseLong(numValidator.numberValidator(txtExenta.getText()));
                        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((0 + tExe) + "")));
                    } else {
                        long tExe = Long.parseLong(numValidator.numberValidator(txtExenta.getText()));
                        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((Long.parseLong(numValidator.numberValidator(txtGsExenta.getText())) + tExe) + "")));
                    }
                    break;
                case "5":
                    if (numValidator.numberValidator(txtGsGrav5.getText()).equals("")) {
                        long tGra5 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));
                        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((0 + tGra5) + "")));
                    } else {
                        long tGra5 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));
                        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((Long.parseLong(numValidator.numberValidator(txtGsGrav5.getText())) + tGra5) + "")));
                    }
                    long data5 = Long.parseLong(numValidator.numberValidator(txtCosto.getText())) * Long.parseLong(txtCantidad.getText());
                    long tGra5 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));

                    long iva5Ant = 0;
                    if (!numValidator.numberValidator(txtGsIva5.getText()).equals("")) {
                        iva5Ant = Long.parseLong(numValidator.numberValidator(txtGsIva5.getText()));
                    }

                    long val5 = data5 - tGra5;
                    txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(((val5 + iva5Ant) + ""))));
                    break;
                case "10":
                    if (numValidator.numberValidator(txtGsGrav10.getText()).equals("")) {
                        long tGra10 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));
                        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((0 + tGra10) + "")));
                    } else {
                        long tGra10 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));
                        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((Long.parseLong(numValidator.numberValidator(txtGsGrav10.getText())) + tGra10) + "")));
                    }
                    long data10 = Long.parseLong(txtCosto.getText()) * Long.parseLong(txtCantidad.getText());
                    long tGra10 = Long.parseLong(numValidator.numberValidator(txtGravada.getText()));

                    long iva10Ant = 0;
                    if (!numValidator.numberValidator(txtGsIva10.getText()).equals("")) {
                        iva10Ant = Long.parseLong(numValidator.numberValidator(txtGsIva10.getText()));
                    }
                    long val10 = data10 - tGra10;
                    txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(((val10 + iva10Ant) + ""))));
                    break;
                default:
                    break;
            }
            long gsExe = 0l;
            if (!numValidator.numberValidator(txtGsExenta.getText()).equals("")) {
                gsExe = Long.parseLong(numValidator.numberValidator(txtGsExenta.getText()));
            }
            long gsGra5 = 0l;
            if (!numValidator.numberValidator(txtGsGrav5.getText()).equals("")) {
                gsGra5 = Long.parseLong(numValidator.numberValidator(txtGsGrav5.getText()));
            }
            long gsGra10 = 0l;
            if (!numValidator.numberValidator(txtGsGrav10.getText()).equals("")) {
                gsGra10 = Long.parseLong(numValidator.numberValidator(txtGsGrav10.getText()));
            }
            long gsIva5 = 0l;
            if (!numValidator.numberValidator(txtGsIva5.getText()).equals("")) {
                gsIva5 = Long.parseLong(numValidator.numberValidator(txtGsIva5.getText()));
            }
            long gsIva10 = 0l;
            if (!numValidator.numberValidator(txtGsIva10.getText()).equals("")) {
                gsIva10 = Long.parseLong(numValidator.numberValidator(txtGsIva10.getText()));
            }
            long total = gsExe + gsGra5 + gsGra10 + gsIva5 + gsIva10;
            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble((total + ""))));// + Long.parseLong(txtGsGrav5.getText())) + Long.parseLong(txtGsGrav10.getText()) + Long.parseLong(txtGsIva5.getText()) + Long.parseLong(txtGsIva10.getText()) + "");
            JSONObject jsonDetalle = new JSONObject();
            JSONObject jsonRecep = new JSONObject();

            try {
                PedidoCab pc = pedidoCabDAO.getByOC(txtOC.getText());
                jsonRecep.put("idPedidoCab", pc.getIdPedidoCab());
                jsonDetalle.put("pedidoCab", jsonRecep);
            } catch (Exception e) {
                jsonDetalle.put("pedidoCab", null);
            } finally {
            }

            JSONObject jsonArt = new JSONObject();
            jsonArt.put("idArticulo", artDAO.buscarCod(txtCodigo.getText()).getIdArticulo());

            jsonDetalle.put("articulo", jsonArt);
            jsonDetalle.put("cantidad", txtCantidad.getText());
            jsonDetalle.put("precio", txtCosto.getText());
            jsonDetalle.put("total", (Long.parseLong(txtCosto.getText()) * Long.parseLong(txtCantidad.getText())));
            jsonDetalle.put("descripcion", txtDescripcion.getText());
            jsonDetalle.put("diferencia", "");
            if (txtExenta.getText().equals("")) {
                jsonDetalle.put("poriva", numValidator.numberValidator(txtPorcIva.getText()));
                jsonDetalle.put("iva", numValidator.numberValidator(txtGravada.getText()));
            } else {
                jsonDetalle.put("poriva", "0");
                jsonDetalle.put("iva", numValidator.numberValidator(txtExenta.getText()));
            }
            jsonDetalle.put("orden", orden);
            mapeoCheck.put(orden + "-" + txtCodigo.getText(), true);
            jsonDetalle.put("sec1", txtSec1.getText());
            jsonDetalle.put("sec2", txtSec2.getText());
            jsonDetalle.put("codArticulo", txtCodigo.getText());
            jsonDetalle.put("deposito", chkDepDestino.getSelectionModel().getSelectedItem());
            jsonDetalle.put("tipo", chkTipo.getSelectionModel().getSelectedItem());
            jsonDetalle.put("medida", chkMedida.getSelectionModel().getSelectedItem());
            jsonDetalle.put("contenido", txtContenido.getText());
            jsonDetalle.put("existencia", txtExistencia.getText());
            jsonDetalle.put("descuento", txtDescuento.getText());

            detalleArtList.add(jsonDetalle);
            vistaJSONObjectArtDet();

            chkDepDestino.getSelectionModel().select(0);
            chkTipo.setValue("MER");
            chkMedida.setValue("UNIDAD");
            txtCodigo.setText("");
            txtContenido.setText("");
            txtNroPedido.setText("");
            txtCantidad.setText("");
            txtExistencia.setText("");
            txtDescuento.setText("");
            txtGasto.setText("");
            txtCosto.setText("");
            txtDescriIva.setText("");
            txtPorcIva.setText("");
            txtExenta.setText("");
            txtGravada.setText("");

            txtSec1.setText("");
            txtDescripcion.setText("");
            txtSec2.setText("");

            btnVerificarPrecio.setDisable(true);

            depositoList = new ArrayList<>();
            tableViewDeposito.getItems().clear();
            repeatFocus(txtCodigo);

//            if (!detalleArtList.isEmpty()) {
//                Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));a
//            }
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    tableViewFactura.requestFocus();
                    tableViewFactura.getSelectionModel().select(detalleArtList.size());
                    tableViewFactura.getFocusModel().focus((detalleArtList.size()));
                    tableViewFactura.scrollTo((detalleArtList.size()));
                }
            });
        } else {
            mensajeAlerta("DEBE SELECCIONAR EL DEPOSITO, TIPO Y CANTIDAD PARA AGREGAR UN DETALLE");
        }
    }

    private void guardarCompra() {
        if (!busquedaRecep) {
            suprimirSelecciones();
            guardarCompraSinOC();
        } else {
//            if (busquedaRecep) {
            int cant = 0;
            suprimirSelecciones();
            if (detalleArtList.size() > 0) {
                for (JSONObject detalle : detalleArtList) {
                    cant += Integer.parseInt(detalle.get("cantidad").toString());
                }
            }
            PedidoCab recepc = pedidoCabDAO.getByOC(txtOC.getText());
            FacturaCompraCab fcc = new FacturaCompraCab();
            fcc.setTipoMovimiento("CONTADO");
            fcc.setNroOrden(txtOC.getText());
            fcc.setNroDoc(txtNroFactura.getText());
            try {
                java.sql.Date date = new java.sql.Date(recepc.getFecha().getTime());
                fcc.setFechaDoc(date);
            } catch (Exception e) {
                java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
                fcc.setFechaDoc(date);
            } finally {
            }
            fcc.setSc(false);

            if (chkCentral.isSelected()) {
                fcc.setCc(true);
//            NUEVO HAY QUE SACAR DESPUES
                fcc.setSc(true);
            } else {
                fcc.setCc(false);
            }
            if (chkCacique.isSelected()) {
                fcc.setSc(true);
                //            NUEVO HAY QUE SACAR DESPUES
                fcc.setCc(true);
            }
//        else {
            //            NUEVO HAY QUE SACAR DESPUES
//        fcc.setSc(false);
//        }
            if (chkSanLorenzo.isSelected()) {
                fcc.setSl(true);
            } else {
                fcc.setSl(false);
            }

            fcc.setCantidad(cant + "");
            fcc.setSucursal(chkEmpresa.getSelectionModel().getSelectedItem());
            fcc.setObservacion(txtObservacion.getText());

            try {
                fcc.setTipoDocumento(recepc.getTipo());
            } catch (Exception e) {
                fcc.setTipoDocumento(null);
            } finally {
            }

            fcc.setClaseDocumento("MERCADERIA");
            fcc.setTotal(Integer.parseInt(numValidator.numberValidator(txtGsTotal.getText())));
            fcc.setSaldo(Integer.parseInt(numValidator.numberValidator(txtGsTotal.getText())));
            fcc.setEstadoFactura("C");
            fcc.setOc(txtOC.getText());

            try {
                PedidoCab ped = new PedidoCab();
                ped.setIdPedidoCab(recepc.getIdPedidoCab());
                fcc.setPedidoCab(ped);
            } catch (Exception e) {
                fcc.setPedidoCab(null);
            } finally {
            }

            try {
                Proveedor pro = new Proveedor();
                pro.setIdProveedor(Long.parseLong(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString()));
                fcc.setProveedor(pro);
            } catch (Exception e) {
                fcc.setProveedor(null);
            } finally {
            }
            FacturaCompraCab fccDATA = facturaCompraCabDAO.insertarObtenerObj(fcc);

            if (detalleArtList.size() > 0) {
                long i = 0;
                for (JSONObject detalle : detalleArtList) {
                    i++;
                    FacturaCompraDet fcd = gson.fromJson(detalle.toString(), FacturaCompraDet.class
                    );
                    FacturaCompraCab fccDetalle = new FacturaCompraCab();
                    fccDetalle.setIdFacturaCompraCab(fccDATA.getIdFacturaCompraCab());
                    fcd.setFacturaCompraCab(fccDetalle);
                    try {
                        JSONObject jsonArt = (JSONObject) parser.parse(detalle.get("articulo").toString());
                        long idArt = Long.parseLong(jsonArt.get("idArticulo").toString());
                        long idDeposito = depositoDAO.getByDescripcion(detalle.get("deposito").toString());

                        Existencia existencia = exisDAO.filtrarPorArtDeposito(idArt, idDeposito);
                        if (existencia != null) {
//                        Existencia exis = new Existencia();
                            existencia.setCantidad((Long.parseLong(detalle.get("cantidad").toString()) + existencia.getCantidad()));
                            exisDAO.actualizar(existencia);
//                        Articulo art = new Articulo();
//                        art.setIdArticulo(idArt);
//                        Deposito dep = new Deposito();
//                        dep.setIdDeposito(idDeposito);
//                        exis.setArticulo(art);
//                        exis.setDeposito(dep);
                        } else {
                            Existencia exis = new Existencia();
                            exis.setCantidad(Long.parseLong(detalle.get("cantidad").toString()));
                            Articulo art = new Articulo();
                            art.setIdArticulo(idArt);
                            Deposito dep = new Deposito();
                            dep.setIdDeposito(idDeposito);
                            exis.setArticulo(art);
                            exis.setDeposito(dep);
                            exisDAO.insertar(exis);
                        }
                        try {
                            fcdDAO.insercionMasiva(fcd, i);
                            LocalDate nowLast = LocalDate.now();
                            boolean cc = true;
                            boolean sc = true;
                            boolean sl = false;
                            if (chkSanLorenzo.isSelected()) {
                                sl = true;
                                cc = false;
                                sc = false;
                            }
                            ArticuloCompraMatriz acm = artCompraMatrizDAO.getByCodFecha(detalle.get("codArticulo").toString(), nowLast, cc, sc, sl);
                            if (acm != null) {
                                long canti = acm.getCantidad() + Long.parseLong(detalle.get("cantidad").toString());
                                acm.setCantidad(canti);
                                artCompraMatrizDAO.actualizar(acm);
                            } else {
                                ArticuloCompraMatriz artComp = new ArticuloCompraMatriz();
                                artComp.setCodigo(detalle.get("codArticulo").toString());
                                artComp.setCantidad(Long.parseLong(detalle.get("cantidad").toString()));
                                artComp.setFecha(java.sql.Date.valueOf(nowLast));
                                artComp.setCc(cc);
                                artComp.setSc(sc);
                                artComp.setSl(sl);

                                artCompraMatrizDAO.insertar(artComp);
                            }
                        } catch (Exception e) {
                            System.out.println("-> " + e.getLocalizedMessage());
                            System.out.println("-> " + e.getMessage());
                            System.out.println("-> " + e.fillInStackTrace());
                        } finally {
                        }

                    } catch (ParseException ex) {
                        System.out.println("-> " + ex.getLocalizedMessage());
                        System.out.println("-> " + ex.getMessage());
                        System.out.println("-> " + ex.fillInStackTrace());
                    }

                }
//            recepc.setTotal(Long.parseLong(txtGsTotal.getText()));
//            recepc.setSaldo(Long.parseLong(txtGsTotal.getText()));
//            pedidoCabDAO.actualizar(recepc);
                limpiarCampos();

//            secondPane1.setVisible(false);
                mensajeAlerta("COMPRA HA SIDO AGREGADO EXITOSAMENTE...");

                btnVerificarPrecio.setDisable(true);

                toaster = new Toaster();
                btnVerificarPrecio.setDisable(true);
                tableViewFactura.getItems().clear();
                detalleArtList = new ArrayList<>();
                orden = 0;
//            ubicandoContenedorSecundario();
//            cargarTipoMovimiento();
//            cargarDeposito();
//            cargarTipo();
//            cargarMedida();
//            cargarSucursal();
//            cargarSituacion();
//            cargarClaseMovimiento();
                repeatFocus(txtNroFactura);
            } else {
                mensajeAlerta("NO SE HA CARGADO NINGUN DETALLE A LA FACTURA");
            }
//            } else {
//                mensajeAlerta("DEBE GENERAR LA BUSQUEDA POR OC");
            busquedaRecep = false;
//            }
        }
        chkTodo.setSelected(true);
    }

    private void guardarCompraSinOC() {
        int cant = 0;
        if (detalleArtList.size() > 0) {
            for (JSONObject detalle : detalleArtList) {
                cant += Integer.parseInt(detalle.get("cantidad").toString());
            }
        }
//        PedidoCab recepc = pedidoCabDAO.getByOC(txtOC.getText());
        FacturaCompraCab fcc = new FacturaCompraCab();
        fcc.setTipoMovimiento("CONTADO");
        fcc.setNroOrden(null);
        fcc.setNroDoc(txtNroFactura.getText());
        try {
            java.sql.Date date = java.sql.Date.valueOf(dpFecha.getValue());
            fcc.setFechaDoc(date);
        } catch (Exception e) {
            java.sql.Date date = new java.sql.Date(new java.util.Date().getTime());
            fcc.setFechaDoc(date);
        } finally {
        }
        fcc.setSc(false);

//        if (chkCentral.isSelected()) {
        fcc.setCc(true);
//            NUEVO HAY QUE SACAR DESPUES
//        fcc.setSc(true);
//        } else {
//            fcc.setCc(false);
//        }
//        if (chkCacique.isSelected()) {
//            fcc.setSc(true);
//            //            NUEVO HAY QUE SACAR DESPUES
//            fcc.setCc(true);
//        }
//        else {
        //            NUEVO HAY QUE SACAR DESPUES
//        fcc.setSc(false);
//        }
//        if (chkSanLorenzo.isSelected()) {
//            fcc.setSl(true);
//        } else {
        fcc.setSl(false);
//        }

        fcc.setCantidad(cant + "");
        fcc.setSucursal(chkEmpresa.getSelectionModel().getSelectedItem());
        fcc.setObservacion(txtObservacion.getText());

        try {
            fcc.setTipoDocumento(txtTipoMovimiento.getText());
        } catch (Exception e) {
            fcc.setTipoDocumento(null);
        } finally {
        }

        fcc.setClaseDocumento("MERCADERIA");
        fcc.setTotal(Integer.parseInt(numValidator.numberValidator(txtGsTotal.getText())));
        fcc.setSaldo(Integer.parseInt(numValidator.numberValidator(txtGsTotal.getText())));
        fcc.setEstadoFactura("C");
        fcc.setOc(null);
        fcc.setPedidoCab(null);

        try {
            Proveedor pro = new Proveedor();
            pro.setIdProveedor(Long.parseLong(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString()));
            fcc.setProveedor(pro);
        } catch (Exception e) {
            fcc.setProveedor(null);
        } finally {
        }
        FacturaCompraCab fccDATA = facturaCompraCabDAO.insertarObtenerObj(fcc);

        if (detalleArtList.size() > 0) {
            long i = 0;
            for (JSONObject detalle : detalleArtList) {
                i++;
                FacturaCompraDet fcd = gson.fromJson(detalle.toString(), FacturaCompraDet.class
                );
                FacturaCompraCab fccDetalle = new FacturaCompraCab();
                fccDetalle.setIdFacturaCompraCab(fccDATA.getIdFacturaCompraCab());
                fcd.setFacturaCompraCab(fccDetalle);
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(detalle.get("articulo").toString());
                    long idArt = Long.parseLong(jsonArt.get("idArticulo").toString());
                    long idDeposito = depositoDAO.getByDescripcion(detalle.get("deposito").toString());

                    Existencia existencia = exisDAO.filtrarPorArtDeposito(idArt, idDeposito);
                    if (existencia != null) {
//                        Existencia exis = new Existencia();
                        existencia.setCantidad((Long.parseLong(detalle.get("cantidad").toString()) + existencia.getCantidad()));
                        exisDAO.actualizar(existencia);
                    } else {
                        Existencia exis = new Existencia();
                        exis.setCantidad(Long.parseLong(detalle.get("cantidad").toString()));
                        Articulo art = new Articulo();
                        art.setIdArticulo(idArt);
                        Deposito dep = new Deposito();
                        dep.setIdDeposito(idDeposito);
                        exis.setArticulo(art);
                        exis.setDeposito(dep);
                        exisDAO.insertar(exis);
                    }
                    try {
                        fcdDAO.insercionMasiva(fcd, i);
                        LocalDate nowLast = LocalDate.now();
                        boolean cc = true;
                        boolean sc = true;
                        boolean sl = false;
                        if (chkSanLorenzo.isSelected()) {
                            sl = true;
                            cc = false;
                            sc = false;
                        }
                        ArticuloCompraMatriz acm = artCompraMatrizDAO.getByCodFecha(detalle.get("codArticulo").toString(), nowLast, cc, sc, sl);
                        if (acm != null) {
                            long canti = acm.getCantidad() + Long.parseLong(detalle.get("cantidad").toString());
                            acm.setCantidad(canti);
                            artCompraMatrizDAO.actualizar(acm);
                        } else {
                            ArticuloCompraMatriz artComp = new ArticuloCompraMatriz();
                            artComp.setCodigo(detalle.get("codArticulo").toString());
                            artComp.setCantidad(Long.parseLong(detalle.get("cantidad").toString()));
                            artComp.setFecha(java.sql.Date.valueOf(nowLast));
                            artComp.setCc(cc);
                            artComp.setSc(sc);
                            artComp.setSl(sl);

                            artCompraMatrizDAO.insertar(artComp);
                        }
                    } catch (Exception e) {
                        System.out.println("-> " + e.getLocalizedMessage());
                        System.out.println("-> " + e.getMessage());
                        System.out.println("-> " + e.fillInStackTrace());
                    } finally {
                    }

                } catch (ParseException ex) {
                    System.out.println("-> " + ex.getLocalizedMessage());
                    System.out.println("-> " + ex.getMessage());
                    System.out.println("-> " + ex.fillInStackTrace());
                }

            }
//            recepc.setTotal(Long.parseLong(txtGsTotal.getText()));
//            recepc.setSaldo(Long.parseLong(txtGsTotal.getText()));
//            pedidoCabDAO.actualizar(recepc);
            limpiarCampos();

//            secondPane1.setVisible(false);
            mensajeAlerta("COMPRA HA SIDO AGREGADO EXITOSAMENTE...");

            btnVerificarPrecio.setDisable(true);

            toaster = new Toaster();
            btnVerificarPrecio.setDisable(true);
            tableViewFactura.getItems().clear();
            detalleArtList = new ArrayList<>();
            orden = 0;
//            ubicandoContenedorSecundario();
//            cargarTipoMovimiento();
//            cargarDeposito();
//            cargarTipo();
//            cargarMedida();
//            cargarSucursal();
//            cargarSituacion();
//            cargarClaseMovimiento();
            repeatFocus(txtNroEntrada);
        } else {
            mensajeAlerta("NO SE HA CARGADO NINGUN DETALLE A LA FACTURA");
        }
    }

    public void limpiarCampos() {
        detalleArtList = new ArrayList<>();
        tableViewFactura.getItems().clear();
        mapeoCheck = new HashMap();
        orden = 0;
        txtMonExtranjeraExenta.setText("");
        txtMonExtranjeraGrav5.setText("");
        txtMonExtranjeraGrav10.setText("");
        txtMonExtranjeraIva5.setText("");
        txtMonExtranjeraIva10.setText("");
        txtMonExtranjeraTotal.setText("");

        txtGsExenta.setText("");
        txtGsGrav5.setText("");
        txtGsGrav10.setText("");
        txtGsIva5.setText("");
        txtGsIva10.setText("");
        txtGsTotal.setText("");

        txtGsExentaManual.setText("");
        txtGsGrav5Manual.setText("");
        txtGsGrav10Manual.setText("");
        txtGsIva5Manual.setText("");
        txtGsIva10Manual.setText("");
        txtGsTotalManual.setText("");

        chkDepDestino.setValue("CC");
        chkTipo.setValue("MER");
        chkMedida.setValue("UNIDAD");
        txtCodigo.setText("");
        txtContenido.setText("");
        txtNroPedido.setText("");
        txtCantidad.setText("");
        txtExistencia.setText("");
        txtDescuento.setText("");
        txtGasto.setText("");
        txtCosto.setText("");
        txtDescriIva.setText("");
        txtPorcIva.setText("");
        txtExenta.setText("");
        txtGravada.setText("");

        txtSec1.setText("");
        txtDescripcion.setText("");
        txtSec2.setText("");

        chkTipoMovimiento.setValue("CO");
        chkClaseMov.setValue("MER");
        chkSituacion.setValue("CONTRIBUYENTE");
        chkEmpresa.setValue("CASA CENTRAL");
        txtTipoMovimiento.setText("");
        txtProveedorDoc.setText("");
        txtProveedor.setText("");
        txtNroTimbrado.setText("");
        txtRucProveedor.setText("");
        txtNroEntrada.setText("");
        txtObservacion.setText("");
        txtClaseMov.setText("");
        txtNroFactura.setText("");
        txtFechaFactura.setText("");
        txtVtoDias.setText("");
        txtFechaRecepcion.setText("");
        txtNroDespacho.setText("");
        txtNroPedidoSis.setText("");

        txtMonedaExtran.setText("");
        txtEntregaInicial.setText("");
        txtCantCuota.setText("");
        txtCuota.setText("");

        LocalDate now = LocalDate.now();
        dpFecha.setValue(now);
//        tableViewFactura = new TableView<>();
    }

    private void buscarProveedor() {
        BuscarProveedorFXMLController.cargarDatos(txtRucProveedor, txtProveedor, txtNroTimbrado);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void buscarOrden() {
        BuscarRecepcionFXMLController.cargarOrden(txtNroEntrada);
        this.sc.loadScreenModal("/vista/stock/BuscarRecepcionFXML.fxml", 454, 450, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void verificarPrecio() {
        if (!txtCodigo.getText().equals("")) {
            ReplicaFacturaCompraFXMLController.setCodArt(txtCodigo.getText());
            this.sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
        } else {
            mensajeAlerta("NO SE HA CARGADO NINGUN CODIGO DE ARTICULO");
        }
    }

    @FXML
    private void txtCantidadKeyPress(KeyEvent event) {
        listenCantidad(event);
    }

    private void listenCantidad(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
            txtCantidad.textProperty().addListener((observable, oldValue, newValue) -> {
                Platform.runLater(() -> {
                    calcularCosto(newValue);
                });
            });
        }
    }

    private void calcularCosto(String newValue) {
        if (!newValue.equals("")) {
            if (txtPorcIva.getText().equalsIgnoreCase("0")) {
                txtDescriIva.setText("EXE");
                txtPorcIva.setText(0 + "");
                long total = Long.parseLong(txtCantidad.getText()) * Long.parseLong(txtCosto.getText());
                txtExenta.setText(total + "");
            } else {
                txtDescriIva.setText("GRA");
                txtPorcIva.setText(txtPorcIva.getText());
                long iva = 0;
                if (txtPorcIva.equals("5")) {
                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 21);
                    iva = Long.parseLong(txtCosto.getText()) - iva;
                    iva = iva * Long.parseLong(txtCantidad.getText());
                } else {
                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 11);
                    iva = Long.parseLong(txtCosto.getText()) - iva;
                    iva = iva * Long.parseLong(txtCantidad.getText());
                }
                txtGravada.setText(iva + "");
            }
        }
    }

    private void cargarDetalleByOC(String oc) {
        PedidoCab pc = pedidoCabDAO.listarPorOrden(oc);
//        FacturaCompraCab pc = facturaCompraCabDAO.getByOC(oc);
        List<PedidoCompra> listPedido = pedidoCompraDAO.listarPorCabecera(pc.getIdPedidoCab());
        long sinIva = 0;
        long sumExenta = 0;
        long sumGra5 = 0;
        long sumGra10 = 0;
        long sumGravadaCinco = 0;
        long sumGravadaDiez = 0;
        for (PedidoCompra pd : listPedido) {
            long cantidad = 0;
            switch (chkEmpresa.getSelectionModel().getSelectedItem()) {
                case "CASA CENTRAL":
                    cantidad = pd.getCantCc();
                    break;
                case "SAN LORENZO":
                    cantidad = pd.getCantSl();
                    break;
                default:
                    cantidad = pd.getCantSc();
                    break;
            }
            if (cantidad != 0) {
                JSONObject jsonDetalle = new JSONObject();
                JSONObject jsonRecep = new JSONObject();
                jsonRecep.put("idPedidoCab", pc.getIdPedidoCab());

                JSONObject jsonArt = new JSONObject();
                jsonArt.put("idArticulo", pd.getArticulo().getIdArticulo());

                jsonDetalle.put("pedidoCab", jsonRecep);
                jsonDetalle.put("articulo", jsonArt);

                jsonDetalle.put("cantidad", cantidad);
                jsonDetalle.put("precio", pd.getPrecio());
                jsonDetalle.put("total", (pd.getPrecio() * cantidad));
                jsonDetalle.put("descripcion", pd.getDescripcion());
                if (!pd.getExenta().equals("0")) {
                    jsonDetalle.put("poriva", "0");
                    long exe = 0L;
                    if (Long.parseLong(pd.getCantidad()) != 0L) {
                        exe = Long.parseLong(pd.getExenta()) / Long.parseLong(pd.getCantidad());
                    }
                    jsonDetalle.put("iva", (exe * cantidad));
                    sinIva += (exe * cantidad);
                    sumExenta += (exe * cantidad);
                } else if (!pd.getGrav5().equals("0")) {
                    jsonDetalle.put("poriva", "5");
                    long gr = 0L;
                    if (Long.parseLong(pd.getCantidad()) != 0L) {
                        gr = Long.parseLong(pd.getGrav5()) / Long.parseLong(pd.getCantidad());
                    }
//                long gr = Long.parseLong(pd.getGrav5()) / Long.parseLong(pd.getCantidad());
                    jsonDetalle.put("iva", (gr * cantidad));
                    sinIva += gr * cantidad;
                    sumGra5 += gr * cantidad;
                    sumGravadaCinco += cantidad * pd.getPrecio();
//                sumGravadaCinco += Long.parseLong(pd.getCantidad()) * pd.getPrecio();
                } else {
                    jsonDetalle.put("poriva", "10");
                    long gr = 0L;
                    if (Long.parseLong(pd.getCantidad()) != 0L) {
                        gr = Long.parseLong(pd.getGrav10()) / Long.parseLong(pd.getCantidad());
                    }
//                long gr = Long.parseLong(pd.getGrav10()) / Long.parseLong(pd.getCantidad());
                    jsonDetalle.put("iva", (gr * cantidad));
                    sinIva += gr * cantidad;
                    sumGra10 += gr * cantidad;
                    sumGravadaDiez += cantidad * pd.getPrecio();
//                sumGravadaDiez += Long.parseLong(pd.getCantidad()) * pd.getPrecio();
                }
                orden++;

                FacturaCompraDet factCompra = factCompraDetDAO.getByLastCodigo(Long.parseLong(pd.getArticulo().getCodArticulo()));

                jsonDetalle.put("orden", orden);
                if (factCompra == null) {
                    jsonDetalle.put("sec1", "");
                    jsonDetalle.put("sec2", "");
                } else {
                    jsonDetalle.put("sec1", factCompra.getSec1());
                    jsonDetalle.put("sec2", factCompra.getSec2());
                }
                jsonDetalle.put("codArticulo", pd.getArticulo().getCodArticulo());
                jsonDetalle.put("deposito", pd.getPedidoCab().getSucursal());
                jsonDetalle.put("tipo", "MER");
                jsonDetalle.put("medida", "UNIDAD");
                jsonDetalle.put("contenido", "1");
                jsonDetalle.put("existencia", "0");
                jsonDetalle.put("descuento", pd.getDescuento());

                detalleArtList.add(jsonDetalle);
                vistaJSONObjectArtDet();
            }
        }
        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(sumExenta + "")));
        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(sumGra5 + "")));
        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(sumGra10 + "")));
        long iva5 = sumGravadaCinco - sumGra5;
//        long iva5 = Math.round(sumGra5 * 1.05);
//        iva5 -= sumGra5;
        txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
        long iva10 = sumGravadaDiez - sumGra10;
//        long iva10 = Math.round(sumGra10 * 1.1);
//        iva10 -= sumGra10;
        txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
        sinIva += iva5 + iva10;
        txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(sinIva + "")));
    }

    @FXML
    private void txtOCKeyPressed(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void chkClaseMovAction(ActionEvent event) {
        escuchandoClaseMovimiento();
    }

    private void escuchandoClaseMovimiento() {
        if (chkClaseMov.getSelectionModel().getSelectedIndex() >= 0) {
            if (chkClaseMov.getSelectionModel().getSelectedItem().equalsIgnoreCase("MER")) {
                txtClaseMov.setText("MERCADERIA");
            } else if (chkClaseMov.getSelectionModel().getSelectedItem().equalsIgnoreCase("SER")) {
                txtClaseMov.setText("SERVICIO");
            } else {
                txtClaseMov.setText("DESCRIPCION");
            }
        }
    }

    private void escuchandoTipoMovimiento() {
        if (chkTipoMovimiento.getSelectionModel().getSelectedIndex() >= 0) {
            if (chkTipoMovimiento.getSelectionModel().getSelectedItem().equalsIgnoreCase("CR")) {
                txtTipoMovimiento.setText("CREDITO");
            } else if (chkTipoMovimiento.getSelectionModel().getSelectedItem().equalsIgnoreCase("CO")) {
                txtTipoMovimiento.setText("CONTADO");
            }
//            else {
//                txtTipoMovimiento.setText("DESCRIPCION");
//            }
        }
    }

    @FXML
    private void chkTipoMovimientoAction(ActionEvent event) {
        escuchandoTipoMovimiento();
    }

    private void recargarDatos() {
        //        vistaJSONObjectArtDet();
        List<JSONObject> array = getDetalleArtList();
        detalleArtList = new ArrayList<>();
        tableViewFactura.getItems().clear();
//        orden = 0;
//        limpiarCampos();
        try {
            long exenta = 0;
            long gravada10 = 0;
            long gravada5 = 0;
            for (JSONObject jSONObject : array) {
                if (edicion.equalsIgnoreCase("cantidad") || edicion.equalsIgnoreCase("precio")) {
                    long total = (Long.parseLong(jSONObject.get("cantidad").toString()) * Long.parseLong(jSONObject.get("precio").toString()));
                    jSONObject.put("total", total);
                } else if (edicion.equalsIgnoreCase("total")) {
                    long precio = (Long.parseLong(jSONObject.get("total").toString()) / Long.parseLong(jSONObject.get("cantidad").toString()));
                    jSONObject.put("precio", precio);
                }
                detalleArtList.add(jSONObject);
                //NUEVA LINEA
//                mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), true);
                try {
                    boolean val = Boolean.parseBoolean(String.valueOf(mapeoCheck.get(jSONObject.get("orden").toString() + "-" + jSONObject.get("codArticulo").toString())));
                    if (val) {
                        switch (jSONObject.get("poriva").toString()) {
                            case "0":
                                exenta += Long.parseLong(jSONObject.get("total").toString());
                                break;
                            case "5":
                                gravada5 += Long.parseLong(jSONObject.get("total").toString());
                                break;
                            case "10":
                                gravada10 += Long.parseLong(jSONObject.get("total").toString());
                                break;
                            default:
                                break;
                        }
                    }
                } catch (Exception e) {
                } finally {
                }
            }
            vistaJSONObjectArtDet();
            long iva5 = Math.round(gravada5 / 21);
            long grav5 = gravada5 - iva5;
            long iva10 = Math.round(gravada10 / 11);
            long grav10 = gravada10 - iva10;
            long total = exenta + iva5 + grav5 + iva10 + grav10;

            txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
            txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
            txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
            txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
            txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));
            estadoValor = true;

        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            System.out.println("-->> " + e.getMessage());
            mensajeAlerta("DATOS CARGADOS INCORRECTAMENTE...");
            detalleArtList = array;
            vistaJSONObjectArtDet();
        } finally {
        }
    }

    private void abrirBusquedaOC() {
        txtCod.setText("");
//        tableViewCompra.getItems().clear();
        orden = 0;
        anchorPane3Sub11.setDisable(true);
        secondPane1.setDisable(true);
        panelBusquedaCompra.setVisible(true);
        panelBusquedaCompra.setDisable(false);
        txtOrdenCompra.setText(txtOC.getText());
        repeatFocus(txtCod);
    }

    @FXML
    private void tableViewCompraKeyReleased(KeyEvent event) {
    }

    @FXML
    private void textFieldNombreClienteNuevoKeyReleased(KeyEvent event) {
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        anchorPane3Sub11.setDisable(false);
        secondPane1.setDisable(false);
        panelBusquedaCompra.setVisible(false);
        mantenerSeleccionadosOC();
        tableViewFactura.getItems().clear();
        vistaJSONObjectArtDet();
        repeatFocus(txtOC);
    }

    @FXML
    private void buttonVolverAction(ActionEvent event) {
        anchorPane3Sub11.setDisable(false);
        secondPane1.setDisable(false);
        panelBusquedaCompra.setVisible(false);
        repeatFocus(txtOC);
    }

    @FXML
    private void buttonBuscarAction(ActionEvent event) {
        buscandoOC();
    }

    @FXML
    private void buttonBuscarArtAction(ActionEvent event) {
        buscarArt();
    }

    @FXML
    private void tableViewCompraKeyReleased2(KeyEvent event) {
    }

    @FXML
    private void txtTotalManualKeyPressed(KeyEvent event) {
        keyPressEventos(event);

    }

    @FXML
    private void tableViewFacturaMouseClicked(MouseEvent event) {
//        tableViewFactura.getSelectionModel().getSelectedIndex();
//        tableViewFactura.getTab
//        getTableRow().setStyle("-fx-alignment: CENTER; -fx-background-color:  #3399CC; -fx-font-weight: bold;");
    }

    class BooleanCell extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCell(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
//                    table.getSelectionModel().select(getTableRow().getIndex());
//                    int posicion = tableViewFactura.getSelectionModel().getSelectedIndex();
                    int posicion = getTableRow().getIndex();
                    System.out.println("NEW: " + newValue + " OLD: " + oldValue);
                    if (posicion >= 0) {
//                        String ord = tableViewFactura.getSelectionModel()
//                                .getSelectedItem().get("orden") + "-" + tableViewFactura.getSelectionModel()
//                                .getSelectedItem().get("codigo");
                        String ord = tableViewFactura.getItems().get(posicion)
                                .get("orden") + "-" + tableViewFactura.getItems().get(posicion)
                                .get("codArticulo");
                        if (newValue) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
                            long exenta = 0;
                            long gravada10 = 0;
                            long gravada5 = 0;
                            switch (tableViewFactura.getItems().get(posicion).get("poriva").toString()) {
                                case "0":
                                    exenta += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                case "5":
                                    gravada5 += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                case "10":
                                    gravada10 += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                default:
                                    break;
                            }
                            try {
                                if (mapeoCheck.size() == 1) {
                                    txtGsIva5.setText("Gs 0");
                                    txtGsIva10.setText("Gs 0");
                                    txtGsGrav5.setText("Gs 0");
                                    txtGsGrav10.setText("Gs 0");
                                    txtGsExenta.setText("Gs 0");
                                }
                                if (!estadoValor) {
                                    if (!val) {
                                        //                                vistaJSONObjectArtDet();
                                        long iva5 = Math.round(gravada5 / 21);
                                        long grav5 = gravada5 - iva5;
                                        long iva10 = Math.round(gravada10 / 11);
                                        long grav10 = gravada10 - iva10;
                                        System.out.println("1) -->> " + txtGsIva5.getText());
                                        System.out.println("2) -->> " + txtGsIva10.getText());
                                        System.out.println("3) -->> " + txtGsGrav5.getText());
                                        System.out.println("4) -->> " + txtGsGrav10.getText());
                                        iva5 += Long.parseLong(numValidator.numberValidator(txtGsIva5.getText()));
                                        iva10 += Long.parseLong(numValidator.numberValidator(txtGsIva10.getText()));
                                        grav5 += Long.parseLong(numValidator.numberValidator(txtGsGrav5.getText()));
                                        grav10 += Long.parseLong(numValidator.numberValidator(txtGsGrav10.getText()));
                                        exenta += Long.parseLong(numValidator.numberValidator(txtGsExenta.getText()));

                                        long total = exenta + iva5 + grav5 + iva10 + grav10;

                                        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
                                        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
                                        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
                                        txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
                                        txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
                                        txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));
                                    } else {
                                        val = false;
                                    }
                                } else {
                                    estadoValor = false;
                                    val = true;
                                }
                                setTextFill(Color.CHOCOLATE);
                                setStyle("-fx-alignment: CENTER; -fx-background-color: #3399CC; -fx-fill: black; -fx-font-weight: bold;");

//                                getTableRow().setStyle("-fx-alignment: CENTER; -fx-background-color: yellow");
                                getTableRow().setStyle("-fx-alignment: CENTER; -fx-background-color:  #3399CC; -fx-fill: black; -fx-font-weight: bold;");
//                                getTableRow().setStyle("-fx-text-fill:  #000000");
//                                tableViewFactura.getColumns().get(2)//.setStyle("-fx-alignment: CENTER; -fx-background-color: yellow");
                                tableViewFactura.getSelectionModel().getSelectedItem().put("check", true);
                            } catch (Exception e) {
                                System.out.println("-->> " + e.getLocalizedMessage());
                                System.out.println("-->> " + e.getMessage());
//            mensajeAlerta("DATOS CARGADOS INCORRECTAMENTE...");
//            detalleArtList = array;
//            vistaJSONObjectArtDet();
                            } finally {
                            }
                        } else if (!newValue) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
//                            System.out.println(ord + " | " + false);
                            long exenta = 0;
                            long gravada10 = 0;
                            long gravada5 = 0;
                            switch (tableViewFactura.getItems().get(posicion).get("poriva").toString()) {
                                case "0":
                                    exenta += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                case "5":
                                    gravada5 += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                case "10":
                                    gravada10 += Long.parseLong(tableViewFactura.getItems().get(posicion).get("total").toString());
                                    break;
                                default:
                                    break;
                            }
                            try {
//                                vistaJSONObjectArtDet();
                                long iva5 = Math.round(gravada5 / 21);
                                long grav5 = gravada5 - iva5;
                                long iva10 = Math.round(gravada10 / 11);
                                long grav10 = gravada10 - iva10;

                                iva5 = Long.parseLong(numValidator.numberValidator(txtGsIva5.getText())) - iva5;
                                iva10 = Long.parseLong(numValidator.numberValidator(txtGsIva10.getText())) - iva10;
                                grav5 = Long.parseLong(numValidator.numberValidator(txtGsGrav5.getText())) - grav5;
                                grav10 = Long.parseLong(numValidator.numberValidator(txtGsGrav10.getText())) - grav10;
                                exenta = Long.parseLong(numValidator.numberValidator(txtGsExenta.getText())) - exenta;

                                long total = exenta + iva5 + grav5 + iva10 + grav10;

                                txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
                                txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
                                txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
                                txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
                                txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
                                txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));

                                tableViewFactura.getSelectionModel().getSelectedItem().put("check", false);
                            } catch (Exception e) {
                                System.out.println("-->> " + e.getLocalizedMessage());
                                System.out.println("-->> " + e.getMessage());
//            mensajeAlerta("DATOS CARGADOS INCORRECTAMENTE...");
//            detalleArtList = array;
//            vistaJSONObjectArtDet();
                            } finally {
                            }
                            setTextFill(Color.BLACK);
//                            getStyleClass().clear();
//                            setStyle(null);
//                            setStyle(".table-row-cell:selected; -fx-font-weight: bold;");
                            setStyle("-fx-background-color: WHITE; -fx-fill: black; -fx-font-weight: bold;");
//                            tableViewFactura.getColumns().get(2).setStyle("-fx-alignment: CENTER; -fx-background-color: WHITE");
                            getTableRow().setStyle("-fx-background-color: WHITE; -fx-fill: black; -fx-font-weight: bold;");
                        }
                    }
                }
            }
            );
//            this.setGraphic(checkBox);
//            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            this.setEditable(true);
        }

        @Override

        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    private void mantenerSeleccionadosOC() {
        suprimirSeleccionesOC();
    }

    private void suprimirSeleccionesOC() {
//        JSONParser parser = new JSONParser();
////        int val = 0;
////        long tam = tableViewCompra.getItems().size();
//        List aEliminar = new ArrayList();
//        categoriasList = new ArrayList<>();
//        long exenta = 0;
//        long gravada10 = 0;
//        long gravada5 = 0; //        ObservableList<JSONObject> ol = tableViewCompra.getItems();
//        for (JSONObject item : tableViewCompra.getItems()) {
////            org.json.JSONObject json = new org.json.JSONObject(item);
//            JSONObject jsonArti = new JSONObject();
//            try {
////                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
//                boolean estado = Boolean.parseBoolean(mapeoCheck.get(item.get("orden").toString() + "-" + item.get("codArticulo").toString()).toString());
//                if (estado == false) {
//                    aEliminar.add(item);
//                } else {
//                    switch (item.get("poriva").toString()) {
//                        case "0":
//                            exenta += Long.parseLong(item.get("total").toString());
//                            break;
//                        case "5":
//                            gravada5 += Long.parseLong(item.get("total").toString());
//                            break;
//                        case "10":
//                            gravada10 += Long.parseLong(item.get("total").toString());
//                            break;
//                        default:
//                            break;
//                    }
//                }
//            } catch (Exception e) {
////                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
////                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
//                aEliminar.add(item);
//            } finally {
//            }
////            columnOpcion.setVisible(false);
//        }
//        long iva5 = Math.round(gravada5 / 21);
//        long grav5 = gravada5 - iva5;
//        long iva10 = Math.round(gravada10 / 11);
//        long grav10 = gravada10 - iva10;
//        long total = exenta + iva5 + grav5 + iva10 + grav10;
//
//        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
//        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
//        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
//        txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
//        txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
//        txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));
//
//        for (int i = 0; i < aEliminar.size(); i++) {
//            try {
//                System.out.println("-> " + aEliminar.get(i));
//                getDetalleArtList().remove(aEliminar.get(i));
////                matrizList.remove(aEliminar.get(i));a
//                System.out.println("-STATUS : " + tableViewCompra.getItems().remove(aEliminar.get(i)));
//            } catch (Exception e) {
//            } finally {
//            }
//        }
    }

    private void mantenerSeleccionados() {
        //        if (!estadoCheck) {
        //            long total = 0;
        suprimirSelecciones();
        Map mapping = new HashMap();
        List<JSONObject> list = new ArrayList<>();
        long exenta = 0;
        long gravada10 = 0;
        long gravada5 = 0;
        for (JSONObject json : getDetalleArtList()) {
            org.json.JSONObject jsonDat = new org.json.JSONObject(json);
            mapeoCheck.put(jsonDat.getInt("orden") + "-" + jsonDat.getBigInteger("codArticulo"), true);
            switch (json.get("poriva").toString()) {
                case "0":
                    exenta += Long.parseLong(json.get("total").toString());
                    break;
                case "5":
                    gravada5 += Long.parseLong(json.get("total").toString());
                    break;
                case "10":
                    gravada10 += Long.parseLong(json.get("total").toString());
                    break;
                default:
                    break;
            }
        }
        try {
            vistaJSONObjectArtDet();
            long iva5 = Math.round(gravada5 / 21);
            long grav5 = gravada5 - iva5;
            long iva10 = Math.round(gravada10 / 11);
            long grav10 = gravada10 - iva10;
            long total = exenta + iva5 + grav5 + iva10 + grav10;

            txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(exenta + "")));
            txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav5 + "")));
            txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(grav10 + "")));
            txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva5 + "")));
            txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(iva10 + "")));
            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(total + "")));

        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            System.out.println("-->> " + e.getMessage());
//            mensajeAlerta("DATOS CARGADOS INCORRECTAMENTE...");
//            detalleArtList = array;
//            vistaJSONObjectArtDet();
        } finally {
        }
//        int ord = 1;
//        for (JSONObject jSONObject : list) {
//            jSONObject.put("orden", ord);
//            categoriasList.add(jSONObject);
//            ord++;
//        }
        tableViewFactura.getItems().clear();
        vistaJSONObjectArtDet();
//        actualizandoTablaMatriz();
//        actualizarTablaCategoria();
//        chkLocal.setSelected(true);
//        cbSucursal.getSelectionModel().select("CASA CENTRAL");
//        cbImpresion.getSelectionModel().select("--SELECCIONE IMPRESION--");
//        } else {
//            suprimirSelecciones();
//        }
        mensajeAlerta("DATOS CONFIRMADOS...");
    }

    private void suprimirSelecciones() {
        JSONParser parser = new JSONParser();
//        int val = 0;
//        long tam = tableViewFactura.getItems().size();
        List aEliminar = new ArrayList();
        categoriasList = new ArrayList<>();
//        ObservableList<JSONObject> ol = tableViewFactura.getItems();
        for (JSONObject item : tableViewFactura.getItems()) {
//            org.json.JSONObject json = new org.json.JSONObject(item);
            JSONObject jsonArti = new JSONObject();
            try {
//                jsonArti = (JSONObject) parser.parse(json.get("articulo").toString());
                boolean estado = Boolean.parseBoolean(mapeoCheck.get(item.get("orden").toString() + "-" + item.get("codArticulo").toString()).toString());
                if (estado == false) {
//                    System.out.println("-STATUS : " + tableViewFactura.getItems().remove(item));
//                    hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                    hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                    aEliminar.add(item);
                }
            } catch (Exception e) {
//                hashJsonArtDet.remove(Long.parseLong(jsonArti.get("idArticulo").toString()));//si
//                hashJsonArticulo.remove(Long.parseLong(jsonArti.get("codArticulo").toString()));//sia
                aEliminar.add(item);
            } finally {
            }
//            columnOpcion.setVisible(false);
        }

        for (int i = 0; i < aEliminar.size(); i++) {
            try {
                System.out.println("-> " + aEliminar.get(i));
                detalleArtList.remove(aEliminar.get(i));
//                matrizList.remove(aEliminar.get(i));a
                System.out.println("-STATUS : " + tableViewFactura.getItems().remove(aEliminar.get(i)));
            } catch (Exception e) {
            } finally {
            }
        }
//        actualizarTotales(false);
    }

    private void buscandoOC() {
        mapeoCheck = new HashMap();
        txtGsExenta.setText("Gs 0");
        txtGsGrav5.setText("Gs 0");
        txtGsGrav10.setText("Gs 0");
        txtGsIva5.setText("Gs 0");
        txtGsIva10.setText("Gs 0");
        txtGsTotal.setText("Gs 0");
        int dato = 0;
        if (chkCentral.isSelected()) {
            dato++;
        }
        if (chkCacique.isSelected()) {
            dato++;
        }
        if (chkSanLorenzo.isSelected()) {
            dato++;
        }
        if (dato >= 1) {
//            int orden = 0;
            PedidoCab pc = pedidoCabDAO.listarPorOrden(txtOC.getText());
            List<PedidoCompra> listPedido = pedidoCompraDAO.listarPorCabecera(pc.getIdPedidoCab());

//            List<JSONObject> array = getDetalleArtList();
            detalleArtList = new ArrayList<>();
            tableViewCompra.getItems().clear();
            tableViewFactura.getItems().clear();
            mapeoCheck = new HashMap();

//            long sinIva = 0;
//            long sumExenta = 0;
//            long sumGra5 = 0;
//            long sumGra10 = 0;
//            long sumGravadaCinco = 0;
//            long sumGravadaDiez = 0;
            orden = 0;

            for (PedidoCompra pd : listPedido) {
                long cantidad = 0;
                if (chkTodo.isSelected()) {
                    if (chkCentral.isSelected()) {
                        cantidad += pd.getCantCc();
                    }
                    if (chkCacique.isSelected()) {
                        cantidad += pd.getCantSc();
                    }
                    if (chkSanLorenzo.isSelected()) {
                        cantidad += pd.getCantSl();
                    }
                } else {
                    if (chkCentral.isSelected()) {
                        cantidad += pd.getCantCc();
                        cantidad = cantidad - factCompraDetDAO.listarPorOCAndCodigoCC(txtOC.getText(), Long.parseLong(pd.getArticulo().getCodArticulo()));
                    }
                    if (chkCacique.isSelected()) {
                        cantidad += pd.getCantSc();
                        cantidad = cantidad - factCompraDetDAO.listarPorOCAndCodigoSC(txtOC.getText(), Long.parseLong(pd.getArticulo().getCodArticulo()));
                    }
                    if (chkSanLorenzo.isSelected()) {
                        cantidad += pd.getCantSl();
                        cantidad = cantidad - factCompraDetDAO.listarPorOCAndCodigoSL(txtOC.getText(), Long.parseLong(pd.getArticulo().getCodArticulo()));
                    }
                }

                if (cantidad > 0) {
                    JSONObject jsonDetalle = new JSONObject();
                    JSONObject jsonRecep = new JSONObject();
                    jsonRecep.put("idPedidoCab", pc.getIdPedidoCab());

                    JSONObject jsonArt = new JSONObject();
                    jsonArt.put("idArticulo", pd.getArticulo().getIdArticulo());

                    jsonDetalle.put("pedidoCab", jsonRecep);
                    jsonDetalle.put("articulo", jsonArt);

                    jsonDetalle.put("cantidad", cantidad);
                    long prec = 0;
                    if (pd.getArticulo().getIva().getPoriva() == 5) {
                        prec = pd.getPrecio() - (pd.getPrecio() / 21);
                    } else if (pd.getArticulo().getIva().getPoriva() == 10) {
                        prec = pd.getPrecio() - (pd.getPrecio() / 11);
                    }
                    jsonDetalle.put("precio", prec);
                    jsonDetalle.put("total", (prec * cantidad));
                    jsonDetalle.put("descripcion", pd.getDescripcion());
                    Articulo art = artDAO.buscarCod(pd.getArticulo().getCodArticulo() + "");
                    System.out.println(orden + ") " + art.getCosto() + " - " + prec);
                    if (String.valueOf(art.getCosto()).equalsIgnoreCase(String.valueOf(pd.getPrecio()))) {
                        jsonDetalle.put("diferencia", "");
                    } else {
                        jsonDetalle.put("diferencia", "*");
                    }

                    if (!pd.getExenta().equals("0")) {
                        jsonDetalle.put("poriva", "0");
                        long exe = 0L;
                        if (Long.parseLong(pd.getCantidad()) != 0L) {
                            exe = Long.parseLong(pd.getExenta()) / Long.parseLong(pd.getCantidad());
                        }
                        jsonDetalle.put("iva", (exe * cantidad));
//                        sinIva += (exe * cantidad);
//                        sumExenta += (exe * cantidad);
                    } else if (!pd.getGrav5().equals("0")) {
                        jsonDetalle.put("poriva", "5");
                        long gr = 0L;
                        if (Long.parseLong(pd.getCantidad()) != 0L) {
                            gr = Long.parseLong(pd.getGrav5()) / Long.parseLong(pd.getCantidad());
                        }
//                long gr = Long.parseLong(pd.getGrav5()) / Long.parseLong(pd.getCantidad());
                        jsonDetalle.put("iva", (gr * cantidad));
//                        sinIva += gr * cantidad;
//                        sumGra5 += gr * cantidad;
//                        sumGravadaCinco += cantidad * pd.getPrecio();
//                sumGravadaCinco += Long.parseLong(pd.getCantidad()) * pd.getPrecio();
                    } else {
                        jsonDetalle.put("poriva", "10");
                        long gr = 0L;
                        if (Long.parseLong(pd.getCantidad()) != 0L) {
                            gr = Long.parseLong(pd.getGrav10()) / Long.parseLong(pd.getCantidad());
                        }
//                long gr = Long.parseLong(pd.getGrav10()) / Long.parseLong(pd.getCantidad());
                        jsonDetalle.put("iva", (gr * cantidad));
//                        sinIva += gr * cantidad;
//                        sumGra10 += gr * cantidad;
//                        sumGravadaDiez += cantidad * pd.getPrecio();
//                sumGravadaDiez += Long.parseLong(pd.getCantidad()) * pd.getPrecio();
                    }
                    orden++;

                    FacturaCompraDet factCompra = factCompraDetDAO.getByLastCodigo(Long.parseLong(pd.getArticulo().getCodArticulo()));

                    jsonDetalle.put("orden", orden);
                    jsonDetalle.put("orden", orden);
                    if (factCompra == null) {
                        jsonDetalle.put("sec1", "");
                        jsonDetalle.put("sec2", "");
                    } else {
                        jsonDetalle.put("sec1", factCompra.getSec1());
                        jsonDetalle.put("sec2", factCompra.getSec2());
                    }
                    jsonDetalle.put("codArticulo", pd.getArticulo().getCodArticulo());
                    jsonDetalle.put("deposito", pd.getPedidoCab().getSucursal());
                    jsonDetalle.put("tipo", "MER");
                    jsonDetalle.put("medida", "UNIDAD");
                    jsonDetalle.put("contenido", "1");
                    jsonDetalle.put("existencia", "0");
                    jsonDetalle.put("descuento", pd.getDescuento());

                    detalleArtList.add(jsonDetalle);
                }
            }
            vistaJSONObjectArtDet();

//            txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
//            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
            mensajeAlerta("DATOS CARGADOS EXITOSAMENTE...");
        } else if (dato == 0) {
            mensajeAlerta("DEBE SELECCIONAR UNA SUCURSAL PARA REALIZAR EL FILTRO DE BUSQUEDA...");
        } else if (dato > 1) {
            mensajeAlerta("DEBE SELECCIONAR UNA SOLA SUCURSAL PARA REALIZAR EL FILTRO DE BUSQUEDA...");
        }
        Platform.runLater(() -> {
            txtGsExenta.setText("Gs 0");
            txtGsGrav5.setText("Gs 0");
            txtGsGrav10.setText("Gs 0");
            txtGsIva5.setText("Gs 0");
            txtGsIva10.setText("Gs 0");
            txtGsTotal.setText("Gs 0");
        });
    }

    private void buscarArt() {
        for (int i = 0; i < tableViewFactura.getItems().size(); i++) {
            JSONObject json = (JSONObject) tableViewFactura.getItems().get(i);
            if (json.get("codArticulo").toString().equalsIgnoreCase(txtCod.getText())) {
                anchorPane3Sub11.setDisable(false);
                secondPane1.setDisable(false);
                panelBusquedaCompra.setVisible(false);
                repeatFocus(tableViewFactura);
                tableViewFactura.requestFocus();
                tableViewFactura.getSelectionModel().select(i);
                tableViewFactura.getFocusModel().focus(i);
                tableViewFactura.scrollTo(i);
                break;
            }
        }
    }

    public void vistaJSONObjectArtDetOC() {
        numValidator = new NumberValidator();
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        tableColumnOpc.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>> booleanCellFactory
                = new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> p) {
                return new BooleanCellOC(tableViewCompra);
            }
        };
        tableColumnOpc.setCellValueFactory((data) -> {
            org.json.JSONObject json = new org.json.JSONObject(data.getValue());
            try {
                if (Boolean.parseBoolean(mapeoCheck.get(json.getInt("orden") + "-" + json.getBigInteger("codArticulo")).toString())) {
                    return new SimpleBooleanProperty(true);
                } else {
                    return new SimpleBooleanProperty(false);
                }
            } catch (Exception e) {
                return new SimpleBooleanProperty(false);
            } finally {
            }
        });
        tableColumnOpc.setCellFactory(booleanCellFactory);
        //columna Sección..............................................
        tableColumnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
//                return new SimpleStringProperty(data.getValue().get("tipo").toString());
//            }
//        });
        tableColumnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codArticulo").toString());
            }
        });
        tableColumnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        tableColumnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        tableColumnCantidad.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        tableColumnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        tableColumnTotal.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        tableColumnTotal.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                String ruc = String.valueOf(data.getValue().get("total"));
                if (ruc.contentEquals("null")) {
                    ruc = "";
                }
//                return new ReadOnlyStringWrapper(ruc);
                return new ReadOnlyStringWrapper(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(ruc + ""))));
            }
        });
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> (t.getTableView().getItems().get(
//                        t.getTablePosition().getRow())).put("cantidad", t.getNewValue())
//        );
//        columnCantidad.setOnEditCommit(
//                (TableColumn.CellEditEvent<JSONObject, String> t)
//                -> getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", t.getNewValue())
//        );
//        columnCantidad.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "cantidad";
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("cantidad", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("cantidad", numValidator.numberValidator(t.getNewValue()));
//            }
//        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
//        columnMedida.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("medida").toString());
//            }
//        });
        tableColumnCosto.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
        tableColumnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                prec = data.getValue().get("precio").toString();
//                long prec = (Long.parseLong(data.getValue().get("precio").toString()) * Long.parseLong(data.getValue().get("cantidad").toString()));
//                tot = prec + "";
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(prec + "")));
            }
        });
//        columnTotal.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "total";
//                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("total", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("total", numValidator.numberValidator(t.getNewValue()));
//                if (!numValidator.numberValidator(tot).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
//                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
//                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                }
////                ReplicaFacturaCompraFXMLController.setCodArt(getDetalleArtList().get(t.getTablePosition().getRow()).get("codArticulo").toString());
////                sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//            }
//        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
//        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("medida").toString());
//            }
//        });
//        columnContenido.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("contenido").toString());
//            }
//        });
//        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("descuento").toString());
//            }
//        });
//        tableColumnCosto.setStyle("-fx-alignment: CENTER-RIGHT; -fx-font-weight: bold;");
//        tableColumnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                prec = data.getValue().get("precio").toString();
//                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("precio").toString()))));
////                return new SimpleStringProperty(data.getValue().get("precio").toString());
//            }
//        });
//        columnCosto.setOnEditCommit(new EventHandler<TableColumn.CellEditEvent<JSONObject, String>>() {
//            @Override
//            public void handle(TableColumn.CellEditEvent<JSONObject, String> t) {
//                edicion = "precio";
//                List<JSONObject> detalle = getDetalleArtList();
//                (t.getTableView().getItems().get(t.getTablePosition().getRow())).put("precio", numValidator.numberValidator(t.getNewValue()));
//                getDetalleArtList().get(t.getTablePosition().getRow()).put("precio", numValidator.numberValidator(t.getNewValue()));
//                if (!numValidator.numberValidator(prec).equalsIgnoreCase(numValidator.numberValidator(t.getNewValue()))) {
//                    ReplicaFacturaCompraFXMLController.setCodArt(detalle.get(t.getTablePosition().getRow()).get("codArticulo").toString());
//                    sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 1355, 631, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                }
//            }
//        });
//        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty("0");
//                } else {
//                    return new SimpleStringProperty(data.getValue().get("poriva").toString());
//                }
//            }
//        });
//        columnExenta.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(data.getValue().get("iva").toString()))));
////                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf("0"))));
////                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnGravada.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) != 0) {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(data.getValue().get("iva").toString())));
////                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
//                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble("0")));
////                    return new SimpleStringProperty("0");
//                }
//            }
//        });
//        columnDeposito.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
//        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                return new SimpleStringProperty(data.getValue().get("deposito").toString());
//            }
//        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewCompra.setItems(articuloDetData);
        //**********************************************************************

    }

    class BooleanCellOC extends TableCell<JSONObject, Boolean> {

        private CheckBox checkBox;

        public BooleanCellOC(TableView table) {
            checkBox = new CheckBox();
//            checkBox.setDisable(true);
            checkBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                    if (isEditing()) {
//                        commitEdit(newValue == null ? false : newValue);
//                    }
                    estadoCheck = true;
//                    table.getSelectionModel().select(getTableRow().getIndex());
//                    int posicion = tableViewCompra.getSelectionModel().getSelectedIndex();
                    int posicion = getTableRow().getIndex();
                    if (posicion >= 0) {
//                        String ord = tableViewCompra.getSelectionModel()
//                                .getSelectedItem().get("orden") + "-" + tableViewCompra.getSelectionModel()
//                                .getSelectedItem().get("codigo");
                        String ord = tableViewCompra.getItems().get(posicion)
                                .get("orden") + "-" + tableViewCompra.getItems().get(posicion)
                                .get("codArticulo");
                        if (checkBox.isSelected()) {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, true);
//                            System.out.println(ord + " | " + true);
                            tableViewCompra.getSelectionModel().getSelectedItem().put("check", true);
                        } else {
                            mapeoCheck.remove(ord);
                            mapeoCheck.put(ord, false);
//                            System.out.println(ord + " | " + false);
                            tableViewCompra.getSelectionModel().getSelectedItem().put("check", false);
                        }
                    }
                }
            });
//            this.setGraphic(checkBox);
//            this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
//            this.setEditable(true);
        }

        @Override
        public void startEdit() {
            super.startEdit();
            if (isEmpty()) {
                return;
            }
            checkBox.setDisable(false);
            checkBox.requestFocus();
        }

        @Override
        public void cancelEdit() {
            super.cancelEdit();
            checkBox.setDisable(true);
        }

        public void commitEdit(Boolean value) {
            super.commitEdit(value);
            checkBox.setDisable(true);
        }

        @Override
        public void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!isEmpty()) {
                checkBox.setSelected(item);
                this.setGraphic(checkBox);
                this.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                this.setEditable(true);
            }
        }
    }

    private void listenTextField(TextField txtMontoApertura) {
//        txtMontoApertura.setStyle("-fx-font-size: 14px; -fx-font-weight: bold;");
        txtMontoApertura.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (oldValue.length() == 0) {
//                patternMonto = true;
//            }
            String newV = "";
            String oldV = "";
            if (!newValue.contentEquals("")) {
                newV = numValidator.numberValidator(newValue);
                oldV = numValidator.numberValidator(oldValue);
                long lim = -1l;
                boolean limite = true;
                if (!newV.contentEquals("")) {//límite del monto...
                    if (newV.length() < 19) {
                        lim = Long.valueOf(newV);
                        if (lim >= 0 && lim < 2147483647) {
                            limite = false;
                        }
                    }
                }
                if (limite) {
                    if (!newValue.contentEquals("Gs ") && !newV.contentEquals("")) {
                        if (patternMonto) {
                            if (oldV.length() != 0) {
                                param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                            } else {
                                param = oldValue;
                            }
                            patternMonto = false;
                        } else {
                            patternMonto = true;
                            param = oldValue;
                        }
                        Platform.runLater(() -> {
                            txtMontoApertura.setText(param);
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            txtMontoApertura.setText("");
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    }
                } else if (!intValidator.isValid(newV)) {//si es la primera vez con letra o la última vez...
                    if (!oldValue.contentEquals("")) {
                        if (!newValue.contentEquals("Gs ")) {
                            if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                                if (patternMonto) {
                                    param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(oldV));
                                    patternMonto = false;
                                } else {
                                    patternMonto = true;
                                    param = oldValue;
                                }
                                Platform.runLater(() -> {
                                    txtMontoApertura.setText(param);
                                    txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                                });
                            }
                        } else {
                            Platform.runLater(() -> {
                                txtMontoApertura.setText("");
                                txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                            });
                        }
                    } else {
                        Platform.runLater(() -> {
                            txtMontoApertura.setText(oldValue);
                            txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                        });
                    }
                } else if (GenericValidator.isInRange(Integer.valueOf(newV), 0, 2147483647)) {
                    if (patternMonto) {
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(newV));
                        patternMonto = false;
                    } else {
                        patternMonto = true;
                        param = numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(newValue)));
                    }
                    Platform.runLater(() -> {
                        txtMontoApertura.setText(param);
                        txtMontoApertura.positionCaret(txtMontoApertura.getLength());
                    });
                }
            }
        });
    }
}
