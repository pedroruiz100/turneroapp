package com.javafx.controllers.stock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.peluqueria.MainApp;
import com.javafx.controllers.caja.*;
import com.peluqueria.core.domain.Articulo;
import com.peluqueria.core.domain.ArticuloNf1Tipo;
import com.peluqueria.core.domain.ArticuloNf2Sfamilia;
import com.peluqueria.core.domain.ArticuloNf3Sseccion;
import com.peluqueria.core.domain.ArticuloNf4Seccion1;
import com.peluqueria.core.domain.ArticuloNf5Seccion2;
import com.peluqueria.core.domain.ArticuloNf6Secnom6;
import com.peluqueria.core.domain.ArticuloNf7Secnom7;
import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.core.domain.PedidoCab;
import com.peluqueria.core.domain.PedidoCompra;
import com.peluqueria.core.domain.PedidoDet;
import com.peluqueria.core.domain.Proveedor;
import com.peluqueria.core.domain.TalonariosSucursales;
import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.ArticuloNf1TipoDAO;
import com.peluqueria.dao.ArticuloNf2SfamiliaDAO;
import com.peluqueria.dao.ArticuloNf3SseccionDAO;
import com.peluqueria.dao.ArticuloNf4Seccion1DAO;
import com.peluqueria.dao.ArticuloNf5Seccion2DAO;
import com.peluqueria.dao.ArticuloNf6Secnom6DAO;
import com.peluqueria.dao.ArticuloNf7Secnom7DAO;
import com.peluqueria.dao.DepositoDAO;
import com.peluqueria.dao.ExistenciaDAO;
import com.peluqueria.dao.FacturaCompraDetDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.PedidoCabDAO;
import com.peluqueria.dao.PedidoCompraDAO;
import com.peluqueria.dao.PedidoDetDAO;
import com.peluqueria.dao.RangoDetalleDAO;
import com.peluqueria.dao.RangoFacturaDAO;
import com.peluqueria.dao.RangoOrdenCompraDAO;
import com.peluqueria.dao.RangoPedidoDAO;
import com.peluqueria.dao.RecepcionDAO;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.peluqueria.dao.TipoMonedaDAO;
import com.peluqueria.dao.impl.ArticuloNf1TipoDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf2SfamiliaDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf3SseccionDAOImpl;
import com.peluqueria.dao.impl.ArticuloNf4Seccion1DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf5Seccion2DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf6Secnom6DAOImpl;
import com.peluqueria.dao.impl.ArticuloNf7Secnom7DAOImpl;
import com.peluqueria.dao.impl.FacturaCompraDetDAOImpl;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.peluqueria.dao.impl.PedidoCabDAOImpl;
import com.peluqueria.dao.impl.RangoDetalleDAOImpl;
import com.peluqueria.dao.impl.RangoFacturaDAOImpl;
import com.peluqueria.dao.impl.RangoOrdenCompraDAOImpl;
import com.peluqueria.dao.impl.RangoPedidoDAOImpl;
import com.peluqueria.dao.impl.RecepcionDAOImpl;
import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.jrviewer.JRViewerFXML;
import com.javafx.jrviewer.JRViewerFXMLMode;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.CajaDeDatos;
import com.javafx.util.ConexionParana;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.DatosEnCaja;
import com.javafx.util.FacturaVentaDatos;
import com.javafx.util.Identity;
import com.javafx.util.NumberValidator;
import com.javafx.util.PATH;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import com.javafx.util.VentasUtiles;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Callback;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author
 */
@Controller
public class PedidoCompraFXMLController extends BaseScreenController implements Initializable {

    public static void setTextFieldCompAnt(String aTextFieldCompAnt) {
        textFieldCompAnt = aTextFieldCompAnt;
    }

    public static boolean isActualizarDatosCabecera() {
        return actualizarDatosCabecera;
    }

    public static void setActualizarDatosCabecera(boolean actualizarDatosCabecera) {
        PedidoCompraFXMLController.actualizarDatosCabecera = actualizarDatosCabecera;
    }

    public static List<JSONObject> getCotizacionList() {
        return cotizacionList;
    }

    public static int getPeso() {
        return peso;
    }

    public static int getReal() {
        return real;
    }

    public static int getDolar() {
        return dolar;
    }

    public static List<JSONObject> getDetalleArtList() {
        return detalleArtList;
    }

    public static Long getPrecioTotal() {
        return precioTotal;
    }

    public static JSONObject getCabFactura() {
        return cabFactura;
    }

    public static void setCabFactura(JSONObject aCabFactura) {
        cabFactura = aCabFactura;
    }

    public static boolean isCancelacionProd() {
        return cancelacionProd;
    }

    private static JSONObject cabFactura;
//    private static Stage stageData = new Stage();

    static void setCliente(String ruc, String nombre) {
        nomCli = nombre;
        rucCli = ruc;
    }
    String nomProv = "";
    String idProv = "";

//    public static void setSecondStage(Stage secondStage) {
//        stageData = secondStage;
//        StageSecond.setStageData(secondStage);
//    }
    Toaster toaster;
    boolean enterEstado = false;
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private static boolean dato = false;
    private static boolean facturaVentaEstado = false;
    public static JSONObject facturaCabeceraSupr = new JSONObject();
    public static boolean actualizarDatosCabecera;
    public static Map mapArticulosEnPromo;
    public static ArrayList arrayListadoPromo;
    public static ArrayList arrayListadoPromoConDesc;
    public static Map mapPromoConDesc;
    public static long descPromo;
    public static int cantPromo;
    public static ArticuloNf1TipoDAO anf1TipoDAO = new ArticuloNf1TipoDAOImpl();
    public static ArticuloNf2SfamiliaDAO anf2SfamiliaDAO = new ArticuloNf2SfamiliaDAOImpl();
    public static ArticuloNf3SseccionDAO anf3SeccionDAO = new ArticuloNf3SseccionDAOImpl();
    public static ArticuloNf4Seccion1DAO anf4SeccionDAO = new ArticuloNf4Seccion1DAOImpl();
    public static ArticuloNf5Seccion2DAO anf5SeccionDAO = new ArticuloNf5Seccion2DAOImpl();
    public static ArticuloNf6Secnom6DAO anf6SeccionDAO = new ArticuloNf6Secnom6DAOImpl();
    public static ArticuloNf7Secnom7DAO anf7SeccionDAO = new ArticuloNf7Secnom7DAOImpl();
    public static FacturaCompraDetDAO fcdDAO = new FacturaCompraDetDAOImpl();

    String nivel01 = "";
    String nivel02 = "";
    String nivel03 = "";
    String nivel04 = "";
    String nivel05 = "";
    String nivel06 = "";
    String nivel07 = "";

    static String posicion;

    Alert alertCerrarTurno = null;
    Alert alertArqueo = null;

    static HashMap<Long, JSONObject> hmGift;

    public static HashMap<Long, JSONObject> getHmGift() {
        return hmGift;
    }

    public static void setHmGift(HashMap<Long, JSONObject> hmGift) {
        PedidoCompraFXMLController.hmGift = hmGift;
    }

    public JSONObject objArticulo;

    @Autowired
    private ArticuloDAO artDAO;
    @Autowired
    private ExistenciaDAO existenciaDAO;
    @Autowired
    private PedidoDetDAO pedidoDetDAO;
    @Autowired
    private PedidoCompraDAO pedidoCompraDAO;

    private List<JSONObject> depositoList = new ArrayList<>();

    private ObservableList<JSONObject> depositoData;

    @Autowired
    private TipoMonedaDAO tipoMonedaDAO;
    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static RangoFacturaDAO rangoDAO = new RangoFacturaDAOImpl();
    private static RangoOrdenCompraDAO rangoOCDAO = new RangoOrdenCompraDAOImpl();
    private static RangoDetalleDAO rangoDetalleDAO = new RangoDetalleDAOImpl();
    private static RecepcionDAO recepcionDAO = new RecepcionDAOImpl();
    private static PedidoCabDAO pedidoCabDAO = new PedidoCabDAOImpl();
    static JSONObject datos = new JSONObject();
    ManejoLocal manejoLocal = new ManejoLocal();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocal manejo = new ManejoLocal();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    private static String textFieldCompAnt;
    private JSONArray tipoMonedaJSONArray;
    private static List<JSONObject> cotizacionList;
    private static Long precioTotal;
    private static int peso;
    private static int real;
    private static int dolar;
    //para evitar inconvenientes con el message alert enter y escape...
    private boolean alert;
    //primera inserción
    private boolean primeraInsercion;
    public static int orden;
    //primera inserción
    //lector de código
    private static String codBarra;
    private static String codDecimal;
    //lector de código
    private static NumberValidator numValidator;
    private static HashMap<Long, Integer> hashJsonArtDet;
    private static HashMap<Long, JSONObject> hashJsonArticulo;
    JSONObject tipoCaja;
    public static List<JSONObject> detalleArtList;
    //TABLE VIEW
    private ObservableList<JSONObject> articuloDetData;
    static JSONParser parser = new JSONParser();
    //TABLE VIEW
    private ReentrantLock lock = new ReentrantLock();
    Image image;
    public static boolean cancelacionProd;
    private static boolean cancelacionProdPrimera;
    private static int idFact;
    public static boolean valorIngreso = false;
    private SimpleDateFormat formatador = new SimpleDateFormat("hh:mm:ss a");

    static String rucCli;
    static String nomCli;
    static String sucursal = "";
    static long idPedidoCab;

    @Autowired
    DepositoDAO depositoDAO;

    final KeyCombination altN = new KeyCodeCombination(KeyCode.M, KeyCombination.ALT_DOWN);
    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    public static String codArt = "";

    public static String nroPedido = "";
    public static String moneda = "";
    public static String cotizacion = "";
    public static String diaVisita = "";
    public static String cuota = "";
    public static String plazo = "";
    public static String observa = "";
    public static String frecuencia = "";
    public static String tipoDoc = "";

    public static String getCodArt() {
        return codArt;
    }

    private static RangoPedidoDAO rangoPedidoDAO = new RangoPedidoDAOImpl();

    public static void setCodArt(String codArt) {
        PedidoCompraFXMLController.codArt = codArt;
    }

//    final KeyCombination altI = new KeyCodeCombination(KeyCode.I, KeyCombination.ALT_DOWN);
    @FXML
    private AnchorPane anchorPaneFactura;
    @FXML
    private Label labelTotal11;
    @FXML
    private ImageView imageViewLogo;
    @FXML
    private Pane secondPane;
    @FXML
    private TextField txtCodVendedor;
    @FXML
    private Pane secondPane1;
    @FXML
    private TextField txtCodVendedor1;
    @FXML
    private Label labelSupervisor111111121;
    @FXML
    private Label labelSupervisor1112211;
    @FXML
    private Label labelSupervisor123;
    @FXML
    private Label labelSupervisor111111122;
    @FXML
    private Label labelSupervisor1111111221;
    @FXML
    private Label labelSupervisor11111112211;
    @FXML
    private Label labelSupervisor111111122111;
    @FXML
    private Label labelSupervisor1111111221111;
    @FXML
    private Label labelSupervisor11111112211111;
    @FXML
    private AnchorPane anchorPane3Sub11;
    @FXML
    private TextField txtClaseMov;
    @FXML
    private Label labelRazonTransportista;
    @FXML
    private Label labelSupervisor11111;
    @FXML
    private TextField txtObservacion;
    @FXML
    private Label labelSupervisor111111;
    @FXML
    private Label labelSupervisor;
    @FXML
    private Label labelSupervisor1;
    @FXML
    private Button btnProveedor;
    @FXML
    private TextField txtProveedorDoc;
    @FXML
    private TextField txtProveedor;
    @FXML
    private Label labelSupervisor111;
    @FXML
    private ComboBox<String> chkClaseMov;
    @FXML
    private Label labelSupervisor121;
    @FXML
    private Label labelSupervisor122;
    @FXML
    private Label labelSupervisor1211;
    @FXML
    private Label labelSupervisor1112;
    @FXML
    private Label labelSupervisor111221;
    @FXML
    private Label labelSupervisor11121;
    @FXML
    private Label labelSupervisor111211;
    @FXML
    private Label labelSupervisor1221;
    @FXML
    private Label labelSupervisor12211;
    @FXML
    private TextField txtContenido;
    @FXML
    private TextField txtNroPedido;
    @FXML
    private TextField txtSec1;
    @FXML
    private Button btnSeccion;
    @FXML
    private TextField txtSec2;
    @FXML
    private Button btnNose;
    @FXML
    private Button btnCargarVtoProd;
    @FXML
    private Button btnBorrarDetalle;
    @FXML
    private Button btnBuscarOrdenCompra;
    @FXML
    private Button btnEditar;
    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnBorrar;
    @FXML
    private Button btnNuevo;
    @FXML
    private TextField txtMonExtranjeraExenta;
    @FXML
    private TextField txtGsExenta;
    @FXML
    private TextField txtMonExtranjeraGrav5;
    @FXML
    private TextField txtGsGrav5;
    @FXML
    private TextField txtMonExtranjeraGrav10;
    @FXML
    private TextField txtGsGrav10;
    @FXML
    private TextField txtMonExtranjeraIva5;
    @FXML
    private TextField txtGsIva5;
    @FXML
    private TextField txtMonExtranjeraIva10;
    @FXML
    private TextField txtGsIva10;
    @FXML
    private TextField txtMonExtranjeraTotal;
    @FXML
    private TextField txtGsTotal;
    @FXML
    private ComboBox<String> chkSituacion;
    @FXML
    private ChoiceBox<String> chkEmpresa;
    @FXML
    private CheckBox chkOtros;
    @FXML
    private TextField txtNroPedidoSis;
    @FXML
    private ChoiceBox<String> chkMonedaExtran;
    @FXML
    private Button btnCargar;
    @FXML
    private TextField txtMonedaExtran;
    @FXML
    private TextField txtEntregaInicial;
    @FXML
    private TextField txtCantCuota;
    @FXML
    private TextField txtCuota;
    @FXML
    private Button btnGenerar;
    @FXML
    private Button btnSalir;
    @FXML
    private TableView<JSONObject> tableViewFactura;
    @FXML
    private TableColumn<JSONObject, String> columnOrden;
    @FXML
    private TableColumn<JSONObject, String> columnCodigo;
    @FXML
    private TableColumn<JSONObject, String> columnDescripcion;
    @FXML
    private TableColumn<JSONObject, String> columnIva;
    @FXML
    private TableColumn<JSONObject, String> columnExenta;
    @FXML
    private TableColumn<JSONObject, String> columnGravada;
    @FXML
    private TableColumn<JSONObject, String> columnPeso;
    @FXML
    private TableColumn<JSONObject, String> columnTipo;
    @FXML
    private TableColumn<JSONObject, String> columnCantidad;
    @FXML
    private TableColumn<JSONObject, String> columnMedida;
    @FXML
    private TableColumn<JSONObject, String> columnContenido;
    @FXML
    private TableColumn<JSONObject, String> columnDescuento;
    @FXML
    private TableColumn<JSONObject, String> columnCosto;
    @FXML
    private TableColumn<JSONObject, String> columnDeposito;
    @FXML
    private TextField txtProveedorDoc1;
    @FXML
    private TextField txtProveedorDoc11;
    @FXML
    private TextField txtProveedorDoc111;
    @FXML
    private Label labelSupervisor1212;
    @FXML
    private TextField txtProveedorDoc12;
    @FXML
    private Label labelSupervisor111212;
    @FXML
    private Button btnNuevoArt;
    @FXML
    private Button btnEditarArt;
    @FXML
    private Button btnEliminarArt;
    @FXML
    private TextField txtPedido;
    @FXML
    private TextField txtFechaPedido;
    @FXML
    private TextField txtPlazo;
    @FXML
    private TextField txtCuotas;
    @FXML
    private ChoiceBox<String> chkDiasVisita;
    @FXML
    private TextField txtFrecuencia;
    @FXML
    private ChoiceBox<String> chkTipoDoc;
    @FXML
    private Label labelOrdenCompra;
    @FXML
    private TextField txtRucProveedor;
    @FXML
    private TextField txtNroTimbrado;
    @FXML
    private Button btnProcesar;
    @FXML
    private Button btnImprimir;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadCC;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadSL;
    @FXML
    private TableColumn<JSONObject, String> columnCantidadSC;
    @FXML
    private TextField txtIdProveedor;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    @SuppressWarnings("ConvertToStringSwitch")
    public void initialize(URL url, ResourceBundle rb) {
        chkMonedaExtran.setValue("--SELECCIONE MONEDA--");
        cargarDias();
        cargarTipoDocu();
        cargarTipo();
        cargarMedida();
        cargarSucursal();
        cargarSituacion();
        cargarClaseMovimiento();
        ubicandoContenedorSecundario();
        cargarMoneda();
        numValidator = new NumberValidator();
        switch (ScreensContoller.getFxml()) {
            case "/vista/stock/ModCantidadSucursalFXML.fxml": {
                txtPedido.setText(nroPedido);
                buscarRecep();
                chkDiasVisita.setValue(diaVisita);
                chkTipoDoc.setValue(tipoDoc);
                txtMonedaExtran.setText(cotizacion);
                chkMonedaExtran.setValue(moneda);
                txtCuotas.setText(cuota);
                txtPlazo.setText(plazo);
                txtFrecuencia.setText(frecuencia);
                txtObservacion.setText(observa);
                //                cargarDetalle(pedidoCabDAO.getByNroOrden(txtPedido.getText()).getIdPedidoCab());
                calcularTotales();
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        tableViewFactura.requestFocus();
                        tableViewFactura.getSelectionModel().select(Integer.parseInt(posicion));
                        tableViewFactura.getFocusModel().focus(Integer.parseInt(posicion));
                        tableViewFactura.scrollTo(Integer.parseInt(posicion));
                    }
                });

                break;
            }
            case "/vista/stock/CodArticuloFXML.fxml": {
                txtPedido.setText(nroPedido);
                buscarRecep();
                chkDiasVisita.setValue(diaVisita);
                txtMonedaExtran.setText(cotizacion);
                chkMonedaExtran.setValue(moneda);
                chkTipoDoc.setValue(tipoDoc);
                txtCuotas.setText(cuota);
                txtPlazo.setText(plazo);
                txtFrecuencia.setText(frecuencia);
                txtObservacion.setText(observa);
                //                cargarDetalle(pedidoCabDAO.getByNroOrden(txtPedido.getText()).getIdPedidoCab());
                calcularTotales();

                break;
            }
            default:
                toaster = new Toaster();
//        btnVerificarPrecio.setDisable(true);
                detalleArtList = new ArrayList<>();
                depositoList = new ArrayList<>();
                orden = 0;
//        cargarTipoMovimiento();
                labelOrdenCompra.setText(rangoOCDAO.recuperarActual());
                repeatFocus(txtPedido);
        }
        txtProveedor.setText(nomProv);
        txtIdProveedor.setText(idProv);
        cargandoImagen();
//            } catch (ParseException ex) {
//                Utilidades.log.error("ParseException", ex.fillInStackTrace());
//        } catch (IOException ex) {
//            Utilidades.log.error("IOException", ex.fillInStackTrace());
//        }
//        }
    }

    private void cargarDias() {
        chkDiasVisita.getItems().addAll("DOMINGO", "LUNES", "MARTES", "MIERCOLES", "JUEVES", "VIERNES", "SABADO");
//        chkTipoMovimiento.getItems().addAll("CR", "CO");
    }

    private void cargarTipoDocu() {
        chkTipoDoc.getItems().addAll("CO", "CC");
    }

    private void cargarTipo() {
//        chkTipo.getItems().addAll("MER", "SER", "DESC");
    }

    private void cargarMedida() {
//        chkMedida.getItems().addAll("UNIDAD", "KG");
    }

    private void cargarSucursal() {
        chkEmpresa.getItems().addAll("CASA CENTRAL", "SAN LORENZO", "CACIQUE");
    }

    private void cargarSituacion() {
        chkSituacion.getItems().addAll("CONTRIBUYENTE", "OTROS");
    }

    private void cargarClaseMovimiento() {
        chkClaseMov.getItems().addAll("MER", "SER", "DESC");
    }

    @FXML
    private void btnCerrarAction(ActionEvent event) {
        cargarDetalle(idPedidoCab);
    }

    private void btnRetiroDineroAction(ActionEvent event) {
        verificarMontoFacturado();
    }

    private void btnCerrarTurnoAction(ActionEvent event) {
        if (!verificandoCaidaFormaPago()) {
            resetTray();
            verificandoDatosCierre();
        }
    }

    @FXML
    private void anchorPaneFacturaKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void anchorPaneFacturaKeyPressed(KeyEvent event) {

    }

    private void textFieldCodKeyReleased(KeyEvent event) {
        keyPressTextCod(event);
    }

    private void cargandoInicial() throws ParseException, IOException {
//        mapArticulosEnPromo = new HashMap();
//        arrayListadoPromo = new ArrayList();
//        arrayListadoPromoConDesc = new ArrayList();
//        mapPromoConDesc = new HashMap();
//        descPromo = 0;
//        cantPromo = 0;
//
//        hmGift = new HashMap<>();
//
//        if (StageSecond.getStageData()
//                .isShowing()) {
//            StageSecond.getStageData().close();
//        }
//
//        if (ScreensContoller.getFxml()
//                .contentEquals("/vista/caja/FormasDePagoFXML.fxml")) {
//            toaster.mensajeDiario();
////            toaster.mensajeFactCierreDet(MensajeFinalVenta.getVueltoPopUp(), MensajeFinalVenta.getClientePopUp(), 5);
//            toaster.mensajeDeNavidadAnhoNuevo();
//        }
//        objArticulo = new JSONObject();
//        facturaVentaEstado = false;
//
//        setActualizarDatosCabecera(
//                false);
//        cargandoImagen();
//        facturaCabeceraSupr = new JSONObject();
//        numValidator = new NumberValidator();
//        boolean estado = false;
//
//        if (DatosEnCaja.getDatos()
//                != null) {
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = new JSONObject();
//            if (DatosEnCaja.getFacturados() == null) {
//                fact = new JSONObject();
//            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
//                fact = new JSONObject();
//            } else {
//                fact = DatosEnCaja.getFacturados();
//                estado = true;
//            }
//            //SETEAR CAMPOS PRIMERAMENTE
//            //**--VERIFICANDO--**
//            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
//            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
//            tipoCaja = (JSONObject) caj.get("tipoCaja");
//            txtNumCaja.setText("N°Caja: " + caj.get("descripcion").toString());
//            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
//            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
//            if (jsonFuncionario != null) {
//                String nomFuncionario = "";
//                String apeFuncionario = "";
//                if (jsonFuncionario.get("nombre") != null) {
//                    nomFuncionario = jsonFuncionario.get("nombre").toString();
//                }
//                if (jsonFuncionario.get("apellido") != null) {
//                    apeFuncionario = jsonFuncionario.get("apellido").toString();
//                }
//                labelCajeroFunc.setText("Cajero: " + nomFuncionario);
//                labelCajeroFunc2.setText(apeFuncionario);
//            } else {
//                labelCajeroFunc.setText("Cajero: N/A");
//            }
////            iniciandoCotizacion();
//            //FIN DEL SETEO DE CAMPOS
//            cargandoDatosIniciales();
//            if (estado) {
//                cargandoDetalleManeraLocal();
//            }
//        }
//        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
//
//        if (jsonDatos.isNull(
//                "rendicion")) {
//            datos.put("rendicion", false);
//        }
////        try {
////            JSONObject empresa = (JSONObject) parser.parse(datos.get("empresa").toString());
////            txtNumRuc.setText(empresa.get("ruc").toString());
////        } catch (ParseException ex) {
////            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
////        }
//
//        if (!hashJsonArticulo.isEmpty()) {
//            if (!jsonDatos.isNull("exentaGlobal")) {
//                chkExtranjero.setSelected(true);
//            }
//            chkExtranjero.setDisable(true);
//        } else {
//            chkExtranjero.setDisable(false);
//        }
    }

    private void repeatFocus(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocus(node);
            }
        });
    }

    private void cargandoImagen() {
        File file = new File(PATH.PATH_LOGO_VENTA);
        this.image = new Image(file.toURI().toString());
        this.imageViewLogo.setImage(this.image);
    }

    static void iniciandoFactCab() {
        cabFactura = new JSONObject();
        cabFactura = creandoJsonFactCab();
        cancelacionProd = false;
        cancelacionProdPrimera = true;
    }

    private void listenFactura() {
        tableViewFactura.addEventFilter(KeyEvent.ANY, new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                if (event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.CONTROL) {
                }
            }
        });

//        textFieldCant.textProperty().addListener((observable, oldValue, newValue) -> {
//            if (!newValue.contentEquals("")) {
//                try {
//                    if (GenericValidator.isDouble(newValue)) {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(newValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    } else {
//                        Platform.runLater(() -> {
//                            textFieldCant.setText(oldValue);
//                            textFieldCant.positionCaret(textFieldCant.getLength());
//                            if (!textFieldCant.getText().endsWith(".")) {
//                                if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                    textFieldCant.setText("1");
//                                    mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                                }
//                            }
//                        });
//                    }
//                } catch (NumberFormatException e) {
//                    Platform.runLater(() -> {
//                        textFieldCant.setText(oldValue);
//                        textFieldCant.positionCaret(textFieldCant.getLength());
//                        if (!textFieldCant.getText().endsWith(".")) {
//                            if (Double.parseDouble(textFieldCant.getText()) > 999) {
//                                textFieldCant.setText("1");
//                                mensajeErrorAlertModal("EXCEDIÓ EL LÍMITE CANTIDAD.");
//                            }
//                        }
//                    });
//                }
//            }
//        });
    }

    private void mensajeAlerta(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorArt(String msj) {
        toaster.mensajeGenericoError("Mensaje del Sistema", msj, true, 2);
    }

    private void cargandoDetalleManeraLocal() {
        try {
            resetMapeo();
            detalleArtList = new ArrayList<>();
            JSONParser parser = new JSONParser();
            JSONArray facturaDetalle = (JSONArray) parser.parse(fact.get("facturaDetalle").toString());
            JSONObject facturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
            cabFactura = new JSONObject();
            org.json.JSONObject jsonFact = new org.json.JSONObject(facturaCabecera);
            try {
                if (tableViewFactura.getItems().isEmpty()) {
                    if (!jsonFact.isNull("montoFactura")) {
                        facturaCabecera.remove("montoFactura");
                        fact.put("facturaClienteCab", facturaCabecera);
                    } else {
                        cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                    }
                } else {
                    cabFactura.put("montoFactura", facturaCabecera.get("montoFactura").toString());
                }
                valorIngreso = true;
            } catch (Exception e) {
                mensajeError2("DEBE GENERAR EL INFORME FINANCIERO PARA VOLVER A INICIAR SESION COMO CAJERO");
                valorIngreso = false;
            }
            if (valorIngreso) {
                cabFactura.put("estadoFactura", facturaCabecera.get("estadoFactura").toString());
                cabFactura.put("nroActual", facturaCabecera.get("nroActual").toString());
                cabFactura.put("idFacturaClienteCab", facturaCabecera.get("idFacturaClienteCab").toString());
                cabFactura.put("nroFactura", facturaCabecera.get("nroFactura").toString());
                primeraInsercion = false;
                for (int i = 0; i < facturaDetalle.size(); i++) {
                    JSONObject detalleArticulo = (JSONObject) parser.parse(facturaDetalle.get(i).toString());
                    JSONObject articulo = (JSONObject) parser.parse(detalleArticulo.get("articulo").toString());
                    hashJsonArticulo.put((Long.parseLong(articulo.get("codArticulo").toString())), articulo);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long.parseLong(articulo.get("idArticulo").toString())), detalleArtList.lastIndexOf(detalleArticulo));

                    cargandoCamposInterfaceLocal(detalleArticulo);
                    orden++;
                }
                CajaDeDatos.generandoNroComprobante();
                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                // primer trío
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                long nroActual = talos.getNroActual();
                JSONObject jsonFacturaCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);

                File file = new File(PATH.PATH_NO_IMG);
                Image image = new Image(file.toURI().toString());
//                centerImage();
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (!detalleArtList.isEmpty()) {
            Platform.runLater(() -> tableViewFactura.scrollTo(detalleArtList.size() - 1));
        }
    }

    private static void resetMapeo() {
        hashJsonArticulo = new HashMap<>();
        hashJsonArtDet = new HashMap<>();
    }

    private void mensajeError2(String msj) {
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    public void vistaJSONObjectArtDet() {
        //......................................................................
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Sección..............................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("tipo").toString());
            }
        });
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codigo").toString());
            }
        });
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnMedida.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnContenido.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descuento").toString());
            }
        });
        columnCosto.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("costo").toString());
            }
        });
        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty("0");
//                } else {a
                return new SimpleStringProperty(data.getValue().get("iva").toString());
//                }
            }
        });
        columnExenta.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) == 0) {
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
                return new SimpleStringProperty(data.getValue().get("exenta").toString());
//                }
            }
        });
        columnGravada.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                if (Long.parseLong(data.getValue().get("poriva").toString()) != 0) {
//                    return new SimpleStringProperty(data.getValue().get("iva").toString());
//                } else {
                return new SimpleStringProperty(data.getValue().get("gravada").toString());
//                }
            }
        });
        columnDeposito.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("deposito").toString());
            }
        });
        //columna Gravada......................................................
        //columna Porcentaje......................................................
        tableViewFactura.setItems(articuloDetData);
        //**********************************************************************
    }

    private void actualizandoTablaFactura() {
        numValidator = new NumberValidator();
        articuloDetData = FXCollections.observableArrayList(getDetalleArtList());
        //columna Ruc .................................................
        columnOrden.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnOrden.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("orden").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnTipo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnTipo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("tipo").toString());
            }
        });
        columnCodigo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCodigo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                JSONObject jsonArticulo = (JSONObject) data.getValue().get("articulo");
                return new SimpleStringProperty(data.getValue().get("codigo").toString());
            }
        });
        columnDescripcion.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnDescripcion.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("descripcion").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Sección..............................................
        columnCantidad.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidad.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidad").toString());
            }
        });
        columnCantidadCC.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadCC.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidadCC").toString());
            }
        });
        columnCantidadSC.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadSC.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidadSC").toString());
            }
        });
        columnCantidadSL.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnCantidadSL.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("cantidadSL").toString());
            }
        });
        //columna Sección......................................................
        //columna Porcentaje..............................................
        columnMedida.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnMedida.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        //columna Porcentaje......................................................
        //columna Peso..............................................
        columnPeso.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnPeso.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnContenido.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnContenido.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(data.getValue().get("medida").toString());
            }
        });
        columnDescuento.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDescuento.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    return new SimpleStringProperty(data.getValue().get("descuento").toString());
                } catch (Exception e) {
                    return new SimpleStringProperty("0");
                } finally {
                }
            }
        });
        columnCosto.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnCosto.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(data.getValue().get("costo").toString()))));
//                return new SimpleStringProperty(data.getValue().get("costo").toString());
            }
        });
        columnIva.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnIva.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    return new SimpleStringProperty(data.getValue().get("iva").toString());
                } catch (Exception e) {
                    return new SimpleStringProperty("0");
                } finally {
                }

//                }
            }
        });
        columnExenta.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnExenta.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(data.getValue().get("exenta").toString()))));
//                    return new SimpleStringProperty(data.getValue().get("exenta").toString());
                } catch (Exception e) {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator("0"))));
                } finally {
                }
//                }
            }
        });
        columnGravada.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnGravada.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
//                    return new SimpleStringProperty(data.getValue().get("gravada").toString());
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(data.getValue().get("gravada").toString()))));
                } catch (Exception e) {
                    return new SimpleStringProperty(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator("0"))));
                } finally {
                }
            }
        });
        columnDeposito.setStyle("-fx-alignment: CENTER; -fx-font-weight: bold;");
        columnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                try {
                    return new SimpleStringProperty(data.getValue().get("deposito").toString());
                } catch (Exception e) {
                    return new SimpleStringProperty("-");
                } finally {
                }
            }
        });
        //columna Ruc .................................................
        tableViewFactura.setItems(articuloDetData);
    }

    private void cargandoCamposInterfaceLocal(JSONObject detalleArticulo) {

    }

    private void cerrandoCabecera() {
//        if (txtTipoMovimiento.getText().equals("")
//                || txtProveedor.getText().equals("")
//                || txtRucProveedor.getText().equals("")
//                || txtNroEntrada.getText().equals("")
//                || txtClaseMov.getText().equals("")
//                || txtNroFactura.getText().equals("")) {
//            mensajeAlerta("EXISTEN CAMPOS QUE REQUIEREN CARGARSE");
//        } else {
//            secondPane1.setVisible(true);
//            txtCodigo.requestFocus();
//        }
    }

    private void pagando() {
//        if ("factura_cerrar")) {
        org.json.JSONObject json = new org.json.JSONObject(fact);
        if (!json.isNull("facturaClienteCab")) {
            try {
                JSONObject factu = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                setCabFactura(factu);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }

        this.sc.loadScreenModal("/vista/caja/FormasDePagoFXML.fxml", 667, 501, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void mensajeError(String msj) {
//        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
//        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ok);
//        this.alert = true;
//        alert2.showAndWait();
//        if (alert2.getResult() == ok) {
//            alert2.close();
//        }
        toaster.mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }

    private void mensajeErrorAlertModal(String msj) {
        ButtonType ok = new ButtonType("CANCELAR (ESC)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.ERROR, msj, ok);
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
        }
    }

    private void verificandoDatosCierre() {

    }
//    private HashMap () {
//        HashMap valor = new HashMap();
//        valor.put("cajero", "nverificandoDatosCierreull");
//        int items = tableViewFactura.getItems().size();
//        if (items > 0) {
//            mensajeAlerta("¡EXISTE UNA FACTURA QUE AÚN NO HA SIDO CERRADA!");
//        } else {
//            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//            alertCerrarTurno = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CERRAR TURNO COMO CAJERO?", ok, cancel);
//            alertCerrarTurno.showAndWait();
//            LoginFXMLController.setLlamarTask(false);
//            if (alertCerrarTurno.getResult() == ok) {
//                ButtonType okData = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                ButtonType cancelData = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                alertArqueo = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA REALIZAR EL ARQUEO?", okData, cancelData);
//                alertArqueo.showAndWait();
//                if (alertArqueo.getResult() == okData) {
//                    users = null;
//                    fact = null;
//                    DatosEnCaja.setUsers(null);
//                    actualizarDatos();
//                    LoginFXMLController.setLlamarTask(false);
//                    this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//                    valor.put("cajero", "cambio");
//                    alertArqueo.close();
//                }
//            } else if (alertCerrarTurno.getResult() == cancel) {
//                alertCerrarTurno.close();
//            }
//            alertArqueo = null;
//            alertCerrarTurno = null;
//        }
//        return valor;
//    }

    private static void actualizarDatos() {
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        if (DatosEnCaja.getFacturados() == null) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            DatosEnCaja.setFacturados(new JSONObject());
        } else {
            DatosEnCaja.setFacturados(fact);
        }
        long idManejo = manejoDAO.recuperarId();
        manejo.setIdManejo(idManejo);
        manejo.setCaja(DatosEnCaja.getDatos().toString());
        if (DatosEnCaja.getUsers() != null) {
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
        } else {
            manejo.setUsuario(null);
        }

        if (DatosEnCaja.getFacturados() == null) {
            manejo.setFactura(null);
        } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
            manejo.setFactura(null);
        } else {
            manejo.setFactura(DatosEnCaja.getFacturados().toString());
        }
        boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    private void cancelarFactura() {

    }

    private void verificarMontoFacturado() {
        int items = tableViewFactura.getItems().size();
        if (items > 0) {
            mensajeAlerta("¡EXISTE UNA FACTURA QUE NO HA SIDO CERRADA!");
        } else {
            if (datos.containsKey("montoFacturado")) {
                int montoFacturado = Integer.parseInt(datos.get("montoFacturado").toString());
                if (montoFacturado > 0) {
                    retirandoDinero();
                } else {
                    mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
                }
            } else {
                mensajeAlerta("¡AÚN NO SE REALIZÓ FACTURACIÓN EN CAJA!");
            }
        }
    }

    private void retirandoDinero() {
//        if ("retiro_dinero")) {

        this.sc.loadScreenModal("/vista/caja/retiroDineroFXML.fxml", 529, 266, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        }
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F2) {
            guardarCompra();
        }
        if (keyCode == event.getCode().F4) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificarMontoFacturado();
                    actualizarDatos();
                }
            }
        }
        if (keyCode == event.getCode().F6) {
            tableViewFactura.requestFocus();
        }
        if (keyCode == event.getCode().F3) {
            if (alert) {
                alert = false;
            } else if (detalleArtList.isEmpty()) {
                mensajeAlerta("NO DISPONE DE ARTÍCULO ALGUNO PARA CANCELAR FACTURA.");
            } else {

                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                cancelarFactura();
                actualizarDatos();
            }
        }
        if (keyCode == event.getCode().DELETE) {
            if (!verificandoCaidaFormaPago()) {
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    mensajeError("NO SE PUEDE ELIMINAR LOS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                } else if (alert) {
                    alert = false;
                } else if (detalleArtList.isEmpty()) {
                    mensajeAlerta("DEBE DISPONER COMO MÍNIMO UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                } else {
                    JSONObject productos = tableViewFactura.getSelectionModel().getSelectedItem();
                    if (productos == null) {
                        mensajeAlerta("DEBE SELECCIONAR UN DETALLE PARA LA CANCELACIÓN DE ARTÍCULO.");
                    } else {
//                        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
//                        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
//                        Alert alert1 = new Alert(Alert.AlertType.INFORMATION, "¿DESEA ELIMINAR EL ARTÍCULO " + productos.get("descripcion").toString().toUpperCase() + "?", ok, cancel);
//                        alert1.showAndWait();
//                        if (alert1.getResult() == ok) {
                        try {
                            facturaCabeceraSupr = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                            PedidoCompraFXMLController.setCabFactura(facturaCabeceraSupr);
//                                textFieldDescripcion.setText("");
                            this.alert = false;
                            Label labelCantidad = new Label();

                            this.sc.loadScreenModal("/vista/caja/CancelacionProductoFXML.fxml", 519, 275, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
//                        } else if (alert1.getResult() == cancel) {
//                            alert1.close();
//                        }
                        actualizarDatos();
                    }
                }
            }
        }
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                if (!verificandoCaidaFormaPago()) {
                    resetTray();
                    verificandoDatosCierre();
//                    String cajero = verificandoDatosCierre().get("cajero").toString();
//                    if (cajero.equalsIgnoreCase("arqueo")) {
//                        users = null;
//                        fact = null;
//                        DatosEnCaja.setUsers(null);
//                        datos.put("modSup", true);
//                        DatosEnCaja.setDatos(datos);
//                        actualizarDatosNuevo();
//                    }
                }
            }
        }
        if (altN.match(event)) {
            registrandoCliente();
        }
        if (altC.match(event)) {
            buscandoCliente();
        }
        if (keyCode == event.getCode().F5) {
            nuevoArt();
        }
        if (keyCode == event.getCode().F10) {
            editarDetalle();
        }
        if (keyCode == event.getCode().F11) {
            eliminarDetalle();
        }
        if (keyCode == event.getCode().F2) {
            guardarCompra();
        }
        if (keyCode == event.getCode().F7) {
            cargarDetalle(idPedidoCab);
        }
        if (keyCode == event.getCode().ESCAPE) {
            this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        }
//        if (keyCode == event.getCode().F10) {
//            mensajeDialog();
//        }
        if (event.getCode().isDigitKey() || event.getCode().getName().contentEquals("Enter")) {
//            if (alert) {
//                alert = false;
//            } else {
//                lecturaCodBarra(event);
//            }
        }
    }

    private void cargarArticulosOrdenar() {
        mapArticulosEnPromo = new HashMap();
        try {
//            int valor = 0;
            cantPromo = 0;
            int orden = 0;
            for (JSONObject jsonDetalle : detalleArtList) {
                orden++;
                long cantidad = Long.parseLong(String.valueOf(jsonDetalle.get("cantidad").toString()).replace(".0", ""));
                JSONObject jsonArt = (JSONObject) parser.parse(jsonDetalle.get("articulo").toString());
                if (LoginCajeroFXMLController.articuloPromo.containsKey(jsonArt.get("codArticulo"))) {
                    mapArticulosEnPromo.put(jsonArt.get("codArticulo") + "-" + cantidad + "-" + orden, Long.parseLong(String.valueOf(jsonDetalle.get("precio").toString())));
//                    valor++;
                    cantPromo += cantidad;
                }
            }
            if (cantPromo >= 4) {
                ordenarMapeo();

            }
        } catch (ParseException ex) {
            Logger.getLogger(PedidoCompraFXMLController.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void ordenarMapeo() {
        Set<Map.Entry<String, Long>> set = mapArticulosEnPromo.entrySet();
        List<Map.Entry<String, Long>> list = new ArrayList<Map.Entry<String, Long>>(
                set);
        Collections.sort(list, new Comparator<Map.Entry<String, Long>>() {
            public int compare(Map.Entry<String, Long> o2,
                    Map.Entry<String, Long> o1) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        int orden = 0;
        arrayListadoPromo = new ArrayList();
        for (Map.Entry<String, Long> entry : list) {

            StringTokenizer st = new StringTokenizer(entry.getKey(), "-");
            String cod = st.nextElement().toString();
            String cant = st.nextElement().toString();
            for (int i = 0; i < Integer.parseInt(cant); i++) {
                orden++;
                arrayListadoPromo.add(orden + "-" + cod + "-" + entry.getValue());
            }
        }

//        for (int i = 0; i < arrayListadoPromo.size(); i++) {
//            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
//            String ord = st.nextElement().toString();
//            String cod = st.nextElement().toString();
//            String precio = st.nextElement().toString();
//
//            System.out.println(ord + ") -->> " + cod + " - " + precio);
//        }
        System.out.println("------------------SIGUIENTE--------------------");
        int vueltaEnCiclo = 0;
        int cantVueltaEnCiclo = 1;

        double cantDesc = orden / 4;
        String str = String.valueOf(cantDesc);
        int cantVueltaTotalDesc = Integer.parseInt(str.substring(0, str.indexOf('.')));
        descPromo = 0;
        arrayListadoPromoConDesc = new ArrayList();
        mapPromoConDesc = new HashMap();

        for (int i = 0; i < arrayListadoPromo.size(); i++) {
            StringTokenizer st = new StringTokenizer(arrayListadoPromo.get(i).toString(), "-");
            String ord = st.nextElement().toString();
            String cod = st.nextElement().toString();
            String precio = st.nextElement().toString();

            vueltaEnCiclo++;

            if (cantVueltaTotalDesc >= cantVueltaEnCiclo) {
                switch (vueltaEnCiclo) {
                    case 1:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 50%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.5) + "-50");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.5);
                        break;
                    case 2:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 40%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.4) + "-40");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.4);
                        break;
                    case 3:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 30%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.3) + "-30");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.3);
                        break;
                    case 4:
//                        System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 20%");
                        arrayListadoPromoConDesc.add(ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.2) + "-20");
                        if (mapPromoConDesc.containsKey(cod)) {
                            int valor = Integer.parseInt(mapPromoConDesc.get(cod).toString());
                            mapPromoConDesc.put(cod, (valor + 1));
                        } else {
                            mapPromoConDesc.put(cod, 1);
                        }
                        descPromo += (Long.parseLong(precio) * 0.2);
                        vueltaEnCiclo = 0;
                        cantVueltaEnCiclo++;
                        break;
//                default:
//                    break;
                }
            } else {
                System.out.println(ord + ") -->> " + cod + " - " + precio + " DESC: 0%");
            }
        }
    }

    private void buscandoCliente() {
//        if ("cliente_caja")) {

        this.sc.loadScreenModal("/vista/caja/BuscarClienteFXML.fxml", 602, 87, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    private void registrandoCliente() {
//        if ("cliente_caja")) {

        this.sc.loadScreenModal("/vista/caja/NuevoClienteFXML.fxml", 519, 187, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
//        }
    }

    public static void resetParam() {
        precioTotal = 0l;
    }

    private void resetTray() {
        Toaster.quitandoMsj();
    }

    private void keyPressTextCod(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            keyPressEventos(event);
            if (!enterEstado) {
                enterEstado = false;
            } else {
                if (alertArqueo != null) {
                    if (alertArqueo.isShowing()) {
                        Utilidades.log.info("FUI A UN ARQUEO");
                    }
                }
                if (alertCerrarTurno != null) {
                    if (alertCerrarTurno.isShowing()) {
                        Utilidades.log.info("FUI A UN CIERRE DE TURNO");
                    }
                }
                datos.remove("exentaGlobal");
                //NUEVO
                if (DatosEnCaja.getDatos() != null) {
                    datos = DatosEnCaja.getDatos();
                }
                if (DatosEnCaja.getFacturados() != null) {
                    fact = DatosEnCaja.getFacturados();
                }
                org.json.JSONObject json = new org.json.JSONObject(datos);
                boolean formaPago = false;
                if (!json.isNull("caida")) {
                    String caida = datos.get("caida").toString();
                    if (caida.equalsIgnoreCase("factura_venta")) {
                        formaPago = false;
                    } else {
                        formaPago = true;
                    }
                }
                if (formaPago) {
                    if (!this.alert) {
                        mensajeError("NO SE PUEDE AGREGAR MAS PRODUCTOS YA QUE SE HA GENERADO LA FORMA DE PAGO.");
                    }
                } else {
                    JSONObject jsonCabecera = new JSONObject();
                    if (!json.isNull("sitio") && !facturaVentaEstado) {
                        try {
                            jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        } catch (ParseException ex) {
                            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                        }
                    }
//              FIN NUEVO
                }
                enterEstado = false;
            }
        } else if (event.getCode().isDigitKey()) {
            switch (event.getCode().getName()) {
                case "Numpad 0":
                    codBarra = codBarra + "0";
                    break;
                case "Numpad 1":
                    codBarra = codBarra + "1";
                    break;
                case "Numpad 2":
                    codBarra = codBarra + "2";
                    break;
                case "Numpad 3":
                    codBarra = codBarra + "3";
                    break;
                case "Numpad 4":
                    codBarra = codBarra + "4";
                    break;
                case "Numpad 5":
                    codBarra = codBarra + "5";
                    break;
                case "Numpad 6":
                    codBarra = codBarra + "6";
                    break;
                case "Numpad 7":
                    codBarra = codBarra + "7";
                    break;
                case "Numpad 8":
                    codBarra = codBarra + "8";
                    break;
                case "Numpad 9":
                    codBarra = codBarra + "9";
                    break;
                default:
                    codBarra = codBarra + event.getCode().getName();
                    break;
            }
        } else if (event.getCode() == KeyCode.INSERT) {

        }
    }

    private void lecturaCodBarra(KeyEvent event) {
    }

    private void mensajeDetalle(String msj, String title) {
        ButtonType btnAcept = new ButtonType("Salir (ENTER)", ButtonBar.ButtonData.OK_DONE);
        Alert alerta = new Alert(Alert.AlertType.INFORMATION, msj, btnAcept);
        alerta.setTitle(title);
        alerta.setHeaderText("Mensaje del Sistema!");
        DialogPane dialogPane = alerta.getDialogPane();
        dialogPane.getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialogPane.getStyleClass().add("myDialogInformation");
        alerta.showAndWait();
        if (alerta.getResult() == btnAcept) {
            alerta.close();
//            if (detalleArtList.size() >= 0) {
//                if (cbImpresion.getSelectionModel().getSelectedIndex() == 1) {
//                    if (!txtPlazo.getText().equals("") && !txtFrecuencia.getText().equals("")) {
            imprimir(); //                    } else {
            //            mensajeAlerta("EL CAMPO PLAZO Y FRECUENCIA NO DEBE QUEDAR VACIO");
            //                    }
            //                } 
            //            } else {
            //                mensajeAlerta("NO HAY DETALLE EN EL PEDIDO DE COMPRA..");
            //            }
        }
    }

    private boolean verificandoCaidaFormaPago() {
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (!json.isNull("energiaElectrica")) {
            mensajeAlerta("Esta Factura debe ser cancelada por problemas de caída de la Energía Eléctrica, ya que podrían contener datos corruptos");
            return true;
        } else {
            return false;
        }
    }

//    public static void actualizandoCabFacturaLocalmente() {
//        JSONParser parser = new JSONParser();
//        if (DatosEnCaja.getFacturados() != null) {
//            try {
//                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
//            } catch (ParseException ex) {
//                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//            }
//        }
//        org.json.JSONObject json = new org.json.JSONObject(datos)
//                datos.put("caida", "forma_pago");
//            }
//        }
//        if (!facturaVentaEstado) {
//            if (!FacturaDeVentaFXMLController.isCancelacionProd()) {
//                //OBTENER ID RANGO ACTUAL DE LA FACTURA
//                long idRangoFact = 0;
//                if (!json.isNull("idRangoFacturaActual")) {
//                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
//                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                        datos.put("idRangoFacturaActual", idRangoFact);
//                    } else {
//                        String rango = datos.get("idRangoFacturaActual").toString();
//                        idRangoFact = Long.parseLong(rango);
//                    }
//                } else {
//                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
//                    datos.put("idRangoFacturaActual", idRangoFact);
//                }
//                FacturaDeVentaFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
//                JSONObject jsonCabecera = new JSONObject();
//                if (!jsonFact.isNull("facturaClienteCab")) {
//                    try {
//                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
//                        datos.put("nroFact", FacturaDeVentaFXMLController.getCabFactura().get("nroFact"));
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                } else {
//                    try {
//                        editandoJsonFactCab();
//                        Map<String, String> mapeo = Utilidades.splitNroActual(FacturaDeVentaFXMLController.getCabFactura().get("nroActual").toString());
//                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
//                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
//                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
//                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
//                        datos.put("nroFact", nroActualmente);
//                        FacturaDeVentaFXMLController.getCabFactura().put("nroFactura", nroActualmente);
//                    } catch (ParseException ex) {
//                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                    }
//                }
//            }
//        }
//        fact.put("facturaClienteCab", FacturaDeVentaFXMLController.getCabFactura().toString());
//        if (FacturaDeVentaFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
//            JSONArray jsonArrayFactDet = creandoJsonFactDet(FacturaDeVentaFXMLController.getCabFactura());
//            JSONArray arrayDetalle = new JSONArray();
//            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
//                try {
//                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
//                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
//                    art.put("fechaAlta", null);
//                    art.put("fechaMod", null);
//                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
//                    iva.put("fechaAlta", null);
//                    iva.put("fechaMod", null);
//                    art.put("iva", iva);
//                    jsonArt.put("articulo", art);
//                    arrayDetalle.add(jsonArt);
//                } catch (ParseException ex) {
//                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
//                }
//            }
//            DatosEnCaja.setDatos(datos);
//            DatosEnCaja.setUsers(users);
//            DatosEnCaja.setFacturados(fact);
//            datos = DatosEnCaja.getDatos();
//            users = DatosEnCaja.getUsers();
//            fact = DatosEnCaja.getFacturados();
//            fact.put("facturaDetalle", arrayDetalle);
//        }
//        actualizarDatosBD();
//        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
//    }
    private void mensajeDialog() {
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("Consulta de Precio");
        dialog.setHeaderText("Mensaje del Sistema!");
        dialog.setContentText("INGRESE CODIGO DEL PRODUCTO:");
        ButtonType btnAcept = new ButtonType("Aceptar", ButtonBar.ButtonData.OK_DONE);
        ButtonType btnCancel = new ButtonType("Cancelar", ButtonBar.ButtonData.CANCEL_CLOSE);
        dialog.getDialogPane().getButtonTypes().removeAll(ButtonType.CANCEL, ButtonType.OK);
        dialog.getDialogPane().getButtonTypes().addAll(btnAcept, btnCancel);
        dialog.getDialogPane().getStylesheets().add(
                getClass().getResource("/styles/Styles.css").toExternalForm());
        dialog.getDialogPane().getStyleClass().add("myDialogInformation");
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
            Articulo art = artDAO.buscarCod(result.get());
            art.setFechaAlta(null);
            art.setFechaMod(null);
            String mensaje = "";
            mensaje = art.getDescripcion() + "\nPRECIO: " + numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(art.getCosto())));
            mensaje += "\n\nLOS DESCUENTOS QUEDAN SUJETO A VARIACIONES DE ACUERDO A LA FORMA DE PAGO.";
            mensajeDetalle(mensaje, "Detalle del Artículo");
        } else {
            Utilidades.log.info("No haz seleccionado nada");
        }
    }

    public static void actualizandoCabFacturaLocalmente() {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getFacturados() != null) {
            try {
                fact = (JSONObject) parser.parse(Utilidades.setToJson(DatosEnCaja.getFacturados().toString()));
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        org.json.JSONObject json = new org.json.JSONObject(datos);
        org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
        if (json.isNull("caida")) {
            PedidoCompraFXMLController.cancelacionProd = false;
            datos.put("caida", "factura_venta");
        } else {
            String caida = json.get("caida").toString();
            if (caida.equalsIgnoreCase("factura_venta")) {
                if (!isActualizarDatosCabecera()) {
                    PedidoCompraFXMLController.cancelacionProd = false;
                } else {
                    PedidoCompraFXMLController.cancelacionProd = true;
                }
                datos.put("caida", "factura_venta");
            } else {
                PedidoCompraFXMLController.cancelacionProd = true;
                datos.put("caida", "forma_pago");
            }
        }
        if (!facturaVentaEstado) {
            if (!PedidoCompraFXMLController.isCancelacionProd()) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                long idRangoFact = 0;
                if (!json.isNull("idRangoFacturaActual")) {
                    if (datos.get("idRangoFacturaActual").toString().equalsIgnoreCase("0")) {
                        idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                        datos.put("idRangoFacturaActual", idRangoFact);
                    } else {
                        String rango = datos.get("idRangoFacturaActual").toString();
                        idRangoFact = Long.parseLong(rango);
                    }
                } else {
                    idRangoFact = rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal());
                    datos.put("idRangoFacturaActual", idRangoFact);
                }
                PedidoCompraFXMLController.getCabFactura().put("idFacturaClienteCab", idRangoFact);
                JSONObject jsonCabecera = new JSONObject();
                if (!jsonFact.isNull("facturaClienteCab")) {
                    try {
                        jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                        PedidoCompraFXMLController.getCabFactura().put("nroFactura", jsonCabecera.get("nroFactura"));
                        datos.put("nroFact", PedidoCompraFXMLController.getCabFactura().get("nroFact"));
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                } else {
                    try {
                        editandoJsonFactCab();
                        Map<String, String> mapeo = Utilidades.splitNroActual(PedidoCompraFXMLController.getCabFactura().get("nroActual").toString());
                        long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                        JSONObject jsonSucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                        JSONObject jsonCaja = (JSONObject) parser.parse(datos.get("caja").toString());
                        String nroActualmente = Utilidades.procesandoNro(Long.parseLong(jsonSucursal.get("idSucursal").toString()), Long.parseLong(jsonCaja.get("nroCaja").toString()), nroActual);
                        datos.put("nroFact", nroActualmente);
                        PedidoCompraFXMLController.getCabFactura().put("nroFactura", nroActualmente);
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                }
            }
        }
        PedidoCompraFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());
        fact.put("facturaClienteCab", PedidoCompraFXMLController.getCabFactura().toString());
        if (PedidoCompraFXMLController.getCabFactura().get("idFacturaClienteCab") != null) {
            JSONArray jsonArrayFactDet = creandoJsonFactDet(PedidoCompraFXMLController.getCabFactura());
            JSONArray arrayDetalle = new JSONArray();
            for (int i = 0; i < jsonArrayFactDet.size(); i++) {
                try {
                    JSONObject jsonArt = (JSONObject) parser.parse(jsonArrayFactDet.get(i).toString());
                    JSONObject art = (JSONObject) parser.parse(jsonArt.get("articulo").toString());
                    art.put("fechaAlta", null);
                    art.put("fechaMod", null);
                    JSONObject iva = (JSONObject) parser.parse(art.get("iva").toString());
                    iva.put("fechaAlta", null);
                    iva.put("fechaMod", null);
                    art.put("iva", iva);
                    jsonArt.put("articulo", art);
                    arrayDetalle.add(jsonArt);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                }
            }
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            DatosEnCaja.setFacturados(fact);
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();
            fact = DatosEnCaja.getFacturados();
            fact.put("facturaDetalle", arrayDetalle);
        }
        actualizarDatosBD();
        //comparar lo que hay en ande.manejo_local y lo que hay en la BD 
    }

    private static void editandoJsonFactCab() {
        try {
            JSONParser parser = new JSONParser();
            JSONObject talonario = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            long idTalonario = Long.parseLong(talonario.get("idTalonariosSucursales").toString());
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            PedidoCompraFXMLController.getCabFactura().put("nroActual", tal.getNroActual() + " - " + String.valueOf(talonario.get("idTalonariosSucursales")));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                PedidoCompraFXMLController.getCabFactura().put("cliente", NuevoClienteFXMLController.getJsonCliente());//en nulo default NN
            } else {
                PedidoCompraFXMLController.getCabFactura().put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            }

        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
    }

    private static void actualizarDatosBD() {
        try {
            JSONParser parser = new JSONParser();
            DatosEnCaja.setDatos(datos);
            DatosEnCaja.setUsers(users);
            if (DatosEnCaja.getFacturados() == null) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else if (DatosEnCaja.getFacturados().toString().equals("{}")) {
                DatosEnCaja.setFacturados(new JSONObject());
            } else {
                DatosEnCaja.setFacturados(fact);
            }
            long idManejo = manejoDAO.recuperarId();
            manejo.setIdManejo(idManejo);
            manejo.setCaja(DatosEnCaja.getDatos().toString());
            manejo.setUsuario(DatosEnCaja.getUsers().toString());
            String jsonFact = Utilidades.setToJson(DatosEnCaja.getFacturados().toString());
            jsonFact = jsonFact.replace("\"[", "[");
            jsonFact = jsonFact.replace("]\"", "]");
            manejo.setFactura(jsonFact);
            boolean valor = manejoDAO.actualizarObtenerEstado(manejo);
            if (valor) {
                Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
            } else {
                Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
            }
            datos = DatosEnCaja.getDatos();
            users = DatosEnCaja.getUsers();

            fact = DatosEnCaja.getFacturados();
        } catch (Exception ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        } finally {
        }
    }

    private void actualizarDatosNuevo() {
        datos.remove("vueltoSi");
        datos.remove("vueltoNo");
        datos.remove("dona");
        datos.remove("caida");
        datos.remove("sitio");
        datos.remove("insercionFacturaVentaCab");
        datos.remove("insercionFacturaVentaCabLocal");
        datos.remove("actualizacionLocal");
        datos.remove("idRangoFacturaActual");
        datos.remove("modSup");
        datos.put("rendicion", true);
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setUsers(users);
        DatosEnCaja.setFacturados(null);
        long idManejo = manejoDAO.recuperarId();
        manejoLocal.setIdManejo(idManejo);
        if (DatosEnCaja.getDatos() != null) {
            manejoLocal.setCaja(DatosEnCaja.getDatos().toString());
        } else {
            manejoLocal.setCaja(null);
        }
        manejoLocal.setUsuario(null);
        manejoLocal.setFactura(null);
        boolean valor = manejoDAO.actualizarObtenerEstado(manejoLocal);
        if (valor) {
            Utilidades.log.info("-->> LOS DATOS HAN SIDO ACTUALIZADOS CON EXITO");
        } else {
            Utilidades.log.info(" XXXX LOS DATOS NO HAN PODIDO SER ACTUALIZADOS! XXX");
        }
        datos = DatosEnCaja.getDatos();
        users = DatosEnCaja.getUsers();
        fact = DatosEnCaja.getFacturados();
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleUnidad() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        Map mapeo = recuperarGiftSinUso(codBarra);
        boolean estadoGift = recuperarGiftEnUso(codBarra);
        if (estadoGift) {
            toaster.mensajeGenerico("Mensaje del Sistema", "GIFTCARD YA ESTA EN USO", "", 2);
        } else if (Boolean.parseBoolean(mapeo.get("comprado").toString())) {
            if (getHmGift().containsKey(Long.valueOf(codBarra))) {
                toaster.mensajeGenerico("Mensaje del Sistema", "YA SE HA CARGADO LA GIFTCARD", "", 2);
            } else {
//                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!getDetalleArtList().isEmpty()) {
                    orden++;
                }
                GiftCardFXMLController.setCodigo(codBarra, tableViewFactura, mapeo, orden);
                this.sc.loadScreenModal("/vista/caja/GiftCardFXML.fxml", 425, 206, "/vista/caja/FacturaDeVentaFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
            }
        } else if (!estadoGift) {
            if (!codBarra.contentEquals("")) {
                if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                    jsonArticulo = jsonArtDet(codBarra);
                } else {
                    jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
                }
            }
            JSONArray jsonArrayArticulo = new JSONArray();
            try {
                org.json.JSONObject jsonArticu = new org.json.JSONObject(jsonArticulo);
                if (jsonArticu.isNull("articuloNf3Sseccion")) {
                    jsonArrayArticulo = new JSONArray();
                } else {
                    jsonArrayArticulo = (JSONArray) parser.parse(jsonArticulo.get("articuloNf3Sseccion").toString());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
            }
            boolean pasa = false;
            if (jsonArrayArticulo.isEmpty()) {
                pasa = true;
            } else {
                JSONObject jsonObj = (JSONObject) jsonArrayArticulo.get(0);
                JSONObject nf3Sseccion = (JSONObject) jsonObj.get("nf3Sseccion");
                pasa = !nf3Sseccion.get("descripcion").toString().equalsIgnoreCase("GIFT CARD");
            }
            if (!pasa) {
                toaster.mensajeGenerico("Mensaje del Sistema", "NO SE HA ESTABLECIDO CONEXION CON LA BASE GIFTCARD", "", 2);
            } else {
                boolean data = false;
                if (jsonArticulo != null) {
                    org.json.JSONObject jsonArti = new org.json.JSONObject(jsonArticulo);
                    if (jsonArti.get("costo").toString().equals("0")) {
                        mensajeDetalle("EL ARTICULO NO TIENE PRECIO, CONTACTESE CON STOCK", "Mensaje del Sistema");

                        data = true;
                    }
                }
                if (!data) {
                    if (jsonArticulo != null) {
                        try {
                            JSONObject detalleArticulo = null;
                            if (primeraInsercion) {
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                                detalleArtList.add(detalleArticulo);
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);

                                primeraInsercion = false;
                                detalleArticulo.put("primeraInsercion", true);
                                cargandoCamposInterface(detalleArticulo);
                                CajaDeDatos.generandoNroComprobante();
                                JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                                JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                                TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                                // primer trío
                                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                                // segundo trío
                                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                                long nroActual = talos.getNroActual();
                            } else {
                                orden++;
                                detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);//si
                                detalleArtList.add(detalleArticulo);//si
                                hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));//si
                                hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);//si

                                cargandoCamposInterface(detalleArticulo);
                            }
                            tableViewFactura.getItems().clear();
                            tableViewFactura.getItems().addAll(detalleArtList);
//                }
                            if (!tableViewFactura.getItems().isEmpty()) {
                                tableViewFactura.getSelectionModel().selectLast();
                            }
                            Image image = null;
                            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                                image = jsonArtDetImg(codBarra);
                            }
                            if (image == null) {
                                File file = new File(PATH.PATH_NO_IMG);
                                image = new Image(file.toURI().toString());

                            }
                        } catch (ParseException e) {
                            estado = false;
                            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
                        }
                    } else {

                        estado = false;
                    }
                }
            }
        }
        txtCodVendedor.setText("");

        return estado;
    }

    @SuppressWarnings("null")
    private boolean cargandoDetalleDecimal() {
        JSONObject jsonArticulo = null;
        boolean estado = true;
        if (!codBarra.contentEquals("")) {
            if (hashJsonArticulo.get(Long.valueOf(codBarra)) == null) {
                jsonArticulo = jsonArtDet(codBarra);
            } else {
                jsonArticulo = hashJsonArticulo.get(Long.valueOf(codBarra));
            }
        }
        if (jsonArticulo != null) {
            try {
                JSONObject detalleArticulo = null;
                if (primeraInsercion) {
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);

                    primeraInsercion = false;
                    detalleArticulo.put("primeraInsercion", true);
                    cargandoCamposInterface(detalleArticulo);
                    CajaDeDatos.generandoNroComprobante();
                    JSONObject talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
                    JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
                    JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                    TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
                    // primer trío
                    long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                    // segundo trío
                    long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                    long nroActual = talos.getNroActual();

                } else {
                    orden++;
                    detalleArticulo = creandoJsonDetalleArt(jsonArticulo, orden);
                    detalleArtList.add(detalleArticulo);
                    hashJsonArtDet.put((Long) jsonArticulo.get("idArticulo"), detalleArtList.lastIndexOf(detalleArticulo));
                    hashJsonArticulo.put((Long) jsonArticulo.get("codArticulo"), jsonArticulo);
                    cargandoCamposInterface(detalleArticulo);
                    tableViewFactura.getItems().clear();
                    tableViewFactura.getItems().addAll(detalleArtList);
                    //en caso de cancelación artículo y vuelva desde 0 a cargar
                }
                if (!tableViewFactura.getItems().isEmpty()) {
                    tableViewFactura.getSelectionModel().selectLast();
                }
                Image image = null;
                if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                    image = jsonArtDetImg(codBarra);
                }

            } catch (NumberFormatException | ParseException e) {
                estado = false;
                Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            }
        } else {

            estado = false;
        }
        return estado;
    }

    private JSONObject jsonArtDet(String cod) {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject articulo = null;
        articulo = generarListaArticuloLocal(cod);
        return articulo;
    }

    private JSONObject generarListaArticuloLocal(String cod) {
        JSONParser parser = new JSONParser();
        try {
            Articulo art = artDAO.buscarCod(cod);
            return (JSONObject) parser.parse(gson.toJson(art.toArticuloDTO()));
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
            return null;
        }
    }

    private static JSONObject creandoJsonFactCab() {
        try {
            JSONObject jsonCabFactura = new JSONObject();
            JSONObject estadoFactura = new JSONObject();
            JSONParser parser = new JSONParser();
            estadoFactura.put("idEstadoFactura", 1L);//normal
            JSONObject tipoMoneda = new JSONObject();
            tipoMoneda.put("idTipoMoneda", 1L);//guaraníes
            JSONObject tipoComprobante = new JSONObject();
            //acaité para el tema de mayorista...
            tipoComprobante.put("idTipoComprobante", 1L);//factura contado
            //**********************************************************************
            jsonCabFactura.put("cancelado", true);
            jsonCabFactura.put("caja", datos.get("caja"));
            if (BuscarClienteFXMLController.getJsonCliente() == null) {
                jsonCabFactura.put("cliente", NuevoClienteFXMLController.getJsonCliente());
            } else {
                jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());
            }
//            jsonCabFactura.put("cliente", BuscarClienteFXMLController.getJsonCliente());//en nulo default NN
            jsonCabFactura.put("sucursal", datos.get("sucursal"));//en nulo default NN
            jsonCabFactura.put("nroFactura", null);
            jsonCabFactura.put("estadoFactura", estadoFactura);
            jsonCabFactura.put("tipoComprobante", tipoComprobante);
            jsonCabFactura.put("tipoMoneda", tipoMoneda);
            //**********************************************************************
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            Long timestampEmision = tsNow.getTime();
            jsonCabFactura.put("fechaEmision", timestampEmision);
            jsonCabFactura.put("fechaMod", timestampEmision);
            jsonCabFactura.put("usuAlta", Identity.getNomFun());
            jsonCabFactura.put("usuMod", Identity.getNomFun());
            //**********************************************************************
            JSONObject talona = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            jsonCabFactura.put("nroActual", talona.get("nroActual") + " - " + String.valueOf(talona.get("idTalonariosSucursales")));
            //Consulta de nroActual de talonarios en la BD local....
            return jsonCabFactura;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return null;
        }
    }

    private JSONObject creandoJsonDetalleArt(JSONObject jsonArticulo, int orden) {
        return null;
    }

    private void cargandoCamposInterface(JSONObject detalleArticulo) {
        org.json.JSONObject jsonDetalle = new org.json.JSONObject(detalleArticulo);
//        labelCantidad.setText(detalleArticulo.get("cantidad").toString());

//        textFieldDescripcion.setText(detalleArticulo.get("descripcion").toString());
//        if (jsonDetalle.isNull("exenta")) {
        if (codDecimal == null) {
            codDecimal = "";
        }
        if (!codDecimal.isEmpty()) {

        } else {

        }

        seteandoMontoFact();
    }

    private static void seteandoMontoFact() {
        int monto = Integer.valueOf(String.valueOf(precioTotal));
        cabFactura.put("montoFactura", monto);
    }

    private Image jsonArtDetImg(String cod) {
        byte[] bytes = null;
        Image image = null;
        try {
            URL url = new URL("http://192.168.8.202:8888/ServerParana/util/img/" + cod);
//            URL url = new URL(Utilidades.ip + "/ServerParana/util/img/" + cod);
            InputStream is = null;
            is = url.openStream();
            image = new Image(is);
        } catch (FileNotFoundException e) {
            Utilidades.log.error("ERROR FileException: ", e.fillInStackTrace());
        } catch (IOException e) {
            Utilidades.log.error("ERROR IOException: ", e.fillInStackTrace());
        }
        return image;
    }

    static void persistiendoFact(boolean cancelProd, long idArt) {
        try {
            JSONParser parser = new JSONParser();
            if (cancelProd) {
                if (cancelacionProdPrimera) {
                    FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(false));
                    cancelacionProdPrimera = false;
                    cancelacionProd = true;//permite cambiar a PUT en FormaPagoFXMLController, al finalizar venta...
                }
                FacturaVentaDatos.setIdProducto(idArt);
            } else {
                FacturaVentaDatos.setIdFacturaClienteCab(creandoCabFactura(true));
                cancelacionProd = false;
            }
            JSONObject aperturaCa = (JSONObject) parser.parse(datos.get("aperturaCaja").toString());
            JSONObject usuarioCajero = (JSONObject) aperturaCa.get("usuarioCajero");
            FacturaVentaDatos.setIdCajero(Long.valueOf(usuarioCajero.get("idUsuario").toString()));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }

    private static long creandoCabFactura(boolean cancelFact) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean estadoCancelProd = false;
        boolean estado = false;
        org.json.JSONObject jsonDatos = new org.json.JSONObject(datos);
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONObject talonarioSucursal = null;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        long idTalonario = Long.parseLong(talonarioSucursal.get("idTalonariosSucursales").toString());
        long idFact = 0l;
        if (cancelFact) {
            JSONObject estadoFactura = new JSONObject();
            estadoFactura.put("idEstadoFactura", 2L);//anulado
            cabFactura.put("estadoFactura", estadoFactura);
        }
        try {
            // seteando numeracion actual de talonario de manera local
            TalonariosSucursales tal = taloDAO.getById(idTalonario);
            cabFactura.put("nroActual", tal.getNroActual() + " - " + String.valueOf(idTalonario));
            // NUEVO 
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                if (cancelFact) {
                    JSONObject estadoFactura = new JSONObject();
                    estadoFactura.put("idEstadoFactura", 2L);//anulado
                    cabe.put("estadoFactura", estadoFactura);
                    fact.put("facturaClienteCab", cabe);
                }
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                    JSONObject objFact = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                    cabFactura = objFact;
                    if (!jsonDatos.isNull("caida")) {
                        String caida = datos.get("caida").toString();
                        if (caida.equalsIgnoreCase("forma_pago")) {
                            cancelacionProdPrimera = false;
                        } else if (!jsonDatos.isNull("cancelProducto")) {
                            cancelacionProdPrimera = false;
                            estadoCancelProd = true;
                        } else {
                            cancelacionProdPrimera = true;
                        }
                    }
                }
            }
            // NUEVO
            if (cancelacionProdPrimera && idCab == 0L) {
                //OBTENER ID RANGO ACTUAL DE LA FACTURA
                datos.put("idRangoFacturaActual", rangoDAO.actualizarObtenerRangoActual(Utilidades.getIdRangoLocal()));
                //Linea para prueba por el rango actual
                long n = 0l;
                org.json.JSONObject json = new org.json.JSONObject(datos);
                if (!json.isNull("idRangoFacturaActual")) {
                    n = Long.parseLong(datos.get("idRangoFacturaActual").toString());
                }
                cabFactura.put("idFacturaClienteCab", n);
                tal.setNroActual(tal.getNroActual() + 1);
                taloDAO.actualizarNroActual(tal);
                //recuperarNroActual y otros datos para la numeracion de la FACTURA
                String nroAct = cabFactura.get("nroActual").toString();
                Map<String, String> mapeo = Utilidades.splitNroActual(nroAct);
                long nroActual = Long.parseLong(mapeo.get("nroActual").toString());
                // primer trío
                JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
                JSONObject sucursal = (JSONObject) parser.parse(caja.get("sucursal").toString());
                long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
                // segundo trío
                long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
                String nroFact = Utilidades.procesandoNro(idSucursal, nroCaja, nroActual);
                cabFactura.put("nroFactura", nroFact);
                datos.put("nroFact", nroFact);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteCab");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                int actulizacion = 0;
//                boolean insertVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    insertVenta = Boolean.parseBoolean(datos.get("insercionFacturaVentaCab").toString());
//                }
//                boolean actualizacionVenta = false;
//                if (!jsonDatos.isNull("insercionFacturaVentaCab")) {
//                    if (jsonDatos.isNull("actualizacionLocal")) {
//                        actualizacionVenta = false;
//                    } else {
//                        actualizacionVenta = Boolean.parseBoolean(datos.get("actualizacionLocal").toString());
//                    }
//                }
//                if (!jsonDatos.isNull("cancelProd")) {
//                    cancelacionProdPrimera = false;
//                }
//                if (cancelacionProdPrimera) {
//                    conn.setRequestMethod("POST");//primera vez, sin importar factura cancelación o artículo...
//                    estado = true;
//                    actulizacion = 1;
//                } else if (insertVenta && !actualizacionVenta) {//Operacion realizada para saber si se persistio datos en el Servidor, sino lo realiza de manera local(ACTUALIZACION DE FACTURA CABECERA)
//                    conn.setRequestMethod("PUT");
//                    actulizacion = 1;
//                    if (estadoCancelProd) {
//                        estado = true;
//                    } else {
//                        estado = false;
//                    }
//                }
//                if (actulizacion == 1) {
//                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                    wr.write(cabFactura.toString());
//                    fact.put("facturaClienteCab", cabFactura.toString());
//                    wr.flush();
//                    int HttpResult = conn.getResponseCode();
//                    if (HttpResult == HttpURLConnection.HTTP_OK) {
//                        BufferedReader br = new BufferedReader(
//                                new InputStreamReader(conn.getInputStream(), "utf-8"));
//                        while ((inputLine = br.readLine()) != null) {
//                            cabFactura = (JSONObject) parser.parse(inputLine);
//                            datos.put("ventaServer", true);
//                            if (cabFactura.get("idFacturaClienteCab") != null) {
//                                idFact = (long) cabFactura.get("idFacturaClienteCab");
//                                //Setear Datos para saber que id se persistió en facturaClienteCab del servidor
//                                //en el caso que persista primeramente con conexion, y para la actualizacion, la conexión se vaya al maso...
//                                if (cancelacionProdPrimera) {
//                                    datos.put("insercionFacturaVentaCab", true);
//                                    datos.put("insercionIdFactClienteCabServidor", idFact);
//                                    datos.put("nroFact", cabFactura.get("nroFactura").toString());
//                                }
//                            }
//
//                        }
//                        br.close();
//                    } else {
//                        idFact = generarFacturaCabLocal();
//                    }
//                } else {
//                    idFact = generarFacturaCabLocal();
//                }
//            } else {
            idFact = generarFacturaCabLocal();
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        //para registrar facturaIni
        org.json.JSONObject jsonEstadoInicio = new org.json.JSONObject(datos);
        boolean estadoFactInicial = false;
        if (!jsonEstadoInicio.isNull("estadoFacturaInicial")) {
            estadoFactInicial = Boolean.parseBoolean(datos.get("estadoFacturaInicial").toString());
        }
        if (!estadoFactInicial) {
            datos.put("facturaInicial", cabFactura.get("nroFactura").toString());
            datos.put("estadoFacturaInicial", true);
        }
        datos.put("FacturaFinal", cabFactura.get("nroFactura").toString());
        if (idFact != 0l && cancelFact && estado) {//que sea del tipo cancelación factura...
            JSONArray jsonArrayFactDet = creandoJsonFactDet(cabFactura);
            if (creandoFactDet(jsonArrayFactDet)) {
            }
        }
        actualizarDatos();
        return idFact;
    }

    public static JSONArray creandoJsonFactDet(JSONObject factCab) {
        JSONArray jsonArrayFactDet = new JSONArray();
        //**********************************************************************
        for (int i = 0; i < detalleArtList.size(); i++) {
            detalleArtList.get(i).put("facturaClienteCab", factCab);
            jsonArrayFactDet.add(detalleArtList.get(i));
        }
        //**********************************************************************
        return jsonArrayFactDet;
    }

    private static boolean creandoFactDet(JSONArray jsonArray) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exitoInsertarDet = false;
        if (DatosEnCaja.getFacturados() != null) {
            fact = DatosEnCaja.getFacturados();
        }
        JSONArray jsonArrayFactDet = new JSONArray();
        try {
            for (int i = 0; i < jsonArray.size(); i++) {
                long id = rangoDetalleDAO.actualizarObtenerRango(Utilidades.getIdRangoLocal());
                JSONObject objeto = (JSONObject) parser.parse(jsonArray.get(i).toString());
                JSONObject articulo = (JSONObject) parser.parse(objeto.get("articulo").toString());
                objeto.put("idFacturaClienteDet", id);
                objeto.put("codArticulo", Long.parseLong(articulo.get("codArticulo").toString()));
                jsonArrayFactDet.add(objeto);
            }
//            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time) && Boolean.parseBoolean(datos.get("ventaServer").toString())) {
//                URL url = new URL(Utilidades.ip + "/ServerParana/facturaClienteDet/insercionMasiva");
//                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//                conn.setDoOutput(true);
//                conn.setDoInput(true);
//                conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
//                conn.setRequestProperty("Accept", "application/json; charset=UTF-8");
//                conn.setRequestMethod("POST");
//                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//                wr.write(jsonArrayFactDet.toString());
//                fact.put("facturaDetalle", jsonArrayFactDet.toString());
//                wr.flush();
//                int HttpResult = conn.getResponseCode();
//                if (HttpResult == HttpURLConnection.HTTP_OK) {
//                    BufferedReader br = new BufferedReader(
//                            new InputStreamReader(conn.getInputStream(), "utf-8"));
//                    while ((inputLine = br.readLine()) != null) {
//                        exitoInsertarDet = (boolean) parser.parse(inputLine);
//                    }
//                    br.close();
//                } else {
//                    exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//                }
//            } else {
            exitoInsertarDet = registrandoFacturaDetLocal(jsonArrayFactDet.toString());
//            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        actualizarDatos();
        return exitoInsertarDet;
    }

    private static boolean registrandoFacturaDetLocal(String jsonArray) {
        boolean valor = false;
        ConexionPostgres.conectar();
        String sql = "INSERT INTO desarrollo.datos (fecha ,descripcion_dato, tabla_dato, operacion) VALUES (now(),'" + jsonArray + "','facturaClienteDet', 'insertar');";
        Utilidades.log.info("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS DE MANERA LOCAL FACTURA DET ********");
                valor = true;
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }

    private static long generarFacturaCabLocal() {
        String sql = "";
        ConexionPostgres.conectar();
        org.json.JSONObject json = new org.json.JSONObject(datos);
        if (cancelacionProdPrimera) {
            datos.put("insercionFacturaVentaCabLocal", true);
            //UUID ACTUAL PARA MODIFICAR FACTURA
            long uuid = VentasUtiles.recuperarId() + 1;
            datos.put("uuidCassandraActual", String.valueOf(uuid));
            sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'insertar');";
            Utilidades.log.info("-->> " + sql);
        } else {
            long idFactCab = 0L;
            if (!json.isNull("idFactClienteCabServidor")) {
                idFactCab = Long.parseLong(datos.get("idFactClienteCabServidor").toString());
            }
            if (idFactCab != 0 && !dato) {
                //UUID ACTUAL PARA MODIFICAR FACTURA
                long uuid = VentasUtiles.recuperarId() + 1;
                cabFactura.put("idFacturaClienteCab", idFactCab);
                sql = "INSERT INTO desarrollo.cabecera (fecha, id_dato ,descripcion_dato, operacion) VALUES (now()," + uuid + ",'" + cabFactura.toString() + "', 'actualizar');";
                Utilidades.log.info("-->> " + sql);
                datos.put("uuidCassandraActual", String.valueOf(uuid));
                //para que solo una ves entre aqui luego ya actualice nada mas...
                dato = true;
                datos.put("actualizacionLocal", true);
            } else {
                String operacion = "insertar";
                if (idFactCab != 0) {
                    cabFactura.put("idFacturaClienteCab", idFactCab);
                    operacion = "actualizar";
                }
                long uuid = VentasUtiles.recuperarId();
//                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + datos.get("uuidCassandraActual").toString() + ";";
                sql = "UPDATE desarrollo.cabecera SET descripcion_dato = '" + cabFactura.toString() + "', fecha=now(), operacion ='" + operacion + "' WHERE id_dato=" + uuid + ";";
                Utilidades.log.info("-->> " + sql);
                datos.put("actualizacionLocal", true);
            }
        }
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                Utilidades.log.info("******* ALMACENANDO DATOS FACTURA VENTA CAB ********");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        cabFactura.put("idFacturaClienteCab", datos.get("idRangoFacturaActual"));

        return Long.parseLong(datos.get("idRangoFacturaActual").toString());
    }

    static void suprimirProducto(TableView<JSONObject> tabla, Label labelTotalGs, double declarado,
            Label labelCantidad, ImageView imgProducto, CheckBox chkExtranj, TextField txtCod) {
        JSONObject detalle = tabla.getSelectionModel().getSelectedItem();
        JSONObject articulos = (JSONObject) detalle.get("articulo");
        long codArticulo = (long) articulos.get("codArticulo");
        JSONObject jsonArticulo = hashJsonArticulo.get(codArticulo);
        double cantTotal = Double.parseDouble(detalle.get("cantidad").toString());
        double dif = cantTotal - declarado;
        int total = Integer.valueOf(numValidator.numberValidator(labelTotalGs.getText()));
        double neto = 0;
        double resultado = 0;
        String dato = "";
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        double nArt = Double.parseDouble(datos.get("nArticulos").toString());
//        datos.put("nArticulos", nArt - cantTotal);
        datos.put("nArticulos", nArt - declarado);
        if (dif <= 0) {
            resultado = Double.parseDouble(detalle.get("precio").toString()) * cantTotal;
            List<JSONObject> detalleAux = new ArrayList<>();
            resetMapeo();
            int ord = 0;
            for (JSONObject detalleEnCuestion : detalleArtList) {
                JSONObject jsonArtAux = (JSONObject) detalleEnCuestion.get("articulo");
                if (!(codArticulo + "|" + detalle.get("orden").toString()).equalsIgnoreCase(jsonArtAux.get("codArticulo") + "|" + detalleEnCuestion.get("orden"))) {
                    ord++;
                    detalleEnCuestion.put("orden", ord);
                    detalleAux.add(detalleEnCuestion);
                    hashJsonArtDet.put((Long) jsonArtAux.get("idArticulo"), detalleAux.lastIndexOf(detalleEnCuestion));
                    hashJsonArticulo.put((Long) jsonArtAux.get("codArticulo"), jsonArtAux);
                }
            }
            orden = ord;
            detalleArtList = new ArrayList<>();
            detalleArtList = detalleAux;
//            if (ord == 0 && detalleAux.size() == 0) {
//                BuscarClienteFXMLController.resetParamCliente();
//                BuscarClienteFXMLController.resetParamClienteFiel();
//            }
        } else {
            int index = hashJsonArtDet.get((Long) jsonArticulo.get("idArticulo"));
            resultado = Double.parseDouble(detalle.get("precio").toString()) * declarado;
            long resultadoActual = Math.round(Double.parseDouble(detalle.get("precio").toString()) * dif);
            long iva = (long) detalle.get("poriva");
            if (iva == 0l || chkExtranj.isSelected()) {
                detalleArtList.get(index).put("exenta", resultadoActual);
            } else {
                detalleArtList.get(index).put("gravada", resultadoActual);
            }
            detalleArtList.get(index).put("cantidad", dif);
        }
        neto = total - resultado;
        dato = formateador.format(neto);
        labelTotalGs.setText("Gs " + dato);
        labelCantidad.setText("0");
        imgProducto.setImage(null);
        precioTotal = Math.round(neto);
        seteandoMontoFact();
        try {
            org.json.JSONObject jsonFact = new org.json.JSONObject(fact);
            JSONObject jsonCabecera = new JSONObject();
            long idCab = 0L;
            if (!jsonFact.isNull("facturaClienteCab")) {
                JSONObject cabe = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                org.json.JSONObject jsonIdFact = new org.json.JSONObject(cabe);
                jsonCabecera = (JSONObject) parser.parse(fact.get("facturaClienteCab").toString());
                if (!jsonIdFact.isNull("idFacturaClienteCab")) {
                    idCab = Long.parseLong(jsonCabecera.get("idFacturaClienteCab").toString());
                }
            }
            if (idCab == 0L) {
                creandoCabFactura(false);
            }
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        tabla.getItems().clear();
        tabla.getItems().addAll(detalleArtList);
        //NEW
        DatosEnCaja.setDatos(datos);
        DatosEnCaja.setFacturados(fact);
        actualizarDatos();
        if (tabla.getItems().isEmpty()) {
            chkExtranj.setDisable(false);
            chkExtranj.setSelected(false);
        }
        repeatFocusData(txtCod);
    }

    static void setTearDatos(Label lblRuc, Label lblNombre, String ruc, String nombre) {
        lblRuc.setText(ruc);
        lblNombre.setText(nombre);
    }

    static void regresar(TextField txtCod) {
        repeatFocusData(txtCod);
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    public static String getFechaActual() {
        Date ahora = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        return formateador.format(ahora);
    }

    @FXML
    private void textFieldCantKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    @FXML
    private void tableViewFacturaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void keyPressEventos(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (txtPedido.isFocused()) {
                buscarRecepcion();
            } else if (tableViewFactura.isFocused()) {
                posicion = tableViewFactura.getSelectionModel().getSelectedIndex() + "";
            }
//            else if (txtCodigo.isFocused()) {
//                buscarArticulo();
//            }
        }
    }

    private void buscarArticulo() {
        Articulo art = artDAO.buscarCod("");
        if (art != null) {
//            depositoList = new ArrayList<>();
//            tableViewDeposito.getItems().clear();
//
//            btnVerificarPrecio.setDisable(false);
//            chkMedida.setValue("UNIDAD");
//            txtContenido.setText("1");
//            txtExistencia.setText("0");
//            txtDescuento.setText("0");
//            txtGasto.setText("0");
//            txtCantidad.setText("1");
//
//            txtDescripcion.setText(art.getDescripcion());
//            txtCosto.setText(art.getCosto() + "");
//            if (art.getIva().getIdIva() == 1) {
//                txtDescriIva.setText("EXE");
//                txtPorcIva.setText(art.getIva().getPoriva() + "");
//                txtExenta.setText(art.getCosto() + "");
//            } else {
//                txtDescriIva.setText("GRA");
//                txtPorcIva.setText(art.getIva().getPoriva() + "");
//                long iva = 0;
//                if (art.getIva().getIdIva() == 2) {
//                    iva = Math.round(art.getCosto() / 21);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                } else {
//                    iva = Math.round(art.getCosto() / 11);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                }
//                txtGravada.setText(iva + "");
//            }
//            verificarNivel1(art.getIdArticulo());
//            List<Existencia> listExis = existenciaDAO.listarPorArticulo(art.getIdArticulo());
//            for (Existencia ex : listExis) {
////                try {
//                Articulo articu = new Articulo();
//                articu.setIdArticulo(art.getIdArticulo());
//
//                ex.setArticulo(articu);
////                    JSONObject objJSON = (JSONObject) parser.parse(gson.toJson(ex).toString());
//                JSONObject objJSON = new JSONObject();//(JSONObject) parser.parse(gson.toJson(ex).toString());
//                JSONObject objART = new JSONObject();
//                objART.put("idArticulo", art.getIdArticulo());
//
//                JSONObject objDEPO = new JSONObject();
//                objDEPO.put("idDeposito", ex.getDeposito().getIdDeposito());
//                objDEPO.put("descripcion", ex.getDeposito().getDescripcion());
//
//                objJSON.put("idExistencia", ex.getIdExistencia());
//                objJSON.put("cantidad", ex.getCantidad());
//                objJSON.put("articulo", objART);
//                objJSON.put("deposito", objDEPO);
//
//                depositoList.add(objJSON);
//
//                actualizandoTablaDeposito();
////                } catch (ParseException ex1) {
////                    Logger.getLogger(FacturaCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex1);
////                }
//            }
        } else {
            mensajeAlerta("NO SE ENCUENTRAN RESULTADO DE LA BUSQUEDA");
        }
    }

//    private void actualizandoTablaDeposito() {
//        depositoData = FXCollections.observableArrayList(depositoList);
//        //columna Nombre ..................................................
////        tableColumnNombre.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
////        tableColumnNombre.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
////            @Override
////            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
////                String nombre = String.valueOf(data.getValue().get("nombre"));
////                return new ReadOnlyStringWrapper(nombre.toUpperCase());
////            }
////        });
//        //columna Nombre ..................................................
//        //columna Apellido ..................................................
//        tableColumnDeposito.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnDeposito.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                try {
//                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
//                    String apellido = String.valueOf(jsonDepo.get("descripcion"));
//                    if (apellido.contentEquals("null") || apellido.contentEquals("")) {
//                        apellido = "-";
//                    }
//                    return new ReadOnlyStringWrapper(apellido.toUpperCase());
//                } catch (ParseException ex) {
//                    Logger.getLogger(PedidoCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex);
//                    return null;
//                }
//            }
//        });
//        //columna Apellido ..................................................
//        //columna Ruc .................................................
//        tableColumnCantDepo.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
//        tableColumnCantDepo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
//            @Override
//            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
//                //                    JSONObject jsonDepo = (JSONObject) parser.parse(data.getValue().get("deposito").toString());
//                String apellido = String.valueOf(data.getValue().get("cantidad"));
//                if (apellido.contentEquals("null") || apellido.contentEquals("")) {
//                    apellido = "-";
//                }
//                return new ReadOnlyStringWrapper(apellido.toUpperCase());
//            }
//        });
//        //columna Ruc .................................................
//        tableViewDeposito.setItems(depositoData);
////        if (!escucha) {
////            escucha();
////        }
//    }
    private void ubicandoContenedorSecundario() {
        //***
//        anchorPaneFactura.setStyle("-fx-background-color: red;");
//        secondPane.setStyle("-fx-background-color: red;");
//        anchorPaneFactura.getChildren().get(anchorPaneFactura.getChildren().indexOf(secondPane)).setStyle("-fx-background-color: red;");
        anchorPaneFactura.getChildren()
                .get(anchorPaneFactura.getChildren().indexOf(secondPane))
                .relocate(((Utilidades.getWidth() - secondPane.getPrefWidth()) / 2), ((Utilidades.getHeight() - secondPane.getPrefHeight()) / 2));
        //***
//        anchorPaneFactura.getChildren()
//                .get(anchorPaneFactura.getChildren().indexOf(imageViewLogoEstetica))
//                .relocate(((MainApp.getWidth() - imageViewLogoEstetica.getLayoutBounds().getWidth()) / 2), ((MainApp.getHeight() - imageViewLogoEstetica.getLayoutBounds().getHeight()) / 2));
        //***
    }

//    PARA NUESTRO ENTORNO
//    private boolean recuperarGiftEnUso(String codigo) {
//        boolean comprado = false;
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT comprado FROM stock.articulo WHERE comprado=true AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    comprado = true;
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        }
//        return comprado;
//    }
    private boolean recuperarGiftEnUso(String codigo) {
        boolean comprado = false;
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT comprado FROM stk_articulos WHERE comprado=1 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    comprado = true;
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        }
        return comprado;
    }
//    PARA NUESTRO ENTORNO
//    private Map recuperarGiftSinUso(String codigo) {
//        Map mapeo = new HashMap();
////        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarServer()) {
//            String sql = "SELECT * FROM stock.articulo WHERE comprado=false AND UPPER(descripcion) LIKE 'TARJETA GIFT%' AND cod_articulo=" + codigo;
////            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
//                ResultSet rs = ps.executeQuery();
//                if (rs.next()) {
//                    mapeo.put("comprado", true);
//                    mapeo.put("codigo", rs.getLong("cod_articulo"));
//                    mapeo.put("saldogift", rs.getDouble("saldogift"));
//                    mapeo.put("fecha", rs.getDate("fechavtogift"));
//                    mapeo.put("descripcion", rs.getString("descripcion"));
//                } else {
//                    mapeo.put("comprado", false);
//                }
//                ps.close();
//            } catch (SQLException ex) {
//                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
//            }
////            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarServer();
//        } else {
//            mapeo.put("comprado", false);
//        }
//        return mapeo;
//    }

    private Map recuperarGiftSinUso(String codigo) {
        Map mapeo = new HashMap();
        if (ConexionParana.conectarBloque3()) {
//        if (ConexionPostgres.conectarLocal()) {
            String sql = "SELECT * FROM stk_articulos WHERE comprado=0 AND upper(ssecion)='GIFT CARD' AND codigo=" + codigo;
            try (PreparedStatement ps = ConexionParana.getConBloque3().prepareStatement(sql)) {
//            try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    mapeo.put("comprado", true);
                    mapeo.put("codigo", rs.getLong("codigo"));
                    mapeo.put("saldogift", rs.getDouble("saldogift"));
                    mapeo.put("fecha", rs.getDate("fechavtogift"));
                    mapeo.put("descripcion", rs.getString("descripcion"));
                } else {
                    mapeo.put("comprado", false);
                }
                ps.close();
            } catch (SQLException ex) {
                Utilidades.log.info("-->> " + ex.getLocalizedMessage());
            }
            ConexionParana.cerrarBloque3();
//            ConexionPostgres.cerrarLocal();
        } else {
            mapeo.put("comprado", false);
        }
        return mapeo;
    }

    private void resetFormulario(long total) {
        JSONObject talonarioSucursal;
        try {
            talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
            JSONObject sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
            JSONObject caja = (JSONObject) parser.parse(datos.get("caja").toString());
            TalonariosSucursales talos = taloDAO.getById(Long.valueOf(talonarioSucursal.get("idTalonariosSucursales").toString()));
            // primer trío
            long idSucursal = Long.parseLong(sucursal.get("idSucursal").toString());
            // segundo trío
            long nroCaja = Long.parseLong(caja.get("nroCaja").toString());
            long nroActual = talos.getNroActual();

//                datos.put("nroFact", Long.parseLong(Utilidades.procesandoNro(idSucursal, nroCaja, nroActual).replace("-", "")));
            JSONObject cajas = (JSONObject) parser.parse(datos.toString());
            JSONObject caj = (JSONObject) parser.parse(cajas.get("caja").toString());
            tipoCaja = (JSONObject) caj.get("tipoCaja");

            JSONObject usuario = (JSONObject) parser.parse(users.get("usuario").toString());
            JSONObject jsonFuncionario = (JSONObject) usuario.get("funcionario");
            if (jsonFuncionario != null) {
                String nomFuncionario = "";
                String apeFuncionario = "";
                if (jsonFuncionario.get("nombre") != null) {
                    nomFuncionario = jsonFuncionario.get("nombre").toString();
                }
                if (jsonFuncionario.get("apellido") != null) {
                    apeFuncionario = jsonFuncionario.get("apellido").toString();
                }

            } else {

            }
        } catch (ParseException ex) {
            Utilidades.log.error("ParseException", ex.fillInStackTrace());
        }
        primeraInsercion = false;

        cargandoImagen();
    }

    private void listenCheckExtranjero() {
//        chkExtranjero.selectedProperty().addListener(new ChangeListener<Boolean>() {
//            @Override
//            public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
//                CajaDeDatos.getCaja().put("estadoExt", newValue);
//            }
//        });
    }

    @FXML
    private void btnProveedorAction(ActionEvent event) {
        buscarProveedor();
    }

    @FXML
    private void btnBorrarDetalleAction(ActionEvent event) {
    }

    @FXML
    private void btnSeccionAction(ActionEvent event) {
    }

    @FXML
    private void btnNoserAction(ActionEvent event) {
    }

    @FXML
    private void btnCargarVtoProdAction(ActionEvent event) {
    }

    private void btnVerificarPrecioAction(ActionEvent event) {
        verificarPrecio();
    }

    private void btnGuardarDetalleAction(ActionEvent event) {
        guardarDetalle();
    }

    @FXML
    private void btnBuscarOrdenCompraAction(ActionEvent event) {
    }

    @FXML
    private void btnEditarAction(ActionEvent event) {
    }

    @FXML
    private void btnGuardarAction(ActionEvent event) {
        actualizarPedido();
    }

    @FXML
    private void btnProcesarAction(ActionEvent event) {
        guardarCompra();
    }

    @FXML
    private void btnBorrarAction(ActionEvent event) {
    }

    @FXML
    private void btnNuevoAction(ActionEvent event) {
    }

    @FXML
    private void btnSalirAction(ActionEvent event) {
        this.sc.loadScreen("/vista/stock/menuStockFXML.fxml", 540, 359, "/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
    }

    @FXML
    private void txtNroEntradaKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void buscarRecepcion() {
        if (txtPedido.getText().equals("")) {

        } else {
            PedidoCab ped = pedidoCabDAO.getByNroOrden(txtPedido.getText());
            if (ped != null) {
                if (ped.getOc() == null) {
                    nroPedido = txtPedido.getText();

                    if (ped.getMoneda().equals("GUARANIES")) {
                        chkMonedaExtran.setValue("--SELECCIONE MONEDA--");
                        txtMonedaExtran.setText(ped.getCotizacion());
                    } else {
                        chkMonedaExtran.setValue(ped.getMoneda());
                        txtMonedaExtran.setText(ped.getCotizacion());
                    }

                    cotizacion = txtMonedaExtran.getText();
                    moneda = chkMonedaExtran.getSelectionModel().getSelectedItem();

                    txtProveedor.setText(ped.getProveedor().getDescripcion());
                    txtIdProveedor.setText(ped.getProveedor().getIdProveedor() + "");
                    ped.getProveedor().setFechaAlta(null);
                    ped.getProveedor().setFechaMod(null);
                    JSONObject jsonProveedor;
                    try {
                        jsonProveedor = (JSONObject) parser.parse(gson.toJson(ped.getProveedor().toProveedorBdDTO()));
                        BuscarClienteFXMLController.seteandoParamCliente(jsonProveedor);
                    } catch (ParseException ex) {
                        jsonProveedor = new JSONObject();
                        jsonProveedor.put("idProveedor", ped.getProveedor().getIdProveedor() + "");
                        jsonProveedor.put("descripcion", ped.getProveedor().getDescripcion());
                        jsonProveedor.put("ruc", ped.getProveedor().getRuc());
                        BuscarClienteFXMLController.seteandoParamCliente(jsonProveedor);
                        Logger.getLogger(PedidoCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    txtNroTimbrado.setText(ped.getProveedor().getTimbrado() + "");
                    txtRucProveedor.setText(ped.getProveedor().getRuc());

                    txtObservacion.setText(ped.getObservacion());
                    txtPedido.setText(ped.getNroPedido());
                    String date = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date(ped.getFecha().getTime()));
                    txtFechaPedido.setText(date);

                    chkEmpresa.setValue(ped.getSucursal().toUpperCase());
                    chkTipoDoc.setValue("CO");
                    txtPlazo.setText(ped.getPlazo());
                    txtFrecuencia.setText(ped.getFrecuencia());
//            chkClaseMov.setValue("MER");
//            chkSituacion.setValue("CONTRIBUYENTE");
                    idPedidoCab = ped.getIdPedidoCab();
//            cargarDetalle();a   
                } else {
                    mensajeAlerta("YA SE HA GENERADO LA ORDEN DE COMPRA...");
                }

            } else {
                mensajeAlerta("NO EXISTE EL NUMERO DE PEDIDO INGRESADO...");
//            if (BuscarProveedorFXMLController.getJsonCliente() == null) {
//                mensajeAlerta("DEBE SELECCIONAR UN PROVEEDOR...");
//            } else {
//                buscarOrden();
//            }

            }
        }
    }

    private void buscarRecep() {
        PedidoCab ped = pedidoCabDAO.getByNroOrden(txtPedido.getText());
        try {
            JSONObject jsonProveedor = (JSONObject) parser.parse(gson.toJson(ped.getProveedor().toProveedorBdDTO()));
            BuscarClienteFXMLController.seteandoParamCliente(jsonProveedor);
        } catch (ParseException ex) {
            Logger.getLogger(PedidoCompraFXMLController.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (ped != null) {
//        if (ped.getOc() == null) {
            nroPedido = txtPedido.getText();

            txtProveedor.setText(ped.getProveedor().getDescripcion());
            txtNroTimbrado.setText(ped.getProveedor().getTimbrado() + "");
            txtRucProveedor.setText(ped.getProveedor().getRuc());

            txtObservacion.setText(ped.getObservacion());
            txtPedido.setText(ped.getNroPedido());
            String date = new java.text.SimpleDateFormat("dd/MM/yyyy").format(new Date(ped.getFecha().getTime()));
            txtFechaPedido.setText(date);

            chkEmpresa.setValue(ped.getSucursal().toUpperCase());
            chkTipoDoc.setValue("CO");
//            chkClaseMov.setValue("MER");
//            chkSituacion.setValue("CONTRIBUYENTE");
            idPedidoCab = ped.getIdPedidoCab();
//            cargarDetalle();a   
//        } else {
//            mensajeAlerta("YA SE HA GENERADO LA ORDEN DE COMPRA...");
//        }

        } else {
            mensajeAlerta("NO EXISTE EL NUMERO DE PEDIDO INGRESADO...");
            //            if (BuscarProveedorFXMLController.getJsonCliente() == null) {
            //                mensajeAlerta("DEBE SELECCIONAR UN PROVEEDOR...");
            //            } else {
            //                buscarOrden();
            //            }
            //        }
        }
    }

    private void txtCodigoKeyReleased(KeyEvent event) {
        keyPressEventos(event);
    }

    private void verificarNivel1(long id) {
        ArticuloNf1Tipo anf1Tipo = anf1TipoDAO.getByIdArticulo(id);
        if (anf1Tipo != null) {
            nivel01 = anf1Tipo.getNf1Tipo().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel01);
            txtSec1.setText(nivel01);
            verificarNivel2(id);
        }
    }

    private void verificarNivel2(long id) {
        ArticuloNf2Sfamilia anf2Familia = anf2SfamiliaDAO.getByIdArticulo(id);
        if (anf2Familia != null) {
            nivel02 = anf2Familia.getNf2Sfamilia().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel02);
            verificarNivel3(id);
        }
    }

    private void verificarNivel3(long id) {
        ArticuloNf3Sseccion anf3Seccion = anf3SeccionDAO.getByIdArticulo(id);
        if (anf3Seccion != null) {
            nivel03 = anf3Seccion.getNf3Sseccion().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel03);
            verificarNivel4(id);
        }
    }

    private void verificarNivel4(long id) {
        ArticuloNf4Seccion1 anf4Seccion = anf4SeccionDAO.getByIdArticulo(id);
        if (anf4Seccion != null) {
            nivel04 = anf4Seccion.getNf4Seccion1().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel04);
            verificarNivel5(id);
        }
    }

    private void verificarNivel5(long id) {
        ArticuloNf5Seccion2 anf5Seccion = anf5SeccionDAO.getByIdArticulo(id);
        if (anf5Seccion != null) {
            nivel05 = anf5Seccion.getNf5Seccion2().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel05);
            verificarNivel6(id);
        }
    }

    private void verificarNivel6(long id) {
        ArticuloNf6Secnom6 anf6Seccion = anf6SeccionDAO.getByIdArticulo(id);
        if (anf6Seccion != null) {
            nivel06 = anf6Seccion.getNf6Secnom6().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel06);
            verificarNivel7(id);
        }
    }

    private void verificarNivel7(long id) {
        ArticuloNf7Secnom7 anf7Seccion = anf7SeccionDAO.getByIdArticulo(id);
        if (anf7Seccion != null) {
            nivel07 = anf7Seccion.getNf7Secnom7().getDescripcion();//json.get("descripcion").toString();
            txtSec2.setText(nivel07);
        }
    }

    private void guardarDetalle() {
//        numValidator = new NumberValidator();
//        if (chkDepDestino.getSelectionModel().getSelectedIndex() >= 0 && chkTipo.getSelectionModel().getSelectedIndex() >= 0 && !txtCantidad.getText().equals("") && !txtCantidad.getText().equals("0")) {
//            orden++;
//           
//                    if (txtGsGrav5.getText().equals("")) {
//                        long tGra5 = Long.parseLong(txtGravada.getText());
//                        txtGsGrav5.setText((0 + tGra5) + "");
//                    } else {
//                        long tGra5 = Long.parseLong(txtGravada.getText());
//                        txtGsGrav5.setText((Long.parseLong(txtGsGrav5.getText()) + tGra5) + "");
//                    }
//                    long val5 = (Long.parseLong(txtCosto.getText()) * Long.parseLong(txtCantidad.getText())) - Long.parseLong(txtGsGrav5.getText());
//                    txtGsIva5.setText(val5 + "");
////                   
//                case "10":
//                    if (txtGsGrav10.getText().equals("")) {
//                        long tGra10 = Long.parseLong(txtGravada.getText());
//                        txtGsGrav10.setText((0 + tGra10) + "");
//                    } else {
//                        long tGra10 = Long.parseLong(txtGravada.getText());
//                        txtGsGrav10.setText((Long.parseLong(txtGsGrav10.getText()) + tGra10) + "");
//                    }
//                    long val10 = (Long.parseLong(txtCosto.getText()) * Long.parseLong(txtCantidad.getText())) - Long.parseLong(txtGsGrav10.getText());
//                    
//                    break;
//                default:
//                    break;
//            }
////          
//            }
//            long gsGra5 = 0l;
//            if (!txtGsGrav5.getText().equals("")) {
//                gsGra5 = Long.parseLong(txtGsGrav5.getText());
//            }
//            long gsGra10 = 0l;
//            if (!txtGsGrav10.getText().equals("")) {
//                gsGra10 = Long.parseLong(txtGsGrav10.getText());
//            }
//            long gsIva5 = 0l;
//            if (!txtGsIva5.getText().equals("")) {
//                gsIva5 = Long.parseLong(txtGsIva5.getText());
//            }
//            long gsIva10 = 0l;
//            if (!txtGsIva10.getText().equals("")) {
//                gsIva10 = Long.parseLong(txtGsIva10.getText());
//            }
//            long total = gsExe + gsGra5 + gsGra10 + gsIva5 + gsIva10;
//            
//            JSONObject jsonDetalle = new JSONObject();
//            JSONObject jsonRecep = new JSONObject();
//            jsonRecep.put("idRecepcion", recepcionDAO.getByNroOrden(txtNroEntrada.getText()).getIdRecepcion());
//
//            JSONObject jsonArt = new JSONObject();
//            jsonArt.put("idArticulo", artDAO.buscarCod(txtCodigo.getText()).getIdArticulo());
//
//            jsonDetalle.put("recepcion", jsonRecep);
//            jsonDetalle.put("articulo", jsonArt);
//            jsonDetalle.put("cantidad", txtCantidad.getText());
//            jsonDetalle.put("precio", txtCosto.getText());
//            jsonDetalle.put("descripcion", txtDescripcion.getText());
//            if (txtExenta.getText().equals("")) {
//                jsonDetalle.put("poriva", txtPorcIva.getText());
//                jsonDetalle.put("iva", txtGravada.getText());
//            } else {
//                jsonDetalle.put("poriva", "0");
//                jsonDetalle.put("iva", txtExenta.getText());
//            }
//
//            jsonDetalle.put("orden", orden);
//            jsonDetalle.put("sec1", txtSec1.getText());
//            jsonDetalle.put("sec2", txtSec2.getText());
//            jsonDetalle.put("codArticulo", txtCodigo.getText());
//            jsonDetalle.put("deposito", chkDepDestino.getSelectionModel().getSelectedItem());
//            jsonDetalle.put("tipo", chkTipo.getSelectionModel().getSelectedItem());
//            jsonDetalle.put("medida", chkMedida.getSelectionModel().getSelectedItem());
//            jsonDetalle.put("contenido", txtContenido.getText());
//            jsonDetalle.put("existencia", txtExistencia.getText());
//            jsonDetalle.put("descuento", txtDescuento.getText());
//
//            detalleArtList.add(jsonDetalle);
//            vistaJSONObjectArtDet();
//
//            chkDepDestino.setValue("CC");
//            chkTipo.setValue("MER");
//            chkMedida.setValue("UNIDAD");
//            txtCodigo.setText("");
//            txtContenido.setText("");
//            txtNroPedido.setText("");
//            txtCantidad.setText("");
//            txtExistencia.setText("");
//            txtDescuento.setText("");
//            txtGasto.setText("");
//            txtCosto.setText("");
//            txtDescriIva.setText("");
//            txtPorcIva.setText("");
//            txtExenta.setText("");
//            txtGravada.setText("");
//
//            txtSec1.setText("");
//            txtDescripcion.setText("");
//            txtSec2.setText("");
//
//            btnVerificarPrecio.setDisable(true);
//
//            depositoList = new ArrayList<>();
//            tableViewDeposito.getItems().clear();
//            repeatFocus(txtCodigo);
//        } else {
//            mensajeAlerta("DEBE SELECCIONAR EL DEPOSITO, TIPO Y CANTIDAD PARA AGREGAR UN DETALLE");
//        }
    }

    public void limpiarCampos() {
        detalleArtList = new ArrayList<>();
        tableViewFactura.getItems().clear();
        BuscarProveedorFXMLController.resetParam();
        orden = 0;
        idPedidoCab = 0;
        txtMonExtranjeraExenta.setText("");
        txtMonExtranjeraGrav5.setText("");
        txtMonExtranjeraGrav10.setText("");
        txtMonExtranjeraIva5.setText("");
        txtMonExtranjeraIva10.setText("");
        txtMonExtranjeraTotal.setText("");

        txtGsExenta.setText("");
        txtGsGrav5.setText("");
        txtGsGrav10.setText("");
        txtGsIva5.setText("");
        txtGsIva10.setText("");
        txtGsTotal.setText("");

//        chkDepDestino.setValue("CC");
        chkTipoDoc.setValue("CO");
        chkEmpresa.setValue("CASA CENTRAL");
        chkDiasVisita.setValue("LUNES");
//        chkMedida.setValue("UNIDAD");
//        txtCodigo.setText("");
        txtContenido.setText("");
        txtPedido.setText("");
        txtFechaPedido.setText("");
        txtObservacion.setText("");
        txtCuotas.setText("");
        txtPlazo.setText("");
        txtFrecuencia.setText("");
        txtMonedaExtran.setText("");
        txtProveedor.setText("");
//        txtExenta.setText("");
//        txtGravada.setText("");

        txtSec1.setText("");
//        txtDescripcion.setText("");
        txtSec2.setText("");

//        chkTipoMovimiento.setValue("FAC");
        chkClaseMov.setValue("MER");
        chkSituacion.setValue("CONTRIBUYENTE");

//        txtTipoMovimiento.setText("");
        txtProveedorDoc.setText("");
        txtProveedor.setText("");
        txtNroTimbrado.setText("");
        txtRucProveedor.setText("");
//        txtNroEntrada.setText("");
        txtObservacion.setText("");
        txtClaseMov.setText("");
//        txtNroFactura.setText("");
//        txtFechaFactura.setText("");
//        txtVtoDias.setText("");
//        txtFechaRecepcion.setText("");
//        txtNroDespacho.setText("");
        txtNroPedidoSis.setText("");

        txtMonedaExtran.setText("");
        txtEntregaInicial.setText("");
        txtCantCuota.setText("");
        txtCuota.setText("");
//        rangoOCDAO.actualizarObtenerRangoActual(1l);
        labelOrdenCompra.setText(rangoOCDAO.recuperarActual() + "");
        repeatFocus(txtPedido);

//        tableViewFactura = new TableView<>();
    }

    private void buscarProveedor() {
        BuscarProveedorFXMLController.cargarDatos2(txtRucProveedor, txtProveedor, txtNroTimbrado, txtIdProveedor);
        this.sc.loadScreenModal("/vista/stock/BuscarProveedorFXML.fxml", 581, 450, "/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void buscarOrden() {
//        BuscarRecepcionFXMLController.cargarOrden(txtNroEntrada);
//        this.sc.loadScreenModal("/vista/stock/BuscarRecepcionFXML.fxml", 454, 450, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private void verificarPrecio() {
//        if (!txtCodigo.getText().equals("")) {
//            PedidoCompraFXMLController.setCodArt(txtCodigo.getText());
//            this.sc.loadScreenModal("/vista/stock/VerificarPrecioFXML.fxml", 745, 600, "/vista/stock/FacturaCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
//        } else {
//            mensajeAlerta("NO SE HA CARGADO NINGUN CODIGO DE ARTICULO");
//        }
    }

    private void txtCantidadKeyPress(KeyEvent event) {
        listenCantidad(event);
    }

    private void listenCantidad(KeyEvent event) {
        if (event.getCode().isDigitKey()) {
//            txtCantidad.textProperty().addListener((observable, oldValue, newValue) -> {
//                Platform.runLater(() -> {
//                    calcularCosto(newValue);
//                });
//            });
        }
    }

    private void calcularCosto(String newValue) {
//        if (!newValue.equals("")) {
//            if (txtPorcIva.getText().equalsIgnoreCase("0")) {
//                txtDescriIva.setText("EXE");
//                txtPorcIva.setText(0 + "");
//                long total = Long.parseLong(txtCantidad.getText()) * Long.parseLong(txtCosto.getText());
//                txtExenta.setText(total + "");
//            } else {
//                txtDescriIva.setText("GRA");
//                txtPorcIva.setText(txtPorcIva.getText());
//                long iva = 0;
//                if (txtPorcIva.equals("5")) {
//                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 21);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                    iva = iva * Long.parseLong(txtCantidad.getText());
//                } else {
//                    iva = Math.round(Long.parseLong(txtCosto.getText()) / 11);
//                    iva = Long.parseLong(txtCosto.getText()) - iva;
//                    iva = iva * Long.parseLong(txtCantidad.getText());
//                }
//                txtGravada.setText(iva + "");
//            }
//        }
    }

    @FXML
    private void btnNuevoArtAction(ActionEvent event) {
        nuevoArt();
    }

    @FXML
    private void btnEditarArtAction(ActionEvent event) {
        editarDetalle();
    }

    @FXML
    private void btnEliminarArtAction(ActionEvent event) {
        eliminarDetalle();
    }

    private void cargarDetalle(long idPedCab) {
        PedidoCab pc = actualizarCabecera(idPedCab);
        pedidoCabDAO.actualizar(pc);
//    private List<PedidoDet> cargarDetalle(long idPedCab) {
        if (!txtCuotas.equals("") && !txtPlazo.equals("") && !txtFrecuencia.equals("") && !chkTipoDoc.getSelectionModel().getSelectedItem().equals("")) {
            diaVisita = chkDiasVisita.getSelectionModel().getSelectedItem();
            tipoDoc = chkTipoDoc.getSelectionModel().getSelectedItem();
            cuota = txtCuotas.getText();
            plazo = txtPlazo.getText();
            observa = txtObservacion.getText();
            frecuencia = txtFrecuencia.getText();
            List<PedidoDet> listPed = pedidoDetDAO.listarPorCabecera(idPedCab);
            detalleArtList = new ArrayList<>();
            long exenta = 0;
            long gravada5 = 0;
            long gravada10 = 0;
            long totiva5 = 0;
            long totiva10 = 0;
            for (PedidoDet pedidoDet : listPed) {
                JSONObject jsonDet = new JSONObject();
                orden++;
                jsonDet.put("orden", orden);
                jsonDet.put("tipo", "MER");
                jsonDet.put("codigo", pedidoDet.getArticulo().getCodArticulo());
                jsonDet.put("descripcion", pedidoDet.getArticulo().getDescripcion());
                long cantidades = Long.parseLong(pedidoDet.getCantCacique()) + Long.parseLong(pedidoDet.getCantSanlo()) + Long.parseLong(pedidoDet.getCantCentral());
                jsonDet.put("cantidad", cantidades);
                jsonDet.put("cantidadCC", pedidoDet.getCantCentral());
                jsonDet.put("cantidadSL", pedidoDet.getCantSanlo());
                jsonDet.put("cantidadSC", pedidoDet.getCantCacique());
                jsonDet.put("medida", "UNI");
                jsonDet.put("descuento", pedidoDet.getDescuento());
                jsonDet.put("costo", pedidoDet.getPrecio());

                jsonDet.put("fecha", pedidoDet.getPedidoCab().getFecha());
                jsonDet.put("plazo", pedidoDet.getPedidoCab().getPlazo());
                jsonDet.put("obs", pedidoDet.getPedidoCab().getObservacion());
                jsonDet.put("tipo", pedidoDet.getPedidoCab().getTipo());
                jsonDet.put("comprador", pedidoDet.getPedidoCab().getUsuario());
                jsonDet.put("cc", Long.parseLong(pedidoDet.getCantCentral()));
                jsonDet.put("sl", Long.parseLong(pedidoDet.getCantSanlo()));
                jsonDet.put("sc", Long.parseLong(pedidoDet.getCantCacique()));
                long iva = 0;
                if (pedidoDet.getArticulo().getIva().getPoriva() == 0) {
                    jsonDet.put("iva", "0");
                    jsonDet.put("exenta", (pedidoDet.getPrecio() * cantidades));
                    exenta += pedidoDet.getPrecio() * cantidades;
                } else {
                    if (pedidoDet.getArticulo().getIva().getPoriva() == 5) {
                        jsonDet.put("iva", "5");
                        long resul = cantidades * pedidoDet.getPrecio();
                        iva = Math.round(iva * 1.05);
                        iva -= resul;
                        totiva5 += iva;

//                        iva = (pedidoDet.getPrecio() * cantidades) - iva;
                        //                    iva = iva * ;
                        gravada5 += resul;
                    } else {
                        jsonDet.put("iva", "10");
//                        iva = cantidades * pedidoDet.getPrecio();
                        long resul = cantidades * pedidoDet.getPrecio();
                        iva = Math.round(iva * 1.1);
                        iva -= resul;
                        totiva10 += iva;

//                        iva = (pedidoDet.getPrecio() * cantidades) - iva;
//                    iva = iva * cantidades;
                        gravada10 += resul;
                    }
                    jsonDet.put("gravada", (cantidades * pedidoDet.getPrecio()) + "");
                }
                jsonDet.put("deposito", "DEPOSITO");
                detalleArtList.add(jsonDet);
            }
            txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(exenta))));
            txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(gravada5))));
            txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(gravada10))));
            txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totiva5))));
            txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totiva10))));
            long tot = (exenta + gravada5 + gravada10 + totiva5 + totiva10);
            txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(tot))));
            actualizandoTablaFactura();
        } else {
            mensajeAlerta("LOS CAMPOS CUOTAS, PLAZO, FRECUENCIA Y TIPO DOCUMENTO NO DEBE QUEDAR VACIO");
        }

    }

    private PedidoCab actualizarCabecera(long idPedCab) {
        PedidoCab pedCab = pedidoCabDAO.getById(idPedCab);
        pedCab.setTipo(chkTipoDoc.getSelectionModel().getSelectedItem());
        pedCab.setVisita(chkDiasVisita.getSelectionModel().getSelectedItem());
        pedCab.setCuotas(txtCuotas.getText());
        pedCab.setPlazo(txtPlazo.getText());
        pedCab.setFrecuencia(txtFrecuencia.getText());
        pedCab.setOc(null);

        return pedCab;
    }

    private void eliminarDetalle() {
        if (tableViewFactura.getSelectionModel().getSelectedIndex() >= 0) {
            System.out.println("INDICE -->> " + tableViewFactura.getSelectionModel().getSelectedIndex());
            System.out.println("ELEMENTO -->> " + tableViewFactura.getSelectionModel().getSelectedItem());
            tableViewFactura.getItems().remove(tableViewFactura.getSelectionModel().getSelectedIndex());
            detalleArtList.remove(tableViewFactura.getSelectionModel().getSelectedIndex());
            mensajeAlerta("UN DETALLE DE ARTICULO HA SIDO ELIMINADO");
            calcularTotales();
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE DE ARTICULO PARA ELIMINAR...");
        }
    }

    private void calcularTotales() {
        long exenta = 0;
        long gravada5 = 0;
        long gravada10 = 0;
        long totiva5 = 0;
        long totiva10 = 0;
        orden = 0;
        List<JSONObject> lista = getDetalleArtList();
        detalleArtList = new ArrayList<>();
        for (JSONObject JSONpedidoDet : lista) {
//            PedidoDet pedidoDet = gson.fromJson(JSONpedidoDet.toString(), PedidoDet.class);
            orden++;
            JSONObject jsonDet = new JSONObject();
            jsonDet.put("orden", orden);
            jsonDet.put("tipo", "MER");
            jsonDet.put("codigo", JSONpedidoDet.get("codigo").toString());
            jsonDet.put("descripcion", JSONpedidoDet.get("descripcion").toString());
//            long cantidades = Long.parseLong(pedidoDet.getCantCacique()) + Long.parseLong(pedidoDet.getCantSanlo()) + Long.parseLong(pedidoDet.getCantCentral());
            jsonDet.put("cantidad", JSONpedidoDet.get("cantidad").toString());
            jsonDet.put("cantidadCC", JSONpedidoDet.get("cantidadCC").toString());
            jsonDet.put("cantidadSL", JSONpedidoDet.get("cantidadSL").toString());
            jsonDet.put("cantidadSC", JSONpedidoDet.get("cantidadSC").toString());
            jsonDet.put("cc", Long.parseLong(JSONpedidoDet.get("cantidadCC").toString()));
            jsonDet.put("sl", Long.parseLong(JSONpedidoDet.get("cantidadSL").toString()));
            jsonDet.put("sc", Long.parseLong(JSONpedidoDet.get("cantidadSC").toString()));
            try {
                jsonDet.put("fecha", JSONpedidoDet.get("fecha").toString());
            } catch (Exception e) {
                jsonDet.put("fecha", null);
            }
            jsonDet.put("medida", "UNI");
            try {
                jsonDet.put("plazo", JSONpedidoDet.get("plazo").toString());
            } catch (Exception e) {
                jsonDet.put("plazo", "0");
            } finally {
            }

            try {
                jsonDet.put("obs", JSONpedidoDet.get("obs").toString());
            } catch (Exception e) {
                jsonDet.put("obs", "");
            } finally {
            }
            try {
                jsonDet.put("comprador", JSONpedidoDet.get("comprador").toString());
            } catch (Exception e) {
                jsonDet.put("comprador", "");
            } finally {
            }
            try {
                jsonDet.put("descuento", JSONpedidoDet.get("descuento").toString());
            } catch (Exception e) {
                jsonDet.put("descuento", "0");
            } finally {
            }
            jsonDet.put("costo", JSONpedidoDet.get("costo").toString());
            long iva = 0;

            long costo = (Long.parseLong(JSONpedidoDet.get("costo").toString()) * Long.parseLong(JSONpedidoDet.get("cantidad").toString()));
            if (JSONpedidoDet.get("iva").toString().equals("0")) {
                exenta += Long.parseLong(JSONpedidoDet.get("exenta").toString());
                jsonDet.put("exenta", JSONpedidoDet.get("exenta").toString());
                jsonDet.put("iva", "0");
            } else {
                if (JSONpedidoDet.get("iva").toString().equals("5")) { //                    gravada5 += Long.parseLong(JSONpedidoDet.get("gravada").toString());
                    //                    iva5 += (Long.parseLong(JSONpedidoDet.get("costo").toString()) * Long.parseLong(JSONpedidoDet.get("cantidad").toString())) - Long.parseLong(JSONpedidoDet.get("gravada").toString());
                    gravada5 += costo;
                    jsonDet.put("gravada", costo);
                    jsonDet.put("iva", "5");
                    long iva5 = Math.round(costo * 1.05);
                    totiva5 += (iva5 - costo);
                } else {
//                    gravada10 += Long.parseLong(JSONpedidoDet.get("gravada").toString());
                    jsonDet.put("gravada", costo);
                    gravada10 += costo;
//                    iva10 += (Long.parseLong(JSONpedidoDet.get("costo").toString()) * Long.parseLong(JSONpedidoDet.get("cantidad").toString())) - Long.parseLong(JSONpedidoDet.get("gravada").toString());
                    jsonDet.put("iva", "10");
                    long iva10 = Math.round(costo * 1.1);
                    totiva10 += (iva10 - costo);
                }
            }
            jsonDet.put("deposito", "DEPOSITO");
            detalleArtList.add(jsonDet);
        }
        txtGsExenta.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(exenta))));
        txtGsGrav5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(gravada5))));
        txtGsGrav10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(gravada10))));
        txtGsIva5.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totiva5))));
        txtGsIva10.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(totiva10))));
        long tot = (exenta + gravada5 + gravada10 + totiva5 + totiva10);
        txtGsTotal.setText(numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(String.valueOf(tot))));
//        detalleArtList = new ArrayList<>();
        tableViewFactura.getItems().clear();
        actualizandoTablaFactura();
    }

    private void editarDetalle() {
        if (tableViewFactura.getSelectionModel().getSelectedIndex() >= 0) {
            JSONObject jsonProveedor = new JSONObject();
            jsonProveedor.put("idProveedor", txtIdProveedor.getText() + "");
            jsonProveedor.put("descripcion", txtProveedor.getText());
            jsonProveedor.put("ruc", "");
            nomProv = txtProveedor.getText();
            idProv = txtIdProveedor.getText();
            BuscarClienteFXMLController.seteandoParamCliente(jsonProveedor);
            posicion = tableViewFactura.getSelectionModel().getSelectedIndex() + "";
            ModCantidadSucursalFXMLController.setCantidad(tableViewFactura.getSelectionModel().getSelectedItem().get("cantidad").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantidadCC").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantidadSL").toString(), tableViewFactura.getSelectionModel().getSelectedItem().get("cantidadSC").toString(), tableViewFactura);
            this.sc.loadScreenModal("/vista/stock/ModCantidadSucursalFXML.fxml", 247, 196, "/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        } else {
            mensajeAlerta("DEBE SELECCIONAR UN DETALLE DE ARTICULO PARA EDITAR...");
        }
    }

    private void nuevoDetalle() {
//        if (tableViewFactura.getSelectionModel().getSelectedIndex() >= 0) {
//        posicion = tableViewFactura.getSelectionModel().getSelectedIndex() + "";
        JSONObject jsonProveedor = new JSONObject();
        jsonProveedor.put("idProveedor", txtIdProveedor.getText() + "");
        jsonProveedor.put("descripcion", txtProveedor.getText());
        jsonProveedor.put("ruc", "");
        nomProv = txtProveedor.getText();
        idProv = txtIdProveedor.getText();
        BuscarClienteFXMLController.seteandoParamCliente(jsonProveedor);
        CodArticuloFXMLController.setCantidad("0", tableViewFactura);
        this.sc.loadScreenModal("/vista/stock/CodArticuloFXML.fxml", 286, 203, "/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), false);
        //            System.out.println("INDICE -->> " + tableViewFactura.getSelectionModel().getSelectedIndex());
        //            System.out.println("ELEMENTO -->> " + tableViewFactura.getSelectionModel().getSelectedItem());
        //            tableViewFactura.getItems().remove(tableViewFactura.getSelectionModel().getSelectedIndex());
        //            detalleArtList.remove(tableViewFactura.getSelectionModel().getSelectedIndex());
        //            mensajeAlerta("UN DETALLE DE ARTICULO HA SIDO ELIMINADO");
        //            calcularTotales();
//        } else {
//            mensajeAlerta("DEBE SELECCIONAR UN DETALLE DE ARTICULO PARA EDITAR...");
//        }
    }

    private void cargarMoneda() {
        chkMonedaExtran.getItems().addAll("--SELECCIONE MONEDA--", "DOLAR", "REAL", "PESO");
    }

    private void guardarCompra() {
        PedidoCab pCab = pedidoCabDAO.getByNroOrden(txtPedido.getText());
        pCab.setOc(rangoOCDAO.actualizarObtenerRangoActual(1L) + "");
        pCab.setTotal(Long.parseLong(numValidator.numberValidator(txtGsTotal.getText())));
        if (chkMonedaExtran.getSelectionModel().getSelectedItem().equals("--SELECCIONE MONEDA--")) {
            pCab.setMoneda("GUARANIES");
            pCab.setCotizacion("0");
        } else {
            pCab.setMoneda(chkMonedaExtran.getSelectionModel().getSelectedItem());
            pCab.setCotizacion(txtMonedaExtran.getText());
        }
        if (chkTipoDoc.getSelectionModel().getSelectedItem().equals("CO")) {
            pCab.setTipo("CONTADO");
        } else {
            pCab.setTipo("CREDITO");
        }
//        pCab.setMoneda(chkMonedaExtran.getValue());
//        pCab.setCotizacion(txtMonedaExtran.getText());
        pedidoCabDAO.actualizar(pCab);
        if (pCab.getIdPedidoCab() != null) {
            if (detalleArtList.size() > 0) {
                long i = 0;
                for (JSONObject detalle : detalleArtList) {
                    i++;
                    PedidoCompra pc = new PedidoCompra();
                    Articulo art = new Articulo();
                    art.setIdArticulo(artDAO.buscarCod(detalle.get("codigo").toString()).getIdArticulo());
                    pc.setArticulo(art);
                    pc.setCantidad(detalle.get("cantidad").toString());
                    pc.setCantCc(Long.parseLong(detalle.get("cantidadCC").toString()));
                    pc.setCantSc(Long.parseLong(detalle.get("cantidadSC").toString()));
                    pc.setCantSl(Long.parseLong(detalle.get("cantidadSL").toString()));
                    pc.setDescripcion(detalle.get("descripcion").toString());
                    PedidoCab pcabe = new PedidoCab();
                    pcabe.setIdPedidoCab(pCab.getIdPedidoCab());
                    pc.setPedidoCab(pcabe);
                    pc.setPrecio(Long.parseLong(detalle.get("costo").toString()));
                    try {
                        pc.setDescuento(detalle.get("descuento").toString());
                    } catch (Exception e) {
                        pc.setDescuento("0");
                    } finally {
                    }

                    long exenta = 0;
                    long gra5 = 0;
                    long gra10 = 0;
                    if (detalle.get("iva").toString().equals("0")) {
                        exenta = Long.parseLong(detalle.get("costo").toString()) * Long.parseLong(detalle.get("cantidad").toString());
                    } else {
                        if (detalle.get("iva").toString().equals("5")) {
                            long gravada5 = Long.parseLong(detalle.get("costo").toString()) * Long.parseLong(detalle.get("cantidad").toString());
                            long iva5 = Math.round(gravada5 / 21);
                            gra5 = gravada5 - iva5;
                        } else {
                            long gravada10 = Long.parseLong(detalle.get("costo").toString()) * Long.parseLong(detalle.get("cantidad").toString());
                            long iva10 = Math.round(gravada10 / 11);
                            gra10 = gravada10 - iva10;
                        }
                    }

                    pc.setTipo(detalle.get("tipo").toString());
                    pc.setExenta(exenta + "");
                    pc.setGrav5(gra5 + "");
                    pc.setGrav10(gra10 + "");
//                PedidoDe fcd = gson.fromJson(detalle.toString(), FacturaCompraDet.class);
                    try {
                        pedidoCompraDAO.insercionMasiva(pc, i);
                    } catch (Exception e) {
                        System.out.println("-> " + e.getLocalizedMessage());
                        System.out.println("-> " + e.getMessage());
                        System.out.println("-> " + e.fillInStackTrace());
                    } finally {
                    }
                }
//                        secondPane1.setVisible(false);
//                mensajeAlerta("COMPRA HA SIDO GENERADO EXITOSAMENTE...");
                mensajeDetalle("SE HA GENERADO LA ORDEN DE COMPRA... N° de OC: " + pCab.getOc(), "MENSAJE DEL SISTEMA");

//                        btnVerificarPrecio.setDisable(true);
                limpiarCampos();
                toaster = new Toaster();
//                        btnVerificarPrecio.setDisable(true);
                tableViewFactura.getItems().clear();
                detalleArtList = new ArrayList<>();
                orden = 0;
                repeatFocus(txtPedido);
//            ubicandoContenedorSecundario();
//            cargarTipoMovimiento();
//            cargarDeposito();
//            cargarTipo();
//            cargarMedida();
//            cargarSucursal();
//            cargarSituacion();
//            cargarClaseMovimiento();
//                        repeatFocus(txtNroEntrada);
            } else {
                mensajeAlerta("NO SE HA CARGADO NINGUN DETALLE A LA FACTURA");
            }
        }
    }

    private void actualizarPedido() {
//        if (pedidoDetDAO.eliminarPorcabecera(pedidoCabDAO.getByNroOrden(txtPedido.getText()).getIdPedidoCab())) {
//
//        }
    }

    @FXML
    private void btnImprimirAction(ActionEvent event) {
//        if (cbImpresion.getSelectionModel().getSelectedIndex() > 0) {
        if (detalleArtList.size() >= 0) {
//                if (cbImpresion.getSelectionModel().getSelectedIndex() == 1) {
//                    if (!txtPlazo.getText().equals("") && !txtFrecuencia.getText().equals("")) {
            imprimir();
//                    } else {
//            mensajeAlerta("EL CAMPO PLAZO Y FRECUENCIA NO DEBE QUEDAR VACIO");
//                    }
//                } 
        } else {
            mensajeAlerta("NO HAY DETALLE EN EL PEDIDO DE COMPRA..");
        }
//        } else {
//            mensajeAlerta("DEBE SELECCIONAR UN MODO DE IMPRESION..");
//            repeatFocus(cbImpresion);
//        }
    }

    private void imprimir() {
        JSONArray jsonFacturas = recuperarDatos();
        informandoVentas("{\"ventas\": " + jsonFacturas + "}", "REP_ORDEN_COMPRA");
        this.sc.loadScreenModal("/vista/util/JRViewerFXML.fxml", MainApp.getWidth(), MainApp.getHeight(), "/vista/stock/BuscarOrdenCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), true);
    }

    private JSONArray recuperarDatos() {
        JSONArray arrayCompra = new JSONArray();
        numValidator = new NumberValidator();
        long montoCC = 0;
        long montoSL = 0;
        long montoSC = 0;

        long totExe = 0;
        long totGra5 = 0;
        long totIva5 = 0;

        long totGra10 = 0;
        long totIva10 = 0;
        long totGral = 0;

        long grav = 0;
        long imp = 0;

        long rango = Long.parseLong(rangoOCDAO.recuperarActual()) - 1;
        for (JSONObject detalle : detalleArtList) {
            JSONObject json = new JSONObject();
            json.put("codigo", detalle.get("codigo").toString());
            json.put("descripcion", detalle.get("descripcion").toString());
            long result = rango;
            json.put("oc", result + "");
            try {
                json.put("descuento", Integer.parseInt(detalle.get("descuento").toString()));
            } catch (Exception e) {
                json.put("descuento", 0);
            }

            Articulo arti = artDAO.buscarCod(detalle.get("codigo").toString());
            if (arti.getIva().getPoriva() == 0) {
                totExe += Integer.parseInt(detalle.get("cantidad").toString()) * Integer.parseInt(detalle.get("costo").toString());;
            } else if (arti.getIva().getPoriva() == 5) {
                long costo = Integer.parseInt(detalle.get("cantidad").toString()) * Integer.parseInt(detalle.get("costo").toString());;
                long iva5 = Math.round(costo * 1.05);
                totIva5 += iva5 - costo;
                totGra5 += (costo);
            } else {
                long costo = Integer.parseInt(detalle.get("cantidad").toString()) * Integer.parseInt(detalle.get("costo").toString());;
                long iva10 = Math.round(costo * 1.1);
                totIva10 += iva10 - costo;
                totGra10 += (costo);
            }
            totGral = totGra10 + totGra5 + totIva5 + totIva10 + totExe;

            grav = totGra10 + totGra5;
            imp = totIva5 + totIva10;

            json.put("totExe", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totExe + ""))));
            json.put("totGra10", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totGra10 + ""))));
            json.put("totIva10", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totIva10 + ""))));
            json.put("totGra5", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totGra5 + ""))));
            json.put("totIva5", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totIva5 + ""))));
            json.put("totGral", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(totGral + ""))));
            json.put("grav", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(grav + ""))));
            json.put("imp", numValidator.numberFormat("Gs ###,###.###", Double.parseDouble(numValidator.numberValidator(imp + ""))));

            montoCC += Integer.parseInt(detalle.get("cc").toString()) * Integer.parseInt(detalle.get("costo").toString());
            montoSL += Integer.parseInt(detalle.get("sl").toString()) * Integer.parseInt(detalle.get("costo").toString());
            montoSC += Integer.parseInt(detalle.get("sc").toString()) * Integer.parseInt(detalle.get("costo").toString());

            if (montoCC > 0) {
                json.put("suc1", "Asuncion");
                json.put("mon1", montoCC + "");
                json.put("entre1", "Antequera 661 esq/ Herrera(Asuncion)");
            } else {
                json.put("suc1", "");
                json.put("mon1", "");
                json.put("entre1", "");
            }
            if (montoSC > 0) {
                json.put("suc2", "Suc Cacique");
                json.put("mon2", montoSC + "");
                json.put("entre2", "Avda. Cacique de Lambare N° 3425 c/ Juan de Ayolas");
            } else {
                json.put("suc2", "");
                json.put("mon2", "");
                json.put("entre2", "");
            }
            if (montoSL > 0) {
                json.put("suc3", "San Lorenzo");
                json.put("mon3", montoSL + "");
                json.put("entre3", "Ruta Mcal.Estigarribia Km.15-S.Lorenzo");
            } else {
                json.put("suc3", "");
                json.put("mon3", "");
                json.put("entre3", "");
            }

            if (txtPedido.getText().equalsIgnoreCase("0")) {
                json.put("nroPedido", "0");
            } else {
                json.put("nroPedido", txtPedido.getText());
            }

            json.put("cantCC", Integer.parseInt(detalle.get("cc").toString()));
            json.put("cantSL", Integer.parseInt(detalle.get("sl").toString()));
            json.put("cantSC", Integer.parseInt(detalle.get("sc").toString()));
            json.put("cantTotal", Integer.parseInt(detalle.get("cantidad").toString()));
            json.put("precio", Integer.parseInt(detalle.get("costo").toString()));
            int total = Integer.parseInt(detalle.get("cantidad").toString()) * Integer.parseInt(detalle.get("costo").toString());
            json.put("total", total);

            json.put("ruc", BuscarClienteFXMLController.getJsonCliente().get("ruc").toString());
            json.put("proveedor", BuscarClienteFXMLController.getJsonCliente().get("descripcion").toString());

            try {
                StringTokenizer st0 = new StringTokenizer(detalle.get("fecha").toString(), " ");
                String fec = st0.nextElement().toString(); //1
                json.put("fecha", fec);
            } catch (Exception e) {
                json.put("fecha", String.valueOf(LocalDate.now()));
            } finally {
            }
            json.put("plazo", detalle.get("plazo").toString());
            try {
                json.put("obs", txtObservacion.getText());
            } catch (Exception e) {
                json.put("obs", "");
            } finally {
            }

            json.put("tipo", detalle.get("tipo").toString());
            json.put("comprador", detalle.get("comprador").toString());

            Date date = new Date();
            Timestamp ts = new Timestamp(date.getTime());
            String fechaArray[] = ts.toString().split(" ");
//                System.out.println("ESTO RETORNA: " + fechaArray[1].toString());
            StringTokenizer st = new StringTokenizer(fechaArray[1].toString(), ".");
//                System.out.println("ESTO RETORNA 2: " + st.nextElement().toString());
            String subRepTimestamp = Utilidades.ordenandoFechaString(fechaArray[0]) + " " + st.nextElement().toString();
            json.put("subRepNomFun", Identity.getNomFun());
            json.put("subRepTimestamp", subRepTimestamp);
            json.put("sucursal", "CASA CENTRAL");

            arrayCompra.add(json);
        }
        return arrayCompra;
    }

    private void informandoVentas(String jsonString, String rep) {
        JasperPrint jasperPrint = null;
        JasperReport reporte = null;
        Map pSQL = null;
        try {
            InputStream subHeader = this.getClass().getResourceAsStream(PATH.JASPER_HEADER_VERTICAL);
            InputStream subImg = this.getClass().getResourceAsStream(PATH.JASPER_IMG);
            InputStream subImgCP = this.getClass().getResourceAsStream(PATH.JASPER_IMG_CP);
            reporte = (JasperReport) JRLoader.loadObject(this.getClass().getResourceAsStream(PATH.JASPER_REPORT + rep + ".jasper"));
            String dir = System.getProperty("user.dir") + PATH.PDF_JASPER_REPORT + rep + ".pdf";
            File fichero = new File(dir);

            if (fichero.delete()) {
                System.out.println("El fichero ha sido borrado satisfactoriamente");
            } else {
                System.out.println("El fichero no pudó ser borrado");
            }
//            System.out.println("REPORTE UBICACION -->> " + subHeader.);
            pSQL = new HashMap<String, Object>();
            // CUERPO - REPORTE
            pSQL.put("repJsonString", jsonString);
            pSQL.put("empresa", "PARANA FUNCIONAL S.A.");
            jasperPrint = JasperFillManager.fillReport(reporte, pSQL);
            new JRViewerFXML(jasperPrint, JRViewerFXMLMode.REPORT_VIEW, reporte.getPageWidth(), reporte.getPageHeight());
        } catch (JRException ex) {
            Utilidades.log.error("ERROR JRException: ", ex.fillInStackTrace());
        }
    }

    private void nuevoArt() {
        boolean val = true;
        try {
            Long.parseLong(txtIdProveedor.getText());
        } catch (Exception e) {
            val = false;
            mensajeError("DEBES SELECCIONAR EL PROVEEDOR...");
        } finally {
        }

        if (txtPedido.getText().equalsIgnoreCase("") && val) {
            PedidoCab pedidoCab = new PedidoCab();

            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            pedidoCab.setFecha(tsNow);
            pedidoCab.setNroPedido(rangoPedidoDAO.actualizarObtenerRangoActual(1L) + "");
            pedidoCab.setOc(rangoOCDAO.actualizarObtenerRangoActual(1L) + "");
            Proveedor prov = new Proveedor();
            prov.setIdProveedor(Long.parseLong(BuscarProveedorFXMLController.getJsonCliente().get("idProveedor").toString()));
            pedidoCab.setProveedor(prov);
            pedidoCab.setUsuario(Identity.getNomFun());
            pedidoCab.setSucursal(chkEmpresa.getSelectionModel().getSelectedItem());
            pedidoCab.setTotal(0l);
            if (chkMonedaExtran.getSelectionModel().getSelectedItem().equals("--SELECCIONE MONEDA--")) {
                pedidoCab.setMoneda("GUARANIES");
                pedidoCab.setCotizacion("0");
            } else {
                pedidoCab.setMoneda(chkMonedaExtran.getSelectionModel().getSelectedItem());
                pedidoCab.setCotizacion(txtMonedaExtran.getText());
            }
            PedidoCab pCab = pedidoCabDAO.insertarObtenerObj(pedidoCab);
            txtPedido.setText(pCab.getOc());
            nroPedido = pCab.getNroPedido();
            diaVisita = chkDiasVisita.getSelectionModel().getSelectedItem();
            tipoDoc = chkTipoDoc.getSelectionModel().getSelectedItem();
            cuota = txtCuotas.getText();
            plazo = txtPlazo.getText();
            observa = txtObservacion.getText();
            frecuencia = txtFrecuencia.getText();
        }
        if (val) {
            nuevoDetalle();
        }
    }
}
