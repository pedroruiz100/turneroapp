/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.stock;

import com.peluqueria.core.domain.ManejoLocal;
import com.peluqueria.dao.ClienteDAO;
import com.peluqueria.dao.ManejoLocalDAO;
import com.peluqueria.dao.RangoClienteDAO;
import com.peluqueria.dao.RangoClientefielDAO;
import com.peluqueria.dao.TarjetaClienteFielDAO;
import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.NumberValidator;
import com.javafx.util.StageSecond;
import com.javafx.util.Toaster;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ModCantidadFXMLController extends BaseScreenController implements Initializable {

    static void setCantidad(String cant, TableView tv) {
        cantidad = cant;
        tableFact = tv;
    }

    //para evitar inconvenientes con el message alert enter y escape...
    private NumberValidator numVal;
    static String cantidad = "";
    static TableView<JSONObject> tableFact;
    private ObservableList<JSONObject> clienteData;
    private List<JSONObject> clienteList;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ").create();
    boolean alertEscEnter;
//    boolean crearNuevo;

    final KeyCombination altC = new KeyCodeCombination(KeyCode.C, KeyCombination.ALT_DOWN);

    private List<JSONObject> clienteFielList;
    private static JSONObject jsonClienteFiel;

    @Autowired
    private ClienteDAO cliDAO;

    @Autowired
    private TarjetaClienteFielDAO tarDAO;

    @Autowired
    private RangoClienteDAO rangoCliDAO;

    @Autowired
    private RangoClientefielDAO rangoClifielDAO;

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();
    static ManejoLocal manejo = new ManejoLocal();
    //json cliente, necesario en forma de pago...
    private static JSONObject jsonCliente = new JSONObject();
    //json cliente, necesario en forma de pago...
    private JSONObject jsonSeleccionActual;
    private JSONObject clienteFiel;

    @FXML
    private AnchorPane anchorPaneBuscar;
    @FXML
    private Label labelRucClienteNuevo11;
    @FXML
    private AnchorPane anchorPaneCliente;
    @FXML
    private Label labelRetiroDinero;
    @FXML
    private TextField textFieldCantidad;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    private static void repeatFocusData(Node node) {
        Platform.runLater(() -> {
            if (!node.isFocused()) {
                node.requestFocus();
                repeatFocusData(node);
            }
        });
    }

    private void buttonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneClienteKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        repeatFocusData(textFieldCantidad);
        textFieldCantidad.setText(cantidad);
    }
    //INICIAL INICIAL INICIAL **************************************************

    private void navegandoFacturaVenta() {

    }

    private void volviendo() {
        if (StageSecond.getStageData().isShowing()) {
            StageSecond.getStageData().close();
        }
//        this.sc.loadScreenModal("/vista/cajamay/FacturaDeVentaMayFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/cajamay/ModCantidadMayFXML.fxml", 232, 95, false);
    }

    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (!textFieldCantidad.getText().equalsIgnoreCase("")) {
                if (StageSecond.getStageData().isShowing()) {
                    StageSecond.getStageData().close();
                }

                tableFact.getSelectionModel().getSelectedItem().put("cantidad", Integer.parseInt(textFieldCantidad.getText()));

                switch (ScreensContoller.getFxml()) {
                    case "/vista/stock/ConteoFXML.fxml": {
                        JSONParser parser = new JSONParser();
                        int val = 0;
                        ConteoFXMLController.detalleArtList = new ArrayList<>();
                        long iva5 = 0;
                        long iva10 = 0;
                        long iva = 0;
                        for (JSONObject item : tableFact.getItems()) {
//                            if (item.get("iva").toString().equalsIgnoreCase("0")) {
////                        jsonDet.put("iva", "0");
//                                item.put("exenta", (Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString())));
//                            } else {
//                                if (item.get("iva").toString().equalsIgnoreCase("5")) {
////                            jsonDet.put("iva", "5");
//                                    long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
//                                    iva5 = Math.round(costo / 21);
//
//                                    iva = costo - iva5;
//                                    item.put("gravada", iva);
//                                } else {
//                                    long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
//                                    iva10 = Math.round(costo / 11);
//
//                                    iva = costo - iva10;
//                                    item.put("gravada", iva);
//                                }
//                            }
                            int precio = Integer.parseInt(item.get("precio").toString());
                            int canti = Integer.parseInt(item.get("cantidad").toString());
                            int total = precio * canti;
                            item.put("total", total);
//
                            ConteoFXMLController.detalleArtList.add(item);//si
                        }

                        this.sc.loadScreen("/vista/stock/ConteoFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/ModCantidadFXML.fxml", 232, 95, true);
                        break;
                    }
                    case "/vista/stock/PedidoCompraFXML.fxml": {
                        JSONParser parser = new JSONParser();
                        int val = 0;
                        PedidoCompraFXMLController.detalleArtList = new ArrayList<>();
                        long iva5 = 0;
                        long iva10 = 0;
                        long iva = 0;
                        for (JSONObject item : tableFact.getItems()) {
                            if (item.get("iva").toString().equalsIgnoreCase("0")) {
//                        jsonDet.put("iva", "0");
                                item.put("exenta", (Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString())));
                            } else {
                                if (item.get("iva").toString().equalsIgnoreCase("5")) {
//                            jsonDet.put("iva", "5");
                                    long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
                                    iva5 = Math.round(costo / 21);

                                    iva = costo - iva5;
                                    item.put("gravada", iva);
                                } else {
                                    long costo = Long.parseLong(item.get("costo").toString()) * Long.parseLong(item.get("cantidad").toString());
                                    iva10 = Math.round(costo / 11);

                                    iva = costo - iva10;
                                    item.put("gravada", iva);
                                }
                            }
//
                            PedidoCompraFXMLController.detalleArtList.add(item);//si
                        }

                        this.sc.loadScreen("/vista/stock/PedidoCompraFXML.fxml", Utilidades.getWidth(), Utilidades.getHeight(), "/vista/stock/ModCantidadFXML.fxml", 232, 95, true);
                        break;
                    }
                    default: {
                        break;
                    }
                }
            } else {
                mensajeAlerta("CANTIDAD NO DEBE QUEDAR VACIO");
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }

    private void mensajeAlerta(String msj) {
        new Toaster().mensajeGenerico("Mensaje del Sistema", msj, "", 2);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buscandoClienteFiel() {

    }

    private void nuevoClienteFiel() {

    }

    private static boolean isNumeric(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

}
