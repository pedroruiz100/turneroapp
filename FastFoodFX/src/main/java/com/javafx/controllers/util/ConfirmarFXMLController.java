/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.util;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.PATH;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ConfirmarFXMLController extends BaseScreenController implements Initializable {

    private static boolean procesar = false;
    private static boolean msjEliminar;

    @FXML
    private AnchorPane anchorPaneConfirmar;
    @FXML
    private DialogPane dialogPaneConfirmar;
    @FXML
    private HBox hBoxConfirmar;
    @FXML
    private ImageView imageViewConfirmar;
    @FXML
    private Label labelMsjConfirmar;
    @FXML
    private VBox vBoxConfirmar;
    @FXML
    private Label labelMsjPreguntaConfirmar;
    @FXML
    private HBox hBoxBottomConfirmar;
    @FXML
    private Button buttonSiConfirmar;
    @FXML
    private Button buttonNoConfirmar;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buttonSiConfirmarAction(ActionEvent event) {
        presionandoSI();
    }

    @FXML
    private void buttonNoConfirmarAction(ActionEvent event) {
        presionandoNO();
    }

    @FXML
    private void anchorPaneConfirmarKeyReleased(KeyEvent event) {
        confirmando(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        if (msjEliminar) {
            File file = new File(PATH.PATH_ADV);
            Image image = new Image(file.toURI().toString());
            this.imageViewConfirmar.setImage(image);
            labelMsjPreguntaConfirmar.setText("¿Desea eliminar el registro seleccionado?");
        } else {
            File file = new File(PATH.PATH_ADV);
            Image image = new Image(file.toURI().toString());
            this.imageViewConfirmar.setImage(image);
            labelMsjPreguntaConfirmar.setText("¿Desea guardar los procesos realizados?");
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void presionandoSI() {
        procesar = true;
        this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
                ScreensContoller.getHeight(), ScreensContoller.getFxml(),
                ScreensContoller.getWidth(), ScreensContoller.getHeight(), true);
    }

    private void presionandoNO() {
        procesar = false;
        this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
                ScreensContoller.getHeight(), ScreensContoller.getFxml(),
                ScreensContoller.getWidth(), ScreensContoller.getHeight(), false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void confirmando(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            presionandoSI();
        } else if (keyCode == event.getCode().ESCAPE) {
            presionandoNO();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    public static boolean isProcesar() {
        return procesar;
    }

    public static void setProcesar(boolean aEliminar) {
        procesar = aEliminar;
    }

    public static boolean isMsjEliminar() {
        return msjEliminar;
    }

    public static void setMsjEliminar(boolean aMsjEliminar) {
        msjEliminar = aMsjEliminar;
    }
}
