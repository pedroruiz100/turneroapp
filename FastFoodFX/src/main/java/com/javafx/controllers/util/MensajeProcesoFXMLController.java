/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.util;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.screen.ScreensContoller;
import com.javafx.util.MessageGenerator;
import com.javafx.util.PATH;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class MensajeProcesoFXMLController extends BaseScreenController implements Initializable {
    
    @FXML
    private VBox vBox;
    @FXML
    private AnchorPane anchorPaneMP;
    @FXML
    private DialogPane dialogPaneMP;
    @FXML
    private HBox hBoxMP;
    @FXML
    private ImageView imageViewGenericMP;
    @FXML
    private Label labelMensajeGenericMP;
    @FXML
    private Label labelGeneric1MP;
    @FXML
    private Label labelGeneric2MP;
    @FXML
    private Button buttonAceptarMP;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }
    
    @FXML
    private void botonAceptarMPAction(ActionEvent event) {
        navegandoSgteForm();
    }
    
    @FXML
    private void anchorPaneMPKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        File file;
        labelGeneric2MP.setText(PATH.MSG_CONT_GEN);
        if (MessageGenerator.isProceso()) {//confirmación o advertencia, ícono...
            labelMensajeGenericMP.setText(PATH.MSG_CONFIR);
            labelGeneric1MP.setText(PATH.MSG_CONT_CONFI);
            file = new File(PATH.PATH_SI);
        } else {
            labelMensajeGenericMP.setText(PATH.MSG_ADVERT);
            labelGeneric1MP.setText(PATH.MSG_CONT_ADVER);
            file = new File(PATH.PATH_NO);
        }
        Image image = new Image(file.toURI().toString());
        this.imageViewGenericMP.setImage(image);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoSgteForm() {//se mantiene referencia del último formulario, se navega al mismo
        this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
                ScreensContoller.getHeight(), ScreensContoller.getFxml(),
                ScreensContoller.getWidth(), ScreensContoller.getHeight(), true);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER || keyCode == event.getCode().ESCAPE) {
            navegandoSgteForm();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
}
