package com.javafx.controllers.util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author PC
 */
@Controller
//@ScreenScoped
public class GenerarInformeAvisoFXMLController extends BaseScreenController implements Initializable {

    @FXML
    private DialogPane dialogPaneAccesoAviso;
    @FXML
    private Label labelAvisoAccesoAviso;
    @FXML
    private VBox vBoxAccesoAviso;
    @FXML
    private Label labelPermisoAccesoAviso;
    @FXML
    private Label labelPermiso2AccesoAviso;
    @FXML
    private Button buttonAnteriorAccesoAviso;
    @FXML
    private AnchorPane anchorPaneAccesoAviso;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonAnteriorAccesoAvisoAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneAccesoAvisoKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
//        if (ScreensContoller.getFxml().contentEquals("/vista/util/GenerarInformeAvisoFXML.fxml")) {
        labelPermisoAccesoAviso.setText("DEBE GENERAR EL INFORME FINANCIERO");
        labelPermiso2AccesoAviso.setText(" PARA VOLVER A INICIAR SESION COMO CAJERO");
//        } 
//        else {
//            labelPermisoAccesoAviso.setText("No dispone de los");
//            labelPermiso2AccesoAviso.setText("permisos necesarios");
//        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
//        LoginFXMLController.setLlamarTask(false);
        this.sc.loadScreen("/vista/caja/loginCajeroFXML.fxml", 545, 317, "/vista/util/GenerarInformeAvisoFXML.fxml", 269, 123, true);
//        this.sc.loadScreen(ScreensContoller.getFxml(), ScreensContoller.getWidth(),
//                ScreensContoller.getHeight(), ScreensContoller.getFxml(),
//                ScreensContoller.getWidth(), ScreensContoller.getHeight(), false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER || keyCode == event.getCode().ESCAPE) {
            volviendo();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

}
