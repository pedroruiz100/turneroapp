/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.util;

import com.google.gson.Gson;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.ConexionPostgres;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import com.peluqueria.dao.UsuarioDAO;
import com.google.gson.GsonBuilder;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class ModificarPassFXMLController extends BaseScreenController implements Initializable {

    private boolean exito;
    private boolean alert;
    public static String modulo;

    public static void setModulo(String modulo) {
        ModificarPassFXMLController.modulo = modulo;
    }

    @Autowired
    UsuarioDAO usuDAO;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    @FXML
    private AnchorPane anchorPaneMP;
    @FXML
    private VBox vBoxLabelMP;
    @FXML
    private Label labelPassActualMP;
    @FXML
    private Label labelPassNuevaMP;
    @FXML
    private Label labelPassConfirMP;
    @FXML
    private Label fl1;
    @FXML
    private Label fl2;
    @FXML
    private Label fl3;
    @FXML
    private VBox vBoxTextFieldMP;
    @FXML
    private PasswordField passFieldPassActualMP;
    @FXML
    private PasswordField passFielPassNuevaMP;
    @FXML
    private PasswordField passFieldPassConfirmarMP;
    @FXML
    private Button buttonActualizarMP;
    @FXML
    private Button buttonVolverMP;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonActualizarMPAction(ActionEvent event) {
        cambiandoPass();
    }

    @FXML
    private void botonVolverMPAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneMPKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        exito = false;
        alert = false;
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (modulo == null) {
            this.sc.loadScreen("/vista/login/LoginFXML.fxml", 599, 245, "/vista/util/ModificarPassFXML.fxml", 377, 152, false);
        } else if (modulo.equalsIgnoreCase("caja")) {
            this.sc.loadScreen("/vista/caja/facturaVentaFXML.fxml", 1248, 743, "/vista/util/ModificarPassFXML.fxml", 377, 152, false);
        } else if (modulo.equalsIgnoreCase("estetica")) {
            this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, "/vista/util/ModificarPassFXML.fxml", 377, 152, false);
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void cambiandoPass() {
        String mc = todoCorrecto(passFieldPassActualMP.getText(), passFielPassNuevaMP.getText(),
                passFieldPassConfirmarMP.getText());
        if (mc.contentEquals("PassExito")) {
            backResponse();
            if (exito) {
                Alert alert2 = new Alert(Alert.AlertType.INFORMATION, "¡CONTRASEÑA ACTUALIZADA!", ButtonType.OK);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.OK) {
                    alert2.close();
                } else {
                    alert2.close();
                }
                volviendo();
            } else {
                Alert alert2 = new Alert(Alert.AlertType.ERROR, "LA CONTRASEÑA ACTUAL ESPECIFICADA NO ES VÁLIDA.", ButtonType.OK);
                this.alert = true;
                alert2.showAndWait();
                if (alert2.getResult() == ButtonType.OK) {
                    alert2.close();
                }
            }
        } else if (mc.contentEquals("passAnteriorEmpty")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CAMPO CONTRASEÑA ACTUAL ES OBLIGATORIO.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("passNuevoEmpty")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CAMPO NUEVA CONTRASEÑA ES OBLIGATORIO.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("ConfirmPassEmpty")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "EL CAMPO CONFIRMAR CONTRASEÑA ES OBLIGATORIO.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("passViejoNO")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LAS CONTRASEÑAS INDICADAS NO COINCIDEN.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("passNuevoNO")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LAS CONTRASEÑAS INDICADAS NO COINCIDEN.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("passError")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LA CONTRASEÑA ACTUAL ESPECIFICADA NO ES VÁLIDA.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("numberEmpty")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LA NUEVA CONTRASEÑA DEBE DISPONER DE AL MENOS UN NÚMERO.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("letterEmpty")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LA NUEVA CONTRASEÑA DEBE DISPONER DE AL MENOS UNA LETRA.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        } else if (mc.contentEquals("longitud")) {
            Alert alert2 = new Alert(Alert.AlertType.ERROR, "LA NUEVA CONTRASEÑA DEBE DISPONER DE AL MENOS 6 CARACTERES.", ButtonType.OK);
            this.alert = true;
            alert2.showAndWait();
            if (alert2.getResult() == ButtonType.OK) {
                alert2.close();
            }
        }
    }

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                cambiandoPass();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                volviendo();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private String todoCorrecto(String passAnterior, String passNuevo, String confirmPass) {
        //"actual" vacío...
        if (passAnterior.isEmpty()) {
            return "passAnteriorEmpty";
        }
        //"nuevo" vacío...
        if (passNuevo.isEmpty()) {
            return "passNuevoEmpty";
        }
        //"nuevo" confirmación vacío...
        if (confirmPass.isEmpty()) {
            return "ConfirmPassEmpty";
        }
        //longitud mínima requerida...
        if (confirmPass.length() < 6) {
            return "longitud";
        }
        boolean existeNro = false;
        boolean existeLet = false;
        for (int i = 0; i < confirmPass.length(); i++) {
            if (!existeNro) {
                if (confirmPass.substring(i, i + 1).matches("[0-9]")) {
                    existeNro = true;
                }
            }
            if (!existeLet) {
                if (confirmPass.substring(i, i + 1).matches("[a-zA-Z]")) {
                    existeLet = true;
                }
            }
            if (existeLet && existeNro) {
                break;
            }
        }
        if (!existeLet) {
            return "letterEmpty";
        }
        if (!existeNro) {
            return "numberEmpty";
        }
        CryptoFront cf = new CryptoFront();
        CryptoBack cb = new CryptoBack();
        //contraseña "actual" no coincide...
        if (!cb.getHash(cf.getHash(passAnterior.trim())).equals(Identity.getUsuario().get("contrasenha").toString())) {
            return "passViejoNO";
        }
        //"nuevo" no coincide con -> "nuevo" confirmación
        if (!passNuevo.trim().equals(confirmPass.trim())) {
            return "passNuevoNO";
        }
        return "PassExito";
    }

    private boolean localPass() {
        if (localResponse()) {
            //actualizando contraseña en el lado cliente...
            CryptoFront cf = new CryptoFront();
            CryptoBack cb = new CryptoBack();
            Identity.getUsuario().put("contrasenha", cb.getHash(cf.getHash(passFielPassNuevaMP.getText())));
            return true;
        } else {
            return false;
        }
    }

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //UPDATE, USUARIO
    public boolean localResponse() {
        try {
            CryptoFront cf = new CryptoFront();
            CryptoBack cb = new CryptoBack();
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            //manejo local
            boolean exito = usuDAO.actualizarContrasenha(cb.getHash(cf.getHash(passFielPassNuevaMP.getText())), Long.valueOf(Identity.getUsuario().get("idUsuario").toString()),
                    Identity.getNomFun(), tsNow);
            if (exito) {
                //manejo servidor - msg broker, no debe enviar al nodo inicio
                actualizarPendientes(jsonUpdatePass(Long.valueOf(Identity.getUsuario().get("idUsuario").toString()),
                        Identity.getNomFun(), tsNow, cb.getHash(cf.getHash(passFielPassNuevaMP.getText()))));
            }
            return exito;
        } catch (Exception ex) {
            Utilidades.log.error("ERROR Exception localContras: ", ex.fillInStackTrace());
            return false;
        }
    }
    //UPDATE, USUARIO

    //////UPDATE, PENDIENTES - USUARIO
    private boolean actualizarPendientes(JSONObject usuario) {
        ConexionPostgres.conectar();
        boolean valor = false;
        String sql = "INSERT INTO pendiente.usuario_pendientes (ip, dml, msj, fecha_registro, tabla) VALUES ('" + Utilidades.ipLocal + "', 'U', '" + usuario.toString() + "', now(), 'usuario')";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            int op = ps.executeUpdate();
            if (op >= 1) {
                valor = true;
                System.out.println(" --------- >> DATOS PERSISTIDOS EN HANDLER PARANA  << --------- ");
            }
            ps.close();
            ConexionPostgres.getCon().commit();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
            try {
                ConexionPostgres.getCon().rollback();
            } catch (SQLException ex1) {
                Utilidades.log.error("ERROR SQLException: ", ex1.fillInStackTrace());
            }
        }
        ConexionPostgres.cerrar();
        return valor;
    }
    //////UPDATE, PENDIENTES - USUARIO
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////UPDATE -> PUT
    private boolean backResponse() {
        String inputLine;
        JSONParser parser = new JSONParser();
        CryptoFront cf = new CryptoFront();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/usuario/cambiarClave/"
                        + cf.getHash(passFielPassNuevaMP.getText()) + "/"
                        + UtilLoaderBase.msjIda(Identity.getUsuario().get("idUsuario").toString()) + "/"
                        + UtilLoaderBase.msjIda(Identity.getNomFun()));
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("PUT");
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((inputLine = br.readLine()) != null) {
                    exito = (boolean) parser.parse(inputLine);
                }
                if (exito) {
                    //actualizando contraseña en el lado cliente...
                    CryptoBack cb = new CryptoBack();
                    Identity.getUsuario().put("contrasenha", cb.getHash(cf.getHash(passFielPassNuevaMP.getText())));
                } else {
                    exito = localPass();
                }
                br.close();
            } else {
                exito = localPass();
            }
        } catch (FileNotFoundException ex) {
            exito = localPass();
            Utilidades.log.error("ERROR FileNotFoundException: ", ex.fillInStackTrace());
        } catch (IOException | ParseException ex) {
            exito = localPass();
            Utilidades.log.error("ERROR IOException/ParseException: ", ex.fillInStackTrace());
        }
        return exito;
    }
    //////UPDATE -> PUT
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON UPDATE USUARIO
    private JSONObject jsonUpdatePass(long idUsu, String nomFun, Timestamp tsNow, String pass) {
        JSONObject jsonUser = new JSONObject();
        jsonUser.put("idUsuario", idUsu);
        jsonUser.put("contrasenha", pass);
        jsonUser.put("usuMod", nomFun);
        jsonUser.put("fechaMod", tsNow.toString());
        jsonUser.put("generico", false);
        jsonUser.put("resetPass", true);
        return jsonUser;
    }
    //JSON UPDATE USUARIO
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

}
