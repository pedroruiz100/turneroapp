/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.util;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import org.springframework.stereotype.Controller;

/**
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class FXMLCalculadoraController extends BaseScreenController implements Initializable {

    Float data = 0f;
    int operation = -1;

    public static String modulo;

    public static void setModulo(String modulo) {
        FXMLCalculadoraController.modulo = modulo;
    }

    @FXML
    private Button minus;
    @FXML
    private Button nine;
    @FXML
    private Button six;
    @FXML
    private Button mult;
    @FXML
    private Button one;
    @FXML
    private TextField display;
    @FXML
    private Button clear;
    @FXML
    private Button seven;
    @FXML
    private Label label;
    @FXML
    private Button two;
    @FXML
    private Button three;
    @FXML
    private Button plus;
    @FXML
    private Button eight;
    @FXML
    private Button zero;
    @FXML
    private Button div;
    @FXML
    private Button four;
    @FXML
    private Button equals;
    @FXML
    private Button five;
    @FXML
    private Button buttonVolver;
    @FXML
    private AnchorPane anchorPaneCalc;

    @FXML
    private void botonVolverAction(ActionEvent event) {
        volviendo();
    }

    @FXML
    private void anchorPaneCalcKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    void handleButtonAction(ActionEvent event) {
        calculando(event);
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void volviendo() {
        if (modulo.equalsIgnoreCase("caja")) {
            this.sc.loadScreen("/vista/caja/facturaVentaFXML.fxml", 1248, 743, "/vista/util/FXMLCalculadora.fxml", 419, 452, false);
        } else if (modulo.equalsIgnoreCase("estetica")) {
            this.sc.loadScreen("/vista/estetica/FacturaVentaEsteticaFXML.fxml", 1248, 771, "/vista/util/FXMLCalculadora.fxml", 419, 452, false);
        }
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            volviendo();
        } else if (keyCode == event.getCode().DIGIT1 || keyCode == event.getCode().NUMPAD1) {
            display.setText(display.getText() + "1");
        } else if (keyCode == event.getCode().DIGIT2 || keyCode == event.getCode().NUMPAD2) {
            display.setText(display.getText() + "2");
        } else if (keyCode == event.getCode().DIGIT3 || keyCode == event.getCode().NUMPAD3) {
            display.setText(display.getText() + "3");
        } else if (keyCode == event.getCode().DIGIT4 || keyCode == event.getCode().NUMPAD4) {
            display.setText(display.getText() + "4");
        } else if (keyCode == event.getCode().DIGIT5 || keyCode == event.getCode().NUMPAD5) {
            display.setText(display.getText() + "5");
        } else if (keyCode == event.getCode().DIGIT6 || keyCode == event.getCode().NUMPAD6) {
            display.setText(display.getText() + "6");
        } else if (keyCode == event.getCode().DIGIT7 || keyCode == event.getCode().NUMPAD7) {
            display.setText(display.getText() + "7");
        } else if (keyCode == event.getCode().DIGIT8 || keyCode == event.getCode().NUMPAD8) {
            display.setText(display.getText() + "8");
        } else if (keyCode == event.getCode().DIGIT9 || keyCode == event.getCode().NUMPAD9) {
            display.setText(display.getText() + "9");
        } else if (keyCode == event.getCode().DIGIT0 || keyCode == event.getCode().NUMPAD0) {
            display.setText(display.getText() + "0");
        } else if (keyCode == event.getCode().DELETE) {
            display.setText("");
        } else if (keyCode == event.getCode().ADD) {
            data = Float.parseFloat(display.getText());
            operation = 1; //Addition
            display.setText("");
        } else if (keyCode == event.getCode().SUBTRACT) {
            data = Float.parseFloat(display.getText());
            operation = 2; //Substraction
            display.setText("");
        } else if (keyCode == event.getCode().MULTIPLY) {
            data = Float.parseFloat(display.getText());
            operation = 3; //Mul
            display.setText("");
        } else if (keyCode == event.getCode().DIVIDE) {
            data = Float.parseFloat(display.getText());
            operation = 4; //Division
            display.setText("");
        } else if (keyCode == event.getCode().ENTER) {
            Float secondOperand = Float.parseFloat(display.getText());
            switch (operation) {
                case 1: //Addition
                    Float ans = data + secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 2: //Subtraction
                    ans = data - secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 3: //Mul
                    ans = data * secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 4: //Div
                    ans = 0f;
                    try {
                        ans = data / secondOperand;
                    } catch (Exception e) {
                        display.setText("Error");
                    }
                    display.setText(String.valueOf(ans));
                    break;
            }
        }
    }

    private void calculando(ActionEvent event) {
        if (event.getSource() == one) {
            display.setText(display.getText() + "1");
        } else if (event.getSource() == two) {
            display.setText(display.getText() + "2");
        } else if (event.getSource() == three) {
            display.setText(display.getText() + "3");
        } else if (event.getSource() == four) {
            display.setText(display.getText() + "4");
        } else if (event.getSource() == five) {
            display.setText(display.getText() + "5");
        } else if (event.getSource() == six) {
            display.setText(display.getText() + "6");
        } else if (event.getSource() == seven) {
            display.setText(display.getText() + "7");
        } else if (event.getSource() == eight) {
            display.setText(display.getText() + "8");
        } else if (event.getSource() == nine) {
            display.setText(display.getText() + "9");
        } else if (event.getSource() == zero) {
            display.setText(display.getText() + "0");
        } else if (event.getSource() == clear) {
            display.setText("");
        } else if (event.getSource() == plus) {
            data = Float.parseFloat(display.getText());
            operation = 1; //Addition
            display.setText("");
        } else if (event.getSource() == minus) {
            data = Float.parseFloat(display.getText());
            operation = 2; //Substraction
            display.setText("");
        } else if (event.getSource() == mult) {
            data = Float.parseFloat(display.getText());
            operation = 3; //Mul
            display.setText("");
        } else if (event.getSource() == div) {
            data = Float.parseFloat(display.getText());
            operation = 4; //Division
            display.setText("");
        } else if (event.getSource() == equals) {
            Float secondOperand = Float.parseFloat(display.getText());
            switch (operation) {
                case 1: //Addition
                    Float ans = data + secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 2: //Subtraction
                    ans = data - secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 3: //Mul
                    ans = data * secondOperand;
                    display.setText(String.valueOf(ans));
                    break;
                case 4: //Divi
                    ans = 0f;
                    try {
                        ans = data / secondOperand;
                    } catch (Exception e) {
                        display.setText("Error");
                    }
                    display.setText(String.valueOf(ans));
                    break;
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
}
