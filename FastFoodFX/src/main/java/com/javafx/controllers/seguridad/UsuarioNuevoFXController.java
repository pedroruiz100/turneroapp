/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.Identity;
import com.javafx.util.PasswordGenerator;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Funcionario;
import com.peluqueria.core.domain.Rol;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.core.domain.UsuarioRol;
import com.peluqueria.dao.FuncionarioDAO;
import com.peluqueria.dao.RolDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.UsuarioDAO;
import com.peluqueria.dao.UsuarioRolDAO;
import com.peluqueria.dto.SupervisorDTO;
import com.peluqueria.dto.UsuarioDTO;
import com.peluqueria.dto.UsuarioRolDTO;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class UsuarioNuevoFXController extends BaseScreenController implements Initializable {

    private boolean alert;
    HashMap<String, JSONObject> hashMapFunc;
    HashMap<String, JSONObject> hashMapRol;
    private ObservableList<String> itemsFunc;
    private ObservableList<String> itemsAsignados;
    private ObservableList<String> itemsNOAsignados;
    private String msjError;

    @Autowired
    FuncionarioDAO funcionarioDAO;
    @Autowired
    RolDAO rolDAO;
    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    UsuarioRolDAO usuarioRolDAO;
    @Autowired
    SupervisorDAO supervisorDAO;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneUsuarioNuevo;
    @FXML
    private SplitPane splitPaneUsuarioNuevo;
    @FXML
    private AnchorPane anchorPaneHeaderUsuarioNuevo;
    @FXML
    private Label labelNombreUsuarioNuevo;
    @FXML
    private Label labelEmailUsuarioNuevo;
    @FXML
    private Label AstNombreUsuarioNuevo;
    @FXML
    private Label AstEmailUsuarioNuevo;
    @FXML
    private TextField textFieldUsuarioNuevo;
    @FXML
    private TextField textFieldEmailUsuarioNuevo;
    @FXML
    private Label labelNuevoUsuario;
    @FXML
    private AnchorPane anchorPaneListViewUsuarioNuevo;
    @FXML
    private ListView<String> listViewSinAsignarUsuarioNuevo;
    @FXML
    private ListView<String> listViewAsignadoUsuarioNuevo;
    @FXML
    private Button buttonAsignarUsuarioNuevo;
    @FXML
    private Button buttonAsignarTodosUsuarioNuevo;
    @FXML
    private Button buttonQuitarUsuarioNuevo;
    @FXML
    private Button buttonQuitarTodosUsuarioNuevo;
    @FXML
    private CheckBox checkActivoUsuarioNuevo;
    @FXML
    private Label labelRolSinAsignarUsuarioNuevo;
    @FXML
    private Label labelRolAsignadoUsuarioNuevo;
    @FXML
    private AnchorPane anchorPaneFooterUsuarioNuevo;
    @FXML
    private Button buttonGuardarUsuarioNuevo;
    @FXML
    private Button buttonVolverUsuarioNuevo;
    @FXML
    private AnchorPane anchorPaneFuncionario;
    @FXML
    private ListView<String> listViewFuncionarioDisp;
    @FXML
    private Label labelFuncionarioDisp;
    @FXML
    private TextField textFieldFuncionarioSelecc;
    @FXML
    private Label labelFuncionarioSelecc;
    @FXML
    private Button buttonRemoverFunc;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;
    @FXML
    private Label AstNombreUsuarioNuevo1;
    @FXML
    private AnchorPane anchorPaneCodSupervisor;
    @FXML
    private TextField textFieldCodSupervisor;
    @FXML
    private CheckBox checkBoxCodSuper;
    @FXML
    private Label labelFiltroBusq;
    @FXML
    private TextField textFieldFiltroCI;
    @FXML
    private Button btnAgregarFuncionario;
    @FXML
    private AnchorPane panelFuncionario;
    @FXML
    private Button buttonGuardar;
    @FXML
    private Button buttonSalir;
    @FXML
    private Label labelRucCiCliente1;
    @FXML
    private TextField txtCiFuncionario;
    @FXML
    private TextField txtNombreFuncionario;
    @FXML
    private TextField txtApellidoFuncionario;
    @FXML
    private CheckBox chkActivoFuncionario;
    @FXML
    private CheckBox chkHabilitadoFuncionario;
    @FXML
    private Button btnProcesarFuncionario;
    @FXML
    private Button btnVolverFuncionario;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial(true);
    }

    @FXML
    private void botonAsignarUsuarioNuevoAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void botonAsignarTodosUsuarioNuevoAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void botonQuitarUsuarioNuevoAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void botonQuitarTodosUsuarioNuevoAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void checkBoxActivoUsuarioNuevoAction(ActionEvent event) {
        checkBoxActivo();
    }

    @FXML
    private void botonGuardarUsuarioNuevoAction(ActionEvent event) {
        guardandoUsuario();
    }

    @FXML
    private void botonVolverUsuarioNuevoAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void buttonRemoverFuncAction(ActionEvent event) {
        removiendoFuncionario();
    }

    @FXML
    private void checkBoxCodSuperAction(ActionEvent event) {
        checkBoxSupervisor();
    }

    @FXML
    private void textFieldFiltroCIKeyReleased(KeyEvent event) {
        jsonArrayFuncionariosDisponibles(textFieldFiltroCI.getText());
    }

    @FXML
    private void anchorPaneUsuarioNuevoKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial(boolean inicial) {
        if (inicial) {
            alert = false;
            listViewFuncionarioDisp.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
            listViewSinAsignarUsuarioNuevo.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            listViewAsignadoUsuarioNuevo.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            escuchaFunc();
            textFieldCodSupervisor.textProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue.contentEquals("")) {
                    try {
                        if (GenericValidator.isLong(newValue)) {
                            Platform.runLater(() -> {
                                textFieldCodSupervisor.setText(newValue.toString());
                                textFieldCodSupervisor.positionCaret(textFieldCodSupervisor.getLength());
                            });
                        } else {
                            Platform.runLater(() -> {
                                textFieldCodSupervisor.setText(oldValue.toString());
                                textFieldCodSupervisor.positionCaret(textFieldCodSupervisor.getLength());
                            });
                        }
                    } catch (NumberFormatException e) {
                        Platform.runLater(() -> {
                            textFieldCodSupervisor.setText(oldValue.toString());
                            textFieldCodSupervisor.positionCaret(textFieldCodSupervisor.getLength());
                        });
                    }
                }
            });
        } else {
            textFieldFiltroCI.setText("");
            removiendoFuncionario();
            checkBoxCodSuper.setSelected(false);
            textFieldCodSupervisor.setText("");
            textFieldCodSupervisor.setDisable(true);
            textFieldUsuarioNuevo.setText("");
            textFieldEmailUsuarioNuevo.setText("");
            checkActivoUsuarioNuevo.setSelected(false);
            checkBoxCodSuper.setSelected(false);
        }
        msjError = "";
        jsonArrayFuncionariosDisponibles("");
        jsonArrayRolesDisponibles();
        checkBoxActivo();
        checkBoxSupervisor();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/UsuarioFX.fxml", 781, 615, "/vista/seguridad/UsuarioNuevoFX.fxml", 720, 738, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeExito(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                removiendoFuncionario();
            }
        }
        if (keyCode == event.getCode().F2) {
            //            if (alert) {
            //                alert = false;
            //            } else {
            //                removiendoFuncionario();
            //            }
            if (panelFuncionario.isVisible()) {
                procesarFuncionario();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else if (panelFuncionario.isVisible()) {
                deshabilitarFuncionario();
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else if (panelFuncionario.isVisible()) {
                if (txtCiFuncionario.isFocused()) {
                    buscarFuncionario();
                }
            } else {
                guardandoUsuario();
            }
        }
    }

    private void checkBoxActivo() {
        if (checkActivoUsuarioNuevo.isSelected()) {
            checkActivoUsuarioNuevo.setText("Activo");
            checkActivoUsuarioNuevo.setEffect(new DropShadow(10, Color.GREEN));
            checkActivoUsuarioNuevo.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            checkActivoUsuarioNuevo.setText("Inactivo");
            checkActivoUsuarioNuevo.setEffect(new DropShadow(10, Color.RED));
            checkActivoUsuarioNuevo.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }

    private void checkBoxSupervisor() {
        textFieldCodSupervisor.setText("");
        if (checkBoxCodSuper.isSelected()) {
            textFieldCodSupervisor.setDisable(false);
            checkBoxCodSuper.setTextFill(Paint.valueOf("#3ba40e"));
            checkBoxCodSuper.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            textFieldCodSupervisor.setDisable(true);
            checkBoxCodSuper.setTextFill(Paint.valueOf("black"));
            checkBoxCodSuper.setEffect(null);
        }
    }

    private void escuchaFunc() {
        listViewFuncionarioDisp.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (listViewFuncionarioDisp.getSelectionModel().getSelectedItem() != null) {
                textFieldFuncionarioSelecc.setText(listViewFuncionarioDisp.getSelectionModel().getSelectedItem());
            } else {
                textFieldFuncionarioSelecc.setText("");
            }
        });
    }

    private void asignando() {
        itemsAsignados.addAll(listViewSinAsignarUsuarioNuevo.getSelectionModel().getSelectedItems());
        itemsNOAsignados.removeAll(listViewSinAsignarUsuarioNuevo.getSelectionModel().getSelectedItems());
        listViewAsignadoUsuarioNuevo.setItems(itemsAsignados);
        listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
    }

    private void asignandoTodos() {
        itemsAsignados.addAll(itemsNOAsignados);
        itemsNOAsignados.removeAll(itemsNOAsignados);
        listViewAsignadoUsuarioNuevo.setItems(itemsAsignados);
        listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
    }

    private void quitando() {
        itemsNOAsignados.addAll(listViewAsignadoUsuarioNuevo.getSelectionModel().getSelectedItems());
        itemsAsignados.removeAll(listViewAsignadoUsuarioNuevo.getSelectionModel().getSelectedItems());
        listViewAsignadoUsuarioNuevo.setItems(itemsAsignados);
        listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
    }

    private void quitandoTodos() {
        itemsNOAsignados.addAll(itemsAsignados);
        itemsAsignados.removeAll(itemsAsignados);
        listViewAsignadoUsuarioNuevo.setItems(itemsAsignados);
        listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void removiendoFuncionario() {
        textFieldFuncionarioSelecc.setText("");
        jsonArrayFuncionariosDisponibles(textFieldFiltroCI.getText());
    }

    private void guardandoUsuario() {
        if (!textFieldUsuarioNuevo.getText().contentEquals("")) {
            if (!textFieldEmailUsuarioNuevo.getText().contentEquals("")) {
                if (EmailValidator.getInstance().isValid(textFieldEmailUsuarioNuevo.getText())) {
                    if (!textFieldFuncionarioSelecc.getText().contentEquals("")) {
                        if (checkBoxCodSuper.isSelected()) {
                            if (textFieldCodSupervisor.getText().contentEquals("")) {
                                mensajeError("EL CÓDIGO DEL SUPERVISOR NO DEBE ESTAR EN NULO.");
                            } else {
                                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                                ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CREAR USUARIO?", ok, cancel);
                                this.alert = true;
                                alert.showAndWait();
                                if (alert.getResult() == ok) {
                                    JSONObject jsonUsuario = creandoUsuario();
                                    if (jsonUsuario.isEmpty()) {
                                        alert.close();
                                        mensajeError(msjError);
                                    } else {
                                        alert.close();
                                        mensajeExito("\t\t¡ÉXITO! NUEVO USUARIO REGISTRADO.\n\t\t\t\t\t-> "
                                                + jsonUsuario.get("nomUsuario").toString().toUpperCase() + " <-\n\t\t\t\t\t-> "
                                                + UtilLoaderBase.msjVuelta(jsonUsuario.get("contrasenha").toString()) + " <-\n"
                                                + "NO OLVIDES ANOTAR, Y LUEGO CAMBIAR TU CONTRASEÑA.");
                                    }
                                    cargandoInicial(false);
                                } else {
                                    alert.close();
                                }
                            }
                        } else {
                            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CREAR USUARIO?", ok, cancel);
                            this.alert = true;
                            alert.showAndWait();
                            if (alert.getResult() == ok) {
                                JSONObject jsonUsuario = creandoUsuario();
                                if (jsonUsuario.isEmpty()) {
                                    alert.close();
                                    mensajeError(msjError);
                                } else {
                                    alert.close();
                                    mensajeExito("\t\t¡ÉXITO! NUEVO USUARIO REGISTRADO.\n\t\t\t\t\t-> "
                                            + jsonUsuario.get("nomUsuario").toString().toUpperCase() + " <-\n\t\t\t\t\t-> "
                                            + UtilLoaderBase.msjVuelta(jsonUsuario.get("contrasenha").toString()) + " <-\n"
                                            + "NO OLVIDES ANOTAR, Y LUEGO CAMBIAR TU CONTRASEÑA.");
                                }
                                cargandoInicial(false);
                            } else {
                                alert.close();
                            }
                        }
                    } else {
                        mensajeError("NO OLVIDES SELECCIONAR UN FUNCIONARIO PARA EL USUARIO.");
                    }
                } else {
                    mensajeError("EL E-MAIL ASIGNADO NO ES VÁLIDO, VERIFIQUE.");
                }
            } else {
                mensajeError("NO OLVIDES ASIGNAR UN E-MAIL AL USUARIO.");
            }
        } else {
            mensajeError("NO OLVIDES ASIGNAR UN NOMBRE AL USUARIO.");
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, FUNCIONARIOS DISPONIBLES -> GET
    private void jsonArrayFuncionariosDisponibles(String filtro) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioDisp = new JSONArray();
        hashMapFunc = new HashMap<>();
        itemsFunc = FXCollections.observableArrayList(new ArrayList<>());
        listViewFuncionarioDisp.setItems(itemsFunc);
        if (filtro.isEmpty()) {
            filtro = "null";
        }
        filtro = UtilLoaderBase.msjIda(filtro);
        try {
//            List<FuncionarioDTO> listFunDTO = new ArrayList<FuncionarioDTO>();
            filtro = Utilidades.msjVuelta(filtro);

            List<String> listFuncDisp = new ArrayList<>();
            for (Funcionario funcionario : funcionarioDAO.listarFuncionariosDisponibles(filtro)) {
//                FuncionarioDTO funDTO = funcionario.toFuncionarioDTO();
//                listFunDTO.add(funDTO);
                JSONObject jsonFuncionario = (JSONObject) parser.parse(gson.toJson(funcionario.toFuncionarioDTO()));
//                JSONObject jsonFuncionario = (JSONObject) objFuncionario;
                String datosFunc = jsonFuncionario.get("apellido").toString() + ", "
                        + jsonFuncionario.get("nombre").toString() + "; C.I: "
                        + jsonFuncionario.get("ci").toString();
                listFuncDisp.add(datosFunc);
                jsonArrayFuncionarioDisp.add(jsonFuncionario);
                hashMapFunc.put(datosFunc, jsonFuncionario);
            }

//            List<String> listFuncDisp = new ArrayList<>();
//            jsonArrayFuncionarioDisp = (JSONArray) parser.parse(funcionarioDAO.actualizar(obj));
//            for (Object objFuncionario : jsonArrayFuncionarioDisp) {
//                JSONObject jsonFuncionario = (JSONObject) objFuncionario;
//                String datosFunc = jsonFuncionario.get("apellido").toString() + ", "
//                        + jsonFuncionario.get("nombre").toString() + "; C.I: "
//                        + jsonFuncionario.get("ci").toString();
//                listFuncDisp.add(datosFunc);
//                hashMapFunc.put(datosFunc, jsonFuncionario);
//            }
            itemsFunc = FXCollections.observableArrayList(listFuncDisp);
            listViewFuncionarioDisp.setItems(itemsFunc);
//            URL url = new URL(Utilidades.ip + "/ServerParana/funcionario/fhab/" + filtro);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                List<String> listFuncDisp = new ArrayList<>();
//                jsonArrayFuncionarioDisp = (JSONArray) parser.parse(inputLine);
//                for (Object objFuncionario : jsonArrayFuncionarioDisp) {
//                    JSONObject jsonFuncionario = (JSONObject) objFuncionario;
//                    String datosFunc = jsonFuncionario.get("apellido").toString() + ", "
//                            + jsonFuncionario.get("nombre").toString() + "; C.I: "
//                            + jsonFuncionario.get("ci").toString();
//                    listFuncDisp.add(datosFunc);
//                    hashMapFunc.put(datosFunc, jsonFuncionario);
//                }
//                itemsFunc = FXCollections.observableArrayList(listFuncDisp);
//                listViewFuncionarioDisp.setItems(itemsFunc);
//            }
//            br.close();
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception -> " + e.getMessage());
            Utilidades.log.error("ERROR Exception -> " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
        }
    }
    //////READ, FUNCIONARIOS DISPONIBLES -> GET

    //////READ, ROLES -> GET
    private void jsonArrayRolesDisponibles() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayRolDisp = new JSONArray();
        hashMapRol = new HashMap<>();
        itemsNOAsignados = FXCollections.observableArrayList(new ArrayList<>());
        listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
        itemsAsignados = FXCollections.observableArrayList(new ArrayList<>());
        listViewAsignadoUsuarioNuevo.setItems(itemsAsignados);
        try {
            List<String> listRol = new ArrayList<>();
            for (Rol rol : rolDAO.listar()) {
                rol.setFechaAlta(null);
                rol.setFechaMod(null);
                JSONObject jsonRol = (JSONObject) parser.parse(gson.toJson(rol.toBDRolDTO()));
//                JSONObject jsonRol = (JSONObject) objRol;
                String rolDesc = jsonRol.get("descripcion").toString();
                listRol.add(rolDesc);
                hashMapRol.put(rolDesc, jsonRol);
            }
//            jsonArrayRolDisp = (JSONArray) parser.parse(inputLine);
//            for (Object objRol : jsonArrayRolDisp) {
//                JSONObject jsonRol = (JSONObject) objRol;
//                String rolDesc = jsonRol.get("descripcion").toString();
//                listRol.add(rolDesc);
//                hashMapRol.put(rolDesc, jsonRol);
//            }
            itemsNOAsignados = FXCollections.observableArrayList(listRol);
            listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                List<String> listRol = new ArrayList<>();
//                jsonArrayRolDisp = (JSONArray) parser.parse(inputLine);
//                for (Object objRol : jsonArrayRolDisp) {
//                    JSONObject jsonRol = (JSONObject) objRol;
//                    String rolDesc = jsonRol.get("descripcion").toString();
//                    listRol.add(rolDesc);
//                    hashMapRol.put(rolDesc, jsonRol);
//                }
//                itemsNOAsignados = FXCollections.observableArrayList(listRol);
//                listViewSinAsignarUsuarioNuevo.setItems(itemsNOAsignados);
//            }
//            br.close();
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception -> " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR Exception -> " + e.getMessage());
            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
        }
    }
    //////READ, ROLES -> GET

    //////CREATE, USUARIO -> POST
    private JSONObject creandoUsuario() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonUsuario = new JSONObject();
        try {
            jsonUsuario = creandoJsonUsuario();
            UsuarioDTO usuarioDTO = gson.fromJson(jsonUsuario.toString(), UsuarioDTO.class);
            UsuarioDTO val = insertarObtenerObj(usuarioDTO);
            if (val.getIdUsuario() == null) {
                msjError = "FALLO AL INTENTAR CREAR USUARIO\n-> VERIFIQUE, QUE LOS CAMPOS OBLIGATORIOS SEAN ÚNICOS O QUE EL CODIGO TARJETA SUPERVISOR NO ESTE EN USO.";
                jsonUsuario = new JSONObject();
            } else {
                jsonUsuario.put("contrasenha", val.getContrasenha());
            }
//                        msjError = "FALLO AL INTENTAR CREAR USUARIO\n-> VERIFIQUE, QUE LOS CAMPOS OBLIGATORIOS SEAN ÚNICOS O QUE EL CODIGO TARJETA SUPERVISOR NO ESTE EN USO.";
//                        jsonUsuario = new JSONObject();
//                    }
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("POST");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonUsuario.toString());
//            System.out.println("ESTO ENVIA -->> " + jsonUsuario.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    jsonUsuario = (JSONObject) parser.parse(inputLine);
//                    if (jsonUsuario.get("idUsuario") == null) {
//                        msjError = "FALLO AL INTENTAR CREAR USUARIO\n-> VERIFIQUE, QUE LOS CAMPOS OBLIGATORIOS SEAN ÚNICOS O QUE EL CODIGO TARJETA SUPERVISOR NO ESTE EN USO.";
//                        jsonUsuario = new JSONObject();
//                    }
//                }
//                br.close();
//            } else {
//                msjError = "FALLO AL INTENTAR CREAR USUARIO\n-> PROBLEMAS CON EL SERVIDOR (" + HttpResult + ").";
//                jsonUsuario = new JSONObject();
//            }
//        } catch (IOException e) {
//            jsonUsuario = new JSONObject();
//            msjError = "FALLO AL INTENTAR CREAR USUARIO\n-> VERIFIQUE, CONEXIÓN.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//            System.out.println(e.getLocalizedMessage());
        } catch (Exception ex) {
            jsonUsuario = new JSONObject();
            msjError = "NO SE PUDO CREAR USUARIO\nINTENTELO MÁS TARDE.";
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        return jsonUsuario;
    }
    //////CREATE, USUARIO -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO USUARIO
    private JSONObject creandoJsonUsuario() {
        JSONObject jsonUsuario = new JSONObject();
        //**********************************************************************
        jsonUsuario.put("nomUsuario", textFieldUsuarioNuevo.getText().toUpperCase());
        jsonUsuario.put("activo", checkActivoUsuarioNuevo.isSelected());
        jsonUsuario.put("contrasenha", "");
        jsonUsuario.put("generico", true);
        jsonUsuario.put("email", textFieldEmailUsuarioNuevo.getText());
        jsonUsuario.put("funcionario", hashMapFunc.get(textFieldFuncionarioSelecc.getText()));
        if (!listViewAsignadoUsuarioNuevo.getItems().isEmpty()) {
            JSONArray usuarioRols = new JSONArray();
            for (int i = 0; i < listViewAsignadoUsuarioNuevo.getItems().size(); i++) {
                JSONObject jsonUsuarioRol = new JSONObject();
                jsonUsuarioRol.put("rol", hashMapRol.get(listViewAsignadoUsuarioNuevo.getItems().get(i)));
                jsonUsuarioRol.put("usuario", null);
                //**********************************************************************
                jsonUsuarioRol.put("usuMod", Identity.getNomFun());
                jsonUsuarioRol.put("fechaMod", null);//en el servidor
                jsonUsuarioRol.put("usuAlta", Identity.getNomFun());
                jsonUsuarioRol.put("fechaAlta", null);
                //**********************************************************************
                usuarioRols.add(jsonUsuarioRol);
            }
            jsonUsuario.put("usuarioRols", usuarioRols);
        } else {
            jsonUsuario.put("usuarioRols", null);
        }
        if (checkBoxCodSuper.isSelected()) {
            JSONObject jsonSupervisor = new JSONObject();
            jsonSupervisor.put("activo", true);
            jsonSupervisor.put("codSupervisor", UtilLoaderBase.msjIda(textFieldCodSupervisor.getText()));
            jsonSupervisor.put("nroSupervisor", 0);
            jsonSupervisor.put("usuario", null);
            //falta del lado server...
            //**********************************************************************
            jsonSupervisor.put("usuMod", Identity.getNomFun());
            jsonSupervisor.put("fechaMod", null);//en el servidor
            jsonSupervisor.put("usuAlta", Identity.getNomFun());
            jsonSupervisor.put("fechaAlta", null);
            //**********************************************************************
            JSONArray supervisor = new JSONArray();
            supervisor.add(jsonSupervisor);
            jsonUsuario.put("supervisor", supervisor);
        } else {
            jsonUsuario.put("supervisor", null);
        }
        //**********************************************************************
        jsonUsuario.put("usuMod", Identity.getNomFun());
        jsonUsuario.put("fechaMod", null);//en el servidor
        jsonUsuario.put("usuAlta", Identity.getNomFun());
        jsonUsuario.put("fechaAlta", null);
        //**********************************************************************
        return jsonUsuario;
    }
    //JSON CREANDO USUARIO
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    private UsuarioDTO insertarObtenerObj(UsuarioDTO oldUsuarioDTO) {
        java.util.Date date = new java.util.Date();
        Timestamp tiempoActual = new Timestamp(date.getTime());
        oldUsuarioDTO.setFechaMod(tiempoActual);
        oldUsuarioDTO.setFechaAlta(tiempoActual);
        CryptoBack cb = new CryptoBack();
        CryptoFront cf = new CryptoFront();
        String p = PasswordGenerator.getPassword(6);
        try {
            oldUsuarioDTO.setContrasenha(cb.getHash(cf.getHash(p)));
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
            System.out.println("-->> " + e.getMessage());
            System.out.println("-->> " + e.fillInStackTrace());
            e.printStackTrace();
        }
        // campo genérico seteado en true, en el front.
        Usuario usu = usuarioDAO.insertarObtenerObjeto(Usuario.fromUsuarioFuncDTO(oldUsuarioDTO));
        UsuarioDTO newUsuarioDTO = usu.toUsuarioDTOSeguridadRead();

//        String jsonUsuario = Recv.objectToJson(newUsuarioDTO);
//        SendLocalQueue.enviandoControlLocalRol(jsonUsuario, "I", "usuario", "localhost");
        // ******se le asignó código supervisor...******
        if (oldUsuarioDTO.getSupervisor() != null) {
            List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
            for (SupervisorDTO supervisorDTO : oldUsuarioDTO.getSupervisor()) {
                supervisorDTO.setUsuario(newUsuarioDTO);
                supervisorDTO.setNroSupervisor((supervisorDAO.obteniendoMaxNroSupervisor() + 1));
                supervisorDTO.setFechaAlta(tiempoActual);
                supervisorDTO.setFechaMod(tiempoActual);

                Supervisor supervisor = supervisorDAO.insertarObtenerObjeto(Supervisor.fromSupervisorAsociadoDTO(supervisorDTO));
                SupervisorDTO supervDTO = supervisor.toSupervisorSeguridadDTO();
                listSupervisorDTO.add(supervDTO);

                supervDTO.setUsuario(newUsuarioDTO);
//                String jsonSuperv = Recv.objectToJson(supervDTO);
//                SendLocalQueue.enviandoControlLocalRol(jsonSuperv, "I", "supervisor", "localhost");

            }
            newUsuarioDTO.setSupervisor(listSupervisorDTO);
        } else {
            newUsuarioDTO.setSupervisor(null);
        }
        // ******se le asignó código supervisor...******
        // ******se le asignó o asignaron roles...******
        if (oldUsuarioDTO.getUsuarioRols() != null) {
            List<UsuarioRolDTO> listUsuarioRolDTO = new ArrayList<>();
            for (UsuarioRolDTO usuarioRolDTO : oldUsuarioDTO.getUsuarioRols()) {
                usuarioRolDTO.setUsuario(newUsuarioDTO);

                UsuarioRol usuRol = usuarioRolDAO.insertarObtenerObjeto(UsuarioRol.fromUsuarioRolSeguridadDTO(usuarioRolDTO));
                UsuarioRolDTO usuRolDTO = usuRol.toBDUsuarioRolSeguridadDTO();
                listUsuarioRolDTO.add(usuRolDTO);

//                String jsonUsuRol = Recv.objectToJson(usuRolDTO);
//                SendLocalQueue.enviandoControlLocalRol(jsonUsuRol, "I", "usuario_rol", "localhost");
            }
            newUsuarioDTO.setUsuarioRols(listUsuarioRolDTO);
        }
        // ******se le asignó o asignaron roles...******
        // ******devolución de contraseña, por única vez******
        try {
            p = Utilidades.msjIda(p);
            newUsuarioDTO.setContrasenha(p);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Usuario usua = newUsuarioDTO.fromUsuarioSetNullDTO();
        newUsuarioDTO = usua.toBDUsuarioDTO();
        // ******devolución de contraseña, por única vez******
        return newUsuarioDTO/* .nullAllData() */;
    }

    @FXML
    private void btnAgregarFuncionarioAction(ActionEvent event) {
        habilitarFuncionario();
    }

    @FXML
    private void buttonGuardarAction(ActionEvent event) {
    }

    @FXML
    private void buttonSalirAction(ActionEvent event) {
    }

    @FXML
    private void chkActivoFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void chkHabilitadoFuncionarioAction(ActionEvent event) {
    }

    @FXML
    private void btnProcesarFuncionarioAction(ActionEvent event) {
        procesarFuncionario();
    }

    @FXML
    private void btnVolverFuncionarioAction(ActionEvent event) {
        deshabilitarFuncionario();
    }

    @FXML
    private void anchorPaneInsertarClienteKeyReleased(KeyEvent event) {
    }

    public void habilitarFuncionario() {
        txtCiFuncionario.setText("");
        txtNombreFuncionario.setText("");
        txtApellidoFuncionario.setText("");
        chkActivoFuncionario.setSelected(false);
        chkHabilitadoFuncionario.setSelected(false);
        splitPaneUsuarioNuevo.setDisable(true);
        panelFuncionario.setVisible(true);
        txtCiFuncionario.requestFocus();
    }

    public void deshabilitarFuncionario() {
        splitPaneUsuarioNuevo.setDisable(false);
        panelFuncionario.setVisible(false);
    }

    private void procesarFuncionario() {
        if (txtCiFuncionario.getText().equals("") || txtNombreFuncionario.getText().equals("") || txtApellidoFuncionario.getText().equals("")) {
            mensajeError("LOS CAMPOS CI, NOMBRE Y APELLIDO NO DEBEN QUEDAR VACIOS");
        } else {
            Funcionario fun = funcionarioDAO.listarFuncionarioPorCi(txtCiFuncionario.getText());
            if (fun.getIdFuncionario() == null) {
                fun.setCi(txtCiFuncionario.getText());
                fun.setNombre(txtNombreFuncionario.getText());
                fun.setApellido(txtApellidoFuncionario.getText());
                fun.setActivo(chkActivoFuncionario.isSelected());
                fun.setHabilitado(chkHabilitadoFuncionario.isSelected());
                funcionarioDAO.insertar(fun);
            } else {
                fun.setCi(txtCiFuncionario.getText());
                fun.setNombre(txtNombreFuncionario.getText());
                fun.setApellido(txtApellidoFuncionario.getText());
                fun.setActivo(chkActivoFuncionario.isSelected());
                fun.setHabilitado(chkHabilitadoFuncionario.isSelected());
                funcionarioDAO.actualizar(fun);
            }
            mensajeExito("DATOS PROCESADOS CORRECTAMENTE...");
            deshabilitarFuncionario();
            jsonArrayFuncionariosDisponibles("");
        }
    }

    private void buscarFuncionario() {
        if (txtCiFuncionario.getText().equals("")) {
            mensajeError("EL CAMPO CI NO DEBE QUEDAR VACIO");
        } else {
            Funcionario fun = funcionarioDAO.listarFuncionarioPorCi(txtCiFuncionario.getText());
            if (fun.getIdFuncionario() == null) {
                txtCiFuncionario.setText(fun.getCi());
                txtNombreFuncionario.setText(fun.getNombre());
                txtApellidoFuncionario.setText(fun.getApellido());
                chkActivoFuncionario.setSelected(fun.getActivo());
                chkHabilitadoFuncionario.setSelected(fun.getHabilitado());
            }
        }
    }
}
