/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Funcion;
import com.peluqueria.core.domain.Modulo;
import com.peluqueria.core.domain.Rol;
import com.peluqueria.core.domain.RolFuncion;
import com.peluqueria.dao.FuncionDAO;
import com.peluqueria.dao.ModuloDAO;
import com.peluqueria.dao.RolDAO;
import com.peluqueria.dao.RolFuncionDAO;
import com.peluqueria.dao.impl.FuncionDAOImpl;
import com.peluqueria.dao.impl.ModuloDAOImpl;
import com.peluqueria.dao.impl.RolDAOImpl;
import com.peluqueria.dao.impl.RolFuncionDAOImpl;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class RolEditarFXController extends BaseScreenController implements Initializable {

    private boolean alert;
    private JSONArray moduloJSONArray;
    private HashMap<String, JSONObject> hashMapModulo;
    private ObservableList<String> itemsAsignados;
    private ObservableList<String> itemsNOAsignados;
    private String descripcionRolNM;
    private boolean activoNM;
    Tooltip toolTipCombo;
    private String msjError;

    private RolDAO rolDAO = new RolDAOImpl();
    private ModuloDAO moduloDAO = new ModuloDAOImpl();
    private FuncionDAO funcionDAO = new FuncionDAOImpl();
    private RolFuncionDAO rolFuncionDAO = new RolFuncionDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneEditarRol;
    @FXML
    private SplitPane splitPaneEditarRol;
    @FXML
    private AnchorPane anchorPaneHeaderEditarRol;
    @FXML
    private ComboBox<String> comboBoxModuloEditarRol;
    @FXML
    private Label lableModuloEditarRol;
    @FXML
    private Label lableDescripcionEditarRol;
    @FXML
    private TextField textFieldEditarRol;
    @FXML
    private AnchorPane anchorPaneListViewEditarRol;
    @FXML
    private ListView<String> listViewSinAsignarEditarRol;
    @FXML
    private ListView<String> listViewAsignadoEditarRol;
    @FXML
    private Button buttonAsignarEditarRol;
    @FXML
    private Button buttonAsignarTodosEditarRol;
    @FXML
    private Button buttonQuitarEditarRol;
    @FXML
    private Button buttonQuitarTodosEditarRol;
    @FXML
    private CheckBox checkActivoEditarRol;
    @FXML
    private Label labelFuncionNoRolEditar;
    @FXML
    private Label labelFuncionSiRolEditar;
    @FXML
    private AnchorPane anchorPaneBottomEditarRol;
    @FXML
    private Button buttonActualizarEditarRol;
    @FXML
    private Button buttonVolverEditarRol;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial(true);
    }

    @FXML
    private void comboBoxEditarRolAction(ActionEvent event) {
        escuchandoComboBox();
    }

    @FXML
    private void botonAsignarEditarRolAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void botonAsignarTodosEditarRolAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void botonQuitarEditarRolAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void botonQuitarTodosEditarRolAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void checkBoxActivoEditarRolAction(ActionEvent event) {
        checkBox();
    }

    @FXML
    private void botonActualizarEditarRolAction(ActionEvent event) {
        actualizarRol();
    }

    @FXML
    private void botonVolverEditarRolAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void anchorPaneEditarRolKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL ***********************************
    private void cargandoInicial(boolean inicial) {
        if (inicial) {
            alert = false;
            msjError = "";
            textFieldEditarRol.requestFocus();
            jsonArrayModuloFetch();
            descripcionRolNM = RolFXController.getJsonRol().get("descripcion").toString().toUpperCase();
            activoNM = checkActivoEditarRol.isSelected();
            textFieldEditarRol.setText(descripcionRolNM);
            checkActivoEditarRol.setSelected((boolean) RolFXController.getJsonRol().get("activo"));
            listViewAsignadoEditarRol.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            listViewSinAsignarEditarRol.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
            buttonAsignarEditarRol.setDisable(true);
            buttonAsignarTodosEditarRol.setDisable(true);
            buttonQuitarEditarRol.setDisable(true);
            buttonQuitarTodosEditarRol.setDisable(true);
            checkBox();
            tootTip();
        } else {
            msjError = "";
            textFieldEditarRol.requestFocus();
            buttonAsignarEditarRol.setDisable(true);
            buttonAsignarTodosEditarRol.setDisable(true);
            buttonQuitarEditarRol.setDisable(true);
            buttonQuitarTodosEditarRol.setDisable(true);
            comboBoxModuloEditarRol.getSelectionModel().selectFirst();
            checkBox();
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/RolFX.fxml", 394, 575, "/vista/seguridad/RolEditarFX.fxml", 600, 494, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeExito(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                actualizarRol();
            }
        }
    }

    private void checkBox() {
        if (checkActivoEditarRol.isSelected()) {
            checkActivoEditarRol.setText("Activo");
            checkActivoEditarRol.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            checkActivoEditarRol.setText("Inactivo");
            checkActivoEditarRol.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }

    private void escuchandoComboBox() {
        if (comboBoxModuloEditarRol.getSelectionModel().getSelectedIndex() == 0) {
            buttonAsignarEditarRol.setDisable(true);
            buttonAsignarTodosEditarRol.setDisable(true);
            buttonQuitarEditarRol.setDisable(true);
            buttonQuitarTodosEditarRol.setDisable(true);
        } else {
            buttonAsignarEditarRol.setDisable(false);
            buttonAsignarTodosEditarRol.setDisable(false);
            buttonQuitarEditarRol.setDisable(false);
            buttonQuitarTodosEditarRol.setDisable(false);
        }
        JSONObject jsonModulo = hashMapModulo.get(comboBoxModuloEditarRol.getSelectionModel().getSelectedItem());
        long idModulo = -1;
        long idRol = Long.valueOf(RolFXController.getJsonRol().get("idRol").toString());
        if (jsonModulo != null) {
            idModulo = Long.valueOf(jsonModulo.get("idModulo").toString());
        }
        itemsAsignados = FXCollections.observableArrayList(listFuncionesAsignadas(idModulo, idRol));
        listViewAsignadoEditarRol.setItems(itemsAsignados);
        itemsNOAsignados = FXCollections.observableArrayList(listFuncionesNoAsignadas(idModulo, idRol));
        listViewSinAsignarEditarRol.setItems(itemsNOAsignados);
    }

    private void tootTip() {
        toolTipCombo = new Tooltip("No olvides actualizar\ncada vez que asignes y quites\nfunciones de un módulo.");
        toolTipCombo.setStyle("-fx-font: normal bold 12 System; "
                + "-fx-background-color: #F2F2F2; "
                + "-fx-text-fill: #39aedd;");
        comboBoxModuloEditarRol.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Point2D p = comboBoxModuloEditarRol.localToScreen(comboBoxModuloEditarRol.getLayoutBounds().getMinX(), comboBoxModuloEditarRol.getLayoutBounds().getMaxY()); //I position the tooltip at bottom right of the node (see below for explanation)
                toolTipCombo.show(comboBoxModuloEditarRol, p.getX(), p.getY());
                toolTipCombo = AnimationFX.fadeTooltip(toolTipCombo);
            }
        });
        comboBoxModuloEditarRol.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                toolTipCombo.hide();
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void asignando() {
        itemsAsignados.addAll(listViewSinAsignarEditarRol.getSelectionModel().getSelectedItems());
        itemsNOAsignados.removeAll(listViewSinAsignarEditarRol.getSelectionModel().getSelectedItems());
        listViewAsignadoEditarRol.setItems(itemsAsignados);
        listViewSinAsignarEditarRol.setItems(itemsNOAsignados);
    }

    private void asignandoTodos() {
        itemsAsignados.addAll(itemsNOAsignados);
        itemsNOAsignados.removeAll(itemsNOAsignados);
        listViewAsignadoEditarRol.setItems(itemsAsignados);
        listViewSinAsignarEditarRol.setItems(itemsNOAsignados);
    }

    private void quitando() {
        itemsNOAsignados.addAll(listViewAsignadoEditarRol.getSelectionModel().getSelectedItems());
        itemsAsignados.removeAll(listViewAsignadoEditarRol.getSelectionModel().getSelectedItems());
        listViewAsignadoEditarRol.setItems(itemsAsignados);
        listViewSinAsignarEditarRol.setItems(itemsNOAsignados);
    }

    private void quitandoTodos() {
        itemsNOAsignados.addAll(itemsAsignados);
        itemsAsignados.removeAll(itemsAsignados);
        listViewAsignadoEditarRol.setItems(itemsAsignados);
        listViewSinAsignarEditarRol.setItems(itemsNOAsignados);
    }

    private void actualizarRol() {
        JSONParser parser = new JSONParser();
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA ACTUALIZAR ROL?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            alert.close();
            boolean rol = false;//registrar quién editó, en caso de ser solo permisos (funciones)           
            if (textFieldEditarRol.getText().contentEquals("")) {
                alert.close();
                mensajeError("EL NOMBRE DEL ROL NO DEBE ESTAR NULO.");
            } else {
                //se tiene una modificación pendiente con Rol (UPDATE)
                if (!descripcionRolNM.contentEquals(textFieldEditarRol.getText().toUpperCase()) || checkActivoEditarRol.isSelected() != activoNM) {
                    Rol jsonRol = editandoRol();
                    if (jsonRol.getIdRol() != null) {
                        Rol roles = rolDAO.getById(Long.parseLong(RolFXController.getJsonRol().get("idRol").toString()));
                        roles.setFechaAlta(null);
                        roles.setFechaMod(null);
                        roles.setRolFuncions(null);
                        roles.setUsuarioRols(null);
                        JSONObject jsonRoles = new JSONObject();
                        try {
                            jsonRoles = (JSONObject) parser.parse(gson.toJson(roles));
                        } catch (ParseException ex) {
                            System.out.println("-->> " + ex.getLocalizedMessage());
                            System.out.println("-->> " + ex.fillInStackTrace());
                        }

                        List<RolFuncion> rolFunc = rolFuncionDAO.buscarPorRol(Long.parseLong(RolFXController.getJsonRol().get("idRol").toString()));
                        JSONArray array = new JSONArray();
                        for (RolFuncion rolFuncion : rolFunc) {
                            rolFuncion.setFechaAlta(null);
                            rolFuncion.setFechaMod(null);
                            rolFuncion.getRol().setFechaAlta(null);
                            rolFuncion.getRol().setFechaMod(null);
                            rolFuncion.getRol().setRolFuncions(null);
                            rolFuncion.getRol().setUsuarioRols(null);
                            rolFuncion.getFuncion().setRolFuncions(null);
                            JSONObject jsonRolFunc = new JSONObject();
                            try {
                                jsonRolFunc = (JSONObject) parser.parse(gson.toJson(rolFuncion));
                            } catch (ParseException ex) {
                                System.out.println("-->> " + ex.getLocalizedMessage());
                                System.out.println("-->> " + ex.fillInStackTrace());
                            }
                            array.add(jsonRolFunc);
                        }

                        jsonRoles.put("rolFuncions", array);
                        RolFXController.setJsonRol(jsonRoles);
                        descripcionRolNM = textFieldEditarRol.getText().toUpperCase();
                        activoNM = checkActivoEditarRol.isSelected();
                        rol = true;
                    }
                }
                //se tiene una modificación pendiente con Rol - Función (INSERT - DELETE)
                if (comboBoxModuloEditarRol.getSelectionModel().getSelectedIndex() != 0) {
                    boolean primerElemento = true;
                    String cadenaFuncionQuitar = "";
                    String cadenaFuncionAgregar = "";
                    JSONObject jsonModulo = hashMapModulo.get(comboBoxModuloEditarRol.getSelectionModel().getSelectedItem());
                    long idModulo = -1;
                    long idRol = Long.valueOf(RolFXController.getJsonRol().get("idRol").toString());
                    if (jsonModulo != null) {
                        idModulo = Long.valueOf(jsonModulo.get("idModulo").toString());
                    }
                    List<String> stringSourceBD = listFuncionesAsignadas(idModulo, idRol);
                    List<String> stringNOSourceBD = listFuncionesNoAsignadas(idModulo, idRol);
                    //Se verifica la función que el usuario AGREGÓ al Rol, con lo***  
                    //que hay en la BD..********************************************
                    if (itemsAsignados.size() > 0) {
                        for (int i = 0; i < itemsAsignados.size(); i++) {
                            if (stringSourceBD.isEmpty()) {
                                if (primerElemento) {
                                    cadenaFuncionAgregar = "'" + itemsAsignados.get(i) + "'";
                                    primerElemento = false;
                                } else {
                                    cadenaFuncionAgregar = cadenaFuncionAgregar + ", '" + itemsAsignados.get(i) + "'";
                                }
                            } else {
                                boolean esNuevo = true;
                                for (int j = 0; j < stringSourceBD.size(); j++) {
                                    String aux = itemsAsignados.get(i);
                                    if (aux.contentEquals(stringSourceBD.get(j))) {
                                        esNuevo = false;
                                        break;
                                    }
                                }
                                if (esNuevo) {
                                    if (primerElemento) {
                                        cadenaFuncionAgregar = "'" + itemsAsignados.get(i) + "'";
                                        primerElemento = false;
                                    } else {
                                        cadenaFuncionAgregar = cadenaFuncionAgregar + ", '" + itemsAsignados.get(i) + "'";
                                    }
                                }
                            }
                        }

                    }
                    primerElemento = true;
                    //**************************************************************
                    //Se verifica la función que el usuario QUITÓ al Rol, con lo que
                    //hay en la BD...***********************************************
                    if (itemsNOAsignados.size() > 0) {
                        for (int i = 0; i < itemsNOAsignados.size(); i++) {
                            if (stringNOSourceBD.isEmpty()) {
                                if (primerElemento) {
                                    cadenaFuncionQuitar = "'" + itemsNOAsignados.get(i) + "'";
                                    primerElemento = false;
                                } else {
                                    cadenaFuncionQuitar = cadenaFuncionQuitar + ", '" + itemsNOAsignados.get(i) + "'";
                                }
                            } else {
                                boolean esNuevo = true;
                                for (int j = 0; j < stringNOSourceBD.size(); j++) {
                                    String aux = itemsNOAsignados.get(i);
                                    if (aux.contentEquals(stringNOSourceBD.get(j))) {
                                        esNuevo = false;
                                        break;
                                    }
                                }
                                if (esNuevo) {
                                    if (primerElemento) {
                                        cadenaFuncionQuitar = "'" + itemsNOAsignados.get(i) + "'";
                                        primerElemento = false;
                                    } else {
                                        cadenaFuncionQuitar = cadenaFuncionQuitar + ", '" + itemsNOAsignados.get(i) + "'";
                                    }
                                }
                            }
                        }
                    }
                    //registrar quién editó, en caso de ser solo permisos (funciones)
                    if (!rol) {
                        Rol jsonRol = editandoRol();
                        if (jsonRol.getIdRol() != null) {
                            Rol roles = rolDAO.getById(Long.parseLong(RolFXController.getJsonRol().get("idRol").toString()));
                            roles.setFechaAlta(null);
                            roles.setFechaMod(null);
                            roles.setRolFuncions(null);
                            roles.setUsuarioRols(null);
                            JSONObject jsonRoles = new JSONObject();
                            try {
                                jsonRoles = (JSONObject) parser.parse(gson.toJson(roles));
                            } catch (ParseException ex) {
                                System.out.println("-->> " + ex.getLocalizedMessage());
                                System.out.println("-->> " + ex.fillInStackTrace());
                            }

                            List<RolFuncion> rolFunc = rolFuncionDAO.buscarPorRol(Long.parseLong(RolFXController.getJsonRol().get("idRol").toString()));
                            JSONArray array = new JSONArray();
                            for (RolFuncion rolFuncion : rolFunc) {
                                rolFuncion.setFechaAlta(null);
                                rolFuncion.setFechaMod(null);
                                rolFuncion.getRol().setFechaAlta(null);
                                rolFuncion.getRol().setFechaMod(null);
                                rolFuncion.getRol().setRolFuncions(null);
                                rolFuncion.getRol().setUsuarioRols(null);
                                rolFuncion.getFuncion().setRolFuncions(null);
                                JSONObject jsonRolFunc = new JSONObject();
                                try {
                                    jsonRolFunc = (JSONObject) parser.parse(gson.toJson(rolFuncion));
                                } catch (ParseException ex) {
                                    System.out.println("-->> " + ex.getLocalizedMessage());
                                    System.out.println("-->> " + ex.fillInStackTrace());
                                }
                                array.add(jsonRolFunc);
                            }

                            jsonRoles.put("rolFuncions", array);
                            RolFXController.setJsonRol(jsonRoles);
                            descripcionRolNM = textFieldEditarRol.getText().toUpperCase();
                            activoNM = checkActivoEditarRol.isSelected();
                            rol = true;
                        }
//                        if (!jsonRol.isEmpty()) {
//                            jsonRol.put("rolFuncions", RolFXController.getJsonRol().get("rolFuncions"));
//                            RolFXController.setJsonRol(jsonRol);
//                            descripcionRolNM = textFieldEditarRol.getText().toUpperCase();
//                            activoNM = checkActivoEditarRol.isSelected();
//                            rol = true;
//                        }
                    }
                    //**************************************************************
                    boolean exitoQuitar = true;
                    boolean exitoAsignar = true;
                    //Se concatena y se filtra, para quitar funciones al rol...*****
                    if (!cadenaFuncionQuitar.contentEquals("")) {
                        cadenaFuncionQuitar = "( " + cadenaFuncionQuitar + " )";
                        exitoQuitar = deleteRolFuncion(buscarRolFuncion(idRol, cadenaFuncionQuitar));
                        if (!exitoQuitar) {
                            msjError = "FALLO AL INTENTAR QUITAR FUNCIONES.";
                        }
                    }
                    //**************************************************************
                    //Se concatena y se filtra, para asignar nuevas funciones al rol
                    if (!cadenaFuncionAgregar.contentEquals("")) {
                        cadenaFuncionAgregar = "( " + cadenaFuncionAgregar + " )";
                        exitoAsignar = createRolFuncion(RolFXController.getJsonRol(), buscarFuncion(cadenaFuncionAgregar));
                        if (!exitoAsignar) {
                            msjError = "FALLO AL INTENTAR AGREGAR FUNCIONES.";
                        }
                    }
                    //**************************************************************
                    if (rol && exitoQuitar && exitoAsignar) {
                        alert.close();
                        mensajeExito("¡ÉXITO! EL ROL " + descripcionRolNM + " SE ACTUALIZÓ.");
                        cargandoInicial(false);
                    } else {
                        alert.close();
                        mensajeError(msjError);
                    }
                } else if (rol) {
                    alert.close();
                    mensajeExito("¡ÉXITO! EL ROL " + descripcionRolNM + " SE ACTUALIZÓ.");
                    cargandoInicial(false);
                } else {
                    if (!msjError.isEmpty()) {
                        alert.close();
                        mensajeError(msjError);
                    } else {
                        alert.close();
                    }
                }
            }
        } else {
            alert.close();
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, MÓDULO -> GET
    private void jsonArrayModuloFetch() {
        JSONParser parser = new JSONParser();
        moduloJSONArray = new JSONArray();
        hashMapModulo = new HashMap<>();
        comboBoxModuloEditarRol.getItems().add("N/A");
        comboBoxModuloEditarRol.getSelectionModel().selectFirst();
        hashMapModulo.put("N/A", null);
        List<Modulo> list = moduloDAO.listar();
        for (Modulo modulo : list) {
            Timestamp fechaAlta = null;
            Timestamp fechaMod = null;
            try {
                fechaAlta = modulo.getFechaAlta();
            } catch (Exception e) {
                fechaAlta = null;
            } finally {
            }
            try {
                fechaMod = modulo.getFechaMod();
            } catch (Exception e) {
                fechaMod = null;
            } finally {
            }

            modulo.setFechaAlta(null);
            modulo.setFechaMod(null);
            List<Funcion> func = new ArrayList<>();
            try {
                func = modulo.getFuncions();
            } catch (Exception e) {
                func = new ArrayList<>();
            } finally {
            }
            modulo.setFuncions(null);
            try {
                JSONObject jsonModulo = (JSONObject) parser.parse(gson.toJson(modulo));
                jsonModulo.put("fechaAlta", fechaAlta);
                jsonModulo.put("fechaMod", fechaMod);
                JSONArray array = new JSONArray();
                if (func == null) {
                    func = new ArrayList<>();
                }
                if (!func.isEmpty()) {
                    for (Funcion funci : func) {
                        funci.setRolFuncions(null);
                        JSONObject jsonFunci = (JSONObject) parser.parse(gson.toJson(funci));
                        array.add(jsonFunci);
                    }
                }
                if (!array.isEmpty()) {
                    jsonModulo.put("funcions", array);
                }
                if (!hashMapModulo.containsKey(modulo.getDescripcion())) {
                    hashMapModulo.put(modulo.getDescripcion(), jsonModulo);
                    comboBoxModuloEditarRol.getItems().add(modulo.getDescripcion());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/modulo");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                moduloJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object moduloObj : moduloJSONArray) {
//                    JSONObject moduloJSONObj = (JSONObject) moduloObj;
//                    if (!hashMapModulo.containsKey(moduloJSONObj.get("descripcion").toString())) {
//                        hashMapModulo.put(moduloJSONObj.get("descripcion").toString(), moduloJSONObj);
//                        comboBoxModuloEditarRol.getItems().add(moduloJSONObj.get("descripcion").toString());
//                    }
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
    }
    //////READ, MÓDULO -> GET

    //////READ, FUNCIONES ASIGNADAS -> GET
    private List<String> listFuncionesAsignadas(long idModulo, long idRol) {
        JSONParser parser = new JSONParser();
        List<String> listFuncionesAsignadas = new ArrayList<>();
        listFuncionesAsignadas = funcionDAO.buscarRolFuncionAsignado(idModulo, idRol);
//        try {
//            String idM = UtilLoaderBase.msjIda(String.valueOf(idModulo));
//            String idR = UtilLoaderBase.msjIda(String.valueOf(idRol));
//            URL url = new URL(Utilidades.ip + "/ServerParana/funcion/asig/" + idM + "/" + idR);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                listFuncionesAsignadas = (List<String>) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return listFuncionesAsignadas;
    }
    //////READ, FUNCIONES ASIGNADAS -> GET

    //////READ, FUNCIONES NO ASIGNADAS -> GET
    private List<String> listFuncionesNoAsignadas(long idModulo, long idRol) {
        JSONParser parser = new JSONParser();
        List<String> listFuncionesNoAsignadas = new ArrayList<>();
        listFuncionesNoAsignadas = funcionDAO.buscarRolFuncionNoAsignado(idModulo, idRol);
//        try {
//            String idM = UtilLoaderBase.msjIda(String.valueOf(idModulo));
//            String idR = UtilLoaderBase.msjIda(String.valueOf(idRol));
//            URL url = new URL(Utilidades.ip + "/ServerParana/funcion/noasig/" + idM + "/" + idR);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                listFuncionesNoAsignadas = (List<String>) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return listFuncionesNoAsignadas;
    }
    //////READ, FUNCIONES NO ASIGNADAS -> GET

    //////UPDATE, ROL -> PUT
    private Rol editandoRol() {
        String inputLine;
        JSONParser parser = new JSONParser();
        Rol rol = new Rol();
        try {
            rol = editandoJsonRol();
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("PUT");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(rol.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    rol = (JSONObject) parser.parse(inputLine);
//                    if (rol.get("idRol") == null) {
//                        msjError = "FALLO AL INTENTAR EDITAR ROL\n-> VERIFIQUE, EL NOMBRE DEL ROL SEA ÚNICO.";
//                        rol = new JSONObject();
//                    }
//                }
//                br.close();
//            } else {
//                msjError = "FALLO AL INTENTAR EDITAR ROL\n-> PROBLEMAS CON EL SERVIDOR (" + HttpResult + ").";
//                rol = new JSONObject();
//            }
        } catch (Exception ex) {
            rol = new Rol();
            msjError = "FALLO AL INTENTAR EDITAR ROL\n-> VERIFIQUE, CONEXIÓN.";
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        return rol;
    }
    //////UPDATE, ROL -> PUT

    //////DELETE, ROL - FUNCIÓN -> DELETE
    private boolean deleteRolFuncion(JSONArray jsonRolFuncion) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exito = false;
        for (Object object : jsonRolFuncion) {
            JSONObject objJSON;
            try {
                objJSON = (JSONObject) parser.parse(object.toString());
                long idRolFunc = Long.parseLong(objJSON.get("idRolFuncion").toString());
                RolFuncion rolF = rolFuncionDAO.getById(idRolFunc);
                rolFuncionDAO.eliminarByObj(rolF);
                exito = true;
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.getMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
                exito = false;
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rolFuncion/delete");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("DELETE");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonRolFuncion.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    exito = (boolean) parser.parse(inputLine);
//                }
//                br.close();
//            }
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return exito;
    }
    //////DELETE, ROL - FUNCIÓN -> DELETE

    //////READ, ROL - FUNCIÓN -> GET
    private JSONArray buscarRolFuncion(long idR, String cIN) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayRolFuncion = new JSONArray();
        try {
//            String idRol = UtilLoaderBase.msjIda(String.valueOf(idR));
//            String cadenaIN = UtilLoaderBase.msjIda(String.valueOf(cIN));
            List<RolFuncion> listRolFuncion = rolFuncionDAO.buscarRolFuncion(idR, cIN);
            for (RolFuncion rolFuncion : listRolFuncion) {
                rolFuncion.setFechaAlta(null);
                rolFuncion.setFechaMod(null);
                rolFuncion.getRol().setFechaAlta(null);
                rolFuncion.getRol().setFechaMod(null);
                rolFuncion.getRol().setRolFuncions(null);
                rolFuncion.getRol().setUsuarioRols(null);
                rolFuncion.getFuncion().setRolFuncions(null);
                JSONObject jsonFunci = (JSONObject) parser.parse(gson.toJson(rolFuncion));
                jsonArrayRolFuncion.add(jsonFunci);
            }

//            URL url = new URL(Utilidades.ip + "/ServerParana/rolFuncion/brf/" + idRol + "/" + cadenaIN + "");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonArrayRolFuncion = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
        } catch (Exception e) {
            Utilidades.log.error("ERROR Exception: " + e.getLocalizedMessage());
            Utilidades.log.error("ERROR Exception: " + e.getMessage());
            Utilidades.log.error("ERROR Exception: " + e.fillInStackTrace());
        }
//        catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return jsonArrayRolFuncion;
    }
    //////READ, ROL - FUNCIÓN -> GET

    ///////READ, FUNCIÓN -> GET
    private JSONArray buscarFuncion(String cIN) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncion = new JSONArray();
        List<Funcion> func = funcionDAO.buscarFuncion(cIN);
        if (!func.isEmpty()) {
            for (Funcion funcion : func) {
                funcion.setRolFuncions(null);
                Modulo mod = funcion.getModulo();
                Modulo modu = new Modulo();
                modu.setIdModulo(mod.getIdModulo());
                modu.setFechaAlta(null);
                modu.setFechaMod(null);
                funcion.setModulo(modu);
                try {
                    JSONObject jsonFunc = (JSONObject) parser.parse(gson.toJson(funcion));
                    jsonArrayFuncion.add(jsonFunc);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    Utilidades.log.error("ERROR Exception: " + ex.getMessage());
                }
            }

        }
//        try {
//            String cadenaIN = UtilLoaderBase.msjIda(cIN);
//            URL url = new URL(Utilidades.ip + "/ServerParana/funcion/bf/" + cadenaIN + "");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonArrayFuncion = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return jsonArrayFuncion;
    }
    //////READ, FUNCIÓN -> GET

    //////CREATE, ROL - FUNCIÓN -> POST
    private boolean createRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exito = false;
        exito = creandoJsonRolFuncion(jsonRol, jsonArrayFuncion);

//        JSONArray jsonArrayRolFuncion = creandoJsonRolFuncion(jsonRol, jsonArrayFuncion);
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rolFuncion/c");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("POST");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonArrayRolFuncion.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    exito = (boolean) parser.parse(inputLine);
//                }
//                br.close();
//            }
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return exito;
    }
    //////CREATE, ROL - FUNCIÓN -> POST
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON EDITANDO ROL
    private Rol editandoJsonRol() {
        Rol rol = new Rol();
        rol.setIdRol(Long.parseLong(RolFXController.getJsonRol().get("idRol").toString()));
        rol.setActivo(checkActivoEditarRol.isSelected());
        rol.setDescripcion(textFieldEditarRol.getText().toUpperCase());
        rol.setTipo("");
        rol.setUsuAlta(RolFXController.getJsonRol().get("usuAlta").toString());
        rol.setUsuMod(Identity.getNomFun());
//        rol.setFechaAlta(null);
//        rol.setFechaMod(null);

        rolDAO.actualizar(rol);

//        JSONObject rol = new JSONObject();
//        //**********************************************************************
//        rol.put("idRol", RolFXController.getJsonRol().get("idRol"));
//        rol.put("activo", checkActivoEditarRol.isSelected());
//        rol.put("descripcion", textFieldEditarRol.getText().toUpperCase());
//        rol.put("tipo", "");
//        //**********************************************************************
//        rol.put("usuMod", Identity.getNomFun());
//        rol.put("fechaMod", null);//en el servidor
//        rol.put("usuAlta", RolFXController.getJsonRol().get("usuAlta"));
//        rol.put("fechaAlta", RolFXController.getJsonRol().get("fechaAlta"));
//        //**********************************************************************
        return rol;
    }
//    private JSONObject editandoJsonRol() {
//        JSONObject rol = new JSONObject();
//        //**********************************************************************
//        rol.put("idRol", RolFXController.getJsonRol().get("idRol"));
//        rol.put("activo", checkActivoEditarRol.isSelected());
//        rol.put("descripcion", textFieldEditarRol.getText().toUpperCase());
//        rol.put("tipo", "");
//        //**********************************************************************
//        rol.put("usuMod", Identity.getNomFun());
//        rol.put("fechaMod", null);//en el servidor
//        rol.put("usuAlta", RolFXController.getJsonRol().get("usuAlta"));
//        rol.put("fechaAlta", RolFXController.getJsonRol().get("fechaAlta"));
//        //**********************************************************************
//        return rol;
//    }
    //JSON EDITANDO ROL

    //JSON CREANDO ROL - FUNCIÓN
    private boolean creandoJsonRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
        List<RolFuncion> listRolFun = new ArrayList<>();
        boolean val = false;
        for (Object objFuncion : jsonArrayFuncion) {
            JSONObject jsonFuncion = (JSONObject) objFuncion;
            RolFuncion rf = new RolFuncion();

            Rol rol = new Rol();
            rol.setIdRol(Long.parseLong(jsonRol.get("idRol").toString()));

            Funcion func = new Funcion();
            func.setIdFuncion(Long.parseLong(jsonFuncion.get("idFuncion").toString()));

            rf.setRol(rol);
            rf.setFuncion(func);
            rf.setFechaMod(null);
            rf.setFechaAlta(null);
            rf.setUsuMod(Identity.getNomFun());
            rf.setUsuAlta(Identity.getNomFun());

            val = rolFuncionDAO.crearDeUno(rf);

//            JSONObject jsonFuncion = (JSONObject) objFuncion;
//            JSONObject jsonRolFuncion = new JSONObject();
//            //******************************************************************
//            jsonRolFuncion.put("rol", jsonRol);
//            jsonRolFuncion.put("funcion", jsonFuncion);
//            //******************************************************************
//            jsonRolFuncion.put("usuMod", Identity.getNomFun());
//            jsonRolFuncion.put("fechaMod", null);//en el servidor
//            jsonRolFuncion.put("usuAlta", Identity.getNomFun());
//            jsonRolFuncion.put("fechaAlta", null);
//            //******************************************************************
//            jsonArrayrolFuncion.add(jsonRolFuncion);
        }
        return val;
    }
//    private JSONArray creandoJsonRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
//        JSONArray jsonArrayrolFuncion = new JSONArray();
//        for (Object objFuncion : jsonArrayFuncion) {
//            JSONObject jsonFuncion = (JSONObject) objFuncion;
//            JSONObject jsonRolFuncion = new JSONObject();
//            //**********************************************************************
//            jsonRolFuncion.put("rol", jsonRol);
//            jsonRolFuncion.put("funcion", jsonFuncion);
//            //**********************************************************************
//            jsonRolFuncion.put("usuMod", Identity.getNomFun());
//            jsonRolFuncion.put("fechaMod", null);//en el servidor
//            jsonRolFuncion.put("usuAlta", Identity.getNomFun());
//            jsonRolFuncion.put("fechaAlta", null);
//            //**********************************************************************
//            jsonArrayrolFuncion.add(jsonRolFuncion);
//        }
//        return jsonArrayrolFuncion;
//    }
    //JSON CREANDO ROL - FUNCIÓN
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
}
