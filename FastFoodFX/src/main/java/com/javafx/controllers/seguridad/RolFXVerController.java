/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Funcion;
import com.peluqueria.core.domain.Modulo;
import com.peluqueria.dao.FuncionDAO;
import com.peluqueria.dao.impl.FuncionDAOImpl;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Paint;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class RolFXVerController extends BaseScreenController implements Initializable {

    private ObservableList<JSONObject> rolData;
    private List<JSONObject> clienteList;
    private JSONArray jsonArrayRolFuncion;
    private List<JSONObject> jsonList;
    private FuncionDAO funcDAO = new FuncionDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneVerRol;
    @FXML
    private SplitPane splitPaneRolVer;
    @FXML
    private AnchorPane anchorPaneDet;
    @FXML
    private Label labelDescripcionVerRol;
    @FXML
    private Label labelActivoVerRol;
    @FXML
    private TextField textDescripcionVerRol;
    @FXML
    private RadioButton radioActivoVerRol;
    @FXML
    private AnchorPane anchorPaneTableViewVerRol;
    @FXML
    private TableView<JSONObject> tableViewVerRol;
    @FXML
    private TableColumn<JSONObject, String> columnFuncionVerRol;
    @FXML
    private TableColumn<JSONObject, String> columnModuloVerRol;
    @FXML
    private AnchorPane anchorPaneFootVerRol;
    @FXML
    private Label labelAltaPorVerRol;
    @FXML
    private Label labelModificadoPorVerRol;
    @FXML
    private Label labelFechaAltaVerRol;
    @FXML
    private Label labelFechaModificacionVerRol;
    @FXML
    private Label lableFechasVerRol;
    @FXML
    private TextField textAltaPorVerRol;
    @FXML
    private TextField textModificadoPorVerRol;
    @FXML
    private TextField textFechaAltaVerRol;
    @FXML
    private TextField textFechaModificacionVerRol;
    @FXML
    private Button buttonVolverVerRol;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverVerRolAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void anchorPaneVerRolKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        String usuMod = "N/A";
        String usuAlta = "N/A";
        String fechaMod = "N/A";
        String fechaAlta = "N/A";
        if (RolFXController.getJsonRol().get("usuMod") != null) {
            usuMod = RolFXController.getJsonRol().get("usuMod").toString();
        }
        if (RolFXController.getJsonRol().get("usuAlta") != null) {
            usuAlta = RolFXController.getJsonRol().get("usuAlta").toString();
        }
        if (RolFXController.getJsonRol().get("fechaMod") != null) {
//            try {
//                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
//                Date parsedDate = dateFormat.parse(RolFXController.getJsonRol().get("fechaMod").toString());
//                Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
            fechaMod = RolFXController.getJsonRol().get("fechaMod").toString();
//            } catch (Exception e) { //this generic but you can control another types of exception
            // look the origin of excption 
//            }
//            Long timestampJSON = Long.valueOf(RolFXController.getJsonRol().get("fechaMod").toString());
//            Timestamp tsMod = new Timestamp(timestampJSON);
//            fechaMod = Utilidades.getTSFormat().format(tsMod);
        }
        if (RolFXController.getJsonRol().get("fechaAlta") != null) {
//            Long timestampJSON = Long.valueOf(RolFXController.getJsonRol().get("fechaAlta").toString());
//            Timestamp tsAlta = new Timestamp(timestampJSON);
//            fechaAlta = Utilidades.getTSFormat().format(tsAlta);
            fechaAlta = RolFXController.getJsonRol().get("fechaAlta").toString();
        }
        textDescripcionVerRol.setText(RolFXController.getJsonRol().get("descripcion").toString());
        radioActivoVerRol.setSelected((boolean) RolFXController.getJsonRol().get("activo"));
        radioBox();
        textAltaPorVerRol.setText(usuAlta.toUpperCase());
        textModificadoPorVerRol.setText(usuMod.toUpperCase());
        textFechaAltaVerRol.setText(fechaAlta.toUpperCase());
        textFechaModificacionVerRol.setText(fechaMod.toUpperCase());
        JSONArray jsonArrayRolFuncion = (JSONArray) RolFXController.getJsonRol().get("rolFuncions");
        if (jsonArrayRolFuncion != null) {
            this.jsonArrayRolFuncion = sortJsonArrayFuncion(jsonArrayRolFuncion, "descripcion", true);
            actualizandoTablaCliente();
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/RolFX.fxml", 394, 575, "/vista/seguridad/RolFXVer.fxml", 600, 456, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            navegandoAAnterior();
        }
    }

    private void radioBox() {
        if (radioActivoVerRol.isSelected()) {
            labelActivoVerRol.setText("Activo");
            labelActivoVerRol.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            labelActivoVerRol.setText("Inactivo");
            labelActivoVerRol.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //ejemplo estudiado de -> http://stackoverflow.com/questions/19543862/how-can-i-sort-a-jsonarray-in-java
    //segundo orden "MÓDULO"
    private JSONArray sortJsonArrayModulo(JSONArray jsonArray, String campo, boolean asc) {
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < jsonArray.size(); i++) {
            jsonValues.add((JSONObject) jsonArray.get(i));
        }
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();
                try {
                    JSONObject funcionA = (JSONObject) a.get("funcion");
                    JSONObject funcionB = (JSONObject) b.get("funcion");
                    Funcion funA = funcDAO.getByIdFuncion(Long.parseLong(funcionA.get("idFuncion").toString()));
                    Funcion funB = funcDAO.getByIdFuncion(Long.parseLong(funcionB.get("idFuncion").toString()));
//                    JSONObject moduloA = (JSONObject) funcionA.get("modulo");
//                    JSONObject moduloB = (JSONObject) funcionB.get("modulo");
//                    valA = (String) moduloA.get(campo);
//                    valB = (String) moduloB.get(campo);
                    valA = funA.getModulo().getDescripcion();
                    valB = funB.getModulo().getDescripcion();
                } catch (JSONException e) {
                    Utilidades.log.error("ERROR JSONException: ", e.fillInStackTrace());
                }
                if (asc) {
                    return valA.compareTo(valB);
                } else {
                    return -valA.compareTo(valB);
                }
            }
        });
        JSONArray sortedJsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject obj = jsonValues.get(i);
            try {
                JSONObject jsonFunc = (JSONObject) parser.parse(obj.get("funcion").toString());
                Funcion fun = funcDAO.getByIdFuncion(Long.parseLong(jsonFunc.get("idFuncion").toString()));
                Modulo mod = fun.getModulo();
                mod.setFechaAlta(null);
                mod.setFechaMod(null);
                mod.setFuncions(null);

                JSONObject json = (JSONObject) parser.parse(gson.toJson(mod));
                jsonFunc.put("modulo", json);
                obj.remove("funcion");
                obj.put("funcion", jsonFunc);
                sortedJsonArray.add(obj);
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            }
        }
        return sortedJsonArray;
    }

    //primer orden "FUNCIÓN"
    private JSONArray sortJsonArrayFuncion(JSONArray jsonArray, String campo, boolean asc) {
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < jsonArray.size(); i++) {
            jsonValues.add((JSONObject) jsonArray.get(i));
        }
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();
                try {
                    JSONObject funcionA = (JSONObject) a.get("funcion");
                    JSONObject funcionB = (JSONObject) b.get("funcion");
                    valA = (String) funcionA.get(campo);
                    valB = (String) funcionB.get(campo);
                } catch (JSONException e) {
                    Utilidades.log.error("ERROR JSONException: ", e.fillInStackTrace());
                }
                if (asc) {
                    return valA.compareTo(valB);
                } else {
                    return -valA.compareTo(valB);
                }
            }
        });
        JSONArray sortedJsonArray = new JSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            sortedJsonArray.add(jsonValues.get(i));
        }
        return sortJsonArrayModulo(sortedJsonArray, "descripcion", asc);
    }

    //TABLE VIEW TABLE VIEW TABLE VIEW  **************** -> -> -> -> -> -> -> ->
    private void actualizandoTablaCliente() {
        rolData = FXCollections.observableArrayList(jsonArrayRolFuncion);
        //columna Nombre ..................................................
        columnFuncionVerRol.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFuncionVerRol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonFuncion = (JSONObject) data.getValue().get("funcion");
                String descripcionF = String.valueOf(jsonFuncion.get("descripcion"));
                return new ReadOnlyStringWrapper(descripcionF.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        //columna Apellido ..................................................
        columnModuloVerRol.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnModuloVerRol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonFuncion = (JSONObject) data.getValue().get("funcion");
                JSONObject jsonModulo = (JSONObject) jsonFuncion.get("modulo");
                String descripcionM = String.valueOf(jsonModulo.get("descripcion"));
                return new ReadOnlyStringWrapper(descripcionM.toUpperCase());
            }
        });
        //columna Apellido ..................................................
        tableViewVerRol.setItems(rolData);
    }
    //TABLE VIEW TABLE VIEW TABLE VIEW  **************** -> -> -> -> -> -> -> ->

}
