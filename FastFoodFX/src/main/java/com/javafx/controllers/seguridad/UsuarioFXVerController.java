/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.util.Callback;
import org.json.JSONException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class UsuarioFXVerController extends BaseScreenController implements Initializable {

    private ObservableList<JSONObject> usuarioData;
    private JSONArray jsonArrayUsuarioRol;
    private JSONArray jsonArraySupervisor;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneUsuarioVer;
    @FXML
    private SplitPane splitPaneUsuarioVer;
    @FXML
    private AnchorPane anchorPaneHeaderUsuarioVer;
    @FXML
    private Label labelUsuarioVer;
    @FXML
    private RadioButton radioActivoUsuarioVer;
    @FXML
    private Label labelEmailUsuarioVer;
    @FXML
    private AnchorPane anchorPaneListViewUsuarioVer;
    @FXML
    private TableView<JSONObject> tableUsuarioVer;
    @FXML
    private TableColumn<JSONObject, String> columnRolVerUsuarioVer;
    @FXML
    private AnchorPane anchorPaneFooterUsuarioVer;
    @FXML
    private Label labelAltaPorUsuarioVer;
    @FXML
    private Label labelModificadoPorUsuarioVer;
    @FXML
    private Label labelFechaAltaUsuarioVer;
    @FXML
    private Label labelFechaModificacionUsuarioVer;
    @FXML
    private Label lableFechasUsuarioVer;
    @FXML
    private TextField textAltaPorUsuarioVer;
    @FXML
    private TextField textModificadoPorUsuarioVer;
    @FXML
    private TextField textFechaAltaUsuarioVer;
    @FXML
    private TextField textFechaModificacionUsuarioVer;
    @FXML
    private Button buttonVolverUsuarioVer;
    @FXML
    private Label labelFuncionarioVer;
    @FXML
    private Label labelUsuarioVerTitulo;
    @FXML
    private CheckBox checkBoxNroSup;
    @FXML
    private TextField textFieldNroSup;
    @FXML
    private TextField textFieldCodSup;
    @FXML
    private Label labelCodSup;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void botonVolverAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void anchorPaneUsuarioVerKeyReleased(KeyEvent event) {
        keyPress(event);
    }

    @FXML
    private void checkBoxNroSupAction(ActionEvent event) {
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        String usuMod = "N/A";
        String usuAlta = "N/A";
        String fechaMod = "N/A";
        String fechaAlta = "N/A";
        if (UsuarioFXController.getJsonUsuario().get("usuMod") != null) {
            usuMod = UsuarioFXController.getJsonUsuario().get("usuMod").toString();
        }
        if (UsuarioFXController.getJsonUsuario().get("usuAlta") != null) {
            usuAlta = UsuarioFXController.getJsonUsuario().get("usuAlta").toString();
        }
        if (UsuarioFXController.getJsonUsuario().get("fechaMod") != null) {
            Long timestampJSON = Long.valueOf(UsuarioFXController.getJsonUsuario().get("fechaMod").toString());
            Timestamp tsMod = new Timestamp(timestampJSON);
            fechaMod = Utilidades.getTSFormat().format(tsMod);
        }
        if (UsuarioFXController.getJsonUsuario().get("fechaAlta") != null) {
            Long timestampJSON = Long.valueOf(UsuarioFXController.getJsonUsuario().get("fechaAlta").toString());
            Timestamp tsAlta = new Timestamp(timestampJSON);
            fechaAlta = Utilidades.getTSFormat().format(tsAlta);
        }
        labelUsuarioVer.setText(UsuarioFXController.getJsonUsuario().get("nomUsuario").toString().toUpperCase());
        labelEmailUsuarioVer.setText(UsuarioFXController.getJsonUsuario().get("email").toString());
        radioActivoUsuarioVer.setSelected((boolean) UsuarioFXController.getJsonUsuario().get("activo"));
        JSONObject jsonFuncionario = (JSONObject) UsuarioFXController.getJsonUsuario().get("funcionario");
        labelFuncionarioVer.setText(jsonFuncionario.get("apellido").toString() + ", " + jsonFuncionario.get("nombre").toString()
                + "; C.I.: " + jsonFuncionario.get("ci").toString());
        radioBoxUsuActivo();
        textAltaPorUsuarioVer.setText(usuAlta.toUpperCase());
        textModificadoPorUsuarioVer.setText(usuMod.toUpperCase());
        textFechaAltaUsuarioVer.setText(fechaAlta.toUpperCase());
        textFechaModificacionUsuarioVer.setText(fechaMod.toUpperCase());
        JSONArray jsonArrayUsuarioRol = (JSONArray) UsuarioFXController.getJsonUsuario().get("usuarioRols");
        if (!jsonArrayUsuarioRol.isEmpty()) {
            this.jsonArrayUsuarioRol = sortJsonArrayRol(jsonArrayUsuarioRol, "descripcion", true);
            actualizandoTablaUsuario();
        }
        jsonArraySupervisor = (JSONArray) UsuarioFXController.getJsonUsuario().get("supervisor");
        if (jsonArraySupervisor == null) {
            jsonArraySupervisor = new JSONArray();
        }
        if (!jsonArraySupervisor.isEmpty()) {
            Object objSupervisor = jsonArraySupervisor.get(0);
            JSONObject jsonSupervisor = (JSONObject) objSupervisor;
            checkBoxNroSup.setSelected((boolean) jsonSupervisor.get("activo"));
            textFieldNroSup.setText(jsonSupervisor.get("nroSupervisor").toString());
            textFieldCodSup.setText(UtilLoaderBase.msjVuelta(jsonSupervisor.get("codSupervisor").toString()));
            checkBoxSupActivo();
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/UsuarioFX.fxml", 781, 615, "/vista/seguridad/UsuarioFXVer.fxml", 677, 518, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            navegandoAAnterior();
        }
    }

    private void radioBoxUsuActivo() {
        if (radioActivoUsuarioVer.isSelected()) {
            radioActivoUsuarioVer.setText("Activo");
            radioActivoUsuarioVer.setEffect(new DropShadow(10, Color.GREEN));
            radioActivoUsuarioVer.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            radioActivoUsuarioVer.setText("Inactivo");
            radioActivoUsuarioVer.setEffect(new DropShadow(10, Color.RED));
            radioActivoUsuarioVer.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }

    private void checkBoxSupActivo() {
        if (checkBoxNroSup.isSelected()) {
            checkBoxNroSup.setEffect(new DropShadow(10, Color.GREEN));
            checkBoxNroSup.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            checkBoxNroSup.setEffect(new DropShadow(10, Color.RED));
            checkBoxNroSup.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    //ejemplo estudiado de -> http://stackoverflow.com/questions/19543862/how-can-i-sort-a-jsonarray-in-java
    //primer orden "ROL"
    private JSONArray sortJsonArrayRol(JSONArray jsonArray, String campo, boolean asc) {
        List<JSONObject> jsonValues = new ArrayList<JSONObject>();
        for (int i = 0; i < jsonArray.size(); i++) {
            jsonValues.add((JSONObject) jsonArray.get(i));
        }
        Collections.sort(jsonValues, new Comparator<JSONObject>() {
            @Override
            public int compare(JSONObject a, JSONObject b) {
                String valA = new String();
                String valB = new String();
                try {
                    JSONObject rolA = (JSONObject) a.get("rol");
                    JSONObject rolB = (JSONObject) b.get("rol");
                    valA = (String) rolA.get(campo);
                    valB = (String) rolB.get(campo);
                } catch (JSONException e) {
                    Utilidades.log.error("ERROR JSONException: ", e.fillInStackTrace());
                }
                if (asc) {
                    return valA.compareTo(valB);
                } else {
                    return -valA.compareTo(valB);
                }
            }
        });
        JSONArray sortedJsonArray = new JSONArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            sortedJsonArray.add(jsonValues.get(i));
        }
        return sortedJsonArray;
    }

    //TABLE VIEW TABLE VIEW TABLE VIEW  **************** -> -> -> -> -> -> -> ->
    private void actualizandoTablaUsuario() {
        usuarioData = FXCollections.observableArrayList(jsonArrayUsuarioRol);
        //columna Nombre ..................................................
        columnRolVerUsuarioVer.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnRolVerUsuarioVer.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonRol = (JSONObject) data.getValue().get("rol");
                String descripcionR = String.valueOf(jsonRol.get("descripcion"));
                return new ReadOnlyStringWrapper(descripcionR.toUpperCase());
            }
        });
        //columna Nombre ..................................................
        tableUsuarioVer.setItems(usuarioData);
    }
    //TABLE VIEW TABLE VIEW TABLE VIEW  **************** -> -> -> -> -> -> -> ->
}
