/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choo Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Rol;
import com.peluqueria.dao.RolDAO;
import com.peluqueria.dao.impl.RolDAOImpl;
import com.peluqueria.dto.RolDTO;
import com.peluqueria.dto.RolFuncionDTO;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class RolFXController extends BaseScreenController implements Initializable {

    private JSONArray rolJSONArray;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private List<JSONObject> rolList;
    private ToggleGroup group;
    //PAGINATION
    private int filaIniRol, filaCantRol, filaLimitRol;
    private int pageCountRol, pageActualRol;
    private ObservableList<JSONObject> rolData;
    private RolDAO rolDAO = new RolDAOImpl();
    private TableView<JSONObject> tableRol;
    private TableColumn<JSONObject, String> columnRol;
    private TableColumn<JSONObject, CheckBox> columnActivo;
    private TableColumn<JSONObject, Boolean> columnButton;
    //PAGINATION
    private boolean alert;
    private boolean buscarTodosRol;
    private static JSONObject jsonRol = new JSONObject();

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane AnchorPaneRol;
    @FXML
    private AnchorPane AnchorPaneFiltroRol;
    @FXML
    private Label labelFIltroRol;
    @FXML
    private TextField textFiltroRol;
    @FXML
    private Group GroupRadioRol;
    @FXML
    private RadioButton radioActivoRol;
    @FXML
    private RadioButton radioInactivoRol;
    @FXML
    private RadioButton radioTodosRol;
    @FXML
    private Button buttonBuscarRol;
    @FXML
    private Button buttonTodosRol;
    @FXML
    private Pagination paginationRol;
    @FXML
    private HBox HBoxBottomRol;
    @FXML
    private Button buttonNuevoRol;
    @FXML
    private Button buttonVolverRol;
    @FXML
    private Button buttonLimpiar;
    @FXML
    private Label labelRol;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buscarRolAction(ActionEvent event) {
        buscandoRolFiltro();
    }

    @FXML
    private void buscarTodosRolAction(ActionEvent event) {
        buscandoRolTodo();
    }

    @FXML
    private void botonNuevoRolAction(ActionEvent event) {
        navegandoANuevoRol();
    }

    @FXML
    private void botonVolverRolAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void AnchorPaneRolKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        buttonGroup();
        alert = false;
        buscarTodosRol = true;
        primeraPaginacion();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoANuevoRol() {
//        if ("rol_create")) {
        this.sc.loadScreen("/vista/seguridad/RolNuevoFX.fxml", 600, 479, "/vista/seguridad/RolFX.fxml", 394, 575, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/RolFX.fxml", 394, 575, true);
//        }
    }

    private void navegandoAEditarRol() throws ParseException {
//        if ("rol_edit")) {
        this.sc.loadScreen("/vista/seguridad/RolEditarFX.fxml", 600, 479, "/vista/seguridad/RolFX.fxml", 394, 575, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/RolFX.fxml", 394, 575, true);
//        }
    }

    private void navegandoAVerRol() throws ParseException {
//        if ("rol_view") {
//            
//        }
//        
//            ) {
        this.sc.loadScreen("/vista/seguridad/RolFXVer.fxml", 600, 456, "/vista/seguridad/RolFX.fxml", 394, 575, false);
//        }else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/RolFX.fxml", 394, 575, true);
//        }
    }

    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/seguridad/RolFX.fxml", 394, 575, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            buscandoRolFiltro();
        }
        if (keyCode == event.getCode().F3) {
            buscandoRolTodo();
        }
        if (keyCode == event.getCode().F5) {
            limpiandoBusqueda();
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            navegandoANuevoRol();
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buttonGroup() {
        group = new ToggleGroup();
        radioActivoRol.setToggleGroup(group);
        radioInactivoRol.setToggleGroup(group);
        radioTodosRol.setToggleGroup(group);
    }

    private void limpiandoBusqueda() {
        textFiltroRol.setText("");
        radioTodosRol.setSelected(true);
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////ROW COUNT -> GET
    private int jsonRowCount() {
        String inputLine = "0L";
        int rowCount = 0;
        rowCount = Integer.parseInt(rolDAO.rowCount() + "");
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/count");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                rowCount = Integer.valueOf(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return rowCount;
    }
    //////ROW COUNT -> GET

    //////ROW COUNT FILTER -> GET
    private int jsonRowCountFilter(String rolFilter) {
        String inputLine;
        int rowCountFilter = 0;
        rowCountFilter = Integer.parseInt(rolDAO.rowCountFiltro(rolFilter, radioActivoRol.isSelected(), radioInactivoRol.isSelected()) + "");
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/countFiltro/"
//                    + rolFilter + "/" + radioActivoRol.isSelected() + "/" + radioInactivoRol.isSelected());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                rowCountFilter = Integer.valueOf(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return rowCountFilter;
    }
    //////ROW COUNT FILTER -> GET

    //////READ FETCH -> GET
    private List<JSONObject> jsonArrayRolFetch(int limRow, int offSet) throws ParseException {
        JSONParser parser = new JSONParser();
        rolJSONArray = new JSONArray();
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> jsonListRol = new ArrayList<>();
        List<Rol> listRol = rolDAO.listarFETCH(limRow, offSet);
        if (!listRol.isEmpty()) {
            for (Rol rol : listRol) {
//                JSONObject json = new JSONObject();
//                json.put("idRol", rol.getIdRol());
//                json.put("descripcion", rol.getDescripcion());
//                json.put("activo", rol.getActivo());
//                try {
//                    json.put("usuAlta", rol.getUsuAlta());
//                } catch (Exception e) {
//                    json.put("usuAlta", "");
//                } finally {
//                }
//                try {
//                    json.put("usuMod", rol.getUsuMod());
//                } catch (Exception e) {
//                    json.put("usuMod", "");
//                } finally {
//                }
//                try {
//                    json.put("fechaAlta", rol.getFechaAlta());
//                } catch (Exception e) {
//                    json.put("fechaAlta", "");
//                } finally {
//                }
//                try {
//                    json.put("fechaMod", rol.getFechaMod());
//                } catch (Exception e) {
//                    json.put("fechaMod", "");
//                } finally {
//                }
//                try {
//                    json.put("tipo", rol.getTipo());
//                } catch (Exception e) {
//                    json.put("tipo", "");
//                } finally {
//                }
                String fecAlta = "";
                try {
                    fecAlta = rol.getFechaAlta().toString();;
                } catch (Exception e) {
                    fecAlta = "";
                } finally {
                }
                String fecMod = "";
                try {
                    fecMod = rol.getFechaMod().toString();
                } catch (Exception e) {
                    fecMod = "";
                } finally {
                }
                RolDTO rolDTO = rol.toRolSinFechaDTO();
                rolDTO.setFechaAlta(null);
                rolDTO.setFechaMod(null);
                List<RolFuncionDTO> listRolFUnDTO = rolDTO.getRolFuncions();
                rolDTO.setRolFuncions(null);
                rolDTO.setUsuarioRols(null);
                JSONObject json = (JSONObject) parser.parse(gson.toJson(rolDTO));
                JSONArray array = new JSONArray();
                for (RolFuncionDTO rolFuncDTO : listRolFUnDTO) {
                    JSONObject jsonRolFunc = (JSONObject) parser.parse(gson.toJson(rolFuncDTO));
                    array.add(jsonRolFunc);
                }
                json.put("rolFuncions", array);
                json.put("fechaAlta", fecAlta);
                json.put("fechaMod", fecMod);
                jsonListRol.add(json);
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/fetch/" + limRowS + "/" + offSetS); // URL datos de JSON
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                rolJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object objRol : rolJSONArray) {
//                    JSONObject jsonRol = (JSONObject) objRol;
//                    jsonListRol.add(jsonRol);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return jsonListRol;
    }
    //////READ FETCH -> GET

    //////READ FETCH FILTRO -> GET
    private List<JSONObject> jsonArrayRolesFetchFiltro(int limRow, int offSet, String rolFiltro) throws ParseException {
        JSONParser parser = new JSONParser();
        rolJSONArray = new JSONArray();
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String role = "null";
        if (!rolFiltro.contentEquals("")) {
            role = rolFiltro;
        }
        List<JSONObject> rolJSONObjList = new ArrayList<>();
        List<Rol> listRol = rolDAO.listarFETCHFiltro(limRow, offSet, role, radioActivoRol.isSelected(), radioInactivoRol.isSelected());
        if (!listRol.isEmpty()) {
            for (Rol rol : listRol) {
//                JSONObject json = new JSONObject();
//                json.put("idRol", rol.getIdRol());
//                json.put("descripcion", rol.getDescripcion());
//                json.put("activo", rol.getActivo());
//                try {
//                    json.put("usuAlta", rol.getUsuAlta());
//                } catch (Exception e) {
//                    json.put("usuAlta", "");
//                } finally {
//                }
//                try {
//                    json.put("usuMod", rol.getUsuMod());
//                } catch (Exception e) {
//                    json.put("usuMod", "");
//                } finally {
//                }
//                try {
//                    json.put("fechaAlta", rol.getFechaAlta());
//                } catch (Exception e) {
//                    json.put("fechaAlta", "");
//                } finally {
//                }
//                try {
//                    json.put("fechaMod", rol.getFechaMod());
//                } catch (Exception e) {
//                    json.put("fechaMod", "");
//                } finally {
//                }
//                try {
//                    json.put("tipo", rol.getTipo());
//                } catch (Exception e) {
//                    json.put("tipo", "");
//                } finally {
//                }
                String fecAlta = rol.getFechaAlta().toString();
                String fecMod = rol.getFechaMod().toString();
                RolDTO rolDTO = rol.toRolSinFechaDTO();
                JSONObject json = (JSONObject) parser.parse(gson.toJson(rolDTO));
                json.put("fechaAlta", fecAlta);
                json.put("fechaMod", fecMod);
                rolJSONObjList.add(json);
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/fetchFiltro/" + limRowS + "/" + offSetS
//                    + "/" + rol + "/" + radioActivoRol.isSelected() + "/" + radioInactivoRol.isSelected());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                rolJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object rolObj : rolJSONArray) {
//                    JSONObject rolJSONObj = (JSONObject) rolObj;
//                    rolJSONObjList.add(rolJSONObj);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return rolJSONObjList;
    }
    //////READ FETCH FILTRO -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantRol = 10;
        filaIniRol = 0;
        pageActualRol = 0;
        buscandoRolTodo();
    }

    private void buscandoRolTodo() {
        filaLimitRol = jsonRowCount();
        buscarTodosRol = true;
        paginandoRol();
    }

    private void buscandoRolFiltro() {
        String filterParam = "null";
        if (!textFiltroRol.getText().contentEquals("")) {
            filterParam = textFiltroRol.getText();
        }
        filaLimitRol = jsonRowCountFilter(filterParam);
        buscarTodosRol = false;
        paginandoRol();
    }

    private void paginandoRol() {
        paginationRol.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                try {
                    return crearPaginacionRol(pageIndex);
                    //**************************************************************
                } catch (ParseException ex) {
                    return null;
                }
            }
        });
        paginationRol.setCurrentPageIndex(this.pageActualRol);//index inicial paginación
        paginationRol.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountRol = filaLimitRol / filaCantRol;//cantidad index paginación
        if (filaLimitRol % filaCantRol > 0) {//index EXTRA (resto de filas)***********
            pageCountRol++;
        }
        paginationRol.setPageCount(pageCountRol);//cantidad index paginación SET***
    }

    private TableView crearPaginacionRol(int pageIndex) throws ParseException {
        filaIniRol = pageIndex * filaCantRol;
        if (buscarTodosRol) {
            rolList = jsonArrayRolFetch(filaCantRol, filaIniRol);
        } else {
            String filterParam = "null";
            if (!textFiltroRol.getText().contentEquals("")) {
                filterParam = textFiltroRol.getText();
            }
            rolList = jsonArrayRolesFetchFiltro(filaCantRol, filaIniRol, filterParam);
        }
        actualizandoTablaRol();
        return tableRol;
    }

    private void actualizandoTablaRol() {
        tableRol = new TableView<JSONObject>();
        rolData = FXCollections.observableArrayList(rolList);
        //columna Descripción ..................................................
        columnRol = new TableColumn("Rol");
        columnRol.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnRol.setMinWidth(160);
        columnRol.setEditable(false);
        columnRol.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("descripcion").toString());
            }
        });
        //columna Descripción ..................................................
        //columna Activo
        columnActivo = new TableColumn("Activo");
        columnActivo.setMinWidth(80);
        columnActivo.setStyle("-fx-alignment: CENTER;");
        columnActivo.setEditable(false);
        columnActivo.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                Boolean activo = (Boolean) data.getValue().get("activo");
                CheckBox cb = new CheckBox();
                cb.setSelected(activo);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<CheckBox>(cb);
            }
        });
        //columna Activo
        //columna Acciones
        columnButton = new TableColumn("Acciones");
        columnButton.setCellFactory(new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn) {
                return new RolFXController.AddObjectsCell(tableRol);
            }
        });
        //columna Acciones
        tableRol.getColumns().addAll(columnRol, columnActivo, columnButton);
        tableRol.setItems(rolData);
    }

    //Añadir botones a cada fila************************************************
    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button editarButton = new Button("editar");
        Button verButton = new Button("ver");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            editarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(editarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    try {
//                        if ("rol_view") {
//                            
//                        }
//                        
//                            ) {
                        table.getSelectionModel().select(getTableRow().getIndex());
                        int index = table.getSelectionModel().getFocusedIndex();
                        setJsonRol(new JSONObject());
                        setJsonRol(tableRol.getItems().get(index));
                        navegandoAVerRol();
//                        }else {
//                            mensajeError("NO DISPONE DEL PERMISO NECESARIO\nPARA REALIZAR LA ACCIÓN.");
//                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
            });
            editarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            editarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
                    try {
//                        if ("rol_edit") {
//                            
//                        }
//                        
//                            ) {
                        table.getSelectionModel().select(getTableRow().getIndex());
                        int index = table.getSelectionModel().getFocusedIndex();
                        setJsonRol(new JSONObject());
                        setJsonRol(tableRol.getItems().get(index));
                        navegandoAEditarRol();
//                        }else {
//                            mensajeError("NO DISPONE DEL PERMISO NECESARIO\nPARA REALIZAR LA ACCIÓN.");
//                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    public static JSONObject getJsonRol() {
        return jsonRol;
    }

    public static void setJsonRol(JSONObject aJsonRol) {
        jsonRol = aJsonRol;
    }
}
