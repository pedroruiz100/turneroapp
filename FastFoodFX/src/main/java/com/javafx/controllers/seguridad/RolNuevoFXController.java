/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.AnimationFX;
import com.javafx.util.Identity;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Funcion;
import com.peluqueria.core.domain.Modulo;
import com.peluqueria.core.domain.Rol;
import com.peluqueria.core.domain.RolFuncion;
import com.peluqueria.dao.FuncionDAO;
import com.peluqueria.dao.ModuloDAO;
import com.peluqueria.dao.RolDAO;
import com.peluqueria.dao.RolFuncionDAO;
import com.peluqueria.dao.impl.FuncionDAOImpl;
import com.peluqueria.dao.impl.ModuloDAOImpl;
import com.peluqueria.dao.impl.RolDAOImpl;
import com.peluqueria.dao.impl.RolFuncionDAOImpl;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Point2D;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Paint;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class RolNuevoFXController extends BaseScreenController implements Initializable {

    private ObservableList<String> itemsAsignados;
    private ObservableList<String> itemsNOAsignados;
    private JSONArray moduloJSONArray;
    private HashMap<String, JSONObject> hashMapModulo;
    private RolDAO rolDAO = new RolDAOImpl();
    private ModuloDAO moduloDAO = new ModuloDAOImpl();
    private FuncionDAO funcionDAO = new FuncionDAOImpl();
    private RolFuncionDAO rolFuncionDAO = new RolFuncionDAOImpl();
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private boolean alert;
    Tooltip toolTipCombo;
    private boolean primera = true;
    private String msjError;

    //FXML FXML FXML ******************************************** -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneNuevoRol;
    @FXML
    private SplitPane splitPaneNuevoRol;
    @FXML
    private AnchorPane anchorPaneHeaderNuevoRol;
    @FXML
    private ComboBox<String> comboBoxModuloNuevoRol;
    @FXML
    private Label lableModuloNuevoRol;
    @FXML
    private Label lableDescripcionNuevoRol;
    @FXML
    private TextField textFieldDescripcionNuevoRol;
    @FXML
    private Label labelNuevoRol;
    @FXML
    private AnchorPane anchorPaneListViewNuevoRol;
    @FXML
    private ListView<String> listViewSinAsignarNuevoRol;
    @FXML
    private ListView<String> listViewAsignadoNuevoRol;
    @FXML
    private Button buttonAsignarNuevoRol;
    @FXML
    private Button buttonAsignarTodosNuevoRol;
    @FXML
    private Button buttonQuitarNuevoRol;
    @FXML
    private Button buttonQuitarTodosNuevoRol;
    @FXML
    private CheckBox checkActivoNuevoRol;
    @FXML
    private Label labelFunSin;
    @FXML
    private Label labelFunAsig;
    @FXML
    private AnchorPane anchorPaneBottomNuevoRol;
    @FXML
    private Button buttonGuardarNuevoRol;
    @FXML
    private Button buttonVolverNuevoRol;
    @FXML
    private HBox hBoxObligatorio;
    @FXML
    private Label lableCampoOblCliente1;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void comboBoxNuevoRolAction(ActionEvent event) {
        escuchandoComboBox();
    }

    @FXML
    private void botonAsignarNuevoRolAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void botonAsignarTodosNuevoRolAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void botonQuitarNuevoRolAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void botonQuitarTodosNuevoRolAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void checkBoxActivoNuevoRolAction(ActionEvent event) {
        checkBox();
    }

    @FXML
    private void botonGuardarNuevoRolAction(ActionEvent event) {
        creandoRolFuncion();
    }

    @FXML
    private void botonVolverNuevoRolAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void anchorPaneNuevoRolKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        textFieldDescripcionNuevoRol.setText("");
        textFieldDescripcionNuevoRol.requestFocus();
        msjError = "";
        if (primera) {
            tootTip();
            alert = false;
            jsonArrayModuloFetch();
            primera = false;
        }
        checkBox();
        listViewAsignadoNuevoRol.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        listViewSinAsignarNuevoRol.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        buttonAsignarNuevoRol.setDisable(true);
        buttonAsignarTodosNuevoRol.setDisable(true);
        buttonQuitarNuevoRol.setDisable(true);
        buttonQuitarTodosNuevoRol.setDisable(true);
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/RolFX.fxml", 394, 575, "/vista/seguridad/RolNuevoFX.fxml", 600, 479, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeExito(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                creandoRolFuncion();
            }
        }
    }

    private void escuchandoComboBox() {
        if (comboBoxModuloNuevoRol.getSelectionModel().getSelectedIndex() == 0) {
            buttonAsignarNuevoRol.setDisable(true);
            buttonAsignarTodosNuevoRol.setDisable(true);
            buttonQuitarNuevoRol.setDisable(true);
            buttonQuitarTodosNuevoRol.setDisable(true);
        } else {
            buttonAsignarNuevoRol.setDisable(false);
            buttonAsignarTodosNuevoRol.setDisable(false);
            buttonQuitarNuevoRol.setDisable(false);
            buttonQuitarTodosNuevoRol.setDisable(false);
        }
        JSONObject jsonModulo = hashMapModulo.get(comboBoxModuloNuevoRol.getSelectionModel().getSelectedItem());
        JSONArray jsonArrayFunciones = new JSONArray();
        List<String> listFuncionDesc = new ArrayList<>();
        if (jsonModulo != null) {
            jsonArrayFunciones = (JSONArray) jsonModulo.get("funcions");
            if (jsonArrayFunciones != null) {
                for (Object objFuncion : jsonArrayFunciones) {
                    JSONObject jsonFuncion = (JSONObject) objFuncion;
                    listFuncionDesc.add(jsonFuncion.get("descripcion").toString());
                }
            }
        }
        itemsAsignados = FXCollections.observableArrayList(new ArrayList());
        listViewAsignadoNuevoRol.setItems(itemsAsignados);
        itemsNOAsignados = FXCollections.observableArrayList(listFuncionDesc);
        listViewSinAsignarNuevoRol.setItems(itemsNOAsignados);
    }

    private void checkBox() {
        if (checkActivoNuevoRol.isSelected()) {
            checkActivoNuevoRol.setText("Activo");
            checkActivoNuevoRol.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            checkActivoNuevoRol.setText("Inactivo");
            checkActivoNuevoRol.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }

    private void tootTip() {
        toolTipCombo = new Tooltip("Puedes personalizar\ncon más módulos\nen Editar Rol.");
        toolTipCombo.setStyle("-fx-font: normal bold 12 System; "
                + "-fx-background-color: #F2F2F2; "
                + "-fx-text-fill: #39aedd;");
        comboBoxModuloNuevoRol.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Point2D p = comboBoxModuloNuevoRol.localToScreen(comboBoxModuloNuevoRol.getLayoutBounds().getMinX(), comboBoxModuloNuevoRol.getLayoutBounds().getMaxY()); //I position the tooltip at bottom right of the node (see below for explanation)
                toolTipCombo.show(comboBoxModuloNuevoRol, p.getX(), p.getY());
                toolTipCombo = AnimationFX.fadeTooltip(toolTipCombo);
            }
        });
        comboBoxModuloNuevoRol.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                toolTipCombo.hide();
            }
        });
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void asignando() {
        itemsAsignados.addAll(listViewSinAsignarNuevoRol.getSelectionModel().getSelectedItems());
        itemsNOAsignados.removeAll(listViewSinAsignarNuevoRol.getSelectionModel().getSelectedItems());
        listViewAsignadoNuevoRol.setItems(itemsAsignados);
        listViewSinAsignarNuevoRol.setItems(itemsNOAsignados);
    }

    private void asignandoTodos() {
        itemsAsignados.addAll(itemsNOAsignados);
        itemsNOAsignados.removeAll(itemsNOAsignados);
        listViewAsignadoNuevoRol.setItems(itemsAsignados);
        listViewSinAsignarNuevoRol.setItems(itemsNOAsignados);
    }

    private void quitando() {
        itemsNOAsignados.addAll(listViewAsignadoNuevoRol.getSelectionModel().getSelectedItems());
        itemsAsignados.removeAll(listViewAsignadoNuevoRol.getSelectionModel().getSelectedItems());
        listViewAsignadoNuevoRol.setItems(itemsAsignados);
        listViewSinAsignarNuevoRol.setItems(itemsNOAsignados);
    }

    private void quitandoTodos() {
        itemsNOAsignados.addAll(itemsAsignados);
        itemsAsignados.removeAll(itemsAsignados);
        listViewAsignadoNuevoRol.setItems(itemsAsignados);
        listViewSinAsignarNuevoRol.setItems(itemsNOAsignados);
    }

    private void creandoRolFuncion() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA CREAR ROL?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            if (textFieldDescripcionNuevoRol.getText().contentEquals("")) {
                alert.close();
                mensajeError("EL NOMBRE DEL ROL NO DEBE ESTAR EN NULO.");
            } else {
                JSONObject jsonRol = creandoRol();
                String cadenaFuncionAgregar = "";
                boolean primerElemento = true;
                //Se verifica la función que el usuario AGREGÓ al Rol, con lo***  
                //que hay en la BD..********************************************
                if (itemsAsignados != null && !jsonRol.isEmpty()) {
                    if (!itemsAsignados.isEmpty()) {
                        for (int i = 0; i < itemsAsignados.size(); i++) {
                            if (primerElemento) {
                                cadenaFuncionAgregar = "'" + itemsAsignados.get(i).toString() + "'";
                                primerElemento = false;
                            } else {
                                cadenaFuncionAgregar = cadenaFuncionAgregar + ", '" + itemsAsignados.get(i).toString() + "'";
                            }
                        }
                        cadenaFuncionAgregar = "(" + cadenaFuncionAgregar + ")";
                        if (createRolFuncion(jsonRol, buscarFuncion(cadenaFuncionAgregar))) {
                            alert.close();
                            mensajeExito("¡ÉXITO! SE AGREGÓ EL ROL -> " + jsonRol.get("descripcion").toString().toUpperCase() + ".");
                            cargandoInicial();
                        } else {
                            alert.close();
                            mensajeAdv("LAS FUNCIONES PERTINENTES AL ROL\n"
                                    + "NO SE AGREGARON APROPIADAMENTE\n"
                                    + "INTENTELO MÁS TARDE EN EDITAR ROL.");
                            cargandoInicial();
                        }
                    }
                } else if (!jsonRol.isEmpty()) {
                    alert.close();
                    mensajeExito("¡ÉXITO! SE AGREGÓ EL ROL -> " + jsonRol.get("descripcion").toString().toUpperCase() + ".");
                    cargandoInicial();
                } else {
                    alert.close();
                    mensajeError(msjError);
                    cargandoInicial();
                }
            }
        } else {
            alert.close();
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, MÓDULO -> GET
    private void jsonArrayModuloFetch() {
        JSONParser parser = new JSONParser();
        moduloJSONArray = new JSONArray();
        hashMapModulo = new HashMap<>();
        comboBoxModuloNuevoRol.getItems().add("N/A");
        comboBoxModuloNuevoRol.getSelectionModel().selectFirst();
        hashMapModulo.put("N/A", null);
//        moduloJSONArray = (JSONArray) parser.parse(inputLine);
        List<Modulo> list = moduloDAO.listar();
        for (Modulo modulo : list) {
            Timestamp fechaAlta = null;
            Timestamp fechaMod = null;
            try {
                fechaAlta = modulo.getFechaAlta();
            } catch (Exception e) {
                fechaAlta = null;
            } finally {
            }
            try {
                fechaMod = modulo.getFechaMod();
            } catch (Exception e) {
                fechaMod = null;
            } finally {
            }

            modulo.setFechaAlta(null);
            modulo.setFechaMod(null);
            List<Funcion> func = new ArrayList<>();
            try {
                func = modulo.getFuncions();
            } catch (Exception e) {
                func = new ArrayList<>();
            } finally {
            }
            modulo.setFuncions(null);
            try {
                JSONObject jsonModulo = (JSONObject) parser.parse(gson.toJson(modulo));
                jsonModulo.put("fechaAlta", fechaAlta);
                jsonModulo.put("fechaMod", fechaMod);
                JSONArray array = new JSONArray();
                if (func == null) {
                    func = new ArrayList<>();
                }
                if (!func.isEmpty()) {
                    for (Funcion funci : func) {
                        funci.setRolFuncions(null);
                        JSONObject jsonFunci = (JSONObject) parser.parse(gson.toJson(funci));
                        array.add(jsonFunci);
                    }
                }
                if (!array.isEmpty()) {
                    jsonModulo.put("funcions", array);
                }
                if (!hashMapModulo.containsKey(modulo.getDescripcion())) {
                    hashMapModulo.put(modulo.getDescripcion(), jsonModulo);
                    comboBoxModuloNuevoRol.getItems().add(modulo.getDescripcion());
                }
            } catch (ParseException ex) {
                System.out.println("-> " + ex.getLocalizedMessage());
                System.out.println("-> " + ex.fillInStackTrace());
            }
        }
//        try {
//            aaaaaaa URL url = new URL(Utilidades.ip + "/ServerParana/modulo");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                moduloJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object moduloObj : moduloJSONArray) {
//                    JSONObject moduloJSONObj = (JSONObject) moduloObj;
//                    if (!hashMapModulo.containsKey(moduloJSONObj.get("descripcion").toString())) {
//                        hashMapModulo.put(moduloJSONObj.get("descripcion").toString(), moduloJSONObj);
//                        comboBoxModuloNuevoRol.getItems().add(moduloJSONObj.get("descripcion").toString());
//                    }
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
    }
    //////READ, MÓDULO -> GET

    //////CREATE, ROL -> POST
    private JSONObject creandoRol() {
        JSONParser parser = new JSONParser();
        JSONObject jsonRol = new JSONObject();

        Rol rol = creandoJsonRol();
        if (rol.getIdRol() != null) {
            try {
                rol.setFechaAlta(null);
                rol.setFechaMod(null);
                rol.setRolFuncions(null);
                rol.setUsuarioRols(null);
                jsonRol = (JSONObject) parser.parse(gson.toJson(rol));
            } catch (ParseException ex) {
                Logger.getLogger(RolNuevoFXController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("POST");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonRol.toString());
//            System.out.println("ROL -->> " + jsonRol.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    jsonRol = (JSONObject) parser.parse(inputLine);
//                    if (jsonRol.get("idRol") == null) {
//                        msjError = "FALLO AL INTENTAR CREAR ROL\n-> VERIFIQUE, EL NOMBRE DEL ROL SEA ÚNICO.";
//                        jsonRol = new JSONObject();
//                    }
//                }
//                br.close();
//            } else {
//                msjError = "FALLO AL INTENTAR CREAR ROL\n-> PROBLEMAS CON EL SERVIDOR (" + HttpResult + ").";
//                jsonRol = new JSONObject();
//            }
//        } catch (IOException e) {
//            jsonRol = new JSONObject();
//            msjError = "FALLO AL INTENTAR CREAR ROL\n-> VERIFIQUE, CONEXIÓN.";
//            System.out.println(e.getLocalizedMessage());
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException ex) {
//            jsonRol = new JSONObject();
//            msjError = "FALLO AL INTENTAR CREAR ROL\n-> VERIFIQUE, CONEXIÓN.";
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }
        return jsonRol;
    }
    //////CREATE, ROL -> POST

    ///////READ, FUNCIÓN -> GET
    private JSONArray buscarFuncion(String cIN) {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncion = new JSONArray();
//        String cadenaIN = UtilLoaderBase.msjIda(cIN);
        List<Funcion> func = funcionDAO.buscarFuncion(cIN);
        if (!func.isEmpty()) {
            for (Funcion funcion : func) {
                funcion.setRolFuncions(null);
                Modulo mod = funcion.getModulo();
                Modulo modu = new Modulo();
                modu.setIdModulo(mod.getIdModulo());
                modu.setFechaAlta(null);
                modu.setFechaMod(null);
                funcion.setModulo(modu);
                try {
                    JSONObject jsonFunc = (JSONObject) parser.parse(gson.toJson(funcion));
                    jsonArrayFuncion.add(jsonFunc);
                } catch (ParseException ex) {
                    Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    Utilidades.log.error("ERROR Exception: " + ex.getMessage());
                }
            }

        }

//        try {
//
//            URL url = new URL(Utilidades.ip + "/ServerParana/funcion/bf/" + cadenaIN + "");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonArrayFuncion = (JSONArray) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return jsonArrayFuncion;
    }
    //////READ, FUNCIÓN -> GET

    //////CREATE, ROL - FUNCIÓN -> CREATE
    private boolean createRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
        String inputLine;
        JSONParser parser = new JSONParser();
        boolean exito = false;
        exito = creandoJsonRolFuncion(jsonRol, jsonArrayFuncion);
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rolFuncion/c");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("POST");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonArrayRolFuncion.toString());
//            System.out.println("ROL_FUNCION -->> " + jsonArrayRolFuncion.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    exito = (boolean) parser.parse(inputLine);
//                }
//                br.close();
//            }
//        } catch (IOException | ParseException ex) {
//            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
//        }
        return exito;
    }
    //////CREATE, ROL - FUNCIÓN -> CREATE
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON CREANDO ROL
    private Rol creandoJsonRol() {
        Rol rol = new Rol();
        rol.setActivo(checkActivoNuevoRol.isSelected());
        rol.setDescripcion(textFieldDescripcionNuevoRol.getText().toUpperCase());
        rol.setTipo("");
        rol.setUsuMod(Identity.getNomFun());
        rol.setUsuAlta(Identity.getNomFun());
        rol.setFechaMod(null);
        rol.setFechaAlta(null);

//        JSONObject jsonRol = new JSONObject();
//        //**********************************************************************
//        jsonRol.put("activo",);
//        jsonRol.put("descripcion",);
//        jsonRol.put("tipo", "");
//        //**********************************************************************
//        jsonRol.put("usuMod", Identity.getNomFun());
//        jsonRol.put("fechaMod", null);//en el servidor
//        jsonRol.put("usuAlta", Identity.getNomFun());
//        jsonRol.put("fechaAlta", null);
//        //**********************************************************************
//        return jsonRol;
        return rolDAO.insertarObtenerObjeto(rol);
    }
//    private JSONObject creandoJsonRol() {
//        JSONObject jsonRol = new JSONObject();
//        //**********************************************************************
//        jsonRol.put("activo", checkActivoNuevoRol.isSelected());
//        jsonRol.put("descripcion", textFieldDescripcionNuevoRol.getText().toUpperCase());
//        jsonRol.put("tipo", "");
//        //**********************************************************************
//        jsonRol.put("usuMod", Identity.getNomFun());
//        jsonRol.put("fechaMod", null);//en el servidor
//        jsonRol.put("usuAlta", Identity.getNomFun());
//        jsonRol.put("fechaAlta", null);
//        //**********************************************************************
//        return jsonRol;
//    }
    //JSON CREANDO ROL

    //JSON CREANDO ROL - FUNCIÓN
    private boolean creandoJsonRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
        List<RolFuncion> listRolFun = new ArrayList<>();
        boolean val = false;
        for (Object objFuncion : jsonArrayFuncion) {
            JSONObject jsonFuncion = (JSONObject) objFuncion;
            RolFuncion rf = new RolFuncion();

            Rol rol = new Rol();
            rol.setIdRol(Long.parseLong(jsonRol.get("idRol").toString()));

            Funcion func = new Funcion();
            func.setIdFuncion(Long.parseLong(jsonFuncion.get("idFuncion").toString()));

            rf.setRol(rol);
            rf.setFuncion(func);
            rf.setFechaMod(null);
            rf.setFechaAlta(null);
            rf.setUsuMod(Identity.getNomFun());
            rf.setUsuAlta(Identity.getNomFun());

            val = rolFuncionDAO.crearDeUno(rf);

//            JSONObject jsonFuncion = (JSONObject) objFuncion;
//            JSONObject jsonRolFuncion = new JSONObject();
//            //******************************************************************
//            jsonRolFuncion.put("rol", jsonRol);
//            jsonRolFuncion.put("funcion", jsonFuncion);
//            //******************************************************************
//            jsonRolFuncion.put("usuMod", Identity.getNomFun());
//            jsonRolFuncion.put("fechaMod", null);//en el servidor
//            jsonRolFuncion.put("usuAlta", Identity.getNomFun());
//            jsonRolFuncion.put("fechaAlta", null);
//            //******************************************************************
//            jsonArrayrolFuncion.add(jsonRolFuncion);
        }
        return val;
    }
//    private JSONArray creandoJsonRolFuncion(JSONObject jsonRol, JSONArray jsonArrayFuncion) {
//        JSONArray jsonArrayrolFuncion = new JSONArray();
//        for (Object objFuncion : jsonArrayFuncion) {
//            JSONObject jsonFuncion = (JSONObject) objFuncion;
//            JSONObject jsonRolFuncion = new JSONObject();
//            //******************************************************************
//            jsonRolFuncion.put("rol", jsonRol);
//            jsonRolFuncion.put("funcion", jsonFuncion);
//            //******************************************************************
//            jsonRolFuncion.put("usuMod", Identity.getNomFun());
//            jsonRolFuncion.put("fechaMod", null);//en el servidor
//            jsonRolFuncion.put("usuAlta", Identity.getNomFun());
//            jsonRolFuncion.put("fechaAlta", null);
//            //******************************************************************
//            jsonArrayrolFuncion.add(jsonRolFuncion);
//        }
//        return jsonArrayrolFuncion;
//    }
    //JSON CREANDO ROL - FUNCIÓN
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
}
