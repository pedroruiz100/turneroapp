/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.Identity;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.UsuarioDAO;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.Pagination;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class UsuarioFXController extends BaseScreenController implements Initializable {

    private JSONArray usuarioJSONArray;
    private List<JSONObject> usuarioList;
    private ToggleGroup group;

    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    SupervisorDAO supervisorDAO;

    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    //PAGINATION
    private int filaIniUsuario, filaCantUsuario, filaLimitUsuario;
    private int pageCountUsuario, pageActualUsuario;
    private ObservableList<JSONObject> usuarioData;
    private TableView<JSONObject> tableUsuario;
    private TableColumn<JSONObject, String> columnUsuario;
    private TableColumn<JSONObject, String> columnFuncionario;
    private TableColumn<JSONObject, String> columnFuncionarioCI;
    private TableColumn<JSONObject, CheckBox> columnSupervisor;
    private TableColumn<JSONObject, CheckBox> columnActivo;
    private TableColumn<JSONObject, Boolean> columnButton;
    //PAGINATION
    private boolean alert;
    private boolean buscarTodosUsuario;
    private static JSONObject jsonUsuario = new JSONObject();

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneUsuario;
    @FXML
    private AnchorPane anchorPaneFiltroUsuario;
    @FXML
    private Label labelFiltroUsuario;
    @FXML
    private TextField textFiltroUsuario;
    @FXML
    private Group GroupRadioUsuario;
    @FXML
    private RadioButton radioTodosUsuario;
    @FXML
    private RadioButton radioActivoUsuario;
    @FXML
    private RadioButton radioInactivoUsuario;
    @FXML
    private Button buttonBuscarUsuario;
    @FXML
    private Button buttonTodosUsuario;
    @FXML
    private Pagination paginationUsuario;
    @FXML
    private HBox hboxNuevoUsuario;
    @FXML
    private Button buttonNuevoUsuario;
    @FXML
    private Button buttonVolverUsuario;
    @FXML
    private TextField textFiltroFuncCI;
    @FXML
    private Label labelUsuario;
    @FXML
    private Label labelCIFunc;
    @FXML
    private Button buttonLimpiar;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial();
    }

    @FXML
    private void buscarUsuarioAction(ActionEvent event) {
        buscandoUsuarioFiltro();
    }

    @FXML
    private void buscarTodosUsuarioAction(ActionEvent event) {
        buscandoUsuarioTodo();
    }

    @FXML
    private void botonNuevoUsuarioAction(ActionEvent event) {
        navegandoANuevoUsuario();
    }

    @FXML
    private void botonVolverUsuarioAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void buttonLimpiarAction(ActionEvent event) {
        limpiandoBusqueda();
    }

    @FXML
    private void anchorPaneUsuarioKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial() {
        buttonGroup();
        alert = false;
        buscarTodosUsuario = true;
        primeraPaginacion();
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoANuevoUsuario() {
//        if ("user_create")) {
        this.sc.loadScreen("/vista/seguridad/UsuarioNuevoFX.fxml", 720, 738, "/vista/seguridad/UsuarioFX.fxml", 781, 615, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/UsuarioFX.fxml", 781, 615, true);
//        }
    }

    private void navegandoAEditarUsuario() {
//        if ("user_edit")) {
        this.sc.loadScreen("/vista/seguridad/UsuarioFXEditar.fxml", 677, 518, "/vista/seguridad/UsuarioFX.fxml", 781, 615, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/UsuarioFX.fxml", 781, 615, true);
//        }
    }

    private void navegandoAVerUsuario() {
//        if ("user_view")) {
        this.sc.loadScreen("/vista/seguridad/UsuarioFXVer.fxml", 677, 518, "/vista/seguridad/UsuarioFX.fxml", 781, 615, false);
//        } else {
//            this.sc.loadScreen("/vista/util/AccesoAvisoFXML.fxml", 269, 138, "/vista/seguridad/UsuarioFX.fxml", 781, 615, true);
//        }
    }

    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/login/menuEsteticaFXML.fxml", 540, 359, "/vista/seguridad/UsuarioFX.fxml", 781, 615, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F1) {
            if (alert) {
                alert = false;
            } else {
                buscandoUsuarioFiltro();
            }
        }
        if (keyCode == event.getCode().F3) {
            if (alert) {
                alert = false;
            } else {
                buscandoUsuarioTodo();
            }
        }
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                limpiandoBusqueda();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                navegandoANuevoUsuario();
            }
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void buttonGroup() {
        group = new ToggleGroup();
        radioActivoUsuario.setToggleGroup(group);
        radioInactivoUsuario.setToggleGroup(group);
        radioTodosUsuario.setToggleGroup(group);
    }

    private void limpiandoBusqueda() {
        textFiltroUsuario.setText("");
        textFiltroFuncCI.setText("");
        radioTodosUsuario.setSelected(true);
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////ROW COUNT -> GET
    private int jsonRowCount() {
        String inputLine = "0L";
        int rowCount = 0;
        rowCount = Integer.parseInt(usuarioDAO.rowCount() + "");
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/count");
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                rowCount = Integer.valueOf(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return rowCount;
    }
    //////ROW COUNT -> GET

    //////ROW COUNT FILTER -> GET
    private int jsonRowCountFilter(String nom, String cif) {
        String inputLine;
        int rowCountFilter = 0;
        rowCountFilter = Integer.parseInt(usuarioDAO.rowCountFiltro(nom, cif, radioActivoUsuario.isSelected(), radioInactivoUsuario.isSelected()) + "");
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/countFiltro/"
//                    + nom + "/" + cif + "/" + radioActivoUsuario.isSelected() + "/" + radioInactivoUsuario.isSelected());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            while ((inputLine = br.readLine()) != null) {
//                rowCountFilter = Integer.valueOf(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return rowCountFilter;
    }
    //////ROW COUNT FILTER -> GET

    //////READ FETCH -> GET
    private List<JSONObject> jsonArrayUsuarioFetch(int limRow, int offSet) {
        JSONParser parser = new JSONParser();
        usuarioJSONArray = new JSONArray();
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        List<JSONObject> usuarioJSONObjList = new ArrayList<>();
        List<Usuario> usuList = usuarioDAO.listarFETCH(limRow, offSet);
        for (Usuario usu : usuList) {
            usu.setFechaAlta(null);
            usu.setFechaMod(null);
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(usu.toUsuarioDTO()));
                Supervisor supervisor = supervisorDAO.buscarIdUsuario(usu.getIdUsuario());
                JSONArray jsonUsuario = new JSONArray();
                if (supervisor != null) {
                    supervisor.setFechaAlta(null);
                    supervisor.setFechaMod(null);
                    JSONObject objUsu = (JSONObject) parser.parse(gson.toJson(supervisor.toSupervisorSinUsuarioDTO()));
                    jsonUsuario.add(objUsu);
                    obj.put("supervisor", jsonUsuario);
                }
                usuarioJSONObjList.add(obj);
                usuarioJSONArray.add(obj);
            } catch (ParseException ex) {
                Logger.getLogger(UsuarioFXController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/fetch/" + limRowS + "/" + offSetS); // URL datos de JSON
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                usuarioJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object objUsuario : usuarioJSONArray) {
//                    JSONObject jsonUsuario = (JSONObject) objUsuario;
//                    usuarioJSONObjList.add(jsonUsuario);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return usuarioJSONObjList;
    }
    //////READ FETCH -> GET

    //////READ FETCH FILTRO -> GET
    private List<JSONObject> jsonArrayUsuariosFetchFiltro(int limRow, int offSet, String usuarioFiltro, String funcCI) {
        JSONParser parser = new JSONParser();
        usuarioJSONArray = new JSONArray();
        String limRowS = String.valueOf(limRow);
        String offSetS = String.valueOf(offSet);
        String usuario = "null";
        String funcionarioCI = "null";
        if (!usuarioFiltro.contentEquals("")) {
            usuario = usuarioFiltro;
        }
        if (!funcCI.contentEquals("")) {
            funcionarioCI = funcCI;
        }
        List<JSONObject> listJsonUsuario = new ArrayList<>();
        List<Usuario> listUsu = usuarioDAO.listarFETCHFiltro(limRow, offSet, usuario, funcCI, radioActivoUsuario.isSelected(), radioInactivoUsuario.isSelected());
//        usuarioJSONArray = (JSONArray) parser.parse(inputLine);
        for (Usuario usu : listUsu) {
//            JSONObject jsonUsuario = (JSONObject) objUsuario;
//            listJsonUsuario.add(jsonUsuario);
            usu.setFechaAlta(null);
            usu.setFechaMod(null);
            try {
                JSONObject obj = (JSONObject) parser.parse(gson.toJson(usu.toUsuarioDTO()));
                Supervisor supervisor = supervisorDAO.buscarIdUsuario(usu.getIdUsuario());
                JSONArray jsonUsuario = new JSONArray();
                if (supervisor != null) {
                    supervisor.setFechaAlta(null);
                    supervisor.setFechaMod(null);
                    JSONObject objUsu = (JSONObject) parser.parse(gson.toJson(supervisor.toSupervisorSinUsuarioDTO()));
                    jsonUsuario.add(objUsu);
                    obj.put("supervisor", jsonUsuario);
                }
                listJsonUsuario.add(obj);
                usuarioJSONArray.add(obj);
            } catch (ParseException ex) {
                System.out.println("-->> " + ex.getLocalizedMessage());
                System.out.println("-->> " + ex.fillInStackTrace());
            } finally {
            }
        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/fetchFiltro/" + limRowS + "/" + offSetS
//                    + "/" + usuario + "/" + funcionarioCI + "/" + radioActivoUsuario.isSelected() + "/" + radioInactivoUsuario.isSelected());
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                usuarioJSONArray = (JSONArray) parser.parse(inputLine);
//                for (Object objUsuario : usuarioJSONArray) {
//                    JSONObject jsonUsuario = (JSONObject) objUsuario;
//                    listJsonUsuario.add(jsonUsuario);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return listJsonUsuario;
    }
    //////READ FETCH FILTRO -> GET
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->
    private void primeraPaginacion() {
        filaCantUsuario = 10;
        filaIniUsuario = 0;
        pageActualUsuario = 0;
        buscandoUsuarioTodo();
    }

    private void buscandoUsuarioTodo() {
        filaLimitUsuario = jsonRowCount();
        buscarTodosUsuario = true;
        paginandoUsuario();
    }

    private void buscandoUsuarioFiltro() {
        String filterParam = "null";
        String filterParam2 = "null";
        if (!textFiltroUsuario.getText().contentEquals("")) {
            filterParam = textFiltroUsuario.getText();
        }
        if (!textFiltroFuncCI.getText().contentEquals("")) {
            filterParam2 = textFiltroFuncCI.getText();
        }
        filaLimitUsuario = jsonRowCountFilter(filterParam, filterParam2);
        buscarTodosUsuario = false;
        paginandoUsuario();
    }

    private void paginandoUsuario() {
        paginationUsuario.setPageFactory(new Callback<Integer, Node>() {
            @Override
            public Node call(Integer pageIndex) {
                return crearPaginacionUsuario(pageIndex);
                //**************************************************************
            }
        });
        paginationUsuario.setCurrentPageIndex(this.pageActualUsuario);//index inicial paginación
        paginationUsuario.setMaxPageIndicatorCount(5);//límite paginación***********
        this.pageCountUsuario = filaLimitUsuario / filaCantUsuario;//cantidad index paginación
        if (filaLimitUsuario % filaCantUsuario > 0) {//index EXTRA (resto de filas)***********
            pageCountUsuario++;
        }
        paginationUsuario.setPageCount(pageCountUsuario);//cantidad index paginación SET***
    }

    private TableView crearPaginacionUsuario(int pageIndex) {
        filaIniUsuario = pageIndex * filaCantUsuario;
        if (buscarTodosUsuario) {
            usuarioList = jsonArrayUsuarioFetch(filaCantUsuario, filaIniUsuario);
        } else {
            String filterParam = "null";
            String filterParam2 = "null";
            if (!textFiltroUsuario.getText().contentEquals("")) {
                filterParam = textFiltroUsuario.getText();
            }
            if (!textFiltroFuncCI.getText().contentEquals("")) {
                filterParam2 = textFiltroFuncCI.getText();
            }
            usuarioList = jsonArrayUsuariosFetchFiltro(filaCantUsuario, filaIniUsuario, filterParam, filterParam2);
        }
        actualizandoTablaUsuario();
        return tableUsuario;
    }

    private void actualizandoTablaUsuario() {
        tableUsuario = new TableView<JSONObject>();
        usuarioData = FXCollections.observableArrayList(usuarioList);
        //columna Nombre usuario ..................................................
        columnUsuario = new TableColumn("Usuario");
        columnUsuario.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnUsuario.setMinWidth(120);
        columnUsuario.setEditable(false);
        columnUsuario.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                return new ReadOnlyStringWrapper(data.getValue().get("nomUsuario").toString());
            }
        });
        //columna Nombre usuario ..................................................
        //columna Funcionario ..................................................
        columnFuncionario = new TableColumn("Funcionario");
        columnFuncionario.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFuncionario.setMinWidth(240);
        columnFuncionario.setEditable(false);
        columnFuncionario.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonFuncionario = (JSONObject) data.getValue().get("funcionario");
                String nom = jsonFuncionario.get("nombre").toString().toUpperCase();
                String ape = jsonFuncionario.get("apellido").toString().toUpperCase();
                return new ReadOnlyStringWrapper(ape + ", " + nom);
            }
        });
        //columna Funcionario ..................................................
        //columna Funcionario CI ...............................................
        columnFuncionarioCI = new TableColumn("C.I.");
        columnFuncionarioCI.setStyle("-fx-alignment: CENTER-LEFT; -fx-font-weight: bold;");
        columnFuncionarioCI.setMinWidth(70);
        columnFuncionarioCI.setEditable(false);
        columnFuncionarioCI.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, String>, ObservableValue<String>>() {
            @Override
            public ObservableValue<String> call(TableColumn.CellDataFeatures<JSONObject, String> data) {
                JSONObject jsonFuncionario = (JSONObject) data.getValue().get("funcionario");
                return new ReadOnlyStringWrapper(jsonFuncionario.get("ci").toString());
            }
        });
        //columna Funcionario CI ...............................................
        //columna Supervisor
        columnSupervisor = new TableColumn("Supervisor");
        columnSupervisor.setMinWidth(100);
        columnSupervisor.setStyle("-fx-alignment: CENTER;");
        columnSupervisor.setEditable(false);
        columnSupervisor.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data) {
                JSONArray jsonArraySupervisor = new JSONArray();
                try {
                    jsonArraySupervisor = (JSONArray) data.getValue().get("supervisor");
                    if (jsonArraySupervisor == null) {
                        jsonArraySupervisor = new JSONArray();
                    }
                } catch (Exception e) {
                    jsonArraySupervisor = new JSONArray();
                } finally {
                }
                Boolean activo = false;
                if (!jsonArraySupervisor.isEmpty()) {
                    activo = true;
                }
                CheckBox cb = new CheckBox();
                cb.setSelected(activo);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<CheckBox>(cb);
            }
        }
        );
        //columna Supervisor
        //columna Activo
        columnActivo = new TableColumn("Activo");

        columnActivo.setMinWidth(
                100);
        columnActivo.setStyle(
                "-fx-alignment: CENTER;");
        columnActivo.setEditable(
                false);
        columnActivo.setCellValueFactory(
                new Callback<TableColumn.CellDataFeatures<JSONObject, CheckBox>, ObservableValue<CheckBox>>() {
            @Override
            public ObservableValue<CheckBox> call(TableColumn.CellDataFeatures<JSONObject, CheckBox> data
            ) {
                Boolean activo = (Boolean) data.getValue().get("activo");
                CheckBox cb = new CheckBox();
                cb.setSelected(activo);
                cb.setDisable(true);
                cb.setStyle("-fx-opacity: 1");
                if (cb.isSelected()) {
                    cb.setEffect(new DropShadow(10, Color.GREEN));
                } else {
                    cb.setEffect(new DropShadow(10, Color.RED));
                }
                return new SimpleObjectProperty<CheckBox>(cb);
            }
        }
        );
        //columna Activo
        //columna Acciones
        columnButton = new TableColumn("Acciones");

        columnButton.setCellFactory(
                new Callback<TableColumn<JSONObject, Boolean>, TableCell<JSONObject, Boolean>>() {
            @Override
            public TableCell<JSONObject, Boolean> call(TableColumn<JSONObject, Boolean> cabFielBooleanTableColumn
            ) {
                return new UsuarioFXController.AddObjectsCell(tableUsuario);
            }
        }
        );
        //columna Acciones
        tableUsuario.getColumns()
                .addAll(columnUsuario, columnFuncionario, columnFuncionarioCI, columnSupervisor, columnActivo, columnButton);
        tableUsuario.setItems(usuarioData);
    }
    //Añadir botones a cada fila************************************************

    private class AddObjectsCell extends TableCell<JSONObject, Boolean> {

        Button editarButton = new Button("editar");
        Button verButton = new Button("ver");
        HBox HBoxButtons = new HBox();
        DoubleProperty buttonY = new SimpleDoubleProperty();

        AddObjectsCell(TableView table) {
            HBoxButtons.alignmentProperty().set(Pos.CENTER);
            verButton.contentDisplayProperty().setValue(ContentDisplay.LEFT);
            editarButton.contentDisplayProperty().setValue(ContentDisplay.RIGHT);
            HBoxButtons.getChildren().add(verButton);
            HBoxButtons.getChildren().add(editarButton);
            HBoxButtons.setSpacing(6);
            verButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            verButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
//                    if ("user_view")) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    setJsonUsuario(new JSONObject());
                    setJsonUsuario(tableUsuario.getItems().get(index));
                    navegandoAVerUsuario();
//                    } else {
//                        mensajeError("NO DISPONE DEL PERMISO NECESARIO\nPARA REALIZAR LA ACCIÓN.");
//                    }
                }
            });
            editarButton.setOnMousePressed(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    buttonY.set(mouseEvent.getScreenY());
                }
            });
            editarButton.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent actionEvent) {
//                    if ("user_edit")) {
                    table.getSelectionModel().select(getTableRow().getIndex());
                    int index = table.getSelectionModel().getFocusedIndex();
                    setJsonUsuario(new JSONObject());
                    setJsonUsuario(tableUsuario.getItems().get(index));
                    navegandoAEditarUsuario();
//                    } else {
//                        mensajeError("NO DISPONE DEL PERMISO NECESARIO\nPARA REALIZAR LA ACCIÓN.");
//                    }
                }
            });
        }

        @Override
        protected void updateItem(Boolean item, boolean empty) {
            super.updateItem(item, empty);
            if (!empty) {
                setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
                setGraphic(HBoxButtons);
            } else {
                setGraphic(null);
            }
        }
    }
    //PAGINATION PAGINATION PAGINATION ************** -> -> -> -> -> -> -> -> ->

    public static JSONObject getJsonUsuario() {
        return jsonUsuario;
    }

    public static void setJsonUsuario(JSONObject aJsonUsuario) {
        jsonUsuario = aJsonUsuario;
    }
}
