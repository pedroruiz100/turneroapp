/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.controllers.seguridad;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.javafx.scope.ScreenScoped;
import com.javafx.screen.BaseScreenController;
import com.javafx.util.CryptoBack;
import com.javafx.util.CryptoFront;
import com.javafx.util.Identity;
import com.javafx.util.PasswordGenerator;
import com.javafx.util.UtilLoaderBase;
import com.javafx.util.Utilidades;
import com.peluqueria.core.domain.Rol;
import com.peluqueria.core.domain.Supervisor;
import com.peluqueria.core.domain.Usuario;
import com.peluqueria.core.domain.UsuarioRol;
import com.peluqueria.dao.RolDAO;
import com.peluqueria.dao.SupervisorDAO;
import com.peluqueria.dao.UsuarioDAO;
import com.peluqueria.dao.UsuarioRolDAO;
import com.peluqueria.dto.SupervisorDTO;
import com.peluqueria.dto.UsuarioDTO;
import com.peluqueria.dto.UsuarioRolDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import org.apache.commons.validator.GenericValidator;
import org.apache.commons.validator.routines.EmailValidator;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

/**
 * FXML Controller class
 *
 * @author ExcelsisWalker
 */
@Controller
@ScreenScoped
public class UsuarioFXEditarController extends BaseScreenController implements Initializable {

    private boolean alert;
    private JSONArray jsonArrayRolAsignado;
    private JSONArray jsonArrayRolNOAsignado;
    private ObservableList<String> itemsAsignados;
    private ObservableList<String> itemsNOAsignados;
    List<String> listDescrRolAsig;
    List<String> listDescrRolNOAsig;
    @Autowired
    RolDAO rolDAO;
    @Autowired
    UsuarioDAO usuarioDAO;
    @Autowired
    SupervisorDAO supervisorDAO;
    @Autowired
    UsuarioRolDAO usuarioRolDAO;
    private Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private String idUsuario;
    private HashMap<String, JSONObject> hashMapRolAll;
    private HashMap<String, JSONObject> hashMapRolNOAsig;
    private HashMap<String, JSONObject> hashMapRolAsig;
    private String msjError;
    private JSONObject jsonUsuOrig;

    //FXML FXML FXML ********************************************* -> -> -> -> ->
    @FXML
    private AnchorPane anchorPaneUsuarioEditar;
    @FXML
    private SplitPane splitPaneUsuarioEditar;
    @FXML
    private AnchorPane anchorPaneHeaderUsuarioEditar;
    @FXML
    private Label labelUsuarioEditar;
    @FXML
    private CheckBox checkActivoUsuarioEditar;
    @FXML
    private Label labelEmailUsuarioEditar;
    @FXML
    private AnchorPane anchorPaneListViewUsuarioEditar;
    @FXML
    private ListView<String> listViewSinAsignarUsuarioEditar;
    @FXML
    private ListView<String> listViewAsignadoUsuarioEditar;
    @FXML
    private Button buttonAsignarUsuarioEditar;
    @FXML
    private Button buttonAsignarTodosUsuarioEditar;
    @FXML
    private Button buttonQuitarUsuarioEditar;
    @FXML
    private Button buttonQuitarTodosUsuarioEditar;
    @FXML
    private Label labelRolesSinAsignarUsuarioEditar;
    @FXML
    private Label labelRolesAsignadosUsuarioEditar;
    @FXML
    private AnchorPane anchorPaneBottomUsuarioEditar;
    @FXML
    private Button buttonActualizarUsuarioEditar;
    @FXML
    private Button buttonRestaurarPassUsuarioEditar;
    @FXML
    private Button buttonVolverUsuarioEditar;
    @FXML
    private Label labelFuncEditar;
    @FXML
    private CheckBox checkBoxNroSup;
    @FXML
    private TextField textFieldNroSup;
    @FXML
    private TextField textFieldCodSup;
    @FXML
    private Label labelCodSup;
    @FXML
    private Label labelUsuarioEditarTitulo;
    @FXML
    private TextField textFieldEmailUsuarioEditar;
    @FXML
    private TextField textFieldUsuarioEditar;
    @FXML
    private TextField textFieldCodSupervisorNuevo;
    @FXML
    private CheckBox checkBoxCodSuperNuevo;

    /**
     * Initializes the controller class.
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        cargandoInicial(true);
    }

    @FXML
    private void checkBoxActivoAction(ActionEvent event) {
        checkBoxUsuarioActivo();
    }

    @FXML
    private void botonAsignarUsuarioEditarAction(ActionEvent event) {
        asignando();
    }

    @FXML
    private void botonAsignarTodosUsuarioEditarAction(ActionEvent event) {
        asignandoTodos();
    }

    @FXML
    private void botonQuitarUsuarioEditarAction(ActionEvent event) {
        quitando();
    }

    @FXML
    private void botonQuitarTodosUsuarioEditarAction(ActionEvent event) {
        quitandoTodos();
    }

    @FXML
    private void botonActualizarUsuarioEditarAction(ActionEvent event) {
        guardandoEditarUsuario();
    }

    @FXML
    private void botonRestaurarPassUsuarioEditarAction(ActionEvent event) {
        restaurandoPass();
    }

    @FXML
    private void botonVolverUsuarioEditarAction(ActionEvent event) {
        navegandoAAnterior();
    }

    @FXML
    private void checkBoxNroSupAction(ActionEvent event) {
        checkBoxSupervisorExistente();
    }

    @FXML
    private void checkBoxCodSuperNuevoAction(ActionEvent event) {
        checkBoxSupervisorNuevo();
    }

    @FXML
    private void anchorPaneUsuarioEditarKeyReleased(KeyEvent event) {
        keyPress(event);
    }
    //FXML FXML FXML ******************************************** -> -> -> -> ->

    //INICIAL INICIAL INICIAL **************************************************
    private void cargandoInicial(boolean inicio) {
        msjError = "";
        jsonUsuOrig = UsuarioFXController.getJsonUsuario();
        idUsuario = UsuarioFXController.getJsonUsuario().get("idUsuario").toString();
        checkActivoUsuarioEditar.setSelected((boolean) UsuarioFXController.getJsonUsuario().get("activo"));
        textFieldUsuarioEditar.setText(UsuarioFXController.getJsonUsuario().get("nomUsuario").toString());
        textFieldEmailUsuarioEditar.setText(UsuarioFXController.getJsonUsuario().get("email").toString());
        JSONObject jsonFuncionario = (JSONObject) UsuarioFXController.getJsonUsuario().get("funcionario");
        labelFuncEditar.setText(jsonFuncionario.get("apellido").toString() + ", "
                + jsonFuncionario.get("nombre").toString() + "; C.I: "
                + jsonFuncionario.get("ci").toString());
        JSONArray jsonArraySup = (JSONArray) UsuarioFXController.getJsonUsuario().get("supervisor");
        if (jsonArraySup == null) {
            textFieldCodSupervisorNuevo.setVisible(true);
            checkBoxCodSuperNuevo.setVisible(true);
            checkBoxNroSup.setVisible(false);
            textFieldNroSup.setVisible(false);
            textFieldCodSup.setVisible(false);
            labelCodSup.setVisible(false);
            checkBoxSupervisorNuevo();
        } else {
            if (jsonArraySup.isEmpty()) {
                textFieldCodSupervisorNuevo.setVisible(true);
                checkBoxCodSuperNuevo.setVisible(true);
                checkBoxNroSup.setVisible(false);
                textFieldNroSup.setVisible(false);
                textFieldCodSup.setVisible(false);
                labelCodSup.setVisible(false);
                checkBoxSupervisorNuevo();
            } else {
                JSONObject jsonSupervisor = (JSONObject) jsonArraySup.get(0);
                checkBoxNroSup.setVisible(true);
                textFieldNroSup.setVisible(true);
                textFieldCodSup.setVisible(true);
                labelCodSup.setVisible(true);
                textFieldCodSupervisorNuevo.setVisible(false);
                checkBoxCodSuperNuevo.setVisible(false);
                checkBoxNroSup.setSelected((boolean) jsonSupervisor.get("activo"));
                textFieldNroSup.setText(jsonSupervisor.get("nroSupervisor").toString());
                textFieldCodSup.setText(UtilLoaderBase.msjVuelta(jsonSupervisor.get("codSupervisor").toString()));
                checkBoxSupervisorExistente();
            }
        }
        checkBoxUsuarioActivo();
        hashMapRolAll = new HashMap<>();
        jsonArrayRolAsignado();
        jsonArrayRolNOAsignado();
        cargandolistRoles();
        if (inicio) {
            listenTextField();
            alert = false;
        }
    }
    //INICIAL INICIAL INICIAL **************************************************

    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->
    private void navegandoAAnterior() {
        this.sc.loadScreen("/vista/seguridad/UsuarioFX.fxml", 781, 615, "/vista/seguridad/UsuarioFXEditar.fxml", 600, 479, false);
    }
    //NAVEGACIÓN FORMULARIOS ************************************ -> -> -> -> ->

    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->
    private void mensajeError(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.ERROR, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeExito(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.INFORMATION, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }

    private void mensajeAdv(String msj) {
        Alert alert2 = new Alert(Alert.AlertType.WARNING, msj, ButtonType.OK);
        this.alert = true;
        alert2.showAndWait();
        if (alert2.getResult() == ButtonType.OK) {
            alert2.close();
        }
    }
    //AVISOS AVISOS AVISOS ************************************** -> -> -> -> ->

    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->
    private void keyPress(KeyEvent event) {
        KeyCode keyCode = event.getCode();
        if (keyCode == event.getCode().F5) {
            if (alert) {
                alert = false;
            } else {
                restaurandoPass();
            }
        }
        if (keyCode == event.getCode().ESCAPE) {
            if (alert) {
                alert = false;
            } else {
                navegandoAAnterior();
            }
        }
        if (keyCode == event.getCode().ENTER) {
            if (alert) {
                alert = false;
            } else {
                guardandoEditarUsuario();
            }
        }
    }

    private void listenTextField() {
        textFieldCodSupervisorNuevo.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isLong(newValue)) {
                        Platform.runLater(() -> {
                            textFieldCodSupervisorNuevo.setText(newValue.toString());
                            textFieldCodSupervisorNuevo.positionCaret(textFieldCodSupervisorNuevo.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldCodSupervisorNuevo.setText(oldValue.toString());
                            textFieldCodSupervisorNuevo.positionCaret(textFieldCodSupervisorNuevo.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldCodSupervisorNuevo.setText(oldValue.toString());
                        textFieldCodSupervisorNuevo.positionCaret(textFieldCodSupervisorNuevo.getLength());
                    });
                }
            }
        });
        textFieldCodSup.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.contentEquals("")) {
                try {
                    if (GenericValidator.isLong(newValue)) {
                        Platform.runLater(() -> {
                            textFieldCodSup.setText(newValue.toString());
                            textFieldCodSup.positionCaret(textFieldCodSup.getLength());
                        });
                    } else {
                        Platform.runLater(() -> {
                            textFieldCodSup.setText(oldValue.toString());
                            textFieldCodSup.positionCaret(textFieldCodSup.getLength());
                        });
                    }
                } catch (NumberFormatException e) {
                    Platform.runLater(() -> {
                        textFieldCodSup.setText(oldValue.toString());
                        textFieldCodSup.positionCaret(textFieldCodSup.getLength());
                    });
                }
            }
        });
    }

    private void cargandolistRoles() {
        itemsAsignados = FXCollections.observableArrayList(listDescrRolAsig);
        listViewAsignadoUsuarioEditar.setItems(itemsAsignados);
        itemsNOAsignados = FXCollections.observableArrayList(listDescrRolNOAsig);
        listViewSinAsignarUsuarioEditar.setItems(itemsNOAsignados);
    }

    private void asignando() {
        itemsAsignados.addAll(listViewSinAsignarUsuarioEditar.getSelectionModel().getSelectedItems());
        itemsNOAsignados.removeAll(listViewSinAsignarUsuarioEditar.getSelectionModel().getSelectedItems());
        listViewAsignadoUsuarioEditar.setItems(itemsAsignados);
        listViewSinAsignarUsuarioEditar.setItems(itemsNOAsignados);
    }

    private void asignandoTodos() {
        itemsAsignados.addAll(itemsNOAsignados);
        itemsNOAsignados.removeAll(itemsNOAsignados);
        listViewAsignadoUsuarioEditar.setItems(itemsAsignados);
        listViewSinAsignarUsuarioEditar.setItems(itemsNOAsignados);
    }

    private void quitando() {
        itemsNOAsignados.addAll(listViewAsignadoUsuarioEditar.getSelectionModel().getSelectedItems());
        itemsAsignados.removeAll(listViewAsignadoUsuarioEditar.getSelectionModel().getSelectedItems());
        listViewAsignadoUsuarioEditar.setItems(itemsAsignados);
        listViewSinAsignarUsuarioEditar.setItems(itemsNOAsignados);
    }

    private void quitandoTodos() {
        itemsNOAsignados.addAll(itemsAsignados);
        itemsAsignados.removeAll(itemsAsignados);
        listViewAsignadoUsuarioEditar.setItems(itemsAsignados);
        listViewSinAsignarUsuarioEditar.setItems(itemsNOAsignados);
    }

    private void checkBoxUsuarioActivo() {
        if (checkActivoUsuarioEditar.isSelected()) {
            checkActivoUsuarioEditar.setText("Activo");
            checkActivoUsuarioEditar.setEffect(new DropShadow(10, Color.GREEN));
            checkActivoUsuarioEditar.setTextFill(Paint.valueOf("#3ba40e"));
        } else {
            checkActivoUsuarioEditar.setText("Inactivo");
            checkActivoUsuarioEditar.setEffect(new DropShadow(10, Color.RED));
            checkActivoUsuarioEditar.setTextFill(Paint.valueOf("#f10e0e"));
        }
    }

    private void checkBoxSupervisorExistente() {
        if (checkBoxNroSup.isSelected()) {
            checkBoxNroSup.setText("Supervisor Activo");
            checkBoxNroSup.setTextFill(Paint.valueOf("#3ba40e"));
            checkBoxNroSup.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            checkBoxNroSup.setText("Supervisor Inactivo");
            checkBoxNroSup.setTextFill(Paint.valueOf("black"));
            checkBoxNroSup.setEffect(null);
        }
    }

    private void checkBoxSupervisorNuevo() {
        textFieldCodSupervisorNuevo.setText("");
        if (checkBoxCodSuperNuevo.isSelected()) {
            textFieldCodSupervisorNuevo.setDisable(false);
            checkBoxCodSuperNuevo.setTextFill(Paint.valueOf("#3ba40e"));
            checkBoxCodSuperNuevo.setEffect(new DropShadow(10, Color.GREEN));
        } else {
            textFieldCodSupervisorNuevo.setDisable(true);
            checkBoxCodSuperNuevo.setTextFill(Paint.valueOf("black"));
            checkBoxCodSuperNuevo.setEffect(null);
        }
    }
    //LISTEN LISTEN LISTEN ************************************** -> -> -> -> ->

    private void restaurandoPass() {
        ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
        ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA RESTAURAR CONTRASEÑA?", ok, cancel);
        this.alert = true;
        alert.showAndWait();
        if (alert.getResult() == ok) {
            JSONObject jsonP = jsonObjectRestaurandoPass();
            if (jsonP.isEmpty()) {
                mensajeAdv("LA RESTAURACIÓN DE CONTRASEÑA NO SE REALIZÓ.\nINTENTELO DE NUEVO MÁS TARDE.");
            } else {
                mensajeExito("¡ÉXITO!\n\n" + "\t\t\t\t\t-> " + (jsonP.get("contrasenha").toString()) + " <-\n"
                        + "NO OLVIDES ANOTAR, Y LUEGO CAMBIAR TU CONTRASEÑA.");
//                mensajeExito("¡ÉXITO!\n\n" + "\t\t\t\t\t-> " + UtilLoaderBase.msjVuelta(jsonP.get("contrasenha").toString()) + " <-\n"
//                        + "NO OLVIDES ANOTAR, Y LUEGO CAMBIAR TU CONTRASEÑA.");
            }
        } else {
            alert.close();
        }
    }

    private void guardandoEditarUsuario() {
        if (!textFieldUsuarioEditar.getText().contentEquals("")) {
            if (!textFieldEmailUsuarioEditar.getText().contentEquals("")) {
                if (EmailValidator.getInstance().isValid(textFieldEmailUsuarioEditar.getText())) {
                    if (checkBoxCodSuperNuevo.isSelected()) {
                        if (textFieldCodSupervisorNuevo.getText().contentEquals("")) {
                            mensajeError("EL CÓDIGO DEL SUPERVISOR NO DEBE ESTAR EN NULO.");
                        } else {
                            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA EDITAR USUARIO?", ok, cancel);
                            this.alert = true;
                            alert.showAndWait();
                            if (alert.getResult() == ok) {
                                JSONObject jsonUsuario = editandoUsuario();
                                if (jsonUsuario.isEmpty()) {
                                    alert.close();
                                    UsuarioFXController.setJsonUsuario(null);
                                    UsuarioFXController.setJsonUsuario(jsonUsuOrig);
                                    mensajeError(msjError);
                                } else {
                                    UsuarioFXController.setJsonUsuario(null);
                                    UsuarioFXController.setJsonUsuario(jsonUsuario);
                                    alert.close();
                                    mensajeExito("\t\t¡ÉXITO! USUARIO ACTUALIZADO.\n\t\t\t\t\t-> "
                                            + jsonUsuario.get("nomUsuario").toString().toUpperCase() + " <-");
                                }
                                cargandoInicial(false);
                            } else {
                                alert.close();
                            }
                        }
                    } else {
                        if (checkBoxNroSup.isVisible()) {
                            if (!textFieldCodSup.getText().contentEquals("")) {
                                ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                                ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA EDITAR USUARIO?", ok, cancel);
                                this.alert = true;
                                alert.showAndWait();
                                if (alert.getResult() == ok) {
                                    JSONObject jsonUsuario = editandoUsuario();
                                    if (jsonUsuario.isEmpty()) {
                                        alert.close();
                                        UsuarioFXController.setJsonUsuario(null);
                                        UsuarioFXController.setJsonUsuario(jsonUsuOrig);
                                        mensajeError(msjError);
                                    } else {
                                        UsuarioFXController.setJsonUsuario(null);
                                        UsuarioFXController.setJsonUsuario(jsonUsuario);
                                        alert.close();
                                        mensajeExito("\t\t¡ÉXITO! USUARIO ACTUALIZADO.\n\t\t\t\t\t"
                                                + "-> " + jsonUsuario.get("nomUsuario").toString().toUpperCase() + " <-");
                                    }
                                    cargandoInicial(false);
                                } else {
                                    alert.close();
                                }
                            } else {
                                mensajeError("EL CÓDIGO SUPERVISOR NO DEBE ESTAR EN NULO.");
                            }
                        } else {
                            ButtonType ok = new ButtonType("Aceptar (Enter)", ButtonBar.ButtonData.OK_DONE);
                            ButtonType cancel = new ButtonType("Cancelar (Esc)", ButtonBar.ButtonData.CANCEL_CLOSE);
                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "¿DESEA EDITAR USUARIO?", ok, cancel);
                            this.alert = true;
                            alert.showAndWait();
                            if (alert.getResult() == ok) {
                                JSONObject jsonUsuario = editandoUsuario();
                                if (jsonUsuario.isEmpty()) {
                                    alert.close();
                                    UsuarioFXController.setJsonUsuario(null);
                                    UsuarioFXController.setJsonUsuario(jsonUsuOrig);
                                    mensajeError(msjError);
                                } else {
                                    UsuarioFXController.setJsonUsuario(null);
                                    UsuarioFXController.setJsonUsuario(jsonUsuario);
                                    alert.close();
                                    mensajeExito("\t\t¡ÉXITO! USUARIO ACTUALIZADO.\n\t\t\t\t\t"
                                            + "-> " + jsonUsuario.get("nomUsuario").toString().toUpperCase() + " <-");
                                }
                                cargandoInicial(false);
                            } else {
                                alert.close();
                            }
                        }
                    }
                } else {
                    mensajeError("EL E-MAIL ASIGNADO NO ES VÁLIDO, VERIFIQUE.");
                }
            } else {
                mensajeError("NO OLVIDES ASIGNAR UN E-MAIL AL USUARIO.");
            }
        } else {
            mensajeError("NO OLVIDES ASIGNAR UN NOMBRE AL USUARIO.");
        }
    }

    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, ROL ASIGNADO -> GET
    private void jsonArrayRolAsignado() {
        JSONParser parser = new JSONParser();
        jsonArrayRolAsignado = new JSONArray();
        hashMapRolAsig = new HashMap<>();
        listDescrRolAsig = new ArrayList<>();
//        jsonArrayRolAsignado = (JSONArray) parser.parse(inputLine);
        List<Rol> listRol = rolDAO.buscarRolUsuarioAsignado(Long.parseLong(idUsuario));
        for (Rol rolObj : listRol) {
            rolObj.setFechaAlta(null);
            rolObj.setFechaMod(null);
            try {
                JSONObject jsonRol = (JSONObject) parser.parse(gson.toJson(rolObj.toBDRolDTO()));
                jsonArrayRolAsignado.add(jsonRol);
                listDescrRolAsig.add(jsonRol.get("descripcion").toString());
                hashMapRolAll.put(jsonRol.get("descripcion").toString(), jsonRol);
                hashMapRolAsig.put(jsonRol.get("descripcion").toString(), jsonRol);
            } catch (ParseException ex) {
                Logger.getLogger(UsuarioFXController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }
//            JSONObject jsonRol = (JSONObject) rolObj;

        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/listAsig/" + idUsuario);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonArrayRolAsignado = (JSONArray) parser.parse(inputLine);
//                for (Object rolObj : jsonArrayRolAsignado) {
//                    JSONObject jsonRol = (JSONObject) rolObj;
//                    listDescrRolAsig.add(jsonRol.get("descripcion").toString());
//                    hashMapRolAll.put(jsonRol.get("descripcion").toString(), jsonRol);
//                    hashMapRolAsig.put(jsonRol.get("descripcion").toString(), jsonRol);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
    }
    //////READ, ROL ASIGNADO -> GET

    //////READ, ROL NO ASIGNADO -> GET
    private void jsonArrayRolNOAsignado() {
        JSONParser parser = new JSONParser();
        jsonArrayRolNOAsignado = new JSONArray();
        hashMapRolNOAsig = new HashMap<>();
        listDescrRolNOAsig = new ArrayList<>();
//        jsonArrayRolNOAsignado = (JSONArray) parser.parse(inputLine);
        List<Rol> listRol = rolDAO.buscarRolUsuarioNOAsignado(Long.parseLong(idUsuario));
        for (Rol rol : listRol) {

            rol.setFechaAlta(null);
            rol.setFechaMod(null);
            try {
                JSONObject jsonRol = (JSONObject) parser.parse(gson.toJson(rol.toBDRolDTO()));
                jsonArrayRolAsignado.add(jsonRol);
                listDescrRolNOAsig.add(jsonRol.get("descripcion").toString());
                hashMapRolAll.put(jsonRol.get("descripcion").toString(), jsonRol);
                hashMapRolNOAsig.put(jsonRol.get("descripcion").toString(), jsonRol);
            } catch (ParseException ex) {
                Logger.getLogger(UsuarioFXController.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
            }

        }
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/rol/listNOAsig/" + idUsuario);
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonArrayRolNOAsignado = (JSONArray) parser.parse(inputLine);
//                for (Object rolObj : jsonArrayRolNOAsignado) {
//                    JSONObject jsonRol = (JSONObject) rolObj;
//                    listDescrRolNOAsig.add(jsonRol.get("descripcion").toString());
//                    hashMapRolAll.put(jsonRol.get("descripcion").toString(), jsonRol);
//                    hashMapRolNOAsig.put(jsonRol.get("descripcion").toString(), jsonRol);
//                }
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
    }
    //////READ, ROL NO ASIGNADO -> GET

    //////READ, RESTAURANDO PASS -> GET
    private JSONObject jsonObjectRestaurandoPass() {
        JSONParser parser = new JSONParser();
        JSONObject jsonUserPC = new JSONObject();
//        jsonUserPC = (JSONObject) parser.parse(inputLine);

//        metodoService();
//        usuarioDAO.restaurandoPass(UtilLoaderBase.msjIda(idUsuario), UtilLoaderBase.msjIda(Identity.getNomFun()));
//        try {
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/rcont/"
//                    + UtilLoaderBase.msjIda(idUsuario) + "/" + UtilLoaderBase.msjIda(Identity.getNomFun()));
//            URLConnection uc = url.openConnection();
//            BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
//            String inputLine;
//            while ((inputLine = br.readLine()) != null) {
//                jsonUserPC = (JSONObject) parser.parse(inputLine);
//            }
//            br.close();
//        } catch (FileNotFoundException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (IOException | ParseException e) {
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        return metodoService();
    }
    //////READ, RESTAURANDO PASS -> GET

    //////EDIT, USUARIO -> PUT
    private JSONObject editandoUsuario() {
        String inputLine;
        JSONParser parser = new JSONParser();
        JSONObject jsonUsuario = new JSONObject();
//        editandoJsonUsuario();
        UsuarioDTO cliDTO = gson.fromJson(editandoJsonUsuario().toString(), UsuarioDTO.class);
        cliDTO = actualizarDatos(cliDTO);
//        UsuarioDTO usuDTO = consultarMetodoService(cliDTO);
//        try {
//            jsonUsuario = editandoJsonUsuario();
//            URL url = new URL(Utilidades.ip + "/ServerParana/usuario/actseg");
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setDoInput(true);
//            conn.setRequestProperty("Content-Type", "application/json");
//            conn.setRequestProperty("Accept", "application/json");
//            conn.setRequestMethod("PUT");
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
//            wr.write(jsonUsuario.toString());
//            wr.flush();
//            int HttpResult = conn.getResponseCode();
//            if (HttpResult == HttpURLConnection.HTTP_OK) {
//                BufferedReader br = new BufferedReader(
//                        new InputStreamReader(conn.getInputStream(), "utf-8"));
//                while ((inputLine = br.readLine()) != null) {
//                    jsonUsuario = (JSONObject) parser.parse(inputLine);
//                    if (jsonUsuario.get("idUsuario") == null) {
//                        msjError = "FALLO AL INTENTAR EDITAR USUARIO\n-> VERIFIQUE, QUE LOS CAMPOS OBLIGATORIOS SEAN ÚNICOS.";
//                        jsonUsuario = new JSONObject();
//                    }
//                }
//                br.close();
//            } else {
//                msjError = "FALLO AL INTENTAR EDITAR USUARIO\n-> PROBLEMAS CON EL SERVIDOR (" + HttpResult + ").";
//                jsonUsuario = new JSONObject();
//            }
//        } catch (IOException e) {
//            jsonUsuario = new JSONObject();
//            msjError = "FALLO AL INTENTAR EDITAR USUARIO\n-> VERIFIQUE, CONEXIÓN.";
//            System.out.println(e.getLocalizedMessage());
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        } catch (ParseException e) {
//            jsonUsuario = new JSONObject();
//            msjError = "NO SE PUDO EDITAR USUARIO\nINTENTELO MÁS TARDE.";
//            Utilidades.log.error("ERROR Exception: ", e.fillInStackTrace());
//        }
        String obj = gson.toJson(cliDTO);
        JSONObject json;
        try {
            json = (JSONObject) parser.parse(obj);
            return json;
        } catch (ParseException ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
            return new JSONObject();
        }
    }
    //////EDIT, USUARIO -> PUT
    //BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->
    //JSON EDITANDO USUARIO
    private JSONObject editandoJsonUsuario() {
        JSONObject jsonUsuario = new JSONObject();
        //**********************************************************************
        jsonUsuario.put("idUsuario", UsuarioFXController.getJsonUsuario().get("idUsuario"));
        jsonUsuario.put("generico", UsuarioFXController.getJsonUsuario().get("generico"));
        jsonUsuario.put("nomUsuario", textFieldUsuarioEditar.getText().toUpperCase());
        jsonUsuario.put("activo", checkActivoUsuarioEditar.isSelected());
        jsonUsuario.put("contrasenha", UsuarioFXController.getJsonUsuario().get("contrasenha").toString());
        jsonUsuario.put("email", textFieldEmailUsuarioEditar.getText());
        jsonUsuario.put("funcionario", UsuarioFXController.getJsonUsuario().get("funcionario"));
        if (!listViewAsignadoUsuarioEditar.getItems().isEmpty()) {
            JSONArray usuarioRols = new JSONArray();
            for (int i = 0; i < listViewAsignadoUsuarioEditar.getItems().size(); i++) {
                JSONObject jsonUsuarioRol = new JSONObject();
                jsonUsuarioRol.put("rol", hashMapRolAll.get(listViewAsignadoUsuarioEditar.getItems().get(i)));
                jsonUsuarioRol.put("usuario", null);
                //**********************************************************************
                jsonUsuarioRol.put("usuMod", Identity.getNomFun());
                jsonUsuarioRol.put("fechaMod", null);//en el servidor
                jsonUsuarioRol.put("usuAlta", Identity.getNomFun());
                jsonUsuarioRol.put("fechaAlta", null);
                //**********************************************************************
                usuarioRols.add(jsonUsuarioRol);
            }
            jsonUsuario.put("usuarioRols", usuarioRols);
        } else {
            jsonUsuario.put("usuarioRols", null);
        }
        if (checkBoxNroSup.isVisible()) {//ya existe un código supervisor...
            JSONArray jsonArraySupervisorOld = (JSONArray) UsuarioFXController.getJsonUsuario().get("supervisor");
            JSONObject jsonSup = (JSONObject) jsonArraySupervisorOld.get(0);
            jsonSup.put("activo", checkBoxNroSup.isSelected());
            jsonSup.put("codSupervisor", UtilLoaderBase.msjIda(textFieldCodSup.getText()));
            //**********************************************************************
            jsonSup.put("usuMod", Identity.getNomFun());
            jsonSup.put("fechaMod", null);//en el servidor
            jsonSup.put("usuAlta", jsonSup.get("usuAlta"));
//            jsonSup.put("fechaAlta", jsonSup.get("fechaAlta"));
            jsonSup.put("fechaAlta", null);
            //**********************************************************************
            JSONArray jsonArraySupervisorNew = new JSONArray();
            jsonArraySupervisorNew.add(jsonSup);
            jsonUsuario.put("supervisor", jsonArraySupervisorNew);
        } else if (checkBoxCodSuperNuevo.isSelected() && textFieldCodSupervisorNuevo.getText().length() != 0) {
            JSONObject jsonSupervisor = new JSONObject();
            jsonSupervisor.put("activo", true);//valor default al crear...
            jsonSupervisor.put("codSupervisor", UtilLoaderBase.msjIda(textFieldCodSupervisorNuevo.getText()));
            jsonSupervisor.put("nroSupervisor", 0);
            jsonSupervisor.put("usuario", null);
            //falta del lado server...
            //**********************************************************************
            jsonSupervisor.put("usuMod", Identity.getNomFun());
            jsonSupervisor.put("fechaMod", null);//en el servidor
            jsonSupervisor.put("usuAlta", Identity.getNomFun());
            jsonSupervisor.put("fechaAlta", null);
            //**********************************************************************
            JSONArray supervisor = new JSONArray();
            supervisor.add(jsonSupervisor);
            jsonUsuario.put("supervisor", supervisor);
        } else {
            jsonUsuario.put("supervisor", null);
        }
        //**********************************************************************
        jsonUsuario.put("usuMod", Identity.getNomFun());
        jsonUsuario.put("fechaMod", null);//en el servidor
        jsonUsuario.put("usuAlta", UsuarioFXController.getJsonUsuario().get("usuAlta"));
//        jsonUsuario.put("fechaAlta", UsuarioFXController.getJsonUsuario().get("fechaAlta"));
        jsonUsuario.put("fechaAlta", null);
        //**********************************************************************
        return jsonUsuario;
    }
    //JSON EDITANDO USUARIO
    //JSON JSON JSON JSON JSON ************************* -> -> -> -> -> -> -> ->

    private JSONObject metodoService() {
        CryptoBack cb = new CryptoBack();
        CryptoFront cf = new CryptoFront();
        String p = PasswordGenerator.getPassword(6);
        UsuarioDTO newUsuarioDTO = new UsuarioDTO();
        JSONObject jsonUsu = new JSONObject();
        try {
//            long idU = Long.valueOf(Utilidades.msjVuelta(idUsuario));ORIGINAL, LO DE ABAJO ES CAMBIADO
            long idU = Long.valueOf((idUsuario));
            String usuMod = Utilidades.msjVuelta("");
            java.util.Date date = new java.util.Date();
            Timestamp tiempoActual = new Timestamp(date.getTime());
            // pass genérico false, seteado en capa DAO
            newUsuarioDTO = Usuario.toBDUsuarioSeguridadDTO(usuarioDAO.restaurandoPass(idU, cb.getHash(cf.getHash(p)), usuMod, tiempoActual));
            newUsuarioDTO.setFuncionario(null);
            newUsuarioDTO.setContrasenha(Utilidades.msjIda(p));

            jsonUsu.put("idUsuario", idU);
            jsonUsu.put("contrasenha", p);
            jsonUsu.put("fechaMod", tiempoActual.toString());
            jsonUsu.put("usuMod", usuMod);
            jsonUsu.put("resetPass", true);
            jsonUsu.put("generico", true);

//            SendLocalQueue.enviandoControlLocalRol(jsonUsu.toString(), "U", "usuario", "localhost");
        } catch (NumberFormatException | UnsupportedEncodingException | NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (Exception ex) {
            Logger.getLogger(UsuarioFXEditarController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
        return jsonUsu;
    }

    private UsuarioDTO consultarMetodoService(UsuarioDTO oldUsuarioDTO) {
        java.util.Date date = new java.util.Date();
        Timestamp tiempoActual = new Timestamp(date.getTime());
        oldUsuarioDTO.setFechaMod(tiempoActual);

        UsuarioDTO newUsuarioDTO = usuarioDAO.actualizarObtenerObjeto(Usuario.fromUsuarioFuncDTO(oldUsuarioDTO)).toUsuarioDTOSeguridadRead();

//        String jsonUsuario = Recv.objectToJson(newUsuarioDTO);
//        SendLocalQueue.enviandoControlLocalRol(jsonUsuario, "U", "usuario", "localhost");
        if (!oldUsuarioDTO.getSupervisor().isEmpty()) {
            if (oldUsuarioDTO.getSupervisor().get(0).getNroSupervisor() == 0
                    && oldUsuarioDTO.getSupervisor().get(0).getUsuario() == null) {
                // ******se le asignó código nuevo supervisor...******
                List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
                for (SupervisorDTO supervisorDTO : oldUsuarioDTO.getSupervisor()) {
                    supervisorDTO.setUsuario(newUsuarioDTO);
                    supervisorDTO.setNroSupervisor((supervisorDAO.obteniendoMaxNroSupervisor() + 1));
                    supervisorDTO.setFechaMod(tiempoActual);

                    SupervisorDTO supDTO = supervisorDAO.insertarObtenerObjeto(Supervisor.fromSupervisorAsociadoDTO(supervisorDTO)).toSupervisorDTO();
                    listSupervisorDTO.add(supDTO);

//                    String jsonSupv = Recv.objectToJson(supDTO);
//                    SendLocalQueue.enviandoControlLocalRol(jsonSupv, "I", "supervisor", "localhost");
                }
                newUsuarioDTO.setSupervisor(listSupervisorDTO);
            } else {
                // ******se le asignó código editar supervisor...******
                List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
                for (SupervisorDTO supervisorDTO : oldUsuarioDTO.getSupervisor()) {
                    supervisorDTO.setUsuario(newUsuarioDTO);
                    supervisorDTO.setFechaMod(tiempoActual);

                    SupervisorDTO supDTO = supervisorDAO.actualizarObtenerObjeto(Supervisor.fromSupervisorAsociadoDTO(supervisorDTO)).toSupervisorDTO();
                    listSupervisorDTO.add(supDTO);

//                    String jsonSupv = Recv.objectToJson(supDTO);
//                    SendLocalQueue.enviandoControlLocalRol(jsonSupv, "U", "supervisor", "localhost");
                }
                newUsuarioDTO.setSupervisor(listSupervisorDTO);
            }
        } else {
            // ******NO se le asignó ningún código supervisor...******
            newUsuarioDTO.setSupervisor(null);
        }
        // ******se actualiza de cero, todos los roles que hacen referencia a
        // usuario, de haberlos...******
        // 1) delete - usuario <-> rol

        usuarioRolDAO.delete(newUsuarioDTO.getIdUsuario());

        JSONObject jsonUs = new JSONObject();
        jsonUs.put("idUsuario", newUsuarioDTO.getIdUsuario());
//        SendLocalQueue.enviandoControlLocalRol(jsonUs + "", "D", "usuario_rol", "localhost");

        // 1) delete - usuario <-> rol
        // 2) create - usuario <-> rol
        if (oldUsuarioDTO.getUsuarioRols() != null) {
            List<UsuarioRolDTO> listUsuarioRolDTO = new ArrayList<>();
            for (UsuarioRolDTO usuarioRolDTO : oldUsuarioDTO.getUsuarioRols()) {
                usuarioRolDTO.setUsuario(newUsuarioDTO);

                UsuarioRolDTO urDTO = usuarioRolDAO.insertarObtenerObjeto(UsuarioRol.fromUsuarioRolSeguridadDTO(usuarioRolDTO)).toUsuarioRolDTO();
                listUsuarioRolDTO.add(urDTO);

//                String jsonUrDTO = Recv.objectToJson(urDTO);
//                SendLocalQueue.enviandoControlLocalRol(jsonUrDTO, "I", "usuario_rol", "localhost");
            }
            newUsuarioDTO.setUsuarioRols(listUsuarioRolDTO);
        }
        // 2) create - usuario <-> rol
        return newUsuarioDTO/* .nullAllData() */;
    }

    private UsuarioDTO actualizarDatos(UsuarioDTO oldUsuarioDTO) {
        java.util.Date date = new java.util.Date();
        Timestamp tiempoActual = new Timestamp(date.getTime());
        oldUsuarioDTO.setFechaMod(tiempoActual);
        oldUsuarioDTO.setActivo(checkActivoUsuarioEditar.isSelected());

        Usuario usu = usuarioDAO.actualizarObtenerObjeto(Usuario.fromUsuarioFuncDTO(oldUsuarioDTO));

        UsuarioDTO newUsuarioDTO = new Usuario().toUsuarioDTOSeguridadRead(usu);

//        String jsonUsuario = Recv.objectToJson(newUsuarioDTO);
//        SendLocalQueue.enviandoControlLocalRol(jsonUsuario, "U", "usuario", "localhost");
        if (!oldUsuarioDTO.getSupervisor().isEmpty()) {
            if (oldUsuarioDTO.getSupervisor().get(0).getNroSupervisor() == 0
                    && oldUsuarioDTO.getSupervisor().get(0).getUsuario() == null) {
                // ******se le asignó código nuevo supervisor...******
                List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
                for (SupervisorDTO supervisorDTO : oldUsuarioDTO.getSupervisor()) {
                    supervisorDTO.setUsuario(newUsuarioDTO);
                    supervisorDTO.setNroSupervisor((supervisorDAO.obteniendoMaxNroSupervisor() + 1));
                    supervisorDTO.setFechaMod(tiempoActual);

                    SupervisorDTO supDTO = Supervisor.toSupervisorUsuarioDTO(
                            supervisorDAO.insertarObtenerObjeto(Supervisor.fromSupervisorAsociadoDTO(supervisorDTO)));
                    listSupervisorDTO.add(supDTO);

//                    String jsonSupv = Recv.objectToJson(supDTO);
//                    SendLocalQueue.enviandoControlLocalRol(jsonSupv, "I", "supervisor", "localhost");
                }
                newUsuarioDTO.setSupervisor(listSupervisorDTO);
            } else {
                // ******se le asignó código editar supervisor...******
                List<SupervisorDTO> listSupervisorDTO = new ArrayList<>();
                for (SupervisorDTO supervisorDTO : oldUsuarioDTO.getSupervisor()) {
                    supervisorDTO.setUsuario(newUsuarioDTO);
                    supervisorDTO.setFechaMod(tiempoActual);

                    SupervisorDTO supDTO = Supervisor.toSupervisorUsuarioDTO(
                            supervisorDAO.actualizarObtenerObjeto(Supervisor.fromSupervisorAsociadoDTO(supervisorDTO)));
                    listSupervisorDTO.add(supDTO);

//                    String jsonSupv = Recv.objectToJson(supDTO);
//                    SendLocalQueue.enviandoControlLocalRol(jsonSupv, "U", "supervisor", "localhost");
                }
                newUsuarioDTO.setSupervisor(listSupervisorDTO);
            }
        } else {
            // ******NO se le asignó ningún código supervisor...******
            newUsuarioDTO.setSupervisor(null);
        }
        // ******se actualiza de cero, todos los roles que hacen referencia a
        // usuario, de haberlos...******
        // 1) delete - usuario <-> rol

        usuarioRolDAO.delete(newUsuarioDTO.getIdUsuario());

        JSONObject jsonUs = new JSONObject();
//        jsonUs.put("idUsuario", newUsuarioDTO.getIdUsuario());
//        SendLocalQueue.enviandoControlLocalRol(jsonUs + "", "D", "usuario_rol", "localhost");

        // 1) delete - usuario <-> rol
        // 2) create - usuario <-> rol
        if (oldUsuarioDTO.getUsuarioRols() != null) {
            List<UsuarioRolDTO> listUsuarioRolDTO = new ArrayList<>();
            for (UsuarioRolDTO usuarioRolDTO : oldUsuarioDTO.getUsuarioRols()) {
                usuarioRolDTO.setUsuario(newUsuarioDTO);

                UsuarioRolDTO urDTO = UsuarioRol.toBDUsuarioRolSeguridadDTO(
                        usuarioRolDAO.insertarObtenerObjeto(UsuarioRol.fromUsuarioRolSeguridadDTO(usuarioRolDTO)));
                listUsuarioRolDTO.add(urDTO);

//                String jsonUrDTO = Recv.objectToJson(urDTO);
//                SendLocalQueue.enviandoControlLocalRol(jsonUrDTO, "I", "usuario_rol", "localhost");
            }
            newUsuarioDTO.setUsuarioRols(listUsuarioRolDTO);
        }
        return newUsuarioDTO/* .nullAllData() */;
    }
}
