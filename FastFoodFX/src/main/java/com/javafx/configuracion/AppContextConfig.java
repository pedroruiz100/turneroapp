/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.configuracion;

import com.javafx.scope.ScreenScope;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.javafx.screen", "com.javafx.controllers", "com.peluqueria.dao", "com.javafx.jrviewer"})
public class AppContextConfig {

    @Bean
    public static CustomScopeConfigurer getCustomScopeConfigurer() {
        CustomScopeConfigurer configurer = new CustomScopeConfigurer();
        final Map<String, Object> scopeMap = new HashMap<>();
        scopeMap.put("screen", screenScope());
        configurer.setScopes(scopeMap);
        return configurer;
    }

    @Bean
    public static ScreenScope screenScope() {
        return new ScreenScope();
    }

}
