/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;

/**
 *
 * @author ExcelsisWalker
 */
public class Fecha {

    public static HashMap<Long, Boolean> hashDia() {
        LocalDate now = LocalDate.now();
        HashMap<Long, Boolean> hashDia = new HashMap<>();
        LocalDate primerDomingo = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.SUNDAY));
        if (now.equals(primerDomingo)) {
            hashDia.put(1l, true);
        }
        LocalDate ultimoDomingo = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.SUNDAY));
        if (now.equals(ultimoDomingo)) {
            hashDia.put(1l, false);
        }
        LocalDate primerLunes = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        if (now.equals(primerLunes)) {
            hashDia.put(2l, true);
        }
        LocalDate ultimoLunes = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.MONDAY));
        if (now.equals(ultimoLunes)) {
            hashDia.put(2l, false);
        }
        LocalDate primerMartes = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.TUESDAY));
        if (now.equals(primerMartes)) {
            hashDia.put(3l, true);
        }
        LocalDate ultimoMartes = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.TUESDAY));
        if (now.equals(ultimoMartes)) {
            hashDia.put(3l, false);
        }
        LocalDate primerMiercoles = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.WEDNESDAY));
        if (now.equals(primerMiercoles)) {
            hashDia.put(4l, true);
        }
        LocalDate ultimoMiercoles = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.WEDNESDAY));
        if (now.equals(ultimoMiercoles)) {
            hashDia.put(4l, false);
        }
        LocalDate primerJueves = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.THURSDAY));
        if (now.equals(primerJueves)) {
            hashDia.put(5l, true);
        }
        LocalDate ultimoJueves = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.THURSDAY));
        if (now.equals(ultimoJueves)) {
            hashDia.put(5l, false);
        }
        LocalDate primerViernes = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.FRIDAY));
        if (now.equals(primerViernes)) {
            hashDia.put(6l, true);
        }
        LocalDate ultimoViernes = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.FRIDAY));
        if (now.equals(ultimoViernes)) {
            hashDia.put(6l, false);
        }
        LocalDate primerSabado = now.with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        if (now.equals(primerSabado)) {
            hashDia.put(7l, true);
        }
        LocalDate ultimoSabado = now.with(TemporalAdjusters.lastInMonth(DayOfWeek.SATURDAY));
        if (now.equals(ultimoSabado)) {
            hashDia.put(7l, false);
        }
        return hashDia;
    }
}
