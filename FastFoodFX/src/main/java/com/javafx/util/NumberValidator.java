/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.text.DecimalFormat;

/**
 *
 * @author ExcelsisWalker
 */
public class NumberValidator {

    private static final String SCHARS = "1234567890ABCDEFGHJKLMNPQRSTUVWXYZ";
    private static final char[] ACHARS = SCHARS.toCharArray();

    public String formatoCantidad(double nro) {
        return numberFormat("###.###", nro);
    }

    public String numberValidator(String text) {
        if (text.length() != 0) {
            //last character intro key event, para evitar problemas con los números...
            String nuevo = "";
            for (int i = 0; i < text.length(); i++) {
                if (text.substring(i, i + 1).matches("[0-9]")) {
                    nuevo = nuevo + text.substring(i, i + 1);
                }
            }
            return nuevo;
        }
        return text;
    }

    public String numberFormat(String pattern, double value) {
        DecimalFormat myFormatter = new DecimalFormat(pattern);
        String output = myFormatter.format(value);
        return output;
    }

    public static boolean letterValidator(String text) {
        if (text.length() != 0) {
            for (int i = 0; i < text.length(); i++) {
                if (text.substring(i, i + 1).matches("[a-zA-Z]")) {
                    return true;
                }
                if (text.substring(i, i + 1).matches("[ñÑáÁéÉíÍóÓúÚ]")) {
                    return true;
                }
            }
        }
        return false;
    }
}
