package com.javafx.util;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;

public class Utilidades {

    public final static String host = "localhost";
    public final static int time = 20;
    public final static int port = 8080;
    public static final boolean DESARROLLO = true;

    public final static String ip = "http://" + host + ":" + port;
    public static String ipLocal = "";
    public static String version = "1.6";
    public static String empresa = "";
    public static final String IMPRESORA = "BIXOLON SRP-270 (Copiar 1)";
    public static final String URL_IMG_SRV = "C:/Users/sebastian/Desktop/JavaFx/";
    public static final String SA = "C:\\Users\\sebastian\\Desktop\\Desarrollo\\FastFoodFX\\Cola\\target\\Colas-1.0-SNAPSHOT.jar";

//    public static final String SA = "";
    public final static long idRango1 = 1;
    public static PrintStream ps;

    public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    public static SimpleDateFormat TSFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public static Logger log = Logger.getLogger(Utilidades.class.getName());

    private static long idRangoLocal = -1l;

    public static long idClienteSinNombre = 161168L;

    public static int getWidth() {
        return width;
    }

    public static int getHeight() {
        return height;
    }

    private static int width;
    private static int height;

    public static void adaptandoAnchor() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        width = gd.getDisplayMode().getWidth();
        height = gd.getDisplayMode().getHeight();
    }

    public static java.sql.Date stringToSqlDate(String fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = new java.util.Date();
        java.sql.Date sqlDate = null;
        try {
            utilDate = sdf.parse(fecha);
            sqlDate = new java.sql.Date(utilDate.getTime());
        } catch (ParseException ex) {
            log.error("Error ParseException: ", ex.fillInStackTrace());
        }
        return sqlDate;
    }

    public static String sqlDateToString(java.sql.Date fecha) {
        String string;
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(fecha);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        string = sdf.format(fecha);
        return string;
    }

    public static boolean pingHost(String host, int port, int timeout) {
        try (Socket socket = new Socket()) {
            socket.connect(new InetSocketAddress(host, port), timeout);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    public static java.util.Date stringToUtilDate(String fecha) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = new java.util.Date();
        try {
            utilDate = sdf.parse(fecha);
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        return utilDate;
    }

    public static String procesandoNro(long idSucursal, long nroCaja, long nroActual) {
        nroActual++;
        // queda definir el primer y segundo trío de números...
        String primerTrio = idSucursal + "";
        String segundoTrio = nroCaja + "";
        String nro = String.valueOf(nroActual);

        //Para agregar ceros delante
        // Para el primer trío
//		int tamTrio1 = primerTrio.length();
//		if (tamTrio1 < 3) {
//			tamTrio1 = 3 - tamTrio1;
//			for (int i = 0; i < tamTrio1; i++) {
//				primerTrio = "0" + primerTrio;
//			}
//		}
        // Para el segundo trío
        int tamTrio2 = segundoTrio.length();
        if (tamTrio2 < 3) {
            tamTrio2 = 3 - tamTrio2;
            for (int i = 0; i < tamTrio2; i++) {
                segundoTrio = "0" + segundoTrio;
            }
        }

        // Para la última numeración
        int tam = nro.length();
        if (tam < 7) {
            tam = 7 - tam;
            for (int i = 0; i < tam; i++) {
                nro = "0" + nro;
            }
        }
        nro = primerTrio + "" + segundoTrio + "" + nro;
        return nro;
    }

    //yyyy-MM-dd -> dd-MM-yyyy
    public static String ordenandoFechaString(String fecha) {
        String[] fechaSplit = fecha.split("-");
        if (fechaSplit[0].length() == 4) {
            return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
        } else {
            return fecha;
        }
    }

//    public static String procesandoNro(long idSucursal, long nroCaja, long nroActual) {
//        String nro = String.valueOf(nroActual);
//        return nro;
//    }
    public static Map<String, String> splitNroActual(String dato) {
        Map<String, String> valor = new HashMap<String, String>();
        StringTokenizer st = new StringTokenizer(dato, " - ");
        String nroActual = st.nextElement().toString();
        String id = st.nextElement().toString();
        valor.put("idTalonarioSucursal", id);
        valor.put("nroActual", nroActual);
        return valor;
    }

    public static String encodingAlambrado(String str) {
        str = str.replace('Á', 'A');
        str = str.replace('É', 'E');
        str = str.replace('Í', 'I');
        str = str.replace('Ó', 'O');
        str = str.replace('Ú', 'U');
        str = str.replace("Ñ", "NH");
        return str;
    }

    public static int verificarExistenciaPendientes() {
        if (ConexionPostgres.getCon() == null) {
            ConexionPostgres.conectar();
        }
        int dato = 0;
        String sql = "SELECT id_descuento_pendientes FROM pendiente.descuento_pendientes limit 1";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                dato++;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (ConexionPostgres.getCon() != null) {
            ConexionPostgres.cerrar();
        }
        return dato;
    }

    public static int verificarExistenciaPendientesArqueo() {
        ConexionPostgres.conectar();
        int dato = 0;
        String sql = "SELECT id_arqueo_pendientes FROM pendiente.arqueo_pendientes limit 1";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                dato++;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        ConexionPostgres.cerrar();
        return dato;
    }

    public static int verificarExistenciaGenerico(String columna, String tabla) {
        ConexionPostgres.conectar();
        int dato = 0;
        String sql = "SELECT " + columna + " FROM " + tabla + " limit 1";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                dato++;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        ConexionPostgres.cerrar();
        return dato;
    }

    public static String setToJson(String x) {
        x = x.replace("\"{", "{");
        x = x.replace("}\"", "}");
        x = x.replace("\\", "");
        return x;
    }

    public static String obteniendoFuncionario() throws org.json.simple.parser.ParseException {
        JSONObject jsonFuncionario = (JSONObject) Identity.getUsuario().get("funcionario");
        String user = "";
        if (jsonFuncionario != null) {
            if (jsonFuncionario.get("nombre") != null) {
                user = jsonFuncionario.get("nombre").toString();
            }
            if (jsonFuncionario.get("apellido") != null) {
                if (user.contentEquals("")) {
                    if (!jsonFuncionario.get("apellido").toString().contentEquals("")) {
                        user = jsonFuncionario.get("apellido").toString();
                    }
                } else if (!jsonFuncionario.get("apellido").toString().contentEquals("")) {
                    user = user + " " + jsonFuncionario.get("apellido").toString();
                }
            }
            if (user.contentEquals("")) {
                return Identity.getUsuario().get("nomUsuario").toString();
            }
        } else {
            return Identity.getUsuario().get("nomUsuario").toString();
        }
        return user;
    }

    //null, funcionario por default, inicio de sesión; else -> algún funcionario en determinado momento del proceso.
    public static String obteniendoFuncionario(JSONObject jsonUser, boolean funcAMitadDeProceso) {
        if (jsonUser != null && funcAMitadDeProceso) {
            String userFunc = "";
            JSONObject jsonFuncionario = (JSONObject) jsonUser.get("funcionario");
            if (jsonFuncionario.get("nombre") != null) {
                userFunc = jsonFuncionario.get("nombre").toString();
            }
            if (jsonFuncionario.get("apellido") != null) {
                if (userFunc.contentEquals("")) {
                    if (!jsonFuncionario.get("apellido").toString().contentEquals("")) {
                        userFunc = jsonFuncionario.get("apellido").toString();
                    }
                } else if (!jsonFuncionario.get("apellido").toString().contentEquals("")) {
                    userFunc = userFunc + " " + jsonFuncionario.get("apellido").toString();
                }
            }
            if (userFunc.contentEquals("")) {
                return jsonUser.get("nomUsuario").toString();
            }
            return userFunc;
        } else if (jsonUser == null && !funcAMitadDeProceso) {
            JSONObject jsonFunc = (JSONObject) Identity.getUsuario().get("funcionario");
            String userFunc = "";
            if (jsonFunc != null) {
                if (jsonFunc.get("nombre") != null) {
                    userFunc = jsonFunc.get("nombre").toString();
                }
                if (jsonFunc.get("apellido") != null) {
                    if (userFunc.contentEquals("")) {
                        if (!jsonFunc.get("apellido").toString().contentEquals("")) {
                            userFunc = jsonFunc.get("apellido").toString();
                        }
                    } else if (!jsonFunc.get("apellido").toString().contentEquals("")) {
                        userFunc = userFunc + " " + jsonFunc.get("apellido").toString();
                    }
                }
                if (userFunc.contentEquals("")) {
                    return Identity.getUsuario().get("nomUsuario").toString();
                }
            } else {
                return Identity.getUsuario().get("nomUsuario").toString();
            }
            return userFunc;
        }
        return "N/A";
    }

    public static String patternFactura(String dato) {
        String factura = "";
        int num = 0;
        int tamanho = dato.length();

        if (tamanho < 13) {
            int dif = 13 - tamanho;
            for (int i = 0; i < dif; i++) {
                dato = "0" + dato;
            }
        }
        for (int i = dato.length() - 1; i >= 0; i--) {
            num++;
            if (num == 7) {
                factura += dato.substring(i);
                factura = "-" + factura;
            }
            if (num == 10) {
                String datoSubString = dato.substring(i);
                String elemento = datoSubString.substring(0, 3);
                factura = "-" + elemento + factura;
            }
            if (num == 13) {
                String datoSubString = dato.substring(i);
                String elemento = datoSubString.substring(0, 3);
                factura = elemento + factura;
            }
        }
        return factura;
    }

    public static void logData() throws FileNotFoundException, IOException {
        String data = "Li9sb2dzL2RhdGEubG9n";
        File file = new File(UtilLoaderBase.msjVuelta(data));
        if (!file.exists()) {
            file.createNewFile();
        }
        FileOutputStream fos = new FileOutputStream(file, true);
        ps = new PrintStream(fos);
        System.setOut(ps);
    }

    public static Timestamp objectToTimestamp(Object fecha) {
        Timestamp ts = null;
        if (fecha instanceof Long) {
            ts = new Timestamp((long) fecha);
        } else if (fecha instanceof String) {//se supone que será String, sino...... bueno......
            try {
                Date parsedDate = null;
                if (((String) fecha).length() < 11) {
                    parsedDate = dateFormat.parse((String) fecha);
                } else if (((String) fecha).indexOf('T') > 0) {
                    String arrayFecha[] = ((String) fecha).split("T");
                    parsedDate = dateFormat.parse((String) arrayFecha[0]);
                } else {
                    parsedDate = TSFormat.parse((String) fecha);
                }
                ts = new Timestamp(parsedDate.getTime());
            } catch (ParseException ex) {
                log.error("ERROR: ", ex.fillInStackTrace());
            }
        } else {
            ts = new Timestamp(System.currentTimeMillis());
        }
        return ts;
    }

    public static void closeSystem() {
        if (SubAppRun.p != null) {
            if (SubAppRun.p.isAlive()) {
                SubAppRun.p.destroyForcibly();
                System.out.println("Kill Sub-app");
            } else {
                log.warn("Sub-app notAlive");
            }
        } else {
            log.warn("Sub-app null");
        }
        System.exit(0);
    }

    public static int genCodClienteParana(String ruc) {
        //R. El diccionario de la RAE contiene 88.000 palabras. 
        //El de americanismos 70.000; pero en este último aparecen muchas variantes que en el diccionario académico ocuparían una sola entrada, 
        //como guaira, huaira, huayra, waira, wayra, guayra. 
        //Se suele estimar el léxico de una lengua añadiendo un 30% al de los diccionarios.27 nov. 2010

        //2021741105 máx. int BD Paraná
        //2147483647 máx. int
        //tipo de valores aceptados -> números, letras y guiones medios
        //no se agrega filtro... se supone que se validará antes de llamar al método
        int codLength = ruc.length();
        int codGen = codLength;
        //máx. 83640, ideal...
        for (int i = 0; i < codLength; i++) {
            if ((codGen + (int) ruc.charAt(i)) < 83647) {
                if (codLength > 1) {
                    if (i == (codLength - 1)) {
                        codGen = codGen + ((int) ruc.charAt(i - 1) - (int) ruc.charAt(i));
                    } else if (i == 0) {
                        codGen = codGen + ((int) ruc.charAt(i + 1) - (int) ruc.charAt(i));
                    } else {
                        if (i % 2 == 0) {
                            codGen = codGen + (((int) ruc.charAt(i - 1) + (int) ruc.charAt(i + 1)) - (int) ruc.charAt(i));
                        } else {
                            codGen = codGen + (((int) ruc.charAt(i - 1) - (int) ruc.charAt(i + 1)) + (int) ruc.charAt(i));
                        }
                    }
                    if (i % 2 == 0) {
                        codGen = codGen + i;
                    } else {
                        codGen = codGen - i;
                    }
                } else {
                    codGen = codGen + (int) ruc.charAt(i) + i;
                }
            }
        }
        codGen = codGen + 2147400000;
        return codGen;
    }

    public static boolean verificandoPendientesAlerta() {
        if (ConexionPostgres.getConAviso() == null) {
            ConexionPostgres.conectarAviso();
        }
        boolean existe = false;
        String sql = "SELECT id_descuento_pendientes FROM pendiente.descuento_pendientes limit 1";
        try (PreparedStatement ps = ConexionPostgres.getConAviso().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                existe = true;
            }
            ps.close();
        } catch (SQLException ex) {
            log.error("ERROR: ", ex.fillInStackTrace());
        }
        if (ConexionPostgres.getConAviso() != null) {
            ConexionPostgres.cerrarAviso();
        }
        return existe;
    }

    public static long getIdRangoLocal() {
        return idRangoLocal;
    }

    public static void setIdRangoLocal(long aIdRangoLocal) {
        idRangoLocal = aIdRangoLocal;
    }

    public static SimpleDateFormat getTSFormat() {
        return TSFormat;
    }

    public static Time objectToTime(Object time) {
        Time t = null;
        if (time instanceof Long) {
            t = new Time((long) time);
        } else if (time instanceof String) {
            //                07:00:00 AM 10:00:00 PM
            String arrayAmPm[] = ((String) time).split(" ");
            if (arrayAmPm.length > 1) {
                String arraySplit[] = arrayAmPm[0].split(":");
                if (arrayAmPm[1].equalsIgnoreCase("PM")) {
                    int hora = Integer.valueOf(arraySplit[0]) + 12;
                    if (arraySplit.length > 2) {
                        t = Time.valueOf(hora + ":" + arraySplit[1] + ":" + arraySplit[2]);
                    } else {
                        t = Time.valueOf(hora + ":" + arraySplit[1]);
                    }
                } else {
                    t = Time.valueOf(arrayAmPm[0]);
                }
            } else {
                t = Time.valueOf((String) time);
            }
        }
        return t;
    }

//    public static String calculoSET(String ci) {
//        try {
//            int sum = 0;
//            for (int i = 0; i < ci.length(); i++) {
//                sum += Integer.parseInt(ci.charAt(i) + "") * ((ci.length() + 1) - i);
//            }
//            return ci + "-" + (11 - (sum % 11));
//        } catch (Exception e) {
//            return "";
//        }
//    }
    public static String calculoSET(String p_numero) {
        int p_basemax = 10;
//    public static String calculoSET(String p_numero, int p_basemax) {
        int v_total, v_resto, k, v_numero_aux, v_digit;
        String v_numero_al = "";

        for (int i = 0; i < p_numero.length(); i++) {
            char c = p_numero.charAt(i);
            if (Character.isDigit(c)) {
                v_numero_al += c;
            } else {
                v_numero_al += (int) c;
            }
        }

        k = 2;
        v_total = 0;

        for (int i = v_numero_al.length() - 1; i >= 0; i--) {
            k = k > p_basemax ? 2 : k;
            v_numero_aux = v_numero_al.charAt(i) - 48;
            v_total += v_numero_aux * k++;
        }

        v_resto = v_total % 11;
        v_digit = v_resto > 1 ? 11 - v_resto : 0;
        return p_numero + "-" + v_digit;
    }

    public static String msjVuelta(String msj) {
        return msjDecVuelta(msj);
    }

    public static String msjDecVuelta(String msj) {
        byte[] var = Base64.decodeBase64(msj.getBytes());
        String st = "";
        try {
            st = new String(var, "ISO-8859-1");
        } catch (Exception ex) {
            System.out.println("-->> " + ex.getLocalizedMessage());
            System.out.println("-->> " + ex.fillInStackTrace());
        }
        return st;
    }

    public static String msjIda(String msj) throws Exception {
        return msjDecIda(msj);
    }

    public static String msjDecIda(String msj)
            throws Exception {
        byte[] byteArray = Base64.encodeBase64(msj.getBytes());
        String encodedString = new String(byteArray);
        return encodedString;
    }
    
    public static String recuperarEmpresa() {
        ConexionPostgres.conectarServer();
        String empresa = "";
        String sql = "SELECT * FROM general.empresa WHERE id_empresa=1";
        System.out.println("-->> " + sql);
        try (PreparedStatement ps = ConexionPostgres.getConServer().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
//                System.out.println("******* DATOS ELIMINADOS DEL AUXILIAR CANCELACION PRODUCTO ********");
                empresa = rs.getString("descripcion_empresa");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.info(ex.getLocalizedMessage());
        }
//        actualizarRangoCancelFact(valor);
        ConexionPostgres.cerrarServer();
        return empresa;
    }
}
