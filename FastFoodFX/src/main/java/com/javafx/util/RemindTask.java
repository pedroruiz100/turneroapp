/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.TimerTask;
import javafx.application.Platform;
import javafx.scene.control.CheckBox;
import javafx.scene.effect.DropShadow;
import javafx.scene.paint.Color;

/**
 *
 * @author ExcelsisWalker
 */
public class RemindTask extends TimerTask {

    private static CheckBox checkBoxSincroTask;

    @Override
    public void run() {
        if (Utilidades.verificandoPendientesAlerta()) {
            Platform.runLater(() -> {
                try {
                    checkBoxSincroTask.setText("envío pendiente");
                    checkBoxSincroTask.setSelected(false);
                    checkBoxSincroTask.setEffect(new DropShadow(10, Color.RED));
                } catch (NullPointerException nEx) {
                    Utilidades.log.error("NullPointerException checkBoxRemind: ", nEx.fillInStackTrace());
                } finally {
                }
            });
        } else {
            Platform.runLater(() -> {
                try {
                    checkBoxSincroTask.setText("envío óptimo");
                    checkBoxSincroTask.setSelected(true);
                    checkBoxSincroTask.setEffect(new DropShadow(10, Color.GREEN));
                } catch (NullPointerException nEx) {
                    Utilidades.log.error("NullPointerException checkBoxRemind: ", nEx.fillInStackTrace());
                } finally {
                }
            });
        }
    }

    public static void enviandoNodoCheckBox(CheckBox checkBoxSincro) {
        if (checkBoxSincro != null) {
            checkBoxSincroTask = checkBoxSincro;
        } else {
            checkBoxSincroTask = new CheckBox();
        }
        if (ConexionPostgres.getConAviso() != null) {
            ConexionPostgres.cerrarAviso();
        }
    }

}
