/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.prefs.Preferences;
import javax.swing.JOptionPane;

/**
 *
 * @author PC
 */
public class VerificandoInstancias {

    public void main(String[] args) {
        if (isRunning()) {
            JOptionPane.showMessageDialog(null, "2 instances of this program cannot be running at the same time. \n Exiting now");
            System.exit(0);
        } else {
            onStart();
        }
    }

    public final void onStart() {
        Preferences prefs;
        prefs = Preferences.userRoot().node(this.getClass().getName());
        System.out.println("INICIANDO...");
        prefs.put("RUNNING", "true");
    }

    public final void onFinish() {
        Preferences prefs;
        prefs = Preferences.userRoot().node(this.getClass().getName());
        System.out.println("CERRANDO...");
        prefs.put("RUNNING", "false");
    }

    public boolean isRunning() {
        Preferences prefs;
        System.out.println("-> " + this.getClass().getName());
        prefs = Preferences.userRoot().node(this.getClass().getName());
        return prefs.get("RUNNING", null) != null ? Boolean.valueOf(prefs.get("RUNNING", null)) : false;
    }

    private void formWindowClosing(java.awt.event.WindowEvent evt) {
        onFinish();
    }
}
