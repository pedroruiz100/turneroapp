/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util.queries;

import com.javafx.util.ConexionPostgres;
import com.javafx.util.Utilidades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ExcelsisWalker
 */
public class CuponeraLocalQueries {

    public static JSONObject cuponera() {
        String sql = "select row_to_json(cup) as \"cuponera\" from \n"
                + "(select \n"
                + "nombre as \"nombre\", \n"
                + "activo as \"activo\", \n"
                + "cliente_fiel as \"clienteFiel\", \n"
                + "monto_min as \"montoMin\"\n"
                + "from \n"
                + "cuponera.cuponera as cuponera) cup \n"
                + "where cup.\"activo\" = true order by cup.\"nombre\" limit 1";
        JSONObject json = new JSONObject();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                json = (JSONObject) parser.parse(rs.getObject("cuponera").toString());
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries Cuponera: ", ex.fillInStackTrace());
        } finally {
        }
        return json;
    }
}
