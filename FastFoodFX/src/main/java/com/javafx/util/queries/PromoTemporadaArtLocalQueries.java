/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util.queries;

import com.javafx.util.ConexionPostgres;
import com.javafx.util.Utilidades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ExcelsisWalker
 */
public class PromoTemporadaArtLocalQueries {

    public static JSONArray promocionArticulos() {
        String sql = "select row_to_json(pta) as \"promoTemporadaArt\" from \n"
                + "(select descripcion_temporada_art as \"descripcionTemporadaArt\", \n"
                + "estado_promo as \"estadoPromo\", \n"
                + "fecha_inicio as \"fechaInicio\", \n"
                + "fecha_fin as \"fechaFin\", \n"
                + "id_temporada_art as \"idTemporadaArt\", \n"
                + "(select json_agg(apt) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada as \"idArtPromoTemporada\", porcentaje_desc as \"porcentajeDesc\", \n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromo.id_articulo) art) as \"articulo\"  \n"
                + "from cuenta.art_promo_temporada as artPromo where id_temporada_art = promoArt.id_temporada_art) apt) as \"artPromoTemporadaDTO\", \n"
                + "(select json_agg(aptobs) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada_obsequio as \"idArtPromoTemporadaObsequio\", min_req as \"minReq\", cant_obsequio as \"cantObsequio\",\n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromoObs.id_articulo) art) as \"articulo\"  \n"
                + "from descuento.art_promo_temporada_obsequio as artPromoObs where id_temporada_art = promoArt.id_temporada_art) aptobs) as \"artPromoTemporadaObsequio\" \n"
                + "from \n"
                + "cuenta.promo_temporada_art as promoArt) pta where pta.\"estadoPromo\" = true order by pta.\"descripcionTemporadaArt\"";
        JSONArray jsonArray = new JSONArray();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(rs.getObject("promoTemporadaArt").toString());
                jsonArray.add(json);
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
        } finally {
        }
        return jsonArray;
    }

    public static JSONArray promocionArticulosFETCH(long limit, long offset) {
        String sql = "select row_to_json(pta) as \"promoTemporadaArt\" from \n"
                + "(select descripcion_temporada_art as \"descripcionTemporadaArt\", \n"
                + "estado_promo as \"estadoPromo\", \n"
                + "fecha_inicio as \"fechaInicio\", \n"
                + "fecha_fin as \"fechaFin\", \n"
                + "id_temporada_art as \"idTemporadaArt\", \n"
                + "(select json_agg(apt) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada as \"idArtPromoTemporada\", porcentaje_desc as \"porcentajeDesc\", \n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromo.id_articulo) art) as \"articulo\"  \n"
                + "from cuenta.art_promo_temporada as artPromo where id_temporada_art = promoArt.id_temporada_art) apt) as \"artPromoTemporadaDTO\", \n"
                + "(select json_agg(aptobs) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada_obsequio as \"idArtPromoTemporadaObsequio\", min_req as \"minReq\", cant_obsequio as \"cantObsequio\",\n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromoObs.id_articulo) art) as \"articulo\"  \n"
                + "from descuento.art_promo_temporada_obsequio as artPromoObs where id_temporada_art = promoArt.id_temporada_art) aptobs) as \"artPromoTemporadaObsequio\"  \n"
                + "from \n"
                + "cuenta.promo_temporada_art as promoArt) pta where pta.\"estadoPromo\" = true order by pta.\"descripcionTemporadaArt\" limit " + limit + " offset " + offset;
        JSONArray jsonArray = new JSONArray();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(rs.getObject("promoTemporadaArt").toString());
                jsonArray.add(json);
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
        } finally {
        }
        return jsonArray;
    }

    public static JSONArray promocionArticulosFETCHFilter(long limit, long offset, String promTempFiltro, String fechaDesde, String fechaHasta) {
        String sql = "select row_to_json(pta) as \"promoTemporadaArt\" from \n"
                + "(select descripcion_temporada_art as \"descripcionTemporadaArt\", \n"
                + "estado_promo as \"estadoPromo\", \n"
                + "fecha_inicio as \"fechaInicio\", \n"
                + "fecha_fin as \"fechaFin\", \n"
                + "id_temporada_art as \"idTemporadaArt\", \n"
                + "(select json_agg(apt) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada as \"idArtPromoTemporada\", porcentaje_desc as \"porcentajeDesc\", \n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromo.id_articulo) art) as \"articulo\"  \n"
                + "from cuenta.art_promo_temporada as artPromo where id_temporada_art = promoArt.id_temporada_art) apt) as \"artPromoTemporadaDTO\", \n"
                + "(select json_agg(aptobs) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada_obsequio as \"idArtPromoTemporadaObsequio\", min_req as \"minReq\", cant_obsequio as \"cantObsequio\",\n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromoObs.id_articulo) art) as \"articulo\"  \n"
                + "from descuento.art_promo_temporada_obsequio as artPromoObs where id_temporada_art = promoArt.id_temporada_art) aptobs) as \"artPromoTemporadaObsequio\"  \n"
                + "from \n"
                + "cuenta.promo_temporada_art as promoArt) pta where pta.\"estadoPromo\" = true";
        if (!promTempFiltro.equalsIgnoreCase("null")) {
            sql = sql + " and upper(pta.\"descripcionTemporadaArt\") like '" + promTempFiltro.toUpperCase() + "%'";
        }
        if (!fechaDesde.equalsIgnoreCase("null") && !fechaHasta.equalsIgnoreCase("null")) {
            sql = sql + " and pta.\"fechaInicio\" >= '" + fechaDesde + "' and pta.\"fechaFin\" <= '" + fechaHasta + "'";
        } else if (!fechaDesde.equalsIgnoreCase("null")) {
            sql = sql + " and pta.\"fechaInicio\" = '" + fechaDesde + "'";
        } else if (!fechaHasta.equalsIgnoreCase("null")) {
            sql = sql + " and pta.\"fechaFin\" = '" + fechaHasta + "'";
        }
        sql = sql + " order by pta.\"descripcionTemporadaArt\" limit " + limit + " offset " + offset;
        JSONArray jsonArray = new JSONArray();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(rs.getObject("promoTemporadaArt").toString());
                jsonArray.add(json);
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
        } finally {
        }
        return jsonArray;
    }

    public static String promocionNom(long codArt) {
        String sql = "select pta.descripcion_temporada_art \n"
                + "FROM cuenta.promo_temporada_art pta left join cuenta.art_promo_temporada apt on pta.id_temporada_art = apt.id_temporada_art \n"
                + "left join stock.articulo art on art.id_articulo = apt.id_articulo \n"
                + "where pta.estado_promo = true and art.cod_articulo = " + codArt + " limit 1";
        String nomProm = " POR TEMPORADA";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                nomProm = " [" + rs.getString("descripcion_temporada_art") + "]";
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
            return nomProm;
        } finally {
        }
        return nomProm;
    }
    
    public static String promocionNomObsequio(long codArt) {
        String sql = "select pta.descripcion_temporada_art \n"
                + "FROM cuenta.promo_temporada_art pta left join descuento.art_promo_temporada_obsequio aptobsequio on pta.id_temporada_art = aptobsequio.id_temporada_art \n"
                + "left join stock.articulo art on art.id_articulo = aptobsequio.id_articulo \n"
                + "where pta.estado_promo = true and art.cod_articulo = " + codArt + " limit 1";
        String nomProm = " POR TEMPORADA";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                nomProm = " [" + rs.getString("descripcion_temporada_art") + "]";
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
            return nomProm;
        } finally {
        }
        return nomProm;
    }

    public static long verificandoCodArt(long codArt, String inicio, String fin) {
        String sql = "SELECT art.cod_articulo FROM \n"
                + "cuenta.art_promo_temporada apt left join cuenta.promo_temporada_art pta on apt.id_temporada_art = pta.id_temporada_art \n"
                + "left join stock.articulo art on apt.id_articulo = art.id_articulo where pta.estado_promo = true and art.cod_articulo = " + codArt
                + "and (\n"
                + "(pta.fecha_inicio <= '" + inicio + "' and pta.fecha_fin >= '" + fin + "') or \n"
                + "(pta.fecha_inicio >= '" + inicio + "' and pta.fecha_fin >= '" + fin + "' and pta.fecha_inicio <= '" + fin + "') or \n"
                + "(pta.fecha_inicio <= '" + inicio + "' and pta.fecha_fin <= '" + fin + "' and pta.fecha_fin >= '" + inicio + "') or \n"
                + "(pta.fecha_inicio >= '" + inicio + "' and pta.fecha_fin <= '" + fin + "'))\n"
                + "limit 1";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                codArt = -1;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
            return -1l;
        } finally {
        }
        return codArt;
    }
    
    public static long verificandoCodArtObsequio(long codArt, String inicio, String fin) {
        String sql = "SELECT art.cod_articulo FROM \n"
                + "descuento.art_promo_temporada_obsequio aptObs left join cuenta.promo_temporada_art pta on aptObs.id_temporada_art = pta.id_temporada_art \n"
                + "left join stock.articulo art on aptObs.id_articulo = art.id_articulo where pta.estado_promo = true and art.cod_articulo = " + codArt
                + "and (\n"
                + "(pta.fecha_inicio <= '" + inicio + "' and pta.fecha_fin >= '" + fin + "') or \n"
                + "(pta.fecha_inicio >= '" + inicio + "' and pta.fecha_fin >= '" + fin + "' and pta.fecha_inicio <= '" + fin + "') or \n"
                + "(pta.fecha_inicio <= '" + inicio + "' and pta.fecha_fin <= '" + fin + "' and pta.fecha_fin >= '" + inicio + "') or \n"
                + "(pta.fecha_inicio >= '" + inicio + "' and pta.fecha_fin <= '" + fin + "'))\n"
                + "limit 1";
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                codArt = -1;
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
            return -1l;
        } finally {
        }
        return codArt;
    }

    public static JSONObject promocionCodArticulosBusqueda(String codArt) {
        String sql = "select row_to_json(pta) as \"promoTemporadaArt\" from \n"
                + "(select descripcion_temporada_art as \"descripcionTemporadaArt\", estado_promo as \"estadoPromo\", \n"
                + "fecha_inicio as \"fechaInicio\", fecha_fin as \"fechaFin\", promoArt.id_temporada_art as \"idTemporadaArt\", \n"
                + "(select json_agg(apt) from (select descri_articulo as \"descriArticulo\", id_art_promo_temporada as \"idArtPromoTemporada\", porcentaje_desc as \"porcentajeDesc\", \n"
                + "(select row_to_json(art) from (select descripcion as \"descripcion\", cod_articulo as \"codArticulo\" from stock.articulo where id_articulo = artPromo.id_articulo) art) as \"articulo\"  \n"
                + "from cuenta.art_promo_temporada as artPromo, stock.articulo artStock \n"
                + "where artPromo.id_temporada_art = promoArt.id_temporada_art and artStock.id_articulo = artPromo.id_articulo and artStock.cod_articulo = " + codArt + ") apt) as \"artPromoTemporadaDTO\" from \n"
                + "cuenta.promo_temporada_art as promoArt left join cuenta.art_promo_temporada as artiPromoTemp on promoArt.id_temporada_art = artiPromoTemp.id_temporada_art \n"
                + "left join stock.articulo arti on artiPromoTemp.id_articulo = arti.id_articulo where arti.cod_articulo = " + codArt + ") pta where pta.\"estadoPromo\" = true";
        JSONObject jsonPromTemp = new JSONObject();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                jsonPromTemp = (JSONObject) parser.parse(rs.getObject("promoTemporadaArt").toString());
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
        } finally {
        }
        return jsonPromTemp;
    }
}
