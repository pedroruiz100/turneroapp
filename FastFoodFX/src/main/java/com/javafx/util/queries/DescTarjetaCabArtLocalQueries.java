/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util.queries;

import com.javafx.util.ConexionPostgres;
import com.javafx.util.Utilidades;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ExcelsisWalker
 */
public class DescTarjetaCabArtLocalQueries {

    public static JSONArray tarjetaDtoArticulos() {
        String sql = "select row_to_json(dtc) as \"descuentoTarjetaCab\" from \n"
                + "(select \n"
                + "dtocab.id_descuento_tarjeta_cab as \"idDescuentoTarjetaCab\", \n"
                + "dtocab.porcentaje_parana as \"porcentajeParana\", \n"
                + "dtocab.descri_tarjeta as \"descriTarjeta\", \n"
                + "EXTRACT(EPOCH FROM dtocab.fecha_alta::timestamp with time zone) * 1000 as \"fechaAlta\", \n"
                + "EXTRACT(EPOCH FROM dtocab.fecha_mod::timestamp with time zone) * 1000 as \"fechaMod\", \n"
                + "dtocab.extracto as \"extracto\", \n"
                + "dtocab.usu_alta as \"usuAlta\", \n"
                + "dtocab.estado_desc as \"estadoDesc\", \n"
                + "dtocab.hora_inicio as \"horaInicio\", \n"
                + "dtocab.porcentaje_desc as \"porcentajeDesc\", \n"
                + "dtocab.hora_fin as \"horaFin\",\n"
                + "EXTRACT(EPOCH FROM dtocab.fecha_inicio::timestamp with time zone) * 1000 as \"fechaInicio\", \n"
                + "EXTRACT(EPOCH FROM dtocab.fecha_fin::timestamp with time zone) * 1000 as \"fechaFin\", \n"
                + "dtocab.monto_min as \"montoMin\", \n"
                + "dtocab.usu_mod as \"usuMod\", \n"
                + "(select json_agg(dtd) from (select  \n"
                + "	id_descuento_tarjeta_det as \"idDescuentoTarjetaDet\", \n"
                + "	dia_especial as \"diaEspecial\", \n"
                + "	(select row_to_json(dias) from (select \n"
                + "		id_dia as \"idDia\", \n"
                + "		descripcion_dia as \"descripcionDia\" from general.dias where id_dia = dtodet.id_dia) dias) as \"dia\" \n"
                + "from cuenta.descuento_tarjeta_det as dtodet where dtodet.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtd) as \"descuentoTarjetaDets\", \n"
                + "(select json_agg(dtca) from (select  \n"
                + "	id_descuento_tarjeta_cab_articulo as \"idDescuentoTarjetaCabArticulo\",  \n"
                + "	(select row_to_json(articulo) from (select \n"
                + "		cod_articulo as \"codArticulo\", \n"
                + "		descripcion as \"descripcion\" from stock.articulo where id_articulo = dtocabart.id_articulo) articulo) as \"articulo\" \n"
                + "from descuento.descuento_tarjeta_cab_articulo as dtocabart where dtocabart.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtca) as \"descuentoTarjetaCabArticulos\", \n"
                + "(select json_agg(dtce) from (select  \n"
                + "	id_descuento_tarjeta_cab_entidad as \"idDescuentoTarjetaCabEntidad\", \n"
                + "	porcentaje_entidad as \"porcentajeEntidad\", \n"
                + "	retorno as \"retorno\", \n"
                + "	(select row_to_json(entidad) from (select \n"
                + "		id_entidad as \"idEntidad\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from cuenta.entidad where id_entidad = dtocabent.id_entidad) entidad) as \"entidad\"\n"
                + "from descuento.descuento_tarjeta_cab_entidad as dtocabent where dtocabent.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtce) as \"descuentoTarjetaCabEntidads\", \n"
                + "(select json_agg(t) from (select  \n"
                + "	id_tarjeta as \"idTarjeta\", 	\n"
                + "	descripcion as \"descripcion\", \n"
                + "	codtar as \"codtar\", \n"
                + "	habilitado as \"habilitado\", \n"
                + "	(select row_to_json(tipotarjeta) from (select \n"
                + "		id_tipo_tarjeta as \"idTipoTarjeta\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from cuenta.tipo_tarjeta where id_tipo_tarjeta = dtotar.id_tipo_tarjeta) tipotarjeta) as \"tipoTarjeta\" \n"
                + "from cuenta.tarjeta as dtotar where dtotar.id_tarjeta = dtocab.id_tarjeta) t) as \"tarjeta\", \n"
                + "(select json_agg(dtcnf1) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf1 as \"idDescuentoTarjetaCabNf1\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf1Tipo) from (select \n"
                + "		id_nf1_tipo as \"idNf1Tipo\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf1_tipo where id_nf1_tipo = dtocabnf1.id_nf1_tipo) nf1Tipo) as \"nf1Tipo\"\n"
                + "from descuento.descuento_tarjeta_cab_nf1 as dtocabnf1 where dtocabnf1.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf1) as \"descuentoTarjetaCabNf1s\", \n"
                + "(select json_agg(dtcnf2) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf2 as \"idDescuentoTarjetaCabNf2\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf2Sfamilia) from (select \n"
                + "		id_nf2_sfamilia as \"idNf2Sfamilia\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf2_sfamilia where id_nf2_sfamilia = dtocabnf2.id_nf2_sfamilia) nf2Sfamilia) as \"nf2Sfamilia\"\n"
                + "from descuento.descuento_tarjeta_cab_nf2 as dtocabnf2 where dtocabnf2.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf2) as \"descuentoTarjetaCabNf2s\", \n"
                + "(select json_agg(dtcnf3) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf3 as \"idDescuentoTarjetaCabNf3\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf3Sseccion) from (select \n"
                + "		id_nf3_sseccion as \"idNf3Sseccion\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf3_sseccion where id_nf3_sseccion = dtocabnf3.id_nf3_sseccion) nf3Sseccion) as \"nf3Sseccion\"\n"
                + "from descuento.descuento_tarjeta_cab_nf3 as dtocabnf3 where dtocabnf3.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf3) as \"descuentoTarjetaCabNf3s\", \n"
                + "(select json_agg(dtcnf4) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf4 as \"idDescuentoTarjetaCabNf4\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf4Seccion1) from (select \n"
                + "		id_nf4_seccion1 as \"idNf4Seccion1\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf4_seccion1 where id_nf4_seccion1 = dtocabnf4.id_nf4_seccion1) nf4Seccion1) as \"nf4Seccion1\"\n"
                + "from descuento.descuento_tarjeta_cab_nf4 as dtocabnf4 where dtocabnf4.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf4) as \"descuentoTarjetaCabNf4s\", \n"
                + "(select json_agg(dtcnf5) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf5 as \"idDescuentoTarjetaCabNf5\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf5Seccion2) from (select \n"
                + "		id_nf5_seccion2 as \"idNf5Seccion2\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf5_seccion2 where id_nf5_seccion2 = dtocabnf5.id_nf5_seccion2) nf5Seccion2) as \"nf5Seccion2\"\n"
                + "from descuento.descuento_tarjeta_cab_nf5 as dtocabnf5 where dtocabnf5.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf5) as \"descuentoTarjetaCabNf5s\", \n"
                + "(select json_agg(dtcnf6) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf6 as \"idDescuentoTarjetaCabNf6\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf6Secnom6) from (select \n"
                + "		id_nf6_secnom6 as \"idNf6Secnom6\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf6_secnom6 where id_nf6_secnom6 = dtocabnf6.id_nf6_secnom6) nf6Secnom6) as \"nf6Secnom6\"\n"
                + "from descuento.descuento_tarjeta_cab_nf6 as dtocabnf6 where dtocabnf6.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf6) as \"descuentoTarjetaCabNf6s\", \n"
                + "(select json_agg(dtcnf7) from (select  \n"
                + "	id_descuento_tarjeta_cab_nf7 as \"idDescuentoTarjetaCabNf7\", \n"
                + "	descri_seccion as \"descriSeccion\", \n"
                + "	(select row_to_json(nf7Secnom7) from (select \n"
                + "		id_nf7_secnom7 as \"idNf7Secnom7\", \n"
                + "		descripcion as \"descripcion\" \n"
                + "		from stock.nf7_secnom7 where id_nf7_secnom7 = dtocabnf7.id_nf7_secnom7) nf7Secnom7) as \"nf7Secnom7\"\n"
                + "from descuento.descuento_tarjeta_cab_nf7 as dtocabnf7 where dtocabnf7.id_descuento_tarjeta_cab = dtocab.id_descuento_tarjeta_cab) dtcnf7) as \"descuentoTarjetaCabNf7s\" \n"
                + "from cuenta.descuento_tarjeta_cab as dtocab) dtc \n"
                + "where dtc.\"estadoDesc\" = true order by dtc.\"descriTarjeta\"";
        JSONArray jsonArray = new JSONArray();
        try (PreparedStatement ps = ConexionPostgres.getConLocal().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(rs.getObject("descuentoTarjetaCab").toString());
                jsonArray.add(json);
            }
            ps.close();
        } catch (SQLException | ParseException ex) {
            Utilidades.log.error("ERROR Exception localQueries: ", ex.fillInStackTrace());
        } finally {
        }
        return jsonArray;
    }
}
