/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author ExcelsisWalker
 */
public class EMF2 {

    private static EntityManagerFactory emf;
    private static boolean emfOk;

    private static void entityManagerFactory() {
        try {
            Map props = new HashMap();
            props.put("hibernate.connection.driver_class", "org.postgresql.Driver");
//            props.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/food_local");//de manejo local para cada nodo o caja.
            props.put("hibernate.connection.url", "jdbc:postgresql://localhost:5432/food_local");//de manejo local para cada nodo o caja.
            props.put("hibernate.connection.username", UtilLoaderBase.msjVuelta("cG9zdGdyZXM="));
            props.put("hibernate.connection.password", "1");
//            props.put("hibernate.connection.password", UtilLoaderBase.msjVuelta("MQ=="));
            props.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
            props.put("hibernate.flushMode", "FLUSH_AUTO");
            props.put("hibernate.hbm2ddl.auto", "update");
            props.put("hibernate.enable_lazy_load_no_trans", "true");
            emf = Persistence.createEntityManagerFactory("py.com.peluqueria_CasaParanaFX_jar_1.0-SNAPSHOTPU", props);
            emfOk = true;
        } catch (Exception e) {
            emfOk = false;
            Utilidades.log.error("ERROR EMF: ", e.fillInStackTrace());
        }
    }

    public EMF2() {
        entityManagerFactory();
    }

    public static EntityManagerFactory getEmf() {
        return emf;
    }

    public static boolean isEmfOk() {
        return emfOk;
    }
}
