package com.javafx.util;

import org.json.simple.JSONObject;

public class DatosEnCaja {

    private static JSONObject datos;
    private static JSONObject users;
    private static JSONObject facturados;

    public DatosEnCaja() {
    }

    public static JSONObject getDatos() {
        return datos;
    }

    public static void setDatos(JSONObject datos) {
        DatosEnCaja.datos = datos;
    }

    public static JSONObject getUsers() {
        return users;
    }

    public static void setUsers(JSONObject users) {
        DatosEnCaja.users = users;
    }

    public static JSONObject getFacturados() {
        return facturados;
    }

    public static void setFacturados(JSONObject facturados) {
        DatosEnCaja.facturados = facturados;
    }

}
