package com.javafx.util;

public class FacturaVentaDatos {

    private static Long idCajero;
    private static Long idProducto;
    private static Long idFacturaClienteCab;
    private static Boolean cancelarProducto;
    private static Boolean cancelarFactura;

    public static Long getIdCajero() {
        return idCajero;
    }

    public static void setIdCajero(Long idCajero) {
        FacturaVentaDatos.idCajero = idCajero;
    }

    public static Long getIdProducto() {
        return idProducto;
    }

    public static void setIdProducto(Long idProducto) {
        FacturaVentaDatos.idProducto = idProducto;
    }

    public static Long getIdFacturaClienteCab() {
        return idFacturaClienteCab;
    }

    public static void setIdFacturaClienteCab(Long idFacturaClienteCab) {
        FacturaVentaDatos.idFacturaClienteCab = idFacturaClienteCab;
    }

    public static Boolean getCancelarProducto() {
        return cancelarProducto;
    }

    public static void setCancelarProducto(Boolean cancelarProducto) {
        FacturaVentaDatos.cancelarProducto = cancelarProducto;
    }

    public static Boolean getCancelarFactura() {
        return cancelarFactura;
    }

    public static void setCancelarFactura(Boolean cancelarFactura) {
        FacturaVentaDatos.cancelarFactura = cancelarFactura;
    }

    public static void resetParam() {
        idCajero = 0l;
        idProducto = 0l;
        idFacturaClienteCab = 0l;
    }
}
