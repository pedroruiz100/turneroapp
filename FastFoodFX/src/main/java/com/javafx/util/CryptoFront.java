/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author ExcelsisWalker
 */
public class CryptoFront {

    private MessageDigest md;
    private byte[] buffer, digest;
    private String hash = "";

    public CryptoFront() {
    }

    public String getHash(String message) {
        return get(message);
    }

    private String get(String message) {
        try {
            hash = "";
            buffer = message.getBytes();
            md = MessageDigest.getInstance("SHA1");
            md.update(buffer);
            digest = md.digest();
            
            for (byte aux : digest) {
                int b = aux & 0xff;
                if (Integer.toHexString(b).length() == 1) {
                    hash += "0";
                }
                hash += Integer.toHexString(b);
            }
            return hash;
        } catch (NoSuchAlgorithmException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return "";
        }
    }
}
