package com.javafx.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexionPostgres {

    private static Connection con = null;
    private static Statement st = null;
    private static Connection conAviso = null;
    private static Statement stAviso = null;
    private static Connection conLocal = null;
    private static Statement stLocal = null;
    private static Connection conServer = null;
    private static Statement stServer = null;

    public static boolean conectar() {
        boolean valor = false;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/" + "handler_food";
//            con = DriverManager.getConnection(url, "postgres", "1");
//            String url = "jdbc:postgresql://localhost:5433/" + "handler_food";
            con = DriverManager.getConnection(url, "postgres", "1");
            st = con.createStatement();
            con.setAutoCommit(false);
            st.setFetchSize(100);
            valor = true;
        } catch (ClassNotFoundException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static boolean cerrar() {
        boolean valor = false;
        try {
            st.close();
            con.close();
            st = null;
            con = null;
            valor = true;
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static Connection getCon() {
        return con;
    }

    public static Statement getSt() {
        return st;
    }

    public static boolean conectarAviso() {
        boolean valor = false;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/" + "handler_food";
//            conAviso = DriverManager.getConnection(url, "postgres", "1");
            conAviso = DriverManager.getConnection(url, "postgres", "1");
            stAviso = getConAviso().createStatement();
            conAviso.setAutoCommit(false);
            stAviso.setFetchSize(100);
            valor = true;
        } catch (ClassNotFoundException | SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static boolean cerrarAviso() {
        boolean valor = false;
        try {
            stAviso.close();
            conAviso.close();
            stAviso = null;
            conAviso = null;
            valor = true;
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static Connection getConAviso() {
        return conAviso;
    }

    public static Statement getStAviso() {
        return stAviso;
    }

//    -----------------------------PARANA LOCAL--------------------------------------
    public static boolean conectarLocal() {
        boolean valor = false;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/food_local";
            conLocal = DriverManager.getConnection(url, "postgres", "1");
//            conLocal = DriverManager.getConnection(url, "postgres", "1");
            stLocal = getConLocal().createStatement();
            conLocal.setAutoCommit(false);
            stLocal.setFetchSize(100);
            valor = true;
        } catch (ClassNotFoundException | SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static boolean cerrarLocal() {
        boolean valor = false;
        try {
            stLocal.close();
            conLocal.close();
            stLocal = null;
            conLocal = null;
            valor = true;
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static Connection getConLocal() {
        return conLocal;
    }

    public static Statement getStLocal() {
        return stLocal;
    }
//    -----------------------------PARANA SERVIDOR--------------------------------------

    public static boolean conectarServer() {
        boolean valor = false;
        try {
            Class.forName("org.postgresql.Driver");
            String url = "jdbc:postgresql://localhost:5432/food_proyecto";
//            conServer = DriverManager.getConnection(url, "postgres", "1");
            conServer = DriverManager.getConnection(url, "postgres", "1");
            stServer = getConServer().createStatement();
            conServer.setAutoCommit(false);
            stServer.setFetchSize(100);
            valor = true;
        } catch (ClassNotFoundException | SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static boolean cerrarServer() {
        boolean valor = false;
        try {
            stServer.close();
            conServer.close();
            stServer = null;
            conServer = null;
            valor = true;
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static Connection getConServer() {
        return conServer;
    }

    public static Statement getStServer() {
        return stServer;
    }

}
