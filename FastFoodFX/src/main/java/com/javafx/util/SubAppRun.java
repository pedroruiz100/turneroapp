/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.io.IOException;

/**
 *
 * @author ExcelsisWalker
 */
public class SubAppRun extends Thread {

    public static Process p;

    @Override
    public void run() {
        iniciandoSubApp();
    }

    public void iniciandoSubApp() {
        try {
            ProcessBuilder pb = new ProcessBuilder("java", "-jar", Utilidades.SA);
            pb.redirectErrorStream(true);
            p = pb.start();
            StreamGobbler errorGobbler = new StreamGobbler(p.getErrorStream(), "ERROR");
            StreamGobbler outputGobbler = new StreamGobbler(p.getInputStream(), "OUTPUT");
            outputGobbler.start();
            errorGobbler.start();
            Thread.sleep(2000);
        } catch (IOException ex) {
            Utilidades.log.info("Error IOException: " + ex.getLocalizedMessage());
        } catch (InterruptedException ex) {
            Utilidades.log.info("Error InterruptedException: " + ex.getLocalizedMessage());
        }
    }

}
