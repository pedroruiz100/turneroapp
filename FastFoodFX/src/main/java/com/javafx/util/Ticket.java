package com.javafx.util;

import com.peluqueria.dao.ArticuloDAO;
import com.peluqueria.dao.impl.ArticuloDAOImpl;
import com.javafx.controllers.caja.FacturaDeVentaFXMLController;
import com.javafx.controllers.caja.MensajeFinalVentaFXMLController;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Timer;
import javax.print.CancelablePrintJob;
import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.Doc;
import javax.print.PrintException;
import javax.print.attribute.AttributeSet;
import javax.print.attribute.HashPrintServiceAttributeSet;
import javax.print.attribute.standard.*;

public class Ticket {

    private static DocPrintJob job;
    public static Timer timer = null;
    private NumberValidator numValidator;
    byte[] bytes;
    boolean slip;
    ArticuloDAO articuloDAO = new ArticuloDAOImpl();

    private String contentTicket
            = (char) 27 + "d" + (char) 0
            + (char) 27 + "a" + (char) 1
            + (char) 27 + "R" + (char) 7
            + (char) 27 + "c0" + (char) 3
            + (char) 27 + "z" + (char) 1 + (char) 13
            + "{{empresa}} \n"
                + "{{suc}} - RUC: {{ruc}}\n"
            + "TEL. {{tel}}-COMERCIO RAMOS GENERALES\n"
            + "{{dir}}\n"
            + (char) 27 + "z" + (char) 0 + (char) 13
            + (char) 27 + "c0" + (char) 2
            + "**ORIGINAL**\n"
            + (char) 13
            + (char) 27 + "c0" + (char) 1
            + "**DUPLICADO**\n"
            + (char) 13
            + (char) 27 + "c0" + (char) 3
            + (char) 27 + "z" + (char) 1
            + (char) 27 + "a" + (char) 0 + (char) 13
            + "----------------------------------------\n"
            + "TIMBRADO NRO. {{timb}} \n"
            + "INICIO VIGENCIA: {{desde}}\n"
            + "FIN VIGENCIA: {{hasta}}\n"
            + "CAJA INTERNA: {{cj}} FECHA: {{fH}}\n"
            + "FACTURA CONTADO: {{factNro}}\n"
            + "CAJERO: {{usuario}}\n"
            + "----------------------------------------\n"
            + "Articulo  Cant.  Precio      Total\n"
            + "{{art}}\n"
            + "---DETALLE FISCAL-----------------------\n"
            + "GRAVADA 10% :  {{grav10}}\n"
            + "GRAVADA 5%  :  {{grav5}}\n"
            + "EXENTA      :  {{exenta}}\n"
            + "---LIQUIDACION DE IVA-------------------\n"
            + "10%:  {{grav10Liq}}\n"
            + " 5%:   {{grav5Liq}}\n"
            + "----------------------------------------\n"
            + "RUC: {{rucCliente}}\n"
            + "CLIENTE: \n"
            + "{{cliente}}\n"
            + "----------------------------------------\n"
            + "ARTICULOS: {{cant}}TOTAL: {{total}}\n"
            + "DESCUENTO: {{desc}} NETO: {{neto}}\n"
            + "----------------------------------------\n"
            + "{{efectivo}}"
            + "{{dolar}}"
            + "{{peso}}"
            + "{{real}}"
            + "{{tarjCred}}"
            + "{{tarjDeb}}"
            + "{{cheque}}"
            + "{{vale}}"
            + "{{asoc}}"
            + "{{notCre}}"
            + "{{retencion}}"
            + "{{giftcard}}"
            + "DONACION: {{redondeo}}\n"
            + "VUELTO: {{vuelto}}\n"
            + (char) 27 + "z" + (char) 0 + (char) 13
            + (char) 27 + "c0" + (char) 1 + (char) 13
            + "\n"
            + (char) 27 + "c0" + (char) 2 + (char) 13
            + (char) 27 + "a" + (char) 1
            + "----------------------------------------\n"
            + "Verifique su compra antes de retirar\n"
//            + "Cambios hasta 5 dias, no se realizan\n"
//            + "cambios en oferta, liquidacion,\n"
//            + "perfumeria, bijouterie, prendas de \n"
//            + "lenceria, trajes de bano\n"
//            + "y aseo personal\n"
//            + "----------------------------------------\n"
            + "!!!!LAS COSAS MAS RICAS\n"
            + "AL MEJOR PRECIO!!\n"
            + "VISITA NUESTROS LOCALES Y APROVECHA\n"
            + "!!!!NO TE LO PODES PERDER!!!!\n"
            + "{{cupones}}"
            + "{{numPedido}}"
            + "{{crucijuegos}}"
            + (char) 27 + (char) 100 + (char) 13
            + (char) 27 + (char) 105
            + (char) 0;

    private String retiroTicket
            = (char) 27 + "d" + (char) 0
            + (char) 27 + "a" + (char) 1
            + (char) 27 + "c0" + (char) 2 + (char) 13
            + "{{empresa}} \n"
            + "{{suc}} - RUC: {{ruc}}\n"
            + "TEL. {{tel}}-COMERCIO RAMOS GENERALES\n"
            + "{{dir}}\n"
            + (char) 27 + "a" + (char) 0 + (char) 13
            + "----------------------------------------\n"
            + "RETIRO DE DINERO\n"
            + "Fecha.....: {{fecha}}\n"
            + "Hora......: {{hora}}\n"
            + "Supervisor: {{supervisor}}\n"
            + "Cajero....: {{cajero}}\n"
            + "Caja nro..: {{cajaNro}}\n"
            + "Retiro....: {{retiro}}\n"
            + "----------------------------------------\n"
            + "FIRMA SUPERVISOR/A...: .................\n\n"
            + "FIRMA CAJERO/A.......: .................\n"
            + (char) 27 + (char) 100 + (char) 10
            + (char) 27 + (char) 105
            + (char) 0;

    private String cierreTicket
            = (char) 27 + "d" + (char) 0
            + (char) 27 + "a" + (char) 1
            + (char) 27 + "c0" + (char) 2 + (char) 13
            + "{{empresa}} \n"
            + "{{suc}} - RUC: {{ruc}}\n"
            + "TEL. {{tel}}-COMERCIO RAMOS GENERALES\n"
            + "{{dir}}\n"
            + "----------------------------------------\n"
            + "FECHA:  {{fecha}}\n"
            + "{{Total a rendir}}:  {{totalRendir}}"
            + (char) 27 + (char) 100 + (char) 10
            + (char) 27 + (char) 105
            + (char) 0;

    private String cierreArqueo
            = (char) 27 + "z" + (char) 0 + (char) 13
            + (char) 27 + "c0" + (char) 1 + (char) 13
            + "----------------------------------------\n"
            + "{{datoArqueo}}" + "\n"
            + "----------------------------------------\n"
            + (char) 27 + (char) 100
            + (char) 0;

    private String chequeSlip
            = (char) 27 + "d" + (char) 0
            + (char) 27 + "c0" + (char) 4
            + (char) 10
            + (char) 10
            + (char) 10
            + (char) 10
            + (char) 27 + "a" + (char) 1 + (char) 13
            + "{{empresa}} {{cheque}}\n"
            + "Caja: {{nroCaja}}; Cajero: {{usuario}}; Fact. nro. {{factNro}}; fecha: {{fecha}} h.\n"
            + (char) 10
            + (char) 12
            + (char) 0;

    private String contentTicketReimpresion
            = (char) 27 + "d" + (char) 0
            + (char) 27 + "a" + (char) 1
            + (char) 27 + "R" + (char) 7
            + (char) 27 + "c0" + (char) 2
            + (char) 27 + "z" + (char) 0 + (char) 13
            + "{{empresa}} \n"
            + "{{suc}} - RUC: {{ruc}}\n"
            + "TEL. {{tel}}-COMERCIO RAMOS GENERALES\n"
            + "{{dir}}\n"
            + "{{dup}}\n"
            + (char) 27 + "a" + (char) 0
            + "----------------------------------------\n"
            + "TIMBRADO NRO. {{timb}} \n"
            + "INICIO VIGENCIA: {{desde}}\n"
            + "FIN VIGENCIA: {{hasta}}\n"
            + "CAJA INTERNA: {{cj}} FECHA: {{fH}}\n"
            + "FACTURA CONTADO: {{factNro}}\n"
            + "CAJERO: {{usuario}}\n"
            + "----------------------------------------\n"
            + "Articulo  Cant.  Precio      Total\n"
            + "{{art}}\n"
            + "---DETALLE FISCAL-----------------------\n"
            + "GRAVADA 10% :  {{grav10}}\n"
            + "GRAVADA 5%  :  {{grav5}}\n"
            + "EXENTA      :  {{exenta}}\n"
            + "---LIQUIDACION DE IVA-------------------\n"
            + "10%:  {{grav10Liq}}\n"
            + " 5%:   {{grav5Liq}}\n"
            + "----------------------------------------\n"
            + "RUC: {{rucCliente}}\n"
            + "CLIENTE: \n"
            + "{{cliente}}\n"
            + "----------------------------------------\n"
            + "ARTICULOS: {{cant}}TOTAL: {{total}}\n"
            + "DESCUENTO: {{desc}} NETO: {{neto}}\n"
            + "----------------------------------------\n"
            + "{{efectivo}}"
            + "{{dolar}}"
            + "{{peso}}"
            + "{{real}}"
            + "{{tarjCred}}"
            + "{{tarjDeb}}"
            + "{{cheque}}"
            + "{{vale}}"
            + "{{asoc}}"
            + "{{notCre}}"
            + "{{retencion}}"
            + "{{giftcard}}"
            + "DONACION: {{redondeo}}\n"
            + "VUELTO: {{vuelto}}"
            + (char) 27 + (char) 100 + (char) 13
            + (char) 27 + (char) 105
            + (char) 0;

    private String contentSlipMay
            = "\n"
            + "\n"
            + "\n"
            + "                                                                   INICIO VIGENCIA: {{desde}}\n"
            + "                                                                   FIN VIGENCIA: {{hasta}}\n"
            + "                                                                RUC: {{ruc}} TIMBRADO {{timb}}\n"
            + "\n"
            + "\n"
            + "                                                                            {{factNro}}\n"
            + "\n"
            + "               {{fH}}    {{tipoComprobante}}\n"
            + "                 {{rucCliente}}\n"
            + "                 {{cliente}}\n"
            + "\n"
            + "\n"
            + "{{art}}\n"
            + "{{formatoLetras}} {{neto}}\n\n"
            + "{{grav5Liq}}{{grav10Liq}}{{totalIVA}}\n"
            + (char) 27 + (char) 100 + (char) 10 + (char) 0 + (char) 27 + (char) 105;

    public Ticket() {
    }

    public void slipCheque(String empresa, String cheque, String nroCaja, String usuario, String factNro, String fecha) {
        usuario = Utilidades.encodingAlambrado(usuario.toUpperCase());
        empresa = Utilidades.encodingAlambrado(empresa.toUpperCase());
        if (cheque.contentEquals("Gs 0")) {
            cheque = "";
        } else {
            cheque = cheque + "\n";
            cheque = cheque.replace("Gs", "GUARANIES");
        }
        this.chequeSlip = this.chequeSlip.replace("{{empresa}}", empresa);
        this.chequeSlip = this.chequeSlip.replace("{{cheque}}", cheque);
        this.chequeSlip = this.chequeSlip.replace("{{nroCaja}}", nroCaja);
        this.chequeSlip = this.chequeSlip.replace("{{usuario}}", usuario);
        this.chequeSlip = this.chequeSlip.replace("{{factNro}}", factNro);
        this.chequeSlip = this.chequeSlip.replace("{{fecha}}", fecha);
        this.slip = true;
        this.bytes = this.chequeSlip.getBytes();
    }

    //El constructor que setea los valores a la instancia
    public Ticket(String empresa, String suc, String ruc, String tel, String dir, String timb, String desde, String hasta,
            String nroCaja, String fH, String factNro, String user, String articulos, String grav10, String grav5, String exenta,
            String grav10Liq, String grav5Liq, String rucCliente, String cliente, String cant, String total, String desc,
            String neto, String efectivo, String tarjCred, String tarjDeb, String cheque, String vale, String asoc, String notCre, String redondeo,
            String vuelto, String dolar, String peso, String real, String retencion, String estado, String giftcard) {
        this.contentTicket = this.contentTicket.replace("{{empresa}}", empresa);
        this.contentTicket = this.contentTicket.replace("{{suc}}", suc);
        this.contentTicket = this.contentTicket.replace("{{ruc}}", ruc);
        this.contentTicket = this.contentTicket.replace("{{tel}}", tel);
        this.contentTicket = this.contentTicket.replace("{{dir}}", dir);
        this.contentTicket = this.contentTicket.replace("{{timb}}", timb);
        this.contentTicket = this.contentTicket.replace("{{desde}}", desde);
        this.contentTicket = this.contentTicket.replace("{{hasta}}", hasta);
        this.contentTicket = this.contentTicket.replace("{{cj}}", nroCaja);
        this.contentTicket = this.contentTicket.replace("{{fH}}", fH);
        this.contentTicket = this.contentTicket.replace("{{factNro}}", factNro);
        this.contentTicket = this.contentTicket.replace("{{usuario}}", user);
        this.contentTicket = this.contentTicket.replace("{{art}}", articulos);
        this.contentTicket = this.contentTicket.replace("{{grav10}}", grav10);
        this.contentTicket = this.contentTicket.replace("{{grav5}}", grav5);
        this.contentTicket = this.contentTicket.replace("{{exenta}}", exenta);
        this.contentTicket = this.contentTicket.replace("{{grav10Liq}}", grav10Liq);
        this.contentTicket = this.contentTicket.replace("{{grav5Liq}}", grav5Liq);
        this.contentTicket = this.contentTicket.replace("{{rucCliente}}", rucCliente);
        this.contentTicket = this.contentTicket.replace("{{cliente}}", cliente);
        this.contentTicket = this.contentTicket.replace("{{cant}}", cant);
        this.contentTicket = this.contentTicket.replace("{{total}}", total);
        this.contentTicket = this.contentTicket.replace("{{desc}}", desc);
        this.contentTicket = this.contentTicket.replace("{{neto}}", neto);
        //**********************************************************************
        if (efectivo.contentEquals("Gs 0")) {
            efectivo = "";
        } else {
            efectivo = "EFECTIVO: " + efectivo + "\n";
        }
//        if (!giftcard.equals("")) {
//            giftcard = "GIFTCARD: " + giftcard + "\n";
//        }
        if (tarjCred.contentEquals("Gs 0")) {
            tarjCred = "";
        } else {
            tarjCred = "TARJ. CRED: " + tarjCred + "\n";
        }
        if (tarjDeb.contentEquals("Gs 0")) {
            tarjDeb = "";
        } else {
            tarjDeb = "TARJ. DEB: " + tarjDeb + "\n";
        }
        if (cheque.contentEquals("Gs 0")) {
            cheque = "";
        } else {
            cheque = "CHEQUE: " + cheque + "\n";
        }
        if (vale.contentEquals("Gs 0")) {
            vale = "";
        } else {
            vale = "VALE: " + vale + "\n";
        }
        if (asoc.contentEquals("Gs 0")) {
            asoc = "";
        } else {
            asoc = "ASOCIACION: " + asoc + "\n";
        }
        if (notCre.contentEquals("Gs 0")) {
            notCre = "";
        } else {
            notCre = "NOTA CREDITO: " + notCre + "\n";
        }
        if (retencion.contentEquals("Gs 0")) {
            retencion = "";
        } else {
            retencion = "RETENCION: " + retencion + "\n";
        }
        if (peso == null) {
            peso = "";
        }
        if (real == null) {
            real = "";
        }
        if (dolar == null) {
            dolar = "";
        }
        //cupones
        numValidator = new NumberValidator();
//        if (Long.parseLong(numValidator.numberValidator(total)) >= 50000 && !CalculoCupones.getMapeo().get("ci").toString().equalsIgnoreCase("") && !CalculoCupones.getMapeo().get("cliente").toString().equalsIgnoreCase("")) {
        if (!CalculoCupones.getMapeo().isEmpty()) {
            if (!CalculoCupones.getMapeo().get("ci").toString().equalsIgnoreCase("") && !CalculoCupones.getMapeo().get("cliente").toString().equalsIgnoreCase("")) {
                String cuponesData = "----------------------------------------\n"
                        + "****CASA PARANA****\n"
                        + "CI: " + CalculoCupones.getMapeo().get("ci").toString() + "\n"
                        + "NOMBRE: " + CalculoCupones.getMapeo().get("cliente").toString().toUpperCase() + "\n"
                        + "CUPONES ELECTRONICOS: " + CalculoCupones.getMapeo().get("cantidadCupones").toString() + " \n"
                        + "!!!!TU CUPON YA SE HA REGISTRADO!!!!\n";
                this.contentTicket = this.contentTicket.replace("{{cupones}}", cuponesData);
                CalculoCupones.resetMap();
            } else {
                this.contentTicket = this.contentTicket.replace("{{cupones}}", "");
            }
        } else {
            this.contentTicket = this.contentTicket.replace("{{cupones}}", "");
        }

        //NUEVO retiro pedido
        String numPedido = "----------------------------------------\n"
                + "        ****NUMERO DE PEDIDO****\n"
                + "  *************** " + giftcard + " *************\n";
        this.contentTicket = this.contentTicket.replace("{{numPedido}}", numPedido);
        //FIN retiro pedido

        if (MensajeFinalVenta.estadoCrucijuegos) {
            String articu = "";
            int size = FacturaDeVentaFXMLController.arrayListadoPromoConDesc.size();
            for (int i = 0; i < FacturaDeVentaFXMLController.arrayListadoPromoConDesc.size(); i++) {
                size--;
                StringTokenizer st = new StringTokenizer(FacturaDeVentaFXMLController.arrayListadoPromoConDesc.get(i).toString(), "-");
//                ord + "-" + cod + "-" + precio + "-" + (Long.parseLong(precio) * 0.5) + "-50"
                String ord = st.nextElement().toString();
                String cod = st.nextElement().toString();
                String precio = st.nextElement().toString();

                String descuen = st.nextElement().toString();
                String porc = st.nextElement().toString();
                articu += ord + ". " + cod + " " + articuloDAO.buscarCod(cod).getDescripcion().substring(0, 17) + " " + porc + "%\n"
                        + "     " + porc + " X     " + numValidator.numberFormat("###,###.###", Double.parseDouble(precio)) + "     " + numValidator.numberFormat("###,###.###", Double.parseDouble(descuen)) + " Gs.";
                if (size != 0) {
                    articu = articu + "\n";
                }

            }
            String crucijuegosData = "\n----------------------------------------\n"
                    + "         ññ +COMPRAS +AHORRAS ññ     \n"
                    + "----------------------------------------\n"
                    + "Articulo Precio  %   Descuento\n"
                    + "----------------------------------------\n"
                    + articu + "\n"
                    + "----------------------------------------\n"
                    + "TOTAL MONT :     " + numValidator.numberFormat("###,###.###", FacturaDeVentaFXMLController.descPromo) + " Gs." + "\n"
                    + "----------------------------------------\n";
            this.contentTicket = this.contentTicket.replace("{{crucijuegos}}", crucijuegosData);
            MensajeFinalVentaFXMLController.estadoCrucijuegos = false;
        } else {
            this.contentTicket = this.contentTicket.replace("{{crucijuegos}}", "");
        }

        //ORIGINAL
//        if (MensajeFinalVentaFXMLController.estadoCrucijuegos) {
//            String crucijuegosData = "----------------------------------------\n"
//                    + "****** CLUB PARANA - CRUCIJUEGOS ******\n"
//                    + "** VALE 10.000 GS. DE CARGA **\n"
//                    + "** HASTA 13/12/2017 **\n";
//            this.contentTicket = this.contentTicket.replace("{{crucijuegos}}", crucijuegosData);
//            MensajeFinalVentaFXMLController.estadoCrucijuegos = false;
//        } else {
//            this.contentTicket = this.contentTicket.replace("{{crucijuegos}}", "");
//        }
        this.contentTicket = this.contentTicket.replace("{{efectivo}}", efectivo);
        this.contentTicket = this.contentTicket.replace("{{giftcard}}", giftcard);
        this.contentTicket = this.contentTicket.replace("{{tarjCred}}", tarjCred);
        this.contentTicket = this.contentTicket.replace("{{tarjDeb}}", tarjDeb);
        this.contentTicket = this.contentTicket.replace("{{cheque}}", cheque);
        this.contentTicket = this.contentTicket.replace("{{vale}}", vale);
        this.contentTicket = this.contentTicket.replace("{{asoc}}", asoc);
        this.contentTicket = this.contentTicket.replace("{{notCre}}", notCre);
        this.contentTicket = this.contentTicket.replace("{{dolar}}", dolar);
        this.contentTicket = this.contentTicket.replace("{{peso}}", peso);
        this.contentTicket = this.contentTicket.replace("{{real}}", real);
        this.contentTicket = this.contentTicket.replace("{{retencion}}", retencion);
        //**********************************************************************
        this.contentTicket = this.contentTicket.replace("{{redondeo}}", redondeo);
        this.contentTicket = this.contentTicket.replace("{{vuelto}}", vuelto);
        this.contentTicket = this.contentTicket.replace("{{estado}}", estado);
        slip = false;
        this.bytes = this.contentTicket.getBytes();
        Utilidades.log.info(contentTicket);
    }

    public void ticketReimpresion(String empresa, String suc, String ruc, String tel, String dir, String timb, String desde, String hasta,
            String nroCaja, String fH, String factNro, String user, String articulos, String grav10, String grav5, String exenta,
            String grav10Liq, String grav5Liq, String rucCliente, String cliente, String cant, String total, String desc,
            String neto, String efectivo, String tarjCred, String tarjDeb, String cheque, String vale, String asoc, String notCre, String redondeo,
            String vuelto, String dolar, String peso, String real, String retencion, String estado, boolean duplicado, String giftcard) {
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{empresa}}", empresa);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{suc}}", suc);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{ruc}}", ruc);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{tel}}", tel);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{dir}}", dir);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{timb}}", timb);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{desde}}", desde);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{hasta}}", hasta);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{cj}}", nroCaja);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{fH}}", fH);
        if (duplicado) {
            this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{dup}}", "**DUPLICADO**");
        } else {
            this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{dup}}", "**DUPLICADO**\n** SIN VALOR COMERCIAL **");
        }
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{factNro}}", factNro);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{usuario}}", user);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{art}}", articulos);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{grav10}}", grav10);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{grav5}}", grav5);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{exenta}}", exenta);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{grav10Liq}}", grav10Liq);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{grav5Liq}}", grav5Liq);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{rucCliente}}", rucCliente);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{cliente}}", cliente);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{cant}}", cant);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{total}}", total);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{desc}}", desc);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{neto}}", neto);
        //**********************************************************************
        if (efectivo.contentEquals("Gs 0")) {
            efectivo = "";
        } else {
            efectivo = "EFECTIVO: " + efectivo + "\n";
        }
        if (giftcard.contentEquals("Gs 0")) {
            giftcard = "";
        } else {
            giftcard = "GIFTCARD: " + giftcard + "\n";
        }
        if (tarjCred.contentEquals("Gs 0")) {
            tarjCred = "";
        } else {
            tarjCred = "TARJ. CRED: " + tarjCred + "\n";
        }
        if (tarjDeb.contentEquals("Gs 0")) {
            tarjDeb = "";
        } else {
            tarjDeb = "TARJ. DEB: " + tarjDeb + "\n";
        }
        if (cheque.contentEquals("Gs 0")) {
            cheque = "";
        } else {
            cheque = "CHEQUE: " + cheque + "\n";
        }
        if (vale.contentEquals("Gs 0")) {
            vale = "";
        } else {
            vale = "VALE: " + vale + "\n";
        }
        if (asoc.contentEquals("Gs 0")) {
            asoc = "";
        } else {
            asoc = "ASOCIACION: " + asoc + "\n";
        }
        if (notCre.contentEquals("Gs 0")) {
            notCre = "";
        } else {
            notCre = "NOTA CREDITO: " + notCre + "\n";
        }
        if (retencion.contentEquals("Gs 0")) {
            retencion = "";
        } else {
            retencion = "RETENCION: " + retencion + "\n";
        }
        if (peso == null) {
            peso = "";
        }
        if (real == null) {
            real = "";
        }
        if (dolar == null) {
            dolar = "";
        }
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{efectivo}}", efectivo);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{giftcard}}", giftcard);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{tarjCred}}", tarjCred);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{tarjDeb}}", tarjDeb);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{cheque}}", cheque);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{vale}}", vale);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{asoc}}", asoc);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{notCre}}", notCre);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{dolar}}", dolar);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{peso}}", peso);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{real}}", real);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{retencion}}", retencion);
        //**********************************************************************
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{redondeo}}", redondeo);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{vuelto}}", vuelto);
        this.contentTicketReimpresion = this.contentTicketReimpresion.replace("{{estado}}", estado);
        slip = false;
        this.bytes = this.contentTicketReimpresion.getBytes();
        Utilidades.log.info(contentTicketReimpresion);
    }

    public void ticketMayorista(String tipoComprobante, String ruc, String timb, String desde, String hasta,
            String fH, String factNro, String articulos, String grav10, String grav5, String exenta,
            String grav10Liq, String grav5Liq, String rucCliente, String cliente, String total, String desc,
            String neto) {
        //**********************************************************************
        this.contentSlipMay = this.contentSlipMay.replace("{{ruc}}", ruc);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{timb}}", timb);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{desde}}", desde);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{hasta}}", hasta);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{fH}}", fH);//sí, con otro formato
        this.contentSlipMay = this.contentSlipMay.replace("{{factNro}}", factNro);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{art}}", articulos);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{grav10}}", grav10);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{grav5}}", grav5);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{exenta}}", exenta);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{grav10Liq}}", grav10Liq);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{grav5Liq}}", grav5Liq);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{rucCliente}}", rucCliente);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{cliente}}", cliente);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{total}}", total);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{desc}}", desc);//sí
        this.contentSlipMay = this.contentSlipMay.replace("{{neto}}", neto);//sí
        if (tipoComprobante.contentEquals("CONT")) {
            this.contentSlipMay = this.contentSlipMay.replace("{{tipoComprobante}}", "XX");
        } else {
            this.contentSlipMay = this.contentSlipMay.replace("{{tipoComprobante}}", "          XX");
        }
        String formatoLetras = "Son guaraníes ";
        numValidator = new NumberValidator();
        int netoLetra = Integer.valueOf(numValidator.numberValidator(neto));
        n2t numero = new n2t(netoLetra);
        formatoLetras = formatoLetras + numero.convertirLetras(netoLetra);
        if (formatoLetras.length() < 71) {
            formatoLetras = formatoLetras + " ";
        }
        this.contentSlipMay = this.contentSlipMay.replace("{{formatoLetras}}", formatoLetras);//sí
        String totalIvaAux = String.valueOf((Integer.valueOf(grav10Liq) + Integer.valueOf(grav5Liq)));
        String totalIVA = numValidator.numberFormat("###,###.###", Double.parseDouble(totalIvaAux));
        this.contentSlipMay = this.contentSlipMay.replace("{{totalIVA}}", totalIVA);//sí
        //**********************************************************************
        this.bytes = this.contentSlipMay.getBytes();
        Utilidades.log.info(contentSlipMay);
    }

    public void ticketRetiro(String empresa, String suc, String ruc, String tel, String dir, String fecha, String hora, String supervisor,
            String cajaNro, String cajero, String retiro) {
        this.retiroTicket = this.retiroTicket.replace("{{empresa}}", empresa);
        this.retiroTicket = this.retiroTicket.replace("{{suc}}", suc);
        this.retiroTicket = this.retiroTicket.replace("{{ruc}}", ruc);
        this.retiroTicket = this.retiroTicket.replace("{{tel}}", tel);
        this.retiroTicket = this.retiroTicket.replace("{{dir}}", dir);
        this.retiroTicket = this.retiroTicket.replace("{{empresa}}", empresa);
        this.retiroTicket = this.retiroTicket.replace("{{fecha}}", fecha);
        this.retiroTicket = this.retiroTicket.replace("{{hora}}", hora);
        this.retiroTicket = this.retiroTicket.replace("{{supervisor}}", supervisor);
        this.retiroTicket = this.retiroTicket.replace("{{cajaNro}}", cajaNro);
        this.retiroTicket = this.retiroTicket.replace("{{cajero}}", cajero);
        this.retiroTicket = this.retiroTicket.replace("{{retiro}}", retiro);
        slip = false;
        this.bytes = this.retiroTicket.getBytes();
        Utilidades.log.info(retiroTicket);
    }

    public void ticketCierre(String empresa, String suc, String ruc, String tel, String dir, String fecha, String totalRendir) {
        this.cierreTicket = this.cierreTicket.replace("{{empresa}}", empresa);
        this.cierreTicket = this.cierreTicket.replace("{{suc}}", suc);
        this.cierreTicket = this.cierreTicket.replace("{{ruc}}", ruc);
        this.cierreTicket = this.cierreTicket.replace("{{tel}}", tel);
        this.cierreTicket = this.cierreTicket.replace("{{dir}}", dir);
        this.cierreTicket = this.cierreTicket.replace("{{fecha}}", fecha);
        DecimalFormat formateador = new DecimalFormat("###,###.##");
        if (Integer.parseInt(totalRendir) < 0) {
            this.cierreTicket = this.cierreTicket.replace("{{Total a rendir}}", "Total sobrante");
            Object rendicion = (Object) (Integer.parseInt(totalRendir) * -1);
            String rendicionSeparados = formateador.format(rendicion);
            this.cierreTicket = this.cierreTicket.replace("{{totalRendir}}", rendicionSeparados);
        } else {
            this.cierreTicket = this.cierreTicket.replace("{{Total a rendir}}", "Total a rendir");
            Object rendicion = (Object) (Integer.parseInt(totalRendir));
            String rendicionSeparados = formateador.format(rendicion);
            this.cierreTicket = this.cierreTicket.replace("{{totalRendir}}", rendicionSeparados);
        }
        slip = false;
        this.bytes = this.cierreTicket.getBytes();
        Utilidades.log.info(cierreTicket);
    }

    public void ticketArqueo() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        this.cierreArqueo = this.cierreArqueo.replace("{{datoArqueo}}", "CIERRE VENTA     " + dateFormat.format(date));
        slip = false;
        this.bytes = this.cierreArqueo.getBytes();
        Utilidades.log.info(cierreArqueo);
    }

    public void print() {
        DocFlavor flavor = DocFlavor.BYTE_ARRAY.AUTOSENSE;
        Doc doc = new SimpleDoc(bytes, flavor, null);
        AttributeSet attributes = new HashPrintServiceAttributeSet(
                new PrinterName(Utilidades.IMPRESORA, Locale.getDefault()));
        PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, attributes);
        job = null;
        if (services.length > 0) {
            for (int i = 0; i < services.length; i++) {
                if (services[i].getName().equals(Utilidades.IMPRESORA)) {
                    job = services[i].createPrintJob();
//                    verificandoColaImpresora(services[i]);
                }
            }
        }
//        if (job != null) { ORGINAL, PARA QUE CANCELE LAS DEMAS COLAS DE IMPRESIONES.
//            try {
//                job.print(doc, null);
//                if (!slip) {
//                    timer = new Timer();
//                    timer.schedule(new PrinterHandler(), 3000, 10000);
//                }
//            } catch (PrintException ex) {
//                cancelPrinting();
//                Utilidades.log.error("ERROR PrintException: ", ex.fillInStackTrace());
//            }
//        }
        if (job != null) {
            try {
                job.print(doc, null);
//                if (!slip) {
//                    timer = new Timer();
//                    timer.schedule(new PrinterHandler(), 3000, 10000);
//                }
            } catch (PrintException ex) {
//                cancelPrinting();
                Utilidades.log.error("ERROR PrintException: ", ex.fillInStackTrace());
            }
        }

    }

    private static void cancelPrinting() {
        try {
            CancelablePrintJob cancelableJob = (CancelablePrintJob) job;
            cancelableJob.cancel();
        } catch (PrintException ex) {
            Utilidades.log.error("ERROR PrintException: ", ex.fillInStackTrace());

        }
    }

    private void verificandoColaImpresora(PrintService printService) {
        if (Integer.valueOf(printService.getAttribute(QueuedJobCount.class).toString()) > 1) {
            cancelPrinting();
        }
    }
}
