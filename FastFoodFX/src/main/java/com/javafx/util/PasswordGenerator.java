package com.javafx.util;

public class PasswordGenerator {

    public static String NUMEROS = "123456789";
    public static String MAYUSCULAS = "ABCDEFGHJKLMNPQRSTUVWXYZ";
    public static String MINUSCULAS = "abcdefghijkmnpqrstuvwxyz";
//    public static String ESPECIALES = "";

    public PasswordGenerator() {
    }

    public static String getPassword() {
        return getPassword(6);
    }

    public static String getPassword(int length) {
        return getPassword(NUMEROS + MAYUSCULAS + MINUSCULAS, length);
    }

    public static String getPassword(String key, int length) {
        String pswd = "";
        for (int i = 0; i < length; i++) {
            pswd += (key.charAt((int) (Math.random() * key.length())));
        }
        return pswd;
    }
}
