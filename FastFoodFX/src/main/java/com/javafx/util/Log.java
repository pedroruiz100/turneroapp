/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 *
 * @author ExcelsisWalker
 */
public class Log {

    static public Logger logger = Logger.getLogger("frontLog");
    static FileHandler fh;

    public Log() {
        try {
            fh = new FileHandler("C:/log/front.log");
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);
            logger.info("############################################## INICIO FRONTEND ##############################################");
        } catch (IOException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        } catch (SecurityException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
}
