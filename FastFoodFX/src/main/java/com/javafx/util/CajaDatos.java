/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author ExcelsisWalker
 */
public class CajaDatos {

    private static JSONObject caja;

    private JSONArray talonarioSucursalArray;
    private JSONObject talonarioSucursalAux;
    private JSONObject ipBocaCaja;
    private JSONObject ipBocaTalonario;
    //datos varios para factura...
    private static JSONObject talonarioSucursal;
    private static JSONObject sucursal;
    private static JSONObject empresa;
    private static JSONObject ciudad;
    private static JSONObject timbrado;
    //datos varios para factura...

    //datos para Apertura y Cierre de Caja
    private static JSONObject aperturaCaja;
    private static int montoFacturado = 0; //Monto Facturado de manerra global dentro de esta caja
    private static int donaciones = 0; // Donaciones Globales
    private static int retiroDinero = 0;//Todos los retiros de dinero que se realizo

    private static int montoCajero; // Monto Facturado por cajero que inicia sesión en el módulo Caja
    private static int donacionCajero;// Donaciones por cajeros
    private static int retiroDineroCajero;//Retiro de Dinero Por Cajero

    private boolean primera;
    private static boolean rendicion;
    private static boolean estadoInformeFinanciero;

    private static long zeta;
    private static String facturaInicial;
    private static String facturaFinal;

    //Estados de las facturasiniciales y finales
    private static boolean estadoFacturaInicial = false;

    private static String nroComprobante;

//    DATOS PARA ARQUEO
    private static int contDesc = 0; // CONTADOR DE DESCUENTOS DENTRO DE ESTA CAJA
    private static int sumDesc = 0;  // SUMADOR DE DESCUENTOS DENTRO DE ESTA CAJA

    private static int contTarj = 0;
    private static int sumTarj = 0;

    //Nuevos 
    private static int contEfectivo = 0;
    private static int sumEfectivo = 0;

    private static int totales = 0;
    private static int nClientes = 0;
    private static int nFuncionarios = 0;
    private static int nArticulos = 0;

    private static int gra5 = 0;
    private static int gra10 = 0;
    private static int exe = 0;
    private static int gra = 0;

    private static int contDonaciones = 0;
    private static int sumDonaciones = 0;

    private static int contNotaCred = 0;
    private static int sumNotaCred = 0;
    private static int contFact = 0;
    private static int sumFact = 0;
    private static int contCheque = 0;
    private static int sumCheque = 0;
    private static int contVale = 0;
    private static int sumVale = 0;
    private static int contAsoc = 0;
    private static int sumAsoc = 0;

    private static int contDolar = 0;
    private static int sumDolar = 0;
    private static int contReal = 0;
    private static int sumReal = 0;
    private static int contPeso = 0;
    private static int contRetencion = 0;
    private static int sumPeso = 0;
    private static int sumRetencion = 0;
    private static int sumEfeRecibido = 0;

    private static String uuidCassandraActual = "";
    private static long idRangoFacturaActual = 0L;
    //id que se persisto en el servidor....
    private static long idFactClienteCabServidor = 0L;
    private static boolean insercionFacturaVentaCab = false;
    private static boolean actualizacionLocal = false;
    private static boolean insercionFacturaVentaCabLocal = false;

    //PARA LOS TIPO DE DESCUENTOS, recuperando los id insertados de la cabecera para almacenarlos en el detalle
    private static long idDescuentoClienteFiel = 0;
    private static long idDescuentoTarjetaConvenio = 0;
    private static long idPromoTemporada = 0;
    private static long idDescuentoTarjeta = 0;
    private static long idDescuentoFuncionario = 0;
    private static long idPromoTemporadaArt = 0;
    private static long idDescuentoFuncionarioNf1 = 0;
    private static long idDescuentoFuncionarioNf2 = 0;
    private static long idDescuentoFuncionarioNf3 = 0;
    private static long idDescuentoFuncionarioNf4 = 0;
    private static long idDescuentoFuncionarioNf5 = 0;
    private static long idDescuentoFuncionarioNf6 = 0;
    private static long idDescuentoFuncionarioNf7 = 0;
    //para detalle proveedor, más adelante...
    //se toma de dto. fiel registro 999
    private static long idCuponera = 0;

    private static String nroFact = "";
    private static boolean ventaServer = false;
    private static boolean descuentoClienteFielCab = false;
    private static boolean descuentoTarjetaCab = false;
    private static boolean descuentoTarjetaConvCab = false;
    private static boolean descuentoPromo = false;
    private static boolean descuentoPromoArt = false;
    private static boolean cuponera = false;

    private static boolean facturaCheque = false;
    private static boolean facturaEfectivo = false;
    private static boolean facturaFuncionario = false;
    private static boolean facturaMonedaExtranjera = false;
    private static boolean facturaPromo = false;
    private static boolean facturaTarjeta = false;
    private static boolean facturaTarjetaConv = false;
    private static boolean facturaTarjetaFiel = false;

    //**************************************************************************
    public void mapeandoCaja(JSONObject caja) {
        mapCaja(caja);
    }

    private void mapCaja(JSONObject caja) {
        primera = true;
        this.caja = caja;//caja
        ipBocaCaja = (JSONObject) this.caja.get("ipBoca");//ip caja
        sucursal = (JSONObject) this.caja.get("sucursal");//sucursal
        ciudad = (JSONObject) sucursal.get("ciudadDTO");//ciudad
        empresa = (JSONObject) sucursal.get("empresaDTO");//empresa
        talonarioSucursalArray = (JSONArray) sucursal.get("talonariosSucursalesDTO");
        for (Object objectTalonario : talonarioSucursalArray) {
            talonarioSucursalAux = (JSONObject) objectTalonario;
            ipBocaTalonario = (JSONObject) talonarioSucursalAux.get("ipBoca");//ip talonario, comparativa con ip caja...
            if ((long) ipBocaCaja.get("idIpBoca") == (long) ipBocaTalonario.get("idIpBoca")) {
                if (primera) {//pueden existir varios timbrados, se obtiene el más "actual"...
                    timbrado = (JSONObject) talonarioSucursalAux.get("timbrado");
                    talonarioSucursal = (JSONObject) talonarioSucursalAux;
                    primera = false;
                } else {
                    try {
                        JSONObject jsontTimbrado = (JSONObject) talonarioSucursalAux.get("timbrado");
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        Date dateVenNew = sdf.parse(jsontTimbrado.get("fecVencimiento").toString());
                        Date dateVenOld = sdf.parse(getTimbrado().get("fecVencimiento").toString());
                        if (dateVenNew.after(dateVenOld)) {//más actual...
                            timbrado = jsontTimbrado;
                            talonarioSucursal = (JSONObject) talonarioSucursalAux;
                        }
                    } catch (ParseException ex) {
                        Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
                    }
                }
            }
        }
    }
    //**************************************************************************

    //**************************************************************************
    public static JSONObject onlyCaja() {
        JSONObject cajaOnly = new JSONObject();
        if (caja != null) {
            cajaOnly.put("idCaja", caja.get("idCaja"));
        }
        return cajaOnly;
    }

    public static void generandoNroComprobante() {
        numeracion();
    }

    private static void numeracion() {
        String suc = sucursal.get("idSucursal").toString();
        int sucLength = suc.length();
        String caj = caja.get("nroCaja").toString();
        int cajLength = caj.length();
        String fac = talonarioSucursal.get("nroActual").toString();
        int facLength = fac.length();
        while (sucLength < 3) {
            sucLength++;
            suc = "0" + suc;
        }
        while (cajLength < 3) {
            cajLength++;
            caj = "0" + caj;
        }
        while (facLength < 7) {
            facLength++;
            fac = "0" + fac;
        }
        nroComprobante = suc + "-" + caj + "-" + fac;
        //primera instancia, se prepara nroActual para la siguiente...
        talonarioSucursal.put("nroActual", ((long) talonarioSucursal.get("nroActual") + 1));
    }

    public static JSONObject getTalonarioSucursal() {
        return talonarioSucursal;
    }

    public static JSONObject getSucursal() {
        return sucursal;
    }

    public static JSONObject getEmpresa() {
        return empresa;
    }

    public static JSONObject getCiudad() {
        return ciudad;
    }

    public static long getZeta() {
        return zeta;
    }

    public static void setZeta(long zeta) {
        CajaDatos.zeta = zeta;
    }

    public static String getFacturaInicial() {
        return facturaInicial;
    }

    public static void setFacturaInicial(String facturaInicial) {
        CajaDatos.facturaInicial = facturaInicial;
    }

    public static String getFacturaFinal() {
        return facturaFinal;
    }

    public static boolean isEstadoFacturaInicial() {
        return estadoFacturaInicial;
    }

    public static void setEstadoFacturaInicial(boolean estadoFacturaInicial) {
        CajaDatos.estadoFacturaInicial = estadoFacturaInicial;
    }

    public static void setFacturaFinal(String facturaFinal) {
        CajaDatos.facturaFinal = facturaFinal;
    }

    public static JSONObject getTimbrado() {
        return timbrado;
    }

    public static JSONObject getCaja() {
        return caja;
    }
    //**************************************************************************

    public static JSONObject getAperturaCaja() {
        return aperturaCaja;
    }

    public static void setAperturaCaja(JSONObject aperturaCaja) {
        CajaDatos.aperturaCaja = aperturaCaja;
    }

    public static void setCaja(JSONObject caja) {
        CajaDatos.caja = caja;
    }

    public static int getRetiroDinero() {
        return retiroDinero;
    }

    public static void setRetiroDinero(int retiroDinero) {
        CajaDatos.retiroDinero = retiroDinero;
    }

    public static int getMontoFacturado() {
        return montoFacturado;
    }

    public static void setMontoFacturado(int montoFacturado) {
        CajaDatos.montoFacturado = montoFacturado;
    }

    public static int getDonaciones() {
        return donaciones;
    }

    public static void setDonaciones(int donaciones) {
        CajaDatos.donaciones = donaciones;
    }

    public static int getMontoCajero() {
        return montoCajero;
    }

    public static void setMontoCajero(int montoCajero) {
        CajaDatos.montoCajero = montoCajero;
    }

    public static int getDonacionCajero() {
        return donacionCajero;
    }

    public static boolean isRendicion() {
        return rendicion;
    }

    public static void setRendicion(boolean rendicion) {
        CajaDatos.rendicion = rendicion;
    }

    public static void setDonacionCajero(int donacionCajero) {
        CajaDatos.donacionCajero = donacionCajero;
    }

    public static int getRetiroDineroCajero() {
        return retiroDineroCajero;
    }

    public static void setRetiroDineroCajero(int retiroDineroCajero) {
        CajaDatos.retiroDineroCajero = retiroDineroCajero;
    }

    public static boolean isEstadoInformeFinanciero() {
        return estadoInformeFinanciero;
    }

    public static void setEstadoInformeFinanciero(boolean estadoInformeFinanciero) {
        CajaDatos.estadoInformeFinanciero = estadoInformeFinanciero;
    }

    public static int getContDesc() {
        return contDesc;
    }

    public static void setContDesc(int contDesc) {
        CajaDatos.contDesc = contDesc;
    }

    public static int getSumDesc() {
        return sumDesc;
    }

    public static void setSumDesc(int sumDesc) {
        CajaDatos.sumDesc = sumDesc;
    }

    public static int getContTarj() {
        return contTarj;
    }

    public static void setContTarj(int contTarj) {
        CajaDatos.contTarj = contTarj;
    }

    public static int getSumTarj() {
        return sumTarj;
    }

    public static void setSumTarj(int sumTarj) {
        CajaDatos.sumTarj = sumTarj;
    }

    public static int getContEfectivo() {
        return contEfectivo;
    }

    public static void setContEfectivo(int contEfectivo) {
        CajaDatos.contEfectivo = contEfectivo;
    }

    public static int getSumEfectivo() {
        return sumEfectivo;
    }

    public static void setSumEfectivo(int sumEfectivo) {
        CajaDatos.sumEfectivo = sumEfectivo;
    }

    public static int getTotales() {
        return totales;
    }

    public static void setTotales(int totales) {
        CajaDatos.totales = totales;
    }

    public static int getnClientes() {
        return nClientes;
    }

    public static void setnClientes(int nClientes) {
        CajaDatos.nClientes = nClientes;
    }

    public static int getnFuncionarios() {
        return nFuncionarios;
    }

    public static void setnFuncionarios(int nFuncionarios) {
        CajaDatos.nFuncionarios = nFuncionarios;
    }

    public static int getnArticulos() {
        return nArticulos;
    }

    public static void setnArticulos(int nArticulos) {
        CajaDatos.nArticulos = nArticulos;
    }

    public static int getGra5() {
        return gra5;
    }

    public static void setGra5(int gra5) {
        CajaDatos.gra5 = gra5;
    }

    public static int getGra10() {
        return gra10;
    }

    public static void setGra10(int gra10) {
        CajaDatos.gra10 = gra10;
    }

    public static int getExe() {
        return exe;
    }

    public static void setExe(int exe) {
        CajaDatos.exe = exe;
    }

    public static int getContDonaciones() {
        return contDonaciones;
    }

    public static void setContDonaciones(int contDonaciones) {
        CajaDatos.contDonaciones = contDonaciones;
    }

    public static int getSumDonaciones() {
        return sumDonaciones;
    }

    public static void setSumDonaciones(int sumDonaciones) {
        CajaDatos.sumDonaciones = sumDonaciones;
    }

    public static int getGra() {
        return gra;
    }

    public static void setGra(int gra) {
        CajaDatos.gra = gra;
    }

    public static int getContNotaCred() {
        return contNotaCred;
    }

    public static void setContNotaCred(int contNotaCred) {
        CajaDatos.contNotaCred = contNotaCred;
    }

    public static int getSumNotaCred() {
        return sumNotaCred;
    }

    public static void setSumNotaCred(int sumNotaCred) {
        CajaDatos.sumNotaCred = sumNotaCred;
    }

    public static int getContFact() {
        return contFact;
    }

    public static void setContFact(int contFact) {
        CajaDatos.contFact = contFact;
    }

    public static int getSumFact() {
        return sumFact;
    }

    public static void setSumFact(int sumFact) {
        CajaDatos.sumFact = sumFact;
    }

    public static int getContCheque() {
        return contCheque;
    }

    public static void setContCheque(int contCheque) {
        CajaDatos.contCheque = contCheque;
    }

    public static int getSumCheque() {
        return sumCheque;
    }

    public static void setSumCheque(int sumCheque) {
        CajaDatos.sumCheque = sumCheque;
    }

    public static int getContVale() {
        return contVale;
    }

    public static void setContVale(int contVale) {
        CajaDatos.contVale = contVale;
    }

    public static int getSumVale() {
        return sumVale;
    }

    public static void setSumVale(int sumVale) {
        CajaDatos.sumVale = sumVale;
    }

    public static int getContAsoc() {
        return contAsoc;
    }

    public static void setContAsoc(int contAsoc) {
        CajaDatos.contAsoc = contAsoc;
    }

    public static int getSumAsoc() {
        return sumAsoc;
    }

    public static void setSumAsoc(int sumAsoc) {
        CajaDatos.sumAsoc = sumAsoc;
    }

    public static int getContDolar() {
        return contDolar;
    }

    public static void setContDolar(int contDolar) {
        CajaDatos.contDolar = contDolar;
    }

    public static int getSumDolar() {
        return sumDolar;
    }

    public static void setSumDolar(int sumDolar) {
        CajaDatos.sumDolar = sumDolar;
    }

    public static int getContReal() {
        return contReal;
    }

    public static void setContReal(int contReal) {
        CajaDatos.contReal = contReal;
    }

    public static int getSumReal() {
        return sumReal;
    }

    public static void setSumReal(int sumReal) {
        CajaDatos.sumReal = sumReal;
    }

    public static int getContPeso() {
        return contPeso;
    }

    public static void setContPeso(int contPeso) {
        CajaDatos.contPeso = contPeso;
    }

    public static int getSumPeso() {
        return sumPeso;
    }

    public static void setSumPeso(int sumPeso) {
        CajaDatos.sumPeso = sumPeso;
    }

    public static int getSumRetencion() {
        return sumRetencion;
    }

    public static void setSumRetencion(int sumRetencion) {
        CajaDatos.sumRetencion = sumRetencion;
    }

    public static int getSumEfeRecibido() {
        return sumEfeRecibido;
    }

    public static void setSumEfeRecibido(int sumEfeRecibido) {
        CajaDatos.sumEfeRecibido = sumEfeRecibido;
    }

    public static int getContRetencion() {
        return contRetencion;
    }

    public static void setContRetencion(int contRetencion) {
        CajaDatos.contRetencion = contRetencion;
    }

    public static String getUuidCassandraActual() {
        return uuidCassandraActual;
    }

    public static void setUuidCassandraActual(String uuidCassandraActual) {
        CajaDatos.uuidCassandraActual = uuidCassandraActual;
    }

    public static long getIdRangoFacturaActual() {
        return idRangoFacturaActual;
    }

    public static void setIdRangoFacturaActual(long idRangoFacturaActual) {
        CajaDatos.idRangoFacturaActual = idRangoFacturaActual;
    }

    public static long getIdFactClienteCabServidor() {
        return idFactClienteCabServidor;
    }

    public static void setIdFactClienteCabServidor(long idFactClienteCabServidor) {
        CajaDatos.idFactClienteCabServidor = idFactClienteCabServidor;
    }

    public static boolean isInsercionFacturaVentaCab() {
        return insercionFacturaVentaCab;
    }

    public static void setInsercionFacturaVentaCab(boolean insercionFacturaVentaCab) {
        CajaDatos.insercionFacturaVentaCab = insercionFacturaVentaCab;
    }

    public static boolean isActualizacionLocal() {
        return actualizacionLocal;
    }

    public static void setActualizacionLocal(boolean actualizacionLocal) {
        CajaDatos.actualizacionLocal = actualizacionLocal;
    }

    public static long getIdDescuentoClienteFiel() {
        return idDescuentoClienteFiel;
    }

    public static void setIdDescuentoClienteFiel(long idDescuentoClienteFiel) {
        CajaDatos.idDescuentoClienteFiel = idDescuentoClienteFiel;
    }

    public static long getIdDescuentoTarjetaConvenio() {
        return idDescuentoTarjetaConvenio;
    }

    public static void setIdDescuentoTarjetaConvenio(long idDescuentoTarjetaConvenio) {
        CajaDatos.idDescuentoTarjetaConvenio = idDescuentoTarjetaConvenio;
    }

    public static boolean isInsercionFacturaVentaCabLocal() {
        return insercionFacturaVentaCabLocal;
    }

    public static void setInsercionFacturaVentaCabLocal(boolean insercionFacturaVentaCabLocal) {
        CajaDatos.insercionFacturaVentaCabLocal = insercionFacturaVentaCabLocal;
    }

    public static String getNroFact() {
        return nroFact;
    }

    public static void setNroFact(String nroFact) {
        CajaDatos.nroFact = nroFact;
    }

    public static long getIdPromoTemporada() {
        return idPromoTemporada;
    }

    public static void setIdPromoTemporada(long idPromoTemporada) {
        CajaDatos.idPromoTemporada = idPromoTemporada;
    }

    public static long getIdDescuentoTarjeta() {
        return idDescuentoTarjeta;
    }

    public static void setIdDescuentoTarjeta(long idDescuentoTarjeta) {
        CajaDatos.idDescuentoTarjeta = idDescuentoTarjeta;
    }

    public static long getIdDescuentoFuncionario() {
        return idDescuentoFuncionario;
    }

    public static void setIdDescuentoFuncionario(long idDescuentoFuncionario) {
        CajaDatos.idDescuentoFuncionario = idDescuentoFuncionario;
    }

    public static boolean isVentaServer() {
        return ventaServer;
    }

    public static void setVentaServer(boolean ventaServer) {
        CajaDatos.ventaServer = ventaServer;
    }

    public static boolean isDescuentoClienteFielCab() {
        return descuentoClienteFielCab;
    }

    public static void setDescuentoClienteFielCab(boolean descuentoClienteFielCab) {
        CajaDatos.descuentoClienteFielCab = descuentoClienteFielCab;
    }

    public static boolean isDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public static void setDescuentoTarjetaCab(boolean descuentoTarjetaCab) {
        CajaDatos.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public static boolean isDescuentoTarjetaConvCab() {
        return descuentoTarjetaConvCab;
    }

    public static void setDescuentoTarjetaConvCab(boolean descuentoTarjetaConvCab) {
        CajaDatos.descuentoTarjetaConvCab = descuentoTarjetaConvCab;
    }

    public static boolean isDescuentoPromo() {
        return descuentoPromo;
    }

    public static void setDescuentoPromo(boolean descuentoPromo) {
        CajaDatos.descuentoPromo = descuentoPromo;
    }

    public static boolean isFacturaCheque() {
        return facturaCheque;
    }

    public static void setFacturaCheque(boolean facturaCheque) {
        CajaDatos.facturaCheque = facturaCheque;
    }

    public static boolean isFacturaEfectivo() {
        return facturaEfectivo;
    }

    public static void setFacturaEfectivo(boolean facturaEfectivo) {
        CajaDatos.facturaEfectivo = facturaEfectivo;
    }

    public static boolean isFacturaFuncionario() {
        return facturaFuncionario;
    }

    public static void setFacturaFuncionario(boolean facturaFuncionario) {
        CajaDatos.facturaFuncionario = facturaFuncionario;
    }

    public static boolean isFacturaMonedaExtranjera() {
        return facturaMonedaExtranjera;
    }

    public static void setFacturaMonedaExtranjera(boolean facturaMonedaExtranjera) {
        CajaDatos.facturaMonedaExtranjera = facturaMonedaExtranjera;
    }

    public static boolean isFacturaPromo() {
        return facturaPromo;
    }

    public static void setFacturaPromo(boolean facturaPromo) {
        CajaDatos.facturaPromo = facturaPromo;
    }

    public static boolean isFacturaTarjeta() {
        return facturaTarjeta;
    }

    public static void setFacturaTarjeta(boolean facturaTarjeta) {
        CajaDatos.facturaTarjeta = facturaTarjeta;
    }

    public static boolean isFacturaTarjetaConv() {
        return facturaTarjetaConv;
    }

    public static void setFacturaTarjetaConv(boolean facturaTarjetaConv) {
        CajaDatos.facturaTarjetaConv = facturaTarjetaConv;
    }

    public static boolean isFacturaTarjetaFiel() {
        return facturaTarjetaFiel;
    }

    public static void setFacturaTarjetaFiel(boolean facturaTarjetaFiel) {
        CajaDatos.facturaTarjetaFiel = facturaTarjetaFiel;
    }

    public static String getNroComprobante() {
        return nroComprobante;
    }

    public static long getIdPromoTemporadaArt() {
        return idPromoTemporadaArt;
    }

    public static void setIdPromoTemporadaArt(long aIdPromoTemporadaArt) {
        idPromoTemporadaArt = aIdPromoTemporadaArt;
    }

    public static boolean isDescuentoPromoArt() {
        return descuentoPromoArt;
    }

    public static void setDescuentoPromoArt(boolean aDescuentoPromoArt) {
        descuentoPromoArt = aDescuentoPromoArt;
    }

    public static long getIdDescuentoFuncionarioNf1() {
        return idDescuentoFuncionarioNf1;
    }

    public static void setIdDescuentoFuncionarioNf1(long aIdDescuentoFuncionarioNf1) {
        idDescuentoFuncionarioNf1 = aIdDescuentoFuncionarioNf1;
    }

    public static long getIdDescuentoFuncionarioNf2() {
        return idDescuentoFuncionarioNf2;
    }

    public static void setIdDescuentoFuncionarioNf2(long aIdDescuentoFuncionarioNf2) {
        idDescuentoFuncionarioNf2 = aIdDescuentoFuncionarioNf2;
    }

    public static long getIdDescuentoFuncionarioNf3() {
        return idDescuentoFuncionarioNf3;
    }

    public static void setIdDescuentoFuncionarioNf3(long aIdDescuentoFuncionarioNf3) {
        idDescuentoFuncionarioNf3 = aIdDescuentoFuncionarioNf3;
    }

    public static long getIdDescuentoFuncionarioNf4() {
        return idDescuentoFuncionarioNf4;
    }

    public static void setIdDescuentoFuncionarioNf4(long aIdDescuentoFuncionarioNf4) {
        idDescuentoFuncionarioNf4 = aIdDescuentoFuncionarioNf4;
    }

    public static long getIdDescuentoFuncionarioNf5() {
        return idDescuentoFuncionarioNf5;
    }

    public static void setIdDescuentoFuncionarioNf5(long aIdDescuentoFuncionarioNf5) {
        idDescuentoFuncionarioNf5 = aIdDescuentoFuncionarioNf5;
    }

    public static long getIdDescuentoFuncionarioNf6() {
        return idDescuentoFuncionarioNf6;
    }

    public static void setIdDescuentoFuncionarioNf6(long aIdDescuentoFuncionarioNf6) {
        idDescuentoFuncionarioNf6 = aIdDescuentoFuncionarioNf6;
    }

    public static long getIdDescuentoFuncionarioNf7() {
        return idDescuentoFuncionarioNf7;
    }

    public static void setIdDescuentoFuncionarioNf7(long aIdDescuentoFuncionarioNf7) {
        idDescuentoFuncionarioNf7 = aIdDescuentoFuncionarioNf7;
    }

    public static long getIdCuponera() {
        return idCuponera;
    }

    public static void setIdCuponera(long aIdCuponera) {
        idCuponera = aIdCuponera;
    }

    public static boolean isCuponera() {
        return cuponera;
    }

    public static void setCuponera(boolean aCuponera) {
        cuponera = aCuponera;
    }
}
