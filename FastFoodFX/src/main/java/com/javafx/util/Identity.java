/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.ArrayList;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author ExcelsisWalker
 */
public class Identity {

    private static JSONObject usuario;
    private JSONArray usuarioRolJsonArray;
    private JSONArray rolFuncionJsonArray;
    private static List<String> urlScene = new ArrayList<>();
    private static String nomFun = "";

    static JSONObject datos = new JSONObject();
    static JSONObject users = new JSONObject();
    static JSONObject fact = new JSONObject();

    public Identity() {
    }

    private void mapeoUserLogin(JSONObject jsonUser) {//se instancia para cerrar sesión también... con null
        //**--VERIFICANDO--**
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        if (jsonUser != null) {
            usuario = jsonUser;
            users.put("usuario", jsonUser);
            nomFun = Utilidades.obteniendoFuncionario(null, false);
            urlScene = new ArrayList<>();
            usuarioRolJsonArray = (JSONArray) jsonUser.get("usuarioRols");
            for (Object uRObj : usuarioRolJsonArray) {
                JSONObject jsonUsuarioRol = (JSONObject) uRObj;
                JSONObject jsonRol = (JSONObject) jsonUsuarioRol.get("rol");
                if ((Boolean) jsonRol.get("activo")) {
                    rolFuncionJsonArray = (JSONArray) jsonRol.get("rolFuncions");
                    for (Object rFObj : rolFuncionJsonArray) {//funcion
                        JSONObject jsonRolFuncion = (JSONObject) rFObj;
                        JSONObject jsonFuncion = (JSONObject) jsonRolFuncion.get("funcion");
                        if (!urlScene.contains(jsonFuncion.get("urlPantalla").toString())) {
                            urlScene.add(jsonFuncion.get("urlPantalla").toString());
                        }
                    }
                }
            }
            users.put("urlScene", urlScene);
        } else {
            nomFun = "";
            usuario = new JSONObject();
            users.put("usuario", "");
            urlScene = new ArrayList<>();
            users.put("urlScene", "");
        }
        DatosEnCaja.setUsers(users);
    }

    public void usuarioLogueado(JSONObject user) {//vueltas al pedo... peor que la línea 13 xD
        mapeoUserLogin(user);
    }

    public static boolean rolIdentity(String rolUrlScene) {
        return rolI(rolUrlScene);
    }

    private static boolean rolI(String rolUrlScene) {
        boolean permiso = false;
        if (DatosEnCaja.getDatos() != null) {
            users = DatosEnCaja.getUsers();
        }
        String url = users.get("urlScene").toString();
        url = url.replace("[", "");
        url = url.replace("]", "");
        String[] lista = stringToVector(url);
        for (int i = 0; i < lista.length; i++) {
            String dato = lista[i].replace(" ", "");
            dato = dato.replace("\"", "");
            if (rolUrlScene.equalsIgnoreCase(dato)) {
                permiso = true;
                break;
            }
        }
        return permiso;
    }

    public static JSONObject getUsuario() {
        try {
            JSONParser parser = new JSONParser();
            if (DatosEnCaja.getUsers() != null) {
                users = DatosEnCaja.getUsers();
            }
            JSONObject usuarios = (JSONObject) parser.parse(users.get("usuario").toString());
            return usuarios;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return null;
        }
    }

    public static JSONObject onlyUser() {
        JSONParser parser = new JSONParser();
        JSONObject user = new JSONObject();
        try {
            users = (JSONObject) parser.parse(DatosEnCaja.getUsers().toString());
            usuario = (JSONObject) parser.parse(users.get("usuario").toString());
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        if (usuario != null) {
            user.put("idUsuario", usuario.get("idUsuario"));
            user.put("nomUsuario", usuario.get("nomUsuario"));
        }
        return user;
    }

    public static String[] stringToVector(String sampleString) {
        String[] items = sampleString.split(",");
        return items;
    }

    public static String getNomFun() {
        return nomFun;
    }

}
