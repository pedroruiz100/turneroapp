/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import static com.sun.javafx.PlatformUtil.isMac;
import static com.sun.javafx.PlatformUtil.isUnix;
import static com.sun.javafx.PlatformUtil.isWindows;
import java.io.File;
import java.util.TimerTask;

/**
 *
 * @author PC
 */
public class PrinterHandler extends TimerTask {

    @Override
    public void run() {
        String OS = System.getProperty("os.name").toLowerCase();
        System.out.println(OS);
        try {
            if (isWindows()) {
                for (File file : new File("C:\\Windows\\System32\\spool\\PRINTERS").listFiles()) {
                    if (!file.isDirectory()) {
                        file.delete();
                    }
                }
            } else if (isMac()) {
                System.out.println("Mac");
                Runtime.getRuntime().exec("cancel -a -");
            } else if (isUnix()) {
                Runtime.getRuntime().exec("lprm -");
            }
        } catch (Exception e) {
            System.out.println("-->> " + e.getLocalizedMessage());
        }
        Ticket.timer.cancel();
        Ticket.timer.purge();
    }
}
