/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import com.peluqueria.dao.impl.ManejoLocalDAOImpl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.peluqueria.dao.ManejoLocalDAO;

/**
 *
 * @author ExcelsisWalker
 */
public class CajaDeDatos {

    private static JSONObject caja;

    static JSONObject datos = new JSONObject();
    static ManejoLocalDAO manejoDAO = new ManejoLocalDAOImpl();

    private JSONArray talonarioSucursalArray;
    private JSONObject talonarioSucursalAux;
    private JSONObject ipBocaCaja;
    private JSONObject ipBocaTalonario;
    //datos varios para factura...
    private static JSONObject talonarioSucursal;
    private static JSONObject sucursal;
    private static JSONObject empresa;
    private static JSONObject ciudad;
    private static JSONObject timbrado;
    //datos varios para factura...

    //datos para Apertura y Cierre de Caja
    private static JSONObject aperturaCaja;
    private static int montoFacturado = 0; //Monto Facturado de manerra global dentro de esta caja
    private static int donaciones = 0; // Donaciones Globales
    private static int retiroDinero = 0;//Todos los retiros de dinero que se realizo

    private static int montoCajero; // Monto Facturado por cajero que inicia sesión en el módulo Caja
    private static int donacionCajero;// Donaciones por cajeros
    private static int retiroDineroCajero;//Retiro de Dinero Por Cajero

    private boolean primera;
    private static boolean rendicion;
    private static boolean estadoInformeFinanciero;

    private static long zeta;
    private static String facturaInicial;
    private static String facturaFinal;

    //Estados de las facturasiniciales y finales
    private static boolean estadoFacturaInicial = false;

    private static String nroComprobante;

//    DATOS PARA ARQUEO
    private static int contDesc = 0; // CONTADOR DE DESCUENTOS DENTRO DE ESTA CAJA
    private static int sumDesc = 0;  // SUMADOR DE DESCUENTOS DENTRO DE ESTA CAJA

    private static int contTarj = 0;
    private static int sumTarj = 0;

    //Nuevos 
    private static int contEfectivo = 0;
    private static int sumEfectivo = 0;

    private static int totales = 0;
    private static int nClientes = 0;
    private static int nFuncionarios = 0;
    private static int nArticulos = 0;

    private static int gra5 = 0;
    private static int gra10 = 0;
    private static int exe = 0;
    private static int gra = 0;

    private static int contDonaciones = 0;
    private static int sumDonaciones = 0;

    private static int contNotaCred = 0;
    private static int sumNotaCred = 0;
    private static int contFact = 0;
    private static int sumFact = 0;
    private static int contCheque = 0;
    private static int sumCheque = 0;
    private static int contVale = 0;
    private static int sumVale = 0;
    private static int contAsoc = 0;
    private static int sumAsoc = 0;

    private static int contDolar = 0;
    private static int sumDolar = 0;
    private static int contReal = 0;
    private static int sumReal = 0;
    private static int contPeso = 0;
    private static int contRetencion = 0;
    private static int sumPeso = 0;
    private static int sumRetencion = 0;
    private static int sumEfeRecibido = 0;

    private static String uuidCassandraActual = "";
    private static long idRangoFacturaActual = 0L;
    //id que se persisto en el servidor....
    private static long idFactClienteCabServidor = 0L;
    private static boolean insercionFacturaVentaCab = false;
    private static boolean actualizacionLocal = false;
    private static boolean insercionFacturaVentaCabLocal = false;

    //PARA LOS TIPO DE DESCUENTOS, recuperando los id insertados de la cabecera para almacenarlos en el detalle
    private static long idDescuentoClienteFiel = 0;
    private static long idDescuentoTarjetaConvenio = 0;
    private static long idPromoTemporada = 0;
    private static long idDescuentoTarjeta = 0;
    private static long idDescuentoFuncionario = 0;

    private static String nroFact = "";
    private static boolean ventaServer = false;
    private static boolean descuentoClienteFielCab = false;
    private static boolean descuentoTarjetaCab = false;
    private static boolean descuentoTarjetaConvCab = false;
    private static boolean descuentoPromo = false;

    private static boolean facturaCheque = false;
    private static boolean facturaEfectivo = false;
    private static boolean facturaFuncionario = false;
    private static boolean facturaMonedaExtranjera = false;
    private static boolean facturaPromo = false;
    private static boolean facturaTarjeta = false;
    private static boolean facturaTarjetaConv = false;
    private static boolean facturaTarjetaFiel = false;

    //**************************************************************************
    public void mapeandoCaja(JSONObject caja) throws ParseException {
        mapCaja(caja);
    }

    private void mapCaja(JSONObject caja) throws ParseException {
        //**--VERIFICANDO--**
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        primera = true;
        datos.put("primera", true);
        this.caja = caja;//caja
        datos.put("caja", caja);
        ipBocaCaja = (JSONObject) this.caja.get("ipBoca");//ip caja
        datos.put("ipBocaCaja", caja.get("ipBoca"));
        sucursal = (JSONObject) this.caja.get("sucursal");//sucursal
        datos.put("sucursal", caja.get("sucursal"));
        ciudad = (JSONObject) sucursal.get("ciudadDTO");//ciudad
        datos.put("ciudad", sucursal.get("ciudadDTO"));
        empresa = (JSONObject) sucursal.get("empresaDTO");//empresa
        datos.put("empresa", sucursal.get("empresaDTO"));
        talonarioSucursalArray = (JSONArray) sucursal.get("talonariosSucursalesDTO");
        datos.put("talonarioSucursalArray", (JSONArray) sucursal.get("talonariosSucursalesDTO"));
        for (Object objectTalonario : talonarioSucursalArray) {
            talonarioSucursalAux = (JSONObject) objectTalonario;
            datos.put("talonarioSucursalAux", (JSONObject) objectTalonario);
            ipBocaTalonario = (JSONObject) talonarioSucursalAux.get("ipBoca");//ip talonario, comparativa con ip caja...
            datos.put("ipBocaTalonario", (JSONObject) talonarioSucursalAux.get("ipBoca"));
            if ((long) ipBocaCaja.get("idIpBoca") == (long) ipBocaTalonario.get("idIpBoca")) {
                if (primera) {//pueden existir varios timbrados, se obtiene el más "actual"...
                    timbrado = (JSONObject) talonarioSucursalAux.get("timbrado");
                    datos.put("timbrado", timbrado);
                    talonarioSucursal = (JSONObject) talonarioSucursalAux;
                    datos.put("talonarioSucursal", (JSONObject) talonarioSucursalAux);
                    primera = false;
                    datos.put("primera", false);
                } else {
                    JSONObject jsontTimbrado = (JSONObject) talonarioSucursalAux.get("timbrado");
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dateVenNew = sdf.parse(Utilidades.objectToTimestamp(jsontTimbrado.get("fecVencimiento")).toString());
                    Date dateVenOld = sdf.parse(Utilidades.objectToTimestamp(getTimbrado().get("fecVencimiento")).toString());
                    if (dateVenNew.after(dateVenOld)) {//más actual...
                        timbrado = jsontTimbrado;
                        datos.put("timbrado", jsontTimbrado);
                        talonarioSucursal = (JSONObject) talonarioSucursalAux;
                        datos.put("talonarioSucursal", (JSONObject) talonarioSucursalAux);
                    }
                }
            }
        }
        DatosEnCaja.setDatos(datos);
    }

    //**************************************************************************
    //**************************************************************************
    public static JSONObject onlyCaja() {
        JSONObject cajaOnly = new JSONObject();
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        JSONObject cajas = null;
        try {
            cajas = (JSONObject) parser.parse(datos.get("caja").toString());
        } catch (org.json.simple.parser.ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
        }
        if (cajas != null) {
            cajaOnly.put("idCaja", cajas.get("idCaja"));
        }
        return cajaOnly;
    }

    public static void generandoNroComprobante() throws org.json.simple.parser.ParseException {
        numeracion();
    }

    private static void numeracion() throws org.json.simple.parser.ParseException {
        JSONParser parser = new JSONParser();
        if (DatosEnCaja.getDatos() != null) {
            datos = DatosEnCaja.getDatos();
        }
        caja = (JSONObject) parser.parse(datos.get("caja").toString());
        sucursal = (JSONObject) parser.parse(datos.get("sucursal").toString());
        talonarioSucursal = (JSONObject) parser.parse(datos.get("talonarioSucursal").toString());
        String suc = sucursal.get("idSucursal").toString();
        int sucLength = suc.length();
        String caj = caja.get("nroCaja").toString();
        int cajLength = caj.length();
        String fac = talonarioSucursal.get("nroActual").toString();
        int facLength = fac.length();
        while (sucLength < 3) {
            sucLength++;
            suc = "0" + suc;
        }
        while (cajLength < 3) {
            cajLength++;
            caj = "0" + caj;
        }
        while (facLength < 7) {
            facLength++;
            fac = "0" + fac;
        }
        nroComprobante = suc + "-" + caj + "-" + fac;
        //primera instancia, se prepara nroActual para la siguiente...
        talonarioSucursal.put("nroActual", ((long) talonarioSucursal.get("nroActual") + 1));
    }

    public static JSONObject getTalonarioSucursal() {
        return talonarioSucursal;
    }

    public static JSONObject getSucursal() {
        return sucursal;
    }

    public static JSONObject getEmpresa() {
        return empresa;
    }

    public static JSONObject getCiudad() {
        return ciudad;
    }

    public static long getZeta() {
        return zeta;
    }

    public static void setZeta(long zeta) {
        CajaDeDatos.zeta = zeta;
    }

    public static String getFacturaInicial() {
        return facturaInicial;
    }

    public static void setFacturaInicial(String facturaInicial) {
        CajaDeDatos.facturaInicial = facturaInicial;
    }

    public static String getFacturaFinal() {
        return facturaFinal;
    }

    public static boolean isEstadoFacturaInicial() {
        return estadoFacturaInicial;
    }

    public static void setEstadoFacturaInicial(boolean estadoFacturaInicial) {
        CajaDeDatos.estadoFacturaInicial = estadoFacturaInicial;
    }

    public static void setFacturaFinal(String facturaFinal) {
        CajaDeDatos.facturaFinal = facturaFinal;
    }

    public static JSONObject getTimbrado() {
        return timbrado;
    }

    public static JSONObject getCaja() {
        return caja;
    }
    //**************************************************************************

    public static JSONObject getAperturaCaja() {
        return aperturaCaja;
    }

    public static void setAperturaCaja(JSONObject aperturaCaja) {
        CajaDeDatos.aperturaCaja = aperturaCaja;
    }

    public static void setCaja(JSONObject caja) {
        CajaDeDatos.caja = caja;
    }

    public static int getRetiroDinero() {
        return retiroDinero;
    }

    public static void setRetiroDinero(int retiroDinero) {
        CajaDeDatos.retiroDinero = retiroDinero;
    }

    public static int getMontoFacturado() {
        return montoFacturado;
    }

    public static void setMontoFacturado(int montoFacturado) {
        CajaDeDatos.montoFacturado = montoFacturado;
    }

    public static int getDonaciones() {
        return donaciones;
    }

    public static void setDonaciones(int donaciones) {
        CajaDeDatos.donaciones = donaciones;
    }

    public static int getMontoCajero() {
        return montoCajero;
    }

    public static void setMontoCajero(int montoCajero) {
        CajaDeDatos.montoCajero = montoCajero;
    }

    public static int getDonacionCajero() {
        return donacionCajero;
    }

    public static boolean isRendicion() {
        return rendicion;
    }

    public static void setRendicion(boolean rendicion) {
        CajaDeDatos.rendicion = rendicion;
    }

    public static void setDonacionCajero(int donacionCajero) {
        CajaDeDatos.donacionCajero = donacionCajero;
    }

    public static int getRetiroDineroCajero() {
        return retiroDineroCajero;
    }

    public static void setRetiroDineroCajero(int retiroDineroCajero) {
        CajaDeDatos.retiroDineroCajero = retiroDineroCajero;
    }

    public static boolean isEstadoInformeFinanciero() {
        return estadoInformeFinanciero;
    }

    public static void setEstadoInformeFinanciero(boolean estadoInformeFinanciero) {
        CajaDeDatos.estadoInformeFinanciero = estadoInformeFinanciero;
    }

    public static int getContDesc() {
        return contDesc;
    }

    public static void setContDesc(int contDesc) {
        CajaDeDatos.contDesc = contDesc;
    }

    public static int getSumDesc() {
        return sumDesc;
    }

    public static void setSumDesc(int sumDesc) {
        CajaDeDatos.sumDesc = sumDesc;
    }

    public static int getContTarj() {
        return contTarj;
    }

    public static void setContTarj(int contTarj) {
        CajaDeDatos.contTarj = contTarj;
    }

    public static int getSumTarj() {
        return sumTarj;
    }

    public static void setSumTarj(int sumTarj) {
        CajaDeDatos.sumTarj = sumTarj;
    }

    public static int getContEfectivo() {
        return contEfectivo;
    }

    public static void setContEfectivo(int contEfectivo) {
        CajaDeDatos.contEfectivo = contEfectivo;
    }

    public static int getSumEfectivo() {
        return sumEfectivo;
    }

    public static void setSumEfectivo(int sumEfectivo) {
        CajaDeDatos.sumEfectivo = sumEfectivo;
    }

    public static int getTotales() {
        return totales;
    }

    public static void setTotales(int totales) {
        CajaDeDatos.totales = totales;
    }

    public static int getnClientes() {
        return nClientes;
    }

    public static void setnClientes(int nClientes) {
        CajaDeDatos.nClientes = nClientes;
    }

    public static int getnFuncionarios() {
        return nFuncionarios;
    }

    public static void setnFuncionarios(int nFuncionarios) {
        CajaDeDatos.nFuncionarios = nFuncionarios;
    }

    public static int getnArticulos() {
        return nArticulos;
    }

    public static void setnArticulos(int nArticulos) {
        CajaDeDatos.nArticulos = nArticulos;
    }

    public static int getGra5() {
        return gra5;
    }

    public static void setGra5(int gra5) {
        CajaDeDatos.gra5 = gra5;
    }

    public static int getGra10() {
        return gra10;
    }

    public static void setGra10(int gra10) {
        CajaDeDatos.gra10 = gra10;
    }

    public static int getExe() {
        return exe;
    }

    public static void setExe(int exe) {
        CajaDeDatos.exe = exe;
    }

    public static int getContDonaciones() {
        return contDonaciones;
    }

    public static void setContDonaciones(int contDonaciones) {
        CajaDeDatos.contDonaciones = contDonaciones;
    }

    public static int getSumDonaciones() {
        return sumDonaciones;
    }

    public static void setSumDonaciones(int sumDonaciones) {
        CajaDeDatos.sumDonaciones = sumDonaciones;
    }

    public static int getGra() {
        return gra;
    }

    public static void setGra(int gra) {
        CajaDeDatos.gra = gra;
    }

    public static int getContNotaCred() {
        return contNotaCred;
    }

    public static void setContNotaCred(int contNotaCred) {
        CajaDeDatos.contNotaCred = contNotaCred;
    }

    public static int getSumNotaCred() {
        return sumNotaCred;
    }

    public static void setSumNotaCred(int sumNotaCred) {
        CajaDeDatos.sumNotaCred = sumNotaCred;
    }

    public static int getContFact() {
        return contFact;
    }

    public static void setContFact(int contFact) {
        CajaDeDatos.contFact = contFact;
    }

    public static int getSumFact() {
        return sumFact;
    }

    public static void setSumFact(int sumFact) {
        CajaDeDatos.sumFact = sumFact;
    }

    public static int getContCheque() {
        return contCheque;
    }

    public static void setContCheque(int contCheque) {
        CajaDeDatos.contCheque = contCheque;
    }

    public static int getSumCheque() {
        return sumCheque;
    }

    public static void setSumCheque(int sumCheque) {
        CajaDeDatos.sumCheque = sumCheque;
    }

    public static int getContVale() {
        return contVale;
    }

    public static void setContVale(int contVale) {
        CajaDeDatos.contVale = contVale;
    }

    public static int getSumVale() {
        return sumVale;
    }

    public static void setSumVale(int sumVale) {
        CajaDeDatos.sumVale = sumVale;
    }

    public static int getContAsoc() {
        return contAsoc;
    }

    public static void setContAsoc(int contAsoc) {
        CajaDeDatos.contAsoc = contAsoc;
    }

    public static int getSumAsoc() {
        return sumAsoc;
    }

    public static void setSumAsoc(int sumAsoc) {
        CajaDeDatos.sumAsoc = sumAsoc;
    }

    public static int getContDolar() {
        return contDolar;
    }

    public static void setContDolar(int contDolar) {
        CajaDeDatos.contDolar = contDolar;
    }

    public static int getSumDolar() {
        return sumDolar;
    }

    public static void setSumDolar(int sumDolar) {
        CajaDeDatos.sumDolar = sumDolar;
    }

    public static int getContReal() {
        return contReal;
    }

    public static void setContReal(int contReal) {
        CajaDeDatos.contReal = contReal;
    }

    public static int getSumReal() {
        return sumReal;
    }

    public static void setSumReal(int sumReal) {
        CajaDeDatos.sumReal = sumReal;
    }

    public static int getContPeso() {
        return contPeso;
    }

    public static void setContPeso(int contPeso) {
        CajaDeDatos.contPeso = contPeso;
    }

    public static int getSumPeso() {
        return sumPeso;
    }

    public static void setSumPeso(int sumPeso) {
        CajaDeDatos.sumPeso = sumPeso;
    }

    public static int getSumRetencion() {
        return sumRetencion;
    }

    public static void setSumRetencion(int sumRetencion) {
        CajaDeDatos.sumRetencion = sumRetencion;
    }

    public static int getSumEfeRecibido() {
        return sumEfeRecibido;
    }

    public static void setSumEfeRecibido(int sumEfeRecibido) {
        CajaDeDatos.sumEfeRecibido = sumEfeRecibido;
    }

    public static int getContRetencion() {
        return contRetencion;
    }

    public static void setContRetencion(int contRetencion) {
        CajaDeDatos.contRetencion = contRetencion;
    }

    public static String getUuidCassandraActual() {
        return uuidCassandraActual;
    }

    public static void setUuidCassandraActual(String uuidCassandraActual) {
        CajaDeDatos.uuidCassandraActual = uuidCassandraActual;
    }

    public static long getIdRangoFacturaActual() {
        return idRangoFacturaActual;
    }

    public static void setIdRangoFacturaActual(long idRangoFacturaActual) {
        CajaDeDatos.idRangoFacturaActual = idRangoFacturaActual;
    }

    public static long getIdFactClienteCabServidor() {
        return idFactClienteCabServidor;
    }

    public static void setIdFactClienteCabServidor(long idFactClienteCabServidor) {
        CajaDeDatos.idFactClienteCabServidor = idFactClienteCabServidor;
    }

    public static boolean isInsercionFacturaVentaCab() {
        return insercionFacturaVentaCab;
    }

    public static void setInsercionFacturaVentaCab(boolean insercionFacturaVentaCab) {
        CajaDeDatos.insercionFacturaVentaCab = insercionFacturaVentaCab;
    }

    public static boolean isActualizacionLocal() {
        return actualizacionLocal;
    }

    public static void setActualizacionLocal(boolean actualizacionLocal) {
        CajaDeDatos.actualizacionLocal = actualizacionLocal;
    }

    public static long getIdDescuentoClienteFiel() {
        return idDescuentoClienteFiel;
    }

    public static void setIdDescuentoClienteFiel(long idDescuentoClienteFiel) {
        CajaDeDatos.idDescuentoClienteFiel = idDescuentoClienteFiel;
    }

    public static long getIdDescuentoTarjetaConvenio() {
        return idDescuentoTarjetaConvenio;
    }

    public static void setIdDescuentoTarjetaConvenio(long idDescuentoTarjetaConvenio) {
        CajaDeDatos.idDescuentoTarjetaConvenio = idDescuentoTarjetaConvenio;
    }

    public static boolean isInsercionFacturaVentaCabLocal() {
        return insercionFacturaVentaCabLocal;
    }

    public static void setInsercionFacturaVentaCabLocal(boolean insercionFacturaVentaCabLocal) {
        CajaDeDatos.insercionFacturaVentaCabLocal = insercionFacturaVentaCabLocal;
    }

    public static String getNroFact() {
        return nroFact;
    }

    public static void setNroFact(String nroFact) {
        CajaDeDatos.nroFact = nroFact;
    }

    public static long getIdPromoTemporada() {
        return idPromoTemporada;
    }

    public static void setIdPromoTemporada(long idPromoTemporada) {
        CajaDeDatos.idPromoTemporada = idPromoTemporada;
    }

    public static long getIdDescuentoTarjeta() {
        return idDescuentoTarjeta;
    }

    public static void setIdDescuentoTarjeta(long idDescuentoTarjeta) {
        CajaDeDatos.idDescuentoTarjeta = idDescuentoTarjeta;
    }

    public static long getIdDescuentoFuncionario() {
        return idDescuentoFuncionario;
    }

    public static void setIdDescuentoFuncionario(long idDescuentoFuncionario) {
        CajaDeDatos.idDescuentoFuncionario = idDescuentoFuncionario;
    }

    public static boolean isVentaServer() {
        return ventaServer;
    }

    public static void setVentaServer(boolean ventaServer) {
        CajaDeDatos.ventaServer = ventaServer;
    }

    public static boolean isDescuentoClienteFielCab() {
        return descuentoClienteFielCab;
    }

    public static void setDescuentoClienteFielCab(boolean descuentoClienteFielCab) {
        CajaDeDatos.descuentoClienteFielCab = descuentoClienteFielCab;
    }

    public static boolean isDescuentoTarjetaCab() {
        return descuentoTarjetaCab;
    }

    public static void setDescuentoTarjetaCab(boolean descuentoTarjetaCab) {
        CajaDeDatos.descuentoTarjetaCab = descuentoTarjetaCab;
    }

    public static boolean isDescuentoTarjetaConvCab() {
        return descuentoTarjetaConvCab;
    }

    public static void setDescuentoTarjetaConvCab(boolean descuentoTarjetaConvCab) {
        CajaDeDatos.descuentoTarjetaConvCab = descuentoTarjetaConvCab;
    }

    public static boolean isDescuentoPromo() {
        return descuentoPromo;
    }

    public static void setDescuentoPromo(boolean descuentoPromo) {
        CajaDeDatos.descuentoPromo = descuentoPromo;
    }

    public static boolean isFacturaCheque() {
        return facturaCheque;
    }

    public static void setFacturaCheque(boolean facturaCheque) {
        CajaDeDatos.facturaCheque = facturaCheque;
    }

    public static boolean isFacturaEfectivo() {
        return facturaEfectivo;
    }

    public static void setFacturaEfectivo(boolean facturaEfectivo) {
        CajaDeDatos.facturaEfectivo = facturaEfectivo;
    }

    public static boolean isFacturaFuncionario() {
        return facturaFuncionario;
    }

    public static void setFacturaFuncionario(boolean facturaFuncionario) {
        CajaDeDatos.facturaFuncionario = facturaFuncionario;
    }

    public static boolean isFacturaMonedaExtranjera() {
        return facturaMonedaExtranjera;
    }

    public static void setFacturaMonedaExtranjera(boolean facturaMonedaExtranjera) {
        CajaDeDatos.facturaMonedaExtranjera = facturaMonedaExtranjera;
    }

    public static boolean isFacturaPromo() {
        return facturaPromo;
    }

    public static void setFacturaPromo(boolean facturaPromo) {
        CajaDeDatos.facturaPromo = facturaPromo;
    }

    public static boolean isFacturaTarjeta() {
        return facturaTarjeta;
    }

    public static void setFacturaTarjeta(boolean facturaTarjeta) {
        CajaDeDatos.facturaTarjeta = facturaTarjeta;
    }

    public static boolean isFacturaTarjetaConv() {
        return facturaTarjetaConv;
    }

    public static void setFacturaTarjetaConv(boolean facturaTarjetaConv) {
        CajaDeDatos.facturaTarjetaConv = facturaTarjetaConv;
    }

    public static boolean isFacturaTarjetaFiel() {
        return facturaTarjetaFiel;
    }

    public static void setFacturaTarjetaFiel(boolean facturaTarjetaFiel) {
        CajaDeDatos.facturaTarjetaFiel = facturaTarjetaFiel;
    }

    public static String getNroComprobante() {
        return nroComprobante;
    }

}
