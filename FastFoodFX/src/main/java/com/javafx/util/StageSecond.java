/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import javafx.stage.Stage;
import org.springframework.stereotype.Controller;

/**
 *
 * @author PC
 */
@Controller
public class StageSecond {

    private static Stage stageData = new Stage();

    public static Stage getStageData() {
        return stageData;
    }

    public static void setStageData(Stage stageData) {
        StageSecond.stageData = stageData;
    }

}
