package com.javafx.util;

import java.io.IOException;
import java.net.*;

public class ClosePort {

    private static final int chatPort = 4345;

//    public static void main(String[] args) {
//        go();
//    }
    public static void go() {
        try {
            Socket socket = new Socket("127.0.0.1", chatPort);
            if (socket != null && !socket.isClosed()) {
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace(System.err);
                }
            }
        } catch (Exception e) {
            System.out.println("Error in establishing connection with the client");
        }
    }

}
