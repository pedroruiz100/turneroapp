package com.javafx.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoBack {

    public String getHash(String message) {
        return getH(message);
    }

    private String getH(String message) {
        StringBuilder sb = new StringBuilder();
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.update(message.getBytes());
            byte byteData[] = md.digest();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            return "";
        }
    }
}
