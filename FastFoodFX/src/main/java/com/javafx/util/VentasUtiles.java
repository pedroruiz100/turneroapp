package com.javafx.util;

import com.peluqueria.dao.impl.TalonariosSucursaleDAOImpl;
import com.peluqueria.dto.TalonariosSucursaleDTO;
import com.google.gson.Gson;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.peluqueria.dao.TalonariosSucursaleDAO;
import com.google.gson.GsonBuilder;

public class VentasUtiles {

    private static TalonariosSucursaleDAO taloDAO = new TalonariosSucursaleDAOImpl();
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // Actualiza la tabla talonarioSucursal, el campo nroActual de manera local,
    //para ello debe existir un registro de talonarioSucursales dentro de tabla_dato y se procede a la actualizacion
    //crear un index en tabla_dato y siempre operacion dejarlo en actualizar
    public static boolean registrarTalonarioSucursal(long idTalonario) {
        try {
            TalonariosSucursaleDTO talDTO = taloDAO.getById(idTalonario).toBDTalonariosDTO();
            JSONParser parser = new JSONParser();
            boolean estado = false;
            String uuid = recuperarUuidTalonarioSucursal();
            ConexionPostgres.conectar();
            JSONObject obj = (JSONObject) parser.parse(gson.toJson(talDTO));
            String sql = "UPDATE desarrollo.datos SET descripcion_dato = '" + obj + "', fecha=now(), operacion='actualizar' WHERE id_dato=" + uuid + ";";
            try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
                int op = ps.executeUpdate();
                if (op >= 1) {
                    System.out.println("******* ALMACENANDO DATOS DE MANERA LOCAL  ********");
                    estado = true;
                }
                ps.close();
                ConexionPostgres.getCon().commit();
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                try {
                    ConexionPostgres.getCon().rollback();
                } catch (SQLException ex1) {
                    Utilidades.log.error("ERROR Exception: ", ex1.fillInStackTrace());
                }
            }
            return estado;
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR ParseException: ", ex.fillInStackTrace());
            return false;
        }
    }

    private static String recuperarUuidTalonarioSucursal() {
        //El primer registro debe ser el de talonarioSucursales en CASSANDRA BD
        ConexionPostgres.conectar();
        String sql = "SELECT * FROM desarrollo.datos WHERE tabla_dato='talonarioSucursales';";
        String x = "";
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                x = String.valueOf(rs.getLong("id_dato"));
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
        }
        return x;
    }

    public static long recuperarId() {
        ConexionPostgres.conectar();
        String sql = "select max(id_dato) as id_dato from desarrollo.cabecera";
        long uuid = 0L;
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                uuid = rs.getLong("id_dato");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
        }
        return uuid;
    }

    public static long recuperarIdDetalle() {
        ConexionPostgres.conectar();
        String sql = "select max(id_dato) as id_dato from desarrollo.datos";
        long uuid = 0L;
        try (PreparedStatement ps = ConexionPostgres.getCon().prepareStatement(sql)) {
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                uuid = rs.getLong("id_dato");
            }
            ps.close();
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR SQLException: ", ex.fillInStackTrace());
        }
        return uuid;
    }

}
