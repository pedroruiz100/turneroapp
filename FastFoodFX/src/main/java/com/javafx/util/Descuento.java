/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import com.javafx.util.queries.PromoTemporadaArtLocalQueries;
import com.peluqueria.core.domain.DescuentoFielCab;
import com.peluqueria.core.domain.DescuentoTarjetaCab;
import com.peluqueria.core.domain.DescuentoTarjetaConvenioCab;
import com.peluqueria.core.domain.FuncionarioNf1;
import com.peluqueria.core.domain.FuncionarioNf2;
import com.peluqueria.core.domain.FuncionarioNf3;
import com.peluqueria.core.domain.FuncionarioNf4;
import com.peluqueria.core.domain.FuncionarioNf5;
import com.peluqueria.core.domain.FuncionarioNf6;
import com.peluqueria.core.domain.FuncionarioNf7;
import com.peluqueria.core.domain.PromoTemporada;
import com.peluqueria.core.domain.PromoTemporadaArt;
import com.peluqueria.core.domain.SeccionFunc;
import com.peluqueria.dao.impl.DescuentoFielCabDAOImpl;
import com.peluqueria.dao.impl.DescuentoTarjetaCabDAOImpl;
import com.peluqueria.dao.impl.DescuentoTarjetaConvenioCabDAOImpl;
import com.peluqueria.dao.impl.PromoTemporadaDAOImpl;
import com.peluqueria.dao.impl.SeccionFuncDAOImpl;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.List;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.peluqueria.dao.DescuentoFielCabDAO;
import com.peluqueria.dao.DescuentoFielFormasDePagoDAO;
import com.peluqueria.dao.DescuentoTarjetaCabDAO;
import com.peluqueria.dao.DescuentoTarjetaConvenioCabDAO;
import com.peluqueria.dao.FuncionarioNf1DAO;
import com.peluqueria.dao.FuncionarioNf2DAO;
import com.peluqueria.dao.FuncionarioNf3DAO;
import com.peluqueria.dao.FuncionarioNf4DAO;
import com.peluqueria.dao.FuncionarioNf5DAO;
import com.peluqueria.dao.FuncionarioNf6DAO;
import com.peluqueria.dao.FuncionarioNf7DAO;
import com.peluqueria.dao.PromoTemporadaArtDAO;
import com.peluqueria.dao.PromoTemporadaDAO;
import com.peluqueria.dao.SeccionFuncDAO;
import com.peluqueria.dao.impl.DescuentoFielFormasDePagoDAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf1DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf2DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf3DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf4DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf5DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf6DAOImpl;
import com.peluqueria.dao.impl.FuncionarioNf7DAOImpl;
import com.peluqueria.dao.impl.PromoTemporadaArtDAOImpl;
import com.google.gson.GsonBuilder;
import com.javafx.util.queries.CuponeraLocalQueries;
import com.javafx.util.queries.DescTarjetaCabArtLocalQueries;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import javax.json.bind.Jsonb;
import javax.json.bind.JsonbBuilder;

/**
 *
 * @author ExcelsisWalker
 */
public class Descuento {

    public static final String f1 = "descuentoFiel";
    public static final String f2 = "tarjetaConvenio";
    public static final String f3 = "promocionTemporadaNf";
    public static final String f4 = "tarjetaCreDeb";
    public static final String f5 = "descuentoFuncionario";
    public static final String f6 = "promocionTemporadaArt";
    //almacenando datos para descuento... si se sabe, debería estar en el backend, 
    //pero esto es especial, debido a la inestabilidad en la conexión...
    private static HashMap<String, JSONObject> hashDescFuncionario;//sección; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf1;//nf1; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf2;//nf2; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf3;//nf3; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf4;//nf4; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf5;//nf5; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf6;//nf6; objeto
    private static HashMap<String, JSONObject> hashDescFuncionarioNf7;//nf7; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaConv;//nombre tarjeta convenio; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFiel;//sección; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf1;//nf1; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf2;//nf2; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf3;//nf3; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf4;//nf4; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf5;//nf5; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf6;//nf6; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaFielNf7;//nf7; objeto
    private static JSONObject jsonDescuentoFielFormasDePago;
    private static LinkedHashMap<String, JSONObject> hashDescTarjeta;//nombre tarjeta crédito, débito; objeto
    private static HashMap<String, JSONObject> hashDescTarjetaArt;//nombre tarjeta crédito, débito; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf1;//nf1 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf2;//nf2 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf3;//nf3 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf4;//nf4 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf5;//nf5 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf6;//nf6 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempCabNf7;//nf7 cab; objeto
    private static JSONArray promTempJSONArray;//promociones - secciones en vigencia
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf1;//nf1 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf2;//nf2 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf3;//nf3 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf4;//nf4 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf5;//nf5 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf6;//nf6 cab tar; objeto
    private static HashMap<Long, JSONObject> hashDescTarjetaCabNf7;//nf7 cab tar; objeto

    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf1;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf2;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf3;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf4;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf5;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf6;//nf7 cab tar; objeto última verificación
    private static HashMap<String, JSONObject> hashDescTarjetaCabVerifNf7;//nf7 cab tar; objeto última verificación

    private static HashMap<String, JSONObject> hashPromoTempDetNf1;//nf1 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf2;//nf2 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf3;//nf3 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf4;//nf4 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf5;//nf5 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf6;//nf6 cab; objeto
    private static HashMap<String, JSONObject> hashPromoTempDetNf7;//nf7 cab; objeto
    private static JSONArray promTempArtJSONArray;//promociones - artículos en vigencia

    private static HashMap<Long, JSONObject> hashPromTempArt;
    private static HashMap<Long, JSONObject> hashPromTempArtObsequio;
    private static HashMap<JSONObject, JSONObject> hashPromTempArtCab;

    private static DescuentoFielCabDAO descFielDAO = new DescuentoFielCabDAOImpl();
    private static DescuentoFielFormasDePagoDAO descuentoFielFormasDePagoDAO = new DescuentoFielFormasDePagoDAOImpl();
    private static SeccionFuncDAO seccionFuncDAO = new SeccionFuncDAOImpl();
    private static FuncionarioNf1DAO FuncionarioNf1DAO = new FuncionarioNf1DAOImpl();
    private static FuncionarioNf2DAO FuncionarioNf2DAO = new FuncionarioNf2DAOImpl();
    private static FuncionarioNf3DAO FuncionarioNf3DAO = new FuncionarioNf3DAOImpl();
    private static FuncionarioNf4DAO FuncionarioNf4DAO = new FuncionarioNf4DAOImpl();
    private static FuncionarioNf5DAO FuncionarioNf5DAO = new FuncionarioNf5DAOImpl();
    private static FuncionarioNf6DAO FuncionarioNf6DAO = new FuncionarioNf6DAOImpl();
    private static FuncionarioNf7DAO FuncionarioNf7DAO = new FuncionarioNf7DAOImpl();
    private static DescuentoTarjetaConvenioCabDAO descTarjConvDAO = new DescuentoTarjetaConvenioCabDAOImpl();
    private static DescuentoTarjetaCabDAO descTarjetaDAO = new DescuentoTarjetaCabDAOImpl();
    private static PromoTemporadaDAO promoDAO = new PromoTemporadaDAOImpl();
    private static PromoTemporadaArtDAO promoArtDAO = new PromoTemporadaArtDAOImpl();
    private static Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    //cuponera
    private static JSONObject cuponera;

    private static void iniciando() {
        hashDescTarjetaFiel = new HashMap<>();
        hashDescTarjetaFielNf1 = new HashMap<>();
        hashDescTarjetaFielNf2 = new HashMap<>();
        hashDescTarjetaFielNf3 = new HashMap<>();
        hashDescTarjetaFielNf4 = new HashMap<>();
        hashDescTarjetaFielNf5 = new HashMap<>();
        hashDescTarjetaFielNf6 = new HashMap<>();
        hashDescTarjetaFielNf7 = new HashMap<>();
        hashDescFuncionarioNf1 = new HashMap<>();
        hashDescFuncionarioNf2 = new HashMap<>();
        hashDescFuncionarioNf3 = new HashMap<>();
        hashDescFuncionarioNf4 = new HashMap<>();
        hashDescFuncionarioNf5 = new HashMap<>();
        hashDescFuncionarioNf6 = new HashMap<>();
        hashDescFuncionarioNf7 = new HashMap<>();
        hashPromoTempCabNf1 = new HashMap<>();
        hashPromoTempCabNf2 = new HashMap<>();
        hashPromoTempCabNf3 = new HashMap<>();
        hashPromoTempCabNf4 = new HashMap<>();
        hashPromoTempCabNf5 = new HashMap<>();
        hashPromoTempCabNf6 = new HashMap<>();
        hashPromoTempCabNf7 = new HashMap<>();
        hashPromoTempDetNf1 = new HashMap<>();
        hashPromoTempDetNf2 = new HashMap<>();
        hashPromoTempDetNf3 = new HashMap<>();
        hashPromoTempDetNf4 = new HashMap<>();
        hashPromoTempDetNf5 = new HashMap<>();
        hashPromoTempDetNf6 = new HashMap<>();
        hashPromoTempDetNf7 = new HashMap<>();
        hashDescTarjetaConv = new HashMap<>();
        hashDescTarjetaCabNf1 = new HashMap<>();
        hashDescTarjetaCabNf2 = new HashMap<>();
        hashDescTarjetaCabNf3 = new HashMap<>();
        hashDescTarjetaCabNf4 = new HashMap<>();
        hashDescTarjetaCabNf5 = new HashMap<>();
        hashDescTarjetaCabNf6 = new HashMap<>();
        hashDescTarjetaCabNf7 = new HashMap<>();
        hashDescTarjetaCabVerifNf1 = new HashMap<>();
        hashDescTarjetaCabVerifNf2 = new HashMap<>();
        hashDescTarjetaCabVerifNf3 = new HashMap<>();
        hashDescTarjetaCabVerifNf4 = new HashMap<>();
        hashDescTarjetaCabVerifNf5 = new HashMap<>();
        hashDescTarjetaCabVerifNf6 = new HashMap<>();
        hashDescTarjetaCabVerifNf7 = new HashMap<>();
        hashDescTarjeta = new LinkedHashMap<>();
        hashDescTarjetaArt = new HashMap<>();
        hashDescFuncionario = new HashMap<>();
        cuponera();
    }

    public static void resetParam(long idTipoCaja) {
        long ini = System.currentTimeMillis();
        iniciando();
        if (idTipoCaja == 1l) {
            descuentoFuncionario();
            descuentoPromocionTemporada();
            descuentoPromocionTemporadaArt();
            descuentoTarjeta();
            descuentoTarjetaConvenio();
            descuentoTarjetaFiel();
        }
        long fin = System.currentTimeMillis() - ini;
        System.out.println("descuento -> proceso en ms: " + fin);
    }

    private static void descuentoFuncionario() {
        jsonArrayDescFuncionario();
        jsonArrayDescFuncionarioNf1();
        jsonArrayDescFuncionarioNf2();
        jsonArrayDescFuncionarioNf3();
        jsonArrayDescFuncionarioNf4();
        jsonArrayDescFuncionarioNf5();
        jsonArrayDescFuncionarioNf6();
        jsonArrayDescFuncionarioNf7();
    }

    private static void descuentoPromocionTemporada() {
        jsonArrayDescPromTemp();
        for (int i = 0; i < promTempJSONArray.size(); i++) {
            JSONObject jsonPromoTemp = (JSONObject) promTempJSONArray.get(i);
            if (jsonPromoTemp.get("promoTemporadaNf1s") != null) {
                JSONArray jsonArrayPromoTemporadaNf1s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf1s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf1s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf1 = (JSONObject) jsonArrayPromoTemporadaNf1s.get(j);
                    hashPromoTempCabNf1.put(jsonPromoTemporadaNf1.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf1.put(jsonPromoTemporadaNf1.get("descriSeccion").toString(), jsonPromoTemporadaNf1);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf2s") != null) {
                JSONArray jsonArrayPromoTemporadaNf2s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf2s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf2s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf2 = (JSONObject) jsonArrayPromoTemporadaNf2s.get(j);
                    hashPromoTempCabNf2.put(jsonPromoTemporadaNf2.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf2.put(jsonPromoTemporadaNf2.get("descriSeccion").toString(), jsonPromoTemporadaNf2);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf3s") != null) {
                JSONArray jsonArrayPromoTemporadaNf3s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf3s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf3s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf3 = (JSONObject) jsonArrayPromoTemporadaNf3s.get(j);
                    hashPromoTempCabNf3.put(jsonPromoTemporadaNf3.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf3.put(jsonPromoTemporadaNf3.get("descriSeccion").toString(), jsonPromoTemporadaNf3);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf4s") != null) {
                JSONArray jsonArrayPromoTemporadaNf4s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf4s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf4s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf4 = (JSONObject) jsonArrayPromoTemporadaNf4s.get(j);
                    hashPromoTempCabNf4.put(jsonPromoTemporadaNf4.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf4.put(jsonPromoTemporadaNf4.get("descriSeccion").toString(), jsonPromoTemporadaNf4);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf5s") != null) {
                JSONArray jsonArrayPromoTemporadaNf5s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf5s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf5s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf5 = (JSONObject) jsonArrayPromoTemporadaNf5s.get(j);
                    hashPromoTempCabNf5.put(jsonPromoTemporadaNf5.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf5.put(jsonPromoTemporadaNf5.get("descriSeccion").toString(), jsonPromoTemporadaNf5);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf6s") != null) {
                JSONArray jsonArrayPromoTemporadaNf6s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf6s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf6s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf6 = (JSONObject) jsonArrayPromoTemporadaNf6s.get(j);
                    hashPromoTempCabNf6.put(jsonPromoTemporadaNf6.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf6.put(jsonPromoTemporadaNf6.get("descriSeccion").toString(), jsonPromoTemporadaNf6);
                }
            }
            if (jsonPromoTemp.get("promoTemporadaNf7s") != null) {
                JSONArray jsonArrayPromoTemporadaNf7s = (JSONArray) jsonPromoTemp.get("promoTemporadaNf7s");
                for (int j = 0; j < jsonArrayPromoTemporadaNf7s.size(); j++) {
                    JSONObject jsonPromoTemporadaNf7 = (JSONObject) jsonArrayPromoTemporadaNf7s.get(j);
                    hashPromoTempCabNf7.put(jsonPromoTemporadaNf7.get("descriSeccion").toString(), jsonPromoTemp);
                    hashPromoTempDetNf7.put(jsonPromoTemporadaNf7.get("descriSeccion").toString(), jsonPromoTemporadaNf7);
                }
            }
        }
    }

    private static void cuponera() {
        if (ConexionPostgres.conectarLocal()) {
            cuponera = CuponeraLocalQueries.cuponera();
        }
        if (ConexionPostgres.getConLocal() != null) {
            ConexionPostgres.cerrarLocal();
        }
    }

    ///BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->
    //////READ, DESCUENTO FIEL -> GET
    private static void jsonArrayDescTarjetaFiel() {
        JSONParser parser = new JSONParser();
        JSONArray cabFielJSONArray = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielCab");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    cabFielJSONArray = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                cabFielJSONArray = generarDescuentoFielCabLocal();
            }
        } catch (IOException | ParseException ex) {
            cabFielJSONArray = generarDescuentoFielCabLocal();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object cabFielObj : cabFielJSONArray) {
            JSONObject cabFielJSONObj = (JSONObject) cabFielObj;
            if (cabFielJSONObj.get("descuentoFielNf1s") != null) {
                JSONArray jsonArrayDescuentoFielNf1s = (JSONArray) cabFielJSONObj.get("descuentoFielNf1s");
                JSONObject jsonObjectDescuentoFielNf1 = (JSONObject) jsonArrayDescuentoFielNf1s.get(0);
                JSONObject jsonObjectNf1 = (JSONObject) jsonObjectDescuentoFielNf1.get("nf1Tipo");
                hashDescTarjetaFielNf1.put(String.valueOf(jsonObjectNf1.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf2s") != null) {
                JSONArray jsonArrayDescuentoFielNf2s = (JSONArray) cabFielJSONObj.get("descuentoFielNf2s");
                JSONObject jsonObjectDescuentoFielNf2 = (JSONObject) jsonArrayDescuentoFielNf2s.get(0);
                JSONObject jsonObjectNf2 = (JSONObject) jsonObjectDescuentoFielNf2.get("nf2Sfamilia");
                hashDescTarjetaFielNf2.put(String.valueOf(jsonObjectNf2.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf3s") != null) {
                JSONArray jsonArrayDescuentoFielNf3s = (JSONArray) cabFielJSONObj.get("descuentoFielNf3s");
                JSONObject jsonObjectDescuentoFielNf3 = (JSONObject) jsonArrayDescuentoFielNf3s.get(0);
                JSONObject jsonObjectNf3 = (JSONObject) jsonObjectDescuentoFielNf3.get("nf3Sseccion");
                hashDescTarjetaFielNf3.put(String.valueOf(jsonObjectNf3.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf4s") != null) {
                JSONArray jsonArrayDescuentoFielNf4s = (JSONArray) cabFielJSONObj.get("descuentoFielNf4s");
                JSONObject jsonObjectDescuentoFielNf4 = (JSONObject) jsonArrayDescuentoFielNf4s.get(0);
                JSONObject jsonObjectNf4 = (JSONObject) jsonObjectDescuentoFielNf4.get("nf4Seccion1");
                hashDescTarjetaFielNf4.put(String.valueOf(jsonObjectNf4.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf5s") != null) {
                JSONArray jsonArrayDescuentoFielNf5s = (JSONArray) cabFielJSONObj.get("descuentoFielNf5s");
                JSONObject jsonObjectDescuentoFielNf5 = (JSONObject) jsonArrayDescuentoFielNf5s.get(0);
                JSONObject jsonObjectNf5 = (JSONObject) jsonObjectDescuentoFielNf5.get("nf5Seccion2");
                hashDescTarjetaFielNf5.put(String.valueOf(jsonObjectNf5.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf6s") != null) {
                JSONArray jsonArrayDescuentoFielNf6s = (JSONArray) cabFielJSONObj.get("descuentoFielNf6s");
                JSONObject jsonObjectDescuentoFielNf6 = (JSONObject) jsonArrayDescuentoFielNf6s.get(0);
                JSONObject jsonObjectNf6 = (JSONObject) jsonObjectDescuentoFielNf6.get("nf6Secnom6");
                hashDescTarjetaFielNf6.put(String.valueOf(jsonObjectNf6.get("descripcion")), cabFielJSONObj);
            } else if (cabFielJSONObj.get("descuentoFielNf7s") != null) {
                JSONArray jsonArrayDescuentoFielNf7s = (JSONArray) cabFielJSONObj.get("descuentoFielNf7s");
                JSONObject jsonObjectDescuentoFielNf7 = (JSONObject) jsonArrayDescuentoFielNf7s.get(0);
                JSONObject jsonObjectNf7 = (JSONObject) jsonObjectDescuentoFielNf7.get("nf7Secnom7");
                hashDescTarjetaFielNf7.put(String.valueOf(jsonObjectNf7.get("descripcion")), cabFielJSONObj);
            }
        }
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoFielFormasDePago/getById/1");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonDescuentoFielFormasDePago = (JSONObject) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonDescuentoFielFormasDePago = generandoDescuentoFielFormasDePago();
            }
        } catch (IOException | ParseException ex) {
            jsonDescuentoFielFormasDePago = generandoDescuentoFielFormasDePago();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //////READ, DESCUENTO FIEL -> GET

    //////READ, DESCUENTO FUNCIONARIO -> GET
    private static void jsonArrayDescFuncionario() {
        JSONParser parser = new JSONParser();
        JSONArray cabFuncionarioJSONArray = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/seccionFunc");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    cabFuncionarioJSONArray = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                cabFuncionarioJSONArray = generarSeccionFuncLocal();
            }
        } catch (IOException | ParseException ex) {
            cabFuncionarioJSONArray = generarSeccionFuncLocal();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : cabFuncionarioJSONArray) {
            JSONObject descFuncionarioJSONObj = (JSONObject) descFuncionarioObj;
            JSONObject seccion = (JSONObject) descFuncionarioJSONObj.get("seccion");
            hashDescFuncionario.put(String.valueOf(seccion.get("descripcion")), descFuncionarioJSONObj);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO -> GET

    //////READ, DESCUENTO FUNCIONARIO NF1 -> GET
    private static void jsonArrayDescFuncionarioNf1() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf1 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf1");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf1 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf1 = generarFuncionarioNf1Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf1 = generarFuncionarioNf1Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf1) {
            JSONObject jsonFuncionarioNf1 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf1 = (JSONObject) jsonFuncionarioNf1.get("nf1Tipo");
            hashDescFuncionarioNf1.put(String.valueOf(jsonNf1.get("descripcion")), jsonFuncionarioNf1);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF1 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF2 -> GET
    private static void jsonArrayDescFuncionarioNf2() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf2 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf2");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf2 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf2 = generarFuncionarioNf2Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf2 = generarFuncionarioNf2Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf2) {
            JSONObject jsonFuncionarioNf2 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf2 = (JSONObject) jsonFuncionarioNf2.get("nf2Sfamilia");
            hashDescFuncionarioNf2.put(String.valueOf(jsonNf2.get("descripcion")), jsonFuncionarioNf2);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF2 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF3 -> GET
    private static void jsonArrayDescFuncionarioNf3() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf3 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf3");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf3 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf3 = generarFuncionarioNf3Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf3 = generarFuncionarioNf3Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf3) {
            JSONObject jsonFuncionarioNf3 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf3 = (JSONObject) jsonFuncionarioNf3.get("nf3Sseccion");
            hashDescFuncionarioNf3.put(String.valueOf(jsonNf3.get("descripcion")), jsonFuncionarioNf3);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF3 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF4 -> GET
    private static void jsonArrayDescFuncionarioNf4() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf4 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf4");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf4 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf4 = generarFuncionarioNf4Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf4 = generarFuncionarioNf4Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf4) {
            JSONObject jsonFuncionarioNf4 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf4 = (JSONObject) jsonFuncionarioNf4.get("nf4Seccion1");
            hashDescFuncionarioNf4.put(String.valueOf(jsonNf4.get("descripcion")), jsonFuncionarioNf4);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF4 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF5 -> GET
    private static void jsonArrayDescFuncionarioNf5() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf5 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf5");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf5 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf5 = generarFuncionarioNf5Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf5 = generarFuncionarioNf5Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf5) {
            JSONObject jsonFuncionarioNf5 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf5 = (JSONObject) jsonFuncionarioNf5.get("nf5Seccion2");
            hashDescFuncionarioNf5.put(String.valueOf(jsonNf5.get("descripcion")), jsonFuncionarioNf5);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF5 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF6 -> GET
    private static void jsonArrayDescFuncionarioNf6() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf6 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf6");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf6 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf6 = generarFuncionarioNf6Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf6 = generarFuncionarioNf6Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf6) {
            JSONObject jsonFuncionarioNf6 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf6 = (JSONObject) jsonFuncionarioNf6.get("nf6Secnom6");
            hashDescFuncionarioNf6.put(String.valueOf(jsonNf6.get("descripcion")), jsonFuncionarioNf6);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF6 -> GET

    /////READ, DESCUENTO FUNCIONARIO NF7 -> GET
    private static void jsonArrayDescFuncionarioNf7() {
        JSONParser parser = new JSONParser();
        JSONArray jsonArrayFuncionarioNf7 = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/funcionarioNf7");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    jsonArrayFuncionarioNf7 = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                jsonArrayFuncionarioNf7 = generarFuncionarioNf7Local();
            }
        } catch (IOException | ParseException ex) {
            jsonArrayFuncionarioNf7 = generarFuncionarioNf7Local();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object descFuncionarioObj : jsonArrayFuncionarioNf7) {
            JSONObject jsonFuncionarioNf7 = (JSONObject) descFuncionarioObj;
            JSONObject jsonNf7 = (JSONObject) jsonFuncionarioNf7.get("nf7Secnom7");
            hashDescFuncionarioNf7.put(String.valueOf(jsonNf7.get("descripcion")), jsonFuncionarioNf7);
        }
    }
    //////READ, DESCUENTO FUNCIONARIO NF7 -> GET

    //////READ, DESCUENTO TARJETA CONVENIO -> GET
    private static void jsonArrayDescTarjetaConvenio() {
        JSONParser parser = new JSONParser();
        JSONArray cabTarjetaConvenioJSONArray = new JSONArray();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/descuentoTarjetaConvenioCab");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    cabTarjetaConvenioJSONArray = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                cabTarjetaConvenioJSONArray = generarDescTarjConvLocal();
            }
        } catch (IOException | ParseException ex) {
            cabTarjetaConvenioJSONArray = generarDescTarjConvLocal();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        for (Object cabTarjConvObj : cabTarjetaConvenioJSONArray) {
            JSONObject cabTarjConvJSONObj = (JSONObject) cabTarjConvObj;
            //mapeo mediante el nombre de tarjeta en la línea de tiempo, si esta cambia de descripción y mantiene referencia, no se aplicará el descuento
            //el usuario del sistema deberá dar de baja el descuento con el nombre viejo, para actualizar el nuevo descuento, en caso de querer implementar...
            hashDescTarjetaConv.put(String.valueOf(cabTarjConvJSONObj.get("descriTarjetaConvenio")), cabTarjConvJSONObj);
        }
    }
    //////READ, DESCUENTO TARJETA CONVENIO -> GET

    //////READ, DESCUENTO TARJETA -> GET
    private static void jsonArrayDescTarjeta() {
        JSONArray cabTarjetaJSONArray = new JSONArray();
        if (ConexionPostgres.conectarLocal()) {
            cabTarjetaJSONArray = DescTarjetaCabArtLocalQueries.tarjetaDtoArticulos();
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            for (Object cabTarjObj : cabTarjetaJSONArray) {
                JSONObject cabTarjJSONObj = (JSONObject) cabTarjObj;
                //mapeo mediante el nombre de tarjeta en la línea de tiempo, si esta cambia de descripción y mantiene referencia, no se aplicará el descuento
                //el usuario del sistema deberá dar de baja el descuento con el nombre viejo, para actualizar el nuevo descuento, en caso de querer implementar...
                if (hashDescTarjeta.containsKey(String.valueOf(cabTarjJSONObj.get("descriTarjeta")))) {
                    JSONObject cabTarjJSONObjA = hashDescTarjeta.get(String.valueOf(cabTarjJSONObj.get("descriTarjeta")));
                    Timestamp tsA = Utilidades.objectToTimestamp(cabTarjJSONObjA.get("fechaInicio"));
                    Timestamp tsB = Utilidades.objectToTimestamp(cabTarjJSONObj.get("fechaInicio"));
                    Timestamp tsNow = new Timestamp(System.currentTimeMillis());
                    Date parsedDate;
                    Timestamp tsC = null;
                    try {
                        parsedDate = dateFormat.parse(tsNow.toString());
                        tsC = new Timestamp(parsedDate.getTime());//
                    } catch (java.text.ParseException ex) {
                        Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
                    }
                    if (tsB.after(tsA) && (tsC.compareTo(tsB) >= 0)) {
                        hashDescTarjeta.put(String.valueOf(cabTarjJSONObj.get("descriTarjeta")), cabTarjJSONObj);
                        asignandoDescTarjCabNfArt(cabTarjJSONObj);
                    }
                } else {
                    hashDescTarjeta.put(String.valueOf(cabTarjJSONObj.get("descriTarjeta")), cabTarjJSONObj);
                    asignandoDescTarjCabNfArt(cabTarjJSONObj);
                }
            }
        }
        if (ConexionPostgres.getConLocal() != null) {
            ConexionPostgres.cerrarLocal();
        }
    }

    private static void asignandoDescTarjCabNfArt(JSONObject cabTarjJSONObj) {
        JSONArray jsonArrayDescuentoTarjetaCabArticulo = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabArticulos");
        if (jsonArrayDescuentoTarjetaCabArticulo != null) {
            if (!jsonArrayDescuentoTarjetaCabArticulo.isEmpty()) {
                for (Object objectDescuentoTarjetaCabArticulo : jsonArrayDescuentoTarjetaCabArticulo) {
                    JSONObject jsonDescuentoTarjetaCabArticulo = (JSONObject) objectDescuentoTarjetaCabArticulo;
                    JSONObject jsonArticulo = (JSONObject) jsonDescuentoTarjetaCabArticulo.get("articulo");
                    hashDescTarjetaArt.put(cabTarjJSONObj.get("descriTarjeta") + " (y) " + jsonArticulo.get("codArticulo"), jsonArticulo);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf1 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf1s");
        if (jsonArrayDescuentoTarjetaCabNf1 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf1.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf1 : jsonArrayDescuentoTarjetaCabNf1) {
                    JSONObject jsonDescuentoTarjetaCabNf1 = (JSONObject) objectDescuentoTarjetaCabNf1;
                    JSONObject jsonNf1 = (JSONObject) jsonDescuentoTarjetaCabNf1.get("nf1Tipo");
                    hashDescTarjetaCabNf1.put(Long.valueOf(jsonNf1.get("idNf1Tipo").toString()), jsonNf1);
                    hashDescTarjetaCabVerifNf1.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf1.get("idNf1Tipo").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf2 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf2s");
        if (jsonArrayDescuentoTarjetaCabNf2 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf2.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf2 : jsonArrayDescuentoTarjetaCabNf2) {
                    JSONObject jsonDescuentoTarjetaCabNf2 = (JSONObject) objectDescuentoTarjetaCabNf2;
                    JSONObject jsonNf2 = (JSONObject) jsonDescuentoTarjetaCabNf2.get("nf2Sfamilia");
                    hashDescTarjetaCabNf2.put(Long.valueOf(jsonNf2.get("idNf2Sfamilia").toString()), jsonNf2);
                    hashDescTarjetaCabVerifNf2.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf2.get("idNf2Sfamilia").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf3 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf3s");
        if (jsonArrayDescuentoTarjetaCabNf3 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf3.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf3 : jsonArrayDescuentoTarjetaCabNf3) {
                    JSONObject jsonDescuentoTarjetaCabNf3 = (JSONObject) objectDescuentoTarjetaCabNf3;
                    JSONObject jsonNf3 = (JSONObject) jsonDescuentoTarjetaCabNf3.get("nf3Sseccion");
                    hashDescTarjetaCabNf3.put(Long.valueOf(jsonNf3.get("idNf3Sseccion").toString()), jsonNf3);
                    hashDescTarjetaCabVerifNf3.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf3.get("idNf3Sseccion").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf4 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf4s");
        if (jsonArrayDescuentoTarjetaCabNf4 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf4.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf4 : jsonArrayDescuentoTarjetaCabNf4) {
                    JSONObject jsonDescuentoTarjetaCabNf4 = (JSONObject) objectDescuentoTarjetaCabNf4;
                    JSONObject jsonNf4 = (JSONObject) jsonDescuentoTarjetaCabNf4.get("nf4Seccion1");
                    hashDescTarjetaCabNf4.put(Long.valueOf(jsonNf4.get("idNf4Seccion1").toString()), jsonNf4);
                    hashDescTarjetaCabVerifNf4.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf4.get("idNf4Seccion1").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf5 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf5s");
        if (jsonArrayDescuentoTarjetaCabNf5 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf5.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf5 : jsonArrayDescuentoTarjetaCabNf5) {
                    JSONObject jsonDescuentoTarjetaCabNf5 = (JSONObject) objectDescuentoTarjetaCabNf5;
                    JSONObject jsonNf5 = (JSONObject) jsonDescuentoTarjetaCabNf5.get("nf5Seccion2");
                    hashDescTarjetaCabNf5.put(Long.valueOf(jsonNf5.get("idNf5Seccion2").toString()), jsonNf5);
                    hashDescTarjetaCabVerifNf5.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf5.get("idNf5Seccion2").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf6 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf6s");
        if (jsonArrayDescuentoTarjetaCabNf6 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf6.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf6 : jsonArrayDescuentoTarjetaCabNf6) {
                    JSONObject jsonDescuentoTarjetaCabNf6 = (JSONObject) objectDescuentoTarjetaCabNf6;
                    JSONObject jsonNf6 = (JSONObject) jsonDescuentoTarjetaCabNf6.get("nf6Secnom6");
                    hashDescTarjetaCabNf6.put(Long.valueOf(jsonNf6.get("idNf6Secnom6").toString()), jsonNf6);
                    hashDescTarjetaCabVerifNf6.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf6.get("idNf6Secnom6").toString() + "]", cabTarjJSONObj);
                }
            }
        }
        JSONArray jsonArrayDescuentoTarjetaCabNf7 = (JSONArray) cabTarjJSONObj.get("descuentoTarjetaCabNf7s");
        if (jsonArrayDescuentoTarjetaCabNf7 != null) {
            if (!jsonArrayDescuentoTarjetaCabNf7.isEmpty()) {
                for (Object objectDescuentoTarjetaCabNf7 : jsonArrayDescuentoTarjetaCabNf7) {
                    JSONObject jsonDescuentoTarjetaCabNf7 = (JSONObject) objectDescuentoTarjetaCabNf7;
                    JSONObject jsonNf7 = (JSONObject) jsonDescuentoTarjetaCabNf7.get("nf7Secnom7");
                    hashDescTarjetaCabNf7.put(Long.valueOf(jsonNf7.get("idNf7Secnom7").toString()), jsonNf7);
                    hashDescTarjetaCabVerifNf7.put("[" + cabTarjJSONObj.get("idDescuentoTarjetaCab").toString() + "] [" + jsonNf7.get("idNf7Secnom7").toString() + "]", cabTarjJSONObj);
                }
            }
        }
    }
    //////READ, DESCUENTO TARJETA -> GET

    //////READ, PROMOCIÓN TEMPORADA -> GET
    private static void jsonArrayDescPromTemp() {
        JSONParser parser = new JSONParser();
        try {
            if (Utilidades.pingHost(Utilidades.host, Utilidades.port, Utilidades.time)) {
                URL url = new URL(Utilidades.ip + "/ServerParana/promoTemporada");
                URLConnection uc = url.openConnection();
                BufferedReader br = new BufferedReader(new InputStreamReader(uc.getInputStream(), "UTF-8"));
                String inputLine;
                while ((inputLine = br.readLine()) != null) {
                    promTempJSONArray = (JSONArray) parser.parse(inputLine);
                }
                br.close();
            } else {
                promTempJSONArray = generarPromoTempLocal();
            }
        } catch (IOException | ParseException ex) {
            promTempJSONArray = generarPromoTempLocal();
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
    //////READ, PROMOCIÓN TEMPORADA -> GET

    //////READ, PROMOCIÓN TEMPORADA ART. -> GET
    private static void jsonArrayDescPromTempArt() {
        if (ConexionPostgres.conectarLocal()) {
            promTempArtJSONArray = PromoTemporadaArtLocalQueries.promocionArticulos();
            Date date = new Date();
            Timestamp tsNow = new Timestamp(date.getTime());
            hashPromTempArt = new HashMap<>();
            hashPromTempArtObsequio = new HashMap<>();
            hashPromTempArtCab = new HashMap<>();
            for (Object jsonObjectPromArt : promTempArtJSONArray) {
                JSONObject jsonPromoTempArt = (JSONObject) jsonObjectPromArt;
                Timestamp tsIni = Utilidades.objectToTimestamp(jsonPromoTempArt.get("fechaInicio"));
                Timestamp tsFin = Utilidades.objectToTimestamp(jsonPromoTempArt.get("fechaFin"));
                tsFin.setHours(23);
                tsFin.setMinutes(59);
                tsFin.setSeconds(59);
                if (tsNow.after(tsIni) && tsNow.before(tsFin)) {
                    if (jsonPromoTempArt.containsKey("artPromoTemporadaObsequio")) {
                        if (jsonPromoTempArt.get("artPromoTemporadaObsequio") != null) {
                            JSONArray jsonArrayPromoTempArtDetObsequio = (JSONArray) jsonPromoTempArt.get("artPromoTemporadaObsequio");
                            for (Object jsonObjectPromArtDetObsequio : jsonArrayPromoTempArtDetObsequio) {
                                JSONObject jsonPromoTempArtDetObsequio = (JSONObject) jsonObjectPromArtDetObsequio;
                                JSONObject jsonArticulo = (JSONObject) jsonPromoTempArtDetObsequio.get("articulo");
                                hashPromTempArtObsequio.put(Long.valueOf(jsonArticulo.get("codArticulo").toString()), jsonPromoTempArtDetObsequio);
                                hashPromTempArtCab.put(jsonPromoTempArtDetObsequio, jsonPromoTempArt);
                            }
                        }
                    }
                    if (jsonPromoTempArt.containsKey("artPromoTemporadaDTO")) {
                        if (jsonPromoTempArt.get("artPromoTemporadaDTO") != null) {
                            JSONArray jsonArrayPromoTempArtDet = (JSONArray) jsonPromoTempArt.get("artPromoTemporadaDTO");
                            for (Object jsonObjectPromArtDet : jsonArrayPromoTempArtDet) {
                                JSONObject jsonPromoTempArtDet = (JSONObject) jsonObjectPromArtDet;
                                JSONObject jsonArticulo = (JSONObject) jsonPromoTempArtDet.get("articulo");
                                hashPromTempArt.put(Long.valueOf(jsonArticulo.get("codArticulo").toString()), jsonPromoTempArtDet);
                                hashPromTempArtCab.put(jsonPromoTempArtDet, jsonPromoTempArt);
                            }
                        }
                    }
                }
            }
        }
        if (ConexionPostgres.getConLocal() != null) {
            ConexionPostgres.cerrarLocal();
        }
    }
    //////READ, PROMOCIÓN TEMPORADA ART. -> GET
    ///BACKEND BACKEND BACKEND ************************** -> -> -> -> -> -> -> ->

    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->
    //////READ, DESCUENTO FIEL
    private static JSONArray generarDescuentoFielCabLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<DescuentoFielCab> listDesc = descFielDAO.listar();
        for (DescuentoFielCab desc : listDesc) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoFielCabDTOEntitiesFull()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FIEL

    //////READ, DESCUENTO FIEL FORMAS DE PAGO
    private static JSONObject generandoDescuentoFielFormasDePago() {
        JSONObject jsonDtoFielFormaPago = null;
        JSONParser parser = new JSONParser();
        Jsonb jsonb = JsonbBuilder.create();
        try {
            jsonDtoFielFormaPago = (JSONObject) parser.parse(jsonb.toJson(descuentoFielFormasDePagoDAO.getById(1l)));
        } catch (ParseException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return jsonDtoFielFormaPago;
    }
    //////READ, DESCUENTO FIEL FORMAS DE PAGO

    //////READ, DESCUENTO FUNCIONARIO
    private static JSONArray generarSeccionFuncLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<SeccionFunc> listSeccion = seccionFuncDAO.listar();
        for (SeccionFunc sec : listSeccion) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(sec.toSeccionFuncDTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO

    //////READ, DESCUENTO FUNCIONARIO NF1
    private static JSONArray generarFuncionarioNf1Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf1> funcionarioNf1s = FuncionarioNf1DAO.listar();
        for (FuncionarioNf1 funcionarioNf1 : funcionarioNf1s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf1.toFuncionarioNf1DTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF1

    //////READ, DESCUENTO FUNCIONARIO NF2
    private static JSONArray generarFuncionarioNf2Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf2> funcionarioNf2s = FuncionarioNf2DAO.listar();
        for (FuncionarioNf2 funcionarioNf2 : funcionarioNf2s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf2.toFuncionarioNf2DTONf1()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF2

    //////READ, DESCUENTO FUNCIONARIO NF3
    private static JSONArray generarFuncionarioNf3Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf3> funcionarioNf3s = FuncionarioNf3DAO.listar();
        for (FuncionarioNf3 funcionarioNf3 : funcionarioNf3s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf3.toFuncionarioNf3DTONf2()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF3

    //////READ, DESCUENTO FUNCIONARIO NF4
    private static JSONArray generarFuncionarioNf4Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf4> funcionarioNf4s = FuncionarioNf4DAO.listar();
        for (FuncionarioNf4 funcionarioNf4 : funcionarioNf4s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf4.toFuncionarioNf4DTONf3()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF4

    //////READ, DESCUENTO FUNCIONARIO NF5
    private static JSONArray generarFuncionarioNf5Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf5> funcionarioNf5s = FuncionarioNf5DAO.listar();
        for (FuncionarioNf5 funcionarioNf5 : funcionarioNf5s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf5.toFuncionarioNf5DTONf4()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF5

    //////READ, DESCUENTO FUNCIONARIO NF6
    private static JSONArray generarFuncionarioNf6Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf6> funcionarioNf6s = FuncionarioNf6DAO.listar();
        for (FuncionarioNf6 funcionarioNf6 : funcionarioNf6s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf6.toFuncionarioNf6DTONf5()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF6

    //////READ, DESCUENTO FUNCIONARIO NF7
    private static JSONArray generarFuncionarioNf7Local() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<FuncionarioNf7> funcionarioNf7s = FuncionarioNf7DAO.listar();
        for (FuncionarioNf7 funcionarioNf7 : funcionarioNf7s) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(funcionarioNf7.toFuncionarioNf7DTONf6()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO FUNCIONARIO NF7

    //////READ, DESCUENTO TARJETA CONVENIO
    private static JSONArray generarDescTarjConvLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<DescuentoTarjetaConvenioCab> listDesc = descTarjConvDAO.listar();
        for (DescuentoTarjetaConvenioCab descTarj : listDesc) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(descTarj.toDescuentoTarjetaConvenioCabDTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO TARJETA CONVENIO

    //////READ, DESCUENTO TARJETA
    private static JSONArray generarDescTarjetaLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<DescuentoTarjetaCab> descTarj = descTarjetaDAO.listar();
        for (DescuentoTarjetaCab desc : descTarj) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(desc.toDescuentoTarjetaCabDTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, DESCUENTO TARJETA

    //////READ, PROMOCIÓN TEMPORADA
    private static JSONArray generarPromoTempLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<PromoTemporada> promo = promoDAO.listar();
        for (PromoTemporada p : promo) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(p.toPromoTemporadaDTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        System.out.println(jsonArray.toString());
        return jsonArray;
    }
    //////READ, PROMOCIÓN TEMPORADA

    //////READ, PROMOCIÓN TEMPORADA
    private static JSONArray generarPromoTempArtLocal() {
        JSONArray jsonArray = new JSONArray();
        JSONParser parser = new JSONParser();
        List<PromoTemporadaArt> promo = promoArtDAO.listar();
        for (PromoTemporadaArt p : promo) {
            try {
                JSONObject json = (JSONObject) parser.parse(gson.toJson(p.toPromoTemporadaArtDTO()));
                jsonArray.add(json);
            } catch (ParseException ex) {
                Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
            }
        }
        return jsonArray;
    }
    //////READ, PROMOCIÓN TEMPORADA
    //LOCAL LOCAL LOCAL ******************************** -> -> -> -> -> -> -> ->

    private static void descuentoPromocionTemporadaArt() {
        jsonArrayDescPromTempArt();
    }

    private static void descuentoTarjetaConvenio() {
        jsonArrayDescTarjetaConvenio();
    }

    private static void descuentoTarjeta() {
        jsonArrayDescTarjeta();
    }

    private static void descuentoTarjetaFiel() {
        jsonArrayDescTarjetaFiel();
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFiel() {
        return hashDescTarjetaFiel;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionario() {
        return hashDescFuncionario;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaConv() {
        return hashDescTarjetaConv;
    }

    public static HashMap<String, JSONObject> getHashDescTarjeta() {
        return hashDescTarjeta;
    }

    public static JSONArray getPromTempJSONArray() {
        return promTempJSONArray;
    }

    public static JSONArray getPromTempArtJSONArray() {
        return promTempArtJSONArray;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf1() {
        return hashDescTarjetaFielNf1;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf2() {
        return hashDescTarjetaFielNf2;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf3() {
        return hashDescTarjetaFielNf3;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf4() {
        return hashDescTarjetaFielNf4;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf5() {
        return hashDescTarjetaFielNf5;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf6() {
        return hashDescTarjetaFielNf6;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaFielNf7() {
        return hashDescTarjetaFielNf7;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf1() {
        return hashDescFuncionarioNf1;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf2() {
        return hashDescFuncionarioNf2;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf3() {
        return hashDescFuncionarioNf3;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf4() {
        return hashDescFuncionarioNf4;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf5() {
        return hashDescFuncionarioNf5;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf6() {
        return hashDescFuncionarioNf6;
    }

    public static HashMap<String, JSONObject> getHashDescFuncionarioNf7() {
        return hashDescFuncionarioNf7;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf1() {
        return hashPromoTempCabNf1;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf2() {
        return hashPromoTempCabNf2;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf3() {
        return hashPromoTempCabNf3;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf4() {
        return hashPromoTempCabNf4;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf5() {
        return hashPromoTempCabNf5;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf6() {
        return hashPromoTempCabNf6;
    }

    public static HashMap<String, JSONObject> getHashPromoTempCabNf7() {
        return hashPromoTempCabNf7;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf1() {
        return hashPromoTempDetNf1;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf2() {
        return hashPromoTempDetNf2;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf3() {
        return hashPromoTempDetNf3;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf4() {
        return hashPromoTempDetNf4;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf5() {
        return hashPromoTempDetNf5;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf6() {
        return hashPromoTempDetNf6;
    }

    public static HashMap<String, JSONObject> getHashPromoTempDetNf7() {
        return hashPromoTempDetNf7;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaArt() {
        return hashDescTarjetaArt;
    }

    public static JSONObject getJsonDescuentoFielFormasDePago() {
        return jsonDescuentoFielFormasDePago;
    }

    public static HashMap<Long, JSONObject> getHashPromTempArt() {
        return hashPromTempArt;
    }

    public static HashMap<Long, JSONObject> getHashPromTempArtObsequio() {
        return hashPromTempArtObsequio;
    }

    public static HashMap<JSONObject, JSONObject> getHashPromTempArtCab() {
        return hashPromTempArtCab;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf1() {
        return hashDescTarjetaCabNf1;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf2() {
        return hashDescTarjetaCabNf2;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf3() {
        return hashDescTarjetaCabNf3;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf4() {
        return hashDescTarjetaCabNf4;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf5() {
        return hashDescTarjetaCabNf5;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf6() {
        return hashDescTarjetaCabNf6;
    }

    public static HashMap<Long, JSONObject> getHashDescTarjetaCabNf7() {
        return hashDescTarjetaCabNf7;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf1() {
        return hashDescTarjetaCabVerifNf1;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf2() {
        return hashDescTarjetaCabVerifNf2;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf3() {
        return hashDescTarjetaCabVerifNf3;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf4() {
        return hashDescTarjetaCabVerifNf4;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf5() {
        return hashDescTarjetaCabVerifNf5;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf6() {
        return hashDescTarjetaCabVerifNf6;
    }

    public static HashMap<String, JSONObject> getHashDescTarjetaCabVerifNf7() {
        return hashDescTarjetaCabVerifNf7;
    }

    public static JSONObject getCuponera() {
        return cuponera;
    }

}
