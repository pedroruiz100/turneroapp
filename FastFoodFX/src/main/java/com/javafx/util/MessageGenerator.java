/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

/**
 *
 * @author ExcelsisWalker
 */
public class MessageGenerator {

    private static String titulo;
    private static String generic1;
    private static String generic2;
    private static boolean proceso;

    public MessageGenerator() {
    }

    public static void generandoMensaje(boolean procesoExitoso, String generic2) {
        geneMens(procesoExitoso, generic2);
    }

    private static void geneMens(boolean procesoExitoso, String generic2) {
        MessageGenerator.proceso = procesoExitoso;
        PATH.MSG_CONT_GEN = generic2;
    }

    public static String getTitulo() {
        return titulo;
    }

    public static String getGeneric1() {
        return generic1;
    }

    public static String getGeneric2() {
        return generic2;
    }

    public static boolean isProceso() {
        return proceso;
    }

}
