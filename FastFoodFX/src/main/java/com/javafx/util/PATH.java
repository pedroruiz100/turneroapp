/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author ExcelsisWalker
 */
public class PATH {

    private String filePath;
    public static String PATH_LOGO = "";
    public static String PATH_LOGO_VENTA = "";
    public static String PATH_ICON = "";
    public static String PATH_SUP = "";
    public static String PATH_CAJ = "";
    public static String PATH_ADV = "";
    public static String PATH_NO_IMG = "";
    public static String PATH_SI;
    public static String PATH_NO;

    //**************************************************************************
    public static final String MSG_ADVERT = "Mensaje de Advertencia";
    public static final String MSG_CONFIR = "Mensaje de Confirmación";
    public static final String MSG_CONT_ADVER = "El proceso NO finalizó adecuadamente.";
    public static final String MSG_CONT_CONFI = "El proceso se realizó con éxito.";

    //mensajes genéricos...
    public static String MSG_CONT_GEN = "";

    //REPORTES******************************************************************
    //*******************************SUBREPORTES********************************
    public static final String JASPER_HEADER_VERTICAL = "/jasper/subreport/SUB_header_vertical.jasper";
    public static final String JASPER_HEADER_HORIZONTAL = "/jasper/subreport/SUB_header_horizontal.jasper";
    public static final String JASPER_FOOTER = "/jasper/subreport/SUB_Header.jasper";
    //*********************************REPORTES*********************************
    public static final String JASPER_REPORT = "/jasper/report/";
    public static final String PDF_JASPER_REPORT = "\\src\\main\\resources\\jasper\\report\\";
    //**************************************************************************
    public static final String JASPER_IMG = "/jasper/img/fondoBlanco.jpg";
    public static final String JASPER_IMG_CP = "/jasper/img/casaParana.jpg";
    //REPORTES******************************************************************

    public PATH() {
        try {
            filePath = new File(".").getCanonicalPath();
            PATH_SI = filePath + "\\img\\green_check.png";
            PATH_NO = filePath + "\\img\\errorR.png";
            PATH_LOGO = filePath + "\\img\\logo.jpg";
            PATH_LOGO_VENTA = filePath + "\\img\\logosiv.png";
            PATH_ICON = filePath + "\\img\\icon.jpg";
            PATH_SUP = filePath + "\\img\\loginSupervisor.png";
            PATH_CAJ = filePath + "\\img\\login.png";
            PATH_ADV = filePath + "\\img\\image_thumb.png";
            PATH_NO_IMG = filePath + "\\img\\vacio.png";
        } catch (IOException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
    }
}
