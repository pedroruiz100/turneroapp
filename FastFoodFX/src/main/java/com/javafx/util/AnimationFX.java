/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import com.jfoenix.transitions.JFXFillTransition;
import javafx.animation.Animation;
import javafx.animation.AnimationTimer;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.RotateTransition;
import javafx.animation.Timeline;
import javafx.scene.Node;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

/**
 *
 * @author ExcelsisWalker
 */
public class AnimationFX {

    static AnimationTimer timer;

    //////TRASLACIÓN NODE
    public static Node translationNode(Node node, int x, int y, double t) {
        if (x != 0) {
            if (x > 0) {
                node.setLayoutX(node.getLayoutX() - Math.abs(x));
            } else {
                node.setLayoutX(node.getLayoutX() + Math.abs(x));
            }
            Timeline tX = new Timeline();
            tX.setCycleCount(1);
            KeyValue kvX = null;
            kvX = new KeyValue(node.translateXProperty(), x);
            KeyFrame kfX = new KeyFrame(Duration.millis(t), kvX);
            tX.getKeyFrames().addAll(kfX);
            tX.play();
        }
        if (y != 0) {
            if (y > 0) {
                node.setLayoutY(node.getLayoutY() - Math.abs(y));
            } else {
                node.setLayoutY(node.getLayoutY() + Math.abs(y));
            }
            Timeline tY = new Timeline();
            tY.setCycleCount(1);
            KeyValue kvY = null;
            kvY = new KeyValue(node.translateYProperty(), y);
            KeyFrame kfY = new KeyFrame(Duration.millis(t), kvY);
            tY.getKeyFrames().addAll(kfY);
            tY.play();
        }
        return node;
    }

    public static Node translationNodeInfiniteX(Node node, int x, double t) {
        if (x != 0) {
            FadeTransition ft = new FadeTransition(Duration.millis(t), node);
            ft.setFromValue(1.0);
            ft.setToValue(0.0);
            ft.setCycleCount(Animation.INDEFINITE);
            ft.play();
            if (x > 0) {
                node.setLayoutX(node.getLayoutX() - Math.abs(x));
            } else {
                node.setLayoutX(node.getLayoutX() + Math.abs(x));
            }
            Timeline tX = new Timeline();
            tX.setCycleCount(Animation.INDEFINITE);
            KeyValue kvX = null;
            kvX = new KeyValue(node.translateXProperty(), x);
            KeyFrame kfX = new KeyFrame(Duration.millis(t), kvX);
            tX.getKeyFrames().addAll(kfX);
            tX.setDelay(Duration.millis(t));
            tX.play();
        }
        return node;
    }
    //////TRASLACIÓN NODE

    //////FADE NODE
    public static Node fadeNode(Node node) {
        node.opacityProperty().set(0);
        timer = new MyTimer(node);
        timer.start();
        return node;
    }
    //////FADE NODE

    //////FADE TOOLTIP
    public static Tooltip fadeTooltip(Tooltip tooltip) {
        tooltip.opacityProperty().set(0);
        timer = new MyTimerToolTip(tooltip);
        timer.start();
        return tooltip;
    }
    //////FADE TOOLTIP

    //////ROTATION NODE
    public static Node rotationNodePlay(Node node, double t, boolean infinite) {
        RotateTransition rt = new RotateTransition(Duration.seconds(t), node);
        rt.setToAngle(360);
        if (infinite) {
            rt.setCycleCount(Animation.INDEFINITE);
//            rt.setAutoReverse(true);
        } else {
            rt.setCycleCount(1);
        }
        rt.play();
        node.opacityProperty().set(0);
        timer = new MyTimer(node);
        timer.start();
        return node;
    }
    //////ROTATION NODE

    public static Node fillTransition(Pane pane) {
        JFXFillTransition JFXFillTransition = new JFXFillTransition();
        JFXFillTransition.setDuration(Duration.INDEFINITE);
        JFXFillTransition.setRegion(pane);
        return pane;
    }

    public static class MyTimer extends AnimationTimer {

        Node node;
        private double opacity;

        public MyTimer(Node node) {
            this.opacity = 0;
            this.node = node;
        }

        @Override
        public void handle(long now) {
            doHandleLogin();

        }

        private Node doHandleLogin() {
            if (opacity >= 1) {
                stop();
            } else {
                opacity += 0.02;
                node.opacityProperty().set(opacity);
            }
            return node;
        }
    }

    public static class MyTimerToolTip extends AnimationTimer {

        Tooltip tooltip;
        private double opacity;

        public MyTimerToolTip(Tooltip tooltip) {
            this.opacity = 0;
            this.tooltip = tooltip;
        }

        @Override
        public void handle(long now) {
            doHandleLogin();

        }

        private Tooltip doHandleLogin() {
            if (opacity >= 1) {
                stop();
            } else {
                opacity += 0.02;
                tooltip.opacityProperty().set(opacity);
            }
            return tooltip;
        }
    }
}
