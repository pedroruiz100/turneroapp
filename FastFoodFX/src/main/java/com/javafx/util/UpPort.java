package com.javafx.util;

import java.io.*;
import java.net.*;

public class UpPort {

    private static final int chatPort = 4345;

//    public static void main(String[] args) {
//        UpPort server = new UpPort();
//        server.go();
//    }
    public static void go() {
        try {
            ServerSocket serverSocket = new ServerSocket(chatPort);

            while (true) {
                Socket socket = serverSocket.accept();

                PrintWriter writer = new PrintWriter(socket.getOutputStream());

                String advice = "up";

                writer.println(advice);
                writer.close();
            }
        } catch (Exception e) {
            System.out.println("Error in establishing connection with the client");
        }
    }

}
