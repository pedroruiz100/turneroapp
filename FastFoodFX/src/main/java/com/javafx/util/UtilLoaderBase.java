/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.io.UnsupportedEncodingException;
import org.apache.commons.codec.binary.Base64;

/**
 *
 * @author ExcelsisWalker
 */
public class UtilLoaderBase {

    // ENCRIPTAR ************************************************ -> -> -> -> ->
    public static String msjIda(String msj){
        return msjDecIda(msj);
    }

    private static String msjDecIda(String msj) {
        byte[] byteArray = Base64.encodeBase64(msj.getBytes());
        String encodedString = new String(byteArray);
        return encodedString;
    }
    // ENCRIPTAR ************************************************ -> -> -> -> ->

    // DESENCRIPTAR ********************************************* -> -> -> -> ->
    public static String msjVuelta(String msj) {
        return msjDecVuelta(msj);
    }

    private static String msjDecVuelta(String msj) {
        byte[] var = Base64.decodeBase64(msj.getBytes());
        String st = "";
        try {
            st = new String(var, "ISO-8859-1");
        } catch (UnsupportedEncodingException ex) {
            Utilidades.log.error("ERROR Exception: ", ex.fillInStackTrace());
        }
        return st;
    }
    // DESENCRIPTAR ********************************************* -> -> -> -> ->  
}
