/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author PC
 */
public class ConexionParana {

    private static Connection conBloque3;
    private static Statement stBloque3;

    //    ---------------------------------SERVIDOR CASAPARANA--------------------------------
    public static boolean conectarBloque3() {
        boolean valor = false;
        if (Utilidades.pingHost("192.168.2.119", 5432, Utilidades.time)) {
            try {
                Class.forName("org.postgresql.Driver");
                String url = "jdbc:postgresql://192.168.2.119:5432/" + "parana";
                conBloque3 = DriverManager.getConnection(url, "postgres", "para00tisol");
                stBloque3 = conBloque3.createStatement();
                conBloque3.setAutoCommit(false);
                stBloque3.setFetchSize(100);
                valor = true;
            } catch (ClassNotFoundException ex) {
                Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
            } catch (SQLException ex) {
                Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
            }
        }
        return valor;
    }

    public static boolean cerrarBloque3() {
        boolean valor = false;
        try {
            stBloque3.close();
            conBloque3.close();
            stBloque3 = null;
            conBloque3 = null;
            valor = true;
        } catch (SQLException ex) {
            Utilidades.log.error("ERROR: ", ex.fillInStackTrace());
        }
        return valor;
    }

    public static Connection getConBloque3() {
        return conBloque3;
    }

    public static Statement getStBloque3() {
        return stBloque3;
    }
//    -----------------------------------------------------------------

}
