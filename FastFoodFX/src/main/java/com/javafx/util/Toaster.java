/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.Calendar;
import javafx.geometry.Pos;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import tray.animations.AnimationType;
import tray.notification.NotificationType;
import tray.notification.TrayNotification;

/**
 *
 * @author ExcelsisWalker
 */
public class Toaster {

    private static TrayNotification tray;
    private static boolean trayActivo;
    private static TrayNotification trayExceso;

    public static void setTray(String msj, String tipoSeleccion, String tipoAnimacion, double tiempo, String tituloFact) {
        tray(msj, tipoSeleccion, tipoAnimacion, tiempo, tituloFact);
    }

    private static void tray(String msj, String tipoSeleccion, String tipoAnimacion, double tiempo, String tituloFact) {
        NotificationType selectedType = null;
        tray = new TrayNotification();
        tray.setMessage(msj);

        //SELECCIÓN ********************************************* -> -> -> -> ->
        if (tipoSeleccion.toUpperCase().contentEquals("I")) {
            tray.setTitle("¡INFORMACIÓN!");
            tray.setNotificationType(selectedType.INFORMATION);
        } else if (tipoSeleccion.toUpperCase().contentEquals("E")) {
            tray.setTitle("¡ERROR!");
            tray.setNotificationType(selectedType.ERROR);
        } else if (tipoSeleccion.toUpperCase().contentEquals("N")) {
            tray.setTitle("¡NOTICIA!");
            tray.setNotificationType(selectedType.NOTICE);
        } else if (tipoSeleccion.toUpperCase().contentEquals("S")) {
            tray.setTitle("¡ÉXITO!");
            tray.setNotificationType(selectedType.SUCCESS);
        } else if (tipoSeleccion.toUpperCase().contentEquals("W")) {
            tray.setTitle("¡ADVERTENCIA!");
            tray.setNotificationType(selectedType.WARNING);
        } else if (tipoSeleccion.toUpperCase().contentEquals("F")) {
            tray.setTitle(tituloFact.toUpperCase());
            tray.setImage(new Image(Toaster.class.getResourceAsStream("/vista/img/logosiv.png")));
        }
        //SELECCIÓN ********************************************* -> -> -> -> ->

        //ANIMACIÓN ********************************************* -> -> -> -> ->
        if (tipoAnimacion.toUpperCase().contentEquals("P")) {
            tray.setAnimationType(AnimationType.POPUP);
        } else if (tipoAnimacion.toUpperCase().contentEquals("S")) {
            tray.setAnimationType(AnimationType.SLIDE);
        } else if (tipoAnimacion.toUpperCase().contentEquals("F")) {
            tray.setAnimationType(AnimationType.FADE);
        }
        //ANIMACIÓN ********************************************* -> -> -> -> ->

        //TIEMPO ************************************************ -> -> -> -> ->
        if (tiempo == 0) {
            trayActivo = true;
            tray.showAndWait();
        } else {
            trayActivo = false;
            tray.showAndDismiss(Duration.millis(tiempo));
        }
        //TIEMPO ************************************************ -> -> -> -> ->
    }

    public void mensajeGenerico(String titulo, String msj, String pathGraphic, int time) {
        if (pathGraphic.isEmpty()) {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER)
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.show();
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER)
                    .graphic(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/paranaPopUp.png"))))
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.show();
        }
    }

    public static void quitandoMsj() {
        dismissTray();
    }

    private static void dismissTray() {
        if (tray != null) {
            if (tray.isTrayShowing()) {
                trayActivo = false;
                tray.dismiss();
                tray = null;
            }
        }
    }

    private static void trayExceso() {
        NotificationType selectedType = null;
        trayExceso = new TrayNotification();
        trayExceso.setMessage("Exceso de efectivo.");
        trayExceso.setTitle("¡ADVERTENCIA!");
        trayExceso.setAnimationType(AnimationType.POPUP);
        trayExceso.setNotificationType(selectedType.WARNING);
        trayExceso.showAndDismiss(Duration.millis(2000));
    }

    public static void setTrayExceso() {
        trayExceso();
    }

    public static boolean isTrayActivo() {
        return trayActivo;
    }

    //CONTROLSFX, CONTROLSFX, CONTROLSFX
    public void mensajeDeNavidadAnhoNuevo() {
        //por algo estaba deprecated el ts con parámetros al constructor desde 1.1...
        Calendar calendar = Calendar.getInstance();
        Calendar tsIni = Calendar.getInstance();
        tsIni.set(calendar.get(Calendar.YEAR), 11, 15, 0, 0, 0);
        Calendar tsFin = Calendar.getInstance();
        tsFin.set(calendar.get(Calendar.YEAR), 11, 33, 0, 0, 0);
        System.out.println("INICIO  " + tsIni.getTime());
        System.out.println("FIN     " + tsFin.getTime());
        System.out.println("NOW     " + calendar.getTime());
        if (calendar.getTime().after(tsIni.getTime()) && calendar.getTime().before(tsFin.getTime())) {
            ImageView imageView;
            String msj = "   CASA PARANÁ TE DESEA\n"
                    + "       ¡FELICES FIESTAS Y \n"
                    + "    PRÓSPERO AÑO NUEVO!";
            if ((int) (Math.random() * 2 + 0) == 0) {
                imageView = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/corona.gif")));
            } else {
                imageView = new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/campana.gif")));
            }
            if ((int) (Math.random() * 2 + 0) == 0) {
                Notifications notificationBuilderNav = Notifications.create()
                        .title(msj)
                        .position(Pos.CENTER)
                        .graphic(imageView)
                        .hideAfter(Duration.seconds(7))
                        .hideCloseButton();
                notificationBuilderNav.show();
            } else {
                Notifications notificationBuilderNav = Notifications.create()
                        .title(msj)
                        .position(Pos.CENTER)
                        .graphic(imageView)
                        .hideAfter(Duration.seconds(7))
                        .darkStyle()
                        .hideCloseButton();
                notificationBuilderNav.show();
            }
        }
    }

    public void mensajeDiario() {
        Notifications notificationBuilder = Notifications.create()
                .title("¡AVISO!")
                .text("¡GRACIAS POR LA PREFERENCIA!\nLO ESPERAMOS PRONTO.")
                .position(Pos.BOTTOM_LEFT)
                .graphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/paranaPopUp.png"))))
                .hideAfter(Duration.seconds(10))
                .darkStyle();
        notificationBuilder.show();
    }

    public void mensajeTarjetaDto(String aviso, int time) {
        Notifications notificationBuilder = Notifications.create()
                .title("¡" + aviso + "!")
                .text("¡PAGO EXCLUSIVO CON TARJETA!")
                .position(Pos.CENTER)
                .graphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/paranaPopUp.png"))))
                .hideAfter(Duration.seconds(time));
        notificationBuilder.show();
    }

    public void mensajeGenericoError(String titulo, String msj, boolean pathGraphic, int time) {
        if (!pathGraphic) {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER)
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.darkStyle().showError();
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER)
                    .graphic(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/errorR.png"))))
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.darkStyle().showError();
        }
    }

    public Notifications mensajeFactCierreDet(String vuelto, String cliente, int time) {
        Notifications notificationBuilder = Notifications.create()
                .title("¡" + vuelto.toUpperCase() + "!")
                .text(cliente.toUpperCase())
                .position(Pos.CENTER)
                .graphic(new ImageView(
                        new Image(getClass().getResourceAsStream("/vista/img/symbol-check-icon.png")))) /*.hideAfter(Duration.seconds(time))*/;
        notificationBuilder.hideAfter(Duration.seconds(time)).show();
        return notificationBuilder;
    }

    public void mensajeGenericoRight(String titulo, String msj, String pathGraphic, int time) {
        if (pathGraphic.isEmpty()) {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER_RIGHT)
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.show();
        } else {
            Notifications notificationBuilder = Notifications.create()
                    .title(titulo)
                    .text(msj)
                    .position(Pos.CENTER_RIGHT)
                    .graphic(new ImageView(
                            new Image(getClass().getResourceAsStream("/vista/img/paranaPopUp.png"))))
                    .hideAfter(Duration.seconds(time));
            notificationBuilder.show();
        }
    }
    //CONTROLSFX, CONTROLSFX, CONTROLSFX
}
