/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javafx.util;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author PC
 */
public class CalculoCupones {
    
    private static Map mapeo = new HashMap();

    public CalculoCupones() {
    }

    public static Map getMapeo() {
        return mapeo;
    }

    public static void setMapeo(Map mapeo) {
        CalculoCupones.mapeo = mapeo;
    }

    public static long generarCuponesPorMontoTotal(long totalVenta, long montoConsiderado) {
        return (int) totalVenta / (int) montoConsiderado;
    }
    
    public static void resetMap(){
        mapeo = new HashMap();
    }

}
