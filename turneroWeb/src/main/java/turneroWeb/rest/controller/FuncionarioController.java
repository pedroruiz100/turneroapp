package turneroWeb.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.FuncionarioDTO;
import turneroWeb.core.service.FuncionarioService;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {

	@Autowired
	private FuncionarioService cliPenSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<FuncionarioDTO> RetiroPedidoDTO = cliPenSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	// @RequestMapping(value = "/listarPendiente", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarPendiente() {
	// try {
	// List<FuncionarioDTO> RetiroPedidoDTO = cliPenSrv
	// .listarPendiente();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<Object>(
	// new ArrayList<FuncionarioDTO>(), HttpStatus.OK);
	// } finally {
	// }
	// }

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		try {
			FuncionarioDTO RetiroPedidoDTO = cliPenSrv.getById(id);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<Object>(new FuncionarioDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody FuncionarioDTO FuncionarioDTO) {
		cliPenSrv.insertar(FuncionarioDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	// @RequestMapping(value = "/insertar", method = RequestMethod.POST,
	// produces = "application/json")
	// public ResponseEntity<Object> crearObtenerObj(
	// @RequestBody FuncionarioDTO FuncionarioDTO) {
	// try {
	// FuncionarioDTO cpDTO = cliPenSrv
	// .insertarObtenerObj(FuncionarioDTO);
	// return new ResponseEntity<Object>(cpDTO, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<Object>(new FuncionarioDTO(),
	// HttpStatus.OK);
	// } finally {
	// }
	// }

	@RequestMapping(value = "/filtro/{nom}/{ape}/{ci}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarObtenerObj(
			@PathVariable("nom") String nom, @PathVariable("ape") String ape,
			@PathVariable("ci") String ci) {
		try {
			List<FuncionarioDTO> val = cliPenSrv.filtrarNomApeCi(nom, ape, ci);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ArrayList<FuncionarioDTO>(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> actualizar(
			@RequestBody FuncionarioDTO FuncionarioDTO) {
		cliPenSrv.actualizar(FuncionarioDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
