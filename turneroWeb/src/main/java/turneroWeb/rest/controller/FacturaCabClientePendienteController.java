package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.ClientePendienteDTO;
import turneroWeb.core.dto.FacturaCabClientePendienteDTO;
import turneroWeb.core.service.FacturaCabClientePendienteService;

@RestController
@RequestMapping("/facturaCabClientePendiente")
public class FacturaCabClientePendienteController {

	@Autowired
	private FacturaCabClientePendienteService facCabCliPenSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<FacturaCabClientePendienteDTO> RetiroPedidoDTO = facCabCliPenSrv
				.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		try {
			FacturaCabClientePendienteDTO RetiroPedidoDTO = facCabCliPenSrv
					.getById(id);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<Object>(new ClientePendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody FacturaCabClientePendienteDTO clientePendienteDTO) {
		facCabCliPenSrv.insertar(clientePendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crearObtenerObj(
			@RequestBody FacturaCabClientePendienteDTO clientePendienteDTO) {
		try {
			FacturaCabClientePendienteDTO cpDTO = facCabCliPenSrv
					.insertarObtenerObj(clientePendienteDTO);
			return new ResponseEntity<Object>(cpDTO, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ClientePendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	// @RequestMapping(value = "/actualizar/{id}/{time}/{nom}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarObtenerObj(
	// @PathVariable("id") long id, @PathVariable("time") Timestamp time,
	// @PathVariable("nom") String nom) {
	// try {
	// boolean val = facCabCliPenSrv.actualizarEstado(id, time, nom);
	// return new ResponseEntity<Object>(val, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<Object>(false, HttpStatus.OK);
	// } finally {
	// }
	// }

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> actualizar(
			@RequestBody FacturaCabClientePendienteDTO clientePendienteDTO) {
		facCabCliPenSrv.actualizar(clientePendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
