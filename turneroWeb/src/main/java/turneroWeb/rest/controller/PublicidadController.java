package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.PublicidadDTO;
import turneroWeb.core.service.PublicidadService;

@RestController
@RequestMapping("/publicidad")
public class PublicidadController {

	@Autowired
	private PublicidadService PublicidadSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPublicidads() {
		// long timepoIni = System.currentTimeMillis();
		List<PublicidadDTO> PublicidadDTO = PublicidadSrv.listar();
		// System.out.println("Tiempo en ms.: "
		// + (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(PublicidadDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/aperturaCaja", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> aperturaCaja() {
		try {
			return new ResponseEntity<Object>(PublicidadSrv.aperturaCaja(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/fullTRUE", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPublicidadTRUE() {
		// long timepoIni = System.currentTimeMillis();
		List<PublicidadDTO> PublicidadDTO = PublicidadSrv
				.listarPublicidadTRUE();
		// System.out.println("Tiempo en ms.: "
		// + (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(PublicidadDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/actualizarPorId/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarPorId(@PathVariable("id") long id) {
		boolean valor = false;
		try {
			PublicidadSrv.actualizarPorId(id);
			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}

	@RequestMapping(value = "/bajaPorId/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> bajaPorId(@PathVariable("id") long id) {
		boolean valor = false;
		try {
			PublicidadSrv.bajaPorId(id);
			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(@RequestBody PublicidadDTO PublicidadDTO) {
		PublicidadSrv.insertar(PublicidadDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(
			@RequestBody PublicidadDTO PublicidadDTO) {
		PublicidadSrv.actualizar(PublicidadDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		PublicidadSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
