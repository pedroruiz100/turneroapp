package turneroWeb.rest.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.ClientePendienteDTO;
import turneroWeb.core.service.ClientePendienteService;

@RestController
@RequestMapping("/clientePendiente")
public class ClientePendienteController {

	@Autowired
	private ClientePendienteService cliPenSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<ClientePendienteDTO> RetiroPedidoDTO = cliPenSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorCi/{ci}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCi(@PathVariable("ci") String ci) {
		try {
			ClientePendienteDTO RetiroPedidoDTO = cliPenSrv.listarPorCi(ci);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ArrayList<ClientePendienteDTO>(), HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/insertarIdTRUE/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> insertarIdTRUE(@PathVariable("id") long id) {
		try {
			ClientePendienteDTO RetiroPedidoDTO = cliPenSrv.insertarIdTRUE(id);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ClientePendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/listarPendiente", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPendiente() {
		try {
			List<ClientePendienteDTO> RetiroPedidoDTO = cliPenSrv
					.listarPendiente();
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(
					new ArrayList<ClientePendienteDTO>(), HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		try {
			ClientePendienteDTO RetiroPedidoDTO = cliPenSrv.getById(id);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<Object>(new ClientePendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody ClientePendienteDTO clientePendienteDTO) {
		cliPenSrv.insertar(clientePendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crearObtenerObj(
			@RequestBody ClientePendienteDTO clientePendienteDTO) {
		try {
			ClientePendienteDTO cpDTO = cliPenSrv
					.insertarObtenerObj(clientePendienteDTO);
			return new ResponseEntity<Object>(cpDTO, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ClientePendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizar/{id}/{time}/{nom}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarObtenerObj(
			@PathVariable("id") long id, @PathVariable("time") Timestamp time,
			@PathVariable("nom") String nom) {
		try {
			boolean val = cliPenSrv.actualizarEstado(id, time, nom);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarEstado/{id}/{estado}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarEstado(@PathVariable("id") long id,
			@PathVariable("estado") boolean estado) {
		try {
			boolean val = cliPenSrv.actualizarEstadoNuevo(id, estado);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> actualizar(
			@RequestBody ClientePendienteDTO clientePendienteDTO) {
		cliPenSrv.actualizar(clientePendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
