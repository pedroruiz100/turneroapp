package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.TalonariosSucursaleDTO;
import turneroWeb.core.service.TalonariosSucursaleService;

@Controller
@RequestMapping("/talonariosSucursale")
public class TalonariosSucursaleController {

	@Autowired
	private TalonariosSucursaleService talonariosSucursaleSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarTalonariosSucursales() {
		List<TalonariosSucursaleDTO> talonariosSucursaleDTO = talonariosSucursaleSrv
				.listar();
		return new ResponseEntity<Object>(talonariosSucursaleDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		TalonariosSucursaleDTO sucursalDTO = talonariosSucursaleSrv.getById(id);
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "listarPorSucursal/{id}/{idTimbrado}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorSucursal(
			@PathVariable("id") long id, @PathVariable("idTimbrado") long idTimbrado) {
		TalonariosSucursaleDTO sucursalDTO = new TalonariosSucursaleDTO();
		try {
			sucursalDTO = talonariosSucursaleSrv.listarPorSucursal(id, idTimbrado);
		} catch (Exception e) {
			sucursalDTO = new TalonariosSucursaleDTO();
		} finally {
		}
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody TalonariosSucursaleDTO talonariosSucursaleDTO) {
		talonariosSucursaleSrv.insertar(talonariosSucursaleDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(
			@RequestBody TalonariosSucursaleDTO talonariosSucursaleDTO) {
		talonariosSucursaleSrv.actualizar(talonariosSucursaleDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		talonariosSucursaleSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
