package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.RetiroPedidoDTO;
import turneroWeb.core.service.RetiroPedidoService;

@RestController
@RequestMapping("/retiroPedido")
public class RetiroPedidoController {

	@Autowired
	private RetiroPedidoService RetiroPedidoSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		long timepoIni = System.currentTimeMillis();
		RetiroPedidoDTO RetiroPedidoDTO = RetiroPedidoSrv.getById(id);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPreparacion", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPreparacion() {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv
				.listarPreparacion();
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/listarFullPreparacion", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFullPreparacion() {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv
				.listarFullPreparacion();
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarListo", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarListo() {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarListo();
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/listarFullListo", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFullListo() {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarFullListo();
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/actualizarPreparado/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarPreparado(
			@PathVariable("id") long id) {
		boolean valor = false;
		try {
			RetiroPedidoSrv.actualizarPreparado(id);
			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/buscarPorNumeroPedido/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> buscarPorNumeroPedido(
			@PathVariable("id") String num) {
		RetiroPedidoDTO valor = new RetiroPedidoDTO();
		try {
			valor = RetiroPedidoSrv.buscarPorNumeroPedido(num);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}

	@RequestMapping(value = "/actualizarListo/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarListo(@PathVariable("id") long id) {
		boolean valor = false;
		try {
			RetiroPedidoSrv.actualizarListo(id);
			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/actualizarEntregado/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarEntregado(@PathVariable("id") long id) {
		boolean valor = false;
		try {
			RetiroPedidoSrv.actualizarEntregado(id);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}

	@RequestMapping(value = "/count", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Long> rowCount() {
		Long RetiroPedidoCount = RetiroPedidoSrv.rowCount();
		return new ResponseEntity<Long>(RetiroPedidoCount, HttpStatus.OK);
	}

	@RequestMapping(value = "/fetch/{limRow}/{offSet}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFetch(
			@PathVariable("limRow") long limRow,
			@PathVariable("offSet") long offSet) {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarFETCH(
				limRow, offSet);
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/no_fetch/{limRow}/{offSet}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarNoFetch(
			@PathVariable("limRow") long limRow,
			@PathVariable("offSet") long offSet) {
		List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarNoFETCH(
				limRow, offSet);
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
		RetiroPedidoSrv.insertar(RetiroPedidoDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(
			@RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
		RetiroPedidoSrv.actualizar(RetiroPedidoDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		RetiroPedidoSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
