package turneroWeb.rest.controller;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.CajaDTO;
import turneroWeb.core.dto.TimbradoDTO;
import turneroWeb.core.service.CajaService;

@Controller
@RequestMapping("/caja")
public class CajaController {

	@Autowired
	private CajaService cajaSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarCajas() {
		List<CajaDTO> cajaDTO = cajaSrv.listar();
		return new ResponseEntity<Object>(cajaDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		CajaDTO sucursalDTO = new CajaDTO();
		try {
			sucursalDTO = cajaSrv.getById(id);
		} catch (Exception e) {
			sucursalDTO = new CajaDTO();
		} finally {
		}
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(@RequestBody CajaDTO cajaDTO) {
		cajaSrv.insertar(cajaDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(@RequestBody CajaDTO cajaDTO) {
		cajaSrv.actualizar(cajaDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		cajaSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/auth/{num}/{clave}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<CajaDTO> listarFetch(@PathVariable("num") Integer u,
			@PathVariable("clave") String c) throws NoSuchAlgorithmException {
		CajaDTO acceso = cajaSrv.buscarCaja(u, c);
		return new ResponseEntity<CajaDTO>(acceso, HttpStatus.OK);
	}

}
