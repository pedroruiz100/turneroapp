package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.ArticuloDTO;
import turneroWeb.core.service.ArticuloService;

@RestController
@RequestMapping("/articulos")
public class ArticuloController {

	@Autowired
	private ArticuloService ArticuloSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<ArticuloDTO> RetiroPedidoDTO = ArticuloSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorId/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		long timepoIni = System.currentTimeMillis();
		ArticuloDTO RetiroPedidoDTO = ArticuloSrv.getById(id);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/actualizarCantidad/{id}/{canti}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarCantidad(
			@PathVariable("id") long id, @PathVariable("canti") long canti) {
		long timepoIni = System.currentTimeMillis();
		boolean val = ArticuloSrv.actualizarCantidad(id, canti);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(val, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorCodigo/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCodigo(@PathVariable("id") String id) {
		long timepoIni = System.currentTimeMillis();
		ArticuloDTO RetiroPedidoDTO = ArticuloSrv.listarPorCodigo(id);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorSeccion/{idSec}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorSecciones(
			@PathVariable("idSec") long id) {
		long timepoIni = System.currentTimeMillis();
		List<ArticuloDTO> RetiroPedidoDTO = ArticuloSrv.listarPorSecciones(id);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorDescripcion/{descri}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorDescripcion(
			@PathVariable("descri") String descri) {
		long timepoIni = System.currentTimeMillis();
		List<ArticuloDTO> RetiroPedidoDTO = ArticuloSrv
				.listarPorDescripcion(descri);
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}
	// @RequestMapping(value = "/listarFullPreparacion", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> listarFullPreparacion() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv
	// .listarFullPreparacion();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/listarListo", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarListo() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarListo();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/listarFullListo", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarFullListo() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO =
	// RetiroPedidoSrv.listarFullListo();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarPreparado/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarPreparado(
	// @PathVariable("id") long id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarPreparado(id);
	// valor = true;
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarListo/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarListo(@PathVariable("id") long
	// id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarListo(id);
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarEntregado/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarEntregado(@PathVariable("id")
	// long id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarEntregado(id);
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }

	// @RequestMapping(method = RequestMethod.POST, produces =
	// "application/json")
	// public ResponseEntity<Object> crear(
	// @RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
	// RetiroPedidoSrv.insertar(RetiroPedidoDTO);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }
	//
	// @RequestMapping(method = RequestMethod.PUT, produces =
	// "application/json")
	// public ResponseEntity<Object> modificar(
	// @RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
	// RetiroPedidoSrv.actualizar(RetiroPedidoDTO);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }

	// @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	// public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
	// RetiroPedidoSrv.eliminar(id);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }

}
