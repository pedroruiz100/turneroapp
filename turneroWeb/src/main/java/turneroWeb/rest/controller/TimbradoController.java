package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.TimbradoDTO;
import turneroWeb.core.service.TimbradoService;

@Controller
@RequestMapping("/timbrado")
public class TimbradoController {

	@Autowired
	private TimbradoService timbradoSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarTimbrados() {
		List<TimbradoDTO> timbradoDTO = timbradoSrv.listar();
		return new ResponseEntity<Object>(timbradoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		TimbradoDTO sucursalDTO = new TimbradoDTO();
		try {
			sucursalDTO = timbradoSrv.getById(id);
		} catch (Exception e) {
			sucursalDTO = new TimbradoDTO();
		} finally {
		}
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(@RequestBody TimbradoDTO timbradoDTO) {
		timbradoSrv.insertar(timbradoDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(@RequestBody TimbradoDTO timbradoDTO) {
		timbradoSrv.actualizar(timbradoDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		timbradoSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
