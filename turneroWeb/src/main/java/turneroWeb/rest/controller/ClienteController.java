package turneroWeb.rest.controller;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.ArticuloDTO;
import turneroWeb.core.dto.ClienteDTO;
import turneroWeb.core.dto.FacturaClienteCabDTO;
import turneroWeb.core.dto.FacturaClienteCabEfectivoDTO;
import turneroWeb.core.dto.FacturaClienteCabHistoricoDTO;
import turneroWeb.core.dto.FacturaClienteCabNotaCreditoDTO;
import turneroWeb.core.dto.FacturaClienteCabTarjetaDTO;
import turneroWeb.core.dto.FacturaClienteDetDTO;
import turneroWeb.core.dto.RetiroPedidoDTO;
import turneroWeb.core.dto.TarjetaDTO;
import turneroWeb.core.service.ArticuloService;
import turneroWeb.core.service.ClientePendienteService;
import turneroWeb.core.service.ClienteService;
import turneroWeb.core.service.FacturaClienteCabEfectivoService;
import turneroWeb.core.service.FacturaClienteCabHistoricoService;
import turneroWeb.core.service.FacturaClienteCabNotaCreditoService;
import turneroWeb.core.service.FacturaClienteCabService;
import turneroWeb.core.service.FacturaClienteCabTarjetaService;
import turneroWeb.core.service.FacturaClienteDetService;
import turneroWeb.core.service.RangoFacturaService;
import turneroWeb.core.service.RetiroPedidoService;
import turneroWeb.core.service.ServPendienteService;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

	@Autowired
	private ClienteService clienteSrv;

	@Autowired
	private ClientePendienteService clientePendienteSrv;

	@Autowired
	private ServPendienteService servPendienteSrv;

	@Autowired
	private ArticuloService artSrv;

	@Autowired
	private RangoFacturaService rangoFacturaSrv;

	@Autowired
	private RetiroPedidoService retiroPedidoSrv;

	@Autowired
	private FacturaClienteCabService facturaClienteCabService;

	@Autowired
	private FacturaClienteCabHistoricoService facturaClienteCabHistoricoService;

	@Autowired
	private FacturaClienteDetService facturaClienteDetService;

	@Autowired
	private FacturaClienteCabEfectivoService facturaClienteCabEfectivoService;

	@Autowired
	private FacturaClienteCabTarjetaService facturaClienteCabTarjetaService;

	@Autowired
	private FacturaClienteCabNotaCreditoService facturaClienteCabNotaCreditoService;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<ClienteDTO> RetiroPedidoDTO = clienteSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/enviarDetalleFactura/{indice}/{idArticulo}/{codigo}/{descri}/{iva}/{cantidad}/{monto}/{total}/{idFactCab}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> enviarDetalleFactura(
			@PathVariable("indice") int indice,
			@PathVariable("idArticulo") long idArticulo,
			@PathVariable("codigo") String codigo,
			@PathVariable("descri") String descri,
			@PathVariable("iva") String iva,
			@PathVariable("cantidad") String cantidad,
			@PathVariable("monto") String monto,
			@PathVariable("total") String total,
			@PathVariable("idFactCab") long idFactCab) {

		ArticuloDTO articuloDTO = artSrv.listarPorCodigoSeccion(codigo);

		FacturaClienteCabDTO fcc = new FacturaClienteCabDTO();
		fcc.setIdFacturaClienteCab(idFactCab);

		ArticuloDTO artDTO = new ArticuloDTO();
		artDTO.setIdArticulo(articuloDTO.getIdArticulo());

		FacturaClienteDetDTO fcdDTO = new FacturaClienteDetDTO();
		fcdDTO.setFacturaClienteCab(fcc);
		fcdDTO.setArticulo(artDTO);
		fcdDTO.setBajada(false);
		fcdDTO.setCantidad(new BigDecimal(cantidad));
		fcdDTO.setCodArticulo(Long.parseLong(codigo));
		fcdDTO.setCodVendedor("0");
		fcdDTO.setDescripcion(descri);
		fcdDTO.setOrden(indice);
		fcdDTO.setPermiteDesc(false);
		if (iva.equalsIgnoreCase("G5")) {
			fcdDTO.setPoriva(5);
		} else if (iva.equalsIgnoreCase("G10")) {
			fcdDTO.setPoriva(10);
		} else {
			fcdDTO.setPoriva(0);
		}
		fcdDTO.setPrecio(Long.parseLong(monto));
		fcdDTO.setSeccion(articuloDTO.getSeccion().getDescripcion());
		fcdDTO.setSeccionSub("NINGUNA");

		facturaClienteDetService.insertar(fcdDTO);

		return new ResponseEntity<Object>(true, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorCodigo/{cod}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCodigo(@PathVariable("cod") int cod) {
		ClienteDTO RetiroPedidoDTO = clienteSrv.listarPorCodigo(cod);
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/imprimirTicket/{cod}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> imprimirTicket(
			@PathVariable("cod") String ticket) {
		// ticket =
		// "<center>LA GALOPA</center><center>CENTRAL - RUC: 80008038-6</center><center>TEL. 0983213492-COMERCIO RAMOS GENERALES</center><center>Antequera c/ Herrera y Azara</center><center>----------------------------------------</center><center>TIMBRADO NRO. 123321456</center><center>INICIO VIGENCIA: 2017-08-01</center><center>FIN VIGENCIA: 2018-08-31</center><center>CAJA INTERNA: 1 FECHA: 28-06-2019 09:58:30</center><center>FACTURA CONTADO: 001-001-1084258</center><center>CAJERO: cajero</center><center>----------------------------------------</center><center>Articulo  Cant.  Precio      Total</center><center>1. 0371967873127 AQUARIUS NARANJA G10 <br>         1  x   7,500 &nbsp;&nbsp;&nbsp;&nbsp;Gs. 7,500</center><center>2. 0461441101261 AQUARIUS PERA G5 <br>         1  x   7,000 &nbsp;&nbsp;&nbsp;&nbsp;Gs. 7,000</center><center>3. 7564622417888 COCA COLA G10 <br>         1  x   5,000 &nbsp;&nbsp;&nbsp;&nbsp;Gs. 5,000</center><center>---DETALLE FISCAL-----------------------</center><center>GRAVADA 10% :  Gs 12,500</center><center>GRAVADA 5%  :  Gs 7,000</center><center>EXENTA      :  Gs 0</center><center>---LIQUIDACION DE IVA-------------------</center><center>10%:  Gs 1,136</center><center>5%:   Gs 333</center><center>----------------------------------------</center><center>RUC: XXX</center><center>CLIENTE: SIN NOMBRE</center><center>----------------------------------------</center><center>ARTICULOS: 3          TOTAL: 19,500</center><center>DESCUENTO:   0        NETO: 19,500</center><center>----------------------------------------</center><center>EFECTIVO: Gs 19,500</center><center style=\"color: red\">VUELTO: Gs 0</center><center>----------------------------------------</center><center>Verifique su compra antes de retirar</center><center>Cambios hasta 5 dias, no se realizan</center><center>cambios en oferta, liquidacion,</center><center>perfumeria, bijouterie, prendas de </center><center>lenceria, trajes de bano,</center><center>y aseo personal</center><center>----------------------------------------</center><center>!!LAS COSAS, MAS LINDAS</center><center>AL MEJOR PRECIO!!</center><center>VISITA NUESTROS LOCALES Y APROVECHA</center><center>!!!!LAS SUPER OFERTAS!!!!</center><center>----------------------------------------</center><center>****NUMERO DE PEDIDO****</center><center>*************** A1174 *************</center>";
		// Base64.Encoder encoder = Base64.getUrlEncoder();
		// ticket = new String(encoder.encodeToString(ticket.getBytes()));
		// System.out.println("TICKET ORIGINAL " + ticket);
		byte[] decodedBytes = Base64.decodeBase64(ticket.getBytes());
		String dStr = new String(decodedBytes);
		try {
			// System.out.println("TICKET 00 " + dStr);
			dStr = dStr.replaceAll("<center>", "");
			dStr = dStr.replaceAll("&&", "/");
			dStr = dStr.replaceAll("</center>", "\n");
			dStr = dStr.replaceAll("<center style='color: red'>", "");
			dStr = dStr.replaceAll("<br>", "\n");
			dStr = dStr.replaceAll("&nbsp;", " ");
			System.out.println("TICKET 01 " + dStr);
			System.out.println("*********************OPA********************");
			dStr = (char) 27 + "d" + (char) 0 + (char) 27 + "a" + (char) 1
					+ (char) 27 + "R" + (char) 7 + (char) 27 + "c0" + (char) 3
					+ (char) 27 + "z" + (char) 1 + (char) 13 + dStr
					+ +(char) 27 + (char) 100 + (char) 13 + (char) 27
					+ (char) 105 + (char) 0;
			System.out.println("TICKET 02 " + dStr);
			Ticket tic = new Ticket();
			tic.bytes = dStr.getBytes();
			tic.print();
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/insertarCabeceraHistorico/{idCab}/{empresa}/{sucursal}/{nroCaja}/{rucEmpresa}/"
			+ "{telefono}/{direccion}/{timbrado}/{inicio}/{fin}/"
			+ "{iva10}/{iva5}/{exenta}/{liq10}/{liq5}/"
			+ "{rucFac}/{nomFac}/{montoTotal}/{sumaEfe}/{sumaCred}/"
			+ "{sumaDeb}/{sumaNota}/{vuelto}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> insertarCabeceraHistorico(
			@PathVariable("idCab") long idCab,
			@PathVariable("empresa") String empresa,
			@PathVariable("sucursal") String sucursal,
			@PathVariable("nroCaja") String nroCaja,
			@PathVariable("rucEmpresa") String rucEmpresa,
			@PathVariable("telefono") String telefono,
			@PathVariable("direccion") String direccion,
			@PathVariable("timbrado") String timbrado,
			@PathVariable("inicio") String inicio,
			@PathVariable("fin") String fin, @PathVariable("iva10") int iva10,
			@PathVariable("iva5") int iva5, @PathVariable("exenta") int exenta,
			@PathVariable("liq10") int liq10, @PathVariable("liq5") int liq5,
			@PathVariable("rucFac") String rucFac,
			@PathVariable("nomFac") String nomFac,
			@PathVariable("montoTotal") int montoTotal,
			@PathVariable("sumaEfe") int sumaEfe,
			@PathVariable("sumaCred") int sumaCred,
			@PathVariable("sumaDeb") int sumaDeb,
			@PathVariable("sumaNota") int sumaNota,
			@PathVariable("vuelto") int vuelto) {

		try {
			FacturaClienteCabDTO fccDTO = new FacturaClienteCabDTO();
			fccDTO.setIdFacturaClienteCab(idCab);

			FacturaClienteCabHistoricoDTO fcchDTO = new FacturaClienteCabHistoricoDTO();
			fcchDTO.setAsoc(0);
			fcchDTO.setCheque(0);
			if (rucFac.equalsIgnoreCase("null")
					|| nomFac.equalsIgnoreCase("null")) {
				fcchDTO.setRucCliente("XXX");
				fcchDTO.setCliente("SIN NOMBRE");
			} else {
				fcchDTO.setRucCliente(rucFac);
				fcchDTO.setCliente(nomFac);
			}

			fcchDTO.setCotiDolar(0l);
			fcchDTO.setCotiPeso(0l);
			fcchDTO.setCotiReal(0l);
			fcchDTO.setDescuento(0);
			fcchDTO.setDireccion(direccion);
			fcchDTO.setDolar(0);
			fcchDTO.setEfectivo(sumaEfe);
			fcchDTO.setEmpresa(empresa);
			fcchDTO.setExenta(exenta);
			fcchDTO.setFacturaClienteCab(fccDTO);
			fcchDTO.setTotal(montoTotal);

			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date fecIni = format.parse(inicio);
			java.util.Date fecFin = format.parse(fin);
			java.sql.Date sqlInicio = new java.sql.Date(fecIni.getTime());
			java.sql.Date sqlFin = new java.sql.Date(fecFin.getTime());

			fcchDTO.setFecVencimiento(sqlFin);
			fcchDTO.setFecInicial(sqlInicio);
			fcchDTO.setGiftcard(0);
			fcchDTO.setGrav10(liq10);
			fcchDTO.setGrav5(liq5);
			fcchDTO.setLiqui10(iva10);
			fcchDTO.setLiqui5(iva5);
			fcchDTO.setNotCre(sumaNota);
			fcchDTO.setNroCaja(Integer.parseInt(nroCaja));
			fcchDTO.setNroTimbrado(timbrado);
			fcchDTO.setPeso(0);
			fcchDTO.setRealb(0);
			fcchDTO.setRedondeo(0);
			fcchDTO.setRetencion(0);
			fcchDTO.setRuc(rucEmpresa);
			fcchDTO.setRucCliente(rucFac);
			fcchDTO.setSucursal(sucursal);
			fcchDTO.setTarjCred(sumaCred);
			fcchDTO.setTarjDeb(sumaDeb);
			fcchDTO.setTelef(telefono);
			fcchDTO.setVale(0);
			fcchDTO.setVuelto(vuelto);

			facturaClienteCabHistoricoService.insertarActual(fcchDTO, idCab);
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarClientePendiente/{idCP}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarClientePendiente(
			@PathVariable("idCP") long idCP) {
		try {
			clientePendienteSrv.actualizarEstado(idCP);

			// for (ServPendienteDTO spDTO : servPendienteSrv
			// .buscandoServiciosAsignados(idCP, 0)) {
			servPendienteSrv.eliminarPorIdClientePendiente(idCP);
			// }

			return new ResponseEntity<Object>(true, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarMontoCabecera/{idFCC}/{monto}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCodigo(
			@PathVariable("idFCC") long idFCC, @PathVariable("monto") int monto) {
		try {
			// EstadoFacturaDTO efDTO = new EstadoFacturaDTO();
			// efDTO.setIdEstadoFactura(1l);
			//
			// FacturaClienteCabDTO fccDTO = facturaClienteCabService
			// .getById(idFCC);
			// fccDTO.setMontoFactura(monto);
			// fccDTO.setEstadoFactura(efDTO);
			facturaClienteCabService.actualizarNativo(idFCC, monto);
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	// @RequestMapping(value = "/enviarPrueba", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> enviarPrueba(
	// @RequestBody FacturaClienteCabDTO facturaClienteCabDTO) {
	// JSONParser parser = new JSONParser();
	// try {
	// // JSONObject json = (JSONObject) parser.parse(datos);
	// // JSONObject jsonFactura = (JSONObject)
	// // json.get("facturaClienteCab");
	// System.out.println(facturaClienteCabDTO.getFechaEmision()
	// .toString()
	// + " - "
	// + facturaClienteCabDTO.getNroFactura()
	// + " - " + facturaClienteCabDTO.getMontoFactura());
	// } catch (Exception e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	// return new ResponseEntity<Object>(true, HttpStatus.OK);
	// }

	@RequestMapping(value = "/enviarPrueba/{caja}/{nroPedido}/{observacion}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(@PathVariable("caja") long idCaja,
			@PathVariable("nroPedido") String nroPedido,
			@PathVariable("observacion") String observacion,
			@RequestBody FacturaClienteCabDTO facturaClienteCabDTO) {
		long idRangoFactura = rangoFacturaSrv
				.actualizarObtenerRangoActual(idCaja);

		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		facturaClienteCabDTO.setIdFacturaClienteCab(idRangoFactura);
		facturaClienteCabDTO.setFechaEmision(timestamp);
		facturaClienteCabDTO.setFechaMod(timestamp);
		if (observacion.equalsIgnoreCase("null")) {
			observacion = "";
		}
		facturaClienteCabDTO.setObservacion(observacion);
		String val = facturaClienteCabDTO.getNroFactura().replaceAll("-", "");
		facturaClienteCabDTO.setNroFactura(Long.parseLong(val) + "");

		RetiroPedidoDTO rpDTO = new RetiroPedidoDTO();

		rpDTO.setNumero(nroPedido);

		ClienteDTO cliDTO = clienteSrv.getById(facturaClienteCabDTO
				.getCliente().getIdCliente());
		if (cliDTO.getApellido().equals("") || cliDTO.getApellido() == null) {
			rpDTO.setCliente(cliDTO.getNombre());
		} else {
			rpDTO.setCliente(cliDTO.getNombre() + " " + cliDTO.getApellido());
		}
		rpDTO.setEntregado(false);
		rpDTO.setPreparacion(true);
		rpDTO.setListo(false);

		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		rpDTO.setHora(dateFormat.format(date));

		facturaClienteCabDTO = facturaClienteCabService
				.insertarObtenerObjeto(facturaClienteCabDTO);

		FacturaClienteCabDTO fccDTO = new FacturaClienteCabDTO();
		fccDTO.setIdFacturaClienteCab(idRangoFactura);

		rpDTO.setFacturaClienteCab(fccDTO);
		retiroPedidoSrv.insertar(rpDTO);

		// System.out.println(facturaClienteCabDTO.getCliente().getIdCliente()
		// + " - " + facturaClienteCabDTO.getNroFactura() + " - "
		// + facturaClienteCabDTO.getMontoFactura());
		return new ResponseEntity<Object>(facturaClienteCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/enviarformaPago/{idFac}/{descri}/{monto}/{cod}/{num}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> enviarformaPago(
			@PathVariable("idFac") long idFac,
			@PathVariable("descri") String descri,
			@PathVariable("monto") long monto, @PathVariable("cod") String cod,
			@PathVariable("num") String num) {
		boolean val = false;
		if (descri.equalsIgnoreCase("EFECTIVO")) {
			FacturaClienteCabDTO fcc = new FacturaClienteCabDTO();
			fcc.setIdFacturaClienteCab(idFac);

			FacturaClienteCabEfectivoDTO fccEfe = new FacturaClienteCabEfectivoDTO();
			fccEfe.setFacturaClienteCab(fcc);
			fccEfe.setMontoEfectivo(Integer.parseInt(monto + ""));

			facturaClienteCabEfectivoService.insertar(fccEfe);
		} else if (descri.equalsIgnoreCase("DEBITO")) {
			FacturaClienteCabDTO fcc = new FacturaClienteCabDTO();
			fcc.setIdFacturaClienteCab(idFac);

			TarjetaDTO tarj = new TarjetaDTO();
			tarj.setIdTarjeta(1l);

			FacturaClienteCabTarjetaDTO fccEfe = new FacturaClienteCabTarjetaDTO();
			fccEfe.setFacturaClienteCab(fcc);
			fccEfe.setCodAutorizacion(cod);
			fccEfe.setDescripcionTarj("DEBITO");
			fccEfe.setTarjeta(tarj);
			fccEfe.setMonto(Integer.parseInt(monto + ""));

			facturaClienteCabTarjetaService.insertar(fccEfe);
		} else if (descri.equalsIgnoreCase("CREDITO")) {
			FacturaClienteCabDTO fcc = new FacturaClienteCabDTO();
			fcc.setIdFacturaClienteCab(idFac);

			TarjetaDTO tarj = new TarjetaDTO();
			tarj.setIdTarjeta(2l);

			FacturaClienteCabTarjetaDTO fccEfe = new FacturaClienteCabTarjetaDTO();
			fccEfe.setFacturaClienteCab(fcc);
			fccEfe.setCodAutorizacion(cod);
			fccEfe.setDescripcionTarj("CREDITO");
			fccEfe.setTarjeta(tarj);
			fccEfe.setMonto(Integer.parseInt(monto + ""));

			facturaClienteCabTarjetaService.insertar(fccEfe);
		} else if (descri.equalsIgnoreCase("NOTA DE CREDITO")) {
			FacturaClienteCabDTO fcc = new FacturaClienteCabDTO();
			fcc.setIdFacturaClienteCab(idFac);

			FacturaClienteCabNotaCreditoDTO fccEfe = new FacturaClienteCabNotaCreditoDTO();
			fccEfe.setFacturaClienteCab(fcc);
			fccEfe.setNroNota(num);
			fccEfe.setMonto(Integer.parseInt(monto + ""));

			facturaClienteCabNotaCreditoService.insertar(fccEfe);
		}
		return new ResponseEntity<Object>(val, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorCI/{cod}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCI(@PathVariable("cod") String cod) {
		ClienteDTO RetiroPedidoDTO = clienteSrv.listarPorCI(cod);
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/listarPorRuc/{cod}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorRuc(@PathVariable("cod") String cod) {
		ClienteDTO RetiroPedidoDTO = clienteSrv.listarPorRuc(cod);
		if (RetiroPedidoDTO == null) {
			RetiroPedidoDTO = new ClienteDTO();
		}
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertarClie/{ci}/{nombre}/{apellido}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCodigo(
			@PathVariable("ci") String ci,
			@PathVariable("nombre") String nombre,
			@PathVariable("apellido") String apellido) {
		ClienteDTO cliDTO = new ClienteDTO();
		try {
			cliDTO = clienteSrv.insetarDatos(ci, nombre, apellido);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
		} finally {
		}
		return new ResponseEntity<Object>(cliDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/insertarClieData/{ci}/{nombre}/{apellido}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorCodigoData(
			@PathVariable("ci") String ci,
			@PathVariable("nombre") String nombre,
			@PathVariable("apellido") String apellido) {
		ClienteDTO cliDTO = new ClienteDTO();
		try {
			cliDTO = clienteSrv.insetarDatosData(ci, nombre, apellido);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
		} finally {
		}
		return new ResponseEntity<Object>(cliDTO, HttpStatus.OK);
	}

	// @RequestMapping(value = "/listarFullPreparacion", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> listarFullPreparacion() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv
	// .listarFullPreparacion();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/listarListo", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarListo() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO = RetiroPedidoSrv.listarListo();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/listarFullListo", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarFullListo() {
	// List<RetiroPedidoDTO> RetiroPedidoDTO =
	// RetiroPedidoSrv.listarFullListo();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarPreparado/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarPreparado(
	// @PathVariable("id") long id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarPreparado(id);
	// valor = true;
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarListo/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarListo(@PathVariable("id") long
	// id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarListo(id);
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }
	//
	// @RequestMapping(value = "/actualizarEntregado/{id}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarEntregado(@PathVariable("id")
	// long id) {
	// boolean valor = false;
	// try {
	// RetiroPedidoSrv.actualizarEntregado(id);
	// } catch (Exception e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// System.out.println("-->> " + e.fillInStackTrace());
	// valor = false;
	// }
	// return new ResponseEntity<Object>(valor, HttpStatus.OK);
	// }

	// @RequestMapping(method = RequestMethod.POST, produces =
	// "application/json")
	// public ResponseEntity<Object> crear(
	// @RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
	// RetiroPedidoSrv.insertar(RetiroPedidoDTO);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }
	//
	// @RequestMapping(method = RequestMethod.PUT, produces =
	// "application/json")
	// public ResponseEntity<Object> modificar(
	// @RequestBody RetiroPedidoDTO RetiroPedidoDTO) {
	// RetiroPedidoSrv.actualizar(RetiroPedidoDTO);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }

	// @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	// public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
	// RetiroPedidoSrv.eliminar(id);
	// return new ResponseEntity<Object>(HttpStatus.OK);
	// }

}
