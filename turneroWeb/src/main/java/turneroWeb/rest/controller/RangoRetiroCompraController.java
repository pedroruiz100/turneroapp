package turneroWeb.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.RangoRetiroCompraDTO;
import turneroWeb.core.service.RangoRetiroCompraService;

@RestController
@RequestMapping("/rangoRetiroCompra")
public class RangoRetiroCompraController {

	@Autowired
	private RangoRetiroCompraService rangoRetiroCompraService;

	@RequestMapping(value = "/actualizarObtenerRango/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarObtenerRango(
			@PathVariable("id") long id) {
		long valor = -1L;
		try {
			valor = rangoRetiroCompraService.actualizarObtenerRango(id);
//			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			// valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> recuperarRangoCompra(
			@PathVariable("id") long id) {
		RangoRetiroCompraDTO valor = new RangoRetiroCompraDTO();
		try {
			valor = rangoRetiroCompraService.getById(id);
//			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			// valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/actualizarObtenerRangoObjectActual/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarObtenerRangoObjectActual(
			@PathVariable("id") long id) {
		RangoRetiroCompraDTO valor = new RangoRetiroCompraDTO();
		try {
			valor = rangoRetiroCompraService.actualizarObtenerRangoObjectActual(id);
//			valor = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
			// valor = false;
		}
		return new ResponseEntity<Object>(valor, HttpStatus.OK);
	}
	
	

//	@RequestMapping(value = "/bajaPorId/{id}", method = RequestMethod.GET, produces = "application/json")
//	public ResponseEntity<Object> bajaPorId(@PathVariable("id") long id) {
//		boolean valor = false;
//		try {
//			PublicidadSrv.bajaPorId(id);
//			valor = true;
//		} catch (Exception e) {
//			System.out.println("-->> " + e.getLocalizedMessage());
//			System.out.println("-->> " + e.fillInStackTrace());
//			valor = false;
//		}
//		return new ResponseEntity<Object>(valor, HttpStatus.OK);
//	}
//
//	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
//	public ResponseEntity<Object> crear(@RequestBody PublicidadDTO PublicidadDTO) {
//		PublicidadSrv.insertar(PublicidadDTO);
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
//
//	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
//	public ResponseEntity<Object> modificar(
//			@RequestBody PublicidadDTO PublicidadDTO) {
//		PublicidadSrv.actualizar(PublicidadDTO);
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}
//
//	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
//	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
//		PublicidadSrv.eliminar(id);
//		return new ResponseEntity<Object>(HttpStatus.OK);
//	}

}
