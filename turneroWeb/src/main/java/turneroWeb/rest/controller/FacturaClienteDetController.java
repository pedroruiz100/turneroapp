package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.FacturaClienteDetDTO;
import turneroWeb.core.service.FacturaClienteDetService;
@Controller
@RequestMapping("/facturaClienteDet")
public class FacturaClienteDetController {
	@Autowired
	private FacturaClienteDetService facturaClienteDetSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFacturaClienteDets() {
		List<FacturaClienteDetDTO> facturaClienteDetDTO = facturaClienteDetSrv
				.listar();
		return new ResponseEntity<Object>(facturaClienteDetDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "list/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFacturaClienteDetsPorId(@PathVariable("id") long id) {
		FacturaClienteDetDTO facturaClienteDetDTO = facturaClienteDetSrv
				.listarPorId(id);
		return new ResponseEntity<Object>(facturaClienteDetDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "listFacturaCab/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listFacturaCab(@PathVariable("id") long id) {
		List<FacturaClienteDetDTO> facturaClienteDetDTO = facturaClienteDetSrv
				.listFacturaCab(id);
		return new ResponseEntity<Object>(facturaClienteDetDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody FacturaClienteDetDTO facturaClienteDetDTO) {
		facturaClienteDetSrv.insertar(facturaClienteDetDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(
			@RequestBody FacturaClienteDetDTO facturaClienteDetDTO) {
		facturaClienteDetSrv.actualizar(facturaClienteDetDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		facturaClienteDetSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	// Prueba de inserci�n masiva
	@RequestMapping(value = "insercionMasiva", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> insercionMasiva(
			@RequestBody List<FacturaClienteDetDTO> facDTO) {
		boolean estado = false;
		try {
			facturaClienteDetSrv.insercionMasiva(facDTO);
			estado = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		}
		return new ResponseEntity<Object>(estado, HttpStatus.OK);
	}
	
	@RequestMapping(value = "filtroFechaDescuento/{nroFact}/{fechaDesde}/{fechaHasta}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> filtroFechaDescuentoFact(
			@PathVariable("fechaDesde") String fechaDesde,
			@PathVariable("fechaHasta") String fechaHasta,
			@PathVariable("nroCaja") String nroCaja,
			@PathVariable("nroFact") String nroFact){
		List<FacturaClienteDetDTO> listFactCliCabDTO = facturaClienteDetSrv
				.filtroFechaDescuentoFact(fechaDesde, fechaHasta, nroCaja, nroFact);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}
}
