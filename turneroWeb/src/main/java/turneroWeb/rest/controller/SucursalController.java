package turneroWeb.rest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.SucursalDTO;
import turneroWeb.core.service.SucursalService;

@Controller
@RequestMapping("/sucursal")
public class SucursalController {

	@Autowired
	private SucursalService sucursalSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarSucursals() {
		List<SucursalDTO> sucursalDTO = sucursalSrv.listar();
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		SucursalDTO sucursalDTO = sucursalSrv.getById(id);
		return new ResponseEntity<Object>(sucursalDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(@RequestBody SucursalDTO sucursalDTO) {
		sucursalSrv.insertar(sucursalDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(@RequestBody SucursalDTO sucursalDTO) {
		sucursalSrv.actualizar(sucursalDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		sucursalSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}
}
