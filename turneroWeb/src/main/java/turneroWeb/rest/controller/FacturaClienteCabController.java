package turneroWeb.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import turneroWeb.core.dto.FacturaClienteCabDTO;
import turneroWeb.core.service.FacturaClienteCabService;

@Controller
@RequestMapping("/facturaClienteCab")
public class FacturaClienteCabController {
	@Autowired
	private FacturaClienteCabService facturaClienteCabSrv;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarFacturaClienteCabs() {
		List<FacturaClienteCabDTO> facturaClienteCabDTO = facturaClienteCabSrv
				.listar();
		return new ResponseEntity<Object>(facturaClienteCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		FacturaClienteCabDTO facturaClienteCabDTO = facturaClienteCabSrv
				.getById(id);
		return new ResponseEntity<Object>(facturaClienteCabDTO, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> insertarObtenerObjeto(
			@RequestBody FacturaClienteCabDTO facturaClienteCabDTO) {
		FacturaClienteCabDTO fac = new FacturaClienteCabDTO();
		try {
			fac = facturaClienteCabSrv
					.insertarObtenerObjeto(facturaClienteCabDTO);
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return new ResponseEntity<Object>(fac, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> modificar(
			@RequestBody FacturaClienteCabDTO facturaClienteCabDTO) {
		FacturaClienteCabDTO facDTO = new FacturaClienteCabDTO();
		try {
			facDTO = facturaClienteCabSrv
					.actualizarObtenerObjeto(facturaClienteCabDTO);
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return new ResponseEntity<Object>(facDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<Object> eliminar(@PathVariable("id") int id) {
		facturaClienteCabSrv.eliminar(id);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "cancelar/{id}/{usuMod}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> cancelarFac(@PathVariable("id") long id,
			@PathVariable("usuMod") String usuario) {
		boolean estado = false;
		try {
			facturaClienteCabSrv.cancelarFactura(id, usuario);
			estado = true;
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return new ResponseEntity<Object>(estado, HttpStatus.OK);
	}

	@RequestMapping(value = "recuperarLaguna/{fechaInicio}/{fechaFin}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> cancelarFac(
			@PathVariable("fechaInicio") String fechaInicio,
			@PathVariable("fechaFin") String fechaFin,
			@PathVariable("nroCaja") String nroCaja) {
		List<JSONObject> listFacturaClienteCabDTO = new ArrayList<JSONObject>();
		try {
			listFacturaClienteCabDTO = facturaClienteCabSrv.recuperarLaguna(
					fechaInicio, fechaFin, nroCaja);
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return new ResponseEntity<Object>(listFacturaClienteCabDTO,
				HttpStatus.OK);
	}

	@RequestMapping(value = "consultar/{nroFactura}/{tipoComprobante}/{fechaEmision}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> consultaHistorico(
			@PathVariable("nroFactura") String nroFactura,
			@PathVariable("tipoComprobante") String tipoComprobante,
			@PathVariable("fechaEmision") String fechaEmision,
			@PathVariable("nroFactura") String nroCaja,
			@PathVariable("timbrado") String timbrado) {
		FacturaClienteCabDTO facturaClienteCabDTO = facturaClienteCabSrv
				.consultaHistorico(nroFactura, tipoComprobante, fechaEmision,
						nroFactura, timbrado);
		return new ResponseEntity<Object>(facturaClienteCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "filtroFecha/{fechaDesde}/{fechaHasta}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> consultaHistorico(
			@PathVariable("fechaDesde") String fechaDesde,
			@PathVariable("fechaHasta") String fechaHasta,
			@PathVariable("nroCaja") String nroCaja) {
		List<FacturaClienteCabDTO> listFactCliCabDTO = facturaClienteCabSrv
				.filtroFecha(fechaDesde, fechaHasta, nroCaja);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "filtroFechaDescuento/{fechaDesde}/{fechaHasta}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> filtroFechaDescuento(
			@PathVariable("fechaDesde") String fechaDesde,
			@PathVariable("fechaHasta") String fechaHasta,
			@PathVariable("nroCaja") String nroCaja) {
		List<FacturaClienteCabDTO> listFactCliCabDTO = facturaClienteCabSrv
				.filtroFechaDescuento(fechaDesde, fechaHasta, nroCaja);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "filtroFechaDesc/{fechaDesde}/{fechaHasta}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> filtroFechaDesc(
			@PathVariable("fechaDesde") String fechaDesde,
			@PathVariable("fechaHasta") String fechaHasta,
			@PathVariable("nroCaja") String nroCaja) {
		List<FacturaClienteCabDTO> listFactCliCabDTO = facturaClienteCabSrv
				.filtroFechaDesc(fechaDesde, fechaHasta, nroCaja);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "filtroFechaDescuento/{nroFact}/{fechaDesde}/{fechaHasta}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> filtroFechaDescuentoFact(
			@PathVariable("fechaDesde") String fechaDesde,
			@PathVariable("fechaHasta") String fechaHasta,
			@PathVariable("nroCaja") String nroCaja,
			@PathVariable("nroFact") String nroFact) {
		List<FacturaClienteCabDTO> listFactCliCabDTO = facturaClienteCabSrv
				.filtroFechaDescuentoFact(fechaDesde, fechaHasta, nroCaja,
						nroFact);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "filtroNroFactura/{nroFactura}/{nroCaja}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> filtroFechaDescuento(
			@PathVariable("nroFactura") String nroFactura,
			@PathVariable("nroCaja") String nroCaja) {
		List<FacturaClienteCabDTO> listFactCliCabDTO = facturaClienteCabSrv
				.filtroNroFact(nroFactura, nroCaja);
		return new ResponseEntity<Object>(listFactCliCabDTO, HttpStatus.OK);
	}

}
