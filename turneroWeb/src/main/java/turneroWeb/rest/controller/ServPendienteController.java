package turneroWeb.rest.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import turneroWeb.core.dto.FuncionarioDTO;
import turneroWeb.core.dto.ServPendienteDTO;
import turneroWeb.core.service.ServPendienteService;

@RestController
@RequestMapping("/servPendiente")
public class ServPendienteController {

	@Autowired
	private ServPendienteService srvPenSrv;

	@RequestMapping(value = "/full", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarRetiroPedidos() {
		long timepoIni = System.currentTimeMillis();
		List<ServPendienteDTO> RetiroPedidoDTO = srvPenSrv.listar();
		System.out.println("Tiempo en ms.: "
				+ (System.currentTimeMillis() - timepoIni));
		return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	}

	@RequestMapping(value = "/buscandoServiciosComision/{id}/{inicio}/{fin}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> buscandoServiciosComision(
			@PathVariable("id") long id, @PathVariable("inicio") String inicio,
			@PathVariable("fin") String fin) {
		try {
			List<ServPendienteDTO> val = srvPenSrv.buscandoServiciosComision(
					id, inicio, fin);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ArrayList<FuncionarioDTO>(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarPorLista/{lista}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> buscandoServiciosComision(
			@RequestBody List<Object> lista) {
		try {
			boolean val = srvPenSrv.actualizarPorLista(lista);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarCantidad/{id}/{cant}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarCantidad(
			@PathVariable("id") long id, @PathVariable("cant") int cant) {
		try {
			boolean val = srvPenSrv.actualizarCantidad(id, cant);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/actualizarByEstado", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> actualizarByEstado(
			@RequestBody ServPendienteDTO ServPendienteDTO) {
		try {
			boolean val = srvPenSrv.actualizarByEstado(ServPendienteDTO);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/buscandoServicios/{idCP}/{idFun}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> buscandoServicios(
			@PathVariable("idCP") long idCP, @PathVariable("idFun") long idFun) {
		try {
			List<ServPendienteDTO> val = srvPenSrv.buscandoServiciosAsignados(
					idCP, idFun);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ArrayList<FuncionarioDTO>(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/eliminarPorId/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> eliminarPorId(@PathVariable("id") long id) {
		try {
			srvPenSrv.eliminar(id);
			return new ResponseEntity<Object>(true, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(value = "/buscandoServiciosComisionOrderArt/{id}/{inicio}/{fin}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> actualizarObtenerObj(
			@PathVariable("id") long id, @PathVariable("inicio") String inicio,
			@PathVariable("fin") String fin) {
		try {
			List<ServPendienteDTO> val = srvPenSrv
					.buscandoServiciosComisionOrderArt(id, inicio, fin);
			return new ResponseEntity<Object>(val, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(new ArrayList<FuncionarioDTO>(),
					HttpStatus.OK);
		} finally {
		}
	}

	// @RequestMapping(value = "/listarPendiente", method = RequestMethod.GET,
	// produces = "application/json")
	// public ResponseEntity<Object> listarPendiente() {
	// try {
	// List<ServPendienteDTO> RetiroPedidoDTO = srvPenSrv
	// .listarPendiente();
	// return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<Object>(
	// new ArrayList<ServPendienteDTO>(), HttpStatus.OK);
	// } finally {
	// }
	// }

	@RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<Object> listarPorId(@PathVariable("id") long id) {
		try {
			ServPendienteDTO RetiroPedidoDTO = srvPenSrv.getById(id);
			return new ResponseEntity<Object>(RetiroPedidoDTO, HttpStatus.OK);
		} catch (Exception e) {

			return new ResponseEntity<Object>(new ServPendienteDTO(),
					HttpStatus.OK);
		} finally {
		}
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crear(
			@RequestBody ServPendienteDTO ServPendienteDTO) {
		srvPenSrv.insertar(ServPendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@RequestMapping(value = "/insertar", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<Object> crearObtenerObj(
			@RequestBody ServPendienteDTO ServPendienteDTO) {
		try {
			ServPendienteDTO cpDTO = srvPenSrv
					.insertarObtenerObj(ServPendienteDTO);
			if (cpDTO.getIdServPendiente() > 0) {
				return new ResponseEntity<Object>(true, HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(false, HttpStatus.OK);
			}

		} catch (Exception e) {
			return new ResponseEntity<Object>(false, HttpStatus.OK);
		} finally {
		}
	}

	// @RequestMapping(value = "/actualizar/{id}/{time}/{nom}", method =
	// RequestMethod.GET, produces = "application/json")
	// public ResponseEntity<Object> actualizarObtenerObj(
	// @PathVariable("id") long id, @PathVariable("time") Timestamp time,
	// @PathVariable("nom") String nom) {
	// try {
	// boolean val = srvPenSrv.actualizarEstado(id, time, nom);
	// return new ResponseEntity<Object>(val, HttpStatus.OK);
	// } catch (Exception e) {
	// return new ResponseEntity<Object>(false, HttpStatus.OK);
	// } finally {
	// }
	// }

	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<Object> actualizar(
			@RequestBody ServPendienteDTO ServPendienteDTO) {
		srvPenSrv.actualizar(ServPendienteDTO);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
