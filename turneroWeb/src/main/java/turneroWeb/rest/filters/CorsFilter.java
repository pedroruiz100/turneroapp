package turneroWeb.rest.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class CorsFilter
 */
@WebFilter(filterName="/CorsFilter", urlPatterns="/*")
public class CorsFilter implements Filter {

    /**
     * Default constructor. 
     */
    public CorsFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse res = (HttpServletResponse)response;
//		res.setHeader("Access-Control-Allow-Origin", "http://localhost:8017");
		res.setHeader("Access-Control-Allow-Headers", "Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With, Accept");
		res.setHeader("Access-Control-Allow-Origin", obtenerOrigenesHabilitados());
		res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
//		res.setHeader("Access-Control-Allow-Credentials", "true");
		chain.doFilter(request, response);
	}

	private String obtenerOrigenesHabilitados() {
//		return "http://localhost:8888, http://localhost:8017, http://192.168.2.94:8888";
//		return "http://192.168.2.28:8888";
		return "*";
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
