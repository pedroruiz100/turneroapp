package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.ServPendienteDTO;

public interface ServPendienteService extends
		CRUDGenericService<ServPendienteDTO> {

	ServPendienteDTO insertarObtenerObj(
			ServPendienteDTO fromServPendienteAsociado);

	List<ServPendienteDTO> buscandoServiciosAsignados(long idCP, long idFu);

	List<ServPendienteDTO> buscandoServiciosComision(long idFu,
			String fechaInicio, String fechaFin);

	List<ServPendienteDTO> buscandoServiciosComisionOrderArt(long idFu,
			String fechaInicio, String fechaFin);

	void eliminarPorId(long id);

	boolean actualizarPorLista(List<Object> lista);

	boolean actualizarByEstado(ServPendienteDTO servPendienteDTO);

	boolean actualizarCantidad(long id, int cant);

	void eliminarPorIdClientePendiente(long idCP);

}
