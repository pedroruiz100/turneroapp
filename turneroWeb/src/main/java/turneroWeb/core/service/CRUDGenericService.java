/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.util.List;

/**
 *
 * @author ExcelsisWalker
 */
public interface CRUDGenericService <T> {
    
    public abstract List<T> listar();

    public abstract T getById(long id);

    public abstract void insertar(T obj);

    public abstract void actualizar(T obj);

    public abstract void eliminar(long id);
    
}
