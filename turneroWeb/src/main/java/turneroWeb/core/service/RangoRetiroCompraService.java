package turneroWeb.core.service;

import turneroWeb.core.dto.RangoRetiroCompraDTO;

public interface RangoRetiroCompraService {

	long actualizarObtenerRango(long idRango);

	public RangoRetiroCompraDTO actualizarObtenerRangoObjectActual(long idRango1);

	RangoRetiroCompraDTO getById(long id);

}
