package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dao.CRUDGenericDAO;
import turneroWeb.core.dto.FacturaClienteCabHistoricoDTO;

public interface FacturaClienteCabHistoricoService extends
CRUDGenericDAO<FacturaClienteCabHistoricoDTO> {

public List<FacturaClienteCabHistoricoDTO> listarPorFactura(
	Long idFacturaClienteCab);

public FacturaClienteCabHistoricoDTO consultar(String nroFactFiltro,
	String tipoFactFiltro, String fechaFactFiltro,
	String nroCajaFiltro, String timbradoFiltro);

public List<Integer> listarCajaDistinct();

public List<String> listarTimbDistinct();

public void insertarActual(FacturaClienteCabHistoricoDTO fcchDTO, long idCab);

}
