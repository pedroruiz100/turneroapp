package turneroWeb.core.service;

import turneroWeb.core.dto.RangoFacturaDTO;

public interface RangoFacturaService {

	boolean insertarObtenerEstado(RangoFacturaDTO rango);

	boolean actualizarObtenerEstado(long idRango);

	long actualizarObtenerRangoActual(long idRango);
	
}
