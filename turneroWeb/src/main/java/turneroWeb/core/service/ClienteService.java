/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.sql.Timestamp;
import java.util.List;

import turneroWeb.core.dto.ClienteDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface ClienteService extends
		CRUDGenericService<ClienteDTO> {

	List<ClienteDTO> listarPorNomRuc(String nombre, String apellido, String ruc);

    void actualizarNomApeRuc(String nombre, String apellido, String ruc, long id, String usuMod, Timestamp fechaMod);

    ClienteDTO listarPorRuc(String ruc);

    ClienteDTO listarPorCodigo(int codCliente);

    public ClienteDTO getByCod(int codCli);

    public void actualizarNomApeRuc(String toString, String toString0, String toString1, String toString2, String toString3, long parseLong, String nombreCaj, Timestamp timestamp);

	ClienteDTO insetarDatos(String ci, String nombre, String apellido);

	ClienteDTO listarPorCI(String cod);

	ClienteDTO insetarDatosData(String ci, String nombre, String apellido);
}
