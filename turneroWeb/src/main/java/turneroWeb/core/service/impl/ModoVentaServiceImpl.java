package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.ModoVentaDAO;
import turneroWeb.core.domain.ModoVenta;
import turneroWeb.core.dto.ModoVentaDTO;
import turneroWeb.core.service.ModoVentaService;

@Service
@Transactional
public class ModoVentaServiceImpl implements ModoVentaService {

	@Autowired
	private ModoVentaDAO modoVentaDAO;

	@Override
	public List<ModoVentaDTO> listar() {
		List<ModoVentaDTO> RetiroPedidoDTOs = new ArrayList<ModoVentaDTO>();
		for (ModoVenta RetiroPedido : modoVentaDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toModoVentaDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ModoVentaDTO getById(long id) {
		return modoVentaDAO.getById(id).toModoVentaDTO();
	}

	@Override
	public void insertar(ModoVentaDTO obj) {
		modoVentaDAO.insertar(ModoVenta.fromModoVentaDTO(obj));
	}

	@Override
	public void actualizar(ModoVentaDTO obj) {
		modoVentaDAO.actualizar(ModoVenta.fromModoVentaDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		modoVentaDAO.eliminar(id);
	}

	@Override
	public Boolean sinLogin() {
		return modoVentaDAO.sinLogin();
	}

}
