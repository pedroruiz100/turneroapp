package turneroWeb.core.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteCabDAO;
import turneroWeb.core.domain.FacturaClienteCab;
import turneroWeb.core.dto.ClienteDTO;
import turneroWeb.core.dto.FacturaClienteCabDTO;
import turneroWeb.core.service.FacturaClienteCabService;

@Service
@Transactional
public class FacturaClienteCabServiceImpl implements FacturaClienteCabService {

	@Autowired
	private FacturaClienteCabDAO facturaClienteCabDAO;

	// @Autowired
	// private TalonariosSucursaleDAO talSucuDAO;

	@Override
	public List<FacturaClienteCabDTO> listar() {
		List<FacturaClienteCabDTO> facDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.listar()) {
			facDTO.add(fac.toFacturaClienteCabDTO());
		}
		return facDTO;
	}

	@Override
	public FacturaClienteCabDTO getById(long id) {
		return facturaClienteCabDAO.getById(id).toFacturaClienteCabDTO();
	}

	@Override
	public void insertar(FacturaClienteCabDTO obj) {

	}

	@Override
	public void actualizar(FacturaClienteCabDTO obj) {
		java.util.Date date = new java.util.Date();
		Timestamp tiempoActual = new Timestamp(date.getTime());
		obj.setFechaMod(tiempoActual);

		if (obj.getCliente() == null) {
			ClienteDTO cliDTO = new ClienteDTO();
			cliDTO.setIdCliente(161168L);
			obj.setCliente(cliDTO);
		}

		facturaClienteCabDAO.actualizar(FacturaClienteCab.fromFacturaClienteCabAsociado(obj));

	}

	@Override
	public void eliminar(long id) {

	}

	@Override
	public FacturaClienteCabDTO insertarObtenerObjeto(FacturaClienteCabDTO facturaClienteCabDTO) {

		// java.util.Date date = new java.util.Date();
		// Timestamp tiempoActual = new Timestamp(date.getTime());
		// facturaClienteCabDTO.setFechaEmision(tiempoActual);
		if (facturaClienteCabDTO.getCliente() == null) {
			ClienteDTO cliDTO = new ClienteDTO();
			cliDTO.setIdCliente(161168L);
			facturaClienteCabDTO.setCliente(cliDTO);
		}

		// Map<String, String> mapeo = splitNroActual(facturaClienteCabDTO
		// .getNroActual());
		// long idTalonarioSucursal = Long.parseLong(mapeo.get(
		// "idTalonarioSucursal").toString());
		// int nroActual = Integer.parseInt(mapeo.get("nroActual").toString());
		//
		// // primer tr�o
		// long idSucursal = facturaClienteCabDTO.getSucursal().getIdSucursal();
		// // segundo tr�o
		// long nroCaja = facturaClienteCabDTO.getCaja().getNroCaja();

		// facturaClienteCabDTO.setNroFactura();

		FacturaClienteCab fac = FacturaClienteCab.fromFacturaClienteCabAsociado(facturaClienteCabDTO);
		FacturaClienteCab fact = facturaClienteCabDAO.insertarObtenerObjeto(fac);

		FacturaClienteCabDTO facDTOs = fact.toFacturaClienteCabDTO();
		// if (facDTOs.getIDFACTURACLIENTECAB() != NULL) {
		// NROACTUAL++;
		// TALSUCUDAO.ACTUALIZARNROACTUAL(IDTALONARIOSUCURSAL, NROACTUAL);
		// }
		return facDTOs;
	}

	@Override
	public void cancelarFactura(long id, String usuario) {
		facturaClienteCabDAO.cancelarFactura(id, usuario);

	}

	@Override
	public void actualizarMontoVenta(int monto, long id) {
		facturaClienteCabDAO.actualizarMontoVenta(monto, id);
	}

	private String procesandoNro(long idSucursal, long nroCaja, int nroActual) {
		nroActual++;
		// queda definir el primer y segundo tr�o de n�meros...
		String primerTrio = idSucursal + "";
		String segundoTrio = nroCaja + "";
		String nro = String.valueOf(nroActual);

		// Para agregar ceros delante
		// Para el primer tr�o
		// int tamTrio1 = primerTrio.length();
		// if (tamTrio1 < 3) {
		// tamTrio1 = 3 - tamTrio1;
		// for (int i = 0; i < tamTrio1; i++) {
		// primerTrio = "0" + primerTrio;
		// }
		// }

		// Para el segundo tr�o
		int tamTrio2 = segundoTrio.length();
		if (tamTrio2 < 3) {
			tamTrio2 = 3 - tamTrio2;
			for (int i = 0; i < tamTrio2; i++) {
				segundoTrio = "0" + segundoTrio;
			}
		}

		// Para la �ltima numeraci�n
		int tam = nro.length();
		if (tam < 7) {
			tam = 7 - tam;
			for (int i = 0; i < tam; i++) {
				nro = "0" + nro;
			}
		}
		nro = primerTrio + "" + segundoTrio + "" + nro;
		return nro;
	}

	private Map<String, String> splitNroActual(String dato) {
		Map<String, String> valor = new HashMap<String, String>();
		StringTokenizer st = new StringTokenizer(dato, " - ");
		String nroActual = st.nextElement().toString();
		String id = st.nextElement().toString();
		valor.put("idTalonarioSucursal", id);
		valor.put("nroActual", nroActual);
		return valor;
	}

	@Override
	public FacturaClienteCabDTO actualizarObtenerObjeto(FacturaClienteCabDTO facturaClienteCabDTO) {

		if (facturaClienteCabDTO.getCliente() == null) {
			ClienteDTO cliDTO = new ClienteDTO();
			cliDTO.setIdCliente(161168L);
			facturaClienteCabDTO.setCliente(cliDTO);
		}

		FacturaClienteCab fac = facturaClienteCabDAO
				.actualizarObtenerObjeto(FacturaClienteCab.fromFacturaClienteCabAsociado(facturaClienteCabDTO));
		System.out.println("Se ha impreso -->> "+true);
		return fac.toFacturaClienteCabDTO();
	}

	@Override
	public FacturaClienteCabDTO consultaHistorico(String nroFactura, String tipoComprobante, String fechaEmision,
			String nroCaja, String timbrado) {
//		nroFactura = Utilidades.msjVuelta(nroFactura);
//		tipoComprobante = Utilidades.msjVuelta(tipoComprobante);
//		fechaEmision = Utilidades.msjVuelta(fechaEmision);
//		nroCaja = Utilidades.msjVuelta(nroCaja);
//		timbrado = Utilidades.msjVuelta(timbrado);
		return null;
	}

	@Override
	public List<FacturaClienteCabDTO> filtroFecha(String fechaDesde,
			String fechaHasta, String nroCaja) {
		List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFecha(fechaDesde, fechaHasta, nroCaja)) {
			listFacDTO.add(fac.toFacturaClienteCabHistoricoDTO());
		}
		return listFacDTO;
	}

	@Override
	public List<FacturaClienteCabDTO> filtroFechaDescuento(String fechaDesde,
			String fechaHasta, String nroCaja) {
		List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFechaDescuento(fechaDesde, fechaHasta, nroCaja)) {
			listFacDTO.add(fac.toDescuentoFacturaClienteCabDTO());
		}
		return listFacDTO;
	}

	@Override
	public List<FacturaClienteCabDTO> filtroNroFact(String nroFactura,
			String nroCaja) {
		List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.filtroNroFact(nroFactura, nroCaja)) {
			listFacDTO.add(fac.toDescuentoDetallesFacturaClienteCabDTO());
		}
		return listFacDTO;
	}

	@Override
	public List<FacturaClienteCabDTO> filtroFechaDescuentoFact(
			String fechaDesde, String fechaHasta, String nroCaja, String nroFact) {
		List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFechaDescuentoFact(fechaDesde, fechaHasta, nroCaja, nroFact)) {
			listFacDTO.add(fac.toBDHIstoricoFormaPagoCajaDTO());
		}
		return listFacDTO;
	}

	@Override
	public List<FacturaClienteCabDTO> filtroFechaDesc(String fechaDesde,
			String fechaHasta, String nroCaja) {
		List<FacturaClienteCabDTO> listFacDTO = new ArrayList<FacturaClienteCabDTO>();
		for (FacturaClienteCab fac : facturaClienteCabDAO.filtroFechaDescuento(fechaDesde, fechaHasta, nroCaja)) {
			listFacDTO.add(fac.toDescuentoDetallesFacturaClienteCabDTO());
		}
		return listFacDTO;
	}

	@Override
	public List<JSONObject> recuperarLaguna(String fechaInicio,
			String fechaFin, String nroCaja) {
		List<JSONObject> listFacDTO = new ArrayList<JSONObject>();
		for (JSONObject json : facturaClienteCabDAO.recuperarLaguna(fechaInicio, fechaFin, nroCaja)) {
			listFacDTO.add(json);
		}
		return listFacDTO;
	}

	@Override
	public void actualizarNativo(long idFCC, int monto) {
		facturaClienteCabDAO.actualizarNativo(idFCC, monto);
	}
}
