package turneroWeb.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteCabHistoricoDAO;
import turneroWeb.core.domain.FacturaClienteCabHistorico;
import turneroWeb.core.dto.FacturaClienteCabHistoricoDTO;
import turneroWeb.core.service.FacturaClienteCabHistoricoService;

@Service
@Transactional
public class FacturaClienteCabHistoricoServiceImpl implements
		FacturaClienteCabHistoricoService {

	@Autowired
	private FacturaClienteCabHistoricoDAO facturaClienteCabHistoricoDAO;

	@Override
	public List<FacturaClienteCabHistoricoDTO> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FacturaClienteCabHistoricoDTO getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(FacturaClienteCabHistoricoDTO obj) {
		System.out.println("ACA 03");
		facturaClienteCabHistoricoDAO.insertar(FacturaClienteCabHistorico
				.fromFacturaClienteCabHistoricoAsociadoDTO(obj));
		System.out.println("ACA 04");
	}

	@Override
	public void actualizar(FacturaClienteCabHistoricoDTO obj) {
		facturaClienteCabHistoricoDAO.actualizar(FacturaClienteCabHistorico
				.fromFacturaClienteCabHistoricoAsociadoDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<FacturaClienteCabHistoricoDTO> listarPorFactura(
			Long idFacturaClienteCab) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FacturaClienteCabHistoricoDTO consultar(String nroFactFiltro,
			String tipoFactFiltro, String fechaFactFiltro,
			String nroCajaFiltro, String timbradoFiltro) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Integer> listarCajaDistinct() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> listarTimbDistinct() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertarActual(FacturaClienteCabHistoricoDTO fcchDTO, long idCab) {
		facturaClienteCabHistoricoDAO.insertarActual(FacturaClienteCabHistorico
				.fromFacturaClienteCabHistoricoAsociadoDTO(fcchDTO), idCab);
	}

}
