/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.AperturaCajaDAO;
import turneroWeb.core.dao.ArqueoCajaDAO;
import turneroWeb.core.dao.PublicidadDAO;
import turneroWeb.core.domain.Publicidad;
import turneroWeb.core.dto.PublicidadDTO;
import turneroWeb.core.service.PublicidadService;

@Service
@Transactional
public class PublicidadServiceImpl implements PublicidadService {

	@Autowired
	private PublicidadDAO PublicidadDAO;

	@Autowired
	private AperturaCajaDAO aperturaCajaDAO;

	@Autowired
	private ArqueoCajaDAO arqueoCajaDAO;

	@Override
	public List<PublicidadDTO> listar() {
		List<PublicidadDTO> PublicidadDTOs = new ArrayList<PublicidadDTO>();
		for (Publicidad Publicidad : PublicidadDAO.listar()) {
			PublicidadDTOs.add(Publicidad.toPublicidadDTO());
		}
		return PublicidadDTOs;
	}

	@Override
	public PublicidadDTO getById(long id) {
		return PublicidadDAO.getById(id).toPublicidadDTO();
	}

	@Override
	public void insertar(PublicidadDTO obj) {
		PublicidadDAO.insertar(Publicidad.fromPublicidadAsociado(obj));
	}

	@Override
	public void actualizar(PublicidadDTO obj) {
		PublicidadDAO.actualizar(Publicidad.fromPublicidadAsociado(obj));
	}

	@Override
	public void eliminar(long id) {
		PublicidadDAO.eliminar(id);
	}

	@Override
	public void actualizarPorId(long id) {
		Publicidad pu = PublicidadDAO.getById(id);
		pu.setEstado(true);
		PublicidadDAO.actualizar(pu);
	}

	@Override
	public void bajaPorId(long id) {
		Publicidad pu = PublicidadDAO.getById(id);
		pu.setEstado(false);
		PublicidadDAO.actualizar(pu);
	}

	@Override
	public List<PublicidadDTO> listarPublicidadTRUE() {
		List<PublicidadDTO> PublicidadDTOs = new ArrayList<PublicidadDTO>();
		for (Publicidad Publicidad : PublicidadDAO.listarPublicidadTRUE()) {
			PublicidadDTOs.add(Publicidad.toPublicidadDTO());
		}
		return PublicidadDTOs;
	}

	@Override
	public boolean aperturaCaja() {
		boolean apertura = false;
		if (aperturaCajaDAO.verificarAperturaDia()) {
			if (arqueoCajaDAO.verificarArqueoDia()) {
				long cantApertura = aperturaCajaDAO.getCantidadAperturaDia();
				long cantArqueo = arqueoCajaDAO.getCantidadArqueoDia();
				apertura = cantApertura == cantArqueo;
			} else {
				apertura = false;
			}
		} else {
			apertura = true;
		}
		return apertura;
	}

}
