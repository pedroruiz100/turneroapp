/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.RetiroPedidoDAO;
import turneroWeb.core.domain.RetiroPedido;
import turneroWeb.core.dto.RetiroPedidoDTO;
import turneroWeb.core.service.RetiroPedidoService;

@Service
@Transactional
public class RetiroPedidoServiceImpl implements RetiroPedidoService {

	@Autowired
	private RetiroPedidoDAO RetiroPedidoDAO;

	@Override
	public List<RetiroPedidoDTO> listar() {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public RetiroPedidoDTO getById(long id) {
		return RetiroPedidoDAO.getById(id).toRetiroPedidoDTO();
	}

	@Override
	public void insertar(RetiroPedidoDTO obj) {
		RetiroPedidoDAO.insertar(RetiroPedido.fromRetiroPedidoDTOAsociado(obj));
	}

	@Override
	public void actualizar(RetiroPedidoDTO obj) {
		RetiroPedidoDAO.actualizar(RetiroPedido
				.fromRetiroPedidoDTOAsociado(obj));
	}

	@Override
	public void eliminar(long id) {
		RetiroPedidoDAO.eliminar(id);
	}

	@Override
	public Long rowCount() {
		return RetiroPedidoDAO.rowCount();
	}

	@Override
	public List<RetiroPedidoDTO> listarNoFETCH(long limRow, long offSet) {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listarNoFetch(limRow,
				offSet)) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public List<RetiroPedidoDTO> listarFETCH(long limRow, long offSet) {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listarFETCH(limRow,
				offSet)) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public List<RetiroPedidoDTO> listarPreparacion() {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listarPreparacion()) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public List<RetiroPedidoDTO> listarListo() {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listarListo()) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public void actualizarPreparado(long id) {
		RetiroPedido rp = RetiroPedidoDAO.getById(id);
		rp.setEntregado(false);
		rp.setPreparacion(true);
		rp.setListo(false);
		RetiroPedidoDAO.actualizar(rp);
	}

	@Override
	public void actualizarListo(long id) {
		RetiroPedido rp = RetiroPedidoDAO.getById(id);
		rp.setEntregado(false);
		rp.setPreparacion(true);
		rp.setListo(true);
		RetiroPedidoDAO.actualizar(rp);
	}

	@Override
	public List<RetiroPedidoDTO> listarFullPreparacion() {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO
				.listarFullPreparacion()) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public List<RetiroPedidoDTO> listarFullListo() {
		List<RetiroPedidoDTO> RetiroPedidoDTOs = new ArrayList<RetiroPedidoDTO>();
		for (RetiroPedido RetiroPedido : RetiroPedidoDAO.listarFullListo()) {
			RetiroPedidoDTOs.add(RetiroPedido.toRetiroPedidoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public void actualizarEntregado(long id) {
		RetiroPedido rp = RetiroPedidoDAO.getById(id);
		rp.setEntregado(true);
		rp.setPreparacion(true);
		rp.setListo(true);
		RetiroPedidoDAO.actualizar(rp);
	}

	@Override
	public RetiroPedidoDTO buscarPorNumeroPedido(String num) {
		return RetiroPedidoDAO.buscarPorNumeroPedido(num).toRetiroPedidoDTO();
	}

}
