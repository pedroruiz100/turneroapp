/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.ClienteDAO;
import turneroWeb.core.domain.Barrio;
import turneroWeb.core.domain.Ciudad;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.Departamento;
import turneroWeb.core.domain.Pais;
import turneroWeb.core.dto.ClienteDTO;
import turneroWeb.core.service.ClienteService;
import turneroWeb.core.utiles.Utilidades;

@Service
@Transactional
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDAO clienteDAO;

	@Override
	public List<ClienteDTO> listar() {
		List<ClienteDTO> RetiroPedidoDTOs = new ArrayList<ClienteDTO>();
		for (Cliente RetiroPedido : clienteDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toClienteBDDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ClienteDTO getById(long id) {
		return clienteDAO.getById(id).toClienteBDDTO();
	}

	@Override
	public void insertar(ClienteDTO obj) {
		clienteDAO.insertar(Cliente.fromClienteDTO(obj));
	}

	@Override
	public void actualizar(ClienteDTO obj) {
		clienteDAO.actualizar(Cliente.fromClienteDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		clienteDAO.eliminar(id);
	}

	@Override
	public List<ClienteDTO> listarPorNomRuc(String nombre, String apellido,
			String ruc) {
		List<ClienteDTO> RetiroPedidoDTOs = new ArrayList<ClienteDTO>();
		for (Cliente RetiroPedido : clienteDAO.listarPorNomRuc(nombre,
				apellido, ruc)) {
			RetiroPedidoDTOs.add(RetiroPedido.toClienteBDDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public void actualizarNomApeRuc(String nombre, String apellido, String ruc,
			long id, String usuMod, Timestamp fechaMod) {
		clienteDAO.actualizarNomApeRuc(nombre, apellido, ruc, id, usuMod,
				fechaMod);
	}

	@Override
	public ClienteDTO listarPorCodigo(int codCliente) {
		Cliente cli = clienteDAO.listarPorCodigo(codCliente);
		if (cli != null) {
			return cli.toClienteBDDTO();
		} else {
			return null;
		}
	}

	@Override
	public ClienteDTO getByCod(int codCli) {
		Cliente cli = clienteDAO.getByCod(codCli);
		if (cli != null) {
			return cli.toClienteBDDTO();
		} else {
			return null;
		}
	}

	@Override
	public void actualizarNomApeRuc(String toString, String toString0,
			String toString1, String toString2, String toString3,
			long parseLong, String nombreCaj, Timestamp timestamp) {
		clienteDAO.actualizarNomApeRuc(toString, toString0, toString1,
				toString2, toString3, parseLong, nombreCaj, timestamp);
	}

	@Override
	public ClienteDTO insetarDatos(String ci, String nombre, String apellido) {
		Cliente cli = new Cliente();
		cli.setRuc(ci);
		cli.setNombre(nombre);
		if (apellido.equalsIgnoreCase("NULL")) {
			cli.setApellido("");
		} else {
			cli.setApellido(apellido);
		}

		Pais p = new Pais();
		p.setIdPais(0l);

		Barrio b = new Barrio();
		b.setIdBarrio(0l);

		Ciudad c = new Ciudad();
		c.setIdCiudad(0l);

		Departamento d = new Departamento();
		d.setIdDepartamento(0l);

		cli.setPais(p);
		cli.setBarrio(b);
		cli.setCiudad(c);
		cli.setDepartamento(d);
		cli.setCodCliente(Integer.parseInt(ci));

		Cliente clienteDato = clienteDAO.insertarDatos(cli);
		if (clienteDato == null) {
			return new ClienteDTO();
		} else {
			return clienteDato.toClienteBDDTO();
		}

	}

	@Override
	public ClienteDTO listarPorCI(String cod) {
		Cliente cli = clienteDAO.listarPorCI(cod);
		if (cli != null) {
			return cli.toClienteBDDTO();
		} else {
			return null;
		}
	}

	@Override
	public ClienteDTO listarPorRuc(String ruc) {
		Cliente cli = clienteDAO.listarPorRuc(ruc);
		if (cli != null) {
			return cli.toClienteBDDTO();
		} else {
			return null;
		}
	}

	@Override
	public ClienteDTO insetarDatosData(String ci, String nombre, String apellido) {
		Cliente cli = new Cliente();

		char[] xyz = ci.toCharArray();
		String dataString = "";
		boolean val = false;
		for (int i = 0; i < ci.length(); i++) {
			if (Character.isDigit(xyz[i])) {
				val = false;
			} else if (Character.isLetter(xyz[i])) {
				val = false;
			} else {
				val = true;
				String[] data = ci.split(xyz[i] + "");
				dataString = data[0];
				break;
			}
		}
		if (val) {
			ci = Utilidades.calculoSET(dataString);
		} else {
			dataString = ci;
			ci = Utilidades.calculoSET(ci);
		}
		cli = clienteDAO.listarPorCI(dataString);
		boolean value = false;
		if (cli == null) {
			cli = new Cliente();
			value = false;
		} else {
			value = true;
		}
		cli.setRuc(ci);
		cli.setNombre(nombre);
		if (apellido.equalsIgnoreCase("NULL")) {
			cli.setApellido("");
		} else {
			cli.setApellido(apellido);
		}
		Pais p = new Pais();
		p.setIdPais(0l);

		Barrio b = new Barrio();
		b.setIdBarrio(0l);

		Ciudad c = new Ciudad();
		c.setIdCiudad(0l);

		Departamento d = new Departamento();
		d.setIdDepartamento(0l);

		cli.setPais(p);
		cli.setBarrio(b);
		cli.setCiudad(c);
		cli.setDepartamento(d);
		try {
			cli.setCodCliente(Integer.parseInt(ci));
		} catch (Exception e) {
			cli.setCodCliente(Integer.parseInt(dataString));
		} finally {
		}
		if (!value) {
			System.out.println(cli.getRuc() + " - INSERTAR");
			Cliente clienteDato = clienteDAO.insertarDatos(cli);
			if (clienteDato == null) {
				return new ClienteDTO();
			} else {
				return clienteDato.toClienteBDDTO();
			}
		} else {
			System.out.println(cli.getRuc() + " - ACTUALIZAR");
			Cliente clienteDato = clienteDAO.actualizarDatos(cli);
			if (clienteDato == null) {
				return new ClienteDTO();
			} else {
				return clienteDato.toClienteBDDTO();
			}
		}
	}

}
