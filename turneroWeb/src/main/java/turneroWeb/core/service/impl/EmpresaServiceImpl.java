package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.EmpresaDAO;
import turneroWeb.core.domain.Empresa;
import turneroWeb.core.dto.EmpresaDTO;
import turneroWeb.core.service.EmpresaService;

@Service
@Transactional
public class EmpresaServiceImpl implements EmpresaService {

	@Autowired
	private EmpresaDAO empresaDAO;

	@Override
	public List<EmpresaDTO> listar() {
		List<EmpresaDTO> RetiroPedidoDTOs = new ArrayList<EmpresaDTO>();
		for (Empresa RetiroPedido : empresaDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toEmpresaDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public EmpresaDTO getById(long id) {
		return empresaDAO.getById(id).toEmpresaDTO();
	}

	@Override
	public void insertar(EmpresaDTO obj) {
		empresaDAO.insertar(Empresa.fromEmpresaDTO(obj));
	}

	@Override
	public void actualizar(EmpresaDTO obj) {
		empresaDAO.actualizar(Empresa.fromEmpresaDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		empresaDAO.eliminar(id);
	}

}
