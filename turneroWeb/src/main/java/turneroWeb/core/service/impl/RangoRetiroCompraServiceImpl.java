package turneroWeb.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.RangoRetiroCompraDAO;
import turneroWeb.core.dto.RangoRetiroCompraDTO;
import turneroWeb.core.service.RangoRetiroCompraService;

@Service
@Transactional
public class RangoRetiroCompraServiceImpl implements RangoRetiroCompraService {

	@Autowired
	private RangoRetiroCompraDAO rangoRetiroCompraDAO;

	@Override
	public long actualizarObtenerRango(long idRango) {
		return rangoRetiroCompraDAO.actualizarObtenerRango(idRango);
	}

	@Override
	public RangoRetiroCompraDTO actualizarObtenerRangoObjectActual(long idRango1) {
		return rangoRetiroCompraDAO
				.actualizarObtenerRangoObjectActual(idRango1)
				.toRangoRetiroCompraDTO();
	}

	@Override
	public RangoRetiroCompraDTO getById(long id) {
		return rangoRetiroCompraDAO.getById(id).toRangoRetiroCompraDTO();
	}

}
