package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteDetDAO;
import turneroWeb.core.domain.FacturaClienteDet;
import turneroWeb.core.dto.FacturaClienteDetDTO;
import turneroWeb.core.service.FacturaClienteDetService;

@Service
@Transactional
public class FacturaClienteDetServiceImpl implements FacturaClienteDetService {
	@Autowired
	private FacturaClienteDetDAO facturaClienteDetDAO;

	@Override
	public List<FacturaClienteDetDTO> listar() {
		List<FacturaClienteDetDTO> facDTO = new ArrayList<FacturaClienteDetDTO>();
		for (FacturaClienteDet fac : facturaClienteDetDAO.listar()) {
			facDTO.add(fac.toFacturaClienteDetDTO());
		}
		return facDTO;
	}

	@Override
	public FacturaClienteDetDTO getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(FacturaClienteDetDTO obj) {
		facturaClienteDetDAO.insercionMasiva(
				FacturaClienteDet.fromFacturaClienteDetAsociado(obj), 0);
	}

	@Override
	public void actualizar(FacturaClienteDetDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insercionMasiva(List<FacturaClienteDetDTO> facDTO) {
		long numero = 0;
		// int precio = 0;
		// long id = 0L;
		for (FacturaClienteDetDTO fDTO : facDTO) {
			numero++;
			facturaClienteDetDAO.insercionMasiva(
					FacturaClienteDet.fromFacturaClienteDetAsociado(fDTO),
					numero);
			// precio += (fDTO.getPrecio() * fDTO.getCantidad());
			// id = fDTO.getFacturaClienteCab().getIdFacturaClienteCab();
		}
		// facDAO.actualizarMontoVenta(precio, id);
	}

	@Override
	public List<FacturaClienteDetDTO> filtroFechaDescuentoFact(
			String fechaDesde, String fechaHasta, String nroCaja, String nroFact) {
		List<FacturaClienteDetDTO> listFacDTO = new ArrayList<FacturaClienteDetDTO>();
		for (FacturaClienteDet fac : facturaClienteDetDAO
				.filtroFechaDescuentoFact(fechaDesde, fechaHasta, nroCaja,
						nroFact)) {
			listFacDTO.add(fac.toFacturaClienteDetDTO());
		}
		return listFacDTO;
	}

	@Override
	public FacturaClienteDetDTO listarPorId(long id) {
		// List<FacturaClienteDetDTO> facDTO = new
		// ArrayList<FacturaClienteDetDTO>();
		// for (FacturaClienteDet fac : facturaClienteDetDAO.listar()) {
		// facDTO.add(fac.toFacturaClienteDetDTO());
		// }
		FacturaClienteDet factDet = facturaClienteDetDAO.listarPorId(id);
		if (factDet != null) {
			return factDet.toArtFacturaClienteDetDTO();
		} else {
			return null;
		}
	}

	@Override
	public List<FacturaClienteDetDTO> listFacturaCab(long id) {
		List<FacturaClienteDetDTO> facDTO = new ArrayList<FacturaClienteDetDTO>();
		for (FacturaClienteDet fac : facturaClienteDetDAO.listFacturaCab(id)) {
			facDTO.add(fac.toFacturaClienteDetDTO());
		}
		return facDTO;
	}
}
