package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.UsuarioDAO;
import turneroWeb.core.domain.Usuario;
import turneroWeb.core.dto.UsuarioDTO;
import turneroWeb.core.service.UsuarioService;

@Service
@Transactional
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioDAO UsuarioDAO;

	@Override
	public List<UsuarioDTO> listar() {
		List<UsuarioDTO> RetiroPedidoDTOs = new ArrayList<UsuarioDTO>();
		for (Usuario RetiroPedido : UsuarioDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toUsuarioDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public UsuarioDTO getById(long id) {
		return UsuarioDAO.getById(id).toUsuarioDTO();
	}

	@Override
	public void insertar(UsuarioDTO obj) {
		UsuarioDAO.insertar(Usuario.fromUsuarioSupervisorDTO(obj));
	}

	@Override
	public void actualizar(UsuarioDTO obj) {
		UsuarioDAO.actualizar(Usuario.fromUsuarioSupervisorDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		UsuarioDAO.eliminar(id);
	}

}
