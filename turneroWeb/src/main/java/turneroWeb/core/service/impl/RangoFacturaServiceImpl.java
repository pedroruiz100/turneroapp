package turneroWeb.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.RangoFacturaDAO;
import turneroWeb.core.domain.RangoFactura;
import turneroWeb.core.dto.RangoFacturaDTO;
import turneroWeb.core.service.RangoFacturaService;

@Service
@Transactional
public class RangoFacturaServiceImpl implements RangoFacturaService {

	@Autowired
	RangoFacturaDAO rangoFacturaDAO;

	@Override
	public boolean insertarObtenerEstado(RangoFacturaDTO rango) {
		return rangoFacturaDAO.insertarObtenerEstado(RangoFactura
				.fromRangoFacturaDTO(rango));
	}

	@Override
	public boolean actualizarObtenerEstado(long idRango) {
		return rangoFacturaDAO.actualizarObtenerEstado(idRango);
	}

	@Override
	public long actualizarObtenerRangoActual(long idRango) {
		// TODO Auto-generated method stub
		return rangoFacturaDAO.actualizarObtenerRangoActual(idRango);
	}

}
