package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteCabTarjetaDAO;
import turneroWeb.core.domain.FacturaClienteCabTarjeta;
import turneroWeb.core.dto.FacturaClienteCabTarjetaDTO;
import turneroWeb.core.service.FacturaClienteCabTarjetaService;

@Service
@Transactional
public class FacturaClienteCabTarjetaServiceImpl implements
		FacturaClienteCabTarjetaService {

	@Autowired
	private FacturaClienteCabTarjetaDAO facturaClienteCabTarjetaDAO;

	@Override
	public List<FacturaClienteCabTarjetaDTO> listar() {
		List<FacturaClienteCabTarjetaDTO> RetiroPedidoDTOs = new ArrayList<FacturaClienteCabTarjetaDTO>();
		for (FacturaClienteCabTarjeta RetiroPedido : facturaClienteCabTarjetaDAO
				.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido
					.toBDFacturaClienteCabTarjetaConDetalleDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public FacturaClienteCabTarjetaDTO getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(FacturaClienteCabTarjetaDTO obj) {
		facturaClienteCabTarjetaDAO.insertarObtenerObj(FacturaClienteCabTarjeta
				.fromFacturaClienteCabTarjetaAsociadoDTO(obj));
	}

	@Override
	public void actualizar(FacturaClienteCabTarjetaDTO obj) {
		facturaClienteCabTarjetaDAO.actualizar(FacturaClienteCabTarjeta
				.fromFacturaClienteCabTarjetaAsociadoDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public FacturaClienteCabTarjetaDTO insercionMasiva(
			FacturaClienteCabTarjetaDTO fac, int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FacturaClienteCabTarjetaDTO insertarObtenerObj(
			FacturaClienteCabTarjetaDTO facturaClienteCabTarjeta) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FacturaClienteCabTarjetaDTO> listarPorFactura(long parseLong) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FacturaClienteCabTarjetaDTO> filtroFechaTarjeta(
			String fechaInicio, String fechaFin, String selectedItem) {
		// TODO Auto-generated method stub
		return null;
	}

}
