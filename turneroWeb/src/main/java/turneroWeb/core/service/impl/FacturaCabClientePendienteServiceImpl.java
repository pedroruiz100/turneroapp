package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaCabClientePendienteDAO;
import turneroWeb.core.domain.FacturaCabClientePendiente;
import turneroWeb.core.dto.FacturaCabClientePendienteDTO;
import turneroWeb.core.service.FacturaCabClientePendienteService;

@Service
@Transactional
public class FacturaCabClientePendienteServiceImpl implements
		FacturaCabClientePendienteService {

	@Autowired
	private FacturaCabClientePendienteDAO fccpDAO;

	@Override
	public List<FacturaCabClientePendienteDTO> listar() {
		List<FacturaCabClientePendienteDTO> RetiroPedidoDTOs = new ArrayList<FacturaCabClientePendienteDTO>();
		for (FacturaCabClientePendiente RetiroPedido : fccpDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido
					.toBDFacturaCabClientePendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public FacturaCabClientePendienteDTO getById(long id) {
		return fccpDAO.getById(id).toBDFacturaCabClientePendienteDTO();
	}

	@Override
	public void insertar(FacturaCabClientePendienteDTO obj) {
		fccpDAO.insertar(FacturaCabClientePendiente
				.fromFacturaCabClientePendienteDTO(obj));
	}

	@Override
	public void actualizar(FacturaCabClientePendienteDTO obj) {
		fccpDAO.actualizar(FacturaCabClientePendiente
				.fromFacturaCabClientePendienteDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		fccpDAO.eliminar(id);
	}

	@Override
	public FacturaCabClientePendienteDTO insertarObtenerObj(
			FacturaCabClientePendienteDTO fromFacturaCabClientePendienteAsociado) {
		FacturaCabClientePendiente cliPen = fccpDAO.insertarObtenerObj(FacturaCabClientePendiente
				.fromFacturaCabClientePendienteDTO(fromFacturaCabClientePendienteAsociado));
		return cliPen.toBDFacturaCabClientePendienteDTO();
	}

}
