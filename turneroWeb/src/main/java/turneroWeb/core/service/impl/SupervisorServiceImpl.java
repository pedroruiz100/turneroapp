package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.SupervisorDAO;
import turneroWeb.core.domain.Supervisor;
import turneroWeb.core.dto.SupervisorDTO;
import turneroWeb.core.service.SupervisorService;
import turneroWeb.core.utiles.Utilidades;

@Service
@Transactional
public class SupervisorServiceImpl implements SupervisorService {

	@Autowired
	private SupervisorDAO supervisorDAO;

	@Override
	public List<SupervisorDTO> listar() {
		List<SupervisorDTO> RetiroPedidoDTOs = new ArrayList<SupervisorDTO>();
		for (Supervisor RetiroPedido : supervisorDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toSupervisorDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public SupervisorDTO getById(long id) {
		return supervisorDAO.getById(id).toSupervisorDTO();
	}

	@Override
	public void insertar(SupervisorDTO obj) {
		supervisorDAO.insertar(Supervisor.fromSupervisorDTO(obj));
	}

	@Override
	public void actualizar(SupervisorDTO obj) {
		supervisorDAO.actualizar(Supervisor.fromSupervisorDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		supervisorDAO.eliminar(id);
	}

	@Override
	public SupervisorDTO buscarCodSup(String cod) {
		try {
			return supervisorDAO.buscarCodSup(Utilidades.msjIda(cod))
					.toSupervisorDTO();
		} catch (Exception e) {
			return new SupervisorDTO();
		} finally {
		}
	}

}
