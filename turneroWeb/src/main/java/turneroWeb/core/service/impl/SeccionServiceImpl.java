package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.SeccionDAO;
import turneroWeb.core.domain.Seccion;
import turneroWeb.core.dto.SeccionDTO;
import turneroWeb.core.service.SeccionService;

@Service
@Transactional
public class SeccionServiceImpl implements SeccionService {

	@Autowired
	private SeccionDAO SeccionDAO;

	@Override
	public List<SeccionDTO> listar() {
		List<SeccionDTO> RetiroPedidoDTOs = new ArrayList<SeccionDTO>();
		for (Seccion RetiroPedido : SeccionDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toSeccionDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public SeccionDTO getById(long id) {
		return SeccionDAO.getById(id).toSeccionDTO();
	}

	@Override
	public void insertar(SeccionDTO obj) {
		SeccionDAO.insertar(Seccion.fromSeccionDTO(obj));
	}

	@Override
	public void actualizar(SeccionDTO obj) {
		SeccionDAO.actualizar(Seccion.fromSeccionDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		SeccionDAO.eliminar(id);
	}

	@Override
	public List<SeccionDTO> fullActivos() {
		List<SeccionDTO> RetiroPedidoDTOs = new ArrayList<SeccionDTO>();
		for (Seccion RetiroPedido : SeccionDAO.fullActivos()) {
			RetiroPedidoDTOs.add(RetiroPedido.toSeccionDTO());
		}
		return RetiroPedidoDTOs;
	}

}
