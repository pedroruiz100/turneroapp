package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.ArticuloDAO;
import turneroWeb.core.dao.ServPendienteDAO;
import turneroWeb.core.domain.Articulo;
import turneroWeb.core.domain.ServPendiente;
import turneroWeb.core.dto.ArticuloDTO;
import turneroWeb.core.dto.ServPendienteDTO;
import turneroWeb.core.service.ServPendienteService;

@Service
@Transactional
public class ServPendienteServiceImpl implements ServPendienteService {

	@Autowired
	private ServPendienteDAO ServPendienteDAO;
	@Autowired
	private ArticuloDAO artDAO;

	@Override
	public List<ServPendienteDTO> listar() {
		List<ServPendienteDTO> RetiroPedidoDTOs = new ArrayList<ServPendienteDTO>();
		for (ServPendiente RetiroPedido : ServPendienteDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toBDServPendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ServPendienteDTO getById(long id) {
		return ServPendienteDAO.getById(id).toBDServPendienteDTO();
	}

	@Override
	public void insertar(ServPendienteDTO obj) {
		ServPendienteDAO.insertar(ServPendiente.fromServPendienteDTO(obj));
	}

	@Override
	public void actualizar(ServPendienteDTO obj) {
		ServPendienteDAO.actualizar(ServPendiente.fromServPendienteDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		ServPendienteDAO.eliminar(id);
	}

	@Override
	public List<ServPendienteDTO> buscandoServiciosAsignados(long id1, long id2) {
		List<ServPendienteDTO> RetiroPedidoDTOs = new ArrayList<ServPendienteDTO>();
		for (ServPendiente RetiroPedido : ServPendienteDAO
				.buscandoServiciosAsignados(id1, id2)) {
			System.out.println(RetiroPedido.getArticulo().getIdArticulo()
					+ " - " + RetiroPedido.getArticulo().getDescripcion());
			ServPendienteDTO servPendienteDTO = RetiroPedido
					.toBDServPendienteDTO();
			ArticuloDTO artDTO = RetiroPedido.getArticulo().toArticuloDTO();

			ArticuloDTO articuDTO = new ArticuloDTO();
			articuDTO.setIdArticulo(artDTO.getIdArticulo());

			Articulo articulado = artDAO.getById(artDTO.getIdArticulo());
			articuDTO.setCodArticulo(articulado.getCodArticulo());

			servPendienteDTO.setArticulo(articuDTO);
			RetiroPedidoDTOs.add(servPendienteDTO);
		}
		// Map mapeo = new HashedMap();
		// for (ServPendiente RetiroPedido : ServPendienteDAO
		// .buscandoServiciosAsignados(id1, id2)) {
		// if (mapeo.containsKey(RetiroPedido.getCodArticulo())) {
		// ServPendienteDTO spDTO = (ServPendienteDTO) mapeo
		// .get(RetiroPedido.getCodArticulo());
		// int cantArt = spDTO.getCantidad() + 1;
		// RetiroPedidoDTOs.remove(spDTO);
		// spDTO.setCantidad(cantArt);
		// RetiroPedidoDTOs.add(spDTO);
		// } else {
		// ServPendienteDTO spDTO = RetiroPedido.toBDServPendienteDTO();
		// spDTO.setCantidad(1);
		// mapeo.put(RetiroPedido.getCodArticulo(), spDTO);
		// RetiroPedidoDTOs.add(spDTO);
		// }
		// }
		return RetiroPedidoDTOs;
	}

	@Override
	public ServPendienteDTO insertarObtenerObj(
			ServPendienteDTO fromServPendienteAsociado) {
		ServPendiente sp = ServPendienteDAO.insertarObtenerObj(ServPendiente
				.fromServPendienteDTO(fromServPendienteAsociado));
		if (sp != null) {

			return sp.toBDServPendienteDTO();
		} else {
			return new ServPendienteDTO();
		}
	}

	@Override
	public List<ServPendienteDTO> buscandoServiciosComision(long idFu,
			String fechaInicio, String fechaFin) {
		List<ServPendienteDTO> RetiroPedidoDTOs = new ArrayList<ServPendienteDTO>();
		for (ServPendiente RetiroPedido : ServPendienteDAO
				.buscandoServiciosComision(idFu, fechaInicio, fechaFin)) {
			RetiroPedidoDTOs.add(RetiroPedido.toBDServPendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public List<ServPendienteDTO> buscandoServiciosComisionOrderArt(long idFu,
			String fechaInicio, String fechaFin) {
		List<ServPendienteDTO> RetiroPedidoDTOs = new ArrayList<ServPendienteDTO>();
		for (ServPendiente RetiroPedido : ServPendienteDAO
				.buscandoServiciosComisionOrderArt(idFu, fechaInicio, fechaFin)) {
			RetiroPedidoDTOs.add(RetiroPedido.toBDServPendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public void eliminarPorId(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean actualizarPorLista(List<Object> lista) {
		boolean val = false;
		// List<ServPendienteDTO> RetiroPedidoDTOs = new
		// ArrayList<ServPendienteDTO>();
		System.out.println("LLEGAMOS HASTA AQUI...");
		for (Object obj : lista) {
			ServPendienteDTO RetiroPedido = (ServPendienteDTO) obj;
			System.out.println("DATAS ---->> "
					+ RetiroPedido.getIdServPendiente());
			ServPendienteDAO.actualizar(ServPendiente
					.fromServPendienteDTO(RetiroPedido));
			val = true;
		}
		return val;
	}

	@Override
	public boolean actualizarByEstado(ServPendienteDTO servPendienteDTO) {
		try {
			ServPendiente sp = ServPendienteDAO.getById(servPendienteDTO
					.getIdServPendiente());
			sp.setCantidad(servPendienteDTO.getCantidad());
			System.out.println("HOLA NOMAS ---->> " + sp.getIdServPendiente()
					+ " - " + sp.getDescripcion() + " - " + sp.getCantidad());
			ServPendienteDAO.actualizar(sp);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public boolean actualizarCantidad(long id, int cant) {
		try {
			ServPendiente sp = ServPendienteDAO.getById(id);
			sp.setCantidad(cant);
			System.out.println("HOLA NOMAS ---->> " + sp.getIdServPendiente()
					+ " - " + sp.getDescripcion() + " - " + sp.getCantidad());
			ServPendienteDAO.actualizar(sp);
			return true;
		} catch (Exception e) {
			return false;
		} finally {
		}
	}

	@Override
	public void eliminarPorIdClientePendiente(long idCP) {
		ServPendienteDAO.eliminarPorIdClientePendiente(idCP);
	}

}
