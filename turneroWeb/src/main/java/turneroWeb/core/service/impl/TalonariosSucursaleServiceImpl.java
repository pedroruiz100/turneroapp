package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.TalonariosSucursaleDAO;
import turneroWeb.core.domain.TalonariosSucursale;
import turneroWeb.core.dto.TalonariosSucursaleDTO;
import turneroWeb.core.service.TalonariosSucursaleService;

@Service
@Transactional
public class TalonariosSucursaleServiceImpl implements
		TalonariosSucursaleService {

	@Autowired
	TalonariosSucursaleDAO talonariosSucursaleDAO;

	@Override
	public List<TalonariosSucursaleDTO> listar() {
		List<TalonariosSucursaleDTO> RetiroPedidoDTOs = new ArrayList<TalonariosSucursaleDTO>();
		for (TalonariosSucursale RetiroPedido : talonariosSucursaleDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toBDTalonariosDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public TalonariosSucursaleDTO getById(long id) {
		return talonariosSucursaleDAO.getById(id).toBDTalonariosDTO();
	}

	@Override
	public void insertar(TalonariosSucursaleDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizar(TalonariosSucursaleDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public TalonariosSucursaleDTO listarPorSucursal(long id, long idTimbrado) {
		TalonariosSucursale ts = talonariosSucursaleDAO.listarPorSucursal(id,
				idTimbrado);
		ts.setNroActual(ts.getNroActual() + 1);
		talonariosSucursaleDAO.actualizar(ts);
		return ts.toBDTalonariosDTO();
	}
}
