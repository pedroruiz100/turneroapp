package turneroWeb.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteCabNotaCreditoDAO;
import turneroWeb.core.domain.FacturaClienteCabNotaCredito;
import turneroWeb.core.dto.FacturaClienteCabNotaCreditoDTO;
import turneroWeb.core.service.FacturaClienteCabNotaCreditoService;

@Service
@Transactional
public class FacturaClienteCabNotaCreditoServiceImpl implements
		FacturaClienteCabNotaCreditoService {

	@Autowired
	private FacturaClienteCabNotaCreditoDAO facturaClienteCabNotaCreditoDAO;

	@Override
	public List<FacturaClienteCabNotaCreditoDTO> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public FacturaClienteCabNotaCreditoDTO getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(FacturaClienteCabNotaCreditoDTO obj) {
		facturaClienteCabNotaCreditoDAO
				.insertarObtenerObjeto(FacturaClienteCabNotaCredito
						.fromFacturaClienteCabNotaCreditoAsociado(obj));
	}

	@Override
	public void actualizar(FacturaClienteCabNotaCreditoDTO obj) {
		facturaClienteCabNotaCreditoDAO.actualizar(FacturaClienteCabNotaCredito
				.fromFacturaClienteCabNotaCreditoAsociado(obj));
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public FacturaClienteCabNotaCreditoDTO insertarObtenerObjeto(
			FacturaClienteCabNotaCreditoDTO fac) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<FacturaClienteCabNotaCreditoDTO> listarPorFactura(long parseLong) {
		// TODO Auto-generated method stub
		return null;
	}

}
