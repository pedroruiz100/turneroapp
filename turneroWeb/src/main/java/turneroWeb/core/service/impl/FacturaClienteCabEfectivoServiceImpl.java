package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FacturaClienteCabEfectivoDAO;
import turneroWeb.core.domain.FacturaClienteCabEfectivo;
import turneroWeb.core.dto.FacturaClienteCabEfectivoDTO;
import turneroWeb.core.service.FacturaClienteCabEfectivoService;

@Service
@Transactional
public class FacturaClienteCabEfectivoServiceImpl implements
		FacturaClienteCabEfectivoService {

	@Autowired
	private FacturaClienteCabEfectivoDAO facturaClienteCabEfectivoDAO;

	@Override
	public List<FacturaClienteCabEfectivoDTO> listar() {
		List<FacturaClienteCabEfectivoDTO> facDTO = new ArrayList<FacturaClienteCabEfectivoDTO>();
		for (FacturaClienteCabEfectivo fac : facturaClienteCabEfectivoDAO
				.listar()) {
			facDTO.add(fac.toFacturaClienteCabEfectivoDTO());
		}
		return facDTO;
	}

	@Override
	public FacturaClienteCabEfectivoDTO getById(long id) {
		return facturaClienteCabEfectivoDAO.getById(id)
				.toFacturaClienteCabEfectivoDTO();
	}

	@Override
	public void insertar(FacturaClienteCabEfectivoDTO obj) {
		facturaClienteCabEfectivoDAO.insertar(FacturaClienteCabEfectivo
				.fromFacturaClienteCabEfectivoAsociadoDTO(obj));
	}

	@Override
	public void actualizar(FacturaClienteCabEfectivoDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public FacturaClienteCabEfectivoDTO insertarObtenerObjeto(
			FacturaClienteCabEfectivoDTO facturaClienteCabEfectivo) {
		// TODO Auto-generated method stub
		// return
		FacturaClienteCabEfectivo fcce = FacturaClienteCabEfectivo
				.fromFacturaClienteCabEfectivoDTO(facturaClienteCabEfectivo);
		// facturaClienteCabEfectivoDAO.insertarObtenerObjeto(FacturaClienteCabEfectivo.fromFacturaClienteCabEfectivoAsociadoDTO(facturaClienteCabEfectivo));
		return facturaClienteCabEfectivoDAO.insertarObtenerObjeto(fcce)
				.toFacturaClienteCabEfectivoDTO();
	}

	@Override
	public List<FacturaClienteCabEfectivoDTO> listarPorFactura(long parseLong) {
		// TODO Auto-generated method stub
		return null;
	}

}
