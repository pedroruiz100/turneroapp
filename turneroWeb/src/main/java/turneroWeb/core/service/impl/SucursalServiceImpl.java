package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.SucursalDAO;
import turneroWeb.core.domain.Sucursal;
import turneroWeb.core.dto.SucursalDTO;
import turneroWeb.core.service.SucursalService;

@Service
@Transactional
public class SucursalServiceImpl implements SucursalService {

	@Autowired
	SucursalDAO sucursalDAO;

	@Override
	public List<SucursalDTO> listar() {
		List<SucursalDTO> RetiroPedidoDTOs = new ArrayList<SucursalDTO>();
		for (Sucursal RetiroPedido : sucursalDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toSucursalEmpresaDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public SucursalDTO getById(long id) {
		return sucursalDAO.getById(id).toSucursalEmpresaDTO();
	}

	@Override
	public void insertar(SucursalDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizar(SucursalDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

}
