package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.TimbradoDAO;
import turneroWeb.core.domain.Timbrado;
import turneroWeb.core.dto.TimbradoDTO;
import turneroWeb.core.service.TimbradoService;

@Service
@Transactional
public class TimbradoServiceImpl implements TimbradoService {

	@Autowired
	TimbradoDAO timbradoDAO;

	@Override
	public List<TimbradoDTO> listar() {
		List<TimbradoDTO> RetiroPedidoDTOs = new ArrayList<TimbradoDTO>();
		for (Timbrado RetiroPedido : timbradoDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toBDTimbradoDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public TimbradoDTO getById(long id) {
		return timbradoDAO.getById(id).toBDTimbradoDTO();
	}

	@Override
	public void insertar(TimbradoDTO obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizar(TimbradoDTO obj) {
		// TODO Auto-generated method stub
		timbradoDAO.actualizar(Timbrado.fromTimbrado(obj));
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

}
