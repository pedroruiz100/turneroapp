package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.ArticuloDAO;
import turneroWeb.core.dao.IvaDAO;
import turneroWeb.core.domain.Articulo;
import turneroWeb.core.domain.Iva;
import turneroWeb.core.dto.ArticuloDTO;
import turneroWeb.core.service.ArticuloService;

@Service
@Transactional
public class ArticuloServiceImpl implements ArticuloService {

	@Autowired
	private ArticuloDAO ArticuloDAO;

	@Autowired
	private IvaDAO ivaDAO;

	@Override
	public List<ArticuloDTO> listar() {
		List<ArticuloDTO> RetiroPedidoDTOs = new ArrayList<ArticuloDTO>();
		for (Articulo RetiroPedido : ArticuloDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toArticuloDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ArticuloDTO getById(long id) {
		return ArticuloDAO.getById(id).toArticuloDTO();
	}

	@Override
	public void insertar(ArticuloDTO obj) {
		ArticuloDAO.insertar(Articulo.fromArticuloDTO(obj));
	}

	@Override
	public void actualizar(ArticuloDTO obj) {
		ArticuloDAO.actualizar(Articulo.fromArticuloDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		ArticuloDAO.eliminar(id);
	}

	@Override
	public List<ArticuloDTO> listarPorSecciones(long id) {
		List<ArticuloDTO> RetiroPedidoDTOs = new ArrayList<ArticuloDTO>();
		for (Articulo RetiroPedido : ArticuloDAO.listarPorSecciones(id)) {
			RetiroPedidoDTOs.add(RetiroPedido.toArticuloDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ArticuloDTO listarPorCodigo(String id) {
		long idIva = ivaDAO.recuperarIvaPorCodigo(id);
		System.out.println("1) -->> " + idIva);
		if (idIva > 0) {
			Iva iva = ivaDAO.getById(idIva);
			System.out.println("2) -->> " + iva.getIdIva());
			ArticuloDTO artDTO = ArticuloDAO.listarPorCodigo(id)
					.toArticuloDTO();
			artDTO.setIva(iva.toIvaDTO());
			return artDTO;
		} else {
			return ArticuloDAO.listarPorCodigo(id).toArticuloDTO();
		}
	}

	@Override
	public ArticuloDTO listarPorCodigoSeccion(String codigo) {
		long idIva = ivaDAO.recuperarIvaPorCodigo(codigo);
		System.out.println("1) -->> " + idIva);
		if (idIva > 0) {
			Iva iva = ivaDAO.getById(idIva);
			System.out.println("2) -->> " + iva.getIdIva());
			ArticuloDTO artDTO = ArticuloDAO.listarPorCodigo(codigo)
					.toArticuloSecionDTO();
			artDTO.setIva(iva.toIvaDTO());
			return artDTO;
		} else {
			return ArticuloDAO.listarPorCodigo(codigo).toArticuloSecionDTO();
		}
	}

	@Override
	public List<ArticuloDTO> listarPorDescripcion(String descri) {
		List<ArticuloDTO> RetiroPedidoDTOs = new ArrayList<ArticuloDTO>();
		for (Articulo RetiroPedido : ArticuloDAO.listarPorDescripcion(descri)) {
			RetiroPedidoDTOs.add(RetiroPedido.toArticuloDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public boolean actualizarCantidad(long id, long canti) {
		return ArticuloDAO.actualizarCantidad(id, canti);
	}

}
