/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service.impl;

import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.CajaDAO;
import turneroWeb.core.domain.Caja;
import turneroWeb.core.dto.CajaDTO;
import turneroWeb.core.service.CajaService;

/**
 *
 * @author ADMIN
 */
@Service
@Transactional
public class CajaServiceImpl implements CajaService {

	@Autowired
	CajaDAO cajaDAO;

//	@Autowired
//	PromoTemporadaDAO promoTemporadaDAO;
//
//	@Autowired
//	DescuentoTarjetaCabDAO descCabDAO;

	@Override
	public List<CajaDTO> listar() {
		List<CajaDTO> listaCaja = new ArrayList<CajaDTO>();
		for (Caja c : cajaDAO.listar()) {
			listaCaja.add(c.toCajaDTO());
		}
		return listaCaja;
	}

	@Override
	public CajaDTO getById(long id) {
		// TODO Auto-generated method stub
		return cajaDAO.getById(id).toCajaDTO();
	}

	@Override
	public void insertar(CajaDTO obj) {
		// TODO Auto-generated method stub
		cajaDAO.insertar(Caja.fromCajaDTOAsociado(obj));
	}

	@Override
	public void actualizar(CajaDTO obj) {
		// TODO Auto-generated method stub
		cajaDAO.actualizar(Caja.fromCajaDTOAsociado(obj));
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public CajaDTO buscarCaja(Integer nroCaja, String clave)
			throws NoSuchAlgorithmException {

		Caja cj = cajaDAO.buscarCaja(nroCaja, clave);
		CajaDTO cajaDTO = cj.toCajaDTO();
		if (cajaDTO != null) {
			actualizarDescTarjPorFechaActual();
			actualizarPromoPorFechaActual();
		}
		return cajaDTO;
	}

	public void actualizarDescTarjPorFechaActual() {
		java.util.Date date = new java.util.Date();
		Timestamp tiempoActual = new Timestamp(date.getTime());

//		List<DescuentoTarjetaCab> descuentoCab = descCabDAO
//				.listarPorFechaActual(tiempoActual);
//		int i = 0;
//		for (DescuentoTarjetaCab desc : descuentoCab) {
//			i++;
//			desc.setEstadoDesc(false);
//			desc.setUsuMod("Sistema");
//			desc.setFechaMod(tiempoActual);
//			descCabDAO.actualizarPorFecha(desc, i);
//		}
	}

	public void actualizarPromoPorFechaActual() {
		java.util.Date date = new java.util.Date();
		Timestamp tiempoActual = new Timestamp(date.getTime());

//		List<PromoTemporada> listaPromo = promoTemporadaDAO
//				.listarPorFechaActual(tiempoActual);
//		int i = 0;
//		for (PromoTemporada pro : listaPromo) {
//			i++;
//			pro.setUsuMod("Sistema");
//			pro.setFechaMod(tiempoActual);
//			pro.setEstadoPromo(false);
//			promoTemporadaDAO.actualizarPorFecha(pro, i);
//		}
	}
}
