package turneroWeb.core.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.FuncionarioDAO;
import turneroWeb.core.domain.Funcionario;
import turneroWeb.core.dto.FuncionarioDTO;
import turneroWeb.core.service.FuncionarioService;

@Service
@Transactional
public class FuncionarioServiceImpl implements FuncionarioService {

	@Autowired
	private FuncionarioDAO funcionarioDAO;

	@Override
	public List<FuncionarioDTO> listar() {
		List<FuncionarioDTO> RetiroPedidoDTOs = new ArrayList<FuncionarioDTO>();
		for (Funcionario RetiroPedido : funcionarioDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toFuncionarioDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public FuncionarioDTO getById(long id) {
		return funcionarioDAO.getById(id).toFuncionarioDTO();
	}

	@Override
	public void insertar(FuncionarioDTO obj) {
		funcionarioDAO.insertar(Funcionario.fromFuncionarioDTO(obj));
	}

	@Override
	public void actualizar(FuncionarioDTO obj) {
		funcionarioDAO.actualizar(Funcionario.fromFuncionarioDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		funcionarioDAO.eliminar(id);
	}

	@Override
	public List<FuncionarioDTO> filtrarNomApeCi(String nombre, String apellido,
			String ci) {
		List<FuncionarioDTO> RetiroPedidoDTOs = new ArrayList<FuncionarioDTO>();
		for (Funcionario RetiroPedido : funcionarioDAO.filtrarNomApeCi(nombre,
				apellido, ci)) {
			RetiroPedidoDTOs.add(RetiroPedido.toFuncionarioDTO());
		}
		return RetiroPedidoDTOs;
	}

}
