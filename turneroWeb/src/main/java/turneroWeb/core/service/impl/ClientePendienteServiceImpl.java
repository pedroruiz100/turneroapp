package turneroWeb.core.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import turneroWeb.core.dao.ClientePendienteDAO;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.ClientePendiente;
import turneroWeb.core.dto.ClientePendienteDTO;
import turneroWeb.core.service.ClientePendienteService;

@Service
@Transactional
public class ClientePendienteServiceImpl implements ClientePendienteService {

	@Autowired
	private ClientePendienteDAO cliPenDAO;

	@Override
	public List<ClientePendienteDTO> listar() {
		List<ClientePendienteDTO> RetiroPedidoDTOs = new ArrayList<ClientePendienteDTO>();
		for (ClientePendiente RetiroPedido : cliPenDAO.listar()) {
			RetiroPedidoDTOs.add(RetiroPedido.toClientePendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public ClientePendienteDTO getById(long id) {
		return cliPenDAO.getById(id).toClientePendienteDTO();
	}

	@Override
	public void insertar(ClientePendienteDTO obj) {
		cliPenDAO.insertar(ClientePendiente.fromClientePendienteDTO(obj));
	}

	@Override
	public void actualizar(ClientePendienteDTO obj) {
		cliPenDAO.actualizar(ClientePendiente.fromClientePendienteDTO(obj));
	}

	@Override
	public void eliminar(long id) {
		cliPenDAO.eliminar(id);
	}

	@Override
	public ClientePendienteDTO insertarObtenerObj(
			ClientePendienteDTO fromClientePendiente) {
		ClientePendiente cliPen = cliPenDAO.insertarObtenerObj(ClientePendiente
				.fromClientePendienteDTO(fromClientePendiente));
		return cliPen.toClientePendienteDTO();
	}

	@Override
	public List<ClientePendienteDTO> listarPendiente() {
		List<ClientePendienteDTO> RetiroPedidoDTOs = new ArrayList<ClientePendienteDTO>();
		for (ClientePendiente RetiroPedido : cliPenDAO.listarPendiente()) {
			RetiroPedidoDTOs.add(RetiroPedido.toClientePendienteDTO());
		}
		return RetiroPedidoDTOs;
	}

	@Override
	public boolean actualizarEstado(long idCliPen, Timestamp timestamp,
			String nombreCaj) {
		return cliPenDAO.actualizarEstado(idCliPen, timestamp, nombreCaj);
	}

	@Override
	public ClientePendienteDTO listarPorCi(String ci) {
		// char[] xyz = ci.toCharArray();
		// String dataString = "";
		// boolean val = false;
		// for (int i = 0; i < ci.length(); i++) {
		// if (Character.isDigit(xyz[i])) {
		// val = false;
		// } else if (Character.isLetter(xyz[i])) {
		// val = false;
		// } else {
		// val = true;
		// String[] data = ci.split(xyz[i] + "");
		// dataString = data[0];
		// break;
		// }
		// }
		// if (val) {
		// return cliPenDAO.listarPorCi(dataString).toClientePendienteDTO();
		// } else {
		return cliPenDAO.listarPorCi(ci).toClientePendienteDTO();
		// }
	}

	@Override
	public boolean actualizarEstadoNuevo(long idCliPen, boolean estado) {
		// TODO Auto-generated method stub
		// System.out.println("INGRESAMOS -> " + idCliPen + " - " + estado);
		ClientePendiente clipen = cliPenDAO.getById(idCliPen);
		// System.out.println("HA UPEI -> " + clipen.getIdClientePendiente());
		clipen.setProcesado(estado);
		// System.out.println("PASAMOS " + clipen.getProcesado());
		return cliPenDAO.actualizarEstadoNuevo(idCliPen, clipen);
	}

	@Override
	public ClientePendienteDTO insertarIdTRUE(long id) {
		ClientePendiente clipen = new ClientePendiente();
		clipen.setProcesado(false);
		clipen.setUsuAlta("SISTEMA ADMIN");
		clipen.setUsuMod("SISTEMA ADMIN");
		clipen.setFechaAlta(new Timestamp(System.currentTimeMillis()));
		clipen.setFechaMod(new Timestamp(System.currentTimeMillis()));

		Cliente cli = new Cliente();
		cli.setIdCliente(id);

		clipen.setCliente(cli);
		try {
			return cliPenDAO.insertarObj(clipen).toClientePendienteDTO();
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public void actualizarEstado(long idCP) {
		cliPenDAO.actualizarEstado(idCP);
	}

}
