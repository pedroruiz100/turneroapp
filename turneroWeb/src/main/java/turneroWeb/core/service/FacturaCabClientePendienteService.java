package turneroWeb.core.service;

import turneroWeb.core.dto.FacturaCabClientePendienteDTO;

public interface FacturaCabClientePendienteService extends
		CRUDGenericService<FacturaCabClientePendienteDTO> {

	FacturaCabClientePendienteDTO insertarObtenerObj(
			FacturaCabClientePendienteDTO fromFacturaCabClientePendienteAsociado);

}
