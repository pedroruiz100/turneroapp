package turneroWeb.core.service;

import turneroWeb.core.dto.TalonariosSucursaleDTO;

public interface TalonariosSucursaleService extends
		CRUDGenericService<TalonariosSucursaleDTO> {

	TalonariosSucursaleDTO listarPorSucursal(long id, long idTimbrado);

}
