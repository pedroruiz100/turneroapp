package turneroWeb.core.service;

import turneroWeb.core.dto.SucursalDTO;

public interface SucursalService extends CRUDGenericService<SucursalDTO> {

}
