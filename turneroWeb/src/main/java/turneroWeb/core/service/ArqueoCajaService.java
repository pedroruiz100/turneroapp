package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.ArqueoCajaDTO;

public interface ArqueoCajaService extends CRUDGenericService<ArqueoCajaDTO> {

	ArqueoCajaDTO insertarObtenerObj(ArqueoCajaDTO ar);

	int recuperarZeta(long idCaja, String nro);

	boolean insertarObtenerEstado(ArqueoCajaDTO fromArqueoCajaDTO);

	long recuperarNumMaxZeta();

	public String recuperarPorCajaFecha(long idCaja, String toString);

	public ArqueoCajaDTO recuperarPorCajaFechaObj(long id, String fecha);

	public List<ArqueoCajaDTO> filtroFecha(String fechaInicio, String fechaFin);

	public boolean verificarArqueoDia();

	public long getCantidadArqueoDia();
	
}
