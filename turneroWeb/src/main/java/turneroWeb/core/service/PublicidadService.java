/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.PublicidadDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface PublicidadService extends CRUDGenericService<PublicidadDTO> {

	void actualizarPorId(long id);

	void bajaPorId(long id);

	List<PublicidadDTO> listarPublicidadTRUE();

	boolean aperturaCaja();

}
