/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import turneroWeb.core.dto.UsuarioDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface UsuarioService extends CRUDGenericService<UsuarioDTO> {
	
}
