package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.FacturaClienteDetDTO;

public interface FacturaClienteDetService extends CRUDGenericService<FacturaClienteDetDTO> {

	void insercionMasiva(List<FacturaClienteDetDTO> facDTO);

	List<FacturaClienteDetDTO> filtroFechaDescuentoFact(String fechaDesde,
			String fechaHasta, String nroCaja, String nroFact);

	FacturaClienteDetDTO listarPorId(long id);

	List<FacturaClienteDetDTO> listFacturaCab(long id);

}
