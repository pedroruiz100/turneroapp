/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.security.NoSuchAlgorithmException;

import turneroWeb.core.dto.CajaDTO;

/**
 *
 * @author ADMIN
 */
public interface CajaService extends CRUDGenericService<CajaDTO> {

	CajaDTO buscarCaja(Integer nroCaja, String clave)
			throws NoSuchAlgorithmException;

}
