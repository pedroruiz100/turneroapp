package turneroWeb.core.service;

import turneroWeb.core.dto.TimbradoDTO;

public interface TimbradoService extends CRUDGenericService<TimbradoDTO> {

}
