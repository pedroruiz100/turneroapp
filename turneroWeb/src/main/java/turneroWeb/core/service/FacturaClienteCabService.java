package turneroWeb.core.service;

import java.util.List;

import org.json.simple.JSONObject;

import turneroWeb.core.dto.FacturaClienteCabDTO;

public interface FacturaClienteCabService extends CRUDGenericService<FacturaClienteCabDTO> {

	FacturaClienteCabDTO insertarObtenerObjeto(FacturaClienteCabDTO facturaClienteCabDTO);

	void cancelarFactura(long id, String usuMod);

	void actualizarMontoVenta(int monto, long id);

	FacturaClienteCabDTO actualizarObtenerObjeto(FacturaClienteCabDTO facturaClienteCabDTO);

	FacturaClienteCabDTO consultaHistorico(String nroFactura, String tipoComprobante, String fechaEmision,
			String nroCaja, String timbrado);

	List<FacturaClienteCabDTO> filtroFecha(String fechaDesde, String fechaHasta, String nroCaja);

	List<FacturaClienteCabDTO> filtroFechaDescuento(String fechaDesde,
			String fechaHasta, String nroCaja);

	List<FacturaClienteCabDTO> filtroNroFact(String nroFactura, String nroCaja);

	List<FacturaClienteCabDTO> filtroFechaDescuentoFact(String fechaDesde,
			String fechaHasta, String nroCaja, String nroFact);

	List<FacturaClienteCabDTO> filtroFechaDesc(String fechaDesde,
			String fechaHasta, String nroCaja);

	List<JSONObject> recuperarLaguna(String fechaInicio,
			String fechaFin, String nroCaja);

	void actualizarNativo(long idFCC, int monto);

}
