package turneroWeb.core.service;

import turneroWeb.core.dto.AperturaCajaDTO;

public interface AperturaCajaService extends
		CRUDGenericService<AperturaCajaDTO> {
	
	AperturaCajaDTO insertarObtenerObj(AperturaCajaDTO aperturaCaja);

    String recuperarPorCajaFecha(long idCaja, String fecha);

    public AperturaCajaDTO recuperarPorCaja(Long idCaja, int x);

    public boolean insertarRecuperarEstado(AperturaCajaDTO ac);

    public boolean verificarAperturaDia();

    public long getCantidadAperturaDia();

}
