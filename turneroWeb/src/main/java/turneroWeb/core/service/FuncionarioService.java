package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.FuncionarioDTO;

public interface FuncionarioService extends CRUDGenericService<FuncionarioDTO> {

	List<FuncionarioDTO> filtrarNomApeCi(String nombre, String apellido,
			String ci);

}
