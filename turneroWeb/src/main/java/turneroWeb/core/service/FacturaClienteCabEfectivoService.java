package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.FacturaClienteCabEfectivoDTO;

public interface FacturaClienteCabEfectivoService extends CRUDGenericService<FacturaClienteCabEfectivoDTO> {

	FacturaClienteCabEfectivoDTO insertarObtenerObjeto(
			FacturaClienteCabEfectivoDTO facturaClienteCabEfectivo);

    public List<FacturaClienteCabEfectivoDTO> listarPorFactura(long parseLong);

}
