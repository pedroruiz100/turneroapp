package turneroWeb.core.service;

import java.sql.Timestamp;
import java.util.List;

import turneroWeb.core.dto.ClientePendienteDTO;

public interface ClientePendienteService extends
		CRUDGenericService<ClientePendienteDTO> {
	ClientePendienteDTO insertarObtenerObj(
			ClientePendienteDTO fromClientePendiente);

	List<ClientePendienteDTO> listarPendiente();

	public boolean actualizarEstado(long idCliPen, Timestamp timestamp,
			String nombreCaj);

	ClientePendienteDTO listarPorCi(String ci);

	boolean actualizarEstadoNuevo(long idCliPen, boolean estado);

	ClientePendienteDTO insertarIdTRUE(long id);

	void actualizarEstado(long idCP);
}
