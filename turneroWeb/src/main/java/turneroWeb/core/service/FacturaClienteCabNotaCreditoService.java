package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.FacturaClienteCabNotaCreditoDTO;

public interface FacturaClienteCabNotaCreditoService extends CRUDGenericService<FacturaClienteCabNotaCreditoDTO> {

	FacturaClienteCabNotaCreditoDTO insertarObtenerObjeto(
			FacturaClienteCabNotaCreditoDTO fac);

	public List<FacturaClienteCabNotaCreditoDTO> listarPorFactura(long parseLong);
	
}
