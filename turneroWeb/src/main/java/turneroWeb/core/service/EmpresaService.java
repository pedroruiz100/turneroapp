/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import turneroWeb.core.dto.EmpresaDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface EmpresaService extends
 CRUDGenericService<EmpresaDTO> {
}
