package turneroWeb.core.service;

import turneroWeb.core.dto.ModoVentaDTO;

public interface ModoVentaService extends CRUDGenericService<ModoVentaDTO> {

	Boolean sinLogin();
	
}
