/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.SeccionDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface SeccionService extends CRUDGenericService<SeccionDTO> {

	List<SeccionDTO> fullActivos();
}
