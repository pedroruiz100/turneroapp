package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.FacturaClienteCabTarjetaDTO;

public interface FacturaClienteCabTarjetaService extends
		CRUDGenericService<FacturaClienteCabTarjetaDTO> {

	FacturaClienteCabTarjetaDTO insercionMasiva(
			FacturaClienteCabTarjetaDTO fac, int id);

	FacturaClienteCabTarjetaDTO insertarObtenerObj(
			FacturaClienteCabTarjetaDTO facturaClienteCabTarjeta);

	public List<FacturaClienteCabTarjetaDTO> listarPorFactura(long parseLong);

	public List<FacturaClienteCabTarjetaDTO> filtroFechaTarjeta(
			String fechaInicio, String fechaFin, String selectedItem);

}
