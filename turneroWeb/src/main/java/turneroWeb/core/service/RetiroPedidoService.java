/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.service;

import java.util.List;

import turneroWeb.core.dto.RetiroPedidoDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface RetiroPedidoService extends
		CRUDGenericService<RetiroPedidoDTO> {

	Long rowCount();

	List<RetiroPedidoDTO> listarNoFETCH(long limRow, long offSet);

	List<RetiroPedidoDTO> listarFETCH(long limRow, long offSet);

	List<RetiroPedidoDTO> listarPreparacion();

	List<RetiroPedidoDTO> listarListo();

	void actualizarPreparado(long id);

	void actualizarListo(long id);

	List<RetiroPedidoDTO> listarFullPreparacion();

	List<RetiroPedidoDTO> listarFullListo();

	void actualizarEntregado(long id);

	RetiroPedidoDTO buscarPorNumeroPedido(String num);
}
