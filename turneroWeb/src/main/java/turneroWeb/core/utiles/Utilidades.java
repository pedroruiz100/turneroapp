package turneroWeb.core.utiles;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.codec.binary.Base64;

public class Utilidades {

	// public static void main(String[] args) {
	private static SecureRandom random = new SecureRandom();
	public static String ipArduino = "192.168.0.19";
	public static int port = 80;
	public final static int time = 4;
	public final static int idImpresora = 1;

	/**
	 * different dictionaries used
	 */
	private static final String ALPHA_CAPS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String ALPHA = "abcdefghijklmnopqrstuvwxyz";
	private static final String NUMERIC = "0123456789";
	private static final String SPECIAL_CHARS = "!@#$%^&*_=+-/";
	public static String reportePath = "C:\\tools\\jboss\\EAP-6.4.0\\bin\\rpt";

	/**
	 * Method will generate random string based on the parameters
	 *
	 * @param len
	 *            the length of the random string
	 * @param dic
	 *            the dictionary used to generate the password
	 * @return the random password
	 */

	int datosParaEncriptar[] = { 9, 8, 7, 6, 5, 4, 3, 2, 1 };

	public static java.sql.Date stringToSqlDate(String fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date utilDate = new java.util.Date();
		java.sql.Date sqlDate = null;
		try {
			utilDate = sdf.parse(fecha);
			sqlDate = new java.sql.Date(utilDate.getTime());
		} catch (ParseException ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return sqlDate;
	}

	public static java.util.Date stringToUtilDate(String fecha) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date utilDate = new java.util.Date();
		try {
			utilDate = sdf.parse(fecha);
		} catch (ParseException ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
		}
		return utilDate;
	}

	public static void enviarPorCorreo(String email, String asunto,
			String cuerpo) {
		String USER_NAME = "pedroruizd2017@gmail.com"; // GMail user name (just
		// the part before
		// "@gmail.com")
		String PASSWORD = "fernandez10"; // GMail password

		String RECIPIENT = email;

		String from = USER_NAME;
		String pass = PASSWORD;
		String[] to = { RECIPIENT }; // list of recipient email addresses
		String subject = asunto;
		String body = cuerpo;
		// "Ya se ha registrado en nuestra Base de Datos. Puedes acceder a
		// DomoAdmin\\n\\n"
		// + "Usuario: "
		// + usu.getUsuarioUsuario()
		// + "\\n"
		// + "Nombre Usuario: "
		// + usu.getPersona().getNombrePersona()
		// + " " + usu.getPersona().getApellidoPersona() + "\\n";

		sendFromGMail(from, pass, to, subject, body);
	}

	private static void sendFromGMail(String from, String pass, String[] to,
			String subject, String body) {
		Properties props = System.getProperties();
		String host = "smtp.gmail.com";

		props.put("mail.smtp.starttls.enable", "true");

		props.put("mail.smtp.ssl.trust", host);
		props.put("mail.smtp.user", from);
		props.put("mail.smtp.password", pass);
		props.put("mail.smtp.port", "587");
		props.put("mail.smtp.auth", "true");

		Session session = Session.getDefaultInstance(props);
		MimeMessage message = new MimeMessage(session);

		try {

			message.setFrom(new InternetAddress(from));
			InternetAddress[] toAddress = new InternetAddress[to.length];

			// To get the array of addresses
			for (int i = 0; i < to.length; i++) {
				toAddress[i] = new InternetAddress(to[i]);
			}

			for (int i = 0; i < toAddress.length; i++) {
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}

			message.setSubject(subject);
			message.setText(body);

			Transport transport = session.getTransport("smtp");

			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();

		} catch (AddressException ae) {
			ae.printStackTrace();
		} catch (MessagingException me) {
			me.printStackTrace();
		}
	}

	public static String generatePassword(int len, String dic) {
		String result = "";
		for (int i = 0; i < len; i++) {
			int index = random.nextInt(dic.length());
			result += dic.charAt(index);
		}
		return result;
	}

	public static int genNumAleatorio(String ruc) {
		// R. El diccionario de la RAE contiene 88.000 palabras.
		// El de americanismos 70.000; pero en este �ltimo aparecen muchas
		// variantes que
		// en el diccionario acad�mico ocupar�an una sola entrada,
		// como guaira, huaira, huayra, waira, wayra, guayra.
		// Se suele estimar el l�xico de una lengua a�adiendo un 30% al de los
		// diccionarios.27 nov. 2010

		// 2021741105 m�x. int BD Paran�
		// 2147483647 m�x. int
		// tipo de valores aceptados -> n�meros, letras y guiones medios
		// no se agrega filtro... se supone que se validar� antes de llamar al
		// m�todo
		int codLength = ruc.length();
		int codGen = codLength;
		// m�x. 83640, ideal...
		for (int i = 0; i < codLength; i++) {
			if ((codGen + (int) ruc.charAt(i)) < 83647) {
				if (codLength > 1) {
					if (i == (codLength - 1)) {
						codGen = codGen
								+ ((int) ruc.charAt(i - 1) - (int) ruc
										.charAt(i));
					} else if (i == 0) {
						codGen = codGen
								+ ((int) ruc.charAt(i + 1) - (int) ruc
										.charAt(i));
					} else {
						if (i % 2 == 0) {
							codGen = codGen
									+ (((int) ruc.charAt(i - 1) + (int) ruc
											.charAt(i + 1)) - (int) ruc
											.charAt(i));
						} else {
							codGen = codGen
									+ (((int) ruc.charAt(i - 1) - (int) ruc
											.charAt(i + 1)) + (int) ruc
											.charAt(i));
						}
					}
					if (i % 2 == 0) {
						codGen = codGen + i;
					} else {
						codGen = codGen - i;
					}
				} else {
					codGen = codGen + (int) ruc.charAt(i) + i;
				}
			}
		}
		codGen = codGen + 2147400000;
		return codGen;
	}

	public static String generarClave() {
		int len = 10;
		return generatePassword(len, ALPHA_CAPS + ALPHA);
	}

	public static boolean pingHost(String host, int port, int timeout) {
		try (Socket socket = new Socket()) {
			socket.connect(new InetSocketAddress(host, port), timeout);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public static String ordenandoFechaString(String fecha) {
		String[] fechaSplit = fecha.split("-");
		if (fechaSplit[0].length() == 4) {
			return fechaSplit[2] + "-" + fechaSplit[1] + "-" + fechaSplit[0];
		} else {
			return fecha;
		}
	}

	public static String msjIda(String msj) {
		return msjDecIda(msj);
	}

	private static String msjDecIda(String msj) {
		byte[] byteArray = Base64.encodeBase64(msj.getBytes());
		String encodedString = new String(byteArray);
		return encodedString;
	}

	// ENCRIPTAR ************************************************ -> -> -> -> ->

	// DESENCRIPTAR ********************************************* -> -> -> -> ->
	public static String msjVuelta(String msj) {
		return msjDecVuelta(msj);
	}

	private static String msjDecVuelta(String msj) {
		byte[] var = Base64.decodeBase64(msj.getBytes());
		String st = "";
		try {
			st = new String(var, "ISO-8859-1");
		} catch (UnsupportedEncodingException ex) {
			System.out.println("ERROR Exception: " + ex.fillInStackTrace());
		}
		return st;
	}

	// public static boolean iniciarConsumoEspecifico() {
	// String urlDato = "http://192.168.0.30/LED=T";
	// String inputLine = "";
	// JSONParser parser = new JSONParser();
	// boolean valor = false;
	// JSONObject dato = new JSONObject();
	// // if (Utilidades.pingHost("192.168.0.30", Utilidades.port,
	// Utilidades.time)) {
	// try {
	// URL url = new URL(urlDato);
	// URLConnection uc = url.openConnection();
	// BufferedReader br = new BufferedReader(new
	// InputStreamReader(uc.getInputStream(), "UTF-8"));
	// while ((inputLine = br.readLine()) != null) {
	// dato = (JSONObject) parser.parse(inputLine);
	// }
	// if (dato.containsKey("ESTADO")) {
	// if (dato.get("ESTADO").toString().equalsIgnoreCase("ON")) {
	// valor = true;
	// }
	// }
	// br.close();
	// } catch (IOException ex) {
	// System.out.println("-->> " + ex.getLocalizedMessage());
	// valor = false;
	// } catch (org.json.simple.parser.ParseException e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// valor = false;
	// }
	// // }
	// return valor;
	// }

	// public static String stopConsumoEspecifico() {
	// String urlDato = "http://192.168.0.30/LED=T";
	// String inputLine = "";
	// JSONParser parser = new JSONParser();
	// String valor = "0";
	// JSONObject dato = new JSONObject();
	// // if (Utilidades.pingHost("192.168.0.30", Utilidades.port,
	// Utilidades.time)) {
	// try {
	// URL url = new URL(urlDato);
	// URLConnection uc = url.openConnection();
	// BufferedReader br = new BufferedReader(new
	// InputStreamReader(uc.getInputStream(), "UTF-8"));
	// while ((inputLine = br.readLine()) != null) {
	// dato = (JSONObject) parser.parse(inputLine);
	// }
	// if (dato.containsKey("ESTADO")) {
	// if (dato.get("ESTADO").toString().equalsIgnoreCase("0")) {
	// valor = true;
	// }
	// }
	// br.close();
	// } catch (IOException ex) {
	// System.out.println("-->> " + ex.getLocalizedMessage());
	// valor = false;
	// } catch (org.json.simple.parser.ParseException e) {
	// System.out.println("-->> " + e.getLocalizedMessage());
	// valor = false;
	// }
	// // }
	// return valor;
	// }

	public String getHash(String message) {
		return getH(message);
	}

	private String getH(String message) {
		StringBuilder sb = new StringBuilder();
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(message.getBytes());
			byte byteData[] = md.digest();
			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16)
						.substring(1));
			}
			System.out.println("MY CODE -->> " + sb.toString());
			return sb.toString();
		} catch (NoSuchAlgorithmException ex) {
			System.out.println("ERROR Exception: " + ex.fillInStackTrace());
			return "";
		}
	}

	public static String calculoSET(String p_numero) {
		int p_basemax = 10;
		// public static String calculoSET(String p_numero, int p_basemax) {
		int v_total, v_resto, k, v_numero_aux, v_digit;
		String v_numero_al = "";

		for (int i = 0; i < p_numero.length(); i++) {
			char c = p_numero.charAt(i);
			if (Character.isDigit(c)) {
				v_numero_al += c;
			} else {
				v_numero_al += (int) c;
			}
		}

		k = 2;
		v_total = 0;

		for (int i = v_numero_al.length() - 1; i >= 0; i--) {
			k = k > p_basemax ? 2 : k;
			v_numero_aux = v_numero_al.charAt(i) - 48;
			v_total += v_numero_aux * k++;
		}

		v_resto = v_total % 11;
		v_digit = v_resto > 1 ? 11 - v_resto : 0;
		return p_numero + "-" + v_digit;
	}
}
