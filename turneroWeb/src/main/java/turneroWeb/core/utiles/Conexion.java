package turneroWeb.core.utiles;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Conexion {

	static Connection con;
	static Statement st;

	public static boolean conectar() {
		boolean valor = false;
		try {
			Class.forName("org.postgresql.Driver");
			String url = "jdbc:postgresql://localhost:5432/food_proyecto";
			con = DriverManager.getConnection(url, "postgres", "1");
			st = con.createStatement();
			con.setAutoCommit(false);
			st.setFetchSize(100);
			valor = true;
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null,
					ex);
		} catch (SQLException ex) {
			Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		return valor;
	}

	public static boolean cerrar() {
		boolean valor = false;
		try {
			st.close();
			con.close();
			valor = true;
		} catch (SQLException ex) {
			Logger.getLogger(Conexion.class.getName()).log(Level.SEVERE, null,
					ex);
		}
		return valor;
	}

	public static Statement getSt() {
		return st;
	}

	public static Connection getCon() {
		return con;
	}

}
