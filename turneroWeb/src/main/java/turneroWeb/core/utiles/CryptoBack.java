package turneroWeb.core.utiles;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class CryptoBack {

	public String getHash(String message) throws NoSuchAlgorithmException {
		return getH(message);
	}

	private String getH(String message) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(message.getBytes());
		byte byteData[] = md.digest();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}
		return sb.toString();
	}
}
