package turneroWeb.core.dto;

import java.io.Serializable;

public class FacturaClienteCabNotaCreditoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long idFacturaClienteCabNotaCredito;
    private FacturaClienteCabDTO facturaClienteCab;
    private String nroNota;
    private Integer monto;

    public FacturaClienteCabNotaCreditoDTO() {
        super();
    }

    public Long getIdFacturaClienteCabNotaCredito() {
        return idFacturaClienteCabNotaCredito;
    }

    public void setIdFacturaClienteCabNotaCredito(
            Long idFacturaClienteCabNotaCredito) {
        this.idFacturaClienteCabNotaCredito = idFacturaClienteCabNotaCredito;
    }

    public FacturaClienteCabDTO getFacturaClienteCab() {
        return facturaClienteCab;
    }

    public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
        this.facturaClienteCab = facturaClienteCab;
    }

    public String getNroNota() {
        return nroNota;
    }

    public void setNroNota(String nroNota) {
        this.nroNota = nroNota;
    }

    public Integer getMonto() {
        return monto;
    }

    public void setMonto(Integer monto) {
        this.monto = monto;
    }

}
