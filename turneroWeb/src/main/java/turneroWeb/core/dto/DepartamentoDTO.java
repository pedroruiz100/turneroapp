package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class DepartamentoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idDepartamento;

	private Boolean activo;

	private String descripcion;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private String usuAlta;

	private String usuMod;

	private PaisDTO paisDTO;

	public DepartamentoDTO() {
		super();
	}

	public Long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public PaisDTO getPaisDTO() {
		return paisDTO;
	}

	public void setPaisDTO(PaisDTO paisDTO) {
		this.paisDTO = paisDTO;
	}

}