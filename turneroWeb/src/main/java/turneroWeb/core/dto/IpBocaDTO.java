package turneroWeb.core.dto;

import java.io.Serializable;

public class IpBocaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idIpBoca;

	private String ipBoca;

	public IpBocaDTO() {
		super();
	}

	public Long getIdIpBoca() {
		return idIpBoca;
	}

	public void setIdIpBoca(Long idIpBoca) {
		this.idIpBoca = idIpBoca;
	}

	public String getIpBoca() {
		return ipBoca;
	}

	public void setIpBoca(String ipBoca) {
		this.ipBoca = ipBoca;
	}

}