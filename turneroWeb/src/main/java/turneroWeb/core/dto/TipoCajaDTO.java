package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class TipoCajaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idTipoCaja;

	private String descripcion;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private String usuAlta;

	private String usuMod;

	public TipoCajaDTO() {
	}

	public Long getIdTipoCaja() {
		return idTipoCaja;
	}

	public void setIdTipoCaja(Long idTipoCaja) {
		this.idTipoCaja = idTipoCaja;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}