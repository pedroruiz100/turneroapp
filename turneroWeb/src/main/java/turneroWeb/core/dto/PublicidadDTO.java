package turneroWeb.core.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PublicidadDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idPublicidad;

	private String link;

	private String descripcion;

	private boolean estado;

	public PublicidadDTO() {
	}

	public Long getIdPublicidad() {
		return idPublicidad;
	}

	public void setIdPublicidad(Long idPublicidad) {
		this.idPublicidad = idPublicidad;
	}

	public String getLink() {
		return link;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}