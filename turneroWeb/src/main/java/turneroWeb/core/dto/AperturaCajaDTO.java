package turneroWeb.core.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

public class AperturaCajaDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idApertura;
	private Integer diferenciaApertura;
	private Timestamp fechaApertura;
	private UsuarioDTO usuarioCajero;
	private BigDecimal montoApertura;
	private Integer montoCaja;
	private CajaDTO caja;

	public AperturaCajaDTO() {
	}

	public Long getIdApertura() {
		return this.idApertura;
	}

	public void setIdApertura(Long idApertura) {
		this.idApertura = idApertura;
	}

	public Integer getDiferenciaApertura() {
		return this.diferenciaApertura;
	}

	public void setDiferenciaApertura(Integer diferenciaApertura) {
		this.diferenciaApertura = diferenciaApertura;
	}

	public Timestamp getFechaApertura() {
		return this.fechaApertura;
	}

	public void setFechaApertura(Timestamp fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public BigDecimal getMontoApertura() {
		return this.montoApertura;
	}

	public void setMontoApertura(BigDecimal montoApertura) {
		this.montoApertura = montoApertura;
	}

	public Integer getMontoCaja() {
		return this.montoCaja;
	}

	public void setMontoCaja(Integer montoCaja) {
		this.montoCaja = montoCaja;
	}

	public CajaDTO getCaja() {
		return this.caja;
	}

	public void setCaja(CajaDTO caja) {
		this.caja = caja;
	}

	public UsuarioDTO getUsuarioCajero() {
		return usuarioCajero;
	}

	public void setUsuarioCajero(UsuarioDTO usuarioCajero) {
		this.usuarioCajero = usuarioCajero;
	}
}
