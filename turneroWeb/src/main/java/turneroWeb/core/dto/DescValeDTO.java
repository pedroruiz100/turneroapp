package turneroWeb.core.dto;

import java.io.Serializable;

public class DescValeDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idDescVale;

	private ValeDTO vale;

	private Integer montoDescVale;

	private FacturaClienteCabDTO facturaClienteCab;

	public DescValeDTO() {
	}

	public Long getIdDescVale() {
		return this.idDescVale;
	}

	public void setIdDescVale(Long idDescVale) {
		this.idDescVale = idDescVale;
	}

	public Integer getMontoDescVale() {
		return this.montoDescVale;
	}

	public void setMontoDescVale(Integer montoDescVale) {
		this.montoDescVale = montoDescVale;
	}

	public FacturaClienteCabDTO getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public ValeDTO getVale() {
		return vale;
	}

	public void setVale(ValeDTO vale) {
		this.vale = vale;
	}

}
