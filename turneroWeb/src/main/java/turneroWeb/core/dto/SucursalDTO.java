package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class SucursalDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idSucursal;

	private String callePrincipal;

	private String codSucursal;

	private String descripcion;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private Integer nroLocal;

	private String primeraLateral;

	private String segundaLateral;

	private String usuAlta;

	private String usuMod;

	private BarrioDTO barrioDTO;

	private CiudadDTO ciudadDTO;

	private DepartamentoDTO departamentoDTO;

	private EmpresaDTO empresaDTO;

	private String telefono;

	public SucursalDTO() {
	}

	public Long getIdSucursal() {
		return idSucursal;
	}

	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getCallePrincipal() {
		return callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public String getCodSucursal() {
		return codSucursal;
	}

	public void setCodSucursal(String codSucursal) {
		this.codSucursal = codSucursal;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Integer getNroLocal() {
		return nroLocal;
	}

	public void setNroLocal(Integer nroLocal) {
		this.nroLocal = nroLocal;
	}

	public String getPrimeraLateral() {
		return primeraLateral;
	}

	public void setPrimeraLateral(String primeraLateral) {
		this.primeraLateral = primeraLateral;
	}

	public String getSegundaLateral() {
		return segundaLateral;
	}

	public void setSegundaLateral(String segundaLateral) {
		this.segundaLateral = segundaLateral;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public BarrioDTO getBarrioDTO() {
		return barrioDTO;
	}

	public void setBarrioDTO(BarrioDTO barrioDTO) {
		this.barrioDTO = barrioDTO;
	}

	public CiudadDTO getCiudadDTO() {
		return ciudadDTO;
	}

	public void setCiudadDTO(CiudadDTO ciudadDTO) {
		this.ciudadDTO = ciudadDTO;
	}

	public DepartamentoDTO getDepartamentoDTO() {
		return departamentoDTO;
	}

	public void setDepartamentoDTO(DepartamentoDTO departamentoDTO) {
		this.departamentoDTO = departamentoDTO;
	}

	public EmpresaDTO getEmpresaDTO() {
		return empresaDTO;
	}

	public void setEmpresaDTO(EmpresaDTO empresaDTO) {
		this.empresaDTO = empresaDTO;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}