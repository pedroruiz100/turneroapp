package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class FacturaClienteCabDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idFacturaClienteCab;

	private Boolean cancelado;

	private Timestamp fechaEmision;

	private Timestamp fechaMod;

	private String usuAlta;

	private String usuMod;

	private CajaDTO caja;

	private ClienteDTO cliente;

	private SucursalDTO sucursal;

	private String observacion;

	private Integer montoFactura;

	private String nroFactura;

	private EstadoFacturaDTO estadoFactura;

	private TipoComprobanteDTO tipoComprobante;

	private TipoMonedaDTO tipoMoneda;

	private String nroActual;

	public FacturaClienteCabDTO() {
	}

	public Long getIdFacturaClienteCab() {
		return idFacturaClienteCab;
	}

	public void setIdFacturaClienteCab(Long idFacturaClienteCab) {
		this.idFacturaClienteCab = idFacturaClienteCab;
	}

	public Boolean getCancelado() {
		return cancelado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public void setCancelado(Boolean cancelado) {
		this.cancelado = cancelado;
	}

	public Timestamp getFechaEmision() {
		return fechaEmision;
	}

	public void setFechaEmision(Timestamp fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public CajaDTO getCaja() {
		return caja;
	}

	public void setCaja(CajaDTO caja) {
		this.caja = caja;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	public Integer getMontoFactura() {
		return montoFactura;
	}

	public void setMontoFactura(Integer montoFactura) {
		this.montoFactura = montoFactura;
	}

	public String getNroFactura() {
		return nroFactura;
	}

	public void setNroFactura(String nroFactura) {
		this.nroFactura = nroFactura;
	}

	public EstadoFacturaDTO getEstadoFactura() {
		return estadoFactura;
	}

	public void setEstadoFactura(EstadoFacturaDTO estadoFactura) {
		this.estadoFactura = estadoFactura;
	}

	public TipoComprobanteDTO getTipoComprobante() {
		return tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobanteDTO tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public TipoMonedaDTO getTipoMoneda() {
		return tipoMoneda;
	}

	public void setTipoMoneda(TipoMonedaDTO tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	public String getNroActual() {
		return nroActual;
	}

	public void setNroActual(String nroActual) {
		this.nroActual = nroActual;
	}

}