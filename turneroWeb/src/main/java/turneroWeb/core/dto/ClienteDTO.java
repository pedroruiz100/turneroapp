package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

/**
 * The persistent class for the cliente database table.
 * 
 */
public class ClienteDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCliente;

	private String callePrincipal;

	private Integer codCliente;

	private Timestamp compraIniFecha;

	private Timestamp compraUltFecha;

	private String email;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private BarrioDTO barrio;

	private CiudadDTO ciudad;

	private DepartamentoDTO departamento;

	private PaisDTO pais;

	private String nombre;

	private String apellido;

	private Integer nroLocal;

	private String primeraLateral;

	private String ruc;

	private String segundaLateral;

	private String telefono;

	private String telefono2;

	private String usuAlta;

	private String usuMod;

	private Date fecNac;

	public ClienteDTO() {
	}

	public Long getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getCallePrincipal() {
		return callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	public Integer getCodCliente() {
		return codCliente;
	}

	public void setCodCliente(Integer codCliente) {
		this.codCliente = codCliente;
	}

	public Timestamp getCompraIniFecha() {
		return compraIniFecha;
	}

	public void setCompraIniFecha(Timestamp compraIniFecha) {
		this.compraIniFecha = compraIniFecha;
	}

	public Timestamp getCompraUltFecha() {
		return compraUltFecha;
	}

	public void setCompraUltFecha(Timestamp compraUltFecha) {
		this.compraUltFecha = compraUltFecha;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public BarrioDTO getBarrio() {
		return barrio;
	}

	public void setBarrio(BarrioDTO barrio) {
		this.barrio = barrio;
	}

	public CiudadDTO getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadDTO ciudad) {
		this.ciudad = ciudad;
	}

	public DepartamentoDTO getDepartamento() {
		return departamento;
	}

	public void setDepartamento(DepartamentoDTO departamento) {
		this.departamento = departamento;
	}

	public PaisDTO getPais() {
		return pais;
	}

	public void setPais(PaisDTO pais) {
		this.pais = pais;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Integer getNroLocal() {
		return nroLocal;
	}

	public void setNroLocal(Integer nroLocal) {
		this.nroLocal = nroLocal;
	}

	public String getPrimeraLateral() {
		return primeraLateral;
	}

	public void setPrimeraLateral(String primeraLateral) {
		this.primeraLateral = primeraLateral;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getSegundaLateral() {
		return segundaLateral;
	}

	public void setSegundaLateral(String segundaLateral) {
		this.segundaLateral = segundaLateral;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Date getFecNac() {
		return fecNac;
	}

	public void setFecNac(Date fecNac) {
		this.fecNac = fecNac;
	}

}