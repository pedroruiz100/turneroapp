package turneroWeb.core.dto;

import java.math.BigDecimal;

public class FacturaClienteDetDTO {

	private Long idFacturaClienteDet;

	private Boolean bajada;

	private BigDecimal cantidad;

	private Long codArticulo;

	private String codVendedor;

	private String descripcion;

	private ArticuloDTO articulo;

	private Integer orden;

	private Boolean permiteDesc;

	private Integer poriva;

	private Long precio;

	private String seccion;

	private String seccionSub;

	private FacturaClienteCabDTO facturaClienteCab;

	public FacturaClienteDetDTO() {
		super();
	}

	public Long getIdFacturaClienteDet() {
		return idFacturaClienteDet;
	}

	public void setIdFacturaClienteDet(Long idFacturaClienteDet) {
		this.idFacturaClienteDet = idFacturaClienteDet;
	}

	public Boolean getBajada() {
		return bajada;
	}

	public void setBajada(Boolean bajada) {
		this.bajada = bajada;
	}

	public BigDecimal getCantidad() {
		return cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCodArticulo() {
		return codArticulo;
	}

	public void setCodArticulo(Long codArticulo) {
		this.codArticulo = codArticulo;
	}

	public String getCodVendedor() {
		return codVendedor;
	}

	public void setCodVendedor(String codVendedor) {
		this.codVendedor = codVendedor;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public ArticuloDTO getArticulo() {
		return articulo;
	}

	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Boolean getPermiteDesc() {
		return permiteDesc;
	}

	public void setPermiteDesc(Boolean permiteDesc) {
		this.permiteDesc = permiteDesc;
	}

	public Integer getPoriva() {
		return poriva;
	}

	public void setPoriva(Integer poriva) {
		this.poriva = poriva;
	}

	public Long getPrecio() {
		return precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public String getSeccion() {
		return seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getSeccionSub() {
		return seccionSub;
	}

	public void setSeccionSub(String seccionSub) {
		this.seccionSub = seccionSub;
	}

	public FacturaClienteCabDTO getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}
