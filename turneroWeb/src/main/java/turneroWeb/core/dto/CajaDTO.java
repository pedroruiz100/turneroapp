package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

public class CajaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idCaja;

	private Boolean activo;

	private String descripcion;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private String nroSerie;

	private String usuAlta;

	private String usuMod;

	private TipoCajaDTO tipoCaja;

	private SucursalDTO sucursal;

	private Integer nroCaja;

	private String claveCaja;

	private IpBocaDTO ipBoca;

	public CajaDTO() {
	}

	public Long getIdCaja() {
		return idCaja;
	}

	public void setIdCaja(Long idCaja) {
		this.idCaja = idCaja;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getNroSerie() {
		return nroSerie;
	}

	public void setNroSerie(String nroSerie) {
		this.nroSerie = nroSerie;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public TipoCajaDTO getTipoCaja() {
		return tipoCaja;
	}

	public void setTipoCaja(TipoCajaDTO tipoCaja) {
		this.tipoCaja = tipoCaja;
	}

	public SucursalDTO getSucursal() {
		return sucursal;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursal = sucursal;
	}

	public Integer getNroCaja() {
		return nroCaja;
	}

	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}

	public String getClaveCaja() {
		return claveCaja;
	}

	public void setClaveCaja(String claveCaja) {
		this.claveCaja = claveCaja;
	}

	public IpBocaDTO getIpBoca() {
		return ipBoca;
	}

	public void setIpBoca(IpBocaDTO ipBoca) {
		this.ipBoca = ipBoca;
	}

}