package turneroWeb.core.dto;

import java.io.Serializable;

public class TalonariosSucursaleDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idTalonariosSucursales;

	private Long nroActual;

	private Long nroFinal;

	private Long nroInicial;

	private IpBocaDTO ipBocaDTO;

	private SucursalDTO sucursalDTO;

	private TimbradoDTO timbradoDTO;

	private String primero;

	private String segundo;

	public TalonariosSucursaleDTO() {
	}

	public Long getIdTalonariosSucursales() {
		return this.idTalonariosSucursales;
	}

	public void setIdTalonariosSucursales(Long idTalonariosSucursales) {
		this.idTalonariosSucursales = idTalonariosSucursales;
	}

	public Long getNroActual() {
		return this.nroActual;
	}

	public void setNroActual(Long nroActual) {
		this.nroActual = nroActual;
	}

	public Long getNroFinal() {
		return this.nroFinal;
	}

	public IpBocaDTO getIpBocaDTO() {
		return ipBocaDTO;
	}

	public void setIpBocaDTO(IpBocaDTO ipBocaDTO) {
		this.ipBocaDTO = ipBocaDTO;
	}

	public SucursalDTO getSucursalDTO() {
		return sucursalDTO;
	}

	public void setSucursalDTO(SucursalDTO sucursalDTO) {
		this.sucursalDTO = sucursalDTO;
	}

	public TimbradoDTO getTimbradoDTO() {
		return timbradoDTO;
	}

	public void setTimbradoDTO(TimbradoDTO timbradoDTO) {
		this.timbradoDTO = timbradoDTO;
	}

	public String getPrimero() {
		return primero;
	}

	public void setPrimero(String primero) {
		this.primero = primero;
	}

	public String getSegundo() {
		return segundo;
	}

	public void setSegundo(String segundo) {
		this.segundo = segundo;
	}

	public void setNroFinal(Long nroFinal) {
		this.nroFinal = nroFinal;
	}

	public Long getNroInicial() {
		return this.nroInicial;
	}

	public void setNroInicial(Long nroInicial) {
		this.nroInicial = nroInicial;
	}

	public IpBocaDTO getIpBoca() {
		return this.ipBocaDTO;
	}

	public void setIpBoca(IpBocaDTO ipBoca) {
		this.ipBocaDTO = ipBoca;
	}

	public SucursalDTO getSucursal() {
		return this.sucursalDTO;
	}

	public void setSucursal(SucursalDTO sucursal) {
		this.sucursalDTO = sucursal;
	}

	public TimbradoDTO getTimbrado() {
		return this.timbradoDTO;
	}

	public void setTimbrado(TimbradoDTO timbrado) {
		this.timbradoDTO = timbrado;
	}

}