package turneroWeb.core.dto;

import java.sql.Date;

public class FacturaClienteCabHistoricoDTO {

	private Long idFacturaClienteCabHistorico;

	private FacturaClienteCabDTO facturaClienteCab;

	private String empresa;

	private String sucursal;

	private Integer nroCaja;

	private String ruc;

	private String telef;

	private String direccion;

	private String nroTimbrado;

	private Date fecInicial;

	private Date fecVencimiento;

	private Integer grav10;

	private Integer grav5;

	private Integer exenta;

	private Integer liqui10;

	private Integer liqui5;

	private String rucCliente;

	private String cliente;

	private Integer descuento;

	private Integer total;

	private Integer efectivo;

	private Integer tarjCred;

	private Integer tarjDeb;

	private Integer cheque;

	private Integer vale;

	private Integer asoc;

	private Integer notCre;

	private Integer redondeo;

	private Integer vuelto;

	private Integer dolar;

	private Integer peso;

	private Integer realb;

	private Integer retencion;

	private Long cotiDolar;

	private Long cotiPeso;

	private Integer giftcard;

	private Long cotiReal;

	public FacturaClienteCabHistoricoDTO() {
	}

	public Long getIdFacturaClienteCabHistorico() {
		return idFacturaClienteCabHistorico;
	}

	public void setIdFacturaClienteCabHistorico(
			Long idFacturaClienteCabHistorico) {
		this.idFacturaClienteCabHistorico = idFacturaClienteCabHistorico;
	}

	public FacturaClienteCabDTO getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Integer getGiftcard() {
		return giftcard;
	}

	public void setGiftcard(Integer giftcard) {
		this.giftcard = giftcard;
	}

	public String getSucursal() {
		return sucursal;
	}

	public Long getCotiDolar() {
		return cotiDolar;
	}

	public void setCotiDolar(Long cotiDolar) {
		this.cotiDolar = cotiDolar;
	}

	public Long getCotiPeso() {
		return cotiPeso;
	}

	public void setCotiPeso(Long cotiPeso) {
		this.cotiPeso = cotiPeso;
	}

	public Long getCotiReal() {
		return cotiReal;
	}

	public void setCotiReal(Long cotiReal) {
		this.cotiReal = cotiReal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public Integer getNroCaja() {
		return nroCaja;
	}

	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getTelef() {
		return telef;
	}

	public void setTelef(String telef) {
		this.telef = telef;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getNroTimbrado() {
		return nroTimbrado;
	}

	public void setNroTimbrado(String nroTimbrado) {
		this.nroTimbrado = nroTimbrado;
	}

	public Date getFecInicial() {
		return fecInicial;
	}

	public void setFecInicial(Date fecInicial) {
		this.fecInicial = fecInicial;
	}

	public Date getFecVencimiento() {
		return fecVencimiento;
	}

	public void setFecVencimiento(Date fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}

	public Integer getGrav10() {
		return grav10;
	}

	public void setGrav10(Integer grav10) {
		this.grav10 = grav10;
	}

	public Integer getGrav5() {
		return grav5;
	}

	public void setGrav5(Integer grav5) {
		this.grav5 = grav5;
	}

	public Integer getExenta() {
		return exenta;
	}

	public void setExenta(Integer exenta) {
		this.exenta = exenta;
	}

	public Integer getLiqui10() {
		return liqui10;
	}

	public void setLiqui10(Integer liqui10) {
		this.liqui10 = liqui10;
	}

	public Integer getLiqui5() {
		return liqui5;
	}

	public void setLiqui5(Integer liqui5) {
		this.liqui5 = liqui5;
	}

	public String getRucCliente() {
		return rucCliente;
	}

	public void setRucCliente(String rucCliente) {
		this.rucCliente = rucCliente;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Integer getDescuento() {
		return descuento;
	}

	public void setDescuento(Integer descuento) {
		this.descuento = descuento;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getEfectivo() {
		return efectivo;
	}

	public void setEfectivo(Integer efectivo) {
		this.efectivo = efectivo;
	}

	public Integer getTarjCred() {
		return tarjCred;
	}

	public void setTarjCred(Integer tarjCred) {
		this.tarjCred = tarjCred;
	}

	public Integer getTarjDeb() {
		return tarjDeb;
	}

	public void setTarjDeb(Integer tarjDeb) {
		this.tarjDeb = tarjDeb;
	}

	public Integer getCheque() {
		return cheque;
	}

	public void setCheque(Integer cheque) {
		this.cheque = cheque;
	}

	public Integer getVale() {
		return vale;
	}

	public void setVale(Integer vale) {
		this.vale = vale;
	}

	public Integer getAsoc() {
		return asoc;
	}

	public void setAsoc(Integer asoc) {
		this.asoc = asoc;
	}

	public Integer getNotCre() {
		return notCre;
	}

	public void setNotCre(Integer notCre) {
		this.notCre = notCre;
	}

	public Integer getRedondeo() {
		return redondeo;
	}

	public void setRedondeo(Integer redondeo) {
		this.redondeo = redondeo;
	}

	public Integer getVuelto() {
		return vuelto;
	}

	public void setVuelto(Integer vuelto) {
		this.vuelto = vuelto;
	}

	public Integer getDolar() {
		return dolar;
	}

	public void setDolar(Integer dolar) {
		this.dolar = dolar;
	}

	public Integer getPeso() {
		return peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Integer getRealb() {
		return realb;
	}

	public void setRealb(Integer realb) {
		this.realb = realb;
	}

	public Integer getRetencion() {
		return retencion;
	}

	public void setRetencion(Integer retencion) {
		this.retencion = retencion;
	}

}
