package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import turneroWeb.core.domain.Seccion;

/**
 * The persistent class for the articulo database table.
 * 
 */
public class ArticuloDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idArticulo;

	private IvaDTO iva;

	private UnidadDTO unidad;

	private String descripcion;

	private Long precioMay;

	private Long precioMin;

	private String usuAlta;

	private Timestamp fechaAlta;

	private String usuMod;

	private Timestamp fechaMod;

	private Boolean permiteDesc;

	private String codArticulo;

	private SeccionDTO seccion;

	private Boolean servicio;

	private Boolean stockeable;

	private Boolean importado;

	private Boolean bajada;

	private Long costo;

	private String pack;

	private String estante;

	private String minimo;

	private String maximo;

	private String dtoMayorista;

	private String dtoMinorista;

	private String observacion;
	
	private String stockActual;

	private byte[] imagen;

	public ArticuloDTO() {
		super();
	}

	public Long getIdArticulo() {
		return idArticulo;
	}

	public void setIdArticulo(Long idArticulo) {
		this.idArticulo = idArticulo;
	}

	public IvaDTO getIva() {
		return iva;
	}

	public void setIva(IvaDTO iva) {
		this.iva = iva;
	}

	public UnidadDTO getUnidad() {
		return unidad;
	}

	public String getStockActual() {
		return stockActual;
	}

	public void setStockActual(String stockActual) {
		this.stockActual = stockActual;
	}

	public void setUnidad(UnidadDTO unidad) {
		this.unidad = unidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getPrecioMay() {
		return precioMay;
	}

	public void setPrecioMay(Long precioMay) {
		this.precioMay = precioMay;
	}

	public Long getPrecioMin() {
		return precioMin;
	}

	public void setPrecioMin(Long precioMin) {
		this.precioMin = precioMin;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Boolean getPermiteDesc() {
		return permiteDesc;
	}

	public void setPermiteDesc(Boolean permiteDesc) {
		this.permiteDesc = permiteDesc;
	}

	public String getCodArticulo() {
		return codArticulo;
	}

	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}

	public SeccionDTO getSeccion() {
		return seccion;
	}

	public void setSeccion(SeccionDTO seccion) {
		this.seccion = seccion;
	}

	public Boolean getServicio() {
		return servicio;
	}

	public void setServicio(Boolean servicio) {
		this.servicio = servicio;
	}

	public Boolean getStockeable() {
		return stockeable;
	}

	public void setStockeable(Boolean stockeable) {
		this.stockeable = stockeable;
	}

	public Boolean getImportado() {
		return importado;
	}

	public void setImportado(Boolean importado) {
		this.importado = importado;
	}

	public Boolean getBajada() {
		return bajada;
	}

	public void setBajada(Boolean bajada) {
		this.bajada = bajada;
	}

	public Long getCosto() {
		return costo;
	}

	public void setCosto(Long costo) {
		this.costo = costo;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getEstante() {
		return estante;
	}

	public void setEstante(String estante) {
		this.estante = estante;
	}

	public String getMinimo() {
		return minimo;
	}

	public void setMinimo(String minimo) {
		this.minimo = minimo;
	}

	public String getMaximo() {
		return maximo;
	}

	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}

	public String getDtoMayorista() {
		return dtoMayorista;
	}

	public void setDtoMayorista(String dtoMayorista) {
		this.dtoMayorista = dtoMayorista;
	}

	public String getDtoMinorista() {
		return dtoMinorista;
	}

	public void setDtoMinorista(String dtoMinorista) {
		this.dtoMinorista = dtoMinorista;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

}