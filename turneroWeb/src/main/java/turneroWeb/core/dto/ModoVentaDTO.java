package turneroWeb.core.dto;

import java.io.Serializable;

public class ModoVentaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idVenta;

	private Boolean sinLogin;

	public ModoVentaDTO() {
	}

	public Long getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(Long idVenta) {
		this.idVenta = idVenta;
	}

	public Boolean getSinLogin() {
		return sinLogin;
	}

	public void setSinLogin(Boolean sinLogin) {
		this.sinLogin = sinLogin;
	}

}
