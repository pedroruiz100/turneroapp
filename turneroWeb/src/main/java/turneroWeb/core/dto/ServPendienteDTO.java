package turneroWeb.core.dto;

public class ServPendienteDTO {

	private Long idServPendiente;

	private ClientePendienteDTO clientePendiente;

	private ArticuloDTO articulo;

	private FuncionarioDTO funcionario;

	private Integer montoServ;

	private Integer montoComision;

	private Boolean comisionPorc;

	private String descripcion;

	private Integer poriva;

	private String codArticulo;

	private Integer porc;

	private Integer cantidad;

	public ServPendienteDTO() {
		super();
	}

	public Long getIdServPendiente() {
		return idServPendiente;
	}

	public void setIdServPendiente(Long idServPendiente) {
		this.idServPendiente = idServPendiente;
	}

	public ClientePendienteDTO getClientePendiente() {
		return clientePendiente;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public void setClientePendiente(ClientePendienteDTO clientePendiente) {
		this.clientePendiente = clientePendiente;
	}

	public ArticuloDTO getArticulo() {
		return articulo;
	}

	public void setArticulo(ArticuloDTO articulo) {
		this.articulo = articulo;
	}

	public FuncionarioDTO getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(FuncionarioDTO funcionario) {
		this.funcionario = funcionario;
	}

	public Integer getMontoServ() {
		return montoServ;
	}

	public void setMontoServ(Integer montoServ) {
		this.montoServ = montoServ;
	}

	public Integer getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(Integer montoComision) {
		this.montoComision = montoComision;
	}

	public Boolean getComisionPorc() {
		return comisionPorc;
	}

	public void setComisionPorc(Boolean comisionPorc) {
		this.comisionPorc = comisionPorc;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getPoriva() {
		return poriva;
	}

	public void setPoriva(Integer poriva) {
		this.poriva = poriva;
	}

	public String getCodArticulo() {
		return codArticulo;
	}

	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}

	public Integer getPorc() {
		return porc;
	}

	public void setPorc(Integer porc) {
		this.porc = porc;
	}

}
