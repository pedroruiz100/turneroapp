package turneroWeb.core.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RetiroPedidoDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idRetiro;

	private FacturaClienteCabDTO facturaClienteCab;

	private String numero;

	private String cliente;

	private boolean entregado;

	private boolean preparacion;

	private boolean listo;

	private String hora;

	public RetiroPedidoDTO() {
		super();
	}

	public Long getIdRetiro() {
		return idRetiro;
	}

	public void setIdRetiro(Long idRetiro) {
		this.idRetiro = idRetiro;
	}

	public FacturaClienteCabDTO getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public boolean isEntregado() {
		return entregado;
	}

	public void setEntregado(boolean entregado) {
		this.entregado = entregado;
	}

	public boolean isPreparacion() {
		return preparacion;
	}

	public void setPreparacion(boolean preparacion) {
		this.preparacion = preparacion;
	}

	public boolean isListo() {
		return listo;
	}

	public void setListo(boolean listo) {
		this.listo = listo;
	}

}