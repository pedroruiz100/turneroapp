package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the estado_factura database table.
 * 
 */
public class EstadoFacturaDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idEstadoFactura;

	private String descripcion;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private String usuAlta;

	private String usuMod;

	public EstadoFacturaDTO() {
	}

	public Long getIdEstadoFactura() {
		return idEstadoFactura;
	}

	public void setIdEstadoFactura(Long idEstadoFactura) {
		this.idEstadoFactura = idEstadoFactura;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}