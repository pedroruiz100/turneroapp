package turneroWeb.core.dto;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The persistent class for the unidad database table.
 * 
 */
public class UnidadDTO implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long idUnidad;

	private Boolean activo;

	private Integer decimal;

	private String descripcion;

	private String descripcionDet;

	private Timestamp fechaAlta;

	private Timestamp fechaMod;

	private String usuAlta;

	private String usuMod;

	public UnidadDTO() {
	}

	public Long getIdUnidad() {
		return this.idUnidad;
	}

	public void setIdUnidad(Long idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getDecimal() {
		return this.decimal;
	}

	public void setDecimal(Integer decimal) {
		this.decimal = decimal;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionDet() {
		return this.descripcionDet;
	}

	public void setDescripcionDet(String descripcionDet) {
		this.descripcionDet = descripcionDet;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

}