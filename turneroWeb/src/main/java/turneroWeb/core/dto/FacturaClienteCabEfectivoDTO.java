package turneroWeb.core.dto;

import java.io.Serializable;
public class FacturaClienteCabEfectivoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idFacturaClienteCabEfectivo;
	private Integer montoEfectivo;
	private FacturaClienteCabDTO facturaClienteCab;

	public FacturaClienteCabEfectivoDTO() {
		super();
	}

	public Long getIdFacturaClienteCabEfectivo() {
		return idFacturaClienteCabEfectivo;
	}

	public void setIdFacturaClienteCabEfectivo(Long idFacturaClienteCabEfectivo) {
		this.idFacturaClienteCabEfectivo = idFacturaClienteCabEfectivo;
	}

	public Integer getMontoEfectivo() {
		return montoEfectivo;
	}

	public void setMontoEfectivo(Integer montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}

	public FacturaClienteCabDTO getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCabDTO facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}
