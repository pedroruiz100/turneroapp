/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import turneroWeb.core.domain.Iva;

/**
 *
 * @author ExcelsisWalker
 */
public interface IvaDAO extends CRUDGenericDAO<Iva> {

	long recuperarIvaPorCodigo(String id);

//	List<ArtIvaiculo> listarPorSecciones(long id);

//	Iva listarPorCodigo(long id);

}
