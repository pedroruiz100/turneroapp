/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import turneroWeb.core.domain.Supervisor;

/**
 *
 * @author ExcelsisWalker
 */
public interface SupervisorDAO extends CRUDGenericDAO<Supervisor> {

	Supervisor buscarNumeroCodigoSup(int numSup, String codSup);

	Supervisor buscarCodSup(String codSup);

	Supervisor buscarSupervisor(String usuario, String clave);
}
