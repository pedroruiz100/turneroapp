package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.FacturaClienteCabNotaCredito;

public interface FacturaClienteCabNotaCreditoDAO extends
		CRUDGenericDAO<FacturaClienteCabNotaCredito> {

	FacturaClienteCabNotaCredito insertarObtenerObjeto(
			FacturaClienteCabNotaCredito fac);

	public List<FacturaClienteCabNotaCredito> listarPorFactura(long parseLong);

}
