package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.FacturaClienteCabTarjeta;

public interface FacturaClienteCabTarjetaDAO extends
		CRUDGenericDAO<FacturaClienteCabTarjeta> {

	FacturaClienteCabTarjeta insercionMasiva(FacturaClienteCabTarjeta fac,
			int id);

	FacturaClienteCabTarjeta insertarObtenerObj(
			FacturaClienteCabTarjeta facturaClienteCabTarjeta);

	public List<FacturaClienteCabTarjeta> listarPorFactura(long parseLong);

	public List<FacturaClienteCabTarjeta> filtroFechaTarjeta(
			String fechaInicio, String fechaFin, String selectedItem);
}
