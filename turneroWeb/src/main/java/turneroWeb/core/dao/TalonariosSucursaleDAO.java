package turneroWeb.core.dao;

import turneroWeb.core.domain.TalonariosSucursale;

public interface TalonariosSucursaleDAO extends CRUDGenericDAO<TalonariosSucursale> {

	void actualizarNroActual(long idTalonarioSucursal, long nroActual);

	TalonariosSucursale listarPorSucursal(long id, long idTimbrado);

}
