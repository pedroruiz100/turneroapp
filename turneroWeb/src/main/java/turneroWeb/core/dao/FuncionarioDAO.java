package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.Funcionario;

public interface FuncionarioDAO extends CRUDGenericDAO<Funcionario> {

    List<Funcionario> filtrarNomApeCi(String nombre, String apellido, String ci);

}
