package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.ServPendiente;

public interface ServPendienteDAO extends CRUDGenericDAO<ServPendiente> {

	ServPendiente insertarObtenerObj(ServPendiente fromServPendienteAsociado);

	List<ServPendiente> buscandoServiciosAsignados(long idCP, long idFu);

	List<ServPendiente> buscandoServiciosComision(long idFu,
			String fechaInicio, String fechaFin);

	List<ServPendiente> buscandoServiciosComisionOrderArt(long idFu,
			String fechaInicio, String fechaFin);

	void eliminarPorIdClientePendiente(long idCP);

}
