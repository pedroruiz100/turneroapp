package turneroWeb.core.dao;

import turneroWeb.core.domain.AperturaCaja;

public interface AperturaCajaDAO extends CRUDGenericDAO<AperturaCaja> {

    AperturaCaja insertarObtenerObj(AperturaCaja aperturaCaja);

    String recuperarPorCajaFecha(long idCaja, String fecha);

    public AperturaCaja recuperarPorCaja(Long idCaja, int x);

    public boolean insertarRecuperarEstado(AperturaCaja ac);

    public boolean verificarAperturaDia();

    public long getCantidadAperturaDia();

}
