package turneroWeb.core.dao;

import turneroWeb.core.domain.FacturaCabClientePendiente;

public interface FacturaCabClientePendienteDAO extends CRUDGenericDAO<FacturaCabClientePendiente> {

    FacturaCabClientePendiente insertarObtenerObj(
            FacturaCabClientePendiente fromFacturaCabClientePendienteAsociado);

}
