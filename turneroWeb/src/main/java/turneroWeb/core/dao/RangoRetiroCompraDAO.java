package turneroWeb.core.dao;

import turneroWeb.core.domain.RangoRetiroCompra;

public interface RangoRetiroCompraDAO extends CRUDGenericDAO<RangoRetiroCompra> {

    long actualizarObtenerRango(long idRango);

    public RangoRetiroCompra actualizarObtenerRangoObjectActual(long idRango1);

}
