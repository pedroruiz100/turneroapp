/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.Articulo;

/**
 *
 * @author ExcelsisWalker
 */
public interface ArticuloDAO extends CRUDGenericDAO<Articulo> {

	List<Articulo> listarPorSecciones(long id);

	Articulo listarPorCodigo(String id);

	List<Articulo> listarPorDescripcion(String descri);

	boolean actualizarCantidad(long id, long canti);

}
