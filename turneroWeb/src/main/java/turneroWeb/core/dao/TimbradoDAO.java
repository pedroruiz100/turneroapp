package turneroWeb.core.dao;

import turneroWeb.core.domain.Timbrado;

public interface TimbradoDAO extends CRUDGenericDAO<Timbrado> {

}
