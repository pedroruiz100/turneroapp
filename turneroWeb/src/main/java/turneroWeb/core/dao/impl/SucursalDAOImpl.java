package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.SucursalDAO;
import turneroWeb.core.domain.Sucursal;


@Repository
public class SucursalDAOImpl implements SucursalDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Sucursal> listar() {
		return em.createQuery("FROM Sucursal s").getResultList();
	}

	@Override
	public Sucursal getById(long id) {
		// TODO Auto-generated method stub
		return em.find(Sucursal.class, id);
	}

	@Override
	public void insertar(Sucursal obj) {
		// TODO Auto-generated method stub
		em.persist(obj);
	}

	@Override
	public void actualizar(Sucursal obj) {
		// TODO Auto-generated method stub
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

}
