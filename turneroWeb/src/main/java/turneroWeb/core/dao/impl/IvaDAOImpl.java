/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.IvaDAO;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.Iva;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class IvaDAOImpl implements IvaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Iva> listar() {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Iva> ls = em.createQuery("FROM Iva c").getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public Iva getById(long id) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Iva c = em.find(Iva.class, id);
			return c;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Iva obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.persist(obj);
			em.getTransaction().commit();
			em.refresh(obj);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}

	}

	@Override
	public void actualizar(Iva obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.merge(obj);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente c = em.find(Cliente.class, id);
			em.remove(c);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public long recuperarIvaPorCodigo(String id) {
		try {
			long idIva = Long.parseLong(em
					.createNativeQuery(
							"SELECT id_iva FROM stock.articulo WHERE cod_articulo='"
									+ id + "'").getSingleResult().toString());
			return idIva;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return 0L;
		} finally {
			em.close();
		}
	}

}
