/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.UsuarioDAO;
import turneroWeb.core.domain.Usuario;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class UsuarioDAOImpl implements UsuarioDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Usuario> listar() {
		try {
			List<Usuario> u = em.createQuery("FROM Usuario u").getResultList();
			return u;
		} finally {
			em.close();
		}
	}

	@Override
	public Usuario getById(long id) {
		try {
			return em.find(Usuario.class, id);
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Usuario obj) {
		try {
			em.getTransaction().begin();
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(Usuario obj) {
		try {
			em.getTransaction().begin();
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public Usuario busUsuCI(String CI) {
		return busUsuLocalCI(CI);
	}

	private Usuario busUsuLocalCI(String CI) {
		try {
			return (Usuario) em
					.createQuery(
							"FROM Usuario u WHERE (u.funcionario.ci = :CI) and u.activo=true")
					.setParameter("CI", CI).getSingleResult();
		} finally {
			em.close();
		}
	}

	@Override
	public boolean actualizarContrasenha(String contrasenha, long id,
			String usuMod, Timestamp fechaMod) {
		EntityManager em = null;
		try {
			em.getTransaction().begin();
			Usuario usuario = em.find(Usuario.class, id);
			usuario.setUsuMod(usuMod);
			usuario.setFechaMod(fechaMod);
			usuario.setContrasenha(contrasenha);
			usuario.setGenerico(false);
			em.merge(usuario);
			em.getTransaction().commit();
			return true;
		} catch (Exception ex) {
			System.out.println("ERROR actualizarContrasenha: -->> "
					+ ex.fillInStackTrace());
			return false;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Usuario buscarCodSup(String cod) {
		// TODO Auto-generated method stub
		return null;
	}

}
