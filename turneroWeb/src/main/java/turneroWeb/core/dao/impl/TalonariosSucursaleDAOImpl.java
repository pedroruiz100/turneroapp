package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.TalonariosSucursaleDAO;
import turneroWeb.core.domain.TalonariosSucursale;

@Repository
public class TalonariosSucursaleDAOImpl implements TalonariosSucursaleDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<TalonariosSucursale> listar() {
		return em.createQuery("FROM TalonariosSucursale ts").getResultList();
	}

	@Override
	public TalonariosSucursale getById(long id) {
		// TODO Auto-generated method stub
		return em.find(TalonariosSucursale.class, id);
	}

	@Override
	public void insertar(TalonariosSucursale obj) {
		// TODO Auto-generated method stub
		em.persist(obj);
	}

	@Override
	public void actualizar(TalonariosSucursale obj) {
		// TODO Auto-generated method stub
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizarNroActual(long idTalonarioSucursal, long nroActual) {
		TalonariosSucursale tal = em.find(TalonariosSucursale.class,
				idTalonarioSucursal);
		tal.setNroActual(nroActual);
		em.merge(tal);
	}

	@Override
	public TalonariosSucursale listarPorSucursal(long id, long idTimbrado) {
		return (TalonariosSucursale) em
				.createQuery(
						"FROM TalonariosSucursale ts WHERE ts.sucursal.idSucursal="
								+ id + " AND ts.timbrado.idTimbrado="
								+ idTimbrado).setMaxResults(1)
				.getSingleResult();
	}

}
