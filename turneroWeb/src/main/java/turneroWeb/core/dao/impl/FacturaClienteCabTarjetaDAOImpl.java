package turneroWeb.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaClienteCabTarjetaDAO;
import turneroWeb.core.domain.FacturaClienteCabTarjeta;

@Repository
public class FacturaClienteCabTarjetaDAOImpl implements
		FacturaClienteCabTarjetaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<FacturaClienteCabTarjeta> listar() {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return em.createQuery("FROM FacturaClienteCabTarjeta fct")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public FacturaClienteCabTarjeta getById(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return (FacturaClienteCabTarjeta) em
					.createQuery(
							"FROM FacturaClienteCabTarjeta fct WHERE fct.idFacturaClienteCabTarjeta =: idFac")
					.setParameter("idFac", id).getSingleResult();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void insertar(FacturaClienteCabTarjeta obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(obj);
			// em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(FacturaClienteCabTarjeta obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.merge(obj);
			// em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			FacturaClienteCabTarjeta fac = em.find(
					FacturaClienteCabTarjeta.class, id);
			em.remove(fac);
			// em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public FacturaClienteCabTarjeta insercionMasiva(
			FacturaClienteCabTarjeta fac, int i) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(fac);
			// em.getTransaction().commit();
			if (i % 30 == 0) {
				em.flush();
				em.clear();
			}
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return fac;
	}

	@Override
	public FacturaClienteCabTarjeta insertarObtenerObj(
			FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(facturaClienteCabTarjeta);
			// em.getTransaction().commit();
			em.refresh(facturaClienteCabTarjeta);
			return facturaClienteCabTarjeta;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<FacturaClienteCabTarjeta> listarPorFactura(long parseLong) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return em
					.createQuery(
							"FROM FacturaClienteCabTarjeta fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
					.setParameter("idFac", parseLong).getResultList();
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
			System.out.println("-->> " + ex.fillInStackTrace());
			return new ArrayList<>();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<FacturaClienteCabTarjeta> filtroFechaTarjeta(
			String fechaInicio, String fechaFin, String nroCaja) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return em
					.createQuery(
							"FROM FacturaClienteCabTarjeta fccf JOIN FETCH fccf.facturaClienteCab fcc "
									+ "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
									+ "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin")
					.setParameter("fecIni", fechaInicio)
					.setParameter("fecFin", fechaFin)
					.setParameter("numCaja", Integer.parseInt(nroCaja))
					.getResultList();
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
			System.out.println("-->> " + ex.fillInStackTrace());
			return new ArrayList<>();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
