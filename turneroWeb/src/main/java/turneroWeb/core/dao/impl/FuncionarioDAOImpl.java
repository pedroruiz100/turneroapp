package turneroWeb.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FuncionarioDAO;
import turneroWeb.core.domain.Funcionario;

@Repository
public class FuncionarioDAOImpl implements FuncionarioDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Funcionario> listar() {

		try {
			return em.createQuery("FROM Funcionario f ORDER BY f.nombre")
					.setMaxResults(10).getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Funcionario getById(long id) {

		try {
			return (Funcionario) em
					.createQuery(
							"FROM Funcionario f WHERE f.idFuncionario =:idFun")
					.setParameter("idFun", id).getSingleResult();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void insertar(Funcionario obj) {

		try {
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(Funcionario obj) {

		try {
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Funcionario> filtrarNomApeCi(String nombre, String apellido,
			String ci) {

		try {
			List<Funcionario> lista = new ArrayList<>();
			if (!nombre.equalsIgnoreCase("null")
					&& !apellido.equalsIgnoreCase("null")
					&& !ci.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.apellido) like :apellido "
										+ "and upper(c.ci) like :numci ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setParameter("numci", ci.toUpperCase() + "%")
						.setMaxResults(10).getResultList();

			} else if (!nombre.equalsIgnoreCase("null")
					&& !apellido.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.apellido) like :apellido "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!nombre.equalsIgnoreCase("null")
					&& !ci.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.ci) like :numci "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("numci", ci.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!apellido.equalsIgnoreCase("null")
					&& !ci.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.apellido) like :apellido "
										+ "and upper(c.ci) like :numci "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setParameter("numci", ci.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!apellido.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.apellido) like :apellido ORDER BY c.apellido")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!nombre.equalsIgnoreCase("null")) {
				lista = em
						.createQuery(
								"FROM Funcionario c WHERE upper(c.nombre) like :nombre ORDER BY c.nombre")
						.setMaxResults(10)
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.getResultList();
			} else if (!ci.equalsIgnoreCase("null")) {
				lista = em.createQuery("FROM Funcionario c WHERE c.ci =:numci")
						.setParameter("numci", ci).getResultList();

			} else {
				lista = em
						.createQuery(
								"FROM Funcionario c ORDER BY c.nombre, c.apellido")
						.setMaxResults(10).getResultList();
			}
			return lista;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
