/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ClienteDAO;
import turneroWeb.core.domain.Barrio;
import turneroWeb.core.domain.Ciudad;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.Departamento;
import turneroWeb.core.domain.Pais;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class ClienteDAOImpl implements ClienteDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Cliente> listar() {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Cliente> ls = em.createQuery(
					"FROM Cliente c ORDER BY c.fechaAlta").getResultList();
			return ls;
		} finally {
			em.close();
		}
	}

	@Override
	public Cliente getById(long id) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Cliente c = em.find(Cliente.class, id);
			return c;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Cliente obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(obj);
			// em.getTransaction().commit();
			em.refresh(obj);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			System.out.println("-->> " + e.fillInStackTrace());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(Cliente obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.merge(obj);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente c = em.find(Cliente.class, id);
			em.remove(c);
			em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<Cliente> listarPorNomRuc(String nombre, String apellido,
			String ruc) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Cliente> listaCliente = new ArrayList<Cliente>();
			if (!nombre.equalsIgnoreCase("null")
					&& !apellido.equalsIgnoreCase("null")
					&& !ruc.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.apellido) like :apellido "
										+ "and upper(c.ruc) like :numRuc ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setParameter("numRuc", ruc.toUpperCase() + "%")
						.setMaxResults(10).getResultList();

			} else if (!nombre.equalsIgnoreCase("null")
					&& !apellido.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.apellido) like :apellido "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!nombre.equalsIgnoreCase("null")
					&& !ruc.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.nombre) like :nombre "
										+ "and upper(c.ruc) like :numRuc "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.setParameter("numRuc", ruc.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!apellido.equalsIgnoreCase("null")
					&& !ruc.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.apellido) like :apellido "
										+ "and upper(c.ruc) like :numRuc "
										+ "ORDER BY c.nombre, c.apellido")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setParameter("numRuc", ruc.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!apellido.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.apellido) like :apellido ORDER BY c.apellido")
						.setParameter("apellido", apellido.toUpperCase() + "%")
						.setMaxResults(10).getResultList();
			} else if (!nombre.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery(
								"FROM Cliente c WHERE upper(c.nombre) like :nombre ORDER BY c.nombre")
						.setMaxResults(10)
						.setParameter("nombre", nombre.toUpperCase() + "%")
						.getResultList();
			} else if (!ruc.equalsIgnoreCase("null")) {
				listaCliente = em
						.createQuery("FROM Cliente c WHERE c.ruc =:numRuc")
						.setParameter("numRuc", ruc).getResultList();

			} else {
				listaCliente = em
						.createQuery("FROM Cliente c ORDER BY c.fechaAlta")
						.setMaxResults(10).getResultList();
			}
			return listaCliente;
		} finally {
			em.close();
		}
	}

	@Override
	public void actualizarNomApeRuc(String nombre, String apellido, String ruc,
			long id, String usuMod, Timestamp fechaMod) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente cli = em.find(Cliente.class, id);
			cli.setNombre(nombre);
			cli.setRuc(ruc);
			cli.setApellido(apellido);
			cli.setUsuMod(usuMod);
			cli.setFechaMod(fechaMod);
			Pais pais = new Pais();
			pais.setIdPais(0L);
			Departamento dpto = new Departamento();
			dpto.setIdDepartamento(0l);
			Ciudad ciu = new Ciudad();
			ciu.setIdCiudad(0l);
			Barrio barr = new Barrio();
			barr.setIdBarrio(0l);
			cli.setPais(pais);
			cli.setDepartamento(dpto);
			cli.setCiudad(ciu);
			cli.setBarrio(barr);
			em.merge(cli);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Cliente listarPorRuc(String ruc) {
		// EntityManager em = getEmf().createEntityManager();
		Cliente cli = new Cliente();
		try {
			cli = (Cliente) em
					.createQuery("FROM Cliente c WHERE c.ruc =:numRuc")
					.setParameter("numRuc", ruc).getSingleResult();
			return cli;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Cliente listarPorCI(String cod) {
		// String dato[] = cod.split("-");
		try {
			Cliente cliente = (Cliente) em
					.createQuery("FROM Cliente c WHERE c.ruc LIKE :cod")
					.setParameter("cod", cod + "%").setMaxResults(1)
					.getSingleResult();
			return cliente;
		} catch (Exception ex) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Cliente listarPorCodigo(int codCliente) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Cliente cliente = (Cliente) em
					.createQuery("FROM Cliente c WHERE c.ruc=:cod")
					.setParameter("cod", String.valueOf(codCliente))
					.getSingleResult();
			return cliente;
		} catch (Exception ex) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Cliente getByCod(int codCli) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			Cliente cli = (Cliente) em
					.createQuery("FROM Cliente c WHERE c.codCliente=:cod")
					.setParameter("cod", codCli).getSingleResult();
			return cli;
		} catch (Exception ex) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizarNomApeRuc(String nombre, String apellido, String ruc,
			String telefono, String celular, long id, String usuMod,
			Timestamp fechaMod) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente cli = em.find(Cliente.class, id);
			cli.setNombre(nombre);
			cli.setRuc(ruc);
			cli.setApellido(apellido);
			cli.setUsuMod(usuMod);
			cli.setFechaMod(fechaMod);
			cli.setTelefono(telefono);
			cli.setTelefono2(celular);
			Pais pais = new Pais();
			pais.setIdPais(0L);
			Departamento dpto = new Departamento();
			dpto.setIdDepartamento(0l);
			Ciudad ciu = new Ciudad();
			ciu.setIdCiudad(0l);
			Barrio barr = new Barrio();
			barr.setIdBarrio(0l);
			cli.setPais(pais);
			cli.setDepartamento(dpto);
			cli.setCiudad(ciu);
			cli.setBarrio(barr);
			em.merge(cli);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Cliente insertarDatos(Cliente cli) {
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(cli);
			// em.getTransaction().commit();
			em.refresh(cli);
		} catch (Exception e) {
			System.out.println("INS --> " + e.getLocalizedMessage());
			System.out.println("INS --> " + e.fillInStackTrace());
			cli = null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return cli;
	}

	@Override
	public Cliente actualizarDatos(Cliente cli) {
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.merge(cli);
			// em.getTransaction().commit();
//			em.refresh(cli);
		} catch (Exception e) {
			System.out.println("ACT --> " + e.getLocalizedMessage());
			System.out.println("ACT --> " + e.fillInStackTrace());
			cli = null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
		return cli;
	}
}
