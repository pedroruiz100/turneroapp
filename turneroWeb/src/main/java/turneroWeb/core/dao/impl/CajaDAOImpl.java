/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.CajaDAO;
import turneroWeb.core.domain.Caja;
import turneroWeb.core.utiles.CryptoBack;

/**
 *
 * @author ADMIN
 */
@Repository
public class CajaDAOImpl implements CajaDAO {

	@PersistenceContext
	private EntityManager em;
	private Caja caja;

	@Override
	public List<Caja> listar() {
		return em.createQuery("FROM Caja c").getResultList();
	}

	@Override
	public Caja getById(long id) {
		return (Caja) em.find(Caja.class, id);
	}

	@Override
	public void insertar(Caja obj) {
		em.persist(obj);
	}

	@Override
	public void actualizar(Caja obj) {
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		Caja ca = em.find(Caja.class, id);
		em.remove(ca);
	}

	@Override
	public Caja buscarCaja(Integer nroCaja, String clave)
			throws NoSuchAlgorithmException {

		CryptoBack cb = new CryptoBack();

		caja = (Caja) em
				.createQuery(
						"FROM Caja c "
								+ " JOIN FETCH c.ipBoca ip "
								+ " where (c.nroCaja = :nro)"
								+ " and (c.claveCaja = :clave) and (c.activo = true)"
								+ "")
				.setParameter("nro", nroCaja)
				.setParameter("clave", cb.getHash(clave)).getSingleResult();
		
		return caja;
	}

}
