package turneroWeb.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaClienteCabNotaCreditoDAO;
import turneroWeb.core.domain.FacturaClienteCabNotaCredito;

@Repository
public class FacturaClienteCabNotaCreditoDAOImpl implements
		FacturaClienteCabNotaCreditoDAO {
	
	@PersistenceContext
	private EntityManager em;

	 @Override
	    public List<FacturaClienteCabNotaCredito> listar() {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
	            return em.createQuery("FROM FacturaClienteCabNotaCredito f ORDER BY f.idFacturaClienteCabNotaCredito ASC").getResultList();
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }

	    @Override
	    public FacturaClienteCabNotaCredito getById(long id) {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
	            return em.find(FacturaClienteCabNotaCredito.class, id);
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }

	    @Override
	    public void insertar(FacturaClienteCabNotaCredito obj) {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
//	            em.getTransaction().begin();
	            em.persist(obj);
//	            em.getTransaction().commit();
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }

	    @Override
	    public void actualizar(FacturaClienteCabNotaCredito obj) {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
//	            em.getTransaction().begin();
	            em.merge(obj);
//	            em.getTransaction().commit();
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }

	    @Override
	    public void eliminar(long id) {
	        // TODO Auto-generated method stub
	    }

	    @Override
	    public FacturaClienteCabNotaCredito insertarObtenerObjeto(FacturaClienteCabNotaCredito fac) {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
//	            em.getTransaction().begin();
	            em.persist(fac);
//	            em.getTransaction().commit();
	            em.refresh(fac);
	            return fac;
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }

	    @Override
	    public List<FacturaClienteCabNotaCredito> listarPorFactura(long parseLong) {
//	        EntityManager em = null;
	        try {
//	            em = getEmf().createEntityManager();
	            return em
	                    .createQuery(
	                            "FROM FacturaClienteCabNotaCredito fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
	                    .setParameter("idFac", parseLong)
	                    .getResultList();
	        } catch (Exception ex) {
	            System.out.println("-->> " + ex.getLocalizedMessage());
	            System.out.println("-->> " + ex.fillInStackTrace());
	            return new ArrayList<>();
	        } finally {
	            if (em != null) {
	                em.close();
	            }
	        }
	    }
	}
