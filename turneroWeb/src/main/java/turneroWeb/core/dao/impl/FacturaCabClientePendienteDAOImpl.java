package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaCabClientePendienteDAO;
import turneroWeb.core.domain.FacturaCabClientePendiente;

@Repository
public class FacturaCabClientePendienteDAOImpl implements FacturaCabClientePendienteDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<FacturaCabClientePendiente> listar() {
		try {
			return em
					.createQuery(
							"FROM FacturaCabClientePendiente fac ORDER BY fac.idFactPendiente")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public FacturaCabClientePendiente getById(long id) {
		try {
			return em.find(FacturaCabClientePendiente.class, id);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void insertar(FacturaCabClientePendiente obj) {
		try {
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(FacturaCabClientePendiente obj) {
		try {
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		EntityManager em = null;
		try {
			FacturaCabClientePendiente fac = em.find(
					FacturaCabClientePendiente.class, id);
			em.remove(fac);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public FacturaCabClientePendiente insertarObtenerObj(
			FacturaCabClientePendiente obj) {
		try {
			em.persist(obj);
			em.refresh(obj);
			return obj;
		} catch (Exception ex) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}
}
