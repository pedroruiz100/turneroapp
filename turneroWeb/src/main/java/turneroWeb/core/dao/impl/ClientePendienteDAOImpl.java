package turneroWeb.core.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ClientePendienteDAO;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.ClientePendiente;
import turneroWeb.core.dto.ClientePendienteDTO;

@Repository
public class ClientePendienteDAOImpl implements ClientePendienteDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ClientePendiente> listar() {
		EntityManager em = null;
		try {
			return em
					.createQuery(
							"FROM ClientePendiente cli ORDER BY cli.idClientePendiente")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ClientePendiente getById(long id) {
		// EntityManager em = null;
		// try {
		// return (ClientePendiente) em
		// .createQuery(
		// "FROM ClientePendiente cli WHERE cli.idClientePendiente=:idCli")
		// .setParameter("idCli", id).getSingleResult();
		// } finally {
		// if (em != null) {
		// em.close();
		// }
		// }

		try {
			ClientePendiente c = em.find(ClientePendiente.class, id);
			return c;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(ClientePendiente obj) {
		EntityManager em = null;
		try {
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(ClientePendiente obj) {
		try {
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		try {
			ClientePendiente cli = em.find(ClientePendiente.class, id);
			em.remove(cli);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ClientePendiente insertarObtenerObj(ClientePendiente obj) {
		try {
			em.persist(obj);
			em.refresh(obj);
			return obj;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<ClientePendiente> listarPendiente() {
		try {
			return em
					.createQuery(
							"FROM ClientePendiente cli JOIN FETCH cli.cliente cliente WHERE cli.procesado=false ORDER BY cli.idClientePendiente")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	@SuppressWarnings("JPQLValidation")
	public boolean actualizarEstado(long idCliPen, Timestamp timestamp,
			String nombreCaj) {
		try {
			em.createQuery(
					"UPDATE ClientePendiente cp SET cp.procesado=true, cp.usuMod=:uMod, cp.fechaMod=:fMod WHERE cp.idClientePendiente=:idCliPen")
					.setParameter("uMod", nombreCaj)
					.setParameter("fMod", timestamp)
					.setParameter("idCliPen", idCliPen).executeUpdate();
			return true;
		} catch (Exception e) {
			System.out.println("Exception " + e.fillInStackTrace());
			return false;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ClientePendiente listarPorCi(String ci) {
		try {
			return (ClientePendiente) em
					.createQuery(
							"FROM ClientePendiente cli JOIN FETCH cli.cliente cliente WHERE cliente.ruc LIKE :dato")
					.setParameter("dato", ci + "%").getSingleResult();
		} catch (Exception e) {
			System.out.println("Exception " + e.fillInStackTrace());
			return new ClientePendiente();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public boolean actualizarEstadoNuevo(long idCliPen, ClientePendiente clipen) {
		try {
			em.merge(clipen);
			// em.refresh(clipen);
			// System.out.println("ESTOY AQUI " + clipen.getProcesado());
			return true;
		} catch (Exception e) {
			// System.out.println("ACA MIRAR -->> " + e.getLocalizedMessage());
			return false;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ClientePendiente insertarObj(ClientePendiente clipen) {
		// EntityManager em = null;
		try {
			em.persist(clipen);
			// em.refresh(clipen);
			return clipen;
		} catch (Exception e) {
			System.out.println("ACA MIRAR I -->> " + e.getLocalizedMessage());
			System.out.println("ACA MIRAR II -->> " + e.getStackTrace());
			System.out.println("ACA MIRAR III -->> " + e.getMessage());
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizarEstado(long idCP) {
		em.createNativeQuery(
				"UPDATE estetica.cliente_pendiente SET procesado=" + true
						+ " WHERE id_cliente_pendiente=" + idCP)
				.executeUpdate();
	}
}
