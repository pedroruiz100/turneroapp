/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.RetiroPedidoDAO;
import turneroWeb.core.domain.RetiroPedido;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class RetiroPedidoDAOImpl implements RetiroPedidoDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<RetiroPedido> listar() {
		return em.createQuery("Select b from RetiroPedido b").getResultList();
	}

	@Override
	public RetiroPedido getById(long id) {
		return (RetiroPedido) em
				.createQuery(
						"Select b from RetiroPedido b JOIN FETCH b.facturaClienteCab fcc "
								+ " WHERE b.idRetiro=:idBa")
				.setParameter("idBa", id).getSingleResult();
	}

	@Override
	public void insertar(RetiroPedido obj) {
		em.persist(obj);
	}

	@Override
	public void actualizar(RetiroPedido obj) {
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		RetiroPedido ba = em.find(RetiroPedido.class, id);
		em.remove(ba);
	}

	@Override
	public Long rowCount() {
		String cadena = "SELECT count(1) FROM general.RetiroPedido b";
		String fila = String.valueOf(em.createNativeQuery(cadena)
				.getSingleResult());
		Long row = Long.valueOf(fila);
		return row;
	}

	@Override
	public List<RetiroPedido> listarNoFetch(long limRow, long offSet) {

		return em.createQuery("SELECT B FROM RetiroPedido B")
				.setMaxResults((int) limRow)// LIMIT
				// "+limRow+"
				.setFirstResult((int) offSet)// OFFSET "+offSet
				.getResultList();
	}

	@Override
	public List<RetiroPedido> listarFETCH(long limRow, long offSet) {
		return em
				.createQuery(
						"SELECT B FROM RetiroPedido B JOIN FETCH B.ciudad as C")
				.setMaxResults((int) limRow)// LIMIT
				// "+limRow+"
				.setFirstResult((int) offSet)// OFFSET "+offSet
				.getResultList();
	}

	@Override
	public List<RetiroPedido> listarPreparacion() {
		return em
				.createQuery(
						"Select b from RetiroPedido b WHERE b.entregado=FALSE AND b.preparacion=TRUE AND listo=FALSE ORDER BY b.idRetiro ASC")
				// .setMaxResults(16).getResultList();
				.setMaxResults(12).getResultList();
	}

	@Override
	public List<RetiroPedido> listarListo() {
		return em
				.createQuery(
						"Select b from RetiroPedido b WHERE b.entregado=FALSE AND b.preparacion=TRUE AND listo=TRUE ORDER BY b.idRetiro ASC")
				.setMaxResults(9).getResultList();
	}

	@Override
	public List<RetiroPedido> listarFullListo() {
		return em
				.createQuery(
						"Select b from RetiroPedido b WHERE b.entregado=FALSE AND b.preparacion=TRUE AND listo=TRUE ORDER BY b.idRetiro ASC")
				// .setMaxResults(3)
				.getResultList();
	}

	@Override
	public List<RetiroPedido> listarFullPreparacion() {
		return em
				.createQuery(
						"Select b from RetiroPedido b WHERE b.entregado=FALSE AND b.preparacion=TRUE AND listo=FALSE ORDER BY b.idRetiro ASC")
				// .setMaxResults(6)
				.getResultList();
	}

	@Override
	public RetiroPedido buscarPorNumeroPedido(String num) {
		return (RetiroPedido) em
				.createQuery(
						"Select b from RetiroPedido b JOIN FETCH b.facturaClienteCab fcc "
								+ " WHERE UPPER(b.numero)=:idBa")
				.setParameter("idBa", num.toUpperCase()).getSingleResult();
	}

}
