package turneroWeb.core.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaClienteCabDAO;
import turneroWeb.core.domain.EstadoFactura;
import turneroWeb.core.domain.FacturaClienteCab;

@Repository
public class FacturaClienteCabDAOImpl implements FacturaClienteCabDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<FacturaClienteCab> listar() {
		return em.createQuery("FROM FacturaClienteCab fcc").getResultList();
	}

	@Override
	public FacturaClienteCab getById(long id) {
		// TODO Auto-generated method stub
		// return em.find(FacturaClienteCab.class, id);
		return (FacturaClienteCab) em
				.createQuery(
						"FROM FacturaClienteCab fcc WHERE fcc.idFacturaClienteCab="
								+ id).setMaxResults(1).getSingleResult();
	}

	@Override
	public void insertar(FacturaClienteCab obj) {
		em.persist(obj);
	}

	@Override
	public void actualizar(FacturaClienteCab obj) {
		em.merge(obj);

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
		em.remove(fac);
	}

	@Override
	public FacturaClienteCab insertarObtenerObjeto(
			FacturaClienteCab facturaClienteCab) {

		em.persist(facturaClienteCab);

		// em.refresh(facturaClienteCab);

		return facturaClienteCab;
	}

	@Override
	public void cancelarFactura(long id, String usuario) {
		java.util.Date date = new java.util.Date();
		Timestamp tiempoActual = new Timestamp(date.getTime());

		EstadoFactura ef = new EstadoFactura();
		ef.setIdEstadoFactura(2L);

		FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
		fac.setFechaMod(tiempoActual);
		fac.setEstadoFactura(ef);
		fac.setUsuMod(usuario);

		em.merge(fac);

	}

	@Override
	public void actualizarMontoVenta(int monto, long id) {
		FacturaClienteCab fac = em.find(FacturaClienteCab.class, id);
		fac.setMontoFactura(monto);

		em.merge(fac);
	}

	@Override
	public FacturaClienteCab actualizarObtenerObjeto(
			FacturaClienteCab facturaClienteCab) {

		em.merge(facturaClienteCab);
		// em.refresh(facturaClienteCab);

		return facturaClienteCab;
	}

	@Override
	public List<FacturaClienteCab> filtroFecha(String fechaDesde,
			String fechaHasta, String nroCaja) {
		return em
				.createQuery(
						"FROM FacturaClienteCab fcc JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
								+ "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin")
				.setParameter("fecIni", fechaDesde)
				.setParameter("fecFin", fechaHasta)
				.setParameter("numCaja", Integer.parseInt(nroCaja))
				.getResultList();
	}

	@Override
	public List<FacturaClienteCab> filtroFechaDescuento(String fechaDesde,
			String fechaHasta, String nroCaja) {
		return em
				.createQuery(
						"FROM FacturaClienteCab fcc "
								+ "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
								+ "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin ORDER BY fcc.idFacturaClienteCab")
				.setParameter("fecIni", fechaDesde)
				.setParameter("fecFin", fechaHasta)
				.setParameter("numCaja", Integer.parseInt(nroCaja))
				.getResultList();
	}

	@Override
	public List<FacturaClienteCab> filtroNroFact(String nroFactura,
			String nroCaja) {
		return em
				.createQuery(
						"FROM FacturaClienteCab fcc "
								+ "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and fcc.nroFactura= :nroFact")
				.setParameter("nroFact", nroFactura)
				.setParameter("numCaja", Integer.parseInt(nroCaja))
				.getResultList();
	}

	@Override
	public List<FacturaClienteCab> filtroFechaDescuentoFact(String fechaDesde,
			String fechaHasta, String nroCaja, String nroFact) {
		if (nroFact.equalsIgnoreCase("null")) {
			nroFact = "%%";
		} else {
			nroFact = "%" + nroFact.toLowerCase() + "%";
		}
		return em
				.createQuery(
						"FROM FacturaClienteCab fcc "
								+ "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
								+ "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin "
								+ "and fcc.nroFactura like :nroFact ORDER BY fcc.fechaEmision ")
				.setParameter("fecIni", fechaDesde)
				.setParameter("fecFin", fechaHasta)
				.setParameter("numCaja", Integer.parseInt(nroCaja))
				.setParameter("nroFact", nroFact).getResultList();
	}

	@Override
	public List<JSONObject> recuperarLaguna(String fechaInicio,
			String fechaFin, String nroCaja) {
		String sql = "select fcc.id_factura_cliente_cab as \"ID\", fcc.nro_factura as \"NRO. FACT.\",  "
				+ "COALESCE((fcc.nro_factura::bigint - lag(fcc.nro_factura::bigint) over (order by fcc.nro_factura::bigint asc))::text, 'N/A') as \"SALTO\", "
				+ "c.nro_caja as \"CAJA\", fcc.usu_alta as \"CAJERO\", fcc.fecha_emision as \"FECHA\", "
				+ "COALESCE((fcc.fecha_emision - lag(fcc.fecha_emision) over (order by fcc.fecha_emision asc))::text, 'N/A') as \"LAGUNA\" FROM factura_cliente.factura_cliente_cab fcc "
				+ "LEFT JOIN caja.caja c ON c.id_caja= fcc.id_caja WHERE fcc.fecha_emision>='"
				+ fechaInicio
				+ "' AND fcc.fecha_emision<='"
				+ fechaFin
				+ "' AND c.nro_caja=" + nroCaja;
		System.out.println("SQL -> " + sql);
		List<Object[]> results = this.em.createNativeQuery(sql).getResultList();

		// results.stream().forEach((record) -> {
		// Long id = ((Long) record[0]).longValue();
		// String nroFact = (String) record[1];
		// String salto = (String) record[2];
		// Integer caja = (Integer) record[3];
		// String cajero = (String) record[4];
		// Timestamp fecha = (Timestamp) record[5];
		// String laguna = (String) record[6];
		// System.out.println("ID -> " + id);
		// System.out.println("NRO FACT -> " + nroFact);
		// System.out.println("SALTO -> " + salto);
		// System.out.println("CAJA -> " + caja);
		// System.out.println("CAJERO -> " + cajero);
		// System.out.println("FECHA -> " + fecha);
		// System.out.println("LAGUNA -> " + laguna);
		// });
		return null;
	}

	@Override
	public void actualizarNativo(long idFCC, int monto) {
		em.createNativeQuery(
				"UPDATE factura_cliente.factura_cliente_cab SET monto_factura="
						+ monto + " WHERE id_factura_cliente_cab=" + idFCC)
				.executeUpdate();
	}
}
