package turneroWeb.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ServPendienteDAO;
import turneroWeb.core.domain.ServPendiente;
import turneroWeb.core.utiles.Utilidades;

@Repository
public class ServPendienteDAOImpl implements ServPendienteDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ServPendiente> listar() {

		try {
			return em
					.createQuery(
							"FROM ServPendiente sp JOIN FETCH sp.articulo art ORDER BY sp.idServPendiente")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ServPendiente getById(long id) {

		try {
			return em.find(ServPendiente.class, id);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void insertar(ServPendiente obj) {

		try {
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(ServPendiente obj) {
		try {
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {

		try {
			ServPendiente sp = em.find(ServPendiente.class, id);
			em.remove(sp);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public ServPendiente insertarObtenerObj(ServPendiente obj) {
		try {
			em.persist(obj);
			em.refresh(obj);
			return obj;
		} catch (Exception ex) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<ServPendiente> buscandoServiciosAsignados(long idCP, long idFu) {
		try {
			if (idCP != -1) {
				return em
						.createQuery(
								"FROM ServPendiente sp JOIN FETCH sp.articulo art JOIN FETCH sp.clientePendiente cp JOIN FETCH sp.funcionario f "
										+ "WHERE art.servicio = :serv and cp.idClientePendiente = :idCP and cp.procesado = :proc and f.idFuncionario = :idFu order by art.descripcion")
						.setParameter("serv", true).setParameter("idCP", idCP)
						.setParameter("proc", false).setParameter("idFu", idFu)
						.getResultList();
			} else {
				return new ArrayList<ServPendiente>();
			}
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	@SuppressWarnings("JPQLValidation")
	public List<ServPendiente> buscandoServiciosComision(long idFu,
			String fechaInicio, String fechaFin) {

		try {
			return em
					.createQuery(
							"FROM ServPendiente sp JOIN FETCH sp.articulo a JOIN FETCH sp.clientePendiente cp "
									+ "JOIN FETCH sp.funcionario f JOIN FETCH cp.cliente c JOIN FETCH cp.facturaCabClientePendiente fccp "
									+ "JOIN FETCH fccp.facturaClienteCab fcc "// JOIN
																				// FETCH
																				// fcc.estadoFactura
																				// ef
																				// "
									+ "WHERE cp.procesado = :proc "// AND
																	// upper(ef.descripcion)
																	// LIKE
																	// :estado "
									+ "AND f.idFuncionario = :idFu AND sp.montoComision > :montoComision "
									+ "AND (to_date(to_char(fcc.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY') BETWEEN :inicio AND :fin) "
									+ "order by fcc.fechaEmision ASC")
					.setParameter("proc", true)
					.setParameter("idFu", idFu)
					// .setParameter("estado", "NORMAL")
					.setParameter("montoComision", 0)
					.setParameter("inicio",
							Utilidades.stringToSqlDate(fechaInicio))
					.setParameter("fin", Utilidades.stringToSqlDate(fechaFin))
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<ServPendiente> buscandoServiciosComisionOrderArt(long idFu,
			String fechaInicio, String fechaFin) {

		try {
			// return
			// em.createQuery("FROM ServPendiente sp JOIN FETCH sp.articulo a JOIN FETCH sp.clientePendiente cp "
			// + "JOIN FETCH sp.funcionario f JOIN FETCH cp.cliente c "//JOIN
			// FETCH cp.facturaCabClientePendiente fccp "
			// // + "JOIN FETCH fccp.facturaClienteCab fcc "//JOIN FETCH
			// fcc.estadoFactura ef "
			// + "WHERE cp.procesado = :proc "
			// +
			// "AND f.idFuncionario = :idFu AND sp.montoComision > :montoComision "
			// +
			// "AND (to_date(to_char(cp.fechaMod, 'DD-MON-YY'), 'DD-MON-YY') BETWEEN :inicio AND :fin) "
			// + "order by a.descripcion ASC")
			return em
					.createQuery(
							"FROM ServPendiente sp JOIN FETCH sp.articulo a JOIN FETCH sp.clientePendiente cp "
									+ "JOIN FETCH sp.funcionario f JOIN FETCH cp.cliente c JOIN FETCH cp.facturaCabClientePendiente fccp "
									+ "JOIN FETCH fccp.facturaClienteCab fcc "// JOIN
																				// FETCH
																				// fcc.estadoFactura
																				// ef
																				// "
									+ "WHERE cp.procesado = :proc "// AND
																	// upper(ef.descripcion)
																	// LIKE
																	// :estado "
									+ "AND f.idFuncionario = :idFu AND sp.montoComision > :montoComision "
									+ "AND (to_date(to_char(fcc.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY') BETWEEN :inicio AND :fin) "
									+ "order by a.descripcion ASC")
					.setParameter("proc", true)
					.setParameter("idFu", idFu)
					// .setParameter("estado", "NORMAL")
					.setParameter("montoComision", 0)
					.setParameter("inicio",
							Utilidades.stringToSqlDate(fechaInicio))
					.setParameter("fin", Utilidades.stringToSqlDate(fechaFin))
					.getResultList();
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
			System.out.println("-->> " + ex.fillInStackTrace());
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminarPorIdClientePendiente(long idCP) {
		em.createNativeQuery(
				"DELETE FROM estetica.serv_pendiente WHERE id_cliente_pendiente="
						+ idCP).executeUpdate();
	}

}
