package turneroWeb.core.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ArqueoCajaDAO;
import turneroWeb.core.domain.ArqueoCaja;
import turneroWeb.core.utiles.Utilidades;

@Repository
public class ArqueoCajaDAOImpl implements ArqueoCajaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ArqueoCaja> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArqueoCaja getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(ArqueoCaja obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizar(ArqueoCaja obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArqueoCaja insertarObtenerObj(ArqueoCaja ar) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int recuperarZeta(long idCaja, String nro) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean insertarObtenerEstado(ArqueoCaja fromArqueoCajaDTO) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long recuperarNumMaxZeta() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String recuperarPorCajaFecha(long idCaja, String toString) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArqueoCaja recuperarPorCajaFechaObj(long id, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ArqueoCaja> filtroFecha(String fechaInicio, String fechaFin) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean verificarArqueoDia() {
		boolean val = false;
		try {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			em.createQuery(
					"FROM ArqueoCaja ap where (to_date(to_char(ap.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
					.setParameter("fec",
							Utilidades.stringToUtilDate(format.format(date)))
					.getResultList();
			val = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			val = false;
		} finally {
			em.close();
		}
		return val;
	}

	@Override
	public long getCantidadArqueoDia() {
		// EntityManager em = getEmf().createEntityManager();
		long val = 0l;
		try {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			return em
					.createQuery(
							"FROM ArqueoCaja ap where (to_date(to_char(ap.fechaEmision, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
					.setParameter("fec",
							Utilidades.stringToUtilDate(format.format(date)))
					.getResultList().size();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return 0l;
		} finally {
			em.close();
		}
	}

}
