/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ArticuloDAO;
import turneroWeb.core.domain.Articulo;
import turneroWeb.core.domain.Cliente;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class ArticuloDAOImpl implements ArticuloDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Articulo> listar() {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Articulo> ls = em.createQuery(
					"FROM Articulo c ORDER BY c.descripcion").getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public Articulo getById(long id) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Articulo c = em.find(Articulo.class, id);
			return c;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Articulo obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.persist(obj);
			em.getTransaction().commit();
			em.refresh(obj);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}

	}

	@Override
	public void actualizar(Articulo obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.merge(obj);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente c = em.find(Cliente.class, id);
			em.remove(c);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<Articulo> listarPorSecciones(long id) {
		try {
			List<Articulo> ls = em
					.createQuery(
							"FROM Articulo c JOIN FETCH c.seccion sec WHERE sec.idSeccion=:idSec AND c.servicio = TRUE ORDER BY c.descripcion")
					.setParameter("idSec", id).getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public Articulo listarPorCodigo(String id) {
		try {
			Articulo ls = (Articulo) em
					.createQuery("FROM Articulo c WHERE c.codArticulo=:idSec")
					.setParameter("idSec", id).getSingleResult();
			// Articulo ls = (Articulo) em
			// .createQuery(
			// "FROM Articulo c JOIN FETCH c.seccion sec WHERE c.codArticulo=:idSec")
			// .setParameter("idSec", id).getSingleResult();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public List<Articulo> listarPorDescripcion(String descri) {
		try {
			List<Articulo> ls = em
					.createQuery(
							"FROM Articulo a WHERE UPPER(a.descripcion) LIKE :descri AND a.servicio=TRUE ORDER BY a.descripcion")
					.setParameter("descri", "%" + descri.toUpperCase() + "%")
					.setMaxResults(30).getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public boolean actualizarCantidad(long id, long canti) {
		try {
			em.createNativeQuery(
					"UPDATE stock.articulo SET stock_actual=" + canti + ""
							+ " WHERE id_articulo=" + id).executeUpdate();
			return true;
		} catch (Exception e) {
			return false;
			// TODO: handle exception
		} finally {
		}
	}

}
