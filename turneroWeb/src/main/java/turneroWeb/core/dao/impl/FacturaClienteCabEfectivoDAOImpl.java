package turneroWeb.core.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaClienteCabEfectivoDAO;
import turneroWeb.core.domain.FacturaClienteCabEfectivo;

@Repository
public class FacturaClienteCabEfectivoDAOImpl implements
		FacturaClienteCabEfectivoDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<FacturaClienteCabEfectivo> listar() {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return em.createQuery("FROM FacturaClienteCabEfectivo f")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public FacturaClienteCabEfectivo getById(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return (FacturaClienteCabEfectivo) em
					.createQuery(
							"FROM FacturaClienteCabEfectivo f WHERE f.idFacturaClienteCabEfectivo =:idFac")
					.setParameter("idFac", id).getSingleResult();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void insertar(FacturaClienteCabEfectivo obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(obj);
			// em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(FacturaClienteCabEfectivo obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.merge(obj);
			// em.getTransaction().commit();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
	}

	@Override
	public FacturaClienteCabEfectivo insertarObtenerObjeto(
			FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			// em.getTransaction().begin();
			em.persist(facturaClienteCabEfectivo);
			// em.getTransaction().commit();
			// em.refresh(facturaClienteCabEfectivo);
			return facturaClienteCabEfectivo;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<FacturaClienteCabEfectivo> listarPorFactura(long parseLong) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			return em
					.createQuery(
							"FROM FacturaClienteCabEfectivo fact JOIN FETCH fact.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:idFac")
					.setParameter("idFac", parseLong).getResultList();
		} catch (Exception ex) {
			System.out.println("-->> " + ex.getLocalizedMessage());
			System.out.println("-->> " + ex.fillInStackTrace());
			return new ArrayList<>();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
