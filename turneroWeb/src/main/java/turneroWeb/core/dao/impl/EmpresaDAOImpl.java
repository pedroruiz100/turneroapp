/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.EmpresaDAO;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.Empresa;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class EmpresaDAOImpl implements EmpresaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Empresa> listar() {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Empresa> ls = em.createQuery("FROM Empresa c").getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public Empresa getById(long id) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Empresa c = em.find(Empresa.class, id);
			return c;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Empresa obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.persist(obj);
			em.getTransaction().commit();
			em.refresh(obj);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}

	}

	@Override
	public void actualizar(Empresa obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.merge(obj);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente c = em.find(Cliente.class, id);
			em.remove(c);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
