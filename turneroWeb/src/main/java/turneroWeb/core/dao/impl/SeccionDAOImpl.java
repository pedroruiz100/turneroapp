/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.SeccionDAO;
import turneroWeb.core.domain.Cliente;
import turneroWeb.core.domain.Seccion;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class SeccionDAOImpl implements SeccionDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Seccion> listar() {
		// EntityManager em = getEmf().createEntityManager();
		try {
			List<Seccion> ls = em.createQuery(
					"FROM Seccion c ORDER BY c.descripcion").getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public Seccion getById(long id) {
		// EntityManager em = getEmf().createEntityManager();
		try {
			Seccion c = em.find(Seccion.class, id);
			return c;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void insertar(Seccion obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.persist(obj);
			em.getTransaction().commit();
			em.refresh(obj);
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}

	}

	@Override
	public void actualizar(Seccion obj) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			em.merge(obj);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// EntityManager em = null;
		try {
			// em = getEmf().createEntityManager();
			em.getTransaction().begin();
			Cliente c = em.find(Cliente.class, id);
			em.remove(c);
			em.getTransaction().commit();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public List<Seccion> fullActivos() {
		try {
			List<Seccion> ls = em
					.createQuery(
							"FROM Seccion c WHERE c.estado=TRUE ORDER BY c.descripcion")
					.getResultList();
			return ls;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return null;
		} finally {
			em.close();
		}
	}

}
