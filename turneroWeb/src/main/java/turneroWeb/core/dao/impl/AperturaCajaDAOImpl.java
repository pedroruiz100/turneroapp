package turneroWeb.core.dao.impl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.AperturaCajaDAO;
import turneroWeb.core.domain.AperturaCaja;
import turneroWeb.core.utiles.Utilidades;

@Repository
public class AperturaCajaDAOImpl implements AperturaCajaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<AperturaCaja> listar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AperturaCaja getById(long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void insertar(AperturaCaja obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actualizar(AperturaCaja obj) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public AperturaCaja insertarObtenerObj(AperturaCaja aperturaCaja) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String recuperarPorCajaFecha(long idCaja, String fecha) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public AperturaCaja recuperarPorCaja(Long idCaja, int x) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean insertarRecuperarEstado(AperturaCaja ac) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verificarAperturaDia() {
		boolean val = false;
		try {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			em.createQuery(
					"FROM AperturaCaja ap where (to_date(to_char(ap.fechaApertura, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
					.setParameter("fec",
							Utilidades.stringToUtilDate(format.format(date)))
					.getResultList();
			val = true;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			val = false;
		} finally {
			em.close();
		}
		return val;
	}

	@Override
	public long getCantidadAperturaDia() {
		// EntityManager em = getEmf().createEntityManager();
		long val = 0l;
		try {
			Date date = new Date();
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			return em
					.createQuery(
							"FROM AperturaCaja ap where (to_date(to_char(ap.fechaApertura, 'DD-MON-YY'), 'DD-MON-YY'))=:fec")
					.setParameter("fec",
							Utilidades.stringToUtilDate(format.format(date)))
					.getResultList().size();
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return 0l;
		} finally {
			em.close();
		}
	}

}
