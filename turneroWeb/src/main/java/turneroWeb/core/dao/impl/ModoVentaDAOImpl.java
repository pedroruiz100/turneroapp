/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.ModoVentaDAO;
import turneroWeb.core.domain.ModoVenta;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class ModoVentaDAOImpl implements ModoVentaDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<ModoVenta> listar() {
		return em.createQuery("Select b from ModoVenta b ORDER BY b.idVenta")
				.getResultList();
	}

	@Override
	public ModoVenta getById(long id) {
		return (ModoVenta) em
				.createQuery("Select b from ModoVenta b WHERE b.idVenta=:idBa")
				.setParameter("idBa", id).getSingleResult();
	}

	@Override
	public void insertar(ModoVenta obj) {
		em.persist(obj);
	}

	@Override
	public void actualizar(ModoVenta obj) {
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		ModoVenta ba = em.find(ModoVenta.class, id);
		em.remove(ba);
	}

	@Override
	public Boolean sinLogin() {
		try {
			boolean idIva = Boolean.parseBoolean(em
					.createNativeQuery(
							"SELECT sin_login FROM general.modo_venta WHERE id_venta=1").getSingleResult().toString());
			return idIva;
		} catch (Exception e) {
			System.out.println("-->> " + e.getLocalizedMessage());
			return false;
		} finally {
			em.close();
		}
	}
}
