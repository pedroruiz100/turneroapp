package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.FacturaClienteDetDAO;
import turneroWeb.core.domain.FacturaClienteDet;
import turneroWeb.core.dto.FacturaClienteDetDTO;

@Repository
public class FacturaClienteDetDAOImpl implements FacturaClienteDetDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<FacturaClienteDet> listar() {
		return em.createQuery("FROM FacturaClienteDet fcd").getResultList();
	}

	@Override
	public FacturaClienteDet getById(long id) {
		// TODO Auto-generated method stub
		return em.find(FacturaClienteDet.class, id);
	}

	@Override
	public void insertar(FacturaClienteDet obj) {
		// TODO Auto-generated method stub
		em.persist(obj);
	}

	@Override
	public void actualizar(FacturaClienteDet obj) {
		// TODO Auto-generated method stub
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void insercionMasiva(FacturaClienteDet facDet, long i) {
		em.persist(facDet);
		// if (i % 15 == 0) {
		// em.flush();
		// em.clear();
		// }

	}

	@Override
	public List<FacturaClienteDet> filtroFechaDescuentoFact(String fechaDesde,
			String fechaHasta, String nroCaja, String nroFact) {
		if (nroFact.equalsIgnoreCase("null")) {
			nroFact = "%%";
		} else {
			nroFact = "%" + nroFact.toLowerCase() + "%";
		}
		return em
				.createQuery(
						"FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc "
								+ "JOIN FETCH fcc.caja c WHERE c.nroCaja= :numCaja and to_char(fcc.fechaEmision,'YYYY-MM-DD') >= :fecIni "
								+ "and to_char(fcc.fechaEmision,'YYYY-MM-DD') <= :fecFin "
								+ "and fcc.nroFactura like :nroFact ORDER BY fcc.fechaEmision ")
				.setParameter("fecIni", fechaDesde)
				.setParameter("fecFin", fechaHasta)
				.setParameter("numCaja", Integer.parseInt(nroCaja))
				.setParameter("nroFact", nroFact).getResultList();
	}

	@Override
	public FacturaClienteDet listarPorId(long id) {
		try {
			return (FacturaClienteDet) em
					.createQuery(
							"FROM FacturaClienteDet fcd WHERE fcd.idFacturaClienteDet=:id")
					.setParameter("id", id).getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
		}
	}

	@Override
	public List<FacturaClienteDet> listFacturaCab(long id) {
		try {
			return em
					.createQuery(
							"FROM FacturaClienteDet fcd JOIN FETCH fcd.facturaClienteCab fcc WHERE fcc.idFacturaClienteCab=:id")
					.setParameter("id", id).getResultList();
		} catch (Exception e) {
			return null;
		} finally {
		}
	}
}
