/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.SupervisorDAO;
import turneroWeb.core.domain.Supervisor;
import turneroWeb.core.utiles.Utilidades;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class SupervisorDAOImpl implements SupervisorDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Supervisor> listar() {
		try {
			return em.createQuery("FROM Supervisor s ORDER BY s.idSupervisor")
					.getResultList();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Supervisor getById(long id) {
		try {
			return em.find(Supervisor.class, id);
		} finally {
		}
	}

	@Override
	public void insertar(Supervisor obj) {
		try {
			em.persist(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void actualizar(Supervisor obj) {
		try {
			em.getTransaction().begin();
			em.merge(obj);
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
	}

	@Override
	public Supervisor buscarCodSup(String codSup) {
		try {
			return (Supervisor) em
					.createQuery(
							"FROM Supervisor s "
									+ " where "
									+ "(s.codSupervisor = :codSup) and (s.activo = true)")
					.setParameter("codSup", codSup).getSingleResult();
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Supervisor buscarNumeroCodigoSup(int numSup, String codSup) {
		Utilidades cb = new Utilidades();
		try {
			return (Supervisor) em
					.createQuery(
							"FROM Supervisor s"
									+ " where (s.nroSupervisor = :numSup)"
									// +
									// " and (s.codSupervisor = :codSup) and (s.activo = true)")
									+ " and (s.usuario.contrasenha = :codSup) and (s.activo = true)")
					.setParameter("numSup", numSup)
					.setParameter("codSup", cb.getHash(codSup))
					.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

	@Override
	public Supervisor buscarSupervisor(String usuario, String clave) {
		Utilidades cb = new Utilidades();
		try {
			return (Supervisor) em
					.createQuery(
							"FROM Supervisor s"
									+ " where (s.usuario.nomUsuario = :numUsu)"
									// +
									// " and (s.codSupervisor = :codSup) and (s.activo = true)")
									+ " and (s.usuario.contrasenha = :clave) and (s.activo = true)")
					.setParameter("numUsu", usuario.toUpperCase())
					.setParameter("clave", cb.getHash(clave)).getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			if (em != null) {
				em.close();
			}
		}
	}

}
