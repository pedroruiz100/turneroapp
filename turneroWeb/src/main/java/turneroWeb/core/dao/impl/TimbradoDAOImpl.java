package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.TimbradoDAO;
import turneroWeb.core.domain.Timbrado;

@Repository
public class TimbradoDAOImpl implements TimbradoDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Timbrado> listar() {
		return em.createQuery("FROM Timbrado t").getResultList();
	}

	@Override
	public Timbrado getById(long id) {
		// TODO Auto-generated method stub
		return em.find(Timbrado.class, id);
	}

	@Override
	public void insertar(Timbrado obj) {
		// TODO Auto-generated method stub
		em.persist(obj);
	}

	@Override
	public void actualizar(Timbrado obj) {
		// TODO Auto-generated method stub
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub

	}

}
