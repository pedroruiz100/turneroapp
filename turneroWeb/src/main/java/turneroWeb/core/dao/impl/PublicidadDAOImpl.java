/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import turneroWeb.core.dao.PublicidadDAO;
import turneroWeb.core.domain.Publicidad;

/**
 *
 * @author ExcelsisWalker
 */
@Repository
public class PublicidadDAOImpl implements PublicidadDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Publicidad> listar() {
		return em.createQuery("Select b from Publicidad b ORDER BY b.idPublicidad").getResultList();
	}

	@Override
	public Publicidad getById(long id) {
		return (Publicidad) em
				.createQuery(
						"Select b from Publicidad b WHERE b.idPublicidad=:idBa")
				.setParameter("idBa", id).getSingleResult();
	}

	@Override
	public void insertar(Publicidad obj) {
		em.persist(obj);
	}

	@Override
	public void actualizar(Publicidad obj) {
		em.merge(obj);
	}

	@Override
	public void eliminar(long id) {
		Publicidad ba = em.find(Publicidad.class, id);
		em.remove(ba);
	}
	//

	@Override
	public List<Publicidad> listarPublicidadTRUE() {
		return em.createQuery("Select b from Publicidad b WHERE b.estado=true ORDER BY b.idPublicidad").getResultList();
	}
}
