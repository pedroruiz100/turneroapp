/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.security.NoSuchAlgorithmException;

import turneroWeb.core.domain.Caja;

public interface CajaDAO extends CRUDGenericDAO<Caja>{
	Caja buscarCaja(Integer nroCaja, String clave) throws NoSuchAlgorithmException;
}
