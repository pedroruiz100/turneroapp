package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.FacturaClienteCabEfectivo;

public interface FacturaClienteCabEfectivoDAO extends CRUDGenericDAO<FacturaClienteCabEfectivo> {

    FacturaClienteCabEfectivo insertarObtenerObjeto(
            FacturaClienteCabEfectivo facturaClienteCabEfectivo);

    public List<FacturaClienteCabEfectivo> listarPorFactura(long parseLong);

}
