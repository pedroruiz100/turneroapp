package turneroWeb.core.dao;

import java.sql.Timestamp;
import java.util.List;

import turneroWeb.core.domain.ClientePendiente;

public interface ClientePendienteDAO extends CRUDGenericDAO<ClientePendiente> {

	ClientePendiente insertarObtenerObj(ClientePendiente fromClientePendiente);

	List<ClientePendiente> listarPendiente();

	public boolean actualizarEstado(long idCliPen, Timestamp timestamp,
			String nombreCaj);

	ClientePendiente listarPorCi(String ci);

	boolean actualizarEstadoNuevo(long idCliPen, ClientePendiente clipen);

	ClientePendiente insertarObj(ClientePendiente clipen);

	void actualizarEstado(long idCP);

}
