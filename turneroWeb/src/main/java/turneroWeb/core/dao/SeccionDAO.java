/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.Seccion;

/**
 *
 * @author ExcelsisWalker
 */
public interface SeccionDAO extends CRUDGenericDAO<Seccion> {

	List<Seccion> fullActivos();

}
