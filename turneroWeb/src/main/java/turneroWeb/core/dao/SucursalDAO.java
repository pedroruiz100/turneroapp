package turneroWeb.core.dao;

import turneroWeb.core.domain.Sucursal;

public interface SucursalDAO extends CRUDGenericDAO<Sucursal> {

}
