package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.FacturaClienteCabHistorico;

public interface FacturaClienteCabHistoricoDAO extends
		CRUDGenericDAO<FacturaClienteCabHistorico> {

	public List<FacturaClienteCabHistorico> listarPorFactura(
			Long idFacturaClienteCab);

	public FacturaClienteCabHistorico consultar(String nroFactFiltro,
			String tipoFactFiltro, String fechaFactFiltro,
			String nroCajaFiltro, String timbradoFiltro);

	public List<Integer> listarCajaDistinct();

	public List<String> listarTimbDistinct();

	public void insertarActual(
			FacturaClienteCabHistorico fromFacturaClienteCabHistoricoAsociadoDTO,
			long idCab);

}
