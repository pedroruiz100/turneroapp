package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.FacturaClienteDet;

public interface FacturaClienteDetDAO extends CRUDGenericDAO<FacturaClienteDet> {

	void insercionMasiva(FacturaClienteDet facDet, long i);

	List<FacturaClienteDet> filtroFechaDescuentoFact(String fechaDesde,
			String fechaHasta, String nroCaja, String nroFact);

	FacturaClienteDet listarPorId(long id);

	List<FacturaClienteDet> listFacturaCab(long id);

}
