package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.ArqueoCaja;

public interface ArqueoCajaDAO extends CRUDGenericDAO<ArqueoCaja> {

	ArqueoCaja insertarObtenerObj(ArqueoCaja ar);

	int recuperarZeta(long idCaja, String nro);

	boolean insertarObtenerEstado(ArqueoCaja fromArqueoCajaDTO);

	long recuperarNumMaxZeta();

	public String recuperarPorCajaFecha(long idCaja, String toString);

	public ArqueoCaja recuperarPorCajaFechaObj(long id, String fecha);

	public List<ArqueoCaja> filtroFecha(String fechaInicio, String fechaFin);

	public boolean verificarArqueoDia();

	public long getCantidadArqueoDia();

}