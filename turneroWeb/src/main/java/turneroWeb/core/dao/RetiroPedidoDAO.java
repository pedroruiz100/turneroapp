/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.util.List;

import turneroWeb.core.domain.RetiroPedido;
import turneroWeb.core.dto.RetiroPedidoDTO;

/**
 *
 * @author ExcelsisWalker
 */
public interface RetiroPedidoDAO extends CRUDGenericDAO<RetiroPedido> {

	Long rowCount();

	List<RetiroPedido> listarNoFetch(long limRow, long offSet);

	List<RetiroPedido> listarFETCH(long limRow, long offSet);

	List<RetiroPedido> listarPreparacion();

	List<RetiroPedido> listarListo();
	
	List<RetiroPedido> listarFullListo();

	List<RetiroPedido> listarFullPreparacion();

	RetiroPedido buscarPorNumeroPedido(String num);
}
