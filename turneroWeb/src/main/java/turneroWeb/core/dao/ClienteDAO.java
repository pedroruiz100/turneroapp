/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.sql.Timestamp;
import java.util.List;

import turneroWeb.core.domain.Cliente;

/**
 *
 * @author ExcelsisWalker
 */
public interface ClienteDAO extends CRUDGenericDAO<Cliente> {

    List<Cliente> listarPorNomRuc(String nombre, String apellido, String ruc);

    void actualizarNomApeRuc(String nombre, String apellido, String ruc, long id, String usuMod, Timestamp fechaMod);

    Cliente listarPorRuc(String ruc);

    Cliente listarPorCodigo(int codCliente);

    public Cliente getByCod(int codCli);

    public void actualizarNomApeRuc(String toString, String toString0, String toString1, String toString2, String toString3, long parseLong, String nombreCaj, Timestamp timestamp);

	Cliente insertarDatos(Cliente cli);

	Cliente listarPorCI(String cod);

	Cliente actualizarDatos(Cliente cli);

}
