package turneroWeb.core.dao;

import java.util.List;

import org.json.simple.JSONObject;

import turneroWeb.core.domain.FacturaClienteCab;

public interface FacturaClienteCabDAO extends CRUDGenericDAO<FacturaClienteCab> {

	void cancelarFactura(long id, String usuMod);
	
	void actualizarMontoVenta(int monto, long id);

	FacturaClienteCab insertarObtenerObjeto(FacturaClienteCab fac);
	
	FacturaClienteCab actualizarObtenerObjeto(
			FacturaClienteCab facturaClienteCab);

	List<FacturaClienteCab> filtroFecha(String fechaDesde, String fechaHasta, String nroCaja);

	List<FacturaClienteCab> filtroFechaDescuento(String fechaDesde, String fechaHasta,
			String nroCaja);

	List<FacturaClienteCab> filtroNroFact(String nroFactura, String nroCaja);

	List<FacturaClienteCab> filtroFechaDescuentoFact(String fechaDesde, String fechaHasta,
			String nroCaja, String nroFact);

	List<JSONObject> recuperarLaguna(String fechaInicio, String fechaFin, String nroCaja);

	void actualizarNativo(long idFCC, int monto);


}
