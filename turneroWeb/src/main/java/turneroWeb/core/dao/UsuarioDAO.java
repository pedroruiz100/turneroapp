/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package turneroWeb.core.dao;

import java.sql.Timestamp;

import turneroWeb.core.domain.Usuario;

/**
 *
 * @author ExcelsisWalker
 */
public interface UsuarioDAO extends CRUDGenericDAO<Usuario> {

	Usuario busUsuCI(String CI);

	boolean actualizarContrasenha(String contrasenha, long id, String usuMod,
			Timestamp fechaMod);

	Usuario buscarCodSup(String cod);
}
