package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the desc_efectivo database table.
 * 
 */
@Entity
@Table(name = "desc_efectivo", schema = "factura_cliente")
@NamedQuery(name="DescEfectivo.findAll", query="SELECT d FROM DescEfectivo d")
public class DescEfectivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_efectivo")
	private Long idDescEfectivo;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabEfectivo
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_efectivo")
	private FacturaClienteCabEfectivo facturaClienteCabEfectivo;

	//bi-directional many-to-one association to FacturaClienteCabEfectivo
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_efectivo")
//	private FacturaClienteCabEfectivo facturaClienteCabEfectivo2;

	public DescEfectivo() {
	}

	public Long getIdDescEfectivo() {
		return this.idDescEfectivo;
	}

	public void setIdDescEfectivo(Long idDescEfectivo) {
		this.idDescEfectivo = idDescEfectivo;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabEfectivo getFacturaClienteCabEfectivo() {
		return this.facturaClienteCabEfectivo;
	}

	public void setFacturaClienteCabEfectivo(FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
		this.facturaClienteCabEfectivo = facturaClienteCabEfectivo;
	}

	// public FacturaClienteCabEfectivo getFacturaClienteCabEfectivo2() {
	// return this.facturaClienteCabEfectivo2;
	// }
	//
	// public void setFacturaClienteCabEfectivo2(FacturaClienteCabEfectivo
	// facturaClienteCabEfectivo2) {
	// this.facturaClienteCabEfectivo2 = facturaClienteCabEfectivo2;
	// }

}