package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.BarrioDTO;
import turneroWeb.core.dto.ModoVentaDTO;

/**
 * The persistent class for the donacion_cliente database table.
 * 
 */
@Entity
@Table(name = "modo_venta", schema = "general")
@NamedQuery(name = "ModoVenta.findAll", query = "SELECT d FROM ModoVenta d")
public class ModoVenta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_venta")
	private Long idVenta;

	@Column(name = "sin_login")
	private Boolean sinLogin;

	public ModoVenta() {
	}

	public Long getIdVenta() {
		return idVenta;
	}

	public void setIdVenta(Long idVenta) {
		this.idVenta = idVenta;
	}

	public Boolean getSinLogin() {
		return sinLogin;
	}

	public void setSinLogin(Boolean sinLogin) {
		this.sinLogin = sinLogin;
	}

	public static ModoVentaDTO toModoVentaDTO(ModoVenta barrio) {
		ModoVentaDTO ModoVenta = new ModoVentaDTO();
		BeanUtils.copyProperties(barrio, ModoVenta);
		return ModoVenta;
	}
	
	public ModoVentaDTO toModoVentaDTO() {
		ModoVentaDTO barrioDTO = toModoVentaDTO(this);
		return barrioDTO;
	}

	public static ModoVenta fromModoVentaDTO(ModoVentaDTO barrioDTO) {
		ModoVenta barrio = new ModoVenta();
		BeanUtils.copyProperties(barrioDTO, barrio);
		return barrio;
	}

}