package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the factura_cliente_cab_tarj_fiel database table.
 * 
 */
@Entity
@Table(name="factura_cliente_cab_tarj_fiel", schema = "factura_cliente")
@NamedQuery(name="FacturaClienteCabTarjFiel.findAll", query="SELECT f FROM FacturaClienteCabTarjFiel f")
public class FacturaClienteCabTarjFiel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_factura_cliente_cab_tarj_fiel")
	private Long idFacturaClienteCabTarjFiel;

	@Column(name="id_tarjeta_fiel")
	private Long idTarjetaFiel;

	private Integer monto;

	//bi-directional many-to-one association to DescTarjetaFiel
//	@OneToMany(mappedBy="facturaClienteCabTarjFiel1")
//	private List<DescTarjetaFiel> descTarjetaFiels1;
//
//	//bi-directional many-to-one association to DescTarjetaFiel
//	@OneToMany(mappedBy="facturaClienteCabTarjFiel2")
//	private List<DescTarjetaFiel> descTarjetaFiels2;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	//bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	public FacturaClienteCabTarjFiel() {
	}

	public Long getIdFacturaClienteCabTarjFiel() {
		return this.idFacturaClienteCabTarjFiel;
	}

	public void setIdFacturaClienteCabTarjFiel(Long idFacturaClienteCabTarjFiel) {
		this.idFacturaClienteCabTarjFiel = idFacturaClienteCabTarjFiel;
	}

	public Long getIdTarjetaFiel() {
		return this.idTarjetaFiel;
	}

	public void setIdTarjetaFiel(Long idTarjetaFiel) {
		this.idTarjetaFiel = idTarjetaFiel;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	// public List<DescTarjetaFiel> getDescTarjetaFiels1() {
	// return this.descTarjetaFiels1;
	// }
	//
	// public void setDescTarjetaFiels1(List<DescTarjetaFiel> descTarjetaFiels1)
	// {
	// this.descTarjetaFiels1 = descTarjetaFiels1;
	// }
	//
	// public DescTarjetaFiel addDescTarjetaFiels1(DescTarjetaFiel
	// descTarjetaFiels1) {
	// getDescTarjetaFiels1().add(descTarjetaFiels1);
	// descTarjetaFiels1.setFacturaClienteCabTarjFiel1(this);
	//
	// return descTarjetaFiels1;
	// }
	//
	// public DescTarjetaFiel removeDescTarjetaFiels1(DescTarjetaFiel
	// descTarjetaFiels1) {
	// getDescTarjetaFiels1().remove(descTarjetaFiels1);
	// descTarjetaFiels1.setFacturaClienteCabTarjFiel1(null);
	//
	// return descTarjetaFiels1;
	// }
	//
	// public List<DescTarjetaFiel> getDescTarjetaFiels2() {
	// return this.descTarjetaFiels2;
	// }
	//
	// public void setDescTarjetaFiels2(List<DescTarjetaFiel> descTarjetaFiels2)
	// {
	// this.descTarjetaFiels2 = descTarjetaFiels2;
	// }
	//
	// public DescTarjetaFiel addDescTarjetaFiels2(DescTarjetaFiel
	// descTarjetaFiels2) {
	// getDescTarjetaFiels2().add(descTarjetaFiels2);
	// descTarjetaFiels2.setFacturaClienteCabTarjFiel2(this);
	//
	// return descTarjetaFiels2;
	// }
	//
	// public DescTarjetaFiel removeDescTarjetaFiels2(DescTarjetaFiel
	// descTarjetaFiels2) {
	// getDescTarjetaFiels2().remove(descTarjetaFiels2);
	// descTarjetaFiels2.setFacturaClienteCabTarjFiel2(null);
	//
	// return descTarjetaFiels2;
	// }

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}