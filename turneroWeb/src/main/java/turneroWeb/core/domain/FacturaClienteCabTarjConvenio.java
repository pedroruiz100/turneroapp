package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the factura_cliente_cab_tarj_convenio database
 * table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_tarj_convenio", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabTarjConvenio.findAll", query = "SELECT f FROM FacturaClienteCabTarjConvenio f")
public class FacturaClienteCabTarjConvenio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_tarj_convenio")
	private Long idFacturaClienteCabTarjConvenio;

	@Column(name = "descripcion_tarj")
	private String descripcionTarj;

	@Column(name = "id_tarjeta_convenio")
	private Long idTarjetaConvenio;

	private Integer monto;

	// bi-directional many-to-one association to DescTarjetaConvenio
//	@OneToMany(mappedBy = "facturaClienteCabTarjConvenio1")
//	private List<DescTarjetaConvenio> descTarjetaConvenios1;

	// bi-directional many-to-one association to DescTarjetaConvenio
	// @OneToMany(mappedBy = "facturaClienteCabTarjConvenio2")
//	private List<DescTarjetaConvenio> descTarjetaConvenios2;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabTarjConvenio() {
	}

	public Long getIdFacturaClienteCabTarjConvenio() {
		return this.idFacturaClienteCabTarjConvenio;
	}

	public void setIdFacturaClienteCabTarjConvenio(
			Long idFacturaClienteCabTarjConvenio) {
		this.idFacturaClienteCabTarjConvenio = idFacturaClienteCabTarjConvenio;
	}

	public String getDescripcionTarj() {
		return this.descripcionTarj;
	}

	public void setDescripcionTarj(String descripcionTarj) {
		this.descripcionTarj = descripcionTarj;
	}

	public Long getIdTarjetaConvenio() {
		return this.idTarjetaConvenio;
	}

	public void setIdTarjetaConvenio(Long idTarjetaConvenio) {
		this.idTarjetaConvenio = idTarjetaConvenio;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

//	public List<DescTarjetaConvenio> getDescTarjetaConvenios1() {
//		return this.descTarjetaConvenios1;
//	}
//
//	public void setDescTarjetaConvenios1(
//			List<DescTarjetaConvenio> descTarjetaConvenios1) {
//		this.descTarjetaConvenios1 = descTarjetaConvenios1;
//	}
//
//	public DescTarjetaConvenio addDescTarjetaConvenios1(
//			DescTarjetaConvenio descTarjetaConvenios1) {
//		getDescTarjetaConvenios1().add(descTarjetaConvenios1);
//		descTarjetaConvenios1.setFacturaClienteCabTarjConvenio1(this);
//
//		return descTarjetaConvenios1;
//	}
//
//	public DescTarjetaConvenio removeDescTarjetaConvenios1(
//			DescTarjetaConvenio descTarjetaConvenios1) {
//		getDescTarjetaConvenios1().remove(descTarjetaConvenios1);
//		descTarjetaConvenios1.setFacturaClienteCabTarjConvenio1(null);
//
//		return descTarjetaConvenios1;
//	}
//
//	public List<DescTarjetaConvenio> getDescTarjetaConvenios2() {
//		return this.descTarjetaConvenios2;
//	}
//
//	public void setDescTarjetaConvenios2(
//			List<DescTarjetaConvenio> descTarjetaConvenios2) {
//		this.descTarjetaConvenios2 = descTarjetaConvenios2;
//	}
//
//	public DescTarjetaConvenio addDescTarjetaConvenios2(
//			DescTarjetaConvenio descTarjetaConvenios2) {
//		getDescTarjetaConvenios2().add(descTarjetaConvenios2);
//		descTarjetaConvenios2.setFacturaClienteCabTarjConvenio2(this);
//
//		return descTarjetaConvenios2;
//	}
//
//	public DescTarjetaConvenio removeDescTarjetaConvenios2(
//			DescTarjetaConvenio descTarjetaConvenios2) {
//		getDescTarjetaConvenios2().remove(descTarjetaConvenios2);
//		descTarjetaConvenios2.setFacturaClienteCabTarjConvenio2(null);
//
//		return descTarjetaConvenios2;
//	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab1) {
		this.facturaClienteCab = facturaClienteCab1;
	}

}