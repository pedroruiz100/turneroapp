package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.ClientePendienteDTO;
import turneroWeb.core.dto.FuncionarioDTO;
import turneroWeb.core.dto.ServPendienteDTO;

@Entity
@Table(name = "serv_pendiente", schema = "estetica")
@NamedQuery(name = "ServPendiente.findAll", query = "SELECT r FROM ServPendiente r")
public class ServPendiente implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_serv_pendiente")
	private Long idServPendiente;

	// bi-directional many-to-one association to ClientePendiente
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cliente_pendiente")
	private ClientePendiente clientePendiente;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_funcionario")
	private Funcionario funcionario;

	// bi-directional many-to-one association to Articulo
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_articulo")
	private Articulo articulo;

	@Column(name = "monto_serv")
	private Integer montoServ;

	@Column(name = "monto_comision")
	private Integer montoComision;

	@Column(name = "comision_porc")
	private Boolean comisionPorc;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "poriva")
	private Integer poriva;

	@Column(name = "cod_articulo")
	private String codArticulo;

	@Column(name = "porc")
	private Integer porc;

	private Integer cantidad;

	public ServPendiente() {
	}

	public Long getIdServPendiente() {
		return idServPendiente;
	}

	public void setIdServPendiente(Long idServPendiente) {
		this.idServPendiente = idServPendiente;
	}

	public ClientePendiente getClientePendiente() {
		return clientePendiente;
	}

	public void setClientePendiente(ClientePendiente clientePendiente) {
		this.clientePendiente = clientePendiente;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	// public ServPendiente getServPendiente() {
	// return ServPendiente;
	// }
	//
	// public void setServPendiente(ServPendiente ServPendiente) {
	// this.ServPendiente = ServPendiente;
	// }

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public Integer getMontoServ() {
		return montoServ;
	}

	public void setMontoServ(Integer montoServ) {
		this.montoServ = montoServ;
	}

	public Integer getMontoComision() {
		return montoComision;
	}

	public void setMontoComision(Integer montoComision) {
		this.montoComision = montoComision;
	}

	public Boolean getComisionPorc() {
		return comisionPorc;
	}

	public void setComisionPorc(Boolean comisionPorc) {
		this.comisionPorc = comisionPorc;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public Integer getPoriva() {
		return poriva;
	}

	public void setPoriva(Integer poriva) {
		this.poriva = poriva;
	}

	public String getCodArticulo() {
		return codArticulo;
	}

	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}

	public Integer getPorc() {
		return porc;
	}

	public void setPorc(Integer porc) {
		this.porc = porc;
	}

	public ServPendienteDTO toBDServPendienteDTO() {
		ServPendienteDTO funDTO = toServPendienteDTO(this);
		if (funDTO.getClientePendiente() == null) {
			ClientePendienteDTO pais = new ClientePendienteDTO();
			pais.setIdClientePendiente(0L);
			funDTO.setClientePendiente(pais);
		} else {
			funDTO.setClientePendiente(funDTO.getClientePendiente());
		}
		if (funDTO.getArticulo() != null) {
			funDTO.setArticulo(funDTO.getArticulo());
		}
		if (funDTO.getFuncionario() != null) {
			FuncionarioDTO pais = new FuncionarioDTO();
			funDTO.setFuncionario(pais);
		} else {
			FuncionarioDTO pais = new FuncionarioDTO();
			pais.setIdFuncionario(0L);
			funDTO.setFuncionario(pais);
		}
		return funDTO;
	}

	public static ServPendienteDTO toServPendienteDTO(
			ServPendiente ServPendiente) {
		ServPendienteDTO funDTO = new ServPendienteDTO();
		BeanUtils.copyProperties(ServPendiente, funDTO);
		return funDTO;
	}

	public static ServPendiente fromServPendienteDTO(ServPendienteDTO funDTO) {
		ServPendiente fun = new ServPendiente();
		BeanUtils.copyProperties(funDTO, fun);
		fun.setClientePendiente(ClientePendiente.fromClientePendienteDTO(funDTO
				.getClientePendiente()));
		fun.setArticulo(Articulo.fromArticuloDTONoList(funDTO.getArticulo()));
		fun.setFuncionario(Funcionario.fromFuncionarioDTO(funDTO
				.getFuncionario()));
		return fun;
	}
}
