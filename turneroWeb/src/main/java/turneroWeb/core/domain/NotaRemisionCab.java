package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the nota_remision_cab database table.
 * 
 */
@Entity
@Table(name = "nota_remision_cab", schema = "factura_cliente")
@NamedQuery(name = "NotaRemisionCab.findAll", query = "SELECT n FROM NotaRemisionCab n")
public class NotaRemisionCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_nota_remision_cab")
	private Long idNotaRemisionCab;

	@Column(name = "direc_punto_llegada")
	private String direcPuntoLlegada;

	@Column(name = "direc_punto_partida")
	private String direcPuntoPartida;

	@Column(name = "fecha_fin_traslado")
	private Timestamp fechaFinTraslado;

	@Column(name = "fecha_inicio_traslado")
	private Timestamp fechaInicioTraslado;

	@Column(name = "id_cliente")
	private Long idCliente;

	@Column(name = "id_sucursal")
	private Long idSucursal;

	@Column(name = "nro_nota_remision")
	private String nroNotaRemision;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	// bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	// bi-directional many-to-one association to MotivoTraslado
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_motivo_traslado")
	private MotivoTraslado motivoTraslado;

	// bi-directional many-to-one association to MotivoTraslado
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_motivo_traslado")
//	private MotivoTraslado motivoTraslado2;

	// bi-directional many-to-one association to NotaRemisionDet
//	@OneToMany(mappedBy = "notaRemisionCab1")
//	private List<NotaRemisionDet> notaRemisionDets1;
//
//	// bi-directional many-to-one association to NotaRemisionDet
//	@OneToMany(mappedBy = "notaRemisionCab2")
//	private List<NotaRemisionDet> notaRemisionDets2;

	public NotaRemisionCab() {
	}

	public Long getIdNotaRemisionCab() {
		return this.idNotaRemisionCab;
	}

	public void setIdNotaRemisionCab(Long idNotaRemisionCab) {
		this.idNotaRemisionCab = idNotaRemisionCab;
	}

	public String getDirecPuntoLlegada() {
		return this.direcPuntoLlegada;
	}

	public void setDirecPuntoLlegada(String direcPuntoLlegada) {
		this.direcPuntoLlegada = direcPuntoLlegada;
	}

	public String getDirecPuntoPartida() {
		return this.direcPuntoPartida;
	}

	public void setDirecPuntoPartida(String direcPuntoPartida) {
		this.direcPuntoPartida = direcPuntoPartida;
	}

	public Timestamp getFechaFinTraslado() {
		return this.fechaFinTraslado;
	}

	public void setFechaFinTraslado(Timestamp fechaFinTraslado) {
		this.fechaFinTraslado = fechaFinTraslado;
	}

	public Timestamp getFechaInicioTraslado() {
		return this.fechaInicioTraslado;
	}

	public void setFechaInicioTraslado(Timestamp fechaInicioTraslado) {
		this.fechaInicioTraslado = fechaInicioTraslado;
	}

	public Long getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdSucursal() {
		return this.idSucursal;
	}

	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

	public String getNroNotaRemision() {
		return this.nroNotaRemision;
	}

	public void setNroNotaRemision(String nroNotaRemision) {
		this.nroNotaRemision = nroNotaRemision;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

//	public FacturaClienteCab getFacturaClienteCab2() {
//		return this.facturaClienteCab2;
//	}
//
//	public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
//		this.facturaClienteCab2 = facturaClienteCab2;
//	}

	public MotivoTraslado getMotivoTraslado() {
		return this.motivoTraslado;
	}

	public void setMotivoTraslado(MotivoTraslado motivoTraslado) {
		this.motivoTraslado = motivoTraslado;
	}

//	public MotivoTraslado getMotivoTraslado2() {
//		return this.motivoTraslado2;
//	}
//
//	public void setMotivoTraslado2(MotivoTraslado motivoTraslado2) {
//		this.motivoTraslado2 = motivoTraslado2;
//	}

//	public List<NotaRemisionDet> getNotaRemisionDets1() {
//		return this.notaRemisionDets1;
//	}
//
//	public void setNotaRemisionDets1(List<NotaRemisionDet> notaRemisionDets1) {
//		this.notaRemisionDets1 = notaRemisionDets1;
//	}

//	public NotaRemisionDet addNotaRemisionDets1(
//			NotaRemisionDet notaRemisionDets1) {
//		getNotaRemisionDets1().add(notaRemisionDets1);
//		notaRemisionDets1.setNotaRemisionCab1(this);
//
//		return notaRemisionDets1;
//	}
//
//	public NotaRemisionDet removeNotaRemisionDets1(
//			NotaRemisionDet notaRemisionDets1) {
//		getNotaRemisionDets1().remove(notaRemisionDets1);
//		notaRemisionDets1.setNotaRemisionCab1(null);
//
//		return notaRemisionDets1;
//	}

//	public List<NotaRemisionDet> getNotaRemisionDets2() {
//		return this.notaRemisionDets2;
//	}
//
//	public void setNotaRemisionDets2(List<NotaRemisionDet> notaRemisionDets2) {
//		this.notaRemisionDets2 = notaRemisionDets2;
//	}

//	public NotaRemisionDet addNotaRemisionDets2(
//			NotaRemisionDet notaRemisionDets2) {
//		getNotaRemisionDets2().add(notaRemisionDets2);
//		notaRemisionDets2.setNotaRemisionCab2(this);
//
//		return notaRemisionDets2;
//	}
//
//	public NotaRemisionDet removeNotaRemisionDets2(
//			NotaRemisionDet notaRemisionDets2) {
//		getNotaRemisionDets2().remove(notaRemisionDets2);
//		notaRemisionDets2.setNotaRemisionCab2(null);
//
//		return notaRemisionDets2;
//	}

}