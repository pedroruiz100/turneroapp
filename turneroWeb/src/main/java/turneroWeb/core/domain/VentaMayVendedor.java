package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the venta_may_vendedor database table.
 * 
 */
@Entity
@Table(name = "venta_may_vendedor", schema = "factura_cliente")
@NamedQuery(name = "VentaMayVendedor.findAll", query = "SELECT v FROM VentaMayVendedor v")
public class VentaMayVendedor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_venta")
	private Long idVenta;

	@Column(name = "id_ven_may")
	private Long idVenMay;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public VentaMayVendedor() {
	}

	public Long getIdVenta() {
		return this.idVenta;
	}

	public void setIdVenta(Long idVenta) {
		this.idVenta = idVenta;
	}

	public Long getIdVenMay() {
		return this.idVenMay;
	}

	public void setIdVenMay(Long idVenMay) {
		this.idVenMay = idVenMay;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}