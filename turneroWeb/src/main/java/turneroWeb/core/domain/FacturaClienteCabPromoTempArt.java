package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the factura_cliente_cab_promo_temp_art database
 * table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_promo_temp_art", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPromoTempArt.findAll", query = "SELECT f FROM FacturaClienteCabPromoTempArt f")
public class FacturaClienteCabPromoTempArt implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_promo_temp_art")
	private Long idFacturaClienteCabPromoTempArt;

	@Column(name = "descripcion_temporada")
	private String descripcionTemporada;

	@Column(name = "id_temporada_art")
	private Long idTemporadaArt;

	private Integer monto;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabPromoTempArt() {
	}

	public Long getIdFacturaClienteCabPromoTempArt() {
		return this.idFacturaClienteCabPromoTempArt;
	}

	public void setIdFacturaClienteCabPromoTempArt(
			Long idFacturaClienteCabPromoTempArt) {
		this.idFacturaClienteCabPromoTempArt = idFacturaClienteCabPromoTempArt;
	}

	public String getDescripcionTemporada() {
		return this.descripcionTemporada;
	}

	public void setDescripcionTemporada(String descripcionTemporada) {
		this.descripcionTemporada = descripcionTemporada;
	}

	public Long getIdTemporadaArt() {
		return this.idTemporadaArt;
	}

	public void setIdTemporadaArt(Long idTemporadaArt) {
		this.idTemporadaArt = idTemporadaArt;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab1(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}