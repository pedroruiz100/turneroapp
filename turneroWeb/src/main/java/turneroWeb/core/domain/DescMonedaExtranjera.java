package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the desc_moneda_extranjera database table.
 * 
 */
@Entity
@Table(name = "desc_moneda_extranjera", schema = "factura_cliente")
@NamedQuery(name="DescMonedaExtranjera.findAll", query="SELECT d FROM DescMonedaExtranjera d")
public class DescMonedaExtranjera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_moneda_extranjera")
	private Long idDescMonedaExtranjera;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabMonedaExtranjera
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_moneda_extranjera")
	private FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjera;

	//bi-directional many-to-one association to FacturaClienteCabMonedaExtranjera
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_moneda_extranjera")
//	private FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjera2;

	public DescMonedaExtranjera() {
	}

	public Long getIdDescMonedaExtranjera() {
		return this.idDescMonedaExtranjera;
	}

	public void setIdDescMonedaExtranjera(Long idDescMonedaExtranjera) {
		this.idDescMonedaExtranjera = idDescMonedaExtranjera;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabMonedaExtranjera getFacturaClienteCabMonedaExtranjera() {
		return this.facturaClienteCabMonedaExtranjera;
	}

	public void setFacturaClienteCabMonedaExtranjera(FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjera) {
		this.facturaClienteCabMonedaExtranjera = facturaClienteCabMonedaExtranjera;
	}

	// public FacturaClienteCabMonedaExtranjera
	// getFacturaClienteCabMonedaExtranjera2() {
	// return this.facturaClienteCabMonedaExtranjera2;
	// }
	//
	// public void
	// setFacturaClienteCabMonedaExtranjera2(FacturaClienteCabMonedaExtranjera
	// facturaClienteCabMonedaExtranjera2) {
	// this.facturaClienteCabMonedaExtranjera2 =
	// facturaClienteCabMonedaExtranjera2;
	// }

}