package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the nota_credito_det database table.
 * 
 */
@Entity
@Table(name="nota_credito_det", schema = "factura_cliente")
@NamedQuery(name="NotaCreditoDet.findAll", query="SELECT n FROM NotaCreditoDet n")
public class NotaCreditoDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nota_credito_det")
	private Long idNotaCreditoDet;

	private Integer cantidad;

	private BigDecimal contenido;

	@Column(name="costo_unitario")
	private Integer costoUnitario;

	@Column(name="descripcion_articulo")
	private String descripcionArticulo;

	@Column(name="id_articulo")
	private Long idArticulo;

	@Column(name="id_unidad")
	private Integer idUnidad;

	@Column(name="porc_desc")
	private Integer porcDesc;

	@Column(name="porc_iva")
	private Integer porcIva;

	@Column(name="precio_unitario")
	private Integer precioUnitario;

	@Column(name="total_nota")
	private Integer totalNota;

	//bi-directional many-to-one association to NotaCreditoCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_nota_credito_cab")
	private NotaCreditoCab notaCreditoCab;

	//bi-directional many-to-one association to NotaCreditoCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_nota_credito_cab")
//	private NotaCreditoCab notaCreditoCab2;

	public NotaCreditoDet() {
	}

	public Long getIdNotaCreditoDet() {
		return this.idNotaCreditoDet;
	}

	public void setIdNotaCreditoDet(Long idNotaCreditoDet) {
		this.idNotaCreditoDet = idNotaCreditoDet;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public BigDecimal getContenido() {
		return this.contenido;
	}

	public void setContenido(BigDecimal contenido) {
		this.contenido = contenido;
	}

	public Integer getCostoUnitario() {
		return this.costoUnitario;
	}

	public void setCostoUnitario(Integer costoUnitario) {
		this.costoUnitario = costoUnitario;
	}

	public String getDescripcionArticulo() {
		return this.descripcionArticulo;
	}

	public void setDescripcionArticulo(String descripcionArticulo) {
		this.descripcionArticulo = descripcionArticulo;
	}

	public Long getIdArticulo() {
		return this.idArticulo;
	}

	public void setIdArticulo(Long idArticulo) {
		this.idArticulo = idArticulo;
	}

	public Integer getIdUnidad() {
		return this.idUnidad;
	}

	public void setIdUnidad(Integer idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Integer getPorcDesc() {
		return this.porcDesc;
	}

	public void setPorcDesc(Integer porcDesc) {
		this.porcDesc = porcDesc;
	}

	public Integer getPorcIva() {
		return this.porcIva;
	}

	public void setPorcIva(Integer porcIva) {
		this.porcIva = porcIva;
	}

	public Integer getPrecioUnitario() {
		return this.precioUnitario;
	}

	public void setPrecioUnitario(Integer precioUnitario) {
		this.precioUnitario = precioUnitario;
	}

	public Integer getTotalNota() {
		return this.totalNota;
	}

	public void setTotalNota(Integer totalNota) {
		this.totalNota = totalNota;
	}

	public NotaCreditoCab getNotaCreditoCab() {
		return this.notaCreditoCab;
	}

	public void setNotaCreditoCab(NotaCreditoCab notaCreditoCab) {
		this.notaCreditoCab = notaCreditoCab;
	}

	// public NotaCreditoCab getNotaCreditoCab2() {
	// return this.notaCreditoCab2;
	// }
	//
	// public void setNotaCreditoCab2(NotaCreditoCab notaCreditoCab2) {
	// this.notaCreditoCab2 = notaCreditoCab2;
	// }

}