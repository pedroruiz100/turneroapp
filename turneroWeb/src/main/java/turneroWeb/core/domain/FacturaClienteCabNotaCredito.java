package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.FacturaClienteCabNotaCreditoDTO;

/**
 * The persistent class for the factura_cliente_cab_nota_credito database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_nota_credito", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabNotaCredito.findAll", query = "SELECT f FROM FacturaClienteCabNotaCredito f")
public class FacturaClienteCabNotaCredito implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_nota_credito")
	private Long idFacturaClienteCabNotaCredito;

	private Integer monto;

	@Column(name = "nro_nota")
	private String nroNota;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabNotaCredito() {
	}

	public Long getIdFacturaClienteCabNotaCredito() {
		return this.idFacturaClienteCabNotaCredito;
	}

	public void setIdFacturaClienteCabNotaCredito(
			Long idFacturaClienteCabNotaCredito) {
		this.idFacturaClienteCabNotaCredito = idFacturaClienteCabNotaCredito;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public String getNroNota() {
		return this.nroNota;
	}

	public void setNroNota(String nroNota) {
		this.nroNota = nroNota;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}
	
	public FacturaClienteCabNotaCreditoDTO toBDFacClienteCabNotaCreditoDTO() {
        FacturaClienteCabNotaCreditoDTO facDTO = toFacClienteCabNotaCreditoDTO(this);
        if (this.getFacturaClienteCab() != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
        return facDTO;
    }

    private static FacturaClienteCabNotaCreditoDTO toFacClienteCabNotaCreditoDTO(
            FacturaClienteCabNotaCredito facturaClienteCabNotaCredito) {
        FacturaClienteCabNotaCreditoDTO facDTO = new FacturaClienteCabNotaCreditoDTO();
        BeanUtils.copyProperties(facturaClienteCabNotaCredito, facDTO);
        return facDTO;
    }

    public static FacturaClienteCabNotaCredito fromFacturaClienteCabNotaCredito(FacturaClienteCabNotaCreditoDTO facDTO) {
        FacturaClienteCabNotaCredito fac = new FacturaClienteCabNotaCredito();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabNotaCredito fromFacturaClienteCabNotaCreditoAsociado(FacturaClienteCabNotaCreditoDTO facDTO) {
        FacturaClienteCabNotaCredito fac = fromFacturaClienteCabNotaCredito(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab.fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}