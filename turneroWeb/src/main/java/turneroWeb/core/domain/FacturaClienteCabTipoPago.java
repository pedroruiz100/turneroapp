package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the factura_cliente_cab_tipo_pago database table.
 * 
 */
@Entity
@Table(name="factura_cliente_cab_tipo_pago", schema = "factura_cliente")
@NamedQuery(name="FacturaClienteCabTipoPago.findAll", query="SELECT f FROM FacturaClienteCabTipoPago f")
public class FacturaClienteCabTipoPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_factura_cliente_cab_tipo_pago")
	private Long idFacturaClienteCabTipoPago;

	@Column(name="monto_pago")
	private Integer montoPago;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	//bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	//bi-directional many-to-one association to TipoPago
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_pago")
	private TipoPago tipoPago;

	//bi-directional many-to-one association to TipoPago
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_tipo_pago")
//	private TipoPago tipoPago2;

	public FacturaClienteCabTipoPago() {
	}

	public Long getIdFacturaClienteCabTipoPago() {
		return this.idFacturaClienteCabTipoPago;
	}

	public void setIdFacturaClienteCabTipoPago(Long idFacturaClienteCabTipoPago) {
		this.idFacturaClienteCabTipoPago = idFacturaClienteCabTipoPago;
	}

	public Integer getMontoPago() {
		return this.montoPago;
	}

	public void setMontoPago(Integer montoPago) {
		this.montoPago = montoPago;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

//	public FacturaClienteCab getFacturaClienteCab2() {
//		return this.facturaClienteCab2;
//	}
//
//	public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
//		this.facturaClienteCab2 = facturaClienteCab2;
//	}

	public TipoPago getTipoPago() {
		return this.tipoPago;
	}

	public void setTipoPago(TipoPago tipoPago) {
		this.tipoPago = tipoPago;
	}

	// public TipoPago getTipoPago2() {
	// return this.tipoPago2;
	// }
	//
	// public void setTipoPago2(TipoPago tipoPago2) {
	// this.tipoPago2 = tipoPago2;
	// }

}