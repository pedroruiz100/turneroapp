package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.PublicidadDTO;

/**
 * The persistent class for the ciudad database table.
 * 
 */
@Entity
@Table(name = "publicidad", schema = "general")
@NamedQuery(name = "Publicidad.findAll", query = "SELECT c FROM Publicidad c")
public class Publicidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_publicidad")
	private Long idPublicidad;

	@Column(name = "link")
	private String link;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "estado")
	private boolean estado;

	public Publicidad() {
	}

	public Long getIdPublicidad() {
		return idPublicidad;
	}

	public void setIdPublicidad(Long idPublicidad) {
		this.idPublicidad = idPublicidad;
	}

	public String getLink() {
		return link;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	// Convierte a DTO (ciudadDTO) las entidades que se tienen de Ciudad
	public PublicidadDTO toPublicidadDTO() {
		PublicidadDTO ciudadDTO = new PublicidadDTO();
		BeanUtils.copyProperties(this, ciudadDTO);
		return ciudadDTO;
	}

	public static Publicidad fromPublicidadDTO(PublicidadDTO ciudadDTO) {
		Publicidad ciudad = new Publicidad();
		BeanUtils.copyProperties(ciudadDTO, ciudad);
		return ciudad;
	}

	public static Publicidad fromPublicidadAsociado(PublicidadDTO ciudadDTO) {
		Publicidad ciudad = fromPublicidadDTO(ciudadDTO);
		// ciudad.setDepartamento(Departamento.fromDepartamentoDTO(ciudadDTO
		// .getDepartamento()));
		return ciudad;
	}

}