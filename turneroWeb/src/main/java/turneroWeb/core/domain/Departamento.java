package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.DepartamentoDTO;

/**
 * The persistent class for the departamento database table.
 * 
 */
@Entity
@Table(name = "departamento", schema = "general")
@NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d")
public class Departamento implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_departamento")
	private Long idDepartamento;

	@Column(name = "activo")
	private Boolean activo;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

//	// bi-directional many-to-one association to Ciudad
//	@OneToMany(mappedBy = "departamento", fetch = FetchType.LAZY)
//	private List<Ciudad> ciudads;

	// bi-directional many-to-one association to Pais
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pais")
	private Pais pais;
//
//	// bi-directional many-to-one association to Sucursal
//	@OneToMany(mappedBy = "departamento")
//	private List<Sucursal> sucursals;
//
//	// bi-directional many-to-one association to Cliente
//	@OneToMany(mappedBy = "departamento")
//	private List<Cliente> clientes;
//
//	// bi-directional many-to-one association to Proveedor
//	@OneToMany(mappedBy = "departamento")
//	private List<Proveedor> proveedors;

	public Departamento() {
	}

	public Long getIdDepartamento() {
		return this.idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

//	public List<Ciudad> getCiudads() {
//		return this.ciudads;
//	}
//
//	public void setCiudads(List<Ciudad> ciudads) {
//		this.ciudads = ciudads;
//	}

//	public Ciudad addCiudad(Ciudad ciudad) {
//		getCiudads().add(ciudad);
//		ciudad.setDepartamento(this);
//
//		return ciudad;
//	}
//
//	public Ciudad removeCiudad(Ciudad ciudad) {
//		getCiudads().remove(ciudad);
//		ciudad.setDepartamento(null);
//
//		return ciudad;
//	}

	public Pais getPais() {
		return this.pais;
	}

	public void setPais(Pais pai) {
		this.pais = pai;
	}

//	public List<Sucursal> getSucursals() {
//		return this.sucursals;
//	}
//
//	public void setSucursals(List<Sucursal> sucursals) {
//		this.sucursals = sucursals;
//	}
//
//	public Sucursal addSucursal(Sucursal sucursal) {
//		getSucursals().add(sucursal);
//		sucursal.setDepartamento(this);
//
//		return sucursal;
//	}
//
//	public Sucursal removeSucursal(Sucursal sucursal) {
//		getSucursals().remove(sucursal);
//		sucursal.setDepartamento(null);
//
//		return sucursal;
//	}

	// ****************************************************************************************

//	public List<Cliente> getClientes() {
//		return clientes;
//	}
//
//	public void setClientes(List<Cliente> clientes) {
//		this.clientes = clientes;
//	}
//
//	public Cliente addClientes(Cliente clientes) {
//		getClientes().add(clientes);
//		clientes.setDepartamento(this);
//		return clientes;
//	}
//
//	public Cliente removeClientes(Cliente clientes) {
//		getClientes().remove(clientes);
//		clientes.setDepartamento(null);
//		return clientes;
//	}

	// ****************************************************************************************

//	public List<Proveedor> getProveedors() {
//		return proveedors;
//	}
//
//	public void setProveedors(List<Proveedor> proveedors) {
//		this.proveedors = proveedors;
//	}
//
//	public Proveedor addProveedors(Proveedor proveedor) {
//		getProveedors().add(proveedor);
//		proveedor.setDepartamento(this);
//		return proveedor;
//	}
//
//	public Proveedor removeProveedors(Proveedor proveedor) {
//		getProveedors().remove(proveedor);
//		proveedor.setDepartamento(null);
//		return proveedor;
//	}

	// DTOs****************************************************************************************

	public DepartamentoDTO toDepartamentoDTO() {
		DepartamentoDTO departamentoDTO = toDeparatamentoDTO(this);
//		if (!this.getSucursals().isEmpty()) {
//			this.setSucursals(null);
//		}
//		if (this.getCiudads() != null) {
//			if (!this.getCiudads().isEmpty()) {
//				this.setCiudads(null);
//			}
//		}
		if (this.getPais() != null) {
			departamentoDTO.setPaisDTO(this.getPais().toPaisDTO());
		}
		return departamentoDTO;
	}

	public static DepartamentoDTO toDeparatamentoDTO(Departamento dep) {
		DepartamentoDTO departamentoDTO = new DepartamentoDTO();
		BeanUtils.copyProperties(dep, departamentoDTO);
		return departamentoDTO;
	}

	public static Departamento fromDepartamentoDTO(
			DepartamentoDTO departamentoDTO) {
		Departamento departamento = new Departamento();
		BeanUtils.copyProperties(departamentoDTO, departamento);
		return departamento;
	}

	public static Departamento fromDepartamentoDTOAsociado(
			DepartamentoDTO departamentoDTO) {
		Departamento departamento = fromDepartamentoDTO(departamentoDTO);
		departamento.setPais(Pais.fromPaisDTO(departamentoDTO.getPaisDTO()));
		return departamento;
	}

	// DepartamentoDTO sin datos solo los que figura en la BD

	public DepartamentoDTO toDepartamentoBDDTO() {
		DepartamentoDTO depDTO = toDeparatamentoDTO(this);
		if (this.pais != null) {
			depDTO.setPaisDTO(this.getPais().toPaisDTO());
		}
//		depDTO.setCiudadsDTO(null);
//		depDTO.setClienteDTO(null);
//		depDTO.setProveedorDTO(null);
//		depDTO.setSucursalsDTO(null);
		return depDTO;
	}

	// SOLO DEPARTAMENTO

	public DepartamentoDTO toSinOtrosDatosDepartamentoDTO() {
		DepartamentoDTO depDTO = toDeparatamentoDTO(this);
		depDTO.setPaisDTO(null);
		// depDTO.setCiudadsDTO(null);
		// depDTO.setClienteDTO(null);
		// depDTO.setProveedorDTO(null);
		// depDTO.setSucursalsDTO(null);
		return depDTO;
	}

}