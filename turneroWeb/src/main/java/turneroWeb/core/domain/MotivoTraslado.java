package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the motivo_traslado database table.
 * 
 */
@Entity
@Table(name="motivo_traslado", schema = "factura_cliente")
@NamedQuery(name="MotivoTraslado.findAll", query="SELECT m FROM MotivoTraslado m")
public class MotivoTraslado implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_motivo_traslado")
	private Long idMotivoTraslado;

	private String descripcion;

	@Column(name="fecha_alta")
	private Timestamp fechaAlta;

	@Column(name="fecha_mod")
	private Timestamp fechaMod;

	@Column(name="usu_alta")
	private String usuAlta;

	@Column(name="usu_mod")
	private String usuMod;

	//bi-directional many-to-one association to NotaRemisionCab
//	@OneToMany(mappedBy="MotivoTraslado")
//	private List<NotaRemisionCab> notaRemisionCabs;

	//bi-directional many-to-one association to NotaRemisionCab
//	@OneToMany(mappedBy="motivoTraslado2")
//	private List<NotaRemisionCab> notaRemisionCabs2;

	public MotivoTraslado() {
	}

	public Long getIdMotivoTraslado() {
		return this.idMotivoTraslado;
	}

	public void setIdMotivoTraslado(Long idMotivoTraslado) {
		this.idMotivoTraslado = idMotivoTraslado;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// public List<NotaRemisionCab> getNotaRemisionCabs() {
	// return this.notaRemisionCabs;
	// }
	//
	// public void setNotaRemisionCabs(List<NotaRemisionCab> notaRemisionCabs) {
	// this.notaRemisionCabs = notaRemisionCabs;
	// }

//	public NotaRemisionCab addNotaRemisionCabs1(NotaRemisionCab notaRemisionCabs1) {
//		getNotaRemisionCabs1().add(notaRemisionCabs1);
//		notaRemisionCabs1.setMotivoTraslado1(this);
//
//		return notaRemisionCabs1;
//	}
//
//	public NotaRemisionCab removeNotaRemisionCabs1(NotaRemisionCab notaRemisionCabs1) {
//		getNotaRemisionCabs1().remove(notaRemisionCabs1);
//		notaRemisionCabs1.setMotivoTraslado1(null);
//
//		return notaRemisionCabs1;
//	}

	// public List<NotaRemisionCab> getNotaRemisionCabs2() {
	// return this.notaRemisionCabs2;
	// }
	//
	// public void setNotaRemisionCabs2(List<NotaRemisionCab> notaRemisionCabs2)
	// {
	// this.notaRemisionCabs2 = notaRemisionCabs2;
	// }

//	public NotaRemisionCab addNotaRemisionCabs2(NotaRemisionCab notaRemisionCabs2) {
//		getNotaRemisionCabs2().add(notaRemisionCabs2);
//		notaRemisionCabs2.setMotivoTraslado2(this);
//
//		return notaRemisionCabs2;
//	}
//
//	public NotaRemisionCab removeNotaRemisionCabs2(NotaRemisionCab notaRemisionCabs2) {
//		getNotaRemisionCabs2().remove(notaRemisionCabs2);
//		notaRemisionCabs2.setMotivoTraslado2(null);
//
//		return notaRemisionCabs2;
//	}

}