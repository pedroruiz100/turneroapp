package turneroWeb.core.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.FacturaClienteCabHistoricoDTO;

/**
 * The persistent class for the factura_cliente_cab_historico database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_historico", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabHistorico.findAll", query = "SELECT f FROM FacturaClienteCabHistorico f")
public class FacturaClienteCabHistorico implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_historico")
	private Long idFacturaClienteCabHistorico;

	private Integer asoc;

	private Integer cheque;

	private String cliente;

	@Column(name = "coti_dolar")
	private Long cotiDolar;

	@Column(name = "coti_peso")
	private Long cotiPeso;

	@Column(name = "coti_real")
	private Long cotiReal;

	private Integer descuento;

	private String direccion;

	private Integer dolar;

	private Integer efectivo;

	private String empresa;

	private Integer exenta;

	@Temporal(TemporalType.DATE)
	@Column(name = "fec_inicial")
	private Date fecInicial;

	@Temporal(TemporalType.DATE)
	@Column(name = "fec_vencimiento")
	private Date fecVencimiento;

	private Integer giftcard;

	private Integer grav10;

	private Integer grav5;

	private Integer liqui10;

	private Integer liqui5;

	@Column(name = "not_cre")
	private Integer notCre;

	@Column(name = "nro_caja")
	private Integer nroCaja;

	@Column(name = "nro_timbrado")
	private String nroTimbrado;

	private Integer peso;

	@Column(name = "real_b")
	private Integer realB;

	private Integer realb;

	private Integer redondeo;

	private Integer retencion;

	private String ruc;

	@Column(name = "ruc_cliente")
	private String rucCliente;

	private String sucursal;

	@Column(name = "tarj_cred")
	private Integer tarjCred;

	@Column(name = "tarj_deb")
	private Integer tarjDeb;

	private String telef;

	private Integer total;

	private Integer vale;

	private Integer vuelto;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabHistorico() {
	}

	public Long getIdFacturaClienteCabHistorico() {
		return this.idFacturaClienteCabHistorico;
	}

	public void setIdFacturaClienteCabHistorico(
			Long idFacturaClienteCabHistorico) {
		this.idFacturaClienteCabHistorico = idFacturaClienteCabHistorico;
	}

	public Integer getAsoc() {
		return this.asoc;
	}

	public void setAsoc(Integer asoc) {
		this.asoc = asoc;
	}

	public Integer getCheque() {
		return this.cheque;
	}

	public void setCheque(Integer cheque) {
		this.cheque = cheque;
	}

	public String getCliente() {
		return this.cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Long getCotiDolar() {
		return this.cotiDolar;
	}

	public void setCotiDolar(Long cotiDolar) {
		this.cotiDolar = cotiDolar;
	}

	public Long getCotiPeso() {
		return this.cotiPeso;
	}

	public void setCotiPeso(Long cotiPeso) {
		this.cotiPeso = cotiPeso;
	}

	public Long getCotiReal() {
		return this.cotiReal;
	}

	public void setCotiReal(Long cotiReal) {
		this.cotiReal = cotiReal;
	}

	public Integer getDescuento() {
		return this.descuento;
	}

	public void setDescuento(Integer descuento) {
		this.descuento = descuento;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Integer getDolar() {
		return this.dolar;
	}

	public void setDolar(Integer dolar) {
		this.dolar = dolar;
	}

	public Integer getEfectivo() {
		return this.efectivo;
	}

	public void setEfectivo(Integer efectivo) {
		this.efectivo = efectivo;
	}

	public String getEmpresa() {
		return this.empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public Integer getExenta() {
		return this.exenta;
	}

	public void setExenta(Integer exenta) {
		this.exenta = exenta;
	}

	public Date getFecInicial() {
		return this.fecInicial;
	}

	public void setFecInicial(Date fecInicial) {
		this.fecInicial = fecInicial;
	}

	public Date getFecVencimiento() {
		return this.fecVencimiento;
	}

	public void setFecVencimiento(Date fecVencimiento) {
		this.fecVencimiento = fecVencimiento;
	}

	public Integer getGiftcard() {
		return this.giftcard;
	}

	public void setGiftcard(Integer giftcard) {
		this.giftcard = giftcard;
	}

	public Integer getGrav10() {
		return this.grav10;
	}

	public void setGrav10(Integer grav10) {
		this.grav10 = grav10;
	}

	public Integer getGrav5() {
		return this.grav5;
	}

	public void setGrav5(Integer grav5) {
		this.grav5 = grav5;
	}

	public Integer getLiqui10() {
		return this.liqui10;
	}

	public void setLiqui10(Integer liqui10) {
		this.liqui10 = liqui10;
	}

	public Integer getLiqui5() {
		return this.liqui5;
	}

	public void setLiqui5(Integer liqui5) {
		this.liqui5 = liqui5;
	}

	public Integer getNotCre() {
		return this.notCre;
	}

	public void setNotCre(Integer notCre) {
		this.notCre = notCre;
	}

	public Integer getNroCaja() {
		return this.nroCaja;
	}

	public void setNroCaja(Integer nroCaja) {
		this.nroCaja = nroCaja;
	}

	public String getNroTimbrado() {
		return this.nroTimbrado;
	}

	public void setNroTimbrado(String nroTimbrado) {
		this.nroTimbrado = nroTimbrado;
	}

	public Integer getPeso() {
		return this.peso;
	}

	public void setPeso(Integer peso) {
		this.peso = peso;
	}

	public Integer getRealB() {
		return this.realB;
	}

	public void setRealB(Integer realB) {
		this.realB = realB;
	}

	public Integer getRealb() {
		return this.realb;
	}

	public void setRealb(Integer realb) {
		this.realb = realb;
	}

	public Integer getRedondeo() {
		return this.redondeo;
	}

	public void setRedondeo(Integer redondeo) {
		this.redondeo = redondeo;
	}

	public Integer getRetencion() {
		return this.retencion;
	}

	public void setRetencion(Integer retencion) {
		this.retencion = retencion;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRucCliente() {
		return this.rucCliente;
	}

	public void setRucCliente(String rucCliente) {
		this.rucCliente = rucCliente;
	}

	public String getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	public Integer getTarjCred() {
		return this.tarjCred;
	}

	public void setTarjCred(Integer tarjCred) {
		this.tarjCred = tarjCred;
	}

	public Integer getTarjDeb() {
		return this.tarjDeb;
	}

	public void setTarjDeb(Integer tarjDeb) {
		this.tarjDeb = tarjDeb;
	}

	public String getTelef() {
		return this.telef;
	}

	public void setTelef(String telef) {
		this.telef = telef;
	}

	public Integer getTotal() {
		return this.total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public Integer getVale() {
		return this.vale;
	}

	public void setVale(Integer vale) {
		this.vale = vale;
	}

	public Integer getVuelto() {
		return this.vuelto;
	}

	public void setVuelto(Integer vuelto) {
		this.vuelto = vuelto;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public FacturaClienteCabHistoricoDTO toFacturaClienteCabHistoricoDTO() {
		FacturaClienteCabHistoricoDTO factDTO = toFacturaClienteCabHistoricoDTO(this);
		if (this.facturaClienteCab != null) {
			factDTO.setFacturaClienteCab(this.getFacturaClienteCab()
					.toFacturaClienteCabAndDetalleDTO());
		}
		return factDTO;
	}

	public FacturaClienteCabHistoricoDTO toFacturaClienteCabDTO() {
		FacturaClienteCabHistoricoDTO factDTO = toFacturaClienteCabHistoricoDTO(this);
		factDTO.setFacturaClienteCab(null);
		return factDTO;
	}

	private FacturaClienteCabHistoricoDTO toFacturaClienteCabHistoricoDTO(
			FacturaClienteCabHistorico facturaClienteCabHistorico) {
		FacturaClienteCabHistoricoDTO factDTO = new FacturaClienteCabHistoricoDTO();
		BeanUtils.copyProperties(facturaClienteCabHistorico, factDTO);
		return factDTO;
	}

	public static FacturaClienteCabHistorico fromFacturaClienteCabHistoricoDTO(
			FacturaClienteCabHistoricoDTO factDTO) {
		FacturaClienteCabHistorico fact = new FacturaClienteCabHistorico();
		BeanUtils.copyProperties(factDTO, fact);
		return fact;
	}

	public static FacturaClienteCabHistorico fromFacturaClienteCabHistoricoAsociadoDTO(
			FacturaClienteCabHistoricoDTO factDTO) {
		FacturaClienteCabHistorico fact = fromFacturaClienteCabHistoricoDTO(factDTO);
		fact.setFacturaClienteCab(FacturaClienteCab
				.fromFacturaClienteCabDTO(factDTO.getFacturaClienteCab()));
		return fact;
	}

}