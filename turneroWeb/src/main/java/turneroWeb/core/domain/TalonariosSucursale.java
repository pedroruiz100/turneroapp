package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.domain.Timbrado;
import turneroWeb.core.dto.TalonariosSucursaleDTO;

/**
 * The persistent class for the talonarios_sucursales database table.
 * 
 */
@Entity
@Table(name = "talonarios_sucursales", schema = "general")
@NamedQuery(name = "TalonariosSucursale.findAll", query = "SELECT t FROM TalonariosSucursale t")
public class TalonariosSucursale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_talonarios_sucursales")
	private Long idTalonariosSucursales;

	@Column(name = "nro_actual")
	private Long nroActual;

	@Column(name = "nro_final")
	private Long nroFinal;

	@Column(name = "nro_inicial")
	private Long nroInicial;

	private String primero;

	private String segundo;

	// bi-directional many-to-one association to IpBoca
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_ip_boca")
	private IpBoca ipBoca;

	// bi-directional many-to-one association to Sucursal
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_sucursal")
	private Sucursal sucursal;

	// bi-directional many-to-one association to Timbrado
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_timbrado")
	private Timbrado timbrado;

	public TalonariosSucursale() {
	}

	public Long getIdTalonariosSucursales() {
		return this.idTalonariosSucursales;
	}

	public void setIdTalonariosSucursales(Long idTalonariosSucursales) {
		this.idTalonariosSucursales = idTalonariosSucursales;
	}

	public Long getNroActual() {
		return this.nroActual;
	}

	public String getPrimero() {
		return primero;
	}

	public void setPrimero(String primero) {
		this.primero = primero;
	}

	public String getSegundo() {
		return segundo;
	}

	public void setSegundo(String segundo) {
		this.segundo = segundo;
	}

	public void setNroActual(Long nroActual) {
		this.nroActual = nroActual;
	}

	public Long getNroFinal() {
		return this.nroFinal;
	}

	public void setNroFinal(Long nroFinal) {
		this.nroFinal = nroFinal;
	}

	public Long getNroInicial() {
		return this.nroInicial;
	}

	public void setNroInicial(Long nroInicial) {
		this.nroInicial = nroInicial;
	}

	public IpBoca getIpBoca() {
		return this.ipBoca;
	}

	public void setIpBoca(IpBoca ipBoca) {
		this.ipBoca = ipBoca;
	}

	public Sucursal getSucursal() {
		return this.sucursal;
	}

	public void setSucursal(Sucursal sucursal) {
		this.sucursal = sucursal;
	}

	public Timbrado getTimbrado() {
		return this.timbrado;
	}

	public void setTimbrado(Timbrado timbrado) {
		this.timbrado = timbrado;
	}

	public TalonariosSucursaleDTO toTalonariosDTO() {
		TalonariosSucursaleDTO taDTO = toTalonariosSucursaleDTO(this);
		if (this.timbrado != null) {
			taDTO.setTimbrado(this.getTimbrado().toBDTimbradoDTO());
		}
		if (this.ipBoca != null) {
			taDTO.setIpBoca(this.getIpBoca().toBDIpBocaDTO());
		}
		taDTO.setSucursal(null);
		return taDTO;
	}

	public TalonariosSucursaleDTO toBDTalonariosDTO() {
		TalonariosSucursaleDTO taDTO = toTalonariosSucursaleDTO(this);
		taDTO.setTimbrado(null);
		taDTO.setIpBoca(null);
		taDTO.setSucursal(null);
		return taDTO;
	}

	public static TalonariosSucursaleDTO toTalonariosSucursaleDTO(
			TalonariosSucursale talonariosSucursale) {
		TalonariosSucursaleDTO taDTO = new TalonariosSucursaleDTO();
		BeanUtils.copyProperties(talonariosSucursale, taDTO);
		return taDTO;
	}

	public static TalonariosSucursale fromTalonariosSucursaleDTO(
			TalonariosSucursaleDTO talDTO) {
		TalonariosSucursale tal = new TalonariosSucursale();
		BeanUtils.copyProperties(talDTO, tal);
		return tal;
	}

	public static TalonariosSucursale fromTalonariosSucursale(
			TalonariosSucursaleDTO talDTO) {
		TalonariosSucursale tal = fromTalonariosSucursale(talDTO);
		tal.setIpBoca(IpBoca.fromIpBocaDTO(talDTO.getIpBoca()));
		tal.setSucursal(Sucursal.fromSucursalDTO(talDTO.getSucursal()));
		tal.setTimbrado(Timbrado.fromTimbrado(talDTO.getTimbrado()));
		return tal;
	}

}