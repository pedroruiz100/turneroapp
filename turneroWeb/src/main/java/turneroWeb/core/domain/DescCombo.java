package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;


/**
 * The persistent class for the desc_combo database table.
 * 
 */
@Entity
@Table(name = "desc_combo", schema = "factura_cliente")
@NamedQuery(name="DescCombo.findAll", query="SELECT d FROM DescCombo d")
public class DescCombo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_combo")
	private Long idDescCombo;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteDet
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_det")
	private FacturaClienteDet facturaClienteDet;

	//bi-directional many-to-one association to FacturaClienteDet
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_det")
//	private FacturaClienteDet facturaClienteDet2;

	public DescCombo() {
	}

	public Long getIdDescCombo() {
		return this.idDescCombo;
	}

	public void setIdDescCombo(Long idDescCombo) {
		this.idDescCombo = idDescCombo;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteDet getFacturaClienteDet() {
		return this.facturaClienteDet;
	}

	public void setFacturaClienteDet(FacturaClienteDet facturaClienteDet) {
		this.facturaClienteDet = facturaClienteDet;
	}

	// public FacturaClienteDet getFacturaClienteDet2() {
	// return this.facturaClienteDet2;
	// }
	//
	// public void setFacturaClienteDet2(FacturaClienteDet facturaClienteDet2) {
	// this.facturaClienteDet2 = facturaClienteDet2;
	// }

}