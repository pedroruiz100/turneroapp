package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the factura_cliente_cab_moneda_extranjera database
 * table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_moneda_extranjera", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabMonedaExtranjera.findAll", query = "SELECT f FROM FacturaClienteCabMonedaExtranjera f")
public class FacturaClienteCabMonedaExtranjera implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_moneda_extranjera")
	private Long idFacturaClienteCabMonedaExtranjera;

	private Integer cotizacion;

	@Column(name = "monto_extranjero")
	private Integer montoExtranjero;

	@Column(name = "monto_guaranies")
	private Integer montoGuaranies;

	// bi-directional many-to-one association to DescMonedaExtranjera
//	@OneToMany(mappedBy = "facturaClienteCabMonedaExtranjera1")
//	private List<DescMonedaExtranjera> descMonedaExtranjeras1;
//
//	// bi-directional many-to-one association to DescMonedaExtranjera
//	@OneToMany(mappedBy = "facturaClienteCabMonedaExtranjera2")
//	private List<DescMonedaExtranjera> descMonedaExtranjeras2;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	// bi-directional many-to-one association to TipoMoneda
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_tipo_moneda")
	private TipoMoneda tipoMoneda;

	// bi-directional many-to-one association to TipoMoneda
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_tipo_moneda")
//	private TipoMoneda tipoMoneda2;

	public FacturaClienteCabMonedaExtranjera() {
	}

	public Long getIdFacturaClienteCabMonedaExtranjera() {
		return this.idFacturaClienteCabMonedaExtranjera;
	}

	public void setIdFacturaClienteCabMonedaExtranjera(
			Long idFacturaClienteCabMonedaExtranjera) {
		this.idFacturaClienteCabMonedaExtranjera = idFacturaClienteCabMonedaExtranjera;
	}

	public Integer getCotizacion() {
		return this.cotizacion;
	}

	public void setCotizacion(Integer cotizacion) {
		this.cotizacion = cotizacion;
	}

	public Integer getMontoExtranjero() {
		return this.montoExtranjero;
	}

	public void setMontoExtranjero(Integer montoExtranjero) {
		this.montoExtranjero = montoExtranjero;
	}

	public Integer getMontoGuaranies() {
		return this.montoGuaranies;
	}

	public void setMontoGuaranies(Integer montoGuaranies) {
		this.montoGuaranies = montoGuaranies;
	}

	// public List<DescMonedaExtranjera> getDescMonedaExtranjeras1() {
	// return this.descMonedaExtranjeras1;
	// }
	//
	// public void setDescMonedaExtranjeras1(
	// List<DescMonedaExtranjera> descMonedaExtranjeras1) {
	// this.descMonedaExtranjeras1 = descMonedaExtranjeras1;
	// }
	//
	// public DescMonedaExtranjera addDescMonedaExtranjeras1(
	// DescMonedaExtranjera descMonedaExtranjeras1) {
	// getDescMonedaExtranjeras1().add(descMonedaExtranjeras1);
	// descMonedaExtranjeras1.setFacturaClienteCabMonedaExtranjera1(this);
	//
	// return descMonedaExtranjeras1;
	// }
	//
	// public DescMonedaExtranjera removeDescMonedaExtranjeras1(
	// DescMonedaExtranjera descMonedaExtranjeras1) {
	// getDescMonedaExtranjeras1().remove(descMonedaExtranjeras1);
	// descMonedaExtranjeras1.setFacturaClienteCabMonedaExtranjera1(null);
	//
	// return descMonedaExtranjeras1;
	// }
	//
	// public List<DescMonedaExtranjera> getDescMonedaExtranjeras2() {
	// return this.descMonedaExtranjeras2;
	// }
	//
	// public void setDescMonedaExtranjeras2(
	// List<DescMonedaExtranjera> descMonedaExtranjeras2) {
	// this.descMonedaExtranjeras2 = descMonedaExtranjeras2;
	// }
	//
	// public DescMonedaExtranjera addDescMonedaExtranjeras2(
	// DescMonedaExtranjera descMonedaExtranjeras2) {
	// getDescMonedaExtranjeras2().add(descMonedaExtranjeras2);
	// descMonedaExtranjeras2.setFacturaClienteCabMonedaExtranjera2(this);
	//
	// return descMonedaExtranjeras2;
	// }
	//
	// public DescMonedaExtranjera removeDescMonedaExtranjeras2(
	// DescMonedaExtranjera descMonedaExtranjeras2) {
	// getDescMonedaExtranjeras2().remove(descMonedaExtranjeras2);
	// descMonedaExtranjeras2.setFacturaClienteCabMonedaExtranjera2(null);
	//
	// return descMonedaExtranjeras2;
	// }

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public TipoMoneda getTipoMoneda() {
		return this.tipoMoneda;
	}

	public void setTipoMoneda(TipoMoneda tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

	// public TipoMoneda getTipoMoneda2() {
	// return this.tipoMoneda2;
	// }
	//
	// public void setTipoMoneda2(TipoMoneda tipoMoneda2) {
	// this.tipoMoneda2 = tipoMoneda2;
	// }

}