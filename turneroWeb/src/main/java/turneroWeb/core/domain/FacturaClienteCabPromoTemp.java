package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the factura_cliente_cab_promo_temp database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_promo_temp", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPromoTemp.findAll", query = "SELECT f FROM FacturaClienteCabPromoTemp f")
public class FacturaClienteCabPromoTemp implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_promo_temp")
	private Long idFacturaClienteCabPromoTemp;

	@Column(name = "descripcion_temporada")
	private String descripcionTemporada;

	@Column(name = "id_temporada")
	private Long idTemporada;

	private Integer monto;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabPromoTemp() {
	}

	public Long getIdFacturaClienteCabPromoTemp() {
		return this.idFacturaClienteCabPromoTemp;
	}

	public void setIdFacturaClienteCabPromoTemp(
			Long idFacturaClienteCabPromoTemp) {
		this.idFacturaClienteCabPromoTemp = idFacturaClienteCabPromoTemp;
	}

	public String getDescripcionTemporada() {
		return this.descripcionTemporada;
	}

	public void setDescripcionTemporada(String descripcionTemporada) {
		this.descripcionTemporada = descripcionTemporada;
	}

	public Long getIdTemporada() {
		return this.idTemporada;
	}

	public void setIdTemporada(Long idTemporada) {
		this.idTemporada = idTemporada;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}