package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the nota_credito_cab database table.
 * 
 */
@Entity
@Table(name="nota_credito_cab", schema = "factura_cliente")
@NamedQuery(name="NotaCreditoCab.findAll", query="SELECT n FROM NotaCreditoCab n")
public class NotaCreditoCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nota_credito_cab")
	private Long idNotaCreditoCab;

	@Column(name="fecha_anulacion")
	private Time fechaAnulacion;

	@Column(name="fecha_emision")
	private Time fechaEmision;

	@Column(name="id_cajero_anulacion")
	private Integer idCajeroAnulacion;

	@Column(name="id_cliente")
	private Long idCliente;

	@Column(name="id_sucursal")
	private Long idSucursal;

	@Column(name="monto_pago")
	private BigDecimal montoPago;

	@Column(name="motivo_anulacion")
	private String motivoAnulacion;

	@Column(name="nro_nota_credito")
	private String nroNotaCredito;

	private String observaciones;

	private String razon;

	@Column(name="total_credito")
	private BigDecimal totalCredito;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	//bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	//bi-directional many-to-one association to TipoComprobante
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_comprobante")
	private TipoComprobante tipoComprobante;

	//bi-directional many-to-one association to TipoComprobante
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_tipo_comprobante")
//	private TipoComprobante tipoComprobante2;

	//bi-directional many-to-one association to TipoMoneda
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_moneda")
	private TipoMoneda tipoMoneda;

	//bi-directional many-to-one association to TipoMoneda
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_tipo_moneda")
//	private TipoMoneda tipoMoneda2;

	//bi-directional many-to-one association to NotaCreditoDet
//	@OneToMany(mappedBy="notaCreditoCab1")
//	private List<NotaCreditoDet> notaCreditoDets1;
//
//	//bi-directional many-to-one association to NotaCreditoDet
//	@OneToMany(mappedBy="notaCreditoCab2")
//	private List<NotaCreditoDet> notaCreditoDets2;

	public NotaCreditoCab() {
	}

	public Long getIdNotaCreditoCab() {
		return this.idNotaCreditoCab;
	}

	public void setIdNotaCreditoCab(Long idNotaCreditoCab) {
		this.idNotaCreditoCab = idNotaCreditoCab;
	}

	public Time getFechaAnulacion() {
		return this.fechaAnulacion;
	}

	public void setFechaAnulacion(Time fechaAnulacion) {
		this.fechaAnulacion = fechaAnulacion;
	}

	public Time getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Time fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Integer getIdCajeroAnulacion() {
		return this.idCajeroAnulacion;
	}

	public void setIdCajeroAnulacion(Integer idCajeroAnulacion) {
		this.idCajeroAnulacion = idCajeroAnulacion;
	}

	public Long getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdSucursal() {
		return this.idSucursal;
	}

	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

	public BigDecimal getMontoPago() {
		return this.montoPago;
	}

	public void setMontoPago(BigDecimal montoPago) {
		this.montoPago = montoPago;
	}

	public String getMotivoAnulacion() {
		return this.motivoAnulacion;
	}

	public void setMotivoAnulacion(String motivoAnulacion) {
		this.motivoAnulacion = motivoAnulacion;
	}

	public String getNroNotaCredito() {
		return this.nroNotaCredito;
	}

	public void setNroNotaCredito(String nroNotaCredito) {
		this.nroNotaCredito = nroNotaCredito;
	}

	public String getObservaciones() {
		return this.observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getRazon() {
		return this.razon;
	}

	public void setRazon(String razon) {
		this.razon = razon;
	}

	public BigDecimal getTotalCredito() {
		return this.totalCredito;
	}

	public void setTotalCredito(BigDecimal totalCredito) {
		this.totalCredito = totalCredito;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

//	public FacturaClienteCab getFacturaClienteCab2() {
//		return this.facturaClienteCab2;
//	}
//
//	public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
//		this.facturaClienteCab2 = facturaClienteCab2;
//	}

	public TipoComprobante getTipoComprobante() {
		return this.tipoComprobante;
	}

	public void setTipoComprobante1(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

//	public TipoComprobante getTipoComprobante2() {
//		return this.tipoComprobante2;
//	}
//
//	public void setTipoComprobante2(TipoComprobante tipoComprobante2) {
//		this.tipoComprobante2 = tipoComprobante2;
//	}

	public TipoMoneda getTipoMoneda() {
		return this.tipoMoneda;
	}

	public void setTipoMoneda(TipoMoneda tipoMoneda) {
		this.tipoMoneda = tipoMoneda;
	}

//	public TipoMoneda getTipoMoneda2() {
//		return this.tipoMoneda2;
//	}
//
//	public void setTipoMoneda2(TipoMoneda tipoMoneda2) {
//		this.tipoMoneda2 = tipoMoneda2;
//	}

//	public List<NotaCreditoDet> getNotaCreditoDets1() {
//		return this.notaCreditoDets1;
//	}
//
//	public void setNotaCreditoDets1(List<NotaCreditoDet> notaCreditoDets1) {
//		this.notaCreditoDets1 = notaCreditoDets1;
//	}
//
//	public NotaCreditoDet addNotaCreditoDets1(NotaCreditoDet notaCreditoDets1) {
//		getNotaCreditoDets1().add(notaCreditoDets1);
//		notaCreditoDets1.setNotaCreditoCab1(this);
//
//		return notaCreditoDets1;
//	}
//
//	public NotaCreditoDet removeNotaCreditoDets1(NotaCreditoDet notaCreditoDets1) {
//		getNotaCreditoDets1().remove(notaCreditoDets1);
//		notaCreditoDets1.setNotaCreditoCab1(null);
//
//		return notaCreditoDets1;
//	}

//	public List<NotaCreditoDet> getNotaCreditoDets2() {
//		return this.notaCreditoDets2;
//	}
//
//	public void setNotaCreditoDets2(List<NotaCreditoDet> notaCreditoDets2) {
//		this.notaCreditoDets2 = notaCreditoDets2;
//	}

	// public NotaCreditoDet addNotaCreditoDets2(NotaCreditoDet
	// notaCreditoDets2) {
	// getNotaCreditoDets2().add(notaCreditoDets2);
	// notaCreditoDets2.setNotaCreditoCab2(this);
	//
	// return notaCreditoDets2;
	// }
	//
	// public NotaCreditoDet removeNotaCreditoDets2(NotaCreditoDet
	// notaCreditoDets2) {
	// getNotaCreditoDets2().remove(notaCreditoDets2);
	// notaCreditoDets2.setNotaCreditoCab2(null);
	//
	// return notaCreditoDets2;
	// }

}