package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.FacturaClienteCabEfectivoDTO;

/**
 * The persistent class for the factura_cliente_cab_efectivo database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_efectivo", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabEfectivo.findAll", query = "SELECT f FROM FacturaClienteCabEfectivo f")
public class FacturaClienteCabEfectivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_efectivo")
	private Long idFacturaClienteCabEfectivo;

	@Column(name = "monto_efectivo")
	private Integer montoEfectivo;

	// bi-directional many-to-one association to DescEfectivo
//	@OneToMany(mappedBy = "facturaClienteCabEfectivo")
//	private List<DescEfectivo> descEfectivos;

	// bi-directional many-to-one association to DescEfectivo
//	@OneToMany(mappedBy = "facturaClienteCabEfectivo2")
//	private List<DescEfectivo> descEfectivos2;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabEfectivo() {
	}

	public Long getIdFacturaClienteCabEfectivo() {
		return this.idFacturaClienteCabEfectivo;
	}

	public void setIdFacturaClienteCabEfectivo(Long idFacturaClienteCabEfectivo) {
		this.idFacturaClienteCabEfectivo = idFacturaClienteCabEfectivo;
	}

	public Integer getMontoEfectivo() {
		return this.montoEfectivo;
	}

	public void setMontoEfectivo(Integer montoEfectivo) {
		this.montoEfectivo = montoEfectivo;
	}

	// public List<DescEfectivo> getDescEfectivos1() {
	// return this.descEfectivos1;
	// }
	//
	// public void setDescEfectivos1(List<DescEfectivo> descEfectivos1) {
	// this.descEfectivos1 = descEfectivos1;
	// }
	//
	// public DescEfectivo addDescEfectivos1(DescEfectivo descEfectivos1) {
	// getDescEfectivos1().add(descEfectivos1);
	// descEfectivos1.setFacturaClienteCabEfectivo1(this);
	//
	// return descEfectivos1;
	// }
	//
	// public DescEfectivo removeDescEfectivos1(DescEfectivo descEfectivos1) {
	// getDescEfectivos1().remove(descEfectivos1);
	// descEfectivos1.setFacturaClienteCabEfectivo1(null);
	//
	// return descEfectivos1;
	// }
	//
	// public List<DescEfectivo> getDescEfectivos2() {
	// return this.descEfectivos2;
	// }
	//
	// public void setDescEfectivos2(List<DescEfectivo> descEfectivos2) {
	// this.descEfectivos2 = descEfectivos2;
	// }
	//
	// public DescEfectivo addDescEfectivos2(DescEfectivo descEfectivos2) {
	// getDescEfectivos2().add(descEfectivos2);
	// descEfectivos2.setFacturaClienteCabEfectivo2(this);
	//
	// return descEfectivos2;
	// }
	//
	// public DescEfectivo removeDescEfectivos2(DescEfectivo descEfectivos2) {
	// getDescEfectivos2().remove(descEfectivos2);
	// descEfectivos2.setFacturaClienteCabEfectivo2(null);
	//
	// return descEfectivos2;
	// }

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}
	
	public FacturaClienteCabEfectivoDTO toFacturaClienteCabEfectivoDTO() {
        FacturaClienteCabEfectivoDTO facDTO = toFacturaClienteCabEfectivoDTO(this);
        if (this.facturaClienteCab != null) {
            facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
                    .toBDFacturaClienteCabDTO());
        }
//        facDTO.setDescEfectivo(null);
        return facDTO;
    }
	
	public static FacturaClienteCabEfectivoDTO toFacturaClienteCabEfectivoDTO(
            FacturaClienteCabEfectivo facturaClienteCabEfectivo) {
        FacturaClienteCabEfectivoDTO facDTO = new FacturaClienteCabEfectivoDTO();
        BeanUtils.copyProperties(facturaClienteCabEfectivo, facDTO);
        return facDTO;
    }

    public FacturaClienteCabEfectivoDTO toDescriFacturaClienteCabEfectivoDTO() {
        FacturaClienteCabEfectivoDTO facDTO = toFacturaClienteCabEfectivoDTO(this);
        facDTO.setFacturaClienteCab(null);
//        facDTO.setDescEfectivo(null);
        return facDTO;
    }
    
    public static FacturaClienteCabEfectivo fromFacturaClienteCabEfectivoDTO(
            FacturaClienteCabEfectivoDTO facDTO) {
        FacturaClienteCabEfectivo fac = new FacturaClienteCabEfectivo();
        BeanUtils.copyProperties(facDTO, fac);
        return fac;
    }

    public static FacturaClienteCabEfectivo fromFacturaClienteCabEfectivoAsociadoDTO(
            FacturaClienteCabEfectivoDTO facDTO) {
        FacturaClienteCabEfectivo fac = fromFacturaClienteCabEfectivoDTO(facDTO);
        fac.setFacturaClienteCab(FacturaClienteCab
                .fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
        return fac;
    }

}