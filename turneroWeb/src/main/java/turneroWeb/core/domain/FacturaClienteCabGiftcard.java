package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the factura_cliente_cab_giftcard database table.
 * 
 */
@Entity
@Table(name="factura_cliente_cab_giftcard", schema = "factura_cliente")
@NamedQuery(name="FacturaClienteCabGiftcard.findAll", query="SELECT f FROM FacturaClienteCabGiftcard f")
public class FacturaClienteCabGiftcard implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_factura_cliente_cab_giftcard")
	private Long idFacturaClienteCabGiftcard;

	private Long codigo;

	private Integer monto;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabGiftcard() {
	}

	public Long getIdFacturaClienteCabGiftcard() {
		return this.idFacturaClienteCabGiftcard;
	}

	public void setIdFacturaClienteCabGiftcard(Long idFacturaClienteCabGiftcard) {
		this.idFacturaClienteCabGiftcard = idFacturaClienteCabGiftcard;
	}

	public Long getCodigo() {
		return this.codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

}