package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.RetiroPedidoDTO;

/**
 * The persistent class for the barrio database table.
 * 
 */
@Entity
@Table(name = "retiro_pedido", schema = "general")
@NamedQuery(name = "RetiroPedido.findAll", query = "SELECT b FROM RetiroPedido b")
public class RetiroPedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_retiro")
	private Long idRetiro;

	// bi-directional many-to-one association to Ciudad
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	private String numero;

	private String cliente;

	private Boolean entregado;

	private Boolean preparacion;

	private Boolean listo;

	private String hora;

	// // bi-directional many-to-one association to Sucursal
	// @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
	// private List<Sucursal> sucursals;
	//
	// // bi-directional many-to-one association to Cliente
	// @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
	// private List<Cliente> clientes;
	//
	// // bi-directional many-to-one association to FamiliaTarj
	// @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
	// private List<FamiliaTarj> familiaTarjs;
	//
	// // bi-directional many-to-one association to Proveedor
	// @OneToMany(mappedBy = "barrio", fetch = FetchType.LAZY)
	// private List<Proveedor> proveedors;

	public RetiroPedido() {
	}

	// ****************************************************************************************

	public Long getIdRetiro() {
		return idRetiro;
	}

	public void setIdRetiro(Long idRetiro) {
		this.idRetiro = idRetiro;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public Boolean getEntregado() {
		return entregado;
	}

	public void setEntregado(Boolean entregado) {
		this.entregado = entregado;
	}

	public Boolean getPreparacion() {
		return preparacion;
	}

	public void setPreparacion(Boolean preparacion) {
		this.preparacion = preparacion;
	}

	public Boolean getListo() {
		return listo;
	}

	public void setListo(Boolean listo) {
		this.listo = listo;
	}

	// Este metodo nos sirve para que cargue las listas y las Clases que tiene
	// esta entidad
	// en este caso la clase Ciudad y la lista de Sucursales
	public RetiroPedidoDTO toRetiroPedidoDTO() {
		// convertimos los datos recuperados a DTO
		RetiroPedidoDTO barrioDTO = toRetiroPedidoDTO(this);
		if (this.getFacturaClienteCab() != null) {
			barrioDTO.setFacturaClienteCab(this.getFacturaClienteCab()
					.toDetallesPromoArtFacturaClienteCabDTO());
		}
		// Verificamos que hay una Clase (entidad relacionada con Barrio, en
		// este caso Ciudad)
		// verificamos que los datos de la consulta no nos traiga ciudad con
		// valor null
		// if (this.getCiudad() != null) {
		// // Creamos una variable para cargar los datos recuperados
		// Ciudad ciudad = this.getCiudad();
		// // Colocar null a los campos que vuelve hacer referencia en los list
		// // de la clase Ciudad
		// // Para este caso Barrio y Sucursal
		// ciudad.setBarrios(null);
		// ciudad.setSucursals(null);
		// ciudad.setClientes(null);
		// ciudad.setFamiliaTarjs(null);
		// ciudad.setProveedors(null);
		// // ciudad.toCiudadDTO() este metodo convierte a DTO las entidades
		// // recuperadas en la consulta
		// // barrioDTO.setCiudad(ciudad.toCiudadDTO()); a BarrioDTO le agrega
		// // la ciudadDTO que se obtuvo con el metodo explicado una linea
		// // arriba
		// barrioDTO.setCiudadDTO(ciudad.toCiudadDTO());
		// }

		// Verificamos que la lista de Sucursales no es vacia
		// if (!this.getSucursals().isEmpty()) {
		// // Recorremos los datos obtenidos de sucursales por medio de un for
		// // each
		// for (Sucursal sucu : this.getSucursals()) {
		// // cada vez que haya un recorrido se inicializa un List de
		// // SucursalDTO para pasarlo al barrioDTO
		// List<SucursalDTO> sDTO = new ArrayList<SucursalDTO>();
		//
		// // *************************//
		// // Anteriormente teniamos de esta manera, esto nos servia para
		// // que a la hora de llamarle a la lista de sucursal podamos a
		// // traves de este
		// // recuperar las entidades de Barrio, pero esto ser�a redundante
		// // entonces lo dejamos en null
		//
		// // OBSERVACION : recordar que es conveniente utilizar este
		// // metodo en el caso que la idea sea recuperar entidades de otra
		// // clase por medio de listas
		// // como ejemplo en este caso dentro de la lista sucursal, datos
		// // de la clase Barrio
		//
		// // sucu.toSucursalDTO(this.toBarrioDTO(this)
		// // *************************//
		//
		// // sucu.toSucursalDTO(null) este metodo nos sirve para indicar
		// // que entidad son las que podemos obtener de esa lista, en este
		// // caso si quisieramos obtener
		// // las entidades de Barrio y TalonariosSucursales podemos
		// // solicitarle, caso contrario si queremos que no nos traiga
		// // colocar null, como lo hicimos en este caso
		// // sDTO.add(sucu.toSucursalDTO(null)); agregamos al array sDTO
		// // creado un poco mas arriba
		// sDTO.add(sucu.toSucursalDescriDTO());
		// // esto agregamos a setSucursals(sDTO)
		// barrioDTO.setSucursalsDTOs(sDTO);
		// }
		// barrioDTO.setClientesDTO(null);
		// barrioDTO.setFamiliaTarjDTO(null);
		// barrioDTO.setProveedorDTO(null);
		// }
		return barrioDTO;
	}

	// Metodo que sirve para cargar los datos de Barrio a BarrioDTO
	public static RetiroPedidoDTO toRetiroPedidoDTO(RetiroPedido barrio) {
		RetiroPedidoDTO barrioDTO = new RetiroPedidoDTO();
		BeanUtils.copyProperties(barrio, barrioDTO);
		return barrioDTO;
	}

	public static RetiroPedido fromRetiroPedido(RetiroPedidoDTO barrioDTO) {
		RetiroPedido barrio = new RetiroPedido();
		BeanUtils.copyProperties(barrioDTO, barrio);
		return barrio;
	}

	public static RetiroPedido fromRetiroPedidoDTOAsociado(
			RetiroPedidoDTO barrioDTO) {
		RetiroPedido bar = fromRetiroPedido(barrioDTO);
		bar.setFacturaClienteCab(FacturaClienteCab
				.fromFacturaClienteCabDTO(barrioDTO.getFacturaClienteCab()));
		return bar;
	}

	// BARRIO CLIENTE

	// public BarrioDTO toBarrioAClienteDTO() {
	//
	// BarrioDTO barrioDTO = toBarrioDTO(this);
	//
	// barrioDTO.setCiudadDTO(null);
	// // barrioDTO.setSucursalsDTOs(null);
	// // barrioDTO.setClientesDTO(null);
	// // barrioDTO.setFamiliaTarjDTO(null);
	// // barrioDTO.setProveedorDTO(null);
	//
	// return barrioDTO;
	// }

	// FIN DE BARRIO A CLIENTE

	// PARA FAMILIA TARJETA

	// public BarrioDTO toBarrioDTOAFamiliaTarjDTO() {
	// BarrioDTO barrioDTO = toBarrioDTO(this);
	// barrioDTO.setCiudadDTO(null);
	// // barrioDTO.setClientesDTO(null);
	// // barrioDTO.setFamiliaTarjDTO(null);
	// // barrioDTO.setProveedorDTO(null);
	// // barrioDTO.setSucursalsDTOs(null);
	// return barrioDTO;
	// }

	// FIN PARA FAMILIA TARJETA
}