package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the tipo_pago database table.
 * 
 */
@Entity
@Table(name = "tipo_pago", schema = "factura_cliente")
@NamedQuery(name = "TipoPago.findAll", query = "SELECT t FROM TipoPago t")
public class TipoPago implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_pago")
	private Long idTipoPago;

	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	// bi-directional many-to-one association to FacturaClienteCabTipoPago
//	@OneToMany(mappedBy = "tipoPago1")
//	private List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos1;

	// bi-directional many-to-one association to FacturaClienteCabTipoPago
//	@OneToMany(mappedBy = "tipoPago2")
//	private List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos2;

	public TipoPago() {
	}

	public Long getIdTipoPago() {
		return this.idTipoPago;
	}

	public void setIdTipoPago(Long idTipoPago) {
		this.idTipoPago = idTipoPago;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

//	public List<FacturaClienteCabTipoPago> getFacturaClienteCabTipoPagos1() {
//		return this.facturaClienteCabTipoPagos1;
//	}
//
//	public void setFacturaClienteCabTipoPagos1(
//			List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos1) {
//		this.facturaClienteCabTipoPagos1 = facturaClienteCabTipoPagos1;
//	}
//
//	public FacturaClienteCabTipoPago addFacturaClienteCabTipoPagos1(
//			FacturaClienteCabTipoPago facturaClienteCabTipoPagos1) {
//		getFacturaClienteCabTipoPagos1().add(facturaClienteCabTipoPagos1);
//		facturaClienteCabTipoPagos1.setTipoPago1(this);
//
//		return facturaClienteCabTipoPagos1;
//	}
//
//	public FacturaClienteCabTipoPago removeFacturaClienteCabTipoPagos1(
//			FacturaClienteCabTipoPago facturaClienteCabTipoPagos1) {
//		getFacturaClienteCabTipoPagos1().remove(facturaClienteCabTipoPagos1);
//		facturaClienteCabTipoPagos1.setTipoPago1(null);
//
//		return facturaClienteCabTipoPagos1;
//	}
//
//	public List<FacturaClienteCabTipoPago> getFacturaClienteCabTipoPagos2() {
//		return this.facturaClienteCabTipoPagos2;
//	}
//
//	public void setFacturaClienteCabTipoPagos2(
//			List<FacturaClienteCabTipoPago> facturaClienteCabTipoPagos2) {
//		this.facturaClienteCabTipoPagos2 = facturaClienteCabTipoPagos2;
//	}

//	public FacturaClienteCabTipoPago addFacturaClienteCabTipoPagos2(
//			FacturaClienteCabTipoPago facturaClienteCabTipoPagos2) {
//		getFacturaClienteCabTipoPagos2().add(facturaClienteCabTipoPagos2);
//		facturaClienteCabTipoPagos2.setTipoPago2(this);
//
//		return facturaClienteCabTipoPagos2;
//	}

	// public FacturaClienteCabTipoPago removeFacturaClienteCabTipoPagos2(
	// FacturaClienteCabTipoPago facturaClienteCabTipoPagos2) {
	// getFacturaClienteCabTipoPagos2().remove(facturaClienteCabTipoPagos2);
	// facturaClienteCabTipoPagos2.setTipoPago2(null);
	//
	// return facturaClienteCabTipoPagos2;
	// }

}