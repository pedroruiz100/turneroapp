package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the nota_remision_det database table.
 * 
 */
@Entity
@Table(name = "nota_remision_det", schema = "factura_cliente")
@NamedQuery(name = "NotaRemisionDet.findAll", query = "SELECT n FROM NotaRemisionDet n")
public class NotaRemisionDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_nota_remision_det")
	private Long idNotaRemisionDet;

	private Integer cantidad;

	private String descripcion;

	@Column(name = "id_articulo")
	private Long idArticulo;

	// bi-directional many-to-one association to NotaRemisionCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_nota_remision_cab")
	private NotaRemisionCab notaRemisionCab;

	// bi-directional many-to-one association to NotaRemisionCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_nota_remision_cab")
//	private NotaRemisionCab notaRemisionCab2;

	public NotaRemisionDet() {
	}

	public Long getIdNotaRemisionDet() {
		return this.idNotaRemisionDet;
	}

	public void setIdNotaRemisionDet(Long idNotaRemisionDet) {
		this.idNotaRemisionDet = idNotaRemisionDet;
	}

	public Integer getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getIdArticulo() {
		return this.idArticulo;
	}

	public void setIdArticulo(Long idArticulo) {
		this.idArticulo = idArticulo;
	}

	public NotaRemisionCab getNotaRemisionCab() {
		return this.notaRemisionCab;
	}

	public void setNotaRemisionCab(NotaRemisionCab notaRemisionCab) {
		this.notaRemisionCab = notaRemisionCab;
	}

	// public NotaRemisionCab getNotaRemisionCab2() {
	// return this.notaRemisionCab2;
	// }
	//
	// public void setNotaRemisionCab2(NotaRemisionCab notaRemisionCab2) {
	// this.notaRemisionCab2 = notaRemisionCab2;
	// }

}