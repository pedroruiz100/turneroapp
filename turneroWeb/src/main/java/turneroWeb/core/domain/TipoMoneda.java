package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.TipoMonedaDTO;


/**
 * The persistent class for the tipo_moneda database table.
 * 
 */
@Entity
@Table(name="tipo_moneda", schema = "factura_cliente")
@NamedQuery(name="TipoMoneda.findAll", query="SELECT t FROM TipoMoneda t")
public class TipoMoneda implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tipo_moneda")
	private Long idTipoMoneda;

	private BigDecimal compra;

	private String descripcion;

	@Column(name="fecha_alta")
	private Timestamp fechaAlta;

	@Column(name="fecha_mod")
	private Timestamp fechaMod;

	@Column(name="usu_alta")
	private String usuAlta;

	@Column(name="usu_mod")
	private String usuMod;

	private BigDecimal venta;

//	//bi-directional many-to-one association to FacturaClienteCab
//	@OneToMany(mappedBy="tipoMoneda1")
//	private List<FacturaClienteCab> facturaClienteCabs1;
//
//	//bi-directional many-to-one association to FacturaClienteCab
//	@OneToMany(mappedBy="tipoMoneda2")
//	private List<FacturaClienteCab> facturaClienteCabs2;
//
//	//bi-directional many-to-one association to FacturaClienteCabMonedaExtranjera
//	@OneToMany(mappedBy="tipoMoneda1")
//	private List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjeras1;
//
//	//bi-directional many-to-one association to FacturaClienteCabMonedaExtranjera
//	@OneToMany(mappedBy="tipoMoneda2")
//	private List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjeras2;
//
//	//bi-directional many-to-one association to NotaCreditoCab
//	@OneToMany(mappedBy="tipoMoneda1")
//	private List<NotaCreditoCab> notaCreditoCabs1;
//
//	//bi-directional many-to-one association to NotaCreditoCab
//	@OneToMany(mappedBy="tipoMoneda2")
//	private List<NotaCreditoCab> notaCreditoCabs2;

	public TipoMoneda() {
	}

	public Long getIdTipoMoneda() {
		return this.idTipoMoneda;
	}

	public void setIdTipoMoneda(Long idTipoMoneda) {
		this.idTipoMoneda = idTipoMoneda;
	}

	public BigDecimal getCompra() {
		return this.compra;
	}

	public void setCompra(BigDecimal compra) {
		this.compra = compra;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public BigDecimal getVenta() {
		return this.venta;
	}

	public void setVenta(BigDecimal venta) {
		this.venta = venta;
	}

//	public List<FacturaClienteCab> getFacturaClienteCabs1() {
//		return this.facturaClienteCabs1;
//	}
//
//	public void setFacturaClienteCabs1(List<FacturaClienteCab> facturaClienteCabs1) {
//		this.facturaClienteCabs1 = facturaClienteCabs1;
//	}
//
//	public FacturaClienteCab addFacturaClienteCabs1(FacturaClienteCab facturaClienteCabs1) {
//		getFacturaClienteCabs1().add(facturaClienteCabs1);
//		facturaClienteCabs1.setTipoMoneda1(this);
//
//		return facturaClienteCabs1;
//	}
//
//	public FacturaClienteCab removeFacturaClienteCabs1(FacturaClienteCab facturaClienteCabs1) {
//		getFacturaClienteCabs1().remove(facturaClienteCabs1);
//		facturaClienteCabs1.setTipoMoneda1(null);
//
//		return facturaClienteCabs1;
//	}
//
//	public List<FacturaClienteCab> getFacturaClienteCabs2() {
//		return this.facturaClienteCabs2;
//	}
//
//	public void setFacturaClienteCabs2(List<FacturaClienteCab> facturaClienteCabs2) {
//		this.facturaClienteCabs2 = facturaClienteCabs2;
//	}

//	public FacturaClienteCab addFacturaClienteCabs2(FacturaClienteCab facturaClienteCabs2) {
//		getFacturaClienteCabs2().add(facturaClienteCabs2);
//		facturaClienteCabs2.setTipoMoneda2(this);
//
//		return facturaClienteCabs2;
//	}
//
//	public FacturaClienteCab removeFacturaClienteCabs2(
//			FacturaClienteCab facturaClienteCabs2) {
//		getFacturaClienteCabs2().remove(facturaClienteCabs2);
//		facturaClienteCabs2.setTipoMoneda2(null);
//
//		return facturaClienteCabs2;
//	}

//	public List<FacturaClienteCabMonedaExtranjera> getFacturaClienteCabMonedaExtranjeras1() {
//		return this.facturaClienteCabMonedaExtranjeras1;
//	}
//
//	public void setFacturaClienteCabMonedaExtranjeras1(List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjeras1) {
//		this.facturaClienteCabMonedaExtranjeras1 = facturaClienteCabMonedaExtranjeras1;
//	}

//	public FacturaClienteCabMonedaExtranjera addFacturaClienteCabMonedaExtranjeras1(FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjeras1) {
//		getFacturaClienteCabMonedaExtranjeras1().add(facturaClienteCabMonedaExtranjeras1);
//		facturaClienteCabMonedaExtranjeras1.setTipoMoneda1(this);
//
//		return facturaClienteCabMonedaExtranjeras1;
//	}
//
//	public FacturaClienteCabMonedaExtranjera removeFacturaClienteCabMonedaExtranjeras1(FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjeras1) {
//		getFacturaClienteCabMonedaExtranjeras1().remove(facturaClienteCabMonedaExtranjeras1);
//		facturaClienteCabMonedaExtranjeras1.setTipoMoneda1(null);
//
//		return facturaClienteCabMonedaExtranjeras1;
//	}
//
//	public List<FacturaClienteCabMonedaExtranjera> getFacturaClienteCabMonedaExtranjeras2() {
//		return this.facturaClienteCabMonedaExtranjeras2;
//	}
//
//	public void setFacturaClienteCabMonedaExtranjeras2(List<FacturaClienteCabMonedaExtranjera> facturaClienteCabMonedaExtranjeras2) {
//		this.facturaClienteCabMonedaExtranjeras2 = facturaClienteCabMonedaExtranjeras2;
//	}

//	public FacturaClienteCabMonedaExtranjera addFacturaClienteCabMonedaExtranjeras2(FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjeras2) {
//		getFacturaClienteCabMonedaExtranjeras2().add(facturaClienteCabMonedaExtranjeras2);
//		facturaClienteCabMonedaExtranjeras2.setTipoMoneda2(this);
//
//		return facturaClienteCabMonedaExtranjeras2;
//	}
//
//	public FacturaClienteCabMonedaExtranjera removeFacturaClienteCabMonedaExtranjeras2(FacturaClienteCabMonedaExtranjera facturaClienteCabMonedaExtranjeras2) {
//		getFacturaClienteCabMonedaExtranjeras2().remove(facturaClienteCabMonedaExtranjeras2);
//		facturaClienteCabMonedaExtranjeras2.setTipoMoneda2(null);
//
//		return facturaClienteCabMonedaExtranjeras2;
//	}
//
//	public List<NotaCreditoCab> getNotaCreditoCabs1() {
//		return this.notaCreditoCabs1;
//	}
//
//	public void setNotaCreditoCabs1(List<NotaCreditoCab> notaCreditoCabs1) {
//		this.notaCreditoCabs1 = notaCreditoCabs1;
//	}

//	public NotaCreditoCab addNotaCreditoCabs1(NotaCreditoCab notaCreditoCabs1) {
//		getNotaCreditoCabs1().add(notaCreditoCabs1);
//		notaCreditoCabs1.setTipoMoneda1(this);
//
//		return notaCreditoCabs1;
//	}
//
//	public NotaCreditoCab removeNotaCreditoCabs1(NotaCreditoCab notaCreditoCabs1) {
//		getNotaCreditoCabs1().remove(notaCreditoCabs1);
//		notaCreditoCabs1.setTipoMoneda1(null);
//
//		return notaCreditoCabs1;
//	}
//
//	public List<NotaCreditoCab> getNotaCreditoCabs2() {
//		return this.notaCreditoCabs2;
//	}
//
//	public void setNotaCreditoCabs2(List<NotaCreditoCab> notaCreditoCabs2) {
//		this.notaCreditoCabs2 = notaCreditoCabs2;
//	}
//
//	public NotaCreditoCab addNotaCreditoCabs2(NotaCreditoCab notaCreditoCabs2) {
//		getNotaCreditoCabs2().add(notaCreditoCabs2);
//		notaCreditoCabs2.setTipoMoneda2(this);
//
//		return notaCreditoCabs2;
//	}
//
//	public NotaCreditoCab removeNotaCreditoCabs2(NotaCreditoCab notaCreditoCabs2) {
//		getNotaCreditoCabs2().remove(notaCreditoCabs2);
//		notaCreditoCabs2.setTipoMoneda2(null);
//
//		return notaCreditoCabs2;
//	}
	
	public TipoMonedaDTO toTipoMonedaDTO() {
		TipoMonedaDTO tmDTO = toTipoMonedaDTO(this);
//		tmDTO.setFacturaClienteCabs(null);
//		tmDTO.setNotaCreditoCabs(null);
//		tmDTO.setFacturaClienteCabMonedaExtranjera(null);
		return tmDTO;
	}

	public static TipoMonedaDTO toTipoMonedaDTO(TipoMoneda tipoMoneda) {
		TipoMonedaDTO tmDTO = new TipoMonedaDTO();
		BeanUtils.copyProperties(tipoMoneda, tmDTO);
		return tmDTO;
	}

	// SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
	// tenga)
	public static TipoMoneda fromTipoMonedaDTO(TipoMonedaDTO tmDTO) {
		TipoMoneda tm = new TipoMoneda();
		BeanUtils.copyProperties(tmDTO, tm);
		return tm;
	}

}