package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the factura_cliente_cab_vale database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_vale", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabVale.findAll", query = "SELECT f FROM FacturaClienteCabVale f")
public class FacturaClienteCabVale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_vale")
	private Long idFacturaClienteCabVale;

	private String ci;

	@Column(name = "id_vale")
	private Long idVale;

	private Integer monto;

	@Column(name = "nro_vale")
	private String nroVale;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

//	// bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	public FacturaClienteCabVale() {
	}

	public Long getIdFacturaClienteCabVale() {
		return this.idFacturaClienteCabVale;
	}

	public void setIdFacturaClienteCabVale(Long idFacturaClienteCabVale) {
		this.idFacturaClienteCabVale = idFacturaClienteCabVale;
	}

	public String getCi() {
		return this.ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public Long getIdVale() {
		return this.idVale;
	}

	public void setIdVale(Long idVale) {
		this.idVale = idVale;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public String getNroVale() {
		return this.nroVale;
	}

	public void setNroVale(String nroVale) {
		this.nroVale = nroVale;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}