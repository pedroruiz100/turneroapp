package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;

/**
 * The persistent class for the factura_cliente_cab_porc_vale database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_porc_vale", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabPorcVale.findAll", query = "SELECT f FROM FacturaClienteCabPorcVale f")
public class FacturaClienteCabPorcVale implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_porc_vale")
	private Long idFacturaClienteCabPorcVale;

	private String ci;

	@Column(name = "id_vale")
	private Long idVale;

	private Integer monto;

	@Column(name = "nro_vale")
	private String nroVale;

	@Column(name = "porc_vale")
	private BigDecimal porcVale;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabPorcVale() {
	}

	public Long getIdFacturaClienteCabPorcVale() {
		return this.idFacturaClienteCabPorcVale;
	}

	public void setIdFacturaClienteCabPorcVale(Long idFacturaClienteCabPorcVale) {
		this.idFacturaClienteCabPorcVale = idFacturaClienteCabPorcVale;
	}

	public String getCi() {
		return this.ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}

	public Long getIdVale() {
		return this.idVale;
	}

	public void setIdVale(Long idVale) {
		this.idVale = idVale;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public String getNroVale() {
		return this.nroVale;
	}

	public void setNroVale(String nroVale) {
		this.nroVale = nroVale;
	}

	public BigDecimal getPorcVale() {
		return this.porcVale;
	}

	public void setPorcVale(BigDecimal porcVale) {
		this.porcVale = porcVale;
	}

}