package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the desc_funcionario database table.
 * 
 */
@Entity
@Table(name = "desc_funcionario", schema = "factura_cliente")
@NamedQuery(name="DescFuncionario.findAll", query="SELECT d FROM DescFuncionario d")
public class DescFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_funcionario")
	private Long idDescFuncionario;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabFuncionario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_funcionario")
	private FacturaClienteCabFuncionario facturaClienteCabFuncionario;

	//bi-directional many-to-one association to FacturaClienteCabFuncionario
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_funcionario")
//	private FacturaClienteCabFuncionario facturaClienteCabFuncionario2;

	public DescFuncionario() {
	}

	public Long getIdDescFuncionario() {
		return this.idDescFuncionario;
	}

	public void setIdDescFuncionario(Long idDescFuncionario) {
		this.idDescFuncionario = idDescFuncionario;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabFuncionario getFacturaClienteCabFuncionario() {
		return this.facturaClienteCabFuncionario;
	}

	public void setFacturaClienteCabFuncionario(FacturaClienteCabFuncionario facturaClienteCabFuncionario) {
		this.facturaClienteCabFuncionario = facturaClienteCabFuncionario;
	}

	// public FacturaClienteCabFuncionario getFacturaClienteCabFuncionario2() {
	// return this.facturaClienteCabFuncionario2;
	// }
	//
	// public void setFacturaClienteCabFuncionario2(FacturaClienteCabFuncionario
	// facturaClienteCabFuncionario2) {
	// this.facturaClienteCabFuncionario2 = facturaClienteCabFuncionario2;
	// }

}