package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the factura_cliente_cab_funcionario database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_funcionario", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabFuncionario.findAll", query = "SELECT f FROM FacturaClienteCabFuncionario f")
public class FacturaClienteCabFuncionario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_funcionario")
	private Long idFacturaClienteCabFuncionario;

	@Column(name = "id_funcionario")
	private Long idFuncionario;

	private Integer monto;

//	// bi-directional many-to-one association to DescFuncionario
//	@OneToMany(mappedBy = "facturaClienteCabFuncionario1")
//	private List<DescFuncionario> descFuncionarios1;
//
//	// bi-directional many-to-one association to DescFuncionario
//	@OneToMany(mappedBy = "facturaClienteCabFuncionario2")
//	private List<DescFuncionario> descFuncionarios2;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabFuncionario() {
	}

	public Long getIdFacturaClienteCabFuncionario() {
		return this.idFacturaClienteCabFuncionario;
	}

	public void setIdFacturaClienteCabFuncionario(
			Long idFacturaClienteCabFuncionario) {
		this.idFacturaClienteCabFuncionario = idFacturaClienteCabFuncionario;
	}

	public Long getIdFuncionario() {
		return this.idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	// public List<DescFuncionario> getDescFuncionarios1() {
	// return this.descFuncionarios1;
	// }
	//
	// public void setDescFuncionarios1(List<DescFuncionario> descFuncionarios1)
	// {
	// this.descFuncionarios1 = descFuncionarios1;
	// }
	//
	// public DescFuncionario addDescFuncionarios1(
	// DescFuncionario descFuncionarios1) {
	// getDescFuncionarios1().add(descFuncionarios1);
	// descFuncionarios1.setFacturaClienteCabFuncionario1(this);
	//
	// return descFuncionarios1;
	// }
	//
	// public DescFuncionario removeDescFuncionarios1(
	// DescFuncionario descFuncionarios1) {
	// getDescFuncionarios1().remove(descFuncionarios1);
	// descFuncionarios1.setFacturaClienteCabFuncionario1(null);
	//
	// return descFuncionarios1;
	// }
	//
	// public List<DescFuncionario> getDescFuncionarios2() {
	// return this.descFuncionarios2;
	// }
	//
	// public void setDescFuncionarios2(List<DescFuncionario> descFuncionarios2)
	// {
	// this.descFuncionarios2 = descFuncionarios2;
	// }
	//
	// public DescFuncionario addDescFuncionarios2(
	// DescFuncionario descFuncionarios2) {
	// getDescFuncionarios2().add(descFuncionarios2);
	// descFuncionarios2.setFacturaClienteCabFuncionario2(this);
	//
	// return descFuncionarios2;
	// }
	//
	// public DescFuncionario removeDescFuncionarios2(
	// DescFuncionario descFuncionarios2) {
	// getDescFuncionarios2().remove(descFuncionarios2);
	// descFuncionarios2.setFacturaClienteCabFuncionario2(null);
	//
	// return descFuncionarios2;
	// }

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}