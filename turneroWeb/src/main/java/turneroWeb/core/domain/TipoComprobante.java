package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.TipoComprobanteDTO;

/**
 * The persistent class for the tipo_comprobante database table.
 * 
 */
@Entity
@Table(name = "tipo_comprobante", schema = "factura_cliente")
@NamedQuery(name = "TipoComprobante.findAll", query = "SELECT t FROM TipoComprobante t")
public class TipoComprobante implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tipo_comprobante")
	private Long idTipoComprobante;

	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	// // bi-directional many-to-one association to FacturaClienteCab
	// @OneToMany(mappedBy = "tipoComprobante1")
	// private List<FacturaClienteCab> facturaClienteCabs1;
	//
	// // bi-directional many-to-one association to FacturaClienteCab
	// @OneToMany(mappedBy = "tipoComprobante2")
	// private List<FacturaClienteCab> facturaClienteCabs2;
	//
	// // bi-directional many-to-one association to NotaCreditoCab
	// @OneToMany(mappedBy = "tipoComprobante1")
	// private List<NotaCreditoCab> notaCreditoCabs1;
	//
	// // bi-directional many-to-one association to NotaCreditoCab
	// @OneToMany(mappedBy = "tipoComprobante2")
	// private List<NotaCreditoCab> notaCreditoCabs2;
	//
	// // bi-directional many-to-one association to NotaPresupuestoCab
	// @OneToMany(mappedBy = "tipoComprobante")
	// private List<NotaPresupuestoCab> notaPresupuestoCabs;

	public TipoComprobante() {
	}

	public Long getIdTipoComprobante() {
		return this.idTipoComprobante;
	}

	public void setIdTipoComprobante(Long idTipoComprobante) {
		this.idTipoComprobante = idTipoComprobante;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// public List<FacturaClienteCab> getFacturaClienteCabs1() {
	// return this.facturaClienteCabs1;
	// }
	//
	// public void setFacturaClienteCabs1(
	// List<FacturaClienteCab> facturaClienteCabs1) {
	// this.facturaClienteCabs1 = facturaClienteCabs1;
	// }
	//
	// public FacturaClienteCab addFacturaClienteCabs1(
	// FacturaClienteCab facturaClienteCabs1) {
	// getFacturaClienteCabs1().add(facturaClienteCabs1);
	// facturaClienteCabs1.setTipoComprobante1(this);
	//
	// return facturaClienteCabs1;
	// }
	//
	// public FacturaClienteCab removeFacturaClienteCabs1(
	// FacturaClienteCab facturaClienteCabs1) {
	// getFacturaClienteCabs1().remove(facturaClienteCabs1);
	// facturaClienteCabs1.setTipoComprobante1(null);
	//
	// return facturaClienteCabs1;
	// }
	//
	// public List<FacturaClienteCab> getFacturaClienteCabs2() {
	// return this.facturaClienteCabs2;
	// }
	//
	// public void setFacturaClienteCabs2(
	// List<FacturaClienteCab> facturaClienteCabs2) {
	// this.facturaClienteCabs2 = facturaClienteCabs2;
	// }

	// public FacturaClienteCab addFacturaClienteCabs2(
	// FacturaClienteCab facturaClienteCabs2) {
	// getFacturaClienteCabs2().add(facturaClienteCabs2);
	// facturaClienteCabs2.setTipoComprobante2(this);
	//
	// return facturaClienteCabs2;
	// }
	//
	// public FacturaClienteCab removeFacturaClienteCabs2(
	// FacturaClienteCab facturaClienteCabs2) {
	// getFacturaClienteCabs2().remove(facturaClienteCabs2);
	// facturaClienteCabs2.setTipoComprobante2(null);
	//
	// return facturaClienteCabs2;
	// }

	// public List<NotaCreditoCab> getNotaCreditoCabs1() {
	// return this.notaCreditoCabs1;
	// }
	//
	// public void setNotaCreditoCabs1(List<NotaCreditoCab> notaCreditoCabs1) {
	// this.notaCreditoCabs1 = notaCreditoCabs1;
	// }

	// public NotaCreditoCab addNotaCreditoCabs1(NotaCreditoCab
	// notaCreditoCabs1) {
	// getNotaCreditoCabs1().add(notaCreditoCabs1);
	// notaCreditoCabs1.setTipoComprobante1(this);
	//
	// return notaCreditoCabs1;
	// }
	//
	// public NotaCreditoCab removeNotaCreditoCabs1(NotaCreditoCab
	// notaCreditoCabs1) {
	// getNotaCreditoCabs1().remove(notaCreditoCabs1);
	// notaCreditoCabs1.setTipoComprobante1(null);
	//
	// return notaCreditoCabs1;
	// }
	//
	// public List<NotaCreditoCab> getNotaCreditoCabs2() {
	// return this.notaCreditoCabs2;
	// }
	//
	// public void setNotaCreditoCabs2(List<NotaCreditoCab> notaCreditoCabs2) {
	// this.notaCreditoCabs2 = notaCreditoCabs2;
	// }
	//
	// public NotaCreditoCab addNotaCreditoCabs2(NotaCreditoCab
	// notaCreditoCabs2) {
	// getNotaCreditoCabs2().add(notaCreditoCabs2);
	// notaCreditoCabs2.setTipoComprobante2(this);
	//
	// return notaCreditoCabs2;
	// }
	//
	// public NotaCreditoCab removeNotaCreditoCabs2(NotaCreditoCab
	// notaCreditoCabs2) {
	// getNotaCreditoCabs2().remove(notaCreditoCabs2);
	// notaCreditoCabs2.setTipoComprobante2(null);
	//
	// return notaCreditoCabs2;
	// }
	//
	// public List<NotaPresupuestoCab> getNotaPresupuestoCabs() {
	// return this.notaPresupuestoCabs;
	// }
	//
	// public void setNotaPresupuestoCabs(
	// List<NotaPresupuestoCab> notaPresupuestoCabs) {
	// this.notaPresupuestoCabs = notaPresupuestoCabs;
	// }

	// public NotaPresupuestoCab addNotaPresupuestoCab(
	// NotaPresupuestoCab notaPresupuestoCab) {
	// getNotaPresupuestoCabs().add(notaPresupuestoCab);
	// notaPresupuestoCab.setTipoComprobante(this);
	//
	// return notaPresupuestoCab;
	// }
	//
	// public NotaPresupuestoCab removeNotaPresupuestoCab(
	// NotaPresupuestoCab notaPresupuestoCab) {
	// getNotaPresupuestoCabs().remove(notaPresupuestoCab);
	// notaPresupuestoCab.setTipoComprobante(null);
	//
	// return notaPresupuestoCab;
	// }

	public TipoComprobanteDTO toTipoComprobanteDTO() {
		TipoComprobanteDTO tipoComDTO = toTipoComprobanteDTO(this);
		// tipoComDTO.setFacturaClienteCabs(null);
		// tipoComDTO.setNotaCreditoCabs(null);
		return tipoComDTO;
	}

	public static TipoComprobanteDTO toTipoComprobanteDTO(
			TipoComprobante tipoComprobante) {
		TipoComprobanteDTO tcDTO = new TipoComprobanteDTO();
		BeanUtils.copyProperties(tipoComprobante, tcDTO);
		return tcDTO;
	}

	// SOLO PARA ID Y DESCRIPCION (no para columnas asociadas en el caso que lo
	// tenga)
	public static TipoComprobante fromTipoComprobanteDTO(
			TipoComprobanteDTO tcDTO) {
		TipoComprobante tc = new TipoComprobante();
		BeanUtils.copyProperties(tcDTO, tc);
		return tc;
	}

}