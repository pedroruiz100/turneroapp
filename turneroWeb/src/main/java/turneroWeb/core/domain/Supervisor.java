package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.SupervisorDTO;

@Entity
@Table(name = "supervisor", schema = "seguridad")
@NamedQuery(name = "Supervisor.findAll", query = "SELECT s FROM Supervisor s")
public class Supervisor implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_supervisor")
	private Long idSupervisor;

	@Column(name = "activo")
	private Boolean activo;

	@Column(name = "cod_supervisor")
	private String codSupervisor;

	@Column(name = "nro_supervisor")
	private Integer nroSupervisor;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario")
	private Usuario usuario;

	// bi-directional many-to-one association to ArqueoCaja
	// @OneToMany(mappedBy = "supervisor", fetch = FetchType.LAZY)
	// private List<ArqueoCaja> arqueoCaja;

	public Supervisor() {
	}

	public Long getIdSupervisor() {
		return this.idSupervisor;
	}

	public void setIdSupervisor(Long idSupervisor) {
		this.idSupervisor = idSupervisor;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getCodSupervisor() {
		return this.codSupervisor;
	}

	public void setCodSupervisor(String codSupervisor) {
		this.codSupervisor = codSupervisor;
	}

	public Integer getNroSupervisor() {
		return this.nroSupervisor;
	}

	public void setNroSupervisor(Integer nroSupervisor) {
		this.nroSupervisor = nroSupervisor;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	// public List<ArqueoCaja> getArqueoCaja() {
	// return arqueoCaja;
	// }
	//
	// public void setArqueoCaja(List<ArqueoCaja> arqueoCaja) {
	// this.arqueoCaja = arqueoCaja;
	// }

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public SupervisorDTO toSupervisorDTO() {
		SupervisorDTO supervisorDTO = this.toSupervisorDTO(this);
		if (this.getUsuario() != null) {
			Usuario usuario = this.getUsuario();
			// usuario.setAperturaCajasCajero(null);
			// usuario.setAperturaCajasSupervisor(null);
			// usuario.setCancelacionFacturasCajero(null);
			// usuario.setCancelacionFacturasSupervisor(null);
			// usuario.setCancelacionProductosCajero(null);
			// usuario.setCancelacionProductosSupervisor(null);
			// usuario.setCierreCajasCajero(null);
			// usuario.setCierreCajasSupervisor(null);
			// usuario.setRetiroDinerosCajero(null);
			// usuario.setRetiroDinerosSupervisor(null);
			// supervisorDTO.setArqueoCaja(null);
			supervisorDTO.setUsuario(usuario.toUsuarioFuncionarioDTO());
		}
		return supervisorDTO;
	}

	public SupervisorDTO toSupervisorRolFuncionDTO() {
		SupervisorDTO supervisorDTO = this.toSupervisorDTO(this);
		if (this.getUsuario() != null) {
			Usuario usuario = this.getUsuario();
			supervisorDTO.setUsuario(usuario.toUsuarioDTO());
		}
		// supervisorDTO.setArqueoCaja(null);
		return supervisorDTO;
	}

	public static Supervisor fromSupervisorDTO(SupervisorDTO supervisorDTO) {
		Supervisor supervisor = new Supervisor();
		BeanUtils.copyProperties(supervisorDTO, supervisor);
		return supervisor;
	}

	// PARA REALIZARLO CON USUARIO
	public SupervisorDTO toSupervisorDTO(Supervisor supervisor) {
		SupervisorDTO supervisorDTO = new SupervisorDTO();
		BeanUtils.copyProperties(supervisor, supervisorDTO);
		return supervisorDTO;
	}

	public static Supervisor fromSupervisorAsociadoDTO(SupervisorDTO supDTO) {
		Supervisor s = fromSupervisorDTO(supDTO);
		s.setUsuario(Usuario.fromUsuarioSupervisorDTO(supDTO.getUsuario()));
		return s;
	}
}
