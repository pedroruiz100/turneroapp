package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.EmpresaDTO;

/**
 * The persistent class for the empresa database table.
 * 
 */
@Entity
@Table(name = "empresa", schema = "general")
@NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
public class Empresa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_empresa")
	private Long idEmpresa;

	@Column(name = "descripcion_empresa")
	private String descripcionEmpresa;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	@Column(name = "ruc")
	private String ruc;

	@Column(name = "email")
	private String email;

	@Column(name = "telefono")
	private String telefono;

	// // bi-directional many-to-one association to Sucursal
	// @OneToMany(mappedBy = "empresa", fetch = FetchType.LAZY)
	// private List<Sucursal> sucursals;

	public Empresa() {
	}

	public Long getIdEmpresa() {
		return this.idEmpresa;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public void setIdEmpresa(Long idEmpresa) {
		this.idEmpresa = idEmpresa;
	}

	public String getDescripcionEmpresa() {
		return this.descripcionEmpresa;
	}

	public void setDescripcionEmpresa(String descripcionEmpresa) {
		this.descripcionEmpresa = descripcionEmpresa;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// public List<Sucursal> getSucursals() {
	// return this.sucursals;
	// }
	//
	// public void setSucursals(List<Sucursal> sucursals) {
	// this.sucursals = sucursals;
	// }

	// public Sucursal addSucursal(Sucursal sucursal) {
	// getSucursals().add(sucursal);
	// sucursal.setEmpresa(this);
	//
	// return sucursal;
	// }
	//
	// public Sucursal removeSucursal(Sucursal sucursal) {
	// getSucursals().remove(sucursal);
	// sucursal.setEmpresa(null);
	//
	// return sucursal;
	// }

	// ***************************************************************************

	public EmpresaDTO toEmpresaDTO() {
		EmpresaDTO empresaDTO = toEmpresaDTO(this);
		// empresaDTO.setSucursals(null);
		return empresaDTO;
	}

	public EmpresaDTO toEmpresaCompleDTO() {
		EmpresaDTO empreDTO = toEmpresaDTO(this);
		// if (!this.sucursals.isEmpty()) {
		// List<SucursalDTO> sucuDTO = new ArrayList<SucursalDTO>();
		// List<Sucursal> sucu = new ArrayList<Sucursal>();
		// for (Sucursal s : sucu) {
		// sucuDTO.add(s.toSucursalDescriDTO());
		// }
		// empreDTO.setSucursals(sucuDTO);
		// }
		return empreDTO;
	}

	public static EmpresaDTO toEmpresaDTO(Empresa em) {
		EmpresaDTO emDTO = new EmpresaDTO();
		BeanUtils.copyProperties(em, emDTO);
		return emDTO;
	}

	public static Empresa fromEmpresaDTO(EmpresaDTO emDTO) {
		Empresa em = new Empresa();
		BeanUtils.copyProperties(emDTO, em);
		return em;
	}
}