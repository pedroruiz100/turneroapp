package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.ClienteDTO;
import turneroWeb.core.dto.DepartamentoDTO;

/**
 * The persistent class for the cliente database table.
 * 
 */
@Entity
@Table(name = "cliente", schema = "cuenta")
@NamedQuery(name = "Cliente.findAll", query = "SELECT c FROM Cliente c")
public class Cliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_cliente")
	private Long idCliente;

	@Column(name = "calle_principal")
	private String callePrincipal;

	@Column(name = "cod_cliente", unique = true)
	private Integer codCliente;

	@Column(name = "compra_ini_fecha")
	private Timestamp compraIniFecha;

	@Column(name = "compra_ult_fecha")
	private Timestamp compraUltFecha;

	@Column(name = "email")
	private String email;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	// bi-directional many-to-one association to Barrio
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_barrio")
	private Barrio barrio;

	// bi-directional many-to-one association to Ciudad
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_ciudad")
	private Ciudad ciudad;

	// bi-directional many-to-one association to Departamento
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_departamento")
	private Departamento departamento;

	// bi-directional many-to-one association to Pais
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_pais")
	private Pais pais;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "nro_local")
	private Integer nroLocal;

	@Column(name = "primera_lateral")
	private String primeraLateral;

	@Column(name = "ruc")
	private String ruc;

	@Column(name = "segunda_lateral")
	private String segundaLateral;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "telefono2")
	private String telefono2;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	// bi-directional many-to-one association to TarjetaClienteFiel
	// @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	// private List<TarjetaClienteFiel> tarjetaClienteFiels;
	//
	// // bi-directional many-to-one association to FacturaClienteCab
	// @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	// private List<FacturaClienteCab> facturaClienteCabs;
	//
	// // bi-directional many-to-one association to NotaCreditoCab
	// @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	// private List<NotaCreditoCab> notaCreditoCabs;
	//
	// // bi-directional many-to-one association to NotaCreditoCab
	// @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	// private List<NotaRemisionCab> notaRemisionCabs;
	//
	// // bi-directional many-to-one association to ClientePendiente
	// @OneToMany(mappedBy = "cliente", fetch = FetchType.LAZY)
	// private List<ClientePendiente> clientePendiente;

	@Column(name = "apellido")
	private String apellido;

	@Column(name = "fec_nac")
	private Date fecNac;

	public Cliente() {
	}

	public Long getIdCliente() {
		return this.idCliente;
	}

	public Date getFecNac() {
		return fecNac;
	}

	public void setFecNac(Date fecNac) {
		this.fecNac = fecNac;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public String getCallePrincipal() {
		return this.callePrincipal;
	}

	public void setCallePrincipal(String callePrincipal) {
		this.callePrincipal = callePrincipal;
	}

	// public List<ClientePendiente> getClientePendiente() {
	// return clientePendiente;
	// }
	//
	// public void setClientePendiente(List<ClientePendiente> clientePendiente)
	// {
	// this.clientePendiente = clientePendiente;
	// }

	public Integer getCodCliente() {
		return this.codCliente;
	}

	public void setCodCliente(Integer codCliente) {
		this.codCliente = codCliente;
	}

	public Timestamp getCompraIniFecha() {
		return this.compraIniFecha;
	}

	public void setCompraIniFecha(Timestamp compraIniFecha) {
		this.compraIniFecha = compraIniFecha;
	}

	public Timestamp getCompraUltFecha() {
		return this.compraUltFecha;
	}

	public void setCompraUltFecha(Timestamp compraUltFecha) {
		this.compraUltFecha = compraUltFecha;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getNroLocal() {
		return this.nroLocal;
	}

	public void setNroLocal(Integer nroLocal) {
		this.nroLocal = nroLocal;
	}

	public String getPrimeraLateral() {
		return this.primeraLateral;
	}

	public void setPrimeraLateral(String primeraLateral) {
		this.primeraLateral = primeraLateral;
	}

	public String getRuc() {
		return this.ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getSegundaLateral() {
		return this.segundaLateral;
	}

	public void setSegundaLateral(String segundaLateral) {
		this.segundaLateral = segundaLateral;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono2() {
		return this.telefono2;
	}

	public void setTelefono2(String telefono2) {
		this.telefono2 = telefono2;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// public List<TarjetaClienteFiel> getTarjetaClienteFiels() {
	// return this.tarjetaClienteFiels;
	// }
	//
	// public void setTarjetaClienteFiels(
	// List<TarjetaClienteFiel> tarjetaClienteFiels) {
	// this.tarjetaClienteFiels = tarjetaClienteFiels;
	// }
	//
	// public TarjetaClienteFiel addTarjetaClienteFiel(
	// TarjetaClienteFiel tarjetaClienteFiel) {
	// getTarjetaClienteFiels().add(tarjetaClienteFiel);
	// tarjetaClienteFiel.setCliente(this);
	//
	// return tarjetaClienteFiel;
	// }
	//
	// public TarjetaClienteFiel removeTarjetaClienteFiel(
	// TarjetaClienteFiel tarjetaClienteFiel) {
	// getTarjetaClienteFiels().remove(tarjetaClienteFiel);
	// tarjetaClienteFiel.setCliente(null);
	//
	// return tarjetaClienteFiel;
	// }

	public Barrio getBarrio() {
		return barrio;
	}

	public void setBarrio(Barrio barrio) {
		this.barrio = barrio;
	}

	public Ciudad getCiudad() {
		return ciudad;
	}

	public void setCiudad(Ciudad ciudad) {
		this.ciudad = ciudad;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	// ********************************************************************************

	// public List<FacturaClienteCab> getFacturaClienteCabs() {
	// return facturaClienteCabs;
	// }
	//
	// public void setFacturaClienteCabs(List<FacturaClienteCab>
	// facturaClienteCabs) {
	// this.facturaClienteCabs = facturaClienteCabs;
	// }

	// public FacturaClienteCab addFacturaClienteCab(
	// FacturaClienteCab facturaClienteCab) {
	// getFacturaClienteCabs().add(facturaClienteCab);
	// facturaClienteCab.setCliente(this);
	// return facturaClienteCab;
	// }
	//
	// public FacturaClienteCab removeFacturaClienteCab(
	// FacturaClienteCab facturaClienteCab) {
	// getFacturaClienteCabs().remove(facturaClienteCab);
	// facturaClienteCab.setCliente(null);
	// return facturaClienteCab;
	// }

	// ********************************************************************************

	// public List<NotaCreditoCab> getNotaCreditoCabs() {
	// return notaCreditoCabs;
	// }
	//
	// public void setNotaCreditoCabs(List<NotaCreditoCab> notaCreditoCabs) {
	// this.notaCreditoCabs = notaCreditoCabs;
	// }

	// public NotaCreditoCab addNotaCreditoCab(NotaCreditoCab notaCreditoCab) {
	// getNotaCreditoCabs().add(notaCreditoCab);
	// notaCreditoCab.setCliente(this);
	// return notaCreditoCab;
	// }
	//
	// public NotaCreditoCab removeNotaCreditoCab(NotaCreditoCab notaCreditoCab)
	// {
	// getNotaCreditoCabs().remove(notaCreditoCab);
	// notaCreditoCab.setCliente(null);
	// return notaCreditoCab;
	// }

	// ********************************************************************************

	// public List<NotaRemisionCab> getNotaRemisionCabs() {
	// return notaRemisionCabs;
	// }
	//
	// public void setNotaRemisionCabs(List<NotaRemisionCab> notaRemisionCabs) {
	// this.notaRemisionCabs = notaRemisionCabs;
	// }

	// public NotaRemisionCab addNotaRemisionCab(NotaRemisionCab
	// notaRemisionCab) {
	// getNotaRemisionCabs().add(notaRemisionCab);
	// notaRemisionCab.setCliente(this);
	// return notaRemisionCab;
	// }
	//
	// public NotaRemisionCab removeNotaRemisionCab(NotaRemisionCab
	// notaRemisionCab) {
	// getNotaRemisionCabs().remove(notaRemisionCab);
	// notaRemisionCab.setCliente(null);
	// return notaRemisionCab;
	// }

	// ********************************************************************************

	// LISTA DE CLIENTES CON BARRIO DEPARTAMENTO CIUDAD Y PAIS
	public ClienteDTO toClienteDTO() {
		ClienteDTO cDTO = toClienteDTO(this);
		if (this.getBarrio() != null) {
			Barrio barrio = this.getBarrio();
			cDTO.setBarrio(barrio.toBarrioAClienteDTO());
		}
		if (this.getCiudad() != null) {
			Ciudad ciudad = this.getCiudad();
			// ciudad.setBarrios(null);
			// ciudad.setClientes(null);
			// ciudad.setFamiliaTarjs(null);
			// ciudad.setProveedors(null);
			// ciudad.setSucursals(null);
			cDTO.setCiudad(ciudad.toCiudadDTO());
		}
		// Si coloco este if tambien agrega el Pais pero sera redundante ya
		// que departamento contiene a Pais
		// if (this.getPais() != null) {
		// Pais pais = this.getPais();
		// pais.setClientes(null);
		// pais.setDepartamentos(null);
		// pais.setProveedors(null);
		// cDTO.setPais(pais.toPaisDTO(pais));
		// }
		if (this.getDepartamento() != null) {
			Departamento dep = this.getDepartamento();
			// dep.setClientes(null);
			// dep.setProveedors(null);
			dep.setPais(this.getPais());
			DepartamentoDTO depDTO = dep.toDepartamentoDTO();
			cDTO.setDepartamento(depDTO);
		}
		// cDTO.setFacturaClienteCabs(null);
		// cDTO.setNotaCreditoCabs(null);
		// cDTO.setNotaRemisionCabs(null);
		// cDTO.setTarjetaClienteFiels(null);
		// cDTO.setClientePendiente(null);
		return cDTO;
	}

	// SOLO LISTA DE CLIENTES SIN DEPARTAMENTO CIUDAD PAIS BARRIO PARA
	// BUSQUEDA EN CAJA
	public ClienteDTO toClienteCajaDTO() {
		ClienteDTO cDTO = toClienteDTO(this);
		cDTO.setBarrio(null);
		cDTO.setCiudad(null);
		cDTO.setPais(null);
		cDTO.setDepartamento(null);
		// cDTO.setFacturaClienteCabs(null);
		// cDTO.setNotaCreditoCabs(null);
		// cDTO.setNotaRemisionCabs(null);
		// cDTO.setClientePendiente(null);
		// cDTO.setTarjetaClienteFiels(null);
		return cDTO;
	}

	public static ClienteDTO toClienteDTO(Cliente cliente) {
		ClienteDTO cDTO = new ClienteDTO();
		BeanUtils.copyProperties(cliente, cDTO);
		return cDTO;
	}

	// Para el uso exclusivo del m�dulo CAJA, ATC y demas
	public static Cliente fromClienteParaCajaDTO(ClienteDTO cDTO) {
		Cliente c = new Cliente();
		BeanUtils.copyProperties(cDTO, c);

		// Al insertar entraria los datos por el if ya que no se ingresan
		// barrio, ciudad,
		// Pais en el modulo caja, en caso que se realice una modificacion
		// entraria por
		// el else ya que en caso que tenga datos cargados deberia copiar a la
		// Entidad

		if (cDTO.getBarrio() == null) {
			Barrio bar = new Barrio();
			bar.setIdBarrio(0L);
			c.setBarrio(bar);
		} else {
			c.setBarrio(Barrio.fromBarrioDTO(cDTO.getBarrio()));
		}

		if (cDTO.getCiudad() == null) {
			Ciudad ciu = new Ciudad();
			ciu.setIdCiudad(0L);
			c.setCiudad(ciu);
		} else {
			c.setCiudad(Ciudad.fromCiudadDTO(cDTO.getCiudad()));
		}

		if (cDTO.getDepartamento() == null) {
			Departamento dep = new Departamento();
			dep.setIdDepartamento(0L);
			c.setDepartamento(dep);
		} else {
			c.setDepartamento(Departamento.fromDepartamentoDTO(cDTO
					.getDepartamento()));
		}

		if (cDTO.getPais() == null) {
			Pais pais = new Pais();
			pais.setIdPais(0L);
			c.setPais(pais);
		} else {
			c.setPais(Pais.fromPaisDTO(cDTO.getPais()));
		}

		if (cDTO.getCodCliente() == null) {
			c.setCodCliente(0);
		}
		return c;
	}

	// Para cliente como figura en la BD

	public ClienteDTO toClienteBDDTO() {
		ClienteDTO clienteDTO = toClienteDTO(this);
		clienteDTO.setBarrio(null);
		clienteDTO.setCiudad(null);
		clienteDTO.setDepartamento(null);
		// clienteDTO.setFacturaClienteCabs(null);
		// clienteDTO.setNotaCreditoCabs(null);
		// clienteDTO.setNotaRemisionCabs(null);
		clienteDTO.setPais(null);
		// clienteDTO.setTarjetaClienteFiels(null);
		// clienteDTO.setClientePendiente(null);
		return clienteDTO;
	}

	// SOLO PARA ID Y DESCRIPCION (no para columnas asociadas)
	public static Cliente fromClienteDTO(ClienteDTO clienteDTO) {
		Cliente cliente = new Cliente();
		BeanUtils.copyProperties(clienteDTO, cliente);
		return cliente;
	}

	public static Cliente fromClienteDTOAsociado(ClienteDTO cDTO) {
		Cliente c = fromClienteDTO(cDTO);
		c.setBarrio(Barrio.fromBarrioDTO(cDTO.getBarrio()));
		c.setCiudad(Ciudad.fromCiudadDTO(cDTO.getCiudad()));
		c.setDepartamento(Departamento.fromDepartamentoDTO(cDTO
				.getDepartamento()));
		c.setPais(Pais.fromPaisDTO(cDTO.getPais()));
		return c;
	}

	// FIN DE DATOS COMO FIGURAN EN LA BD
}