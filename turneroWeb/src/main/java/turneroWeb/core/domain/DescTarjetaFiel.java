package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the desc_tarjeta_fiel database table.
 * 
 */
@Entity
@Table(name = "desc_tarjeta_fiel", schema = "factura_cliente")
@NamedQuery(name="DescTarjetaFiel.findAll", query="SELECT d FROM DescTarjetaFiel d")
public class DescTarjetaFiel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_tarjeta_fiel")
	private Long idDescTarjetaFiel;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabTarjFiel
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_tarj_fiel")
	private FacturaClienteCabTarjFiel facturaClienteCabTarjFiel;

	//bi-directional many-to-one association to FacturaClienteCabTarjFiel
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_tarj_fiel")
//	private FacturaClienteCabTarjFiel facturaClienteCabTarjFiel2;

	public DescTarjetaFiel() {
	}

	public Long getIdDescTarjetaFiel() {
		return this.idDescTarjetaFiel;
	}

	public void setIdDescTarjetaFiel(Long idDescTarjetaFiel) {
		this.idDescTarjetaFiel = idDescTarjetaFiel;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabTarjFiel getFacturaClienteCabTarjFiel() {
		return this.facturaClienteCabTarjFiel;
	}

	public void setFacturaClienteCabTarjFiel(FacturaClienteCabTarjFiel facturaClienteCabTarjFiel) {
		this.facturaClienteCabTarjFiel = facturaClienteCabTarjFiel;
	}

	// public FacturaClienteCabTarjFiel getFacturaClienteCabTarjFiel2() {
	// return this.facturaClienteCabTarjFiel2;
	// }
	//
	// public void setFacturaClienteCabTarjFiel2(FacturaClienteCabTarjFiel
	// facturaClienteCabTarjFiel2) {
	// this.facturaClienteCabTarjFiel2 = facturaClienteCabTarjFiel2;
	// }

}