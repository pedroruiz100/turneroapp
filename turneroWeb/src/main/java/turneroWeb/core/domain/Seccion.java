package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.SeccionDTO;

/**
 * The persistent class for the Seccion database table.
 * 
 */
@Entity
@Table(name = "Seccion", schema = "stock")
@NamedQuery(name = "Seccion.findAll", query = "SELECT c FROM Seccion c")
public class Seccion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_seccion")
	private Long idSeccion;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	@Column(name = "estado")
	private Boolean estado;

	public Seccion() {
	}

	public Long getIdSeccion() {
		return idSeccion;
	}

	public void setIdSeccion(Long idSeccion) {
		this.idSeccion = idSeccion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// ***************************************************************************
	// Lista de Secciones por nombre e id.
	public SeccionDTO toSeccionDTO() {
		SeccionDTO secDTO = toSeccionDTO(this);
		secDTO.setFechaAlta(null);
		secDTO.setFechaMod(null);
		secDTO.setUsuAlta(null);
		secDTO.setUsuMod(null);
		return secDTO;
	}

	public static SeccionDTO toSeccionDTO(Seccion sec) {
		SeccionDTO secDTO = new SeccionDTO();
		BeanUtils.copyProperties(sec, secDTO);
		return secDTO;
	}

	public static Seccion fromSeccionDTO(SeccionDTO secDTO) {
		Seccion sec = new Seccion();
		BeanUtils.copyProperties(secDTO, sec);
		return sec;
	}
}