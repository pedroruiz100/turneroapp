package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.ClientePendienteDTO;
import turneroWeb.core.dto.FacturaCabClientePendienteDTO;

/**
 * The persistent class for the factura_cab_cliente_pendiente database table.
 * 
 */
@Entity
@Table(name = "factura_cab_cliente_pendiente", schema = "factura_cliente")
@NamedQuery(name = "FacturaCabClientePendiente.findAll", query = "SELECT f FROM FacturaCabClientePendiente f")
public class FacturaCabClientePendiente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_fact_pendiente")
	private Long idFactPendiente;

	// bi-directional many-to-one association to ClientePendiente
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cliente_pendiente")
	private ClientePendiente clientePendiente;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaCabClientePendiente() {
	}

	public Long getIdFactPendiente() {
		return this.idFactPendiente;
	}

	public void setIdFactPendiente(Long idFactPendiente) {
		this.idFactPendiente = idFactPendiente;
	}

	public ClientePendiente getClientePendiente() {
		return clientePendiente;
	}

	public void setClientePendiente(ClientePendiente clientePendiente) {
		this.clientePendiente = clientePendiente;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	public FacturaCabClientePendienteDTO toBDFacturaCabClientePendienteDTO() {
		FacturaCabClientePendienteDTO funDTO = toFacturaCabClientePendienteDTO(this);
		if (funDTO.getClientePendiente() == null) {
			ClientePendienteDTO pais = new ClientePendienteDTO();
			pais.setIdClientePendiente(0L);
			funDTO.setClientePendiente(pais);
		} else {
			funDTO.setClientePendiente(funDTO.getClientePendiente());
		}
		if (funDTO.getFacturaClienteCab() != null) {
			funDTO.setFacturaClienteCab(funDTO.getFacturaClienteCab());
		}

		return funDTO;
	}

	public static FacturaCabClientePendienteDTO toFacturaCabClientePendienteDTO(
			FacturaCabClientePendiente ServPendiente) {
		FacturaCabClientePendienteDTO funDTO = new FacturaCabClientePendienteDTO();
		BeanUtils.copyProperties(ServPendiente, funDTO);
		return funDTO;
	}

	public static FacturaCabClientePendiente fromFacturaCabClientePendienteDTO(
			FacturaCabClientePendienteDTO funDTO) {
		FacturaCabClientePendiente fun = new FacturaCabClientePendiente();
		BeanUtils.copyProperties(funDTO, fun);
		fun.setClientePendiente(ClientePendiente.fromClientePendienteDTO(funDTO
				.getClientePendiente()));
		fun.setFacturaClienteCab(FacturaClienteCab
				.fromFacturaClienteCabDTO(funDTO.getFacturaClienteCab()));
		return fun;
	}

}