package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.EstadoFacturaDTO;

/**
 * The persistent class for the estado_factura database table.
 * 
 */
@Entity
@Table(name = "estado_factura")
@NamedQuery(name = "EstadoFactura.findAll", query = "SELECT e FROM EstadoFactura e")
public class EstadoFactura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_estado_factura")
	private Long idEstadoFactura;

	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	// bi-directional many-to-one association to FacturaClienteCab
	// @OneToMany(mappedBy="estadoFactura1")
	// private List<FacturaClienteCab> facturaClienteCabs1;

	// bi-directional many-to-one association to FacturaClienteCab
	// @OneToMany(mappedBy="estadoFactura2")
	// private List<FacturaClienteCab> facturaClienteCabs2;

	public EstadoFactura() {
	}

	public Long getIdEstadoFactura() {
		return this.idEstadoFactura;
	}

	public void setIdEstadoFactura(Long idEstadoFactura) {
		this.idEstadoFactura = idEstadoFactura;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	// public List<FacturaClienteCab> getFacturaClienteCabs1() {
	// return this.facturaClienteCabs1;
	// }

	// public void setFacturaClienteCabs1(List<FacturaClienteCab>
	// facturaClienteCabs1) {
	// this.facturaClienteCabs1 = facturaClienteCabs1;
	// }
	//
	// public FacturaClienteCab addFacturaClienteCabs1(FacturaClienteCab
	// facturaClienteCabs1) {
	// getFacturaClienteCabs1().add(facturaClienteCabs1);
	// facturaClienteCabs1.setEstadoFactura1(this);
	//
	// return facturaClienteCabs1;
	// }

	// public FacturaClienteCab removeFacturaClienteCabs1(FacturaClienteCab
	// facturaClienteCabs1) {
	// getFacturaClienteCabs1().remove(facturaClienteCabs1);
	// facturaClienteCabs1.setEstadoFactura1(null);
	//
	// return facturaClienteCabs1;
	// }
	//
	// public List<FacturaClienteCab> getFacturaClienteCabs2() {
	// return this.facturaClienteCabs2;
	// }
	//
	// public void setFacturaClienteCabs2(List<FacturaClienteCab>
	// facturaClienteCabs2) {
	// this.facturaClienteCabs2 = facturaClienteCabs2;
	// }
	//
	// public FacturaClienteCab addFacturaClienteCabs2(FacturaClienteCab
	// facturaClienteCabs2) {
	// getFacturaClienteCabs2().add(facturaClienteCabs2);
	// facturaClienteCabs2.setEstadoFactura2(this);
	//
	// return facturaClienteCabs2;
	// }
	//
	// public FacturaClienteCab removeFacturaClienteCabs2(FacturaClienteCab
	// facturaClienteCabs2) {
	// getFacturaClienteCabs2().remove(facturaClienteCabs2);
	// facturaClienteCabs2.setEstadoFactura2(null);
	//
	// return facturaClienteCabs2;
	// }

	public EstadoFacturaDTO toEstadoFacturaDTO() {
		EstadoFacturaDTO eDTO = toEstadoFacturaDTO(this);
		// eDTO.setFacturaClienteCabs(null);
		return eDTO;
	}

	public static EstadoFacturaDTO toEstadoFacturaDTO(
			EstadoFactura estadoFactura) {
		EstadoFacturaDTO eDTO = new EstadoFacturaDTO();
		BeanUtils.copyProperties(estadoFactura, eDTO);
		return eDTO;
	}

	// SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
	// tenga)
	public static EstadoFactura fromEstadoFacturaDTO(
			EstadoFacturaDTO estadoFacturaDTO) {
		EstadoFactura e = new EstadoFactura();
		BeanUtils.copyProperties(estadoFacturaDTO, e);
		return e;
	}

}