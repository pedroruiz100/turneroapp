package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.FacturaClienteDetDTO;

/**
 * The persistent class for the factura_cliente_det database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_det", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteDet.findAll", query = "SELECT f FROM FacturaClienteDet f")
public class FacturaClienteDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_det")
	private Long idFacturaClienteDet;

	private Boolean bajada;

	private BigDecimal cantidad;

	@Column(name = "cod_articulo")
	private Long codArticulo;

	@Column(name = "cod_vendedor")
	private String codVendedor;

	private String descripcion;

	// @Column(name = "id_articulo")
	// private Long idArticulo;

	private Integer orden;

	@Column(name = "permite_desc")
	private Boolean permiteDesc;

	private Integer poriva;

	private Long precio;

	private String seccion;

	@Column(name = "seccion_sub")
	private String seccionSub;

	// bi-directional many-to-one association to DescCombo
	// @OneToMany(mappedBy="facturaClienteDet")
	// private List<DescCombo> descCombos;

	// bi-directional many-to-one association to DescCombo
	// @OneToMany(mappedBy="facturaClienteDet2")
	// private List<DescCombo> descCombos2;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_articulo")
	private Articulo articulo;

	// bi-directional many-to-one association to FacturaClienteCab
	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name="id_factura_cliente_cab")
	// private FacturaClienteCab facturaClienteCab2;

	// bi-directional many-to-one association to FacturaClienteDetDtoMay
	@OneToMany(mappedBy = "facturaClienteDet")
	private List<FacturaClienteDetDtoMay> facturaClienteDetDtoMays;

	public FacturaClienteDet() {
	}

	public Long getIdFacturaClienteDet() {
		return this.idFacturaClienteDet;
	}

	public void setIdFacturaClienteDet(Long idFacturaClienteDet) {
		this.idFacturaClienteDet = idFacturaClienteDet;
	}

	public Boolean getBajada() {
		return this.bajada;
	}

	public void setBajada(Boolean bajada) {
		this.bajada = bajada;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCodArticulo() {
		return this.codArticulo;
	}

	public void setCodArticulo(Long codArticulo) {
		this.codArticulo = codArticulo;
	}

	public String getCodVendedor() {
		return this.codVendedor;
	}

	public void setCodVendedor(String codVendedor) {
		this.codVendedor = codVendedor;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Articulo getArticulo() {
		return articulo;
	}

	public void setArticulo(Articulo articulo) {
		this.articulo = articulo;
	}

	public Integer getOrden() {
		return this.orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Boolean getPermiteDesc() {
		return this.permiteDesc;
	}

	public void setPermiteDesc(Boolean permiteDesc) {
		this.permiteDesc = permiteDesc;
	}

	public Integer getPoriva() {
		return this.poriva;
	}

	public void setPoriva(Integer poriva) {
		this.poriva = poriva;
	}

	public Long getPrecio() {
		return this.precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public String getSeccion() {
		return this.seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getSeccionSub() {
		return this.seccionSub;
	}

	public void setSeccionSub(String seccionSub) {
		this.seccionSub = seccionSub;
	}

	// public List<DescCombo> getDescCombos() {
	// return this.descCombos;
	// }
	//
	// public void setDescCombos(List<DescCombo> descCombos) {
	// this.descCombos = descCombos;
	// }

	// public DescCombo addDescCombos1(DescCombo descCombos1) {
	// getDescCombos1().add(descCombos1);
	// descCombos1.setFacturaClienteDet1(this);
	//
	// return descCombos1;
	// }
	//
	// public DescCombo removeDescCombos1(DescCombo descCombos1) {
	// getDescCombos1().remove(descCombos1);
	// descCombos1.setFacturaClienteDet1(null);
	//
	// return descCombos1;
	// }

	// public List<DescCombo> getDescCombos() {
	// return this.descCombos;
	// }
	//
	// public void setDescCombos(List<DescCombo> descCombos) {
	// this.descCombos = descCombos;
	// }

	// public DescCombo addDescCombos2(DescCombo descCombos2) {
	// getDescCombos2().add(descCombos2);
	// descCombos2.setFacturaClienteDet2(this);
	//
	// return descCombos2;
	// }
	//
	// public DescCombo removeDescCombos2(DescCombo descCombos2) {
	// getDescCombos2().remove(descCombos2);
	// descCombos2.setFacturaClienteDet2(null);
	//
	// return descCombos2;
	// }

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

	public List<FacturaClienteDetDtoMay> getFacturaClienteDetDtoMays() {
		return this.facturaClienteDetDtoMays;
	}

	public void setFacturaClienteDetDtoMays(
			List<FacturaClienteDetDtoMay> facturaClienteDetDtoMays) {
		this.facturaClienteDetDtoMays = facturaClienteDetDtoMays;
	}

	public FacturaClienteDetDtoMay addFacturaClienteDetDtoMay(
			FacturaClienteDetDtoMay facturaClienteDetDtoMay) {
		getFacturaClienteDetDtoMays().add(facturaClienteDetDtoMay);
		facturaClienteDetDtoMay.setFacturaClienteDet(this);

		return facturaClienteDetDtoMay;
	}

	public FacturaClienteDetDtoMay removeFacturaClienteDetDtoMay(
			FacturaClienteDetDtoMay facturaClienteDetDtoMay) {
		getFacturaClienteDetDtoMays().remove(facturaClienteDetDtoMay);
		facturaClienteDetDtoMay.setFacturaClienteDet(null);

		return facturaClienteDetDtoMay;
	}

	public FacturaClienteDetDTO toFacturaClienteDetDTO() {
		FacturaClienteDetDTO facDTO = toFacturaClienteDetDTO(this);

		if (this.facturaClienteCab != null) {
			facDTO.setFacturaClienteCab(this.getFacturaClienteCab()
					.toBDHIstoricoCajaDTO());
		}
//		if (this.articulo != null) {
//			facDTO.setArticulo(this.getArticulo().toBDArticuloDTO());
//		}
		// facDTO.setDescCombos(null);
		return facDTO;
	}

	public FacturaClienteDetDTO toDescriFacturaClienteDetDTO() {
		FacturaClienteDetDTO facDTO = toFacturaClienteDetDTO(this);
		facDTO.setFacturaClienteCab(null);
		facDTO.setArticulo(null);
		// facDTO.setDescCombos(null);
		return facDTO;
	}

	public FacturaClienteDetDTO toArtFacturaClienteDetDTO() {
		FacturaClienteDetDTO facDTO = toFacturaClienteDetDTO(this);
		facDTO.setFacturaClienteCab(null);
		if (this.articulo != null) {
//			facDTO.setArticulo(this.getArticulo().toPromoArticuloDTO());
		}
		// facDTO.setDescCombos(null);
		return facDTO;
	}

	public static FacturaClienteDetDTO toFacturaClienteDetDTO(
			FacturaClienteDet facturaClienteDet) {
		FacturaClienteDetDTO facDTO = new FacturaClienteDetDTO();
		BeanUtils.copyProperties(facturaClienteDet, facDTO);
		return facDTO;
	}

	public static FacturaClienteDet fromFacturaClienteDetDTO(
			FacturaClienteDetDTO facDTO) {
		FacturaClienteDet fac = new FacturaClienteDet();
		BeanUtils.copyProperties(facDTO, fac);
		return fac;
	}

	public static FacturaClienteDet fromFacturaClienteDetAsociado(
			FacturaClienteDetDTO facDTO) {
		FacturaClienteDet fac = fromFacturaClienteDetDTO(facDTO);
		fac.setFacturaClienteCab(FacturaClienteCab
				.fromFacturaClienteCabDTO(facDTO.getFacturaClienteCab()));
		fac.setArticulo(Articulo.fromArticuloDTO(facDTO.getArticulo()));
		return fac;
	}

}