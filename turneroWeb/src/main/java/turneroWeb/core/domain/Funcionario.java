package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.FuncionarioDTO;

@Entity
@Table(name = "funcionario", schema = "rrhh")
@NamedQuery(name = "Funcionario.findAll", query = "SELECT r FROM Funcionario r")
public class Funcionario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_funcionario")
	private Long idFuncionario;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellido")
	private String apellido;

	@Column(name = "activo")
	private Boolean activo;

	@Column(name = "habilitado")
	private Boolean habilitado;

	@Column(name = "ci", unique = true)
	private String ci;

	public Funcionario() {
	}

	public Long getIdFuncionario() {
		return idFuncionario;
	}

	public void setIdFuncionario(Long idFuncionario) {
		this.idFuncionario = idFuncionario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Boolean getActivo() {
		return activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getHabilitado() {
		return habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getCi() {
		return ci;
	}

	public void setCi(String ci) {
		this.ci = ci;
	}
	
	public FuncionarioDTO toFuncionarioDTO() {
        FuncionarioDTO funDTO = toFuncionarioDTO(this);
//        funDTO.setUsuario(null);
        return funDTO;
    }

    public FuncionarioDTO toBDFuncionarioDTO() {
        FuncionarioDTO funDTO = toFuncionarioDTO(this);
        return funDTO;
    }

    public static FuncionarioDTO toFuncionarioDTO(Funcionario funcionario) {
        FuncionarioDTO funDTO = new FuncionarioDTO();
        BeanUtils.copyProperties(funcionario, funDTO);
        return funDTO;
    }

    public static Funcionario fromFuncionarioDTO(FuncionarioDTO funDTO) {
        Funcionario fun = new Funcionario();
        BeanUtils.copyProperties(funDTO, fun);
        return fun;
    }

}
