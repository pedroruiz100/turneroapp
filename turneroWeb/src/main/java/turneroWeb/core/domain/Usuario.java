package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.UsuarioDTO;

@Entity
@Table(name = "usuario", schema = "seguridad")
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario")
	private Long idUsuario;

	@Column(name = "activo", nullable = false)
	private Boolean activo;

	@Column(name = "contrasenha", nullable = false, length = 64)
	private String contrasenha;

	@Column(name = "email", unique = true, nullable = false, length = 100)
	private String email;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "nom_usuario")
	private String nomUsuario;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	@Column(name = "generico")
	private boolean generico;

	// bi-directional many-to-one association to Funcionario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_funcionario")
	private Funcionario funcionario;

	public Usuario() {
	}

	public Long getIdUsuario() {
		return this.idUsuario;
	}

	public void setIdUsuario(Long idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Funcionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(Funcionario funcionario) {
		this.funcionario = funcionario;
	}

	public boolean isGenerico() {
		return generico;
	}

	public void setGenerico(boolean generico) {
		this.generico = generico;
	}

	public String getContrasenha() {
		return this.contrasenha;
	}

	public void setContrasenha(String contrasenha) {
		this.contrasenha = contrasenha;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public String getNomUsuario() {
		return this.nomUsuario;
	}

	public void setNomUsuario(String nomUsuario) {
		this.nomUsuario = nomUsuario;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public UsuarioDTO toUsuarioDTO() {
		UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);

		// Verificar si exite datos recuperados de la BD para el campo
		// UsuarioRol

		if (this.getFuncionario() != null) {
			usuarioDTO.setFuncionario(this.getFuncionario().toFuncionarioDTO());
		}
		return usuarioDTO;
	}

	public UsuarioDTO toUsuarioFuncionarioDTO() {
		UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
		if (this.getFuncionario() != null) {
			usuarioDTO.setFuncionario(this.getFuncionario().toFuncionarioDTO());
		}
		return usuarioDTO;
	}

	public UsuarioDTO toUsuarioDTO(Usuario usuario) {
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		BeanUtils.copyProperties(usuario, usuarioDTO);
		return usuarioDTO;
	}

	public static Usuario fromUsuarioCajeroDTO(UsuarioDTO usuarioDTO) {
		Usuario usuarioCajero = new Usuario();
		BeanUtils.copyProperties(usuarioDTO, usuarioCajero);
		return usuarioCajero;
	}

	/* SUPERVISOR */
	public UsuarioDTO toUsuarioSupervisorDTO() {
		UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
		return usuarioDTO;
	}

	// SOLO PARA ID Y DESCRIPCION (no para columnas asociadas, en el caso que
	// tenga)
	public static Usuario fromUsuarioSupervisorDTO(UsuarioDTO usuarioDTO) {
		Usuario usuarioSup = new Usuario();
		BeanUtils.copyProperties(usuarioDTO, usuarioSup);
		return usuarioSup;
	}

	/* FIN SUPERVISOR */
	// CON FUNCIONARIO
	public static Usuario fromUsuarioSupervisorAsociadoDTO(UsuarioDTO usuDTO) {
		Usuario usu = fromUsuarioSupervisorDTO(usuDTO);
		usu.setFuncionario(Funcionario.fromFuncionarioDTO(usuDTO
				.getFuncionario()));
		return usu;
	}

	public UsuarioDTO toBDUsuarioDTO() {
		UsuarioDTO usuarioDTO = this.toUsuarioDTO(this);
		return usuarioDTO;
	}

}
