package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the factura_cliente_cab_cheque database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_cheque", schema="factura_cliente")
@NamedQuery(name="FacturaClienteCabCheque.findAll", query="SELECT f FROM FacturaClienteCabCheque f")
public class FacturaClienteCabCheque implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_factura_cliente_cab_cheque")
	private Long idFacturaClienteCabCheque;

	private String descripcion;

	@Column(name="monto_cheque")
	private Integer montoCheque;

	@Column(name="nro_cheque")
	private String nroCheque;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabCheque() {
	}

	public Long getIdFacturaClienteCabCheque() {
		return this.idFacturaClienteCabCheque;
	}

	public void setIdFacturaClienteCabCheque(Long idFacturaClienteCabCheque) {
		this.idFacturaClienteCabCheque = idFacturaClienteCabCheque;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Integer getMontoCheque() {
		return this.montoCheque;
	}

	public void setMontoCheque(Integer montoCheque) {
		this.montoCheque = montoCheque;
	}

	public String getNroCheque() {
		return this.nroCheque;
	}

	public void setNroCheque(String nroCheque) {
		this.nroCheque = nroCheque;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}