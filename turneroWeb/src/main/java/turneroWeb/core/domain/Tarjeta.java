package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.TarjetaDTO;

@Entity
@Table(name = "tarjeta", schema = "cuenta")
@NamedQuery(name = "Tarjeta.findAll", query = "SELECT t FROM Tarjeta t")
public class Tarjeta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_tarjeta")
	private Long idTarjeta;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "habilitado")
	private Boolean habilitado;

	@Column(name = "codtar")
	private Integer codtar;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	public Tarjeta() {
	}

	public Long getIdTarjeta() {
		return this.idTarjeta;
	}

	public void setIdTarjeta(Long idTarjeta) {
		this.idTarjeta = idTarjeta;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Boolean getHabilitado() {
		return this.habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Integer getCodtar() {
		return codtar;
	}

	public void setCodtar(Integer codtar) {
		this.codtar = codtar;
	}

	public TarjetaDTO toTarjetaDTO() {
		TarjetaDTO tarDTO = toTarjetaDTO(this);
		return tarDTO;
	}

	public TarjetaDTO toBDDescriTarjetaDTO() {
		TarjetaDTO tarDTO = toTarjetaDTO(this);
		return tarDTO;
	}

	// solo descripcion para utilizar en el combo descuento tarjeta debito
	public TarjetaDTO toTarjetaDescriDTO() {
		TarjetaDTO tarDTO = toTarjetaDTO(this);
		return tarDTO;
	}

	public TarjetaDTO toTarjetaDTO(Tarjeta tar) {
		TarjetaDTO tarDTO = new TarjetaDTO();
		BeanUtils.copyProperties(tar, tarDTO);
		return tarDTO;
	}

	// sin familia ni tipo de tarjeta solo para el uso de Descuento Tarjeta Cab
	public static Tarjeta fromTarjetaDTO(TarjetaDTO tarDTO) {
		Tarjeta tar = new Tarjeta();
		BeanUtils.copyProperties(tarDTO, tar);
		return tar;
	}

	public static Tarjeta fromTarjetaAsociadoDTO(TarjetaDTO tarDTO) {
		Tarjeta tar = fromTarjetaDTO(tarDTO);
		return tar;
	}
}
