package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the descuento_directivo database table.
 * 
 */
@Entity
@Table(name = "descuento_directivo", schema = "factura_cliente")
@NamedQuery(name="DescuentoDirectivo.findAll", query="SELECT d FROM DescuentoDirectivo d")
public class DescuentoDirectivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_descuento_directivo")
	private Long idDescuentoDirectivo;

	@Column(name="ci_dir")
	private String ciDir;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="motivo_desc")
	private String motivoDesc;

	@Column(name="nombre_dir")
	private String nombreDir;

	@Column(name="porc_desc")
	private BigDecimal porcDesc;

	//bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	//bi-directional many-to-one association to FacturaClienteCab
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab")
//	private FacturaClienteCab facturaClienteCab2;

	public DescuentoDirectivo() {
	}

	public Long getIdDescuentoDirectivo() {
		return this.idDescuentoDirectivo;
	}

	public void setIdDescuentoDirectivo(Long idDescuentoDirectivo) {
		this.idDescuentoDirectivo = idDescuentoDirectivo;
	}

	public String getCiDir() {
		return this.ciDir;
	}

	public void setCiDir(String ciDir) {
		this.ciDir = ciDir;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public String getMotivoDesc() {
		return this.motivoDesc;
	}

	public void setMotivoDesc(String motivoDesc) {
		this.motivoDesc = motivoDesc;
	}

	public String getNombreDir() {
		return this.nombreDir;
	}

	public void setNombreDir(String nombreDir) {
		this.nombreDir = nombreDir;
	}

	public BigDecimal getPorcDesc() {
		return this.porcDesc;
	}

	public void setPorcDesc(BigDecimal porcDesc) {
		this.porcDesc = porcDesc;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}