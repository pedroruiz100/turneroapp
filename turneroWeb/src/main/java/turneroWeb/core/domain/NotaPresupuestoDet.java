package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the nota_presupuesto_det database table.
 * 
 */
@Entity
@Table(name = "nota_presupuesto_det", schema = "factura_cliente")
@NamedQuery(name = "NotaPresupuestoDet.findAll", query = "SELECT n FROM NotaPresupuestoDet n")
public class NotaPresupuestoDet implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_nota_presupuesto_det")
	private Long idNotaPresupuestoDet;

	private Boolean bajada;

	private BigDecimal cantidad;

	@Column(name = "cod_articulo")
	private Long codArticulo;

	private String descripcion;

	@Column(name = "dto_may_aplicado")
	private BigDecimal dtoMayAplicado;

	@Column(name = "id_articulo")
	private Long idArticulo;

	@Column(name = "monto_neto")
	private Long montoNeto;

	private Integer orden;

	@Column(name = "permite_desc")
	private Boolean permiteDesc;

	private Integer poriva;

	private Long precio;

	private String seccion;

	@Column(name = "seccion_sub")
	private String seccionSub;

	// bi-directional many-to-one association to NotaPresupuestoCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_nota_presupuesto_cab")
	private NotaPresupuestoCab notaPresupuestoCab;

	public NotaPresupuestoDet() {
	}

	public Long getIdNotaPresupuestoDet() {
		return this.idNotaPresupuestoDet;
	}

	public void setIdNotaPresupuestoDet(Long idNotaPresupuestoDet) {
		this.idNotaPresupuestoDet = idNotaPresupuestoDet;
	}

	public Boolean getBajada() {
		return this.bajada;
	}

	public void setBajada(Boolean bajada) {
		this.bajada = bajada;
	}

	public BigDecimal getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(BigDecimal cantidad) {
		this.cantidad = cantidad;
	}

	public Long getCodArticulo() {
		return this.codArticulo;
	}

	public void setCodArticulo(Long codArticulo) {
		this.codArticulo = codArticulo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public BigDecimal getDtoMayAplicado() {
		return this.dtoMayAplicado;
	}

	public void setDtoMayAplicado(BigDecimal dtoMayAplicado) {
		this.dtoMayAplicado = dtoMayAplicado;
	}

	public Long getIdArticulo() {
		return this.idArticulo;
	}

	public void setIdArticulo(Long idArticulo) {
		this.idArticulo = idArticulo;
	}

	public Long getMontoNeto() {
		return this.montoNeto;
	}

	public void setMontoNeto(Long montoNeto) {
		this.montoNeto = montoNeto;
	}

	public Integer getOrden() {
		return this.orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public Boolean getPermiteDesc() {
		return this.permiteDesc;
	}

	public void setPermiteDesc(Boolean permiteDesc) {
		this.permiteDesc = permiteDesc;
	}

	public Integer getPoriva() {
		return this.poriva;
	}

	public void setPoriva(Integer poriva) {
		this.poriva = poriva;
	}

	public Long getPrecio() {
		return this.precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public String getSeccion() {
		return this.seccion;
	}

	public void setSeccion(String seccion) {
		this.seccion = seccion;
	}

	public String getSeccionSub() {
		return this.seccionSub;
	}

	public void setSeccionSub(String seccionSub) {
		this.seccionSub = seccionSub;
	}

	public NotaPresupuestoCab getNotaPresupuestoCab() {
		return this.notaPresupuestoCab;
	}

	public void setNotaPresupuestoCab(NotaPresupuestoCab notaPresupuestoCab) {
		this.notaPresupuestoCab = notaPresupuestoCab;
	}

}