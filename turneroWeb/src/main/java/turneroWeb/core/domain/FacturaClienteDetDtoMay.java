package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;

/**
 * The persistent class for the factura_cliente_det_dto_may database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_det_dto_may", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteDetDtoMay.findAll", query = "SELECT f FROM FacturaClienteDetDtoMay f")
public class FacturaClienteDetDtoMay implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_det_dto_may")
	private Long idFacturaClienteDetDtoMay;

	@Column(name = "dto_may_aplicado")
	private BigDecimal dtoMayAplicado;

	@Column(name = "monto_neto")
	private Long montoNeto;

	// bi-directional many-to-one association to FacturaClienteDet
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_det")
	private FacturaClienteDet facturaClienteDet;

	public FacturaClienteDetDtoMay() {
	}

	public Long getIdFacturaClienteDetDtoMay() {
		return this.idFacturaClienteDetDtoMay;
	}

	public void setIdFacturaClienteDetDtoMay(Long idFacturaClienteDetDtoMay) {
		this.idFacturaClienteDetDtoMay = idFacturaClienteDetDtoMay;
	}

	public BigDecimal getDtoMayAplicado() {
		return this.dtoMayAplicado;
	}

	public void setDtoMayAplicado(BigDecimal dtoMayAplicado) {
		this.dtoMayAplicado = dtoMayAplicado;
	}

	public Long getMontoNeto() {
		return this.montoNeto;
	}

	public void setMontoNeto(Long montoNeto) {
		this.montoNeto = montoNeto;
	}

	public FacturaClienteDet getFacturaClienteDet() {
		return this.facturaClienteDet;
	}

	public void setFacturaClienteDet(FacturaClienteDet facturaClienteDet) {
		this.facturaClienteDet = facturaClienteDet;
	}

}