package turneroWeb.core.domain;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.ArticuloDTO;

@Entity
@Table(name = "articulo", schema = "stock")
@NamedQuery(name = "Articulo.findAll", query = "SELECT a FROM Articulo a")
public class Articulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_articulo")
	private Long idArticulo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_iva")
	private Iva iva;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_unidad")
	private Unidad unidad;

	@Column(name = "descripcion")
	private String descripcion;

	@Column(name = "precio_may")
	private Long precioMay;

	@Column(name = "precio_min")
	private Long precioMin;

	@Column(name = "usu_alta")
	private String usuAlta;

	@Column(name = "fecha_alta")
	private Timestamp fechaAlta;

	@Column(name = "usu_mod")
	private String usuMod;

	@Column(name = "fecha_mod")
	private Timestamp fechaMod;

	@Column(name = "permite_desc")
	private Boolean permiteDesc;
	
	@Column(name = "stock_actual")
	private String stockActual;

	@Column(name = "cod_articulo", unique = true)
	private String codArticulo;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_seccion")
	private Seccion seccion;

	@Column(name = "servicio")
	private Boolean servicio;

	@Column(name = "stockeable")
	private Boolean stockeable;

	@Column(name = "importado")
	private Boolean importado;

	@Column(name = "bajada")
	private Boolean bajada;

	@Column(name = "costo")
	private Long costo;

	@Column(name = "pack")
	private String pack;

	@Column(name = "estante")
	private String estante;

	@Column(name = "minimo")
	private String minimo;

	@Column(name = "maximo")
	private String maximo;

	@Column(name = "dto_mayorista")
	private String dtoMayorista;

	@Column(name = "dto_minorista")
	private String dtoMinorista;

	@Column(name = "observacion")
	private String observacion;

	@Column(name = "imagen")
	private byte[] imagen;

	public Articulo() {
		super();
	}

	public Long getIdArticulo() {
		return idArticulo;
	}

	public void setIdArticulo(Long idArticulo) {
		this.idArticulo = idArticulo;
	}

	public Iva getIva() {
		return iva;
	}

	public void setIva(Iva iva) {
		this.iva = iva;
	}

	public Unidad getUnidad() {
		return unidad;
	}

	public void setUnidad(Unidad unidad) {
		this.unidad = unidad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public String getStockActual() {
		return stockActual;
	}

	public void setStockActual(String stockActual) {
		this.stockActual = stockActual;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Long getPrecioMay() {
		return precioMay;
	}

	public void setPrecioMay(Long precioMay) {
		this.precioMay = precioMay;
	}

	public Long getPrecioMin() {
		return precioMin;
	}

	public void setPrecioMin(Long precioMin) {
		this.precioMin = precioMin;
	}

	public String getUsuAlta() {
		return usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public Timestamp getFechaAlta() {
		return fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public String getUsuMod() {
		return usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public Timestamp getFechaMod() {
		return fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Boolean getPermiteDesc() {
		return permiteDesc;
	}

	public void setPermiteDesc(Boolean permiteDesc) {
		this.permiteDesc = permiteDesc;
	}

	public String getCodArticulo() {
		return codArticulo;
	}

	public void setCodArticulo(String codArticulo) {
		this.codArticulo = codArticulo;
	}

	public Seccion getSeccion() {
		return seccion;
	}

	public void setSeccion(Seccion seccion) {
		this.seccion = seccion;
	}

	public Boolean getServicio() {
		return servicio;
	}

	public void setServicio(Boolean servicio) {
		this.servicio = servicio;
	}

	public Boolean getStockeable() {
		return stockeable;
	}

	public void setStockeable(Boolean stockeable) {
		this.stockeable = stockeable;
	}

	public Boolean getImportado() {
		return importado;
	}

	public void setImportado(Boolean importado) {
		this.importado = importado;
	}

	public Boolean getBajada() {
		return bajada;
	}

	public void setBajada(Boolean bajada) {
		this.bajada = bajada;
	}

	public Long getCosto() {
		return costo;
	}

	public void setCosto(Long costo) {
		this.costo = costo;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getEstante() {
		return estante;
	}

	public void setEstante(String estante) {
		this.estante = estante;
	}

	public String getMinimo() {
		return minimo;
	}

	public void setMinimo(String minimo) {
		this.minimo = minimo;
	}

	public String getMaximo() {
		return maximo;
	}

	public void setMaximo(String maximo) {
		this.maximo = maximo;
	}

	public String getDtoMayorista() {
		return dtoMayorista;
	}

	public void setDtoMayorista(String dtoMayorista) {
		this.dtoMayorista = dtoMayorista;
	}

	public String getDtoMinorista() {
		return dtoMinorista;
	}

	public void setDtoMinorista(String dtoMinorista) {
		this.dtoMinorista = dtoMinorista;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public byte[] getImagen() {
		return imagen;
	}

	public void setImagen(byte[] imagen) {
		this.imagen = imagen;
	}

	public static ArticuloDTO toArticuloDTO(Articulo articulo) {
		ArticuloDTO arDTO = new ArticuloDTO();
		BeanUtils.copyProperties(articulo, arDTO);
		return arDTO;
	}

	public ArticuloDTO toArticuloDTO() {
		ArticuloDTO arDTO = toArticuloDTO(this);
		// BeanUtils.copyProperties(articulo, arDTO);
		return arDTO;
	}

	public ArticuloDTO toArticuloSecionDTO() {
		ArticuloDTO arDTO = toArticuloDTO(this);
		if (this.getSeccion() != null) {
			arDTO.setSeccion(this.getSeccion().toSeccionDTO());
		}
		// BeanUtils.copyProperties(articulo, arDTO);
		return arDTO;
	}

	public static Articulo fromArticuloDTOAsociado(ArticuloDTO articuloDTO) {
		Articulo ar = fromArticuloDTO(articuloDTO);
		return ar;
	}

	public static Articulo fromArticuloDTOAsociadoUpdate(ArticuloDTO articuloDTO) {
		Articulo ar = fromArticuloDTO(articuloDTO);
		return ar;
	}

	public static Articulo fromArticuloDTO(ArticuloDTO articuloDTO) {
		Articulo ar = new Articulo();
		BeanUtils.copyProperties(articuloDTO, ar);
		return ar;
	}

	public static Articulo fromArticuloDTONoList(ArticuloDTO entidadDTO) {
		Articulo articulo = fromArticuloDTO(entidadDTO);
		return articulo;
	}

}
