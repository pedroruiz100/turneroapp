package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the factura_cliente_cab_retencion database table.
 * 
 */
@Entity
@Table(name = "factura_cliente_cab_retencion", schema = "factura_cliente")
@NamedQuery(name = "FacturaClienteCabRetencion.findAll", query = "SELECT f FROM FacturaClienteCabRetencion f")
public class FacturaClienteCabRetencion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_factura_cliente_cab_retencion")
	private Long idFacturaClienteCabRetencion;

	private Integer monto;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	public FacturaClienteCabRetencion() {
	}

	public Long getIdFacturaClienteCabRetencion() {
		return this.idFacturaClienteCabRetencion;
	}

	public void setIdFacturaClienteCabRetencion(
			Long idFacturaClienteCabRetencion) {
		this.idFacturaClienteCabRetencion = idFacturaClienteCabRetencion;
	}

	public Integer getMonto() {
		return this.monto;
	}

	public void setMonto(Integer monto) {
		this.monto = monto;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab1) {
		this.facturaClienteCab = facturaClienteCab1;
	}

}