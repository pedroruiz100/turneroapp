package turneroWeb.core.domain;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the nota_presupuesto_cab database table.
 * 
 */
@Entity
@Table(name="nota_presupuesto_cab", schema = "factura_cliente")
@NamedQuery(name="NotaPresupuestoCab.findAll", query="SELECT n FROM NotaPresupuestoCab n")
public class NotaPresupuestoCab implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_nota_presupuesto_cab")
	private Long idNotaPresupuestoCab;

	@Column(name="fecha_emision")
	private Timestamp fechaEmision;

	@Column(name="fecha_mod")
	private Timestamp fechaMod;

	private Boolean habilitado;

	@Column(name="id_caja")
	private Long idCaja;

	@Column(name="id_cliente")
	private Long idCliente;

	@Column(name="id_sucursal")
	private Long idSucursal;

	@Column(name="id_vendedor_may")
	private Long idVendedorMay;

	@Column(name="monto_factura")
	private Integer montoFactura;

	@Column(name="nro_nota_presupuesto")
	private String nroNotaPresupuesto;

	@Column(name="usu_alta")
	private String usuAlta;

	@Column(name="usu_mod")
	private String usuMod;

	//bi-directional many-to-one association to TipoComprobante
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_tipo_comprobante")
	private TipoComprobante tipoComprobante;

	//bi-directional many-to-one association to NotaPresupuestoDet
	@OneToMany(mappedBy="notaPresupuestoCab")
	private List<NotaPresupuestoDet> notaPresupuestoDets;

	public NotaPresupuestoCab() {
	}

	public Long getIdNotaPresupuestoCab() {
		return this.idNotaPresupuestoCab;
	}

	public void setIdNotaPresupuestoCab(Long idNotaPresupuestoCab) {
		this.idNotaPresupuestoCab = idNotaPresupuestoCab;
	}

	public Timestamp getFechaEmision() {
		return this.fechaEmision;
	}

	public void setFechaEmision(Timestamp fechaEmision) {
		this.fechaEmision = fechaEmision;
	}

	public Timestamp getFechaMod() {
		return this.fechaMod;
	}

	public void setFechaMod(Timestamp fechaMod) {
		this.fechaMod = fechaMod;
	}

	public Boolean getHabilitado() {
		return this.habilitado;
	}

	public void setHabilitado(Boolean habilitado) {
		this.habilitado = habilitado;
	}

	public Long getIdCaja() {
		return this.idCaja;
	}

	public void setIdCaja(Long idCaja) {
		this.idCaja = idCaja;
	}

	public Long getIdCliente() {
		return this.idCliente;
	}

	public void setIdCliente(Long idCliente) {
		this.idCliente = idCliente;
	}

	public Long getIdSucursal() {
		return this.idSucursal;
	}

	public void setIdSucursal(Long idSucursal) {
		this.idSucursal = idSucursal;
	}

	public Long getIdVendedorMay() {
		return this.idVendedorMay;
	}

	public void setIdVendedorMay(Long idVendedorMay) {
		this.idVendedorMay = idVendedorMay;
	}

	public Integer getMontoFactura() {
		return this.montoFactura;
	}

	public void setMontoFactura(Integer montoFactura) {
		this.montoFactura = montoFactura;
	}

	public String getNroNotaPresupuesto() {
		return this.nroNotaPresupuesto;
	}

	public void setNroNotaPresupuesto(String nroNotaPresupuesto) {
		this.nroNotaPresupuesto = nroNotaPresupuesto;
	}

	public String getUsuAlta() {
		return this.usuAlta;
	}

	public void setUsuAlta(String usuAlta) {
		this.usuAlta = usuAlta;
	}

	public String getUsuMod() {
		return this.usuMod;
	}

	public void setUsuMod(String usuMod) {
		this.usuMod = usuMod;
	}

	public TipoComprobante getTipoComprobante() {
		return this.tipoComprobante;
	}

	public void setTipoComprobante(TipoComprobante tipoComprobante) {
		this.tipoComprobante = tipoComprobante;
	}

	public List<NotaPresupuestoDet> getNotaPresupuestoDets() {
		return this.notaPresupuestoDets;
	}

	public void setNotaPresupuestoDets(List<NotaPresupuestoDet> notaPresupuestoDets) {
		this.notaPresupuestoDets = notaPresupuestoDets;
	}

	public NotaPresupuestoDet addNotaPresupuestoDet(NotaPresupuestoDet notaPresupuestoDet) {
		getNotaPresupuestoDets().add(notaPresupuestoDet);
		notaPresupuestoDet.setNotaPresupuestoCab(this);

		return notaPresupuestoDet;
	}

	public NotaPresupuestoDet removeNotaPresupuestoDet(NotaPresupuestoDet notaPresupuestoDet) {
		getNotaPresupuestoDets().remove(notaPresupuestoDet);
		notaPresupuestoDet.setNotaPresupuestoCab(null);

		return notaPresupuestoDet;
	}

}