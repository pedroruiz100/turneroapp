package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the desc_tarjeta database table.
 * 
 */
@Entity
@Table(name = "desc_tarjeta", schema = "factura_cliente")
@NamedQuery(name="DescTarjeta.findAll", query="SELECT d FROM DescTarjeta d")
public class DescTarjeta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_tarjeta")
	private Long idDescTarjeta;

	@Column(name="id_descuento_tarjeta_cab")
	private Long idDescuentoTarjetaCab;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabTarjeta
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_tarjeta")
	private FacturaClienteCabTarjeta facturaClienteCabTarjeta;

	//bi-directional many-to-one association to FacturaClienteCabTarjeta
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_tarjeta")
//	private FacturaClienteCabTarjeta facturaClienteCabTarjeta2;

	public DescTarjeta() {
	}

	public Long getIdDescTarjeta() {
		return this.idDescTarjeta;
	}

	public void setIdDescTarjeta(Long idDescTarjeta) {
		this.idDescTarjeta = idDescTarjeta;
	}

	public Long getIdDescuentoTarjetaCab() {
		return this.idDescuentoTarjetaCab;
	}

	public void setIdDescuentoTarjetaCab(Long idDescuentoTarjetaCab) {
		this.idDescuentoTarjetaCab = idDescuentoTarjetaCab;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabTarjeta getFacturaClienteCabTarjeta() {
		return this.facturaClienteCabTarjeta;
	}

	public void setFacturaClienteCabTarjeta(FacturaClienteCabTarjeta facturaClienteCabTarjeta) {
		this.facturaClienteCabTarjeta = facturaClienteCabTarjeta;
	}

	// public FacturaClienteCabTarjeta getFacturaClienteCabTarjeta2() {
	// return this.facturaClienteCabTarjeta2;
	// }
	//
	// public void setFacturaClienteCabTarjeta2(FacturaClienteCabTarjeta
	// facturaClienteCabTarjeta2) {
	// this.facturaClienteCabTarjeta2 = facturaClienteCabTarjeta2;
	// }

}