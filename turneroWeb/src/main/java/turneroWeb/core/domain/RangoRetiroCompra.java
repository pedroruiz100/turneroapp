package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.RangoRetiroCompraDTO;

/**
 * The persistent class for the dias database table.
 *
 */
@Entity
@Table(name = "rango_retiro_compra", schema = "general")
@NamedQuery(name = "RangoRetiroCompra.findAll", query = "SELECT d FROM RangoRetiroCompra d")
public class RangoRetiroCompra implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_rango")
	private Long idRango;

	@Column(name = "serie")
	private String serie;

	@Column(name = "rango_inicial")
	private Long rangoInicial;

	@Column(name = "rango_final")
	private Long rangoFinal;

	@Column(name = "rango_actual")
	private Long rangoActual;

	public RangoRetiroCompra() {
	}

	public Long getIdRango() {
		return idRango;
	}

	public void setIdRango(Long idRango) {
		this.idRango = idRango;
	}

	public Long getRangoInicial() {
		return rangoInicial;
	}

	public void setRangoInicial(Long rangoInicial) {
		this.rangoInicial = rangoInicial;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public Long getRangoFinal() {
		return rangoFinal;
	}

	public void setRangoFinal(Long rangoFinal) {
		this.rangoFinal = rangoFinal;
	}

	public Long getRangoActual() {
		return rangoActual;
	}

	public void setRangoActual(Long rangoActual) {
		this.rangoActual = rangoActual;
	}

	public RangoRetiroCompraDTO toRangoRetiroCompraDTO() {
		RangoRetiroCompraDTO rangoDTO = toRangoRetiroCompraDTO(this);
		return rangoDTO;
	}

	public static RangoRetiroCompraDTO toRangoRetiroCompraDTO(
			RangoRetiroCompra rango) {
		RangoRetiroCompraDTO dDTO = new RangoRetiroCompraDTO();
		BeanUtils.copyProperties(rango, dDTO);
		return dDTO;
	}

	public static RangoRetiroCompra fromRangoRetiroCompraDTO(
			RangoRetiroCompraDTO rangoDTO) {
		RangoRetiroCompra rango = new RangoRetiroCompra();
		BeanUtils.copyProperties(rangoDTO, rango);
		return rango;
	}

}
