package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


/**
 * The persistent class for the desc_tarjeta_convenio database table.
 * 
 */
@Entity
@Table(name = "desc_tarjeta_convenio", schema = "factura_cliente")
@NamedQuery(name="DescTarjetaConvenio.findAll", query="SELECT d FROM DescTarjetaConvenio d")
public class DescTarjetaConvenio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_desc_tarjeta_convenio")
	private Long idDescTarjetaConvenio;

	@Column(name="monto_desc")
	private Integer montoDesc;

	@Column(name="porcentaje_desc")
	private BigDecimal porcentajeDesc;

	//bi-directional many-to-one association to FacturaClienteCabTarjConvenio
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="id_factura_cliente_cab_tarjeta_convenio")
	private FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio;

	//bi-directional many-to-one association to FacturaClienteCabTarjConvenio
//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name="id_factura_cliente_cab_tarjeta_convenio")
//	private FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio2;

	public DescTarjetaConvenio() {
	}

	public Long getIdDescTarjetaConvenio() {
		return this.idDescTarjetaConvenio;
	}

	public void setIdDescTarjetaConvenio(Long idDescTarjetaConvenio) {
		this.idDescTarjetaConvenio = idDescTarjetaConvenio;
	}

	public Integer getMontoDesc() {
		return this.montoDesc;
	}

	public void setMontoDesc(Integer montoDesc) {
		this.montoDesc = montoDesc;
	}

	public BigDecimal getPorcentajeDesc() {
		return this.porcentajeDesc;
	}

	public void setPorcentajeDesc(BigDecimal porcentajeDesc) {
		this.porcentajeDesc = porcentajeDesc;
	}

	public FacturaClienteCabTarjConvenio getFacturaClienteCabTarjConvenio() {
		return this.facturaClienteCabTarjConvenio;
	}

	public void setFacturaClienteCabTarjConvenio(FacturaClienteCabTarjConvenio facturaClienteCabTarjConvenio) {
		this.facturaClienteCabTarjConvenio = facturaClienteCabTarjConvenio;
	}

	// public FacturaClienteCabTarjConvenio getFacturaClienteCabTarjConvenio2()
	// {
	// return this.facturaClienteCabTarjConvenio2;
	// }
	//
	// public void
	// setFacturaClienteCabTarjConvenio2(FacturaClienteCabTarjConvenio
	// facturaClienteCabTarjConvenio2) {
	// this.facturaClienteCabTarjConvenio2 = facturaClienteCabTarjConvenio2;
	// }

}