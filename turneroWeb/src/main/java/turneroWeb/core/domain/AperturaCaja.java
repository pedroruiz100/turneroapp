package turneroWeb.core.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import turneroWeb.core.dto.AperturaCajaDTO;

@Entity
@Table(name = "apertura_caja", schema = "caja")
@NamedQuery(name = "AperturaCaja.findAll", query = "SELECT a FROM AperturaCaja a")
public class AperturaCaja implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_apertura")
	private Long idApertura;

	@Column(name = "diferencia_apertura")
	private Integer diferenciaApertura;

	@Column(name = "fecha_apertura")
	private Timestamp fechaApertura;

	@Column(name = "monto_apertura")
	private BigDecimal montoApertura;

	@Column(name = "monto_caja")
	private Integer montoCaja;

	// bi-directional many-to-one association to Caja
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_caja")
	private Caja caja;

	// bi-directional many-to-one association to Usuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_cajero")
	private Usuario usuarioCajero;

	// bi-directional many-to-one association to Usuario
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_usuario_supervisor")
	private Usuario usuarioSupervisor;

	public AperturaCaja() {
	}

	public Long getIdApertura() {
		return this.idApertura;
	}

	public void setIdApertura(Long idApertura) {
		this.idApertura = idApertura;
	}

	public Integer getDiferenciaApertura() {
		return this.diferenciaApertura;
	}

	public void setDiferenciaApertura(Integer diferenciaApertura) {
		this.diferenciaApertura = diferenciaApertura;
	}

	public Timestamp getFechaApertura() {
		return this.fechaApertura;
	}

	public void setFechaApertura(Timestamp fechaApertura) {
		this.fechaApertura = fechaApertura;
	}

	public BigDecimal getMontoApertura() {
		return this.montoApertura;
	}

	public void setMontoApertura(BigDecimal montoApertura) {
		this.montoApertura = montoApertura;
	}

	public Integer getMontoCaja() {
		return this.montoCaja;
	}

	public void setMontoCaja(Integer montoCaja) {
		this.montoCaja = montoCaja;
	}

	public Caja getCaja() {
		return this.caja;
	}

	public void setCaja(Caja caja) {
		this.caja = caja;
	}

	public Usuario getUsuarioCajero() {
		return this.usuarioCajero;
	}

	public void setUsuarioCajero(Usuario usuarioCajero) {
		this.usuarioCajero = usuarioCajero;
	}

	public Usuario getUsuarioSupervisor() {
		return this.usuarioSupervisor;
	}

	public void setUsuarioSupervisor(Usuario usuarioSupervisor) {
		this.usuarioSupervisor = usuarioSupervisor;
	}

	// DTOs****************************************************************************************
	// RELACION CON UsuarioSupervisor
	public AperturaCajaDTO toAperturaCajaDTO() {
		AperturaCajaDTO apCajaDTO = this.toAperturaCajaDTO(this);
		if (this.getCaja() != null) {
			Caja caja = this.getCaja();
			// Colocar null a los campos que vuelve hacer referencia en los list
			// de la clase Ciudad
			// Para este caso Barrio y Sucursal
//			caja.setAperturaCajas(null);
//			caja.setCierreCajas(null);
//			caja.setFacturaClienteCabs(null);
//			caja.setRetiroDineros(null);
			apCajaDTO.setCaja(caja.toCajaDTO());
		}
		if (this.getUsuarioCajero() != null) {
			Usuario usuCajero = this.getUsuarioCajero();
//			usuCajero.setUsuarioRols(null);
//			usuCajero.setSupervisor(null);
//			usuCajero.setAperturaCajasCajero(null);
//			usuCajero.setAperturaCajasSupervisor(null);
//			usuCajero.setCancelacionFacturasCajero(null);
//			usuCajero.setCancelacionFacturasSupervisor(null);
//			usuCajero.setCancelacionProductosCajero(null);
//			usuCajero.setCancelacionProductosSupervisor(null);
//			usuCajero.setCierreCajasCajero(null);
//			usuCajero.setCierreCajasSupervisor(null);
//			usuCajero.setRetiroDinerosCajero(null);
//			usuCajero.setRetiroDinerosSupervisor(null);
			apCajaDTO.setUsuarioCajero(usuCajero.toUsuarioDTO(usuCajero));
		}
		if (this.getUsuarioSupervisor() != null) {
			Usuario usuSupervisor = this.getUsuarioSupervisor();
//			usuSupervisor.setUsuarioRols(null);
//			usuSupervisor.setSupervisor(null);
//			usuSupervisor.setAperturaCajasCajero(null);
//			usuSupervisor.setAperturaCajasSupervisor(null);
//			usuSupervisor.setCancelacionFacturasCajero(null);
//			usuSupervisor.setCancelacionFacturasSupervisor(null);
//			usuSupervisor.setCancelacionProductosCajero(null);
//			usuSupervisor.setCancelacionProductosSupervisor(null);
//			usuSupervisor.setCierreCajasCajero(null);
//			usuSupervisor.setCierreCajasSupervisor(null);
//			usuSupervisor.setRetiroDinerosCajero(null);
//			usuSupervisor.setRetiroDinerosSupervisor(null);
		}
		return apCajaDTO;
	}

	public AperturaCajaDTO toDescriAperturaCajaDTO() {
		AperturaCajaDTO apDTO = toAperturaCajaDTO(this);
		apDTO.setCaja(null);
		apDTO.setUsuarioCajero(null);
		return apDTO;
	}

	public AperturaCajaDTO toAperturaCajaDTO(AperturaCaja ap) {
		AperturaCajaDTO acDTO = new AperturaCajaDTO();
		BeanUtils.copyProperties(ap, acDTO);
		return acDTO;
	}

	// METODO QUE
	// Se agrega este metodo ya que la relacion la tiene con CAJA, USUARIO
	// CAJERO Y USUARIO SUPERVISOR
	public static AperturaCaja fromAperturaCajaDTO(AperturaCajaDTO dto) {
		AperturaCaja apCa = new AperturaCaja();
		BeanUtils.copyProperties(dto, apCa);
		apCa.setCaja(Caja.fromCajaDTO(dto.getCaja()));
		apCa.setUsuarioCajero(Usuario.fromUsuarioCajeroDTO(dto
				.getUsuarioCajero()));
		return apCa;
	}
}