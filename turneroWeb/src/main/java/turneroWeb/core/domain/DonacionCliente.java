package turneroWeb.core.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * The persistent class for the donacion_cliente database table.
 * 
 */
@Entity
@Table(name = "donacion_cliente", schema = "factura_cliente")
@NamedQuery(name = "DonacionCliente.findAll", query = "SELECT d FROM DonacionCliente d")
public class DonacionCliente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_donacion_cliente")
	private Long idDonacionCliente;

	@Column(name = "monto_donacion")
	private Integer montoDonacion;

	// bi-directional many-to-one association to FacturaClienteCab
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_factura_cliente_cab")
	private FacturaClienteCab facturaClienteCab;

	// bi-directional many-to-one association to FacturaClienteCab
	// @ManyToOne(fetch = FetchType.LAZY)
	// @JoinColumn(name="id_factura_cliente_cab")
	// private FacturaClienteCab facturaClienteCab2;

	public DonacionCliente() {
	}

	public Long getIdDonacionCliente() {
		return this.idDonacionCliente;
	}

	public void setIdDonacionCliente(Long idDonacionCliente) {
		this.idDonacionCliente = idDonacionCliente;
	}

	public Integer getMontoDonacion() {
		return this.montoDonacion;
	}

	public void setMontoDonacion(Integer montoDonacion) {
		this.montoDonacion = montoDonacion;
	}

	public FacturaClienteCab getFacturaClienteCab() {
		return this.facturaClienteCab;
	}

	public void setFacturaClienteCab(FacturaClienteCab facturaClienteCab) {
		this.facturaClienteCab = facturaClienteCab;
	}

	// public FacturaClienteCab getFacturaClienteCab2() {
	// return this.facturaClienteCab2;
	// }
	//
	// public void setFacturaClienteCab2(FacturaClienteCab facturaClienteCab2) {
	// this.facturaClienteCab2 = facturaClienteCab2;
	// }

}